from command import Command
from binascii import hexlify,unhexlify

class ACommand(Command):
    def __init__(self, index, info_handler):
        Command.__init__(self, index, 'A')
        self.info_handler = info_handler
        self.parameters = ({'Value':0, 'Desc':'Get Status'},\
                           {'Value':1, 'Desc':'Send Batch'},\
                           {'Value':2, 'Desc':'Get Current Batch'},\
                           {'Value':3, 'Desc':'Get Previous Batch'},\
                           {'Value':4, 'Desc':'Parameter Download'},\
                           {'Value':5, 'Desc':'Reprint Last Receipt'},\
                           {'Value':6, 'Desc':'Transit Batch Settlement'},\
                           {'Value':7, 'Desc':'Reset Polling'},
                           {'Value':8, 'Desc':'Cancel waiting for card'});

    # process() will automatically be called by command thread
    def process(self):        
        code = self.parameters[self.parm_index]['Value']
        code_str = '%02d'%code
        print 'code='+code_str
        self.send_command('A'+code_str, self.port)
        
        if not code == 8:   # this command does not have a response
            self.receive_response(self.port)
        
            if len(self.command_resp) == 0:
                self.info_handler('A Command failed')
            
                self.info_handler('A Command\n')
                self.info_handler('Status     : ['+self.command_resp[1:3]+']\n')
        
            if code == 0:
                self.info_handler('Serial     : ['+self.command_resp[1:]+']\n')
