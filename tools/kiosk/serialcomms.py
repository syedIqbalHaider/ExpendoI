import serial
import threading
from binascii import hexlify

class SerialComms(threading.Thread):
                  
    def __init__(self, portname, rx_handler, tx_handler):
        self.portname = portname
        self.rx_handler = rx_handler
        self.tx_handler = tx_handler
        self.opened = False
        self.exiting = False
        self.inbuffer = ""
        self.error = ""
        self.lock = threading.Lock()

        try:
            self.port = serial.Serial(self.portname,
                                baudrate=19200,
                                parity=serial.PARITY_NONE,
                                stopbits=serial.STOPBITS_ONE,
                                bytesize=serial.EIGHTBITS,
                                timeout=None,
                                interCharTimeout=0.5)   # 500ms

        except serial.SerialException, e:
            print 'Serial exception'
            self.error = str(e)
            return
        
        self.opened = True
        
        self.port.flushInput()
        
        # Initialise read thread
        threading.Thread.__init__(self)

    def run(self):
        print 'running'
        while self.exiting == False:
            indata = self.port.read()

            if len(indata) > 0:
                self.lock.acquire()
                indata += self.port.read(self.port.inWaiting())
                
                self.rx_handler(indata)
#                print 'Received: '+str(len(indata))+' ['+hexlify(indata)+']'
                
                self.inbuffer += indata
                
                self.lock.release()

    def isOpen(self):
        return self.opened

    def close(self):
        self.exiting = True
        
    def getError(self):
        return self.error

    def send(self, data):
        self.port.write(data)
        self.tx_handler(data)
        