from command import Command

class BCommand(Command):        
    def __init__(self, index, info_handler):
        Command.__init__(self, index, 'B')
        self.info_handler = info_handler
    
    # process() will automatically be called by command thread
    def process(self):
        self.send_command('B', self.port)
        
        self.receive_response(self.port)
        
        if len(self.command_resp) != 26:
            print 'Response is of invalid length!\n'
        
        if self.info_handler == None:
            return
        
        self.info_handler('B Command\n')
        self.info_handler('Status     : ['+self.command_resp[1:3]+']\n')
        self.info_handler('Merchant ID: ['+self.command_resp[3:18]+']\n')
        self.info_handler('Terminal   : ['+self.command_resp[18:]+']\n')
