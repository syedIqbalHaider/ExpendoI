#!/usr/bin/python

import wx
import serialcomms
import command
import time

from binascii import hexlify

import B_Command
import A_Command
import C_Command

VERSION_MAJOR = 1
VERSION_MINOR = 0

FRAME_X = 1050
FRAME_Y = 700

class KioskFrame(wx.Frame):

    def __init__(self, title):
        super(KioskFrame, self).__init__(None, title=title, style=wx.MINIMIZE_BOX|wx.CLOSE_BOX|wx.CAPTION, size=(FRAME_X, FRAME_Y))

        self.connectStatus = False

        vbox = wx.BoxSizer(wx.VERTICAL)
        hbox = wx.BoxSizer(wx.HORIZONTAL)

        # Serial port stuff
        stboxSerial = wx.StaticBox(self, label='Serial Port')
        hboxSerial = wx.StaticBoxSizer(stboxSerial, wx.HORIZONTAL)

        self.tcPortName = wx.TextCtrl(self, size=(110,25))
        self.tcPortName.SetValue('/dev/ttyUSB0')

        self.btConnect = wx.Button(self, label='Connect')
        self.btConnect.Bind(wx.EVT_BUTTON, self.__OnbtConnect)

        hboxSerial.Add(wx.StaticText(self, label='Port'), flag=wx.ALIGN_CENTER|wx.ALL & ~wx.RIGHT, border=10)
        hboxSerial.Add(self.tcPortName, flag=wx.ALIGN_CENTER|wx.ALL, border=10)
        hboxSerial.Add(self.btConnect, flag=wx.ALIGN_CENTER|wx.ALL, border=10)
        hbox.Add(hboxSerial, flag=wx.ALL, border=10)

        # Command stuff
        commands=[A_Command.ACommand(1, self.traceInfo),
                  B_Command.BCommand(2, self.traceInfo),
                  C_Command.CCommand(3, self.traceInfo)]

        command_choices = []

        stboxCommand = wx.StaticBox(self, label='Command')
        hboxCommand = wx.StaticBoxSizer(stboxCommand, wx.HORIZONTAL)
        self.listCommand = wx.ComboBox(self, style=wx.CB_DROPDOWN|wx.CB_READONLY, size=(60,29), choices=command_choices)

        for cmd in commands:                                # Populate command objects
            self.listCommand.Append(cmd.command, cmd)

        self.listCommand.Bind(wx.EVT_COMBOBOX, self.__OncommandSelect)

        self.listParameter = wx.ComboBox(self, style=wx.CB_DROPDOWN|wx.CB_READONLY, size=(200,29))
        self.listParameter.Bind(wx.EVT_COMBOBOX, self.__OnparameterSelect)

        self.tcAmount = wx.TextCtrl(self, size=(110,25))
        self.tcAmount.Bind(wx.EVT_TEXT, self.__Ontcamount)

        self.btSend = wx.Button(self, label='Send')
        self.btSend.Bind(wx.EVT_BUTTON, self.__OnbtSend)
        self.btSend.Disable()

        hboxCommand.Add(wx.StaticText(self, label='ID'), flag=wx.ALIGN_CENTER|wx.ALL & ~wx.RIGHT, border=10)
        hboxCommand.Add(self.listCommand, flag=wx.ALIGN_CENTER|wx.ALL, border=10)
        hboxCommand.Add(wx.StaticText(self, label='Parameter'), flag=wx.ALIGN_CENTER|wx.ALL & ~wx.RIGHT, border=10)
        hboxCommand.Add(self.listParameter, flag=wx.ALIGN_CENTER|wx.ALL, border=10)
        hboxCommand.Add(wx.StaticText(self, label='Amount'), flag=wx.ALIGN_CENTER|wx.ALL & ~wx.RIGHT, border=10)
        hboxCommand.Add(self.tcAmount, flag=wx.ALIGN_CENTER|wx.ALL, border=10)
        hboxCommand.Add(self.btSend, flag=wx.ALIGN_CENTER|wx.ALL, border=10)
        hbox.Add(hboxCommand, flag=wx.ALL, border=10)

        vbox.Add(hbox)

        # Trace box
        self.tcTrace = wx.TextCtrl(self, style=wx.TE_READONLY|wx.TE_MULTILINE|wx.HSCROLL, size=(FRAME_X-20,FRAME_Y-120))
        vbox.Add(wx.StaticText(self, label='Trace'), flag=wx.LEFT, border=10)
        vbox.Add(self.tcTrace, flag=wx.ALIGN_CENTER|wx.ALL & ~wx.TOP, border=10)

        # Frame stuff
        self.SetSizer(vbox)

        self.Bind(wx.EVT_CLOSE, self.__OncloseWindow)

        self.Centre()
        self.Show()

    def __OncloseWindow(self, event):
        print 'Main window closed'

        if self.connectStatus == True:
            self.port.close()

        self.Destroy()

    def __OnbtConnect(self, event):
        if event.Id == self.btConnect.Id:
            print 'Connect button pressed for port ['+self.tcPortName.GetValue()+']'
            if self.btConnect.GetLabel() == ('Connect'):
                self.port = serialcomms.SerialComms(self.tcPortName.GetValue(), self.traceOutput, self.traceInput)

                if not self.port.isOpen():
                    print 'debug'
                    wx.MessageBox('Unable to open '+self.tcPortName.GetValue()+'\n'+self.port.getError(), 'Error', wx.OK)
                    return

                # Start he receive thread
                self.port.start()

                self.btConnect.SetLabel('Disconnect')
                self.updateConnectStatus(True)
            else:
                self.btConnect.SetLabel('Connect')
                self.updateConnectStatus(False)
                self.port.close()

    def __OnbtSend(self, event):
        print 'Send button pressed'
        cmd = self.listCommand.GetClientData(self.listCommand.GetSelection())
        params = cmd.get_parameters()
        if len(params) > 0:
            cmd.start_process(self.port, self.listParameter.GetSelection(), self.tcAmount.GetValue())
        else:
            cmd.start_process(self.port, -1, self.tcAmount.GetValue())

    def __OncommandSelect(self, event):
        cmd = self.listCommand.GetClientData(self.listCommand.GetSelection())
        print 'Selected index '+str(cmd.index)+' for command '+cmd.command

        self.listParameter.Clear() # Clear parameter combo box
        self.listParameter.SetValue('')

        params = cmd.get_parameters()
        if len(params) > 0:
            for param in params:
                self.listParameter.Append(param['Desc'], param)

        self.setSendButton()

    def __OnparameterSelect(self, event):
        cmd = self.listCommand.GetClientData(self.listCommand.GetSelection())

        params = cmd.get_parameters()
        param = params[self.listParameter.GetSelection()-1]

        self.setSendButton()

    # Filter non integer values that might get entered
    def __Ontcamount(self, event):
        if event.Id == self.tcAmount.Id:            
            amount_text = self.tcAmount.GetValue()

            if len(amount_text) == 0:
                return                  # This happens when we are mdofiying the text ourselves

            if amount_text[len(amount_text)-1] < '0' or amount_text[len(amount_text)-1] > '9':
                amount_text = amount_text[:len(amount_text)-1]
                self.tcAmount.SetValue(amount_text)

    def traceOutput(self, data):
        trace = time.strftime("%Y-%m-%dT%H:%M:%S", time.gmtime())+' RX --> '+hexlify(data)+'\n'
        wx.CallAfter(self.tcTrace.AppendText, trace)

    def traceInput(self, data):
        trace = time.strftime("%Y-%m-%dT%H:%M:%S", time.gmtime())+' TX <-- '+hexlify(data)+'\n'
        wx.CallAfter(self.tcTrace.AppendText, trace)

    def traceInfo(self, data):
        wx.CallAfter(self.tcTrace.AppendText, data)

    # Do everything that is needed when the connection status changes,
    # instead of changing the variable all over the place
    def updateConnectStatus(self, status):
        self.connectStatus = status

        self.setSendButton()

        print 'Connect stat -> ' + str(self.connectStatus)

    def toggleConnectionStatus(self):
        if self.connectStatus == True:
            self.updateConnectStatus(False)
        else:
            self.updateConnectStatus(True)

    # Enables or disables the send button based on valid selections or connection state
    def setSendButton(self):
        if self.connectStatus == False:
            self.btSend.Disable()
            return

        if self.listCommand.GetSelection() < 0:
            self.btSend.Disable()
            return

        cmd = self.listCommand.GetClientData(self.listCommand.GetSelection())
        params = cmd.get_parameters()
        if len(params) > 0 and self.listParameter.GetSelection() < 0:
            self.btSend.Disable()
            return

        self.btSend.Enable()

def main():
    app = wx.App()

    KioskFrame(title='Kiosk Simulator '+str(VERSION_MAJOR)+'.'+str(VERSION_MINOR))

    app.MainLoop()


if __name__ == '__main__':
    main()
