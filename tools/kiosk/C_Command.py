from command import Command

class CCommand(Command):
    def __init__(self, index, info_handler):
        Command.__init__(self, index, 'C')
        
        self.info_handler = info_handler
        self.parameters = ({'Value':0, 'Desc':'Sale'},\
                           {'Value':1, 'Desc':'Cash Widthdrawel'},\
                           {'Value':9, 'Desc':'Sale With Cash'},\
                           {'Value':20, 'Desc':'Refund'},\
                           {'Value':30, 'Desc':'Pre Deposit'},\
                           {'Value':31, 'Desc':'Online Balance Enquiry'},\
                           {'Value':32, 'Desc':'Transit Data Enquiry'},\
                           {'Value':33, 'Desc':'Offline Balance Enquiry'},\
                           {'Value':38, 'Desc':'Online Balance Enquiry'},\
                           {'Value':39, 'Desc':'Voucher Balance Enquiry'},\
                           {'Value':40, 'Desc':'Nom. Account Transfer'},\
                           {'Value':41, 'Desc':'Card To Card Transfer'},\
                           {'Value':42, 'Desc':'Funds Transfer'},\
                           {'Value':80, 'Desc':'Display and Confirm Amount'},\
                           {'Value':81, 'Desc':'Card Activation'},\
                           {'Value':82, 'Desc':'Display and Confirm PAN'},\
                           {'Value':83, 'Desc':'Voucher Sale'},\
                           {'Value':84, 'Desc':'Cash Load'},\
                           {'Value':85, 'Desc':'Voucher Load'},\
                           {'Value':86, 'Desc':'Load Transit Product'},\
                           {'Value':88, 'Desc':'Unblock Application'},\
                           {'Value':89, 'Desc':'Consession Update'},\
                           {'Value':90, 'Desc':'Nullify Expired Card'},\
                           {'Value':91, 'Desc':'Copy Card'},\
                           {'Value':92, 'Desc':'PIN Change'},\
                           {'Value':93, 'Desc':'Mini Statement'},\
                           {'Value':94, 'Desc':'Account Statement'},\
                           {'Value':95, 'Desc':'CardDiagnostics'});

    # process() will automatically be called by command thread
    def process(self):
        if self.parm_index < 0:
            return
        
        if len(self.data) == 0:
            if self.parm_index == 5 or self.parm_index == 6 or self.parm_index == 7 or self.parm_index == 8:     # Transactions that dont take an amount
                self.data = '0'
            else:
                self.info_handler('Command Error: Expected amount to be entered\n')
                return

        code = self.parameters[self.parm_index]['Value']
        code_str = '%02d'%code
        print 'code='+code_str
        
        if len(self.data) <= 12:
            spaces = '000000000000'
            amount_str = spaces[:12-len(self.data)]+self.data
        else:
            amount_str = self.data[:12]
        
        amount_text = '   Transaction Amount'
        
        self.send_command('C'+code_str+amount_str+amount_text, self.port)
        
        self.receive_response(self.port)
        
        if len(self.command_resp) == 0:
            self.info_handler('C Command failed')
            return
        
        print 'C command completed\n'
