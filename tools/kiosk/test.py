#!/usr/bin/python

import serialcomms
import command
import time
import sys
from binascii import hexlify
import A_Command
import C_Command

class test:
    portname = "/dev/ttyUSB0"
    
    def __init__(self):
        self.port = serialcomms.SerialComms(self.portname, self.traceInput, self.traceOutput)
        
        if not self.port.isOpen():
            print('Unable to open '+portname+'\n')
            sys.exit(1)
            
        self.port.start()
        
    def traceOutput(self, data):
        trace = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())+' TX --> '+hexlify(data)+'\n'
        print(trace)
    
    def traceInput(self, data):
        trace = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())+' RX <-- '+hexlify(data)+'\n'
        print(trace)
        
    def traceInfo(self, data):
        print(data)
        
    def send_status(self):
        acom = A_Command.ACommand(8, self.traceInfo)
        acom.start_process(self.port, 0, 0)

    def send_cancel_insert(self):
#        acom = A_Command.ACommand(8, self.traceInfo)
#        acom.start_process(self.port, 8, 0)
        self.port.send("\x02\x41\x30\x38\x03\x49")
        
    def do_tx(self):
        com = C_Command.CCommand(0, self.traceInfo)
        com.start_process(self.port, 0, "1000")
            
def main():
    app = test()
    count = 1
    
    while True:
        try:
            print("-------------------Test no %d----------------------"%count)
            app.do_tx()
            time.sleep(5)
            
            app.send_cancel_insert()
            time.sleep(5);
            
            count = count + 1
        except KeyboardInterrupt:
            print("Bye\n")
#            app.port.close()
            return
    
if __name__ == '__main__':
    main()