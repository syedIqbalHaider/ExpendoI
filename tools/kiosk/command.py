import threading
import serialcomms
import operator
import time
from datetime import datetime
from binascii import unhexlify, hexlify

class Command(threading.Thread):
    def __init__(self, index, cmd):
        self.index = index
        self.command = cmd
        self.command_out = ''
        self.command_resp = ''
        self.parameters = []
        self.command_complete = False;
        self.thread = threading.Thread
        
    def start_process(self, port, parm_index, data):        
        self.parm_index = parm_index
        self.port = port
        self.data = data
        
        self.thread.__init__(self)
        self.thread.start(self)
        
    def run(self):
        self.process()
        
    def send_command(self, data, port):
        
        if not port.isOpen():
            return 0
        
        self.command_resp = ''
        
        self.command_out = '\x02'
        self.command_out += data
        self.command_out += '\x03'
        self.command_out += chr(reduce(operator.xor, [ord(c) for c in data]))
        
        start = datetime.now()
        count = 0
        
        ack_received = False
        
        # Send until retried 3 times or ACK received
        while True:
            port.send(self.command_out)
            
            while not ack_received:
                current = datetime.now()
#                time.sleep(0.001)
                
                if (current.microsecond - start.microsecond > 1000000) or (count == 3):
                    break;
                
                if len(port.inbuffer) > 0:
                    if(port.inbuffer[0] == chr(6)):
                       print('ACK received\n')
                       ack_received = True
                       port.inbuffer = port.inbuffer[1:]   # Remove byte from buffer
                       count += 1
                    elif (port.inbuffer[0] == chr(15)):
                        print('NACK received\n')
                        port.inbuffer = port.inbuffer[1:]   # Remove byte from buffer
                    else:
                        print('Garbage received\n')
                        port.inbuffer = ''
                    
            if count >= 3:
                return 0
                
            if ack_received:
                break;
            else:
                time.sleep(1)
                count += 1

        if count == 3:
            print 'retry exceeded'
            self.command_complete = True;
            return 0
            
        return len(self.command_out)

    def receive_response(self, port):
        state = 0
        finished = False
        
        data_received = ""
        
        while not finished:
            time.sleep(0.001)
            
            if len(port.inbuffer):
                data_received += port.inbuffer
                port.inbuffer = ''
                
            if(len(data_received) == 0):
                continue
            
            if state == 0:   # Wait for STX
                if data_received[0] == chr(2):
                    state = 1
                    self.log('STX received\n')
                elif data_received[0] == chr(4):
                    self.log('EOT received\n')
                    finished = True
                    
                data_received = data_received[1:]       # Remove byte from buffer
                    
            elif state == 1:    # Wait for ETX
                index = data_received.find(chr(3))
                
                if index >= 0:
                    self.command_resp += data_received[0:index]
                    data_received = data_received[index+1:]
                    state = 2
                    self.log('ETX received\n')
                else:
                    self.command_resp += data_received
                    data_received = ''
                        
            elif state == 2:
                if len(data_received) > 0:
                    rx_lrc = ord(data_received[0])
                    data_received = data_received[1:]
                    
                    calc_lrc = self.lrc(self.command_resp) 
                    self.log('LRC received\n')
                                        
                    if rx_lrc == calc_lrc:
                        if self.info_reponse():
                            state = 0
                            self.command_resp = ''
                        else:
                            finished = True
                            
                        port.send('\x06')
                    else:
                        port.send('\x15')
                        self.command_resp = ''
                        self.log('LRC failed!\n')
                        self.log('rx_lrc:'+str(rx_lrc))
                        self.log('calc_lrc:'+str(calc_lrc))

            else:
                index = data_received.index('\x04')     # Check for EOT
                if index:
                    data_received = data_received[index+1:]
                    self.command_resp = ''
                    finished = True
                    self.command_complete = True;
                    
        self.command_complete = True;
                    
    def log(self, data):
#        print data
        print ''
        
    def get_parameters(self):
        return self.parameters
    
    def info_reponse(self):
        if self.command_resp[0] == 'E':
            status = int(self.command_resp[1:3])
            info_string = self.get_info_string(status)
            self.info_handler('Info:' + info_string + '\n')
            return True
        else:
            return False
        
    def lrc(self, data):
        lrc = 0
        for i in data:
            lrc = lrc ^ ord(i)
            
        return lrc

    def get_info_string(self, status):
        if status == 0:
            return 'Terminal ready'
        elif status == 1:
            return 'Terminal offline'
        elif status == 2:
            return 'Transaction going online'
        elif status == 3:
            return 'Waiting for card insert'
        elif status == 4:
            return 'Card inserted'
        elif status == 5:
            return 'Card removed'
        elif status == 6:
            return 'Card swiped'
        elif status == 7:
            return 'Comms error'
        elif status == 8:
            return 'Waiting for host response'
        elif status == 9:
            return 'Host response received'
        elif status == 10:
            return 'Terminal busy'
        elif status == 11:
            return 'Banking starting'
        elif status == 12:
            return 'Banking complete'
        elif status == 13:
            return 'Awaiting cardholder verification'
        elif status == 14:
            return 'Awaiting amount confimration'
        elif status == 15:
            return 'Awaiting card removal'
        elif status == 16:
            return 'Card velocity exceeded'
        elif status == 17:
            return 'Awaiting account selection'
        elif status == 18:
            return 'Awaiting budget type selection'
        elif status == 19:
            return 'Awaiting budget period selection'
        elif status == 20:
            return 'Batch file empty'
        elif status == 21:
            return 'Parameter download starting'
        elif status == 22:
            return 'Parameter download complete'
        elif status == 23:
            return 'UNDEFINED'
        elif status == 24:
            return 'UNDEFINED'
        elif status == 25:
            return 'Wrong card type'
        elif status == 26:
            return 'PIN does not match fir PIN entered'
        elif status == 27:
            return 'PIN entered'
        elif status == 28:
            return 'PIN incorrect'
        elif status == 29:
            return 'Script successful'
        elif status == 30:
            return 'Do not remove card'
        elif status == 31:
            return 'No comms cable'
        elif status == 32:
            return 'Balance greater than allowed value'
        elif status == 33:
            return 'Balance less than allowed value'
        elif status == 34:
            return 'Payment type not allowed'
        elif status == 35:
            return 'Invalid card'
        elif status == 36:
            return 'Not a PIN based card'
        elif status == 37:
            return 'Card expired'
        elif status == 38:
            return 'Card not expired'
        elif status == 39:
            return 'Script declined'
        elif status == 40:
            return 'Card authentication failed'
        elif status == 41:
            return 'Chip not read'
        else:
            return 'UNDEFINED'