# Makefile - build script */

APPNAME    = uibuild
APPNAMESTR = '"uibuild"'
CFGDIR     = ./cfg
SOURCEDIR  = ./src
INCLUDEDIR = ./include
OBJDIR     = ./obj

#####################################
# Linux makefile definition section #
#####################################
$(info ********************************* Building for Linux platform *********************************)

PLATFORM=_LINUX_

OUTPUTDIR  = ./out/Platform_linux_gcc/debug

# build environment
GNUPREFIX = 

# Libminini
LIBINCLUDES += -I/usr/include/libxml2
LIBDIR      += -L/usr/lib
LIBS        += -lxml2 -lz -lpthread -lm -lboost_system -lboost_filesystem

DISTRODIR  := ./Platform_linux_gcc/debug/Distro
DISTRO_FILES:=$(CFGDIR)/alieno.ini \
              $(OUTPUTDIR)/$(APPNAME)

######################################
# common makefile definition section #
######################################

# source files
SOURCES_ASM := $(wildcard *.S)
SOURCES_C   := $(wildcard *.c)
SOURCES_CPP := $(wildcard *.cpp)

# object files
OBJS=\
		$(OBJDIR)/bertlv.o	\
     $(OBJDIR)/main.o

# Build flags
DEPENDFLAGS := -MD -MP

INCLUDES    += -I$(INCLUDEDIR)
INCLUDES    += $(LIBINCLUDES)

BASEFLAGS   := -O0 -g -fpic -pedantic -pedantic-errors
WARNFLAGS   += -Wall
#WARNFLAGS   += -Werror
ASFLAGS     := $(INCLUDES) $(DEPENDFLAGS) -D__ASSEMBLY__
CFLAGS      := $(INCLUDES) $(DEPENDFLAGS) $(BASEFLAGS) $(WARNFLAGS) -D$(PLATFORM) -DAPPNAME=$(APPNAMESTR)

.PHONY: all

all: $(OBJS)
	$(info ********************************* Linking *********************************)
	$(GNUPREFIX)g++ $(OBJS) $(LIBS) $(LIBDIR) -o $(OUTPUTDIR)/$(APPNAME)
	cp $(SOURCEDIR)/ui.xsd $(OUTPUTDIR)/ui.xsd 

.PHONY: clean

clean:
	rm -f $(OUTPUTDIR)/$(APPNAME) 
	rm -f $(OBJDIR)/*.d
	rm -f $(OBJS)

# C.
#%.c %.o: | $(OUTPUTDIR) $(OBJDIR)
#	$(GNUPREFIX)c99 $(CFLAGS) -c $< -o $@

# C++
%.cpp %.o: | $(OUTPUTDIR) $(OBJDIR)
	$(info ********************************* Compiling C++ *********************************)
	$(GNUPREFIX)g++ $(CFLAGS) -c $< -o $@

.PHONY:$(OBJDIR)

$(OUTPUTDIR):
	test -d $(OUTPUTDIR) || mkdir -p $(OUTPUTDIR)

$(OBJDIR):
	test -d $(OBJDIR) || mkdir -p $(OBJDIR)

$(OBJDIR)/main.o        : $(SOURCEDIR)/main.cpp
$(OBJDIR)/bertlv.o      : $(SOURCEDIR)/bertlv.cpp

######################################
# Distro building definition section #
######################################
.PHONY:$(DISTRODIR)

$(DISTRODIR):
	$(info ********************************* Building distribution *********************************)
	test -d $(DISTRODIR) || mkdir -p $(DISTRODIR)

BUILD_DISTRO = $(patsubst %,$(DISTRODIR)/%,$(DISTRO_FILES))

$(DISTRODIR)/%: %
	cp $< $(DISTRODIR)/$(notdir $@)

.PHONY:distro

distro: $(DISTRODIR) $(BUILD_DISTRO)
	
###########################
# Help definition section #
###########################
.PHONY:help

help:
	$(info Supported platforms:)
	$(info Vos: platform=vos)
	$(info Linux: platform=linux)
	$(info add 'clean' to do a clean build for the particular platform)
	$(info add 'distro' to build a distribution for a platform)
