#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sstream>
#include <iostream>
#include <fstream>
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include <boost/filesystem.hpp>
#include "bertlv.h"
#include "tags.h"

#define VERSION_MAJOR					1
#define VERSION_MINOR					0
//#define VERSION_REV					0
//#define VERSION_REV					1
// 1. Added root_path to the device element
//#define VERSION_REV					2
// 1. Added STRING template
//#define VERSION_REV					3
// 1. Added report templates
#define VERSION_REV					4
// 1. Added proper return codes so we can be called from a makefile

#define EL_NAME_DEVICE				"DEVICE"
#define EL_NAME_LANGUAGE			"LANGUAGE"
#define EL_NAME_DISPLAY				"DISPLAY"
#define EL_NAME_DGTMPL				"DGTMPL"
#define EL_NAME_IMAGEURL			"IMAGEURL"
#define EL_NAME_HTML				"html"
#define EL_NAME_STRING				"STRING"
#define EL_NAME_REPORT				"REPORT"
#define EL_NAME_LINE				"LINE"

#define PROP_NAME_TERM_TYPE		"terminal_type"
#define PROP_NAME_DISP_ROWS		"disp_rows"
#define PROP_NAME_DISP_COLS		"disp_cols"
#define PROP_NAME_PRN_WIDTH		"prn_width"
#define PROP_NAME_ID			"id"
#define PROP_NAME_LANG_NAME		"lang_name"
#define PROP_NAME_LANG_CODE		"lang_code"
#define PROP_NAME_PATH			"path"
#define PROP_NAME_ROOT_PATH		"root_path"
#define PROP_NAME_TEXT			"text"
#define PROP_NAME_JUSTIFY		"justify"
#define PROP_NAME_FONT			"font"
#define PROP_NAME_VALUE			"value"

#define PROP_VAL_LEFT			"LEFT"
#define PROP_VAL_CENTRE			"CENTRE"
#define PROP_VAL_RIGHT			"RIGHT"
#define PROP_VAL_NORMAL			"NORMAL"
#define PROP_VAL_WIDE			"WIDE"
#define PROP_VAL_HIGH			"HIGH"
#define PROP_VAL_INVERSE		"INVERSE"

//using namespace std;
using namespace za_co_verifone_bertlv;
using namespace boost::filesystem;

string disp_hex(string &data)
{
	stringstream ss;

	ss << hex;

	for (int i = 0; i < (int)data.length(); ++i) {
			if(static_cast<unsigned int>(static_cast<unsigned char>(data[i])) <= 0x0f)
				ss << '0';
			ss << static_cast<unsigned int>(static_cast<unsigned char>(data[i]));
	}

	return ss.str();
}

string Trim(const string& s)
{
	return s;
/*
	stringstream ss;

	for(int i=0; i<(int)s.length(); i++){
		if((s.at(i) != ' ') && (s.at(i) != '\t') && (s.at(i) != '\r') && (s.at(i) != '\n'))
			ss << (char)s.at(i);
	}

	return ss.str();
*/
}

string generate_version()
{
	const int year = 1900;
	time_t ltime;
  struct tm *date_time;
  std::stringstream ss;

	time(&ltime);		// Get time
	date_time = localtime(&ltime);	// Convert to local time

	ss.width(4);
	ss.fill('0');
  ss << year+date_time->tm_year;

  ss.width(2);
  ss.fill('0');
  ss << date_time->tm_mon+1;
  ss << date_time->tm_mday;
  ss << date_time->tm_hour;
  ss << date_time->tm_min;
  ss << date_time->tm_sec;

  ss.width(1);
  ss << date_time->tm_wday;

  return ss.str();
}

int parse_line(xmlNodePtr node, BERTLV &tlv)
{
	xmlAttrPtr attr;
	xmlChar *cprop;
	BERTLV prop;

	attr = xmlCopyPropList(node, node->properties);

	while(attr != NULL){
		cprop = xmlGetProp(node, attr->name);
		printf("\tLINE prop[%s]:[%s]\n", attr->name, (char*)cprop);

		if(strcmp((char*)attr->name, PROP_NAME_TEXT) == 0){
			prop.addTag(TUI_PROP_TEXT, string((char*)cprop));
		}
		else if(strcmp((char*)attr->name, PROP_NAME_JUSTIFY) == 0){
			string val=string((char*)cprop);

			if(val.compare(PROP_VAL_LEFT) == 0){
				prop.addTag(TUI_PROP_JUSTIFY, string(TUI_VAL_LEFT));
			}else if(val.compare(PROP_VAL_CENTRE) == 0){
				prop.addTag(TUI_PROP_JUSTIFY, string(TUI_VAL_CENTRE));
			}else if(val.compare(PROP_VAL_RIGHT) == 0){
				prop.addTag(TUI_PROP_JUSTIFY, string(TUI_VAL_RIGHT));
			}
		}else if(strcmp((char*)attr->name, PROP_NAME_FONT) == 0){
			string val=string((char*)cprop);

			if(val.compare(PROP_VAL_NORMAL) == 0){
				prop.addTag(TUI_PROP_JUSTIFY, string(TUI_VAL_NORMAL));
			}else if(val.compare(PROP_VAL_HIGH) == 0){
				prop.addTag(TUI_PROP_JUSTIFY, string(TUI_VAL_HIGH));
			}else if(val.compare(PROP_VAL_WIDE) == 0){
				prop.addTag(TUI_PROP_JUSTIFY, string(TUI_VAL_WIDE));
			}else if(val.compare(PROP_VAL_INVERSE) == 0){
				prop.addTag(TUI_PROP_JUSTIFY, string(TUI_VAL_INVERSE));
			}
		}else if(strcmp((char*)attr->name, PROP_NAME_VALUE) == 0)
			prop.addTag(TUI_PROP_VALUE, string((char*)cprop));

		attr = attr->next;
	}

	tlv.appendTag(TUI_LINE, prop.getStringData());

	return 0;

}

int parse_report(xmlNodePtr node, BERTLV &tlv)
{
	xmlChar *cid;
	BERTLV report;

	cid = xmlGetProp(node, (const xmlChar*)PROP_NAME_ID);

	if(cid == NULL)
		return -1;

	report.addTag(TUI_PROP_ID, (char*)cid);

	printf("REPORT id:[%s]\n", (char*)cid);

	node = xmlFirstElementChild(node);

	while (node != NULL)
	{
		// Ignore text nodes
		if(xmlNodeIsText(node) == 0){
			printf("\t");

			if(strcmp((char*)node->name, EL_NAME_LINE) == 0){
				string content = (char*)xmlNodeGetContent(node);
				parse_line(node, report);
			}
		}

		node = node->next;
	}

	tlv.appendTag(TUI_REPORT, report.getStringData());

	return 0;
}

int parse_string(xmlNodePtr node, BERTLV &tlv)
{
	xmlAttrPtr attr;
	xmlChar *cprop;
	BERTLV prop;

	attr = xmlCopyPropList(node, node->properties);

	while(attr != NULL){
		cprop = xmlGetProp(node, attr->name);
		printf("STRING prop[%s]:[%s]\n", attr->name, (char*)cprop);

		if(strcmp((char*)attr->name, PROP_NAME_ID) == 0)
			prop.addTag(TUI_PROP_ID, string((char*)cprop));
		else if(strcmp((char*)attr->name, PROP_NAME_TEXT) == 0)
			prop.addTag(TUI_PROP_TEXT, string((char*)cprop));

		attr = attr->next;
	}

	tlv.appendTag(TUI_STRING, prop.getStringData());

	return 0;
}

int parse_device(xmlNodePtr node, BERTLV &tlv)
{
	xmlAttrPtr attr;
	xmlChar *cprop;
	BERTLV prop;

	attr = xmlCopyPropList(node, node->properties);

	while(attr != NULL){
		cprop = xmlGetProp(node, attr->name);
		printf("DEVICE prop[%s]:[%s]\n", attr->name, (char*)cprop);

		if(strcmp((char*)attr->name, PROP_NAME_ROOT_PATH) == 0)
			prop.addTag(TUI_PROP_ROOT_PATH, string((char*)cprop));
		else if(strcmp((char*)attr->name, PROP_NAME_TERM_TYPE) == 0)
			prop.addTag(TUI_PROP_TERM_TYPE, string((char*)cprop));
		else if(strcmp((char*)attr->name, PROP_NAME_DISP_ROWS) == 0)
			prop.addTag(TUI_PROP_DSP_ROWS, string((char*)cprop));
		else if(strcmp((char*)attr->name, PROP_NAME_DISP_COLS) == 0)
			prop.addTag(TUI_PROP_DSP_COLS, string((char*)cprop));
		else if(strcmp((char*)attr->name, PROP_NAME_PRN_WIDTH) == 0)
			prop.addTag(TUI_PROP_PRN_WIDTH, string((char*)cprop));

		attr = attr->next;
	}

	tlv.appendTag(TUI_DEVICE, prop.getStringData());

	return 0;
}

int parse_imageurl(xmlNodePtr node, BERTLV &tlv)
{
	xmlChar *cid, *cpath;
	BERTLV image;

	cid = xmlGetProp(node, (const xmlChar*)PROP_NAME_ID);

	if(cid == NULL)
		return -1;

	cpath = xmlGetProp(node, (const xmlChar*)PROP_NAME_PATH);

		if(cpath == NULL)
			return -1;

	printf("IMAGEURL id:[%s] path:[%s]\n", (char*)cid, (char*)cpath);

	image.addTag(TUI_PROP_PATH, (char*)cpath);

	ifstream fimage;

	fimage.open((char*)cid, ios::in | ios::binary);

	if(!fimage.is_open()){
		cout << "Failed to open file " << cid << endl;
		return -1;
	}

	// Get the length of the file
	int file_length;
	fimage.seekg(0, fimage.end);
	file_length = fimage.tellg();
	fimage.seekg(0, fimage.beg);

	// Check that length is sane
	if(file_length == 0){
		fimage.close();
		return -2;
	}

	unsigned char *buffer = new unsigned char [file_length];

	fimage.read((char*)buffer, file_length);

	image.addTag(TUI_PROP_DATA, string((char*)buffer, file_length));

	tlv.appendTag(TUI_IMAGE, image.getStringData());

	delete[] buffer;

	fimage.close();

	return 0;
}

int parse_dgtmpl(xmlNodePtr node, BERTLV &tlv)
{
	xmlChar *cid;
	BERTLV disp;

	cid = xmlGetProp(node, (const xmlChar*)"id");

	if(cid == NULL)
		return -1;

	disp.addTag(TUI_PROP_ID, (char*)cid);

	printf("DGTMPL id:[%s]\n", (char*)cid);

	node = xmlFirstElementChild(node);

	while (node != NULL)
	{
		// Ignore text nodes
		if(xmlNodeIsText(node) == 0){
			printf("\t");

			if(strcmp((char*)node->name, EL_NAME_HTML) == 0){
				string content = (char*)xmlNodeGetContent(node);
				disp.addTag(TUI_PROP_HTML, Trim(content));
				printf("\thtml->[%s]\n", Trim(content).c_str());
			}
		}

		node = node->next;
	}

	tlv.appendTag(TUI_DGTMPL, disp.getStringData());

	return 0;
}

int parse_display(xmlNodePtr node, BERTLV &tlv)
{
	xmlChar *cid;
	BERTLV disp;

	cid = xmlGetProp(node, (const xmlChar*)"id");

	if(cid == NULL)
		return -1;

	disp.addTag(TUI_PROP_ID, (char*)cid);

	printf("DISPLAY id:[%s]\n", (char*)cid);

	node = xmlFirstElementChild(node);

	while (node != NULL)
	{
		// Ignore text nodes
		if(xmlNodeIsText(node) == 0){
			printf("\t");

			if(strcmp((char*)node->name, EL_NAME_HTML) == 0){
				string content = (char*)xmlNodeGetContent(node);
				disp.addTag(TUI_PROP_HTML, Trim(content));
				printf("\thtml->[%s]\n", Trim(content).c_str());
			}
		}

		node = node->next;
	}

	tlv.appendTag(TUI_DISPLAY, disp.getStringData());

	return 0;
}

void parse_language(xmlNodePtr node, BERTLV &tlv)
{
	xmlAttrPtr attr;
	xmlChar *cprop;
	BERTLV lang;

	attr = xmlCopyPropList(node, node->properties);

	while(attr != NULL){
		cprop = xmlGetProp(node, attr->name);
		printf("LANGUAGE prop[%s]:[%s]\n", attr->name, (char*)cprop);

		if(strcmp((char*)attr->name, PROP_NAME_ID) == 0)
			lang.addTag(TUI_PROP_TERM_TYPE, string((char*)cprop));
		else if(strcmp((char*)attr->name, PROP_NAME_LANG_NAME) == 0)
			lang.addTag(TUI_PROP_LANG_NAME, string((char*)cprop));
		else if(strcmp((char*)attr->name, PROP_NAME_LANG_CODE) == 0)
			lang.addTag(TUI_PROP_LANG_CODE, string((char*)cprop));

		attr = attr->next;
	}

	node = xmlFirstElementChild(node);

	while (node != NULL)
	{
		// Ignore text nodes
		if(xmlNodeIsText(node) == 0){
			printf("\t");

			if(strcmp((char*)node->name, EL_NAME_DISPLAY) == 0){
				parse_display(node, lang);
			}else if(strcmp((char*)node->name, EL_NAME_DGTMPL) == 0){
				parse_dgtmpl(node, lang);
			}else if(strcmp((char*)node->name, EL_NAME_IMAGEURL) == 0){
				if(parse_imageurl(node, lang) < 0)
					break;
			}else if(strcmp((char*)node->name, EL_NAME_STRING) == 0){
				if(parse_string(node, lang) < 0)
					break;
			}else if(strcmp((char*)node->name, EL_NAME_REPORT) == 0){
				if(parse_report(node, lang) < 0)
					break;
			}
		}

		node = node->next;
	}

	tlv.appendTag(TUI_LANGUAGE, lang.getStringData());
}

static int parseDoc(char *docname)
{

	xmlDocPtr doc;
	xmlNodePtr cur;
	BERTLV templ, templ_tlv;

	doc = xmlParseFile(docname);

	if (doc == NULL ){
		printf("Document parsing failed. \n");
		return -1;
	}

	cur = xmlDocGetRootElement(doc); //Gets the root element of the XML Doc

	if (cur == NULL)
	{
		xmlFreeDoc(doc);
		printf("Document is Empty!!!\n");
		return -1;
	}

	templ.addTag(TUI_PROP_VERSION, generate_version());

	printf("parseDoc(): RootElement cur->name:%s\n", cur->name);

	cur = xmlFirstElementChild(cur);

	while (cur != NULL)
	{
		// Ignore text nodes
		if(xmlNodeIsText(cur) == 0){
			if(strcmp((char*)cur->name, EL_NAME_DEVICE) == 0){
				parse_device(cur, templ);
			}else if(strcmp((char*)cur->name, EL_NAME_LANGUAGE) == 0){
				parse_language(cur, templ);
			}
		}

		cur = cur->next;
	}

	xmlFreeDoc(doc);

	templ_tlv.addTag(TUI_TEMPLATE, templ.getStringData());
//	cout << "Template Tlv[" << templ_tlv.getAsciiHex() << "]" << endl;

	std::string filename_noext;
	filename_noext = "UI-"+boost::filesystem::path(docname).stem().string()+".dat";

	ofstream uifile;

	uifile.open(filename_noext.c_str(), ios::out | ios::trunc | ios::binary);

	if(uifile.is_open()){
		uifile << templ_tlv.getStringData();
		uifile.close();
	}else{
		cout << "ERROR: Failed to write to " << filename_noext << endl;
		return -1;
	}

	return 0;
}

void print_usage(void)
{
	cout << "Usage: " << APPNAME << " <filename>" << endl;
	cout << "       <filename> - Name of the UI template xml file" << endl << endl;
}

int main(int argc, char **argv)
{
	char *docname;
	int iret = 0;

	cout << APPNAME << endl;
	cout << "Version " << VERSION_MAJOR << "." << VERSION_MINOR << "." << VERSION_REV << endl;

	if(argc <= 1){
		print_usage();
		return 0;
	}

	docname = argv[1];
	iret = parseDoc (docname);

	return (iret);
}
