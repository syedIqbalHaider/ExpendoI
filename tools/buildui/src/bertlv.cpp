#include <string>
#include "bertlv.h"

//using namespace std;
using namespace za_co_verifone_bertlv;

BERTLV::BERTLV(const string &data)
{
	bertlvData=data;
}

int BERTLV::getTag(int startPosition,string &tag)
{
	int iIndex=0;
	
	while(startPosition+iIndex<(int)bertlvData.length()) {
		if(iIndex==0 && (bertlvData.at(startPosition+iIndex)&0x1f)!=0x1f) break;
		if(iIndex>0 && (bertlvData.at(startPosition+iIndex)&0x80)!=0x80) break;
		iIndex++;
	}
	iIndex++;
	
	if(startPosition+iIndex>(int)bertlvData.length()) return(0);
	
	tag=bertlvData.substr(startPosition,iIndex);
	
	return(startPosition+iIndex);
}

int BERTLV::getLen(int startPosition,string &len)
{
	int iIndex=0;
	
	if(startPosition>=(int)bertlvData.length()) return(0);
	if((bertlvData.at(startPosition)&0x80)==0x80) {
		iIndex=1+(bertlvData.at(startPosition)&0x7f);
	} else {
		iIndex=1;
	}
	
	len=bertlvData.substr(startPosition,iIndex);
	
	return(startPosition+iIndex);
}

unsigned long BERTLV::berLenToLong(const string &len)
{
	unsigned long ulResult=0;
	int start_index=0, len_count=len.length();

	if(len.at(0) & 0x80){
		start_index = 1;
		len_count = (len.at(0) & 0x7f) + 1;
	}
	
	for(int iIndex=start_index;iIndex<len_count;iIndex++) {
		ulResult<<=8;
		ulResult+=(unsigned char)len.at(iIndex);
	}

	return(ulResult);
}

int BERTLV::getTagLenVal(int startPosition,string &tag,string &len,string &val)
{
	int iLenPos;
	int iValPos;
	unsigned long ulValLength;
	
	if(startPosition>(int)bertlvData.length()) return(0);
	
	iLenPos=getTag(startPosition,tag);
	if(iLenPos==0) return(0);
	
	iValPos=getLen(iLenPos,len);
	if(iValPos==0) return(0);
	
	ulValLength=berLenToLong(len);
	val=bertlvData.substr(iValPos,ulValLength);
	
	return(iValPos+ulValLength);
}

void BERTLV::resetPosition()
{
	bertlvIndex=0;
}

int BERTLV::getNextPosition()
{
	string tag,len,val;
	
	bertlvIndex=getTagLenVal(bertlvIndex,tag,len,val);
	
	return(bertlvIndex);
}

int BERTLV::getValForTag(const string &tag,string &val)
{
	string myTag,myLen,myVal;
	int iPos;
	int iSaveBertlvIndex=bertlvIndex;
	
	iPos=0;
	resetPosition();
	while(1) {
		getTagLenVal(iPos,myTag,myLen,myVal);
		if(tag==myTag) {
			bertlvIndex=iSaveBertlvIndex;
			val=myVal;
			return(1);
		}
		iPos=getNextPosition();
		if(iPos==0) break;
	}
	
	bertlvIndex=iSaveBertlvIndex;	
	return(0);
}

void BERTLV::longToBerLen(unsigned long ulLength,string &len)
{
	unsigned char cLength[5];
	
	if(ulLength<0x80) {
		cLength[0]=ulLength;
		len=string((char*)cLength,1);
	} else if(ulLength<0x100) {
		cLength[0]=0x81;
		cLength[1]=ulLength;
		len=string((char*)cLength,2);
	} else if(ulLength<0x10000) {
		cLength[0]=0x82;
		cLength[1]=(ulLength>>8)&0xff;
		cLength[2]=ulLength&0xff;
		len=string((char*)cLength,3);
	} else if(ulLength<0x1000000) {
		cLength[0]=0x83;
		cLength[1]=(ulLength>>16)&0xff;
		cLength[2]=(ulLength>>8)&0xff;
		cLength[3]=ulLength&0xff;
		len=string((char*)cLength,4);
	} else {
		cLength[0]=0x84;
		cLength[1]=(ulLength>>25)&0xff;
		cLength[2]=(ulLength>>16)&0xff;
		cLength[3]=(ulLength>>8)&0xff;
		cLength[4]=ulLength&0xff;
		len=string((char*)cLength,5);
	}
}

int BERTLV::addTag(const string &tag,const string &val)
{
	string myVal,berLen;
	
	if(getValForTag(tag,myVal)) return(0);
	
	longToBerLen(val.length(),berLen);
	
	bertlvData=bertlvData+tag+berLen+val;
	return(1);
}

// Same as addTag, but does not check for duplicates
int BERTLV::appendTag(const string &tag,const string &val)
{
	string myVal,berLen;

	longToBerLen(val.length(),berLen);

	bertlvData=bertlvData+tag+berLen+val;
	return(1);
}

int BERTLV::updateTag(const string &tag,const string &val)
{
	deleteTag(tag);
	return(addTag(tag,val));
}

int BERTLV::deleteTag(const string &tag)
{
	string myTag,myLen,myVal;
	int iPos;
	int iRc;
	
	iPos=0;
	resetPosition();
	while(1) {
		iRc=getTagLenVal(iPos,myTag,myLen,myVal);
		if(tag==myTag) {
			bertlvData=bertlvData.substr(0,iPos)+bertlvData.substr(iRc);
			return(1);
		}
		iPos=getNextPosition();
		if(iPos==0) break;
	}
	
	return(0);
}

string BERTLV::getStringData()
{
	return(bertlvData);
}

/*
 * For debugging purposes
 */
#include <sstream>
#include <iomanip>

string BERTLV::getAsciiHex()
{
	std::stringstream ss;

	ss << std::hex;// << std::setw(2) << std::setfill('0');
	for (size_t i = 0; bertlvData.length() > i; ++i) {
			if(static_cast<unsigned int>(static_cast<unsigned char>(bertlvData[i])) <= 0x0f)
				ss << '0';
			ss << static_cast<unsigned int>(static_cast<unsigned char>(bertlvData[i]));
	}

	return ss.str();
}
