#ifndef _UTILS_H_
#define _UTILS_H_

#include <string>
#include <sstream>

//using namespace std;
using std::string;
using std::stringstream;
using std::istringstream;

namespace Utils{

#define UTILS_ASCII_HEX( x ) dynamic_cast< std::ostringstream & >( std::ostringstream() << std::dec << x ).str()

typedef enum
{
	TT_UNKNOWN,
	TT_MX_925,
	TT_UX_300
}TERMINAL_TYPE;

int UtilStringToBCD(string str, char *bcd);
int UtilsStringToInt(string str);
string UtilsIntToString(int int_val);
string UtilsHexToString(const char *data, unsigned int len);
string UtilsStringToHex(string input);
string UtilsBoolToString(bool b);
string UtilsTimestamp();
TERMINAL_TYPE UtilsGetTerminalType(void);
}

#endif // _UTILS_H_
