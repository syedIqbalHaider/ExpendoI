#ifndef _XMLMENU_H_
#define _XMLMENU_H_

#include <sstream>
#include <gui/gui.h>

#ifndef SSTR
	#define SSTR( x ) dynamic_cast< std::ostringstream & >( std::ostringstream() << std::dec << x ).str()
#endif

namespace za_co_verifone_xmlmenu
{
	// Internal Tag values
	#define XM_SEP				"_"
	#define XM_HEADER			XM_SEP "Header"

	#define XM_MAX_MENU 		99
	#define XM_USE_HEAP			1
}
using namespace vfigui;
namespace za_co_verifone_xmlmenu {

/**  NOTE: At present this code is duplicated in CPA and IdleApp - any fixes need to be done in both places!!
 *
 * Provides an automated menu interface from the .xml file, where the <STRING> ids follow
 * a specific convention to enable this. Example self-explanatory structure below.
 * In the menu tree, the text of leaves must start with R value, which is the value returned and may not be above 10000.
 *
    <STRING id="SM_Header" text="System Menu"/>
    <STRING id="SM_0"  text="Parameter Download"/>
    <STRING id="SM_1"  text="R101:Settlement"/>
    <STRING id="SM_2"  text="Reports"/>
    <STRING id="SM_0_Header"  text="Parameter Download"/>
    <STRING id="SM_0_0"  text="R110:Full"/>
    <STRING id="SM_0_1"  text="R111:Partial"/>
    <STRING id="SM_2_Header"  text="Reports"/>
    <STRING id="SM_2_0"  text="R120:Users"/>
    <STRING id="SM_2_1"  text="Batch"/>
    <STRING id="SM_2_2"  text="R121:Config"/>
    <STRING id="SM_2_1_Header"  text="Batch Report"/>
    <STRING id="SM_2_1_0"  text="R131:Current Batch"/>
    <STRING id="SM_2_1_1"  text="R132:Previous Batch"/>
 *
 *  Typical usage would provide a callback which gets called when the menu is selected
 *  and selectXmlMenu() would stay in submenus and only return once the toplevel returns.
 *
 *  Alternatively, if callback is NULL, every call to selectXmlMenu() would return even
 *  on submenu selection also, with value (10000+menuIndex)
 *
 *
 *
 **/
typedef int (*xmenuCallback)(int selValue, std::string menuId);
#

class XmlMenu
{
public:
	XmlMenu(const std::string menuPrefix);
	~XmlMenu();

	int selectXmlMenu(int initialIdx, xmenuCallback menuCallBack, bool mustAddMenu=false);

	//virtual int menuAction(int selValue, std::string menuId);

private:
	std::string mPrefix;
	std::string mHeader;
	int menuSz;
	//struct UIMenuEntry (& mXmenu)[];
	//std::unique_ptr<UIMenuEntry> mXmenu;
#ifdef XM_USE_HEAP
	struct UIMenuEntry *mXmenu;
#else
	struct UIMenuEntry mXmenu[XM_MAX_MENU];
#endif
	std::vector<std::string> mnuIds;
	int populateMenu(std::string prefix, bool mustAddMenu);
};

}

#endif // _XMLMENU_H_
