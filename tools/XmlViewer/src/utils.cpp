#include <cstring>
#include <sstream>
#include <ctime>
#include <sys/types.h>
#include <sys/timeb.h>
#include <platforminfo_api.h>
#include "utils.h"

namespace Utils
{

int UtilStringToBCD(string str, char *bcd)
{
	int pos=0, count=0;

	for(string::iterator str_it=str.begin(); str_it!=str.end(); ++str_it){
		char c = *str_it;

		if(!((c >= '0') && (c <= '9')))
			return 0;

		if(count%2==0)
			bcd[pos] = ((char)(c - '0') << 4) & 0xf0;
		else{
			bcd[pos] |= (char)(c - '0');
			pos++;
		}

		count++;
	}

	return pos;
}

int UtilsStringToInt(string str)
{
	int numb;
	istringstream ( str ) >> numb;
	return numb;
}

string UtilsIntToString(int int_val)
{
	stringstream ss;
	ss << int_val;
	return ss.str();
}

string UtilsHexToString(const char *data, unsigned int len)
{
	std::stringstream ss;

	ss << std::hex;
	for (size_t i = 0; len > i; ++i) {
			if(static_cast<unsigned int>(static_cast<unsigned char>(data[i])) <= 0x0f)
				ss << '0';
			ss << static_cast<unsigned int>(static_cast<unsigned char>(data[i]));
	}

	return ss.str();
}

string UtilsStringToHex(string input)
{
	string out;
	int count=0;
	unsigned char hex=0;

	// If length is uneven, insert a '0' in front
	if(input.length()%2)
		input.insert(0, 1, '0');

	for(string::iterator str_it=input.begin(); str_it!=input.end(); ++str_it){
		char c = *str_it;

		if(count%2 == 0){
			if((c >= '0') && (c <= '9')){
				hex = ((c - '0') << 4) & 0xf0;
			}else if((toupper(c) >= 'A') && (toupper(c) <= 'F')){
				hex = ((c - 'A' + 0x0a) << 4) & 0xf0;
			}
		}else{
			if((c >= '0') && (c <= '9')){
				hex |= (c - '0') & 0x0f;
			}else if((toupper(c) >= 'A') && (toupper(c) <= 'F')){
				hex |= (c - 'A' + 0x0a) & 0x0f;
			}

			out.append(1, hex);
		}

		count++;
	}

	return out;
}

string UtilsBoolToString(bool b)
{
	std::stringstream converter;
	converter << b;
	return converter.str();
}

string UtilsTimestamp()
{
	struct timeb tp;
	ftime(&tp);
	char buf[16];

	memset(buf, 0, sizeof(buf));
	strftime(buf,sizeof(buf),"%Y%m%d%H%M%S%u",localtime(&tp.time));

	return string(buf);
}

TERMINAL_TYPE UtilsGetTerminalType(void)
{
	char buffer[20];
	unsigned long rlen=0;

	memset(buffer, 0, sizeof(buffer));

	if(platforminfo_get(PI_MIB_MODEL_NUM ,buffer, (unsigned long)sizeof(buffer), &rlen) == PI_OK){
		if(memcmp(buffer, "UX300", 5) == 0)
			return TT_UX_300;
		else if(memcmp(buffer, "MX925", 5) == 0)
			return TT_MX_925;
	}

	return TT_UNKNOWN;
}

}
