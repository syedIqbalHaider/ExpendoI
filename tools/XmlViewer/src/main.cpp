#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <libtui/tui.h>
//#include <sstream>
#include <iostream>
#include <fstream>

#include <sys/types.h>
#include <sys/timeb.h>
#include <gui/gui.h>
#include <gui/jsobject.h>

#include "CLog.h"
#include "XmlViewerver.h"
#include "xmlmenu.h"
#include "libtui/tui.h"
#include "libtui/tags.h"
#include "bertlv.h"
#include "utils.h"

static CLog log(XMLVIEWER_NAME);

//using namespace std;
using namespace vfigui;
using namespace za_co_verifone_tui;
using namespace za_co_verifone_xmlmenu;
using namespace za_co_verifone_bertlv;

/* Ux */
#define UX_STATUS_REG_HEIGHT	8  	// 55
#define UX_POPUP_MARGIN		2 		// 50
#define UX_POPUP_WIDTH		48 // 600
#define UX_POPUP_HEIGHT		48		// 300

/* MX */
#define MX_STATUS_REG_HEIGHT	55
#define MX_POPUP_MARGIN		50
#define MX_POPUP_WIDTH			600
#define MX_POPUP_HEIGHT		300

typedef struct{
	int screen_width;
	int screen_height;
	int default_font_size;
}GuiInfo;

static GuiInfo gui_info;

const std::string GLOBAL_STATUS_HTML          =  "<div style='margin-top:90px;' align='center'"
				"><div align='center' style='color:#17345E; font-size:19px; font-weight:bold;'>"
				"<<msg>></div><div style='margin-top:10px'></div>"
				//"<img border='0' src='../images/Kiosk buttons/ajax-loader.gif'>"
				"</div>";

void testXml(std::string fPath);

//void setMsgLayout(  int left,      /* left position in pixels (+=width if negative) */
//					int right,     /* right position in pixels (+=width if negative) */
//					int bottom = -1,  /* bottom position in pixels (+=height if negative) */
//					int top = 102     /* top position in pixels (+=height if negative) */ // was 100
//				 ) {
//	const struct UIRegion ownLayout[] = {
//			// id, left, 					top, 				right, 					 bottom
//			//{3, POPUP_MARGIN,				STATUS_REG_HEIGHT+POPUP_MARGIN,	gui_info.screen_width-POPUP_MARGIN, 	gui_info.screen_height-POPUP_MARGIN},  	// feedback popup
//			{2, 0, 							0, 					gui_info.screen_width/3, STATUS_REG_HEIGHT},  	// button bar
//			{1, (gui_info.screen_width/3)+1,0, 					gui_info.screen_width, 	 STATUS_REG_HEIGHT},  	// status bar
//			{UI_REGION_DEFAULT, left, 		top,				right,					 bottom} 				// main screen
//			};
//
//	vfigui::uiLayout(ownLayout,	sizeof(ownLayout) / sizeof(ownLayout[0]));
//}
//void setDefaultLayout() {
//	//  		left, 		right		   bottom									 top
//	setMsgLayout(0, gui_info.screen_width, gui_info.screen_height, STATUS_REG_HEIGHT+1);
//}

void setMsgLayout(  int left,      /* left position in pixels (+=width if negative) */
					int right,     /* right position in pixels (+=width if negative) */
					int bottom = -1,  /* bottom position in pixels (+=height if negative) */
					int type = Utils::TT_MX_925
				 )
{
	if(type == Utils::TT_UX_300){
		const struct UIRegion ownLayout[] = {
		// id, left, 					top, 					right, 					 bottom
		{3, UX_POPUP_MARGIN,			UX_STATUS_REG_HEIGHT+UX_POPUP_MARGIN,	gui_info.screen_width-UX_POPUP_MARGIN, 	gui_info.screen_height-UX_POPUP_MARGIN},  	// feedback popup
		{1, 0							,0, 					gui_info.screen_width, 	 UX_STATUS_REG_HEIGHT},  	// status bar
		{UI_REGION_DEFAULT, left, 		UX_STATUS_REG_HEIGHT+1,	right,					 bottom} 				// main screen
		};

		vfigui::uiLayout(ownLayout,	sizeof(ownLayout) / sizeof(ownLayout[0]));
	}else{
		const struct UIRegion ownLayout[] = {
		// id, 			left, 			top, 				right, 					 bottom
		{3, MX_POPUP_MARGIN,				MX_STATUS_REG_HEIGHT+MX_POPUP_MARGIN,	gui_info.screen_width-MX_POPUP_MARGIN, 	gui_info.screen_height-MX_POPUP_MARGIN},  	// feedback popup
		//{2, 0, 							0, 					gui_info.screen_width/3, MX_STATUS_REG_HEIGHT},  	// button bar
		{1, (gui_info.screen_width/3)+1,0, 					gui_info.screen_width, 	 MX_STATUS_REG_HEIGHT},  	// status bar
		{UI_REGION_DEFAULT, left, 		MX_STATUS_REG_HEIGHT+1,				right,					 bottom} 				// main screen
		};

		vfigui::uiLayout(ownLayout,	sizeof(ownLayout) / sizeof(ownLayout[0]));
	}
}
void setDefaultLayout(int type = -1) {
	if (type == -1) type = Utils::UtilsGetTerminalType();
	//  		left, 		right		   bottom									 top
	setMsgLayout(0, gui_info.screen_width, gui_info.screen_height, type);
}



/**
 * Substitute all occurences of fromStr with toStr in inStr, returns a reference to inStr
 * @param inStr
 * @param fromStr
 * @param toStr
 */
std::string& substAll(std::string& inStr, const std::string& fromStr, const std::string& toStr) {
    if(!fromStr.empty()) {
		size_t pos = 0;
		while((pos = inStr.find(fromStr, pos)) != std::string::npos) {
			inStr.replace(pos, fromStr.length(), toStr);
			pos += toStr.length(); // prevent replace a with ba to go into loop
		}
    }
    return inStr;
}


void showStatus(const std::string &statMsg) {
	std::string msg = GLOBAL_STATUS_HTML;
	substAll(msg,"<<msg>>",statMsg);
	uiDisplay(UI_REGION_DEFAULT, msg);
}

void uiMain(int argc, char *argv[])
{
	if (argc <= 1) {
		log.message(MSG_INFO" Usage: XmlViewer appname     (Note:file flash/UI-appname.dat must exist)\n");
	} else {
		std::string sApp = argv[1];
		log.message(MSG_INFO"Viewing entries for app "  + sApp + "\n" );
		int ret = TuiInit(sApp);

		if(ret < 0){
			log.message(MSG_INFO "TuiInit returned "+SSTR(ret)+"\r\n");
		}
		std::string fPath = "flash/" + sApp + "rsc/";
		uiSetPropertyString (UI_PROP_RESOURCE_PATH, fPath);
		uiReadConfig();

//		// Setup the thread that will handle the status bar
//		StatusThread status_info;
//		pthread_t status_pthread, buttonbar_pthread, alarm_thread, popup_pthread;
//
		// Read some config parameters
		uiGetPropertyInt(UI_DEVICE_WIDTH, &gui_info.screen_width);
		uiGetPropertyInt(UI_DEVICE_HEIGHT, &gui_info.screen_height);
		uiGetPropertyInt(UI_PROP_DEFAULT_FONT_SIZE,&gui_info.default_font_size);

		log.message(MSG_INFO"Display width :"+SSTR(gui_info.screen_width)+"\r\n");
		log.message(MSG_INFO"Display height:"+SSTR(gui_info.screen_height)+"\r\n");
		log.message(MSG_INFO"Font size     :"+SSTR(gui_info.default_font_size)+"\r\n");

		setDefaultLayout();

		// Show menu to display xml items
		fPath = "flash/UI-"+sApp+".dat";

		testXml(fPath);
	}
}

int main(int argc, char *argv[])
{
	uiMain(argc, argv);

	return 0;
}

// ----- copied as is from tui.cpp in tuilib

int get_tag(ifstream &fstr, string &tag)
{
	stringstream ss;

	if(!fstr.good())
		return -2;

	char c = fstr.get();

	if((c & 0x1f) > 0)
		ss << c;
	else
		return -1;

	do{
		c = fstr.get();
		ss << c;
	}while((c & 0x80) && (!fstr.eof()));

	tag = ss.str();

	return 0;
}

int get_length(ifstream &fstr, long *length)
{
	if(!fstr.good())
			return -2;

	int lenb=0;
	stringstream ss;
	char c = fstr.get();
	*length = 0;

	if(c & 0x80)
		lenb = (c & 0x7f);	// Get the number of length bytes

	if(lenb == 0){
		*length = (long)c;
		return 1;
	}

	for(int i=0; i<lenb-1; i++){
		c = fstr.get();
		*length |= (long)c;
		*length = *length << 8;
	}

	*length += fstr.get();

	return lenb+1;
}

string disp_hex(string &data)
{
	stringstream ss;

	ss << hex;

	for (int i = 0; i < (int)data.length(); ++i) {
			if(static_cast<unsigned int>(static_cast<unsigned char>(data[i])) <= 0x0f)
				ss << '0';
			ss << static_cast<unsigned int>(static_cast<unsigned char>(data[i]));
	}

	return ss.str();
}
// ------ end copied as is

// Below modified from code in tui lib
int getTemplateItems(std::string fName, std::string templType, std::vector<std::string> &vItems)
{
	ifstream tuifile;
	log.message(MSG_INFO + fName + "\n");
	tuifile.open(fName.c_str(), ifstream::in | ifstream::binary);

	if(!tuifile.is_open())
		return 0;

	// Get the length of the file
	int file_length;
	tuifile.seekg(0, tuifile.end);
	file_length = tuifile.tellg();
	tuifile.seekg(0, tuifile.beg);

	string tag;
	long len;
	get_tag(tuifile, tag);
	int len_bytes = get_length(tuifile, &len);

	// Check the that the file begins correctly
	if(tag != TUI_TEMPLATE){
		tuifile.close();
		return -2;
	}

	// Check that the length makes sense
	if(file_length != (int)(len + tag.length() + len_bytes)){
		tuifile.close();
		return -3;
	}

	while(get_tag(tuifile, tag) == 0){
		if(tag == TUI_DEVICE){
			// Skip tag for now
			get_length(tuifile, &len);
			tuifile.seekg(len, tuifile.cur);
		}else if(tag == TUI_LANGUAGE){
			std::string innerTag;
			long lang_len, inLen;

			get_length(tuifile, &lang_len);

			while((get_tag(tuifile, innerTag) == 0)){
				std::string tlv(innerTag);

				if(innerTag == templType) {
					get_length(tuifile, &inLen);

					// Get the value
					char *buffer = new char [inLen];
					tuifile.read(buffer, inLen);

					// Now we should have a tlv
					BERTLV bertlv(string(buffer, inLen));

					// Check if the id matches what we're searching for
					std::string templ_id;
					if((bertlv.getValForTag(TUI_PROP_ID, templ_id))){
						vItems.push_back(templ_id);
					}
					delete[] buffer;
				}else{
					// Skip tag for now
					get_length(tuifile, &inLen);
					tuifile.seekg(inLen, tuifile.cur);
				}
			}			
		}
		else{
			// Skip tag for now
			get_length(tuifile, &len);
			tuifile.seekg(len, tuifile.cur);
		}
	}

	tuifile.close();

	return 1;
}

void displayMsg(std::string message) {
	std::string templ = "<center style=\"margin-top: 180;\">\n"
			"<b><?var Message?></b>\n"
		"</center>\n"
		"<center style=\"margin-top: 50;\">\n"
			"<button accesskey='&#13;' style='width:120px; border-width:0; color:#fff; font-size:14px; "
			"background-color:#dd8215; padding:8px;' action='return 0'> Ok </button>"
		"</center>";
	map<string,string> values;
	values["Message"] = message;

	uiInvoke(values,templ);
}

void getCustomSizes() {
	std::string html = "<div style='margin-top:13px'></div>"
		"<div align='left' style='color:#000; font-size:19px; font-weight:bold; margin-left:15px;'>"
		"	Define Screensize"
		"</div>"
		"<div style='margin-top:5px;'></div>"
		"<div align='left' style='margin-left:15px; width:100%;'>"
		"<table style='width:80%;font-size:18px;'>"
		"	<tr>"
		"		<td align='left'>Screen Width:</td>"
		"		<td><input size='20' maxlength='20' name='scrwidth' type='text' allowed_chars=’1234567890’></td>"
		"	</tr>"
		"	<tr>"
		"		<td align='left'>Screen Height:</td>"
		"		<td><input size='20' maxlength='20' name='scrheight' type='text' allowed_chars=’1234567890’></td>"
		"	</tr>"
			"	<tr>"
			"		<td align='left'>Screen Font:</td>"
			"		<td><input size='20' maxlength='20' name='scrfont' type='text' allowed_chars=’1234567890’ ></td>"
			"	</tr>"
		"	<tr>"
		"		<td align='left'>"
		"			<button style='border-width:0px; font-size:14px; padding:8px; background-color:#17345E; color:#ffffff;' action='return 1'>"
		"				Cancel"
		"			</button>"
		"		</td>"
		"		<td></td>"
		"		<td align='right'>"
		"			<button style='border-width:0px; font-size:14px; padding:8px; background-color:#17345E; color:#ffffff;' accesskey='&#13;' action='return 0'>"
		"				Confirm"
		"			</button>"
		"		</td>"
		"	</tr>"
		"</table>"
		"</div>";
	map<string,string> values;
	values["scrwidth"] = SSTR(gui_info.screen_width);
	values["scrheight"] = SSTR(gui_info.screen_height);
	values["scrfont"] = SSTR(gui_info.default_font_size);

	int res = uiInvoke(values,html);
	if (res == 0) {
		gui_info.screen_width = Utils::UtilsStringToInt(values["scrwidth"]);
		gui_info.screen_height = Utils::UtilsStringToInt(values["scrheight"]);
		gui_info.default_font_size = Utils::UtilsStringToInt(values["scrfont"]);
		uiSetPropertyInt(UI_PROP_DEFAULT_FONT_SIZE,gui_info.default_font_size);
		uiReadConfig();

	}

}

const std::string templTypes[] = { "ignore", TUI_DGTMPL, TUI_DISPLAY, TUI_STRING, TUI_REPORT };
const std::string templTypeNames[] = { "ignore", "TUI_DGTMPL", "TUI_DISPLAY", "TUI_STRING", "TUI_REPORT" };

int doTemplateMenu(std::string fPath, int nr) {
	log.message(MSG_INFO "doTemplateMenu:" + SSTR(nr) + "=" + templTypeNames[nr] + "\n");

	std::vector<std::string> vItems;
	getTemplateItems(fPath, templTypes[nr],vItems);
	log.message(MSG_INFO "after \n");
	int menuItSz = vItems.size()+1;
	struct UIMenuEntry menuItems[menuItSz];
	log.message(MSG_INFO "Found " + SSTR(menuItSz) + " items \n");

	for (int i=0;i<menuItSz;i++) {
		if (i == 0) {
			if (menuItSz == 1)
				menuItems[i].text = "0.  No items found.";
			else
				menuItems[i].text = "0.  Back";
		} else {
			menuItems[i].text = SSTR(i) + ".  " + vItems[i-1];
		}
		menuItems[i].options = 0;
		menuItems[i].value = i;
	}
	int s=0;
	bool stayHere = true;
	bool mustGetStrings = false;
	std::string hdr = "XML Template:" + templTypeNames[nr]
	                       + ((nr==3 && !mustGetStrings) ? " (name)" : "");// + "</h4>";
	map<string,string> values;
	values["RespCode"] = "123";
	values["Message"] = TuiString("ValuesEnteredError") + "\nAddisional message not given.";

	log.message(MSG_INFO "Menu " + SSTR(menuItSz) + " items \n");
	while(stayHere) {
		s=uiMenu("menu",hdr,menuItems,menuItSz,s>=0?s:0);
		if ((s<0) || s>menuItSz) break;
		switch (s) {
			case 0: stayHere = false; break;
			default: {
				if (nr==1 || nr ==2) { // TMPL/DISPL
					std::string sId = menuItems[s].text.substr(4+ (s>9?1:0));
					std::string tmpl;
					TuiTemplate("bgimage.tmpl", sId, tmpl);
					tmpl += "<button accesskey='&#27;' style='visibility:hidden' action='return 0'></button>";
					uiInvoke(values,tmpl);
				} else if (nr == 3) { // String
					std::string sVal = TuiString(vItems[s]);
					std::string msg = "String: " + vItems[s] + " \n\n"
									  "Value:  " + sVal + "\n\n";
					displayMsg(msg);

					showStatus("Rebuilding string menu ...");
					// Toggle Strings
					mustGetStrings = !mustGetStrings;
					hdr = "XML Template:" + templTypeNames[nr]
					        + ((nr==3 && !mustGetStrings) ? " (name)" : "");// + "</h4>";

					for (int i=0;i<menuItSz;i++) {
						if (i == 0) {
							if (menuItSz == 1)
								menuItems[i].text = "0.  No items found.";
							else
								menuItems[i].text = "0.  Back";
						} else {
							menuItems[i].text = SSTR(i) + ".  " +
									(mustGetStrings ? TuiString(vItems[i-1]) : vItems[i-1]);
						}
						menuItems[i].options = 0;
						menuItems[i].value = i;
					}

				} else {
					displayMsg("Reports not supported");
				}
			}
		}
	}
	return s;
}
int menuAction(int selValue, std::string menuId) {
	(void)menuId;

	log.message(MSG_INFO"menuAction():" + menuId + "\n");

//	switch (selValue) {
//		case 101:
//	}
	log.message(MSG_INFO"menuAction() return:" + SSTR(selValue) + "\n");

	return selValue;
}


void testXml(std::string fPath) {
	const struct UIMenuEntry menu[] = {
			//{ "0. Back", 9, 0 },
			{ "1. XmlMenu with 15 entries", 1, 0 },
			{ "2. Template Items",		 	2, 0 },
			{ "3. Display Items", 			3, 0 },
			{ "4. Strings", 			    4, 0 },
			{ "5. Reports", 			    5, 0 },
			{ "6. Set MX screen size", 	    6, 0 },
			{ "7. Set UX screen size", 	    7, 0 },
			{ "8. Custom Screen Size", 	    8, 0 },
		};

	uint menuSZ = sizeof(menu) / sizeof(menu[0]);
	map<string,string> values;
	values["RespCode"] = "123";
	values["Message"] = TuiString("ValuesEnteredError") + "Addisional message not given.";

	int s=0;
	bool stayHere = true;
	while(stayHere) {
		s=uiMenu("menu","Test XML Templates",menu,menuSZ,s>=0?s:0);
		//if ((s<0) || ((uint)s)>menuSZ) break;

		switch (s) {
			case UI_ERR_ABORT:
			case UI_ERR_BACK:
			case 0: stayHere = false; break;
			case 1:
			{
				XmlMenu xm("XML");
				s = xm.selectXmlMenu(0,menuAction,true);
				break;
			}
			case 2:
			case 3:
			case 4:
			case 5:	{
				doTemplateMenu(fPath,s-1);
				break;
			}
			case 6: {
				setDefaultLayout(Utils::TT_MX_925);
				break;
			}
			case 7: {
				gui_info.screen_width = 280;//128;
				gui_info.screen_height = 200;//64;
				gui_info.default_font_size = 6;//8;
				uiSetPropertyInt(UI_PROP_DEFAULT_FONT_SIZE,gui_info.default_font_size);
				uiReadConfig();
				setDefaultLayout(Utils::TT_UX_300);

				log.message(MSG_INFO"Display width :"+SSTR(gui_info.screen_width)+"\r\n");
				log.message(MSG_INFO"Display height:"+SSTR(gui_info.screen_height)+"\r\n");
				log.message(MSG_INFO"Font size     :"+SSTR(gui_info.default_font_size)+"\r\n");

				break;
			}
			case 8: {
				setDefaultLayout(Utils::TT_MX_925);
				getCustomSizes();
				setDefaultLayout(Utils::TT_MX_925);

				log.message(MSG_INFO"Display width :"+SSTR(gui_info.screen_width)+"\r\n");
				log.message(MSG_INFO"Display height:"+SSTR(gui_info.screen_height)+"\r\n");
				log.message(MSG_INFO"Font size     :"+SSTR(gui_info.default_font_size)+"\r\n");

				break;
			}
		}
	}
}
