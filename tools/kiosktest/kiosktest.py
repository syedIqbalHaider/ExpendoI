import serial
import sys, getopt
import time

def SerialOpen(name):
    try:
        global portHandle
        
        portHandle = serial.Serial(name,
                            19200,
                            serial.EIGHTBITS,
                            serial.PARITY_NONE,
                            serial.STOPBITS_ONE,
                            0.5)
        
    except serial.SerialException, e:
        return -1
    
    return 0

def SerialClose():
    portHandle.close()

def test():
    while True:
        portHandle.write("\x02A00\x03A")    # Get status
#        portHandle.write("\x02\x43\x30\x30\x30\x30\x30\x30\x30\x30\x30\x30\x30\x31\x30\x30\x00\x30\x30\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x03\x42")
        portHandle.read(1)
        portHandle.write("\x15")
        time.sleep(0.3)
        print "."
    
def help():
    print "kiosktest.py -p <portname>"
    sys.exit(2)
    
def main(argv):
    if len(argv) == 0:
        help()
        
    portName = ''
    
    try:
        opts, args = getopt.getopt(argv, "hp:")
    except getopt.GetoptError:
        help()
        
    for opt, arg in opts:
        if opt == '-h':
            help()
        if opt == '-p':
            portName = arg
            
            portstatus = SerialOpen(portName);
    
            if portstatus < 0:
                print "Error opening port ", portName
                sys.exit(3)
        else:
            help()
    
    test()
    
if __name__ == '__main__':
    main(sys.argv[1:])