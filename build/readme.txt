This folder contains OMake customized scripts produced by VeriFone. 
It allows building your sources for different supported targets using
different compilers without touching normal OMake installation.
