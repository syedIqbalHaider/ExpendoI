#include <stdio.h>
#include <string>
#include <iostream>
#include "libtui/tui.h"

//using namespace std;
using namespace za_co_verifone_tui;

int main (int argc, char **argv)
{
	cout << "TuiInit(): returned " <<  TuiInit("IdleApp") << endl;

	string tmpl;
	cout << "TuiTemplate(): returned " << TuiTemplate("bgimage", "disp2", tmpl) << " [" << tmpl << "]" << endl;

	// Report template test
/*
	<REPORT id="TestRprt">
	        <LINE justify="CENTRE" font="WIDE" text="%v\n\n\n" value="ProductName"/>
	        <LINE justify="LEFT" text="Receipt No : %v\n" value="RecpNo"/>
	        <LINE justify="LEFT" text="Expiry Date: %v\n" value="ExpiryDate"/>
	        <LINE justify="LEFT" text="Serial No  : %v\n\n\n" value="SerialNo"/>
	        <LINE justify="CENTRE" font="WIDE" text="PIN\n"/>
	        <LINE justify="CENTRE" font="WIDE" text="%v\n\n\n" value="PINNo"/>
	        <LINE text="----------------------------------------\n"/>
	        <LINE justify="CENTRE" text="%v\n" value="ItemMessage1"/>
	        <LINE justify="CENTRE" text="error %v\n" value="error"/>
	        <LINE justify="CENTRE" text="item1:%v item2:%v item3:%v\n" value="Item1,Item2,Item3"/>
	        <LINE justify="CENTRE" text="%v\n" value="ItemMessage3"/>
	        <LINE justify="CENTRE" text="%v\n" value="ItemMessage4"/>
	        <LINE justify="CENTRE" text="%v\n" value="ItemMessage5"/>
	        <LINE justify="CENTRE" text="%v\n" value="ItemMessage6"/>
	        <LINE justify="CENTRE" text="%v\n" value="ItemMessage7"/>
	        <LINE text="----------------------------------------\n"/>
	  </REPORT>
*/
	map<string, string>dynvals;
	vector<string>report;
	int count=0;

	cout << "Test start" << endl;

	dynvals["ProductName"] = "Product1";
	dynvals["RecpNo"] = "12345";
	dynvals["ExpiryDate"] = "14/12/01";
	dynvals["SerialNo"] = "123-456-789";
	dynvals["PINNo"] = "1234";
	dynvals["ItemMessage1"] = "Item message 1";
	dynvals["ItemMessage2"] = "Item message 2";
	dynvals["Item1"] = "Item1";
	dynvals["Item2"] = "Item2";
	dynvals["Item3"] = "Item3";

	count = TuiReport("TestRprt", report, dynvals);

	cout << "count=" << log.from_int(count) << endl;

	for(vector<string>::iterator it=report.begin(); it != report.end(); ++it){
		cout << "LINE:[" << *it << "]" << endl;
	}

	cout << "Test complete" << endl;

	return 0;
}
