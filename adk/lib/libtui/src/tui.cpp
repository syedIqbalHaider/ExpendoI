#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <boost/filesystem.hpp>
#include "libtui/tui.h"
#include "libtui/tags.h"
#include "libtui/bertlv.h"

//using namespace std;
using namespace za_co_verifone_bertlv;
using namespace boost::filesystem;

namespace za_co_verifone_tui{

int find_resource(string id, BERTLV &templTlv);
int process_language(ifstream &fstr, string &id, BERTLV &templTlv);
int process_display(string &templ, string &tmpldata);
int process_line(BERTLV line_templ, string &line, map<string, string>&values);
int export_template_type(string templ_type, string ext);
int export_images(void);
int get_tag(ifstream &fstr, string &tag);
int get_length(ifstream &fstr, long *length);
string disp_hex(string &data);

static string filename;
static string application_name;
static int prn_width=42;	// TODO: This must be tied to the device element of this platform

int TuiInit(string appname)
{
	filename = "flash/UI-"+appname+".dat";
	ifstream tuifile;

	application_name = appname + "rsc";

	tuifile.open(filename.c_str(), ifstream::in | ifstream::binary);

	if(!tuifile.is_open())
		return -1;

	// Get the length of the file
	int file_length;
	tuifile.seekg(0, tuifile.end);
	file_length = tuifile.tellg();
	tuifile.seekg(0, tuifile.beg);

	string tag;
	long len;
	get_tag(tuifile, tag);
	int len_bytes = get_length(tuifile, &len);

	// Check the that the file begins correctly
	if(tag != TUI_TEMPLATE){
		tuifile.close();
		return -2;
	}

	// Check that the length makes sense
	if(file_length != (int)(len + tag.length() + len_bytes)){
		tuifile.close();
		return -3;
	}

	tuifile.close();

	// Export all DirectGui templates to the file system
	if(export_template_type(TUI_DGTMPL, "tmpl") < 0)
		return -4;

	// Export all images
	if(export_images() < 0)
		return -5;

	return 0;
}

int TuiTemplate(string id, string &template_data)
{
	BERTLV templ;

	if(find_resource(id, templ)){
		string tagstr, lenstr, valstr;

		templ.getTagLenVal(0, tagstr, lenstr, valstr);

		if((tagstr == TUI_DISPLAY) || (tagstr == TUI_DGTMPL)){
			return process_display(valstr, template_data);
		}

		return -1;
	}

	return -1;
}

int TuiTemplate(string tmplid, string id, string &template_data)
{
	string master_templ, templ;
	ifstream tuifile;

	string appfilename = "flash/" + application_name + "/" + tmplid;

	tuifile.open(appfilename.c_str(), ifstream::in);

	if(!tuifile.is_open())
		return -1;

	char buffer[1024];

	while(tuifile.good()){
		memset(buffer, 0, sizeof(buffer));
		tuifile.read(buffer, sizeof(buffer));
		master_templ.append(buffer);
	}

	tuifile.close();

	if(TuiTemplate(id, templ) < 0)
		return -2;

	// Find the insertion point
	size_t pos=0;
	if((pos = master_templ.find("<?insert text?>")) == master_templ.npos)
		return -3;

	master_templ.replace(pos, 15, (const char*)templ.c_str());

	template_data = master_templ;

	return 0;
}

string TuiString(string id)
{
	BERTLV templ;

	if(find_resource(id, templ)){
		string tagstr, lenstr, valstr;

		templ.getTagLenVal(0, tagstr, lenstr, valstr);

		if(tagstr == TUI_STRING){
			string string_tmpl;
			BERTLV text_tlv(valstr);

			if(text_tlv.getValForTag(TUI_PROP_TEXT, string_tmpl) != 1)
				return "";

			return string_tmpl;
		}

		return "";
	}

	return "";
}

int TuiReport(string id, vector<string>&lines, map<string, string>&values)
{
	BERTLV templ;
	int line_count=0, pos=0;

	if(find_resource(id, templ)){
		string strtag, strlen, strval;
		int rpos;

		templ.getValForTag(TUI_REPORT, strval);

		BERTLV rprt_tlv(strval);

		// This skips past the id
		if((rpos = rprt_tlv.getTagLenVal(0, strtag, strlen, strval)) <= 0)
			return 0;

		pos += rpos;

		while(1){
			if((rpos = rprt_tlv.getTagLenVal(rpos, strtag, strlen, strval)) <= 0)
				break;

			if(strtag.compare(TUI_LINE) == 0)
			{
				BERTLV line_tlv;
				line_tlv.addTag(TUI_LINE, strval);

				string line;
				if(process_line(line_tlv, line, values) < 0){
					pos = rpos;
					continue;	// Invalid lines will be ignored
				}
				lines.push_back(line);
				line_count++;
			}

			pos = rpos;
		}
	}

	return line_count;
}

//
//Private methods
//
int find_resource(string id, BERTLV &templTlv)
{
	ifstream tuifile;

	tuifile.open(filename.c_str(), ifstream::in | ifstream::binary);

	if(!tuifile.is_open())
		return 0;

	// Get the length of the file
	int file_length;
	tuifile.seekg(0, tuifile.end);
	file_length = tuifile.tellg();
	tuifile.seekg(0, tuifile.beg);

	string tag;
	long len;
	get_tag(tuifile, tag);
	int len_bytes = get_length(tuifile, &len);

	// Check the that the file begins correctly
	if(tag != TUI_TEMPLATE){
		tuifile.close();
		return -2;
	}

	// Check that the length makes sense
	if(file_length != (int)(len + tag.length() + len_bytes)){
		tuifile.close();
		return -3;
	}

	while(get_tag(tuifile, tag) == 0){
		if(tag == TUI_DEVICE){
			// Skip tag for now
			get_length(tuifile, &len);
			tuifile.seekg(len, tuifile.cur);
		}else if(tag == TUI_LANGUAGE){
			if(process_language(tuifile, id, templTlv) <= 0){
				tuifile.close();
				return -4;
			}
		}
		else{
			// Skip tag for now
			get_length(tuifile, &len);
			tuifile.seekg(len, tuifile.cur);
		}
	}

	tuifile.close();

	return 1;
}

int process_language(ifstream &fstr, string &id, BERTLV &templTlv)
{
	int foundit=-1;
	string tag;
	long lang_len, len;

//TODO: For multiple languages, get the current file position so we can reset it when we're done.

	get_length(fstr, &lang_len);

	while((get_tag(fstr, tag) == 0) && (foundit != 1)){
		string tlv(tag);

		if(tag == TUI_DISPLAY){
			get_length(fstr, &len);

			// Get the value
			char *buffer = new char [len];
			fstr.read(buffer, len);

			// Now we should have a tlv
			BERTLV bertlv(string(buffer, len));

			// Check if the id matches what we're searching for
			string templ_id;
			if((bertlv.getValForTag(TUI_PROP_ID, templ_id)) && (id == templ_id)){
				foundit = 1;

				templTlv.addTag(TUI_DISPLAY, buffer);
			}

			delete[] buffer;
		}else if(tag == TUI_DGTMPL){
			get_length(fstr, &len);

			// Get the value
			char *buffer = new char [len];
			fstr.read(buffer, len);

			// Now we should have a tlv
			BERTLV bertlv(string(buffer, len));

			// Check if the id matches what we're searching for
			string templ_id;
			if((bertlv.getValForTag(TUI_PROP_ID, templ_id)) && (id == templ_id)){
				foundit = 1;

				templTlv.addTag(TUI_DGTMPL, buffer);
			}

			delete[] buffer;
		}else if(tag == TUI_STRING){
			get_length(fstr, &len);

			// Get the value
			char *buffer = new char [len];
			fstr.read(buffer, len);

			// Now we should have a tlv
			BERTLV bertlv(string(buffer, len));

			// Check if the id matches what we're searching for
			string templ_id;
			if((bertlv.getValForTag(TUI_PROP_ID, templ_id)) && (id == templ_id)){
				foundit = 1;

				templTlv.addTag(TUI_STRING, buffer);
			}

			delete[] buffer;
		}else if(tag == TUI_REPORT){
			get_length(fstr, &len);

			// Get the value
			char *buffer = new char [len];
			fstr.read(buffer, len);

			// Now we should have a tlv
			BERTLV bertlv(string(buffer, len));

			// Check if the id matches what we're searching for
			string templ_id;
			if((bertlv.getValForTag(TUI_PROP_ID, templ_id)) && (id == templ_id)){
				foundit = 1;

				templTlv.addTag(TUI_REPORT, buffer);
			}

			delete[] buffer;
		}else{
			// Skip tag for now
			get_length(fstr, &len);
			fstr.seekg(len, fstr.cur);
		}
	}

	return foundit;
}

int process_display(string &templ, string &tmpldata)
{
	BERTLV templ_tlv(templ);
	string html_str;

	if(templ_tlv.getValForTag(TUI_PROP_HTML, html_str) != 1)
		return -1;

	tmpldata = html_str;

	return 0;
}

typedef enum
{
	JUST_NONE,
	JUST_LEFT,
	JUST_CENTRE,
	JUST_RIGHT
}eJUSTIFY;

typedef enum
{
	FONT_NORMAL,
	FONT_WIDE,
	FONT_HIGH,
	FONT_INVERSE
}eFONT;

int process_line(BERTLV line_templ, string &line, map<string, string>&values)
{
	int pos=0;
	string text, str_value;
	eJUSTIFY just=JUST_NONE;
	eFONT font=FONT_NORMAL;

	if(line_templ.getValForTag(TUI_LINE, text) == 0)
		return -1;

	BERTLV line_tlv(text);
	text.clear();

	while(1){
		string strtag, strlen, strval;
		int rpos;

		if((rpos = line_tlv.getTagLenVal(pos, strtag, strlen, strval)) > 0){

			if(strtag.compare(TUI_PROP_TEXT) == 0){
				text = strval;
			}else if(strtag.compare(TUI_PROP_VALUE) == 0){
				str_value = strval;
			}else if(strtag.compare(TUI_PROP_JUSTIFY) == 0){
				if(strval.compare(TUI_VAL_LEFT) == 0)
					just = JUST_LEFT;
				else if(strval.compare(TUI_VAL_CENTRE) == 0)
					just = JUST_CENTRE;
				else if(strval.compare(TUI_VAL_RIGHT) == 0)
					just = JUST_RIGHT;

			}else if(strtag.compare(TUI_PROP_FONT) == 0){
				if(strval.compare(TUI_VAL_NORMAL) == 0)
					font = FONT_NORMAL;
				else if(strval.compare(TUI_VAL_WIDE) == 0)
					font = FONT_WIDE;
				else if(strval.compare(TUI_VAL_HIGH) == 0)
					font = FONT_HIGH;
				else if(strval.compare(TUI_VAL_INVERSE) == 0)
					font = FONT_INVERSE;
			}
		}else
			break;

		pos = rpos;
	}

	// Resolve any dynamic values
	if(str_value.length()){
		string val;
		size_t spos=0, vpos=0;

		while((vpos = str_value.find(",")) != string::npos){

			// First check if we have a %v format specifier in our text
			if((pos = text.find("%v")) == string::npos)
				break;	// No format specifier found, proceed with next step

			val = str_value.substr(spos, vpos);
			str_value.erase(spos, vpos+1);

			if(values[val].length())
				text.replace((size_t)pos, 2, values[val]);
		}

		if((values[str_value].length()) && ((pos = text.find("%v")) != string::npos))
			text.replace((size_t)pos, 2, values[str_value]);
	}

	// If there are any unresolved format specifiers, then the line will ignored
	if((pos = text.find("%v")) != string::npos)
		return -1;
/*
	// Check for justification
	if((just == JUST_CENTRE) && (text.length() < prn_width)){
		int beg_pos = (prn_width - text.length())/2;
		text.insert(0, beg_pos, ' ');
	}else if((just == JUST_RIGHT) && (text.length() < prn_width)){
		int beg_pos = prn_width - text.length();
		text.insert(0, beg_pos, ' ');
	}
*/
	if(just == JUST_CENTRE)
		line.append("\\C");
	else if(just == JUST_RIGHT)
		line.append("\\R");

	if(font == FONT_WIDE)
		line.append("\\W");
	else if(font == FONT_HIGH)
		line.append("\\H");
	else if(font == FONT_INVERSE)
		line.append("\\I");

	// Replace all occurences of \n with appropriate value
	while((pos = text.find("\\n")) != string::npos)
		text.replace((size_t)pos, 2, "\n");

	// Replace all occurences of \r with appropriate value
	while((pos = text.find("\\r")) != string::npos)
		text.replace((size_t)pos, 2, "\r");

	line.append(text);

	return 0;
}

int export_images(void)
{
	ifstream tuifile;

	tuifile.open(filename.c_str(), ifstream::in | ifstream::binary);

	if(!tuifile.is_open())
		return 0;

	// Get the length of the file
	int file_length;
	tuifile.seekg(0, tuifile.end);
	file_length = tuifile.tellg();
	tuifile.seekg(0, tuifile.beg);

	string tag;
	long len;
	get_tag(tuifile, tag);
	int len_bytes = get_length(tuifile, &len);

	// Check the that the file begins correctly
	if(tag != TUI_TEMPLATE){
		tuifile.close();
		return -2;
	}

	// Check that the length makes sense
	if(file_length != (int)(len + tag.length() + len_bytes)){
		tuifile.close();
		return -3;
	}

	while(get_tag(tuifile, tag) == 0){
		if(tag == TUI_LANGUAGE){
			long lang_len;
			get_length(tuifile, &lang_len);

			while(get_tag(tuifile, tag) == 0){
				string tlv(tag);

				if(tag == TUI_IMAGE){
					get_length(tuifile, &len);

					// Get the value
					char *buffer = new char [len];
					tuifile.read(buffer, len);

					// Now we should have a tlv
					BERTLV bertlv(string(buffer, len));

					// Get template id, which will become the name of the file to export to
					string image_path_str;
					if(bertlv.getValForTag(TUI_PROP_PATH, image_path_str)){
						string image_path = "flash/" + image_path_str;

						// Extract the html from the template and write it to a file
						string tmpl_data;
						if(bertlv.getValForTag(TUI_PROP_DATA, tmpl_data)){
							// Export the html to a file
							ofstream image_file;

							try{
								// Make sure that the directory exist before attempting to open the file
								path p = image_path;
								path pdir = p.remove_filename();	// Get the directory without filename portion

								if(!pdir.empty()){
									create_directories(pdir);
								}
							}catch (filesystem_error &fe){
								cout << "Error " << fe.what() << endl;
								delete[] buffer;
								tuifile.close();
								return -4;
							}

							image_file.open(image_path.c_str(), ofstream::out | ofstream::binary);

							if(!image_file.is_open()){
								delete[] buffer;
								tuifile.close();
								return -5;
							}

							// Trim any formatting characters before exporting
							image_file << tmpl_data;	// Skip the id and export everything after it

							image_file.close();
						}
					}

					delete[] buffer;
				}else{
					// Skip tag for now
					get_length(tuifile, &len);
					tuifile.seekg(len, tuifile.cur);
				}
			}
		}
		else{
			// Skip tag for now
			get_length(tuifile, &len);
			tuifile.seekg(len, tuifile.cur);
		}
	}

	tuifile.close();

	return 1;
}

int export_template_type(string templ_type, string ext)
{
	ifstream tuifile;

	tuifile.open(filename.c_str(), ifstream::in | ifstream::binary);

	if(!tuifile.is_open())
		return 0;

	// Get the length of the file
	int file_length;
	tuifile.seekg(0, tuifile.end);
	file_length = tuifile.tellg();
	tuifile.seekg(0, tuifile.beg);

	string tag;
	long len;
	get_tag(tuifile, tag);
	int len_bytes = get_length(tuifile, &len);

	// Check the that the file begins correctly
	if(tag != TUI_TEMPLATE){
		tuifile.close();
		return -2;
	}

	// Check that the length makes sense
	if(file_length != (int)(len + tag.length() + len_bytes)){
		tuifile.close();
		return -3;
	}

	while(get_tag(tuifile, tag) == 0){
		if(tag == TUI_DEVICE){
			long dev_len;
			get_length(tuifile, &dev_len);

			char *dev_buffer = new char[dev_len];
			tuifile.read(dev_buffer, dev_len);

			// Now we should have a tlv
			BERTLV bertlv(string(dev_buffer, dev_len));

			string root_str;
			if(bertlv.getValForTag(TUI_PROP_ROOT_PATH, root_str)){
				string root_path = "flash/" + root_str;
				path pdir = root_path;

				if(!pdir.empty()){
					try{
						create_directories(pdir);

						// At this stage, it is safe to assume that guiserver will be pointed to this directory,
						// so copy the gui.ini file to this path else guiserver will not function properly
						path cur = "gui.ini";
						pdir /= cur;

						copy_file(cur, pdir, copy_option::overwrite_if_exists);
					}catch (filesystem_error &fe){
						cout << "Error " << fe.what() << endl;
						delete[] dev_buffer;
						tuifile.close();
						return -4;
					}
				}
			}

			delete[] dev_buffer;

		}else if(tag == TUI_LANGUAGE){
			long lang_len;
			get_length(tuifile, &lang_len);

			while(get_tag(tuifile, tag) == 0){
				string tlv(tag);

				if(tag == TUI_DGTMPL){
					get_length(tuifile, &len);

					// Get the value
					char *buffer = new char [len];
					tuifile.read(buffer, len);

					// Now we should have a tlv
					BERTLV bertlv(string(buffer, len));

					// Get template id, which will become the name of the file to export to
					string templ_id;
					if(bertlv.getValForTag(TUI_PROP_ID, templ_id)){

						// Extract the html from the template and write it to a file
						string tmpl_html;
						if(bertlv.getValForTag(TUI_PROP_HTML, tmpl_html)){
							string templ_filename = "flash/"+templ_id+"."+ext;

							try{
								// Make sure that the directory exist before attempting to open the file
								path p = templ_filename;
								path pdir = p.remove_filename();	// Get the directory without filename portion

								if(!pdir.empty()){
									create_directories(pdir);
								}
							}catch (filesystem_error &fe){
								cout << "Error " << fe.what() << endl;
								delete[] buffer;
								tuifile.close();
								return -4;
							}

							// Export the html to a file
							ofstream tmpl_file;

							tmpl_file.open(templ_filename.c_str(), ofstream::out);// | ofstream::binary);

							if(!tmpl_file.is_open()){
								delete[] buffer;
								tuifile.close();
								return -5;
							}

							// Trim any formatting characters before exporting
							tmpl_file << tmpl_html;	// Skip the id and export everything after it

							tmpl_file.close();
						}
					}

					delete[] buffer;
				}else{
					// Skip tag for now
					get_length(tuifile, &len);
					tuifile.seekg(len, tuifile.cur);
				}
			}
		}
		else{
			// Skip tag for now
			get_length(tuifile, &len);
			tuifile.seekg(len, tuifile.cur);
		}
	}

	tuifile.close();

	return 1;
}

int get_tag(ifstream &fstr, string &tag)
{
	stringstream ss;

	if(!fstr.good())
		return -2;

	char c = fstr.get();

	if((c & 0x1f) > 0)
		ss << c;
	else
		return -1;

	do{
		c = fstr.get();
		ss << c;
	}while((c & 0x80) && (!fstr.eof()));

	tag = ss.str();

	return 0;
}

int get_length(ifstream &fstr, long *length)
{
	if(!fstr.good())
			return -2;

	int lenb=0;
	stringstream ss;
	char c = fstr.get();
	*length = 0;

	if(c & 0x80)
		lenb = (c & 0x7f);	// Get the number of length bytes

	if(lenb == 0){
		*length = (long)c;
		return 1;
	}

	for(int i=0; i<lenb-1; i++){
		c = fstr.get();
		*length |= (long)c;
		*length = *length << 8;
	}

	*length += fstr.get();

	return lenb+1;
}

string disp_hex(string &data)
{
	stringstream ss;

	ss << hex;

	for (int i = 0; i < (int)data.length(); ++i) {
			if(static_cast<unsigned int>(static_cast<unsigned char>(data[i])) <= 0x0f)
				ss << '0';
			ss << static_cast<unsigned int>(static_cast<unsigned char>(data[i]));
	}

	return ss.str();
}
}
