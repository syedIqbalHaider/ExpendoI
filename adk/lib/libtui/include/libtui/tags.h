#ifndef TAGS_H_
#define TAGS_H_

#define TUI						"\xFF\xAE\x81"
#define TUI_TEMPLATE					TUI"\x01"
#define TUI_DEVICE						TUI"\x02"
#define TUI_LANGUAGE					TUI"\x03"
#define TUI_DISPLAY						TUI"\x04"
#define TUI_DGTMPL						TUI"\x05"
#define TUI_IMAGEURL					TUI"\x06"
#define TUI_IMAGE						TUI"\x07"
#define TUI_STRING						TUI"\x08"
#define TUI_REPORT						TUI"\x09"
#define TUI_LINE						TUI"\x0A"

#define TUI_PROP			"\xDF\xAE\x82"
#define TUI_PROP_TERM_TYPE		TUI_PROP"\x01"
#define TUI_PROP_DSP_ROWS		TUI_PROP"\x02"
#define TUI_PROP_DSP_COLS		TUI_PROP"\x03"
#define TUI_PROP_PRN_WIDTH		TUI_PROP"\x04"
#define TUI_PROP_ID				TUI_PROP"\x05"
#define TUI_PROP_LANG_NAME		TUI_PROP"\x06"
#define TUI_PROP_LANG_CODE		TUI_PROP"\x07"
#define TUI_PROP_HTML			TUI_PROP"\x08"
#define TUI_PROP_VERSION		TUI_PROP"\x09"
#define TUI_PROP_PATH			TUI_PROP"\x0A"
#define TUI_PROP_DATA			TUI_PROP"\x0B"
#define TUI_PROP_ROOT_PATH		TUI_PROP"\x0C"
#define TUI_PROP_TEXT			TUI_PROP"\x0D"
#define TUI_PROP_VALUE			TUI_PROP"\x0E"
#define TUI_PROP_JUSTIFY		TUI_PROP"\x0F"
#define TUI_PROP_FONT			TUI_PROP"\x10"

#define TUI_VAL				"\xDF\xAE\x83"
#define TUI_VAL_LEFT			TUI_VAL"\x01"
#define TUI_VAL_CENTRE			TUI_VAL"\x02"
#define TUI_VAL_RIGHT			TUI_VAL"\x03"
#define TUI_VAL_NORMAL			TUI_VAL"\x04"
#define TUI_VAL_WIDE			TUI_VAL"\x05"
#define TUI_VAL_HIGH			TUI_VAL"\x06"
#define TUI_VAL_INVERSE			TUI_VAL"\x07"

#endif /* TAGS_H_ */
