#ifndef _UI_H_
#define _UI_H_

#include <string>
#include <map>
#include <vector>
#include <iostream>

//using namespace std;
using std::map;
using std::vector;
using std::string;
using std::ifstream;
using std::stringstream;
using std::ofstream;
using std::cout;
using std::endl;
using std::hex;


namespace za_co_verifone_tui{

int TuiInit(string appname);

int TuiTemplate(string id, string &template_data);

int TuiTemplate(string tmplid, string id, string &template_data);

string TuiString(string id);

int TuiReport(string id, vector<string>&lines, map<string, string>&values);

}
#endif // _UI_H_
