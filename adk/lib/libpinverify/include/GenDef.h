/*-------------------------------------------------------------------------
   File :	    GenDef.h
   Project :    SC5000 EMV Application - Geniric Application Defintitions Header
   Library :    n/a
   Description:
   Notes :
   Modification History :
     #     Date      Who		     Description
   ----- -------- ---------- ---------------------------------------------
     1   01-10-25 Ricardo_L1 Creation
	 2   01-12-05 Ramon_C1  adding new variable

 **************************************************************************

   Copyright (C) 1999-2004 by VeriFone, Inc.

   All rights reserved.  No part of this software may be reproduced,
   transmitted, transcribed, stored in a retrieval system, or translated
   into any language or computer language, in any form or by any means,
   electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
   without the prior written permission of VeriFone, Inc.
---------------------------------------------------------------------------*/

#ifndef _GENDEF_H_
#define _GENDEF_H_

#define FALSE	      0
#define TRUE	      1

#define LEFT         0
#define RIGHT        1
#define MIDDLE       2
#define CENTER			2

#define SPACE_CHAR   32

#define SPACE_CHARACTER 0x20
#define SUB          0x1A

#define UNREF_PAR(x) x=x

typedef unsigned char   uchar;
typedef unsigned int    uint;
typedef unsigned short  ushort;
typedef unsigned long   ulong;

/* Node Type - Terminal Model*/
#define UNKNOWN_TYPE     '\0'
#define VX670_TYPE        'D'
#define VX610_TYPE        'B'
#define VX810_TYPE        'G'
#define VX700_TYPE        'H'

#ifdef DTERM_TYPE
#  if (DTERM_TYPE==VX700_TYPE)
#    define VX700
#  endif
#endif

#ifdef VX700
#  include <extlibvoy.h>
#  define g_iCustomerCard EXT_CUSTOMER_CARD
#  define READER_DEVICES  // DEV_COM2, "/dev/crypto",
#else
#  define g_iCustomerCard CUSTOMER_CARD
#  define READER_DEVICES  DEV_ICC1,
#endif

#define APDU_BUFFER_SIZE   322
#define DISPLAY_WIDTH      16

#endif /* _GENDEF_H_ */
