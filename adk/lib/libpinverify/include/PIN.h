/******************************************************************************

  FILE: pin.h

  DESC: functions contained within the PIN Module
  
******************************************************************************/

#ifndef PIN_H
#define PIN_H


void GetPINModuleVersion( void );
void get_pin(unsigned char *data, unsigned short data_length);
void load_pin_key(unsigned char *data, unsigned short data_length);


#endif


