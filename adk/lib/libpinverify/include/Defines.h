#ifndef DEFINES_H
#define DEFINES_H

#define ACK						0x06	
#define NAK 				    0x15
                                       
#define NUM_RETRIES			    4
                                       
#define SECONDS				    114.42
                                       
#define MAX_PIN_LENGTH         12
#define MIN_PIN_LENGTH         4

/*
 * Token ids used in display string
 */

#define INVALID_TOKEN           1
#define STRING                  2
#define BACKSLASH               3
#define NEWLINE                 4
#define CENTER_TEXT             5
#define SMALL_FONT              6
#define LARGE_FONT              7
                                    
/*                                  
 * Commands                         
 */                                 
                                    
#define CARD_REQUEST                    "EM"
#define DISPLAY_REQUEST                 "DM"
#define GET_PIN_REQUEST                 "GP"
#define CARD_STATUS_REQUEST             "CS"
#define CARD_DATA_REQUEST               "CD"
#define CLEAR_RESET_REQUEST             "RS"
#define RESET_REQUEST                   "RE"
#define REBOOT_REQUEST                  "RB"
#define BEEP_REQUEST                    "BP"
#define PROBE_REQUEST                   "PB"
#define CANCEL_REQUEST                  "CA"
#define SELECT_FROM_MENU_REQUEST        "MU"
#define CARDHOLDER_CONF_REQUEST         "CM"
#define LOAD_PIN_KEY_REQUEST            "LK"
#define SWITCH_APP_REQUEST              "SW"
#define PIN_MODULE_VERSION_REQUEST      "PL"
#define BACKLIGHT_REQUEST               "BL"

/*                                  
 * Keys                             
 */                                 
                                    
#define ENTER_KEY			    0x0d
#define CANCEL_KEY              0x18
#define CLEAR_KEY               0x09
#define NUMERIC_KEY_1			0x31
#define NUMERIC_KEY_2			0x32
#define NUMERIC_KEY_3			0x33
#define FUNCTION_KEY_1			0xF1
#define FUNCTION_KEY_2			0xF2
#define FUNCTION_KEY_3			0xF3

//#define NO_CRC

#define MAX_REC_PACKET_LENGTH	511
#define MAX_SEND_PACKET_LENGTH  500
#define MAX_PAYLOAD_LENGTH      502
#define MAX_APDU_BUFFER_SIZE    257  // CVN 255 bytes data + 2 bytes SW1SW2

#define MAX_TRACK1				120 
#define MAX_TRACK2              50
#define MAX_TRACK3              120

#define REQ_HEADER     			"RQ" 
#define RESP_HEADER             "RP"

/*
 * Display modes
 */

#define TEXTDSP					0x01
#define SYS6X8					0x02

/*
 * TLV
 */

#define MAX_TLV_LENGTH			80

/*
 * RSA
 */

#define MAX_MODULUS_LENGTH		248	

/*
 * Configuration parameters
 */

#define CNF_FILE				"CONFIG.SYS"
#define	CNF_BAUD				"#BAUD"
#define	CNF_FORMAT				"#FORMAT"
#define	CNF_INTERCHAR			"#INTERCHAR"

/*
 * CRC-16 Polynomial
 */

#define POLYNOMIAL				0xA001

typedef unsigned char byte;

struct token
{
    int id;
    byte attrib[MAX_PAYLOAD_LENGTH + 1];
    int size;
};

struct cursor
{
    int row;
    int col;
};

struct menu_tlv
{
    byte tag[2];
	int length;
	byte value[MAX_TLV_LENGTH];
};

unsigned short crc_table[256];

#endif
