/*-------------------------------------------------------------------------
   File :	    protocol.h
   Project :     SC5000 EMV Application - Protocol & Communication Header
   Library :    n/a
   Description: 
   Notes :	  
   Modification History :
     #     Date      Who		     Description
   ----- -------- ---------- ---------------------------------------------
     1   01-10-25 Ricardo_L1 Creation

 **************************************************************************

   Copyright (C) 1999-2004 by VeriFone, Inc.

   All rights reserved.  No part of this software may be reproduced,
   transmitted, transcribed, stored in a retrieval system, or translated
   into any language or computer language, in any form or by any means,
   electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
   without the prior written permission of VeriFone, Inc.
---------------------------------------------------------------------------*/

#ifndef _PROTOCOL_H_
#define _PROTOCOL_H_

#include "GenDef.h"

/* Special characters */

#define SI              0x0F    /* shift in */
#define SO              0x0E    /* shift out */
#define STX             0x02
#define ETX             0x03
#define ENQ             0x05
#define SUB             0x1A    /* substitute */
#define EOT             0x04    /* end of transaction */
#define ACK             0x06    /* acknowledge */
#define NAK             0x15    /* not acknowledge */
#define FS              0x1C    /* field seperator */
#define ESC             0x1B    /* escape ? */
#define CAN             0x18
#define VT              0x0B
#define BEL             0x07    /* bell */

#define REC_PCKT_START  0x00

//#ifdef  __cplusplus
//extern "C" {
//#endif
	int iPROT_InitSc5kUART      (uchar fDisplay);
	int iPROT_Sc5kSendPacket    (uchar ucPacketStart);
	int iPROT_SendByte      (uchar ucChar);
	int iPROT_ReceivePacket (void);
	int iPROT_VerifyPacket  (void);
//#ifdef  __cplusplus
//}
//#endif


#endif /* _PROTOCOL_H_ */
