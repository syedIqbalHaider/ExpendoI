/*-------------------------------------------------------------------------
   File :	    Ecl.h
   Project :    SC5000 EMV Application - EMV Command Library Header
   Library :    n/a
   Description: 
   Notes :	  
   Modification History :
     #     Date          Who		     Description
   ----- --------       ---------- ---------------------------------------------
     1   01-11-26       Ricardo_L1 Creation
     2  17/11/2003      Ravi_B3    As part of EMV Module 2.1, EMVG and ECL messages are merged into a 
                                   single file and all the display messages are retrieved from the file. 
                                   The no. total messages used are 54.


 **************************************************************************

   Copyright (C) 1999-2004 by VeriFone, Inc.

   All rights reserved.  No part of this software may be reproduced,
   transmitted, transcribed, stored in a retrieval system, or translated
   into any language or computer language, in any form or by any means,
   electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
   without the prior written permission of VeriFone, Inc.
---------------------------------------------------------------------------*/

#ifndef _ECL_H_
#define _ECL_H_

extern "C"
{
#include "vxemvap.h"
}

// ECL  returns table
// ------------------
// ITS RANGE is -201 TO -500

#define RET_ECL_OK                              0
#define RET_ECL_UNDEFINED                    -201
#define RET_ECL_UNKNOWN_PACKET               -202
#define RET_ECL_INVALID_PARAM                -203
#define RET_ECL_EMV_FAILURE                  -204
#define RET_ECL_EMV_SIGNATURE                -205
#define RET_ECL_EMV_ONLINE_PIN               -206
#define RET_ECL_E_TRANS_CANCELLED            -207                   
#define RET_ECL_E_NONE_COMMON                -208
#define RET_ECL_E_INVALID_APP                -210
#define RET_ECL_E_DO_ABSENT                  -211
#define RET_ECL_E_DO_REPEAT                  -212
#define RET_ECL_E_INVALID_PIN                -213
#define RET_ECL_E_PIN_LAST_CHANCE            -214
#define RET_ECL_E_PIN_RETRYLIMIT             -215
#define RET_ECL_E_GO_OFFLINE                 -216
#define RET_ECL_E_GO_ONLINE                  -217
#define RET_ECL_E_DECLINE                    -218
#define RET_ECL_E_INVALID_SEQUENCE           -219
#define RET_ECL_E_TLVCOLLECTION_FULL         -220
#define RET_ECL_E_TLVFORMAT                  -221
#define RET_ECL_E_INVALID_PDOL               -222
#define RET_ECL_E_INVALID_CDOL               -223
#define RET_ECL_E_INVALID_TDOL               -224
#define RET_ECL_E_INVALID_DDOL               -225
#define RET_ECL_E_SDA_FAILED                 -226
#define RET_ECL_E_DDA_FAILED                 -227
#define RET_ECL_E_AID_LIST_FULL              -228
#define RET_ECL_E_ICC_BLOCKED                -229
#define RET_ECL_ICC_PSE_NOT_FOUND            -230
#define RET_ECL_E_BAD_ICC_RESPONSE           -231
#define RET_ECL_E_ICC_DATA_MISSING           -232
#define RET_ECL_E_CANDIDATELIST_EMPTY        -233
#define RET_ECL_E_6A02_FORMAT                -234
#define RET_ECL_E_6A03_FORMAT                -235
#define RET_ECL_E_6A04_FORMAT                -236
#define RET_ECL_E_6A05_FORMAT                -237
#define RET_ECL_E_0x6983                     -238
#define RET_ECL_E_0x6984                     -239
#define RET_ECL_E_HASH_FAILED                -240
#define RET_ECL_E_PAN_FAILED                 -241
#define RET_ECL_E_CERTIFICATE_EXPIRED        -242
#define RET_ECL_E_HASHALGO                   -243
#define RET_ECL_E_IPKALGO                    -244
#define RET_ECL_E_9F37_ABSENT                -245
#define RET_ECL_E_DIFF_AVN_ERR               -246
#define RET_ECL_E_SERVICE_NOT_ALLOWED        -247
#define RET_ECL_E_APP_NOTYET_EFFECTIVE       -248  
#define RET_ECL_E_APP_EXPIRED                -249
#define RET_ECL_E_TXN_RAND_SELECT            -250
#define RET_ECL_E_TXN_OVER_FLOOR             -251
#define RET_ECL_E_UPPER_LOWER_LMT_EXCEED     -252
#define RET_ECL_E_LOWER_LMT_EXCEED           -253
#define RET_ECL_E_UPPER_LMT_EXCEED           -254
#define RET_ECL_E_NEW_CARD                   -255
#define RET_ECL_E_CVM_FAILED                 -256
#define RET_ECL_E_PINPAD_ABSENT              -257
#define RET_ECL_E_PIN_REQD_ABSENT            -258
#define RET_ECL_E_UNRECOGNIZED_CVM           -259
#define RET_ECL_E_CVM_PASSED                 -260
#define RET_ECL_E_BAD_DATA_FORMAT            -261
#define RET_ECL_E_BAD_HASHALGO               -262
#define RET_ECL_E_BAD_IPKALGO                -263
#define RET_ECL_E_STATWORD_WARNING           -264
#define RET_ECL_E_STATWORD_NORMAL            -265
#define RET_ECL_E_SCRIPTPROCFAILBEFORE_GENAC -266
#define RET_ECL_E_SCRIPTPROCFAILAFTER_GENAC  -267
#define RET_ECL_E_INVALID_LENGTH             -268
#define RET_ECL_E_TAG_NOTFOUND               -269
#define RET_ECL_E_NO_MORE_TLV                -270
#define RET_ECL_E_INVALID_OFFSET             -271
#define RET_ECL_E_CHIP_ERROR                 -272
#define RET_ECL_E_BAD_COMMAND                -273
#define RET_ECL_E_TAG_ALREADY_PRESENT        -274
#define RET_ECL_E_CARD_REMOVED               -275
#define RET_ECL_E_READ_RECORD_FAILED         -276
#define RET_ECL_E_INVALID_SDOL               -277    
#define RET_ECL_E_EXPLICIT_SELEC_REQD        -278
#define RET_ECL_E_COND_NOT_SATISFIED         -279
#define RET_ECL_E_CSN_FAILURE                -280

#define RET_ECL_E_WRONG_PIN_PARAM            -281
#define RET_ECL_E_BAD_SLOT                   -282
#define RET_ECL_E_BAD_LEN                    -283
#define RET_ECL_E_NO_MEM                     -284
#define RET_ECL_E_BAD_EXP_LEN                -285
#define RET_ECL_E_BAD_EXP                    -286
#define RET_ECL_E_INVALID_KEY                -287
#define RET_ECL_E_INVALID_ALGO               -288
#define RET_ECL_E_NO_CERT                    -289
#define RET_ECL_E_NO_PIN_BLOCK               -290
#define RET_ECL_E_BAD_PIN_LEN                -291
#define RET_ECL_E_BAD_PIN_CHAR               -292
#define RET_ECL_E_BAD_SC_CALL                -293
#define RET_ECL_E_BAD_PIN_KEY_TYPE           -294
#define RET_ECL_E_BAD_PIN_KEY_LEN            -295
#define RET_ECL_E_BAD_SAVE_TYPE              -296
#define RET_ECL_E_COMBINED_DDA_AC_GEN_REQD   -297
#define RET_ECL_E_COMBINED_DDA_AC_GEN_FAILED -298
#define RET_ECL_E_CAPK_NOT_FOUND             -299
#define RET_ECL_E_CAPK_CHECKSUM              -300
#define RET_ECL_E_CAPK_CHECKSUM_ABSENT       -301

#define RET_ECL_EST_ERROR                    -302
#define RET_ECL_MVT_ERROR                    -303
#define RET_ECL_SCRIPT_ERR                   -304
#define RET_ECL_TIMEOUT                      -305
#define RET_ECL_CANCELLED                    -306
#define RET_ECL_CARDSLOT_ERR                 -307
#define RET_ECL_USE_CHIP_READER              -308
#define RET_ECL_USE_MAG_STRIPE               -309
#define RET_ECL_BAD_TRACK2                   -310
#define RET_ECL_MORE_PACKETS_AVAILABLE       -311
#define RET_ECL_NON_EMV_CARD                 -312
#define RET_ECL_PREV_STEP_MISSING            -313
#define RET_ECL_LOG_FAIL                     -314
#define RET_ECL_E_0x6985                     -315
#define RET_ECL_E_CARD_INSERTED              -316

// Kamil_P1: Added error codes so that we can cope with additional error 
#define RET_ECL_PIN_BYPASSED                 -317


#define MAX_APPL_TLVS      180

//NKJT #define MAX_DEF_TDOL       32
//NKJT #define MAX_DEF_DDOL       48
#define MAX_ISS_SCRIPT_RES 25		// Max size of Issuer Script Results (5 scripts processed)

/* Values for EMV card decision flag */
#define EMV_GO_ONLINE      -1
#define EMV_OFFLINE_AUTH   -2
#define EMV_DECLINE        -3
#define EMV_VOICE_REFERRAL -4


/* Values EMVGlobConfig.ucCardType */
#define NON_CHIP_CARD        0
#define EMV_CARD             1
#define NON_EMV_CARD         2

// Host or referral decisions
#define HOST_AUTHORISED       1
#define HOST_DECLINED         2
#define FAILED_TO_CONNECT     3
#define REFERRAL_AUTHORISED   4
#define REFERRAL_DECLINED     5

#define GEN_AAR               0xC0
#define GEN_AAC               0x00
#define GEN_TC                0x40
#define GEN_ARQC              0x80

// Merchant Decision
#define NO_DECISION           0
#define FORCE_ONLINE          1
#define FORCE_DECLINE			2

#define EMV_MAX_ISSUER_AUTH_SIZE    16

#define EMV_APP_NOT_AVAIL	   0x6985

/* Defines required by EMV */
#define TC_GEN1AC_APPROVE       "Y1" // Off line approval
#define AAC_GEN1AC_DECLINE      "Z1" // Off line decline
#define TC_GEN2AC_APPROVE       "Y3" // Unable to connect approval
#define AAC_GEN2AC_DECLINE      "Z3" // Unable to connect decline
#define TC_REFERRAL_APPROVE     "Y2" // Referral approval
#define AAC_REFERRAL_DECLINE    "Z2" // Referral decline

#define CREDIT_TRAN            0
#define DEBIT_TRAN             1

// Display messages
//  17/11/2003     Ravi_B3    As part of EMV Module 2.1, EMVG and ECL messages are merged into a 
//                                   single file and all the display messages are retrieved from the file. 
//                                   The no. total messages used are 54.

/*
#define MSG_ECL_PLS_REMOVE_CARD                      0
#define MSG_ECL_ENTER_PIN                            1
#define MSG_ECL_APP_NOT_AVAIL                        2
#define MSG_ECL_SELECT_FAILED                        3
#define MSG_ECL_PROCESSING                           4
#define MSG_ECL_SELECT_STRING                        5
#define MSG_ECL_LANGUAGE_STRING                      6
#define MSG_ECL_CAPK_CHECKSUM_FAILED                 7
#define MSG_ECL_CARD_INSERTED                        8
#define MSG_ECL_CARD_REMOVED                         9
#define MSG_ECL_INVALID_PIN                          10
#define MSG_ECL_PIN_LAST_CHANCE                      11
#define MSG_ECL_PIN_RETRYLIMIT                       12
#define MSG_ECL_CARD_BLOCKED                         13
*/
#define MSG_ECL_PLEASE_WAIT        14


#define MVT_PIN_TIMEOUT          0
#define MVT_PIN_FORMAT           1
#define MVT_PIN_SCRIPT           2
#define MVT_PIN_MACRO            3
#define MVT_KEY_DERIV_FLAG       4
#define MVT_KEY_DERIV_MACRO      5

#define KEY_DERIV_TRADIC         0
#define KEY_DERIV_VIA_SCRIPT     1
#define KEY_DERIV_NONE           2


#define MAX_MESSAGE_SIZE 268


#define CARD_REFERRAL 204
#define NONEMV 207


#define SALE 0x00
#define CASH_ADVANCE 0x01
#define ADJUSTMENT 0x00
#define SALE_WITH_CASHBACK 0x09

// tisha_K1 11/Sep/06
// Added the following for checking the irregular order of packets
#define PKT_CX0_RECVD 0
#define PKT_CX1_RECVD 1
#define PKT_CX2_RECVD 2
#define PKT_CX3_RECVD 3
#define PKT_CX4_RECVD 4

#define DEFAULT_FON     "Default.fon"

#define EMV_APP_LABEL_MAX_LEN    16

#endif /* _ECL_H_ */
