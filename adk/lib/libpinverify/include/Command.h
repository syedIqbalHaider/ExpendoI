#ifndef COMMAND_H
#define COMMAND_H

/* 
 * Command functions
 */

void send_to_card(unsigned char *payload, unsigned short length);
void display(unsigned char *data, unsigned short len);
void get_card_status(unsigned char *data);
void get_card_data(unsigned char *data);
void reset_terminal(void);
void probe_terminal(void);
void select_from_menu(unsigned char *data, unsigned short data_length);
void cardholder_confirmation(unsigned char *data, unsigned short data_length);
void switch_application(unsigned char data);
void display_backlight(unsigned char value);
void reboot_terminal(void);
void clear_reset_terminal(void);
void Beep(void);


/*
 * Other functions
 */


int validate_format(unsigned char *buf);
int validate_cancel_request(unsigned char *packet_data, unsigned short packet_length);
int parse_menu_def_string(struct menu_tlv *template, unsigned char *data, unsigned short data_length);
struct cursor display_format_string(unsigned char *buf);
struct token scan(unsigned char *data);
void setup_keypad(unsigned short funcMask);
void ascii_to_bin(unsigned char *dest, unsigned char *src, int len);
void init_crc(void);
unsigned short compute_crc(unsigned char *strtochk, int length);

#endif

