/******************************************************************************

  FILE: pin.c

  DESC: This file is used to generate a library containing all the PIN entry
        functions for the SC5000 ADPU application.
        This library must contain a embedded version number that indicates
        the version of PIN entry module.
        This library can not be modified with recertification of the POS 
        system with EMV Co.
        Please refer to:
        1) "EMVCo Type Approval Terminal Level 1 Administrative Process"
        2) "EMVCo Type Approval Bulletin No.11"
        
        

******************************************************************************/

// #include "stdio.h"
#include <string.h>
// #include "cardslot.h"
// #include "libCore.h"
// #include "libSecurity.h"
// #include "liberror.h"
#include "Defines.h"
#include "protocol.h"
//#include "command.h"
#include "Card.h"
#include <liblog/logsys.h>
#include <svc_sec.h>
// #include "emvproto.h"
#include <extlibvoy.h>
#include <libfoundation/HWInfo.h>

extern "C" {
#include "emvdef.h"
#include <proto.h>
}

#define LOGGING

using namespace com_verifone_util;

/******************************************************************************
  External variables
******************************************************************************/

/******************************************************************************
  External functions from SC5KEMV
******************************************************************************/
extern unsigned short getUsrPin(unsigned char *);
extern void usEMVDisplayErrorPrompt(unsigned short errorID);
extern int iPC_GenerateRandom( unsigned char * );

/******************************************************************************
  Static Global data
******************************************************************************/
const char PINModuleVersion[] = PINVERIFY_VERSION;


/******************************************************************************
  Static Global data
******************************************************************************/
static unsigned char modulus[MAX_MODULUS_LENGTH];
static unsigned short modulus_length;
static unsigned char exponent[4];
static unsigned short exponent_length;

unsigned char load_pin_key( void );

/******************************************************************************
  Defines for secure script
******************************************************************************/
#define RSA_SLOT 0
#define RSA_MACRO1  0x10
#define RSA_MACRO2  0x11
#define RSA_CORRECT 100
#define RSA_FAILED 101
#define RSA_INVALID_SCRIPT 102
#define RSA_SCRIPT_NOT_PRESENT 103
#define RSA_SYSTEM_ERROR 104


#define RSA_BUFFER_SIZE 300
/*-----------------------------------------------------------------------------
  Get PIN Module Version
-----------------------------------------------------------------------------*/
void GetPINModuleVersion( void )
{
  char buf[20];

  memcpy(buf, "PV", 2);
  buf[2] = strlen(PINModuleVersion) / 0x100;
  buf[3] = strlen(PINModuleVersion) % 0x100;
  memcpy(&buf[4], PINModuleVersion, strlen(PINModuleVersion));

  //send_response((byte *)PIN_MODULE_VERSION_REQUEST, strlen(PINModuleVersion)+4, 
//      (byte *)buf, (byte *)"00");

  return;
}


/*-----------------------------------------------------------------------------
  RSA
-----------------------------------------------------------------------------*/
//Kamil_P1: cleaned up the code. Removed unnecessary buffers. Fixed STS issue.
int RSA(unsigned char *encr, int *encr_len, unsigned char *data,  int data_len, 
    unsigned char *mod, int mod_len, unsigned char *exp, int exp_len)
{
  int ii;
  unsigned short mode = 0;

	union
	{
		short word;
		unsigned char bytes[RSA_BUFFER_SIZE+1];
	}sig;

	union
	{
		short word;
		unsigned char bytes[RSA_BUFFER_SIZE+1];
	}key;

	union
	{
		short word;
		unsigned char bytes[EMV_MAX_DATA_SIZE];
	}stTmpBuf;

  if (data_len != mod_len)
  {
    dlog_msg("Bad data size %d, must match modulus size %d", data_len, mod_len);
    data_len = mod_len;
  }
  if (data_len > RSA_BUFFER_SIZE)
  {
    dlog_msg("Data too long - %d bytes, must be maximum %d bytes", data_len, RSA_BUFFER_SIZE);
    return RSA_SYSTEM_ERROR;
  }

  // try OS rsa function
	if( exp_len == 1 )
	{
		if(exp[0] == 0x02)
			mode = 0;
		else if(exp[0] == 0x03)
			mode = 1;
	  else
	  {
	    dlog_msg("Bad exponent - must be 2 or 3!");
	    return RSA_SYSTEM_ERROR;
	  }
	}
	else if( exp_len == 3 )
	{
	  if (exp[0] == 0x01 && exp[1] == 0x00 && exp[2] == 0x01)
		  mode = 16;
		else 
		{
		  dlog_msg("Bad exponent - must be 65537 (01 00 01!");
		  return RSA_SYSTEM_ERROR;
		}
	}
	
  int offset = 0;
  if (data_len % 2)
  {
    dlog_alert("Bad data len (%d)!", data_len);
    sig.bytes[0] = 0;
    key.bytes[0] = 0;
    offset = 1;
  }
  memcpy( &sig.bytes[offset], data, data_len );
  memcpy( &key.bytes[offset], mod, mod_len );
  memset(stTmpBuf.bytes, 0, sizeof(stTmpBuf));
  #ifdef LOGGING
  dlog_hex(sig.bytes, data_len+offset, "DATA");
  dlog_hex(key.bytes, mod_len+offset, "MODULUS");
  dlog_hex(exp, exp_len, "EXPONENT");
  #endif

  
  int retVal = rsa_calc((unsigned short*)sig.bytes, (unsigned short*)key.bytes, (mod_len+offset)/2, mode, (unsigned short*)stTmpBuf.bytes) ;

  dlog_msg("rsa_calc result %d, output len %d", retVal, data_len);
  if( retVal != 0 )
  {
    dlog_msg("rsa_calc error result %d", retVal);
    return RSA_SYSTEM_ERROR;
  }
  else
  {
    #ifdef LOGGING
    dlog_hex(stTmpBuf.bytes, data_len+offset, "OUTPUT");
    #endif
    // rsa was okay, copy result
    memcpy(encr, &stTmpBuf.bytes[offset], data_len);
    *encr_len = data_len;
  }

  return(RSA_CORRECT);
}


/* ******************************************************************
 *
 * verify_pin - performs pin verification direct to the EMV card
 *              gets the PIN from the user then sends commands 
                directly to the card
 *
 * parameters:
 *
 * ucPinType    0x01 - plaintext pin verification
 *              0x04 - enciphered pin verification
 *
 * ucEntryType  0x00 - normal entry,
                       handles retries if pin is incorrect 
 *              0x01 - atomic entry, 
                       performs one single pin 
                       entry/verification and returns result
 *
 * returns:
 */
   #define RESP_PIN_SUCCESS          RET_ECL_OK                 
/*    0 ->  0 - PIN accepted by card          */

   #define RESP_PIN_INVALID_PARAM    RET_ECL_INVALID_PARAM      
/* -203 ->  2 - Invalid parameter to function */

   #define RESP_PIN_CANCELLED        RET_ECL_E_TRANS_CANCELLED  
/* -207 ->  8 - PIN cancelled ??              */

   #define RESP_PIN_SLOT_ERROR       RET_ECL_CARDSLOT_ERR       
/* -307 -> 10 - ADPU message send failed      */

   #define RESP_PIN_BAD_ICC_RESPONSE RET_ECL_E_BAD_ICC_RESPONSE 
/* -231 -> 21 - Challenge response failed     */

   #define RESP_PIN_CARD_REMOVED     RET_ECL_E_CARD_REMOVED     
/* -275 -> 23 - Card not present              */

   #define RESP_PIN_RETRY            RET_ECL_E_DO_REPEAT        
/* -212 -> 28 - retry PIN entry               */

   #define RESP_PIN_FAILED           RET_ECL_E_INVALID_PIN      
/* -213 -> 29 - last try PIN failed           */

   #define RESP_PIN_EXCEEDCOUNTER    RET_ECL_E_PIN_RETRYLIMIT   
/* -215 -> 31 - PIN try counter is zero       */

   #define RESP_PIN_KEY_MISSING      RET_ECL_E_INVALID_KEY      
/* -287 -> 70 - Cannot get Key                */

   #define RESP_PIN_INTERNAL_FAILURE RET_ECL_EMV_FAILURE        
/* -204 -> 99 - RSA algorithm failed          */

   #define RESP_PIN_BYPASSED         RET_ECL_PIN_BYPASSED
/* -317 -> xx - PIN Bypass was executed       */


/* ************************************************************** */
                        
#define PIN_TYPE_PLAIN_TEXT 0x01
#define PIN_TYPE_ENCIPHERED 0x04

#define ENTRY_TYPE_NORMAL   0x00
#define ENTRY_TYPE_ATOMIC   0x01

#define DEFAULT_PIN_TRIES   3

int verify_pin( unsigned char    ucPinType, 
                unsigned char    ucEntryType,
                unsigned short * pusStatus )
{
    unsigned short  ushPinLength;
    byte pin_number[MAX_PIN_LENGTH+1];
    byte pin_block[256];
    int pin_block_length;
    byte command_buffer[270];
    byte receive_buffer[270];
    unsigned short receive_buffer_len;
    char card_result;
    byte sw1, sw2;
    byte icc_unpredictable_num[8];
    byte pin_encipherment_data[256];
    byte ucPinTryCtr;
    unsigned short usLen;
    unsigned short usResp;
    unsigned long scr_reader = CUSTOMER_CARD;
    HWInfo info;

    int i;

    /*
    * check ucEntryType is allowed
    */

    if( ENTRY_TYPE_NORMAL != ucEntryType
     && ENTRY_TYPE_ATOMIC != ucEntryType )
    {
        /*----------------------------------
        * ucEntryType parameter NOT allowed
        */
        dlog_msg("error: invalid entry type %d",
                     ucEntryType);   

        return RESP_PIN_INVALID_PARAM;
    }

    /*
    * check ucPinType is allowed
    */
 
    if( PIN_TYPE_PLAIN_TEXT != ucPinType
     && PIN_TYPE_ENCIPHERED != ucPinType )
    {
        /*--------------------------------
        * ucPinType parameter NOT allowed
        */

        dlog_msg("error: invalid pin type %d",
                     ucPinType);   

        return RESP_PIN_INVALID_PARAM;
    }

    /*
    * if enciphered PIN required,
    * check that required data is present in collxn
    */
 
    if( PIN_TYPE_ENCIPHERED == ucPinType )
    {
        if( load_pin_key() == FALSE )
        {
            /*--------------------------- 
            * Public key cannot be found
            */

            dlog_msg("error: load_pin_key failed");   

            return RESP_PIN_KEY_MISSING;
        }
    }

    if (info->getTerminalType() == Vx700)
    {
        dlog_msg("External reader (vx700) detected");
        scr_reader = EXT_CUSTOMER_CARD;
        if(EXT_IFD_Is_ICC_Present() != EXT_IFD_Success)
        {
            dlog_msg("error: card absent");
            return RESP_PIN_CARD_REMOVED;
        }

    }
    else
    {
        if (Get_Card_State(CUSTOMER_CARD) != CARD_PRESENT)
        {
            dlog_msg("error: card absent");
            return RESP_PIN_CARD_REMOVED;
        }
    }


    /* 
    * get the pin try counter
    * check if it's in the collection already
    */

    usResp = usEMVGetTLVFromColxn( (ushort)TAG_9F17_PIN_TRY_COUNTER,
                                   &ucPinTryCtr,
                                   &usLen);

    if (usResp == EMV_SUCCESS)
    {
        dlog_msg("got pin try counter, val=%d",
                     ucPinTryCtr );
    }
    else
    {
        dlog_msg("pin try counter not in collxn,\
                     returned = %x", usResp); 

        /* 
        * if not, send command straight to card to get it,
        * then save back to collxn
        * get tag 9F17 command: "\x80\xCA\x9F\x17\x00"
        */

        command_buffer[0] = 0x80;  /* CLA */
        command_buffer[1] = 0xCA;  /* ILS */
        command_buffer[2] = 0x9F;  /* P1  */
        command_buffer[3] = 0x17;  /* P2  */
        command_buffer[4] = 0x00;  /* Lc  */

        card_result = Transmit_APDU( scr_reader,
                                     command_buffer,
                                     5,
                                     receive_buffer,
                                     &receive_buffer_len );

        if (card_result == CARDSLOT_SUCCESS)
        {
//            dlog_msg("resp length=%d",receive_buffer_len );
//            for( i=0; i<receive_buffer_len; i++ )
//            {
//                dlog_msg("resp[%d]=%2.2X", i, receive_buffer[i]); 
//            }
            
            /* 
            * check the status bytes to check result
            */

            sw1 = receive_buffer[receive_buffer_len - 2];
            sw2 = receive_buffer[receive_buffer_len - 1];
            if( sw1 != 0x90 || sw2 != 0x00 )
            {
                dlog_msg("error getting PIN try counter, \
                             sw1sw2 = 0x%2.2X%2.2X", sw1, sw2); 
                ucPinTryCtr = DEFAULT_PIN_TRIES;  // Set to default
                dlog_msg("pin try counter set to 3 - default value");
            }
            else if( receive_buffer_len >= 6 
                  && receive_buffer[2] == 0x01 )
            {
                /* 
                * success, get pin try counter data,
                * format is 9F 17 01 nn 90 00
                */
                ucPinTryCtr = receive_buffer[3];

                dlog_msg("set pin try counter, val=%d",
                     ucPinTryCtr );
            }
            else
            {
                dlog_msg("bad data format, buf_len=%d, buf[2]=0x%2.2x",
                             receive_buffer_len,
                             receive_buffer[2]);
                /* 
                * default to 3 
                */

                ucPinTryCtr = DEFAULT_PIN_TRIES;
            }
        }
        else
        {
            dlog_msg("card_result=%d", card_result); 
            
            /* 
            * default to 3 
            */

            ucPinTryCtr = DEFAULT_PIN_TRIES;
        }

        /*
        * update collxn with counter value
        */
        usResp = usEMVAddTLVToCollxn( (ushort)TAG_9F17_PIN_TRY_COUNTER,
                                &ucPinTryCtr,
                                1 );

        dlog_msg("usEMVAddTLVToCollxn returned %u", usResp);
    }

    if (ucPinTryCtr == 0)
    {
       dlog_msg("error: pin try counter is zero !");

       /*------------------------------- 
       * PIN counter exceeded its value
       */

       return RESP_PIN_EXCEEDCOUNTER;
    }
    
    if ( ucPinTryCtr > DEFAULT_PIN_TRIES )
    {
        dlog_msg("error: bad pin try counter, val=%d",
                     ucPinTryCtr ); 
    }

    /* 
    * get the PIN
    */

    do
    {
        dlog_msg("before pin entry"); 
        if (ucPinTryCtr == 1)
        {
            dlog_msg("Last PIN Try!");
            usEMVDisplayErrorPrompt(E_LAST_PIN_TRY);
        }

        /*
        * calls VerixVDisplayPINPrompt
        * updates gusTransCancld
        * returns the PIN length if the PIN is entered successfully
        * pstPin has PIN filled.
        * E_PIN_BYPASSED: This value should be returned
        * by the application if the secure PIN
        * is not performed and the PIN is bypassed.
        * E_PIN_CANCELLED: This value should be returned
        * by the application if the secure PIN is not performed
        * and the PIN processing is cancelled.
        * On time out, application must return this value.
        */

        ushPinLength = getUsrPin( pin_number );

        dlog_msg("getUsrPin returned %d", ushPinLength);

        if (ushPinLength < MIN_PIN_LENGTH || ushPinLength > MAX_PIN_LENGTH)
        {
            if (info->getTerminalType() == Vx700)
            {
                if(EXT_IFD_Is_ICC_Present() != EXT_IFD_Success)
                {
                    ushPinLength = E_CARD_REMOVED;
                }
            }
            else
            {
                if (Get_Card_State(CUSTOMER_CARD) != CARD_PRESENT)
                {
                    ushPinLength = E_CARD_REMOVED;
                }
            }

            if (ushPinLength == E_USR_PIN_CANCELLED)
            {
              /*------------------------------- 
              * User cancelled PIN transaction
              */
         
              return RESP_PIN_CANCELLED;
            }
            //Rafal_P2: Bug #2850 FIX - Terminal continues processing after card removed
            else if (ushPinLength == E_CARD_REMOVED)
            {
              /*---------------------- 
              * User removed the card
              */
            
              return RESP_PIN_CARD_REMOVED;
            } 
            else if (ushPinLength == E_USR_PIN_BYPASSED)
            {
              /*---------------------- 
              * PIN Bypassed
              */
              return RESP_PIN_CANCELLED;
            }
            else if (ushPinLength == E_USR_ABORT)
            {
              /*---------------------- 
              * PIN Aborted
              */
              return RESP_PIN_CANCELLED;
            }
            /*
              Should NEVER be here... 
            */
            dlog_msg("ERROR: Unmapped error!! Returning 0x%X", RESP_PIN_FAILED);
            return RESP_PIN_FAILED;
        }
        /*
        * Decrement and update PIN counter value
        */
      
        ucPinTryCtr -= 1;
        usResp = usEMVUpdateTLVInCollxn( (ushort)TAG_9F17_PIN_TRY_COUNTER,
                                &ucPinTryCtr,
                                1 );
        dlog_msg("usEMVUpdateTLVInCollxn returned %u", usResp);

        /*
        * fill the remainder of the pin number buffer with F's
        */

        for( i=strlen((char *)pin_number); i<(sizeof(pin_number)-1); i++ )
            pin_number[i] = 'f';

        /*
        * null-terminate buffer
        */
     
        pin_number[sizeof(pin_number)-1] = 0;
        
        dlog_msg("pin entry result = %d", ushPinLength ); 
        
        // TODO: check for cancellation, etc here
        
        /*
        * Format plaintext 8 byte pin block
        */

        pin_block[0] = 0x20 + ushPinLength;
        ascii_to_bin(pin_block + 1, pin_number, MAX_PIN_LENGTH);
        pin_block[7] = 0xff;

        dlog_msg("pin block:");
        for( i=0; i<8; i++ )
        {
            dlog_msg("block[%d]=%2.2X", i, pin_block[i]); 
        }

        if( PIN_TYPE_ENCIPHERED == ucPinType )
        {
            dlog_msg("enciphered pin"); 

            /*
            * Enciphered pin - see IV-27 EMV'96 Card
            */

            command_buffer[0] = 0x00;  /* CLA */
            command_buffer[1] = 0x84;  /* ILS */
            command_buffer[2] = 0x00;  /* P1  */
            command_buffer[3] = 0x00;  /* P2  */
            command_buffer[4] = 0x00;  /* Lc  */

            /*
            * Get Challenge command
            */

            card_result = Transmit_APDU( scr_reader,
                                         command_buffer, 
                                         5,
                                         receive_buffer,
                                         &receive_buffer_len);

            if (card_result == CARDSLOT_SUCCESS
            &&  receive_buffer_len == 10
            && (  (receive_buffer[8] == 0x90)
               && (receive_buffer[9] == 0x00) ))
            {
                memcpy(icc_unpredictable_num, receive_buffer, 8);
            }
            else
            {
                // send_response((byte *)GET_PIN_REQUEST, 0, NULL, (byte *)"09");
                dlog_msg("get challenge failed, card_result = %d", card_result);
                if (card_result == CARDSLOT_SUCCESS && receive_buffer_len >= 2) {
                    dlog_msg("return code %2.2X%2.2X", receive_buffer[receive_buffer_len - 2], receive_buffer[receive_buffer_len - 1]);
                    memcpy(pusStatus, &receive_buffer[receive_buffer_len - 2], 2); // copy status
                }
                /*-----------------------------
                * Get Challenge command failed
                */

                return RESP_PIN_BAD_ICC_RESPONSE;
            }

            for (i = 17; i < modulus_length; i += 8)
            {
                iPC_GenerateRandom(&pin_encipherment_data[i]);
            }

            pin_encipherment_data[0] = 0x7F;
            memcpy(&pin_encipherment_data[1], pin_block, 8);
            memcpy(&pin_encipherment_data[9], icc_unpredictable_num, 8);

            /*
            * Encrypt PIN data
            */

            if (RSA( pin_block,
                     &pin_block_length,
                     pin_encipherment_data,
                     modulus_length, //correct
                     modulus,
                     modulus_length,
                     exponent,
                     exponent_length) != RSA_CORRECT)
            {
                dlog_msg("error performing RSA");
                // send_response((byte *)GET_PIN_REQUEST, 0, NULL, (byte *)"10");
       
                /*--------------------
                * RSA function failed
                */

                return RESP_PIN_INTERNAL_FAILURE;
            }

            command_buffer[0] = 0x00;            /* CLA */
            command_buffer[1] = 0x20;            /* ILS */
            command_buffer[2] = 0x00;            /* P1  */
            command_buffer[3] = 0x88;            /* P2  */
            command_buffer[4] = pin_block_length;/* Lc  */
            memcpy(&command_buffer[5],
                   pin_block,
                   pin_block_length);            /* Data */

            dlog_msg("pin_block_length = 0x%2.2x", pin_block_length);
        }
        else if( PIN_TYPE_PLAIN_TEXT == ucPinType )
        {
            /*
            * Plaintext pin
            */

            command_buffer[0] = 0x00;          /* CLA */
            command_buffer[1] = 0x20;          /* ILS */
            command_buffer[2] = 0x00;          /* P1  */
            command_buffer[3] = 0x80;          /* P2 qualifier - unencrypted pin */
            command_buffer[4] = 0x08;          /* Lc length of pin block         */
            memcpy(&command_buffer[5], pin_block, 8);  /* Data */

            pin_block_length = 8;
        }
        else
        {
            /*
            * Invalid ucPinType parameter 
            * it should never come here
            */

            return RESP_PIN_INVALID_PARAM;
    
        }  /* ucPinType */

        /*
        * Send Verify command to card
        */
        
        card_result = Transmit_APDU(scr_reader,
                                    command_buffer,
                                    pin_block_length + 5,
                                    receive_buffer,
                                    &receive_buffer_len);

        if (card_result == CARDSLOT_SUCCESS)
        {
            /*
            * Check status words to determine response
            */

            sw1 = receive_buffer[receive_buffer_len - 2];
            sw2 = receive_buffer[receive_buffer_len - 1];

            if ((sw1 == 0x90) && (sw2 == 0x00))
            {
                /*
                * Command processed successfully
                */

                // status_word[0] = sw1;
                // status_word[1] = sw2;
                *pusStatus = ((unsigned short)(sw2)
                           | ((unsigned short)(sw1)<<8));

                dlog_msg("pin succeeded, sw1sw2 = 0x9000"); 
                // send_response((byte *)GET_PIN_REQUEST, 2, status_word, (byte *)"00");

                /*-------------
                * PIN accepted
                */

                return RESP_PIN_SUCCESS;
            }
            else
            {
                /*
                * Pin failed
                */

                // status_word[0] = sw1;
                // status_word[1] = sw2;
                *pusStatus = ((unsigned short)(sw2)
                           | ((unsigned short)(sw1)<<8));
                
                dlog_msg("pin failed, sw1sw2 = 0x%2.2x%2.2x", sw1, sw2 ); 
                // send_response((byte *)GET_PIN_REQUEST, 2, status_word, (byte *)"04");

                if (ucPinTryCtr == 0)
                {
                  /*----------------
                   * Last try failed
                   */

                   dlog_msg("ucPinTryCtr is 0");
                   return RESP_PIN_FAILED;
                }
                else if (sw1 == 0x63 && sw2 >= 0xC0 && sw2 <= 0xCF)
                {
                    /*
                    * sw2 contains Cx, where x is the number of retries remaining
                    * Extract number of retries remaining
                    */
                    if (sw2 > 0xC0)
                    {
                       /*-----------------
                       * Repeat PIN entry
                       */

                       dlog_msg("Pin retries remaining: %u", sw2&0x0F);
                       ucPinTryCtr = sw2&0x0F;

                       usResp = usEMVUpdateTLVInCollxn( (ushort)TAG_9F17_PIN_TRY_COUNTER,
                                                 &ucPinTryCtr,
                                                 1 );
                       dlog_msg("usEMVUpdateTLVInCollxn returned %u", usResp);

                       if (ucEntryType == ENTRY_TYPE_ATOMIC)
                       {
                          return RESP_PIN_RETRY;
                       }
                       dlog_msg("Retrying PIN...");
                       usEMVDisplayErrorPrompt(E_INVALID_PIN);
                       continue;
                    }
                    else
                    {
                       /*----------------
                       * Last try failed
                       */

                       dlog_msg("Last PIN try failed");
                       return RESP_PIN_FAILED;
                    } 
                }
                else if (sw1 == 0x69 && sw2 == 0x83)
                {
                    dlog_msg("PIN blocked at Verify");
                    return RESP_PIN_FAILED;
                }
                else if (sw1 == 0x69 && sw2 == 0x84)
                {
                    dlog_msg("Error status at Verify");
                    return RESP_PIN_FAILED;
                }
                else
                {
                    dlog_msg("Invalid response from the ICC");
                    return RESP_PIN_SLOT_ERROR;
                }     
            }   /* SW1, SW2 */
        }
        else if (card_result == CARD_ABSENT)
        {
            /*------------------
            * No card in reader
            */

            dlog_msg("no card in reader"); 
            // send_response((byte *)GET_PIN_REQUEST, 0, NULL, (byte *)"07");

            return RESP_PIN_CARD_REMOVED;
        }
        else
        {
            /*----------------
            * Card slot error
            */

            dlog_msg("card error, card_result=%d",
                         card_result);
            // send_response((byte *)GET_PIN_REQUEST, 0, NULL, (byte *)"02");
            
            return RESP_PIN_SLOT_ERROR;
        
        }  /* card_result = Transmit_APDU */
    } while(true);

    /*
    * it should never come here
    */
    
    // Unreachable:
    //
    // dlog_msg("error: unproper return" ); 
    // return RESP_PIN_INTERNAL_FAILURE;

}   /* verify_pin */


/*-----------------------------------------------------------------------------
  Load PIN Key - used for enciphered PIN entry
-----------------------------------------------------------------------------*/
unsigned char load_pin_key( void )
{
    // we need the following tags present. if not present then return error
    //
    // (9f47 OR 9f2e) AND df0e
    //
    // where:
    // 9f47 = ICC Public Key Exponent
    // 9f2e = ICC PIN Encipherment Public Key Exponent
    // df0e = Public key modulus
    if( usEMVGetTLVFromColxn((ushort)TAG_9F47_ICC_PUBKEY_EXP, exponent, &exponent_length) == EMV_SUCCESS )
    {
        dlog_msg("using tag 9f47");
    }
    else if( usEMVGetTLVFromColxn((ushort)TAG_9F2E_ICC_PIN_EXP, exponent, &exponent_length) == EMV_SUCCESS )
    {
        dlog_msg("using tag 9f2e");
    }
    else
    {
        // couldn't find either, return error
        dlog_msg("error tag 9f47 or 9f2e not present");
        return FALSE;
    }

    if( usEMVGetTLVFromColxn((ushort)0xdf0e, modulus, &modulus_length) != EMV_SUCCESS )
    {
        dlog_msg("error tag df0e not present");
        return FALSE;
    }
    dlog_msg("modulus len=%d", modulus_length);
    return TRUE;
}


