#include "tlv-lite/SafeBuffer.hpp"

#include <cstring>



namespace com_verifone_TLVLite
{

	////////////////////////////////////////////////////////////////////
	//SafeBuffer
	//
	bool SafeBuffer::append(char c)
	{
		if (isFull())
		{
			overflowFlag_ = true;
			return false;
		}

		getBufferChar()[length_++] = c;
		return true;
	}

	bool SafeBuffer::append(void const * data, std::size_t length)
	{
		if (length == 0)
			return true;
			
		if ((length_ + length) > bufferSize_)
		{
			overflowFlag_ = true;
			return false;
		}

		char * dest = getBufferChar() + length_;

		// special case to actually extend the buffer
		if (static_cast<void const *>(dest) == data)
		{
			// Nothing to do - data is already there
		}
		else
		{
			std::memcpy(dest, data, length);
		}

		length_ += length;
		return true;
	}

	bool SafeBuffer::append(SafeBuffer const & srcBuilder)
	{
		return append(srcBuilder.buffer_, srcBuilder.length_);
	}

}
