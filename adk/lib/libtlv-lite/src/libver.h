
#ifndef _LIB_VERSION_H
#define _LIB_VERSION_H
#include <libpml/pml.h>
namespace com_verifone_libver {
    
    static inline void register_library() {
        com_verifone_pml::appver::register_library( "libtlv-lite" , "1.0.1.4" );
    }
}
#endif
    
