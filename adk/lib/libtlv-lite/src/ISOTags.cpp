// NOTE!!!!
// To ensure coherency with the list of tags specified in ISOTags.hpp, this file heavily uses macros
// to properly define the values of the tags in internally visible arrays, then to declare them
// and finally define the symbols. For this the header "ISOTags.hpp" is included *THREE* times.
//
// !!! _WARNING_ !!! DO NOT MODIFY this file unless you REALLY know what you are doing. 
// If unsure preprocess the original file first using compiler, then check 
// how your modifications affect the preprocessed output.
//

namespace com_verifone_TLVLite
{
	namespace ISO
	{
		// Unnamed namespace to avoid exporting those symbols
		namespace
		{
			namespace Internal
			{
				typedef unsigned char byte;

#define EMEAV_TLVLITE_ISO_TAGS_INTERNAL_LIST
#define TLVLITE_DEFINE_TAG(t, ...)				const byte t[] = { __VA_ARGS__ }

#include "tlv-lite/ISO/ISOTags.hpp"

#undef EMEAV_TLVLITE_ISO_TAGS_HPP
#undef TLVLITE_DEFINE_TAG
#undef EMEAV_TLVLITE_ISO_TAGS_INTERNAL_LIST

			}
		}
	}
}

// NOTE: This is done ESPECIALLY this way to make sure the same list of tags is used!
#include "tlv-lite/ISO/ISOTags.hpp"
#undef EMEAV_TLVLITE_ISO_TAGS_HPP


#define TLVLITE_DEFINE_TAG(t, ...)		const ConstData_s t = CONST_DATA_ARRAY_INIT(Internal::t)
#include "tlv-lite/ISO/ISOTags.hpp"
