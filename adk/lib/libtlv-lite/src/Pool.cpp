#include "tlv-lite/Pool.hpp"
#include "tlv-lite/TLVIterator.hpp"

#include <cstring>



namespace com_verifone_TLVLite
{

	////////////////////////////////////////////////////////////////////
	ConstData_t findTagValueInPool(ConstData_t pool, ConstData_t const & tag)
	{
        TLVListWrapper tlvList(pool);

		for (TLVIterator it = tlvList.begin(); it != tlvList.end(); ++it)
		{
			TagLenVal tlv = *it;
			if (tlv.getTag() == tag)
			{
				return tlv.getData();
			}
		}
		return ConstData_t::getInvalid();
	}
}
