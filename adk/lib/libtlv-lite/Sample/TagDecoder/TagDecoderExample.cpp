#include <tlv-lite/decoders/TagDecoder.hpp>

#include <cstdlib>
#include <cstdio>


void printTag(com_verifone_TLVLite::ConstData_t const & tag)
{
	unsigned char const * charBuffer = static_cast<unsigned char const *>(tag.getBuffer());

	std::puts("Tag:");
	
	for (std::size_t idx=0; idx < tag.getSize(); ++idx)
	{
		std::printf(" %.02x", charBuffer[idx]);
	}
	std::puts("\n");
}


int main(int argc, char * argv[])
{
	using com_verifone_TLVLite::TagDecoder;
	using com_verifone_TLVLite::ConstData_t;
	using com_verifone_TLVLite::SafeBufferReader;
	
	// 3 tags here
	unsigned char tagList1[] = { 
		0xdf, 0xc1, 0x01,  	// CICAPP module version
		0x9a,				// Transaction date
		0x9c,				// Transaction type
		0xdf, 0xc1, 0x02,	// CICAPP num of readers
		0x9f, 0x1b			// Terminal floor limit
	};
	

	SafeBufferReader reader(tagList1, sizeof(tagList1));
	while (!reader.isEmpty())
	{
		TagDecoder td(reader);
		if (td.isError())
		{
			std::puts("ERROR!\n");
		}
		else
		{
			printTag(td.getValue());
		}
	}
	
	
	return 0;
}
