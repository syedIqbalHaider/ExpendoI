/*
 * This example shows how to use ConverInteger2BER() function to quickly convert some old code
 * that creates tag from a given byte buffer.
 * NOTE: This is mainly for integrating with other libs than tlv-lite (like e.g. ber-tlv)
*/

#include <tlv-lite/ConstData.h>
#include <tlv-lite/ConstValue.hpp>
#include <tlv-lite/SafeBuffer.hpp>
#include <tlv-lite/TagLenVal.hpp>

#include <cstdlib>
#include <cstdio>





//===================================================================
// Some tag definitions (from CICAPP)
namespace cicapp_tags
{
	using com_verifone_TLVLite::ConstData_t;
	
	typedef unsigned char byte;
	static const byte abyTagDisplayPrompt[] = { 0xDF, 0xC0, 0x10 }; // Display prompt
	const ConstData_t cicappTagDisplayPrompt = CONST_DATA_T_ARRAY_INIT(abyTagDisplayPrompt);

	static const byte abyTagDisplayMessage[] = { 0xDF, 0xC0, 0x11 }; // Display message
	const ConstData_t cicappTagDisplayMessage = CONST_DATA_T_ARRAY_INIT(abyTagDisplayMessage);

	
	// Module info
	static const byte abyQueryTagList[] = { 0xDF, 0xC0, 0x60 };
	const ConstData_t cicappTagQueryTagList = CONST_DATA_T_ARRAY_INIT(abyQueryTagList);

	static const byte abyModuleVersion[] = { 0xDF, 0xC1, 0x01 };
	const ConstData_t cicappTagModuleVersion = CONST_DATA_T_ARRAY_INIT(abyModuleVersion);

	static const byte abyReadersCount[] = { 0xDF, 0xC1, 0x02 };
	const ConstData_t cicappTagReadersCount = CONST_DATA_T_ARRAY_INIT(abyReadersCount);
}

using cicapp_tags::cicappTagDisplayPrompt;
using cicapp_tags::cicappTagDisplayMessage;

using cicapp_tags::cicappTagQueryTagList;
using cicapp_tags::cicappTagModuleVersion;
using cicapp_tags::cicappTagReadersCount;




//===================================================================
//  RUN IT !!!
//

void dumpBuf(void const * buffer, std::size_t length)
{
	unsigned char const * byteBuffer = static_cast<unsigned char const *>(buffer);
	std::puts("Buffer is:");

	for (; length > 0; ++byteBuffer, --length)
	{
		std::printf(" %.02x", *byteBuffer);
	}
	
	std::puts("\n");
}

int main(int argc, char * argv[])
{
	using com_verifone_TLVLite::ConstData_t;
	using com_verifone_TLVLite::SafeBuffer;
	using com_verifone_TLVLite::TagLenVal;

	unsigned char tlvBuffer[1024];

	unsigned char tlvLongBuffer[128];
	for (std::size_t idx = 0; idx < sizeof(tlvLongBuffer); ++idx)
	{
		tlvLongBuffer[idx] = static_cast<unsigned char>(idx);
	}
	
	SafeBuffer buffer(tlvBuffer, sizeof(tlvBuffer));

	// Simple tag and known value
	{
		short value = 0x1234;
		short value_BER = com_verifone_TLVLite::ConvertInteger2BER(value);
		
		buffer.append(
			TagLenVal( cicappTagDisplayPrompt, com_verifone_TLVLite::ConstData_t(&value_BER, sizeof(value_BER)))
		);
	}
		
	{
		int value = 0x11223344;
		int value_BER = com_verifone_TLVLite::ConvertInteger2BER(value);
		
		buffer.append(
			TagLenVal( cicappTagDisplayMessage, com_verifone_TLVLite::ConstData_t(&value_BER, sizeof(value_BER)))
		);
	}

	
	dumpBuf(buffer.getBuffer(), buffer.getLength());
	
	return 0;
}
