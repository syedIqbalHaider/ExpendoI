#include <tlv-lite/ConstData.h>
#include <tlv-lite/TagListIter.hpp>

#include <cstdlib>
#include <cstdio>

// NOTE: VxSTL is NOT fully compatible with C++ standard - does not try access standard types via std namespace.
#ifdef USE_STL
#include <algorithm>
#endif

void printTag(com_verifone_TLVLite::ConstData_t const & tag)
{
	unsigned char const * charBuffer = static_cast<unsigned char const *>(tag.getBuffer());

	std::puts("Tag:");
	
	for (std::size_t idx=0; idx < tag.getSize(); ++idx)
	{
		std::printf(" %.02x", charBuffer[idx]);
	}
	std::puts("\n");
}


int main(int argc, char * argv[])
{
	using com_verifone_TLVLite::TagListWrapper;
	using com_verifone_TLVLite::TagListIterator;
	using com_verifone_TLVLite::ConstData_t;
	
	// 3 tags here
	unsigned char tagList1[] = { 
		0xdf, 0xc1, 0x01,  	// CICAPP module version
		0x9a,				// Transaction date
		0x9c,				// Transaction type
		0xdf, 0xc1, 0x02,	// CICAPP num of readers
		0x9f, 0x1b			// Terminal floor limit
	};
	
	TagListWrapper tagList(tagList1, sizeof(tagList1));
	

	for (TagListIterator it = tagList.begin(); it != tagList.end(); ++it)
	{
		ConstData_t tag = *it;
		printTag(tag);
	}

#ifdef USE_STL
	std::puts("-----\n"
		"Another method:\n"
	);

	std::for_each(tagList.begin(), tagList.end(), printTag);
#endif	
	
	return 0;
}
