/*
	This example shows how to use TLVLite to create TLV triplets in buffer
*/

#include <tlv-lite/SafeBufferReader.hpp>

#include <cassert>
#include <cstdlib>
#include <cstdio>






//===================================================================
//  RUN IT !!!
//

void dumpBuf(void const * buffer, std::size_t length)
{
	unsigned char const * byteBuffer = static_cast<unsigned char const *>(buffer);
	std::puts("Buffer is:");

	for (; length > 0; ++byteBuffer, --length)
	{
		std::printf(" %.02x", *byteBuffer);
	}
	
	std::puts("\n");
}

int main(int argc, char * argv[])
{
	using com_verifone_TLVLite::ConstData_t;
	using com_verifone_TLVLite::SafeBufferReader;

	
	// 3 tags here
	unsigned char tagList1[] = { 
		0xdf, 0xc1, 0x01,  	// CICAPP module version
		0x9a,				// Transaction date
		0x9c,				// Transaction type
		0xdf, 0xc1, 0x02,	// CICAPP num of readers
		0x9f, 0x1b			// Terminal floor limit
	};

	SafeBufferReader reader(tagList1, sizeof(tagList1));

	puts("Buffer is:\n");
	while (!reader.isEmpty())
	{
		unsigned char peekByte = reader.peekByte();
		unsigned char getByte = reader.getByte();
		assert(peekByte == getByte);
		std::printf(" %.02x", getByte);
	}
	

	return 0;
}
