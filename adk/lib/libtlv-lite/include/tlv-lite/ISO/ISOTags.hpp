#ifndef EMEAV_TLVLITE_ISO_TAGS_HPP
#define EMEAV_TLVLITE_ISO_TAGS_HPP 
 
#ifndef __cplusplus 
#  error "This file is for C++ only!"
#endif 
 
/***************************************************************************
** 
 *  
 * Copyright (C) 2006 by VeriFone, Inc. 
 * 
 * All rights reserved.  No part of this software may be reproduced, 
 * transmitted, transcribed, stored in a retrieval system, or translated 
 * into any language or computer language, in any form or by any means, 
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise, 
 * without the prior written permission of VeriFone, Inc.  
 *        
 **************************************************************************/ 
 
/** 
 * @file   ISOTags.hpp
 * 
 * @author  Ireneusz Janiuk (ireneusz_j1@verifone.com)
 *  
 * @brief   Class that declares popular ISO tags as extern symbols
 * 
 * @remarks  This file should be compliant with Verifone EMEA R&D C++ Coding   
 *           Standard 1.0.x  
 */ 
 
/*************************************************************************** 
 * Includes 
 **************************************************************************/ 
#ifndef EMEAV_TLVLITE_ISO_TAGS_INTERNAL_LIST

#include "tlv-lite/ConstData.h"
#include <cstddef>
 
/*************************************************************************** 
 * Module namespace: begin 
 **************************************************************************/ 
 
namespace com_verifone_TLVLite
{ 

/** 
 * @addtogroup TLVLite
 * @{  
 */ 
 
 
/** 
 * Exported variable declarations 
 **/ 
	namespace ISO
	{
#endif	

#ifndef TLVLITE_DEFINE_TAG
#define TLVLITE_DEFINE_TAG(t, ...)	extern const ConstData_s t
#endif

		TLVLITE_DEFINE_TAG(Track1, 0x5f, 0x21);
		TLVLITE_DEFINE_TAG(Track2, 0x5f, 0x22);
		TLVLITE_DEFINE_TAG(Track3, 0x5f, 0x23);


#undef TLVLITE_DEFINE_TAG


#ifndef EMEAV_TLVLITE_ISO_TAGS_INTERNAL_LIST
	}
 
/** 
 * @} 
 */ 
} 
#endif


/***************************************************************************  
 * Module namespace: end   
**************************************************************************/ 
 
#endif /* EMEAV_TLVLITE_ISO_TAGS_HPP */

