#ifndef EMEAV_TLVLITE_TAGLENVAL_HPP
#define EMEAV_TLVLITE_TAGLENVAL_HPP 
 
#ifndef __cplusplus 
#  error "This file is for C++ only!"
#endif 
 
/***************************************************************************
** 
 *  
 * Copyright (C) 2006 by VeriFone, Inc. 
 * 
 * All rights reserved.  No part of this software may be reproduced, 
 * transmitted, transcribed, stored in a retrieval system, or translated 
 * into any language or computer language, in any form or by any means, 
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise, 
 * without the prior written permission of VeriFone, Inc.  
 *        
 **************************************************************************/ 
 
/** 
 * @file   TagLenVal.hpp
 * 
 * @author  Tomasz Saniawa (tomasz_s1@verifone.com)
 *  
 * @brief   Easily add TLV triplets to buffer
 * 
 * @remarks  This file should be compliant with Verifone EMEA R&D C++ Coding   
 *           Standard 1.0.x  
 */ 
 
/*************************************************************************** 
 * Includes 
 **************************************************************************/ 
#include "ConstData.h"
 
#include <cstddef>


/*************************************************************************** 
 * Module namespace: begin 
 **************************************************************************/ 
 
namespace com_verifone_TLVLite
{ 
 
/** 
 * @addtogroup TLVLite
 * @{  
 */ 
 
/** 
 * Exported class declarations 
 **/ 

	// @brief Iterator for a TLV tag list
	class TagLenVal
	{
	// Constants
	public:
		static const std::size_t maxSingleByteLen = 127;
	
	// Methods
	public:
		TagLenVal(ConstData_t const & tag, ConstData_t const & data)
			: tag_(tag), data_(data)
		{}
		
		template <typename T> 
		bool appendTo(T& buffer) const
		{
			std::size_t len = data_.getSize();
			bool result = true;

			// NOTE: Sometimes tag with ZERO len has LEGIT uses (like deleting particular tag in destination collection)
			//if (len > 0)
			{
				result = buffer.append(tag_);
				
				// Add length
				// NOTE: It is limited to size of size_t, which should be more than enough for any reasonable implementation
				std::size_t lenBytes = getLenBytes(len);
				unsigned char byte;
				
				
				if (lenBytes == 1)
				{
					// Store length directly
					byte = len;
					buffer.append(byte);
				}
				else
				{
					// Store Length bytes
					--lenBytes;
					byte = 0x80 | lenBytes;
					buffer.append(byte);
					
					// Append bytes to buffer then - MSB first!
					do
					{
						--lenBytes;
						byte = len >> (8 * lenBytes);
						result = result && buffer.append(byte);
					}
					while (lenBytes > 0);
				}
				
				result = result && buffer.append(data_);
			}
			
			return result;
		}
		
		ConstData_t const & getTag() const { return tag_; }
		ConstData_t const & getData() const { return data_; }

		
		// Size of a whole TLV triplet in message
		std::size_t getLength() const { return tag_.getSize() + getLenBytes(data_.getSize()) + data_.getSize(); }

		// @brief Checks whether the object points to valid data
		bool isValid() const { return tag_.isValid() && data_.isValid(); }

		// @brief Returns a specific invalid object
		static TagLenVal getInvalid() { return TagLenVal(ConstData::getInvalid(), ConstData::getInvalid()); }
		
		
	private:
		// @brief Returns a number of bytes needed to encode given length according to BER
		static std::size_t getLenBytes(std::size_t len)
		{
			// It always takes at least one byte
			std::size_t bytesCnt = 1;

			if (len > maxSingleByteLen)
			{
				// add a number of bytes needed to store this number
				do 
				{
					len >>= 8;
					++bytesCnt;
				}
				while (len != 0);
			}
			
			return bytesCnt;
		}

		
	// Attributes
	private:
		ConstData_t tag_;
		ConstData_t data_;
	};
 
/** 
 * @} 
 */ 
 
} 
/***************************************************************************  
 * Module namespace: end   
**************************************************************************/ 
 
#endif /* EMEAV_TLVLITE_TAGLENVAL_HPP */
