#ifndef EMEAV_TLVLITE_TAGLISTITER_HPP
#define EMEAV_TLVLITE_TAGLISTITER_HPP 
 
#ifndef __cplusplus 
#  error "This file is for C++ only!"
#endif 
 
/***************************************************************************
** 
 *  
 * Copyright (C) 2006 by VeriFone, Inc. 
 * 
 * All rights reserved.  No part of this software may be reproduced, 
 * transmitted, transcribed, stored in a retrieval system, or translated 
 * into any language or computer language, in any form or by any means, 
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise, 
 * without the prior written permission of VeriFone, Inc.  
 *        
 **************************************************************************/ 
 
/** 
 * @file   TagListIter.hpp
 * 
 * @author  Tomasz Saniawa (tomasz_s1@verifone.com)
 *  
 * @brief   Class to quickly iterate over a tag list in some buffer
 * 
 * @remarks  This file should be compliant with Verifone EMEA R&D C++ Coding   
 *           Standard 1.0.x  
 */ 
 
/*************************************************************************** 
 * Includes 
 **************************************************************************/ 
#include "ConstData.h"
#include "SafeBufferReader.hpp"
#include "decoders/TagDecoder.hpp"
 
#include <cstddef>
 
/*************************************************************************** 
 * Module namespace: begin 
 **************************************************************************/ 
 
/* FIXME: It should be #include <iterator> but under VDI libraries
 * it collide with log(). Pls fix in the next time
 */
#ifdef VERIXV
namespace std
{
    struct forward_iterator_tag;
}
#endif

namespace com_verifone_TLVLite
{ 
 
/** 
 * @addtogroup TLVLite
 * @{  
 */ 
 
 
/**  
 * Preprocessor constant definitions 
 **/ 
 
 
/**  
 * Macro definitions 
 **/ 
 
 
/** 
 * Data type definitions 
 **/ 
 
/** 
 * Exported variable declarations 
 **/ 

 
/** 
 * Exported function declarations 
 **/ 
 
/** 
 * Exported class declarations 
 **/ 

	// @brief Iterator for a TLV tag list
	// @see TagListWrapper
	class TagListIterator
	{
	public:
		typedef ConstData_t value_type;
		typedef value_type* pointer;
		typedef value_type& reference;
		
		typedef std::size_t size_type;
		typedef std::ptrdiff_t difference_type;

		// TODO: How to enable using this in STL but NOT forcing anyone to include STL?
		typedef std::forward_iterator_tag iterator_category;

	public:
		TagListIterator(void const * buffer, std::size_t size)
			: reader_(buffer, size), error_(false)
		{}
		
		bool operator ==(TagListIterator const & rhs) const
		{
			return reader_ == rhs.reader_;
		}
		
		bool operator !=(TagListIterator const & rhs) const 
		{
			return !(*this == rhs);
		}

		// prefix
		TagListIterator& operator ++()
		{
			if (isValid() && !isError())
			{
				// NOTE: Decoding advances reader_!
				TagDecoder tag(reader_);
				setError(tag.isError());
			}
			else
			{
				reader_.skip(reader_.getLength());
			}
			
			return *this;
		}

		
		// postfix
		TagListIterator operator ++(int)
		{
			TagListIterator copy = *this;
			++(*this);

			return copy;
		}

		
		value_type operator *()
		{
			// TODO: Cache the result for the next ++ operator!
			SafeBufferReader rdr(reader_);
			TagDecoder td(rdr);
			if (td.isError())
				setError(true);

			return td.getValue();
		}
		
		
		// Helpers
		bool isValid() const { return reader_.isValid(); }

		void const * getTagBuffer() const { return reader_.getCurrentCharPtr(); }
		unsigned char const * getTagByteBuffer() const { return reader_.getCurrentBytePtr(); }
	
		bool isError() const { return error_; }

		
	private:
		void setError(bool newErrorStatus) { error_ = newErrorStatus; }
		
	// Attributes
	private:
		SafeBufferReader reader_;
		bool error_;
	};

	
	// @brief A wrapper to easily enumerate TLV tag list
	class TagListWrapper
	{
	public:
		TagListWrapper(void const * buffer, std::size_t size)
			: buffer_(buffer), size_(size)
		{}

		TagListWrapper(ConstData_t const & tagListData)
			: buffer_(tagListData.getBuffer()), size_(tagListData.getSize())
		{}
		
		
		bool operator ==(TagListWrapper const & rhs) const
		{
			return buffer_ == rhs.buffer_;
		}
		
		bool operator !=(TagListWrapper const & rhs) const 
		{
			return !(*this == rhs);
		}

		TagListIterator begin() const { return TagListIterator(buffer_, size_); }
		TagListIterator end() const { return TagListIterator(static_cast<char const *>(buffer_) + size_, 0); }
		
	// Attributes
	private:
		void const * buffer_;
		std::size_t size_;
	};
 
/** 
 * @} 
 */ 
 
} 
/***************************************************************************  
 * Module namespace: end   
**************************************************************************/ 
 
#endif /* EMEAV_TLVLITE_TAGLISTITER_HPP */
