#ifndef EMEAV_TLVLITE_EMV_TAGS_HPP
#define EMEAV_TLVLITE_EMV_TAGS_HPP 
 
#ifndef __cplusplus 
#  error "This file is for C++ only!"
#endif 
 
/***************************************************************************
** 
 *  
 * Copyright (C) 2006 by VeriFone, Inc. 
 * 
 * All rights reserved.  No part of this software may be reproduced, 
 * transmitted, transcribed, stored in a retrieval system, or translated 
 * into any language or computer language, in any form or by any means, 
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise, 
 * without the prior written permission of VeriFone, Inc.  
 *        
 **************************************************************************/ 
 
/** 
 * @file   EMVTags.hpp
 * 
 * @author  Tomasz Saniawa (tomasz_s1@verifone.com)
 *  
 * @brief   Class that declares popular EMV tags as extern symbols
 * 
 * @remarks  This file should be compliant with Verifone EMEA R&D C++ Coding   
 *           Standard 1.0.x  
 */ 
 
/*************************************************************************** 
 * Includes 
 **************************************************************************/ 
#ifndef EMEAV_TLVLITE_EMV_TAGS_INTERNAL_LIST

#include "tlv-lite/ConstData.h"
#include <cstddef>
 
/*************************************************************************** 
 * Module namespace: begin 
 **************************************************************************/ 
 
namespace com_verifone_TLVLite
{ 

/** 
 * @addtogroup TLVLite
 * @{  
 */ 
 
 
/** 
 * Exported variable declarations 
 **/ 
	namespace EMV
	{
#endif	

#ifndef TLVLITE_DEFINE_TAG
#define TLVLITE_DEFINE_TAG(t, ...)	extern const ConstData_s t
#endif

		// Single-byte tags
		TLVLITE_DEFINE_TAG(applicationLabel, 0x50);
		TLVLITE_DEFINE_TAG(track1EquivData, 0x56);
		TLVLITE_DEFINE_TAG(track2EquivData, 0x57);
		TLVLITE_DEFINE_TAG(PAN, 0x5a);

		TLVLITE_DEFINE_TAG(amountBinary, 0x81);
		TLVLITE_DEFINE_TAG(applicationInterchangeProfile, 0x82);
		TLVLITE_DEFINE_TAG(DFName, 0x84);
		TLVLITE_DEFINE_TAG(applicationPriorityIndicator, 0x87);
		TLVLITE_DEFINE_TAG(authorizationCode, 0x89);
		TLVLITE_DEFINE_TAG(authorizationResponseCode, 0x8a);
		TLVLITE_DEFINE_TAG(CDOL1, 0x8c);
		TLVLITE_DEFINE_TAG(CVMList, 0x8e);
		TLVLITE_DEFINE_TAG(CAPublicKeyIndex, 0x8f);
		TLVLITE_DEFINE_TAG(issuerPublicKeyCertificate, 0x90);
		TLVLITE_DEFINE_TAG(issuerPublicKeyRemainder, 0x92);
		TLVLITE_DEFINE_TAG(applicationFileLocator, 0x94);
		TLVLITE_DEFINE_TAG(TVR, 0x95);
		TLVLITE_DEFINE_TAG(TDOL, 0x97);
		TLVLITE_DEFINE_TAG(transactionDate, 0x9a);
		TLVLITE_DEFINE_TAG(transactionType, 0x9c);



		// 5F ..
		TLVLITE_DEFINE_TAG(expiryDate, 0x5f, 0x24);
		TLVLITE_DEFINE_TAG(effectiveDate, 0x5f, 0x25);
		TLVLITE_DEFINE_TAG(issuerCountryCode, 0x5f, 0x28);
		TLVLITE_DEFINE_TAG(transactionCurrencyCode, 0x5f, 0x2a);
		TLVLITE_DEFINE_TAG(languagePreference, 0x5f, 0x2d);
		TLVLITE_DEFINE_TAG(serviceCode, 0x5f, 0x30);
		TLVLITE_DEFINE_TAG(applicationPANSequenceNumber, 0x5f, 0x34);
		TLVLITE_DEFINE_TAG(transactionCurrencyExponent, 0x5f, 0x36);
		TLVLITE_DEFINE_TAG(accountType, 0x5f, 0x57);
		
		
		// 9F ..
		TLVLITE_DEFINE_TAG(amountAuthorized, 0x9f, 0x02);
		TLVLITE_DEFINE_TAG(amountOther, 0x9f, 0x03);
		TLVLITE_DEFINE_TAG(AID, 0x9f, 0x06);
		TLVLITE_DEFINE_TAG(applicationUsageControl, 0x9f, 0x07);
		TLVLITE_DEFINE_TAG(applicationVersionNumber, 0x9f, 0x09);
		TLVLITE_DEFINE_TAG(IACDefault, 0x9f, 0x0d);
		TLVLITE_DEFINE_TAG(IACDenial, 0x9f, 0x0e);
		TLVLITE_DEFINE_TAG(IACOnline, 0x9f, 0x0f);
		TLVLITE_DEFINE_TAG(issuerApplicationData, 0x9f, 0x10);
		TLVLITE_DEFINE_TAG(merchantCategoryCode, 0x9f, 0x15);
		TLVLITE_DEFINE_TAG(merchantIdentifier, 0x9f, 0x16);
		TLVLITE_DEFINE_TAG(PINTryCounter, 0x9f, 0x17);
		TLVLITE_DEFINE_TAG(terminalCountryCode, 0x9f, 0x1a);
		TLVLITE_DEFINE_TAG(floorLimit, 0x9f, 0x1b);
		TLVLITE_DEFINE_TAG(terminalRiskManagementData, 0x9f, 0x1d);
		TLVLITE_DEFINE_TAG(track1DiscretionaryData, 0x9f, 0x1f);
		TLVLITE_DEFINE_TAG(track2DiscretionaryData, 0x9f, 0x20);
		TLVLITE_DEFINE_TAG(transactionTime, 0x9f, 0x21);
		TLVLITE_DEFINE_TAG(applicationCryptogram, 0x9f, 0x26);
		TLVLITE_DEFINE_TAG(cryptogramInformationData, 0x9f, 0x27);
		TLVLITE_DEFINE_TAG(issuerPublicKeyExponent, 0x9f, 0x32);
		TLVLITE_DEFINE_TAG(terminalCapabilities, 0x9f, 0x33);
		TLVLITE_DEFINE_TAG(CVMResults, 0x9f, 0x34);
		TLVLITE_DEFINE_TAG(terminalType, 0x9f, 0x35);
		TLVLITE_DEFINE_TAG(applicationTransactionCounter, 0x9f, 0x36);
		TLVLITE_DEFINE_TAG(unpredictableNumber, 0x9f, 0x37);
		TLVLITE_DEFINE_TAG(PDOL, 0x9f, 0x38);
		TLVLITE_DEFINE_TAG(POSEntryMode, 0x9f, 0x39);
		TLVLITE_DEFINE_TAG(terminalCapabilitiesAdditional, 0x9f, 0x40);
		TLVLITE_DEFINE_TAG(merchantName, 0x9f, 0x4e);
		TLVLITE_DEFINE_TAG(transactionCategoryCode, 0x9f, 0x53);
		TLVLITE_DEFINE_TAG(applicationProgramID, 0x9f, 0x5a);
		TLVLITE_DEFINE_TAG(visaTTQ, 0x9f, 0x66);
		TLVLITE_DEFINE_TAG(UDOL, 0x9f, 0x69);
		TLVLITE_DEFINE_TAG(ctlsMasterCardTrack2, 0x9f, 0x6b);
		TLVLITE_DEFINE_TAG(CardTranQualifier, 0x9f, 0x6c);
		TLVLITE_DEFINE_TAG(thirdPartyData, 0x9f, 0x6e);
		TLVLITE_DEFINE_TAG(merchantCustomData, 0x9f, 0x7c);
		TLVLITE_DEFINE_TAG(mobileSupportIndicator, 0x9f, 0x7e);
		TLVLITE_DEFINE_TAG(CAPKIndex, 0x9f, 0x22);
		TLVLITE_DEFINE_TAG(IFDSerialNumber, 0x9f, 0x1e);
#undef TLVLITE_DEFINE_TAG


#ifndef EMEAV_TLVLITE_EMV_TAGS_INTERNAL_LIST
	}
 
/** 
 * @} 
 */ 
} 
#endif


/***************************************************************************  
 * Module namespace: end   
**************************************************************************/ 
 
#endif /* EMEAV_TLVLITE_EMV_TAGS_HPP */

