#ifndef EMEAV_TLVLITE_SAFEBUFFER_HPP
#define EMEAV_TLVLITE_SAFEBUFFER_HPP 
 
#ifndef __cplusplus 
#  error "This file is for C++ only!"
#endif 
 
/***************************************************************************
** 
 *  
 * Copyright (C) 2006 by VeriFone, Inc. 
 * 
 * All rights reserved.  No part of this software may be reproduced, 
 * transmitted, transcribed, stored in a retrieval system, or translated 
 * into any language or computer language, in any form or by any means, 
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise, 
 * without the prior written permission of VeriFone, Inc.  
 *        
 **************************************************************************/ 
 
/** 
 * @file   SafeBuffer.hpp
 * 
 * @author  Tomasz Saniawa (tomasz_s1@verifone.com)
 *  
 * @brief   Class to provide a safe wrapper over binary buffer
 * 
 * @remarks  This file should be compliant with Verifone EMEA R&D C++ Coding   
 *           Standard 1.0.x  
 */ 
 
/*************************************************************************** 
 * Includes 
 **************************************************************************/ 
#include "ConstData.h"
#include "fwd/SafeBuffer.hpp"
 
#include <cstddef>
 
/*************************************************************************** 
 * Module namespace: begin 
 **************************************************************************/ 
 
namespace com_verifone_TLVLite
{ 
 
/** 
 * @addtogroup TLVLite
 * @{  
 */ 
 
 
/**  
 * Preprocessor constant definitions 
 **/ 
 
 
/**  
 * Macro definitions 
 **/ 
 
 
/** 
 * Data type definitions 
 **/ 
 
/** 
 * Exported variable declarations 
 **/ 

 
/** 
 * Exported function declarations 
 **/ 
 
/** 
 * Exported class declarations 
 **/ 

	// @brief Safe wrapper over binary buffer
	class SafeBuffer
	{
	public:
		SafeBuffer(void * buffer, std::size_t capacity, std::size_t alreadyUsedLength = 0)
			: buffer_(buffer), bufferSize_(capacity), length_(alreadyUsedLength), overflowFlag_(false)
		{}
	
		
		bool append(char c);
		bool append(unsigned char c) { return append(static_cast<char>(c)); }

		bool append(void const * data, std::size_t length);
		bool append(ConstData_t data) { return append(data.getBuffer(), data.getSize()); }
		bool append(ConstData_s data) { return append(data.buffer, data.size); }

		bool append(SafeBuffer const & srcBuilder);

		void * getBuffer() const { return buffer_; }
		char * getCurrentCharPtr() const { return (static_cast<char *>(buffer_) + getLength()); }
		unsigned char * getCurrentBytePtr() const { return (static_cast<unsigned char *>(buffer_) + getLength()); }

		ConstData_t getDataDesc() const { return ConstData_t(buffer_, length_); }
		

		std::size_t getLength() const { return length_; }
		std::size_t getCapacity() const { return bufferSize_; }
		std::size_t getAvailableLen() const { return bufferSize_ - length_; }

		bool isFull() const { return length_ == bufferSize_; }
		bool isOverflow() const { return overflowFlag_; }

		template <typename T> 
		bool append(T const & rhs) { return rhs.appendTo(*this); }
		
		void clear() {length_= 0; overflowFlag_ = false;}

	private:
		// Methods
		char * getBufferChar() const { return static_cast<char *>(buffer_); }

		// Attributes
		void * buffer_;
		std::size_t bufferSize_;
		std::size_t length_;
		bool overflowFlag_;
	};

 
/** 
 * @} 
 */ 
 
} 
/***************************************************************************  
 * Module namespace: end   
**************************************************************************/ 
 
#endif /* EMEAV_TLVLITE_SAFEBUFFER_HPP */
