#ifndef EMEAV_TLVLITE_VALUECONVERTER_HPP
#define EMEAV_TLVLITE_VALUECONVERTER_HPP 
 
#ifndef __cplusplus 
#  error "This file is for C++ only!"
#endif 
 
/***************************************************************************
** 
 *  
 * Copyright (C) 2006 by VeriFone, Inc. 
 * 
 * All rights reserved.  No part of this software may be reproduced, 
 * transmitted, transcribed, stored in a retrieval system, or translated 
 * into any language or computer language, in any form or by any means, 
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise, 
 * without the prior written permission of VeriFone, Inc.  
 *        
 **************************************************************************/ 
 
/** 
 * @file   ValueConverter.hpp
 * 
 * @author  Tomasz Saniawa (tomasz_s1@verifone.com)
 *  
 * @brief   Class to quickly convert value extracted from TLV to some numerical value
 * 
 * @remarks  This file should be compliant with Verifone EMEA R&D C++ Coding   
 *           Standard 1.0.x  
 */ 
 
/*************************************************************************** 
 * Includes 
 **************************************************************************/ 
#include "ConstData.h"
 
#include <cstddef>


/*************************************************************************** 
 * Module namespace: begin 
 **************************************************************************/ 
namespace com_verifone_TLVLite
{ 
 
/** 
 * @addtogroup TLVLite
 * @{  
 */ 
 
 
/** 
 * Exported class declarations 
 **/ 
	namespace Internal
	{
		template <typename T, std::size_t T_SIZE = sizeof(T)>
		class BytesCollector
		{
		public:
			static void collectBytes(T & value, ConstData_t tagValue)
			{
				value = 0;
				for (std::size_t idx=0; idx < tagValue.getSize(); ++idx)
				{
					value <<= 8;
					value |= tagValue.getByteBuffer()[idx];
				}
			}
		};

		template <typename T>
		class BytesCollector<T, 1>
		{
		public:
			static void collectBytes(T & value, ConstData_t tagValue)
			{
				if (tagValue.getSize() > 0)
					value = tagValue.getByteBuffer()[0];
			}
		};
	}

	template <typename T> 	
	bool deserialize(T& dest, ConstData_t src)
	{
		if (src.getSize() == 0 || src.getSize() > sizeof(dest))
			return false;
		
		Internal::BytesCollector<T>::collectBytes(dest, src);
		return true;
	}

	template <typename T>
	void deserialize(T& dest, ConstData_t src, T defaultVal)
	{
		if (!deserialize(dest, src)) dest = defaultVal;
	}

	template <typename T>
	T deserialize(ConstData_t src, T defaultVal)
	{
		T dest;
		deserialize(dest, src, defaultVal);
		return dest;
	}

	template <typename T>
	class ValueConverter
	{
	// Methods
	public:
		ValueConverter(ConstData_t tagValue)
			: value_(0), isError_(false)
		{
			isError_ = !deserialize(value_, tagValue);
		}
		
		bool isError() const { return isError_; }
		T getValue() const { return value_; }
		T getValue(T defaultVal) 
		{ 
			if (isError_) 
				return defaultVal;
			else
				return value_;
		}

	// Attributes
	private:
		T value_;
		bool isError_;
	};
 
/** 
 * @} 
 */ 
 
} 
/***************************************************************************  
 * Module namespace: end   
**************************************************************************/ 
 
#endif /* EMEAV_TLVLITE_VALUECONVERTER_HPP */
