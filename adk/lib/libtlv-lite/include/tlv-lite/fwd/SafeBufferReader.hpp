#ifndef EMEAV_TLVLITE_FWD_SAFEBUFFERREADER_HPP
#define EMEAV_TLVLITE_FWD_SAFEBUFFERREADER_HPP 
 
#ifndef __cplusplus 
#  error "This file is for C++ only!"
#endif 
 
/***************************************************************************
** 
 *  
 * Copyright (C) 2006 by VeriFone, Inc. 
 * 
 * All rights reserved.  No part of this software may be reproduced, 
 * transmitted, transcribed, stored in a retrieval system, or translated 
 * into any language or computer language, in any form or by any means, 
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise, 
 * without the prior written permission of VeriFone, Inc.  
 *        
 **************************************************************************/ 
 
/** 
 * @file   fwd/SafeBufferReader.hpp
 * 
 * @author  Tomasz Saniawa (tomasz_s1@verifone.com)
 *  
 * @brief   Forward declaration for SafeBufferReader
 * 
 * @remarks  This file should be compliant with Verifone EMEA R&D C++ Coding   
 *           Standard 1.0.x  
 */ 
 
 
/*************************************************************************** 
 * Module namespace: begin 
 **************************************************************************/ 
namespace com_verifone_TLVLite
{ 
/** 
 * @addtogroup TLVLite
 * @{  
 */ 
 
/** 
 * Exported class declarations 
 **/ 
	class SafeBufferReader;

/** 
 * @} 
 */ 
} 
/***************************************************************************  
 * Module namespace: end   
**************************************************************************/ 
 
#endif /* EMEAV_TLVLITE_FWD_SAFEBUFFERREADER_HPP */
