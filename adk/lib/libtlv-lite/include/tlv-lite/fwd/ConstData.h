#ifndef EMEAV_TLVLITE_FWD_CONSTDATA_H
#define EMEAV_TLVLITE_FWD_CONSTDATA_H

/***************************************************************************
** 
 *  
 * Copyright (C) 2006 by VeriFone, Inc. 
 * 
 * All rights reserved.  No part of this software may be reproduced, 
 * transmitted, transcribed, stored in a retrieval system, or translated 
 * into any language or computer language, in any form or by any means, 
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise, 
 * without the prior written permission of VeriFone, Inc.  
 *        
 **************************************************************************/ 
 
/** 
 * @file   fwd/ConstData.h
 * 
 * @author  Tomasz Saniawa (tomasz_s1@verifone.com)
 *  
 * @brief   Forward declarations for ConstData.h
 * 
 * @note Declarations are both C and C++ compatible
 *
 * @remarks  This file should be compliant with Verifone EMEA R&D C++ Coding   
 *           Standard 1.0.x  
 */ 
 
 
/*************************************************************************** 
 * Module namespace: begin 
 **************************************************************************/ 
#ifdef __cplusplus
namespace com_verifone_TLVLite
{ 
#endif
 
/** 
 * @addtogroup TLVLite
 * @{  
 */ 
 
/** 
 * Exported class declarations 
 **/ 
//@note To be compatible with C it HAS to be a POD
struct ConstData_s;
typedef struct ConstData_s ConstData_s;

 
#ifdef __cplusplus
/* C++ version with full class */
class ConstData;
typedef ConstData ConstData_t;
#endif

/** 
 * @} 
 */ 
 
#ifdef __cplusplus
} /*  namespace end */
#endif

/***************************************************************************  
 * Module namespace: end   
**************************************************************************/ 
 
#endif /* EMEAV_TLVLITE_FWD_CONSTDATA_H */ 
