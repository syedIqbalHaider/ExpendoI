#ifndef EMEAV_TLVLITE_TLVITERATOR_HPP
#define EMEAV_TLVLITE_TLVITERATOR_HPP 
 
#ifndef __cplusplus 
#  error "This file is for C++ only!"
#endif 
 
/***************************************************************************
** 
 *  
 * Copyright (C) 2006 by VeriFone, Inc. 
 * 
 * All rights reserved.  No part of this software may be reproduced, 
 * transmitted, transcribed, stored in a retrieval system, or translated 
 * into any language or computer language, in any form or by any means, 
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise, 
 * without the prior written permission of VeriFone, Inc.  
 *        
 **************************************************************************/ 
 
/** 
 * @file   TLVIterator.hpp
 * 
 * @author  Tomasz Saniawa (tomasz_s1@verifone.com)
 *  
 * @brief   Class to quickly iterate all TLV triplets in some buffer
 * 
 * @remarks  This file should be compliant with Verifone EMEA R&D C++ Coding   
 *           Standard 1.0.x  
 */ 
 
/*************************************************************************** 
 * Includes 
 **************************************************************************/ 
#include "ConstData.h"
#include "TagLenVal.hpp"
#include "SafeBufferReader.hpp"
#include "decoders/TLVDecoder.hpp"
//#include <logsys.h>
 
#include <cstddef>
 
#ifndef NO_STL
#include <iterator>
#endif

/*************************************************************************** 
 * Module namespace: begin 
 **************************************************************************/ 
namespace com_verifone_TLVLite
{ 
 
/** 
 * @addtogroup TLVLite
 * @{  
 */ 
 
 
/** 
 * Exported class declarations 
 **/ 

 #if 0
	// TODO: migrate from TagLenVal to TagLenValReader

	// @brief Wrapper for existing data in the buffer. Interface is similar to TagLenVal and it is also convertible to TagLenVal
	class TagLenValReader
	{
	public:
		TagLenValReader(void const * buffer, std::size_t size)
			buffer_(0), dataLen(0), tagBytes_(0), lenBytes(0)
		{
			parse(buffer, size);
		}
		
		bool isValid() const { return buffer_ != NULL && dataLen_ != 0; }
		
		
	// Attributes
	private:
		void const * buffer_;
		std::size_t dataLen_;
		unsigned char tagBytes_;
		unsigned char lenBytes_;
	}
 #endif
 
 
	// @brief Iterator for a TLV tag list
	// @see TLVListWrapper
	template <typename TagDecoder_t = TagDecoder, typename LenDecoder_t = LenDecoder>
	class TLVIteratorFlex
	{
	public:
		typedef TagLenVal value_type;
		typedef value_type* pointer;
		typedef value_type& reference;
		
		typedef std::size_t size_type;
		typedef std::ptrdiff_t difference_type;
		

		// TODO: How to enable using this in STL but NOT forcing anyone to include STL?
#ifndef NO_STL
		typedef std::forward_iterator_tag iterator_category;
#endif
		
		typedef TLVIteratorFlex<TagDecoder_t, LenDecoder_t> TLVIterator_t;
		
	public:
		TLVIteratorFlex(ConstData_t bufferDesc = ConstData::getInvalid())
			: reader_(bufferDesc), currentTLVLen_(0), error_(false)
		{}

		TLVIteratorFlex(void const * buffer, std::size_t size)
			: reader_(buffer, size), currentTLVLen_(0), error_(false)
		{}
		
		bool operator ==(TLVIterator_t const & rhs) const
		{
			if (isError())
				return reader_ == rhs.reader_;
			else
				return reader_ == rhs.reader_ 
					&& error_ == rhs.error_;
		}
		
		bool operator !=(TLVIterator_t const & rhs) const 
		{
			return !(*this == rhs);
		}

		// prefix
		TLVIterator_t& operator ++()
		{
			if (isValid() && !isError())
			{
				reader_.skip(getCurrentTLVLen());
				currentTLVLen_ = 0;
			}
			else
			{
				reader_.skip(reader_.getLength());
				//dlog_alert("Error, reader ptr set to %p", reader_.getCurrentCharPtr());
				currentTLVLen_ = 0;
			}
			
			return *this;
		}

		
		// postfix
		TLVIterator_t operator ++(int unused)
		{
			TLVIterator_t copy = *this;
			++(*this);

			return copy;
		}


		value_type operator *()
		{
			// TODO: Cache the result for the next ++ operator!
			SafeBufferReader rdr(reader_);
			TLVDecoderFlex<TagDecoder_t, LenDecoder_t> tlv(rdr);
			if (tlv.isError())
				setError(true);
			else
			{
				// Cache the advance for the ++ operator
				currentTLVLen_ = rdr.getCurrentCharPtr() - reader_.getCurrentCharPtr();
			}
			
			return tlv.getValue();
		}

		
		// Helpers
		bool isValid()
		{
			return reader_.isValid();
		}


		void const * getTLVBuffer() const { return reader_.getCurrentCharPtr(); }
		unsigned char const * getTLVByteBuffer() const { return reader_.getCurrentBytePtr(); }
		
		bool isError() const { return error_; }
		
		std::size_t getCurrentTLVLen()
		{
			// Compute only once for given position
			if (currentTLVLen_  == 0)
			{
				// NOTE: calling operator * automatically sets the cached len but we don't need the result
				this->operator *();
			}

			return currentTLVLen_;
		}

		
	private:
		void setError(bool newErrorStatus) { error_ = newErrorStatus; }
		
	// Attributes
	private:
		SafeBufferReader reader_;
		std::size_t currentTLVLen_;
		bool error_;
	};

	typedef TLVIteratorFlex<> TLVIterator;
	

	
	// @brief A wrapper to easily enumerate TLV list
	template <typename TagDecoder_t = TagDecoder, typename LenDecoder_t = LenDecoder>
	class TLVListWrapperFlex
	{
	public:
		typedef TLVListWrapperFlex<TagDecoder_t, LenDecoder_t> TLVListWrapper_t;
		typedef TLVIteratorFlex<TagDecoder_t, LenDecoder_t> TLVIterator_t;
		
	
	public:
		TLVListWrapperFlex(void const * buffer, std::size_t size)
			: buffer_(buffer), size_(size)
		{}

		TLVListWrapperFlex(ConstData_t const & rhs)
			: buffer_(rhs.getBuffer()), size_(rhs.getSize())
		{}
		
		

		bool operator ==(TLVListWrapper_t const & rhs) const
		{
			return buffer_ == rhs.buffer_;
		}
		
		bool operator !=(TLVListWrapper_t const & rhs) const 
		{
			return !(*this == rhs);
		}

		TLVIterator_t begin() const { return TLVIterator_t(buffer_, size_); }
		TLVIterator_t end() const { return TLVIterator_t(static_cast<char const *>(buffer_) + size_, 0); }
		//TLVIterator_t end() const { dlog_msg("End ptr %p", static_cast<char const *>(buffer_) + size_); return TLVIterator_t(static_cast<char const *>(buffer_) + size_, 0); }
		
	// Attributes
	private:
		void const * buffer_;
		std::size_t size_;
	};

	typedef TLVListWrapperFlex<> TLVListWrapper;
 
/** 
 * @} 
 */ 
 
} 
/***************************************************************************  
 * Module namespace: end   
**************************************************************************/ 
 
#endif /* EMEAV_TLVLITE_TLVITERATOR_HPP */
