#ifndef EMEAV_TLVLITE_SAFEBUFFERREADER_HPP
#define EMEAV_TLVLITE_SAFEBUFFERREADER_HPP 
 
#ifndef __cplusplus 
#  error "This file is for C++ only!"
#endif 
 
/***************************************************************************
** 
 *  
 * Copyright (C) 2006 by VeriFone, Inc. 
 * 
 * All rights reserved.  No part of this software may be reproduced, 
 * transmitted, transcribed, stored in a retrieval system, or translated 
 * into any language or computer language, in any form or by any means, 
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise, 
 * without the prior written permission of VeriFone, Inc.  
 *        
 **************************************************************************/ 
 
/** 
 * @file   SafeBufferReader.hpp
 * 
 * @author  Tomasz Saniawa (tomasz_s1@verifone.com)
 *  
 * @brief   Class to provide a safe wrapper to read from binary buffer
 * 
 * @remarks  This file should be compliant with Verifone EMEA R&D C++ Coding   
 *           Standard 1.0.x  
 */ 
 
/*************************************************************************** 
 * Includes 
 **************************************************************************/ 
#include "ConstData.h"
 
#include <cstddef>
 
/*************************************************************************** 
 * Module namespace: begin 
 **************************************************************************/ 
 
namespace com_verifone_TLVLite
{ 
 
/** 
 * @addtogroup TLVLite
 * @{  
 */ 
 
 
/** 
 * Exported class declarations 
 **/ 

	// @brief Safe wrapper over binary buffer
	class SafeBufferReader
	{
	public:
		SafeBufferReader(void const * buffer, std::size_t length)
			: buffer_(buffer), bufferSize_(length), overrunFlag_(false)
		{}

		SafeBufferReader(ConstData_t const & rhs)
			: buffer_(rhs.getBuffer()), bufferSize_(rhs.getSize()), overrunFlag_(false)
		{}
		
		
		unsigned char peekByte()
		{
			if (isEmpty())
			{
				setOverrun();
				return '\0';
			}
			else
				return *getCurrentBytePtr();
		}
		
		unsigned char getByte()
		{
			unsigned char value = '\0';
			if (isEmpty())
			{
				setOverrun();
			}
			else
			{
				unsigned char const * byteBuff = getCurrentBytePtr();
				value = *byteBuff;
				buffer_ = ++byteBuff;
				--bufferSize_;
			}

			return value;
		};

		char peekChar() { return static_cast<char>(peekByte()); }
		char getChar() { return static_cast<char>(getByte()); }

		bool skip(std::size_t count)
		{
			if (count > bufferSize_)
			{
				setOverrun();
				buffer_ = getCurrentCharPtr() + bufferSize_;
				bufferSize_ = 0;
			}
			else
			{
				buffer_ = getCurrentCharPtr() + count;
				bufferSize_ -= count;
			}
				
			return !isOverrun();
		}
		
		
		void const * getCurrentPtr() const { return buffer_; }
		std::size_t getLength() const { return bufferSize_; }
		ConstData_t getDataDesc() const { return ConstData_t(buffer_, bufferSize_); }

		bool isEmpty() const { return bufferSize_ == 0; }
		bool isOverrun() const { return overrunFlag_; }
		
		bool isValid() const { return buffer_ != NULL; }

		char const * getCurrentCharPtr() const { return static_cast<char const *>(buffer_); }
		unsigned char const * getCurrentBytePtr() const { return static_cast<unsigned char const *>(buffer_); }

		
		// comparison operators
		bool operator ==(SafeBufferReader const & rhs) const
		{
			return buffer_ == rhs.buffer_ && bufferSize_ == rhs.bufferSize_;
		}
		
		bool operator !=(SafeBufferReader const & rhs) const 
		{
			return !(*this == rhs);
		}

		

	private:
		// Methods
		void setOverrun() { overrunFlag_ = true; }

		// Attributes
		void const * buffer_;
		std::size_t bufferSize_;
		bool overrunFlag_;
	};

 
/** 
 * @} 
 */ 
 
} 
/***************************************************************************  
 * Module namespace: end   
**************************************************************************/ 
 
#endif /* EMEAV_TLVLITE_SAFEBUFFERREADER_HPP */
