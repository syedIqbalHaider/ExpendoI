#ifndef EMEAV_TLVLITE_TAGDECODER_HPP
#define EMEAV_TLVLITE_TAGDECODER_HPP 
 
#ifndef __cplusplus 
#  error "This file is for C++ only!"
#endif 
 
/***************************************************************************
** 
 *  
 * Copyright (C) 2006 by VeriFone, Inc. 
 * 
 * All rights reserved.  No part of this software may be reproduced, 
 * transmitted, transcribed, stored in a retrieval system, or translated 
 * into any language or computer language, in any form or by any means, 
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise, 
 * without the prior written permission of VeriFone, Inc.  
 *        
 **************************************************************************/ 
 
/** 
 * @file   TagDecoder.hpp
 * 
 * @author  Tomasz Saniawa (tomasz_s1@verifone.com)
 *  
 * @brief   Wrapper class to decode bytes forming tag sequence
 * 
 * @remarks  This file should be compliant with Verifone EMEA R&D C++ Coding   
 *           Standard 1.0.x  
 */ 
 
/*************************************************************************** 
 * Includes 
 **************************************************************************/ 
#include <tlv-lite/ConstData.h>
#include <tlv-lite/SafeBufferReader.hpp>

#include <cstddef>
 
/*************************************************************************** 
 * Module namespace: begin 
 **************************************************************************/ 
 
namespace com_verifone_TLVLite
{ 
 
/** 
 * @addtogroup TLVLite
 * @{  
 */ 
 
 
/** 
 * Exported class declarations 
 **/ 
 
	// @brief Base class with rudimentary interface
	class TagDecoderBase
	{
	public:
		TagDecoderBase()
			: tag_(ConstData_t::getInvalid()), error_(false)
		{}
	
		bool isError() const { return error_; }
		
		ConstData_t const & getValue() const { return tag_; }
		void const * getBuffer() const { return tag_.getBuffer(); }
		unsigned char const * getByteBuffer() const { return tag_.getByteBuffer(); }

	protected:
		void setError(bool newErrorStatus) { error_ = newErrorStatus; }
		void setTag(ConstData_t const & rhs) { tag_ = rhs; }
		
	// Attributes
	protected:
		ConstData_t tag_;
		bool error_;
	};

 
 
	// @brief Decoder for bytes encoding tag sequence
	class TagDecoder : public TagDecoderBase
	{
	public:
		TagDecoder(SafeBufferReader & reader)
		{
			parse(reader);
		}

		
	// Methods
	protected:
		// @brief Checks whether a tag is extended one (has continuation on subsequent bytes)
		static bool isExtendedTag(unsigned char firstByte) { return (firstByte & 0x1f) == 0x1f; }
		
		// @brief Checks subsequent byte
		static bool isLastTagByte(unsigned char subsequentByte) { return (subsequentByte & 0x80) == 0; }

		
		bool parse(SafeBufferReader & reader)
		{
			// store beginning of buffer
			tag_ = reader.getDataDesc();

			unsigned char currentByte = reader.getByte();

			if (!reader.isOverrun() && isExtendedTag(currentByte))
			{
				// Only successful search for last byte clears the error
				setError(true);
				
				while (!reader.isEmpty())
				{
					currentByte = reader.getByte();

					// clear error condition only if tag properly terminated
					if (isLastTagByte(currentByte))
					{
						setError(false);
						break;
					}
				}
			}

			if (reader.isOverrun())
				setError(true);
			
			if (isError())
			{
				// Zero it out
				tag_ = ConstData_t::getInvalid();
			}
			else
			{
				// Calculate proper size
				tag_.size -= reader.getLength();
			}
			
			return !isError();
		}
		
	};
	
 
/** 
 * @} 
 */ 
 
} 
/***************************************************************************  
 * Module namespace: end   
**************************************************************************/ 
 
#endif /* EMEAV_TLVLITE_TAGDECODER_HPP */
