#ifndef EMEAV_TLVLITE_TLVDECODER_HPP
#define EMEAV_TLVLITE_TLVDECODER_HPP 
 
#ifndef __cplusplus 
#  error "This file is for C++ only!"
#endif 
 
/***************************************************************************
** 
 *  
 * Copyright (C) 2006 by VeriFone, Inc. 
 * 
 * All rights reserved.  No part of this software may be reproduced, 
 * transmitted, transcribed, stored in a retrieval system, or translated 
 * into any language or computer language, in any form or by any means, 
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise, 
 * without the prior written permission of VeriFone, Inc.  
 *        
 **************************************************************************/ 
 
/** 
 * @file   TLVDecoder.hpp
 * 
 * @author  Tomasz Saniawa (tomasz_s1@verifone.com)
 *  
 * @brief   Wrapper class to decode given buffer as TLV object
 * 
 * @remarks  This file should be compliant with Verifone EMEA R&D C++ Coding   
 *           Standard 1.0.x  
 */ 
 
/*************************************************************************** 
 * Includes 
 **************************************************************************/ 
#include <tlv-lite/ConstData.h>
#include <tlv-lite/SafeBufferReader.hpp>
#include <tlv-lite/TagLenVal.hpp>
#include "TagDecoder.hpp"
#include "LenDecoder.hpp"
 
#include <cstddef>
 
 
/*************************************************************************** 
 * Module namespace: begin 
 **************************************************************************/ 
 
namespace com_verifone_TLVLite
{ 
 
/** 
 * @addtogroup TLVLite
 * @{  
 */ 
 
 
/** 
 * Exported class declarations 
 **/ 

	// @brief Iterator for a TLV tag list
	// @see TagListWrapper
	template <typename TagDecoder_t = TagDecoder, typename LenDecoder_t = LenDecoder>
	class TLVDecoderFlex
	{
	public:
		TLVDecoderFlex(SafeBufferReader & reader)
			: tag_(ConstData_t::getInvalid()), data_(ConstData_t::getInvalid()), 
			error_(false)
		{
			parse(reader);
		}

		bool isError() const { return error_; }

		TagLenVal getValue() const
		{
			return TagLenVal(tag_, data_);
		}
		
	// Methods
	private:
		void setError(bool newErrorStatus) { error_ = newErrorStatus; }

		
		bool parse(SafeBufferReader & reader)
		{
		
			{
				TagDecoder_t td(reader);
				if (td.isError())
					setError(true);
				else
					tag_ = td.getValue();
			}
			
			if (!isError())
			{
				LenDecoder_t ld(reader);
				if (ld.isError())
					setError(true);
				else
				{
					std::size_t len = ld.getValue();
					data_ = ConstData_t(reader.getCurrentPtr(), len);

					if (!reader.skip(len))
					{
						setError(true);
						data_ = ConstData_t(reader.getDataDesc());
					}
				}
			}

			return !isError();
		}
		
		
	// Attributes
	private:
		ConstData_t tag_;
		ConstData_t data_;

		bool error_;
	};
	
	typedef TLVDecoderFlex<> TLVDecoder;
 
/** 
 * @} 
 */ 
 
} 
/***************************************************************************  
 * Module namespace: end   
**************************************************************************/ 
 
#endif /* EMEAV_TLVLITE_TLVDECODER_HPP */
