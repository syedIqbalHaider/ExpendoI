#ifndef EMEAV_TLVLITE_LENDECODER_HPP
#define EMEAV_TLVLITE_LENDECODER_HPP 
 
#ifndef __cplusplus 
#  error "This file is for C++ only!"
#endif 
 
/***************************************************************************
** 
 *  
 * Copyright (C) 2006 by VeriFone, Inc. 
 * 
 * All rights reserved.  No part of this software may be reproduced, 
 * transmitted, transcribed, stored in a retrieval system, or translated 
 * into any language or computer language, in any form or by any means, 
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise, 
 * without the prior written permission of VeriFone, Inc.  
 *        
 **************************************************************************/ 
 
/** 
 * @file   LenDecoder.hpp
 * 
 * @author  Tomasz Saniawa (tomasz_s1@verifone.com)
 *  
 * @brief   Wrapper class to decode bytes forming length sequence in TLV triplet
 * 
 * @remarks  This file should be compliant with Verifone EMEA R&D C++ Coding   
 *           Standard 1.0.x  
 */ 
 
/*************************************************************************** 
 * Includes 
 **************************************************************************/ 
#include <tlv-lite/SafeBufferReader.hpp>

#include <cassert>
#include <cstddef>
#include <cstdio>
/*************************************************************************** 
 * Module namespace: begin 
 **************************************************************************/ 
 
namespace com_verifone_TLVLite
{ 
	namespace
	{
		inline bool assertRem(char const *) { return true; }
	}
 
 
/** 
 * @addtogroup TLVLite
 * @{  
 */ 
 
 
/** 
 * Exported class declarations 
 **/ 

	// @brief Decoder for bytes encoding tag sequence
	class LenDecoder
	{
	public:
		LenDecoder(SafeBufferReader & reader)
			: len_(0), error_(false)
		{
			parse(reader);
		}

		bool isError() const { return error_; }
		
		std::size_t getValue() { return len_; }
		
	// Methods
	private:
		void setError(bool newErrorStatus) { error_ = newErrorStatus; }

		// @brief Checks if the lenght is multi-byte encoded
		static bool isMultibyteEncoded(unsigned char firstByte) { return (firstByte & 0x80) != 0; }

		// @brief Get length bytes count
		static std::size_t decodeBytesCount(unsigned char firstByte) { return firstByte & 0x7f; }

		// @brief Sanity check on bytes count
		static bool isBytesCountSane(std::size_t bytesCount) 
		{
			// bytes count = 0 is BAD. Count > 4 is over any practical limits
			bool isSane = bytesCount > 0 && bytesCount <= 4; 
			//assert(assertRem("TLV len bytes count check") && isSane);
			return isSane;
		}


		
		bool parse(SafeBufferReader & reader)
		{
			unsigned char currentByte = reader.getByte();

			if (!reader.isOverrun())
			{
				if (!isMultibyteEncoded(currentByte))
				{
					len_ = currentByte;
				}
				else
				{
					std::size_t bytesCnt = decodeBytesCount(currentByte);

					// sanity check
					if (isBytesCountSane(bytesCnt))
					{
						for (; bytesCnt > 0; --bytesCnt)
						{
							currentByte = reader.getByte();
							len_ <<= 8;
							len_ |= currentByte;
						}
						
					}
					else
						setError(true);
				}
			}
			// If there was no overrun - we are ok
			if(reader.isOverrun())
				setError(reader.isOverrun());

			return !isError();
		}
		
	// Attributes
	private:
		std::size_t len_;
		bool error_;
	};
	
 
/** 
 * @} 
 */ 
 
} 
/***************************************************************************  
 * Module namespace: end   
**************************************************************************/ 
 
#endif /* EMEAV_TLVLITE_LENDECODER_HPP */
