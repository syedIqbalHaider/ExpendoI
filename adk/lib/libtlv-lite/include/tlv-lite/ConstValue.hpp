#ifndef EMEAV_TLVLITE_CONSTVALUE_HPP
#define EMEAV_TLVLITE_CONSTVALUE_HPP
 
#ifndef __cplusplus 
#  error "This file is for C++ only!"
#endif 
 
/***************************************************************************
** 
 *  
 * Copyright (C) 2006 by VeriFone, Inc. 
 * 
 * All rights reserved.  No part of this software may be reproduced, 
 * transmitted, transcribed, stored in a retrieval system, or translated 
 * into any language or computer language, in any form or by any means, 
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise, 
 * without the prior written permission of VeriFone, Inc.  
 *        
 **************************************************************************/ 
 
/** 
 * @file   ConstValue.hpp
 * 
 * @author  Tomasz Saniawa (tomasz_s1@verifone.com)
 *  
 * @brief   Class to quickly convert integral types values to the big-endian format required by BER-TLV
 * 
 * @remarks  This file should be compliant with Verifone EMEA R&D C++ Coding   
 *           Standard 1.0.x  
 */ 
 
/*************************************************************************** 
 * Includes 
 **************************************************************************/ 
#include "ConstData.h"
 
#include <cstddef>
#include <limits>

#include <boost/static_assert.hpp>
#include <boost/spirit/home/support/detail/endian/endian.hpp>


/*************************************************************************** 
 * Module namespace: begin 
 **************************************************************************/ 
namespace com_verifone_TLVLite
{ 
 
/** 
 * @addtogroup TLVLite
 * @{  
 */ 
 
 
/** 
 * Exported class declarations 
 **/ 


	template <typename T>
	class ConstValue
	{
	// Sanity checks
	private:
		BOOST_STATIC_ASSERT_MSG(std::numeric_limits<T>::is_integer, "Don't use ConstValue with non-integer types!");
	
	// Methods
	public:
		ConstValue(T val)
		: bigendianT_(val)
		{}
		
		
		ConstData_t getDataDesc() const
		{
			return ConstData_t(&bigendianT_, sizeof(bigendianT_));
		}

		ConstData getShortDataDesc() const
		{
			unsigned char const *p = reinterpret_cast<unsigned char const *>(&bigendianT_);
			int size = sizeof(bigendianT_);
			for(;size > 1 && *p == 0; --size, ++p);
			return ConstData(p, size);
		}
		
		operator ConstData_t() const { return getDataDesc(); }
		
	// Attributes
	private:
		// useful class from boost to provide a necessary conversion
		boost::spirit::endian::endian<boost::spirit::endian::endianness::big, T, CHAR_BIT * sizeof(T)> bigendianT_;
	};


	template <typename T>
	ConstValue<T> MakeBERValue(T val) 
	{
		return ConstValue<T>(val); 
	}


	template <typename T>
	T ConvertInteger2BER(T val) 
	{
		BOOST_STATIC_ASSERT_MSG(std::numeric_limits<T>::is_integer, "Don't use ConvertInteger2BER with non-integer types!");
		
		T result;
		boost::spirit::detail::store_big_endian<T, sizeof(result)>(&result, val);
		
		return result;
	}

	template <typename T>
	ConstData MakeShortBERValue(T val) 
	{
		return ConstValue<T>(val).getShortDataDesc(); 
	}
	
/** 
 * @} 
 */ 
 
} 
/***************************************************************************  
 * Module namespace: end   
**************************************************************************/ 
 
#endif /* EMEAV_TLVLITE_CONSTVALUE_HPP */
