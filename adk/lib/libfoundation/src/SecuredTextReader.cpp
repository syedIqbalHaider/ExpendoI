#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 *
 * Copyright (C) 2008 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file       SecuredTextReader.cpp
 *
 * @author     Maciej 'karp' Karpiuk
 *
 * @brief      Implementation of SecuredTextReader class
 */

#include <stdio.h>
#include <stdlib.h>
#include <libpml/pml_abstracted_api.h>
#ifdef POSIX
#include <unistd.h>
#include <fcntl.h>
#endif
#include "libfoundation/TextReader.h"
#include "libfoundation/SecuredTextReader.h"

//using namespace std;

namespace com_verifone_text
{

/**
 * @addtogroup TextReader
 * @{
 */

// constructor
SecuredTextReader::SecuredTextReader(const char *filename) : TextReader(NULL) {
   openFile(filename, NULL);
}

SecuredTextReader::SecuredTextReader(const char *filename, const char *header_keyword) : TextReader(NULL) {
   openFile(filename, header_keyword);
}

// opens the file in secure mode
void     SecuredTextReader::openFile(const char *filename, const char *header_keyword)
{
   closeFile();
   TextReader::openFile(filename);
   if(checkAuthentication(filename)) closeFile();
   else if(header_keyword){
      // check the keyword
      if(!( searchForKeyword("/*")==0           &&
            searchForKeyword(header_keyword)==0 &&
            searchForKeyword("*/")==0) ) closeFile();
   }
   first_line = -1;
}

// check if file was authenticated / 0 for success / 1 for failure
int8_t   SecuredTextReader::checkAuthentication(const char *filename)
{
   if(!filename) return 1;
   return !(com_verifone_pml::isFileSigned(std::string(filename)));
#if 0
#ifdef POSIX
   int fh=-1;
   // check uppercase
   strcpy(buf, filename);
   strcat(buf, ".P7S");
   fh=open(buf, O_RDONLY);
   if(fh>=0) {
      close(fh);
      return 0;
   }
   // check lowercase
   strcpy(buf, filename);
   strcat(buf, ".p7s");
   fh=open(buf, O_RDONLY);
   if(fh>=0) {
      close(fh);
      return 0;
   }
   return 1;
#elif VERIXV
   int attrib = dir_get_attributes(filename);
   if(attrib & ATTR_NOT_AUTH) return 1;
   else return 0;
#endif
#endif
}


// Closes the file - covers the top method
void      SecuredTextReader::closeFile() {
   first_line = -1;
   TextReader::closeFile();
}


// Move the cursor to the starting position just after the header
int32_t   SecuredTextReader::moveToStartingPosition()
{
   if(fileOpened()) {
      if(first_line==-1) {
         // find the end of the header
         scrollToLine(0);
         if(searchForKeyword("*/")==0) {
            // found, find the line
            readLine();
            first_line = getLineNum();
         }
      }
      scrollToLine(first_line);
      return first_line;
   }
   return -1;
}

/**
 * @}
 */

}
