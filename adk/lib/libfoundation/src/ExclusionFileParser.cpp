#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 *
 * Copyright (C) 2008 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file       ExclusionFileParser.cpp
 *
 * @author     Maciej 'karp' Karpiuk
 *
 * @brief      Implementation of ExclusionFileParser class
 */

#include <stdio.h>
#include <stdlib.h>
#include "libfoundation/ExclusionFileParser.h"


using namespace com_verifone_text;

namespace com_verifone_util
{

/**
 * @addtogroup Util
 * @{
 */

// constructor
ExclusionFileParser::ExclusionFileParser(const char *filename) : textreader(filename, "exclusion") {}



// operator == - checks if given filename is in the Exclusion list
bool   ExclusionFileParser::operator==(const char *fname)
{
   char lbuf[MAX_FILENAME+1];
#ifdef VERIXV
   char realName[MAX_FILENAME+1];
   realName[sizeof(realName)-1] = 0; // This fixes random file load issue!
#else
   const char *realName = fname;
#endif
   char inclExcl;
   bool retVal = false;
   // scroll to the begining
  textreader.moveToStartingPosition();
#ifdef VERIXV
  //Convert to upper name
  strncpy(realName,fname,sizeof(realName) - 1);
  textreader.toUpper(realName);
#endif

  // for each found line, perform the test
  while( textreader.readLine(lbuf, sizeof(lbuf))>0 )
  {
         if(!textreader.checkContents(lbuf)) continue;
#ifdef VERIXV
         textreader.toUpper(lbuf);
#endif
         if(lbuf[0]==EXCLUDE_CHAR || lbuf[0]==INCLUDE_CHAR)
         {
           //With inclusion exclusion char
           inclExcl = lbuf[0];
           compare = &lbuf[1];
         }
         else
         {
           //Like a normal filename for include
           inclExcl = INCLUDE_CHAR;
           compare = lbuf;
         }
         if(compare==realName)
         {
           //Filename is found
           if(inclExcl==INCLUDE_CHAR)
           {
             //If included in exclusion list
             retVal = true;
           }
           else if(inclExcl==EXCLUDE_CHAR)
           {
             //Removed from exclusion list
             retVal = false;
             //Return immediately!
             break;
           }
         }
   }
   return retVal;
}

/**
 * @}
 */

}
