#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 *
 * Copyright (C) 2008 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file       PathComparator.cpp
 *
 * @author     Maciej 'karp' Karpiuk
 *
 * @brief      Implementation of PathComparator class
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "libfoundation/PathComparator.h"

namespace com_verifone_util
{

/**
 * @addtogroup Util
 * @{
 */

// constructor
PathComparator::PathComparator(void) { cleanup(); }

// constructor
PathComparator::PathComparator(const char *mask) {
   setNewMask(mask);
}

// operator = - set new mask in PathComparator
PathComparator const &     PathComparator::operator=(const char *new_mask) {
   setNewMask(new_mask);
   return *this;
}


// operator == - checks if given filename matches the mask
bool                       PathComparator::operator==(const char *fname)
{
   bool retVal = false;
   char *tfname = NULL;
   if(!fname || strlen(fname)==0) return 0;
   // scan the filename for drive letter
   tfname = strchr((char*)fname, ':');
   if(!tfname) tfname = (char*)fname;
   else tfname++; // pass the ':'
   // do the comparation
   if(no_blocks==1 && !elem[0].wild) {
      // no wildcards at all
      if(strcmp(mask, tfname)==0) return 1;
   } else {
      // having wildcards
      char  *worker = (char*)tfname;
      char  tmp;
      int   block=0, enable_scroll=0, len;
      while(worker && (block < no_blocks)) {
         // analyze current block
         if( elem[block].wild ) {
            // wildcard one
            block++;
            retVal = true;
            enable_scroll=1;
         } else {
            // regular block
            if(enable_scroll) {
               tmp = elem[block].ptr[elem[block].len];
               elem[block].ptr[elem[block].len] = 0;
               len = strlen(elem[block].ptr);
               worker = strstr(worker, elem[block].ptr);
               elem[block].ptr[elem[block].len] = tmp;
               if(!worker) return 0;
               worker+=len;
            } else {
               if( memcmp(worker, elem[block].ptr, elem[block].len) == 0 ) {
                  retVal=true;
                  worker+=elem[block].len;
               } else return 0;
            }
            block++;
            enable_scroll=0;
         }
      }
      if(retVal && !enable_scroll) {
         // check file string lengths
         if((worker-tfname)<strlen(tfname)) retVal=false;
      }
   }
   return retVal;
}

// cleanup internal mask
void     PathComparator::cleanup(void) {
   mask[0]=0;
   no_blocks=0;
   memset(elem, 0, _PH_MAX_BLOCKS * sizeof(struct _path_elem));
}

// analyze mask
void     PathComparator::setNewMask(const char *new_mask) {
   int i, len, prev=0;
   cleanup();
   if(!new_mask || strlen(new_mask)>sizeof(mask)) return;
   // strcpy(mask, new_mask);
   len = strlen(new_mask);
   for (int i = 0; i <= len; ++i) mask[i] = tolower(new_mask[i]);
   // scan the mask for wildcards
   for(i=0;i<=len;i++) {
      if(mask[i]=='*') {
         elem[no_blocks].ptr = &mask[prev];
         elem[no_blocks].len = i-prev;
         elem[no_blocks++].wild = 1;
         prev = i+1;
      } else if((mask[i]==len || mask[i]==0) && (i-prev)) {
         elem[no_blocks].ptr = &mask[prev];
         elem[no_blocks++].len = i-prev;
         prev = i;
      }
      // check the limit
      if(no_blocks>=_PH_MAX_BLOCKS) break;
   }
}

// Destructor
PathComparator::~PathComparator() {}

/**
 * @}
 */

}
