#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "libfoundation/profiler.h"
#include "libfoundation/hashtable.h"
#include "libfoundation/hashtable_itr.h"
#ifdef VERIXV
#include <svc.h>
#include <liblog/logsys.h>
#elif POSIX
#include <sys/time.h>
#endif

unsigned long  glob_cnt=0, step_cnt = 0, step_max = 0, tree_lev = 0, max_tree = 0;
struct hashtable *h=NULL;

#ifdef VERIXV
#define  klog(...)  { dlog_msg(__VA_ARGS__); }
#define  KNL   ""
#else
#define  klog(...)  { printf(__VA_ARGS__); }
#define  KNL   "\n"
#endif


static unsigned int hashfromkey(void *ky)
{
   unsigned int hash = 0;
   char *szit = (char*)ky;
   int c;
   while(*szit) {
      c = *szit++;
      hash = c + (hash << 6) + (hash << 16) - hash;
   }
   return hash;
}

static int equalkeys(void *k1, void *k2)
{
   char *szit1 = (char*)k1;
   char *szit2 = (char*)k2;
   return (0 == strcmp(szit1, szit2));
}



int            profiler_new(unsigned long log_step)
{
   step_max = log_step;
   h = create_hashtable(16, hashfromkey, equalkeys);
   return 0;
}

int            profiler_new_with_max_depth(  unsigned long log_step,
                                             unsigned long max_depth )
{
   max_tree = max_depth;
   profiler_new(log_step);
   return 0;
}

void           profiler_destroy(void)
{
   hashtable_destroy(h, 1);
}

pritem         profiler_start(const char *fname)
{
   char        *szit = NULL;
   pritem      item = NULL;
   if(!fname)  return NULL;
   // -- find the item in the hash, if not present create new one
   if(max_tree && tree_lev>=max_tree) return NULL;
   item = (pritem)hashtable_search(h, (void*)fname);
   if(!item) {
      szit = (char*)malloc(strlen(fname)+1); if(!szit) return NULL;
      item = (pritem)calloc(1, sizeof(struct pritem_t)); if(!item) { free(szit); return NULL; }
      strcpy(szit, fname);
      hashtable_insert(h, (void *)szit, (void *)item);
   }
   // --- start time cnt
   tree_lev ++;
   item->snatch = 1;
#ifdef POSIX
   gettimeofday(&item->start, NULL);
#elif VERIXV
   item->start = read_ticks();
#endif
   return item;
}


unsigned long  profiler_stop_ptr(pritem fptr)
{
   unsigned long us=0;
#ifdef POSIX
   struct timeval    end_time;
#elif VERIXV
   unsigned long     end_time;
#endif
   if(fptr) {
      if(max_tree && tree_lev>=max_tree) return 0;
   }
   tree_lev --;
   if(!fptr || !fptr->snatch) return 0;
   fptr->snatch = 0;
#ifdef POSIX
   gettimeofday(&end_time, NULL);
   // cnt diff here
   while( fptr->start.tv_sec<end_time.tv_sec ) {
      fptr->start.tv_sec++;
      us += 1000000 - fptr->start.tv_usec;
      fptr->start.tv_usec = 0;
   }
   us += end_time.tv_usec - fptr->start.tv_usec;
   fptr->start.tv_usec = end_time.tv_usec;
   fptr->eaten_us += us;
#elif VERIXV
   end_time = read_ticks();
   fptr->eaten_us += (end_time - fptr->start)*1000;
   fptr->start = end_time;
#endif
   fptr->call_cnt ++;
   step_cnt++;
   glob_cnt++;
   return us;
}

unsigned long  profiler_stop(pritem fptr, const char *fname)
{
   if(fname) {
      fptr = (pritem)hashtable_search(h, (void*)fname);
   }
   return profiler_stop_ptr(fptr);
}


int            profiler_is_display_ready(void) {
   return ( step_cnt>=step_max );
}

// log
void           profiler_dump(void)
{
   struct hashtable_itr *itr=NULL;
   char fname[40];
   const char *unit, *pad;
   int len=0, div;
   double temp_result;
   step_cnt=0;
   itr = hashtable_iterator(h);
   if(hashtable_count(h) > 0) {
      klog(KNL"---------- PROFILER OUTPUT after %ld steps -----------"KNL, glob_cnt);
      do {
         pritem   val  = (pritem)hashtable_iterator_value(itr);
         if(val->eaten_us) {
            char     *key = (char*)hashtable_iterator_key(itr);
            len = strlen(key);
            if(len>36) len=36;
            memset(fname, ' ', sizeof(fname)); fname[sizeof(fname)-1]=0;
            memcpy(fname, key, len); fname[len]=' ';
            temp_result = (double)val->eaten_us/(double)val->call_cnt;
            if(temp_result >= 1000000)   { unit="s"; div = 1000000; }
            else if(temp_result >= 1000) { unit="ms";div = 1000;    }
            else { unit = "us"; div=1; }
            temp_result /= (double)div;
            if(temp_result < 10) pad = "  ";
            else if(temp_result < 100) pad = " ";
            else pad = "";
            klog("%s avg. %s%3.1f [%s]  after %ld executions"KNL, fname, pad,
                  temp_result, unit, val->call_cnt);
         }
      } while(hashtable_iterator_advance(itr));
      klog("----------------- PROFILER OUTPUT END ---------------"KNL KNL);
   }
}
