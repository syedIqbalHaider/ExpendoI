
#ifndef _LIB_VERSION_H
#define _LIB_VERSION_H
#include <libpml/pml.h>
namespace com_verifone_libver {
    
    static inline void register_library() {
        com_verifone_pml::appver::register_library( "libfoundation" , "1.1.0.6" );
    }
}
#endif
    
