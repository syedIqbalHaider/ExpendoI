/*
 * HWInfoVOS.cpp
 *
 *  Created on: 2009-02-16
 *      Author: Lucjan_B1
 */

#include <unistd.h>

#include <cstring>

#include <libfoundation/HWInfo.h>
#include <libfoundation/HWInfoVOS.h>
//#include <svc.h>
#include <liblog/logsys.h>
extern "C" {
  #include <platforminfo_api.h>
}
namespace com_verifone_util
{
/**
 * @addtogroup Util
 * @{
 */


/* ----------------------------------------------------------- */
/** Hw Info V/OS default constructor
 *
 */

HWInfoVOS::HWInfoVOS()
{
	//Detect terminal type
	detectTerminal();

}

/* ----------------------------------------------------------- */
/** Get terminal description data
 *
 */
void HWInfoVOS::detectTerminal()
{
    char local_buf[100]; // Should be enough...
    unsigned long rlen = 0;
    local_buf[sizeof(local_buf)-1] = 0;

    // Get OS version
    memset(eprom,0,sizeof(eprom));
    if (::platforminfo_get(PI_VERSION_RELEASE, local_buf, sizeof(local_buf)-1, &rlen) == PI_OK)
    {
        std::strncpy(eprom, local_buf, sizeof(eprom)-1);
    }

    //Terminal ID information
    memset(ptid,0,sizeof(ptid));
    if (::platforminfo_get(PI_UNIT_ID, local_buf, sizeof(local_buf)-1, &rlen) == PI_OK)
    {
        std::strncpy(ptid, local_buf, sizeof(ptid)-1);
    }

    //Get terminal serial number
    memset(sNo,0,sizeof(sNo));
    if (::platforminfo_get(PI_SERIAL_NUM, local_buf, sizeof(local_buf)-1, &rlen) == PI_OK)
    {
        std::strncpy(sNo, local_buf, sizeof(sNo)-1);
    }

    //Get country
    memset(country,0,sizeof(country));
    if (::platforminfo_get(PI_MIB_COUNTRY, local_buf, sizeof(local_buf)-1, &rlen) == PI_OK)
    {
        std::strncpy(country, local_buf, sizeof(country)-1);
    }

    //Get model No
    memset(modelNo,0,sizeof(modelNo));
    if (::platforminfo_get(PI_CIB_MODEL_ID_STR, local_buf, sizeof(local_buf)-1, &rlen) == PI_OK)
    {
        std::strncpy(modelNo, local_buf, sizeof(modelNo)-1);
    }

    //Translate terminal info into eNum
    terminalType = translateTerminalType(modelNo);

    // RAM and Flash sizes
    ::PI_flash_info_st flashinfo;
    if (!::platforminfo_get( PI_FLASH_INFO, &flashinfo, sizeof(flashinfo), &rlen ))
    {
        flashSize = flashinfo.total;
        flashFree = flashinfo.free / 1024;
    }
    ramSize = long(::sysconf( _SC_PHYS_PAGES )) * long(::sysconf( _SC_PAGE_SIZE )/1024);

    ::PI_display_info_st displayinfo;
    if (!::platforminfo_get( PI_DISPLAY_INFO, &flashinfo, sizeof(displayinfo), &rlen ))
    {
        screenWidth = displayinfo.pixels_per_row;
        screenHeight = displayinfo.pixels_per_col;
    }
}

/* ----------------------------------------------------------- */
/** Get terminal info identifier based on
 *  string given
 */
termTYPE HWInfoVOS::translateTerminalType(const char *modelNo)
{
    termTYPE type = TERM_UNKNOWN;

    if (strstr(modelNo, "UX300"))
        type = Ux300;
    else if (strstr(modelNo, "VX820"))
        type = Vx820VOS;

    dlog_msg("HWInfoVOS terminal detected %d (%s)",type,modelNo);
    return type;
}


/* ----------------------------------------------------------- */
int HWInfoVOS::getModemType()
{
	// ctmp todo
	return 0;
}

/* ----------------------------------------------------------- */
bool HWInfoVOS::isEthernet()
{
	// ctmp todo
	return true;
}
/* ----------------------------------------------------------- */
bool HWInfoVOS::isGPRS()
{
	// ctmp todo
	return false;
}

/* ----------------------------------------------------------- */
bool HWInfoVOS::isWiFi()
{
	// ctmp todo
	return false;
}

/* ----------------------------------------------------------- */
bool HWInfoVOS::isBluetooth()
{
	// ctmp todo
	return false;
}

/* ----------------------------------------------------------- */
/**
* @brief       return executing HW lifetim
* * @return      long [seconds]
* */
long  HWInfoVOS::getLifetime(void)
{
	return 0;
}

/* ----------------------------------------------------------- */
}

