/*
 * HWInfoVerix.cpp
 *
 *  Created on: 2009-02-16
 *      Author: Lucjan_B1
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libfoundation/HWInfo.h>
#include <libfoundation/HWInfoVerix.h>
#include <svc.h>
#include <liblog/logsys.h>
namespace com_verifone_util
{
/**
 * @addtogroup Util
 * @{
 */


/* ----------------------------------------------------------- */
/** Hw Info verix default constructor
 *
 */

HWInfoVerix::HWInfoVerix()
{
	//Detect terminal type
	detectTerminal();

}

/* ----------------------------------------------------------- */
/** Get terminal description data
 *
 */
void HWInfoVerix::detectTerminal()
{
	//EEPROM information
	memset(eprom,0,sizeof(eprom));
	SVC_INFO_EPROM(eprom);


	//Terminal ID information
	memset(ptid,0,sizeof(ptid));
	SVC_INFO_PTID(ptid);

	//Get terminal serial number
	memset(sNo,0,sizeof(sNo));
	SVC_INFO_SERLNO(sNo);
	if (sNo[0] == (char)0xFF)
	    strcpy(sNo, "PROTOTYPE");

	//Get country
	memset(country,0,sizeof(country));
	SVC_INFO_COUNTRY(country);
	if (country[0] == (char)0xFF)
	     strcpy(country, "PROTOTYPE");


	//Get model No
	memset(modelNo,0,sizeof(modelNo));
	SVC_INFO_MODELNO(modelNo);
	dlog_msg("LB Model no %s",modelNo);
	if (modelNo[0] == (char)0xFF)
		strcpy(modelNo, "PROTOTYPE");


	//Translate terminal info into eNum
	terminalType = translateTerminalType(modelNo);

	if ((terminalType == Vx610) || (terminalType == O56xx))
	{
		deviceType1 = SVC_INFO_MODULE_ID(3);
		deviceType2 = SVC_INFO_MODULE_ID(2);
		dlog_msg("Term 610 or omni deviceType [1]=%d [2]=%d",deviceType1,deviceType2);
	}
	else if ( terminalType == Vx670)
	{
		deviceType1 = SVC_INFO_MODULE_ID(2);
		deviceType2 = SVC_INFO_MODULE_ID(2);
		dlog_msg("Term 670 or omni deviceType [1]=%d [2]=%d",deviceType1,deviceType2);
	}
	else
	{
		deviceType1 = SVC_INFO_MODULE_ID(3);
		dlog_msg("Other or omni deviceType [1]=%d [2]=%d",deviceType1,deviceType2);
	}

	usbDeviceBits = get_usb_device_bits();

	ramSize = SVC_RAM_SIZE();

	flashSize = SVC_FLASH_SIZE();

	char buf[20];
	SVC_INFO_DISPLAY(buf);
	buf[3] = 0;
	screenWidth = atoi(buf);

	SVC_INFO_DISPLAY(buf);
	buf[6]=0;
	screenHeight = atoi(buf+3);

	//Flash free
	fs_size fs;
	dir_get_sizes("F:",&fs);
	flashFree = fs.Avail/1024;
}

/* ----------------------------------------------------------- */
/** Get terminal info identifier based on
 *  string given
 */
termTYPE HWInfoVerix::translateTerminalType(const char *modelNo)
{
	   termTYPE type = TERM_UNKNOWN;

	   if (strstr(modelNo,"O3600G"))
           type = O36xxG;
       else if (strstr(modelNo,"O3600P"))
           type = O36xxP;
       else if (strstr(modelNo,"O33"))
           type = O33xx;
       else if (strstr(modelNo,"O51"))
           type = O51xx;
       else if (strstr(modelNo,"O56"))
           type = O56xx;
       else if ((strstr(modelNo,"Vx61")) || (strstr(modelNo,"VX61")))
           type = Vx610;
       else if ((strstr(modelNo,"Vx51")) || (strstr(modelNo,"VX51")))
           type = Vx510;
       else if ((strstr(modelNo,"Vx57")) || (strstr(modelNo,"VX57")))
           type = Vx570;
       else if ((strstr(modelNo,"Vx67")) || (strstr(modelNo,"VX67")))
           type = Vx670;
       else if ((strstr(modelNo,"Vx81")) || (strstr(modelNo,"VX81")))
           type = Vx810;
       else if ((strstr(modelNo,"Vx70")) || (strstr(modelNo,"VX70")))
           type = Vx700;
       else if ((strstr(modelNo,"Vx52")) || (strstr(modelNo,"VX52")))
           type = Vx520;
       else if ((strstr(modelNo,"Vx68")) || (strstr(modelNo,"VX68")))
           type = Vx680;
       else if ((strstr(modelNo,"Vx82")) || (strstr(modelNo,"VX82")))
           type = Vx820;
       else if ((strstr(modelNo,"Vx60")) || (strstr(modelNo,"VX60")))
           type = Vx600;
       else if (strstr(modelNo,"PROTO"))
           type = PROTOTYPE;
       else if ((strstr((char*)(modelNo), "O37")) || (strstr((char*)(modelNo), "TDK_2400")))
           type = O37xx;
       else
    	   type = TERM_UNKNOWN;
	   dlog_msg("HWInfoVerix terminal detected %d (%s)",type,modelNo);
       return type;
}


/* ----------------------------------------------------------- */
int HWInfoVerix::getModemType()
{
	int dType = 0;

	if ((terminalType == Vx610) || (terminalType == O56xx) || (terminalType == Vx670))
			dType = deviceType2;
	else
		dType = deviceType1;

	return dType;
}

/* ----------------------------------------------------------- */
bool HWInfoVerix::isEthernet()
{

	bool retVal =  false;
	int modemType = getModemType();
	if (modemType == MID_CO561_ONLY         || modemType == MID_CARLOS_CO561  ||
		modemType == MID_EISEN_USB_ETHERNET || modemType == MID_BANSHEE_CO210 ||
		modemType == MID_CO210_ONLY
		|| (weAreEVOTerminal() && (modemType == MID_HARLEY_MODEM)) 	//Trident, Vx820
		)
			retVal =  true;
	else if(usbDeviceBits & UDB_ETHER)
			retVal =  true;
	return retVal;
}
/* ----------------------------------------------------------- */
bool HWInfoVerix::isGPRS()
{
	bool retVal =  false;
	int modemType = getModemType();

	if (modemType == MID_MC56_ONLY   || modemType == MID_MC55_ONLY   ||
		modemType == MID_CARLOS_MC56 || modemType == MID_CARLOS_MC55 ||
		modemType == MID_EISEN_MC56  || modemType == MID_EISEN_MC55
		)
			retVal =  true;
	return retVal;
}

/* ----------------------------------------------------------- */
bool HWInfoVerix::isWiFi()
{
	bool retVal =  false;
	int modemType = getModemType();

	dlog_msg("modemType = %d", modemType);
	
	if (modemType == MID_CO710_ONLY || modemType == MID_CARLOS_CO710 || modemType == MID_EISEN_USB_WIFI
		|| (weAreEVOTerminal() && (modemType == MID_USB_MODEM))	//Trident, Vx680 - don't ask me why MID_USB_MODEM!
		)
			retVal =  true;
	else if(usbDeviceBits & UDB_WIFI )
			retVal =  true;

	return retVal;
}

/* ----------------------------------------------------------- */
bool HWInfoVerix::isBluetooth()
{
		bool retVal =  false;
		int modemType = getModemType();

		dlog_msg("modemType = %d", modemType);

       if((modemType == MID_BTEZ1) || (modemType == MID_BTEZ2) || (modemType == MID_BTEZ3) || (modemType == MID_BTEZ4) ||
		(modemType == MID_BTAA1) || (modemType == MID_BTAA2) || (modemType == MID_BTAA3) || (modemType == MID_BTAA4)
		|| (weAreEVOTerminal() && (modemType == MID_USB_MODEM))	//Trident, Vx680 - don't ask me why MID_USB_MODEM!
		)
       {
			retVal =  true;
       }

		return retVal;
}

/* ----------------------------------------------------------- */
/**
* @brief       return executing HW lifetim
* * @return      long [seconds]
* */
long  HWInfoVerix::getLifetime(void)
{
	return SVC_INFO_LIFETIME();
}

/* ----------------------------------------------------------- */
}

