#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 *
 * Copyright (C) 2008 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file       TextWriter.cpp
 *
 * @author     Rafal Pietruch
 *
 * @brief      Implementation of TextWriter class
 */

#include <stdio.h>
#include <stdlib.h>
#if defined(POSIX) || defined(__GNUC__)
#include <unistd.h>
#include <fcntl.h>
#endif
#include <ctype.h>
#include "libfoundation/TextWriter.h"

//using namespace std;

#define NEW_LINE "\n"

namespace com_verifone_text
{

/**
 * @addtogroup TextWriter
 * @{
 */

// constructor
TextWriter::TextWriter(const char *filename) : file_ptr(-1)
{
   openFile(filename);
}

// open a new file
int8_t TextWriter::openFile(const char *filename) {
   // create an empty buffer
   file_ptr = -1;
   if(filename)
      file_ptr = open(filename, O_RDWR|O_TRUNC|O_CREAT);
   if(file_ptr<0) return 1;
   return 0;
}

// Write line of text from given buffer into file
int32_t     TextWriter::writeLine(void) {
   return write(file_ptr, NEW_LINE, strlen(NEW_LINE));
}
// Write line of text from given buffer into file
int32_t     TextWriter::writeLine(const char *source_buf) {
   return writeLine(source_buf, strlen(source_buf));
}
// Write line of text from given buffer into file
int32_t     TextWriter::writeLine(const char *source_buf, size_t buf_size)
{
   int written_bytes=0;
   if (source_buf)
   {
      // printf("write buf: %s\n", source_buf);
      written_bytes = write(file_ptr, source_buf, buf_size);
      writeLine();
      return written_bytes;
   }
   return -1;
}

// Closes the file
void        TextWriter::closeFile() {
   if(file_ptr>=0) {
     // int ret =
     close(file_ptr);
     // printf("close file returned : %d\n", ret);
   }
   file_ptr = -1;
}

// Destructor
TextWriter::~TextWriter(void) {
   closeFile();
}

/**
 * @}
 */

}
