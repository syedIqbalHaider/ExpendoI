#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 *
 * Copyright (C) 2008 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file       SecurityFileParser.cpp
 *
 * @author     Maciej 'karp' Karpiuk
 *
 * @brief      Implementation of SecurityFileParser class
 */

#include <stdio.h>
#include <stdlib.h>
#include "libfoundation/SecurityFileParser.h"
#include "libfoundation/DirScanner.h"

using namespace com_verifone_text;

namespace com_verifone_util
{

/**
 * @addtogroup Util
 * @{
 */

// constructor
SecurityFileParser::SecurityFileParser(const char *filename) : textreader(filename, "secure") {}



// perform full check of files in the security list
int8_t   SecurityFileParser::performCheck(void) { return performCheck(NULL); }
int8_t   SecurityFileParser::performCheck(char *output_buf)
{
   int8_t      retVal = 1;
   char        lbuf[MAX_FILENAME+1], fname[MAX_FILENAME+1];
   DirScanner  scanner(NULL);
   SecuredTextReader reader(NULL);
   if(*this) {
      // scroll to the begining
      textreader.moveToStartingPosition();
      // for each found line, perform the test
      while( textreader.readLine(lbuf, sizeof(lbuf))>0 ) {
         if(!textreader.checkContents(lbuf)) continue;
         // okay, let's give it to the DirScanner
#ifdef VERIXV
         textreader.toUpper(lbuf);
#endif
         scanner = lbuf;
         while( scanner.getNext(fname) ) {
            // dlog_msg("Security check on file %s", fname);
            if(textreader.checkContents(fname) && !strstr(fname, ".P7S")) {
               reader.openFile(fname, NULL);
               if(reader.fileOpened()==0) {
#ifdef VERIXV
                  dlog_msg("error> file \"%s\" is not authenticated! (needed by security file)", fname);
#endif
                  if(output_buf) strcpy(output_buf, fname);
                  return 0;
               }
            }
         }
      }
      retVal = 1;
   } else retVal = 0;
   return retVal;
}

/**
 * @}
 */

}
