#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 *
 * Copyright (C) 2008 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file       TextReader.cpp
 *
 * @author     Maciej 'karp' Karpiuk
 *
 * @brief      Implementation of TextReader class
 */

#include <stdio.h>
#include <stdlib.h>
#if defined(POSIX) || defined(__GNUC__)
#include <unistd.h>
#include <fcntl.h>
#endif
#include <ctype.h>
#include "libfoundation/TextReader.h"

//using namespace std;

namespace com_verifone_text
{

/**
 * @addtogroup TextReader
 * @{
 */

// constructor
TextReader::TextReader(const char *filename) {
   openFile(filename);
}

// open a new file
int8_t TextReader::openFile(const char *filename) {
   // create an empty buffer
   file_ptr = -1;
   line_num = 0;
   if(filename)
      file_ptr = open(filename, O_RDONLY);
   if(file_ptr<0) return 1;
   return 0;
}

// Read line of text into given buffer
int32_t     TextReader::readLine(void) {
   return readLine(buf, MAX_LINE_LEN-1);
}
// Read line of text into given buffer
int32_t     TextReader::readLine(char *target_buf) {
   return readLine(target_buf, MAX_LINE_LEN-1);
}
// Read line of text into given buffer
int32_t     TextReader::readLine(char *target_buf, size_t buf_size)
{
   int i, read_bytes=0, go_back;
   if(target_buf)
   {
      target_buf[0]=0;
      read_bytes = read(file_ptr, target_buf, buf_size-1);
      if(read_bytes>0) {
         // find first new line character
         target_buf[read_bytes]=0; // end of string
         go_back = 0;
         // find new line
         for(i=0; i<read_bytes; i++) {
            if(target_buf[i]=='\n') {
               go_back = read_bytes-i-1;
               read_bytes=i+1;
               target_buf[i]=0; // new end of string
               break;
            }
         }
         // scroll back for go_bytes
         if(go_back) lseek(file_ptr, -1*go_back, SEEK_CUR);
         line_num++;
         return read_bytes;
      } else return -1;
   }
   return -1;
}


// Find a keyword from the actual place - if found, set the internal cursor ptr to line containing the searched keyword.
int8_t      TextReader::searchForKeyword(const char *keyword) {
   int start_line = line_num;
   if(!keyword || !strlen(keyword) || file_ptr<0) return -1;
   while( readLine()!=-1 ) {
      if(strlen(buf) && strstr(buf, keyword)) {
         // found!
         scrollToLine(line_num-1);
         return 0;
      }
   }
   // not found, revert
   scrollToLine(start_line);
   return 1;
}

      
// Scroll to specified line
int32_t     TextReader::scrollToLine(int d_line) {
   if(d_line<0) return -1;
   if(d_line < line_num) {
      // set ptr to begining
      lseek(file_ptr, 0, SEEK_SET);
      line_num=0;
   }
   // move forward
   while( line_num<d_line ) {
      if( readLine()<0 ) {
         return line_num;
      }
   }
   return line_num;
}


// Check if buffer has contents
int8_t      TextReader::checkContents(const char *tbuf)
{
   int i, len;
   if(!tbuf) return 0;
   if(!strlen(tbuf)) return 0;
   len = strlen(tbuf);
   for(i=0;i<len;i++) {
      if(tbuf[i]!=' ' && tbuf[i]!='\n' && tbuf[i]!='\r' && tbuf[i]!='\t') return 1;
   }
   return 0;
}

// Turn string (in place) uppercase
int8_t      TextReader::toUpper(char *tbuf)
{
   int i, len;
   if(!tbuf || !strlen(tbuf)) return 1;
   len = strlen(tbuf);
   for(i=0;i<len;i++) tbuf[i]=toupper(tbuf[i]);
   return 0;
}


// Closes the file
void        TextReader::closeFile() {
   if(file_ptr>=0) close(file_ptr);
   file_ptr = -1;
   line_num = 0;
}

// Destructor
TextReader::~TextReader(void) {
   closeFile();
}

/**
 * @}
 */

}
