#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 *
 * Copyright (C) 2008 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file       DirScanner.cpp
 *
 * @author     Maciej 'karp' Karpiuk
 *
 * @brief      Implementation of DirScanner class
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
//#include <liblog/logsys.h>
#include "libfoundation/DirScanner.h"

namespace com_verifone_util
{

/**
 * @addtogroup Util
 * @{
 */

// constructor
DirScanner::DirScanner(void) {
   started = false;
   compare = "*";
#if defined(POSIX) || defined(__GNUC__)
   dir = NULL;
#elif  VERIXV
   fbuf[0]=0;
   drive='I';
   group=1;
#endif
}
// constructor
DirScanner::DirScanner(const char *mask) {
   started = false;
#if defined(POSIX) || defined(__GNUC__)
   dir = NULL;
#elif  VERIXV
   fbuf[0]=0;
   drive='I';
   group=1;
#endif
   setMask(mask);
}

// set new mask for DirScanner (and reset position)
void     DirScanner::setMask(const char *new_mask)
{
   int start = 0;
   started = false;
#ifdef VERIXV
   fbuf[0]=0;
   drive_index = 0;
   drive_set = false;
   group_set = false;
   drive='I';
   group=1;
   // parse for drive and group
   if(new_mask && strlen(new_mask)>1) {
      // get drive
      if(new_mask[1]==':') {
         drive=toupper(new_mask[0]);
         drive_set = true;
         start = 2;
      }
      // get group
      int i, len=strlen(new_mask);
      if(new_mask[2]=='/') {
         group = 15;
         group_set = true;
         start = 3;
      } else {
         char *new_pos = strchr(new_mask+start, '/');
         if(new_pos) {
            // group is present
            sscanf(new_mask+start, "%d/", &group);
            group_set = true;
            start = (new_pos-new_mask);
         }
      }
   }
#endif
   if(new_mask)   compare = new_mask+start;
   else           compare = "*";
   // if (new_mask) dlog_msg("DirScanner> Mask %s", new_mask+start);
}

// operator = - same as setMask()
DirScanner const &   DirScanner::operator=(const char *new_mask)
{
   setMask(new_mask);
   return *this;
}

// get new filename matching specified mask
int8_t   DirScanner::getNext(char *target_buf)
{
   int8_t   retVal=0;
   if(target_buf) target_buf[0]=0;
#if defined(POSIX) || defined(__GNUC__)
   struct dirent  *dp;
   if(!dir || !started) {
      if(dir) closedir(dir);
      dir = opendir(".");
      started = true;
   }
   if(!dir) return 0;
   // enter the current dir
   while(dp = readdir(dir)) {
      if(dp->d_name) {
         if( strcmp(dp->d_name, ".")      &&  strcmp(dp->d_name, "..") &&
            !strstr(dp->d_name, ".P7S")   && !strstr(dp->d_name, ".p7s") )
         {
            if(strlen(dp->d_name) && (compare==dp->d_name)) {
               if(target_buf) strcpy(target_buf, dp->d_name);
               return 1;
            }
         }
      }
   }
#elif VERIXV
   int one_shot = 0, actual_group=get_group();
   char act_buf[MAX_FILENAME+3];
   char drives[] = { 'I', 'F' };
   //set_group(group);
   if (group_set) set_group(group);
   do
   {
       if(!started) {
          if (!strlen(fbuf))
          {
              if (drive_set) sprintf(fbuf, "%c:", drive);
              else sprintf(fbuf, "%c:", drives[drive_index]);
          }
          if (dir_get_first( fbuf )) {
             // No files
             break;
          }
          started = true;
          one_shot = 1;
       }
       // dlog_msg("Checking group %d, drive %c", get_group(), drive_set ? '!' : drives[drive_index]);
       // main search
       do {
          // dlog_msg("DirScanner> Found file %s", fbuf);
          for (int i = strlen(fbuf)-1; i >= 0; --i) fbuf[i] = tolower(fbuf[i]);
          if(one_shot && strlen(fbuf) && (compare==fbuf)) {
             // dlog_msg("DirScanner> File %s matches mask!", fbuf);
             if(target_buf) strcpy(target_buf, fbuf);
             set_group(actual_group);
             return 1;
          }
          one_shot = 1;
       }
       while( dir_get_next(fbuf)==0 );
       started = false;
       one_shot = 0;
       fbuf[0]=0;
       ++drive_index;
   } while(!drive_set && drive_index <= 1);
   if (group_set) set_group(actual_group);
#endif
   if(target_buf) target_buf[0]=0;
   return retVal;
}

// Destructor
DirScanner::~DirScanner() {
#if defined(POSIX) || defined(__GNUC__)
   if(dir) closedir(dir);
   dir = NULL;
#endif
}

/**
 * @}
 */

}
