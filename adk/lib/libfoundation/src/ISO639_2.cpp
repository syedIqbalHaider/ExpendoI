/*****************************************************************************
 *
 * Copyright (C) 2010 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file       ISO639_2.cpp
 *
 * @author     Maciej 'karp' Karpiuk
 *
 * @brief      Implementation of ISO639-2 checker class
 */

#include "libfoundation/ISO639_2.h"
#include <algorithm>

namespace com_verifone_util
{

const int     ISO639_2::m_item_length       = 3;
// !!!!!!!!THE LIST BELOW IS NOT FULL YET!!!!!!!!
const char *  ISO639_2::m_accepted_codes[]  = { "ace", "afr", "ang", "ara", "aus", "chi", "cze",
                                                "dan", "dum", "eng", "fre", "ger", "ita", "jpn",
                                                "lat", "nno", "pol", "por", "rum", "rus", "spa",
                                                "swe", 0};


bool          ISO639_2::valid_item(std::string      candidate)
{
    if(candidate.size()!=m_item_length) return false;
    // lowercase the candidate
    std::transform(candidate.begin(), candidate.end(), candidate.begin(), (int (*)(int))std::tolower);
    // to be done better!!!
    int i=0;
    while(m_accepted_codes[i]) {
        if(candidate.compare(m_accepted_codes[i])==0) return true;
        i++;
    }
    return false;
}

}
