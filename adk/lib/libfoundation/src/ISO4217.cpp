/*****************************************************************************
 *
 * Copyright (C) 2010 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file       ISO4217.cpp
 *
 * @author     Nick Tristram
 *
 * @brief      Implementation of ISO4217 checker class
 */

#include "libfoundation/ISO4217.h"
#include <algorithm>

namespace com_verifone_util
{

const int     ISO4217::m_item_length       = 3;
// !!!!!!!!THE LIST BELOW IS NOT FULL YET!!!!!!!!
// These lists must be kept in sync
const char *  ISO4217::m_accepted_codes[]  = {  "aud", "cad", "chf", "czk", "eur", "gbp", "gip",
                                                "hkd", "ils", "isk", "jpy", "mxn", "mxv", "nok",
                                                "nzd", "pln", "sek", "usd", 0};

const int     ISO4217::m_accepted_nums[]   = {    036,  124,  756,   203,  978,   826,  292,
                                                  344,  376,  352,   392,  484,   979,  578,
                                                  554,  985,  752,   840,  0};

bool          ISO4217::valid_item(std::string      candidate)
{
    if(candidate.size()!=m_item_length) 
		return false;
	
    // lowercase the candidate
    std::transform(candidate.begin(), candidate.end(), candidate.begin(), (int (*)(int))std::tolower);
	
    // to be done better!!!
    int i=0;
    while(m_accepted_codes[i]) {
        if(candidate.compare(m_accepted_codes[i])==0) return true;
        i++;
    }
    return false;
}

bool          ISO4217::valid_item(int      candidate)
{
    if(candidate <= 0) 
		return false;
	
    // to be done better!!!
    int i=0;
    while(m_accepted_nums[i]) {
        if(candidate == m_accepted_nums[i]) return true;
        i++;
    }
    return false;
}

}
