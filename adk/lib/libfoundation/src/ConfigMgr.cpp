
#ifndef __cplusplus
#error "This file is for C++ only!"
#endif 

/*****************************************************************************
 *
 * Copyright (C) 2008 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file       ConfigMgr.cpp
 * @author     Rafal Pietruch
 * @brief      Implementation of ConfigReader, ConfigEditor, SecureConfigReader
 *             and FileParser classes
 */ 

#if defined(POSIX) || defined(__GNUC__)
#  include <unistd.h>
#  include <fcntl.h>
#endif 
#include "libfoundation/ConfigMgr.h"
#include <map>
// #include <iostream>


//using namespace std;
using std::string;

using namespace com_verifone_text;

namespace com_verifone_configmgr
{

/**
 * @addtogroup ConfigMgr
 * @{
 */

  EditorSharedMap  ConfigMgr::editor_shared_map;
  SecuredSharedMap ConfigMgr::secured_shared_map;
  UserSharedMap    ConfigMgr::user_shared_map;
  DefaultSharedMap ConfigMgr::default_shared_map;

  void operator += (ConfigReader<com_verifone_text::TextReader>& left,
                    ConfigReader<com_verifone_text::TextReader>& right)
  {
    for (SectionMap::iterator section = right.sectionmap.begin();
         section != right.sectionmap.end();
         ++section)
    {
      for (TokenMap::iterator iter = section->second.begin();
           iter != section->second.end();
           ++iter)
      {
        left(section->first, iter->first) = iter->second;
      }
    }
  };

  void operator += (ConfigReader<com_verifone_text::SecuredTextReader>& left,
                    ConfigReader<com_verifone_text::TextReader>& right)
  {
    for (SectionMap::iterator section = right.sectionmap.begin();
         section != right.sectionmap.end();
         ++section)
    {
      for (TokenMap::iterator iter = section->second.begin();
           iter != section->second.end();
           ++iter)
      {
        left(section->first, iter->first) = iter->second;
      }
    }
  };

  EditorPtr&  ConfigMgr::GetConfigEditor(const char* path)
  {
    if (editor_shared_map.find(string(path)) == editor_shared_map.end())
    {
      editor_shared_map.insert(make_pair(string(path),
          EditorPtr(new ConfigEditor(path))));
    }
    return editor_shared_map[string(path)];
  } // GetConfigEditor

  UserPtr&    ConfigMgr::GetUserConfig(const char* path)
  {
    if (user_shared_map.find(string(path)) == user_shared_map.end())
    {   
      if (default_shared_map.find(string(path)) == default_shared_map.end()) {
        user_shared_map.insert(make_pair(string(path),
            UserPtr(new UserConfig(path))));
      }
      else {
        user_shared_map.insert(make_pair(string(path),
            UserPtr(new UserConfig(path, (*default_shared_map[string(path)])))));
      }
    }
    return user_shared_map[string(path)];
  } // GetUserConfig

  SecuredPtr& ConfigMgr::GetSecuredConfig(const char* path)
  {
    if (secured_shared_map.find(string(path)) == secured_shared_map.end())
    {
      if (default_shared_map.find(string(path)) == default_shared_map.end()) {
        secured_shared_map.insert(make_pair(string(path),
            SecuredPtr(new SecuredConfig(path))));
      }
      else {
        secured_shared_map.insert(make_pair(string(path),
            SecuredPtr(new SecuredConfig(path, (*default_shared_map[string(path)])))));
      }
    }
    return secured_shared_map[string(path)];
  } // GetSecuredConfig

  DefaultPtr& ConfigMgr::GetDefaultConfig(const char* path)
  {
    if (default_shared_map.find(string(path)) == default_shared_map.end()) {
      default_shared_map.insert(make_pair(string(path),
          DefaultPtr(new DefaultConfig())));
    }
    return default_shared_map[string(path)];
  } // GetDefaultConfig

  bool ConfigMgr::SetEditedConfig( EditorPtr& edited_config )
  {
    bool bRv = false;

    EditorSharedMap::iterator editor_iter;
    for (editor_iter = editor_shared_map.begin();
         editor_iter !=  editor_shared_map.end() && !bRv;
         ++editor_iter)
    {
      if (editor_iter->second == edited_config)
      {
        editor_iter->second->Save();
        bRv = true;
      }
    }
    if (bRv)
    {
      for (UserSharedMap::iterator user_iter = user_shared_map.begin();
           user_iter !=  user_shared_map.end();
           ++user_iter)
      {
        if (user_iter->first == editor_iter->first) {
            *user_iter->second += *editor_iter->second;
        }
      }
    }
    return bRv;
  } // SetEditedConfig


  DefaultConfig::DefaultConfig()
  : ConfigReader<TextReader>("") {
  } // DefaultConfig

  DefaultConfig::~DefaultConfig() {
  } //~DefaultConfig


  ConfigEditor::ConfigEditor(const string &filename)
  : ConfigReader<TextReader>(filename) {
  } // ConfigEditor

  ConfigEditor::~ConfigEditor() {
  } //~ConfigEditor

  bool ConfigEditor::Save()
  {
    bool bRv = false;
    parser.Close();

    // cout << endl << "********************* Save ***********************" << endl;
    if (parser.Open(name, O_RDWR))
    {
      // cout << "File opened" << endl;
      Token token(make_pair(TokenName(), TokenData()));
      SectionName section_name;
      bool first_section = true;

      while (parser.GetNextToken(token))
      {
        // cout << "1. .............### token : " << token.first << " = " << token.second << endl;                
        if (section_name != parser.GetCurrentSection())
        {
          // check if there were added other tokens to section
          if (!first_section // check if it is not start
           && !sectionmap[section_name].empty())
          {
            for (TokenMap::iterator iter = sectionmap[section_name].begin();
                 iter != sectionmap[section_name].end();
                 ++iter)
            {
              Token new_token = make_pair(iter->first, iter->second);
              // cout << "1.1.............Put token : " << new_token.first << " = " << new_token.second << endl;
              parser.PutToken(new_token);
            }
            sectionmap.erase(section_name);
          }
          // set current section name
          section_name = parser.GetCurrentSection();
          parser.SetCurrentSection(section_name);
          // cout << "1. ...Set current section : " << section_name << endl;
        }
        // find token data 
        if (sectionmap.find(section_name) != sectionmap.end()
         && sectionmap[section_name].find(token.first)
         != sectionmap[section_name].end())
        {
          token.second = sectionmap[section_name][token.first];
          // cout << "1. ...........Clear token : " << token.first << endl;
          sectionmap[section_name].erase(token.first);

          if (sectionmap[section_name].empty()) {
            // cout << "1.2.........Clear section : " << section_name << endl;
            sectionmap.erase(section_name);
          }
        }
        // cout << "1.2.............Put token : " << token.first << " = " << token.second << endl;

        // put token to the config
        parser.PutToken(token);

        // after first token read check for tokens from the same section
        // that was added
        first_section = false;
      }
      // check other sections added
      for (SectionMap::iterator section = sectionmap.begin();
           section != sectionmap.end();
           ++section)
      {
        // cout << "Section : " << section->first << endl;
        if (section_name != section->first)
        {
          section_name = section->first;
          parser.SetCurrentSection(section_name);
          // cout << "2. ...Set current section : " << section_name << endl;
        }
        for (TokenMap::iterator iter = section->second.begin();
             iter != section->second.end();
             ++iter)
        {
          token = make_pair(iter->first, iter->second);
          // cout << "2. .............Put token : " << token.first << " = " << token.second << endl;
          parser.PutToken(token);
        }          
        // cout << "2. .........Clear section : " << section->first << endl;
        section->second.clear();
      }
      // clear all sections
      sectionmap.clear();
      parser.Close();
      Open(name);
      bRv = true;
    }
    return bRv;
  } // Save

/**
 * @}
 */
}

