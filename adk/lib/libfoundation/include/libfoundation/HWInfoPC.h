#ifndef HWINFO_H
#define HWINFO_H

#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 *
 * Copyright (C) 2008 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file       HWInfo.h
 *
 * @author     Maciej 'karp' Karpiuk
 *
 * @brief      Definition of HWInfo class
 */


#include <string.h>
#include <stdint.h>
#include "HWInfo.h"


namespace com_verifone_util
{
/**
 * @addtogroup Util
 * @{
 */


/**
 * @brief        The class is implements a HWInfo class, which gives basic information on the executing HW
 *
 */
class HWInfoPC
{
   public:

      /**
       * @brief       return executing HW model type
       * @return      char buffer
       */
      const char *    getHWModel() { return "IBM PC"; } ;

      /**
       * @brief       return OS id string on the executing HW
       * @return      char buffer
       */
      const char *    getOSid() { return "WINXP"; };

      /**
       * @brief       return RAM size on the executing HW
       * @return      long [kilobytes]
       */
      long            getRAMsize() { return 2048*1024; }

      /**
       * @brief       return FLASH size on the executing HW
       * @return      long [pixels]
       */
      long            getFLASHsize() { return 2048*1024; }

      /**
       * @brief       return display width on the executing HW
       * @return      long [pixels]
       */
      long            getDisplayWidth() { return 1024; }

      /**
       * @brief       return display height on the executing HW
       * @return      long [pixels]
       */
      long            getDisplayHeight() { return 768; }

      /**
       * @brief       return executing HW lifetime
       * @return      long [seconds]
       */
      long            getLifetime() { return 1; }

      /**
       * @brief       return serial number of the executing HW
       * @return      char buffer
       */
      const char *    getSerialNumber() { return "997";}

      /**
       * @brief       return PTID of the executing HW
       * @return      char buffer
       */
      const char *    getPTID() { return "01234"; }

      /**
      	* @brief       return enum with terminal type
      	*   @return    terminal type
      	*/
      	termTYPE       getTerminalType()  { return PCx86; }

      /**
      	* @brief       return true whether we're on EVO (Trident)
      	* @return      true if we're on Evo, false otherwise
      	*/
      	bool           weAreEVOTerminal()  { return false; }

      /**
      	* @brief       return true whether we're on V/OS (820, Ux300)
      	* @return      true if we're on V/OS, false otherwise
      	*/
      	bool           weAreVOSTerminal()  { return false; }

      /**
        * @brief       return true whether we're unattended (for now, this is always false!)
        * @return      true if we're unattended, false otherwise

        */
        bool weAreUnattendedTerminal()  { return false; }

};


/**
 * @}
 */

}
#endif /* HWINFO_H */
