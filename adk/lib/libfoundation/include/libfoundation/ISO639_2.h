/*****************************************************************************
 *
 * Copyright (C) 2010 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file       ISO639_2.h
 *
 * @author     Maciej 'karp' Karpiuk
 *
 * @brief      Definition of ISO639-2 checker class
 */

#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

#ifndef ISO639_2_H
#define ISO639_2_H

#include <string>

namespace com_verifone_util
{
/**
 * @addtogroup Util
 * @{
 */

class ISO639_2
{
   public:
       /**
        * @brief            check if given language code is in the list
        * @param candidate  language code to check
        * @return           true if code is in the list, false if not
        */
       static bool          valid_item(std::string      candidate);

   private:
       static const int     m_item_length;
       static const char *  m_accepted_codes[];
};
}
#endif /* ISO639_2_H */
