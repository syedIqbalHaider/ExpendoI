#ifndef EXCLUSIONPARSER_H
#define EXCLUSIONPARSER_H

#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 *
 * Copyright (C) 2008 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file       ExclusionFileParser.h
 *
 * @author     Maciej 'karp' Karpiuk
 *
 * @brief      Definition of ExclusionFileParser class
 */

#ifdef VERIXV
#include <svc.h>
#include <liblog/logsys.h>
#endif
#include <string.h>
#include <stdint.h>
#include "libfoundation/SecuredTextReader.h"
#include "libfoundation/PathComparator.h"

using namespace com_verifone_text;

namespace com_verifone_util
{
/**
 * @addtogroup Util
 * @{
 */


/**
 * @brief        The class is implements a ExclusionFileParser class, which can read exclusion file and check given filenames for being excluded
 *
 */
class ExclusionFileParser
{
   public:
      /**
       * @brief       Constructor
       *
       * @param[in]   filename - exclusion config file path
       */
      ExclusionFileParser(const char *filename);

      /**
       * @brief       operator == - checks if given filename is in the Exclusion list
       *
       * @param[in]   fname - compared filename
       *
       * @return      true if is excluded / false if is not protected
       */
      bool   operator==(const char *fname);

      /**
       * @brief       operator bool - checks if exclude config file has been properly opened
       *
       * @return      1 if opened / 0 if is not opened
       */
      operator bool() { return (textreader.fileOpened()==1); }

   private:
      SecuredTextReader    textreader; ///< config file handle (authentication needed)
      PathComparator       compare;    ///< comparator mechanism
      enum {INCLUDE_CHAR='+',EXCLUDE_CHAR='-'};
};


/**
 * @}
 */

}
#endif /* EXCLUSIONPARSER_H */
