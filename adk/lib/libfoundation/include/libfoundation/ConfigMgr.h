#ifndef CONFIG_MGR_H
#define CONFIG_MGR_H

#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 *
 * Copyright (C) 2008 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file       ConfigMgr.h
 * @author     Rafal Pietruch
 *
 * @brief      classes for configuration reading from files
 *             ConfigEditor  - interface for edition of configuration file
 *             UserConfig    - interface for reading configuration file
 *             SecuredConfig - interface for reading signed config. file
 */

#include <string>

#if defined USE_STR_STREAM
#  include <sstream>
#elif defined(POSIX) || defined(__GNUC__)
#  include<stdio.h>
#  include<stdlib.h>
#endif

#include <map>
#include "TextWriter.h"
#include "TextReader.h"
#include "SecuredTextReader.h"

#ifdef USE_SMART_PTR
#  include <boost/shared_ptr.hpp>
#endif

#ifndef USE_STR_STREAM
#  define ITOA_MAX_LEN 32
#endif

namespace com_verifone_configmgr
{

/**
 * @addtogroup ConfigMgr
 * @{
 */

  // Classes declarations
  class   TokenData;     // type of configuration key data
  class   StringHelper;  // helper class for strings parsing
          template <class TReader, class TWriter = com_verifone_text::TextWriter>
  class   FileParser;    // class implements the parser of configuration file
          template <class TReader>
  class   ConfigReader;  // helper class to parse tokens using FileParser
  class   ConfigEditor;  // helper class to edit tokens using FileParser
  class   DefaultConfig; // class to be used as default config

  // derived classes
  typedef ConfigReader<com_verifone_text::SecuredTextReader> SecuredConfig;
  typedef ConfigReader<com_verifone_text::TextReader>        UserConfig;

  // types
  typedef int TOpenMode; // file openning mode, for reading or writing tokens
  typedef std::string TokenName; // token name type, usually string
  typedef std::string SectionName; // section name type, usually string
  typedef std::pair<TokenName, TokenData> Token; // the token type-name and data
  typedef std::map<TokenName, TokenData> TokenMap; // UserConfig map of tokens
  typedef std::map<const SectionName, TokenMap> SectionMap; // map of sections

#ifdef USE_SMART_PTR
  typedef  boost::shared_ptr< ConfigEditor  > EditorPtr;
  typedef  boost::shared_ptr< SecuredConfig > SecuredPtr;
  typedef  boost::shared_ptr< UserConfig    > UserPtr;
  typedef  boost::shared_ptr< DefaultConfig > DefaultPtr;
#else
  typedef  ConfigEditor*  EditorPtr;
  typedef  SecuredConfig* SecuredPtr;
  typedef  UserConfig*    UserPtr;
  typedef  DefaultConfig* DefaultPtr;
#endif

  typedef  std::map< std::string, EditorPtr  > EditorSharedMap;
  typedef  std::map< std::string, SecuredPtr > SecuredSharedMap;
  typedef  std::map< std::string, UserPtr    > UserSharedMap;
  typedef  std::map< std::string, DefaultPtr > DefaultSharedMap;

/**
  * @brief The class provides the interface of using
  *        parsers of configuration file in windows INI file format
  */
  class ConfigMgr
  {
  public:

  /**
    * @brief create the new configuration editor (if not created yet)
    * @param [in] path the path to the configuration file to open
    * @return the reference to pointer to the configuration editor
    */
    static EditorPtr&  GetConfigEditor ( const char* path );

  /**
    * @brief create the new configuration reader (if not created yet)
    * @param [in] path the path to the configuration file to open
    * @return the reference to pointer to the configuration reader
    */
    static UserPtr&    GetUserConfig   ( const char* path );

  /**
    * @brief create the new secured config. reader (if not created yet)
    * @param [in] path the path to the configuration file to open
    * @return the reference to pointer to the secured config. reader
    */
    static SecuredPtr& GetSecuredConfig( const char* path );

  /**
    * @brief create the new default configuration (if not created yet)
    * @param [in] path name of conf. file to use edited default config.
    * @return the reference to pointer to the configuration editor
    */
    static DefaultPtr& GetDefaultConfig( const char* path );

  /**
    * @brief flush edited config. to the related configuration file
    * @param [in] edited_config reference to editor object to write the data from
    * @return true if flush was successful
    */
    static bool SetEditedConfig( EditorPtr& edited_config );

  protected:

    static EditorSharedMap  editor_shared_map;  // map of editors for related filename
    static SecuredSharedMap secured_shared_map; // map of secured readers
    static UserSharedMap    user_shared_map;    // map of configuration readers
    static DefaultSharedMap default_shared_map; // map of default configurations

  }; /* ConfigMgr */


/**
  * @brief Type class of configuration key data
  */
  class TokenData : public std::string
  {
  public:

    /**
      * @brief TokenData default constructor
      */
    TokenData() {};


    /**
      * @brief TokenData constructor
      * @param [in] s string to populate initial data
      */
    TokenData(std::string s) : std::string(s) {};


    /**
      * @brief TokenData destructor
      */
    ~TokenData() {};


    /**
      * @brief assign string operator
      * @param [in] s string to assign to TokenData
      */
    TokenData& operator=(const char* s) {
      assign(s);
      return *this;
    }; // =


    /**
      * @brief assign int operator
      * @param [in] i integer to assign to TokenData
      */
    inline TokenData& operator=(int i)
    {
#ifdef USE_STR_STREAM
      std::ostringstream ss;
      ss << i;
      *this = ss.str();
#else
      char str[ITOA_MAX_LEN];
      memset(str, 0, sizeof(str));
//NKJT - Need to write an add in function for Cygwin
#if !defined(__GNUC__)	  
      *this = ltoa(static_cast<long>(i), str, 10);
#endif
#endif
      return *this;
    }; // =


    /**
      * @brief int operator
      * @return integer value of TokenData
      */
    inline operator int() const {
      return atoi(this->c_str());
    }; // int

  }; /* TokenData */


/**
  * @brief Helper class for strings parsing
  */
  class StringHelper
  {
  public:

    /**
      * @brief StringHelper default constructor
      */
    StringHelper() {};


    /**
      * @brief StringHelper constructor
      * @param [in] _str worker string to be parsed by class
      */
    StringHelper(const std::string& _str) : str(_str) {};


    /**
      * @brief StringHelper destructor
      */
    ~StringHelper() {};


    /**
      * @brief removes whitespaces from begining
      *        and end of string
      * @param [in, out] s reference to string to remove whitespaces from
      */
    static inline void RemoveWhitespaces(std::string& s)
    {
      if (!s.empty()
       && s.find_first_not_of(CHAR_SPACE) != 0) {
        s.erase(0, s.find_first_not_of(CHAR_SPACE));
      }
      if (!s.empty()
       && s.find_last_not_of(CHAR_SPACE) != s.length()) {
        s.erase(s.find_last_not_of(CHAR_SPACE) + 1);
      }
    }; // RemoveWhitespaces


    /**
      * @brief divides worker string that was given in constructor
      *        to key and its value:
      *        e.g.: string "left = right" to "left", "right" strings
      * @param [out] _left  reference to output sstring from the left side
      * @param [out] _right reference to output sstring from the right side
      * @return true if the key and its value are found
      *         and parameters populated
      */
    inline bool ParseEquation(std::string& _left, std::string& _right)
    {
      bool bRv = false;
      if (!str.empty() && str.find(CHAR_EQ) != str.npos)
      {
        _left = str.substr(0, str.find(CHAR_EQ));
        _right = str.substr(str.find(CHAR_EQ) + 1);
        RemoveWhitespaces(_left);
        RemoveWhitespaces(_right);
        bRv = true;
      }
      return bRv;
    }; // ParseEquation


    /**
      * @brief tries to retrieve section name from string
      * @param [out] section_name reference to string
      *        to put extracted section name
      * @return true if the name is successfully extracted
      *         and output parameter polpulated
      */
    inline bool GetSection(std::string& section_name)
    {
      bool bRv = false;
      if (!str.empty()
       && str.length() >= 2 // default section []
       && str.find(CHAR_BEGIN_SECT) == 0
       && str.find(CHAR_END_SECT) != str.npos)
      {
        // default section []
        if (str.find(CHAR_END_SECT) == 1) {
          section_name = "";
        } else {
          section_name = str.substr(1, str.find(CHAR_END_SECT) - 1);
          RemoveWhitespaces(section_name);
        }
        bRv = true;
      }
      return bRv;
    }; // GetSection


    /**
      * @brief concstructs section indicator from its name
      * @param [in] section_name the name of the section
      * @return the string of the new section specifier
      *         e.g.: from section name "SECT1" it produces "[SECT1]" string
      */
    inline static std::string MakeSection(const std::string& section_name) {
      return CHAR_BEGIN_SECT + section_name + CHAR_END_SECT;
    }; // MakeSection


    /**
      * @brief concstructs equation string from left and right sides
      * @param [in] _left  left side of equation
      * @param [in] _right right side of equation
      * @return constructed equation string of the form "<left>=<right>"
      */
    inline static std::string MakeEquation(const std::string& _left,
                                           const std::string& _right)
    {
      return _left + CHAR_EQ + _right;
    }; // MakeSection


    static const char CHAR_EQ         = '=';  // equation
    static const char CHAR_SPACE      = ' ';  // space
    static const char CHAR_BEGIN_SECT = '[';  // begin section name
    static const char CHAR_END_SECT   = ']';  // end of section name

  private:

    /// worker string
    std::string str;

  }; /* StringHelper */


/**
  * @brief  The class is a configuration file reader used by ConfigReader
  *         The class implements a parser of configuration file
  *         of windows INI file format
  */
  template <class TReader, class TWriter>
  class FileParser
  {
  public:

    /**
      * @brief Destructor of FileParser
      */
    ~FileParser() {};


    /**
      * @brief Construction of FileParser instance
      * @param [in] filename configuration file name to open
      * @param [in] openmode openning mode (read-only, write-only or read-write)
      * return true if succesfully opened all needed files
      */
    bool Open(const std::string& filename, TOpenMode openmode)
    {
      bool bRv = true;
      if (openmode == O_RDONLY) {
        inname = filename;
        infile.openFile(inname.c_str());
      }
      else if (openmode == O_WRONLY)
      {
        outname = filename;
        outfile.openFile(outname.c_str());
      }
      else if (openmode == O_RDWR)
      {
        inname  = filename;
        outname = filename + ".ptf";
        infile.openFile(inname.c_str());
        outfile.openFile(outname.c_str());
      }
      if ( !inname.empty()  && !infile.fileOpened()
        || !outname.empty() && !outfile.fileOpened()) {
        bRv = false;
      }
      return bRv;
    }; // Open


    /**
      * @brief Destruction of FileParser instance
      *        if instance was opened for writing
      *        it will save temporary file as the original one
      * @return false if overwritting original file fails
      */
    bool Close()
    {
      bool bRv = true;
      if (infile.fileOpened()) {
        infile.closeFile();
      }
      if (outfile.fileOpened()) {
        outfile.closeFile();
      }
      if (!inname.empty() && !outname.empty())
      {
        if (rename(outname.c_str(), inname.c_str()) != 0) {
          bRv = false;
        }
      }
      inname.clear();
      outname.clear();
      section.clear();
      return bRv;
    }; // Close


    /**
      * @brief Retrieves next valid token from opened file
      *        if no more tokens presented in file retuyrns false
      *        if file is opened for writing the function deletes the token
      * @param [out] reference to token pair (name and data) to retrieve
      * @return true if token successfuly extracted
      * NOTICE:
      * the token should be given using PutToken after reading
      * to not loose it
      */
    bool GetNextToken(Token& token)
    {
      bool bRv = false;
      if (infile.fileOpened())
      {
        char lbuf[MAX_LINE_LEN + 1];
        memset(lbuf, 0, sizeof(lbuf));
        while (!bRv && infile.readLine(lbuf, sizeof(lbuf) - 1) >= 0)
        {
          std::string s(lbuf);
          if (!s.empty())
          {
            StringHelper conv(s);
            if (conv.GetSection(section)) {
              // leave section selection to higher class to be able
              // to append any other tags left in previous section
            }
            else if (conv.ParseEquation(token.first, token.second)) {
              if (!token.first.empty()) bRv = true;
            }
            else if (outfile.fileOpened()) { // comment
              outfile.writeLine(s.c_str(), s.length());
            }
          }
          else if (outfile.fileOpened()) { // new line
            outfile.writeLine();
          }
          memset(lbuf, 0, sizeof(lbuf));
        }
      }
      return bRv;
    }; // GetNextToken


    /**
      * @brief Returns current section name (default "")
      * @return section name
      */
    inline SectionName GetCurrentSection() { return section; };


    /**
      * @brief If file is opened for writing
      *        it puts given token in actual place
      * @param [in] token token to put
      * @return true if succeeded
      */
    bool PutToken(Token& token)
    {
      bool bRv = false;
      if (outfile.fileOpened()) {
        outfile.writeLine(StringHelper::MakeEquation(token.first, token.second).c_str());
        bRv = true;
      }
      return bRv;
    }; // PutToken


    /**
      * @brief Puts section name to the config
      * @param [in] section_name name of the section to put
      */
    void SetCurrentSection(const SectionName& section_name)
    {
      if (outfile.fileOpened()) {
        section = section_name;
        outfile.writeLine(StringHelper::MakeSection(section).c_str());
      }
    }; // SetCurrentSection


  protected:

    /**
      * @brief default constructor protected to use the config manager
      *        in case of FileParser
      */
    FileParser() : infile (""), outfile("") {};

    /// Config reader will use the class to manage its functionality
    template <class T> friend class ConfigReader;

  private:

    TReader       infile;    // input file stream
    TWriter       outfile;   // output file stream

    std::string   inname;    // input file name
    std::string   outname;   // output file name

    SectionName   section;   // section name

  }; /* FileParser */


/**
  * @brief  The helper class to parse tokens using FileParser
  */
  template <class TReader>
  class ConfigReader
  {
  public:

    /**
      * @brief destructor
      */
    ~ConfigReader() { parser.Close(); };


    /**
      * @brief check if file for reading was opened
      */
    inline operator bool () const { return opened; };


    /**
      * @brief returns data of token with name and section given as parameter
      * @param  [in] section_name - section name to read the key from
      * @param  [in] token_name - key name name to read the value from
      * @return token data - key value
      */
    TokenData& operator() (const SectionName& section_name,
                           const TokenName&   token_name)
    {
      if (sectionmap.find(section_name) == sectionmap.end()) {
        TokenMap tokenmap;
        tokenmap.insert(make_pair(token_name, std::string()));
        sectionmap.insert(make_pair(section_name, tokenmap));
      }
      else if (sectionmap[section_name].find(token_name) == sectionmap[section_name].end()) {
        sectionmap[section_name].insert(make_pair(token_name, std::string()));
      }
      return sectionmap[section_name][token_name];
    }; // ()


    /**
      * @brief  returns data of token with name given as parameter
      *         from default (empty name) section
      * @param  [in] token_name - parameter name
      * @return token data
      */
    inline TokenData& operator() (const TokenName& token_name) {
      return operator()("", token_name);
    }; // ()


  protected:

    /**
      * @brief protected constructor to be used only by ConfigMgr
      * @param [in] filename reference to filename to open
      */
    ConfigReader(const std::string &filename)
    : name(filename), opened(false) { Open(filename); };


    /**
      * @brief constructor
      * @param [in] filename reference to filename to open
      * @param [in] default_config reference to the config instance of default
      *             data that will be returned if keys will not be found
      *             in configuration file that was opened
      */
    ConfigReader(const std::string &filename,
                 ConfigReader<com_verifone_text::TextReader>& default_config)
    : name(filename), opened(false)
    {
      (*this) += default_config;
      Open(filename);
    };


    /**
      * @brief function opens configuration file and reads its data,
      *        storing it into map of sections and tokens
      * @param [in] filename reference to file name to be opened
      * @return true if file was succesfully opened
      */
    bool Open(const std::string &filename)
    {
      if (opened = parser.Open(filename, O_RDONLY))
      {
        // current token
        Token       token(make_pair(TokenName(), TokenData()));
        TokenMap    tokenmap;       // map of tokens in current section
        SectionName section_name;   // current section name

        while (parser.GetNextToken(token))
        {
          if (section_name != parser.GetCurrentSection())
          {
            if (!tokenmap.empty()) {
              sectionmap[section_name] = tokenmap;
            }
            section_name = parser.GetCurrentSection();

            // if new section already in the map copy all it content
            if (sectionmap.find(section_name) != sectionmap.end()) {
              tokenmap = sectionmap[section_name];
            }
            else {
              tokenmap.clear();
            }
          }
          tokenmap[token.first] = token.second;
        }
        if (!tokenmap.empty()) {
          sectionmap[section_name] = tokenmap;
        }
      }
      return opened;
    }; // Open


    /**
      * @brief frien function to append the configuration into other reader
      * @param [in] left  reader to append the configuration to
      * @param [in] right reader to be appended to configuration given as left
      */
    friend
    void operator += (ConfigReader<com_verifone_text::TextReader>& left,
                      ConfigReader<com_verifone_text::TextReader>& right);
    friend
    void operator += (ConfigReader<com_verifone_text::SecuredTextReader>& left,
                      ConfigReader<com_verifone_text::TextReader>& right);


    /// ConfigMgr will manage the class interface
    friend class ConfigMgr;

    const std::string   name;        // config file name to open
    SectionMap          sectionmap;  // map of tokens read from config file
    bool                opened;      // remembers the state of opeening file

    /// map of pointers to file parsers and related file names
    FileParser<TReader> parser;

  }; /* ConfigReader */


/**
  * @brief  The helper class to edit tokens using FileParser
  */
  class ConfigEditor : public ConfigReader<com_verifone_text::TextReader>
  {
  public:

    /**
      * @brief destructor
      */
    ~ConfigEditor();

  protected:

    /**
      * @brief protected constructor to be used only by ConfigMgr class
      * @param [in] filename reference to constant filename string
      */
    ConfigEditor(const std::string &filename);


    /**
      * @brief  function for flushing configuration to the file
      * @return true if config succesfully updated
      */
    bool Save ();


    /// ConfigMgr will manage the class interface
    friend class ConfigMgr;

  }; /* ConfigEditor */


/**
  * @brief  class to be used as default config
  */
  class DefaultConfig : public ConfigReader<com_verifone_text::TextReader>
  {
  public:

    /**
      * @brief destructor
      */
    ~DefaultConfig();

  protected:

    /**
      * @brief protected default constructor
      *        without assigned filename
      */
    DefaultConfig();


    /// ConfigMgr will manage the class interface
    friend class ConfigMgr;

  }; /* DefaultConfig */

/**
 * @}
 */
}
#endif /* CONFIG_MGR_H */

