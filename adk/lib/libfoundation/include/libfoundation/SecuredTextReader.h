#ifndef SECUREDTEXTREADER_H
#define SECUREDTEXTREADER_H

#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 *
 * Copyright (C) 2008 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file       SecuredTextReader.h
 *
 * @author     Maciej 'karp' Karpiuk
 *
 * @brief      Definition of SecuredTextReader class
 */

#ifdef VERIXV
#include <svc.h>
#include <liblog/logsys.h>
#endif
#include <string.h>
#include <stdint.h>
#include "libfoundation/TextReader.h"

namespace com_verifone_text
{
/**
 * @addtogroup TextReader
 * @{
 */


/**
 * @brief        The class is implements a SecuredTextReader class, which inherits functionality of TextReader class, but also adds file signing check
 *
 */
class SecuredTextReader : public TextReader
{
   public:
      /**
       * @brief       Constructor
       *
       * @param[in]  filename - file path to be opened
       * @param[in]  header_keyword - message in header to be found [optional]
       */
      SecuredTextReader(const char *filename);
      SecuredTextReader(const char *filename, const char *header_keyword);

      /**
       * @brief       Opens the file in secure mode
       *
       * @param[in]   filename - file path to be opened
       * @param[in]   header_keyword - message in header to be found [optional]
       *
       * @return      none
       */
      void     openFile(const char *filename, const char *header_keyword = NULL);

      /**
       * @brief       Check if the authenticated file contains specific header
       *
       * @param[in]   header_keyword - message in header to be found
       *
       * @return      0 for success / 1 for failure / -1 for error
       */
      int8_t   checkHeader(const char *header_keyword);

      /**
       * @brief       Move the cursor to the starting position just after the header
       *
       * @return      line num or -1 in case of error
       */
      int32_t  moveToStartingPosition(void);

      /**
       * @brief       closes the file
       */
      void     closeFile();

   private:

      // check if file was authenticated / 0 for success / 1 for failure
      int8_t   checkAuthentication(const char *filename);
      int32_t  first_line; ///< line from which start scanning
};


/**
 * @}
 */

}
#endif /* SECUREDTEXTREADER_H */
