#ifndef TEXTREADER_H
#define TEXTREADER_H

#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 *
 * Copyright (C) 2008 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file       TextReader.h
 *
 * @author     Maciej 'karp' Karpiuk
 *
 * @brief      Definition of TextReader class
 */

#ifdef VERIXV
#include <svc.h>
#include <liblog/logsys.h>
#endif
#include <string.h>
#include <stdint.h>

namespace com_verifone_text
{
/**
 * @addtogroup TextReader
 * @{
 */

#define  MAX_LINE_LEN      128


/**
 * @brief        The class is implements a TextReader class, which is used to manipulate text files
 *
 */
class TextReader
{
   public:
      /**
       * @brief       Constructor
       *
       * @param[in]  filename - file path to be opened
       */
      TextReader(const char *filename);

      /**
       * @brief       open a new file
       *
       * @param[in]  filename - file path to be opened
       *
       * @return      0 for success / 1 for failure
       */
      int8_t   openFile(const char *filename);

      /**
       * @brief       Tell if a file is currently open
       *
       * @return      1 - open / 0 - not open
       */
      int8_t   fileOpened(void) { return ((file_ptr>=0)?1:0); }

      /**
       * @brief       Read line of text into given buffer
       *
       * @param[out]  target_buf - where to store the text
       * @param[in]   buf_size - max size of the specified buffer [optional]
       *
       * @return      length of read buffer of negative number for an error
       */
      int32_t  readLine(char *target_buf);
      int32_t  readLine(char *target_buf, size_t buf_size);
      // move one line forward without returning data
      int32_t  readLine(void);


      /**
       * @brief       Find a keyword from the actual place - if found, set the internal cursor ptr to line containing the searched keyword.
       *
       * @param[in]   keyword - keyword to be found within the file
       *
       * @return      0 for success / 1 for failure / -1 for error
       */
      int8_t   searchForKeyword(const char *keyword);


      /**
       * @brief       Scroll to specified line
       *
       * @param[in]   d_line - num of line to be set (beg with 0..)
       *
       * @return      line set after method execution or -1 for error. If return < line_num, it means that there were not enough lines of text in the file
       */
      int32_t  scrollToLine(int d_line);


      /**
       * @brief       Get current line number
       *
       * @return      current line number
       */
      int32_t  getLineNum(void) { return line_num; }


      /**
       * @brief       Check if buffer has contents
       *
       * @param[in]   tbuf - buffer to be scanned
       *
       * @return      1 if there is sth, 0 if not
       */
      int8_t   checkContents(const char *tbuf);

      /**
       * @brief       Turn string (in place) uppercase
       *
       * @param[in-out]   tbuf - buffer to be turned uppercase
       *
       * @return      0 for success, 1 for error
       */
      int8_t      toUpper(char *tbuf);

      /**
       * @brief       closes the file
       */
      void     closeFile();

      /**
       * @brief       Destructor
       */
      ~TextReader(void);
   protected:
      char     buf[MAX_LINE_LEN];   ///< work buffer [searching etc.]
   private:
      int      file_ptr;            ///< file handle
      int32_t  line_num;            ///< current line number
};


/**
 * @}
 */

}
#endif /* TEXTREADER_H */
