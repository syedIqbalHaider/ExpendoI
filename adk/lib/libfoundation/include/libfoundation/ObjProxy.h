/*
 * ObjProxy.h
 *
 *  Created on: 2009-02-17
 *      Author: Lucjan_B1
 */

#ifndef OBJPROXY_H_
#define OBJPROXY_H_

namespace com_verifone_util
{


template <class HW> class ObjProxy
{
public:

	ObjProxy()
	{
		if (!hwObject)
			hwObject = new HW;
	}
	~ObjProxy()
	{
		//delete hwObject;
	}
	HW& operator*() const
	{
	   return *hwObject;
	}
	HW* operator->() const
	{
	        return hwObject;
	}
private:
	ObjProxy( const ObjProxy &);
	ObjProxy& operator=(const ObjProxy &);
private:
	static HW *hwObject;
};

template<class HW> HW *ObjProxy<HW>::hwObject = 0;

}

#endif /* OBJPROXY_H_ */
