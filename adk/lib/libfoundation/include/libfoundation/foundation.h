#ifndef FOUNDATION_H
#define FOUNDATION_H

#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 *
 * Copyright (C) 2008 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file       foundation.h
 *
 * @author     Maciej 'karp' Karpiuk
 *
 * @brief      generic include for foundation
 */

#ifdef VERIXV
#include <svc.h>
#include <liblog/logsys.h>
#endif
#include "profiler.h"
#include "TextReader.h"
#include "SecuredTextReader.h"
#include "PathComparator.h"
#include "DirScanner.h"
#include "ExclusionFileParser.h"
#include "SecurityFileParser.h"
//#include <ConfigMgr.h>

#endif /* FOUNDATION_H */
