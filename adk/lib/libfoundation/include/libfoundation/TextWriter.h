#ifndef TEXTWRITER_H
#define TEXTWRITER_H

#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 *
 * Copyright (C) 2008 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file       TextWriter.h
 *
 * @author     Rafal Pietruch
 *
 * @brief      Definition of TextWriter class
 */

#ifdef VERIXV
#include <svc.h>
#include <liblog/logsys.h>
#endif
#include <string.h>
#include <stdint.h>

namespace com_verifone_text
{
/**
 * @addtogroup TextReader
 * @{
 */

#define  MAX_LINE_LEN      128


/**
 * @brief        The class implements a TextWriter class, which is used to manipulate text files
 *
 */
class TextWriter
{
   public:

      /**
       * @brief       Constructor
       *
       * @param[in]  filename - file path to be opened
       */
      TextWriter(const char *filename);

      /**
       * @brief       open a new file
       *
       * @param[in]  filename - file path to be opened
       *
       * @return      0 for success / 1 for failure
       */
      int8_t   openFile(const char *filename);

      /**
       * @brief       Tell if a file is currently open
       *
       * @return      1 - open / 0 - not open
       */
      int8_t   fileOpened(void) { return ((file_ptr>=0)?1:0); }

      /**
       * @brief       Read line of text into given buffer
       *
       * @param[out]  target_buf - where to store the text
       * @param[in]   buf_size - max size of the specified buffer [optional]
       *
       * @return      length of read buffer of negative number for an error
       */
      int32_t  writeLine(const char *source_buf);
      int32_t  writeLine(const char *source_buf, size_t buf_size);
      // move one line forward without returning data
      int32_t  writeLine(void);

      /**
       * @brief       closes the file
       */
      void     closeFile();

      /**
       * @brief       Destructor
       */
      ~TextWriter(void);

   protected:
		//buf unused!
      //char     buf[MAX_LINE_LEN];   ///< work buffer [searching etc.]

   private:
      int      file_ptr;            ///< file handle
};


/**
 * @}
 */

}
#endif /* TEXTWRITER_H */
