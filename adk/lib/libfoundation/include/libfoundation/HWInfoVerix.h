/*
 * HWInfoVerix.h
 *
 *  Created on: 2009-02-16
 *      Author: Lucjan_B1
 */

#ifndef HWINFOVERIX_H_
#define HWINFOVERIX_H_

namespace com_verifone_util
{


/**
 * @addtogroup Util
 * @{
 */


class HWInfoVerix
{
public:

		  HWInfoVerix();

		  bool isEthernet();

		  bool isGPRS();

		  bool isWiFi();

		  bool isBluetooth();

		  /**
	       * @brief       return executing HW model type
	       * @return      char buffer
	       */
	      const char *    getHWModel() { return modelNo; }

	      /**
	        * @brief       return enum with terminal type
	      	* @return      terminal type
	      	*/
	      termTYPE getTerminalType()  { return terminalType; }

	      /**
	       * @brief       return OS id string on the executing HW
	       * @return      char buffer
	       */
	      const char *    getOSid() { return eprom+1; }

	      /**
	       * @brief       return RAM size on the executing HW
	       * @return      long [kilobytes]
	       */
	      long            getRAMsize() { return ramSize; }

	      /**
	       * @brief       return FLASH size on the executing HW
	       * @return      long [pixels]
	       */
	      long            getFLASHsize() { return flashSize; }

	      /**
	       * @brief       return FLASH size on the executing HW
	      * @return      long [pixels]
	      */
	     long            getFLASHfree() { return flashFree; }

	     /**
	      * @brief       return display width on the executing HW
	      * @return      long [pixels]
	     */


	      long           getDisplayWidth() { return screenWidth; }

	      /**
	       * @brief       return display height on the executing HW
	       * @return      long [pixels]
	       */
	      long            getDisplayHeight() { return screenHeight; }

	      /**
	       * @brief       return executing HW lifetime
	       * @return      long [seconds]
	       */
	      long            getLifetime();

	      /**
	       * @brief       return serial number of the executing HW
	       * @return      char buffer
	       */
	      const char *    getSerialNumber() { return sNo;}

	      /**
	       * @brief       return PTID of the executing HW
	       * @return      char buffer
	       */
	      const char *    getPTID() { return ptid+1; }

	      /**
	     	      	* @brief       return platform ID
	     	      	* @return      char buffer
	      */
	      const char *    getPlatform() { return "eVo"; }

      /**
        * @brief       return true whether we're on EVO (Trident)
        * @return      true if we're on Evo, false otherwise
        */
        bool weAreEVOTerminal()  
        { 
            if (terminalType == Vx820 || terminalType == Vx680 || terminalType == Vx520 || terminalType == Vx600) 
                return true; 
            return false; 
        }

      /**
        * @brief       return true whether we're on V/OS (Ux, 820, ...)
        * @return      true if we're on V/OS terminal, false otherwise

        */
        bool weAreVOSTerminal()  
        { 
            return false; 
        }

      /**
        * @brief       return true whether we're unattended (for now, this is always false!)
        * @return      true if we're unattended, false otherwise

        */
        bool weAreUnattendedTerminal()
        { 
            return false; 
        }


private:
	termTYPE translateTerminalType(const char *modelNo);

	void detectTerminal();

	int getModemType();

private:
	int deviceType1;
	int deviceType2;
	int	usbDeviceBits;
	long ramSize;
	long flashSize;
	long flashFree;
	long screenWidth;
	long screenHeight;
	char eprom[20];
	char ptid[20];
	char sNo[12];
	char country[13];
	char modelNo[13];
	termTYPE terminalType;

};


}	/* NAMESPACE END */


#endif /* HWINFOVERIX_H_ */
