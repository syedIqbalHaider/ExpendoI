/*
 * HWInfoVOS.h
 *
 *  Created on: 2009-02-16
 *      Author: Lucjan_B1
 */

#ifndef HWINFOVOS_H_
#define HWINFOVOS_H_

namespace com_verifone_util
{


/**
 * @addtogroup Util
 * @{
 */


class HWInfoVOS
{
public:

		  HWInfoVOS();

		  bool isEthernet();

		  bool isGPRS();

		  bool isWiFi();

		  bool isBluetooth();

		  /**
	       * @brief       return executing HW model type
	       * @return      char buffer
	       */
	      const char *    getHWModel() { return modelNo; }

	      /**
	        * @brief       return enum with terminal type
	      	* @return      terminal type
	      	*/
	      termTYPE getTerminalType()  { return terminalType; }

	      /**
	       * @brief       return OS id string on the executing HW
	       * @return      char buffer
	       */
	      const char *    getOSid() { return eprom; }

	      /**
	       * @brief       return RAM size on the executing HW
	       * @return      long [kilobytes]
	       */
	      long            getRAMsize() { return ramSize; }

	      /**
	       * @brief       return FLASH size on the executing HW
	       * @return      long [pixels]
	       */
	      long            getFLASHsize() { return flashSize; }

	      /**
	       * @brief       return FLASH size on the executing HW
	      * @return      long [pixels]
	      */
	     long            getFLASHfree() { return flashFree; }

	     /**
	      * @brief       return display width on the executing HW
	      * @return      long [pixels]
	     */


	      long           getDisplayWidth() { return screenWidth; }

	      /**
	       * @brief       return display height on the executing HW
	       * @return      long [pixels]
	       */
	      long            getDisplayHeight() { return screenHeight; }

	      /**
	       * @brief       return executing HW lifetime
	       * @return      long [seconds]
	       */
	      long            getLifetime();

	      /**
	       * @brief       return serial number of the executing HW
	       * @return      char buffer
	       */
	      const char *    getSerialNumber() { return sNo;}

	      /**
	       * @brief       return PTID of the executing HW
	       * @return      char buffer
	       */
	      const char *    getPTID() { return ptid; }

	      /**
	     	      	* @brief       return platform ID
	     	      	* @return      char buffer
	      */
	      const char *    getPlatform() { return "V/OS"; }

      /**
        * @brief       return true whether we're on EVO (Trident)
        * @return      true if we're on Evo, false otherwise
        */
        bool weAreEVOTerminal()
        { 
            return false; 
        }

      /**
        * @brief       return true whether we're on V/OS (Ux, 820, ...)
        * @return      true if we're on V/OS terminal, false otherwise
        */
        bool weAreVOSTerminal()  
        { 
            if (terminalType == Ux300 || terminalType == Vx820VOS) 
                return true; 
            return false; 

        }

      /**
        * @brief       return true whether we're unattended (Ux300)
        * @return      true if we're unattended, false otherwise

        */
        bool weAreUnattendedTerminal()
        { 
            return terminalType == Ux300;
        }


private:
	termTYPE translateTerminalType(const char *modelNo);

	void detectTerminal();

	int getModemType();

private:
	int deviceType1;
	int deviceType2;
	long ramSize;
	long flashSize;
	long ramFree;
	long flashFree;
	long screenWidth;
	long screenHeight;
	char eprom[20];
	char ptid[20];
	char sNo[12];
	char country[13];
	char modelNo[13];
	termTYPE terminalType;

};


}	/* NAMESPACE END */


#endif /* HWINFOVOS_H_ */
