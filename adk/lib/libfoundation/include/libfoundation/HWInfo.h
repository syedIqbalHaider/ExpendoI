/*
 * HWInfo.h
 *
 *  Created on: 2009-02-17
 *      Author: Lucjan_B1
 */

/* ----------------------------------------------------------- */


#ifndef HWINFO_H_
#define HWINFO_H_

/* ----------------------------------------------------------- */
namespace com_verifone_util
{

/** Terminal type info **/
enum termTYPE
{
		O37xx = 1,O33xx,O36xxG,O36xxP,O36xxC,O36xxM,O51xx, O56xx,
		Vx610, Vx510, Vx570, Vx670, Vx810, Vx700, Vx520, Vx680, Vx820,
		Vx600,
		Ux300, Vx820VOS,
		PROTOTYPE, PCx86, TERM_UNKNOWN
};

}
/* ----------------------------------------------------------- */

#include "ObjProxy.h"
#ifdef VFI_PLATFORM_VERIXEVO
#include "HWInfoVerix.h"
#elif defined VFI_PLATFORM_VOS
#include "HWInfoVOS.h"
#else
#include "HWInfoPC.h"
#endif

namespace com_verifone_util
{

/* ----------------------------------------------------------- */


//Hardware info class
#ifdef VFI_PLATFORM_VERIXEVO
typedef ObjProxy<HWInfoVerix> HWInfo;
#elif defined VFI_PLATFORM_VOS
typedef ObjProxy<HWInfoVOS> HWInfo;
#else
typedef ObjProxy<HWInfoPC> HWInfo;
#endif


/* ----------------------------------------------------------- */
}

#endif /* HWINFO_H_ */
