#ifndef DIRSCANNER_H
#define DIRSCANNER_H

#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 *
 * Copyright (C) 2008 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file       DirScanner.h
 *
 * @author     Maciej 'karp' Karpiuk
 *
 * @brief      Definition of DirScanner class
 */

#ifdef VERIXV
#include <svc.h>
#include <liblog/logsys.h>
#endif
#include <string.h>
#include <stdint.h>
#if defined(POSIX) || defined(__GNUC__)
#include <dirent.h>
#endif
#include "PathComparator.h"

namespace com_verifone_util
{
/**
 * @addtogroup Util
 * @{
 */


/**
 * @brief        The class is implements a DirScanner class, which can be used to retrieve list of files within the directory maching the specified mask
 *
 */
class DirScanner
{
   public:
      /**
       * @brief       Constructor
       *
       * @param[in]   mask - mask to be used to filter files, if no specified wildcard will be used
       */
      DirScanner(void);
      DirScanner(const char *mask);


      /**
       * @brief       operator = - set new mask to retrieved files and move the cursor to the top of the directory
       *
       * @param[in]   mask - mask to be used to filter files, if no specified wildcard will be used
       *
       * @return      none
       */
      void     setMask(const char *mask);

      /**
       * @brief       operator = - same as setMask()
       *
       * @param[in]   mask - mask to be used to filter files, if no specified wildcard will be used
       *
       * @return      none
       */
      DirScanner const &     operator=(const char *mask);


      /**
       * @brief       get new filename matching specified mask
       *
       * @param[in]   target_buf - buffer to store result filename into
       *
       * @return      1 if file has been found / 0 is nothing found
       */
      int8_t   getNext(char *target_buf);

      /**
       * @brief       Destructor
       */
      ~DirScanner(void);
   private:
      PathComparator       compare;    ///< comparator mechanism
      bool                 started;    ///< indicates if scanning process has been started
#if defined(POSIX) || defined(__GNUC__)
      DIR                  *dir;       ///< pointer to the opened directory
#elif  VERIXV
      char                 fbuf[MAX_FILENAME+1];   ///< last taken file
      char                 drive;
      int                  group;
      int                  drive_index;
      bool                 drive_set, group_set;
#endif
};


/**
 * @}
 */

}
#endif /* DIRSCANNER_H */
