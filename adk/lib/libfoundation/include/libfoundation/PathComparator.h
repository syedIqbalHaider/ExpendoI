#ifndef PATHCOMPARATOR_H
#define PATHCOMPARATOR_H

#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 *
 * Copyright (C) 2008 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file       PathComparator.h
 *
 * @author     Maciej 'karp' Karpiuk
 *
 * @brief      Definition of PathComparator class
 */

#ifdef VERIXV
#include <svc.h>
#include <liblog/logsys.h>
#endif
#include <string.h>
#include <stdint.h>

#if defined(POSIX) || defined(__GNUC__)
#define MAX_FILENAME   128
#elif  VERIXV
#define MAX_FILENAME   32
#endif

#define _PH_MAX_BLOCKS         10

struct _path_elem {
   char              *ptr; ///< block pointer
   unsigned char     len;  ///< block length
   unsigned char     wild; ///< is wildcard?
};

namespace com_verifone_util
{
/**
 * @addtogroup Util
 * @{
 */


/**
 * @brief        The class is implements a PathComparator class, which can match filenames to masks with wildcards
 *
 */
class PathComparator
{
   public:
      /**
       * @brief       Constructor
       *
       * @param[in]   mask - mask of pathnames
       */
      PathComparator();
      PathComparator(const char *mask);

      /**
       * @brief       operator = - set new mask in PathComparator
       *
       * @param[in]   new_mask - new mask for PathComparator
       *
       * @return      none
       */
      PathComparator const &  operator=(const char *new_mask);

      /**
       * @brief       operator == - checks if given filename matches the mask
       *
       * @param[in]   fname - compared filename
       *
       * @return      true if matches / false for not match
       */
      bool                    operator==(const char *fname);

      /**
       * @brief       Destructor
       */
      ~PathComparator(void);
   private:
      // cleanup internal mask
      void     cleanup(void);
      // analyze mask
      void     setNewMask(const char *new_mask);

      char     mask[MAX_FILENAME+1];
      int      no_blocks;     ///< how many blocks are specified?
      struct _path_elem elem[_PH_MAX_BLOCKS];
};


/**
 * @}
 */

}
#endif /* PATHCOMPARATOR_H */
