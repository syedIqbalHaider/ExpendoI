#ifndef FOUNDPROFILER_H
#define FOUNDPROFILER_H
#ifdef __cplusplus
extern "C" {
#endif

//
// author: karp systems incorporated :)
//


#if defined(POSIX) || defined(__GNUC__)
#include <sys/time.h>
#endif

#ifdef PROFILER
typedef struct pritem_t* pritem;
struct pritem_t {
   unsigned long     call_cnt;
   unsigned long     eaten_us;
   int               snatch;
#if defined(POSIX) || defined(__GNUC__)
   struct timeval    start;
#elif  VERIXV
   unsigned long     start;
#endif
};


// if no PROFILER_START* method was called in current function, call PROFILER_TOUCH() just before PROFILER_STOP()
// to declare expected variables
#define  PROFILER_TOUCH()           pritem dupa=NULL;

// start measurement for the specified token. Time counter will start since execution of this macro
#define  PROFILER_START(fname)      pritem dupa=profiler_start(fname);
#define  PROFILER_START_(fname)     dupa=profiler_start(fname);
// stop measurement for the specified token. If it was not started before, it will do nothing
#define  PROFILER_STOP(fname)       { profiler_stop(dupa, fname); dupa=NULL; }

// init profiler
int            profiler_new(unsigned long log_step);
// init profiler with limited tree level (functions deeper than max_depth profiler calls won't be measured)
int            profiler_new_with_max_depth(  unsigned long log_step,
                                             unsigned long max_depth );
// cleanup the profiler
void           profiler_destroy(void);
// tell if profiler execution count exceeded log_step given at initialisation
int            profiler_is_display_ready(void);
// dump the profiler contents
void           profiler_dump(void);



// use macro instead for the below
pritem         profiler_start(const char *fname);
unsigned long  profiler_stop(pritem fptr, const char *fname);
unsigned long  profiler_stop_ptr(pritem fptr);

#else

// dummies if no profiler was compiled
#define  PROFILER_TOUCH()           { }
#define  PROFILER_START(fname)      { }
#define  PROFILER_START_(fname)     { }
#define  PROFILER_STOP(fname)       { }
#define profiler_new(a) { }
#define profiler_new_with_max_depth(a, b) { }
#define profiler_destroy(a)  { }
#define profiler_start(a) { }
#define profiler_stop(a, b) { }
#define profiler_stop_ptr(a) { }
#define profiler_is_display_ready() 0
#define profiler_dump(a) { }
#endif


#ifdef __cplusplus
}
#endif
#endif
