#ifndef SECURITYPARSER_H
#define SECURITYPARSER_H

#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 *
 * Copyright (C) 2008 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file       SecurityFileParser.h
 *
 * @author     Maciej 'karp' Karpiuk
 *
 * @brief      Definition of SecurityFileParser class
 */

#ifdef VERIXV
#include <svc.h>
#include <liblog/logsys.h>
#endif
#include <string.h>
#include <stdint.h>
#include "libfoundation/SecuredTextReader.h"
#include "libfoundation/PathComparator.h"

using namespace com_verifone_text;

namespace com_verifone_util
{
/**
 * @addtogroup Util
 * @{
 */


/**
 * @brief        The class is implements a SecurityFileParser class, which can check if files specified in the security file are all authenticated
 *
 */
class SecurityFileParser
{
   public:
      /**
       * @brief       Constructor
       *
       * @param[in]   filename - security config file path
       */
      SecurityFileParser(const char *filename);

      /**
       * @brief       perform full check of files in the security list
       *
       * @param[out] output_buf - buffer to store faulty file name into
       *
       * @return      1 if is successful / 0 if is not successful
       */
      int8_t   performCheck(void);
      int8_t   performCheck(char *output_buf);

      /**
       * @brief       operator bool - checks if security config file has been properly opened and if security went successfully through system security check
       *
       * @return      1 if successful / 0 if is not successful
       */
      operator bool() { return (textreader.fileOpened()==1); }

   private:
      SecuredTextReader    textreader; ///< config file handle (authentication needed)
};


/**
 * @}
 */

}
#endif /* SECURITYPARSER_H */
