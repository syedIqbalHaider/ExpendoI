#ifndef _DLOGGER_H
#define _DLOGGER_H	1

#include <stdio.h>
//#include <unistd.h>
//#include <stdarg.h>
//#include <syslog.h>

#ifdef __cplusplus
extern "C" {
#endif /* ndef __cplusplus */

#define LOG_MSG			0
#define LOG_WARN			1
#define LOG_ERROR			2
#define LOG_FATAL			3
#define LOG_ENTER			4
#define LOG_LEAVE			5


#ifdef __MODULE__
# define LOG_FILE_NAME	__MODULE__
#else
# define LOG_FILE_NAME	__FILE__
#endif

void vLogInit (const char *appName);
void vLogDeInit ();
void vLogMessage (int logtype, char const * FileName, int LineNum, char const *Format, ...);
void vLogHEXBuffer(char const * FileName, int LineNum, void const * in_buf, int num_bytes, const char *title);
//void vLogHEXData (char const *title, char const * FileName, int LineNum, void const * in_bufRaw, int offset, int num_bytes, int ascii_rep);
char const * szGet_dlogger_Name(void);
char const * szGet_dlogger_Version(void);
#define dlog_init(a)                                        if (1) { vLogInit(a);} else (void) 0
#define dlog_deinit                                           vLogDeInit
#ifdef DEBUG
#define dlog_msg(...)                                       if (1) { vLogMessage(LOG_MSG, LOG_FILE_NAME, __LINE__, __VA_ARGS__);} else (void) 0
#define dlog_error(...)                                     if (1) { vLogMessage(LOG_ERROR, LOG_FILE_NAME, __LINE__, __VA_ARGS__);} else (void) 0
#define dlog_alert(...)                                     if (1) { vLogMessage(LOG_WARN, LOG_FILE_NAME, __LINE__, __VA_ARGS__);} else (void) 0
#define dlog_fatal(...)                                     if (1) { vLogMessage(LOG_FATAL, LOG_FILE_NAME, __LINE__, __VA_ARGS__);} else (void) 0
#define dlog_hex(in_buf, num_bytes, title)                if (1) { vLogHEXBuffer(LOG_FILE_NAME, __LINE__, in_buf, num_bytes, title); } else (void) 0
//#define dlog_hexdata(in_buf, offset, num_bytes, ascii_rep) 	if (1) { vLogHEXData("", LOG_FILE_NAME, __LINE__, in_buf, offset, num_bytes, ascii_rep); } else (void) 0

#define dlog_enter(ClassName, ...)                          if (1) { vLogMessage(LOG_ENTER, LOG_FILE_NAME, __LINE__, __VA_ARGS__); } else (void) 0
#define dlog_leave(ClassName, ...)                          if (1) { vLogMessage(LOG_LEAVE, LOG_FILE_NAME, __LINE__, __VA_ARGS__); } else (void) 0
#else

#define dlog_msg(...)                                       do {} while(0)
#define dlog_error(...)                                     if (1) { vLogMessage(LOG_ERROR, LOG_FILE_NAME, __LINE__, __VA_ARGS__);} else (void) 0
#define dlog_alert(...)                                     if (1) { vLogMessage(LOG_WARN, LOG_FILE_NAME, __LINE__, __VA_ARGS__);} else (void) 0
#define dlog_fatal(...)                                     do {} while(0)
#define dlog_hex(in_buf, num_bytes, title)                  do {} while(0)
//#define dlog_hexdata(in_buf, offset, num_bytes, ascii_rep) 	if (1) { vLogHEXData("", LOG_FILE_NAME, __LINE__, in_buf, offset, num_bytes, ascii_rep); } else (void) 0

#define dlog_enter(ClassName, ...)                          do {} while(0)
#define dlog_leave(ClassName, ...)                          do {} while(0)

#endif


#ifdef __cplusplus
}
#endif

#endif // _DLOGGER_H
