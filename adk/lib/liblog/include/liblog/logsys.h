 /******************************************************************
  Filename:	    logsys.h
  Product:      Multiple Application Manager
  Version:      2.0
  Module:       LOGSYS
  Description:	interface file for LOGSYS API
  
  Modification History:
      #       Date         Who         Comments
#   Date    	Who     			Comments
1   22-Feb-00   Bruce Girdlestone	Initial Draft.                
2. 11-May-2006  Vijay Kumar Jitta	ATS issue 398 fixed.
*******************************************************************/
/******************************************************************
   Copyright (C) 2000-2006 by VeriFone Inc. All rights reserved.

 No part of this software may be used, stored, compiled, reproduced,
 modified, transcribed, translated, transmitted, or transferred, in
 any form or by any means  whether electronic, mechanical,  magnetic,
 optical, or otherwise, without the express prior written permission
                          of VeriFone, Inc.
*******************************************************************/

#ifndef _LOGSYS
#define _LOGSYS	1

#ifndef TRUE
# define TRUE        1
#endif
#ifndef FALSE
# define FALSE       0
#endif

#include <stddef.h>		// Because of NULL use

#ifdef VERIXV

#ifndef LOGSYS_USE_ELOG

#ifdef __cplusplus
extern "C" {
#endif /* ndef __cplusplus */

#ifdef LOGSYS_FLAG

#ifdef __MODULE__
# define LOG_FILE_NAME	__MODULE__
#else
# define LOG_FILE_NAME	__FILE__
#endif

/* Logsys include file */

/* Different logging mechanisms */
#define LOGSYS_NONE				0
#define LOGSYS_FILE				1
#define LOGSYS_COMM				2
#define LOGSYS_COMM_FILE		3
/*
Add a pipe functionality as a new method of channelling debug information
*/ 
#define LOGSYS_PIPE				4 //BRUCE_G1: WWAF ENHANCEMENT 02/15/06


#define LOGSYS_PRINTF_FILTER		 0x00000400L

/* Logfile structure */
typedef struct LOGFILE_HEADER
{
	short  maxsize;
	short  offset;
}
PFR_LOGFILE_HEADER_STR;


/* Logfile size range */
#define PFR_CUST_MAX_LOGFILE_SIZE 15360 
#define PFR_CUST_MIN_LOGFILE_SIZE 500
#define PFR_CUST_DEF_LOGFILE_SIZE 15360


#define PFR_DATE_SIZE 9
#define PFR_TIME_SIZE 7


#define POWER_FAIL_STAT_TABLE "PFR.DAT"

#define CRITICAL_ERROR			1
#define NON_CRITICAL_ERROR		2
#define NULL_POINTER_ERROR		3
#define LOG_MESSAGE				4
#define LOG_MESSAGE_FILTER		5


#ifndef MEMCLR
#define MEMCLR(addr,len) (memset (addr, 0x00, len))
#endif

#ifdef __cplusplus
extern "C"
{
#endif

void LOG_INIT(const char * appName,short  logType,unsigned long filter);
void vPfrStoreErrorMessage(char const * message);
void vLogMessage (char const * pchFormat,...);
void vLogMessageFilter (unsigned long filter, char const * pchFormat,...);
void vEnterMethod(char const * FileName, int LineNum, char const * ClassName, char const *Format, ...);
void vLogMsg(char const *FileName, int LineNum, char const *Format, ...);
void vLeaveMethod(char const * FileName, int LineNum, char const * ClassName, char const *Format, ...);
void vErrorMethod(char const * FileName, int LineNum, char const *Format, ...);
void vAlertMethod(char const * FileName, int LineNum, char const *Format, ...);
void vLogHEXBuffer(char const * FileName, int LineNum, void const * in_buf, int num_bytes, const char *title);
void vLogHEXData (char const * FileName, int LineNum, void const * in_buf, int offset, int num_bytes, int ascii_rep);

void LOG_HEX_Buf ( unsigned char *in_buf, int num_bytes, const char *title );

void vLogError(long lnError, const char * pchFile, short  inLineNumber, short  inErrorType, unsigned long filter);
void vLogComm(const char * message,short shLen);

char const * GetLogsysName(void);
char const * GetLogsysVersion(void);

#ifdef __cplusplus
}
#endif

/****************************************************************************************
 * Default Corp Macros
 *
 * LOG_NONZERO_ERROR(a,b) 		: Error logging MACRO
 *		Use this to log the File, and Line number where a nonzero error has occured
 *
 * LOG_NEGATIVE_ERROR(a,b) 		: Error logging MACRO
 *		Use this to log the File, and Line number where a negative error has occured 
 *
 * LOG_ZERO_ERROR(a,b) 			: Error logging MACRO
 *      Use this to log a zero error response, i.e. Zero bytes were returned from 
 *		a file/comm read
 *
 * LOG_NONZERO_ERROR_CRIT(a,b) 	: Error logging MACRO
 *		Use this to log the File, and Line number where a nonzero error has occured
 *
 * LOG_NEGATIVE_ERROR_CRIT(a,b)	: Error logging MACRO 
 *		Use this to log the File, and Line number where a negative error has occured
 *
 * LOG_ZERO_ERROR_CRIT(a,b)		: Error logging MACRO
 *		Use this to log a zero error response, i.e. Zero bytes were returned from a 
 *		file/comm read
 *
 * LOG_NULL_POINTER(a,b)		: Error logging MACRO
 *		Use this to check for null pointer errors
 *
 * LOG_PRINTF(a)
 * LOG_PRINTFF(a)
 * 		Log using printf format
 ****************************************************************************************/

#define LOG_NONZERO_ERROR(a,b)	 							if (a != 0) {vLogError((long) a,__FILE__,__LINE__,NON_CRITICAL_ERROR,b);}
#define LOG_NEGATIVE_ERROR(a,b)	 							if (a < 0) {vLogError((long) a,__FILE__,__LINE__,NON_CRITICAL_ERROR,b);}
#define LOG_ZERO_ERROR(a,b)	     							if (a == 0) {vLogError((long) a,__FILE__,__LINE__,NON_CRITICAL_ERROR,b);}
#define LOG_NONZERO_ERROR_CRIT(a,b)							if (a != 0) {vLogError((long) a,__FILE__,__LINE__,CRITICAL_ERROR,b);}
#define LOG_NEGATIVE_ERROR_CRIT(a,b)						if (a < 0) {vLogError((long) a,__FILE__,__LINE__,CRITICAL_ERROR,b);}
#define LOG_ZERO_ERROR_CRIT(a,b)	 						if (a == 0) {vLogError((long) a,__FILE__,__LINE__,CRITICAL_ERROR,b);}
#define LOG_NULL_POINTER(a,b)	 							if ((char *) a == NULL) {vLogError((long) a,__FILE__,__LINE__,NULL_POINTER_ERROR,b);}
#define LOG_PRINTF(a)										{ vLogMessage a; vLogError((long) 0,__FILE__,__LINE__,LOG_MESSAGE,0xFFFFFFFF); }
#define LOG_PRINTFF(a)										{ vLogMessageFilter a; vLogError((long) 0,__FILE__,__LINE__,LOG_MESSAGE_FILTER,0xFFFFFFFF); }

#define dlog_init(a)                                        if (1) { LOG_INIT( (char*)a, LOGSYS_PIPE, LOGSYS_PRINTF_FILTER );;} else (void) 0
#define dlog_deinit() do {} while(0)
#define dlog_msg(...)										if (1) { vLogMsg(LOG_FILE_NAME, __LINE__, __VA_ARGS__);} else (void) 0
#define dlog_enter(ClassName, ...)	                     	if (1) { vEnterMethod(LOG_FILE_NAME, __LINE__, ClassName, __VA_ARGS__); } else (void) 0
#define dlog_leave(ClassName, ...)	                     	if (1) { vLeaveMethod(LOG_FILE_NAME, __LINE__, ClassName, __VA_ARGS__); } else (void) 0
#define dlog_error(...)	                                 	if (1) { vErrorMethod(LOG_FILE_NAME, __LINE__, __VA_ARGS__); } else (void) 0
#define dlog_alert(...)	                                 	if (1) { vAlertMethod(LOG_FILE_NAME, __LINE__, __VA_ARGS__); } else (void) 0
#define dlog_hex(in_buf, num_bytes, title)                 	if (1) { vLogHEXBuffer(LOG_FILE_NAME, __LINE__, in_buf, num_bytes, title); } else (void) 0
#define dlog_hexdata(in_buf, offset, num_bytes, ascii_rep) 	if (1) { vLogHEXData(LOG_FILE_NAME, __LINE__, in_buf, offset, num_bytes, ascii_rep); } else (void) 0

#ifdef LOGSYS_USE_LOG_MACRO
/******************************************************
 * Deprecated log macros 
 *                !!! DO NOT USE !!!
 *
 * NOTE:
 *      log() macro clashes with math.h log() function.
 *      Use dlog_XXX macros whenever possible and only
 *      for backward compatibility you can define 
 *      LOGSYS_USE_LOG_MACRO
 ******************************************************/
#define log(...)    	                                                     { vLogMessage(__VA_ARGS__); vLogError((long) 0,__FILE__,__LINE__, LOG_MESSAGE, 0xFFFFFFFF); }
#define log_msg(FileName, LineNum, ...)										 { vLogMsg(FileName, LineNum, __VA_ARGS__);}
#define log_enter(FileName, LineNum, ClassName, ...)	                     { vEnterMethod(FileName, LineNum, ClassName, __VA_ARGS__); }
#define log_leave(FileName, LineNum, ClassName, ...)	                     { vLeaveMethod(FileName, LineNum, ClassName, __VA_ARGS__); }
#define log_error(FileName, LineNum, ...)	                                 { vErrorMethod(FileName, LineNum, __VA_ARGS__); }
#define log_alert(FileName, LineNum, ...)	                                 { vAlertMethod(FileName, LineNum, __VA_ARGS__); }
#define log_hex(FileName, LineNum, in_buf, num_bytes, title)                 { vLogHEXBuffer(FileName, LineNum, in_buf, num_bytes, title); }
#define log_hexdata(FileName, LineNum, in_buf, offset, num_bytes, ascii_rep) { vLogHEXData(FileName, LineNum, in_buf, offset, num_bytes, ascii_rep); }
#endif /*ifdef LOGSYS_LOG_MACRO */

#else	// LOGSYS_FLAG not defined
/******************************************************
 * If LOGSYS_FLAG is NOT defined, then do nothing
 ******************************************************/
#define LOG_INIT(a,b,c)							do {} while(0)
#define vPfrStoreErrorMessage(a)				do {} while(0)
#define vLogMessage (a,...)						do {} while(0)
#define vLogMessageFilter (a,b,...)				do {} while(0)
#define vLogMsg(a, b, c, ...)					do {} while(0)
#define vLogError(a,b,c,d,e)					do {} while(0)
#define vEnterMethod(a, b, c, d, ...)			do {} while(0)
#define vLeaveMethod(a, b, c, d, ...)			do {} while(0)
#define vErrorMethod(a, b, c, ...)				do {} while(0)
#define vAlertMethod(a, b, c, ...)				do {} while(0)
#define vLogHEXBuffer(a, b, c, d, e)			do {} while(0)
#define vLogHEXData(a, b, c, d, e, f)			do {} while(0)

#define LOG_NONZERO_ERROR(a,b)					do {} while(0)
#define LOG_NEGATIVE_ERROR(a,b)					do {} while(0)
#define LOG_ZERO_ERROR(a,b)						do {} while(0)
#define LOG_NONZERO_ERROR_CRIT(a,b)				do {} while(0)
#define LOG_NEGATIVE_ERROR_CRIT(a,b)			do {} while(0)
#define LOG_ZERO_ERROR_CRIT(a,b)				do {} while(0)
#define LOG_NULL_POINTER(a,b)					do {} while(0)
#define LOG_PRINTF(a)							do {} while(0)
#define LOG_PRINTFF(a)							do {} while(0)

#define dlog_init(a)                              do {} while(0)
#define dlog_deinit()                             do {} while(0)
#define dlog_msg(...)							do {} while(0)
#define dlog_enter(ClassName, ...)				do {} while(0)
#define dlog_leave(ClassName, ...)				do {} while(0)
#define dlog_error(...)							do {} while(0)
#define dlog_alert(...)							do {} while(0)
#define dlog_hex(in_buf, num_bytes, title)		do {} while(0)
#define dlog_hexdata(in_buf, offset, num_bytes, ascii_rep)		do {} while(0)

#ifdef LOGSYS_USE_LOG_MACRO
/******************************************************
 * Deprecated log macros 
 *                !!! DO NOT USE !!!
 *
 * NOTE:
 *      log() macro clashes with math.h log() function.
 *      Use dlog_XXX macros whenever possible and only
 *      for backward compatibility you can define 
 *      LOGSYS_USE_LOG_MACRO
 ******************************************************/
#define log(...)
#define log_msg(FileName, LineNum, Format, ...)
#define log_enter(FileName, LineNum, ClassName, Format, ...)
#define log_leave(FileName, LineNum, ClassName, Format, ...)
#define log_error(FileName, LineNum, Format, ...)
#define log_alert(FileName, LineNum, Format, ...)
#define log_hex(FileName, LineNum, in_buf, num_bytes, title)
#define log_hexdata(FileName, LineNum, in_buf, offset, num_bytes, ascii_rep)
#endif

#endif	// of LOGSYS_FLAG

#ifdef __cplusplus
}
#endif /* ndef __cplusplus */

#else /* LOGSYS_USE_ELOG  */

#  define LOGSYS_NEW_API_STYLE
#ifdef __cplusplus
extern "C" {
#endif /* ndef __cplusplus */
//FIXME:: Temporary workarround for no const in eoslog.h
#  include "fix/eoslog.h"

#define dlog_init(a)                                        if (1) { LOG_INIT( (char*)a, LOGSYS_PIPE, LOGSYS_PRINTF_FILTER );;} else (void) 0
#define dlog_deinit()  do {} while (0)
#define dlog_msg(...)										if (1) { LOG_PRINTF(__VA_ARGS__);} else (void) 0
//#define dlog_enter(ClassName, ...)	                     	if (1) { vEnterMethod(LOG_FILE_NAME, __LINE__, ClassName, __VA_ARGS__); } else (void) 0
#define dlog_enter(ClassName, ...)							if (1) { LOG_PRINTF_MOD("ENTER", __VA_ARGS__); } else (void) 0
//#define dlog_leave(ClassName, ...)	                     	if (1) { vLeaveMethod(LOG_FILE_NAME, __LINE__, ClassName, __VA_ARGS__); } else (void) 0
#define dlog_leave(ClassName, ...)	                     	if (1) { LOG_PRINTF_MOD("LEAVE", __VA_ARGS__);} else (void) 0
#define dlog_error(...)	                                 	if (1) { LOG_PRINTF_MOD("ERROR", __VA_ARGS__);} else (void) 0
#define dlog_alert(...)	                                 	if (1) { LOG_PRINTF_MOD("ALERT", __VA_ARGS__);} else (void) 0
//FIXME: Temporary disabled it crashes on longer buffer
#define dlog_hex(in_buf, num_bytes, title)                 	if (1) { LOG_HEX_PRINTF(title, in_buf, num_bytes); } else (void) 0
//#define dlog_hexdata(in_buf, offset, num_bytes, ascii_rep) 	if (1) { vLogHEXData(LOG_FILE_NAME, __LINE__, in_buf, offset, num_bytes, ascii_rep); } else (void) 0
#define GetLogsysVersion() "Elog"
#define GetLogsysName() "elogsys"

#ifndef TRUE
#define TRUE        1
#endif
#ifndef FALSE
#define FALSE       0
#endif
#ifdef __cplusplus
}
#endif

#endif /* LOGSYS_USE_ELOG  */

#else // VERIXV

#include "dlogger.h"

#endif

#ifdef __cplusplus
struct dlog_scope
{
  dlog_scope(const char* name) { (void)name; dlog_init(name); }
  ~dlog_scope() { dlog_deinit(); }
};
#endif

#endif
