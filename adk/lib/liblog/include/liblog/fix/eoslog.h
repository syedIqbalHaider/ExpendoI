//----------------------------------------------------------------------------
//
// eoslog.h - definitions and prototypes for the Vx EOS EOSLog library.
//
// This library builds upon the Vx OS SysLog features to provide applications 
// with "LogSys-type" functionality.    
//
//----------------------------------------------------------------------------
	/******************************************************************
	   Copyright (C) 2009-2010 by VeriFone Inc. All rights reserved.

	 No part of this software may be used, stored, compiled, reproduced,
	 modified, transcribed, translated, transmitted, or transferred, in
	 any form or by any means  whether electronic, mechanical,  magnetic,
	 optical, or otherwise, without the express prior written permission
							  of VeriFone, Inc.
	*******************************************************************/


#ifndef _EOSLOG_H
#define _EOSLOG_H

// Following log mechanism values are defined for LOGSYS backwards compatibility
// These are all treated as LOGSYS_OS
#define LOGSYS_FILE             1
#define LOGSYS_COMM             2
#define LOGSYS_COMM_FILE        3
#define LOGSYS_PIPE             4 

// Error codes
#define LOGSYS_NOT_INIT			-2	// returned if LOGSYS_SET_FILTER/TYPE is called before INIT

// If compiling with logging enabled, define constants and prototypes
#ifdef LOGSYS_FLAG

#define MAX_LOGSYS_APPNAME_LENGTH 10  //max appname length
#define MAX_LOGSYS_FILENAME_LENGTH 19 // max filename length
#define MAX_LOGSYS_MODULE_LENGTH 12 // max module length
#define FIXED_FORMAT_LENGTH 12 //fixed format characters including line number
// Maximum message length is actually 2 greater than this value.   But the other bytes are \r\n.
#define MAX_MESSAGE_LENGTH		173 
// Supported log mechanisms
#define LOGSYS_NONE             0
#define LOGSYS_OS				1

// Message Types
#define CRITICAL_ERROR          1
#define NON_CRITICAL_ERROR      2
#define NULL_POINTER_ERROR      3
#define LOG_MESSAGE             4
#define LOG_MESSAGE_FILTER      5

//-----------------------------------------------------------------------------------------------
// Following are maintained for backwards compatibility but not referenced by the SYSLOG library
#define LOGSYS_PRINTF_FILTER         0x00000400L

// Logfile structure 
typedef struct LOGFILE_HEADER
{
    short  maxsize;
    short  offset;
} PFR_LOGFILE_HEADER_STR;

// Logfile size range 
#define PFR_CUST_MAX_LOGFILE_SIZE 15360
#define PFR_CUST_MIN_LOGFILE_SIZE 500
#define PFR_CUST_DEF_LOGFILE_SIZE 15360

#define PFR_DATE_SIZE 9
#define PFR_TIME_SIZE 7

#define POWER_FAIL_STAT_TABLE "PFR.DAT"
// End unused typedefs & #defines 
//---------------------------------------------------------------------------------------------

// SysLog APIs
#ifdef __cplusplus
extern "C"
{
#endif

void EOSLOG_INIT(const char * appName,short  logType,unsigned long filter);

int LOG_SET_TYPE(short logType);
int LOG_SET_FILTER(unsigned long filter);
int LOG_GET_TYPE(void);
long LOG_GET_FILTER(void);
char * GetEOSLogVersion(void);

//--- These APIs should only be used through the SYSLOG macros
void sysLogMessage(const char* pchModule, const char * pchFile, short inLineNumber, unsigned long filter, const char* pchFormat, ...);
void sysLogError(long lnError,const char* pchModule, const char * pchFile, short  inLineNumber, short  inErrorType);
void sysLogStoreLoc(const char * pchFile, short inLineNumber);
void sysLogFormat(const char * format,...);
void sysLogFormatFilter(unsigned long filter, const char * format,...);
void sysLogFormatModule(const char* module, const char * format,...);
void sysLogFormatModuleFilter(const char* module, unsigned long filter, const char * format,...);
void sysLog_hex_printf(const char *file_name,unsigned int line_number,const char  * tag, const void*data , unsigned int length);
void sysLogHexPrintfFilter(const char* module, unsigned long filter,const char *file_name,unsigned int line_number,const char  * tag, const char*data , unsigned int length);
//--- End APIs used by macros 

#ifdef __cplusplus
}
#endif

#define LOG_INIT 	EOSLOG_INIT

// Error logging - Use this to log the File, and Line number where a nonzero error has occured 
#define LOG_NONZERO_ERROR(a,notUsed) if (a != 0) {sysLogError((long)a, NULL,__MODULE__, __LINE__, NON_CRITICAL_ERROR);}
#define LOG_NONZERO_ERROR_MOD(m,a)   if (a != 0) {sysLogError((long)a, m,   __MODULE__, __LINE__, NON_CRITICAL_ERROR);}

// Error logging - Use this to log the File, and Line number where a negative error has occured 
#define LOG_NEGATIVE_ERROR(a,notUsed) if (a < 0) {sysLogError((long)a, NULL, __MODULE__, __LINE__, NON_CRITICAL_ERROR);}
#define LOG_NEGATIVE_ERROR_MOD(m,a)   if (a < 0) {sysLogError((long)a, m,    __MODULE__, __LINE__, NON_CRITICAL_ERROR);}

// Error logging - Use this to log a zero error response, i.e. Zero bytes were returned from a file/comm read 
#define LOG_ZERO_ERROR(a,notUsed)  if (a == 0) {sysLogError((long)a, NULL, __MODULE__, __LINE__, NON_CRITICAL_ERROR);}
#define LOG_ZERO_ERROR_MOD(m,a)    if (a == 0) {sysLogError((long)a, m,    __MODULE__, __LINE__, NON_CRITICAL_ERROR);}

// Error logging - Use this to log the File, and Line number where a nonzero error has occured 
#define LOG_NONZERO_ERROR_CRIT(a,notUsed) if (a != 0) {sysLogError((long)a, NULL, __MODULE__, __LINE__, CRITICAL_ERROR);}
#define LOG_NONZERO_ERROR_CRIT_MOD(m,a)   if (a != 0) {sysLogError((long)a, m,    __MODULE__, __LINE__, CRITICAL_ERROR);}

// Error logging  - Use this to log the File, and Line number where a negative error has occured 
#define LOG_NEGATIVE_ERROR_CRIT(a,notUsed) if (a < 0) {sysLogError((long) a, NULL, __MODULE__, __LINE__,CRITICAL_ERROR);}
#define LOG_NEGATIVE_ERROR_CRIT_MOD(m,a)   if (a < 0) {sysLogError((long) a, m,    __MODULE__, __LINE__,CRITICAL_ERROR);}

// Error logging - Use this to log a zero error response, i.e. Zero bytes were returned from a file/comm read 
#define LOG_ZERO_ERROR_CRIT(a,notUsed)   if (a == 0) {sysLogError((long)a, NULL, __MODULE__, __LINE__, CRITICAL_ERROR);}
#define LOG_ZERO_ERROR_CRIT_MOD(m,a)     if (a == 0) {sysLogError((long)a, m,    __MODULE__, __LINE__, CRITICAL_ERROR);}

// Error logging  - Use this to check for null pointer errors
#define LOG_NULL_POINTER(a,notUsed)  if ((char *) a == NULL) {sysLogError((long)a, NULL, __MODULE__, __LINE__, NULL_POINTER_ERROR);}
#define LOG_NULL_POINTER_MOD(m,a)    if ((char *) a == NULL) {sysLogError((long)a, m,    __MODULE__, __LINE__, NULL_POINTER_ERROR);}

//Hex logging -  Use this to log in hex format
#define LOG_HEX_PRINTF(a,b,c) 	sysLog_hex_printf(__MODULE__,__LINE__,a,b,c)
#ifndef LOGSYS_NO_PRINTFF
		#define LOG_HEX_PRINTFF(f,s,addr,size)  { sysLogHexPrintfFilter(NULL,f,__MODULE__,__LINE__,s,addr,size); }
		#define LOG_HEX_PRINTFF_MOD(m,f,s,addr,size)  { sysLogHexPrintfFilter(m,f,__MODULE__,__LINE__,s,addr,size); }
#else
		#define LOG_HEX_PRINTFF(f,s,addr,size)
		#define LOG_HEX_PRINTFF_MOD(m,f,s,addr,size)  
#endif

//Conditional logging
#define LOG_PRINTF_IF(a,b,...)  if ( a ) { sysLogMessage(NULL,__MODULE__,__LINE__,0,b,##__VA_ARGS__); }
#ifndef LOGSYS_NO_PRINTFF
	#define LOG_PRINTFF_IF(f,a,b,...)  if ( a ) { sysLogMessage(NULL,__MODULE__,__LINE__,f,b,##__VA_ARGS__); }
	#define LOG_PRINTFF_IF_MOD(m,f,a,b,...)  if ( a ) { sysLogMessage(m,__MODULE__,__LINE__,f,b,##__VA_ARGS__); }
#else
	#define LOG_PRINTFF_IF(f,a,b,...) 
	#define LOG_PRINTFF_IF_MOD(m,f,a,b,...)
#endif

// Log using printf format macros - new style allows arguments to be passed with a single set of parenthesis.  
// For compatibility reasons, the default macros require double sets of parenthesis.  Example:
// Default style : LOG_PRINTF(("Test %d",i));
// New style : LOG_PRINTF("Test %d",i);
#ifdef LOGSYS_NEW_API_STYLE
	#define LOG_PRINTF(a,...)   { sysLogMessage(NULL,__MODULE__,__LINE__,0,a,##__VA_ARGS__); }
	#define LOG_PRINTF_MOD(m,a,...)   { sysLogMessage(m,__MODULE__,__LINE__,0,a,##__VA_ARGS__); }

	#ifndef LOGSYS_NO_PRINTFF
		#define LOG_PRINTFF(f,a,...)  { sysLogMessage(NULL,__MODULE__,__LINE__,f,a,##__VA_ARGS__); }
		#define LOG_PRINTFF_MOD(m,f,a,...)  { sysLogMessage(m,__MODULE__,__LINE__,f,a,##__VA_ARGS__); }
	#else
		#define LOG_PRINTFF(...)
		#define LOG_PRINTFF_MOD(...)
	#endif

#else
	#define LOG_PRINTF(a)   { sysLogStoreLoc(__MODULE__,__LINE__); sysLogFormat a; }
	#define LOG_PRINTF_MOD(a)   { sysLogStoreLoc(__MODULE__,__LINE__); sysLogFormatModule a;}

	#ifndef LOGSYS_NO_PRINTFF
		#define LOG_PRINTFF(a)  { sysLogStoreLoc(__MODULE__,__LINE__); sysLogFormatFilter a; }
		#define LOG_PRINTFF_MOD(a)  { sysLogStoreLoc(__MODULE__,__LINE__); sysLogFormatModuleFilter a;}
	#else
		#define LOG_PRINTFF(...)
		#define LOG_PRINTFF_MOD(...)
	#endif
#endif

#else   // LOGSYS_FLAG not defined
	// Define all macros and APIs are null statements
	#define LOG_INIT(a,b,c)
	#define LOG_SET_TYPE(a)      0
	#define LOG_SET_FILTER(b)    0
	#define LOG_GET_TYPE()       0
	#define LOG_GET_FILTER()     0
	#define LOG_HEX_PRINTF(a,b,c)
	#define LOG_HEX_PRINTFF(f,s,addr,size)
	#define LOG_HEX_PRINTFF_MOD(m,f,s,addr,size)  
	#define LOG_NONZERO_ERROR(a,b)
	#define LOG_NEGATIVE_ERROR(a,b)
	#define LOG_ZERO_ERROR(a,b)
	#define LOG_NONZERO_ERROR_CRIT(a,b)
	#define LOG_NEGATIVE_ERROR_CRIT(a,b)
	#define LOG_ZERO_ERROR_CRIT(a,b)
	#define LOG_NULL_POINTER(a,b)
	#define LOG_PRINTF(a,...)
	#define LOG_PRINTFF(...)
	#define LOG_NONZERO_ERROR_MOD(a,b)
	#define LOG_NEGATIVE_ERROR_MOD(a,b)
	#define LOG_ZERO_ERROR_MOD(a,b)
	#define LOG_NONZERO_ERROR_CRIT_MOD(a,b)
	#define LOG_NEGATIVE_ERROR_CRIT_MOD(a,b)
	#define LOG_ZERO_ERROR_CRIT_MOD(a,b)
	#define LOG_NULL_POINTER_MOD(a,b)
	#define LOG_PRINTF_MOD(...)
	#define LOG_PRINTFF_MOD(...)
	#define GetEOSLogVersion()
	#define LOG_PRINTF_IF(a,b,...)
	#define LOG_PRINTFF_IF(f,a,b,...) 
	#define LOG_PRINTFF_IF_MOD(m,f,a,b,...) 
#endif  // LOGSYS_FLAG

#endif	//_EOSLOG_H
