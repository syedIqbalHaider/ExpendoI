/*
 * dlogger.cpp
 *
 *  Created on: 30 Nov 2012
 *	  Author: Nick_T1
 */
#include "dlogger.h"

#include <sys/file.h>
#include <cstdlib>
#include <time.h>
#include <stdio.h>
#include <string.h>

#include <unistd.h>
#include <stdarg.h>
#include <syslog.h>

#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <sys/time.h>
#include <sys/stat.h>

#define MAX_BUFFER_LENGTH 8000
const size_t MAX_MESSAGE_LENGTH = 256 - 64;	// NOTE: Every text is prepended with something like "<15>Sep 29 15:57:58 CICAPP[936]: " - this has len=33.
											// 64 is to have some safety buffer for longer app name
#ifdef DEBUG
static const char LOGDEV = 'S';
#else
static const char LOGDEV = 'T';
#endif
const static unsigned long MAX_FILESIZE = 1000000;

static char fileName[4096];
static char backupFileName[4096];
static bool isFileNameSet = false;

// S - syslog
// T - /tmp/vfi.log
// C - /dev/ttyS0
// O - stderr

static unsigned int position = 0;
static int fd1 = -1;
char AppName[128];

void vSyslogPush(int priority, char etype, const char * pchFile, int inLineNumber, char * busfer);
void vdDumpFmtString(void const * in_bufRaw, int offset, int num_bytes, int ascii_rep);

void setFileName() {
	fileName[sizeof(fileName)-1] = '\0';
	strncpy(fileName, getenv("HOME"), sizeof(fileName) - 1 );
	strncat(fileName, "/logs/mapp.log", sizeof(fileName) - strlen(fileName) - 1 );
	backupFileName[sizeof(backupFileName)-1] = '\0';
	strncpy(backupFileName, getenv("HOME"), sizeof(backupFileName) - 1 );
	strncat(backupFileName, "/logs/mapp_old.log", sizeof(backupFileName) - strlen(backupFileName) - 1 );
	isFileNameSet = true;
}

int openFile() {
	if (!isFileNameSet)
		setFileName();
	struct stat bufferStruct;
	int result = stat(fileName, &bufferStruct);
	if (result != -1 &&
		bufferStruct.st_size > MAX_FILESIZE &&
		rename(fileName, backupFileName) == -1)
	{
		return -1;
	}
	int oflags = O_CREAT|O_WRONLY|O_APPEND;
	int mode = S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH;
	return open(fileName, oflags, mode);
}

void closeFile() {
	flock(fd1, LOCK_UN);
	close(fd1);
	fd1 = -1;
}

void vLogInit (const char *appName) {

	strcpy(AppName, appName);

	if(LOGDEV == 'S')
	{
		openlog(appName, LOG_PID|LOG_CONS, LOG_USER);
		fd1 = 0;
	}
	else if (LOGDEV == 'C')
	{
		char devName[4096];
		devName[sizeof(devName)-1] = '\0';
		int oflags = O_RDWR | O_NOCTTY | O_NDELAY;
		int mode = S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH;
		strncpy(devName, "/dev/ttyS0", sizeof(devName) - 1);
		fd1=open(devName, oflags, mode);
		if (fd1 == -1 )
		{
			perror("open_port: Unable to open log dev -");
		}
		else
		{
			fcntl(fd1, F_SETFL,0);
			printf("Port 1 has been sucessfully opened and %d is the file description\n",fd1);
		}
	}
}

void vLogDeInit () {

	if(LOGDEV == 'S')
		closelog();
	else if (LOGDEV == 'C')
		close(fd1);

	fd1 = -1;
}

void vLogMessage (int logtype, char const * FileName, int LineNum, char const *Format, ...) {
	va_list argp;
	char szFormatMessageBuff[MAX_BUFFER_LENGTH+1];
	int msgLen = 0;
	char etype = 'I';

	szFormatMessageBuff[0] = '\0';
	if (Format != 0) {
		va_start(argp, Format);
		msgLen = vsnprintf(szFormatMessageBuff, MAX_BUFFER_LENGTH, Format,argp);
		va_end(argp);
	}
	if (msgLen > 0) {
		switch(logtype) {
			case LOG_ERROR: etype = 'E'; break;
			case LOG_WARN: etype = 'W'; break;
			case LOG_FATAL: etype = 'F'; break;
		}
		if (etype == 'F')
			vSyslogPush(LOG_ERR, etype, FileName, LineNum, szFormatMessageBuff);
		else
			vSyslogPush(LOG_DEBUG, etype, FileName, LineNum, szFormatMessageBuff);
	}
}

namespace
{
	inline unsigned long long getTicks()
	{
		struct timeval tv;
		unsigned long long ticks;

		if (gettimeofday(&tv, 0) == 0)
		{
			ticks = tv.tv_sec;
			ticks *= 1000;
			ticks += tv.tv_usec / 1000;
			return ticks;
		}
		else return 0;
	}
}
#define TIMESTAMP_FORMAT "%llu"


void vSyslogPush(int priority, char etype, const char * pchFile, int inLineNumber, char * buffer) {
	if (LOGDEV == 'T') {
		fd1 = openFile();
		if (fd1 != -1)
		{
			flock(fd1, LOCK_EX);
		}
	}

	char messageBuffer[MAX_BUFFER_LENGTH] = {0};
	char * path = (char *)pchFile;
	int mlen = 0, offset = 0;

	if (fd1 == -1)
		return;

	if(LOGDEV == 'T')
	{
		mlen += snprintf(messageBuffer, sizeof(messageBuffer)-mlen, "%s:", AppName);
	}

	mlen += snprintf(messageBuffer + mlen, sizeof(messageBuffer)-mlen, "T:%c|F:%s|L:%05d|" TIMESTAMP_FORMAT "|%s%c", etype, path,inLineNumber, getTicks(), buffer, (LOGDEV == 'S' ? '\0' : '\n'));
	do {
		int ll = (offset + MAX_MESSAGE_LENGTH) < mlen ? MAX_MESSAGE_LENGTH : mlen - offset;

		switch(LOGDEV) {
			case 'O':
					fwrite(messageBuffer + offset, ll, 1, stderr);
					break;
			case 'S':
					syslog(priority, "%.*s", ll, messageBuffer + offset);
					break;
			case 'C':
			case 'T':
					write(fd1, messageBuffer + offset, ll);
					fsync(fd1);
					break;
		}

		offset += ll;


	} while (offset < mlen);
	if (LOGDEV == 'T')
	{
		closeFile();
	}
}

void vLogHEXBuffer(char const * FileName, int LineNum, void const * in_buf, int num_bytes, const char *title) {
	if (LOGDEV == 'T')
	{
		fd1 = openFile();
		if (fd1 != -1)
		{
			flock(fd1, LOCK_EX);
		}
	}
	int offset = 0; //, sz = 0;
	char * path = (char *)FileName;
	char disp_buf[MAX_BUFFER_LENGTH];

	if (fd1 == -1)
		return;

	int pos=0;
	if(LOGDEV == 'T')
	{
		pos += snprintf(disp_buf+pos, sizeof(disp_buf)-pos, "%s:", AppName);
	}

	if ( title[0] !=  '\0' ) {
		pos += snprintf(disp_buf+pos, sizeof(disp_buf)-pos, "T:I|F:%s|L:%05d|" TIMESTAMP_FORMAT "|%s (%d):\n", path,LineNum, getTicks(), title, num_bytes);
	} else {
		pos += snprintf(disp_buf+pos, sizeof(disp_buf)-pos, "T:I|F:%s|L:%05d|" TIMESTAMP_FORMAT "|(%d):\n", path,LineNum, getTicks(), num_bytes);
	}

	switch(LOGDEV) {
		case 'O':
				fwrite(disp_buf, pos, 1, stderr);
				break;
		case 'S':
				syslog(LOG_DEBUG, "%s", disp_buf);
				break;
		case 'T':
		case 'C':
				 write(fd1, disp_buf, pos);
				 fsync(fd1);
				 break;
	}

	do {
		int left = num_bytes - offset;
		vdDumpFmtString(in_buf, offset, (left < 16 ? left : 16), true);
		offset += 16;
	} while (offset < num_bytes);
	if (LOGDEV == 'T')
	{
		closeFile();
	}
}

void vdDumpFmtString(void const * in_bufRaw, int offset, int num_bytes, int ascii_rep) {
	if (LOGDEV == 'T')
	{
		fd1 = openFile();
		if (fd1 != -1)
		{
			flock(fd1, LOCK_EX);
		}
	}
	int index=0;
	int pos;
	char buffer[MAX_MESSAGE_LENGTH];
	unsigned char const * in_buf = (unsigned char const *) in_bufRaw;

	if (fd1 == -1)
		return;


	if ( ascii_rep ) {
		if ( num_bytes > 16 ) // disp_buf overflow!
			syslog(LOG_DEBUG, "LOG_HEX_Data ERR - num_bytes=%d > 15", num_bytes);
	} else {
		if ( num_bytes > 23 ) // disp_buf overflow!
			syslog(LOG_DEBUG, "LOG_HEX_Data ERR - num_bytes=%d > 20", num_bytes);
	}
	pos=0;
//	memset(buffer, 0,sizeof(buffer));
	pos += sprintf(buffer+pos, "%04X ", offset);
	for (index=0; index < num_bytes; ++index) {
		pos += sprintf (buffer+pos, "%02X ", in_buf[offset+index]);
	}

	if ( ascii_rep ) {
		for (; index < 16; ++index) {
			pos += sprintf (buffer+pos, "   ");
		}

		pos += sprintf(buffer+pos, "\t-\t");
		for (index=0; index < num_bytes; ++index) {
			// Display character only if in ascii printable range, otherwise display period (.)
			char c = (in_buf[offset+index] >= 0x20 && in_buf[offset+index] <= 0x7E) ? in_buf[offset+index] : '.';
			pos += sprintf (buffer+pos, "%c", c);
		}
	}

	pos += sprintf (buffer+pos, "\n");

	switch(LOGDEV) {
		case 'O':
			fwrite(buffer, pos, 1, stderr);
			break;
		case 'S':
			syslog(LOG_DEBUG, "%s", buffer);
			break;
		case 'C':
		case 'T':
			 write(fd1, buffer, pos);
			 fsync(fd1);
			 break;
	}
	if (LOGDEV == 'T')
	{
		closeFile();
	}
}

char const * szGet_dlogger_Name(void) { return ("dlogger"); }
char const * szGet_dlogger_Version(void) { return ("1.0.0.0"); }
