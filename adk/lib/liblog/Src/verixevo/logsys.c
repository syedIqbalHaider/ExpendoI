/******************************************************************************

FILE			:	Logsys.C

PRODUCT			:   Verix Multi App Conductor

MODULES         :   LOGSYS 


DESCRIPTION     :  Used to log errors or debug messages as they occur using either the 
					log file, or outputing to a comm port. 

NOTES           :

HISTORY

-------------------------------------------------------------------------------
#   Date    	Who     			Comments
1   22-Feb-00   Bruce Girdlestone	Initial Draft.                
2   14-Jul-00   Prabha_R1			Updated to open the port specified 
									in config.sys variable
3	09-Aug-00	Rajagopal_N1		Enable logsys only if the compile switch LOGSYS_FLAG is present
4	23-Aug-00	Prabha_R1			-Disable logging if the env variable does not exist  
									-Increased the buffer size of the message to be logged
									-to truncate the message if it is more than the buffer length
5   31-Aug-00   Prabha_R1			Used #defines for buffer sizes  
6   31-Aug-00   Prabha_R1			Truncated the buffer which is more than MAX_MESSAGE_LENGTH 
7   23-Nov-01   Prabha_R1			Changed the log file name to VMAC.LOG
8   11-Feb-02   Prabha_R1			Fixed defects -to take care of memory overflow

9   2005		Vijay				Fixed defects -to take care of memory overflow
10	2005		Vijay				Dummy functions defined for release version of VMAC shared libraries.
11  2005		Vijay				Logsys to a file, file size if increased to 15k
12  09-Feb-06   Vijay				COM6 support for LOGSYS is implemented.
13	02-Mar-06	Vijay				minimized all the SVC_WAITS
14  19-Apr-06   Camillee_Y1         Added modification for *VMACLOGSYSMAX
15  17-Jun-06   Francis_G1          Patched the modification (from version 2.0e) by Santhosh.C.Rao:
                                    Fixed the issue 928, where the Device manager was not responding
									                  when the value of #LOGPORT was 'NONE'
16  23-Aug-06   Francis_G1          Patched the modification (from version 2.0ST9) by Santhosh C. Rao:
                                    Handled the event of misconfguration of #LOGPORT value.
																	                  
-------------------------------------------------------------------------------
*******************************************************************************/


/******************************************************************************
 Copyright (C) 1996-2010 by VeriFone Inc. All rights reserved.

 No part of this software may be used, stored, compiled, reproduced,
 modified, transcribed, translated, transmitted, or transferred, in any form 
 or by any means whether electronic, mechanical, magnetic, optical, 
 or otherwise, without the express prior written permission of VeriFone, Inc.
********************************************************************************/

/*****************************************************************************
* @func void | GetLogsysVersion|
* returns the logsys version
* This is used by function GetVMACLibVersion to display the vmac library version.
* This is common area so as to update the version number in single location. Hence it is declared
* outside LOGSYS_FLAG to work in release version as well.
* @rdesc return values.
* @flag void   |
* @end
*****************************************************************************/
char const * GetLogsysName(void)
{
	return ("logsys");
}

char const * GetLogsysVersion(void)
{
	return (LOGSYS_VERSION);
}

#ifdef LOGSYS_FLAG
#undef LOGSYS_USE_ELOG
//#if defined(VERIXV)
#include <svc.h>
#if defined(__GNUC__) && (__GNUC__ >= 3 ) // GCC version 3 and higher.s
#include <fcntl.h>
#endif
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdarg.h>
#include <logsys.h>
#include <errno.h>

//#define MAX_MESSAGE_LENGTH 4200
#define MAX_MESSAGE_LENGTH 1100
#define MAX_APPNAME_LENGTH 10 // EESL_APP_LOGICAL_NAME_SIZE
#define MAX_DATETIME_LENGTH 100
#define PORT_STATUS_BUF_SIZE 4

unsigned char APPNAME[MAX_APPNAME_LENGTH];
short connectDebugServer(void);
int   logToPipe(const char* , short);
void vvLogMessage(char const * pchFormat, va_list argList);
unsigned short LOG_TYPE;
unsigned long FILTER;
unsigned long PRINTF_FILTER = 0xFFFFFFFF;
char pchFormatMessageBuf[MAX_MESSAGE_LENGTH];
short LOGPORT = 0; // =1 - com1 debugging =2 - com2 debugging =6 com6 debugging
short sPortStat = 0;
int logFileSize = 0;

static void makeStringToUpper(char* str);

void vLogFormat(const char * pchFile, short inLineNumber, char const * pchFormatMessageBuf);
void fnLogMessage(char const * FileName, int LineNum, char const *Format, ...);

char pchDebugServer[40];

sem_t PRINTF_LOCK_semaphore; /* For Semaphore */

short  initsem=0;//Dinesh_s4 added for initialising the semaphore at the first instance
short  sem_wait_flag=0;//Santosh_R1 added for having the check to sema phore is initialised or not.

#define ARE_LOGS_ENABLED()				( ( LOGSYS_NONE != LOG_TYPE ) && ( LOGPORT ) ? 1 : 0 )
#define ARE_LOGS_ENABLED_F( filter )	( ( filter & FILTER ) && ( ARE_LOGS_ENABLED() ) ? 1 : 0 )

/***************************************************************************
BRUCE_G1: WWAF ENHANCEMENT 02/15/06
The following two variables store the local anonymouse pipe id, and
the server pipe ID opended to "P:LOGSRV
****************************************************************************/
static int my_pipe_log = -1;
static int  server_pipe = -1;



/***************************************************************************** 
BRUCE_G1: WWAF ENHANCEMENT 02/15/06
  * @func void | connectDebugServer| 
 Attempts to connect to the LOG SERVER PIPE, "P:LOGSRV"
 NOTE: This function is ONLY called if config variable [LOGICAL NAME]LOG=P
 i.e. DEVMANLOG=P
* @rdesc return values.
* @flag void   |  connection status od the pipe..
* @end
*****************************************************************************/
short connectDebugServer()
{
	short retVal = -1;
	int task;

	/* Find and connect to log server pipe.  We may have to wait for the log server to
	 * open it. */


	if (server_pipe <= 0)
	{
		server_pipe = get_owner("P:LOGSERV", &task);
	}

	/*
	NOTE: It may be possible that the debug server task starts after the 
	client, so the server pipe may not be open yet
	*/

	if (server_pipe > 0)
	{
		my_pipe_log = open("P:", O_RDWR);
		pipe_init_msg(my_pipe_log, 50);
		retVal = pipe_connect(my_pipe_log, server_pipe);
		if(retVal == -1)
			server_pipe = -1 ;//Chandran - 6/7/2006
	}

return retVal;//Vijay. 24/4/2006
}

/***************************************************************************** 
* @func void | LOG_INIT| 
* Initialises the logging mechanism
* @rdesc return values.
* @flag void   |  No return value.
* @end
*****************************************************************************/
void LOG_INIT(const char * tAppName, short logType,unsigned long filter)
{
	char envFilter[MAX_APPNAME_LENGTH + 4]; // appname + "FIL"
	char envLogType[MAX_APPNAME_LENGTH + 4]; // appname + "LOG"
	char pchArgBuf[50]; 
	short inEnvLen = 0;
	short inCurrentGroup = 0; 
	char buffer[10];	
	short retVal;
	
	char appName[MAX_APPNAME_LENGTH]; 
//	sem_init(&PRINTF_LOCK_semaphore,1);

	/*
	BRUCE_G1: WWAF ENHANCEMENT 02/15/06
	Initialise pipe handles
	*/
	my_pipe_log = -1;
	server_pipe = -1;

	if(tAppName == NULL)
		return ;

	/* Store the application name and logging mechanism type */
	strncpy(appName, tAppName, MAX_APPNAME_LENGTH-1);
	appName[MAX_APPNAME_LENGTH-1] = '\0';
	
	makeStringToUpper(appName);
	
	strcpy((char *)APPNAME,appName);

	LOG_TYPE=logType;	

	/* Check if the LOGGING type has been overridden by the environement variable */
	strcpy(envLogType,appName);
	strcat(envLogType,"LOG");
	inEnvLen = get_env(envLogType, pchArgBuf, 50);
	if (inEnvLen)
	{
		switch (pchArgBuf[0])
		{
		case 'N':
			LOG_TYPE = LOGSYS_NONE;
			break;
		case 'F':
			LOG_TYPE = LOGSYS_FILE;
			break;
		case 'C':
			LOG_TYPE = LOGSYS_COMM;
			break;
		case 'B':
			LOG_TYPE = LOGSYS_COMM_FILE;
			break;
/*
BRUCE_G1: WWAF ENHANCEMENT 02/15/06
Add a pipe functionality as a new method of channelling debug information
*/ 
		case 'P':
			LOG_TYPE = LOGSYS_PIPE;
		break;

			
		default:
			LOG_TYPE = LOGSYS_NONE;
			break;
		}//switch
	}//if

	FILTER = filter;

	/* Check if a config variable has overridden the filter type */
	strcpy(envFilter,appName);
	strcat(envFilter,"FIL");

	inEnvLen = get_env(envFilter, pchArgBuf, 50);

	if (inEnvLen)
	{
		sscanf(pchArgBuf,"%lX",&FILTER);
	}

	//int	get_env	(char *key, char *buffer, short size);
	inCurrentGroup = get_group();
	set_group(15);
	retVal = get_env("#LOGPORT", buffer, sizeof(buffer)-1);
	//retVal = getkey("#LOGPORT", buffer, size, (char*)0);
	buffer[retVal] ='\0';
	set_group(inCurrentGroup);

	if(retVal > 0)
	{
		if ( 0 == strcmp(buffer, "COM1" ) )
			LOGPORT = 1;
		else if ( 0 == strcmp(buffer, "COM2" ) )
			LOGPORT = 2;
		else if ( 0 == strcmp(buffer, "COM6" ) )
			LOGPORT = 6;
		else if ( 0 == strcmp(buffer, "COM8" ) )
			LOGPORT = 8;
		else if ( 0 == strcmp(buffer, "USBD" ) )
			LOGPORT = 10;

	} else {
		//If the key is not present then set the variable to avoid writing logs into pipe.
		sPortStat = 1;
	}

	if( LOGPORT == 0){
		//If the Port key value is not the specified the set the variable.
		sPortStat = 1;
	}//t_santosh_r1, 7/18/2006, Issue - 1407.


	if( LOG_TYPE == LOGSYS_PIPE )
	{
		int iret;
		char msg[50];

		set_group(15);
		// New filename already exists, so remove inpreperation fro the new file.
		if ((dir_get_file_size ("LOGSVR.OUT") > 0) || (dir_get_file_size ("F:LOGSVR.OUT") > 0))
		{
			int id = 1;
			int found = FALSE;
			char szFilename[16];
			struct task_info info;

			if (dir_get_file_size ("F:LOGSVR.OUT") > 0)
				strcpy(szFilename, "F:LOGSVR.OUT");
			else
				strcpy(szFilename, "LOGSVR.OUT");

			// WC check if the logsvr.out task is running. If it's not, start it.

			while (get_task_info(id, &info) > 0) 
			{
				id++;

				if (strstr("LOGSVR.OUT", info.path) != NULL)
				{
					found = TRUE;
					break;
				}
			}

			if (!found)
			{
				memset(msg, 0, sizeof(msg));
				// logsvr.out needs to be in GID15
				if( (iret = run( szFilename, "" )) < 0 )
				{
					fnLogMessage(__FILE__, __LINE__, "error starting logsvr %d, %d", iret, errno);
				}
				else
				{
					fnLogMessage(__FILE__, __LINE__, "Started logsvr");
				}
			}
		}
		else
		{
			char msg[50];

			memset(msg, 0, sizeof(msg));
			sprintf(msg, "LOGSVR not present !!!\n");
			vLogComm(msg, strlen(msg));
//			SVC_WAIT(50);
			LOG_TYPE = LOGSYS_NONE;
		}
		set_group(inCurrentGroup);
	}
	else if(LOG_TYPE == LOGSYS_COMM || LOG_TYPE == LOGSYS_COMM_FILE)
	{
		int nTries = 5;
		char *portName;
		int portFlags = 0;
		
		switch(LOGPORT) {
			case 1:
				portName = "/dev/COM1";
				break;
			case 2:
				portName = "/dev/COM2";
				break;
			case 6:
				if(!(1<<4) & (get_usb_device_bits()))
				{
					LOG_TYPE = LOGSYS_NONE;
					return;
				}
				portName = "/dev/COM6";
				break;
			case 8:
				portName = "/dev/COM8";
				break;
			case 10:
				portName = "/dev/USBD";
				portFlags = O_MULTI_DEV;
				break;
			default:
				LOG_TYPE = LOGSYS_NONE;
				return;
		}
			
		if(LOG_TYPE != LOGSYS_NONE)
		{
			int xcom = open(portName, portFlags);

			if(xcom == -1)		
			{
				int task = -1;

				dbprintf("LOG_INIT: Can not open port %s for app %s\n", portName, APPNAME);

				get_owner(portName, &task);
				dbprintf("Device: %s is owned by task: %d, my task is %d\n",portName,task,get_task_id());
				if(task == 0)
				{
					LOG_TYPE = LOGSYS_NONE;
				}

			}
			else
			{
				close(xcom);
			}
		}
		
	}
	dbprintf("LOG_INIT: LOG_TYPE = %d for app %s\n", LOG_TYPE, APPNAME);
}

/***************************************************************************** 
* @func void | vLogMessage | 
* @rdesc return values.
* @flag void   |  No return value.
* @end
*****************************************************************************/
void vLogMessage (char const * pchFormat,...)
{
	va_list argp;


	if(LOG_TYPE == LOGSYS_NONE)
		return;

	va_start(argp, pchFormat);
	vvLogMessage(pchFormat, argp);
	va_end(argp);
}//void



/***************************************************************************** 
* @func void | vLogMessageFilter | 
* @rdesc return values.
* @flag void   |  No return value.
* @end
*****************************************************************************/
void  vLogMessageFilter (unsigned long filter, char const * pchFormat,...)
{

	va_list argp;
	char * tmp;

	PRINTF_FILTER = filter;

	if ( ARE_LOGS_ENABLED_F( filter ) == 0)
	{
		//PRINTF_FILTER is not set to 0xFFFFFFFF as this is changed in vLogError function, Santosh_R1 - 1 Jul 2008.
		return;
	}


//dinesh_s4 start for initailising the semaphore only once 01-03-2007 ontime issue 4265
	if(initsem==0)
	{
	sem_init(&PRINTF_LOCK_semaphore,1);
	initsem=1;
	}
//dinesh_s4 end for initailising the semaphore only once

	sem_wait(&PRINTF_LOCK_semaphore);
	sem_wait_flag = 1;




	pchFormatMessageBuf[0] = 0;

//	PRINTF_FILTER = filter;

	if (pchFormat != 0)
	{
		va_start(argp,pchFormat);
		vsnprintf(pchFormatMessageBuf, MAX_MESSAGE_LENGTH, pchFormat, argp);
		va_end(argp);
	}
}//void



/***************************************************************************** 
* @func void | vLogComm| 
* Writes a log message to COMM1
* @rdesc return values.
* @flag void   |  No return value.
* @end
*****************************************************************************/
void vLogComm(const char * message,short shLen)
{
	int xcom = -1;
	struct Opn_Blk xcomblock;
	short retVal;
	unsigned char buf[PORT_STATUS_BUF_SIZE];

	int nTries = 5;
	char *portName;
	int portFlags = 0;


	if(LOGPORT == 0)
		return;
		
	switch(LOGPORT) {
		case 1:
			portName = "/dev/COM1";
			break;
		case 2:
			portName = "/dev/COM2";
			break;
		case 6:
			if(!(1<<4) & (get_usb_device_bits()))
				return;
			portName = "/dev/COM6";
			break;
		case 8:
			portName = "/dev/COM8";
			break;
		case 10:
			portName = "/dev/USBD";
			portFlags = O_MULTI_DEV;
			break;
		default:
			return;
	}

	do
	{
		xcom = open(portName, portFlags);

		if(xcom == -1)		
			SVC_WAIT(10);
	}
	while (xcom == -1 && --nTries);
			
	if(xcom == -1)
		return;
	
	xcomblock.rate = Rt_115200;
	xcomblock.format = Fmt_A8N1;
	xcomblock.protocol = P_char_mode;
	xcomblock.parameter = 0;

	set_opn_blk(xcom,&xcomblock);
	
	{
		int rc = -1;
		int local_errno = errno;
		errno = 0;

		do {
			rc = write(xcom,message,shLen);
			if(rc == -1 && errno == ENOSPC)
				SVC_WAIT(5);
			else
			{			
				break;	
			}			
		}while (1);

		errno = local_errno;
	}
	
	while((retVal = get_port_status((int)xcom,(char*)buf)) != 0)
		SVC_WAIT(1);//Santosh_R1, this is changed to increase the log efficeiency. 06 June 2008

	close(xcom);
}

/***************************************************************************** 
* @func void | logToPipe | 
* Logs the error to a pipen opened to P:LOGSRV.
* @rdesc return values.
* @flag void   |  No return value.
* @end
1	16 June 2008	Santosh_R1		Stored the global errno value in local variable, and
								re-asign to the global errno.
*****************************************************************************/
int  logToPipe(const char * message, short shLen)
{
	int rc = -1;
	int local_errno = errno;
	errno = 0;

	do {
		rc = write(my_pipe_log, message, shLen+1);
		SVC_WAIT(0);
		if(rc == -1 && errno == ENOSPC)
			SVC_WAIT(50);
	}while (rc == -1 && errno == ENOSPC);

	errno = local_errno;
	return rc;
}


/***************************************************************************** 
* @func void | vLogError | 
* Logs the error value provided in the SP2000 Log file.
* @rdesc return values.
* @flag void   |  No return value.
* @end
*****************************************************************************/
void vLogError(long lnError,const char * pchFile, short inLineNumber, short inErrorType,unsigned long filter)
{
	char messageBuffer[MAX_MESSAGE_LENGTH] = {0};
	char * lastPathToken;
	char * path = (char *)pchFile;
	int mlen = 0;
	if ( ARE_LOGS_ENABLED_F( PRINTF_FILTER) == 0 )
	{
		//This is set to 0XFFFFFFFF to continue logging in vLogError if the critical errors are called, Santosh_R1 1 Jul 2008.
		PRINTF_FILTER = 0XFFFFFFFF;	
		return;
	}


//dinesh_s4 start for initailising the semaphore only once 01-03-2007 ontime issue 4265
	if(initsem==0)
	{
	sem_init(&PRINTF_LOCK_semaphore,1);
	initsem=1;
	}
//dinesh_s4 end for initailising the semaphore only once

	//If semaphore is not initialised then only initialise the semaphore, Santosh_R1 - 1 Jul 2008
	if(sem_wait_flag == 0)
		sem_wait(&PRINTF_LOCK_semaphore);

	/* IF THE FILTER DOES NOT MATCH - RETURN */
	if ((filter & FILTER) == 0)
	{
		sem_post(&PRINTF_LOCK_semaphore);
		sem_wait_flag = 0;
		PRINTF_FILTER = 0XFFFFFFFF;	
		return;
	}

	/* Shorten the file path */
	lastPathToken = strrchr(pchFile,'\\');
	if (lastPathToken != NULL)
	{
		path = lastPathToken + 1;
	}

	/* Some error types are handled differently */
	switch (inErrorType)
	{
	case CRITICAL_ERROR:
		mlen = sprintf(messageBuffer,"%s:ER %05ld|F:%s|L:%05d|CRT\n\r",APPNAME,lnError,path,inLineNumber);
		break;
	case NON_CRITICAL_ERROR:
		mlen = sprintf(messageBuffer,"%s:ER %05ld|F:%s|L:%05d|NCT\n\r",APPNAME,lnError,path,inLineNumber);
		break;
	case NULL_POINTER_ERROR:
		mlen = sprintf(messageBuffer,"%s:ER %05ld|F:%s|L:%05d|NULLP\n\r",APPNAME,lnError,path,inLineNumber);
		break;
	case LOG_MESSAGE:
			/* Log messages have their own filter */
			if ((FILTER & LOGSYS_PRINTF_FILTER) == 0L)
			{
				sem_post(&PRINTF_LOCK_semaphore);
				sem_wait_flag = 0;
				PRINTF_FILTER = 0XFFFFFFFF;			
				return;
			}
			//Changed the code to eliminate re-entrant, Santosh_R1 - 30 June 2008
			//As the buffer was replaced by some other message.
/*Oh my God!!!
			sprintf(messageBuffer,"%s:F:%s|L:%05d|",APPNAME,path,inLineNumber);
			if (strlen(pchFormatMessageBuf) > (MAX_MESSAGE_LENGTH - strlen(messageBuffer)))
			{
				pchFormatMessageBuf[MAX_MESSAGE_LENGTH - strlen(messageBuffer) - 2] = '\0';
			}
			sprintf(messageBuffer,"%s:F:%s|L:%05d|%s\n\r",APPNAME,path,inLineNumber,pchFormatMessageBuf);
*/                                                     

			mlen = sprintf(messageBuffer,"%s:F:%s|L:%05d|%08u|",APPNAME,path,inLineNumber, read_ticks());
			mlen += sprintf(messageBuffer+mlen,"%.*s\n\r",MAX_MESSAGE_LENGTH - mlen - 3,pchFormatMessageBuf);

			break;

	case LOG_MESSAGE_FILTER:
			/* Log messages have their own filter */
			if ((FILTER & PRINTF_FILTER) == 0L)
			{
				sem_post(&PRINTF_LOCK_semaphore);
				sem_wait_flag = 0;
				PRINTF_FILTER = 0XFFFFFFFF;			
				return;
			}
/*
			//Changed the code to eliminate re-entrant, Santosh_R1 - 30 June 2008
			//As the buffer was replaced by some other message.
			sprintf(messageBuffer,"%s:F:%s|L:%05d|",APPNAME,path,inLineNumber);
			if (strlen(pchFormatMessageBuf) > (MAX_MESSAGE_LENGTH - strlen(messageBuffer)))
			{
				pchFormatMessageBuf[MAX_MESSAGE_LENGTH - strlen(messageBuffer) - 2] = '\0';
			}
			sprintf(messageBuffer,"%s:F:%s|L:%05d|%s\n\r",APPNAME,path,inLineNumber,pchFormatMessageBuf);
*/
			mlen = sprintf(messageBuffer,"%s:F:%s|L:%05d|",APPNAME,path,inLineNumber);
			mlen += sprintf(messageBuffer+mlen,"%.*s\n\r",MAX_MESSAGE_LENGTH - mlen - 3,pchFormatMessageBuf);

			break;

	}//switch
	
	switch (LOG_TYPE)
	{
	case LOGSYS_FILE:
		vPfrStoreErrorMessage(messageBuffer);
		break;
	case LOGSYS_COMM:
		vLogComm(messageBuffer,mlen);
		break;
	case LOGSYS_COMM_FILE:
		vPfrStoreErrorMessage(messageBuffer);
		vLogComm(messageBuffer,mlen);
		break;
		
	case LOGSYS_PIPE:
		/*
		BRUCE_G1: WWAF ENHANCEMENT 02/15/06
		Initialise pipe handles
		*/ 
		if (server_pipe <= 0)
		{
			/*
			If there is no pipe connection to the debug server (if the server task has not
			started), then try to connect
			*/
			if( connectDebugServer() < 0)
					vLogComm(messageBuffer,mlen); // write to com
		}
		if (server_pipe > 0)
		{
			//Fix for the issue where the device manager was not responding
			// when the #LOGPORT was NONE - Santhosh.C.Rao, 6/7/2006.
			if(sPortStat != 1)
				{
					/*int rc =-1;
					rc =*/ logToPipe(messageBuffer,mlen);

				}
			else
				close(my_pipe_log);
		}
		break;
		
	default:
		break;
	}
	sem_post(&PRINTF_LOCK_semaphore);
	sem_wait_flag = 0;
	PRINTF_FILTER = 0XFFFFFFFF;

	return;

}



/***************************************************************************** 
* @func void | vStoreErrorMessage | 
* Stores a message in the "VMAC.LOG" log file. 
* @rdesc return values.
* @flag void   |  No return value.
* @end
*****************************************************************************/
void vPfrStoreErrorMessage(char const * message)
{

	short logFile  = 0;
	PFR_LOGFILE_HEADER_STR header;
	char pchDateAndTime[MAX_DATETIME_LENGTH];
	short fillIndex;
	short curGroup = get_group();

	/* Variables used in fetching *VMACLOGSYSMAX */
	int retVal = 0;
	char buffer[10] = {'\0'};
	/* No messages greater than 80 bytes */
	if (strlen(message) > 80)
	{
		return;
	}

	/* Try to open the MAL log file */
	set_group(15);
	logFile = open("VMAC.LOG",O_RDWR);

	if (logFile == -1 || logFileSize == 0)  //camillee_y1: if (logFile == -1)
	{
		/* Read the file header */
		logFile = open("VMAC.LOG",O_RDWR | O_CREAT | O_TRUNC);  //camillee_y1: logFile = open("VMAC.LOG",O_RDWR | O_CREAT);
		if (logFile == -1)
		{
			set_group(curGroup);
			return;
		}
		/* There is a limit on the file size */
		//set_file_max (logFile,MAX_LOGFILE_SIZE);
    /* Fetch *VMACLOGSYSMAX from GID 15 */
    memset(buffer, '0', sizeof(buffer));
    retVal = get_env("*VMACLOGSYSMAX", buffer, sizeof(buffer));
    if(retVal > 0)
    {
        buffer[retVal] = '\0';
        if(strspn(buffer, "1234567890") == strlen(buffer))
        {
            logFileSize = atoi(buffer);
            if(logFileSize > PFR_CUST_MAX_LOGFILE_SIZE || logFileSize < PFR_CUST_MIN_LOGFILE_SIZE)
            {
                logFileSize = PFR_CUST_DEF_LOGFILE_SIZE;
            }
        }
        else
        {
            logFileSize = PFR_CUST_DEF_LOGFILE_SIZE;
        }
    }
    else
    {
        logFileSize = PFR_CUST_DEF_LOGFILE_SIZE;
    }
		/* Initialise and write circular file header */
		header.maxsize = logFileSize;   /*camillee_y1: header.maxsize = PFR_MAX_LOGFILE_SIZE;*/
		header.offset = sizeof(header);

		/* Store the updated header */
		lseek(logFile,(long)0,SEEK_SET);
		write(logFile,(char *)&header,sizeof(header));

		/* Fill the LOG file with NULL characters */
		memset(pchDateAndTime,0,50);
		for (fillIndex = header.offset;(fillIndex + 50) < logFileSize;fillIndex += 50)  //camillee_y1: for (fillIndex = header.offset;(fillIndex + 50) < PFR_MAX_LOGFILE_SIZE;fillIndex += 50)
		{
			write(logFile,(char *)pchDateAndTime,50);
		}
 		if (fillIndex < logFileSize)  //camillee_y1: if (fillIndex < PFR_MAX_LOGFILE_SIZE)
		{
			write(logFile,(char *)pchDateAndTime,logFileSize - fillIndex);  //camillee_y1: write(logFile,(char *)pchDateAndTime,PFR_MAX_LOGFILE_SIZE - fillIndex);
		}

	}//if

	/* Read the circular file header */
	lseek(logFile,(long)0,SEEK_SET);
	read(logFile,(char *)&header,sizeof(header));
	
	pchDateAndTime[0] = 0;

	/* Store date time and message */
	strcat(pchDateAndTime,message);
	strcat(pchDateAndTime,"\n\r*<*");

	/* Do we have to return to the beginning of the file */
	if ( (strlen(pchDateAndTime) + header.offset) >= header.maxsize)
	{
		header.offset = sizeof(header);
	}
	
	/* Store the message text */
	lseek(logFile,(long)header.offset,SEEK_SET);
	header.offset += (strlen(pchDateAndTime) -3);
	write(logFile,pchDateAndTime,strlen(pchDateAndTime));
	/* Store the updated header */
	lseek(logFile,(long)0,SEEK_SET);
	write(logFile,(char *)&header,sizeof(header));

	close(logFile);

	set_group(curGroup);
}

 //Converts strings in to  uppercase
static void makeStringToUpper(char* str)
 {
 	unsigned short i;

 	if (str==NULL)
 		return;
 
 	for (i=0; str[i];i++)
 		str[i]=toupper(str[i]);
 }

void vLogMsg(char const *FileName, int LineNum, char const * Format, ...)
{
	va_list argp;

	va_start(argp, Format);
	vvLogMessage(Format, argp);
	va_end(argp);

	vLogError((long) 0, FileName, LineNum, LOG_MESSAGE, 0xFFFFFFFF);
}

void LOG_HEX_Buf ( unsigned char *in_buf, int num_bytes, const char *title )
{
	int offset = 0;

    if ( title[0] !=  '\0' )
	{
		vLogMsg("", 0, "%s (%d):", title, num_bytes);
	}

	do
	{
        int left = num_bytes - offset;
		vLogHEXData ("", 0, in_buf, offset, (left < 16 ? left : 16), TRUE);
		offset += 16;
	} while (offset < num_bytes);
}

void vLogHEXBuffer(char const * FileName, int LineNum, void const * in_buf, int num_bytes, const char *title)
{
	int offset = 0;

    if ( title[0] !=  '\0' )
	{
		vLogMsg(FileName, LineNum, "%s (%d):", title, num_bytes);
	}

	do
	{
        int left = num_bytes - offset;
		vLogHEXData (FileName, LineNum, in_buf, offset, (left < 16 ? left : 16), TRUE);
		offset += 16;
	} while (offset < num_bytes);
}


void vLogHEXData (char const * FileName, int LineNum, void const * in_bufRaw, int offset, int num_bytes, int ascii_rep)
{
	int index=0;
int     pos;
	char disp_buf[100];
	unsigned char const * in_buf = (unsigned char const *) in_bufRaw;

	if ( ascii_rep == TRUE )
	{
		if ( num_bytes > 16 )
		{
			// disp_buf overflow!
			vLogMsg(FileName, LineNum, "LOG_HEX_Data ERR - num_bytes=%d > 15", num_bytes);
			return;
		}
	}
	else
	{
		if ( num_bytes > 23 )
		{
			// disp_buf overflow!
			vLogMsg(FileName, LineNum, "LOG_HEX_Data ERR - num_bytes=%d > 20", num_bytes);
			return;
		}
	}
    
    pos=0;
    disp_buf[pos] = '\0';
    pos += sprintf(disp_buf+pos, "%04X ", offset);
	for (index=0; index < num_bytes; ++index)
	{
        pos += sprintf (disp_buf+pos, "%02X ", in_buf[offset+index]);
    }
    for (; index < 16; ++index)
    {
        pos += sprintf(disp_buf+pos, "   ");
    }

	if ( ascii_rep == TRUE )
	{
		char    c;
        pos += sprintf(disp_buf+pos, "- ");
		for (index=0; index < num_bytes; ++index)
		{
			// Display character only if in ascii printable range, otherwise display period (.)
			//c=(isalnum (in_buf[offset+index]) ? in_buf[offset+index] : '.');
			c = (in_buf[offset+index] >= 0x20 && in_buf[offset+index] <= 0x7F) ? in_buf[offset+index] : '.';
            pos += sprintf (disp_buf+pos, "%c", c);
		}
	}
	vLogMsg(FileName, LineNum, "%s", disp_buf);
}

void vEnterMethod(char const * FileName, int LineNum, char const * ClassName, char const * Format, ...)
{
	va_list argp;
	char * tmp;
	char szFormatMessageBuff[MAX_MESSAGE_LENGTH+1];
	int msgLen = 0;
	
	//if ((SysDebugMode != 0) && (SysDebugMode >= ClassDebugMode))
	//{
		szFormatMessageBuff[0] = '\0';
		if (Format != 0)
		{
			va_start(argp, Format);
			msgLen = vsnprintf(szFormatMessageBuff, MAX_MESSAGE_LENGTH, Format,argp);
			va_end(argp);
		}
		if (msgLen > 0)
		{
			vLogMessage("T:%04d|%s::%s", 1, ClassName, szFormatMessageBuff);
			vLogError((long) 0, FileName, LineNum, LOG_MESSAGE, 0xFFFFFFFF);
		}
	//}
}

void vLeaveMethod(char const * FileName, int LineNum, char const * ClassName, char const *Format, ...)
{
	va_list argp;
	char * tmp;
	char szFormatMessageBuff[MAX_MESSAGE_LENGTH+1];
	int msgLen = 0;
	
	//if ((SysDebugMode != 0) && (SysDebugMode >= ClassDebugMode))
	//{
		szFormatMessageBuff[0] = '\0';
		if (Format != 0)
		{
			va_start(argp, Format);
			msgLen = vsnprintf(szFormatMessageBuff, MAX_MESSAGE_LENGTH, Format,argp);
			va_end(argp);
		}
		if (msgLen > 0)
		{
			//Add enter method type header
			vLogMessage("T:%04d|%s::%s", 2, ClassName, szFormatMessageBuff);
			vLogError((long) 0, FileName, LineNum, LOG_MESSAGE, 0xFFFFFFFF);
		}
	//}
}

void vErrorMethod(char const * FileName, int LineNum, char const * Format, ...)
{
	va_list argp;
	char * tmp;
	char szFormatMessageBuff[MAX_MESSAGE_LENGTH+1];
	int msgLen = 0;
	
	//if (SysDebugMode != 0)
	//{
		szFormatMessageBuff[0] = '\0';
		if (Format != 0)
		{
			va_start(argp, Format);
			msgLen = vsnprintf(szFormatMessageBuff, MAX_MESSAGE_LENGTH, Format,argp);
			va_end(argp);
		}
		if (msgLen > 0)
		{
			//Add enter method type header
			vLogMessage("T:%04d|%s", 5, szFormatMessageBuff);
			vLogError((long) 0, FileName, LineNum, LOG_MESSAGE, 0xFFFFFFFF);
		}
	//}
}


void vAlertMethod(char const * FileName, int LineNum, char const *Format, ...)
{
	va_list argp;
	char * tmp;
	char szFormatMessageBuff[MAX_MESSAGE_LENGTH+1];
	int msgLen = 0;
	
	//if (SysDebugMode != 0)
	//{
		szFormatMessageBuff[0] = '\0';
		if (Format != 0)
		{
			va_start(argp, Format);
			msgLen = vsnprintf(szFormatMessageBuff, MAX_MESSAGE_LENGTH, Format,argp);
			va_end(argp);
		}
		if (msgLen > 0)
		{
			//Add enter method type header
			vLogMessage("T:%04d|%s", 6, szFormatMessageBuff);
			vLogError((long) 0, FileName, LineNum, LOG_MESSAGE, 0xFFFFFFFF);
		}
	//}
}
//***************************************
void vLogFormat(const char * pchFile, short inLineNumber, char const * pchFormatMessageBuf)
{
	char messageBuffer[MAX_MESSAGE_LENGTH];
	char const *lastPathToken;
	char const * path = pchFile;
	int msgLen, availLen, copyLen;

	/* Shorten the file path */
	lastPathToken = strrchr(pchFile,'\\');
	if (lastPathToken != NULL)
	{
		path = lastPathToken + 1;
	}

	msgLen = sprintf(messageBuffer,"%s:F:%s|L:%05d|","LOGSYS", path, inLineNumber);
	availLen = sizeof(messageBuffer) - msgLen - sizeof("\r\n");
	copyLen = strlen(pchFormatMessageBuf);
	if (copyLen > availLen)
		copyLen = availLen;
	// TODO: Better len calculation without this stupid strlen() all the time
	memcpy(messageBuffer + msgLen, pchFormatMessageBuf, copyLen);
	msgLen += copyLen;
	strcpy(messageBuffer + msgLen, "\r\n");
	msgLen += sizeof("\r\n");
	
	vLogComm(messageBuffer, msgLen);
	return;	
}

void fnLogMessage (char const * FileName, int LineNum, char const * Format, ...)
{
	va_list argp;
	char * tmp;
	char szFormatMessageBuff[MAX_MESSAGE_LENGTH+1];
	int msgLen = 0;
		
	szFormatMessageBuff[0] = '\0';
	if (Format != 0)
	{
		va_start(argp, Format);
		msgLen = vsnprintf(szFormatMessageBuff, MAX_MESSAGE_LENGTH, Format,argp);
		va_end(argp);
	}
	if (msgLen > 0)
	{
		//Add enter method type header
		vLogFormat(FileName, LineNum, szFormatMessageBuff);
	}
}//void

void vvLogMessage(char const * pchFormat, va_list argList)
{
	//Asign 0xFFFFFFFF to the global filter to get the logs from dlog_msg(), Santosh_R1 - 23 June 2008.
	PRINTF_FILTER = 0xFFFFFFFF;

	if ( !ARE_LOGS_ENABLED( ) )
	{
		return;
	}

//dinesh_s4 start for initailising the semaphore only once 01-03-2007 ontime issue 4265

	if(initsem==0)
	{
		sem_init(&PRINTF_LOCK_semaphore,1);
		initsem=1;
	}
//dinesh_s4 end for initailising the semaphore only once
	sem_wait(&PRINTF_LOCK_semaphore);
	sem_wait_flag = 1;


	pchFormatMessageBuf[0] = 0;
	
	if (pchFormat != 0)
	{
		vsnprintf(pchFormatMessageBuf, MAX_MESSAGE_LENGTH, pchFormat, argList);
	}

//Commented the next line of code to serialise dlog_msg and dlog_msgF as semaphore locked while formatting needs to be unlocked
//after writing the LOG's, Santosh_R1 - 1 July 2008.
/*	#ifdef __thumb
	sem_post(&PRINTF_LOCK_semaphore);
	#endif*/
	
}

/*****************************************************************************
* @func void | vLogMessageBinary |
* Logs the error value provided in the SP2000 Log file.
* @rdesc return values.
* @flag void   |  No return value.
* @end
*****************************************************************************/
void vLogMessageBinary(long lFilter, const char *pchBuffer, short shLen)
{


	if ( !ARE_LOGS_ENABLED_F( lFilter ) )
	{
		return;
	}


//dinesh_s4 start for initailising the semaphore only once 01-03-2007 ontime issue 4265
	if(initsem==0)
	{
	sem_init(&PRINTF_LOCK_semaphore,1);
	initsem=1;
	}
//dinesh_s4 end for initailising the semaphore only once

	sem_wait(&PRINTF_LOCK_semaphore);
	sem_wait_flag = 1;



	switch (LOG_TYPE)
	{
	case LOGSYS_COMM:
		vLogComm(pchBuffer,shLen);
		break;
	case LOGSYS_COMM_FILE:
		vLogComm(pchBuffer,shLen);
		break;
	case LOGSYS_PIPE:
		/*
		BRUCE_G1: WWAF ENHANCEMENT 02/15/06
		Initialise pipe handles
		*/
		if (server_pipe <= 0)
		{
			/*
			If there is no pipe connection to the debug server (if the server task has not
			started), then try to connect
			*/
			if( connectDebugServer() < 0)
					vLogComm(pchBuffer,shLen); // write to com
		}
		if (server_pipe > 0)
		{
			//Fix for the issue where the device manager was not responding
			// when the #LOGPORT was NONE - Santhosh.C.Rao, 6/7/2006.
			if(sPortStat != 1)
				logToPipe(pchBuffer,shLen);
			else
				close(my_pipe_log);
		}
		break;

	default:
		break;
	}

	#ifdef __thumb
	sem_post(&PRINTF_LOCK_semaphore);
	sem_wait_flag = 0;
	#endif
	return;
}

#else
/*
	VIJAY_19_08_2005_START
	issue analysis id 12
	dummy functions for release version of VMAC

*/
/*****************************************************************************
* @func void | LOG_INIT|
* Initialises the logging mechanism
* @rdesc return values.
* @flag void   |  No return value.
* @end
*****************************************************************************/
void LOG_INIT(char * tAppName, short logType,unsigned long filter)
{


//dummy functions for release version of  VMAC

}



/*****************************************************************************
* @func void | vLogMessage |
* @rdesc return values.
* @flag void   |  No return value.
* @end
*****************************************************************************/
void vLogMessage (char * pchFormat,...)
{

//dummy functions for release version of  VMAC

}//void



/*****************************************************************************
* @func void | vLogMessageFilter |
* @rdesc return values.
* @flag void   |  No return value.
* @end
*****************************************************************************/
void vLogMessageFilter (unsigned long filter,char * pchFormat,...)
{

	//dummy functions for release version of  VMAC

}//void



/*****************************************************************************
* @func void | vLogError |
* Logs the error value provided in the SP2000 Log file.
* @rdesc return values.
* @flag void   |  No return value.
* @end
*****************************************************************************/
#ifdef _TARG_68000
void vLogError(long lnError,char * pchFile, int inLineNumber,int inErrorType,unsigned long filter)
#elif __thumb 	//predator
void vLogError(long lnError,const char * pchFile, short inLineNumber, short inErrorType,unsigned long filter)
#endif 	//_TARG_68000
{

	// dummy function for release version of VMAC

}



/*****************************************************************************
* @func void | vStoreErrorMessage |
* Stores a message in the "VMAC.LOG" log file.
* @rdesc return values.
* @flag void   |  No return value.
* @end
*****************************************************************************/
void vPfrStoreErrorMessage(char * message)
{

	//dummy function
}

void vLogComm(const char * message, short shLen)
{

}

/*
	VIJAY_19_08_2005_END
	issue analysis id 12
	dummy functions for release version of VMAC

*/



#endif
