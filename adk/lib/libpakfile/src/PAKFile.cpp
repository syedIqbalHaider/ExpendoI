#include "PAKFile.hpp"

#include <cerrno>
//#include <cstdio>
#include <cstring>

// NOTE: This is needed to go directly to original fopen() etc. functions
#include "libpakstdio/PAK_stdio.h"
#include "libpakstdio/internal/PAK_stdio_import.h"

namespace com_verifone_PAKFile
{
	namespace
	{
		namespace InternalConst
		{
			const std::size_t dirEntrySize = 0x40;
		}
		
		inline int Errno2Error(int errnoVal)
		{
			return -errnoVal;
		}
	}


	typedef unsigned long uint32_t;

	inline uint32_t getLittleEndianU32(void const * buffer)
	{
		unsigned char const * cb = static_cast<unsigned char const *>(buffer);
		return cb[0] | (cb[1] << 8) | (cb[2] << 16) | (cb[3] << 24);
	}

	
	void FileLocator::dump()
	{
		std::printf(
			"<FileLocator>\n"
			"pakOffset_=%#08lx, size_=%#08lx\n"
			"</FileLocator>\n",
			static_cast<unsigned long>(pakOffset_), static_cast<unsigned long>(size_)
		);
	}


	bool FileDirEntry::deserialize(std::FILE * pakStream)
	{
		uint32_t vars[2];
		std::size_t read, readWanted;
		bool ok = false;
		
		readWanted = sizeof(name_);
		read = PAK_FUNC_ORIGINAL(fread)(name_, 1, readWanted, pakStream);
		ok = (read == readWanted);

		//std::printf("file.name=%s\n", name_);
		
		if (ok)
		{
			readWanted = sizeof(vars);
			read = PAK_FUNC_ORIGINAL(fread)(vars, 1, readWanted, pakStream);
			ok = (read == readWanted);

			if (ok)
			{
				pakOffset_ = getLittleEndianU32(&vars[0]);
				size_ = getLittleEndianU32(&vars[1]);
			}
		}

		if (!ok)
		{
			name_[0] = '\0';
		}
		
		return ok;
	}
	
	
	////////////////////////////////////////////////////////////////
	// PAKFile
	int PAKFile::open(char const * path)
	{
		int result = -1;
		std::FILE * pak = NULL;		

		close();
		pak = PAK_FUNC_ORIGINAL(fopen)(path, "rb");
		
		if (pak)
		{
			char const headerPattern[] = "PACK";
			const std::size_t headerPatternLen = sizeof(headerPattern) - sizeof('\0');

			union
			{
				char header[headerPatternLen];
				uint32_t vars[2];
			} u;
			size_t read, readWanted;
			
			readWanted = headerPatternLen;
			read = PAK_FUNC_ORIGINAL(fread)(u.header, 1, readWanted, pak);
			if (read == readWanted && std::memcmp(u.header, headerPattern, headerPatternLen) == 0)
			{
				readWanted = sizeof(u.vars);
				read = PAK_FUNC_ORIGINAL(fread)(u.vars, 1, readWanted, pak);
				
				if (read == readWanted)
				{
					dirOffset_ = getLittleEndianU32(&u.vars[0]);
					dirSize_ = getLittleEndianU32(&u.vars[1]);
					result = 0;
				}
			}
			
			if (result == 0)
				pakFile_ = pak;
			else
				PAK_FUNC_ORIGINAL(fclose)(pak);
		}
		
		
		return result;
	}

	int PAKFile::close()
	{
		if (pakFile_)
		{
			PAK_FUNC_ORIGINAL(fclose)(pakFile_);
		}
		
		return 0;
	}
	
	
	bool PAKFile::getFileLocator(FileLocator & fileLocator, char const * fileName)
	{
		if (pakFile_)
		{
			FileDirEntry fileEntry;
			const std::size_t entriesCnt = dirSize_ / InternalConst::dirEntrySize;
			PAK_FUNC_ORIGINAL(fseek)(pakFile_, dirOffset_, SEEK_SET);

			for (std::size_t idx = 0; idx < entriesCnt; ++idx)
			{
				if (!fileEntry.deserialize(pakFile_))
				{
					/*
					std::printf("getFile() error\n");
					std::fflush(stdout);
					*/
					break;
				}
				else if (std::strcmp(fileEntry.getName(), fileName) == 0)
				{
					// Found the file!
					fileLocator = fileEntry;
					return true;
				}
			}
		}
		
		return false;
	}
	
	
	int PAKFile::readChunk(FileLocator & fileLocator, long startOffset, void * buffer, std::size_t size)
	{
		if (!fileLocator.isValid())
			return Errno2Error(EBADF);
		
		if (startOffset < 0)
			return Errno2Error(EINVAL);
			
		long sizeAvail = fileLocator.getSize();
		sizeAvail -= startOffset;

		if (sizeAvail <= 0)
			return 0;
			
		if (static_cast<std::size_t>(sizeAvail) < size)
			size = sizeAvail;

		long realOffset = fileLocator.getOffset() + startOffset;
		if (PAK_FUNC_ORIGINAL(fseek)(pakFile_, realOffset, SEEK_SET) != 0)
			return Errno2Error(EIO);
		
		std::size_t count = PAK_FUNC_ORIGINAL(fread)(buffer, 1, size, pakFile_);

		return count;
	}
	
	
	void PAKFile::dump()
	{
		std::printf(
			"<PAKFile>\n"
			"dirOffset_=%#08lx, dirSize_=%#08lx,"
			" pakFile_=%p\n"
			"</PAKFile>\n",
			static_cast<unsigned long>(dirOffset_), static_cast<unsigned long>(dirSize_), 
			static_cast<void *>(pakFile_)
		);
	}
	

	////////////////////////////////////////////////////////////////
	// PackedFile
	//
	bool PackedFile::open(PAKFile * pakFile, char const * fileName)
	{
		if (pakFile_)
			return false;
			
		bool result = pakFile->getFileLocator(fileLoc_, fileName);
		if (result)
		{
			pakFile_ = pakFile;
			filePosition_ = 0;
		}
			
		return result;
	}
	
	
	int PackedFile::read(void * buffer, std::size_t size)
	{
		if (!pakFile_)
		{
			return Errno2Error(EBADF);
		}
		
		// Get the data
		int countRead = pakFile_->readChunk(fileLoc_, filePosition_, buffer, size);
		if (countRead > 0)
		{
			filePosition_ += countRead;
		}
		
		return countRead;
	}

	long PackedFile::lseek(long offset, int origin)
	{
		switch (origin)
		{
		case SEEK_SET:
			filePosition_ = offset;
			break;
			
		case SEEK_CUR:
			filePosition_ += offset;
			break;
			
		case SEEK_END:
			filePosition_ = getSize() + offset;
		}
		
		// normalize
		if (filePosition_ < 0)
			filePosition_ = 0;
		else if (filePosition_ > getSize())
			filePosition_ = getSize();
		
		return filePosition_;
	}
}
