#ifndef PAK_STDIO_H
#define PAK_STDIO_H

#include <stdarg.h>
#include <stdio.h>

/*
#define PAK_STDIO_XCAT( x, y )  x ## y
#define PAK_STDIO_CAT( x, y )   PAK_STDIO_XCAT( x, y )
*/

#ifdef VERIXV
#	define PAK_FUNC_OVERRIDE(a)		$Sub$$##a
#	define PAK_FUNC_ORIGINAL(a)		$Super$$##a
#else
#	define PAK_FUNC_OVERRIDE(a)		pak_##a
#	define PAK_FUNC_ORIGINAL(a)		a
#endif

#define PAK_FUNC		PAK_FUNC_OVERRIDE
#define PAK_FUNCTIONS_NEEDED
#include "internal/PAK_functions.h"
#undef PAK_FUNCTIONS_NEEDED
#undef PAK_FUNC

#endif // #ifdef PAK_STDIO_H
