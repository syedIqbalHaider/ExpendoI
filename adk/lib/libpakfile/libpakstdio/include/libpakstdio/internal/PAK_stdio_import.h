#ifndef PAK_STDIO_IMPORT_H
#define PAK_STDIO_IMPORT_H


#ifdef VERIXV
#	define PAK_FUNC		PAK_FUNC_ORIGINAL
#	define PAK_FUNCTIONS_NEEDED
#	include "PAK_functions.h"
#	undef PAK_FUNCTIONS_NEEDED
#	undef PAK_FUNC
#else
#	include <stdio.h>
#endif

#endif // PAK_STDIO_IMPORT_H
