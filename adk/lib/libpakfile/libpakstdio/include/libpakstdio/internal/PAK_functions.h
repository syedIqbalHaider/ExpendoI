#ifndef PAK_FUNCTIONS_NEEDED
#error "This file should NOT be used directly! Use PAK_stdio.h instead."
#endif

#ifdef __cplusplus
extern "C" {
#endif
	int PAK_FUNC(fclose)(FILE * fp);
	int PAK_FUNC(fflush)(FILE * fp);
	FILE * PAK_FUNC(fopen)(const char * filename, const char * mode);
	FILE * PAK_FUNC(freopen)( const char * filename, const char * mode, FILE * fp);

	void PAK_FUNC(setbuf)(FILE * fp, char * buffer);
	int PAK_FUNC(setvbuf)(FILE * fp, char * buffer, int mode, size_t size);
	int PAK_FUNC(fprintf)(FILE * fp, const char * format, ... );
	int PAK_FUNC(fscanf)(FILE * fp, const char * format, ... );
	int PAK_FUNC(vfprintf)(FILE * fp, const char * format, va_list arg);
	int PAK_FUNC(fgetc)(FILE * fp);
	char * PAK_FUNC(fgets)(char * str, int num, FILE * fp);
	int PAK_FUNC(fputc)(int c, FILE * fp);
	int PAK_FUNC(fputs)(const char * str, FILE * fp);
	int PAK_FUNC(getc)(FILE * fp);
	int PAK_FUNC(putc)(int c, FILE * fp);
	int PAK_FUNC(ungetc)(int c, FILE * fp);
	size_t PAK_FUNC(fread)(void * ptr, size_t size, size_t count, FILE * fp);
	size_t PAK_FUNC(fwrite)(const void * ptr, size_t size, size_t count, FILE * fp);
	int PAK_FUNC(fgetpos)(FILE * fp, fpos_t * position);
	int PAK_FUNC(fseek)(FILE * fp, long offset, int origin);
	int PAK_FUNC(fsetpos)(FILE * fp, const fpos_t * pos);
	long PAK_FUNC(ftell)(FILE * fp);
	void PAK_FUNC(rewind)(FILE * fp);
	int PAK_FUNC(feof)(FILE * fp);
	int PAK_FUNC(ferror)(FILE * fp);

#ifdef VERIXV
	int PAK_FUNC(_fprintf)(FILE * fp, const char * format, ... );
	int PAK_FUNC(_vfprintf)(FILE * fp, const char * format, va_list arg);
#endif

#ifdef __cplusplus
} // extern "C"
#endif
