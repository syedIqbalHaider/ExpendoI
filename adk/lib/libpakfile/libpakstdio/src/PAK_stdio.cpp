#include "libpakstdio/PAK_stdio.h"
#include "libpakstdio/internal/PAK_stdio_import.h"

#include "PAKFile.hpp"

#include <cassert>
#include <cerrno>
#include <cstddef>
#include <cstring>

#include <memory>

#if defined(VERIXV)
#include <strcasecmp.h>
#elif defined(__GNUC__) && (__GNUC__ >= 3 )   // GCC version 3 and higher.s   
/* __iobuf flags */
#define _IOREAD         0x001
#define _IOEOF          0x010
#define _IOERR          0x020
#define _IOREADING      0x080
#define ATTR_NOT_AUTH  (1<<3)

int dir_get_attributes (const char *file)   { return 1; }

#endif

// local namespace
namespace
{
	const std::size_t MaxPAKFileCount = 4;
	
	// NOTE: Normal FILENAME_MAX only reserves a place for a max name + terminating zero. 
	// It does NOT take into account the drive or GID
	const std::size_t FULLFILENAME_MAX = 
		2 	// drive like "I:"
		+ 2	// GID - e.g. "14"
		+ sizeof('/')
		+ FILENAME_MAX;

	char const pakPrefix[] = "pak:";
	const std::size_t pakPrefixLen = sizeof(pakPrefix) - sizeof('\0');

	char const pakSuffix[] = ".pak";
	
	
	class PAKFileHolder
	{
	public:
		PAKFileHolder()
			: pakFile_(NULL), refCounter_(0)
		{
			pakName_[0] = '\0';
		}
		
		void cleanup()
		{
			delete pakFile_;
			pakFile_ = NULL;
			refCounter_ = 0;
			pakName_[0] = '\0';
		}
		
		bool isEmpty() const { return pakFile_ == 0; }
		bool isUnused() const { return refCounter_ <= 0; }
		char const * getName() const { return pakName_; }

		bool isSameFile(com_verifone_PAKFile::PAKFile * pPakFile) const 
		{ 
			return pakFile_ && pPakFile == pakFile_; 
		}
		
		com_verifone_PAKFile::PAKFile * checkoutPakFile() 
		{ 
			++refCounter_;
			return pakFile_; 
		}
		
		void setAndCheckout(com_verifone_PAKFile::PAKFile * pPakFile, char const * pakFileName)
		{
			cleanup();
			
			pakFile_ = pPakFile;
			refCounter_ = 1;
			std::strcpy(pakName_, pakFileName);
		}
		
		bool checkin(com_verifone_PAKFile::PAKFile * pPakFile)
		{
			bool freed = false;
			if (pPakFile && pakFile_ == pPakFile)
			{
				--refCounter_;
				freed = true;
			}
			
			return freed;
		}
	
	private:
		com_verifone_PAKFile::PAKFile * pakFile_;
		char pakName_[FULLFILENAME_MAX];
		int refCounter_;
	};
	

	// Globals
	PAKFileHolder pakFiles[MaxPAKFileCount];
	const std::size_t pakFilesCnt = sizeof(pakFiles) / sizeof(pakFiles[0]);

	com_verifone_PAKFile::PAKFile * getPakFile(char const * pakFileName)
	{
		com_verifone_PAKFile::PAKFile * pPakFile = NULL;
		
		// TODO: Name normalization! Note that a single file can be accessed using different names. 
		// E.g. "/foo.pak", "I:/foo.pak", "I:15/foo.pak" are all equivalent!
	
		// Try to find it in the array 
		for (int idx = 0; idx < pakFilesCnt; ++idx)
		{
			PAKFileHolder & pakHolder = pakFiles[idx];
			
			// NOTE: Case-insensitive search is used becuase under VerixV open works regardless of file name case.
			// Otherwise we would end up with many entries (different case) pointing to the same physiscal file.
			if (strcasecmp(pakHolder.getName(), pakFileName) == 0)
			{
				// Got it!
				pPakFile = pakHolder.checkoutPakFile();
				break;
			}
			
		}
		
		// PAK not cached
		if (!pPakFile)
		{
			int freeIdx = -1;
			
			// Find an empty slot first
			for (int idx = 0; idx < pakFilesCnt; ++idx)
			{
				PAKFileHolder const & pakHolder = pakFiles[idx];
				if (pakHolder.isEmpty())
				{
					freeIdx = idx;
					break;
				}
			}
			
			if (freeIdx < 0)
			{
				// Find an unused slot
				for (int idx = 0; idx < pakFilesCnt; ++idx)
				{
					PAKFileHolder const & pakHolder = pakFiles[idx];
					if (pakHolder.isUnused())
					{
						freeIdx = idx;
						break;
					}
				}
			}
			
			if (freeIdx >= 0)
			{
				// TODO: Check if file authenticated!
				int fileAttr = dir_get_attributes(pakFileName);
				if ( !(fileAttr & ATTR_NOT_AUTH) )
				{
					std::auto_ptr<com_verifone_PAKFile::PAKFile> pAutoPakFile(new com_verifone_PAKFile::PAKFile);
				
					if (pAutoPakFile.get())
					{
						if (pAutoPakFile->open(pakFileName) == 0)
						{
							pPakFile = pAutoPakFile.release();
							pakFiles[freeIdx].setAndCheckout(pPakFile, pakFileName);
						}
					}
				}
			}
		}
		
		return pPakFile;
	}

	void releasePakFile(com_verifone_PAKFile::PAKFile * pPakFile)
	{
		if (!pPakFile)
			return;
		
		for (int idx = 0; idx < pakFilesCnt; ++idx)
		{
			PAKFileHolder & pakHolder = pakFiles[idx];
			if (pakHolder.checkin(pPakFile))
				return;
		}
		
		// Should NEVER get here
		assert(false /* Wrong checkin */);
	}
	

	//
	class FileNameSplit
	{
	public:
		FileNameSplit()
		{
			clear();
		}
	
		static bool isPak(char const * fileName)
		{
			return std::strncmp(fileName, pakPrefix, pakPrefixLen) == 0;
		}
	
		bool parse(char const * fileName)
		{
			char const * fileNameStart = fileName + pakPrefixLen;
			
			// TODO: This is wrong if the user uses a full path for pak like: "pak:F:13/mypak/file"
			char const * slashPos = std::strchr(fileNameStart, '/');
			if (!slashPos)
				return false;
			
			std::size_t pakNameLen = slashPos - fileNameStart;
			if (pakNameLen >= sizeof(pakName_))
				return false;

			std::strncpy(pakName_, fileNameStart, pakNameLen);
			pakName_[pakNameLen] = '\0';
			
			{
				const std::size_t pakNameCharsLeft = sizeof(pakName_) - sizeof('\0') - pakNameLen;
				std::strncpy(pakName_ + pakNameLen, pakSuffix, pakNameCharsLeft);
				pakName_[sizeof(pakName_) - sizeof('\0')] = '\0';
			}
			
			// Move it to next char after '/'
			++slashPos;
			
			const std::size_t maxPackedFileLen = sizeof(packedFile_) - sizeof('\0');
			std::strncpy(packedFile_, slashPos, maxPackedFileLen);
			packedFile_[maxPackedFileLen] = '\0';
			
			return true;
		}
	
		char const * getPakFileName() const { return pakName_; }
		char const * getPackedFileName() const { return packedFile_; }
	
	private:
		void clear()
		{
			pakName_[0] = '\0';
			packedFile_[0] = '\0';
		}
	
	
	// Attributes
	private:
		char pakName_[FULLFILENAME_MAX];
		char packedFile_[FULLFILENAME_MAX];
	};
	
	
	// WARNING: VerixV functions REALLY do allocate the FILE structure but it would be entirely possible to allocate extended version and return pointer to the smaller one
	namespace FILE_PAK_Const
	{
		const char magicValue[] = "PaK_";
		const std::size_t magicValueLen = sizeof(magicValue) - sizeof('\0');
		const int magicFd = -2;
	}

	class FILE_PAK : public FILE
	{
	public:
		FILE_PAK() 
		{
			{
				FILE * fp = this;
				std::memset(fp, 0, sizeof(*fp));
#ifdef VERIXV
				fp->_fd = FILE_PAK_Const::magicFd;
#elif defined(__GNUC__) && (__GNUC__ >= 4 )   // GCC version 3 and higher.s   
				fp->_fileno = FILE_PAK_Const::magicFd;
#elif defined(__GNUC__) && (__GNUC__ >= 3 )   // GCC version 3 and higher.s   
				fp->_file = FILE_PAK_Const::magicFd;
#endif
				
				// NOTE: copied from the original functions
				fp->_flags = _IOREAD | _IOREADING;
			}
			
			std::memcpy(magic_, FILE_PAK_Const::magicValue, FILE_PAK_Const::magicValueLen);
		}
		
		static bool isMe(FILE const * fp)
		{
			FILE_PAK const * pFPak = static_cast<FILE_PAK const *>(fp);
#ifdef VERIXV
				bool isMineInstance = 
					pFPak->_fd == FILE_PAK_Const::magicFd
					&& std::memcmp(pFPak->magic_, FILE_PAK_Const::magicValue, FILE_PAK_Const::magicValueLen) == 0;
#elif defined(__GNUC__) && (__GNUC__ >= 4 )   // GCC version 3 and higher.s   
				bool isMineInstance = 
					pFPak->_fileno == FILE_PAK_Const::magicFd
					&& std::memcmp(pFPak->magic_, FILE_PAK_Const::magicValue, FILE_PAK_Const::magicValueLen) == 0;
#elif defined(__GNUC__) && (__GNUC__ >= 3 )   // GCC version 3 and higher.s   
				bool isMineInstance = 
					pFPak->_file == FILE_PAK_Const::magicFd
					&& std::memcmp(pFPak->magic_, FILE_PAK_Const::magicValue, FILE_PAK_Const::magicValueLen) == 0;
#endif							
			return isMineInstance;
		}
		
		com_verifone_PAKFile::PackedFile & getPackedFile() { return packedFile_; }
		
	private:
		char magic_[FILE_PAK_Const::magicValueLen];
		com_verifone_PAKFile::PackedFile packedFile_;
	};
}



//
// fopen() etc. FUNCTION OVERRIDES
// NOTE: All non-overriden function will raise an error because the _fd is set to invalid file.
//

FILE * PAK_FUNC_OVERRIDE(fopen)(const char * filename, const char * mode)
{
	if (FileNameSplit::isPak(filename))
	{
		FILE * fp = NULL;

		FileNameSplit splitNames;
		if (splitNames.parse(filename))
		{
			com_verifone_PAKFile::PAKFile * pPakFile = getPakFile(splitNames.getPakFileName());
			if (pPakFile)
			{
				// allocate the FILE_PAK
				FILE_PAK * pFilePak = new FILE_PAK;
				if (pFilePak)
				{
					com_verifone_PAKFile::PackedFile & packedFile_ = pFilePak->getPackedFile();
					if (packedFile_.open(pPakFile, splitNames.getPackedFileName()))
					{
						// Everything ok - return the base ptr
						fp = pFilePak;
					}
					else
						delete pFilePak;
				}
			}
		}
		
		return fp;
	}
	else
		return PAK_FUNC_ORIGINAL(fopen)(filename, mode);
}

int PAK_FUNC_OVERRIDE(fclose)(FILE * fp)
{
	if (fp && FILE_PAK::isMe(fp))
	{
		FILE_PAK * pFilePak = static_cast<FILE_PAK *>(fp);
		{
			com_verifone_PAKFile::PackedFile & packedFile_ = pFilePak->getPackedFile();
		
			releasePakFile(packedFile_.getPakFile());
			packedFile_.close();
		}
		
		delete pFilePak;
		return 0;
	}
	else
		return PAK_FUNC_ORIGINAL(fclose)(fp);
}

size_t PAK_FUNC_OVERRIDE(fread)(void * ptr, size_t size, size_t count, FILE * fp)
{
	if (fp && FILE_PAK::isMe(fp))
	{
		FILE_PAK * pFilePak = static_cast<FILE_PAK *>(fp);
		com_verifone_PAKFile::PackedFile & packedFile_ = pFilePak->getPackedFile();
		
		int readCount = packedFile_.read(ptr, size * count);
		if (readCount == 0)
		{
			if (packedFile_.isEOF())
			{
				fp->_flags |= _IOEOF;
			}
		}
		else if (readCount < 0)
		{
			fp->_flags |= _IOERR;
			errno = -readCount;
		}
		
		return readCount;
	}
	else
		return PAK_FUNC_ORIGINAL(fread)(ptr, size, count, fp);
}

int PAK_FUNC_OVERRIDE(fseek)(FILE * fp, long offset, int origin)
{
	if (fp && FILE_PAK::isMe(fp))
	{
		FILE_PAK * pFilePak = static_cast<FILE_PAK *>(fp);
		com_verifone_PAKFile::PackedFile & packedFile_ = pFilePak->getPackedFile();
		
		packedFile_.lseek(offset, origin);
		return 0;
	}
	else
		return PAK_FUNC_ORIGINAL(fseek)(fp, offset, origin);
}

long PAK_FUNC_OVERRIDE(ftell)(FILE * fp)
{
	if (fp && FILE_PAK::isMe(fp))
	{
		FILE_PAK * pFilePak = static_cast<FILE_PAK *>(fp);
		com_verifone_PAKFile::PackedFile & packedFile_ = pFilePak->getPackedFile();
		
		return packedFile_.tell();
	}
	else
		return PAK_FUNC_ORIGINAL(ftell)(fp);
}

void PAK_FUNC_OVERRIDE(rewind)(FILE * fp)
{
	if (fp && FILE_PAK::isMe(fp))
	{
		FILE_PAK * pFilePak = static_cast<FILE_PAK *>(fp);
		com_verifone_PAKFile::PackedFile & packedFile_ = pFilePak->getPackedFile();
		
		packedFile_.lseek(0, SEEK_SET);
	}
	else
		PAK_FUNC_ORIGINAL(rewind)(fp);
}

int PAK_FUNC_OVERRIDE(fgetpos)(FILE * fp, fpos_t * position)
{
	if (fp && FILE_PAK::isMe(fp))
	{
		FILE_PAK * pFilePak = static_cast<FILE_PAK *>(fp);
		com_verifone_PAKFile::PackedFile & packedFile_ = pFilePak->getPackedFile();

#if defined(__GNUC__) && (__GNUC__ >= 4 )   // GCC version 3 and higher.s   
 		long t = packedFile_.tell();
		std::memcpy(position, &t, sizeof(long));
#else
 		*position = packedFile_.tell();
#endif
		return 0;
	}
	else
		return PAK_FUNC_ORIGINAL(fgetpos)(fp, position);
}


int PAK_FUNC_OVERRIDE(fsetpos)(FILE * fp, const fpos_t * position)
{
	if (fp && FILE_PAK::isMe(fp))
	{
		FILE_PAK * pFilePak = static_cast<FILE_PAK *>(fp);
		com_verifone_PAKFile::PackedFile & packedFile_ = pFilePak->getPackedFile();

#if defined(__GNUC__) && (__GNUC__ >= 4 )   // GCC version 3 and higher.s   
 		long p;
		std::memcpy(&p, position, sizeof(long));
		packedFile_.lseek(p, SEEK_SET);
#else
		packedFile_.lseek(*position, SEEK_SET);
#endif		
		return 0;
	}
	else
		return PAK_FUNC_ORIGINAL(fsetpos)(fp, position);
}

int PAK_FUNC_OVERRIDE(feof)(FILE * fp)
{
	if (fp && FILE_PAK::isMe(fp))
	{
		FILE_PAK * pFilePak = static_cast<FILE_PAK *>(fp);
		com_verifone_PAKFile::PackedFile & packedFile_ = pFilePak->getPackedFile();

		return packedFile_.isEOF();
	}
	else
		return PAK_FUNC_ORIGINAL(feof)(fp);
}


#ifdef TODO_FUNC
int PAK_FUNC_OVERRIDE(fgetc)(FILE * fp)
{
}

char * PAK_FUNC_OVERRIDE(fgets)(char * str, int num, FILE * fp)
{
}


int PAK_FUNC_OVERRIDE(getc)(FILE * fp)
{
}


int PAK_FUNC_OVERRIDE(ungetc)(int c, FILE * fp)
{
}
#endif



// NOT NEEDED!
#if 0
int PAK_FUNC_OVERRIDE(fflush)(FILE * fp)
{
}

FILE * PAK_FUNC_OVERRIDE(freopen)( const char * filename, const char * mode, FILE * fp)
{
}


void PAK_FUNC_OVERRIDE(setbuf)(FILE * fp, char * buffer)
{
}

int PAK_FUNC_OVERRIDE(setvbuf)(FILE * fp, char * buffer, int mode, size_t size)
{
}

int PAK_FUNC_OVERRIDE(fprintf)(FILE * fp, const char * format, ... )
{
}

int PAK_FUNC_OVERRIDE(fscanf)(FILE * fp, const char * format, ... )
{
}

int PAK_FUNC_OVERRIDE(vfprintf)(FILE * fp, const char * format, va_list arg)
{
}

// NOTE: PAK files should be rejected because of lacking _IOWRITE flag in _flags
size_t PAK_FUNC_OVERRIDE(fwrite)(const void * ptr, size_t size, size_t count, FILE * fp)
{
	return PAK_FUNC_ORIGINAL(fwrite)(ptr, size, count, fp);
}

int PAK_FUNC_OVERRIDE(fputc)(int c, FILE * fp)
{
	// return error if PAK 
}

int PAK_FUNC_OVERRIDE(fputs)(const char * str, FILE * fp)
{
	// return error if PAK 
}

int PAK_FUNC_OVERRIDE(putc)(int c, FILE * fp)
{
	// return error if PAK 
}

void PAK_FUNC_OVERRIDE(clearerr)(FILE * fp)
{
}

int PAK_FUNC_OVERRIDE(ferror)(FILE * fp)
{
}

#ifdef VERIXV
int PAK_FUNC_OVERRIDE(_fprintf)(FILE * fp, const char * format, ... )
{
	// return error if PAK 
}

int PAK_FUNC_OVERRIDE(_vfprintf)(FILE * fp, const char * format, va_list arg)
{
	// return error if PAK 
}
#endif


#endif




