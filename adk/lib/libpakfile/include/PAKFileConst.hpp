#ifndef EMEAV_LIBPAK_PAKFILECONST_HPP
#define EMEAV_LIBPAK_PAKFILECONST_HPP 
 
#ifndef __cplusplus 
#  error "This file is for C++ only!"
#endif 
 
/***************************************************************************
** 
 *  
 * Copyright (C) 2006 by VeriFone, Inc. 
 * 
 * All rights reserved.  No part of this software may be reproduced, 
 * transmitted, transcribed, stored in a retrieval system, or translated 
 * into any language or computer language, in any form or by any means, 
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise, 
 * without the prior written permission of VeriFone, Inc.  
 *        
 **************************************************************************/ 
 
/** 
 * @file   PAKFileConst.hpp
 * 
 * @author  Tomasz Saniawa (tomasz_s1@verifone.com)
 *  
 * @brief   Constants for PAK file system
 * 
 * @remarks  This file should be compliant with Verifone EMEA R&D C++ Coding   
 *           Standard 1.0.x  
 */ 
 
/*************************************************************************** 
 * Includes 
 **************************************************************************/ 

#include <cstddef>
 
 
/*************************************************************************** 
 * Module namespace: begin 
 **************************************************************************/ 
 
namespace com_verifone_PAKFile
{ 
 
/** 
 * @addtogroup PAKFile
 * @{  
 */ 


	/** 
	 * Constants 
	 **/ 
	namespace Const
	{
		const std::size_t prefixLen = sizeof("pak:") - sizeof('\0');
		const std::size_t drivePrefix = sizeof("F:") - sizeof('\0');
		const std::size_t groupPrefix = sizeof("13/") - sizeof('\0');
		const std::size_t maxPakFileWithoutExt = 32 - sizeof(".pak") - sizeof('\0');
		const std::size_t maxPackedNameLength = 0x38;

		// equivalent of normal FILENAME_MAX but counting all elements of pak path
		// NOTE: Full path to file may look like this: 'pak:F:13/paknamewithoutext/dir1/dir2/packedfile'
		const std::size_t maxPathLen = 
			prefixLen
			+ drivePrefix
			+ groupPrefix
			+ maxPakFileWithoutExt
			+ sizeof('/')
			+ maxPackedNameLength;
	}
 
/** 
 * @} 
 */ 
 
} 
/***************************************************************************  
 * Module namespace: end   
**************************************************************************/ 
 
#endif /* EMEAV_LIBPAK_PAKFILECONST_HPP */
