#ifndef EMEAV_LIBPAK_PAKFILE_HPP
#define EMEAV_LIBPAK_PAKFILE_HPP 
 
#ifndef __cplusplus 
#  error "This file is for C++ only!"
#endif 
 
/***************************************************************************
** 
 *  
 * Copyright (C) 2006 by VeriFone, Inc. 
 * 
 * All rights reserved.  No part of this software may be reproduced, 
 * transmitted, transcribed, stored in a retrieval system, or translated 
 * into any language or computer language, in any form or by any means, 
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise, 
 * without the prior written permission of VeriFone, Inc.  
 *        
 **************************************************************************/ 
 
/** 
 * @file   PAKFile.hpp
 * 
 * @author  Tomasz Saniawa (tomasz_s1@verifone.com)
 *  
 * @brief   Class to handle PAK files
 * 
 * @remarks  This file should be compliant with Verifone EMEA R&D C++ Coding   
 *           Standard 1.0.x  
 */ 
 
/*************************************************************************** 
 * Includes 
 **************************************************************************/ 

#include "PAKFileConst.hpp"

#include <stddef.h>

//#include <cassert>
#include <cstddef>
#include <cstdio>

 
 
/*************************************************************************** 
 * Module namespace: begin 
 **************************************************************************/ 
 
namespace com_verifone_PAKFile
{ 
 
/** 
 * @addtogroup PAKFile
 * @{  
 */ 


	/** 
	 * Constants 
	 **/ 
	namespace Constant
	{
		const std::size_t nameLength = 0x38;
	}
 
	/** 
	 * Exported class declarations 
	 **/ 
	 
 	// @brief Class handling packed file  (file residing in PAK)
	class FileLocator
	{
	private:
		static const std::size_t invalidSize = static_cast<size_t>(-1L);

	public:
		FileLocator()
			: pakOffset_(0), size_(invalidSize)
		{}

		bool isValid() const { return size_ != invalidSize; }
		std::size_t getOffset() const { return pakOffset_; }
		std::size_t getSize() const { return size_; }
		
		void dump();
	
	// Attributes
	protected:
		std::size_t pakOffset_;
		std::size_t size_;
	};
 
 
	class FileDirEntry : public FileLocator
	{
	public:
		FileDirEntry()
		{
			name_[0] = '\0';
		}
	
		bool deserialize(std::FILE * pakStream);
		char const * getName() const { return name_; }
	
	// Attributes
	private:
		char name_[Const::maxPackedNameLength];
	};
	
 
	// @brief Class handling PAK file
	class PAKFile
	{
	public:
		PAKFile()
			: dirOffset_(0), dirSize_(0), pakFile_(0)
		{}
		
		~PAKFile()
		{
			
		}
		
		int open(char const * path);
		int close();
		
		bool getFileLocator(FileLocator & fileLocator, char const * fileName);

		bool isValid() const { return pakFile_ != NULL && dirOffset_ > 0 && dirSize_ > 0; }

		int readChunk(FileLocator & fileLocator, long startOffset, void * buffer, std::size_t size);

		void dump();
		
	// Attributes
	private:
		std::size_t dirOffset_;
		std::size_t dirSize_;
		std::FILE * pakFile_;
	};
 
 
	// TODO: Change to boost::shared_ptr! 
	// TODO: Allow using not only the heap version of PAKFile *
	class PackedFile
	{
	public:
		PackedFile()
			: pakFile_(0), filePosition_(0)
		{}
	
		~PackedFile()
		{
			close();
		}
	
		bool open(PAKFile * pakFile, char const * fileName);
		
		// TODO: Notification to free the slot PAKFile occupies
		void close() { pakFile_ = NULL; }
		
		int read(void * buffer, std::size_t size);
		long lseek(long offset, int origin);
		long tell() const { return filePosition_; }
		bool isEOF() const { return tell() == getSize(); }

		bool isOpened() const { return pakFile_ != NULL; }
		std::size_t getSize() const { return fileLoc_.getSize(); }
		
		
		// TODO: Will be unneeded once the boost::shared_ptr is used
		PAKFile * getPakFile() { return pakFile_; }
		
	private:
		FileLocator fileLoc_;
		PAKFile * pakFile_;
		long filePosition_;
	};
/** 
 * @} 
 */ 
 
} 
/***************************************************************************  
 * Module namespace: end   
**************************************************************************/ 
 
#endif /* EMEAV_LIBPAK_PAKFILE_HPP */
