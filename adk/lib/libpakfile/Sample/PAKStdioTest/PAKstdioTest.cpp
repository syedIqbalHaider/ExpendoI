#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <svc.h>


void printFile(FILE * fp)
{
	char myBuf[128];
	size_t count = fread(myBuf, 1, sizeof(myBuf), fp);
	printf("fread=%lu\n", count);
	
	printf(">>>%.*s<<<\n", count, myBuf);
}

void testFile(char const * fileName)
{
	FILE * fp = fopen(fileName, "rb");
	printf("fopen=%p\n", fp);

	if (fp)
	{
		printFile(fp);
		int result = fclose(fp);
		printf("fclose=%i\n", result);
	}
}

int main(int argc, char const * argv[])
{
	printf("TEST: PQAKstdio\n");

	testFile("pak:F:my/second-file");
	testFile("normal.txt");

	return 0;
}
