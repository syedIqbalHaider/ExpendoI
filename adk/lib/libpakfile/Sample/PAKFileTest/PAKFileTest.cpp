#include "PAKFile.hpp"

#include <cstdio>


void TestGet(com_verifone_PAKFile::PAKFile & pak, char const * filename)
{
	std::printf("\n\n*** TestGet() ***\n");

	com_verifone_PAKFile::FileLocator fileLoc;
	bool result = pak.getFileLocator(fileLoc, filename);
	std::printf("getFileLocator(\"%s\")=%i\n", filename, result);
	fileLoc.dump();
}

void TestFile(com_verifone_PAKFile::PAKFile & pak, char const * filename)
{
	com_verifone_PAKFile::PackedFile file;
	bool result = file.open(&pak, filename);
	
	std::printf("\n\n*** TestFile() ***\n");
	std::printf("PackedFile::open(pak, \"%s\") = %i\n", filename, result);
	std::printf("PackedFile::getSize() = %lu\n", static_cast<unsigned long>(file.getSize()));
	std::printf("PackedFile::isOpened() = %i\n", file.isOpened());

	unsigned char buf[128];
	int readCount = file.read(buf, sizeof(buf));
	std::printf("PackedFile::read(buf, %lu) = %i\n", static_cast<unsigned long>(sizeof(buf)), readCount);

	std::printf(
		"--- BUF ---\n"
		"%.*s\n"
		"-----------\n"
	, readCount, buf);
}


int main(int argc, char * argv[])
{
	char const * pakName = argv[1];
	
	std::printf("pakName = %s\n", pakName);
	com_verifone_PAKFile::PAKFile pak;
	int result = pak.open(pakName);
	std::printf("pak.open()=%i\n", result);
	
	pak.dump();
	
	TestGet(pak, "a");
	TestGet(pak, "second-file");
	TestGet(pak, "zzz");
	
	TestFile(pak, "a");
	TestFile(pak, "second-file");
	
	
	return result;
}
