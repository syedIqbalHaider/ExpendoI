/*
 * Demonstration program for Nano-X graphics.
 * Portions Copyright (c) 2002 by Koninklijke Philips Electronics N.V.
 */
#include <svc.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <logsys.h>

#define MWINCLUDECOLORS
#include "nano-X.h"
#include <Nxcolors.h>
#include "titlebar.h"

extern GR_SCREEN_INFO	si;		/* information about screen */

char * weekday[] = { "Sunday", "Monday",
					 "Tuesday", "Wednesday",
					 "Thursday", "Friday", "Saturday"};
const char *months[] = 
{
  "Jan", "Feb", "Mar",
  "Apr", "May", "Jun",
  "Jul", "Aug", "Sep",
  "Oct", "Nov", "Dec"
};

#define MAX_BATTERY_ICONS 	4
#define BATTERY_FULL 		"battery_full.png"
#define BATTERY_HIGH		"battery_high.png"
#define BATTERY_MEDIUM 		"battery_medium.png"
#define BATTERY_LOW 		"battery_low.png"

static char *battery_icons[MAX_BATTERY_ICONS] = { BATTERY_LOW, BATTERY_MEDIUM, BATTERY_HIGH, BATTERY_FULL };

#define MAX_SIGNAL_ICONS 	6
#define SIGNAL_FULL 		"signal_full.png"
#define SIGNAL_HIGH			"signal_high.png"
#define SIGNAL_MEDIUM 		"signal_medium.png"
#define SIGNAL_LOW 			"signal_low.png"
#define SIGNAL_VLOW			"signal_vlow.png"
#define SIGNAL_NONE			"signal_none.png"

static char *signal_icons[MAX_SIGNAL_ICONS] = { SIGNAL_NONE, SIGNAL_VLOW, SIGNAL_LOW, SIGNAL_MEDIUM, SIGNAL_HIGH, SIGNAL_FULL };

static 	GR_WINDOW_ID pid_battery;
static 	GR_WINDOW_ID pid_signal;
static 	GR_WINDOW_ID offscreen_p;

static int battery_value = -1;
static int signal_value = -1;

static GR_IMAGE_INFO iif_battery;
static GR_IMAGE_INFO iif_signal;

int getBatteryValue(void)
{
	return 3;
}

int addBattery(GR_WINDOW_ID wTitlebar, GR_GC_ID gcTitlebar)
{
	GR_IMAGE_ID iid;
	int new_battery_value = -1;
	
	// Get Battery Value
	new_battery_value = getBatteryValue();
	
	if (battery_value != new_battery_value)
	{
		battery_value = new_battery_value;
		if(!(iid = GrLoadImageFromFile(battery_icons[battery_value], 0))) {
			dlog_error("Failed to load image file \"%s\"", battery_icons[battery_value]);
			return 0;
		}
		GrGetImageInfo(iid, &iif_battery);
		pid_battery = GrNewPixmap(iif_battery.width, iif_battery.height, NULL);
		GrDrawImageToFit(pid_battery, gcTitlebar, 0, 0, iif_battery.width, iif_battery.height, iid);
		GrFreeImage(iid);
	}
	GrCopyArea(wTitlebar, gcTitlebar, 0, 2, iif_battery.width, iif_battery.height, pid_battery, 0, 0, MWROP_SRCCOPY);
	return 1;
}

int getSignalValue(void)
{
	return 3;
}

int addSignal(GR_WINDOW_ID wTitlebar, GR_GC_ID gcTitlebar)
{
	GR_IMAGE_ID iid;
	int new_signal_value = -1;
	
	// Get Signal Value
	new_signal_value = getSignalValue();
	
	if (signal_value != new_signal_value)
	{
		signal_value = new_signal_value;
		if(!(iid = GrLoadImageFromFile(signal_icons[signal_value], 0))) {
			dlog_error("Failed to load image file \"%s\"", signal_icons[signal_value]);
			return 0;
		}
		GrGetImageInfo(iid, &iif_signal);
		pid_signal = GrNewPixmap(iif_signal.width, iif_signal.height, NULL);
		GrDrawImageToFit(pid_signal, gcTitlebar, 0, 0, iif_signal.width, iif_signal.height, iid);
		GrFreeImage(iid);
	}
	GrCopyArea(wTitlebar, gcTitlebar, 236 - iif_signal.width, 2, iif_signal.width, iif_signal.height, pid_signal, 0, 0, MWROP_SRCCOPY);	
	return 1;
}

void addTitlebar (GR_WINDOW_ID wBase, GR_WINDOW_ID *wTitlebar, GR_GC_ID *gcTitlebar)
{
	*wTitlebar = GrNewWindow(wBase, 1, 1, TITLEBAR_HEIGHT , TITLEBAR_WIDTH, 0, BLACK_MW, WHITE_MW);
	GrSelectEvents(*wTitlebar, GR_EVENT_MASK_TIMEOUT);
	
	//GrSelectEvents(*wTitlebar, GR_EVENT_MASK_BUTTON_DOWN | GR_EVENT_MASK_KEY_DOWN  | GR_EVENT_MASK_BUTTON_UP | GR_EVENT_MASK_EXPOSURE | GR_EVENT_MASK_FOCUS_IN    | GR_EVENT_MASK_FOCUS_OUT | GR_EVENT_MASK_CLOSE_REQ);
	/* must select down and up for wTitle_bar to get implicit grab when
	 * running window manager, otherwise the wm-created parent
	 * window will get the grab, and we won't get the button up...
	 */
	*gcTitlebar = GrNewGC();
	GrMapWindow(*wTitlebar);

	offscreen_p = GrNewPixmap(TITLEBAR_HEIGHT, TITLEBAR_WIDTH, NULL);	
	
	//font = GrCreateFont("verdana_12.iso-8859-15.pcf", 0, NULL);
	//GrSetGCFont(gcTitle_bar, font);

	get_timedate(*wTitlebar, *gcTitlebar);
	GrMapWindow(*wTitlebar);

	addBattery(*wTitlebar, *gcTitlebar);
	addSignal(*wTitlebar, *gcTitlebar);

}

void get_timedate(GR_WINDOW_ID td_win, GR_GC_ID td_gc)
{
	GR_SIZE  b, h, w;
	char gTimeStr[10];
	char gDateStr[50];
	char mval[3] = { "th" };
	int minutes, hour, sec;
	typedef struct 
	{
		  char t_yy[4];
		  char t_mm[2];
		  char t_dd[2];
		  char t_hh[2];
		  char t_mi[2];
		  char t_ss[2];
		  char t_w[1];
		  char t_extra[1];		  
	} numdatetime;
	numdatetime szVXTime;
	struct tm   stdTime;	
	char tmp[3];
	
	// Remember to destroy this
	//GR_WINDOW_ID	offscreen_p;		/* off screen pixmap */
	//offscreen_p = GrNewPixmap(238,40,0);

/*
	// Should be able to do this, but it crashes
	time_t rawtime;
	struct tm *ptm;

	time(&rawtime);
	ptm = gmtime ( &rawtime );
	dlog_msg("%d:%d %s", ptm->tm_hour, ptm->tm_min, ptm->tm_hour > 12 ? "pm" : "am");	SVC_WAIT(2000);

	switch (ptm->tm_mday)
	{
		case 1:
		case 21:
		case 31:
			strcpy(mval, "st");
			break;
		case 2:
		case 22:
			strcpy(mval, "nd");
			break;
		case 3:
		case 23:
			strcpy(mval, "rd");			
			break;
	}
	sprintf(time_str, "%d:%d %s", ptm->tm_hour, ptm->tm_min, ptm->tm_hour > 12 ? "pm" : "am");
	dlog_msg("%s", time_str);	SVC_WAIT(2000);
	sprintf(date_str, "%s %d%s %s %d", weekday[ptm->tm_wday], ptm->tm_mday, mval, months[ptm->tm_mon], ptm->tm_year);
	dlog_msg("%s", date_str);	SVC_WAIT(2000);	
*/
	memset(gTimeStr, 0, sizeof(gTimeStr));
	memset(gDateStr, 0, sizeof(gDateStr));
	memset((char*)&szVXTime, 0, sizeof(szVXTime));
    read_clock((char *)&szVXTime);

	memset(tmp, 0, sizeof(tmp));
	strncpy(tmp, szVXTime.t_ss, 2);
	stdTime.tm_sec   = str2int(tmp);     // seconds after the minute - [0,59] 
	strncpy(tmp, szVXTime.t_mi, 2);	
	stdTime.tm_min   = str2int(tmp);     // minutes after the hour - [0,59] 
	strncpy(tmp, szVXTime.t_hh, 2);	
	stdTime.tm_hour  = str2int(tmp);     // hours since midnight - [0,23] 

	strncpy(tmp, szVXTime.t_mm, 2);	
	stdTime.tm_mon = str2int(tmp);
	strncpy(tmp, szVXTime.t_dd, 2);	
	stdTime.tm_mday= str2int(tmp);
	strncpy(tmp, szVXTime.t_w, 2);	
	stdTime.tm_wday= str2int(tmp);
	strncpy(tmp, szVXTime.t_yy, 4);	
	stdTime.tm_year= str2int(tmp);

	switch (stdTime.tm_mday)
	{
		case 1:
		case 21:
		case 31:
			strcpy(mval, "st");
			break;
		case 2:
		case 22:
			strcpy(mval, "nd");
			break;
		case 3:
		case 23:
			strcpy(mval, "rd");			
			break;
	}
	hour = stdTime.tm_hour;
	if (hour > 12) {
		hour -= 12;
	}	
	sprintf(gTimeStr, "%d:%02d %s", hour, stdTime.tm_min, stdTime.tm_hour > 12 ? "pm" : "am");
	sprintf(gDateStr, "%s %d%s %s %d", weekday[stdTime.tm_wday], stdTime.tm_mday, mval, months[stdTime.tm_mon], stdTime.tm_year);

	/* Need to do this... but it will not work until the display_buffer OS issue is fixed!	*/
	// Write this to an offscreen pixmap
	GrGetGCTextSize (td_gc, gTimeStr, strlen(gTimeStr), GR_TFTOP | GR_TFASCII, &w, &h, &b);
	GrText(offscreen_p, td_gc, (si.cols - w)/2 , 15, gTimeStr, -1, GR_TFASCII);
	GrGetGCTextSize (td_gc, gDateStr, strlen(gDateStr), GR_TFTOP | GR_TFASCII, &w, &h, &b);	
	GrText(offscreen_p, td_gc, (si.cols - w)/2,  15+h, gDateStr, -1, GR_TFASCII);		

	// Now copy the pixmap to the diaplsyed window... minimal flicker!
	GrCopyArea(td_win, td_gc, 0, 0, TITLEBAR_HEIGHT, TITLEBAR_WIDTH, offscreen_p, 0, 0, MWROP_SRCCOPY);
	
/*
	GrGetGCTextSize (td_gc, gTimeStr, strlen(gTimeStr), GR_TFTOP | GR_TFASCII, &w, &h, &b);
	GrText(td_win, td_gc, (si.cols - w)/2 , 15, gTimeStr, -1, GR_TFASCII);

	GrGetGCTextSize (td_gc, gDateStr, strlen(gDateStr), GR_TFTOP | GR_TFASCII, &w, &h, &b);	
	GrText(td_win, td_gc, (si.cols - w)/2,  15+h, gDateStr, -1, GR_TFASCII);		
*/
}

void update_titlebar (GR_WINDOW_ID wTitlebar, GR_GC_ID gcTitlebar)
{
	get_timedate(wTitlebar, gcTitlebar);
	addBattery(wTitlebar, gcTitlebar);
	addSignal(wTitlebar, gcTitlebar);
}

