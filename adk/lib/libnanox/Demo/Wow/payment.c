/*
 * Demonstration program for Nano-X graphics.
 * Portions Copyright (c) 2002 by Koninklijke Philips Electronics N.V.
 */
#include <svc.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <logsys.h>

#define MWINCLUDECOLORS
#include "nano-X.h"
#include <Nxcolors.h>

#include "payment.h"

extern GR_SCREEN_INFO	si;		/* information about screen */
static GR_WINDOW_ID main_window;
static GR_WINDOW_ID amnt_window;

static GR_GC_ID main_gc;
static GR_GC_ID amnt_gc;

static GR_WINDOW_ID cancel_btn_window;
static GR_GC_ID 	cancel_btn_gc;

int npayment(void)
{
	main_window = GrNewWindow(GR_ROOT_WINDOW_ID, 0, 36,si.cols , si.rows - 36, 0, WHITE_MW, BLACK_MW);
	main_gc = GrNewGC();
	GrSetGCForeground(main_gc, BLUE);
	GrSetGCBackground(main_gc, WHITE_MW);
	GrMapWindow(main_window);

	enter_amount();
	enter_card();
	enter_signature();
	
	GrDestroyWindow (main_window);
	return 0;
}

int display_buttons (void)
{
	GR_SIZE b, h, w;
	GR_SIZE but_w = 80;
	GR_SIZE but_h = 60;	
	char cancel_btn_name[30] = {"Cancel"};
	
	cancel_btn_window = GrNewWindow(main_window, 10, 200, but_w, but_h, 1, RED, WHITE_MW);
	GrSelectEvents(cancel_btn_window, GR_EVENT_MASK_BUTTON_DOWN | GR_EVENT_MASK_BUTTON_UP | GR_EVENT_MASK_EXPOSURE | GR_EVENT_MASK_CLOSE_REQ);
	GrMapWindow(cancel_btn_window);

	cancel_btn_gc = GrNewGC();
	//GrSetGCForeground(cancel_btn_gc, WHITE_MW);
	//GrSetGCBackground(cancel_btn_gc, GREEN);	

	GrGetGCTextSize (cancel_btn_gc, cancel_btn_name, strlen(cancel_btn_name), GR_TFTOP | GR_TFASCII, &w, &h, &b);		
	GrText(cancel_btn_window, cancel_btn_gc, (but_w - w)/2, (but_h + h)/2, cancel_btn_name, -1, GR_TFASCII);
}

void enter_amount(void)
{
	GR_SIZE  b, h, w;
	GR_SIZE  amnt_win_width = 200;	
	GR_SIZE  amnt_win_height = 50;		
	GR_FONT_ID	font_hlvB12;
	GR_FONT_ID	font_lubI24;
	char prompt_1[25] = {"Enter Amount:"};
	char prompt_2[25] = {"� 9999.99"};
	

	amnt_window = GrNewWindow(main_window, 20, 60, amnt_win_width, amnt_win_height, 2, WHITE_MW, BLACK_MW);
	amnt_gc = GrNewGC();
	GrSetGCForeground(amnt_gc, BLUE);
	GrSetGCBackground(amnt_gc, WHITE_MW);
	GrMapWindow(amnt_window);

	font_hlvB12 = GrCreateFont("helvB12.fnt", 0, NULL);
	GrSetGCFont(main_gc, font_hlvB12);
	GrGetGCTextSize (main_gc, prompt_1, strlen(prompt_1), GR_TFTOP | GR_TFASCII, &w, &h, &b);	
	GrText(main_window, main_gc, 10, h+5, prompt_1, -1, GR_TFASCII);

	font_lubI24 = GrCreateFont("lubI24.fnt", 0, NULL);
	GrSetGCFont(amnt_gc, font_lubI24);
	GrGetGCTextSize (amnt_gc, prompt_2, strlen(prompt_2), GR_TFTOP | GR_TFASCII, &w, &h, &b);	
	GrText(amnt_window, amnt_gc, (amnt_win_width - w)/2, h+5, prompt_2, -1, GR_TFASCII);

	display_buttons();

	SVC_WAIT(20000);
}

void enter_card(void)
{
}

void enter_signature(void)
{
}

