/*
 * Demonstration program for Nano-X graphics.
 * Portions Copyright (c) 2002 by Koninklijke Philips Electronics N.V.
 */
#include <svc.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <logsys.h>

#define MWINCLUDECOLORS
#include "nano-X.h"
#include <Nxcolors.h>

#include "titlebar.h"
#include "payment.h"
#include "ntetris.h"

typedef unsigned char bool;
#define true 1
#define false 0


int m_console = -1;
const char *g_apLogicalName = "WOW";

#define POINTER_VISIBLE		1
#define POINTER_INVISIBLE 	2

/*
 * Definitions to make it easy to define cursors
 */
#define	_	((unsigned) 0)		/* off bits */
#define	X	((unsigned) 1)		/* on bits */
#define	MASK(a,b,c,d,e,f,g) \
	(((((((((((((a * 2) + b) * 2) + c) * 2) + d) * 2) \
	+ e) * 2) + f) * 2) + g) << 9)

#define	W2_WIDTH	70
#define	W2_HEIGHT	40

#define BUT_H 		77
#define BUT_W		75

static	GR_WINDOW_ID	baseWindow;			/* id for base window */
static  GR_GC_ID		gcBaseWindow;		
static	GR_WINDOW_ID	homeWindow;			/* id for base window */
static  GR_GC_ID		gcHomeWindow;	

static  GR_IMAGE_INFO   iif_bg;
static 	GR_WINDOW_ID    pid_gb;
static  GR_WINDOW_ID	pid_btn_sale;
static  GR_WINDOW_ID	pid_btn_slide;
static  GR_WINDOW_ID	pid_btn_clock;
static  GR_WINDOW_ID	pid_btn_setting;

static	GR_WINDOW_ID	wTitle_bar;			/* id for title bar window */
static	GR_GC_ID		gcTitle_bar;		/* graphics context for titlebar */

static	GR_WINDOW_ID	wBtn_sale;		/* id for button for tetris window */
static	GR_GC_ID		gcBtn_sale;		/* graphics context for tetris window */

static	GR_WINDOW_ID	wBtn_slide;		/* id for button for tetris window */
static	GR_GC_ID		gcBtn_slide;	/* graphics context for tetris window */
static	GR_WINDOW_ID	wBtn_clock;		/* id for button for tetris window */
static	GR_GC_ID		gcBtn_clock;	/* graphics context for tetris window */
static	GR_WINDOW_ID	wBtn_setting;		/* id for button for tetris window */
static	GR_GC_ID		gcBtn_setting;	/* graphics context for tetris window */

static  GR_WINDOW_ID    p1;             	/* off screen pixmap */

GR_SCREEN_INFO	si;		/* information about screen */

void do_buttondown(GR_EVENT_BUTTON	*bp);
void do_buttonup(GR_EVENT_BUTTON	*bp);
void do_motion(GR_EVENT_MOUSE		*mp);
void do_keystroke(GR_EVENT_KEYSTROKE	*kp);
void do_exposure(GR_EVENT_EXPOSURE	*ep);
void do_focusin(GR_EVENT_GENERAL	*gp);
void do_focusout(GR_EVENT_GENERAL	*gp);
void do_enter(GR_EVENT_GENERAL		*gp);
void do_exit(GR_EVENT_GENERAL		*gp);
void do_idle(void);

void updateButton (char *name, GR_WINDOW_ID wButton, GR_GC_ID gcButton)
{
	GR_SIZE b, h, w;	
	GrGetGCTextSize (gcButton, name, strlen(name), GR_TFTOP | GR_TFASCII, &w, &h, &b);		
	GrText(wButton, gcButton, (BUT_W - w)/2, h+5, name, -1, GR_TFASCII);	
}

void addButton (char *name, GR_WINDOW_ID wBase, GR_WINDOW_ID *wButton, GR_GC_ID *gcButton, GR_WINDOW_ID *pixmap, GR_COORD x, GR_COORD y)
{
	*wButton = GrNewWindow(wBase, x, y, BUT_W, BUT_H, 0, LTGRAY, WHITE_MW);
	GrSelectEvents(*wButton, GR_EVENT_MASK_BUTTON_DOWN | GR_EVENT_MASK_KEY_DOWN  | GR_EVENT_MASK_BUTTON_UP | GR_EVENT_MASK_EXPOSURE | 
						     GR_EVENT_MASK_FOCUS_IN | GR_EVENT_MASK_FOCUS_OUT | GR_EVENT_MASK_CLOSE_REQ);

	*gcButton = GrNewGC();
	GrSetGCForeground(*gcButton, BLACK_MW);
	GrSetGCBackground(*gcButton, LTGRAY);
 	addBackground(name, wButton, gcButton, pixmap);	
	GrMapWindow(*wButton);	
}

int initConsole( void )
{
	int ret = 0;

	LOG_INIT( (char *)g_apLogicalName, LOGSYS_PIPE,LOGSYS_PRINTF_FILTER);
	dlog_msg(__FILE__, __LINE__, "hello world from %s!", g_apLogicalName );

    // this app and this app alone will own the console!
    m_console = open( "/dev/console", 0 );
    if( m_console < 0 )
    {
        dlog_error(__FILE__, __LINE__, "error opening console");
        ret = -1;		// error opening console
    }
	else
	{
	    //Open conn to nano-X
	    if (GrOpen () != -1)
	    {
	        dlog_msg(__FILE__,__LINE__,"Nano-X connection is open");			
	    }
		else
		{
	        dlog_error(__FILE__,__LINE__,"Nano-X connection is failed");	
			ret = -2;
		}	
	}
    return ret;
}

int addBackground(char *img, GR_WINDOW_ID *w, GR_GC_ID *gc, GR_WINDOW_ID *pixmap)
{
	GR_IMAGE_ID iid;

	dlog_msg(__FILE__,__LINE__,"addBackground"); 		
	if(!(iid = GrLoadImageFromFile(img, 0))) {
		dlog_error(__FILE__, __LINE__, "Failed to load image file \"%s\"", img);
		return 0;
	}
	GrGetImageInfo(iid, &iif_bg);
	*pixmap = GrNewPixmap(iif_bg.width, iif_bg.height, NULL);
	GrDrawImageToFit(*pixmap, *gc, 0, 0, iif_bg.width, iif_bg.height, iid);
	GrFreeImage(iid);

	GrSetBackgroundPixmap(*w, *pixmap, GR_BACKGROUND_TOPLEFT);
	return 1;
}

int
main(int argc,char **argv)
{
	GR_EVENT	event;		/* current event */
	GR_BITMAP	bitmap1fg[7];	/* bitmaps for first cursor */
	GR_BITMAP	bitmap1bg[7];
	GR_BITMAP	bitmap2fg[7];	/* bitmaps for second cursor */
	GR_BITMAP	bitmap2bg[7];
	GR_FONT_ID	font;
	GR_SIZE  b, h, w;	

	initConsole();
	
	GrGetScreenInfo(&si);

	baseWindow = GrNewWindow(GR_ROOT_WINDOW_ID, 0, 0, si.cols , si.rows, 0, BLACK_MW, WHITE_MW);
	GrSelectEvents(baseWindow, GR_EVENT_MASK_BUTTON_DOWN | GR_EVENT_MASK_TIMEOUT | GR_EVENT_MASK_CLOSE_REQ);
	gcBaseWindow = GrNewGC();
	GrSetGCForeground(gcBaseWindow, BLACK_MW);
	GrInjectPointerEvent(0, 0, 0, POINTER_INVISIBLE);	
 	addBackground("homebg.png",&baseWindow, &gcBaseWindow, &pid_gb);	
	GrMapWindow(baseWindow);

	addTitlebar (baseWindow, &wTitle_bar, &gcTitle_bar);
	
	addButton ("btn_sale.png",		baseWindow, &wBtn_sale, 	&gcBtn_sale, 	&pid_btn_sale, 	20,		80);
	addButton ("btn_slide.png",		baseWindow, &wBtn_slide,	&gcBtn_slide,	&pid_btn_slide,	140,	80);
 	addButton ("btn_clock.png",		baseWindow, &wBtn_clock,	&gcBtn_clock,	&pid_btn_clock,	20, 	180);
	addButton ("btn_setting.png",	baseWindow, &wBtn_setting,	&gcBtn_setting,&pid_btn_setting, 	140, 	180);	

	while (1) {
		GrGetNextEventTimeout(&event, 500L);
		//dlog_msg(__FILE__,__LINE__,"Got Evt : %d", event.type);

		switch (event.type) {
			case GR_EVENT_TYPE_BUTTON_DOWN:
				do_buttondown(&event.button);
				break;

			case GR_EVENT_TYPE_BUTTON_UP:
				do_buttonup(&event.button);
				break;

			case GR_EVENT_TYPE_MOUSE_POSITION:
			case GR_EVENT_TYPE_MOUSE_MOTION:
				do_motion(&event.mouse);
				break;

			case GR_EVENT_TYPE_KEY_DOWN:
				do_keystroke(&event.keystroke);
				break;

			case GR_EVENT_TYPE_EXPOSURE:
				do_exposure(&event.exposure);
				break;

			case GR_EVENT_TYPE_FOCUS_IN:
				do_focusin(&event.general);
				break;

			case GR_EVENT_TYPE_FOCUS_OUT:
				do_focusout(&event.general);
				break;

			case GR_EVENT_TYPE_MOUSE_ENTER:
				do_enter(&event.general);
				break;

			case GR_EVENT_TYPE_MOUSE_EXIT:
				do_exit(&event.general);
				break;

			case GR_EVENT_TYPE_NONE:
				do_idle();			
				break;

			case GR_EVENT_TYPE_TIMEOUT:		        
				//dlog_msg(__FILE__,__LINE__,"update_titlebar");
				update_titlebar (wTitle_bar, gcTitle_bar);
				break;				
		}		
		//dlog_msg(__FILE__,__LINE__,"GetNext Evt");
	}
}


/*
 * Here when a button is pressed.
 */
void
do_buttondown(GR_EVENT_BUTTON	*bp)
{
	bool do_update = false;

	dlog_msg(__FILE__,__LINE__,"do_buttondown: %d", bp->wid);
	if (bp->wid == wBtn_sale) {
		npayment();
		do_update = true;
	}	
    else if (bp->wid == wBtn_slide) {
		ntetris();
		do_update = true;
	}

	if (do_update)
	{
		GrSetBackgroundPixmap(baseWindow, pid_gb, GR_BACKGROUND_TOPLEFT);	
		GrMapWindow(baseWindow);
		update_titlebar (wTitle_bar, gcTitle_bar);	
	}
}

/*
 * Here when a button is released.
 */
void
do_buttonup(GR_EVENT_BUTTON	*bp)
{	
	dlog_msg(__FILE__,__LINE__,"do_buttonup: bpX=%d bpY=%d", bp->x, bp->y); 
}

/*
 * Here when the mouse has a motion event.
 */
void
do_motion(GR_EVENT_MOUSE	*mp)
{
	dlog_msg(__FILE__,__LINE__,"do_motion: mpX=%d mpY=%d", mp->x, mp->y); 
}

/*
 * Here when a keyboard press occurs.
 */
void
do_keystroke(GR_EVENT_KEYSTROKE	*kp)
{
	dlog_msg(__FILE__,__LINE__,"do_keystroke: kp=%X", kp->ch); 	
}

/*
 * Here when an exposure event occurs.
 */
void
do_exposure(GR_EVENT_EXPOSURE	*ep)
{
	dlog_msg(__FILE__,__LINE__,"do_exposure:"); 
}

/*
 * Here when a focus in event occurs.
 */
void
do_focusin(GR_EVENT_GENERAL	*gp)
{
	dlog_msg(__FILE__,__LINE__,"do_focusin: %d", gp->wid); 
}

/*
 * Here when a focus out event occurs.
 */
void
do_focusout(GR_EVENT_GENERAL	*gp)
{
	dlog_msg(__FILE__,__LINE__,"do_focusout: %d", gp->wid); 
}

/*
 * Here when a enter window event occurs.
 */
void
do_enter(GR_EVENT_GENERAL	*gp)
{
	dlog_msg(__FILE__,__LINE__,"do_enter: %d", gp->wid); 
}

/*
 * Here when a exit window event occurs.
 */
void
do_exit(GR_EVENT_GENERAL	*gp)
{
	dlog_msg(__FILE__,__LINE__,"do_exit: %d", gp->wid); 
}

/*
 * Here to do an idle task when nothing else is happening.
 * Just draw a randomly colored filled circle in the small window.
 */
void
do_idle(void)
{
}
