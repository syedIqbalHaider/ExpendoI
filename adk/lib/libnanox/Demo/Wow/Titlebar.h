#include "nano-X.h"

#ifndef TITLEBAR
#define TITLEBAR

#define TITLEBAR_WIDTH 		55
#define TITLEBAR_HEIGHT		238


void addTitlebar (GR_WINDOW_ID wBase, GR_WINDOW_ID *wTitlebar, GR_GC_ID *gcTitlebar);
void get_timedate(GR_WINDOW_ID td_win, GR_GC_ID td_gc);
void update_titlebar (GR_WINDOW_ID wTitlebar, GR_GC_ID gcTitlebar);

#endif
