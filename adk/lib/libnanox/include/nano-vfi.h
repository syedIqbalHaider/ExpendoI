#ifndef NANO_VFI_H
#define NANO_VFI_H

#include "device.h"
#include "stddef.h"

#define MWLF_CLASS_DLL		1000	/* Font in DLL (shared lib) */

#if defined(VFI_PLATFORM_VX) || defined(VFI_PLATFORM_VERIXEVO)
#	define MW_FONT_DLL_EXT		".lib"
#else
	/* TODO: .so for Linux etc. */
#	define MW_FONT_DLL_EXT		".dll"
#endif

typedef enum 
{
	MW_FONT_DLL_CFONT_DLL = 0,
	MW_FONT_DLL_COREFONT_DLL = 1,
	MW_FONT_DLL_BUF_GLYPH = 2,

	MW_FONT_DLL_BUF_REST = 1000
} MWFontDLLBuf_e;

typedef struct 
{
	unsigned short startChar;
	unsigned short endChar;
	unsigned short startIndex;
} CharRangeEntry;

typedef struct
{
	unsigned char prefix;
	unsigned char postfix;
} GlyphZeroFill;


typedef struct
{
	MWCOREFONT corefont;

	/* This is the extension space the code can use for its data */
	unsigned char extensionSpace[0x40];
} MWCOREFONT_DLL;

typedef struct
{
	MWCFONT font;
	
	/*** VeriFone extensions ***/
	
	CharRangeEntry const * char_ranges;	/* range information used to reference in all other tables*/
	size_t char_ranges_cnt;					/* number of ranges stored above*/
	
	GlyphZeroFill const * zero_fill;	/* number of zero bytes to prefix/postfix the image data */
	
	unsigned char const * compressed_bits;	/* bytes of image (prefix and postfix zeros cut out) */

	unsigned char offset_step;		/* step between each offset entry index - originally it is 1 - every glyph has its entry in the offset table */
	unsigned char needs_padding;	/* padding was removed from compressed_bits - only the bytes for actual len are used */
	
} MWCFONT_DLL;

#endif /* NANO_VFI_H */
