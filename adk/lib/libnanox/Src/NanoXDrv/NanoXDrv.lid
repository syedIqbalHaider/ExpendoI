#--------------------------------------------------------------------------- 
#	
#	Filename:	TridDrv.lid
#
#	Product:	nanoX
#
#	Author:		Tomasz_S1
#	Created:	2010-07-30
#
#	Description:  Description for Trident color driver shared PIC lib. 
#
#*******************************************************************/

# NOTE: id=27 is assigned to N:/CTLS.LIB but since GUIAPP should NEVER
# use it it is paradoxally a best choice as it lowers the possibility
# of getting another system library with same id.

NanoXDrv	id=27 ver=1.0 thumb

1	GrOpen
2	GrCreateFontEx
3	GrGetScreenInfo
4	GrNewWindow
5	GrMapWindow
6	GrSetGCFont
7	GrSetGCUseBackground
8	GrSetGCForeground
9	GrSetGCBackground
10	GrFlush
11	GrGetGCTextSize
12	GrClearArea
13	GrText
14	GrGetImageInfo
15	GrDrawImagePartToFit
16	GrFreeImage
17	GrLine
18	GuiDisplay_Refresh
19	GrDestroyFont
20	GrGetFontInfo
21	GrGetWindowInfo
22	GrLoadImageFromFile
23	GrNewGC
24	GrPoint
25	GrSetFontAttr
26	GrSetFontSizeEx
27  GIFplayAnimation
28  GIFdestroyAnimation
29  GrArcAngle
30	GrFillRect
31	GrSetBackgroundPixmap
32	GrDestroyGC
33	GrDrawImageFromFile
34	GrNewPixmapEx
35	GrGetGCInfo
36	GrRaiseWindow
37	GrLowerWindow
38	GrSetFocus
39	GrUnmapWindow
40	GrDestroyWindow
41	GrMoveWindow
42	GrResizeWindow
43	GrSetWMProperties
44	GrPoints
45	GrFreeze
