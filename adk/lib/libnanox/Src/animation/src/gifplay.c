/*
	mngplay

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <svc.h>

#include <nano-X.h>
#include <device.h>

#include <gifplay.h>

static int threadEntry(int param);

gifanimstuff* GIFplayAnimation(char *filename, GR_WINDOW_ID winID, GR_GC_ID gcID, int x, int y)
{
	gifanimstuff	*gifanim;

	/* allocate our stream data structure */
	gifanim = (gifanimstuff*)calloc(1, sizeof(*gifanim));
	if (gifanim == NULL) {
		dlog_msg("could not allocate stream structure.");
		return NULL;
	}

	/* pass the name of the file we want to play */
	gifanim->filename = malloc(strlen(filename) + 1);
	strcpy(gifanim->filename, filename);
	gifanim->winID = winID;
	gifanim->gcID = gcID;
    gifanim->x = x;
    gifanim->y = y;

	// initialize to unavailable!
	sem_init(&gifanim->actionGuard_, 0);

	gifanim->masterThreadId_ = get_task_id();

	// start thread
	// 4k should be enough for everybody ;-)
	gifanim->slaveThreadId_ = run_thread((int)threadEntry, (int)gifanim, 4 * 1024);
	if (gifanim->slaveThreadId_ < 0)
	{
		int errnoSnapshot = errno;
		dlog_error("Unable to run thread %i, errno=%i!", gifanim->slaveThreadId_, errnoSnapshot);
		GIFdestroyAnimation(gifanim);
		return NULL;
	}

	// we have a thread started

	return gifanim;
}


int GIFdestroyAnimation(gifanimstuff* gifanimstuff_)
{

	if(gifanimstuff_->slaveThreadId_ > 0)
	{
		if (gifanimstuff_->stopThread == 0)
		{
			gifanimstuff_->stopThread = 1;
			post_user_event(gifanimstuff_->slaveThreadId_, 1);
			sem_wait(&gifanimstuff_->actionGuard_);
		}	
	}

	free(gifanimstuff_->filename);

	/* free our data */
	free(gifanimstuff_);
	
	/* quit */
	return 0;
}

static int gifrefresh(gifanimstuff* gifanim, int frameNo, int* frameDelay)
{
	GR_IMAGE_ID imgID = 0;

	imgID = GrLoadImageFromFile(gifanim->filename, frameNo);

	if (imgID > 0)
	{
		GR_IMAGE_INFO imInfo;
		GrGetImageInfo(imgID, &imInfo);
		*frameDelay = imInfo.frameDelay;

		GrClearArea(gifanim->winID, gifanim->x, gifanim->y, imInfo.width, imInfo.height, 0);

		//dlog_msg("%s: Displaying %s frame %d at x: %d y: %d w: %d h: %d imgID: %d transcolor: %d frameDelay: %d",
		//		__func__, gifanim->filename, frameNo, gifanim->x, gifanim->y, imInfo.width, imInfo.height, imgID, imInfo.transcolor, imInfo.frameDelay);

		GrDrawImageToFit(gifanim->winID, gifanim->gcID, gifanim->x, gifanim->y, -1, -1, imgID);
		GrFreeImage (imgID);
		return 0;
	}

	return -1;
}

static int threadEntry(int param)
{
	gifanimstuff* gifanim = (gifanimstuff*) param;
	int delayTimeout = -1;
	int frameDelay = -1;
	unsigned int startTics = 0;
	int frameNo = 1;
	int frameCnt = 9999999;

	if (gifrefresh(gifanim, frameNo++, &frameDelay) < 0)
	{
		dlog_msg("%s: ERROR: Unable to load file: %s", __func__, gifanim);
		return 0;
	}

	startTics = read_ticks();
	if (frameDelay >= 0)
		delayTimeout = startTics + (frameDelay * 10) - read_ticks();

	/* loop though the frames */
	for (;;)
	{
		//dlog_msg("%s: frameDelay: %d delayTimeout: %d", __func__, frameDelay, delayTimeout);

		if (delayTimeout > 0)
		{
			int iTimer;

			read_evt(EVT_TIMER);
			iTimer = set_timer(delayTimeout, EVT_TIMER);
			wait_evt(EVT_USER | EVT_TIMER);
			clr_timer(iTimer);
		}
		else if (delayTimeout < 0)
		{
			wait_evt(EVT_USER);
		}

		if (gifanim->stopThread)
		{
			gifanim->stopThread = 2;
			sem_post(&gifanim->actionGuard_);
			return 0;
		}

		/* reset the delay in case the decoder
		 doesn't update it again */
		if (gifrefresh(gifanim, frameNo, &frameDelay) < 0)
		{
			frameCnt = frameNo - 1;

			frameNo = 1;
			frameDelay = -1;
			delayTimeout = 0;
			continue;
		}
		else
		{
			frameNo++;
			if (frameNo > frameCnt)
				frameNo = 1;
		}

		startTics = read_ticks();
		if (frameDelay >= 0)
			delayTimeout = startTics + (frameDelay * 10) - read_ticks();
		else
			delayTimeout = -1;

		/* check for user input (just quit at this point) */
		if (gifanim->stopThread)
		{
			gifanim->stopThread = 2;
			sem_post(&gifanim->actionGuard_);
			return 0;
		}
	}

	return 0;
}

