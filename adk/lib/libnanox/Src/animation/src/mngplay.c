/*
	mngplay

*/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include <svc.h>

#include <nano-X.h>
#include <device.h>

#include <libmng.h>

#include <mngplay.h>

/* callbacks for the mng decoder */

/* memory allocation; data must be zeroed */
static mng_ptr mymngalloc(mng_uint32 size)
{
	return (mng_ptr)calloc(1, size);
}

/* memory deallocation */
static void mymngfree(mng_ptr p, mng_uint32 size)
{
	free(p);
	return;
}

static mng_bool mymngopenstream(mng_handle mng)
{
	mngstuff	*mymng;

	/* look up our stream struct */
    mymng = (mngstuff*)mng_get_userdata(mng);
	
	/* open the file */
	mymng->file = fopen(mymng->filename, "rb");
	if (mymng->file == NULL) {
		dlog_error("unable to open '%s'", mymng->filename);
		return MNG_FALSE;
	}

	return MNG_TRUE;
}

static mng_bool mymngclosestream(mng_handle mng)
{
	mngstuff	*mymng;

	dlog_msg("mymngclosestream");

	/* look up our stream struct */
    mymng = (mngstuff*)mng_get_userdata(mng);

	/* close the file */
	fclose(mymng->file);
	mymng->file = NULL;	/* for safety */
	
	return MNG_TRUE;
}

/* feed data to the decoder */
static mng_bool mymngreadstream(mng_handle mng, mng_ptr buffer,
		mng_uint32 size, mng_uint32 *bytesread)
{
	mngstuff *mymng;

	/* look up our stream struct */
	mymng = (mngstuff*)mng_get_userdata(mng);

	/* read the requested amount of data from the file */
	*bytesread = fread(buffer, 1, size, mymng->file);

	return MNG_TRUE;
}

/* the header's been read. set up the display stuff */
static mng_bool mymngprocessheader(mng_handle mng,
		mng_uint32 width, mng_uint32 height)
{
	mngstuff	*mymng;

//	fprintf(stderr, "our mng is %dx%d\n", width,height);

	/* retreive our user data */
 	mymng = (mngstuff*)mng_get_userdata(mng);

	if(mymng->pimage)
	{
		mymngfree(mymng->pimage->imagebits, 0);
		mymngfree(mymng->pimage, 0);
	}
	
	mymng->pimage = mymngalloc(sizeof(GR_IMAGE_HDR));
#if (1)
    mymng->pimage->flags = PSF_IMAGEHDR;		/* PSF_IMAGEHDR*/
	mymng->pimage->width = width;		/* image width in pixels*/
	mymng->pimage->height = height;		/* image height in pixels*/
	mymng->pimage->planes = 1;		/* # image planes*/
	mymng->pimage->bpp = 16;		/* bits per pixel*/
	mymng->pimage->data_format = MWIF_RGB565;/* MWIF_ image data format*/
	mymng->pimage->pitch = mymng->pimage->width*mymng->pimage->bpp/8;	/* bytes per line*/
	mymng->pimage->imagebits = mymngalloc(mymng->pimage->width*mymng->pimage->height*mymng->pimage->bpp/8);	/* image bits (dword padded)*/
	mymng->pimage->palsize = 0;	/* palette size*/
	mymng->pimage->palette = NULL;/* palette*/
	mymng->pimage->transcolor = MWNOCOLOR;/* transparent color or MWNOCOLOR if none*/

	mng_set_canvasstyle(mng, MNG_CANVAS_RGB565);
#endif

#if (0)
    mymng->pimage->flags = PSF_IMAGEHDR;		/* PSF_IMAGEHDR*/
	mymng->pimage->width = width;		/* image width in pixels*/
	mymng->pimage->height = height;		/* image height in pixels*/
	mymng->pimage->planes = 1;		/* # image planes*/
	mymng->pimage->bpp = 24;		/* bits per pixel*/
	mymng->pimage->data_format = MWIF_BGRA8888;/* MWIF_ image data format*/
	mymng->pimage->pitch = mymng->pimage->width*mymng->pimage->bpp/8;	/* bytes per line*/
	mymng->pimage->imagebits = mymngalloc(mymng->pimage->width*mymng->pimage->height*mymng->pimage->bpp/8);	/* image bits (dword padded)*/
	mymng->pimage->palsize = 0;	/* palette size*/
	mymng->pimage->palette = NULL;/* palette*/
	mymng->pimage->transcolor = MWNOCOLOR;/* transparent color or MWNOCOLOR if none*/

	mng_set_canvasstyle(mng, MNG_CANVAS_BGRA8);
#endif

#if (0)
    mymng->pimage->flags = PSF_IMAGEHDR;		/* PSF_IMAGEHDR*/
	mymng->pimage->width = width;		/* image width in pixels*/
	mymng->pimage->height = height;		/* image height in pixels*/
	mymng->pimage->planes = 1;		/* # image planes*/
	mymng->pimage->bpp = 16;		/* bits per pixel*/
	mymng->pimage->data_format = MWIF_RGB888;/* MWIF_ image data format*/
	mymng->pimage->pitch = mymng->pimage->width*mymng->pimage->bpp/8;	/* bytes per line*/
	mymng->pimage->imagebits = mymngalloc(mymng->pimage->width*mymng->pimage->height*mymng->pimage->bpp/8);	/* image bits (dword padded)*/
	mymng->pimage->palsize = 0;	/* palette size*/
	mymng->pimage->palette = NULL;/* palette*/
	mymng->pimage->transcolor = MWNOCOLOR;/* transparent color or MWNOCOLOR if none*/

	mng_set_canvasstyle(mng, MNG_CANVAS_RGB8);
#endif


	return MNG_TRUE;
}

/* return a row pointer for the decoder to fill */
static mng_ptr mymnggetcanvasline(mng_handle mng, mng_uint32 line)
{
	mngstuff	*mymng;
	mng_ptr		row;

	/* dereference our structure */
	mymng = (mngstuff*)mng_get_userdata(mng);

	/* we assume any necessary locking has happened 
	   outside, in the frame level code */
	row = mymng->pimage->imagebits + mymng->pimage->pitch*line;

	memset(row, 0xFF, mymng->pimage->pitch);

//	fprintf(stderr, "   returning pointer to line %d (%p)\n", line, row);
 
	return (row);	
}

/* timer */
static mng_uint32 mymnggetticks(mng_handle mng)
{
	mng_uint32 ticks;

	ticks = (mng_uint32)read_ticks();
//	fprintf(stderr, "  %d\t(returning tick count)\n",ticks);

	return(ticks);
}

static mng_bool mymngrefresh(mng_handle mng, mng_uint32 x, mng_uint32 y,
			mng_uint32 w, mng_uint32 h)
{

	mngstuff	*mymng;
	int i, cnt;

    if(mng == MNG_NULL)
	{
		dlog_error("mymngrefresh mng=NULL");
		return MNG_FALSE;
	}

	/* dereference our structure */
	mymng = (mngstuff*)mng_get_userdata(mng);

	if(mymng == NULL || mymng->pimage == NULL)
	{
//		dlog_error("mymngrefresh (mymng=%p or pimage=%p) = NULL", mymng, mymng ? mymng->pimage : NULL);
		dlog_error("mymngrefresh (mymng or pimage) = NULL");
		return MNG_FALSE;
	}

	cnt = mymng->pimage->width*mymng->pimage->height*mymng->pimage->bpp/8;

//	for(i = 0; i < cnt; i++)
//		mymng->pimage->imagebits[i] = 0xFF - mymng->pimage->imagebits[i];

	GrDrawImageBits(mymng->winID, mymng->gcID, mymng->x, mymng->y, mymng->pimage);

	return MNG_TRUE;
}

/* interframe delay callback */
static mng_bool mymngsettimer(mng_handle mng, mng_uint32 msecs)
{
	mngstuff	*mymng;

//	fprintf(stderr,"  pausing for %d ms\n", msecs);
	
	/* look up our stream struct */
    mymng = (mngstuff*)mng_get_userdata(mng);

	/* set the timer for when the decoder wants to be woken */
	mymng->delay = msecs;

	return MNG_TRUE;
	
}

static mng_bool mymngerror(mng_handle mng, mng_int32 code, mng_int8 severity,
	mng_chunkid chunktype, mng_uint32 chunkseq,
	mng_int32 extra1, mng_int32 extra2, mng_pchar text)
{
	mngstuff	*mymng;
	char		chunk[5];
	
        /* dereference our data so we can get the filename */
        mymng = (mngstuff*)mng_get_userdata(mng);

	/* pull out the chuck type as a string */
	// FIXME: does this assume unsigned char?
	chunk[0] = (char)((chunktype >> 24) & 0xFF);
	chunk[1] = (char)((chunktype >> 16) & 0xFF);
	chunk[2] = (char)((chunktype >>  8) & 0xFF);
	chunk[3] = (char)((chunktype      ) & 0xFF);
	chunk[4] = '\0';

	/* output the error */
	dlog_msg("error playing '%s' chunk %s (%d) - %s:",
		mymng->filename, chunk, chunkseq, text);

	return (0);
}

static int threadEntry(int param)
{
	mngstuff	*mymng = (mngstuff *)param;


	mng_readdisplay(mymng->mnghdl);

	/* loop though the frames */
	while (mymng->delay) {
//		fprintf(stderr, "  waiting for %d ms\n", mymng->delay);
		SVC_WAIT(mymng->delay);

		if(mymng->stopThread)
		{
			return 0;
		}

		/* reset the delay in case the decoder
		   doesn't update it again */
		mymng->delay = 0;

		mng_display_resume((mng_handle)mymng->mnghdl);

		/* check for user input (just quit at this point) */
//		checkevents(mng);
		if(mymng->stopThread)
			return 0;
	}

	return 0;
}


int mngdestroy(mngstuff	*mngstuff_)
{

	if(mngstuff_->slaveThreadId_ > 0)
		mngstuff_->stopThread = 1;


	/* cleanup. this will call mymngclosestream */
	if(mngstuff_->mnghdl)
	    mng_cleanup((mng_handle *)&mngstuff_->mnghdl);


	if(mngstuff_->pimage)
	{
		mymngfree(mngstuff_->pimage->imagebits, 0);
		mymngfree(mngstuff_->pimage, 0);
	}


	/* free our data */
	free(mngstuff_);
	
	/* quit */
	return 0;
}



mngstuff *mng_play(char *filename, GR_WINDOW_ID winID, GR_GC_ID gcID, int x, int y)
{
	mngstuff	*mymng;

	/* allocate our stream data structure */
	mymng = (mngstuff*)calloc(1, sizeof(*mymng));
	if (mymng == NULL) {
		dlog_msg("could not allocate stream structure.");
		return NULL;
	}

	/* pass the name of the file we want to play */
	mymng->filename = filename;
	mymng->winID = winID;
	mymng->gcID = gcID;
    mymng->x = x;
    mymng->y = y;


	/* set up the mng decoder for our stream */
        mymng->mnghdl = (mng_handle)mng_initialize(mymng, mymngalloc, mymngfree, MNG_NULL);
        if (mymng->mnghdl == MNG_NULL) {
				mngdestroy(mymng);
                dlog_msg("could not initialize libmng.");
                return NULL;
        }

	/* set the callbacks */
	mng_setcb_errorproc((mng_handle)mymng->mnghdl, mymngerror);
	mng_setcb_openstream((mng_handle)mymng->mnghdl, mymngopenstream);
	mng_setcb_closestream((mng_handle)mymng->mnghdl, mymngclosestream);
	mng_setcb_readdata((mng_handle)mymng->mnghdl, mymngreadstream);
	mng_setcb_gettickcount((mng_handle)mymng->mnghdl, mymnggetticks);
	mng_setcb_settimer((mng_handle)mymng->mnghdl, mymngsettimer);
	mng_setcb_processheader((mng_handle)mymng->mnghdl, mymngprocessheader);
	mng_setcb_getcanvasline((mng_handle)mymng->mnghdl, mymnggetcanvasline);
	mng_setcb_refresh((mng_handle)mymng->mnghdl, mymngrefresh);

	mng_set_bgcolor((mng_handle)mymng->mnghdl, 0xFF, 0xFF, 0xFF);

	/* FIXME: should check for errors here */



	mymng->masterThreadId_ = get_task_id();

	// start thread
	// 4k should be enough for everybody ;-)
	mymng->slaveThreadId_ = run_thread((int)threadEntry, (int)mymng, 4 * 1024);
	if (mymng->slaveThreadId_ < 0)
	{
		int errnoSnapshot = errno;
		dlog_error ( "Unable to run thread %i, errno=%i!", mymng->slaveThreadId_, errnoSnapshot);
		mngdestroy(mymng);
		return NULL;
	}

	// we have a thread started

	return mymng;
}

