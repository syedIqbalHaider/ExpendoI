
#ifndef GIFPLAY_H
#define GIFPLAY_H

#include <nano-X.h>

#ifdef __cplusplus
extern "C"
{
#endif

/* structure for keeping track of our mng stream inside the callbacks */
typedef struct {
	char		*filename; /* pointer to the file's path/name */
	GR_WINDOW_ID winID;
	GR_GC_ID	 gcID;
	int 		x;
	int 		y;
	void *mnghdl;  /*mng_handle*/
	int masterThreadId_;
	int slaveThreadId_;
	int stopThread;
	sem_t actionGuard_;
} gifanimstuff;

gifanimstuff *GIFplayAnimation(char *filename, GR_WINDOW_ID winID, GR_GC_ID gcID, int x, int y);
int GIFdestroyAnimation(gifanimstuff *gifanimstuff_);


#ifdef __cplusplus
}
#endif

#endif
