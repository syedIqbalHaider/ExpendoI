
#ifndef MNGPLAY_H
#define MNGPLAY_H

#include <nano-X.h>

#ifdef __cplusplus
extern "C"
{
#endif

/* structure for keeping track of our mng stream inside the callbacks */
typedef struct {
	FILE		*file;	   /* pointer to the file we're decoding */
	char		*filename; /* pointer to the file's path/name */
	unsigned int delay;     /* ticks to wait before resuming decode */
	GR_WINDOW_ID winID;
	GR_GC_ID	 gcID;
	int 		x;
	int 		y;
	GR_IMAGE_HDR *pimage;
	void *mnghdl;  /*mng_handle*/
	int masterThreadId_;
	int slaveThreadId_;
	int stopThread;
} mngstuff;

mngstuff *mng_play(char *filename, GR_WINDOW_ID winID, GR_GC_ID gcID, int x, int y);
int mngdestroy(mngstuff	*mngstuff_);


#ifdef __cplusplus
}
#endif

#endif
