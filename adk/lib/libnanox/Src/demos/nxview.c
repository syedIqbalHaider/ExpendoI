/*
 * Copyright (c) 2000, 2001 Greg Haerr <greg@censoft.com>
 *
 * nxview - Nano-X image viewer
 *
 * Autorecognizes and displays BMP, GIF, JPEG, PNG and XPM files
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MWINCLUDECOLORS
#include "nano-X.h"

#if defined(VFI_PLATFORM_VX) || defined(VFI_PLATFORM_VERIXEVO)
#include <svc.h>
#include <liblog/logsys.h>

int m_console = -1;
const char *g_apLogicalName = "DEMO";
#endif

int initConsole( void )
{
	int ret = 0;

	LOG_INIT( (char *)g_apLogicalName, LOGSYS_PIPE,LOGSYS_PRINTF_FILTER);
	dlog_msg(__FILE__, __LINE__, "hello world from %s!", g_apLogicalName );

    // this app and this app alone will own the console!
    m_console = open( "/dev/console", 0 );
    if( m_console < 0 )
    {
        dlog_error(__FILE__, __LINE__, "error opening console");
        ret = -1;		// error opening console
    }
	else
	{
	    //Open conn to nano-X
	    if (GrOpen () != -1)
	    {
	        dlog_msg(__FILE__,__LINE__,"Nano-X connection is open");			
	    }
		else
		{
	        dlog_error(__FILE__,__LINE__,"Nano-X connection is failed");	
			ret = -2;
		}	
	}
    return ret;
}

int
main(int argc,char **argv)
{
	GR_IMAGE_ID	image_id;
	GR_WINDOW_ID	window_id;
	GR_GC_ID	gc_id;
	GR_SIZE		w = -1;
	GR_SIZE		h = -1;
	GR_EVENT	event;
	GR_SCREEN_INFO	sinfo;
	GR_IMAGE_INFO	info;
	char		title[256];

	if (argc < 2) {
		printf("Usage: nxview <image file> [stretch]\n");
		exit(1);
	}

	initConsole();
	
	if (!(image_id = GrLoadImageFromFile(argv[1], 0))) {
		fprintf(stderr, "Can't load image file: %s\n", argv[1]);
		exit(1);
	}

	if(argc > 2) {
		/* stretch to half screen size*/
		GrGetScreenInfo(&sinfo);
		w = sinfo.cols/2;
		h = sinfo.rows/2;
	} else {
		GrGetImageInfo(image_id, &info);
		w = info.width;
		h = info.height;
	}

	sprintf(title, "nxview %s", argv[1]);
	window_id = GrNewWindowEx(GR_WM_PROPS_APPWINDOW, title,
		GR_ROOT_WINDOW_ID, 0, 0, w, h, BLACK);

	GrSelectEvents(window_id,
		GR_EVENT_MASK_CLOSE_REQ|GR_EVENT_MASK_EXPOSURE);

	GrMapWindow(window_id);

	gc_id = GrNewGC();

	while (1) {
		GrGetNextEvent(&event);
		switch(event.type) {
		case GR_EVENT_TYPE_CLOSE_REQ:
			GrDestroyWindow(window_id);
			GrDestroyGC(gc_id);
			GrFreeImage(image_id);
			GrClose();
			exit(0);
			/* no return*/
		case GR_EVENT_TYPE_EXPOSURE:
			GrDrawImageToFit(window_id, gc_id, 0,0, w,h, image_id);
			break;
		}
	}

	return 0;
}
