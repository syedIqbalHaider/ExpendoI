/*
 * pcfdemo - demonstrate PCF font loading for Nano-X
 */
#include <stdio.h>
#include <stdlib.h>

#if defined(VFI_PLATFORM_VX) || defined(VFI_PLATFORM_VERIXEVO)
#include <svc.h>
#include <liblog/logsys.h>

int m_console = -1;
const char *g_apLogicalName = "DEMO";
#else
#include <unistd.h>
#endif

#define MWINCLUDECOLORS
#include "nano-X.h"

GR_FONT_ID font = 0;
GR_WINDOW_ID main_wid;
GR_FONT_INFO finfo;

int initConsole( void )
{
	int ret = 0;

	LOG_INIT( (char *)g_apLogicalName, LOGSYS_PIPE,LOGSYS_PRINTF_FILTER);
	dlog_msg(__FILE__, __LINE__, "hello world from %s!", g_apLogicalName );

    // this app and this app alone will own the console!
    m_console = open( "/dev/console", 0 );
    if( m_console < 0 )
    {
        dlog_error(__FILE__, __LINE__, "error opening console");
        ret = -1;		// error opening console
    }
	else
	{
	    //Open conn to nano-X
	    if (GrOpen () != -1)
	    {
	        dlog_msg(__FILE__,__LINE__,"Nano-X connection is open");			
	    }
		else
		{
	        dlog_error(__FILE__,__LINE__,"Nano-X connection is failed");	
			ret = -2;
		}	
	}
    return ret;
}

static void
draw_string(void)
{
	int count = 0;
	int x = 0;
	int y = 10;
	unsigned char ch;
	GR_GC_ID gc = GrNewGC();

	GrSetGCFont(gc, font);
	GrSetGCForeground(gc, GR_RGB(255, 255, 255));
	GrSetGCBackground(gc, GR_RGB(0, 0, 0));

	printf("First char = %d, last char = %d\n", finfo.firstchar,finfo.lastchar);
	printf("Max width = %d, max height = %d\n", finfo.maxwidth,finfo.height);

	for (ch = 0; ch < 255; ch++) {
		if (ch < finfo.firstchar || ch > finfo.lastchar)
			GrFillRect(main_wid, gc, x, y, finfo.maxwidth,finfo.height);
		else
			GrText(main_wid, gc, x, y, &ch, 1, GR_TFTOP | GR_TFASCII);

		if (++count >= 16) {
			x = 0;
			y += finfo.height;
			count = 0;
		} else
			x += finfo.maxwidth + 2;
	}

	GrDestroyGC(gc);
}

int
main(int argc, char **argv)
{
	int width, height;

	if(initConsole() < 0) {
		dlog_error (__FILE__, __LINE__, "initConsole Failed");
		SVC_WAIT(5000);
		return (-1);
	}

	if (argc < 2) {
		dlog_error (__FILE__, __LINE__, "Not enough args: %d", argc);
		SVC_WAIT(5000);
		return (-1);
	}
	
	font = GrCreateFont((GR_CHAR*)argv[1], 12, 0);
	if (!font)	{
		dlog_error (__FILE__, __LINE__, "Unable to load %s", argv[1]);
		SVC_WAIT(2000);
	}
	GrGetFontInfo(font, &finfo);

	width = ((finfo.maxwidth + 2) * 16);
	height = (((finfo.lastchar - finfo.firstchar) / 16) + 5) * finfo.height;

	main_wid = GrNewWindowEx(GR_WM_PROPS_APPWINDOW, "pcfdemo", GR_ROOT_WINDOW_ID, 0, 0, width, height, BLACK_MW);
	GrSelectEvents(main_wid, GR_EVENT_MASK_EXPOSURE|GR_EVENT_MASK_CLOSE_REQ);
	GrMapWindow(main_wid);

	while (1) {
		GR_EVENT event;
		GrGetNextEvent(&event);

		if (event.type == GR_EVENT_TYPE_EXPOSURE)
			draw_string();

	        if(event.type == GR_EVENT_TYPE_CLOSE_REQ) {
			GrClose();
			exit(0);
	      }
	}
}
