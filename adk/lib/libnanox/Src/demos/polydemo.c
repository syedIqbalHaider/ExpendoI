/*
 * polytest - polygon fill test program for Nano-X
 */
#include <stdio.h>
#include <stdlib.h>
#define MWINCLUDECOLORS
#include "nano-X.h"

#if defined(VFI_PLATFORM_VX) || defined(VFI_PLATFORM_VERIXEVO)
#include <svc.h>
#include <liblog/logsys.h>

int m_console = -1;
const char *g_apLogicalName = "DEMO";
#else
#include <unistd.h>
#endif


int initConsole( void )
{
	int ret = 0;

	LOG_INIT( (char *)g_apLogicalName, LOGSYS_PIPE,LOGSYS_PRINTF_FILTER);
	dlog_msg(__FILE__, __LINE__, "hello world from %s!", g_apLogicalName );

    // this app and this app alone will own the console!
    m_console = open( "/dev/console", 0 );
    if( m_console < 0 )
    {
        dlog_error(__FILE__, __LINE__, "error opening console");
        ret = -1;		// error opening console
    }
	else
	{
	    //Open conn to nano-X
	    if (GrOpen () != -1)
	    {
	        dlog_msg(__FILE__,__LINE__,"Nano-X connection is open");			
	    }
		else
		{
	        dlog_error(__FILE__,__LINE__,"Nano-X connection is failed");	
			ret = -2;
		}	
	}
    return ret;
}

static void
draw(GR_EVENT * e)
{
	GR_WINDOW_ID wid = ((GR_EVENT_EXPOSURE *)e)->wid;
	GR_GC_ID gc = GrNewGC();
	GR_POINT points[4];
	int x = 10;
	int y = 10;
	int sz = 20;
	int sz2 = 5;

	GrSetGCBackground(gc, BLACK_MW);

	/* fill poly #1*/
	points[0].x = x;
	points[0].y = y;
	points[1].x = x + sz;
	points[1].y = y;
	points[2].x = x + (sz / 2);
	points[2].y = y + sz;
	points[3].x = x;
	points[3].y = y;

	GrSetGCForeground(gc, WHITE_MW);
	GrFillPoly(wid, gc, 3, points);

	/* outline poly #1*/
	GrSetGCForeground(gc, GREEN);
	GrPoly(wid, gc, 4, points);

	/* fill poly #2*/
	y += sz + 10;
	points[0].x = x;
	points[0].y = y;
	points[1].x = x + sz + 1;
	points[1].y = y;
	points[2].x = x + (sz / 2);
	points[2].y = y + sz;
	points[3].x = x;
	points[3].y = y;

	GrSetGCForeground(gc, WHITE_MW);
	GrFillPoly(wid, gc, 3, points);

	/* outline poly #2*/
	GrSetGCForeground(gc, GREEN);
	GrPoly(wid, gc, 4, points);

	/* fill poly #3*/
	y += sz + 10;
	points[0].x = x;
	points[0].y = y;
	points[1].x = x + sz - 1;
	points[1].y = y;
	points[2].x = x + (sz / 2);
	points[2].y = y + sz;
	points[3].x = x;
	points[3].y = y;

	GrSetGCForeground(gc, WHITE_MW);
	GrFillPoly(wid, gc, 3, points);

	/* outline poly #3*/
	GrSetGCForeground(gc, GREEN);
	GrPoly(wid, gc, 4, points);

	/* fill right arrow #1*/
	x = 60;
	y = 60;
	sz = 10;
	sz2 = 8;

	points[0].x = x;
	points[0].y = y;
	y -= sz;
	points[1].x = x + sz2;
	points[1].y = y;
	y -= sz;
	points[2].x = x;
	points[2].y = y;
	points[3].x = x;
	points[3].y = 60;

	GrSetGCForeground(gc, WHITE_MW);
	GrFillPoly(wid, gc, 3, points);

	/* outline right arrow #1*/
	GrSetGCForeground(gc, GREEN);
	GrPoly(wid, gc, 4, points);

	/* fill right arrow #2*/
	x = 60;
	y = 90;
	points[0].x = x;
	points[0].y = y;
	y -= sz;
	points[1].x = x + sz2;
	points[1].y = y;
	y -= sz;
	points[2].x = x;
	points[2].y = y;
	points[3].x = x;
	points[3].y = 90;

	GrSetGCForeground(gc, WHITE_MW);
	GrFillPoly(wid, gc, 3, points);

	/* concave polygon filling*/
	{
	static GR_POINT pt1[5] = {{10,120},{150,130},{120,220},{60,160},{15,200}};
	static GR_POINT pt2[5] = {{10,220},{150,230},{120,320},{60,360},{15,300}};
 
	/* concave poly fill #4 - fails with some algorithms*/
	GrSetGCForeground(gc, WHITE_MW);
	GrFillPoly(wid,gc,5,pt1);
	 
	/* concave poly outline #4 - ok*/
	GrSetGCForeground(gc, GREEN);
	GrPoly(wid,gc,5,pt1);

	/* convex poly fill #5 - ok*/
	GrSetGCForeground(gc, WHITE_MW);
	GrFillPoly(wid,gc,5,pt2);

	/* convex poly outline #5 - ok*/
	GrSetGCForeground(gc, GREEN);
	GrPoly(wid,gc,5,pt2);
	}

	GrDestroyGC(gc);
}

int
main(int ac, char **av)
{
	GR_EVENT event;
	GR_WINDOW_ID w;

	if(initConsole() < 0) {
		fprintf(stderr, "cannot open graphics\n");
		exit(1);
	}

	/* create window */
	w = GrNewWindowEx(GR_WM_PROPS_NOAUTOMOVE | GR_WM_PROPS_BORDER |
			      GR_WM_PROPS_CAPTION | GR_WM_PROPS_CLOSEBOX,
			      "polydemo", GR_ROOT_WINDOW_ID, 
			      10, 10, 220, 362, GR_RGB(0, 0, 0));
	GrSelectEvents(w, GR_EVENT_MASK_EXPOSURE | GR_EVENT_MASK_CLOSE_REQ);
	GrMapWindow(w);

	while (1) {
		GrGetNextEvent(&event);

		switch (event.type) {
		case GR_EVENT_TYPE_EXPOSURE:
			draw(&event);
			break;
		case GR_EVENT_TYPE_CLOSE_REQ:
			GrClose();
			exit(0);
		}
	}

}
