#include <svc.h>
#include <stdio.h>
#include <stdlib.h>
#include <liblog/logsys.h>

#define MWINCLUDECOLORS
#include <nano-X.h>
#include <Nxcolors.h>

int m_console = -1;
const char *g_apLogicalName = "DEMO";

int initConsole( void )
{
	int ret = 0;

	LOG_INIT( (char *)g_apLogicalName, LOGSYS_PIPE,LOGSYS_PRINTF_FILTER);
	dlog_msg(__FILE__, __LINE__, "hello world from %s!", g_apLogicalName );

    // this app and this app alone will own the console!
    m_console = open( "/dev/console", 0 );
    if( m_console < 0 )
    {
        dlog_error(__FILE__, __LINE__, "error opening console");
        ret = -1;		// error opening console
    }
	else
	{
	    //Open conn to nano-X
	    if (GrOpen () != -1)
	    {
	        dlog_msg(__FILE__,__LINE__,"Nano-X connection is open");			
	    }
		else
		{
	        dlog_error(__FILE__,__LINE__,"Nano-X connection is failed");	
			ret = -2;
		}	
	}
    return ret;
}

int
main(int argc, char **argv)
{
	GR_SCREEN_INFO	si; 	/* information about screen */
	GR_WINDOW_ID 	w, w2;
	GR_GC_ID	gc;
	GR_EVENT	event;		/* current event */
	GR_WM_PROPERTIES props;
	GR_FONT_ID	font;

	initConsole();
	GrGetScreenInfo(&si);

	/* pass errors through main loop*/
	GrSetErrorHandler(NULL);

	w = GrNewWindow(GR_ROOT_WINDOW_ID, 20, 20, si.cols-40, si.rows-40,	0, GREEN, BLACK_MW);

	w2 = GrNewWindow(w, 20, 20, 40, 40, 0, BLUE, BLACK_MW);

	props.flags = GR_WM_FLAGS_PROPS | GR_WM_FLAGS_TITLE;
	props.props = GR_WM_PROPS_NOBACKGROUND;
	props.title = "Nano-X Demo2";
	GrSetWMProperties(w, &props);

	gc = GrNewGC();
	//font = GrCreateFont("lubI24.fnt", 0, NULL);
	font = GrCreateFont("verdana_12.iso-8859-15.pcf", 0, NULL);
	GrSetGCFont(gc, font);

	GrSelectEvents(w, GR_EVENT_MASK_EXPOSURE | GR_EVENT_MASK_CLOSE_REQ
		| GR_EVENT_MASK_BUTTON_DOWN
		| GR_EVENT_MASK_KEY_DOWN | GR_EVENT_MASK_KEY_UP);
	GrMapWindow(w);
	GrSetFocus(w);
	/* serious bug here: when wm running, w2 is mapped anyway!!*/
	/*GrMapWindow(w2);*/

	for (;;) {
		/*GR_EVENT_KEYSTROKE *kev;*/

		GrGetNextEvent(&event);
		switch (event.type) {
		case GR_EVENT_TYPE_EXPOSURE:
			GrSetGCForeground(gc,GrGetSysColor(GR_COLOR_APPWINDOW));
			GrFillRect(w, gc, event.exposure.x, event.exposure.y,
				event.exposure.width, event.exposure.height);
			GrSetGCForeground(gc, GrGetSysColor(GR_COLOR_APPTEXT));
			GrSetGCUseBackground(gc, GR_FALSE);
			GrText(w, gc, 10, 30, "Hello World @MNOPmn", -1, GR_TFASCII);
			GrRect(w, gc, 5, 5, 150, 60);
			break;
		case GR_EVENT_TYPE_CLOSE_REQ:
			GrClose();
			exit(0);
			break;
		case GR_EVENT_TYPE_ERROR:
			printf("\7demo2: Error (%s) ", event.error.name);
			printf(nxErrorStrings[event.error.code],event.error.id);
			break;
#if 1
		case GR_EVENT_TYPE_BUTTON_DOWN:
			GrUnmapWindow(w);
			GrFlush();
			//sleep(1);
			SVC_WAIT(1000);
			GrMapWindow(w);
			break;
#endif
#if 0
		case GR_EVENT_TYPE_BUTTON_DOWN:
			/* test server error on bad syscall*/
			GrMapWindow(w2);
			GrMoveWindow(GR_ROOT_WINDOW_ID, 0, 0);
			{ GR_SCREEN_INFO sinfo; GrGetScreenInfo(&sinfo); }
			break;
#endif
#if 0
		case GR_EVENT_TYPE_KEY_DOWN:
			kev = (GR_EVENT_KEYSTROKE *)&event;
			printf("DOWN %d (%04x) %04x\n",	kev->ch, kev->ch, kev->modifiers);
			break;
		case GR_EVENT_TYPE_KEY_UP:
			kev = (GR_EVENT_KEYSTROKE *)&event;
			printf("UP %d (%04x) %04x\n", kev->ch, kev->ch, kev->modifiers);
			break;
#endif
		}
	}

	GrClose();

	return 0;
}
