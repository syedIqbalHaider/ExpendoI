/*
 * A simple utility to set the portrait mode of the display.
 * Copyright (c) Alex Holden <alex@alexholden.net> 2002.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MWINCLUDECOLORS
#include "nano-X.h"

#if defined(VFI_PLATFORM_VX) || defined(VFI_PLATFORM_VERIXEVO)
#include <svc.h>
#include <liblog/logsys.h>

int m_console = -1;
const char *g_apLogicalName = "DEMO";
#else
#include <unistd.h>
#endif


int initConsole( void )
{
	int ret = 0;

	LOG_INIT( (char *)g_apLogicalName, LOGSYS_PIPE,LOGSYS_PRINTF_FILTER);
	dlog_msg(__FILE__, __LINE__, "hello world from %s!", g_apLogicalName );

    // this app and this app alone will own the console!
    m_console = open( "/dev/console", 0 );
    if( m_console < 0 )
    {
        dlog_error(__FILE__, __LINE__, "error opening console");
        ret = -1;		// error opening console
    }
	else
	{
	    //Open conn to nano-X
	    if (GrOpen () != -1)
	    {
	        dlog_msg(__FILE__,__LINE__,"Nano-X connection is open");			
	    }
		else
		{
	        dlog_error(__FILE__,__LINE__,"Nano-X connection is failed");	
			ret = -2;
		}	
	}
    return ret;
}

static void usage(void)
{
	fprintf(stderr, "Usage: setportrait <none|left|right|down>\n");
	exit(-1);
}

int main(int argc, char *argv[])
{
	int portrait = -1;

	if(argc != 2) usage();

	if(!strcmp("none", argv[1])) portrait = MWPORTRAIT_NONE;
	else if(!strcmp("left", argv[1])) portrait = MWPORTRAIT_LEFT;
	else if(!strcmp("right", argv[1])) portrait = MWPORTRAIT_RIGHT;
	else if(!strcmp("down", argv[1])) portrait = MWPORTRAIT_DOWN;
	else usage();

	if(initConsole() < 0) {
		fprintf(stderr, "Couldn't connect to Nano-X server\n");
		return -1;
	}

	GrSetPortraitMode(portrait);

	GrClose();

	return 0;
}
