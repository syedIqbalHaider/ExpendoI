#include <stdio.h>
#include <stdlib.h>

#define MWINCLUDECOLORS
#include "nano-X.h"

#if defined(VFI_PLATFORM_VX) || defined(VFI_PLATFORM_VERIXEVO)
#include <svc.h>
#include <liblog/logsys.h>

int m_console = -1;
const char *g_apLogicalName = "DEMO";
#else
#include <unistd.h>
#endif


int initConsole( void )
{
	int ret = 0;

	LOG_INIT( (char *)g_apLogicalName, LOGSYS_PIPE,LOGSYS_PRINTF_FILTER);
	dlog_msg(__FILE__, __LINE__, "hello world from %s!", g_apLogicalName );

    // this app and this app alone will own the console!
    m_console = open( "/dev/console", 0 );
    if( m_console < 0 )
    {
        dlog_error(__FILE__, __LINE__, "error opening console");
        ret = -1;		// error opening console
    }
	else
	{
	    //Open conn to nano-X
	    if (GrOpen () != -1)
	    {
	        dlog_msg(__FILE__,__LINE__,"Nano-X connection is open");			
	    }
		else
		{
	        dlog_error(__FILE__,__LINE__,"Nano-X connection is failed");	
			ret = -2;
		}	
	}
    return ret;
}

int main(int argc, char *argv[])
{
	GR_WINDOW_ID wid;
	GR_EVENT event;
	FILE *fp;
	char *buf = NULL;
	int buf_size = 0;
	int data_len = 0;
	int ret = 0;
	GR_EVENT_CLIENT_DATA_REQ *req;
	GR_EVENT_SELECTION_CHANGED *sc;

	if(argc != 2) {
		fprintf(stderr, "Usage: setselection <text file>\n");
		return 1;
	}

	if(!(fp = fopen(argv[1], "r"))) {
		fprintf(stderr, "Couldn't open text file\n");
		return 2;
	}

	do {
		data_len += ret;
		buf_size = data_len + 65536;
		if(!(buf = realloc(buf, buf_size))) {
			fprintf(stderr, "Out of memory\n");
			return 3;
		}
	} while((ret = fread(buf + data_len, 1, 65536, fp)) > 0);
	if(ret < 0) {
		fprintf(stderr, "Failed to read text file sucessfully\n");
		return 2;
	}

	if(initConsole() < 0) {
		fprintf(stderr, "Couldn't connect to Nano-X server\n");
		return 4;
	}

	wid = GrNewWindow(GR_ROOT_WINDOW_ID, 0, 0, 1, 1, 0, 0, 0);
	if(!wid) {
		fprintf(stderr, "Couldn't get a window\n");
		return 5;
	}

	GrSelectEvents(wid, GR_EVENT_MASK_CLIENT_DATA_REQ |
				GR_EVENT_MASK_SELECTION_CHANGED);

	GrSetSelectionOwner(wid, "nota/realtype text/plain non/existant "
				"something/else");

	while(1) {
		GrGetNextEvent(&event);
		switch(event.type) {
			case GR_EVENT_TYPE_CLIENT_DATA_REQ:
				req = &event.clientdatareq;
				fprintf(stderr, "Got request with serial "
					"number %ld from window %d for mime "
					"type %d\n", req->serial, req->rid,
							req->mimetype);
				GrSendClientData(wid, req->rid, req->serial,
						data_len, data_len, buf);
				break;
			case GR_EVENT_TYPE_SELECTION_CHANGED:
				sc = &event.selectionchanged;
				if(sc->new_owner != wid) {
					fprintf(stderr, "Lost selection to "
						"window %d\n", sc->new_owner);
					return 0;
				}
			default:
				break;
		}
	}

	return 0;
}
