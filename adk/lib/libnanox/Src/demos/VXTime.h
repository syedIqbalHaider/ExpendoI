#include <time.h>

#ifndef _SUSECONDS_T_DEFINED
#define _SUSECONDS_T_DEFINED
typedef long suseconds_t;
#endif

#ifndef _TM_DEFINED

struct timeval {
	time_t	tv_sec;
	suseconds_t	tv_usec;
};

struct timezone {
    int tz_minuteswest; /* minutes W of Greenwich */
    int tz_dsttime;     /* type of dst correction */
};

#define _TM_DEFINED
#endif

#define YEAR0                   1900
#define EPOCH_YR                1970
#define SECS_DAY                (24L * 60L * 60L)
#define LEAPYEAR(year)          (!((year) % 4) && (((year) % 100) || !((year) % 400)))
#define YEARSIZE(year)          (LEAPYEAR(year) ? 366 : 365)
#define FIRSTSUNDAY(timp)       (((timp)->tm_yday - (timp)->tm_wday + 420) % 7)
#define FIRSTDAYOF(timp)        (((timp)->tm_wday - (timp)->tm_yday + 420) % 7)

#define TIME_MAX                2147483647L

//char* ctime (time_t *t_ignore);
time_t time(time_t *t_ignore);
struct tm *localtime(const time_t *timer); 
time_t mktime(struct tm *tmbuf);
int gettimeofday(struct timeval *tv, struct timezone *tz);




