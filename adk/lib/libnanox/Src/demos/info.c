/*
 * Display the results of various GrGetsomethingInfo() calls.
 */

#include <stdio.h>
#include <conio.h>
#include "nano-X.h"

#if defined(VFI_PLATFORM_VX) || defined(VFI_PLATFORM_VERIXEVO)
#include <svc.h>
#include <liblog/logsys.h>
int m_console = -1;
const char *g_apLogicalName = "DEMO";
#endif

int initConsole( void )
{
	int ret = 0;

	LOG_INIT( (char *)g_apLogicalName, LOGSYS_PIPE,LOGSYS_PRINTF_FILTER);
	dlog_msg(__FILE__, __LINE__, "hello world from %s!", g_apLogicalName );

    // this app and this app alone will own the console!
    m_console = open( "/dev/console", 0 );
    if( m_console < 0 )
    {
        dlog_error(__FILE__, __LINE__, "error opening console");
        ret = -1;		// error opening console
    }
	else
	{
	    //Open conn to nano-X
	    if (GrOpen () != -1)
	    {
	        dlog_msg(__FILE__,__LINE__,"Nano-X connection is open");			
	    }
		else
		{
	        dlog_error(__FILE__,__LINE__,"Nano-X connection is failed");	
			ret = -2;
		}	
	}
    return ret;
}

int main()
{
	GR_SCREEN_INFO  si;
	GR_FONT_INFO    fi;
	GR_GC_INFO      gi;
	GR_FONT         fonts;
	int             x, y;

	initConsole();
	GrGetScreenInfo(&si);

	printf("rows = %d\n", si.rows);
	printf("cols = %d\n", si.cols);
	printf("bpp = %d\n", si.bpp);
	printf("planes = %d\n", si.planes);
	printf("ncolors = %d\n", si.ncolors);
	printf("buttons = 0x%x\n", si.buttons);
	printf("modifiers = 0x%x\n", si.modifiers);
	printf("fonts = %d\n", si.fonts);

	getch();

	for(fonts = 0; fonts < si.fonts; fonts++) {
/*		if(!GrGetFontInfo(fonts, &fi)) { */
		GrGetFontInfo(fonts, &fi);
		if(1) {
			printf("\nfont = %d\n", fi.font);
			printf("height = %d\n", fi.height);
			printf("maxwidth = %d\n", fi.maxwidth);
			printf("baseline = %d\n", fi.baseline);
			printf("fixed = %s\n", fi.fixed ? "TRUE" : "FALSE");
			printf("widths =\n");
			for(y = 0; y != 3; y++) {
				for(x = 0; x != 7; x++)
					printf("%2d", fi.widths[x * y]);
				printf("\n");

				getch();

			}
		}
	}

	getch();

	GrClose();
}
