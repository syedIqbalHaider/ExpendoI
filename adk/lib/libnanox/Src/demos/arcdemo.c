/*
 * Arc drawing demo for Nano-X
 *
 * Copyright (C) 2002 Alex Holden <alex@alexholden.net>
 * Modified by G Haerr
 */
#include <svc.h>
#include <stdlib.h>
#include <liblog/logsys.h>

#define MWINCLUDECOLORS
#include <nano-X.h>
#include <Nxcolors.h>

int m_console = -1;
const char *g_apLogicalName = "DEMO";

int initConsole( void )
{
	int ret = 0;

	LOG_INIT( (char *)g_apLogicalName, LOGSYS_PIPE,LOGSYS_PRINTF_FILTER);
	dlog_msg(__FILE__, __LINE__, "hello world from %s!", g_apLogicalName );

    // this app and this app alone will own the console!
    m_console = open( "/dev/console", 0 );
    if( m_console < 0 )
    {
        dlog_error(__FILE__, __LINE__, "error opening console");
        ret = -1;		// error opening console
    }
	else
	{
	    //Open conn to nano-X
	    if (GrOpen () != -1)
	    {
	        dlog_msg(__FILE__,__LINE__,"Nano-X connection is open");			
	    }
		else
		{
	        dlog_error(__FILE__,__LINE__,"Nano-X connection is failed");	
			ret = -2;
		}	
	}
    return ret;
}

static void
draw(GR_EVENT *ep)
{
	GR_WINDOW_ID wid = ((GR_EVENT_EXPOSURE *)ep)->wid;
	GR_GC_ID gc = GrNewGC();
	int x = 40;
	int y = 40;
	int rx = 30;
	int ry = 30;
	int xoff = (rx + 10) * 2;

#if 1
	/* filled arc*/
	GrSetGCForeground(gc, GREEN);
	GrArcAngle(wid, gc, x, y, 3, 3, 0, 0, GR_PIE);

	GrSetGCForeground(gc, BLACK);
	GrArcAngle(wid, gc, x, y, 3, 3, 0, 0, GR_ARC);

	GrSetGCForeground(gc, BLACK);
	GrPoint(wid, gc, x, y);
#else
	GrSetGCForeground(gc, GREEN);
	GrArc(wid, gc, x, y, rx, ry, 0, -30, -30, 0, GR_PIE);
	GrArc(wid, gc, x+5, y, rx, ry, 30, 0, 0, -30, GR_PIE);
	GrArc(wid, gc, x, y+5, rx, ry, -30, 0, 0, 30, GR_PIE);
	GrArc(wid, gc, x+5, y+5, rx, ry, 0, 30, 30, 0, GR_PIE);
#endif

	/* outlined arc*/
	GrSetGCForeground(gc, GREEN);
	x += xoff;
	GrArc(wid, gc, x, y, rx, ry, 0, -30, -30, 0, GR_ARCOUTLINE);
	GrArc(wid, gc, x+5, y, rx, ry, 30, 0, 0, -30, GR_ARCOUTLINE);
	GrArc(wid, gc, x, y+5, rx, ry, -30, 0, 0, 30, GR_ARCOUTLINE);
	GrArc(wid, gc, x+5, y+5, rx, ry, 0, 30, 30, 0, GR_ARCOUTLINE);

	/* arc only*/
	x += xoff;
	GrArc(wid, gc, x, y, rx, ry, 0, -30, -30, 0, GR_ARC);
	GrArc(wid, gc, x+5, y, rx, ry, 30, 0, 0, -30, GR_ARC);
	GrArc(wid, gc, x, y+5, rx, ry, -30, 0, 0, 30, GR_ARC);
	GrArc(wid, gc, x+5, y+5, rx, ry, 0, 30, 30, 0, GR_ARC);

	GrDestroyGC(gc);
}

int
main(int argc, char **argv)
{
	GR_SCREEN_INFO	si; 	/* information about screen */
	GR_EVENT ev;
	GR_WINDOW_ID wid;

	initConsole();
	GrGetScreenInfo(&si);

	wid = GrNewWindowEx(GR_WM_PROPS_BORDER|GR_WM_PROPS_CAPTION|
		GR_WM_PROPS_CLOSEBOX, "arcdemo",
		GR_ROOT_WINDOW_ID, 0, 0, 240, 90, WHITE);

	GrSelectEvents(wid, GR_EVENT_MASK_EXPOSURE | GR_EVENT_MASK_CLOSE_REQ);
	GrMapWindow(wid);

	while (1) {
		GrGetNextEvent(&ev);

		if (ev.type == GR_EVENT_TYPE_CLOSE_REQ)
			break;
		if (ev.type == GR_EVENT_TYPE_EXPOSURE)
			draw(&ev);
	}

	GrClose();

	return 0;
}
