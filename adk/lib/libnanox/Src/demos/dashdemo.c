#include <svc.h>
#include <stdio.h>
#include <stdlib.h>
#include <liblog/logsys.h>

#define MWINCLUDECOLORS
#include <nano-X.h>
#include <Nxcolors.h>

int COLS, ROWS;
GR_WINDOW_ID g_main;

int m_console = -1;
const char *g_apLogicalName = "DEMO";

int initConsole( void )
{
	int ret = 0;

	LOG_INIT( (char *)g_apLogicalName, LOGSYS_PIPE,LOGSYS_PRINTF_FILTER);
	dlog_msg("hello world from %s!", g_apLogicalName );

    // this app and this app alone will own the console!
    m_console = open( "/dev/console", 0 );
    if( m_console < 0 )
    {
        dlog_error("error opening console");
        ret = -1;		// error opening console
    }
	else
	{
	    //Open conn to nano-X
	    if (GrOpen () != -1)
	    {
	        dlog_msg("Nano-X connection is open");
	    }
		else
		{
	        dlog_error("Nano-X connection is failed");
			SVC_WAIT(3000);
			ret = -2;
		}	
	}
    return ret;
}

#define FGCOLOR		BLACK_MW
#define BGCOLOR		WHITE_MW

static void
draw_screen(void)
{
	GR_POINT tri[4] = { {5, 115}, {105, 115}, {55, 200}, {5, 115} };
	GR_WINDOW_INFO winfo;
	GR_GC_ID gc;
	char dash1[2] = { 10, 5 };
	char dash2[4] = { 5, 2, 1, 2 };
	char dash3[4] = { 5, 2, 5, 5 };
	char dash4[2] = { 2, 2 };

	dlog_msg("1-1");

	GrGetWindowInfo(g_main, &winfo);

	/* Draw several lines and a few boxes */
	gc = GrNewGC();
	GrSetGCLineAttributes(gc, GR_LINE_ONOFF_DASH);
	GrSetGCUseBackground(gc, GR_FALSE);
	GrSetGCForeground(gc, FGCOLOR);
	GrSetGCBackground(gc, BGCOLOR);

	GrText(g_main, gc, 30, 5, "Hello", -1, GR_TFASCII|GR_TFTOP);
	GrFlush ();

	/* Draw a dashed box */
	GrSetGCDash(gc, dash1, 2);
	GrRect(g_main, gc, 5, 5, 100, 100);
	GrFlush ();
	
    /* Draw a diagonal line in the box, in the form of dash dot dash */
	GrSetGCDash(gc, dash2, 4);
	GrLine(g_main, gc, 10, 10, 95, 95);
	GrFlush ();
	
	/* Draw a circle to the right of the box*/
	GrSetGCDash(gc, dash3, 4);
	GrEllipse(g_main, gc, 160, 55, 50, 50);
	GrFlush ();

	/* Draw a triangle below the box*/
	GrSetGCDash(gc, dash4, 2);
	GrPoly(g_main, gc, 4, tri);
	GrFlush ();

	GrDestroyGC(gc);
}

int
main(int argc, char **argv)
{
	static	GR_SCREEN_INFO	si; 	/* information about screen */

	initConsole();

	GrGetScreenInfo(&si);

	g_main = GrNewWindowEx(GR_WM_PROPS_APPWINDOW, "dashdemo", GR_ROOT_WINDOW_ID, 0, 0, si.cols-1, si.rows-1, WHITE_MW);
		
	GrSelectEvents(g_main, GR_EVENT_MASK_EXPOSURE|GR_EVENT_MASK_CLOSE_REQ);

	GrMapWindow(g_main);

	GrFlush ();

	while (1) {
		GR_EVENT event;
		GrGetNextEvent(&event);

		switch (event.type) {
		case GR_EVENT_TYPE_EXPOSURE:
			draw_screen();
			break;

		case GR_EVENT_TYPE_CLOSE_REQ:
			GrClose();
			exit(0);
		}
	}
}
