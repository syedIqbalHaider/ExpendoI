/*
 * fontdemo - freetype font demonstration program for Nano-X
 */
#include <stdio.h>
#include <stdlib.h>
#define MWINCLUDECOLORS
#include "nano-X.h"

#if defined(VFI_PLATFORM_VX) || defined(VFI_PLATFORM_VERIXEVO)
#include <svc.h>
#include <liblog/logsys.h>

int m_console = -1;
const char *g_apLogicalName = "DEMO";
#else
#include <unistd.h>
#include <signal.h>
#endif

/*#define FONTNAME	"lt1-r-omega-serif.ttf"*/
/*#define FONTNAME	"arial.ttf"*/
#define FONTNAME	"times.ttf"

#define FGCOLOR		BLACK_MW
#define BGCOLOR		WHITE_MW

GR_WINDOW_ID	w;
GR_GC_ID	gc;
GR_FONT_ID	font;
GR_BOOL		aa = GR_TRUE;


int initConsole( void )
{
	int ret = 0;

	LOG_INIT( (char *)g_apLogicalName, LOGSYS_PIPE,LOGSYS_PRINTF_FILTER);
	dlog_msg(__FILE__, __LINE__, "hello world from %s!", g_apLogicalName );

    // this app and this app alone will own the console!
    m_console = open( "/dev/console", 0 );
    if( m_console < 0 )
    {
        dlog_error(__FILE__, __LINE__, "error opening console");
        ret = -1;		// error opening console
    }
	else
	{
	    //Open conn to nano-X
	    if (GrOpen () != -1)
	    {
	        dlog_msg(__FILE__,__LINE__,"Nano-X connection is open");			
	    }
		else
		{
	        dlog_error(__FILE__,__LINE__,"Nano-X connection is failed");	
			ret = -2;
		}	
	}
    return ret;
}

static void
do_paint(GR_EVENT_EXPOSURE *ep)
{
	int	i, y = 0;

	for (i=3; i<=30; ++i) {
		GR_FONT_INFO	info;
		char		buf[64];

		font = GrCreateFont(FONTNAME, i, NULL);
		if (aa)
			GrSetFontAttr(font, GR_TFANTIALIAS|GR_TFKERNING, 0);
		/*GrSetFontRotation(font, 150);*/
		GrSetGCFont(gc, font);

		sprintf(buf, "%d The Quick Brown Fox Jumped Over The Lazy Dog", i);
		GrText(w, gc, 0, y, buf, -1, GR_TFASCII|GR_TFTOP);

		GrGetFontInfo(font, &info);
		y += info.height;

		GrDestroyFont(font);
	}
}

int
main(int ac, char **av)
{
    if (initConsole() < 0)
		exit(1);

	w = GrNewWindowEx(GR_WM_PROPS_APPWINDOW, "fontdemo", GR_ROOT_WINDOW_ID,
		10, 10, 640, 530, BGCOLOR);
	GrSelectEvents(w, GR_EVENT_MASK_EXPOSURE|GR_EVENT_MASK_BUTTON_DOWN|
		GR_EVENT_MASK_CLOSE_REQ);
	GrMapWindow(w);

	gc = GrNewGC();
	GrSetGCUseBackground(gc, GR_FALSE);
	GrSetGCForeground(gc, FGCOLOR);
	GrSetGCBackground(gc, BGCOLOR);

	while (1) {
		GR_EVENT event;

		GrGetNextEvent(&event);
		switch (event.type) {
		case GR_EVENT_TYPE_EXPOSURE:
			do_paint(&event.exposure);
			break;

		case GR_EVENT_TYPE_BUTTON_DOWN:
			{
			GR_WINDOW_INFO info;

			aa = !aa;
			GrGetWindowInfo(w, &info);
			GrSetGCForeground(gc, BGCOLOR);
			GrFillRect(w, gc, 0, 0, info.width, info.height);
			GrSetGCForeground(gc, FGCOLOR);
			do_paint(&event.exposure);	/*FIXME*/
			}
			break;

		case GR_EVENT_TYPE_CLOSE_REQ:
			GrClose();
			exit(0);
		}
	}
}
