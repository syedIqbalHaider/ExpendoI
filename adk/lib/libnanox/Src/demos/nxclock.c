/* 
 * nxclock - Nano-X clock program
 *
 * Copyright (C) 2000 by Greg Haerr <greg@censoft.com>
 * Copyright (C) 1999 Alistair Riddoch <ajr@ecs.soton.ac.uk>
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define MWINCLUDECOLORS
#include "nano-X.h"
#include <nxcolors.h>

#if defined(VFI_PLATFORM_VX) || defined(VFI_PLATFORM_VERIXEVO)
#include <svc.h>
#include <liblog/logsys.h>
#include "vxtime.h"

int m_console = -1;
const char *g_apLogicalName = "DEMO";
#else
#include <unistd.h>
#include <sys/time.h>
#endif

/* If you need a clock bigger than 200x200 you will need to re-write the trig *
 * to use longs. (Only applies under ELKS I think. */
#define CWIDTH		100		/* Max 200 */
#define CHEIGHT		100		/* Max 200 */

/*
 * Definitions to make it easy to define cursors
 */
#define	_	((unsigned) 0)		/* off bits */
#define	X	((unsigned) 1)		/* on bits */
#define	MASK(a,b,c,d,e,f,g) \
	(((((((((((((a * 2) + b) * 2) + c) * 2) + d) * 2) \
	+ e) * 2) + f) * 2) + g) << 9)

static	GR_WINDOW_ID	w1;		/* id for window */
static	GR_GC_ID	gc1;		/* graphics context for text */
static	GR_GC_ID	gc2;		/* graphics context for rectangle */
static int lh = -1, lm = -1, ls = -1;
static time_t then;

static unsigned char trig[91] =
	{ 0, 4, 8, 13, 17, 22, 26, 31, 35, 40, 44, 48, 53, 57, 61, 66,
	70, 74, 79, 83, 87, 91, 95, 100, 104, 108, 112, 116, 120, 124, 128,
	131, 135, 139, 143, 146, 150, 154, 157, 161, 164, 167, 171, 174, 177,
	181, 184, 187, 190, 193, 196, 198, 201, 204, 207, 209, 212, 214, 217,
	219, 221, 223, 226, 228, 230, 232, 233, 235, 237, 238, 240, 242, 243,
	244, 246, 247, 248, 249, 250, 251, 252, 252, 253, 254, 254, 255, 255,
	255, 255, 255, 255}; 

void do_exposure(GR_EVENT_EXPOSURE *ep);
void do_clock(void);
#if 0
void do_idle(void);
#endif
int bsin(int angle);
int bcos(int angle);
void draw_time(int hour, int minutes, int sec, GR_GC_ID gc);

int initConsole( void )
{
	int ret = 0;

	LOG_INIT( (char *)g_apLogicalName, LOGSYS_PIPE,LOGSYS_PRINTF_FILTER);
	dlog_msg(__FILE__, __LINE__, "hello world from %s!", g_apLogicalName );

    // this app and this app alone will own the console!
    m_console = open( "/dev/console", 0 );
    if( m_console < 0 )
    {
        dlog_error(__FILE__, __LINE__, "error opening console");
        ret = -1;		// error opening console
    }
	else
	{
	    //Open conn to nano-X
	    if (GrOpen () != -1)
	    {
	        dlog_msg(__FILE__,__LINE__,"Nano-X connection is open");			
	    }
		else
		{
	        dlog_error(__FILE__,__LINE__,"Nano-X connection is failed");	
			ret = -2;
		}	
	}
    return ret;
}

int
main(int ac, char **av)
{
	GR_EVENT	event;		/* current event */
	GR_BITMAP	bitmap1fg[7];	/* bitmaps for first cursor */
	GR_BITMAP	bitmap1bg[7];

	if (initConsole() < 0) {
		dlog_error(__FILE__, __LINE__, "cannot open graphics");
		exit(1);
	}
	
	/* create window*/
	w1 = GrNewWindowEx(
		GR_WM_PROPS_NOAUTOMOVE|GR_WM_PROPS_BORDER|GR_WM_PROPS_CAPTION|
		GR_WM_PROPS_CLOSEBOX, "nxclock", GR_ROOT_WINDOW_ID, 
		10, 10, CWIDTH, CHEIGHT, GrGetSysColor(GR_COLOR_WINDOW));

	GrSelectEvents(w1, GR_EVENT_MASK_EXPOSURE | GR_EVENT_MASK_CLOSE_REQ);

	gc1 = GrNewGC();
	gc2 = GrNewGC();

	GrSetGCForeground(gc1, GrGetSysColor(GR_COLOR_WINDOW));
	GrSetGCBackground(gc1, GrGetSysColor(GR_COLOR_WINDOWTEXT));
	GrSetGCForeground(gc2, GrGetSysColor(GR_COLOR_WINDOWTEXT));
	GrSetGCBackground(gc2, GrGetSysColor(GR_COLOR_WINDOW));

	bitmap1bg[0] = MASK(_,_,X,X,X,_,_);
	bitmap1bg[1] = MASK(_,X,X,X,X,X,_);
	bitmap1bg[2] = MASK(X,X,X,X,X,X,X);
	bitmap1bg[3] = MASK(X,X,X,X,X,X,X);
	bitmap1bg[4] = MASK(X,X,X,X,X,X,X);
	bitmap1bg[5] = MASK(_,X,X,X,X,X,_);
	bitmap1bg[6] = MASK(_,_,X,X,X,_,_);

	bitmap1fg[0] = MASK(_,_,_,X,_,_,_);
	bitmap1fg[1] = MASK(_,X,_,X,_,X,_);
	bitmap1fg[2] = MASK(_,_,_,X,_,_,_);
	bitmap1fg[3] = MASK(X,_,_,X,X,_,X);
	bitmap1fg[4] = MASK(_,_,_,_,_,_,_);
	bitmap1fg[5] = MASK(_,X,_,_,_,X,_);
	bitmap1fg[6] = MASK(_,_,_,X,_,_,_);

	GrSetCursor(w1, 7, 7, 3, 3, WHITE_MW, BLACK_MW, bitmap1fg, bitmap1bg);
	GrMapWindow(w1);

	while (1) {
		GrGetNextEventTimeout(&event, 500L);

		switch (event.type) {
			case GR_EVENT_TYPE_EXPOSURE:
				do_exposure(&event.exposure);
				break;

			case GR_EVENT_TYPE_CLOSE_REQ:
				GrClose();
				exit(0);

			case GR_EVENT_TYPE_TIMEOUT:
				do_clock();
				break;
		}
	}
}

int bsin(int angle)
{
	if(angle < 91) {
		return trig[angle];
	} else if (angle < 181) {
		return trig[180 - angle];
	} else if (angle < 271) {
		return -trig[angle - 180];
	} else if (angle < 361) {
		return -trig[360 - angle];
	} else {
		return 0;
	}
}

int bcos(int angle)
{
	if(angle < 91) {
		return trig[90 - angle];
	} else if (angle < 181) {
		return -trig[angle - 90];
	} else if (angle < 271) {
		return -trig[270 - angle];
	} else if (angle < 361) {
		return trig[angle - 270];
	} else {
		return 0;
	}
}

/*
 * Here when an exposure event occurs.
 */
void
do_exposure(GR_EVENT_EXPOSURE *ep)
{
	GR_COORD	midx = CWIDTH / 2;
	GR_COORD	midy = CHEIGHT / 2;
	int i, l;

/*	GrFillRect(w1, gc2, 0, 0, CWIDTH, CHEIGHT); */
/*	GrFillEllipse(w1, gc1, midx, midy, midx, midy); */
	GrEllipse(w1, gc2, midx, midy, midx - 1, midy - 1);
	for(i = 0; i < 60; i++) {
		if (i%5 == 0) {
			l = 5;
		} else {
			l = 0;
		}
		GrLine(w1, gc2,
			midx + (((midx - 3) * bsin(i * 6)) >> 8), 
			midy + (((midy - 3) * bcos(i * 6)) >> 8), 
			midx + (((midx - 3 - l) * bsin(i * 6)) >> 8), 
			midy + (((midy - 3 - l) * bcos(i * 6)) >> 8));
	}

	lh = -1; lm = -1; ls = -1;
	then = 0;
	do_clock();
}

void draw_time(int hour, int minutes, int sec, GR_GC_ID gc)
{
	GR_COORD	midx = CWIDTH / 2;
	GR_COORD	midy = CHEIGHT / 2;

	GrLine(w1, gc1,
		midx + (((midx - 8 - midx / 10) * bsin(ls)) >> 8), 
		midy - (((midy - 8 - midy / 10) * bcos(ls)) >> 8), 
		midx + (((midx - 8 - midx / 4) * bsin(ls)) >> 8), 
		midy - (((midy - 8 - midx / 4) * bcos(ls)) >> 8));
	GrLine(w1, gc2,
		midx + (((midx - 8 - midx / 10) * bsin(sec)) >> 8), 
		midy - (((midy - 8 - midy / 10) * bcos(sec)) >> 8), 
		midx + (((midx - 8 - midx / 4) * bsin(sec)) >> 8), 
		midy - (((midy - 8 - midx / 4) * bcos(sec)) >> 8));
	if ((lm != minutes) || (ls == minutes)) {
		GrLine(w1, gc1,
			midx + (((midx - 8 - midx / 10) * bsin(lm)) >> 8), 
			midy - (((midy - 8 - midy / 10) * bcos(lm)) >> 8), 
			midx + (((midx / 5) * bsin(lm)) >> 8), 
			midy - (((midy / 5) * bcos(lm)) >> 8));
		GrLine(w1, gc2,
			midx + (((midx - 8 - midx / 10) * bsin(minutes)) >> 8), 
			midy - (((midy - 8 - midy / 10) * bcos(minutes)) >> 8), 
			midx + (((midx / 5) * bsin(minutes)) >> 8), 
			midy - (((midy / 5) * bcos(minutes)) >> 8));
		GrLine(w1, gc1,
			midx + (((midx - 8 - midx / 2) * bsin(lh)) >> 8), 
			midy - (((midy - 8 - midy / 2) * bcos(lh)) >> 8), 
			midx + (((midx / 5) * bsin(lh)) >> 8), 
			midy - (((midy / 5) * bcos(lh)) >> 8));
		GrLine(w1, gc2,
			midx + (((midx - 8 - midx / 2) * bsin(hour)) >> 8), 
			midy - (((midy - 8 - midy / 2) * bcos(hour)) >> 8), 
			midx + (((midx / 5) * bsin(hour)) >> 8), 
			midy - (((midy / 5) * bcos(hour)) >> 8));
	}
	lh = hour;
	lm = minutes;
	ls = sec;

}

/*
 * Update the clock if the seconds have changed.
 */
void
do_clock(void)
{
	struct timeval tv;
	struct timezone tz;
	time_t now;
	int minutes, hour, sec;
	typedef struct 
	{
		  char t_yy[4];
		  char t_mm[2];
		  char t_dd[2];
		  char t_hh[2];
		  char t_mi[2];
		  char t_ss[2];
		  char t_w[1];
		  char t_extra[1];		  
	} numdatetime;
	numdatetime szVXTime;
	struct tm   stdTime;	
	char tmp[3];

	gettimeofday(&tv, &tz);
	
	now = tv.tv_sec - (60 * tz.tz_minuteswest);
	if (now == then) {
		dlog_msg(__FILE__, __LINE__, "do_clock now==then"); SVC_WAIT(2000);
		return;
	}
	then = now;

	memset((char*)&szVXTime, 0, sizeof(szVXTime));
    //
	//yyyy year
	//mm month (01 = January...12 = December)
	//dd day of month (01�31)
	//hh hour (for example, 00 = 12 AM, 12 = 12 PM, 13 = 1 PM, and so on).
	//mm minute (in the range 00�59)
    //	ss second (in the range 00�59)
    //	w day of week (0 = Sunday...6 = Saturday)
    //
    //  We want it in the format Wed Apr 20 15:32:40 1983\n\0
    //
    read_clock((char *)&szVXTime);

	memset(tmp, 0, sizeof(tmp));
	strncpy(tmp, szVXTime.t_ss, 2);
	stdTime.tm_sec   = str2int(tmp);     // seconds after the minute - [0,59] 
	strncpy(tmp, szVXTime.t_mi, 2);	
	stdTime.tm_min   = str2int(tmp);     // minutes after the hour - [0,59] 
	strncpy(tmp, szVXTime.t_hh, 2);	
	stdTime.tm_hour  = str2int(tmp);     // hours since midnight - [0,23] 

	minutes = stdTime.tm_min * 6;
	sec = stdTime.tm_sec * 6;
	hour = stdTime.tm_hour;
	if (hour > 12) {
		hour -= 12;
	}
	hour = hour*30 + minutes/12;

	draw_time(hour, minutes, sec, gc2);
}

#if 0
/*
 * Sleep a while to avoid using too much CPU time.
 */
void do_idle(void)
{
	struct timespec idletime;
	idletime.tv_sec = 0;
	idletime.tv_nsec = 100000;
	nanosleep(&idletime, NULL);
}
#endif
