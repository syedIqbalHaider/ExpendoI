/*
 * logical font descriptor demo for Nano-X
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#if UNIX
#include <time.h>
#endif
#define MWINCLUDECOLORS
#include "nano-X.h"

#if defined(VFI_PLATFORM_VX) || defined(VFI_PLATFORM_VERIXEVO)
#include <svc.h>
#include <liblog/logsys.h>

int m_console = -1;
const char *g_apLogicalName = "DEMO";
#else
#include <unistd.h>
#endif

#define FONT	"arial.ttf"

/*
 * logical font demo for Nano-X
 */

#define MAXW 		630
#define MAXH 		470


int initConsole( void )
{
	int ret = 0;

	LOG_INIT( (char *)g_apLogicalName, LOGSYS_PIPE,LOGSYS_PRINTF_FILTER);
	dlog_msg(__FILE__, __LINE__, "hello world from %s!", g_apLogicalName );

    // this app and this app alone will own the console!
    m_console = open( "/dev/console", 0 );
    if( m_console < 0 )
    {
        dlog_error(__FILE__, __LINE__, "error opening console");
        ret = -1;		// error opening console
    }
	else
	{
	    //Open conn to nano-X
	    if (GrOpen () != -1)
	    {
	        dlog_msg(__FILE__,__LINE__,"Nano-X connection is open");			
	    }
		else
		{
	        dlog_error(__FILE__,__LINE__,"Nano-X connection is failed");	
			ret = -2;
		}	
	}
    return ret;
}

int main(int argc, char **argv)
{
	GR_WINDOW_ID 	window;
	GR_EVENT 	event;
        GR_GC_ID 	gc;
	GR_FONT_ID	fontid;
        int 		x, y, rnd = 0;
	MWLOGFONT	lf;
	char		description[128];
   
        srand(time(0));
   
    if (initConsole() < 0)
		exit(1);

	window = GrNewWindowEx(GR_WM_PROPS_APPWINDOW, "logfont",
		GR_ROOT_WINDOW_ID, 5, 5, MAXW, MAXH, BLACK);
	GrSelectEvents(window, GR_EVENT_MASK_EXPOSURE|GR_EVENT_MASK_CLOSE_REQ);
	GrMapWindow(window);

        gc = GrNewGC();

        GrSetGCUseBackground(gc,GR_FALSE);
	GrSetGCBackground(gc, GR_RGB(0, 0, 0));

	y = 30;
	x = 0;

	while(1) {
	      GrCheckNextEvent(&event);

	      if(event.type == GR_EVENT_TYPE_CLOSE_REQ) {
			GrClose();
			exit(0);
	      }

	      /*sleep(1);*/

	      MWLF_Clear(&lf);
	      description[0] = '\0';
	      /*lf.lfSerif = 1;*/
	      lf.lfHeight = 12;	/* FIXME will change this requirement*/

	      if ( rnd & 1 ) {
		      lf.lfWeight = MWLF_WEIGHT_BOLD;
		      strcat(description,"Bold ");
	      }

	      if ( rnd & 2 ) {
		      lf.lfItalic = 1;
		      strcat(description,"Italics ");
	      }
	      if ( rnd & 4 ) {
		      lf.lfOblique = 1;
		      strcat(description,"Oblique ");
	      }

	      if ( rnd & 8 ) {
		      lf.lfMonospace = 1;
		      strcat(description,"Monospace ");
	      } else {
		      lf.lfProportional = 1;
		      strcat(description,"Proportional ");
	      }

	      if ( argc > 1 )
		      strcpy(lf.lfFaceName,argv[1]);
	      else
		      strcpy(lf.lfFaceName, FONT);

	      fontid = GrCreateFont(0, 0, &lf);
	      /* GrSetFontSize(fontid, 1+(int)(80.0 * rand() / (RAND_MAX+1.0))); */
	      GrSetFontSize(fontid,26);
	      /*GrSetFontRotation(fontid, 330);*/ /* 33 degrees*/
  	      GrSetFontAttr(fontid, GR_TFKERNING | GR_TFANTIALIAS, 0);
  	      GrSetGCFont(gc, fontid);
	      /*GrSetGCBackground(gc, rand() & 0xffffff);*/
 	      GrSetGCForeground(gc, 0xffffff);
	      /* x = (int) ((MAXW * 1.0) *rand()/(RAND_MAX+1.0));
		 y = (int) ((MAXH * 1.0) *rand()/(RAND_MAX+1.0)); */

	      GrText(window, gc,x,y, description, -1, GR_TFTOP|GR_TFASCII);
	      GrDestroyFont(fontid);

	      rnd++;
	      y += 30;
	      if ( y > 460 )
		      y = 0;
	}

	GrClose();
}
