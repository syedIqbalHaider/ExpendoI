/*
 * Demonstrates loading a binary PPM file and displaying it in a window
 * as a Pixmap.
 */

/* Comment this definition out if you don't want to use server side pixmaps */
/* (it will be slower but will work on device drivers without bitblt) */
#define USE_PIXMAPS

#include <svc.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <liblog/logsys.h>

#define MWINCLUDECOLORS
#include <nano-X.h>

GR_WINDOW_ID window_x;	/* ID for output window */
#ifdef USE_PIXMAPS
GR_WINDOW_ID pmap;	/* ID for pixmap */
#endif
GR_GC_ID gc;		/* Graphics context */
int width, height;	/* Size of image */
unsigned char *data;	/* Local copy of image data */
int m_console = -1;
const char *g_apLogicalName = "DEMO";

static void do_exposure(GR_EVENT_EXPOSURE *event)
{
	/* The window has been exposed so redraw it */
#ifdef USE_PIXMAPS
	GrCopyArea(window_x, gc, 0, 0, width, height, pmap, 0, 0, MWROP_SRCCOPY);
#else
	GrArea(window_x, gc, 0, 0, width, height, data, MWPF_RGB);
#endif
}

static void errorhandler(GR_EVENT *ep)
{
	printf("Error (%s) code %d id %d", ep->error.name, 
		ep->error.code, ep->error.id);
	exit(1);
}

int initConsole( void )
{
	int ret = 0;

	LOG_INIT( (char *)g_apLogicalName, LOGSYS_PIPE,LOGSYS_PRINTF_FILTER);
	dlog_msg(__FILE__, __LINE__, "hello world from %s!", g_apLogicalName );

    // this app and this app alone will own the console!
    m_console = open( "/dev/console", 0 );
    if( m_console < 0 )
    {
        dlog_error(__FILE__, __LINE__, "error opening console");
        ret = -1;		// error opening console
    }
	else
	{
	    //Open conn to nano-X
	    if (GrOpen () != -1)
	    {
	        dlog_msg(__FILE__,__LINE__,"Nano-X connection is open");			
	    }
		else
		{
	        dlog_error(__FILE__,__LINE__,"Nano-X connection is failed");	
			ret = -2;
		}	
	}
    return ret;
}


int main(int argc, char *argv[])
{
	unsigned char line[256];
	GR_EVENT event;
	FILE *infile;
	int i, o;
	unsigned char *p;

//Test for terminal
/*
	if(argc != 2) {
		printf("Usage: demo6 <filename.ppm>\n");
		exit(1);
	}

	if(!(infile = fopen(argv[1], "r"))) {
		printf("Couldn't open \"%s\" for reading: %s\n", argv[1],
						strerror(errno));
		exit(2);
	}
*/
	if(!(infile = fopen("ntetris.ppm", "r"))) {
		dlog_error(__FILE__,__LINE__,"Couldn't open \"%s\" for reading: %s\n", "ntetris.ppm", strerror(errno));
		SVC_WAIT(2000);
		exit(2);
	}

	/* Read magic number (P6 = colour, binary encoded PPM file) */
	if(!fgets(line, 256, infile)) goto truncated;
	if(line[0] != 'P' || line[1] != '6') {
		dlog_error(__FILE__,__LINE__,"Unsupported PPM type or not a PPM file.");
		dlog_error(__FILE__,__LINE__,"Please supply a valid P6 format file (colour, with binary encoding).");
		SVC_WAIT(2000);
	}

	/* Strip comments */
	do {
		if(!fgets(line, 256, infile)) goto truncated;
	} while(line[0] == '#');

	/* Read width and height */
	sscanf(line, "%i %i", &width, &height);

	/* Read the maximum colour value */
	if(!fgets(line, 256, infile)) goto truncated;
	sscanf(line, "%i", &i);
	if(i != 255) {
		dlog_error(__FILE__,__LINE__,"Truecolour mode only is supported");
		SVC_WAIT(2000);		
		exit(4);
	}

	/* Calculate how many bytes of image data there is */
	i = width * height * 3;
	/* Calculate how many bytes of data there will be after unpacking */
	o = width * height * 4;

	/* Allocate the space to store the data whilst it's being loaded */
	if(!(data = malloc(o))) {
		dlog_error(__FILE__,__LINE__,"Not enough memory to load image");
		SVC_WAIT(2000);		
		exit(5);
	}

	/* Read the data in and unpack it to RGBX format */
	/* The lower byte isn't used so we don't set it to anything */
	p = data;
	while(o) {
		if(fread(p, 1, 3, infile) != 3) goto truncated;
		p += 4;
		o -= 4;
	}

	/* We don't need the input file anymore so close it */
	fclose(infile);

	/* Register the error handler */
	GrSetErrorHandler(errorhandler);

	initConsole();

#ifdef USE_PIXMAPS
	/* Create the pixmap to store the picture in */
	pmap = GrNewPixmap(width, height, NULL);
#endif

	/* Create a graphics context */
	gc = GrNewGC();

#ifdef USE_PIXMAPS
	/* Copy the image data into the pixmap */
	GrArea(pmap, gc, 0, 0, width, height, data, MWPF_RGB);
	/* We can free the image data now because it's stored in the pixmap */
	free(data);
#endif

	/* Create a window to output the image to */
	window_x = GrNewWindow(GR_ROOT_WINDOW_ID, 0, 0, width, height, 0, 0, 0);

	/* Select expose events so we can redraw the image when necessary */
	GrSelectEvents(window_x, GR_EVENT_MASK_EXPOSURE |
				GR_EVENT_MASK_CLOSE_REQ);

	/* Make the window visible */
	GrMapWindow(window_x);

#ifdef USE_PIXMAPS
	/* Paint the pixmap onto it */
	GrCopyArea(window_x, gc, 0, 0, width, height, pmap, 0, 0,
							MWROP_SRCCOPY);
#else
	GrArea(window_x, gc, 0, 0, width, height, data, MWPF_RGB);
#endif

	while(1) {
		GrGetNextEvent(&event);
		switch(event.type) {
			case GR_EVENT_TYPE_EXPOSURE:
				do_exposure(&event.exposure);
				break;
			case GR_EVENT_TYPE_CLOSE_REQ:
				GrClose();
				exit(0);
		}
	}

	return 0;

truncated:
	dlog_error(__FILE__,__LINE__,"Error: File appears to be truncated");
	SVC_WAIT(2000);	
	exit(3);
}
