#include <svc.h>
#include <stdlib.h>
#include <string.h>

#include "vxtime.h"

int _daylight = 0;                  // Non-zero if daylight savings time is used
long _dstbias = 0;                  // Offset for Daylight Saving Time
long _timezone = 0;                 // Difference in seconds between GMT and local time

char *_tzname[2] = {"GMT", "GMT"};  // Standard/daylight savings time zone names

const char *_days[] = 
{
  "Sunday", "Monday", "Tuesday", "Wednesday",
  "Thursday", "Friday", "Saturday"
};

const char *_days_abbrev[] = 
{
  "Sun", "Mon", "Tue", "Wed", 
  "Thu", "Fri", "Sat"
};

const char *_months[] = 
{
  "January", "February", "March",
  "April", "May", "June",
  "July", "August", "September",
  "October", "November", "December"
};

const char *_months_abbrev[] = 
{
  "Jan", "Feb", "Mar",
  "Apr", "May", "Jun",
  "Jul", "Aug", "Sep",
  "Oct", "Nov", "Dec"
};

const int _ytab[2][12] = 
{
  {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
  {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
};


int ntocs( char *dest_buf, char *src_buf)
{
   int i;

   // DCS #1045 : Included following 2 lines
	if ( (dest_buf == NULL) || (src_buf == NULL) ) 
	   return -1;

      /* get length of current string add one for count */
   i = strlen(src_buf)+1;

       /* max counted string length is 255 */
   if ((i > 255) || (i == 0))
       dest_buf[0] = 0x00;
   else
   {
       strcpy(dest_buf+1, src_buf);
       dest_buf[0] = i;
   }

   /* return the count length */
   return (dest_buf [0]);

}

int str2int(char *buffer)
{
   int sign=0;
	int num;
	
			/* incoming string must not be longer than 40 characters */
   char  temp[42];
     
   // DCS #1045 : Included following 2 lines
   if ( buffer == NULL) 
	   return -1;

  if (strlen(buffer) > 40)
     return (0);

  if (*buffer == '-')
     sign= '-';

			/* SVC_2INT requires counted strings */
  ntocs(temp, buffer);
 
  num = SVC_2INT (temp);

  return(sign?-num:num);

}

struct tm *localtime(const time_t *timer)
{
	return NULL;
}

time_t mktime(struct tm *tmbuf)
{
  long day, year;
  int tm_year;
  int yday, month;
  /*unsigned*/ long seconds;
  int overflow;
  long dst;

  tmbuf->tm_min += tmbuf->tm_sec / 60;
  tmbuf->tm_sec %= 60;
  if (tmbuf->tm_sec < 0) 
  {
    tmbuf->tm_sec += 60;
    tmbuf->tm_min--;
  }
  tmbuf->tm_hour += tmbuf->tm_min / 60;
  tmbuf->tm_min = tmbuf->tm_min % 60;
  if (tmbuf->tm_min < 0) 
  {
    tmbuf->tm_min += 60;
    tmbuf->tm_hour--;
  }
  day = tmbuf->tm_hour / 24;
  tmbuf->tm_hour= tmbuf->tm_hour % 24;
  if (tmbuf->tm_hour < 0) 
  {
    tmbuf->tm_hour += 24;
    day--;
  }
  tmbuf->tm_year += tmbuf->tm_mon / 12;
  tmbuf->tm_mon %= 12;
  if (tmbuf->tm_mon < 0) 
  {
    tmbuf->tm_mon += 12;
    tmbuf->tm_year--;
  }
  day += (tmbuf->tm_mday - 1);
  while (day < 0) 
  {
    if(--tmbuf->tm_mon < 0) 
    {
      tmbuf->tm_year--;
      tmbuf->tm_mon = 11;
    }
    day += _ytab[LEAPYEAR(YEAR0 + tmbuf->tm_year)][tmbuf->tm_mon];
  }
  while (day >= _ytab[LEAPYEAR(YEAR0 + tmbuf->tm_year)][tmbuf->tm_mon]) 
  {
    day -= _ytab[LEAPYEAR(YEAR0 + tmbuf->tm_year)][tmbuf->tm_mon];
    if (++(tmbuf->tm_mon) == 12) 
    {
      tmbuf->tm_mon = 0;
      tmbuf->tm_year++;
    }
  }
  tmbuf->tm_mday = day + 1;
  year = EPOCH_YR;
  if (tmbuf->tm_year < year - YEAR0) return (time_t) -1;
  seconds = 0;
  day = 0;                      // Means days since day 0 now
  overflow = 0;

  // Assume that when day becomes negative, there will certainly
  // be overflow on seconds.
  // The check for overflow needs not to be done for leapyears
  // divisible by 400.
  // The code only works when year (1970) is not a leapyear.
  tm_year = tmbuf->tm_year + YEAR0;

  if (TIME_MAX / 365 < tm_year - year) overflow++;
  day = (tm_year - year) * 365;
  if (TIME_MAX - day < (tm_year - year) / 4 + 1) overflow++;
  day += (tm_year - year) / 4 + ((tm_year % 4) && tm_year % 4 < year % 4);
  day -= (tm_year - year) / 100 + ((tm_year % 100) && tm_year % 100 < year % 100);
  day += (tm_year - year) / 400 + ((tm_year % 400) && tm_year % 400 < year % 400);

  yday = month = 0;
  while (month < tmbuf->tm_mon)
  {
    yday += _ytab[LEAPYEAR(tm_year)][month];
    month++;
  }
  yday += (tmbuf->tm_mday - 1);
  if (day + yday < 0) overflow++;
  day += yday;

  tmbuf->tm_yday = yday;
  tmbuf->tm_wday = (day + 4) % 7;               // Day 0 was thursday (4)

  seconds = ((tmbuf->tm_hour * 60L) + tmbuf->tm_min) * 60L + tmbuf->tm_sec;

  if ((TIME_MAX - seconds) / SECS_DAY < day) overflow++;
  seconds += day * SECS_DAY;

  // Now adjust according to timezone and daylight saving time
  if (((_timezone > 0) && (TIME_MAX - _timezone < seconds))
      || ((_timezone < 0) && (seconds < -_timezone)))
          overflow++;
  seconds += _timezone;

  if (tmbuf->tm_isdst)
    dst = _dstbias;
  else 
    dst = 0;

  if (dst > seconds) overflow++;        // dst is always non-negative
  seconds -= dst;

  if (overflow) return (time_t) -1;

  if ((time_t) seconds != seconds) return (time_t) -1;
  return (time_t) seconds;
}

int gettimeofday(struct timeval *tv, struct timezone *tz)
{
	long l = read_ticks();

	if (l > 0)
	{
		tv->tv_sec = l / 1000;
		tv->tv_usec = (l % 1000) * 1000;
		l = 0;
	}
	return l;
}

#if 0
time_t time(time_t *t_ignore) 
{
	typedef struct 
	{
		  char t_yy[4];
		  char t_mm[2];
		  char t_dd[2];
		  char t_hh[2];
		  char t_mi[2];
		  char t_ss[2];
		  char t_w[1];
	} numdatetime;
	numdatetime szVXTime;
	struct tm   stdTime;

	if (t_ignore != NULL)
	{
		return (time_t) -1;
	}

	memset((char*)&szVXTime, 0, sizeof(szVXTime));
    //
	//yyyy year
	//mm month (01 = January...12 = December)
	//dd day of month (01�31)
	//hh hour (for example, 00 = 12 AM, 12 = 12 PM, 13 = 1 PM, and so on).
	//mm minute (in the range 00�59)
    //	ss second (in the range 00�59)
    //	w day of week (0 = Sunday...6 = Saturday)
    //
    //  We want it in the format Wed Apr 20 15:32:40 1983\n\0
    //
    read_clock((char *)&szVXTime);

	stdTime.tm_sec   = str2int(szVXTime.t_ss);     // seconds after the minute - [0,59] 
	stdTime.tm_min   = str2int(szVXTime.t_mi);     // minutes after the hour - [0,59] 
	stdTime.tm_hour  = str2int(szVXTime.t_hh);     // hours since midnight - [0,23] 
	stdTime.tm_mday  = str2int(szVXTime.t_dd);     // day of the month - [1,31] 
	stdTime.tm_mon   = str2int(szVXTime.t_mm) - 1; // months since January - [0,11] 
	stdTime.tm_year  = str2int(szVXTime.t_yy);     // years since 1900 
	stdTime.tm_wday  = str2int(szVXTime.t_w);      // days since Sunday - [0,6] 

	// Need some kind of substitute
	stdTime.tm_yday  = 0;  // days since January 1 - [0,365] 
	stdTime.tm_isdst = 0; // daylight savings time flag 

	return (time_t)mktime(&stdTime);
}

char* ctime (time_t *t_ignore)
{
  //We want it in the format Wed Apr 20 15:32:40 1983\n\0
  static char szCTime[26] = "Day Mon 00 00:00:00 1900\n\0";
  char tmp[3];
  char *t_DateTime = szCTime;
  char *t_Day;
  char *t_Month;
  int i =0;
  
  typedef struct 
  {
	  char t_yy[4];
	  char t_mm[2];
	  char t_dd[2];
	  char t_hh[2];
	  char t_mi[2];
	  char t_ss[2];
	  char t_w[1];
  } numdatetime;

  numdatetime szVXTime;
  memset((char*)&szVXTime, 0, sizeof(szVXTime));

  /*
	yyyy year
	mm month (01 = January...12 = December)
	dd day of month (01�31)
	hh hour (for example, 00 = 12 AM, 12 = 12 PM, 13 = 1 PM, and so on).
	mm minute (in the range 00�59)
	ss second (in the range 00�59)
	w day of week (0 = Sunday...6 = Saturday)

    We want it in the format Wed Apr 20 15:32:40 1983\n\0
  */
  read_clock((char *)&szVXTime);

  //Day of the week
  memset(tmp, 0, sizeof(tmp));
  memcpy(tmp,szVXTime.t_w,1);
  t_Day   = &"SunMonTueWedThuFriSat"[3*atoi(tmp)];
  for (i=0; i < 3; i++) *t_DateTime++ = *t_Day++;
  
  //Month of the year
  memset(tmp, 0, sizeof(tmp));
  memcpy(tmp,szVXTime.t_mm,2);
  t_DateTime++; // Skip a Byte
  t_Month = &"JanFebMarAprMayJunJulAugSepOctNovDec"[3*(atoi(tmp)-1)];
  for (i=0; i < 3; i++) *t_DateTime++ = *t_Month++;

  //Day of the Month
  t_DateTime++; // Skip a Byte
  for (i=0; i < 2; i++) *t_DateTime++ = szVXTime.t_dd[i];

  //Hour
  t_DateTime+=1; // Skip a Byte
  for (i=0; i < 2; i++) *t_DateTime++ = szVXTime.t_hh[i];
  *t_DateTime++ = ':';
  //Minute
  for (i=0; i < 2; i++) *t_DateTime++ = szVXTime.t_mi[i];
  *t_DateTime++ = ':';
  //Second
  for (i=0; i < 2; i++) *t_DateTime++ = szVXTime.t_ss[i];
  t_DateTime++; // Skip a Byte

  //Day of the Month
  for (i=0; i < 4; i++) *t_DateTime++ = szVXTime.t_yy[i];

  return szCTime;
}
#endif


