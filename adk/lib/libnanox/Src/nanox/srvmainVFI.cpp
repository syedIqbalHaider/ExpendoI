/*
 * Copyright (c) 1999, 2000, 2001, 2003, 2004, 2010 Greg Haerr <greg@censoft.com>
 * Portions Copyright (c) 2002 by Koninklijke Philips Electronics N.V.
 * Copyright (c) 1991 David I. Bell
 *
 * Main module of graphics server.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MWINCLUDECOLORS
#include "serv.h"

extern int	keyb_fd;		/* the keyboard file descriptor */
#if MW_FEATURE_TWO_KEYBOARDS
extern int	keyb2_fd;		/* the keyboard file descriptor */
#endif
extern int	mouse_fd;		/* the mouse file descriptor */


#include <liblog/logsys.h>
#include <libipc/ipc.h>
#include <libpml/pml.h>
#include <libpml/pml_port.h>


#  if !NONETWORK
#    error Network version not ready yet for Vx/Trident!
#  endif /* NONETWORK */

#if NONETWORK

/*
	NOTE: We are using event masks in place of file descriptors. 
	TODO: Needs an additional layer to allow registering for a particular bit in EVT_USER
	TODO2: Needs some global manager of unused event masks (for a given task) where code can allocate a mask for its use.
*/
//static long registeredEvents = 0;

static int VFIEventHandler = 0;

extern "C" void
GrRegisterInput(int fd)
{
	SERVER_LOCK();
	VFIEventHandler = fd;

	dlog_msg("GrRegisterInput keyb_fd=%d\n", keyb_fd);
	
	com_verifone_pml::event_item ei;
	ei.event_id = com_verifone_pml::events::keyboard;
	ei.evt.com.fd = keyb_fd;
	ei.evt.com.flags = EPOLLIN | EPOLLPRI;

	int ret = com_verifone_pml::event_ctl(VFIEventHandler, com_verifone_pml::ctl::ADD, ei);
	dlog_msg("GrRegisterInput ret=%d flags=%d\n", ret, ei.evt.com.flags);

	
	dlog_msg("GrRegisterInput mouse_fd=%d\n", mouse_fd);
	
	ei.event_id = com_verifone_pml::events::touch;
	ei.evt.com.fd = mouse_fd;
	ei.evt.com.flags = EPOLLIN | EPOLLPRI;

	ret = com_verifone_pml::event_ctl(VFIEventHandler, com_verifone_pml::ctl::ADD, ei);
	dlog_msg("GrRegisterInput ret=%d flags=%d\n", ret, ei.evt.com.flags);
	
	
	ei.event_id = com_verifone_pml::events::activate;
	com_verifone_pml::event_ctl(VFIEventHandler, com_verifone_pml::ctl::ADD, ei);

	com_verifone_pml::event_ctl(VFIEventHandler, com_verifone_pml::ctl::ADD, com_verifone_ipc::get_events_handler());
	
	SERVER_UNLOCK();
}

extern "C" void
GrUnregisterInput(int fd)
{
	int i, max;

	SERVER_LOCK();

	VFIEventHandler = 0;

	SERVER_UNLOCK();
}
#endif

void handleEvents(com_verifone_pml::eventset_t revents)
{
	using namespace com_verifone_pml;

	GR_EVENT_FDINPUT * gfd = 0;
	// GR_EVENT_GENERAL * gp = 0;

	dlog_msg("Processing %d events", revents.size());
	while (!revents.empty())
	{
		event_item event = revents.back();
		revents.pop_back();
		switch (event.event_id)
		{
			case events::cascade:
			{
				eventset_t cascade_events;
				int cascadeEventCount = event_read(event.evt.cascade.fd, cascade_events);
				dlog_msg("We got %d cascaded events!", cascadeEventCount);
				handleEvents(cascade_events);
				break;
			}
			case events::timer:
				dlog_msg("Timer event, id %lu, expired %d times", event.evt.tmr.id, event.evt.tmr.num_expires);
				// fall back to next case
			case events::activate:
			case events::ipc:
				dlog_msg("We got ipc event!");
				gfd = (GR_EVENT_FDINPUT *)GsAllocEvent(curclient);
				if (gfd) 
				{
					gfd->type = GR_EVENT_TYPE_FDINPUT;
					gfd->fd = event.event_id;
				}
				break;
			case events::keyboard:
			case events::user0:
				dlog_msg("We got keyboard event!");
				if(keyb_fd > 0 && kbddev.Poll())
				{
					while(GsCheckKeyboardEvent())
					{
						continue;
					}
				}
				break;
			case events::touch:
				dlog_msg("We got touch event!");
				if(mouse_fd > 0 && mousedev.Poll())
					while(GsCheckMouseEvent())
						continue;
				break;
		#if 0
			case events::timer:
				/* There were no useful events - timeout! */
				/* 
				 * Timeout has occured.  Currently return
				 * a timeout event regardless of whether
				 * client has selected for it.
				 * Note: this will be changed back to GR_EVENT_TYPE_NONE
				 * for the GrCheckNextEvent/LINK_APP_TO_SERVER case
				 */
				dlog_msg("Timer event, id %lu, expired %d times", event.evt.tmr.id, event.evt.tmr.num_expires);
				gp = (GR_EVENT_GENERAL *)GsAllocEvent(curclient);
				if (gp)
				{
					gp->type = GR_EVENT_TYPE_TIMER;
				}
				com_verifone_pml::clr_timer( event );
				if (VFIEventHandler)
				{
					com_verifone_pml::event_ctl(VFIEventHandler, com_verifone_pml::ctl::DEL, event);
				}
				break;
		#endif
			default:
				dlog_alert("Unhandled event type %d", event.event_id);
				break;
		}
	}
	return;
}


extern "C" void
GsSelect(GR_TIMEOUT timeout)
{
//	const long timerEvent = EVT_BIO;
//	int timerId = 0;
//	long interestingEvents = 0;
//	long events;
//	long firedRegistered;

	dlog_msg("GsSelect timeout = %d", timeout);


	/* perform pre-select duties, if any*/
	if(rootwp->psd->PreSelect)
		rootwp->psd->PreSelect(rootwp->psd);
	

#if NONETWORK
	/* Add registered set of events - equivalent of regfdset */
//	interestingEvents |= registeredEvents;
#endif

	/* Use version with timeout or without */
	/* TODO: Make a "select" equivalent that will choose itself between read_evt/wait_evt */
	if (timeout == (GR_TIMEOUT) -1L) 
	{
		/* poll */
//		events = read_evt(interestingEvents);
		
		/* If mouse data present, service it*/
		if(mouse_fd > 0 && mousedev.Poll())
			while(GsCheckMouseEvent())
				continue;
				
		/* If keyboard data present, service it*/
		if(keyb_fd > 0 && kbddev.Poll())
			while(GsCheckKeyboardEvent())
				continue;

				
#if 0
		/* check for input on registered file descriptors */
		if (VFIEventHandler)
		{
			int i;
			GR_EVENT_FDINPUT * gp;

			com_verifone_pml::eventset_t revents;
			int n = com_verifone_pml::event_read( VFIEventHandler, revents );
			if (n > 0) handleEvents(revents);
			/*if(GdTimeout())
			{
				GR_EVENT_GENERAL * gp;
				gp = (GR_EVENT_GENERAL *)GsAllocEvent(curclient);
				if(gp)
				{
					gp->type = GR_EVENT_TYPE_TIMEOUT;
				}
			} */
			/*
			for (i = 0; i < n; ++i)
			{
				gp = (GR_EVENT_FDINPUT *)GsAllocEvent(curclient);
				if (gp) 
				{
					gp->type = GR_EVENT_TYPE_FDINPUT;
					gp->fd = revents[i].evt.com.fd;
				}
			}
			*/
		}
#endif
				
		return;
	}
	else 
	{
		if (VFIEventHandler)
		{
			int i;
			GR_EVENT_FDINPUT * gp;

			com_verifone_pml::eventset_t revents;
			int n = com_verifone_pml::event_wait( VFIEventHandler, revents, timeout == 0L ? com_verifone_pml::INFINITE_TIMEOUT : static_cast<long>(timeout));
			
			dlog_alert("Wait completed, event count %d", n);
			handleEvents(revents);
			/*
			for (i = 0; i < n; ++i)
			{
				gp = (GR_EVENT_FDINPUT *)GsAllocEvent(curclient);
				if (gp) 
				{
					gp->type = GR_EVENT_TYPE_FDINPUT;
					gp->fd = revents[i].evt.com.fd;
				}
			}
			*/
		}
		
	}

#if 0	
	/* Handle events here */

	/* If data is present on the mouse fd, service it: */
	if (mouse_fd > 0 && (events & mouse_fd))
	{
		while(GsCheckMouseEvent())
			continue;
	}
	
	/* If data is present on the keyboard fd, service it: */
	if ( (keyb_fd > 0 && (events & keyb_fd))
#if MW_FEATURE_TWO_KEYBOARDS
	 || (keyb_fd > 0 && (events & keyb2_fd)) )
#endif
	)
	{
		while(GsCheckKeyboardEvent())
			continue;
	}
	
#if NONETWORK
	/* check for input on registered file descriptors */
	firedRegistered = events & registeredEvents;
	if (firedRegistered)
	{
		const int ULONG_BITS = CHAR_BIT * sizeof(unsigned long);
		int i;
		long mask = 1;
		long singleEvent;
		GR_EVENT_FDINPUT * gp;

		for (i = ULONG_BITS; i >0; --i)
		{
			singleEvent = firedRegistered & mask;
			if (singleEvent)
			{
				gp = (GR_EVENT_FDINPUT *)GsAllocEvent(curclient);
				if (gp) 
				{
					gp->type = GR_EVENT_TYPE_FDINPUT;
					gp->fd = singleEvent;
				}
			}
			
			mask <<= 1;
		}
	}
#endif

	
	/* Handle timer - NOTE: timer event may fire at the same time as other useful events! */
	if (events & timerEvent)
	{
		timerId = 0;
	}
		
	/* Clear timer if not expired */
	if (timerId)
	{
		clr_timer(timerId);
		/* Make sure timerEvent is cleared! It could fire after wait_evt() but before clr_timer() ! */
		read_evt(timerEvent);
	}

//IJ	if ((events & ~timerEvent) == 0)
	if (events & timerEvent)
	{
		/* There were no useful events - timeout! */

		/* 
		 * Timeout has occured.  Currently return
		 * a timeout event regardless of whether
		 * client has selected for it.
		 * Note: this will be changed back to GR_EVENT_TYPE_NONE
		 * for the GrCheckNextEvent/LINK_APP_TO_SERVER case
		 */
		GR_EVENT_GENERAL *	gp;
		gp = (GR_EVENT_GENERAL *)GsAllocEvent(curclient);
		if (gp)
			gp->type = GR_EVENT_TYPE_TIMEOUT;
	}
#endif	
}

