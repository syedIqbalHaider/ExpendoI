/* 
 * Loadable FNT font engine for Microwindows
 * Copyright (c) 2003, 2005, 2010 Greg Haerr <greg@censoft.com>
 *
 * Load a .fnt/.fnt.gz (Microwindows native) binary font, store in incore format.
 */
#include <svc.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include "swap.h"
#include "device.h"
#include "devfont.h"
#include "genfont.h"

/* configurable defaults*/
#ifndef FNT_FONT_DIR
#define FNT_FONT_DIR	"fonts/fnt"		/* default .fnt file location*/
#endif

/*
 * .fnt loadable font file format definition
 *
 * format                     len	description
 * -------------------------  ----	------------------------------
 * UCHAR version[4]				4	magic number and version bytes
 * UCHAR name[64]	       		64	font name, space padded
 * UCHAR copyright[256]	      256	copyright info, space padded
 * USHORT maxwidth				2	font max width in pixels
 * USHORT height				2	font height in pixels
 * USHORT ascent				2	font ascent (baseline) in pixels
 * USHORT pad                   2       unused, pad to 32-bit boundary
 * ULONG firstchar				4	first character code in font
 * ULONG defaultchar			4	default character code in font
 * ULONG size					4	# characters in font
 * ULONG nbits					4	# words imagebits data in file
 * ULONG noffset				4	# longs offset data in file
 * ULONG nwidth					4	# bytes width data in file
 * MWIMAGEBITS bits	  			nbits*2	image bits variable data
 * [MWIMAGEBITS padded to 32-bit boundary]
 * ULONG offset         		noffset*4	offset variable data
 * UCHAR width		 			nwidth*1	width variable data
 */

/* loadable font magic and version #*/
#define VERSION		"RB11"

/* The user hase the option including ZLIB and being able to    */
/* directly read compressed .fnt files, or to omit it and save  */
/* space.  The following defines make life much easier          */
#if HAVE_FNTGZ_SUPPORT
#include <zlib.h>
#define FILEP gzFile
#define FOPEN(path, mode)           gzopen(path, mode)
#define FREAD(file, buffer, size)   gzread(file, buffer, size)
#define FSEEK(file, offset, whence) gzseek(file, offset, whence)
#define FCLOSE(file)                gzclose(file)
#else
#define FILEP  FILE *
#define FOPEN(path, mode)           fopen(path, mode)
#define FREAD(file, buffer, size)   fread(buffer, 1, size, file)
#define FSEEK(file, offset, whence) fseek(file, offset, whence)
#define FCLOSE(file)                fclose(file)
#endif

/* Handling routines for FNT fonts, use MWCOREFONT structure */
PMWFONT fnt_createfont(const char *name, MWCOORD height, MWCOORD width, int attr);
static void fnt_unloadfont(PMWFONT font);
static PMWCFONT fnt_load_font(const char *path);

/* these procs used when font ASCII indexed*/
static const MWFONTPROCS fnt_fontprocs = {
	0,				/* can't scale*/
	MWTF_ASCII,		/* routines expect ascii */
	NULL,			/* init*/
	fnt_createfont,
	gen_getfontinfo,
	gen_gettextsize,
	gen_gettextbits,
	fnt_unloadfont,
	NULL,
	gen_drawtext,
	NULL,			/* setfontsize */
	NULL,			/* setfontrotation */
	NULL,			/* setfontattr */
	NULL			/* duplicate not supported */
};

/* these procs used when font requires UC16 index*/
static const MWFONTPROCS fnt_fontprocs16 = {
	0,				/* can't scale*/
	MWTF_UC16,		/* large font, expect UC16*/
	NULL,			/* init*/
	fnt_createfont,
	gen_getfontinfo,
	gen_gettextsize,
	gen_gettextbits,
	fnt_unloadfont,
	gen_drawtext,
	NULL,			/* setfontsize */
	NULL,			/* setfontrotation */
	NULL,			/* setfontattr */
	NULL			/* duplicate not supported */
};

/* load font and allocate MWCOREFONT structure*/
PMWFONT
fnt_createfont(const char *name, MWCOORD height, MWCOORD width, int attr)
{
	PMWCOREFONT	pf;
	PMWCFONT	cfont;
	int		uc16;

	/* try to open file and read in font data*/
	cfont = fnt_load_font(name);
	if (!cfont)
	{
		EPRINTF("fnt_load_font: cfont is NULL"); SVC_WAIT (2000);
		return NULL;
	}
	
	if (!(pf = (MWCOREFONT *) malloc(sizeof(MWCOREFONT)))) {
		free(cfont);
		EPRINTF("fnt_createfont: Malloc Failed"); SVC_WAIT (2000);
		return NULL;
	}

	/* determine if unicode-16 indexing required*/
	uc16 = cfont->firstchar > 255 || (cfont->firstchar + cfont->size) > 255;
	pf->fontprocs = uc16? &fnt_fontprocs16: &fnt_fontprocs;

	pf->fontsize = pf->fontrotation = pf->fontattr = 0;
	pf->name = "FNT";
	pf->cfont = cfont;
	return (PMWFONT)pf;
}

void
fnt_unloadfont(PMWFONT font)
{
	PMWCOREFONT pf = (PMWCOREFONT)font;
	PMWCFONT    pfc = pf->cfont;

	if (pfc) {
		if (pfc->width)
			free((void *) pf->cfont->width);
		if (pfc->offset)
			free((void *) pf->cfont->offset);
		if (pfc->bits)
			free((void *) pf->cfont->bits);
		if (pfc->name)
			free((void *) pf->cfont->name);

		free(pfc);
	}

	free(font);
}

static int
READBYTE(FILEP fp, unsigned char *cp)
{
#if HAVE_FNTGZ_SUPPORT
	unsigned char buf[1];

	if (FREAD(fp, buf, 1) != 1)
		return 0;
	*cp = buf[0];
#else
	int c;

	if ((c = getc(fp)) == EOF)
		return 0;
	*cp = (unsigned char)c;
#endif
	return 1;
}

static int
READSHORT(FILEP fp, unsigned short *sp)
{
#if HAVE_FNTGZ_SUPPORT
	unsigned char buf[2];

	if (FREAD(fp, buf, 2) != 2)
		return 0;
	*sp = buf[0] | (buf[1] << 8);
#else
	int c;
	unsigned short s;

	if ((c = getc(fp)) == EOF)
		return 0;
	s = c & 0xff;
	if ((c = getc(fp)) == EOF)
		return 0;
	*sp = (c << 8) | s;
#endif
	return 1;
}

static int
READLONG(FILEP fp, uint32_t *lp)
{
#if HAVE_FNTGZ_SUPPORT
	unsigned char buf[4];

	if (FREAD(fp, buf, 4) != 4)
		return 0;
	*lp = buf[0] | (buf[1] << 8) | (buf[2] << 16) | (buf[3] << 24);
#else
	int c;
	uint32_t l;

	if ((c = getc(fp)) == EOF)
		return 0;
	l = c & 0xff;
	if ((c = getc(fp)) == EOF)
		return 0;
	l |= (c << 8);
	if ((c = getc(fp)) == EOF)
		return 0;
	l |= (c << 16);
	if ((c = getc(fp)) == EOF)
		return 0;
	*lp = (c << 24) | l;
#endif
	return 1;
}

/* read count bytes*/
static int
READSTR(FILEP fp, char *buf, int count)
{
	return FREAD(fp, buf, count);
}

/* read totlen bytes, return NUL terminated string*/
/* may write 1 past buf[totlen]; removes blank pad*/
static int
READSTRPAD(FILEP fp, char *buf, int totlen)
{
	char *p;

	if (FREAD(fp, buf, totlen) != totlen)
		return 0;
	p = &buf[totlen];
	*p-- = 0;
	while (*p == ' ' && p >= buf)
		*p-- = '\0';
	return totlen;
}

/* read and load font, return incore font structure*/
static PMWCFONT
fnt_load_font(const char *path)
{
	FILEP ifp;
	PMWCFONT pf = NULL;
	int i;
	unsigned short maxwidth, height, ascent, pad;
	uint32_t firstchar, defaultchar, size;
	uint32_t nbits, noffset, nwidth;
	char version[4+1];
	char name[64+1];
	char copyright[256+1];
	char fname[256];

	MWIMAGEBITS * imageBits = NULL;
	uint32_t * offsetTable = NULL;
	unsigned char * widthTable = NULL;
	
	ifp = FOPEN(path, "rb");
	DPRINTF("fnt_load_font: %s", path);

#if 0
	if (!ifp) {
		strcpy(fname, FNT_FONT_DIR "/");
		strcpy(fname + sizeof(FNT_FONT_DIR), path);
		ifp = FOPEN(fname, "rb");
		
		/* Try to grab it from the MWFONTDIR directory */
		if (!ifp) {
			char *env = getenv("MWFONTDIR");
			if (env) {
				sprintf(fname, "%s/%s", env, path);
				
				DPRINTF("Trying to get font from %s\n", fname);
				ifp = FOPEN(fname, "rb");
			}
		}
		
	}
#endif

	if (!ifp)
	{
		EPRINTF("fnt_load_font: %s Failed %d", path, ifp);
		return NULL;
	}

	/* read magic and version #*/
	memset(version, 0, sizeof(version));
	if (READSTR(ifp, version, 4) != 4) 
	{   
		EPRINTF("fnt_load_font: read version"); SVC_WAIT(2000);
		goto errout;
	}

	DPRINTF("fnt_load_font: Version=%s", version);
	if (strcmp(version, VERSION) != 0)
	{	
		EPRINTF("fnt_load_font: read version"); SVC_WAIT(2000);
		goto errout;
	}

	pf = (PMWCFONT)calloc(1, sizeof(MWCFONT));
	if (!pf)
	{	
		EPRINTF("fnt_load_font: calloc"); SVC_WAIT(2000);
		goto errout;
	}

	/* internal font name*/
	if (READSTRPAD(ifp, name, 64) != 64)
	{	
		EPRINTF("fnt_load_font: font name"); SVC_WAIT(2000);
		goto errout;
	}

	{
		char * fontName = (char *)malloc(strlen(name)+1);
		if (!fontName)
		{	
			EPRINTF("fnt_load_font: malloc"); SVC_WAIT(2000);
			goto errout;
		}
		strcpy(fontName, name);
		pf->name = fontName;
	}
	DPRINTF("fnt_load_font: Name=%s", name);

	/* copyright, not currently stored*/
	if (READSTRPAD(ifp, copyright, 256) != 256)
	{	
		EPRINTF("fnt_load_font: copyright"); SVC_WAIT(2000);
		goto errout;
	}
	DPRINTF("fnt_load_font: Copyright=%s", copyright);

	/* font info*/
	if (!READSHORT(ifp, &maxwidth))
	{	
		EPRINTF("fnt_load_font: maxwidth"); SVC_WAIT(2000);
		goto errout;
	}
	pf->maxwidth = maxwidth;
	DPRINTF("fnt_load_font: maxwidth=%ld", maxwidth);
	
	if (!READSHORT(ifp, &height))
	{	
		EPRINTF("fnt_load_font: height"); SVC_WAIT(2000);
		goto errout;
	}
	pf->height = height;
	DPRINTF("fnt_load_font: height=%ld", height);
	
	if (!READSHORT(ifp, &ascent))
	{	
		EPRINTF("fnt_load_font: ascent"); SVC_WAIT(2000);
		goto errout;
	}
	pf->ascent = ascent;
	DPRINTF("fnt_load_font: ascent=%ld", ascent);
	
	if (!READSHORT(ifp, &pad))
	{	
		EPRINTF("fnt_load_font: pad"); SVC_WAIT(2000);
		goto errout;
	}
	DPRINTF("fnt_load_font: pad=%ld", pad);
		
	if (!READLONG(ifp, &firstchar))
	{	
		EPRINTF("fnt_load_font: firstchar"); SVC_WAIT(2000);
		goto errout;
	}
	pf->firstchar = firstchar;
	DPRINTF("fnt_load_font: firstchar=%ld", firstchar);
		
	if (!READLONG(ifp, &defaultchar))
	{	
		EPRINTF("fnt_load_font: defaultchar"); SVC_WAIT(2000);
		goto errout;
	}
	pf->defaultchar = defaultchar;
	DPRINTF("fnt_load_font: defaultchar=%ld", defaultchar);
	
	if (!READLONG(ifp, &size))
	{	
		EPRINTF("fnt_load_font: size"); SVC_WAIT(2000);
		goto errout;
	}
	pf->size = size;
	DPRINTF("fnt_load_font: size=%ld", size);

	/* variable font data sizes*/
	/* # words of MWIMAGEBITS*/
	if (!READLONG(ifp, &nbits))
	{	
		EPRINTF("fnt_load_font: nbits"); SVC_WAIT(2000);
		goto errout;
	}
	
	imageBits = (MWIMAGEBITS *)malloc(nbits * sizeof(MWIMAGEBITS));
	if (!imageBits)
	{	
		EPRINTF("fnt_load_font: malloc %ld", nbits * sizeof(MWIMAGEBITS)); SVC_WAIT(2000);
		goto errout;
	}
	pf->bits = imageBits;
	pf->bits_size = nbits;
	DPRINTF("fnt_load_font: nbits=%ld", nbits);

	/* # longs of offset*/
	if (!READLONG(ifp, &noffset))
	{	
		EPRINTF("fnt_load_font: noffset"); SVC_WAIT(2000);
		goto errout;
	}
	DPRINTF("fnt_load_font: noffset=%ld", noffset);
	
	if (noffset) 
	{
		offsetTable = (uint32_t *)malloc(noffset * sizeof(uint32_t));
		if (!offsetTable)
		{	
			EPRINTF("fnt_load_font: malloc"); SVC_WAIT(2000);
			goto errout;
		}
		pf->offset = offsetTable;
	}

	/* # bytes of width*/
	if (!READLONG(ifp, &nwidth))
	{	
		EPRINTF("fnt_load_font: nwidth"); SVC_WAIT(2000);
		goto errout;
	}
	DPRINTF("fnt_load_font: nwidth=%ld", nwidth);
	
	if (nwidth) 
	{
		widthTable = (unsigned char *)malloc(nwidth * sizeof(unsigned char));
		if (!widthTable)
		{	
			EPRINTF("fnt_load_font: malloc"); SVC_WAIT(2000);
			goto errout;
		}
		pf->width = widthTable;
	}

	/* variable font data*/
	for (i=0; i<nbits; ++i)
	{
		if (!READSHORT(ifp, (unsigned short *)&imageBits[i]))
		{	
			EPRINTF("fnt_load_font: nbits"); SVC_WAIT(2000);
			goto errout;
		}
	}
	
	/* pad to longword boundary*/
	if (nbits & 01)
		if (!READSHORT(ifp, &pad))
			goto errout;
				
	if (noffset)
	{
		DPRINTF("fnt_load_font: noffset=%ld, size=%ld", noffset, pf->size);
		for (i=0; i<pf->size; ++i)
		{
			if (!READLONG(ifp, (uint32_t *)&offsetTable[i]))
			{	
				EPRINTF("fnt_load_font: offset: %ld", i); SVC_WAIT(5000);
				goto errout;
			}
//			else {
//				DPRINTF("fnt_load_font: offset=%ld, count=%d", pf->offset[i], i);
//			}
		}
	}

	if (nwidth)
	{
		DPRINTF("fnt_load_font: nwidth=%ld, size=%ld", nwidth, pf->size);	
//		for (i=0; i<=(pf->size-2); ++i)
		for (i=0; i<pf->size; ++i)
		{
			if (!READBYTE(ifp, &widthTable[i]))
			{	
				EPRINTF("fnt_load_font: width: %ld", i); SVC_WAIT(2000);
				goto errout;
			}
//			else {
//				DPRINTF("fnt_load_font: width=%ld, count=%ld", pf->width[i], i);
//			}
		}
	}
	FCLOSE(ifp);
	return pf;	/* success!*/

errout:
	FCLOSE(ifp);
	if (!pf)
		return NULL;
	if (pf->name)
		free((void *) pf->name);
	if (pf->bits)
		free((void *) pf->bits);
	if (pf->offset)
		free((void *) pf->offset);
	if (pf->width)
		free((void *) pf->width);
	free(pf);
	return NULL;
}
