/* 
 * PCF font engine for Microwindows
 * Written by Tomasz Saniawa (Tomasz_S1@verifone.com)
 *
 * Supports dynamically loading .fnt.lib fonts (mainly for VerixV platform). Those fonts are smaller and much faster than font_pcf_onthefly.c implementation with practically no additional memory requirements.
 *
 */

#include "device.h"
#include "nano-vfi.h"
#include "genfont.h"

#include <cstddef> 
#include <cstdlib> 
#include <cstring> 
#include <memory> 

#include <boost/noncopyable.hpp>


extern "C" PMWFONT FntDLL_createfont(const char *name, MWCOORD height, MWCOORD width, int attr);

/* Handling routines for PCF fonts, use MWCOREFONT structure */
namespace
{
	extern "C"
	{
		void FntDLL_unloadfont(PMWFONT font);
		void FntDLL_gettextbits(PMWFONT pfont, int ch, const MWIMAGEBITS **retmap,
			MWCOORD *pwidth, MWCOORD *pheight, MWCOORD *pbase);

		MWBOOL FntDLL_getfontinfo(PMWFONT pfont, PMWFONTINFO pfontinfo);
		
		void FntDLL_gettextsize(PMWFONT pfont, const void *text, int cc, MWTEXTFLAGS flags,
			MWCOORD *pwidth, MWCOORD *pheight, MWCOORD *pbase);
	} // extern "C"
	

	/* these procs used when font ASCII indexed*/
	const MWFONTPROCS FntDLL_fontprocs = 
	{
		0,				/* can't scale*/
		MWTF_ASCII,
		NULL,			/* init */
		FntDLL_createfont,
		FntDLL_getfontinfo,
		FntDLL_gettextsize,
		FntDLL_gettextbits,
		FntDLL_unloadfont,
		NULL,
		gen_drawtext,
		NULL,			/* setfontsize */
		NULL,			/* setfontrotation */
		NULL,			/* setfontattr */
		NULL			/* duplicate*/
	};

	/* these procs used when font requires UC16 index*/
	const MWFONTPROCS FntDLL_fontprocs16 = 
	{
		0,				/* can't scale*/
		MWTF_UC16,		/* routines expect unicode 16 */
		NULL,			/* init */
		FntDLL_createfont,
		FntDLL_getfontinfo,
		FntDLL_gettextsize,
		FntDLL_gettextbits,
		FntDLL_unloadfont,
		NULL,
		gen_drawtext,
		NULL,			/* setfontsize */
		NULL,			/* setfontrotation */
		NULL,			/* setfontattr */
		NULL,			/* duplicate not yet implemented */
	};



	namespace AutoPtrSelectablePolicy
	{
		template <typename T>
		class NormalPtr
		{
		public:
			static void destroyHeap(T * p) { delete p; }

			// Call destructor explicitly!
			static void destroyPlacement(T * p) { p->~T(); }
		};

		template <typename T>
		class ArrayPtr
		{
		public:
			static void destroyHeap(T * p) { delete [] p; }

			// TODO: What should be here?
			static void destroyPlacement(T * p) {  }
		};
	}


	template <typename T, class DestructionPolicy = AutoPtrSelectablePolicy::NormalPtr<T> >
	class AutoPtrSelectable : public boost::noncopyable
	{
	public:
		AutoPtrSelectable(T * p = 0, bool onHeap = true)
			: p_(p), onHeap_(onHeap)
		{}
		
		~AutoPtrSelectable()
		{
			cleanup();
		}
		
		void assign(T * p = 0, bool onHeap = true)
		{
			cleanup();
			p_ = p;
			onHeap_ = onHeap;
		}
		
		T * get() { return p_; }
		void reset() { cleanup(); }
		T * release() 
		{
			T * p = p_;
			p_ = NULL;
			return p;
		}
		
		T & operator *() { return *p_; }
		T * operator ->() { return p_; }
		
	private:
		void cleanup()
		{
			if (p_)
			{
				if (onHeap_)
					DestructionPolicy::destroyHeap(p_);
				else
				{
					DestructionPolicy::destroyPlacement(p_);
				}
				p_ = NULL;
			}
		}
	
	// Attributes
	private:
		T * p_;
		bool onHeap_;
	};



	
	namespace DLLFunction
	{
		namespace Index
		{
			const int Copy_MWCFONT_DLL = 1;
			const int GetBuffer = 2;
		}

		namespace Type
		{
			typedef int Copy_MWCFONT_DLL(MWCFONT_DLL *);
			typedef void * GetBuffer(int bufferIdx, size_t * pSize);
		}
	}


	int CharRangeComparator(const void * e1, const void * e2)
	{
		CharRangeEntry const & r1 = *static_cast<CharRangeEntry const *>(e1);
		CharRangeEntry const & r2 = *static_cast<CharRangeEntry const *>(e2);
		int compareResult;
		
		if (r1.startChar > r2.endChar)
			compareResult = 1;
		else if (r2.startChar > r1.endChar)
			compareResult = -1;
		else
		{
			// NOTE: This is for any overlap of r1 and r2
			compareResult = 0;
		}
		
		return compareResult;
	}
	
	int findGlyphIndex(int ch, CharRangeEntry const * char_ranges, std::size_t char_ranges_cnt)
	{
		CharRangeEntry key = { ch, ch, 0 };
		
		// TODO: First check with first range - most of the hits will be there
		//void * bsearch ( const void * key, const void * base, size_t num, size_t size, int ( * comparator ) ( const void *, const void * ) );
		void * found = std::bsearch(&key, char_ranges, char_ranges_cnt, sizeof(char_ranges[0]), CharRangeComparator);
		
		int glyphIndex = -1;
		if (found)
		{
			CharRangeEntry & range = *static_cast<CharRangeEntry *>(found);
			glyphIndex = ch - range.startChar + range.startIndex;
		}
		
		return glyphIndex;
	}
	
	
	class LibData : private MWCOREFONT
	{
	public:
		LibData(int libHandle, MWCFONT_DLL * pCFontDLL = 0, MWIMAGEBITS * glyphBuffer = 0)
			: libHandle_(libHandle), pCFontDLL_(pCFontDLL, false), GlyphBuffer_(glyphBuffer, false), onHeap_(true),
			defaultGlyph_(-1)
		{
			// Zero the underlying MWCOREFONT and contained MWCFONT_DLL struct
			MWCOREFONT * pCoreFont = this;
			std::memset(pCoreFont, 0, sizeof(*pCoreFont));
			
			if (!pCFontDLL_.get())
			{
				// Now assign it and tell it to delete on destroy
				pCFontDLL_.assign(new MWCFONT_DLL);
			}
			std::memset(pCFontDLL_.get(), 0, sizeof(*pCFontDLL_));
			pCoreFont->cfont = &(pCFontDLL_->font);
		}
		
		~LibData()
		{
			// NOTE: all the pointers in  MWCFONT_DLL are taken from const data so there's nothing to free...
		}
		
		static LibData * allocateInPlace(void * mem, std::size_t size, 
			int libHandle, MWCFONT_DLL * pCFontDLL = 0, MWIMAGEBITS * glyphBuffer = 0)
		{
			LibData * pLibData = NULL;
			if (sizeof(LibData) <= size)
			{
				pLibData = new (mem) LibData(libHandle, pCFontDLL, glyphBuffer);
				pLibData->onHeap_ = false;
			}
			return pLibData;
		}
		
		void Destroy()
		{
			// It will call the proper destruction upon exit
			AutoPtrSelectable<LibData> pLibData(this, onHeap_);
		}

		
		MWCOREFONT * getCoreFontPtr()
		{
			// should cast automatically
			return this;
		}

		static LibData * fromMWFontPtr(MWFONT * pMWFont)
		{
			MWCOREFONT * pCoreFont = reinterpret_cast<MWCOREFONT *>(pMWFont);
			LibData * pLibData = static_cast<LibData *>(pCoreFont);
			return pLibData;
		}
		
		MWCFONT_DLL & getCFontDLL() { return *pCFontDLL_; }
		MWIMAGEBITS * getGlyphBuffer()
		{
			// Create on-demand only
			// TODO: A shared buffer (reallocated if needed) for all the fonts could work as well
			if (!GlyphBuffer_.get())
			{
				std::size_t maxGlyphBuffer = MWIMAGE_WORDS(pCFontDLL_->font.maxwidth) * pCFontDLL_->font.height;
				GlyphBuffer_.assign(new MWIMAGEBITS[maxGlyphBuffer]);
			}
			return GlyphBuffer_.get();
		}
		
		int getDefaultGlyph()
		{
			if (defaultGlyph_ < 0)
			{
				defaultGlyph_ = findGlyphIndex(pCFontDLL_->font.defaultchar, pCFontDLL_->char_ranges, pCFontDLL_->char_ranges_cnt);
				if (defaultGlyph_ < 0)
				{
					// If defaultchar is not defined - assume the first existing char to be a default
					defaultGlyph_ = 0;
				}
			}
			
			return defaultGlyph_;
		}
		
		int getGlyphIndex(int ch)
		{
			int glyphIndex;
			if (ch < pCFontDLL_->font.firstchar || ch > pCFontDLL_->char_ranges[pCFontDLL_->char_ranges_cnt - 1].endChar)
				glyphIndex = getDefaultGlyph();
			else
			{
				glyphIndex = findGlyphIndex(ch, pCFontDLL_->char_ranges, pCFontDLL_->char_ranges_cnt);
				if (glyphIndex < 0)
					glyphIndex = getDefaultGlyph();
			}

			return glyphIndex;
		}
		int getLibHandle() const { return libHandle_; }
		
	private:
		int libHandle_;
		AutoPtrSelectable<MWCFONT_DLL> pCFontDLL_;
		AutoPtrSelectable<MWIMAGEBITS, AutoPtrSelectablePolicy::ArrayPtr<MWIMAGEBITS> > GlyphBuffer_;
		bool onHeap_;
		int defaultGlyph_;
	};
}


PMWFONT FntDLL_createfont(const char *name, MWCOORD height, MWCOORD width, int attr)
{
	int libHandle = load_named_DLL(name);
	static char const * functionName = "FntDLL_createfont()";
	
	if (libHandle < 0)
	{
        DPRINTF2("<NANOX> %s: Failed to open font file\n", functionName);
		return NULL;
	}

	int copyFunHandle = DLL_function_address(libHandle, DLLFunction::Index::Copy_MWCFONT_DLL);
	if (copyFunHandle == 0)
	{
        DPRINTF2("<NANOX> %s: Failed to get the font data entry.\n", functionName);
		return NULL;
	}

	DLLFunction::Type::Copy_MWCFONT_DLL * const copyMWCFONT_DLL = 
		reinterpret_cast<DLLFunction::Type::Copy_MWCFONT_DLL *>(copyFunHandle);
	
	// The following may or may not exist
	DLLFunction::Type::GetBuffer * getBuffer = NULL;
	{
		int getBufferFunHandle = DLL_function_address(libHandle, DLLFunction::Index::GetBuffer);
		if (getBufferFunHandle)
		{
			getBuffer = reinterpret_cast<DLLFunction::Type::GetBuffer *>(getBufferFunHandle);
		}
	}
	
	// Now we can try to get the actual data
	// TODO: scoped ptr for this
	// TODO: boost::noncopyable!
	AutoPtrSelectable<LibData> pLibData;
	if (getBuffer)
	{
		std::size_t CFontSize = 0;
		MWCFONT_DLL * pCFontDLL = static_cast<MWCFONT_DLL *>(getBuffer(MW_FONT_DLL_CFONT_DLL, &CFontSize));
		if (sizeof(MWCFONT_DLL) > CFontSize)
			pCFontDLL = NULL;
	
		std::size_t glyphBufferSize = 0;
		MWIMAGEBITS * glyphBuffer = static_cast<MWIMAGEBITS *>(getBuffer(MW_FONT_DLL_BUF_GLYPH, &glyphBufferSize));

		std::size_t coreFontExtSize = 0;
		void * pCoreFontExt = getBuffer(MW_FONT_DLL_COREFONT_DLL, &coreFontExtSize);
		
		pLibData.assign(
			LibData::allocateInPlace(pCoreFontExt, coreFontExtSize, libHandle, pCFontDLL, glyphBuffer), 
			false
		);
	}
	
	if (!pLibData.get())
	{
		pLibData.assign(new LibData(libHandle));
		if (!pLibData.get())
		{
			DPRINTF2("<NANOX> %s: Failed to allocate mem for data.\n", functionName);
			return NULL;
		}
	}
	
	
	MWCFONT_DLL & fontDLL = pLibData->getCFontDLL();
	MWCFONT & cfont = fontDLL.font;
	int result = copyMWCFONT_DLL(&fontDLL);
	if (result != 0)
	{
        DPRINTF2("<NANOX> %s: Failed to obtain font data.\n", functionName);
		return NULL;
	}

	
	// Set the correct fontprocs
	{
		MWCOREFONT * pCoreFont = pLibData->getCoreFontPtr();
	
		bool isUnicode = cfont.firstchar > 255 || fontDLL.char_ranges[fontDLL.char_ranges_cnt - 1].endChar > 255;
		pCoreFont->fontprocs = isUnicode ? &FntDLL_fontprocs16: &FntDLL_fontprocs;
		pCoreFont->fontsize = pCoreFont->fontrotation = pCoreFont->fontattr = 0;
		pCoreFont->name = "FNT.DLL";
	}

	// Finally return the correct pointer
	return reinterpret_cast<PMWFONT>(pLibData.release()->getCoreFontPtr());
}

namespace
{
	void FntDLL_unloadfont(PMWFONT font)
	{

		LibData * pLibData = LibData::fromMWFontPtr(font);
		
		int libHandle = pLibData->getLibHandle();
		
		pLibData->Destroy();
		
		unload_DLL(libHandle);

		// NOTE: That's it! LibData destructor will take care of destroying what's needed
	}
	
	
	/*
	 * Specialized low level routine to get the bitmap associated
	 * with a character.  Handles fixed and proportional fonts.
	 * Gets the data from the file on the fly
	 */
	void FntDLL_gettextbits(PMWFONT pfont, int ch, const MWIMAGEBITS **retmap,
		MWCOORD *pwidth, MWCOORD *pheight, MWCOORD *pbase)
	{
		LibData * pLibData = LibData::fromMWFontPtr(pfont);
		MWCFONT_DLL & fontDLL = pLibData->getCFontDLL();
		MWCFONT & cfont = fontDLL.font;
		
		// Translate the character index to the glyph index
		int glyphIndex = pLibData->getGlyphIndex(ch);
		
		// Get all the lengths
		unsigned char zeroPrefix = fontDLL.zero_fill[glyphIndex].prefix;
		unsigned char zeroPostfix = fontDLL.zero_fill[glyphIndex].postfix;
		unsigned char glyphWidth = (cfont.width ? cfont.width[glyphIndex] : cfont.maxwidth);
		const std::size_t glyphTotalBytes = cfont.height * MWIMAGE_BYTES(glyphWidth);
		const std::size_t glyphCompressedBytes = glyphTotalBytes - zeroPrefix - zeroPostfix;

		// TODO: Implement offset_step != 1
		std::size_t glyphOffset = cfont.offset[glyphIndex];
		
		MWIMAGEBITS * glyphBuffer = pLibData->getGlyphBuffer();
		unsigned char * destBufferChar = reinterpret_cast<unsigned char *>(glyphBuffer);
		
		std::memset(destBufferChar, 0, zeroPrefix);
		destBufferChar += zeroPrefix;
		
		std::memcpy(destBufferChar, &fontDLL.compressed_bits[glyphOffset], glyphCompressedBytes);
		destBufferChar += glyphCompressedBytes;
		
		std::memset(destBufferChar, 0, zeroPostfix);
		
		
		*retmap = glyphBuffer;

		/* return width depending on fixed pitch or not*/
		*pwidth = glyphWidth; 
		*pheight = cfont.height;
		*pbase = cfont.ascent; 
	}

	MWBOOL FntDLL_getfontinfo(PMWFONT pfont, PMWFONTINFO pfontinfo)
	{
		LibData * pLibData = LibData::fromMWFontPtr(pfont);
		MWCFONT_DLL & fontDLL = pLibData->getCFontDLL();
		MWCFONT & cfont = fontDLL.font;

		pfontinfo->maxwidth = cfont.maxwidth;
		pfontinfo->height = cfont.height;
		pfontinfo->baseline = cfont.ascent;
		pfontinfo->firstchar = cfont.firstchar;
		pfontinfo->lastchar = fontDLL.char_ranges[fontDLL.char_ranges_cnt - 1].endChar;
		
		// NOTE: Copied from t1lib_getfontinfo() - font_t1lib.c
		pfontinfo->linespacing = 0;
		pfontinfo->descent = pfontinfo->height - pfontinfo->baseline;
		pfontinfo->maxascent = pfontinfo->baseline;
		pfontinfo->maxdescent = pfontinfo->descent; 

		if (cfont.width)
		{
			pfontinfo->fixed = FALSE;
			std::memset(pfontinfo->widths, 0, sizeof(pfontinfo->widths));
			
			const int fontInfoWidthsCnt = sizeof(pfontinfo->widths) / sizeof(pfontinfo->widths[0]);
			int i;
			bool doLoop = true;
			
			for (int rangeIdx=0; doLoop && rangeIdx < fontDLL.char_ranges_cnt; ++rangeIdx)
			{
				CharRangeEntry const & range = fontDLL.char_ranges[rangeIdx];
				for (i = range.startChar; i <= range.endChar; ++i)
				{
					if (i >= fontInfoWidthsCnt)
					{
						doLoop = false;
						break;
					}
					
					int glyphIndex = i - range.startChar + range.startIndex;
					pfontinfo->widths[i] = cfont.width[glyphIndex];
				}
			}
		}
		else
		{
			pfontinfo->fixed = TRUE;
		}
		
		return TRUE;
	}

	void FntDLL_gettextsize(PMWFONT pfont, const void *text, int cc, MWTEXTFLAGS flags,
		MWCOORD *pwidth, MWCOORD *pheight, MWCOORD *pbase)
	{
		LibData * pLibData = LibData::fromMWFontPtr(pfont);
		MWCFONT_DLL & fontDLL = pLibData->getCFontDLL();
		MWCFONT & cfont = fontDLL.font;
	
		const unsigned char * str = static_cast<const unsigned char *>(text);
		const unsigned short *istr = static_cast<const unsigned short *>(text);;
		unsigned int c;
		int width;

		width = 0;
		while (--cc >= 0) 
		{
			if (pfont->fontprocs->encoding == MWTF_UC16)
				c = *istr++;
			else
				c = *str++;
			
			int glyphIndex = pLibData->getGlyphIndex(c);
			width += cfont.width[glyphIndex];
		}
		
		*pwidth = width;
		*pheight = cfont.height;
		*pbase = cfont.ascent;
	}
}
