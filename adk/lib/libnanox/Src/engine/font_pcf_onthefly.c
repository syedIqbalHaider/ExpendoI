/* 
 * PCF font engine for Microwindows
 * Copyright (c) 2002, 2003 Greg Haerr <greg@censoft.com>
 * Copyright (c) 2001, 2002 by Century Embedded Technologies
 *
 * Supports dynamically loading .pcf and pcf.gz X11 fonts
 *
 * Written by Jordan Crouse
 * Bugfixed by Greg Haerr
 *
 * 28.01.2003:
 *   Patch for big-endian-machines by Klaus Fuerth <Fuerth.ELK@gmx.de>
 * 
 * 24.11.2009:
 *   Changed to read PCF files on the fly allocating very small, constant size memory - by Tomasz Saniawa <Tomasz_S1@verifone.com>
 *
 */
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include "swap.h"
#include "svc.h"
#include "device.h"
#include "devfont.h"
#include "genfont.h"

#if defined(VFI_PLATFORM_VERIXEVO) || defined(VFI_PLATFORM_VX)
#	include <strcasecmp.h>
#endif

extern MWPIXELVAL gr_background;
extern MWBOOL gr_usebg;

/* The user hase the option including ZLIB and being able to    */
/* directly read compressed .pcf files, or to omit it and save  */
/* space.  The following defines make life much easier          */
#ifdef HAVE_PCFGZ_SUPPORT
#include <zlib.h>
#define FILEP gzFile
#define FOPEN(path, mode)           gzopen(path, mode)
#define FREAD(file, buffer, size)   gzread(file, buffer, size)
#define FSEEK(file, offset, whence) gzseek(file, offset, whence)
#define FTELL(file)                 gztell(file)
#define FCLOSE(file)                gzclose(file)
#else
#define FILEP  FILE *
#define FOPEN(path, mode)           fopen(path, mode)
#define FREAD(file, buffer, size)   fread(buffer, 1, size, file)
#define FSEEK(file, offset, whence) fseek(file, offset, whence)
#define FTELL(file)                 ftell(file)
#define FCLOSE(file)                fclose(file)
#endif

/* Handling routines for PCF fonts, use MWCOREFONT structure */
static void pcf_unloadfont(PMWFONT font);


typedef struct PCFOnDemandData_s
{
	FILEP file;

	// *** BITMAPS section ***
	long bitmapsFOffset;		// pcf_get_offset(PCF_BITMAPS)
	unsigned long glyphCount;	// num_glyphs in pcf_readbitmaps
	unsigned long formatPCF;	// format in pcf_readbitmaps
	unsigned long bitmapSize;	// bits_size in pcf_readbitmaps
	
	// *** METRICS section ***
	long metricsFOffset;		// pcf_get_offset(PCF_BITMAPS)
	long metricsCount;
	unsigned long metricsFormat;

	// *** ENCODINGS section ***
	long encodingFOffset;
	unsigned long encodingFormat;
	unsigned short encodingDefaultChar;
	
	// *** GLYPH BITS ***
	MWIMAGEBITS * glyphBitmap;
	size_t rawGlyphBufferOffset;		// in MWIMAGEBITS units
} PCFOnDemandData_s;

static void PCFFlyData_Init(PCFOnDemandData_s * dataDemandPCF);

static void pcf_gettextbits_fly(PMWFONT pfont, int ch, const MWIMAGEBITS **retmap,
	MWCOORD *pwidth, MWCOORD *pheight, MWCOORD *pbase);

static MWBOOL pcf_getfontinfo_fly(PMWFONT pfont, PMWFONTINFO pfontinfo);
static void pcf_gettextsize_fly(PMWFONT pfont, const void *text, int cc, MWTEXTFLAGS flags,
	MWCOORD *pwidth, MWCOORD *pheight, MWCOORD *pbase);


/* these procs used when font ASCII indexed*/
static const MWFONTPROCS pcf_fontprocs = {
	0,				/* can't scale*/
	MWTF_ASCII,
	NULL,			/* init*/
	pcf_createfont,
	pcf_getfontinfo_fly,
	pcf_gettextsize_fly,
	pcf_gettextbits_fly,
	pcf_unloadfont,
	NULL,
	gen_drawtext,
	NULL,			/* setfontsize */
	NULL,			/* setfontrotation */
	NULL,			/* setfontattr */
	NULL			/* duplicate*/
};

/* these procs used when font requires UC16 index*/
static const MWFONTPROCS pcf_fontprocs16 = {
	0,				/* can't scale*/
	MWTF_UC16,		/* routines expect unicode 16 */
	NULL,			/* init*/
	pcf_createfont,
	pcf_getfontinfo_fly,
	pcf_gettextsize_fly,
	pcf_gettextbits_fly,
	pcf_unloadfont,
	gen_drawtext,
	NULL,			/* setfontsize */
	NULL,			/* setfontrotation */
	NULL,			/* setfontattr */
	NULL			/* duplicate*/
};

/* These are maintained statically for ease FIXME*/
static struct toc_entry *toc;
static unsigned long toc_size;

/* Various definitions from the Free86 PCF code */
#define PCF_FILE_VERSION	(('p'<<24)|('c'<<16)|('f'<<8)|1)
#define PCF_PROPERTIES		(1 << 0)
#define PCF_ACCELERATORS	(1 << 1)
#define PCF_METRICS		(1 << 2)
#define PCF_BITMAPS		(1 << 3)
#define PCF_INK_METRICS		(1 << 4)
#define PCF_BDF_ENCODINGS	(1 << 5)
#define PCF_SWIDTHS		(1 << 6)
#define PCF_GLYPH_NAMES		(1 << 7)
#define PCF_BDF_ACCELERATORS	(1 << 8)
#define PCF_FORMAT_MASK		0xFFFFFF00
#define PCF_DEFAULT_FORMAT	0x00000000

#define PCF_GLYPH_PAD_MASK	(3<<0)
#define PCF_BYTE_MASK		(1<<2)
#define PCF_BIT_MASK		(1<<3)
#define PCF_SCAN_UNIT_MASK	(3<<4)
#define GLYPHPADOPTIONS		4

#define PCF_LSB_FIRST		0
#define PCF_MSB_FIRST		1

/* A few structures that define the various fields within the file */
struct toc_entry {
	int type;
	int format;
	int size;
	int offset;
};

struct prop_entry {
	unsigned int name;
	unsigned char is_string;
	unsigned int value;
};

struct string_table {
	unsigned char *name;
	unsigned char *value;
};

struct metric_entry {
	short leftBearing;
	short rightBearing;
	short width;
	short ascent;
	short descent;
	short attributes;
};

struct encoding_entry {
	unsigned short min_byte2;	/* min_char or min_byte2 */
	unsigned short max_byte2;	/* max_char or max_byte2 */
	unsigned short min_byte1;	/* min_byte1 (hi order) */
	unsigned short max_byte1;	/* max_byte1 (hi order) */
	unsigned short defaultchar;
	unsigned long count;		/* count of map entries */
	unsigned short *map;		/* font index -> glyph index */
};

/* This is used to quickly reverse the bits in a field */
static const unsigned char _reverse_byte[0x100] = 
{
	0x00, 0x80, 0x40, 0xc0, 0x20, 0xa0, 0x60, 0xe0,
	0x10, 0x90, 0x50, 0xd0, 0x30, 0xb0, 0x70, 0xf0,
	0x08, 0x88, 0x48, 0xc8, 0x28, 0xa8, 0x68, 0xe8,
	0x18, 0x98, 0x58, 0xd8, 0x38, 0xb8, 0x78, 0xf8,
	0x04, 0x84, 0x44, 0xc4, 0x24, 0xa4, 0x64, 0xe4,
	0x14, 0x94, 0x54, 0xd4, 0x34, 0xb4, 0x74, 0xf4,
	0x0c, 0x8c, 0x4c, 0xcc, 0x2c, 0xac, 0x6c, 0xec,
	0x1c, 0x9c, 0x5c, 0xdc, 0x3c, 0xbc, 0x7c, 0xfc,
	0x02, 0x82, 0x42, 0xc2, 0x22, 0xa2, 0x62, 0xe2,
	0x12, 0x92, 0x52, 0xd2, 0x32, 0xb2, 0x72, 0xf2,
	0x0a, 0x8a, 0x4a, 0xca, 0x2a, 0xaa, 0x6a, 0xea,
	0x1a, 0x9a, 0x5a, 0xda, 0x3a, 0xba, 0x7a, 0xfa,
	0x06, 0x86, 0x46, 0xc6, 0x26, 0xa6, 0x66, 0xe6,
	0x16, 0x96, 0x56, 0xd6, 0x36, 0xb6, 0x76, 0xf6,
	0x0e, 0x8e, 0x4e, 0xce, 0x2e, 0xae, 0x6e, 0xee,
	0x1e, 0x9e, 0x5e, 0xde, 0x3e, 0xbe, 0x7e, 0xfe,
	0x01, 0x81, 0x41, 0xc1, 0x21, 0xa1, 0x61, 0xe1,
	0x11, 0x91, 0x51, 0xd1, 0x31, 0xb1, 0x71, 0xf1,
	0x09, 0x89, 0x49, 0xc9, 0x29, 0xa9, 0x69, 0xe9,
	0x19, 0x99, 0x59, 0xd9, 0x39, 0xb9, 0x79, 0xf9,
	0x05, 0x85, 0x45, 0xc5, 0x25, 0xa5, 0x65, 0xe5,
	0x15, 0x95, 0x55, 0xd5, 0x35, 0xb5, 0x75, 0xf5,
	0x0d, 0x8d, 0x4d, 0xcd, 0x2d, 0xad, 0x6d, 0xed,
	0x1d, 0x9d, 0x5d, 0xdd, 0x3d, 0xbd, 0x7d, 0xfd,
	0x03, 0x83, 0x43, 0xc3, 0x23, 0xa3, 0x63, 0xe3,
	0x13, 0x93, 0x53, 0xd3, 0x33, 0xb3, 0x73, 0xf3,
	0x0b, 0x8b, 0x4b, 0xcb, 0x2b, 0xab, 0x6b, 0xeb,
	0x1b, 0x9b, 0x5b, 0xdb, 0x3b, 0xbb, 0x7b, 0xfb,
	0x07, 0x87, 0x47, 0xc7, 0x27, 0xa7, 0x67, 0xe7,
	0x17, 0x97, 0x57, 0xd7, 0x37, 0xb7, 0x77, 0xf7,
	0x0f, 0x8f, 0x4f, 0xcf, 0x2f, 0xaf, 0x6f, 0xef,
	0x1f, 0x9f, 0x5f, 0xdf, 0x3f, 0xbf, 0x7f, 0xff
};

/*
 *	Invert bit order within each BYTE of an array.
 */
static void
bit_order_invert(unsigned char *buf, int nbytes)
{
	for (; --nbytes >= 0; buf++)
		*buf = _reverse_byte[*buf];
}

/*
 *	Invert byte order within each 16-bits of an array.
 */
void
two_byte_swap(unsigned char *buf, int nbytes)
{
	unsigned char c;

	for (; nbytes > 0; nbytes -= 2, buf += 2) {
		c = buf[0];
		buf[0] = buf[1];
		buf[1] = c;
	}
}

/*
 *	Invert byte order within each 32-bits of an array.
 */
void
four_byte_swap(unsigned char *buf, int nbytes)
{
	unsigned char c;

	for (; nbytes > 0; nbytes -= 4, buf += 4) {
		c = buf[0];
		buf[0] = buf[3];
		buf[3] = c;
		c = buf[1];
		buf[1] = buf[2];
		buf[2] = c;
	}
}

/* read an 8 bit byte*/
static unsigned short
readINT8(FILEP file)
{
	unsigned char b;

	FREAD(file, &b, sizeof(b));
	return b;
}

/* read a 16-bit integer LSB16 format*/
static unsigned short
readLSB16(FILEP file)
{
	unsigned short s;

	FREAD(file, &s, sizeof(s));
	return wswap(s);
}

/* read a 32-bit integer LSB32 format*/
static unsigned long
readLSB32(FILEP file)
{
	unsigned long n;

	FREAD(file, &n, sizeof(n));
	return dwswap(n);
}

/* Get the offset of the given field */
static int
pcf_get_offset(int item)
{
	int i;

	for (i = 0; i < toc_size; i++)
		if (item == toc[i].type)
			return toc[i].offset;
	return -1;
}


/* Read the actual bitmaps into memory */
static int
pcf_readbitmaps_init(FILEP file, PCFOnDemandData_s * dataDemandPCF)
{
	long offset;
	unsigned long format;
	unsigned long num_glyphs;
	unsigned long pad;
	unsigned int i;
	int endian;
	unsigned long *o;
	unsigned char *b;
	unsigned long bmsize[GLYPHPADOPTIONS];

    DPRINTF2("<NANOX> pcf_readbitmaps(): Reading Bitmaps");
	if ((offset = pcf_get_offset(PCF_BITMAPS)) == -1)
		return -1;
		
	
    DPRINTF3("<NANOX> pcf_readbitmaps(): Reading bitmaps at offset %08lX", offset);
	FSEEK(file, offset, SEEK_SET);

	dataDemandPCF->formatPCF = format = readLSB32(file);
	endian = (format & PCF_BIT_MASK)? PCF_LSB_FIRST: PCF_MSB_FIRST;
    DPRINTF3("FORMAT: %08lX Endian: %d", format, endian);

	num_glyphs = dataDemandPCF->glyphCount = readLSB32(file);
    DPRINTF3("NUM_GLYPHS: %08lX", num_glyphs);

	dataDemandPCF->bitmapsFOffset = FTELL(file);
	
	// skip offsets table
	FSEEK(file, num_glyphs * sizeof(unsigned long), SEEK_CUR);

	for (i=0; i < GLYPHPADOPTIONS; ++i)
	{
		bmsize[i] = readLSB32(file);
        DPRINTF3("BMSIZE %d: %08lX\n", i, bmsize[i]);
	}

	pad = format & PCF_GLYPH_PAD_MASK;
	dataDemandPCF->bitmapSize = bmsize[pad]? bmsize[pad] : 1;
    DPRINTF3("BITS_SIZE: %08X\n", dataDemandPCF->bitmapSize);
	
	return num_glyphs;
}

static int
pcf_readbitmap_index(PCFOnDemandData_s * dataDemandPCF, int index, 
	void * glyphBuffer, size_t glyphBufferSize)
{
	long fileOffset;
	FILEP file = dataDemandPCF->file;
	int count;
	
	// Go to the offsets table
	fileOffset = dataDemandPCF->bitmapsFOffset + index * 4;
	FSEEK(file, fileOffset, SEEK_SET);
	
	fileOffset = readLSB32(file);
	
	// Go to the bitmaps table
	fileOffset += dataDemandPCF->bitmapsFOffset;
	fileOffset += dataDemandPCF->glyphCount * 4;	// skip offsets
	fileOffset += GLYPHPADOPTIONS * 4;				// skip glyph options
	
	FSEEK(file, fileOffset, SEEK_SET);
	
	count = FREAD(file, glyphBuffer, glyphBufferSize);
	return count;
}



static int
pcf_readmetric_record_default(FILEP file, struct metric_entry * metric, int indexHint)
{
	metric->leftBearing = readLSB16(file);
	metric->rightBearing = readLSB16(file);
	metric->width = readLSB16(file);
	metric->ascent = readLSB16(file);
	metric->descent = readLSB16(file);
	metric->attributes = readLSB16(file);
	DPRINTF3(
		"%04lX   %8d   %8d   %8d   %8d   %8d   %08X\n",
		indexHint,
		metric->leftBearing,
		metric->rightBearing,
		metric->width,
		metric->ascent,
		metric->descent,
		metric->attributes
	);

	return 0;
}

static int
pcf_readmetric_record_nondefault(FILEP file, struct metric_entry * metric, int indexHint)
{
	metric->leftBearing = readINT8(file) - 0x80;
	metric->rightBearing = readINT8(file) - 0x80;
	metric->width = readINT8(file) - 0x80;
	metric->ascent = readINT8(file) - 0x80;
	metric->descent = readINT8(file) - 0x80;
	metric->attributes = 0;
	DPRINTF3(
		"%04lX   %8d   %8d   %8d   %8d   %8d\n",
		indexHint,
		metric->leftBearing,
		metric->rightBearing,
		metric->width,
		metric->ascent,
		metric->descent
	);

	return 0;
}


/* read character metric data*/
static int
pcf_readmetrics_init(FILE * file, PCFOnDemandData_s * dataDemandPCF, PMWCFONT cfont)
{
	static const char logHeader[] = "<NANOX> pcf_readmetrics_init()";
	long i, size, offset;
	unsigned long format;

	typedef int record_read_fun_t(FILEP file, struct metric_entry * metric, int indexHint);
	record_read_fun_t * pFunReadRecord = NULL;
	struct metric_entry metric;
	int max_width = 0, max_descent = 0, max_ascent = 0, max_height = 0;

	
	if ((offset = pcf_get_offset(PCF_METRICS)) == -1)
		return -1;

	FSEEK(file, offset, SEEK_SET);
	format = readLSB32(file);

    DPRINTF2("%s: Reading Metrics\n", logHeader);
    DPRINTF3("%s: Reading format %08lX at offset %08lX\n", logHeader, format, offset);
	if ((format & PCF_FORMAT_MASK) == PCF_DEFAULT_FORMAT) 
	{
		size = readLSB32(file);		/* 32 bits - Number of metrics*/
		pFunReadRecord = pcf_readmetric_record_default;

		DPRINTF3("#      LBEARING   RBEARING   WIDTH      ASCENT     DESCENT    ATTRIBUTES\n");
		DPRINTF3("----   --------   --------   --------   --------   --------   ----------\n");
	}
	else
	{
		DPRINTF2("%s: Metrics in compressed FORMAT DETECTED!!!\n\n", logHeader);

		size = readLSB16(file);		/* 16 bits - Number of metrics*/
		pFunReadRecord = pcf_readmetric_record_nondefault;

		DPRINTF3("#      LBEARING   RBEARING   WIDTH      ASCENT     DESCENT\n");
		DPRINTF3("----   --------   --------   --------   --------   --------\n");
	}
	DPRINTF3("%s: Found %08lX metrics:\n", logHeader, size);
		
	dataDemandPCF->metricsFOffset = FTELL(file);
	dataDemandPCF->metricsCount = size;
	dataDemandPCF->metricsFormat = format;
	
	for (i=0; i < size; i++) 
	{
		pFunReadRecord(file, &metric, i);
		
		if (metric.width > max_width)
			max_width = metric.width;
		if (metric.ascent > max_ascent)
			max_ascent = metric.ascent;
		if (metric.descent > max_descent)
			max_descent = metric.descent;
	}
	max_height = max_ascent + max_descent;

	cfont->maxwidth = max_width;
	cfont->height = max_height;
	cfont->ascent = max_ascent;
	
	DPRINTF2("%s: font settings:\n", logHeader);
	DPRINTF2("  maxwidth=%08X\n", max_width);
	DPRINTF2("  maxascent=%08X\n", max_ascent);
	DPRINTF2("  maxdescent=%08X\n", max_descent);
	DPRINTF2("  maxheight=%08X\n", max_height);

	return size;
}



static int
pcf_readmetric_single(PCFOnDemandData_s * dataDemandPCF, int index, struct metric_entry * metric)
{
	static const char logHeader[] = "<NANOX> pcf_readmetric_single()";
	long offset;
	unsigned long format;
	long metricRecordSize;
	FILEP file = dataDemandPCF->file;

	format = dataDemandPCF->metricsFormat;
	if ((format & PCF_FORMAT_MASK) == PCF_DEFAULT_FORMAT) 
	{
		metricRecordSize = 6 * sizeof(unsigned short);	// 6 times readLSB16
	}
	else
	{
		metricRecordSize = 5;
	}
	
	// Go to metrics and skip format and size
	offset = dataDemandPCF->metricsFOffset;
	offset += metricRecordSize * index;

	FSEEK(file, offset, SEEK_SET);
	
    DPRINTF2("%s: Reading Metric for idx=%i\n", logHeader, index);
	if ((format & PCF_FORMAT_MASK) == PCF_DEFAULT_FORMAT) 
	{
		DPRINTF3("#      LBEARING   RBEARING   WIDTH      ASCENT     DESCENT    ATTRIBUTES\n");
		DPRINTF3("----   --------   --------   --------   --------   --------   ----------\n");

		pcf_readmetric_record_default(file, metric, index);
	} 
	else 
	{
		DPRINTF3("#      LBEARING   RBEARING   WIDTH      ASCENT     DESCENT\n");
		DPRINTF3("----   --------   --------   --------   --------   --------\n");

		pcf_readmetric_record_nondefault(file, metric, index);
	}

	return 0;
}

/* read encoding table*/
static int
pcf_encoding_init(FILE * file, PCFOnDemandData_s * dataDemandPCF, struct encoding_entry * e)
{
	static const char logHeader[] = "<NANOX> pcf_encoding_init()";
	long offset, n;
	unsigned long format;

	if ((offset = pcf_get_offset(PCF_BDF_ENCODINGS)) == -1)
		return -1;
	FSEEK(file, offset, SEEK_SET);

	dataDemandPCF->encodingFormat = format = readLSB32(file);

    DPRINTF2("%s: Reading Encoding", logHeader);
    DPRINTF3("%s: offset %08lX (format %08lX):", logHeader, offset, format);
	e->min_byte2 = readLSB16(file);
	e->max_byte2 = readLSB16(file);
	e->min_byte1 = readLSB16(file);
	e->max_byte1 = readLSB16(file);
	dataDemandPCF->encodingDefaultChar = e->defaultchar = readLSB16(file);
	DPRINTF3("   min_byte2=%08X", e->min_byte2);
	DPRINTF3("   max_byte2=%08X", e->max_byte2);
	DPRINTF3("   min_byte1=%08X", e->min_byte1);
	DPRINTF3("   max_byte1=%08X", e->max_byte1);
	DPRINTF("    def_char %i=%#x", e->defaultchar, e->defaultchar);
	e->count = (e->max_byte2 - e->min_byte2 + 1) *
		(e->max_byte1 - e->min_byte1 + 1);
    DPRINTF3("       count=%08lX", e->count);

	e->map = NULL;

	dataDemandPCF->encodingFOffset = FTELL(file);
    
	DPRINTF("size %ld byte1 %d,%d byte2 %d,%d\n", e->count,
		e->min_byte1, e->max_byte1, e->min_byte2, e->max_byte2);
	return e->count;
}

static
unsigned short pcf_encoding_translate(PCFOnDemandData_s * dataDemandPCF, int encodingIndex)
{
	FILEP file = dataDemandPCF->file;
	long offset;
	unsigned short n;
	
	offset = encodingIndex * sizeof(unsigned short);
	offset += dataDemandPCF->encodingFOffset;
	
	FSEEK(file, offset, SEEK_SET);
	n = readLSB16(file);
	if (n == 0xffff)
	{
		// FIXME: It is currently done as in original font_pcf.c (encodingDefaultChar is treated as an index to bitmap)
		// However XFontStruct documentation says that it should be treated as char and if invalid (
		// outside of encoding range) no printing should be done for such chars.
		n = dataDemandPCF->encodingDefaultChar;
	}
	
	return n;
}


static int
pcf_read_toc(FILE * file, struct toc_entry **toc, unsigned long *size)
{
	long i;
	unsigned long version;
	struct toc_entry *t;

	FSEEK(file, 0, SEEK_SET);

	/* Verify the version */
	version = readLSB32(file);
	if (version != PCF_FILE_VERSION)
    {   
        DPRINTF("version != PCF_FILE_VERSION, version = %x != %x", version, PCF_FILE_VERSION);
		return -1;
   }

	*size = readLSB32(file);
	t = *toc = (struct toc_entry *) calloc(sizeof(struct toc_entry), *size);
	if (!t)
    {   
        DPRINTF("error callocing %d bytes", sizeof(struct toc_entry) );
		return -1;
   }

	/* Read in the entire table of contents */
	for (i=0; i<*size; ++i) {
		t[i].type = readLSB32(file);
		t[i].format = readLSB32(file);
		t[i].size = readLSB32(file);
		t[i].offset = readLSB32(file);
	}

	return 0;
}

PMWFONT
pcf_createfont(const char *name, MWCOORD height, MWCOORD width, int attr)
{
	unsigned long createTicks1 = read_ticks();
	unsigned long createTicks2;

	FILE *file = 0;
	MWCOREFONT *pf = 0;
	int i;
	int count;
	int bwidth;
	int err = 0;
	struct encoding_entry encodingEntry;
	int glyph_count;
	int uc16;
	PCFOnDemandData_s * dataDemandPCF = NULL;
	
    DPRINTF2("<NANOX> pcf_createfont(): Loading font name %s, height %d, attributes 0x%08X\n", name, height, attr);
    
	/* Try to open the file */
	file = FOPEN(name, "rb");

	if (!file) {
        DPRINTF2("<NANOX> pcf_createfont(): Failed to open font file\n");
		return NULL;
    }
    DPRINTF2("<NANOX> pcf_createfont(): filehandle %d", file);

	if (!(pf = (MWCOREFONT *) malloc(sizeof(MWCOREFONT)))) {
		err = -1;
        EPRINTF("<NANOX> pcf_createfont() ERROR: Failed to allocate %d bytes for core font structure\n", sizeof(MWCOREFONT));
		goto leave_func;
	}

	if (!(pf->cfont = (PMWCFONT) calloc(sizeof(MWCFONT), 1))) {
		err = -1;
        EPRINTF("<NANOX> pcf_createfont() ERROR: Failed to allocate %d bytes for prop/fixed font structure\n", sizeof(MWCFONT));
		goto leave_func;
	}

    DPRINTF2("<NANOX> pcf_createfont() Font file %s opened, processing...\n", name);

	/* Read the table of contents */
	if (pcf_read_toc(file, &toc, &toc_size) == -1) {
		err = -1;
        EPRINTF("<NANOX> pcf_createfont() ERROR: Failed to read TOC for font %s", name);
		goto leave_func;
	}

	
	/* Allocate addidtional structure keeping data for on-the-fly read */
	dataDemandPCF = (PCFOnDemandData_s *) malloc(sizeof(*dataDemandPCF));
	if (!dataDemandPCF)
	{
		err = -1;
        EPRINTF("<NANOX> pcf_createfont() ERROR: Failed to allocate %i bytes for on-the-fly data for font %s", sizeof(dataDemandPCF), name);
		goto leave_func;
	}
	PCFFlyData_Init(dataDemandPCF);
	pf->cfont->bits = (MWIMAGEBITS *) dataDemandPCF;
	
	/* move FILE* to the dataDemandPCF ownership */
	dataDemandPCF->file = file;
	file = NULL;

	
	/* Now, read in the bitmaps */
	glyph_count = pcf_readbitmaps_init(dataDemandPCF->file, dataDemandPCF);
	DPRINTF2("glyph_count = %d (%x)\n", glyph_count, glyph_count);

	if (glyph_count == -1) {
		err = -1;
        EPRINTF("<NANOX> pcf_createfont() ERROR: Found no glyphs in font %s", name);
		goto leave_func;
	}

	memset(&encodingEntry, 0, sizeof(encodingEntry));
	if (pcf_encoding_init(dataDemandPCF->file, dataDemandPCF, &encodingEntry) == -1) {
		err = -1;
        EPRINTF("<NANOX> pcf_createfont() ERROR: Failed to read char encoding for font %s", name);
		goto leave_func;
	}

	pf->cfont->firstchar = encodingEntry.min_byte2 * (encodingEntry.min_byte1 + 1);

	/* Read in the metrics */
	count = pcf_readmetrics_init(dataDemandPCF->file, dataDemandPCF, pf->cfont);


	DPRINTF2("<NANOX> pcf_createfont(): font settings:\n");
	DPRINTF2(" glyph_count=%#x (%i)\n", glyph_count, glyph_count);
	DPRINTF2("    minbyte1=%#02x\n", encodingEntry.min_byte1);
	DPRINTF2("    maxbyte1=%#02x\n", encodingEntry.max_byte1);
	DPRINTF2("    minbyte2=%#02x\n", encodingEntry.min_byte2);
	DPRINTF2("    maxbyte2=%#02x\n", encodingEntry.max_byte2);
	DPRINTF2("   firstchar=%04x\n", pf->cfont->firstchar);


	/* Allocate double buffer for single glyph only. Double size is needed because one part is used to read raw data from the file and 2nd as the returned area with glyph */
	bwidth = MWIMAGE_WORDS(pf->cfont->maxwidth);
	{
		// Resulting bitmap size
		size_t bitmapMaxSize = pf->cfont->height * (sizeof(MWIMAGEBITS) * bwidth);

		// Src glyph max size (depends on padding!!!)
		// FIXME: It assumes 4 word padding! Fix it!
		size_t maxSrcBytesLen = ((bwidth + 1) / 2) * pf->cfont->height * sizeof(long);
		
		const size_t alignmentMaskNeg = sizeof(long) - 1;

		// align bitmapMaxSize
		bitmapMaxSize += alignmentMaskNeg;
		bitmapMaxSize &= ~alignmentMaskNeg;
		
		// align maxSrcBytesLen
		maxSrcBytesLen += alignmentMaskNeg;
		maxSrcBytesLen &= ~alignmentMaskNeg;
		
		{
			const size_t allocSize = bitmapMaxSize + maxSrcBytesLen;
			
			dataDemandPCF->glyphBitmap = (MWIMAGEBITS *) calloc(allocSize, 1);

			if (!dataDemandPCF->glyphBitmap)
			{
				err = -1;
				EPRINTF("<NANOX> pcf_createfont() ERROR: Failed to allocate %d bitmap data bytes for font %s\n", allocSize, name);
				goto leave_func;
			}

			dataDemandPCF->rawGlyphBufferOffset = bitmapMaxSize / sizeof(MWIMAGEBITS);
		}
	}



	pf->cfont->offset = NULL;
	pf->cfont->width = NULL;
	pf->cfont->size = encodingEntry.count;

	uc16 = pf->cfont->firstchar > 255 || 
		(pf->cfont->firstchar + pf->cfont->size) > 255;
	pf->fontprocs = uc16? &pcf_fontprocs16: &pcf_fontprocs;
	pf->fontsize = pf->fontrotation = pf->fontattr = 0;
	pf->name = "PCF";


	createTicks2 = read_ticks();
	DPRINTF("pcf_createfont() time %lu\n", createTicks2 - createTicks1);

	
leave_func:
	if (toc)
		free(toc);
	toc = 0;
	toc_size = 0;

	if (file)
		FCLOSE(file);

	if (err == 0 && pf)
		return (PMWFONT)pf;

	pcf_unloadfont((PMWFONT)pf);
	return 0;
}

void
pcf_unloadfont(PMWFONT font)
{
	PMWCOREFONT pf = (PMWCOREFONT) font;
	PMWCFONT    pfc = pf->cfont;

	if (pfc) 
	{
		if (pfc->width)
			free((void *) pf->cfont->width);
		if (pfc->offset)
			free((void *) pf->cfont->offset);

		if (pfc->bits)
		{
			PCFOnDemandData_s * dataDemandPCF = (PCFOnDemandData_s *) pfc->bits;
			
			if (dataDemandPCF->glyphBitmap)
				free(dataDemandPCF->glyphBitmap);

			if (dataDemandPCF->file)
				fclose(dataDemandPCF->file);
				
			free(dataDemandPCF);
		}

		free(pf->cfont);
	}

	free(font);
}

void pcf_freeFontList (MWFONTLIST ***fonts, int n)
{
	int i;
	MWFONTLIST *g, **list = *fonts;

	for (i = 0; i < n; i++) {
		g = list[i];
		if(g) {
			if(g->mwname) 
				free(g->mwname);
			if(g->ttname) 
				free(g->ttname);
			free(g);
		}
	}
	free(list);
	*fonts = 0;
}

void pcf_getFontList (MWFONTLIST ***fonts, int *numfonts)
{
//	DIR *dir;
//	struct dirent *dent;
    char szFilename[40];
	char *p;
	int pl, idx = 0;
	MWFONTLIST **list;
	
/* WC rewrite for verix v
	dir = opendir(PCF_FONT_DIR);

	if (dir <= 0) {
		printf("Error opening font directory\n");
		*numfonts = -1;
		return ;
	}

	// get the number of strings we need to allocate
	while ((dent = readdir(dir)) != NULL) 
	{
		p = strrchr(dent->d_name, '.');
		if (p != NULL)
		{
			if (strcasecmp(p, ".gz") == 0)
				idx++;
		}
	}
*/
    EPRINTF("searching for fonts...");

	// get the number of strings we need to allocate
	strcpy( szFilename, "I:" );     // search in RAM first
    if( dir_get_first( szFilename ) == 0 )
    {
    	do
    	{
    		p = strrchr(szFilename, '.');
    		if (p != NULL)
    		{
    			if (strcasecmp(p, ".gz") == 0)
                {         
                    EPRINTF( "found font file %s", szFilename );
    				idx++;
                 }
    		}
    	} while( dir_get_next(szFilename) == 0 );
    }

	*numfonts = idx;
//	rewinddir(dir);

	/* allocate strings */
	list = (MWFONTLIST**) malloc(idx * sizeof(MWFONTLIST*));
    // WC check for malloc failure
    if( list == NULL )
    {
        EPRINTF( "<NANOX> pcf_getFontList(): malloc failure, idx * sizeof(MWFONTLIST*) = %d", idx * sizeof(MWFONTLIST*) );
        return;
    }
	for (pl = 0; pl < idx; pl++)
		list[pl] = (MWFONTLIST*) malloc(sizeof(MWFONTLIST));

	*fonts = list;

	idx = 0;

//	while ((dent = readdir(dir)) != NULL) 
	strcpy( szFilename, "I:" );     // search in RAM first
    dir_get_first( szFilename );
    do
	{
		/* check extension */
		p = strrchr(szFilename, '.');
    if (p != NULL)
		{
			if (strcasecmp(p, ".gz") == 0) 
			{			
				list[idx]->ttname = "not ttf";
				list[idx]->mwname = malloc (strlen (szFilename) + 1);
				list[idx]->mwname = strcpy (list[idx]->mwname, szFilename);
				idx++;
			}			
		}
	} while( dir_get_next(szFilename) == 0 );
	
//	closedir(dir);
}




//////////////////////////////////////////////////////////////////////////////////////////////////////


void PCFFlyData_Init(PCFOnDemandData_s * dataDemandPCF)
{
	dataDemandPCF->file = NULL;

	// *** BITMAPS section ***
	dataDemandPCF->bitmapsFOffset = 0;
	dataDemandPCF->glyphCount = 0;
	dataDemandPCF->formatPCF = 0;
	dataDemandPCF->bitmapSize = 0;
	
	// *** METRICS section ***
	dataDemandPCF->metricsFOffset = 0;
	dataDemandPCF->metricsCount = 0;
	dataDemandPCF->metricsFormat = 0;

	// *** ENCODINGS section ***
	dataDemandPCF->encodingFOffset = 0;
	dataDemandPCF->encodingFormat = 0;
	dataDemandPCF->encodingDefaultChar = 0;
	
	// *** GLYPH BITS ***
	dataDemandPCF->glyphBitmap = 0;
	dataDemandPCF->rawGlyphBufferOffset = 0;
}

/*
 * Specialized low level routine to get the bitmap associated
 * with a character.  Handles fixed and proportional fonts.
 * Gets the data from the file on the fly
 */
static void
pcf_gettextbits_fly(PMWFONT pfont, int ch, const MWIMAGEBITS **retmap,
	MWCOORD *pwidth, MWCOORD *pheight, MWCOORD *pbase)
{
	PMWCFONT		pf = ((PMWCOREFONT)pfont)->cfont;
	int 			count=0, width=0;
	const MWIMAGEBITS *	bits;

	/* if char not in font, map to first character by default*/
	if (ch < pf->firstchar || ch >= pf->firstchar+pf->size)
		ch = pf->firstchar;

	ch -= pf->firstchar;

	/* get font bitmap depending on fixed pitch or not*/
	bits = NULL;

	if (pf->bits)
	{
		// NOTE: here bits is a small area allocated only for the single glyph
		PCFOnDemandData_s * dataDemandPCF = (PCFOnDemandData_s *) pf->bits;
		long glyphOffset;
		size_t glyphBytes;
		unsigned char * const rawGlyphArea = (unsigned char *)(dataDemandPCF->glyphBitmap + dataDemandPCF->rawGlyphBufferOffset);
		int endian;
		struct metric_entry glyphMetric;
		int xwidth;
		int lineAdvance;
		
		// Remap ch using encoding
		ch = pcf_encoding_translate(dataDemandPCF, ch);

		/////////////////////////////////////////////////////////////////////////
		// get the metrics
		//
		pcf_readmetric_single(dataDemandPCF, ch, &glyphMetric);
		width = glyphMetric.width;
		count = MWIMAGE_WORDS(width) * pf->height; 

		/* # words image width, corrected for bounding box problem*/
		xwidth = MWIMAGE_WORDS(glyphMetric.rightBearing - glyphMetric.leftBearing);
		glyphBytes = ((xwidth + 1) / 2) * 4 * (glyphMetric.ascent + glyphMetric.descent);

		{
			// NOTE: It handles ONLY int (4-byte) and short (2-byte) padding
			const int lineAdvanceMask = (dataDemandPCF->formatPCF & PCF_GLYPH_PAD_MASK) - 1;
			// NOTE: lineAdvance is in shorts!
			lineAdvance = (xwidth + lineAdvanceMask) & (~lineAdvanceMask);
		}
		
		/////////////////////////////////////////////////////////////////////////
		// Read the glyph
		//
		pcf_readbitmap_index(dataDemandPCF, ch, rawGlyphArea, glyphBytes);

		/* convert bitmaps*/
		endian = (dataDemandPCF->formatPCF & PCF_BIT_MASK)? PCF_LSB_FIRST: PCF_MSB_FIRST;
		bit_order_invert(rawGlyphArea, glyphBytes);
#if MW_CPU_BIG_ENDIAN
		if (endian == PCF_LSB_FIRST)
			two_byte_swap(rawGlyphArea, glyphBytes);
#else
		if (endian == PCF_MSB_FIRST)
			two_byte_swap(rawGlyphArea, glyphBytes);
#endif


		/////////////////////////////////////////////////////////////////////////
		// now copy to the final buffer
		//

		//SDOYLE: copy the glyphs. all copied data bitmaps have the max height, padded
		//        above and below with empty bits
		{
			int max_height = pf->height;
			int max_ascent = pf->ascent;
			
			MWIMAGEBITS * output = (MWIMAGEBITS *) dataDemandPCF->glyphBitmap;
			int h, w;
			int y = max_height;
			unsigned short *ptr = (unsigned short *) rawGlyphArea;

			/* # words image width*/
			int lwidth = MWIMAGE_WORDS(glyphMetric.width);


			//SDOYLE: pad with blank lines on top of bitmap
			// FIXME: Change to memset
			for (h = (max_ascent - glyphMetric.ascent); h > 0; --h) 
			{
				for (w = 0; w < lwidth; w++)
					*output++ = 0;
				y--;
			}

			//SDOYLE: copy the data into the bitmap
			for (h = (glyphMetric.ascent + glyphMetric.descent); h > 0; --h) 
			{
				unsigned short *val = (unsigned short *) ptr;
				int            bearing, carry_shift;
				unsigned short carry = 0;

				/* leftBearing correction*/
				bearing = glyphMetric.leftBearing;
				if (bearing < 0)	/* negative bearing not handled yet*/
					bearing = 0;
				carry_shift = 16 - bearing;
		 
				for (w = 0; w < lwidth; w++) 
				{
					*output++ = (val[w] >> bearing) | carry;
					carry = val[w] << carry_shift;
				}
				ptr += lineAdvance;
				y--;
			}

			// FIXME: Change to memset
			//SDOYLE: pad with blank lines on bottom of bitmap
			for (; y > 0; y--)
				for (w = 0; w < lwidth; w++)
					*output++ = 0;
		}
		
		bits = dataDemandPCF->glyphBitmap;
	}
	
	
	*retmap = bits;

	/* return width depending on fixed pitch or not*/
	*pwidth = width; 
	*pheight = pf->height;
	*pbase = pf->ascent; 
}

static MWBOOL
pcf_getfontinfo_fly(PMWFONT pfont, PMWFONTINFO pfontinfo)
{
	PMWCFONT	pf = ((PMWCOREFONT)pfont)->cfont;
	int		i;
	PCFOnDemandData_s * dataDemandPCF = (PCFOnDemandData_s *) pf->bits;
	struct metric_entry glyphMetric;

	pfontinfo->maxwidth = pf->maxwidth;
	pfontinfo->height = pf->height;
	pfontinfo->baseline = pf->ascent;
	pfontinfo->firstchar = pf->firstchar;
	pfontinfo->lastchar = pf->firstchar + pf->size - 1;
	
	// NOTE: Copied from t1lib_getfontinfo() - font_t1lib.c
	pfontinfo->linespacing = 0;
	pfontinfo->descent = pfontinfo->height - pfontinfo->baseline;
	pfontinfo->maxascent = pfontinfo->baseline;
	pfontinfo->maxdescent = pfontinfo->descent; 
	
	
	// NOTE: PCF is not a fixed-width font
	pfontinfo->fixed = FALSE;

	for(i=0; i<256; ++i) 
	{
		if(i<pf->firstchar || i >= pf->firstchar+pf->size)
			pfontinfo->widths[i] = 0;
		else
		{
			int ch = i - pf->firstchar;
			int glyphIndex;
			
			// Translate using encoding map
			glyphIndex = pcf_encoding_translate(dataDemandPCF, ch);
			
			// get the metrics
			pcf_readmetric_single(dataDemandPCF, glyphIndex, &glyphMetric);
			
			pfontinfo->widths[i] = glyphMetric.width;
		}
	}
	
	return TRUE;
}

static void
pcf_gettextsize_fly(PMWFONT pfont, const void *text, int cc, MWTEXTFLAGS flags,
	MWCOORD *pwidth, MWCOORD *pheight, MWCOORD *pbase)
{
	PMWCFONT		pf = ((PMWCOREFONT)pfont)->cfont;
	const unsigned char *	str = text;
	const unsigned short *istr = text;
	unsigned int		c;
	int			width;

	PCFOnDemandData_s * dataDemandPCF = (PCFOnDemandData_s *) pf->bits;
	struct metric_entry glyphMetric;
	int glyphIndex;

	
	width = 0;
	while (--cc >= 0) 
	{
		if (pfont->fontprocs->encoding == MWTF_UC16)
			c = *istr++;
		else
			c = *str++;

		if (c >= pf->firstchar && c < pf->firstchar+pf->size)
		{
			glyphIndex = pcf_encoding_translate(dataDemandPCF, c - pf->firstchar);
			pcf_readmetric_single(dataDemandPCF, glyphIndex, &glyphMetric);
			width += glyphMetric.width;
		}
	}
	*pwidth = width;
	*pheight = pf->height;
	*pbase = pf->ascent;
	
}
