#include "device.h"
#include "genmem.h"

#include <cstddef>
//#include <cstring>
#include <svc.h>
#include <cerrno>

#include "scr_verixv.h"
#include "scr_tridentsub.h"  // Trident screen subdriver defines


extern "C"
{
	PSUBDRIVER TridentDrv_Select(PSD psd);
	static void TridentDrv_RedrawScreen(int force);
	static void TridentDrv_Freeze(int flag);

	static int Trident_Init(PSD psd);

	static void Trident_Update(PSD psd, MWCOORD x, MWCOORD y, MWCOORD width, MWCOORD height);
}

namespace
{
	std::size_t displayWidth;
	std::size_t lineCount;
	std::size_t bpp;
	const std::size_t dispBufCount = 240 * 320;
	short displayBuf[dispBufCount];
	

	class ScreenRefresh
	{
	// Const	
	private:
		static const unsigned long minDisplayDelay = 40;  // 40ms -> 25 fps
	
		class UserEvent
		{
		public:
			static const long update = 1L << 0;
			static const long stop = 1L << 1;
			static const long updateForce = 1L << 2;
		};
	
	public:
		ScreenRefresh() 
			: threadId_(0), needsUpdate_(false), freeze_(false)
		{}
		
		bool start()
		{
			const int param = reinterpret_cast<int>(this); // pass this ptr to the thread
			const int threadStackSize = 1024;

			int result = run_thread(reinterpret_cast<int>(ThreadFuncWrapper), param, threadStackSize);

			if (result > 0)
			{
				threadId_ = result;
				
				// NOTE: This enables child thread to use console but the console events will be received only by the main owner
				result = set_owner_all(get_console(0));
				
				// Notify child it may go!
				post_user_event(threadId_, 1);
			}
				
			return result > 0;
		}
		
		void freeze(int flag)
		{
			freeze_ = flag;
			if (freeze_ == false)
			{
				needsUpdate_ = false;
				update();
			}
		}

		//! @brief Notifies the update of the screen is needed
		void update()
		{
			// NOTE: On Vx it will be atomic anyway. It is done this way to HEAVILY limit the number of times
			// child thread is awaken. Unfortunately the EVT_USER is the only way to notify other thread and
			// you need to awake to do read_user_event()
			if (!needsUpdate_ && !freeze_)
			{
				needsUpdate_ = true;
				post_user_event(threadId_, UserEvent::update);
			}
		}

		//! @brief Forces the screen redraw
		void redraw()
		{
			int result;
			
			// IMPORTANT! Make sure PIXEL_MODE is used for display_frame_buffer to work at all!
			set_display_coordinate_mode(PIXEL_MODE);
			
			// Make sure all threads may access it
			result = set_owner_all(get_console(0));
			post_user_event(threadId_, UserEvent::updateForce);
		}
		
	private:
		// NOTE: Strictly speaking ThreadFuncWrapper should have a "C" linkage but 
		// it creates a mess and code below is NOT portable anyway
		static int ThreadFuncWrapper(int param)
		{
			ScreenRefresh * pThis = reinterpret_cast<ScreenRefresh *>(param);
			return pThis->ThreadFunc();
		}
	
		int ThreadFunc()
		{
			// wait for GO signal from parent
			wait_evt(EVT_USER);
			read_user_event();	// clear mask
			
			const long interestingEvents = EVT_USER | EVT_TIMER | EVT_SHUTDOWN;
			unsigned long lastUpdateMs = 0;
			
			unsigned long ticksDiff;
			long events;
			int timerId = 0;
			bool updateNow = true;
			
			while (true)
			{
				if (updateNow)
				{
					updateNow = false;
					updateScreenNow();
				}
			
				// Wait for the events
				events = wait_evt(interestingEvents);

				if (events & EVT_SHUTDOWN)
				{
					return 0;
				}

				if (events & EVT_USER)
				{
					long userMask = read_user_event();
					if (userMask & UserEvent::stop)
					{
						if (timerId)
							clr_timer(timerId);
						return 0;
					}

					if (userMask & UserEvent::updateForce)
					{
						// Force the update NOW
						updateNow = true;
					}
					else if (userMask & UserEvent::update)
					{
						ticksDiff = read_ticks();
						ticksDiff -= lastUpdateMs;
						if (ticksDiff >= minDisplayDelay)
						{
							updateNow = true;
						}
						else if (timerId == 0)
						{
							// make sure EVT_TIMER is cleared before timer is set
							read_evt(EVT_TIMER);
							timerId = set_timer(minDisplayDelay - ticksDiff, EVT_TIMER);
						}
					}	
				}
				
				if (events & EVT_TIMER)
				{
					timerId = 0;
					updateNow = true;
				}
			}

			return 0;
		}
	
		void updateScreenNow()
		{
			if (freeze_)
				return;

			// TODO: display only part that changed
			// However our tests show that this procedure is pretty fast - it is able to display @330 fps!
			needsUpdate_ = false;

			set_display_coordinate_mode(PIXEL_MODE);  //Sometimes pixel_mode is lost
			
			if(bpp == 16)
			{
				int result = display_frame_buffer(0, 0, displayWidth, lineCount, displayBuf);
			}
			else if(bpp == 1)
			{
				int w = displayWidth;
				int h = lineCount;
			
				const int GridX = 6;
				const int GridY = 8;

				int rem = (w % GridX);

				#define READ_BIT(image, w, x, y) (image[((y) * (w)/8) + ((x) >> 3) ] & (1 << (7 - ((x) & 7))))
				
				char *outputBytes = new char[displayWidth];
					
	            for (int y=0; y < h; y+=GridY)
	            {
	        		for (int x = 0; x < w; x++)
		            for (int l=0; y+l < h && l < GridY; l++)
		            {
        		        int mod = l & 0x07;
                		int pos = x;

		                if (READ_BIT(((char *)displayBuf), w, x, y+l))
   	    		            outputBytes[pos] &= ~(1 << mod);
   	    		        else    
   	    		            outputBytes[pos] |= (1 << mod);
		            }
		            
					gotoxy(0, y);
					putpixelcol(outputBytes, w-rem);
					
					if (rem != 0)	
					{
						gotoxy(w - GridX, y);
						putpixelcol(outputBytes + w - GridX, GridX);
			        }    
			    }
			    
			    delete []outputBytes;
			}
		}
		
	// Attributes
	private:
		int threadId_;
		volatile bool needsUpdate_;
		volatile bool freeze_;
	} screenRefresh;
	
}



PSUBDRIVER TridentDrv_Select(PSD psd)
{
	extern SUBDRIVER fblinear16_none;
	extern SUBDRIVER fblinear1_none;

	psd->Update = Trident_Update;

	g_RefreshFunc = TridentDrv_RedrawScreen;
	g_FreezeFunc = TridentDrv_Freeze;
	g_InitFunc = Trident_Init;

	if(psd->bpp == 1)
		return &fblinear1_none;
	else
		return &fblinear16_none;
}

static void TridentDrv_RedrawScreen(int force)
{
	//dlog_msg("%s: force: %d", __func__, force);
	if (force)
		screenRefresh.redraw();
	else
		screenRefresh.update();
}

static void TridentDrv_Freeze(int flag)
{
	//dlog_msg("%s: flag: %d", __func__, flag);
	screenRefresh.freeze(flag);
}

static void Trident_Update(PSD psd, MWCOORD x, MWCOORD y, MWCOORD width, MWCOORD height)
{
	//dlog_msg("%s: x: %d y: %d width: %d height: %d", __func__, x, y, width, height);
	screenRefresh.update();
}

///////////////////////////
static int Trident_Init(PSD psd)
{

	displayInfo_t info;
	SVC_INFO_DISPLAY_EXT(&info);
	
	dlog_msg("Screen size (%d,%d) bpp=%d",info.width, info.height, info.bitsPerPixel);

	if(info.bitsPerPixel != 16 && info.bitsPerPixel != 1)
	{
		dlog_error("Unsupported bpp!!!");
		return 0;
	}
	
	displayWidth = info.width;
	lineCount = info.height;
	bpp = info.bitsPerPixel;

	if(displayWidth * lineCount * info.bitsPerPixel / 8 > dispBufCount*sizeof(short))
	{
		dlog_error("Screen size grater than buffer!!!");
		return 0;
	}

	psd->addr = (unsigned char *)displayBuf;
	
	switch (psd->bpp) {
		case 16:
			psd->pixtype = MWPF_TRUECOLOR565;
			psd->pitch = displayWidth * sizeof(displayBuf[0]);		// linelen in bytes
			psd->size = psd->yres * psd->pitch;
			psd->data_format = MWIF_RGB565;
			break;
		case 1:

			psd->pixtype = MWPF_PALETTE;
			psd->pitch = displayWidth / 8;		// linelen in bytes
			psd->size = psd->yres * psd->pitch;
			psd->data_format = MWIF_PAL1;
			
			break;
		default:	
			dlog_error("Unsupported bpp!!!");
			return 0;
	}
	
	// NOTE: MUST be set to PIXEL_MODE for display_frame_buffer to work!
//	set_display_coordinate_mode(PIXEL_MODE);
	
	screenRefresh.start();

	return 1;
}

