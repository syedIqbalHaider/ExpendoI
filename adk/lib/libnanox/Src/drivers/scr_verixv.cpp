/*
 * screen driver for VerixV
 */
#include <svc.h>

#define MWINCLUDECOLORS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>

#include <liblog/logsys.h>

#include "device.h"
#include "fb.h"
#include "genmem.h"
#include "genfont.h"
#include "mwtypes.h"

#include "scr_verixv.h"
 #include "scr_tridentsub.h"  // Trident screen subdriver defines

#include <limits.h>

// global console handle
int console = -1;

//static unsigned char GuiLib_DisplayBuf[GuiConst_IBYTE_LINES][GuiConst_IBYTES_PR_LINE];
//unsigned short GuiLib_DisplayBuf[GuiConst_IBYTE_LINES][GuiConst_IBYTES_PR_LINE];
static int screenLines;
static int charSizeRow, charSizeCol;	

static unsigned char g_bScreenUpdated;

MWPALENTRY * g_pal;

VINITFUNC g_InitFunc = NULL;
VREFRESHFUNC g_RefreshFunc = NULL;
VFREEZEFUNC g_FreezeFunc = NULL;

static void VxV_close(PSD psd);
static void VxV_getscreeninfo(PSD psd, PMWSCREENINFO psi);
static void VxV_setpalette(PSD psd, int first, int count, MWPALENTRY * pal);
static PSD VxV_open(PSD psd);
static PSD VxV_allocatememgc(PSD psd);
static MWBOOL VxV_mapmemgc(PSD mempsd, MWCOORD w, MWCOORD h, int planes, int bpp, int data_format, unsigned int pitch, int size, void *addr);
static void VxV_freememgc(PSD mempsd);
static void VxV_update(PSD psd, MWCOORD x, MWCOORD y, MWCOORD width, MWCOORD height);

	
/* ---------------------------------------------------------------- */
extern "C"
{
	// TODO: Update should be automatic! Right now callers force refresh which slows down whole process
	void GuiDisplay_Refresh(int force);
	void GuiDisplay_Freeze(int flag);
}


SCREENDEVICE scrdev = {
	0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, 0, 0, 0, 0, 0, 0, -1,
	gen_fonts,
	VxV_open,
	VxV_close,
	VxV_setpalette,
	VxV_getscreeninfo,
	VxV_allocatememgc,
	VxV_mapmemgc,
	VxV_freememgc,
	NULL, //gen_setportrait,
	VxV_update,				/* Update*/
	NULL				/* PreSelect*/
};


static PSD VxV_open(PSD psd)
{
	DPRINTF( "VxDrv_open" );
	
    console = get_console(0);
    if( console < 0 )
    {
	    EPRINTF( "error getting console handle" );
        return NULL;
    }

	displayInfo_t info;
	SVC_INFO_DISPLAY_EXT(&info);
	
	dlog_msg("Screen size (%d,%d) bpp=%d",info.width, info.height, info.bitsPerPixel);

	if(info.bitsPerPixel != 16 && info.bitsPerPixel != 1)
	{
		dlog_error("Unsupported bpp!!!");
		return 0;
	}

	psd->planes = 1;
	psd->bpp = info.bitsPerPixel;
	
	psd->ncolors = 1 << psd->bpp;
	psd->flags = PSF_SCREEN;
	psd->portrait = MWPORTRAIT_NONE;

	psd->xres = psd->xvirtres = info.width;
	psd->yres = psd->yvirtres = info.height;

	
	PSUBDRIVER pSubdriver = TridentDrv_Select(psd);
	
	set_subdriver(psd, pSubdriver);

	//Init subdriver
	if(g_InitFunc)
		g_InitFunc(psd);	

	// clear the screen on open
	GuiDisplay_Refresh(0);

	return psd;
}

static void VxV_close(PSD psd)
{
	DPRINTF( "VxV_close" );
}

static void VxV_getscreeninfo(PSD psd, PMWSCREENINFO psi)
{
	//dlog_msg( "VxV_getscreeninfo" );

	psi->rows = psd->yvirtres;
	psi->cols = psd->xvirtres;
	psi->planes = psd->planes;
	psi->bpp = psd->bpp;
	psi->ncolors = psd->ncolors;
	psi->pixtype = psd->pixtype;
	psi->fonts = NUMBER_FONTS;

    switch (psd->pixtype) 
    {
    case MWPF_TRUECOLOR8888:
      psi->rmask	= RMASKBGRA;
      psi->gmask	= GMASKBGRA;
      psi->bmask	= BMASKBGRA;
      psi->amask	= AMASKBGRA;
      break;
    case MWPF_TRUECOLOR888:
      psi->rmask	= RMASKBGR;
      psi->gmask	= GMASKBGR;
      psi->bmask	= BMASKBGR;
      psi->amask	= AMASKBGR;
      break;
    case MWPF_TRUECOLOR565:
      psi->rmask 	= RMASK565;
      psi->gmask 	= GMASK565;
      psi->bmask	= BMASK565;
	  psi->amask	= AMASK565;
      break;
    case MWPF_TRUECOLOR555:
	  psi->rmask = RMASK555;
	  psi->gmask = GMASK555;
	  psi->bmask = BMASK555;
	  psi->amask = AMASK555;
      break;
    case MWPF_TRUECOLOR332:
      psi->rmask 	= 0xe0;
      psi->gmask 	= 0x1c;
      psi->bmask	= 0x03;
      break;
    case MWPF_PALETTE:
      psi->rmask 	= 0xff000000;
      psi->gmask 	= 0x00ff0000;
      psi->bmask	= 0x0000ff00;
      break;
    default:
      psi->rmask 	= 0xff;
      psi->gmask 	= 0xff;
      psi->bmask	= 0xff;
      break;
    }

	
	//psi->xdpcm = 20;
	//psi->ydpcm = 20;
	
	psi->xdpcm = 44;	// 240 dots / 5.5 cm (Rounded Up)
	psi->ydpcm = 46;	// 320 dots / 7 cm (Rounded Up)				
}

static void VxV_setpalette(PSD psd, int first, int count, MWPALENTRY * pal)
{
//	dlog_msg( "VxV_setpalette, psd = %x, first = %d, count = %d", psd, first, count );
    g_pal = pal;
}


/* allocate a memory offscreen screen device (pixmap)*/
static PSD VxV_allocatememgc(PSD psd)
{
	PSD	mempsd;

	assert(psd == &scrdev);

	mempsd = (PSD)malloc(sizeof(SCREENDEVICE));
	if (!mempsd)
		return NULL;

	/* copy passed device get initial values*/
	*mempsd = *psd;

	/* initialize*/
	mempsd->flags = PSF_MEMORY;			/* reset PSF_SCREEN or PSF_ADDRMALLOC flags*/
	mempsd->portrait = MWPORTRAIT_NONE; /* don't rotate offscreen pixmaps*/
	mempsd->addr = NULL;
	mempsd->Update = NULL;				/* no external updates required for mem device*/
	mempsd->palette = NULL;				/* don't copy any palette*/
	mempsd->palsize = 0;
	mempsd->transcolor = MWNOCOLOR;		/* no transparent colors unless set by image loader*/

	return mempsd;
}

/* 
 * Initialize memory device with passed parms,
 * select suitable framebuffer subdriver,
 * and set subdriver in memory device.
 *
 * Pixmaps are always drawn using linear fb drivers
 * in non-portrait mode.
 */
static MWBOOL VxV_mapmemgc(PSD mempsd, MWCOORD w, MWCOORD h, int planes, int bpp, int data_format,
	unsigned int pitch, int size, void *addr)
{
	PSUBDRIVER subdriver;

	assert(mempsd->flags & PSF_MEMORY);

	/* pixmaps are always in non-portrait orientation*/
	mempsd->xres = w;
	mempsd->yres = h;		

	mempsd->xvirtres = w;
	mempsd->yvirtres = h;
	mempsd->planes = planes;
	mempsd->bpp = bpp;
	mempsd->data_format = data_format;
	mempsd->pitch = pitch;
	mempsd->size = size;
	mempsd->addr = (unsigned char *)addr;

	/* select and init hw compatible framebuffer subdriver for pixmap drawing*/

	subdriver = TridentDrv_Select(mempsd);
	if(!subdriver)
		return 0;

	set_subdriver(mempsd, subdriver);

	return 1;
}

static void VxV_freememgc(PSD mempsd)
{
	assert(mempsd->flags & PSF_MEMORY);
//	/* may be an image header, just return*/
//	if (!(mempsd->flags & PSF_MEMORY))
//		return;

	if (mempsd->addr && (mempsd->flags & PSF_ADDRMALLOC))
		free(mempsd->addr);

	if (mempsd->palette)
		free(mempsd->palette);
	free(mempsd);
}

static void VxV_update(PSD psd, MWCOORD x, MWCOORD y, MWCOORD width, MWCOORD height)
{
	g_RefreshFunc(false);
}

// TODO: REMOVE THIS!
void GuiDisplay_Refresh(int force)
{
	if (g_RefreshFunc)
		g_RefreshFunc(force);
}

void GuiDisplay_Freeze(int flag)
{
	if (g_FreezeFunc)
		g_FreezeFunc(flag);
}

