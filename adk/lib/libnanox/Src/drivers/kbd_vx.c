/*
 * Copyright (c) 2000 Greg Haerr <greg@censoft.com>
 *
 * VerixV Keyboard Driver
 */
#include <svc.h>
#include <errno.h>
#include <stdio.h>
#include "device.h"

/*
  The VerixV keyboard driver returns the following values for keypresses:
  ascii 0-9
  Need to map these to Microwindows keys
  Note that the openpay keypad driver does not currently do keycodes or scancodes.
*/

static int  VxV_Open(KBDDEVICE *pkd);
static void VxV_Close(void);
static void VxV_GetModifierInfo(MWKEYMOD *modifiers, MWKEYMOD *curmodifiers);
static int  VxV_Read(MWKEY *buf, MWKEYMOD *modifiers, MWSCANCODE *scancode);
static int  VxV_Poll(void);

KBDDEVICE kbddev = {
	VxV_Open,
	VxV_Close,
	VxV_GetModifierInfo,
	VxV_Read,
	VxV_Poll
};

extern int console;

/* The global keymod state */

static MWKEYMOD VxV_keymod = 0;

typedef unsigned long tstamp_t;

typedef struct 
{
	tstamp_t ts;
	char scankey;
	char lastkey;

	char pending_char;
	char pending_scan;
	int  pending_ret;
	
} key_time;

//static const int ALPHA_TOUT = 1000;
static const int ALPHA_TOUT = 0;
static key_time kbd_stamp;


/*
ALPHA 0x8F SI with the high-order bit set.
F0a 0xEE n with the high-order bit set.
F1 0xFA z with the high-order bit set.
F2 0xFB { with the high-order bit set.
F3 0xFC | with the high-order bit set.
F4 0xFD } with the high-order bit set.
F5a 0xEF 0 with the high-order bit set.
a (leftmost horizontal screen key) 0xE1 a with the high-order bit set.
b (mid-left horizontal screen key) 0xE2 b with the high-order bit set.
c (mid-right horizontal screen key) 0xE3 c with the high-order bit set.
d (rightmost horizontal screen key) 0xE4 d with the high-order bit set.
*/
/*
 * Poll for keyboard events
 */
static int
VxV_Poll(void)
{
	if( kbd_stamp.pending_char || kbd_pending_count() > 0 )
		return 1;

	return 0;
}

/*
 * Open the keyboard.
 */
static int
VxV_Open(KBDDEVICE *pkd)
{
	console = get_console(0);
	if( console < 0 )
	{
		dlog_error (__FILE__, __LINE__, "error getting console handle" );
		return -1;
	}
	DPRINTF( "KbdVx_open, console = %d", console );
	return EVT_KBD;	/* -2 = no kbd*/
}

/*
 * Close the keyboard.
 */
static void
VxV_Close(void)
{
}

/*
 * Return the possible modifiers for the keyboard.
 */
static  void
VxV_GetModifierInfo(MWKEYMOD *modifiers, MWKEYMOD *curmodifiers)
{
	if (modifiers)
		*modifiers = MWKMOD_NUM | MWKMOD_CAPS;
/*		
			| MWKMOD_LCTRL
			| MWKMOD_LSHIFT | MWKMOD_RSHIFT
			| MWKMOD_LALT | MWKMOD_RALT
			| MWKMOD_LMETA | MWKMOD_RMETA;
*/			
	if (curmodifiers)
		*curmodifiers = VxV_keymod;
}


static struct {
	MWKEY vxKey;
	MWKEY mwKey;
} vxkey_to_mwkey[] = {

	//Red, yellow, green
	{0x0D, MWKEY_ENTER},
	{0x9B, MWKEY_ESCAPE},
	{0x1B, MWKEY_ESCAPE},
	{0x08, MWKEY_BACKSPACE},
	{0x0E, MWKEY_BACKSPACE},

	//Vx675 joystick
//	{0x5A, MWKEY_UP},
//	{0x5B, MWKEY_DOWN},
//	{0x5C, MWKEY_LEFT},
//	{0x5D, MWKEY_RIGHT},
//	{0x5E, MWKEY_ENTER},
	{0x5A, MWKEY_F1},  //up
	{0x5C, MWKEY_F2},  //left
	{0x5D, MWKEY_F3},  //right
	{0x5B, MWKEY_F4}, //down
	{0x5E, MWKEY_F7},
	
	//Vx520 F1-F4
	{0x7A, MWKEY_F1},
	{0x7B, MWKEY_F2},
	{0x7C, MWKEY_F3},
	{0x7D, MWKEY_F4},

	//Vx520 purple+alpha
	{0x61, MWKEY_F5},
	{0x62, MWKEY_F6},
	{0x0F, MWKEY_F7},
	{0x63, MWKEY_F8},
	{0x64, MWKEY_F9},

	//Vx520 and others: *,#
	{0x2A, MWKEY_F10},
	{0x23, MWKEY_F11},

	//Vx675 -,+
	{0x2d, MWKEY_F10},
	{0x2b, MWKEY_F11},
	
	{0xffff, 0xffff}
};


static int
VxV_Read(MWKEY * kbuf, MWKEYMOD *modifiers, MWSCANCODE *scancode)
{
	int cc = 0;
	char buf[1];
	int keykonv = 0;
	int i;
	
    *modifiers = 0;         /* no modifiers yet */
    *scancode = 0;          /* no scancode yet */
	
	if(kbd_stamp.pending_char)
	{
		*kbuf = kbd_stamp.pending_char;
		*scancode = kbd_stamp.pending_scan;
		kbd_stamp.pending_char = 0;
		
		return kbd_stamp.pending_ret;
	}
	
	cc = read( console, (char*)&buf, 1 );

	if (cc < 0) {
		if ((errno != EBADF) && (errno != EACCES) && (errno != EINVAL)) {
			EPRINTF("VerixV Kbd Read Error: %d", errno );
			return (-1);
		} else {
			return (0);
		}
	}
	if (cc == 0)
	{
		return (0);
	}

	*modifiers = 0; /*	We don't have any modifiers */

	DPRINTF( "Key = %X", *buf  );
	*buf &= 0x7F;

	
	*kbuf = *buf;
	
	for(i=0;vxkey_to_mwkey[i].vxKey != 0xffff;++i)
		if(vxkey_to_mwkey[i].vxKey == *buf)
			{
				*kbuf = vxkey_to_mwkey[i].mwKey;
				keykonv = 1;
				break;
			}
	
	if (!keykonv)
	{
		int isDown = 0;
		tstamp_t curr_ts;
	
		switch(*kbuf) {
			case MWKEY_NUMLOCK:
			if (!isDown)
				VxV_keymod ^= MWKMOD_NUM;
			break;
			case MWKEY_CAPSLOCK:
			if (!isDown)
				VxV_keymod ^= MWKMOD_CAPS;
			break;
		}
		
		curr_ts = read_ticks();
		if( kbd_stamp.scankey && (kbd_stamp.scankey == *buf) && (curr_ts-kbd_stamp.ts)<ALPHA_TOUT )
		{
			kbd_stamp.pending_char = kbd_stamp.lastkey = alpha_shift(kbd_stamp.lastkey);	
			kbd_stamp.ts = curr_ts;
			
			kbd_stamp.pending_scan = kbd_stamp.pending_char;
			kbd_stamp.pending_ret = 1;
			
			*kbuf = *scancode = MWKEY_BACKSPACE;
		}
		else
		{
			kbd_stamp.lastkey = kbd_stamp.scankey = *buf;
			kbd_stamp.ts = curr_ts;
			
			kbd_stamp.pending_char = *kbuf;	
			kbd_stamp.pending_scan = *kbuf;
			kbd_stamp.pending_ret = 2;
		}
	}

	
	
	*scancode = *kbuf;
//	*scancode = 0;
	//*kbuf = kbd_codes[(unsigned int)*buf].mwin_code;
	//*scancode = kbd_codes[(unsigned int)*buf].scancode;
	DPRINTF( "KBuf = %X Scan = %X key='%c'", *kbuf, *scancode, *kbuf);

	/* This function should return the keydown status.
	   Since the keypad driver currently only gives us keypresses, not keyreleases
	   then every time we read a key, keydown is active.
	   So return 1 always for non-error case. 									*/
	return 1;
}
