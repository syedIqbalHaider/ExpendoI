#ifndef TS_DEVICE
#define TS_DEVICE "MX870"

#define TS_DEVICE_FILE "/dev/touchpad"

struct ts_event {
	short x;
	short y;
	short pressure;
};

#endif
