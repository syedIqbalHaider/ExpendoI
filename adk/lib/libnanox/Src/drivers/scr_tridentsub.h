#ifndef VFI_NANOX_SCR_TRIDENSUB_H__
#define VFI_NANOX_SCR_TRIDENSUB_H__

#ifdef __cplusplus
extern "C"
{
#endif

extern PSUBDRIVER TridentDrv_Select(PSD psd);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* VFI_NANOX_SCR_TRIDENSUB_H__ */
