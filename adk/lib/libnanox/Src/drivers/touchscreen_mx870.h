#ifndef TS_DEVICE
#define TS_DEVICE "MX870"

#define TS_DEVICE_FILE "/dev/input/mice"

struct
{
    u_char Left : 1;
    u_char Right : 1;
    u_char Middle : 1;
    u_char : 1;
    u_char XSign : 1;
    u_char YSign : 1;
    u_char Xover : 1;
    u_char Yover : 1;
    u_char X0_7;
    u_char Y0_7;
}  __attribute__((packed)) pkt;
union
{
    struct
    {
        u_short X0_7 : 8;
        u_short X8 : 1;
//      u_short : 7;
        u_short Y0_7 : 8;
        u_short Y8 : 1;
//      u_short : 7;
    } __attribute__((packed));
    struct
    {
        short X : 9;
//      short : 7;
        short Y : 9;
//      short : 7;
    }  __attribute__((packed));
}  __attribute__((packed)) xy;

short X = TPAD_MAX_X/4;
short Y = TPAD_MAX_Y/4;

#endif
