/*
 * Copyright (c) 2008 Davide Rizzo <davide@elpa.it>
 *
 * Keyboard Driver, standard input events version
 */

//globals for ux100 function pointers

struct ux100SecureVersionStruct
{	
	char id_mode[50];
	char family[10];
	char version[12];

};

int (*__ux100_pinStartPinEntry)( unsigned char  pinLenMin, unsigned char pinLenMax, int timeout );
int (*__ux100_pinAbort)( void );
int (*__ux100_pinDelete)(int);
int (*__ux100_pinSendEncPinBlockToVault)( void );  
int (*__ux100_pinGetNbCurrDigitEntered)( void );
int (*__ux100_pinEntryMode)( void );

int (*__ux100_keybdInit)( void );
int (*__ux100_keybdGetKey)(unsigned int timeout);
int (*__ux100_dispSetBacklightMode)(int mode, unsigned int time);
struct ux100SecureVersionStruct (*__ux100_infoGetSecureVersion)(void);
int (*__ux100_infoGetPairingStatus)(void);



#define UX100_PIN_MIN 4
#define UX100_PIN_MAX 4
#define UX100_PIN_TIMEOUT 300*1000
#define UX100_PIN_INTERCHAR_TIMEOUT	10*1000

#define UX100_ENTER 0x10
#define UX100_CANCEL 0x0D
#define UX100_CLEAR 0x0E
#define UX100_HELP 0x0F
#define UX100_DIGIT 0x11

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/input.h>
#include "device.h"
#include "keymap_standard.h"
#include <string.h>
#include <dlfcn.h>

// #include <linux/input.h>



#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))

#define KBD_ENV_VAR_NAME "KEYBOARD"

static int fd = -1;
static MWKEYMOD curmodif = 0, allmodif = 0;
static const char KEYPAD_NAME [] = "Keypad";
static const char KEYPAD_NAME_UX300 [] = "Verifone Virtual Keyboard UX300";

/* Yes, it's a dirty hack to provide the same keymap details only to nxlib */
const MWKEY * const _nanox_event_keymap = keymap;
const unsigned int _nanox_event_keymap_size = ARRAY_SIZE( keymap );

/*
 * Open the keyboard.
 */

void init_ux100()
{
   static int inited;
   if(inited) return;

   inited = 1;

   void *handle;

   handle=dlopen("/usr/local/lib/svcmgr/libsvc_ux100.so",RTLD_LAZY);
   if(handle) 
   {
      __ux100_pinStartPinEntry         =(int (*)(unsigned char,unsigned char, int))dlsym(handle,"ux100_pinStartPinEntry");
      __ux100_pinDelete                =(int (*)(int))                             dlsym(handle,"ux100_pinDelete");
      __ux100_pinAbort                 =(int (*)())                                dlsym(handle,"ux100_pinAbort");
      __ux100_pinSendEncPinBlockToVault=(int (*)())                                dlsym(handle,"ux100_pinSendEncPinBlockToVault");
      __ux100_pinGetNbCurrDigitEntered =(int (*)())                                dlsym(handle,"ux100_pinGetNbCurrDigitEntered");
      __ux100_pinEntryMode             =(int (*)())                                dlsym(handle,"ux100_pinEntryMode");

	  __ux100_keybdInit					=(int (*)())                                dlsym(handle,"ux100_keybdInit");
	  __ux100_keybdGetKey				=(int (*)(unsigned int))                                dlsym(handle,"ux100_keybdGetKey");
	  __ux100_dispSetBacklightMode		=(int (*)(int, unsigned int))                                dlsym(handle,"ux100_dispSetBacklightMode");

	  __ux100_infoGetSecureVersion		=(struct ux100SecureVersionStruct (*)())	dlsym(handle, "ux100_infoGetSecureVersion");
	  __ux100_infoGetPairingStatus		=(int (*)())								dlsym(handle, "ux100_infoGetPairingStatus");

      //__ux100_secGetRemovalSwitchChallenge=(struct uxByteBuffer (*)())     dlsym(handle,"ux100_secGetRemovalSwitchChallenge");
      //__ux100_secRemovalSwitchReset       =(int (*)( struct uxByteBuffer)) dlsym(handle,"ux100_secRemovalSwitchReset");
      //__ux100_infoGetRemSwStatus          =(int (*)())                     dlsym(handle,"ux100_infoGetRemSwStatus");
      //__ux100_secPerformParing            =(int (*)(int))                  dlsym(handle,"ux100_secPerformParing");
      //__ux100_infoGetOperationalMode      =(int (*)())                     dlsym(handle,"ux100_infoGetOperationalMode");

      if(!__ux100_pinStartPinEntry || !__ux100_pinDelete || !__ux100_pinAbort || !__ux100_pinSendEncPinBlockToVault
         || !__ux100_pinGetNbCurrDigitEntered || !__ux100_pinEntryMode || !__ux100_keybdInit || !__ux100_keybdGetKey || !__ux100_dispSetBacklightMode
      //   || !__ux100_secGetRemovalSwitchChallenge || !__ux100_secRemovalSwitchReset  || !__ux100_infoGetRemSwStatus     
      //   || !__ux100_secPerformParing || !__ux100_infoGetOperationalMode 
      ) 
      {
         __ux100_pinStartPinEntry         =0; 
         __ux100_pinDelete                =0; 
         __ux100_pinAbort                 =0; 
         __ux100_pinSendEncPinBlockToVault=0; 
         __ux100_pinGetNbCurrDigitEntered =0; 
         __ux100_pinEntryMode             =0; 
		 __ux100_keybdInit					=0;
		 __ux100_keybdGetKey				=0;
		 __ux100_dispSetBacklightMode		=0;
         //__ux100_secGetRemovalSwitchChallenge=0; 
         //__ux100_secRemovalSwitchReset       =0; 
         //__ux100_infoGetRemSwStatus          =0; 
         //__ux100_secPerformParing            =0; 
         //__ux100_infoGetOperationalMode      =0; 
         dlclose(handle);
      }
	  else
	  {
	  		struct ux100SecureVersionStruct secVersion;
	  		dlog_msg("DETECTED UX300/UX100 device");
			if(__ux100_infoGetSecureVersion != NULL)
			{
				secVersion = __ux100_infoGetSecureVersion();
				char msg[100];
				dlog_msg("UX100 id:%s, family:%s, ver:%s ", secVersion.id_mode, secVersion.family, secVersion.version);
				int pairing_status = __ux100_infoGetPairingStatus();
				dlog_msg("UX100 Pairing status: %d", pairing_status);
			}
	  }
   	}
}


static int KBD_Open(KBDDEVICE *pkd)
{
	int i, r;
	char fname[64];
	char *env_devname;

	// If device name is provided, use it
	env_devname = getenv(KBD_ENV_VAR_NAME);
	if(env_devname != NULL){
		fd = open(env_devname, O_RDONLY | O_NONBLOCK);
		if( fd >= 0 ){
		DPRINTF( "Using Keypad: %s", env_devname );
			return fd;
		}
	}

	init_ux100();
	if(__ux100_keybdInit)
		__ux100_keybdInit();
	
    // Search by name first
    for (i = 0; i < 32; i++)
    {
        sprintf( fname, "/sys/class/input/event%d/device/name", i );
        fd = open( fname, O_RDONLY );
        if (fd < 0) break; // the last one
        r = read(fd, fname, sizeof(fname));
        close(fd);

        if ( !memcmp( fname, KEYPAD_NAME, ( sizeof KEYPAD_NAME - 1 ) ) ||
             !memcmp( fname, KEYPAD_NAME_UX300, ( sizeof KEYPAD_NAME_UX300 - 1 ) ) )
        {
            sprintf( fname, "/dev/input/event%d", i );
            fd = open( fname, O_RDONLY | O_NONBLOCK );
            if (fd < 0) continue;
            DPRINTF( "Using Keypad: %s", fname );
            return fd;
        }
    }

    // Original logic
	for (i = 0; i < 32; i++)
	{
		sprintf(fname, "/sys/class/input/event%d/device/capabilities/ev", i);
		fd = open(fname, O_RDONLY);
		if (fd < 0)
			continue;
		r = read(fd, fname, sizeof(fname));
		close(fd);
		if (r <= 0)
			continue;
    fname[r - 1] = '\0';
		if ((strtoul(fname, NULL, 16) & (1 << EV_MSC)) == 0)
			continue;
    sprintf(fname, "/dev/input/event%d", i);
		fd = open(fname, O_RDONLY | O_NONBLOCK);
		if (fd < 0)
			continue;
		curmodif = 0;
		/* TODO: Assign allmodif */
		allmodif = MWKMOD_LSHIFT | MWKMOD_RSHIFT| MWKMOD_LCTRL |
                        MWKMOD_RCTRL | MWKMOD_LALT | MWKMOD_RALT |
			MWKMOD_LMETA | MWKMOD_RMETA | MWKMOD_NUM |
                        MWKMOD_CAPS | MWKMOD_ALTGR | MWKMOD_SCR;
        DPRINTF( "Using Keypad: %s", fname );
    return fd;
	}
	EPRINTF("Error %d opening keyboard input device\n", errno);
	return errno;
}

/*
 * Close the keyboard.
 */
static void KBD_Close(void)
{
 	if(fd >= 0)
		close(fd);
	fd = -1;
}

/*
 * Return the possible modifiers for the keyboard.
 */
static void KBD_GetModifierInfo(MWKEYMOD *modifiers, MWKEYMOD *curmodifiers)
{
	curmodif &= allmodif;
	if (modifiers)
		*modifiers = allmodif;
	if (curmodifiers)
		*curmodifiers = curmodif;
}

/*
 * This reads one keystroke from the keyboard, and the current state of
 * the mode keys (ALT, SHIFT, CTRL).  Returns -1 on error, 0 if no data
 * is ready, and 1 if data was read.  This is a non-blocking call.
 */

static int KBD_Read(MWKEY *buf, MWKEYMOD *modifiers, MWSCANCODE *pscancode)
{
	struct input_event event;
	int bytes_read;
	/* read a data point */
	while ((bytes_read = read(fd, &event, sizeof(event))) == sizeof(event))
	{
		// EPRINTF("Key code %d %d %d\n", event.type, event.code, event.value);
		//dlog_msg("Key code %d %d %d\n", event.type, event.code, event.value);
		if (event.type == EV_KEY)
		{
			if (event.value)
			{
				switch (event.code)
				{
				case KEY_LEFTSHIFT:
					curmodif |= MWKMOD_LSHIFT;
					break;
				case KEY_RIGHTSHIFT:
					curmodif |= MWKMOD_RSHIFT;
					break;
				case KEY_LEFTCTRL:
					curmodif |= MWKMOD_LCTRL;
					break;
				case KEY_RIGHTCTRL:
					curmodif |= MWKMOD_RCTRL;
					break;
				case KEY_LEFTALT:
					curmodif |= MWKMOD_LALT;
					break;
				case KEY_RIGHTALT:
					curmodif |= MWKMOD_RALT;
					break;
				case KEY_LEFTMETA:
					curmodif |= MWKMOD_LMETA;
					break;
				case KEY_RIGHTMETA:
					curmodif |= MWKMOD_RMETA;
					break;
				case KEY_NUMLOCK:
					curmodif |= MWKMOD_NUM;
					break;
				case KEY_CAPSLOCK:
					curmodif |= MWKMOD_CAPS;
					break;
				case KEY_SCROLLLOCK:
					curmodif |= MWKMOD_SCR;
					break;
				}
			}
			else
			{
				switch (event.code)
				{
				case KEY_LEFTSHIFT:
					curmodif &= ~MWKMOD_LSHIFT;
					break;
				case KEY_RIGHTSHIFT:
					curmodif &= ~MWKMOD_RSHIFT;
					break;
				case KEY_LEFTCTRL:
					curmodif &= ~MWKMOD_LCTRL;
					break;
				case KEY_RIGHTCTRL:
					curmodif &= ~MWKMOD_RCTRL;
					break;
				case KEY_LEFTALT:
					curmodif &= ~MWKMOD_LALT;
					break;
				case KEY_RIGHTALT:
					curmodif &= ~MWKMOD_RALT;
					break;
				case KEY_LEFTMETA:
					curmodif &= ~MWKMOD_LMETA;
					break;
				case KEY_RIGHTMETA:
					curmodif &= ~MWKMOD_RMETA;
					break;
				case KEY_NUMLOCK:
					curmodif &= ~MWKMOD_NUM;
					break;
				case KEY_CAPSLOCK:
					curmodif &= ~MWKMOD_CAPS;
					break;
				case KEY_SCROLLLOCK:
					curmodif &= ~MWKMOD_SCR;
					break;
				}
			}
            // Special magic value for '#' on Vx520 and probably others
            if ( event.code == KEY_NUMERIC_POUND )
            {
                event.code = 88; // mapping to MWKEY_KP_HASH
            }
            
            // Special magic value for 'INFO' on Ux300 and probably others
            if ( event.code == KEY_HELP )
            {
                event.code = 87; // mapping to MWKEY_F11
            }

#ifdef VFI_GUI_GUIAPP
			if (event.code == 240)	//ux100 numeric key
			{
				event.code = 11;	//mapping to '0'
			}
#endif				
			
			if (*modifiers)
				*modifiers = curmodif;
			if (event.code < ARRAY_SIZE(keymap))
			{
				*buf = keymap[event.code];

#ifdef VFI_GUI_GUIAPP
				*pscancode = *buf;
#else				
				*pscancode = event.code;
#endif				
				/*if (*buf == MWKEY_ESCAPE)
					return -2;*/
				return event.value ? 1 : 2;
			}
		}
	}
	if(bytes_read == -1)
	{
		if (errno == EINTR || errno == EAGAIN) return 0;
		EPRINTF("Error %d reading from keyboard\n", errno);
		return -1;
	}
	if(bytes_read != 0)
	{
		EPRINTF("Wrong number of bytes %d read from keyboard "
		"(expected %d)\n", bytes_read, sizeof(event));
		return -1;
	}
	return 0;
}

#define UX100_PIN_INTERCHAR_TIMEOUT	10*1000

static int KBD_Read_UX100(MWKEY *buf, MWKEYMOD *modifiers, MWSCANCODE *pscancode)
{
	if(__ux100_keybdGetKey == NULL)
		return(KBD_Read(buf, modifiers, pscancode));

	if((*pscancode = __ux100_keybdGetKey(UX100_PIN_INTERCHAR_TIMEOUT)) == -1)
		return -1;

	return 1;
}


static int
KBD_Poll(void)
{
	return 1;
}

KBDDEVICE kbddev = {
	KBD_Open,
	KBD_Close,
	KBD_GetModifierInfo,
	KBD_Read,
	KBD_Poll
};

