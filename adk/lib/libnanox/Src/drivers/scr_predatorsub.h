#ifndef VFI_NANOX_SCR_PREDATORSUB_H__
#define VFI_NANOX_SCR_PREDATORSUB_H__

#ifdef __cplusplus
extern "C"
{
#endif

extern PSUBDRIVER VxDrv_Select(PSD psd);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* VFI_NANOX_SCR_PREDATORSUB_H__ */
