/*
 * Copyright (c) 2009 VeriFone
 *
 * VerixV Mouse Driver
 */
#include <stdio.h>
#include "device.h"
#include <svc.h>

#define	SCALE		3	/* default scaling factor for acceleration */
#define	THRESH		5	/* default threshhold for acceleration */

static int  	MouVx_Open(MOUSEDEVICE *pmd);
static void 	MouVx_Close(void);
static int  	MouVx_GetButtonInfo(void);
static void	MouVx_GetDefaultAccel(int *pscale,int *pthresh);
static int  	MouVx_Read(MWCOORD *dx, MWCOORD *dy, MWCOORD *dz, int *bp);
static int  	MouVx_Poll(void);

MOUSEDEVICE mousedev = {
	MouVx_Open,
	MouVx_Close,
	MouVx_GetButtonInfo,
	MouVx_GetDefaultAccel,
	MouVx_Read,
	MouVx_Poll
};

extern int console;
/*
 * Poll for events
 */

static int
MouVx_Poll(void)
{
	if( kbd_pending_count() > 0 )
		return 1;

	return 0;
}

/*
 * Open up the mouse device.
 */
static int
MouVx_Open(MOUSEDEVICE *pmd)
{
	console = get_console(0);
	if( console < 0 )
	{
		dlog_error (__FILE__, __LINE__, "error getting console handle" );
		return -1;
	}
	DPRINTF( "MouVx_open, console = %d", console );
	return 1;
}

/*
 * Close the mouse device.
 */
static void
MouVx_Close(void)
{
}

/*
 * Get mouse buttons supported
 */
static int
MouVx_GetButtonInfo(void)
{
return MWBUTTON_L;
}

/*
 * Get default mouse acceleration settings
 */
static void
MouVx_GetDefaultAccel(int *pscale,int *pthresh)
{
	*pscale = SCALE;
	*pthresh = THRESH;
}

/*
 * Attempt to read bytes from the mouse and interpret them.
 * Returns -1 on error, 0 if either no bytes were read or not enough
 * was read for a complete state, or 1 if the new state was read.
 * When a new state is read, the current buttons and x and y deltas
 * are returned.  This routine does not block.
 */
static int
MouVx_Read(MWCOORD *dx, MWCOORD *dy, MWCOORD *dz, int *bp)
{
	char keyValue;
	
	read( console, (char *)&keyValue, 1 );
	keyValue &= 0x7F;

	*dx = (short)0;
	*dy = (short)0;
	*dz = 0;

	switch( keyValue )
	{
		case '2':	// up
			*dy = -1;
			break;
		case '8':	// down
			*dy = 1;
			break;
		case '4':	// left
			*dx = -1;
			break;
		case '6':	// right
			*dx = 1;
			break;
		case '5':	// mouse button state change
			*bp = !*bp;
			break;
		default:
			break;
	}
	return 1;
}

