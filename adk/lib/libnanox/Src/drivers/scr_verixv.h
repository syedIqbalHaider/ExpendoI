#ifndef VFI_NANOX_SCR_VERIXV_H__
#define VFI_NANOX_SCR_VERIXV_H__

#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif

typedef int (*VINITFUNC)(PSD psd);
typedef void (*VREFRESHFUNC)(int force);
typedef void (*VFREEZEFUNC)(int flag);

extern VINITFUNC g_InitFunc;
extern VREFRESHFUNC g_RefreshFunc;
extern VFREEZEFUNC g_FreezeFunc;

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* VFI_NANOX_SCR_VERIXV_H__ */
