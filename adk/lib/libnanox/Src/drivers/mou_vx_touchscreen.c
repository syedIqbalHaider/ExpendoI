/*
 * Copyright (c) 2009 VeriFone
 *
 * VerixV Mouse Driver
 */
#include <stdio.h>
#include "device.h"
#include <svc.h>

/* TODO: We should have an "event reservation manager" that would allow
   allocating unused events for such purposes without assuming that this
   event won't be used.
*/
#define TAP_EVENT	EVT_BAR


#define	SCALE		3	/* default scaling factor for acceleration */
#define	THRESH		5	/* default threshhold for acceleration */

static int  	TsVx_Open(MOUSEDEVICE *pmd);
static void 	TsVx_Close(void);
static int  	TsVx_GetButtonInfo(void);
static void		TsVx_GetDefaultAccel(int *pscale,int *pthresh);
static int  	TsVx_Read(MWCOORD *dx, MWCOORD *dy, MWCOORD *dz, int *bp);
static int  	TsVx_Poll(void);

MOUSEDEVICE mousedev = 
{
	TsVx_Open,
	TsVx_Close,
	TsVx_GetButtonInfo,
	TsVx_GetDefaultAccel,
	TsVx_Read,
	TsVx_Poll,		
	MOUSE_RAW   /* Input filter flags */
};

/*
 * Poll for events
 */


/*
void signature(void) {
	int state=0;
	int old_state, x, y, old_x, old_y;

	set_display_coordinate_mode(PIXEL_MODE);

	clrscr();
	write_at("Signature",9,0,0);

	while (1) {
		old_x=x;
		old_y=y;
		old_state=state;

		state = get_touchscreen(&x,&y);

		if (state && old_state) {
			draw_line(old_x,old_y,x,y,1,BLACK);
		}

	if(chkKey()) break;
	}
}
*/

#if 0
static int
TsVx_Poll(void)
{
	int new_touch = 0, new_touch_x = 0, new_touch_y = 0;

	set_display_coordinate_mode(PIXEL_MODE);

// Look for the touch event
	new_touch = get_touchscreen (&new_touch_x, &new_touch_y);
	if ((new_touch != touch) || (new_touch_x != touch_x) || (new_touch_y != touch_y))
	{
		//DPRINTF( "Touch = %d (x=%d, y=%d)", new_touch, new_touch_x, new_touch_y );
		if (new_touch) {
			touch_x = new_touch_x;
			touch_y = new_touch_y;
		}
		touch = new_touch;
		touch_evt = 1;
		return 1;
	}
	return 0;
}
#endif

/*
 * Open up the mouse device.
 */
static int
TsVx_Open(MOUSEDEVICE *pmd)
{
	int result = 0;

	int consoleHandle = get_console(0);
	DPRINTF("MouVx_open, console = %d", consoleHandle);

	//GdHideCursor(&scrdev);  // If you want to turn off the mouse by default

	if (consoleHandle >= 0)
	{
		result = set_event_bit(consoleHandle, TAP_EVENT);
		if (result == 0)
			result = TAP_EVENT;
		else
			result = 0;
	}
	
	return result;
}

/*
 * Close the mouse device.
 */
static void
TsVx_Close(void)
{
}

/*
 * Get mouse buttons supported
 */
static int
TsVx_GetButtonInfo(void)
{
	return MWBUTTON_L;
}

/*
 * Get default mouse acceleration settings
 */
static void
TsVx_GetDefaultAccel(int *pscale,int *pthresh)
{
	*pscale = SCALE;
	*pthresh = THRESH;
}

/*
 * Attempt to read bytes from the mouse and interpret them.
 * Returns -1 on error, 0 if either no bytes were read or not enough
 * was read for a complete state, or 1 if the new state was read.
 * When a new state is read, the current buttons and x and y deltas
 * are returned.  This routine does not block.
 */
static int touch = 0;
static int touch_x = 0;
static int touch_y = 0;
 
static int
TsVx_Read(MWCOORD *dx, MWCOORD *dy, MWCOORD *dz, int *bp)
{
	int new_touch_x, new_touch_y;

	touch = get_touchscreen (&new_touch_x, &new_touch_y);

	if (touch)
	{
		/* pen down - only then we get the correct coordinates */
		touch_x = new_touch_x;
		touch_y = new_touch_y;
	}

	*dx = touch_x;
	*dy = touch_y;
	*dz = touch;

	*bp = touch ? MWBUTTON_L : 0;

//	dbprintf("TsVx_Read x=%d, y=%d, button=%d\n", *dx, *dy, *bp);
	
	if(!*bp)
		return 3;
	return 2;
}

static int TsVx_Poll(void)
{
	int new_touch, new_touch_x, new_touch_y;
	int ret;

	new_touch = get_touchscreen (&new_touch_x, &new_touch_y);

	ret = (touch != new_touch) || (new_touch && (touch_x != new_touch_x || touch_y != new_touch_y));
	
	if (new_touch)
	{
		/* pen down - only then we get the correct coordinates */
		touch_x = new_touch_x;
		touch_y = new_touch_y;
		
		// WORKAROUND: repost TAP_EVENT, //For signature we have to constant TAP_EVENT
		set_timer(1, TAP_EVENT);
	}
	
	touch = new_touch;

	return ret;
}
