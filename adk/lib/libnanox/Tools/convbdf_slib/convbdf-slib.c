/*
 * Convert BDF files to C source and/or Rockbox .fnt file format
 *
 * Copyright (c) 2002 by Greg Haerr <greg@censoft.com>
 *
 * NOTE: This version is modified to generate C sources for shared-lib ready to load under VerixV.
 * The following modifications were made:
 *	- [NEED] A Copy_MWCFONT_DLL is introduced. This is needed for position-independent
 *		libs under VerixV because pointers cannot be initialized.
 *	- [OPTIMIZATION] Because for every lib a word of data is added, it means that it will allocate
 *		at least 1024 of bytes from the RAM pool (global not a process one). So it is wise
 *		not to waste this memory and place some buffers there. 
 *	- [OPTIMIZATION] Range table is generated that allows to avoid empty holes in offset 
 *		and width tables
 *	- [OPTIMIZATION] Zero bytes prefix and postfix lenghts are calculated and 
 *		stored in separate tables. Data is stored without them under compressed_bits field
 *
 *
 * What fun it is converting font data...
 *
 * 09/17/02	Version 1.0
 */

/* karp */
#define _CRT_SECURE_NO_WARNINGS
#define _CRT_NONSTDC_NO_WARNINGS



#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


#define SIZE_T_UL(a)	((unsigned long) a)

/* BEGIN font.h*/
/* loadable font magic and version #*/
#define VERSION		"RB11"

/* MWIMAGEBITS helper macros*/
#define MWIMAGE_WORDS(x)	(((x)+15)/16)	/* image size in words*/
#define MWIMAGE_BYTES(x)	(MWIMAGE_WORDS(x)*sizeof(MWIMAGEBITS))
#define	MWIMAGE_BITSPERIMAGE (sizeof(MWIMAGEBITS) * 8)
#define	MWIMAGE_BITVALUE(n)	((MWIMAGEBITS) (((MWIMAGEBITS) 1) << (n)))
#define	MWIMAGE_FIRSTBIT	(MWIMAGE_BITVALUE(MWIMAGE_BITSPERIMAGE - 1))
#define	MWIMAGE_TESTBIT(m)	((m) & MWIMAGE_FIRSTBIT)
#define	MWIMAGE_SHIFTBIT(m)	((MWIMAGEBITS) ((m) << 1))

typedef unsigned short	MWIMAGEBITS;	/* bitmap image unit size*/

/* karp: added generating range table */
typedef struct RANGE_ENTRY 
{
	unsigned int			startChar;
	unsigned int			endChar;
	unsigned int			startIndex;
	struct RANGE_ENTRY  *	next;
} RANGE_ENTRY;

typedef struct
{
	unsigned char prefix;
	unsigned char postfix;
} GlyphZeroFill;


/* builtin C-based proportional/fixed font structure */
/* based on The Microwindows Project http://microwindows.org */
typedef struct {
	char *		name;		/* font name*/
	int		maxwidth;	/* max width in pixels*/
	int 		height;		/* height in pixels*/
	int		ascent;		/* ascent (baseline) height*/
	int		firstchar;	/* first character in bitmap*/
	int		size;		/* font size in glyphs*/

	MWIMAGEBITS *			bits;		/* 16-bit right-padded bitmap data*/
	unsigned long *			offset;		/* offsets into bitmap data*/
	unsigned char *			width;		/* character widths or NULL if fixed*/
	int		defaultchar;	/* default char (not glyph index)*/
	long		bits_size;	/* # words of MWIMAGEBITS bits*/

	/* unused by runtime system, read in by convbdf*/
	char *		facename;	/* facename of font*/
	char *		copyright;	/* copyright info for loadable fonts*/
	int		pixel_size;
	int		descent;
	int		fbbw, fbbh, fbbx, fbby;

	/* VeriFone extensions */
	struct RANGE_ENTRY	*	ranges;		/* karp: range information used to reference in all other tables*/
	unsigned int			range_cnt;	/* karp: number of ranges stored above*/

	GlyphZeroFill const * zero_fill;	/* number of zero bytes to prefix/postfix the image data */
	
	unsigned char offset_step;		/* step between each offset entry index - originally it is 1 - every glyph has its entry in the offset table */




	int realGlyphCount;
	int rangeKillsOffsets;
	
} MWCFONT, *PMWCFONT;
/* END font.h*/

#define isprefix(buf,str)	(!strncmp(buf, str, strlen(str)))
#define	strequal(s1,s2)		(!strcmp(s1, s2))

#define EXTRA	300		/* # bytes extra allocation for buggy .bdf files*/

int gen_c = 0;
int gen_stats = 0;
int gen_map = 1;
int start_char = 0;
int limit_char = 65535;
int oflag = 0;
char outfile[256];

void        usage(void);
void        getopts(int *pac, char ***pav);
int         convbdf(char *path);

void        free_font(PMWCFONT pf);
PMWCFONT    bdf_read_font(char *path);
int         bdf_read_header(FILE *fp, PMWCFONT pf);
int         bdf_read_bitmaps(FILE *fp, PMWCFONT pf);
char *      bdf_getline(FILE *fp, char *buf, int len);
MWIMAGEBITS bdf_hexval(unsigned char *buf, int ndx1, int ndx2);

int         gen_c_source(PMWCFONT pf, char *path);
int			gen_stats_file(PMWCFONT pf, char const * path);

void				ranges_init(PMWCFONT pf);
/* add new index to the table and report range index it was stored into */
void				ranges_add_index(PMWCFONT pf, unsigned int new_index);
/* debug */
void				ranges_dump(PMWCFONT pf);

/** @brief glue ranges if they touch each other */
void				ranges_glue(PMWCFONT pf);

/** @brief Updates statistics for ranges */
void ranges_updateStats(PMWCFONT pf);

/** @brief Remove unused ranges from offset and width */
void				ranges_apply2font(PMWCFONT pf);

/** @brief release ranges */
void				ranges_release(PMWCFONT pf);


void
usage(void)
{
	char help[] = {
	"Usage: convbdf [options] [input-files]\n"
	"       convbdf [options] [-o output-file] [single-input-file]\n"
	"Options:\n"
	"    -c     Convert .bdf to .c source file\n"
	"    -t     Dump font statistics .stat file\n"
	"    -s N   Start output at character encodings >= N\n"
	"    -l N   Limit output to character encodings <= N\n"
	"    -n     Don't generate bitmaps as comments in .c file\n"
	};

	fprintf(stderr, help);
}

/* parse command line options*/
void
getopts(int *pac, char ***pav)
{
	char *p;
	char **av;
	int ac;

	ac = *pac;
	av = *pav;
	while (ac > 0 && av[0][0] == '-') {
		p = &av[0][1]; 
		while( *p)
			switch(*p++) {
		case ' ':			/* multiple -args on av[]*/
			while( *p && *p == ' ')
				p++;
			if( *p++ != '-')	/* next option must have dash*/
				p = "";
			break;			/* proceed to next option*/
		case 'c':			/* generate .c output*/
			gen_c = 1;
			break;
		case 't':			/* generate statistics */
			gen_stats = 1;
			break;
		case 'n':			/* don't gen bitmap comments*/
			gen_map = 0;
			break;
		case 'o':			/* set output file*/
			oflag = 1;
			if (*p) {
				strcpy(outfile, p);
				while (*p && *p != ' ')
					p++;
			} else {
				av++; ac--;
				if (ac > 0)
					strcpy(outfile, av[0]);
			}
			break;
		case 'l':			/* set encoding limit*/
			if (*p) {
				limit_char = atoi(p);
				while (*p && *p != ' ')
					p++;
			} else {
				av++; ac--;
				if (ac > 0)
					limit_char = atoi(av[0]);
			}
			break;
		case 's':			/* set encoding start*/
			if (*p) {
				start_char = atoi(p);
				while (*p && *p != ' ')
					p++;
			} else {
				av++; ac--;
				if (ac > 0)
					start_char = atoi(av[0]);
			}
			break;
		default:
			fprintf(stderr, "Unknown option ignored: %c\r\n", *(p-1));
		}
		++av; --ac;
	}
	*pac = ac;
	*pav = av;
}

/* remove directory prefix and file suffix from full path*/
char *
basename(char *path)
{
	char *p, *b;
	static char base[256];

	/* remove prepended path and extension*/
	b = path;
	for (p=path; *p; ++p) {
		if (*p == '/')
			b = p + 1;
	}
	strcpy(base, b);
	for (p=base; *p; ++p) {
		if (*p == '.') {
			*p = 0;
			break;
		}
	}
	return base;
}

int
convbdf(char *path)
{
	PMWCFONT pf;
	int ret = 0;

	pf = bdf_read_font(path);
	if (!pf)
		exit(1);

	if (gen_c) {
		if (!oflag) {
			strcpy(outfile, basename(path));
			strcat(outfile, ".c");
		}
		ret |= gen_c_source(pf, outfile);
	}
	
	if (gen_stats) {
		if (!oflag) {
			strcpy(outfile, basename(path));
			strcat(outfile, ".stats");
		}
		ret |= gen_stats_file(pf, outfile);
	}

	free_font(pf);
	return ret;
}

int
main(int ac, char **av)
{
	int ret = 0;

	++av; --ac;		/* skip av[0]*/
	getopts(&ac, &av);	/* read command line options*/

	if (ac < 1 || (!gen_c && !gen_stats)) {
		usage();
		exit(1);
	}
	if (oflag) {
		if (ac > 1 || (gen_c && gen_stats)) {
			usage();
			exit(1);
		}
	}

	while (ac > 0) {
		ret |= convbdf(av[0]);
		++av; --ac;
	}

	exit(ret);
}

/* free font structure*/
void
free_font(PMWCFONT pf)
{
	if (!pf)
		return;
	ranges_release(pf);

	if (pf->name)
		free(pf->name);
	if (pf->facename)
		free(pf->facename);
	if (pf->bits)
		free(pf->bits);
	if (pf->offset)
		free(pf->offset);
	if (pf->width)
		free(pf->width);
	free(pf);
}

/* build incore structure from .bdf file*/
PMWCFONT
bdf_read_font(char *path)
{
	FILE *fp;
	PMWCFONT pf;

	fp = fopen(path, "rb");
	if (!fp) {
		fprintf(stderr, "Error opening file: %s\n", path);
		return NULL;
	}

	pf = (PMWCFONT)calloc(1, sizeof(MWCFONT));
	if (!pf)
		goto errout;
	
	pf->name = strdup(basename(path));
	ranges_init(pf);

	if (!bdf_read_header(fp, pf)) {
		fprintf(stderr, "Error reading font header\n");
		goto errout;
	}

	if (!bdf_read_bitmaps(fp, pf)) {
		fprintf(stderr, "Error reading font bitmaps\n");
		goto errout;
	}

	fclose(fp);
	return pf;

errout:
	fclose(fp);
	free_font(pf);
	return NULL;
}

/* read bdf font header information, return 0 on error*/
int
bdf_read_header(FILE *fp, PMWCFONT pf)
{
	int encoding;
	int nchars, maxwidth;
	int firstchar = 65535;
	int lastchar = -1;
	char buf[256];
	char facename[256];
	char copyright[256];

	/* set certain values to errors for later error checking*/
	pf->defaultchar = -1;
	pf->ascent = -1;
	pf->descent = -1;

	for (;;) {
		if (!bdf_getline(fp, buf, sizeof(buf))) {
			fprintf(stderr, "Error: EOF on file\n");
			return 0;
		}
		if (isprefix(buf, "FONT ")) {		/* not required*/
			if (sscanf(buf, "FONT %[^\n]", facename) != 1) {
				fprintf(stderr, "Error: bad 'FONT'\n");
				return 0;
			}
			pf->facename = strdup(facename);
			continue;
		}
		if (isprefix(buf, "COPYRIGHT ")) {	/* not required*/
			if (sscanf(buf, "COPYRIGHT \"%[^\"]", copyright) != 1) {
				fprintf(stderr, "Error: bad 'COPYRIGHT'\n");
				return 0;
			}
			pf->copyright = strdup(copyright);
			continue;
		}
		if (isprefix(buf, "DEFAULT_CHAR ")) {	/* not required*/
			if (sscanf(buf, "DEFAULT_CHAR %d", &pf->defaultchar) != 1) {
				fprintf(stderr, "Error: bad 'DEFAULT_CHAR'\n");
				return 0;
			}
		}
		if (isprefix(buf, "FONT_DESCENT ")) {
			if (sscanf(buf, "FONT_DESCENT %d", &pf->descent) != 1) {
				fprintf(stderr, "Error: bad 'FONT_DESCENT'\n");
				return 0;
			}
			continue;
		}
		if (isprefix(buf, "FONT_ASCENT ")) {
			if (sscanf(buf, "FONT_ASCENT %d", &pf->ascent) != 1) {
				fprintf(stderr, "Error: bad 'FONT_ASCENT'\n");
				return 0;
			}
			continue;
		}
		if (isprefix(buf, "FONTBOUNDINGBOX ")) {
			if (sscanf(buf, "FONTBOUNDINGBOX %d %d %d %d",
			    &pf->fbbw, &pf->fbbh, &pf->fbbx, &pf->fbby) != 4) {
				fprintf(stderr, "Error: bad 'FONTBOUNDINGBOX'\n");
				return 0;
			}
			continue;
		}
		if (isprefix(buf, "CHARS ")) {
			if (sscanf(buf, "CHARS %d", &nchars) != 1) {
				fprintf(stderr, "Error: bad 'CHARS'\n");
				return 0;
			}
			continue;
		}

		/*
		 * Reading ENCODING is necessary to get firstchar/lastchar
		 * which is needed to pre-calculate our offset and widths
		 * array sizes.
		 */
		if (isprefix(buf, "ENCODING ")) 
		{
			if (sscanf(buf, "ENCODING %d", &encoding) != 1) {
				fprintf(stderr, "Error: bad 'ENCODING'\n");
				return 0;
			}
			if (encoding >= 0 && encoding <= limit_char && encoding >= start_char) {
				if (firstchar > encoding)
					firstchar = encoding;
				if (lastchar < encoding)
					lastchar = encoding;

				/* karp: ranges maintenance */
				/* add new encoding index to ranges table */
				if (encoding >= 0)
				{
					ranges_add_index(pf, encoding);
				}
			}
			continue;
		}
		if (strequal(buf, "ENDFONT"))
			break;
		
	}

	/* karp: glue touching ranges */
	ranges_glue(pf);

	/* calc font height*/
	if (pf->ascent < 0 || pf->descent < 0 || firstchar < 0) {
		fprintf(stderr, "Error: Invalid BDF file, requires FONT_ASCENT/FONT_DESCENT/ENCODING\n");
		return 0;
	}
	pf->height = pf->ascent + pf->descent;

	/* calc default char*/
	if (pf->defaultchar < 0 || pf->defaultchar < firstchar ||
	    pf->defaultchar > limit_char)
		pf->defaultchar = firstchar;

	/* calc font size (offset/width entries)*/
	pf->firstchar = firstchar;
	pf->size = lastchar - firstchar + 1;
	
	/* use the font boundingbox to get initial maxwidth*/
	/*maxwidth = pf->fbbw - pf->fbbx;*/
	maxwidth = pf->fbbw;

	/* initially use font maxwidth * height for bits allocation*/
	pf->bits_size = nchars * MWIMAGE_WORDS(maxwidth) * pf->height;

	/* allocate bits, offset, and width arrays*/
	pf->bits = (MWIMAGEBITS *)malloc(pf->bits_size * sizeof(MWIMAGEBITS) + EXTRA);
	pf->offset = (unsigned long *)malloc(pf->size * sizeof(unsigned long));
	pf->width = (unsigned char *)calloc(pf->size, sizeof(unsigned char));
	
	if (!pf->bits || !pf->offset || !pf->width) {
		fprintf(stderr, "Error: no memory for font load\n");
		return 0;
	}

	return 1;
}

/* read bdf font bitmaps, return 0 on error*/
int
bdf_read_bitmaps(FILE *fp, PMWCFONT pf)
{
	long ofs = 0;
	int maxwidth = 0;
	int i, k, encoding, width;
	int bbw, bbh, bbx, bby;
	int proportional = 0;
#if 0
	int encodetable = 0;
	long l;
#endif
	char buf[256];

	/* reset file pointer*/
	fseek(fp, 0L, SEEK_SET);

	/* initially mark offsets as not used*/
	for (i=0; i<pf->size; ++i)
		pf->offset[i] = -1;

	for (;;) {
		if (!bdf_getline(fp, buf, sizeof(buf))) {
			fprintf(stderr, "Error: EOF on file\n");
			return 0;
		}
		if (isprefix(buf, "STARTCHAR")) {
			encoding = width = bbw = bbh = bbx = bby = -1;
			continue;
		}
		if (isprefix(buf, "ENCODING ")) {
			if (sscanf(buf, "ENCODING %d", &encoding) != 1) {
				fprintf(stderr, "Error: bad 'ENCODING'\n");
				return 0;
			}
			if (encoding < start_char || encoding > limit_char)
				encoding = -1;
			continue;
		}
		if (isprefix(buf, "DWIDTH ")) {
			if (sscanf(buf, "DWIDTH %d", &width) != 1) {
				fprintf(stderr, "Error: bad 'DWIDTH'\n");
				return 0;
			}
			/* use font boundingbox width if DWIDTH <= 0*/
			if (width <= 0)
				width = pf->fbbw - pf->fbbx;
			continue;
		}
		if (isprefix(buf, "BBX ")) {
			if (sscanf(buf, "BBX %d %d %d %d", &bbw, &bbh, &bbx, &bby) != 4) {
				fprintf(stderr, "Error: bad 'BBX'\n");
				return 0;
			}
			continue;
		}
		if (strequal(buf, "BITMAP")) {
			MWIMAGEBITS *ch_bitmap = pf->bits + ofs;
			int ch_words;

			if (encoding < 0)
				continue;

			/* set bits offset in encode map*/
			if (pf->offset[encoding-pf->firstchar] != (unsigned long)-1) {
				fprintf(stderr, "Error: duplicate encoding for character %d (0x%02x), ignoring duplicate\n",
					encoding, encoding);
				continue;
			}
			pf->offset[encoding-pf->firstchar] = ofs;

			/* calc char width*/
			if (bbx < 0) {
				width -= bbx;
				/*if (width > maxwidth)
					width = maxwidth;*/
				bbx = 0;
			}
			if (width > maxwidth)
				maxwidth = width;
			pf->width[encoding-pf->firstchar] = width;

			/* clear bitmap*/
			memset(ch_bitmap, 0, MWIMAGE_BYTES(width) * pf->height);

			ch_words = MWIMAGE_WORDS(width);
#define BM(row,col)	(*(ch_bitmap + ((row)*ch_words) + (col)))
#define MWIMAGE_NIBBLES	(MWIMAGE_BITSPERIMAGE/4)

			/* read bitmaps*/
			for (i=0; ; ++i) {
				int hexnibbles;

				if (!bdf_getline(fp, buf, sizeof(buf))) {
					fprintf(stderr, "Error: EOF reading BITMAP data\n");
					return 0;
				}
				if (isprefix(buf, "ENDCHAR"))
					break;

				hexnibbles = strlen(buf);
				for (k=0; k<ch_words; ++k) {
					int ndx = k * MWIMAGE_NIBBLES;
					int padnibbles = hexnibbles - ndx;
					MWIMAGEBITS value;
					
					if (padnibbles <= 0)
						break;
					if (padnibbles >= MWIMAGE_NIBBLES)
						padnibbles = 0;

					value = bdf_hexval((unsigned char *)buf,
						ndx, ndx+MWIMAGE_NIBBLES-1-padnibbles);
					value <<= padnibbles * MWIMAGE_NIBBLES;

					BM(pf->height - pf->descent - bby - bbh + i, k) |=
						value >> bbx;
					/* handle overflow into next image word*/
					if (bbx) {
						BM(pf->height - pf->descent - bby - bbh + i, k+1) =
							value << (MWIMAGE_BITSPERIMAGE - bbx);
					}
				}
			}

			ofs += MWIMAGE_WORDS(width) * pf->height;

			continue;
		}
		if (strequal(buf, "ENDFONT"))
			break;
	}

	/* set max width*/
	pf->maxwidth = maxwidth;

	/* find unused  entries and remove them (now covered by ranges table) */
	/* ranges_apply2font(pf); */

	/* determine whether font doesn't require encode table*/
	/* NOTE: This code is WRONG as later it is assumed that when offset table is missing, the offset may be calculated by (pf->height * i) */
	/* NOTE: Taking ranges into account the offset table may be eliminated in more cases but it is better to skip its output than freeing the table */
#if 0
	l = 0;
	for (i=0; i<pf->size; ++i) 
	{
		if (pf->offset[i] != l) 
		{
			encodetable = 1;
			break;
		}
		l += MWIMAGE_WORDS(pf->width[i]) * pf->height;
	}
	if (!encodetable) {
		free(pf->offset);
		pf->offset = NULL;
	}
#endif
	
	/* determine whether font is fixed-width*/
	for (i=0; i<pf->size; ++i) {
		if (pf->width[i] != maxwidth) {
			proportional = 1;
			break;
		}
	}
	if (!proportional) {
		free(pf->width);
		pf->width = NULL;
	}

	/* reallocate bits array to actual bits used*/
	if (ofs < pf->bits_size) {
		pf->bits = realloc(pf->bits, ofs * sizeof(MWIMAGEBITS));
		pf->bits_size = ofs;
	} else if (ofs > pf->bits_size) {
		fprintf(stderr, "Warning: DWIDTH spec > max FONTBOUNDINGBOX\n");
		if (ofs > pf->bits_size+EXTRA) {
			fprintf(stderr, "Error: Not enough bits initially allocated\n");
			return 0;
		}
		pf->bits_size = ofs;
	}

	/* Update stats for ranges */
	ranges_updateStats(pf);
	
	return 1;
}

/* read the next non-comment line, returns buf or NULL if EOF*/
char *
bdf_getline(FILE *fp, char *buf, int len)
{
	int c;
	char *b;

	for (;;) {
		b = buf;
		while ((c = getc(fp)) != EOF) {
			if (c == '\r')
				continue;
			if (c == '\n')
				break;
			if (b - buf >= (len - 1))
				break;
			*b++ = c;
		}
		*b = '\0';
		if (c == EOF && b == buf)
			return NULL;
		if (b != buf && !isprefix(buf, "COMMENT"))
			break;
	}
	return buf;
}

/* return hex value of portion of buffer*/
MWIMAGEBITS
bdf_hexval(unsigned char *buf, int ndx1, int ndx2)
{
	MWIMAGEBITS val = 0;
	int i, c;

	for (i=ndx1; i<=ndx2; ++i) {
		c = buf[i];
		if (c >= '0' && c <= '9')
			c -= '0';
		else if (c >= 'A' && c <= 'F')
			c = c - 'A' + 10;
		else if (c >= 'a' && c <= 'f')
			c = c - 'a' + 10;
		else c = 0;
		val = (val << 4) | c;
	}
	return val;
}

/* generate C source from in-core font*/
int gen_c_source(PMWCFONT pf, char *path)
{
	FILE *ofp;
	int i;
	time_t t = time(0);
	char buf[256];
	char hdr1[] = 
	{
		"/* Generated by convbdf-slib on %s. */\n"
		"#include \"device.h\"\n"
		"#include \"nano-vfi.h\"\n"
		"#include <stdlib.h>\n"
		"\n"
		"/* Font information:\n"
		"   name: %s\n"
		"   facename: %s\n"
		"   w x h: %dx%d\n"
		"   size: %d\n"
		"   ascent: %d\n"
		"   descent: %d\n"
		"   first char: %d (0x%02x)\n"
		"   last char: %d (0x%02x)\n"
		"   default char: %d (0x%02x)\n"
		"   proportional: %s\n"
		"   %s\n"
		"*/\n"
		"\n"
	};
	unsigned long * fileOffsetTable = NULL;
	unsigned long fileOffset = 0;
	
	
	ofp = fopen(path, "w");
	if (!ofp) {
		fprintf(stderr, "Can't create %s\n", path);
		return 1;
	}
	fprintf(stderr, "Generating %s\n", path);

	strcpy(buf, ctime(&t));
	buf[strlen(buf)-1] = 0;

	fprintf(ofp, hdr1, buf, 
		pf->name,
		pf->facename? pf->facename: "",
		pf->maxwidth, pf->height,
		pf->size,
		pf->ascent, pf->descent,
		pf->firstchar, pf->firstchar,
		pf->firstchar+pf->size-1, pf->firstchar+pf->size-1,
		pf->defaultchar, pf->defaultchar,
		pf->width? "yes": "no",
		pf->copyright? pf->copyright: ""
	);

	/* generate bitmaps*/
	fprintf(ofp, 
		"/* Font character bitmap data. */\n"
		"static const unsigned char compressed_bits[] = {\n"
	);

	
	fileOffsetTable = calloc(pf->realGlyphCount, sizeof(fileOffsetTable[0]));
	
	{
		struct RANGE_ENTRY *worker = 0;
		int charIndex;
		
		for (worker = pf->ranges; worker != NULL; worker = worker->next)
		{
			/* Check all chars in range */
			for (charIndex = worker->startChar; charIndex <= worker->endChar; ++charIndex)
			{
				int i = charIndex - pf->firstchar;
				int width = pf->width ? pf->width[i] : pf->maxwidth;
				int height = pf->height;
				MWIMAGEBITS *bits = pf->bits + (pf->offset? pf->offset[i]: (height * i));
				MWIMAGEBITS bitvalue;
				
				fprintf(ofp, "\n/* Character %d (0x%02x):\n   width %d",
					i+pf->firstchar, i+pf->firstchar, width);
				
				
				/* Generate character map in comment */
				if (gen_map) 
				{
					int x;
					int bitcount = 0;
					
					fprintf(ofp, "\n   +");
					for (x=0; x<width; ++x) fprintf(ofp, "-");
					fprintf(ofp, "+\n");

					x = 0;
					while (height > 0) {
						if (x == 0) fprintf(ofp, "   |");

						if (bitcount <= 0) {
							bitcount = MWIMAGE_BITSPERIMAGE;
							bitvalue = *bits++;
						}

						fprintf(ofp, MWIMAGE_TESTBIT(bitvalue)? "*": " ");

						bitvalue = MWIMAGE_SHIFTBIT(bitvalue);
						--bitcount;
						if (++x == width) {
							fprintf(ofp, "|\n");
							--height;
							x = 0;
							bitcount = 0;
						}
					}
					fprintf(ofp, "   +");
					for (x=0; x<width; ++x) fprintf(ofp, "-");
					fprintf(ofp, "+ */\n");
					
					/* restore pointer */
					bits = pf->bits + (pf->offset? pf->offset[i]: (height * i));
				} 
				else
					fprintf(ofp, " */\n");

				{
					int glyphIndex = charIndex - worker->startChar + worker->startIndex;
					size_t byteOffset = pf->zero_fill[glyphIndex].prefix;
					
					size_t wordsPerLine = MWIMAGE_WORDS(width);
					/* size_t outBytesPerLine = (width + 7) / 8; */
					
					size_t byteCount = wordsPerLine * sizeof(MWIMAGEBITS) * pf->height;
					
					size_t outputByteCount = 0;
					unsigned char byteVal;
					
					/* skip the zeroes */
					byteCount -= byteOffset;
					byteCount -= pf->zero_fill[glyphIndex].postfix;

					
					if (byteCount == 0)
					{
						fprintf(ofp, "/* Zero only image */\n");
					}
					else
					{
						while (byteCount > 0)
						{
							int lineBytes = (byteCount < 16) ? byteCount : 16;
							
							for (; lineBytes > 0; --lineBytes)
							{
								bitvalue = bits[byteOffset >> 1];
								if (byteOffset & 1)
									byteVal = bitvalue & 0xff;
								else
									byteVal = (bitvalue >> 8) & 0xff;
									
								fprintf(ofp, "0x%02x, ", byteVal);
								++outputByteCount;
								++byteOffset;
								--byteCount;
							}
							fprintf(ofp, "\n");
						}
					}
					
					fileOffsetTable[glyphIndex] = fileOffset;
					fileOffset += outputByteCount;
				}
			}
		}
	}
	fprintf(ofp, "};\n\n");

	/* Generate offset table. */
	/* NOTE: It has NOTHING to do with the offset table read from file! */
	{
		struct RANGE_ENTRY *worker = 0;
		int charIndex;
		
		fprintf(ofp, 
			"/* Character->glyph mapping. */\n"
			"static const unsigned long offset[] = {\n"
		);
		
		for (worker = pf->ranges; worker != NULL; worker = worker->next)
		{
			/* Check all chars in range */
			for (charIndex = worker->startChar; charIndex <= worker->endChar; ++charIndex)
			{
				int glyphIndex = charIndex - worker->startChar + worker->startIndex;
				
				fprintf(ofp, "  %lu,\t/* (0x%02x) */\n", fileOffsetTable[glyphIndex], charIndex);
			}
		}

		fprintf(ofp, "};\n\n");
	}
	

	/* output width table for proportional fonts*/
	if (pf->width) 
	{
		fprintf(ofp,
			"/* Character width data. */\n"
			"static const unsigned char width[] = {\n"
		);

		for (i=0; i<pf->size; ++i)
		{
			if (pf->width[i])
			{
				fprintf(ofp, "  %d,\t/* (0x%02x) */\n", pf->width[i], i+pf->firstchar);
			}
		}
		fprintf(ofp, "};\n\n");
	}


		
	if (pf->ranges) 
	{
		struct RANGE_ENTRY *worker = 0;
		fprintf(ofp, "/* Ranges table */\n");
		fprintf(ofp, 
			"/*\nstruct CharRangeEntry\n"
			"{\n"
			"\tunsigned short startChar; // first character in range\n"
			"\tunsigned short endChar; // last character in range\n"
			"\tunsigned short startIndex; // resulting index corresponding to the startChar\n"
			"};\n"
			"*/\n\n"
		);
		fprintf(ofp, "static const CharRangeEntry ranges[] = \n{\n");
		worker = pf->ranges;
		while (worker) 
		{
			fprintf(ofp, "\t{ %i, %i, %i },\n", worker->startChar, worker->endChar, worker->startIndex);
			worker = worker->next;
		}
		fprintf(ofp, "};\n\n");
	}

	if (pf->zero_fill) 
	{
		size_t idx;
		
		fprintf(ofp, "/* Zero-bytes prefix/postfix count table (for big-endian glyph images!) */\n");

		fprintf(ofp, "static const GlyphZeroFill zero_fill[] = \n{\n");
		for (idx=0; idx < pf->realGlyphCount; ++idx)
		{
			fprintf(ofp, "\t{%u, %u},\n", pf->zero_fill[idx].prefix, pf->zero_fill[idx].postfix);
		}
		fprintf(ofp, "};\n\n");
	}
	
	
	/* output MWCFONT_DLL struct*/

	/* NOTE: Can't use fixed structs initialized with pointers in shared-libs (position independent)  - all pointers are filled in as diffs */
	fprintf(ofp, 	"/* Exported structure definition. */\n"
		"static const MWCFONT_DLL font_struct = {\n"
		"  /* Embedded MWCFONT struct */\n"
		"  {\n"
		"    NULL, /* name - filled by Copy_MWCFONT_DLL() */\n"
		"    %d, /* maxwidth */\n"
		"    %d, /* height */\n"
		"    %d, /* ascent */\n"
		"    %d, /* firstchar */\n"
		"    %d, /* size */\n"
		"    NULL, /* bits - see compressed_bits intead */\n"
		"    NULL, /* offset - filled by Copy_MWCFONT_DLL()*/\n"
		"    NULL, /* width - filled by Copy_MWCFONT_DLL()*/\n"
		"    %d, /* defaultchar */\n"
		"    sizeof(compressed_bits), /* size */\n"
		"  },\n\n",
		pf->maxwidth, pf->height,
		pf->ascent,
		pf->firstchar,
		pf->realGlyphCount,
		pf->defaultchar
	);

	fprintf(ofp,
		"  NULL, /* char_ranges */\n"
		"  sizeof(ranges)/sizeof(ranges[0]), /* char_ranges_cnt */\n"
		"  NULL, /* zero_fill */\n"
		"  NULL, /* compressed_bits */\n"
		"  1, /* offset_step */\n"
		"  0, /* needs_padding */\n"
		"};\n\n"
	);

		
		
	/* DLL (shared-lib) version with function */
	fprintf(ofp, 	"/* Function to insert correct values to structure. */\n"
		"int Copy_MWCFONT_DLL(MWCFONT_DLL * pf)\n"
		"{\n"
		"  *pf = font_struct;\n"
		"  pf->font.name = \"%s\";\n"
		"  pf->font.offset = %s;\n"
		"  pf->font.width = %s;\n"
		"\n"
		"  pf->char_ranges = ranges;\n"
		"  pf->zero_fill = zero_fill;\n"
		"  pf->compressed_bits = compressed_bits;\n"
		"\n"
		"  return 0;\n"
		"}\n",
		pf->name,
		(pf->offset && !pf->rangeKillsOffsets) ? "offset" : "NULL",
		(pf->width) ? "width" : "NULL"
	);
	
	/* [OPTIMIZATION] Some structures and buffers to fill the unavoidable 1020 bytes (1k - sizeof(void *))*/
	fprintf(ofp,
		"\n\n"
		"/* MWCFONT_DLL structure */\n"
		"static MWCFONT_DLL cfontDLL;\n\n"

		"/* Extended MWCOREFONT structure */\n"
		"static MWCOREFONT_DLL corefontDLL;\n\n"

		"/* Buffer to recreate glyph data */\n"
		"static MWIMAGEBITS glyphBuffer[MWIMAGE_WORDS(%i /* maxwidth */) * %i /* height */];\n\n"

		"/* fillerBuf - rest of the 1k - reserved for the future */\n"
		"static unsigned char fillerBuf[1024 - sizeof(void *) - sizeof(cfontDLL) - sizeof(corefontDLL) - sizeof(glyphBuffer)];\n\n"
		,
		pf->maxwidth, pf->height
	);

	fputs(
		"void * GetBuffer(int bufferIdx, size_t * pSize)\n"
		"{\n"
		"	void * buffer = NULL;\n"
		"	size_t size = 0;\n"
		"\n"
		"	switch (bufferIdx)\n"
		"	{\n"
		"		case MW_FONT_DLL_CFONT_DLL:\n"
		"			buffer = &cfontDLL;\n"
		"			size = sizeof(cfontDLL);\n"
		"			break;\n"
		"			\n"
		"		case MW_FONT_DLL_COREFONT_DLL:\n"
		"			buffer = &corefontDLL;\n"
		"			size = sizeof(corefontDLL);\n"
		"			break;\n"
		"			\n"
		,
		ofp
	);

	fputs(
		"		case MW_FONT_DLL_BUF_GLYPH:\n"
		"			buffer = glyphBuffer;\n"
		"			size = sizeof(glyphBuffer);\n"
		"			break;\n"
		"			\n"
		"		case MW_FONT_DLL_BUF_REST:\n"
		"			buffer = fillerBuf;\n"
		"			size = sizeof(fillerBuf);\n"
		"			break;\n"
		"	};\n"
		"\n"
		"	if (pSize)\n"
		"		*pSize = size;\n"
		"\n"
		"	return buffer;\n"
		"}\n"
		, 
		ofp
	);
		
	fclose(ofp);
	ofp = NULL;
	
	if (fileOffsetTable)
		free(fileOffsetTable);

	return 0;
}

int gen_stats_file(PMWCFONT pf, char const * path)
{
	FILE *ofp;
	

	ofp = fopen(path, "w");
	if (!ofp) 
	{
		fprintf(stderr, "Can't create %s\n", path);
		return 1;
	}
	fprintf(stderr, "Generating %s\n", path);

	
	
	
	fprintf(ofp, 
		"Original offsets count = %i\n"
		"Optimized offsets/widths count = %i (diff=%i bytes)\n"
		"Ranges killed offsets = %i (diff=%i bytes)\n"
		"Ranges count = %u (diff=%i bytes)\n"
		"\n",

		pf->size, /* Original offsets count */
		pf->realGlyphCount /* Optimized offsets count */, (pf->realGlyphCount - pf->size) * (sizeof(pf->offset[0]) + sizeof(pf->width[0])), 
		pf->rangeKillsOffsets, pf->realGlyphCount * sizeof(pf->offset[0]),
		pf->range_cnt, pf->range_cnt * 6
	);

	
	
	/************************/
	/* Count prefix and postfix bytes */
#if 1
	if (pf->zero_fill)
	{
		int i;
		size_t maxPrefix = 0, maxPostfix = 0;
		size_t totalPrefix = 0, totalPostfix = 0;
		int maxPrefixChar = 0, maxPostfixChar=0;
		size_t maxBytesPrefix=0, maxBytesPostfix=0;
		size_t totalBytesPrefix=0, totalBytesPostfix=0;

		size_t totalPaddingCount = 0;
		
		
		for (i=0; i<pf->size; ++i) 
		{
			unsigned long ofs = pf->offset[i];

			if (ofs == (unsigned long) -1)
				continue;
			else
			{
				MWIMAGEBITS * bits = pf->bits + ofs;
				size_t wordCount = MWIMAGE_WORDS(pf->width[i]) * pf->height;
				size_t prefixZeroCnt = 0, postfixZeroCnt = 0;
				size_t bytesPrefixCnt, bytesPostfixCnt;

				/* Count prefix 0 words */
				for (; wordCount > 0; --wordCount, ++bits)
				{
					if (*bits == 0)
					{
						++prefixZeroCnt;
					}
					else
						break;
				}
				
				bytesPrefixCnt = sizeof(*bits) * prefixZeroCnt;
				if ((wordCount > 0 && bits[0] & 0xff00) == 0)
					++bytesPrefixCnt;
				
				/* count postfix 0 words */
				for (; wordCount > 0; --wordCount)
				{
					if (bits[wordCount - 1] == 0)
					{
						++postfixZeroCnt;
					}
					else
						break;
				}
				
				bytesPostfixCnt = sizeof(*bits) * postfixZeroCnt;
				if (wordCount > 0 && (bits[wordCount - 1] & 0xff) == 0)
					++bytesPostfixCnt;
				
				
				if (prefixZeroCnt > maxPrefix)
				{
					maxPrefix = prefixZeroCnt;
					maxPrefixChar = i + pf->firstchar;
				}
				
				if (postfixZeroCnt > maxPostfix)
				{
					maxPostfix = postfixZeroCnt;
					maxPostfixChar = i + pf->firstchar;
				}
				
				if (bytesPrefixCnt > maxBytesPrefix)
					maxBytesPrefix = bytesPrefixCnt;
					
				if (bytesPostfixCnt > maxBytesPostfix)
					maxBytesPostfix = bytesPostfixCnt;
				
				
				totalPrefix += prefixZeroCnt;
				totalPostfix += postfixZeroCnt;
				
				totalBytesPrefix += bytesPrefixCnt;
				totalBytesPostfix += bytesPostfixCnt;
				
				
				/* count padding 0's */
				{
					size_t wordsPerLine = MWIMAGE_WORDS(pf->width[i]);
					size_t inBytesPerLine = wordsPerLine * sizeof(bits[0]);
					size_t outBytesPerLine = (pf->width[i] + 7) / 8;
					size_t startByteOffset = bytesPrefixCnt;
					size_t byteCount = (sizeof(bits[0]) * MWIMAGE_WORDS(pf->width[i]) * pf->height) - bytesPrefixCnt - bytesPostfixCnt;

					size_t byteOffset;
					size_t paddingCount = 0;
					/*MWIMAGEBITS wordValue;
					unsigned char byteVal;*/
					
					bits = pf->bits + ofs;

					for (byteOffset = startByteOffset; byteOffset < (startByteOffset + byteCount); ++byteOffset)
					{
						size_t byteOfsInLine = byteOffset % inBytesPerLine;
						if (byteOfsInLine >= outBytesPerLine)
							++ paddingCount;
						/*
						wordvalue = bits[byteOffset >> 1];
						if (byteOffset & 1)
							byteVal = bitvalue & 0xff;
						else
							byteVal = (bitvalue >> 8) & 0xff;
						*/
					}
					fprintf(ofp, "PADDING for %i = %lu\n", i+pf->firstchar, SIZE_T_UL(paddingCount));
					totalPaddingCount += paddingCount;
				}
				
				/* fprintf(ofp, "ZERO %i, pre=%i, post=%i\n", i + pf->firstchar, prefixZeroCnt, postfixZeroCnt); */
			}
		}

		fprintf(ofp, "TOTAL PADDING %lu\n\n", SIZE_T_UL(totalPaddingCount));
		
		fprintf(ofp, "MAX prefixZERO=%lu for %i\n", SIZE_T_UL(maxPrefix), maxPrefixChar);
		fprintf(ofp, "MAX postfixZERO=%lu for %i\n", SIZE_T_UL(maxPostfix), maxPostfixChar);
		fprintf(ofp, "TOTAL prefix=%lu, postfix=%lu\n\n", SIZE_T_UL(totalPrefix), SIZE_T_UL(totalPostfix));
		
		fprintf(ofp, "MAX big-endian zero bytes prefix=%lu, postfix=%lu\n", SIZE_T_UL(maxBytesPrefix), SIZE_T_UL(maxBytesPostfix));
		fprintf(ofp, "TOTAL big-endian zero bytes prefix=%lu, postfix=%lu\n\n", SIZE_T_UL(totalBytesPrefix), SIZE_T_UL(totalBytesPostfix));
	}

	/************************/
	/* Test prefix and postfix BYTES */
	if (pf->offset)
	{
		int i;
		size_t maxPrefix = 0, maxPostfix = 0;
		size_t totalPrefix = 0, totalPostfix = 0;
		int maxPrefixChar = 0, maxPostfixChar=0;
		
		for (i=0; i<pf->size; ++i) 
		{
			unsigned long ofs = pf->offset[i];

			if (ofs == (unsigned long) -1)
				continue;
			else
			{
				const MWIMAGEBITS *bits = pf->bits + ofs;
				const size_t wordCount = MWIMAGE_WORDS(pf->width[i]) * pf->height;
				unsigned char * imageBytes = (unsigned char *) bits;
				size_t byteCount = sizeof(*bits) * wordCount;
				size_t prefixZeroCnt = 0, postfixZeroCnt = 0;

				/* Count prefix 0 words */
				for (; byteCount > 0; --byteCount, ++imageBytes)
				{
					if (*imageBytes == 0)
					{
						++prefixZeroCnt;
					}
					else
						break;
				}
				
				/* count postfix 0 words */
				for (; byteCount > 0; --byteCount)
				{
					if (imageBytes[byteCount - 1] == 0)
					{
						++postfixZeroCnt;
					}
					else
						break;
				}
				
				if (prefixZeroCnt > maxPrefix)
				{
					maxPrefix = prefixZeroCnt;
					maxPrefixChar = i + pf->firstchar;
				}
				
				if (postfixZeroCnt > maxPostfix)
				{
					maxPostfix = postfixZeroCnt;
					maxPostfixChar = i + pf->firstchar;
				}	
				
				totalPrefix += prefixZeroCnt;
				totalPostfix += postfixZeroCnt;
				
				/* fprintf(ofp, "%s"
					"ZERO bytes %i, pre=%i, post=%i"
					" (total=%i, left=%i)\n", 
					(prefixZeroCnt + postfixZeroCnt + byteCount == sizeof(*bits) * wordCount) ? "" : "[ERROR]",
					i + pf->firstchar, prefixZeroCnt, postfixZeroCnt,
					sizeof(*bits) * wordCount, byteCount
				);*/
			}
		}

		fprintf(ofp, "MAX bytes prefixZERO=%lu for %i\n", SIZE_T_UL(maxPrefix), maxPrefixChar);
		fprintf(ofp, "MAX bytes postfixZERO=%lu for %i\n", SIZE_T_UL(maxPostfix), maxPostfixChar);
		fprintf(ofp, "TOTAL bytes prefix=%lu, postfix=%lu\n\n", SIZE_T_UL(totalPrefix), SIZE_T_UL(totalPostfix));
	}
#endif

	fclose(ofp);
	ofp = NULL;
	
	return 0;
}



/* karp */
void ranges_init(PMWCFONT pf)
{
	if(!pf || pf->ranges) return;
	pf->ranges = NULL;
	pf->range_cnt = 0;

	pf->zero_fill = 0;
	
	pf->offset_step = 1;		/* step between each offset entry index - originally it is 1 - every glyph has its entry in the offset table */


	pf->realGlyphCount = 0;
	pf->rangeKillsOffsets = 0;

	
}

/* add new index to the table and report range index it was stored into */
void ranges_add_index(PMWCFONT pf, unsigned int new_index)
{
	struct RANGE_ENTRY *worker = 0, *previous = 0;

	if (!pf) 
		return;
		
	/* go through ranges and try to place it to correct location */
	worker = pf->ranges;
	while (pf->range_cnt && worker) 
	{
		/* if the upper bound of range is below the index and if so, just continue */
		if (worker->endChar >= new_index) 
		{
			/* check if we need to insert new range into middle of existing ranges */
			if (worker->startChar <= new_index) 
				return;
		}
 		/* try to glue new index to existing range */
		else if ((worker->endChar+1) == new_index) 
		{
			worker->endChar = new_index;
			return;
		}
		/* check if index already exists in the range */
		previous = worker;
		worker = worker->next;
	}
	/* if we are here, there's need to add a new range on the end */
	worker = calloc(1, sizeof(struct RANGE_ENTRY));
	worker->startChar = worker->endChar = new_index;
	if (previous)
		previous->next	= worker;
	else
		pf->ranges		= worker;
	pf->range_cnt++;
	
	/* fprintf(stderr, "ranges_add_index=%i\n", new_index); */
}

void ranges_dump(PMWCFONT pf)
{
	struct RANGE_ENTRY *worker = 0;
	if(!pf || !pf->ranges) return;
	worker = pf->ranges;
	fprintf(stderr, "dump> dump start, range count: %d\n", pf->range_cnt);
	while (worker) 
	{
		fprintf(stderr, "dump> range %02d: %d->%d\n", worker->startIndex, worker->startChar, worker->endChar);
		worker = worker->next;
	}
}


/* glue ranges if they touch each other */
void ranges_glue(PMWCFONT pf)
{
	struct RANGE_ENTRY *worker = 0, *previous = 0;
	
	if (!pf || !pf->ranges) 
		return;

	worker = pf->ranges;
	while (worker) 
	{
		/* check if touch */
		if (previous && (previous->endChar+1) == worker->startChar) 
		{
			/* glue */
			previous->endChar = worker->endChar;
			previous->next = worker->next;
			--pf->range_cnt;
			free(worker);
			worker = previous->next;
			if (!worker) 
				break;
		}
		if (previous)
			worker->startIndex = previous->startIndex + (previous->endChar-previous->startChar) + 1;
		/* step */
		previous = worker;
		worker = worker->next;
	}
}

void ranges_updateStats(PMWCFONT pf)
{
	struct RANGE_ENTRY *worker = 0;

	if (!pf || !pf->ranges) 
		return;

	/* Loop through all the ranges */
	{
		int totalChars = 0;

		worker = pf->ranges;
		while (worker) 
		{
			/* Update  */
			totalChars += (worker->endChar + 1 - worker->startChar);

			worker = worker->next;
		}
		
		pf->realGlyphCount = totalChars;
	}

	/*************************************************************/
	/* Check if the offset table after the ranges is really needed */
	/* NOTE: This code is fixed to check in fact whether (after applying ranges) the following is always true: pf->offset[i] == (pf->height * i)*/
	/* TODO: There's something wrong with this table */
#if 0
	if (pf->offset)
	{
		unsigned long l = 0;
		int encodetable = 0;
		int i;
		
		for (i=0; i<pf->size; ++i) 
		{
			if (pf->offset[i] == (unsigned long) -1)
				continue;
				
			if (pf->offset[i] != l) 
			{
				encodetable = 1;
				break;
			}
			l += pf->height;
		}

		if (!encodetable) 
		{
			pf->rangeKillsOffsets = 1;
		}
	}
#endif
	
	/*************************************************************/
	/* calculate zero_prefix_bytes and zero_postfix_bytes tables */
	if (pf->offset)
	{
		int i, idx;
		GlyphZeroFill * zero_fill;
		
		/* allocate mem for those tables */
		pf->zero_fill = zero_fill = calloc(pf->realGlyphCount, sizeof(GlyphZeroFill));

		
		for (i=0, idx=0; i<pf->size; ++i) 
		{
			unsigned long ofs = pf->offset[i];

			if (ofs == (unsigned long) -1)
				continue;
			else
			{
				MWIMAGEBITS *bits = pf->bits + ofs;
				size_t wordCount = MWIMAGE_WORDS(pf->width[i]) * pf->height;
				size_t prefixZeroCnt = 0, postfixZeroCnt = 0;
				size_t bytesPrefixCnt, bytesPostfixCnt;
				

				/* Count prefix 0 words */
				for (; wordCount > 0; --wordCount, ++bits)
				{
					if (*bits == 0)
					{
						++prefixZeroCnt;
					}
					else
						break;
				}

				
				bytesPrefixCnt = sizeof(*bits) * prefixZeroCnt;
				if (wordCount > 0 && (bits[0] & 0xff00) == 0)
				{
					++bytesPrefixCnt;
				}

				
				/* count postfix 0 words */
				for (; wordCount > 0; --wordCount)
				{
					if (bits[wordCount - 1] == 0)
					{
						++postfixZeroCnt;
					}
					else
						break;
				}
				
								
				
				bytesPostfixCnt = sizeof(*bits) * postfixZeroCnt;
				if (wordCount > 0 && (bits[wordCount - 1] & 0xff) == 0)
				{
					++bytesPostfixCnt;
				}
				
				
				zero_fill[idx].prefix = bytesPrefixCnt;
				zero_fill[idx].postfix = bytesPostfixCnt;
				
				idx++;
			}
		}
	}	

	
}

#if 0
void ranges_apply2font(PMWCFONT pf)
{
	/* Kill all the ranges that are unused */
	struct RANGE_ENTRY *worker, * next;
	int unusedIdxStart, unusedIdxEnd, idx;
	int maxSize;

	if (!pf)
		return;

	assert(pf->ranges != NULL);
	assert(pf->ranges->startChar == pf->firstchar);
	
		
	worker = pf->ranges;
	while (worker) 
	{
		/* Get range */
		unusedIdxStart = worker->endChar + 1 - pf->firstchar;
		maxSize = worker->endChar + 1 - worker->startChar + worker->startIndex;

		next = worker->next;
		if (next)
		{
			unusedIdxEnd = next->startChar - 1 - pf->firstchar;
			
			for (idx = unusedIdxStart; idx <= unusedIdxEnd; ++idx)
			{
				/* Make sure those are is unused entries */
				assert(pf->offset[idx] == (unsigned long) -1);
				assert(pf->width[idx] == 0);
			}
			
			memmove(&pf->offset[next->startIndex], &pf->offset[unusedIdxStart], (unusedIdxEnd + 1 - unusedIdxStart) * sizeof(pf->offset[0]));
			memmove(&pf->width[next->startIndex], &pf->width[unusedIdxStart], (unusedIdxEnd + 1 - unusedIdxStart) * sizeof(pf->width[0]));
		}
	
		/* Next item on list */
		worker = next;
	}
	
	pf->size = maxSize;
}
#endif

void ranges_release(PMWCFONT pf)
{
	struct RANGE_ENTRY *worker, *next = 0;
	if (!pf)
		return;

	worker = pf->ranges;
	while (worker) 
	{
		next = worker->next;
		free(worker);
		worker = next;
	}
	
	if (pf->zero_fill)
		free((void *) pf->zero_fill);
}
