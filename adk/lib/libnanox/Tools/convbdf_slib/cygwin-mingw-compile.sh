#! /bin/bash
gcc -Wall --pedantic -g -mno-cygwin convbdf-slib.c -o convbdf-slib.dbg.exe \
&& cp convbdf-slib.dbg.exe convbdf-slib.exe \
&& strip convbdf-slib.exe
