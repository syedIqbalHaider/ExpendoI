#include <svc.h>
#include <stdio.h>
#include <stdlib.h>
#include <liblog/logsys.h>

#define MWINCLUDECOLORS
#include <nano-X.h>
#include <Nxcolors.h>

#define	MARGIN	50			/* margin around window */
int m_console = -1;

int initConsole( void )
{
	int ret = 0;

    // this app and this app alone will own the console!
    m_console = open( "/dev/console", 0 );
    if( m_console < 0 )
    {
        dlog_error(__FILE__, __LINE__, "error opening console");
        ret = -1;		// error opening console
    }
	else
	{
	    //Open conn to nano-X
	    if (GrOpen () != -1)
	    {
	        dlog_msg(__FILE__,__LINE__,"Nano-X connection is open");			
	    }
		else
		{
	        dlog_error(__FILE__,__LINE__,"Nano-X connection is failed");	
			ret = -2;
		}	
	}
    return ret;
}

main()
{
	GR_WINDOW_ID	wid;		/* window id */
	GR_GC_ID	gc;		/* graphics context id */
	GR_EVENT	event;		/* current event */
	GR_SCREEN_INFO	si;		/* screen information */
	
	initConsole();
	
	GrGetScreenInfo(&si);
	wid = GrNewWindow(GR_ROOT_WINDOW_ID, MARGIN, MARGIN, si.cols - MARGIN * 2, si.rows - MARGIN * 2, 1, GR_COLOR_WHITE, GR_COLOR_BLACK);
	GrSelectEvents(wid, GR_EVENT_MASK_BUTTON_DOWN | GR_EVENT_MASK_EXPOSURE);
	GrMapWindow(wid);
	gc = GrNewGC();
	while (1) {
		GrGetNextEvent(&event);
		switch (event.type) {
			case GR_EVENT_TYPE_BUTTON_DOWN:
				if (event.button.wid != wid)
					break;
				GrClose();
				exit(0);
			case GR_EVENT_TYPE_EXPOSURE:
				if (event.exposure.wid == wid)
					GrText(wid, gc, 1, 0, "EXIT", 4, GR_TFTOP | GR_TFASCII);
				break;
		}
	}
}



#ifdef NEVER

#endif
