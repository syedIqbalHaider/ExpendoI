libfbdisplat allows drawing texts directly on framebuffer device. 
Lib uses hardcoded font set - no additional dependencies. 

Use this lib for simple debugging purposes on devices with console in graphics mode.

Usage:

		FBDisplay fb("/dev/fb0");

 		// this opens fb device and performs some system calls,
		// so it's better to do it once
		if (!fb.init()) return -1;

		...
		
		fb.clearScreen();
		fb.draw("Some text\n");
		fb.setColor(Color::C_RED);
		fb.draw("More text will be added in red,\n and in the next line...");

That's all ;)