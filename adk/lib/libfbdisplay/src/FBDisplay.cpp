#include "FBDisplay.h"

#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>
#include <liblog/logsys.h>
#include <assert.h>
#include "FBWriter.h"

namespace com_verifone_fbdisplay{

FBDisplay::FBDisplay(std::string fbDeviceName, FontType fontType) :
		initialized(false),
		fbDevice(fbDeviceName),
		fbfd(-1),
		cursorPos(0,0),
		textColor(Color::C_BLACK),
		fbWriter(NULL),
		font(Courier, Courier_w, Courier_h)
{

}

bool FBDisplay::init(){
	fbfd = open(fbDevice.c_str(), O_RDWR);

	if (fbfd < 0){
		dlog_error("Framebuffer device open error, %d", fbfd);
		return false;
	}

	if (ioctl(fbfd, FBIOGET_VSCREENINFO, &varInfo) != 0){
		dlog_error("Getting FBIOGET_VSCREENINFO failed");
		close(fbfd);
		return false;
	}

	dlog_msg("Got framebuffer params: %dx%d, %d bits per pixel", varInfo.xres, varInfo.yres, varInfo.bits_per_pixel);

	if (ioctl(fbfd, FBIOGET_FSCREENINFO, &fixedInfo) != 0){
		dlog_error("Getting FBIOGET_FSCREENINFO failed");
		close(fbfd);
		return false;
	}

	if (varInfo.bits_per_pixel == 32){
		fbWriter = new FBWriter<uint32_t>(varInfo.xres, varInfo.yres, varInfo.bits_per_pixel, fbfd);
	}
	else if (varInfo.bits_per_pixel == 1 && fixedInfo.visual == FB_VISUAL_MONO01){
		fbWriter = new FBWriter<uint8_t>(varInfo.xres, varInfo.yres, varInfo.bits_per_pixel, fbfd);
	}
	else{
		dlog_error("Unsupported bits per pixel value [%d] - new FBWriter needed... ", varInfo.bits_per_pixel);
		assert(false);
		return false;
	}

	if (!fbWriter->init()){
		return false;
	}

	fbWriter->setFont(&font);

	initialized = true;
	return true;
}


FBDisplay::~FBDisplay() {

	delete fbWriter;

	if (fbfd > 0){
		close(fbfd);
	}
}


void FBDisplay::clearScreen(const Color & c){
	assert(initialized);
	fbWriter->clearScreen(c);
	fbWriter->forceScreenUpdate();

	// resetting cursor position
	cursorPos.x = 0;
	cursorPos.y = 0;
}

void FBDisplay::draw(const char * const text){
	assert(initialized);
	for (size_t i = 0; i < strlen(text); ++i) {

		// go to next line
		if (text[i] == '\n'){
			cursorPos.y += font.h;
			cursorPos.x = 0;
			if (cursorPos.y > varInfo.yres){
				clearScreen();
			}
			continue;
		}

		// text wrapping
		if (cursorPos.x + font.w > varInfo.xres){
			cursorPos.y += font.h;
			cursorPos.x = 0;

			if (cursorPos.y > varInfo.yres){
				clearScreen();
			}
		}

		fbWriter->drawLetter(cursorPos.x, cursorPos.y, text[i], textColor);
		fbWriter->forceScreenUpdate();

		cursorPos.x += font.w;
	}
}

void FBDisplay::setTextColor(const Color & c){
	textColor = c;
}

}	//namespace end
