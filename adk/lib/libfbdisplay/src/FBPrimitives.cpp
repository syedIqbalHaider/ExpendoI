#include "FBPrimitives.h"
#include "fonts/Courier_13.h"
#include <liblog/logsys.h>

namespace com_verifone_fbdisplay {

const Color Color::C_WHITE	(0xff,	0xff,	0xff);
const Color Color::C_BLACK	(0,		0,		0);
const Color Color::C_RED 	(0xff,	0,		0);
const Color Color::C_GREEN	(0,		0xff,	0);
const Color Color::C_BLUE 	(0,		0,		0xff);

Color::Color(char r_, char g_, char b_, char a_) :
		r(r_), g(g_), b(b_), a(a_) {
}

uint32_t Color::toARGB() const{
	uint32_t result = (a << 24) | (r << 16) | (g << 8) | b;
	return result;
}

uint8_t Color::toMonochrome01() const {
	if (r == 0xFF && g == 0xFF && b == 0xFF){
		return 0x00;
	}

	return 0xFF;
}


Font::Font(const char * const fontDesc, unsigned glyphW, unsigned glyphH) :
	w(glyphW), h(glyphH), points(fontDesc)
{

}

char Font::getLetterRow(const char c, const unsigned row){
	int offset = ((c - firstChar) * h) + row;
	return points[offset];
}



} // com_verifone_fbdisplay::
