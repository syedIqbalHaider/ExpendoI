# global settings for a font to be generated
w = 8
h = 13
#use 'convert -list font' to check available names
font_name = "Courier"
pointsize = 11

output_font_file = 'Courier_13.h'

##############################################

from subprocess import call

def generate_c_code_line(c):
    ''' Main procedure to change character c into sequence of bytes 
    suitable for direct include in c/cpp file '''
    
    xbm_file = generate_xbm(c)
    print(xbm_file)
        
    code_line = get_character_representation(xbm_file)
    
    # remove the xbm file
    call(["rm", xbm_file])
    
    return code_line
    
def generate_xbm(c):
    ''' Invoke imagemagic conver method to create file with given char c'''
    # Template: 
    # convert -resize 7x13\! -font DroidSansMono -pointsize 10 label:A A.xbm
    
    call(["echo \\" + c + " > tmpfile"], shell=True)
    
    params = "-size %dx%d\! -font %s -pointsize %d label:@tmpfile" % (w,h, font_name, pointsize)
        
    print("*** PARAMS:", params)
    output_file_name = "char_%s.xbm" % (ord(c))    
    print ("Creating file",output_file_name,"...")
    call(["convert " + params + " " + output_file_name], shell=True)
    call(["convert " + params + " " + output_file_name+".png"], shell=True)
    call(["rm","tmpfile"])
    return output_file_name
    
def get_character_representation(xbm_file):
    ''' Parses xbm file and returns {00,00,22,00,...} sequence of bits as string'''
    
    with open(xbm_file,'r') as f:
        content = f.read()
        content = content.replace("\n","")
        trash, definition = content.split("{")
        definition, trash = definition.split("}")
        definition = definition.strip()
        return definition

if __name__ == "__main__" :        
    with open(output_font_file, 'w') as f:
        
        def_guard = "_" + output_font_file.upper().replace(".","_") + "_"
        f.write("/* Autogenerated by " + __file__ + " */\n")
        
        f.write("#ifndef " + def_guard + "\n")
        f.write("#define " + def_guard + "\n\n")  
        
        font_variable_name = font_name.replace(" ","_").replace(".","").replace("-","")
        
        f.write("const unsigned " + font_variable_name + "_w = " + str(w) + ";\n")
        f.write("const unsigned " + font_variable_name + "_h = " + str(h) + ";\n")
              
        f.write( "const char " + font_variable_name + "[] = {\n")
        for c in range(ord(' '), ord('~')+1):
            character = chr(c)
            print("Current character:" + character)
            line = generate_c_code_line(character)
            f.write("\t" + line + " // \'" + character + "\'\n")
            
        f.write("};\n#endif\n")
        
    
