#ifndef _FB_DISPLAY_H
#define _FB_DISPLAY_H

#include <string>
#include <linux/fb.h>

#include "fonts/Courier_13.h"
#include "FBPrimitives.h"

namespace com_verifone_fbdisplay{

class BasicFBWriter;

/////////////////////////////////////////////////////////////////////////////////////

/**
 * Main class responsible for framebuffer handling.
 * Uses hardcoded font representation,
 * so no additional libraries are required.
 *
 * Usage:
 * {
 * 		FBDisplay fb("/dev/fb0");
 *
 *		// this opens fb device and performs some system calls,
 *		// so it's better to do it once
 * 		if (!fb.init()) return -1;
 *
 * 		// fb functions don't check, if it was successfully initialized
 * 		// for efficiency reasons - client has to do this!
 * 		...
 * 		fb.clearScreen();
 * 		fb.draw("Some text\n");
 * 		fb.setColor(Color::C_RED);
 * 		fb.draw("More text will be added in red,\n and in the next line...");
 * }
 */
class FBDisplay{
public:
	enum FontType{
		FONT_COURIER_13
	};

	FBDisplay(std::string fbDeviceName, FontType f = FONT_COURIER_13);

	/**
	 * This must be called after FBDisplay construction. If init() fails,
	 * it is not guaranteed that other FBDisplay methods will work.
	 *
	 * @return true on successful init, false otherwise; check log error output for more info
	 */
	bool init();

	~FBDisplay();

	/**
	 * Draws text at current cursor position. \n is handled
	 * Remember to call init() first.
	 *
	 * @param text text to be written
	 */
	void draw(const char * const text);

	/**
	 * Clears screen with given color, white as default.
	 * For monochrome screen, setting color to something
	 * different than white will set the screen black
	 * Resets cursor position to (0,0).
	 *
	 * @param c color to be used for cleaning
	 *
	 */
	void clearScreen(const Color & c = Color::C_WHITE);

	/**
	 * Sets text color to given param
	 * @param c color to be set
	 */
	void setTextColor(const Color & c);

private:
	bool initialized;
	std::string fbDevice;			///< Device name
	int fbfd;						///< Frame buffer file descriptor
	fb_var_screeninfo varInfo;		///< Screen parameters (variable)
	fb_fix_screeninfo fixedInfo;	///< Screen parameters (fixed)

	Point2d cursorPos;					///< Current cursor position (x,y)

	// todo: improve font representation
	static const char firstChar = ' ';	// those should be moved to font params....
	static const char lastChar = '~';

	Color textColor;

	BasicFBWriter * fbWriter;

	Font font;
};

} // namespace end

#endif
