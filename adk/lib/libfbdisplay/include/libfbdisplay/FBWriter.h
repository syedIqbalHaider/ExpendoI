
#ifndef _FB_WRITER_H_
#define _FB_WRITER_H_

#include <liblog/logsys.h>
#include "FBPrimitives.h"

namespace com_verifone_fbdisplay{

namespace {

// lookup table for bites reversal in byte
uint8_t lookup[16] = {
   0x0, 0x8, 0x4, 0xC,
   0x2, 0xA, 0x6, 0xE,
   0x1, 0x9, 0x5, 0xD,
   0x3, 0xB, 0x7, 0xF };

// reverses bits order in a byte
uint8_t flipBytes( uint8_t n )
{
   return (lookup[n&0x0F] << 4) | lookup[n>>4];
}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Basic fb writer interface.
 */
class BasicFBWriter{
public:
	/**
	 * Init function.
	 * @return true on successful init, false otherwise
	 */
	virtual bool init() = 0;

	/**
	 * Clear framebuffer screen with given color
	 * @param c color to be used for clearing
	 */
	virtual void clearScreen(const Color & c) = 0;

	/**
	 * Draw given character on framebuffer at position (x,y) using color
	 */
	virtual void drawLetter(const unsigned x, const unsigned y, const char character, const Color & color) = 0;

	/**
	 * Force screen update. On UX this must be called, as another write()
	 * operation on framebuffer is needed to actually show anything on the screen
	 */
	virtual void forceScreenUpdate() = 0;

	/**
	 * Set font used for graphic operations. Doesn't affect results of
	 * previous operations (screen isn't rewritten using new font).
	 */
	virtual void setFont(Font * f) = 0;

	/**
	 * Destuctor.
	 */
	virtual ~BasicFBWriter(){}
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////
template<typename pixel_t>
class FBWriter : public BasicFBWriter{
public:
	FBWriter(const unsigned xres, const unsigned yres, const unsigned bpp, const int fbfd);
	virtual bool init();
	virtual void clearScreen(const Color & c);
	virtual void drawLetter(unsigned x, unsigned y, const char character, const Color & color);
	virtual void forceScreenUpdate();
	virtual void setFont(Font * f);
	virtual ~FBWriter();
private:
	const unsigned xres;	///< x screen resolution in pixels
	const unsigned yres;	///< y screen resolution in pixels
	const int fbfd;			///< Framebuffer device file descriptor id

	pixel_t * pixels;		///< Pointer to raw screen pixels
	int fbSize;				///< Size of framebuffer memory mapped to pixels

	Font * font;			///< Current font used for drawing
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////
template<typename pixel_t>
FBWriter<pixel_t>::FBWriter(const unsigned xres_, const unsigned yres_, const unsigned bpp_, const int fbfd_) :
		xres(xres_),
		yres(yres_),
		fbfd(fbfd_),
		pixels(NULL),
		fbSize(0),
		font(NULL)
{
	fbSize = (xres * yres * bpp_) >> 3;	// divided by byte size to get size in bytes
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
template<typename pixel_t>
FBWriter<pixel_t>::~FBWriter(){
	if (pixels){
		munmap(pixels, fbSize);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
template<typename pixel_t>
bool FBWriter<pixel_t>::init() {
	pixels = reinterpret_cast<pixel_t *>(mmap(0, fbSize, PROT_READ | PROT_WRITE, MAP_SHARED, fbfd, 0));
	if (pixels == MAP_FAILED) {
		dlog_error("Failed to map framebuffer memory");
		close(fbfd);
		return false;
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
template<typename pixel_t>
void FBWriter<pixel_t>::clearScreen(const Color & c){}

template<>
void FBWriter<uint32_t>::clearScreen(const Color & c){
	for (unsigned row = 0; row < yres; ++row){
		for(unsigned col = 0; col < xres; ++col){
			*(pixels + col + (row*xres)) = c.toARGB();
		}
	}
}

template<>
void FBWriter<uint8_t>::clearScreen(const Color & c){
	memset(pixels, c.toMonochrome01(), fbSize);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
template<typename pixel_t>
void FBWriter<pixel_t>::drawLetter(unsigned x, unsigned y, char c, const Color & color){}

template<>
void FBWriter<uint32_t>::drawLetter(unsigned x, unsigned y, char c, const Color & color){
	if (c < font->firstChar || c > font->lastChar)
		return;	//unsupported character

	// letter description contains fontParams.h rows
	// each row contains fontParams.w bits, representing each pixel
	// if bit is set to 1, draw that pixel
	//
	// NOTE: this will work only for fonts up to 8 pixels wide,
	// as 8bits fit in one char value
	// this needs to be rewritten for larger fonts support

	for (int row = 0; row < font->h; ++row) {
		for (int col = 0; col < font->w; ++col) {
			if (((font->getLetterRow(c,row) >> col) & 0x01) == true) {
				*(pixels + (x + col) + ((y + row) * xres)) = color.toARGB();
			}
		}
	}
}


template<>
void FBWriter<uint8_t>::drawLetter(unsigned x, unsigned y, const char c, const Color & /*color*/){
	if (c < font->firstChar || c > font->lastChar)
		return;	//unsupported character

	// letter description contains fontParams.h rows
	// each row contains fontParams.w bits, representing each pixel
	// if bit is set to 1, draw that pixel
	//
	// NOTE: this will work only for fonts up to 8 pixels wide,
	// as 8bits fit in one char value
	// this needs to be rewritten for larger fonts support
	int location = 0;

	// x pos is given in pixels, we need bytes
	// assuming x is divisible by 8
	// right now we only have one font with 8px width,
	// but this has to be improved in the future
	assert( (x & 007) == 0);

	// x is given in pixels; we store 1 pixel in 1 bit,
	// so 8 pixels can be stored in one byte ->
	// divide x by 8 -> this means rounding pixel positions...
	// and won't work in case, when x / 8 != 0
	x >>= 3;

	for (int row = 0; row < font->h; ++row) {
		// todo fix that flip!
		// bits in .xbm file that was used to create font header
		// are stored in different order that we need here
		char letter = flipBytes(font->getLetterRow(c, row));
		location = ((y + row) * (xres >> 3 )) + x;
		*(pixels + location) |= letter;
	}

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
template<typename pixel_t>
void FBWriter<pixel_t>::forceScreenUpdate() {}

template<>
void FBWriter<uint8_t>::forceScreenUpdate() {
	// slow disply implies that we must call write manually to do screen update...
	write(fbfd, pixels, fbSize);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
template<typename pixel_t>
void FBWriter<pixel_t>::setFont(Font * f) {
	font = f;
}

}	// com_verifone_fbdisplay::

#endif /* _FB_WRITER_H_ */
