
#ifndef FBPRIMITIVES_H_
#define FBPRIMITIVES_H_

#include <stdint.h>

namespace com_verifone_fbdisplay {

/////////////////////////////////////////////////////////////////////////////////////
/**
 * Point representation (x,y)
 */
class Point2d{
public:
	Point2d(int x_ = 0, int y_ = 0) : x(x_), y(y_) {}
	int x;
	int y;
};

/////////////////////////////////////////////////////////////////////////////////////
/**
 * Class for color representation (RGBA).
 */
class Color{
public:
	/**
	 * Constructor
	 */
	Color(char r_, char g_, char b_, char a_ = 0xff);

	// Color components
	char r;
	char g;
	char b;
	char a;	//alpha

	/**
	 * Converts internal representation to ARGB8888 (32bit)
	 *
	 * @return ARGB8888 value
	 */
	uint32_t toARGB() const;

	uint8_t toMonochrome01() const;

	// Some predefined values to be used
	static const Color C_WHITE;	///C_ prefix because libpml defines MACRO WHITE 0...
	static const Color C_BLACK;
	static const Color C_RED;
	static const Color C_GREEN;
	static const Color C_BLUE;
};


/////////////////////////////////////////////////////////////////////////////////////
/**
 * Class for font representation.
 */
class Font{
public:
	/**
	 * Basic constructor.
	 * @param fontDesc pointer to font table (see fonts/Courier_13.h for example).
	 * 			font desc can be generated using font_generator scripts
	 *
	 * @param glyphW width of single character in pixels
	 * @param glyphH height of single character in pixels
	 */
	Font(const char * const fontDesc, unsigned glyphW, unsigned glyphH);

	/**
	 * Returns description of single pixel row for a given character.
	 */
	char getLetterRow(const char c, const unsigned row);

public:
	const unsigned w;
	const unsigned h;

	// todo: improve font representation
	static const char firstChar = ' ';
	static const char lastChar = '~';

private:
	const char * const points;
};

} // com_verifone_fbdisplay::
#endif /* FBPRIMITIVES_H_ */
