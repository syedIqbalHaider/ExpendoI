#ifndef MAC_H
#define MAC_H

#include <string>
#include "ipc/jsobject.h"

/** \file mac.h
 * \defgroup mac Multi Application Controller functions
 * @{
 */

#if (defined _VRXEVO || defined _WIN32)
#  if   defined VFI_MAC_DLL_EXPORT
#    define SYS_API __declspec(dllexport)
#  elif defined VFI_MAC_DLL_IMPORT && defined _VRXEVO
#    define SYS_API __declspec(dllimport) // dllimport not required for Windows
#  else
#    define SYS_API
#  endif
#elif defined __GNUC__ && defined VFI_MAC_DLL_EXPORT
#  define SYS_API  __attribute__((visibility ("default")))
#else
#  define SYS_API
#endif

#ifndef DOXYGEN
/** Verifone multi applicaton controller (vfimac) namespace */
namespace vfimac 
{
#endif

/** error codes returned by the functions */
enum macError
{
    MAC_ERR_OK                 =  0,  /**< no error */
    MAC_ERR_FAIL               = -1,  /**< generic error */
    MAC_ERR_UNSUPPORTED        = -2,  /**< function is not supported by current platform */
    MAC_ERR_LOCKED             = -3,  /**< layout is locked */
    MAC_ERR_TIMEOUT            = -4,  /**< return code timeout */
    MAC_ERR_LIBNOTIFY          = -5,  /**< Appid is not set, please use sysSetAppid */
    MAC_ERR_APP_NOT_RUNNING    = -6,  /**< App is not running */
    MAC_ERR_WRONG_APPID        = -7,  /**< Appid doesn't exists in any manifest file */
    MAC_ERR_NOT_CP_APP         = -8,  /**< Function supports only CP apps */
    MAC_ERR_UNKNOWN            = -9,  /**< unknown command */
    MAC_ERR_CYCLE              = -10, /**< manifest depencency cycle detected */
    MAC_ERR_DEPENDS            = -11, /**< manifest has missing dependency */
    MAC_ERR_MANIFEST           = -12, /**< manifest validation error */
    MAC_ERR_IPC                = -13, /**< ipc call failed */
    MAC_ERR_NO_APP             = -14, /**< no such app exists */
    MAC_ERR_READY              = -15, /**< mac is not ready to process command. ignored */
    MAC_ERR_ACTIVE             = -16, /**< App was not activated on this terminal */
    MAC_ERR_STOP               = -17, /**< Can't stop application */
    MAC_ERR_NO_APPID           = -18, /**< Appid is not set, please use sysSetAppid */
    MAC_ERR_PERMISSION         = -19, /**< Command is not allowed */
    MAC_ERR_SERVICE            = -20, /**< App is a service */
};

/** virtual keyboard modes */
enum macKeyboard
{
    MAC_KBD_OFF, /**< permanently disable virtual keyboard */
    MAC_KBD_ON, /**< permanently enable virtual keyboard */
    MAC_KBD_AUTO, /**< virtaul keyboard is enabled or disabled automatically on coresponding input field */
};

/** Bring Desktop to foreground.
 * \return error code
 * \note will do nothing if layout is blocked
 */
SYS_API macError sysShowDesktop();

/** Bring control panel to foreground.
 * \return error code
 * \note will do nothing if layout is blocked
 */  
SYS_API macError sysShowControlPanel();

/** Change the layout 
 * \param[in] layoutname 
 * \param[in] statusbar
 * \param[in] not_in_use
 * Switches layout to "layoutname" 
 * \return error code
 * \note will do nothing if layout is blocked
*/
SYS_API macError sysChangeLayout(const std::string & layoutname, 
        bool statusbar = true, 
        bool not_in_use = false );

/** Launch an application with parameter
 * \param[in] appname
 * \param[in] args
 * \return error code
*/
SYS_API macError sysLaunchApp(const std::string & appname, 
        const std::vector<std::string> & args );

/** Launch an application with no args 
 * \param[in] appname
 * \return error code
*/
SYS_API macError sysLaunchApp( const std::string & appname );

/** Launch an application with parameter
 * \param[in] appid
 * \param[in] args
 * \return error code
*/
SYS_API macError sysLaunchAppid(const std::string & appid, 
        const std::vector<std::string> & args );

/** Launch an application with no args 
 * \param[in] appid
 * \return error code
*/
SYS_API macError sysLaunchAppid(const std::string & appid);

/** Show/hide statusbar
 * \param[in] visible
 * \return error code
 * \note will do nothing if layout is blocked
*/
SYS_API macError sysStatusbar(bool visible);

/** Enter/leave fullscreen mode
 * \param[in] enable
 * \return error code
 * \note will do nothing if layout is blocked
*/
SYS_API macError sysFullscreenMode(bool enable);

/** Enable/disable virtual keyboard
 * \param[in] mode
 * \return error code
 * \note will do nothing if layout is blocked
*/ 
SYS_API macError sysVirtualKeyboard(macKeyboard mode);

/** Lock/unlock the layout.
 * \note Prevents other application from changing the layout (e.g. requesting full screen or virtual keyboard).
 *  Prevents from showing the application desktop.
 *  Typically this is being called when applications do user processing and don't want the layout to be changed.
 * \param[in] enable 
 * \return error code
 */
SYS_API macError sysLockLayout(bool enable);

/** Scan for manifests in the predefined locations for manifests.
 * \return error code
 * \note Typically done after a SW update and new installation of Apps
 */
SYS_API macError sysScanManifests();

/** Bring passed appid application to foreground.
 * \param[in] appid 
 * \return error code
 * \note Fails if the layout is currently locked.
 */
SYS_API macError sysToForeground( const std::string & appid );
SYS_API macError sysToForground( const std::string & appid );

/** Stops application by appid
 * \param[in] appid 
 * \return error code
 * \note Stopping native applications is not supported by Verix platform
 */  
SYS_API macError sysStopAppid( const std::string & appid );

/** Get information about availible application
 * \param[in] appList  
 * \return error code
 */ 
SYS_API macError sysGetAppList( vfiipc::JSObject & appList );

/** Stops application by appname
 * \param[in] appname 
 * \return error code
 * \note Stopping native applications is not supported by Verix platform
 */  
SYS_API macError sysStopAppname( const std::string & appname );

/** Stops all CP applications
 * \return error code
 */ 
SYS_API macError sysStopAllCPApps();

/** Shows desktop only with CP apps with certain trigger id
 * \param[in] triggerID 
 * \param[in] args 
 * \return error code
 */  
SYS_API macError sysLaunchCPAppFromDesktop(
        const std::string & triggerID, 
        const std::vector<std::string> & args);

/** Shows desktop only with CP apps with certain trigger id
 * \param[in] triggerID 
 * \param[in] param 
 * \return error code
 */    
SYS_API macError sysLaunchCPAppFromDesktop(
        const std::string & triggerID, 
        const vfiipc::JSObject & param );

/** returns a zero-terminated string with version and build information of Mac
 *  in ADK version string format: major.minor.patch-build, e.g. "1.2.3-4"
 * \return version string */
SYS_API const char * sysMac_GetVersion();

/** Add additional enviroments for CPR applications
 * \param[in] data 
 * \return error code
 * \note values will be accessible to CP application via ARGV[“cp_envData”]
 */
SYS_API macError sysSetCPEnvData(const vfiipc::JSObject & data);

/** Set application id, which will be passed to MAC
 * \param[in] appid 
 * \return error code
 * \note if not set, the one from vfiipc::ipcSetAppID will be used
 */       
SYS_API macError sysSetAppid(const std::string & appid);

#ifndef DOXYGEN
} // namespace vfimac
#endif

/** @}*/

#endif
