#ifndef LEDS_H_
#define LEDS_H_

/** \file led.h
 * \defgroup led System LED functions
 * @{
 */    

#if (defined _VRXEVO || defined _WIN32)
#  if   defined VFI_SYSINFO_DLL_EXPORT
#    define SYS_INFO_API __declspec(dllexport)
#  elif defined VFI_SYSINFO_DLL_IMPORT && defined _VRXEVO
#    define SYS_INFO_API __declspec(dllimport) // dllimport not required for Windows
#  else
#    define SYS_INFO_API
#  endif
#elif defined __GNUC__ && defined VFI_SYSINFO_DLL_EXPORT
#  define SYS_INFO_API  __attribute__((visibility ("default")))
#else
#  define SYS_INFO_API
#endif

namespace vfisysinfo {

// Contactless LEDs
enum ContactlessLeds
{
	CTLS_LED_NONE	= 0x00,
	CTLS_LED_0		= 0x01,
	CTLS_LED_1		= 0x02,
	CTLS_LED_2		= 0x04,
	CTLS_LED_3		= 0x08,
	CTLS_LED_LOGO	= 0x10,
	CTLS_LED_ALL	= CTLS_LED_0 | CTLS_LED_1 | CTLS_LED_2 | CTLS_LED_3 | CTLS_LED_LOGO
};

// Magstripe LEDs
enum MsrLeds
{
	MSR_LED_NONE	= 0x00,
	MSR_LED_0		= 0x01,
	MSR_LED_1		= 0x02,
	MSR_LED_2		= 0x04,
	MSR_LED_ALL		= MSR_LED_0 | MSR_LED_1 | MSR_LED_2
};

// LEDs states
enum LedStates
{
	SWITCH_OFF	= 0,
	SWITCH_ON	= 1
};

/** Switch on of off Contactless LEDs
 * \param[in] ledMap
 * \return 0 - On success. Otherwise - error code
 * \note Example: status = ctlsLedsChangeState( CTLS_LED_0 | CTLS_LED_1 | CTLS_LED_2 | CTLS_LED_3 ).
*/
SYS_INFO_API int ctlsLedsChangeState( int ledMap );

/** Switch on of off Magnetic Card Reader LEDs
 * \param[in] ledMap
 * \return 0 - On success. Otherwise - error code
 * \note Example: status = msrLedsChangeState( MSR_LED_ALL ).
*/
SYS_INFO_API int msrLedsChangeState( int ledMap );

/** Switch on of off Secure Card Reader LEDs
 * \param[in] state
 * \return 0 - On success. Otherwise - error code
 * \note Example: status = scrLedChangeState( SWITCH_ON ).
*/
SYS_INFO_API int scrLedChangeState( int state );

/** Run the MSR LEDs runway show
 * \param[in] repeatCount; 0 - for infinite repeat times ( until msrShowCancel() )
 * \return 0 - On success. Otherwise - error code
 * \note Example: status = msrShowRunway( 5 ).
*/
SYS_INFO_API int msrShowRunway( int repeatCount );

/** Cancel the MSR LED show runway
  * \return 0 - On success
 */
SYS_INFO_API int msrShowCancel( void );

/** Switch on or off Logo LED
 * \param[in] state
 * \return 0 - On success. Otherwise - error code
 * \note Verix not supported.
 * Example: status = logoLedChangeState( SWITCH_ON ).
*/
SYS_INFO_API int logoLedChangeState( int state );

};

#endif /* LEDS_H_ */
