#ifndef SYSBAR_H_2014_08_01
#define SYSBAR_H_2014_08_01

/** \file sysbar.h 
 * \defgroup sysbar System statusbar functions
 * @{
 */

#include <map>
#include <string>
#include "sysinfo.h"
#include "html/gui.h"

#if (defined _VRXEVO || defined _WIN32)
#  if   defined VFI_SYSBAR_DLL_EXPORT
#    define SYS_BAR_API __declspec(dllexport)
#  elif defined VFI_SYSBAR_DLL_IMPORT && defined _VRXEVO
#    define SYS_BAR_API __declspec(dllimport) 
#  else
#    define SYS_BAR_API
#  endif
#elif defined __GNUC__ && defined VFI_SYSBAR_DLL_EXPORT
#  define SYS_BAR_API  __attribute__((visibility ("default")))
#else
#  define SYS_BAR_API
#endif

#ifndef DOXYGEN
/** Verifone system information (vfisysinfo) namespace */
namespace vfisysinfo {
#endif

/** Callback function that is called every time before the statusbar is refreshed.
 * The statusbar is refreshed at regular intervals, which are specified by 
 * the refresh time that can be configured with function sysSetRefreshTime().
 * Application may add customized values to map \a values that should be passed
 * to next uiInvoke() to refresh the statusbar.
 * \param[in] data data pointer provided by the application
 * \param[in] region_id ID of the GUI region, from which the statusbar callback is triggered
 * \param[in,out] values values to be passed to next uiInvoke() call for this statusbar.
 *                       The map already contains values set by sysSetStatusbarValues().
 * \return true, if the statusbar should be refreshed, else no refresh is done.
 * \note It is not allowed to call any statusbar API functions on the same region
 *       \a region_id from inside the callback function. All API functions will 
 *       return SYS_ERR_PERMISSION in this case.
 */
typedef void (*sysStatusbarCallback)(void *data, int region_id, std::map<std::string,std::string> &values);

/** Callback function which is called by gui in case as an update
 * \note this callback function is discribed in html/gui.h header file
 *      \a if statusbar refersh is disabled - this sysRefresStatusBar should be called to reset callback
 */
typedef vfigui::uiAsyncCallback sysStatusbarReturnCallback;

/** Start a statusbar for the specified region ID using a URL for the HTML document
 * \param[in] region_id GUI region ID
 * \param[in] url string containing the URL to the GUI html file used by the statusbar
 * \param[in] values initial values of user defined variables used in the HTML document for the statusbar. These values will be provided in addition to automatically provided system values.
 * \param[in] cb optional callback function, see ::sysStatusbarCallback
 * \param[in] cbdata data pointer passed on to the callback function
 * \return error code
 * \note The statusbar will be displayed in the provided GUI region ID. Callers have to configure the region previously. 
 *       Multiple statusbars can be provided in different region IDs.
 *       The statusbar does regular updates in the background and needs to be stopped for power-saving to take effect on some platforms.
 *
 *       The following system variables are provided for use in the HTML document that describes the statusbar.
 *       The values for these system variables are automatically supplied and updated by the statusbar background task for this region ID.
 *       They can be used in the HTML document as &lt;?var variablename ?&gt;.
 *       
 *       a) Date/Time variables
 *           - sys_time             :  preformated time string, format: "hh:mm"
 *           - sys_time_hour        :  hours, format: "00"-"23"
 *           - sys_time_hour_monadic:  hours, format: "0"-"23"
 *           - sys_time_min         :  min, format: "00"-"59"
 *           - sys_time_sec         :  sec, format: "00"-"60" (The range is up to 60 to allow for occasional leap seconds)
 *           - sys_time_day         :  day, format: "01"-"31"
 *           - sys_time_month       :  month, format: "01"-"12"
 *           - sys_time_year        :  year, format: "yyyy"
 *           - sys_time_12h         :  hours, format: "01"-"12"
 *           - sys_time_12h_monadic :  hours, format: "1"-"12"
 *           - sys_time_am_pm       :  AM or PM designation, format: "AM" or "PM"
 *
 *          Please note:
 *          Date/Time variables in a statusbar have a special role, since they require an update, if the time unit changes.
 *          For this reason the statusbar is updated out of refresh intervals automatically, if a time unit is increased.
 *          Depending on time format variable the statusbar is refreshed with beginning of the next time unit as follows:
 *              format variable |  time unit
 *              ----------------|---------------
 *              sys_time_sec    |  every second
 *              sys_time_min    |  every minute
 *              sys_time        |  every minute
 *              others          |  every hour
 *          Attention:
 *          sys_time_sec may cause a high CPU load, since the GUI server will reder region of this statusbar every second.
 *
 *       b) Battery information
 *           - sys_battery_percentage  : Battery Charge Percentage, format: "0"-"100", "-1" if no battery is available
 *           - sys_battery_percentage_2: Battery Charge Percentage, format: "0%"-"100%", "" if no battery is available
 *           - sys_battery_level       : Battery Charge Level, format: "1"-"11" (or "101-111" for battery charging), "-1" if no battery available
 *                                       Battery Charge Level is mapped from Battery Charge Percentage as follows:
 *                                         Percentage  |  Level | Level (battery charging)
 *                                         ------------|--------|-------------------------
 *                                            above 95 |   11   |           111           
 *                                            above 90 |   10   |           110           
 *                                            above 80 |   9    |           109           
 *                                            above 70 |   8    |           108           
 *                                            above 60 |   7    |           107           
 *                                            above 50 |   6    |           106           
 *                                            above 40 |   5    |           105           
 *                                            above 30 |   4    |           104           
 *                                            above 20 |   3    |           103           
 *                                            above 10 |   2    |           102           
 *                                            above 5  |   1    |           101           
 *                                            below 6  |   0    |           100           
 * 
 *       c) Docking Status 
 *            - sys_docking_status  : Docking Status (format: "0"==undocked, "1"==docked, battery charging)
*/
SYS_BAR_API int sysStartStatusbarURL(int region_id, const std::string &url, const std::map<std::string,std::string> &values,
                         sysStatusbarCallback cb=0, void *cbdata=0);

/** Start a statusbar for the specified region ID using a URL for the HTML document with additional return callback
 * \param[in] region_id GUI region ID
 * \param[in] url string containing the URL to the GUI html file used by the statusbar
 * \param[in] values initial values of user defined variables used in the HTML document for the statusbar. These values will be provided in addition to automatically provided system values.
 * \param[in] cb optional callback function, see ::sysStatusbarCallback
 * \param[in] cbdata data pointer passed on to the callback function
 * \param[in] returncb optional callback function for return value, see ::sysStatusbarReturnCallback
 * \return error code
 * \note Same as sysStartStatusbarURL but with additional callback for HTML return value
 */
SYS_BAR_API int sysStartStatusbarURLReturnVal(int region_id, 
        const std::string &url, 
        const std::map<std::string,std::string> &values, 
        sysStatusbarCallback cb, 
        void *cbdata,
        sysStatusbarReturnCallback returncb);

/** Start a statusbar for the specified region ID providing the HTML document as a string
 * \param[in] region_id GUI region ID
 * \param[in] html string containing the HTML document used by the statusbar
 * \param[in] values initial values of user defined variables used in the HTML document for the statusbar. These values will be provided in addition to automatically provided system values.
 * \param[in] resource_path search path for resources (e.g. images) referenced by the HTML document used for statusbar. If not provided the GUI resource path set in the creating thread will be used.
 * \param[in] cb optional callback function, see ::sysStatusbarCallback
 * \param[in] cbdata data pointer passed on to the callback function
 * \param[in] cb optional callback function for HTML return value, see ::sysStatusbarReturnCallback
 * \param[in] returncb - callback to be invoked when statusbar html returns
 * \return error code
 * \note see sysStartStatusbarURL for details.
*/
SYS_BAR_API int sysStartStatusbar(int region_id, const std::string &html, const std::map<std::string, std::string> &values,
                      const std::string &resource_path=std::string(), sysStatusbarCallback cb=0, void *cbdata=0,
                      sysStatusbarReturnCallback returncb=0);

/** Stop statusbar display and background update for the specified region ID
 * \param[in] region_id GUI region ID
 * \return error code
 */
SYS_BAR_API int sysStopStatusbar(int region_id);


/** Set statusbar user variables
 * \param[in] region_id GUI region ID
 * \param[in] values updated values of user defined variables used in the HTML document for the statusbar.
 * \return error code
 * \note The statusbar will be updated immediately after this call and the new user variables will be applied.
 */
SYS_BAR_API int sysSetStatusbarValues(int region_id, const std::map<std::string,std::string> &values);


/** Get statusbar user variables
 * \param[in] region_id GUI region ID
 * \param[out] values current values of user defined variables used in the HTML document for the statusbar.
 * \return error code
 */
SYS_BAR_API int sysGetStatusbarValues(int region_id, std::map<std::string,std::string> &values);



/** Set refresh time of the statusbar
 * \param[in] region_id GUI region ID
 * \param[in] refresh_time_ms statusbar refresh time in milliseconds
 * \return error code
 * \note The statusbar will be updated immediately after this call and the new refresh interval will be applied.
 *       The initial default for the statusbar update is 4 sec. Please note that a short refresh time may cause
 *       The initial default for the statusbar update is 60 sec. Please note that a short refresh time may cause
 *       a high CPU load, since the GUI server will reder region of this statusbar again.
 *       A negative refresh time disables update intervals of the statusbar and application is responsible
 *       to update/refresh the statusbar contents with sysRefreshStatusBar() or sysSetStatusbarValues().
 *       If refresh time is set to zero - statusbar will determine refresh time using html fields 
 */
SYS_BAR_API int sysSetRefreshTime(int region_id, int refresh_time_ms);


/** Get refresh time of the statusbar
 * \param[in] region_id GUI region ID
 * \param[out] refresh_time_ms statusbar refresh time in milliseconds
 * \return error code
 */
SYS_BAR_API int sysGetRefreshTime(int region_id, int *refresh_time_ms);


/** Trigger immediate refresh of the statusbar
 * \param[in] region_id GUI region ID
 * \return error code
 * \note The statusbar will be updated immediately after this call.
 */
SYS_BAR_API int sysRefreshStatusBar(int region_id);


/** returns a zero-terminated string with version and build information of libvfisysbar
  * \return version string */
SYS_BAR_API const char *sysStatusbar_GetVersion();

/** returns a zero-terminated string with version and build information of libvfisysbar
  * \return version string */
SYS_BAR_API const char *sysStatusbarGetVersion();

#ifndef DOXYGEN
} // namespace vfisysinfo
#endif

/** @}*/

#endif
