#ifndef SYSINFO_H_2014_08_01
#define SYSINFO_H_2014_08_01

#if (defined _VRXEVO || defined _WIN32)
#  if   defined VFI_SYSINFO_DLL_EXPORT
#    define SYS_INFO_API __declspec(dllexport)
#  elif defined VFI_SYSINFO_DLL_IMPORT && defined _VRXEVO
#    define SYS_INFO_API __declspec(dllimport) // dllimport not required for Windows
#  else
#    define SYS_INFO_API
#  endif
#elif defined __GNUC__ && defined VFI_SYSINFO_DLL_EXPORT
#  define SYS_INFO_API  __attribute__((visibility ("default")))
#else
#  define SYS_INFO_API
#endif
 
/** \file sysinfo.h 
 * \defgroup sysinfo System property functions
 * @{
 */

#include <map>
#include <string>

#ifndef DOXYGEN
/** Verifone system information (vfisysinfo) namespace */
namespace vfisysinfo 
{
#endif

/** error codes returned by the functions */
enum sysError{
  // general errors
  SYS_ERR_OK                 =  0,  /**< no error */
  SYS_ERR_PROP_UNSUPPORTED   = -1,  /**< the property does not exist or is not supported on the platform */
  SYS_ERR_PROP_READ_ONLY     = -2,  /**< the property value can only be get */
  SYS_ERR_PROP_WRITE_ONLY    = -3,  /**< the property value can only be set */
  SYS_ERR_PARAMETER          = -4,  /**< parameter error */
  SYS_ERR_PERMISSION         = -5,  /**< insufficient permissions */
  SYS_ERR_REGION             = -6,  /**< wrong region ID */
  SYS_ERR_FAIL               = -7,  /**< generic error */
  SYS_ERR_UNSUPPORTED        = -8,  /**< the function requested is not supported on the current platform/device/... */
};

/** Int type system properties */
enum SYSPropertyInt {
   SYS_PROP_BATTERY_AVAILABLE=0,        /**< read-only, Battery exists: 1: battery available, else 0*/
   SYS_PROP_BATTERY_STATUS_OK=1,        /**< read-only, Battery status: 1: ok, else 0 */
   SYS_PROP_BATTERY_VOLTAGE=3,          /**< read-only, Battery voltage in units of mV */
   SYS_PROP_BATTERY_CAPACITY=4,         /**< read-only, Battery capacity in mAh */
   SYS_PROP_BATTERY_CHARGE_LEVEL=5,     /**< read-only, Battery charge level in percent (0-100)*/
   SYS_PROP_DOCKING_STATUS=6,           /**< read-only, Docking status, 0:undocked, 1:docked */
   SYS_PROP_INTERNAL_BATTERY_STATUS=7,  /**< read-only, Internal battery battery status, 1: ok, else 0 */
   SYS_PROP_INTERNAL_BATTERY_VOLTAGE=8, /**< read-only, Internal battery voltage in units of mV */
   SYS_PROP_KEYBOARD_BEEP=100,          /**< write-only, Keyboard beep on key press, 1: enabled, 0: disabled */
   SYS_PROP_KEYB_BACKLIGHT,             /**< read/write, Keyboard backlight*/
   SYS_PROP_DISP_CONTRAST=200,          /**< read/write, Display contrast (Not supported on V/OS)*/
   SYS_PROP_DISP_BACKLIGHT,             /**< read/write, Display backlight*/
   SYS_PROP_HW_RAM_SIZE=300,            /**< read-only, RAM size KB*/
   SYS_PROP_HW_FLASH_SIZE,              /**< read-only, Flash size KB*/
   SYS_PROP_HW_RAM_USED,                /**< read-only, Used ram size KB */     
   SYS_PROP_HW_FLASH_USED,              /**< read-only, Used flash size KB */     
   SYS_PROP_LED,                        /**< read-only, LED present (0 - No physical leds, 1 - has leds) */
   SYS_PROP_VOLUME=400,                 /**< read/write, speaker volume (0-100) */
   SYS_PROP_ADE_STATUS=500,             /**< read-only, ADE status 1: active, 0: inactive */
   SYS_PROP_CERT_SPONSOR_PROD=501,      /**< read-only, sponsor certificate 1: prod, 0: test */
};

/** String type system properties */
enum SYSPropertyString {
   SYS_PROP_HW_MODEL_NAME=0,        /**< read-only, Hardware model name  */
   SYS_PROP_HW_SERIALNO=1,          /**< read-only, Hardware serial number */
   SYS_PROP_OS_VERSION=2,           /**< read-only, OS version */
   SYS_PROP_HW_PTID=3,              /**< read-only, Terminal unit identification number */
   SYS_PROP_HW_VARIANT_NAME=4,      /**< read-only, Hardware variant namr*/
   SYS_PROP_HW_PART_NO=5,           /**< read-only, Hardware part number*/
   SYS_PROP_HW_VERSION=6,           /**< read-only, Hardware version*/
   SYS_PROP_HW_LOT_NO=7,            /**< read-only, Hardware LOT number*/
   SYS_PROP_BOOT_VERSION=8,         /**< read-only, Boot(SBI) version*/
   SYS_PROP_TERMINAL_SPONSOR=9,     /**< read-only, Sponsor certificate name*/
   SYS_PROP_MANUF_DATE=10,          /**< read-only, manufacturing date/time in format "yyyymmddhhmmss" */
   SYS_PROP_CERT_SPONSOR_SN=11,     /**< read-only, Sponsor certificate serial number*/
   SYS_PROP_CERT_SPONSOR_MODE=12,   /**< read-only, Sponsor certificate mode: "test" or "prod" */
   SYS_PROP_RTC=100,                /**< read/write, Real-Time-Clock date/time in format "yyyymmddhhmmss" */
   SYS_PROP_BATTERY_SERIALNO=200    /**< read-only, Battery serial number, only available on some devices */
};

/** get int property 
 * \param[in] property property to be get
 * \param[out] value property value
 * \return error code
 */
SYS_INFO_API int sysGetPropertyInt(enum SYSPropertyInt property, int *value);

/** set int property 
 * \param[in] property property to be set
 * \param[in] value property value
 * \return error code
 */
SYS_INFO_API int sysSetPropertyInt(enum SYSPropertyInt property, int value);

/** get string property 
 * \param[in] property property to be get
 * \param[out] value current value
 * \param[in] len size ouf output buffer \a value in bytes
 * \return error code
 */
SYS_INFO_API int sysGetPropertyString(enum SYSPropertyString property, char* value, int len);

/** set string property 
 * \param[in] property property to be set
 * \param[in] value new value, C-string
 * \return error code
 */
SYS_INFO_API int sysSetPropertyString(enum SYSPropertyString property, const char* value);

/** get string property 
 * \param[in] property property to be get
 * \param[out] value current value
 * \return error code
 */
SYS_INFO_API int sysGetPropertyString(enum SYSPropertyString property, std::string &value);

/** set string property 
 * \param[in] property property to be set
 * \param[in] value new value
 * \return error code
 */
SYS_INFO_API int sysSetPropertyString(enum SYSPropertyString property, const std::string &value);

/** returns a zero-terminated string with version and build information of libvfisysinfo
 *  in ADK version string format: major.minor.patch-build, e.g. "1.2.3-4"
 * \return version string */
SYS_INFO_API const char *sysInfo_GetVersion();

/** returns a zero-terminated string with version and build information of libvfisysinfo
  * \return version string */
SYS_INFO_API const char *sysGetVersion();

/** Reboot terminal
 * \return error code
 */
SYS_INFO_API int sysReboot();

/** Puts terminal in sleep mode
 * \return error code
 */
SYS_INFO_API int sysSleep();

/** Shut down terminal
 * \return error code
 */
SYS_INFO_API int sysShutdown();

/** returns a zero-terminated string with SDK version used during application compilation 
  * \return SDK version string */    
inline const char * sysSDKVersion()
{
#if defined _VRXEVO
#include <sdkver.h>
return VVSDK_VER;
#elif defined _VOS2
return "UNKNOWN";
#elif defined _VOS
#include <sdk-version.h>
return SDK_VERSION;
#else
return "UNKNOWN";
#endif
}

#ifndef DOXYGEN
} // namespace vfisysinfo
#endif
/** @}*/

#endif
