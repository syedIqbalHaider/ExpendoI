/** @file posix_file.c
 *
 *  Wrapper function for POSIX compatibility. The functions behave as defined
 *  in the POSIX standard but due to the operating system functions are not high-performance.
 *
 */
#include "posix.h"

#undef fopen
FILE *posix_fopen2(const char *filename, const char *mode)
{
  FILE *f=fopen(filename,mode);
  if(!f) return 0;
  /* Enable buffering with setvbuf() to increase performance 
   * of SDK file functions. Use a 4k buffer that corresponds
   * to the memory page of the NAND flash. */
  setvbuf(f,0,_IOFBF,4096);
  return f;
}
