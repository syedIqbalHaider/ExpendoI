#include "unistd.h"
#include "posix.h" // use fast fopen() for truncate()

int access(const char *pathname, int /* mode */)
{
    if (dir_get_attributes(pathname) >= 0) return 0;
    return -1;
}

pid_t getpid()
{
    return get_task_id();
}

int fsync(int file_descriptor)
{
	return 0;
}

int fchown( int fd, uid_t owner, gid_t group )
{
	return 0;
}

int unlink(const char *path)
{
	return _remove(path);
}

int ftruncate(int fildes, off_t length)
{
  int rc = -1;
  long filesize = lseek(fildes, 0, SEEK_END);
  if(filesize==-1) return -1;
  if(filesize>length)
  {
    if(lseek(fildes, length, SEEK_SET)==-1) return -1;
    rc = delete_(fildes, filesize-length);
  }
  else
  {
    unsigned char zeros[1024]={0};
    long cnt=length-filesize;
    rc=0;
    while(cnt>0) {
      int len=cnt>1024?1024:(int)cnt;
      if(write(fildes,(const char *)zeros,len)!=len)
      {
        rc=-1;
        break;
      }
      cnt-=(long)len;    
    }
  }
  return rc;
}

int truncate(const char *filename, off_t length)
{
   FILE *fp;
   int r;
   if((fp=fopen(filename,"rb+"))==0) return -1;
   r=ftruncate(fileno(fp),length);
   fclose(fp);
   return r;
}

unsigned int sleep(unsigned int seconds)
{
	SVC_WAIT(seconds*1000);
	return 0;
}

int usleep(useconds_t usec)
{
  SVC_WAIT(usec/1000);
  return 0;
}

uid_t getuid (void)
{
	return 1;
}

uid_t geteuid(void)
{
	return 1;
}

int utimes(const char *path, const struct utimbuf *times)
{
	return -1;
}

#undef getcwd
char *posix_getcwd(char *buf, size_t size)
{
  if(getcwd(buf, size)==0) return buf;
  return NULL;
}

#undef mkdir
int posix_mkdir(const char *pathname, unsigned mode)
{
  return mkdir(pathname);
}

