#include <svc_net.h>
#include "pthread_private.h"

/* this function is excluded from pthread.cpp to avoid that libc will depend from net.lib
 * as long as pthread_add_to_sock_transition_list() is not used by application */ 
extern "C" void pthread_add_to_sock_transition_list(int sockfd)
{
  pthread_add_to_fd_list(sockfd,socketset_owner);
}
