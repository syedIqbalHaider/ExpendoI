#include <svc.h>
#include <string.h>
#include <sys/stat.h>

#include "time2.h"

int stat(const char *file_name, struct stat *buf)
{
  int rc = -1;
  long size;
  
  int mode = dir_get_attributes(file_name);

  if (mode != -1)
  {
    memset(buf, 0, sizeof(struct stat));
    if(mode & 0x01) buf->st_mode=0555; else buf->st_mode=0777;
    size = dir_get_file_sz (file_name);

    if (size >= 0)
    {
      buf->st_size = size;
      rc = 0;
    }
    
    {
      char timestamp[15];
      timestamp[sizeof(timestamp)-1] = 0;
      if (dir_get_file_date(file_name, timestamp) == 0)
      {
        // convert timestamp to time_t
        struct tm tms;
        //yyyymmddhhmmss
        if (strptime(timestamp, "%Y%m%d%H%M%s", &tms))
        {
          buf->st_atime = buf->st_mtime = buf->st_ctime = mktime(&tms);
        }
      }
    }
  }
  
  return rc;
}

mode_t umask(mode_t mask)
{
    // Nothing to do here...
    return 0;
}

int fchmod(handle_t f, int mode)
{
    // Nothing to do here...
    return 0;
}

int fstat(int fildes, struct stat *buf)
{
	long size;
	if(!buf)
		return -1;
	
	memset(buf, 0, sizeof(struct stat));
/*
	{
		char dev_id[128];
		if(get_name(fildes, dev_id) != 0)
		{
			return -1;
		}	
		size = dir_get_file_size(dev_id);
	}
*/
	{
		int cur = lseek(fildes, 0, SEEK_CUR);
		if(cur == -1)
		{
			return -1;
		}	
		
		size = lseek(fildes, 0, SEEK_END);
		lseek(fildes, cur, SEEK_SET);
	}
	
	buf->st_size = size;
//	buf->st_mode = S_IFREG;
    buf->st_mode = 0777;
	
	return size != -1 ? 0 : -1;
}
