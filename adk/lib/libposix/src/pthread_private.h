#ifndef PTHREAD_PRIVATE
#define PTHREAD_PRIVATE

void pthread_add_to_fd_list(int fd, int (*set_owner_fct)(int , int));

#endif
