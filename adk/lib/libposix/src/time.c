#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <svc.h>
#include <errno.h>
#include "time2.h"
#include "sys/time.h"
#include <eoslog.h>

// mutex for protection of reentrant functions gmtime_r(), localtime_r()
static sem_t time_mutex={0,0,1};
// mutex for protection of gettimeofday() and settimeofday()
static sem_t gtod_mutex={0,0,1};
// flag set to 1, if gettimeofday tick counter must be synchronized with the clock
static int clock_init;

// seconds as limit to detect if the gettimeofday tick counter is asynchronous with the clock
#define SYNC_SEC_LIMIT 1

struct tm *gmtime_r(const time_t *timep, struct tm *result)
{
  struct tm *res;
  if(!timep || !result) return NULL;
  sem_wait(&time_mutex);
  res=gmtime(timep);
  if(!res) {
    sem_post(&time_mutex);
    return NULL;
  }
  *result=*res;
  sem_post(&time_mutex);
  return result;
}

struct tm *localtime_r(const time_t *timep, struct tm *result)
{
  struct tm *res;
  if(!timep || !result) return NULL;
  sem_wait(&time_mutex);
  res=localtime(timep);
  if(!res) {
    sem_post(&time_mutex);
    return NULL;
  }
  *result=*res;
  sem_post(&time_mutex);
  return result;
}

static long ticks2sec(unsigned ticks_high, unsigned ticks_low, unsigned long *msec)
{
    // assumption: TICKS_PER_SEC == 1000

    long t=ticks_high/1000;
    long r=ticks_high%1000;
    
    t=(t<<16)+((r<<16)|(ticks_low>>16))/1000;
    r=        ((r<<16)|(ticks_low>>16))%1000;
    
    t=(t<<16)+((r<<16)|(ticks_low&0xffff))/1000;
    r=        ((r<<16)|(ticks_low&0xffff))%1000;

    if(msec) *msec=r;
    return t;
}

int gettimeofday(struct timeval *tv, struct timezone *tz)
{
   static unsigned ticks_high,ticks_low;  // 64 bit tick counter to provide time of day in milliseconds unit
   static unsigned long start_time;
   unsigned long now_time;
   unsigned t,i;
   char tbuf[15];
   
   if(!tv) return EINVAL;
   if(tz) memset(tz,0,sizeof(*tz));
   
   sem_wait(&gtod_mutex);
   
   // read the ticks
   t=read_ticks();
   // read the clock
   read_clock(tbuf);

   // update the 64 bit tick counter
   if(t<ticks_low) ticks_high++;
   ticks_low=t;

   // calculate seconds since 1900 to clock time
   datetime2seconds(tbuf, &now_time);
   // now_time+=seconds_offset_1970; // unix time offset (seconds 1970 -> 1900)
   now_time+=seconds_offset_1900;    // use Verix offset (seconds 1980 -> 1900), since other SDK functions like strftime() and time() use same time base 1900 (and not as expected unix time 1970)

   // check if clock is still synchronous with the tick counter
   if(clock_init) {
      unsigned long now_ticks;
      now_ticks=ticks2sec(ticks_high,ticks_low,0)+start_time;
      if(now_ticks>now_time) {
         if(now_ticks-now_time>SYNC_SEC_LIMIT) clock_init=0;
      } else {
         if(now_time-now_ticks>SYNC_SEC_LIMIT) clock_init=0;
      }
   }
   // (re-)init the tick counter start time
   if(!clock_init) {
      unsigned th,tl;
      th=ticks_high;
      tl=ticks_low+500; // round to whole seconds
      if(tl<ticks_low) th++;
      start_time=now_time-ticks2sec(th,tl,0); // decrease ticker seconds that are added again below
      clock_init=1;
   }
   
   tv->tv_sec=ticks2sec(ticks_high,ticks_low,&tv->tv_usec)+start_time;
   tv->tv_usec*=1000;
   
   sem_post(&gtod_mutex);
   
   return 0;
}


int clock_gettime(clockid_t clk_id, struct timespec *res)
{
  if(!res) return -1;

  // realtime clock
  if(clk_id==CLOCK_REALTIME) {
     struct timeval tv;
     gettimeofday(&tv,0);
     res->tv_sec=tv.tv_sec;
     res->tv_nsec=tv.tv_usec*1000; // microseconds to nanoseconds
  }
  // monotonic clock
  else {
     static unsigned ticks_high_m,ticks_low_m;  // 64 bit tick counter to provide monotonic time
     unsigned t;
     // update the 64 bit tick counter
     sem_wait(&time_mutex);
     t=read_ticks();
     if(t<ticks_low_m) ticks_high_m++;
     ticks_low_m=t;
     res->tv_sec=ticks2sec(ticks_high_m,ticks_low_m,(unsigned long *)&res->tv_nsec);
     res->tv_nsec*=1000000; // msec to nanoseconds
     sem_post(&time_mutex);
  }
  return 0;
}


int settimeofday(const struct timeval *tv, const struct timezone *tz)
{
  int    hClock, ret=0;
  char   yyyymmddhhmmss[14];
  struct tm     datetime;

  sem_wait(&gtod_mutex);

  hClock = open(DEV_CLOCK, 0);
  if(hClock>=0)
  {
    memset(yyyymmddhhmmss, 0x00, sizeof(yyyymmddhhmmss));
    localtime_r(&tv->tv_sec, &datetime);
    sprintf(yyyymmddhhmmss,"%04d%02d%02d%02d%02d%02d",
          datetime.tm_year+1900, 
          datetime.tm_mon+1, 
          datetime.tm_mday,
          datetime.tm_hour, 
          datetime.tm_min, 
          datetime.tm_sec); 
    write(hClock, yyyymmddhhmmss, sizeof(yyyymmddhhmmss));
    close(hClock);
    clock_init=0; // synchronize gettimeofday tickcounter with the system clock (just an optimization that avoids check in gettimeofday)
  }
  else ret=-1;

  sem_post(&gtod_mutex);

  return ret;
}
