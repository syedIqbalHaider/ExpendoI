#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>

/* NKJT
 This is required for RVDS2 as this dunctionality is in RVDS4 but not2
*/
#if (__ARMCC_VERSION < 220000 )
int snprintf( char *, size_t, const char *, ... );
int vsnprintf( char *, size_t, const char *, _va_list );

int snprintf(char* str, size_t size, const char* fmt, ...) 
{
  int ret;
  va_list ap;

  va_start(ap, fmt);
  ret = vsnprintf(str, size, fmt, ap);
  va_end(ap);

  return ret;
}

#endif

