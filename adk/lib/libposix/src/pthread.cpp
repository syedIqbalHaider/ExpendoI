#include <svc.h>
#include <time.h>
#include <errno.h>
#include <svc_net.h>
#include "pthread.h"
#include "pthread_private.h"

#include <vector>
#include <map>

// #include "vrx_printf.h"
// #include <eoslog.h>

//using namespace std;

struct WaitNode {
   sem_t wait;
   bool timeout;
   WaitNode() { sem_init(&wait,0); timeout=false;}
};

/** private data for structure pthread_mutex_t */
struct pthread_mutex_priv {
   sem_t blocking;           /**< semaphore used for blocking the waiters */
   pthread_mutexattr_t attr; /**< attribute for mutex variable */
   unsigned waiting_cnt;     /**< counter for the waiters */
   pthread_t owner;          /**< owner task of mutex, e.g. required for mode PTHREAD_MUTEX_RECURSIVE */
   unsigned resursive_cnt;   /**< counter for recursive entries for mode PTHREAD_MUTEX_RECURSIVE */
};

/** private data for structure pthread_cond_t */
struct pthread_cond_priv {
  vector<WaitNode *> *waiters;  /**< vector storing IDs of waiting threads */
  pthread_condattr_t attr;      /**< attribute for condition variable */
};

#define NO_THREAD -1
#define INIT_REQUIRED(s) ((s)->inited<=0)

/* local controller thread id */
static int controlthread=NO_THREAD;

static bool start_control_thread();
static bool add_timeout(WaitNode *wn, const struct timespec *abstime, clockid_t clock_id);
static void remove_timeout(WaitNode *wn);

/* mutex for protection of some initializations */
static sem_t init_mutex={0,0,1};

/* mutex for protection of invoke data map */
static sem_t invoke_mutex={0,0,1};

/* mutex for protection of thread specific data */
static sem_t key_mutex={0,0,1};

/* mutex for protection of file descriptor owner list */
static sem_t fd_mutex={0,0,1};

/* mutex for protection of controller thread data */
static sem_t control_mutex={0,0,1};

/* destructor function pointer for thread specific data */
typedef void (*destructor_func_t)(void*);

/* map storing keys and destructors for thread specific data -> protected by key_mutex
           key,          destructur */
static map<pthread_key_t,destructor_func_t> key_map;
/* map storing values associated to keys for each thread -> protected by key_mutex
        thread,    key,           value */
static map<int, map<pthread_key_t, void *> > value_map;

/* structure storing invokation data for thread_func() */
struct invoke_data {
  void * (*start_routine)(void *);
  void *arg;
  sem_t start_sync_mutex;
  bool detached;
};

/* map storing invokation data created by pthread_create() -> protected by invoke_mutex */
static map<int,struct invoke_data *> invoke_data_map;

/* map storing return values of the threads for pthread_join(),
 * since thread_join() of Verix OS is not working -> protected by invoke_mutex */
static map<int,void *> join_retval_map;

/* structure storing file descriptors and owner transition 
 * function for this descriptor (e.g. set_owner()) */
struct fd_list {
  int fd;
  int (*set_owner_fct)(int , int);
};
/* map storing structures of fd_list that are added by function pthread_add_to_fd_transition_list() 
 * and pthread_add_to_sock_transition_list(), which ownership will be transfered to the new 
 * thread created by pthread_create(). */
static map<int, vector<struct fd_list> > fd_owner_list; 

/* vector storing task ids of detached created threads -> protected by control_mutex */
static vector<int> detached_list;

static void thread_clean_up(void *retval)
{
  // call destructors of thread specific data
  sem_wait(&key_mutex);
  map<int, map<pthread_key_t, void *> >::iterator it_value; 
  // thread specific data available
  if((it_value=value_map.find(get_task_id()))!=value_map.end()) {
    // for all keys of the value map
    map<pthread_key_t, void *>::iterator it_key_values;
    for (it_key_values=it_value->second.begin();
         it_key_values!=it_value->second.end(); 
         ++it_key_values)
    {
      map<pthread_key_t,destructor_func_t>::iterator it_key;  
      if(   it_key_values->second                                      // a value was set
         && (it_key=key_map.find(it_key_values->first))!=key_map.end() // key is member of the map containing the destructors for the value   
         && it_key->second)                                            // destructor was configured
      {
        // call the destructor
        destructor_func_t destr_func_ptr=it_key->second;
        destr_func_ptr(it_key_values->second);
      }
    }
    // delete complete thread specific data
    value_map.erase(it_value);
  } 
  // thread specific data is done
  sem_post(&key_mutex);

  int this_thread=get_task_id();
  sem_wait(&invoke_mutex);
  // get invoke_data created by pthread_create()
  struct invoke_data *data=(struct invoke_data *)invoke_data_map[this_thread];
  invoke_data_map.erase(this_thread);
  // add detached thread to the thread list and signal 
  if(data) {
     // for a detached thread add the thread ID to detach list of control thread
     if(data->detached) {
      sem_wait(&control_mutex);
      detached_list.push_back(this_thread);
      sem_post(&control_mutex);
      post_user_event(controlthread,1);
    }
    // for a joinable thread add return value (if any) to return value map for pthread_join()
    else {
      if(retval) join_retval_map[get_task_id()]=retval;
    }
    // free invoke_data for this thread
    delete data;
  }
  sem_post(&invoke_mutex);
}

int pthread_attr_init(pthread_attr_t *attr)
{
  if(!attr) return EINVAL;
  attr->stacksize=64*1024;
  attr->detachstate=PTHREAD_CREATE_JOINABLE;
  return 0;
}

int pthread_attr_destroy(pthread_attr_t *attr)
{
  return 0; 
}

int pthread_attr_setstacksize(pthread_attr_t *attr, int stacksize)
{
  if(!attr) return EINVAL;
  attr->stacksize=stacksize;
  return 0; 
}

int pthread_attr_getstacksize(pthread_attr_t *attr, size_t *stacksize)
{
  if(!attr || !stacksize) return EINVAL;
  *stacksize=attr->stacksize;
  return 0;
}

int pthread_attr_setdetachstate(pthread_attr_t *attr, int detachstate)
{
  if(!attr) return EINVAL;
  switch(detachstate) {
    case PTHREAD_CREATE_JOINABLE:
    case PTHREAD_CREATE_DETACHED: break;
    default: return EINVAL; // invalid value
  }
  attr->detachstate=detachstate;
  return 0;
}

int pthread_attr_getdetachstate(pthread_attr_t *attr, int *detachstate)
{
  if(!attr || !detachstate) return EINVAL;
  *detachstate=attr->detachstate;  
  return 0;
}

// private function (see pthread_private.h)
void pthread_add_to_fd_list(int fd, int (*set_owner_fct)(int , int))
{
  sem_wait(&fd_mutex);
  vector<struct fd_list> *v=&fd_owner_list[get_task_id()];
  // add file descriptor, if not already part of list
  int i;
  for(i=0;i<v->size();i++) {
    if((*v)[i].fd==fd) {
      (*v)[i].set_owner_fct=set_owner_fct; // update set_owner function
      break;
    }
  }
  if(i>=v->size()) {
    struct fd_list entry={fd,set_owner_fct};
    v->push_back(entry);
  }
  sem_post(&fd_mutex);
}

void pthread_add_to_fd_transition_list(int fd)
{
  pthread_add_to_fd_list(fd,set_owner);
}

static void *thread_func(void *arg)
{
  struct invoke_data *data=(struct invoke_data *)arg;
  sem_wait(&invoke_mutex);
  invoke_data_map[get_task_id()]=data; // store in map for thread_clean_up() function
  sem_post(&invoke_mutex);

  // wait until pthread_create() signalizes start of thread (e.g. until ownership of file descriptors are transfered to this thread)
  sem_wait(&data->start_sync_mutex);
  sem_post(&data->start_sync_mutex); // just to unlock the mutex again
  // call the thread routine
  void *ret=data->start_routine(data->arg);
  // cleanup invokation data and thread specific data
  thread_clean_up(ret);
  return ret;
}

extern "C" int pthread_create(pthread_t * thread, 
                              const pthread_attr_t * attr,
                              void * (*start_routine)(void *), 
                              void *arg)
{
   pthread_attr_t local_attr;

   if(!thread || !start_routine) return EINVAL;
    
   if(!attr) {
     pthread_attr_init(&local_attr);
     attr=&local_attr;
   }  
   // start thread controlling detached threads (and timers)
   if(   attr->detachstate==PTHREAD_CREATE_DETACHED
      && !start_control_thread())
   {
     return EAGAIN;
   }
   
   struct invoke_data *data=new struct invoke_data;
   data->start_routine=start_routine;
   data->arg=arg;
   sem_init(&data->start_sync_mutex,1); // mutex needs semaphore inited with 1
   data->detached=(attr->detachstate==PTHREAD_CREATE_DETACHED);
   // suspend the thread and continue below
   sem_wait(&data->start_sync_mutex);
   *thread=run_thread((int)thread_func,(int)data,attr->stacksize);
   if(*thread<0) {
     sem_post(&data->start_sync_mutex);
     delete data;
     *thread=NO_THREAD;
     return EAGAIN;
   }

   sem_wait(&fd_mutex);
   int this_thread=get_task_id();
   vector<struct fd_list> *v=&fd_owner_list[this_thread];
   // transfer ownership of file descriptors to the new thread
   for(int i=0;i<v->size();i++) (*v)[i].set_owner_fct((*v)[i].fd,*thread);
   fd_owner_list.erase(this_thread);
   sem_post(&fd_mutex);
   // start execution of the thread
   sem_post(&data->start_sync_mutex);
   return 0;
}

int pthread_join(pthread_t thread, void **retval)
{
   // wait for thread termination
   int ret=thread_join(thread,0);
   if(ret!=0) ret=errno;
   sem_wait(&invoke_mutex);
   // read return value for this thread from the return value map
   if(retval) *retval=(void *)join_retval_map[thread];
   join_retval_map.erase(thread);
   sem_post(&invoke_mutex);
   return ret;
}

int pthread_detach(pthread_t thread)
{
   sem_wait(&invoke_mutex);
   // get invoke_data created by pthread_create()
   struct invoke_data *data=(struct invoke_data *)invoke_data_map[thread];
   if(!data) {
      sem_post(&invoke_mutex);
      return ESRCH; // no thread with the thread ID could be found
   }
   if(data->detached) {
      sem_post(&invoke_mutex);
      return EINVAL; // thread is not a joinable thread
   }
   data->detached=true;
   sem_post(&invoke_mutex);
   return 0;
}

static unsigned char cancelstate;
int pthread_setcancelstate(int state, int *oldstate)
{
   if(oldstate) *oldstate=cancelstate;
   cancelstate=state;
   return 0;
}

static unsigned char canceltype;
int pthread_setcanceltype(int type, int *oldtype)
{
   if(oldtype) *oldtype=canceltype;
   canceltype=type;
   return 0;
}

int pthread_cancel(pthread_t thread)
{
  if(cancelstate==PTHREAD_CANCEL_DISABLE) return EINVAL; // cancellation disabled
  if(thread_cancel(thread)<0) return ESRCH; // no thread could be found corresponding to that specified by the given thread ID
  return 0;
}

void pthread_exit(void *retval)
{
  // cleanup invokation data and thread specific data
  thread_clean_up(retval);
  _exit(0);
}

pthread_t pthread_self(void)
{
  return get_task_id();
}

int pthread_equal(pthread_t t1, pthread_t t2)
{
   return t1==t2?1:0;
}

int pthread_mutexattr_init(pthread_mutexattr_t *attr)
{
  if(!attr) return EINVAL;
  attr->type=PTHREAD_MUTEX_NORMAL;
  return 0;
}

int pthread_mutexattr_destroy(pthread_mutexattr_t *attr)
{
   return 0;
}

int pthread_mutexattr_settype(pthread_mutexattr_t *attr, int type)
{
  if(!attr) return EINVAL;
  attr->type=type;
  return 0;
}

int pthread_mutexattr_gettype(const pthread_mutexattr_t *attr, int *type)
{
  if(!attr || !type) return EINVAL;
  *type=attr->type;
  return 0;
}

static int mutex_priv_init(pthread_mutex_t *m)
{
   // PTHREAD_MUTEX_INITIALIZER
   if(!m->priv || m->priv==(struct pthread_mutex_priv *)1) {
     static const pthread_mutex_priv p1={{0,0,0},{PTHREAD_MUTEX_NORMAL},0,-1,0};
     m->priv=(struct pthread_mutex_priv *)malloc(sizeof(struct pthread_mutex_priv));
     if(!m->priv) return ENOMEM;
     memcpy(m->priv,&p1,sizeof(p1));
   }
   // PTHREAD_RECURSIVE_MUTEX_INITIALIZER
   else if(m->priv==(struct pthread_mutex_priv *)2) {
      static const pthread_mutex_priv p2={{0,0,0},{PTHREAD_MUTEX_RECURSIVE},0,-1,0};
      m->priv=(struct pthread_mutex_priv *)malloc(sizeof(struct pthread_mutex_priv));
      if(!m->priv) { m->priv=(struct pthread_mutex_priv *)2; return ENOMEM; }
      memcpy(m->priv,&p2,sizeof(p2));
   }
   // PTHREAD_ERRORCHECK_MUTEX_INITIALIZER
   else if(m->priv==(struct pthread_mutex_priv *)3) {
      static const pthread_mutex_priv p3={{0,0,0},{PTHREAD_MUTEX_ERRORCHECK},0,-1,0};
      m->priv=(struct pthread_mutex_priv *)malloc(sizeof(struct pthread_mutex_priv));
      if(!m->priv) { m->priv=(struct pthread_mutex_priv *)3; return ENOMEM; }
      memcpy(m->priv,&p3,sizeof(p3));
   }
   return 0;
}

int pthread_mutex_init(pthread_mutex_t *mutex, const pthread_mutexattr_t *mutexattr)
{
   static const pthread_mutex_t init=PTHREAD_MUTEX_INITIALIZER;
   if(!mutex) return EINVAL;
   memcpy(mutex,&init,sizeof(init));
   // create and init private data
   int r=mutex_priv_init(mutex);
   if(r) return r;
   if(mutexattr) mutex->priv->attr=*mutexattr;
   return 0;
}

int pthread_mutex_destroy(pthread_mutex_t *mutex)
{
   if(!mutex) return EINVAL;
   if(mutex->priv>(struct pthread_mutex_priv *)3) free(mutex->priv);
   mutex->priv=0;
   return 0;
}

static int mutex_lock(pthread_mutex_t *mutex, unsigned char try_lock)
{
  if(!mutex) return EINVAL;

  // enter critical section
  if(sem_wait(&mutex->critical)) return EINVAL;

  // create/init private mutex data (if not done)
  int r=mutex_priv_init(mutex);
  if(r) {
     if(sem_post(&mutex->critical)) return EINVAL;
     return r;
  }

  // if current thread is the owner
  if(mutex->priv->owner==get_task_id()) {
     // recursive mode
     if(mutex->priv->attr.type==PTHREAD_MUTEX_RECURSIVE) {
        if(mutex->priv->resursive_cnt==(unsigned)~0) {
           if(sem_post(&mutex->critical)) return EINVAL;
           // the mutex could not be acquired because the maximum number of recursive locks for mutex has been exceeded
           return EAGAIN;
        }
        mutex->priv->resursive_cnt++;
        if(sem_post(&mutex->critical)) return EINVAL;
        return 0;
     }
     if(sem_post(&mutex->critical)) return EINVAL;
     return EDEADLK; // the current thread already owns the mutex
  }

  // another thread owns the mutex or we have already some waiters
  if(mutex->priv->owner>NO_THREAD || mutex->priv->waiting_cnt>0) {
    // mode PTHREAD_MUTEX_ERRORCHECK is the same as pthread_mutex_trylock() and PTHREAD_MUTEX_NORMAL
    if(try_lock || mutex->priv->attr.type==PTHREAD_MUTEX_ERRORCHECK) {
       if(sem_post(&mutex->critical)) return EINVAL;
       return EBUSY; // the mutex could not be acquired because it was already locked
    }
    // else: we have a new waiter from now

    // too much waiters
    if(mutex->priv->waiting_cnt==(unsigned)~0) {
       sem_post(&mutex->critical);
       return EINVAL;
    }

    mutex->priv->waiting_cnt++;

    // leave critical section
    if(sem_post(&mutex->critical)) return EINVAL;

    // block the waiter
    if(sem_wait(&mutex->priv->blocking)) return EINVAL;

    // enter critical section
    if(sem_wait(&mutex->critical)) return EINVAL;

    mutex->priv->waiting_cnt--;
  }

  mutex->priv->owner=get_task_id(); // set the new owner and lock the mutex

  // leave critical section
  if(sem_post(&mutex->critical)) return EINVAL;
  return 0;
}

int pthread_mutex_lock(pthread_mutex_t *mutex)
{
  return mutex_lock(mutex,0);
}

int pthread_mutex_trylock(pthread_mutex_t *mutex)
{
  return mutex_lock(mutex,1);
}

int pthread_mutex_unlock(pthread_mutex_t *mutex)
{
  if(!mutex) return EINVAL;

  // enter critical section
  if(sem_wait(&mutex->critical)) return EINVAL;

  // create/init private mutex data (if not done)
  int r=mutex_priv_init(mutex);
  if(r) {
     if(sem_post(&mutex->critical)) return EINVAL;
     return r;
  }

  // check if this thread is the owner
  if(mutex->priv->owner==NO_THREAD || mutex->priv->owner!=get_task_id()) {
     if(sem_post(&mutex->critical)) return EINVAL;
     return EPERM; // the current thread does not own the mutex
  }
  // recursive mode
  if(mutex->priv->attr.type==PTHREAD_MUTEX_RECURSIVE && mutex->priv->resursive_cnt>0) {
     mutex->priv->resursive_cnt--;
     if(sem_post(&mutex->critical)) return EINVAL;
     return 0;
  }
  // awake one waiter (if any)
  if(mutex->priv->waiting_cnt>0) {
     if(sem_post(&mutex->priv->blocking)) {
        sem_post(&mutex->critical);
        return EINVAL;
     }
  }

  // reset owner and unlock the mutex
  mutex->priv->owner=NO_THREAD;
  // leave critical section
  if(sem_post(&mutex->critical)) return EINVAL;
  return 0;
}


int pthread_condattr_init(pthread_condattr_t *attr)
{
   if(!attr) return EINVAL;
   attr->clk_id=CLOCK_REALTIME;
   return 0;
}

int pthread_condattr_destroy(pthread_condattr_t *attr)
{
   return 0;
}

static int cond_priv_init(pthread_cond_t *c)
{
   // PTHREAD_COND_INITIALIZER
   if(!c->priv) {
     static const pthread_cond_priv p={0,{CLOCK_REALTIME,0}};
     c->priv=(struct pthread_cond_priv *)malloc(sizeof(struct pthread_cond_priv));
     if(!c->priv) return ENOMEM;
     memcpy(c->priv,&p,sizeof(p));
   }
   return 0;
}

int pthread_cond_init(pthread_cond_t *cond, const pthread_condattr_t *attr)
{
  static const pthread_cond_t init=PTHREAD_COND_INITIALIZER;
  if(!cond) return EINVAL;
  memcpy(cond,&init,sizeof(init));
  // create and init private data
  int r=cond_priv_init(cond);
  if(r) return r;
  if(attr) cond->priv->attr=*attr;
  return 0;
}

int pthread_cond_destroy(pthread_cond_t *cond)
{
  if(!cond) return EINVAL;
  if(cond->priv) free(cond->priv);
  cond->priv=0;
  return 0;
}

int pthread_condattr_getclock(const pthread_condattr_t *attr, clockid_t *clock_id)
{
   if(!attr || !clock_id) return EINVAL;
   *clock_id=attr->clk_id;
   return 0;
}

int pthread_condattr_setclock(pthread_condattr_t *attr, clockid_t clock_id)
{
   if(!attr) return EINVAL;
   attr->clk_id=clock_id;
   return 0;
}

int pthread_cond_wait(pthread_cond_t *cond, pthread_mutex_t *mutex)
{
  return pthread_cond_timedwait(cond,mutex,NULL);
}

int pthread_cond_timedwait(pthread_cond_t *cond, pthread_mutex_t *mutex, const struct timespec *abstime)
{
  int r, task_id, evt, iTimer=-1;
  
  if(!cond || !mutex) return EINVAL;
  
  // enter critical section
  if(sem_wait(&cond->critical)) return EINVAL;
  
  // create/init private condition data (if not done)
  r=cond_priv_init(cond);
  if(r) {
    if(sem_post(&cond->critical)) return EINVAL;
    return r;
  }
  // create vector storing the waiters on condition
  if(!cond->priv->waiters) cond->priv->waiters=new vector<WaitNode *>;

  // unlock the incomming mutex to allow signalizing thread to call pthread_cond_signal(), pthread_cond_broadcast()
  r=pthread_mutex_unlock(mutex);
  if(r) {
    if(sem_post(&cond->critical)) return EINVAL;
    return r;
  }
  
  WaitNode *wn=new WaitNode;
  
  // register the new waiter
  cond->priv->waiters->push_back(wn);

  // leave critical section to allow waking threads
  if(sem_post(&cond->critical)) goto lock_error;
  
  if(abstime) add_timeout(wn,abstime,cond->priv->attr.clk_id);
  
  sem_wait(&wn->wait);
  
  if(abstime) remove_timeout(wn);
      
  // enter critical section
  if(sem_wait(&cond->critical)) goto lock_error;
  
  // remove from the waiters register
  for(unsigned i=0;i<cond->priv->waiters->size();i++) {
     if((*cond->priv->waiters)[i]==wn) {
        cond->priv->waiters->erase(cond->priv->waiters->begin()+i);
        break;
     }
  }
  
  if(cond->priv->waiters->size()==0) {
    delete cond->priv->waiters;
    cond->priv->waiters=0;
  }
  
  // leave critical section
  if(sem_post(&cond->critical)) goto lock_error;
  
  r=pthread_mutex_lock(mutex);

  if(wn->timeout) r=ETIMEDOUT;
  delete wn;

  if(r) return r;
  
  return 0; 
  
lock_error:
  pthread_mutex_lock(mutex);
  delete wn;
  return EINVAL;
}

int pthread_cond_signal(pthread_cond_t *cond)
{ 
  if(sem_wait(&cond->critical)) return EINVAL;

  // check initialization
  if(!cond->priv || !cond->priv->waiters) {
    sem_post(&cond->critical);
    return EINVAL;
  }
  if(cond->priv->waiters->size()>0) {
     WaitNode *wn=(*cond->priv->waiters)[0];
     sem_post(&wn->wait);
  }
  if(sem_post(&cond->critical)) return EINVAL;
  return 0;
}

int pthread_cond_broadcast(pthread_cond_t *cond)
{
  if(sem_wait(&cond->critical)) return EINVAL;
  // check initialization
  if(!cond->priv || !cond->priv->waiters) {
    sem_post(&cond->critical);
    return EINVAL;
  }
  for(unsigned i=0;i<cond->priv->waiters->size();i++) {
     WaitNode *wn=(*cond->priv->waiters)[i];
     sem_post(&wn->wait);
  }
  if(sem_post(&cond->critical)) return EINVAL;
  return 0;
}

extern "C" int pthread_once(pthread_once_t *once_control, void (*init_routine)(void))
{
  if(!once_control) return EINVAL;
  // once control was inited by macro (e.g. PTHREAD_ONCE_INIT)
  if(INIT_REQUIRED(once_control)) {  // first check (for better performance)
    sem_wait(&init_mutex);
    if(INIT_REQUIRED(once_control)) { // atomic check to ensure that once control is inited at once
      if(init_routine) init_routine();
      once_control->inited=1;
    }
    sem_post(&init_mutex);
  }
  return 0;
}

extern "C" int pthread_key_create(pthread_key_t *key, void (*destructor)(void*))
{
  if(!key) return EINVAL;
  sem_wait(&key_mutex);
  
  pthread_key_t new_key;
  for(new_key=0;new_key<PTHREAD_KEYS_MAX;new_key++) {
    if(key_map.find(new_key)==key_map.end()) break;
  }
  if(new_key>=PTHREAD_KEYS_MAX) {
    sem_post(&key_mutex); // the total number of keys per process {PTHREAD_KEYS_MAX} has been exceeded
    return EAGAIN;
  }
  key_map[new_key]=destructor;
  *key=new_key;
  sem_post(&key_mutex);
  return 0;
}

int pthread_key_delete(pthread_key_t key)
{
  sem_wait(&key_mutex);
  map<pthread_key_t, void(*)(void*)>::iterator it;
  
  if((it=key_map.find(key))==key_map.end()) {
    sem_post(&key_mutex);
    return EINVAL; // the key value is invalid
  }
  key_map.erase(it);
  
  /* Application is responsible to free thread specifing data, if a key is deleted and no such automatic
   * cleanup of is done by pthread_key_delete(), see: http://linux.die.net/man/3/pthread_key_delete.
   * The function just removes the corresponding entries from the map storing values for all threads. */
  map<int,map<pthread_key_t, void *> >::iterator it_value;  
  // for all threads storing thread specifing data
  for(it_value=value_map.begin();it_value!=value_map.end();++it_value) {
    map<pthread_key_t, void *>::iterator it_key_values;
    if((it_key_values=it_value->second.find(key))!=it_value->second.end()) {    
      it_value->second.erase(it_key_values); // remove the entry for the key
    }
  }
  sem_post(&key_mutex);
  return 0; 
}

void *pthread_getspecific(pthread_key_t key)
{
  void *ret=0;
  sem_wait(&key_mutex);
  map<pthread_key_t,destructor_func_t>::iterator it_key;
  if((it_key=key_map.find(key))==key_map.end()) {
    sem_post(&key_mutex);
    return 0; // the key value is invalid
  }
  map<int,map<pthread_key_t, void *> >::iterator it_value;
  if((it_value=value_map.find(get_task_id()))!=value_map.end()) {
    ret=(it_value->second)[key];
  }
  sem_post(&key_mutex);
  return ret;
}


int pthread_setspecific(pthread_key_t key, const void *value)
{
  sem_wait(&key_mutex);
  map<pthread_key_t,destructor_func_t>::iterator it_key;
  if((it_key=key_map.find(key))==key_map.end()) {
    sem_post(&key_mutex);
    return EINVAL; // the key value is invalid
  }
  map<int,map<pthread_key_t, void *> >::iterator it_value;
  if((it_value=value_map.find(get_task_id()))==value_map.end()) {
    map<pthread_key_t, void *> new_map;
    new_map[key]=(void *)value;
    value_map[get_task_id()]=new_map;
  }
  else {  
    map<pthread_key_t, void *>::iterator it_key_values;
    if((it_key_values=it_value->second.find(key))==it_value->second.end()) {    
      (it_value->second)[key]=(void *)value;
    }
    else {
      // call destructor, if a value already exists for the key
      destructor_func_t destr_func_ptr=it_key->second;
      if(destr_func_ptr) destr_func_ptr(it_key_values->second);
      it_key_values->second=(void *)value;
    }
  }
  sem_post(&key_mutex);
  return 0;
}

//////////////////////////////// control thread for timeout handling and detach thread mode ////////////////////////////////

struct TimeoutNode {
  WaitNode *wn;
  const timespec *ts;

  TimeoutNode(WaitNode *w, const timespec *t) { wn=w; ts=t; }
  bool operator<(const TimeoutNode &o) const {
     return ts->tv_sec<o.ts->tv_sec || (ts->tv_sec==o.ts->tv_sec && ts->tv_nsec<o.ts->tv_nsec);
  }
  bool operator==(const TimeoutNode &o) const {
    return ts->tv_sec==o.ts->tv_sec && ts->tv_nsec==o.ts->tv_nsec;
  }
};

static vector<TimeoutNode> rt_timeout_list; // real-time timeout list
static vector<TimeoutNode> mt_timeout_list; // monotonic-time timeout list

static void *control_thread(void *)
{
   //printf("control_thread started\n");
   int timer=-1;
   
   while(1) {
      wait_evt(EVT_USER|EVT_TIMER);
   
      if(timer!=-1) {
         clr_timer(timer);
         timer=-1;
      }

      sem_wait(&control_mutex);

      timespec now;
      long next_tout=-1; // next timeout set for the timer

      // timer handling for real-time timeout list
      clock_gettime(CLOCK_REALTIME,&now);
      while(rt_timeout_list.size()) {
         TimeoutNode &tn=rt_timeout_list[0];
         long tout=(tn.ts->tv_sec-now.tv_sec)*1000+(tn.ts->tv_nsec-now.tv_nsec)/1000000;
         //printf("control_thread tout=%ld\n",tout);
         if(tout<=0) {
            // timeout has occurred -> signal timeout
            tn.wn->timeout=true;
            //printf("control_thread post\n");
            sem_post(&tn.wn->wait);
            rt_timeout_list.erase(rt_timeout_list.begin());
            continue; // get next entry from list
         }
         // set timeout as the very next timeout
         next_tout=tout;
         break; // only one time may be set
      }

      // timer handling for monotonic-time timeout list
      clock_gettime(CLOCK_MONOTONIC,&now);
      while(mt_timeout_list.size()) {
         TimeoutNode &tn=mt_timeout_list[0];
         long tout=(tn.ts->tv_sec-now.tv_sec)*1000+(tn.ts->tv_nsec-now.tv_nsec)/1000000;
         //printf("control_thread tout=%ld\n",tout);
         if(tout<=0) {
            // timeout has occurred -> signal timeout
            tn.wn->timeout=true;
            //printf("control_thread post\n");
            sem_post(&tn.wn->wait);
            mt_timeout_list.erase(mt_timeout_list.begin());
            continue; // get next entry from list
         }
         // set timeout as the very next timeout (if not already set by realtime list processing)
         if(next_tout==-1 || tout<next_tout) next_tout=tout;
         break; // only one time may be set
      }
      
      // set the next timeout (if any)
      if(next_tout>=0) timer=set_timer(next_tout,EVT_TIMER);

      // detached thread handling
      for(int i=0;i<detached_list.size();i++) {
        thread_join(detached_list[i],0);
        // printf("joinded thread=%d\n",detached_list[i]);
      }
      detached_list.clear();
      
      sem_post(&control_mutex);
   }
   return 0;
}

/* starts the controller thread and returns true for success.
 * The function returns true, if the controller thread was already started. */
static bool start_control_thread()
{ 
   if(controlthread!=NO_THREAD) return true; // optimization
   sem_wait(&control_mutex);
   if(controlthread==NO_THREAD) {
     controlthread=run_thread((int)control_thread,0,8*1024);
     if(controlthread<0) controlthread=NO_THREAD;
   }
   sem_post(&control_mutex);
   return (controlthread!=NO_THREAD);
}

static bool add_timeout(WaitNode *wn, const struct timespec *abstime, clockid_t clock_id)
{
   //printf("add_timeout\n");
   
   // start thread controlling timers (and detached threads)
   if(!start_control_thread()) return false;
   
   sem_wait(&control_mutex);

   TimeoutNode tn(wn,abstime);

   // add timeout node to timeout list
   vector<TimeoutNode> *timeout_list=&rt_timeout_list;          // real-time timeout list
   if(clock_id==CLOCK_MONOTONIC) timeout_list=&mt_timeout_list; // monotonic-time timeout list
   unsigned i;
   for(i=0;i<timeout_list->size() && (*timeout_list)[i]<tn;i++);
   timeout_list->insert(timeout_list->begin()+i,tn);
      
   sem_post(&control_mutex);

   //printf("post_user_event\n");
   post_user_event(controlthread,1);
   return true;
}

static void remove_timeout(WaitNode *wn)
{
   //printf("remove_timeout\n");
   if(controlthread==NO_THREAD) return;
    
   sem_wait(&control_mutex);

   for(unsigned i=0;i<rt_timeout_list.size();i++) {
      if(rt_timeout_list[i].wn==wn) {
         rt_timeout_list.erase(rt_timeout_list.begin()+i);
         break;
      }
   }
   for(unsigned i=0;i<mt_timeout_list.size();i++) {
      if(mt_timeout_list[i].wn==wn) {
         mt_timeout_list.erase(mt_timeout_list.begin()+i);
         break;
      }
   }
   sem_post(&control_mutex);

   //printf("post_user_event\n");
   post_user_event(controlthread,1);
}

