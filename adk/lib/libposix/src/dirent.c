#include <svc.h>
#include <stdlib.h>
#include <string.h>
#include "dirent.h"

DIR *opendir(const char *name)
{
   DIR *dir;
   int mode;

   dir=(DIR *)malloc(sizeof(DIR));
   if(!dir) return 0;
   strcpy(dir->dirent.d_name,name);

   if (dir_get_first(dir->dirent.d_name) != 0) {
      free(dir);
      return 0;
   }

   dir->dirent.d_size=dir_get_file_size(dir->dirent.d_name);
   mode=dir_get_attributes(dir->dirent.d_name);
   if(mode & 0x01) dir->dirent.d_mode=0555; else dir->dirent.d_mode=0777;

   dir->dirent_filled=1; // dirent already contains an entry
   return dir;
}

int closedir(DIR *dir)
{
   if(!dir) return -1;
   free(dir);
   return 0;
}

struct dirent *readdir(DIR *dir)
{
   int found=1;
   unsigned char mode;

   if(!dir) return 0;
   if(dir->dirent_filled) {
      dir->dirent_filled=0;
      return &dir->dirent;
   }

   if (dir_get_next(dir->dirent.d_name) != 0) found=0;

   if(!found) return 0;

   dir->dirent.d_size=dir_get_file_size(dir->dirent.d_name);
   mode=dir_get_attributes(dir->dirent.d_name);
   if(mode & 0x01) dir->dirent.d_mode=0555; else dir->dirent.d_mode=0777;

   return &dir->dirent;
}
