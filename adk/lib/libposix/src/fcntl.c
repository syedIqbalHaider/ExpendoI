#include <svc.h>
#include <errno.h>
#include "fcntl.h"

int fcntl(int fildes, int cmd, struct flock *fl)
{
//	dbprintf("fcntl fildes=%d cmd=%d\n", fildes, cmd);
	
	if(fl == NULL)
		return -1;

//	dbprintf("fcntl l_type=%d, l_start=%d l_whence=%d l_len=%d\n", fl->l_type, fl->l_start, fl->l_whence, fl->l_len);

	switch(fl->l_whence)
	{
		case SEEK_SET:
			break;
		default:
		case SEEK_END:  //Not implemented
		case SEEK_CUR:  //Not implemented
			// dbprintf("fcntl l_whence not implemented\n");
			return -1;
			
	}
	

	if(cmd == F_SETLK)
	{
//		dbprintf("fcntl F_SETLK\n");
		
		if(fl->l_type == F_WRLCK || fl->l_type == F_RDLCK)
		{
			if(lock(fildes, fl->l_start, fl->l_len) == -1)
			{
//				dbprintf("fcntl F_SETLK lock failed\n");
				return -1;
			}	
		}
		else if(fl->l_type == F_UNLCK)
		{
			if(unlock(fildes, fl->l_start, fl->l_len) == -1)
			{
//				dbprintf("fcntl F_SETLK unlock failed\n");
				return -1;
			}	
		}
		else
			return -1;
	}	
	else if(cmd == F_GETLK)
	{
//		dbprintf("fcntl F_GETLK\n");
		
		if(lock(fildes, fl->l_start, fl->l_len) == -1)
		{
//			dbprintf("fcntl F_GETLK lock failed\n");
			return -1;
		}	
		else if(unlock(fildes, fl->l_start, fl->l_len) == -1)
		{
//			dbprintf("fcntl F_GETLK unlock failed\n");
			return -1;
		}	

		fl->l_type = F_UNLCK;
	}
	else
	{
		return -1;
	}
		
//	dbprintf("fcntl OK\n");
	return 0;
}
