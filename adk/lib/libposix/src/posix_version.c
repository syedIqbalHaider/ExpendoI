#include "posix_version.h"

const char posix_version[]="$VER: Verix Posix library 01.05 " __DATE__ " " __TIME__;

const char *getPosixLibraryVersion(void) { return posix_version; }
