#include <svc.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>

#include <str2int.h>

int ntocs( char *dest_buf, const char *src_buf)
{
   int i;

   // DCS #1045 : Included following 2 lines
	if ( (dest_buf == NULL) || (src_buf == NULL) ) 
	   return -1;

      /* get length of current string add one for count */
   i = strlen(src_buf)+1;

       /* max counted string length is 255 */
   if ((i > 255) || (i == 0))
       dest_buf[0] = 0x00;
   else
   {
       strcpy(dest_buf+1, src_buf);
       dest_buf[0] = i;
   }

   /* return the count length */
   return (dest_buf [0]);

}

int str2int(const char *buffer)
{
   int sign=0;
	int num;
	
			/* incoming string must not be longer than 40 characters */
   char  temp[42];
     
   // DCS #1045 : Included following 2 lines
   if ( buffer == NULL) 
	   return -1;

  if (strlen(buffer) > 40)
     return (0);

  if (*buffer == '-')
     sign= '-';

			/* SVC_2INT requires counted strings */
  ntocs(temp, buffer);
 
  num = SVC_2INT (temp);

  return(sign?-num:num);

}

