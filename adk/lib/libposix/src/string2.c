#include <string.h>
#include <stdlib.h>
#include "string2.h"

char *strdup(const char *s)
{
   char *mem;
   if(!s) return 0;
   mem=(char *)malloc(strlen(s)+1);
   if(!mem) return 0;
   strcpy(mem,s);
   return mem;
}

char *strndup(const char *s, unsigned n)
{
   char *mem;
   unsigned l;
   
   if(!s) return 0;
   for(l=0;l<n && s[l];l++);
   
   mem=(char *)malloc(l+1);
   if(!mem) return 0;
   memcpy(mem,s,l);
   mem[l]=0;
   return mem;
}
