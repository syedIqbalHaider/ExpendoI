#include <svc.h>
#include <string.h>
#include "sys/statvfs.h"

int statvfs(const char *path, struct statvfs *buf)
{
    struct fs_size fs;
    memset(&fs, 0, sizeof(struct fs_size));
    dir_get_sizes(path, &fs);
    
    memset(buf, 0, sizeof(struct statvfs));
    buf->f_bsize = 1;
    buf->f_bavail = buf->f_bfree = fs.Avail;
    return 0;
}

