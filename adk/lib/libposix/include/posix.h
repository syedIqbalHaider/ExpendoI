/** @file posix.h
 *
 *  Wrapper function for POSIX compatibility. The functions behave as defined
 *  in the POSIX standard but due to the operating system functions are not high-performance.
 *
 */
#if defined VERIXV || defined _VRXEVO

#ifndef POSIX_H
#define POSIX_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdarg.h>

#include "time2.h"     // provide provide time functions (e.g. time(), localtime.h, strptime(), gmtime_r(), localtime_r() etc.) with posix.h
#include "dirent.h"    // provide opendir(), closedir(), readdir() etc. with posix.h
#include "string2.h"   // provide strdup() and strndup() with posix.h
#include "unistd.h"    // provide getcwd(), rmdir(), mkdir(), chdir() etc. with posix.h
#include "sys/stat.h"  // provide stat() etc. with posix.h
#include "sys/types.h" // provide size_t data type

#define PATH_MAX 148 /**< maximum path length */

/** opens a file and associates a stream with it.
 *  By including posix.h function posix_fopen2() replaces fopen() of Verix SDK.
 *  posix_fopen2() opens the specified file with fopen() of SDK and additionally calls 
 *  setvbuf() to enable buffering for the stream. This causes a significant improved 
 *  performance for file operations, e.g. fwrite(), fread() etc.
 * \param[in] filename name of the file to be opened.
 * \param[in] mode access mode to open the file. For supported modes please refer to 
 *                 Verix eVo SDK documentation.
 * \return FILE pointer if successful. Otherwise, NULL is returned.
 */
#  undef fopen
#  define fopen posix_fopen2
FILE *posix_fopen2(const char *filename, const char *mode);

#ifdef __cplusplus
}
#endif

#endif  // #ifndef POSIX_H

#else   // #if defined VERIXV || defined _VRXEVO

#ifdef __GNUC__
#   include <stdio.h>
#   include <stdlib.h>
#   include <sys/types.h>
#   include <sys/stat.h>
#   include <dirent.h>
#   include <unistd.h>
#   include <string.h>
#   include <errno.h>
#endif

#endif

