#if defined VERIXV || defined _VRXEVO

#ifndef UNISTD_LIBC_H
#define UNISTD_LIBC_H

#include <stdlib.h> // svc.h requires stdlib.h, therefore we must include this before svc.h
#include <svc.h>    // provide rmdir(), chdir() with unistd.h
#include <time.h>
#include "sys/types.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Values for the second argument to access.
   These may be OR'd together.  */
#define	R_OK	4		/* Test for read permission.  */
#define	W_OK	2		/* Test for write permission.  */
#define	X_OK	1		/* Test for execute permission.  */
#define	F_OK	0		/* Test for existence.  */

int access(const char *pathname, int /* mode */);

pid_t getpid(void);

int fsync(int file_descriptor);

int fchown( int fd, uid_t owner, gid_t group );

int unlink(const char *path);

/** set the size of a file.
 * \param[in] fd file descriptor. The file must have been opened for writing.
 * \param[in] length new size of the file
 * \retval 0 OK
 * \retval -1 error
 * \note On Artema files will not be extended by ftruncate().
 * \note libutil_ext supports ::errno which is set to one of the following codes in case of error:
 *   - ::EINVAL
 *   - ::ENOENT
 */
int ftruncate(int fildes, off_t length);

/** set the size of a file.
 * \param[in] filename name of the file
 * \param[in] length new size of the file
 * \retval 0 OK
 * \retval -1 error
 * \note On Artema files will not be extended by truncate().
 * \note libutil_ext supports ::errno which is set to one of the following codes in case of error:
 *   - ::ENOENT
 */
int truncate(const char *filename, off_t length);

struct utimbuf {
	time_t actime;		/* Access time */
	time_t modtime;		/* Modification time */
};

int utimes(const char *path, const struct utimbuf *times);

unsigned int sleep(unsigned int);
int usleep(useconds_t usec);
uid_t getuid (void);
uid_t geteuid(void);

/** copy an absolute pathname of the current working
 * directory to the array pointed to by \a buf, which is of length \a size.
 * \param[out] buf  buffer for the pathname
 * \param[in] size size of \a buf in bytes
 * \return In case of success \a buf is returned, else NULL
 */
#undef getcwd   // undef getcwd from VRXSDK
#define getcwd posix_getcwd
char *posix_getcwd(char *buf, size_t size);


/** create a directory
 * \param[in] pathname name of the directory
 * \param[in] mode file permissions. These is ignored on Verix but should be set to 0777 for portability.
 * \retval 0 OK
 * \retval -1 error
 */
#undef mkdir   // undef mkdir from VRXSDK
#define mkdir posix_mkdir
int posix_mkdir(const char *pathname, unsigned mode);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif // UNISTD_LIBC_H

#else // #if defined VERIXV || defined _VRXEVO

#ifdef __GNUC__
#include_next <unistd.h>
#endif

#endif
