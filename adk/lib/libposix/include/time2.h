// Note: time2.h is Verix only. Linux has implemented these functions as part of time.h
#if defined VERIXV || defined _VRXEVO

#ifndef _TIME2_H_
#define _TIME2_H_

#include <time.h> // provide other Verix time functions time(), localtime() etc. with time2.h

#ifdef __cplusplus
extern "C" {
#endif

struct timespec {
  time_t tv_sec; /* Seconds since 00:00:00, 1 January 1900 (Verix time base returned by SDK time())
                  * timezone according environment variable DST */
  long tv_nsec;  /* Additional nanoseconds since tv_sec */
}; 

typedef int clockid_t;

#ifndef CLOCK_REALTIME
#define CLOCK_REALTIME           0 // realtime clock
#define CLOCK_MONOTONIC          1 // momotonic clock
#define CLOCK_PROCESS_CPUTIME_ID CLOCK_MONOTONIC // dummy, use monotonic clock
#define CLOCK_THREAD_CPUTIME_ID  CLOCK_MONOTONIC // dummy, use monotonic clock
#endif

/** converse function to strftime() and converts the character string
 *  pointed to by \a buf to values which are stored in the tm structure 
 *  pointed to by \a tm, using the format specified by \a fm.
 * \param[out] buf buffer used as destination of convertion
 * \param[in]  fmt format string
 * \param[in]  tm struct containing data for source of conversion
 * \return     pointer to parameter \a buf for success, else NULL
 */ 
char *strptime(const char *buf, const char *fmt, struct tm *tm);

/** convert unix time to broken down representation 
 * \param[in] timep pointer to unix time stamp 
 * \param[out] result buffer that is used to store the result of the conversion
 * \return broken down time representation.
 * \note gmtime_r() uses gmtime() and function will be reentrant as long as 
 *       gmtime_r() is used only and gmtime() is not called by another thread.
 */     
struct tm *gmtime_r(const time_t *timep, struct tm *result);

/** convert unix time to broken down representation (reentrant version of localtime())
 * \param[in] timep pointer to unix time stamp 
 * \param[out] result buffer that is used to store the result of the conversion
 * \return broken down time representation. 
 * \note localtime_r() uses localtime() and function will be reentrant as long as 
 *       localtime_r() is used only and localtime() is not called by another thread.
 */     
struct tm *localtime_r(const time_t *timep, struct tm *result);

/** read the clock time (seconds since 1900 (Verix time base returned by SDK time()). 
 * \param[in] clk_id clock type, e.g. CLOCK_REALTIME (a dummy on Verix)
 * \param[out] res timespec structure for returning the time
 * \return 0 in case of success, or -1 in case of failure.
 */
int clock_gettime(clockid_t clk_id, struct timespec *res);

#ifdef __cplusplus
}
#endif

#endif // #ifndef _TIME2_H_

#else  // #if defined VERIXV || defined _VRXEVO

#include <time.h>

#endif
