#ifndef _POSIX_VERSION_H_
#define _POSIX_VERSION_H_

#if defined VERIXV || defined _VRXEVO

#ifdef __cplusplus
extern "C" {
#endif

/** returns the version string for Versix POSIX library
 * \return version string
 */
const char *getPosixLibraryVersion(void);

#ifdef __cplusplus
} /* extern "C" */
#endif

#else // #if defined VERIXV || defined _VRXEVO

#define getPosixLibraryVersion "$VER: unused"

#endif

#endif // _POSIX_VERSION_H_
