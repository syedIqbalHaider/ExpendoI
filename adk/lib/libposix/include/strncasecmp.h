// header file to ensure compatibility for EMEA applications
#ifndef _STRNCASECMP_H_
#define _STRNCASECMP_H_

#include "strings.h"
// ensure compatibility for EMEA applications (using strnicmp() instead of strncasecmp())
#define strnicmp strncasecmp

#endif
