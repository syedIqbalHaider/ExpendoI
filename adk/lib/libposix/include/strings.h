#if defined VERIXV || defined _VRXEVO

#ifndef _STRINGS_H_
#define _STRINGS_H_

#include "sys/types.h"

#ifdef __cplusplus
extern "C" {
#endif

int strcasecmp (const char *s1, const char *s2);
int strncasecmp (const char *s1, const char *s2, size_t n);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* _STRINGS_H_ */

#else  // #if defined VERIXV || defined _VRXEVO

#ifdef __GNUC__
#include_next<strings.h>
#endif

#endif
