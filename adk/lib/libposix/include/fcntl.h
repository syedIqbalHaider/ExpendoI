#if defined VERIXV || defined _VRXEVO

#ifndef _FCNTL_H_
#define _FCNTL_H_

#include <svc.h>
#include "sys/types.h"

#ifdef __cplusplus
extern "C" {
#endif


#define	F_GETLK		7		/* get record locking information */
#define	F_SETLK		8		/* set record locking information */


/* record locking flags (F_GETLK, F_SETLK, F_SETLKW) */
#define	F_RDLCK		1		/* shared or read lock */
#define	F_UNLCK		2		/* unlock */
#define	F_WRLCK		3		/* exclusive or write lock */


/*
 * Advisory file segment locking data type -
 * information passed to system by user
 */
struct flock {
	short	l_type;		/* lock type: read/write, etc. */
	short	l_whence;	/* type of l_start */
	off_t	l_start;	/* starting offset */
	off_t	l_len;		/* len = 0 means until end of file */
	pid_t	l_pid;		/* lock owner */
};

//int	open (const char *, int, ...);
//int	creat (const char *, mode_t);
int	fcntl (int, int, struct flock*);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif // #ifndef _FCNTL_H_

#else  // #if defined VERIXV || defined _VRXEVO

#ifdef __GNUC__
#include_next <fcntl.h>
#endif

#endif
