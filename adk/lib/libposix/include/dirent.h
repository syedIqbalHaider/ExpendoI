#if defined VERIXV || defined _VRXEVO

#ifndef LIBC_DIRENT_H
#define LIBC_DIRENT_H

#ifdef __cplusplus
extern "C" {
#endif

#define FILENAME_SIZE 32+1

/** Directory entry */
struct dirent {
   char d_name[FILENAME_SIZE]; /**< filename or directory name */
   unsigned long d_size; /**< nonportable extension, contains the same as stat.st_size */
   unsigned long d_mode; /**< nonportable extension, contains the same as stat.st_mode */
};

typedef struct {
   struct dirent dirent;
   short dirent_filled;
} DIR;

/** open a directory stream corresponding to the directory name,
 * and returns a pointer to the directory stream. The stream is
 * positioned at the first entry in the directory.
 * \param[in] name name of the directory
 * \return Directory handle or NULL in case of error
 */
DIR *opendir(const char *name);

/** close the directory stream associated with dir.
 * \param[in] dir directory handle
 * \retval 0 OK
 * \retval -1 error
 */
int closedir(DIR *dir);

/** returns a pointer to a dirent structure representing
 * the next directory entry in the directory  stream  pointed  to  by  dir.   It
 * returns NULL on reaching the end-of-file or if an error occurred.
 * The  data  returned  by  readdir()  may be overwritten by subsequent calls to
 * readdir() for the same directory stream.
 * \param[in] dir directory handle
 * \return Pointer to dirent structure containing the next entry or NULL if reaching EOF.
 */
struct dirent *readdir(DIR *dir);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif // LIBC_DIRENT_H

#else // #if defined VERIXV || defined _VRXEVO

#ifdef __GNUC__
#include_next <dirent.h>
#endif

#endif
