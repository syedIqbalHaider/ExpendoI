#if defined VERIXV || defined _VRXEVO

#ifndef PTHREAD_VX_H
#define PTHREAD_VX_H

#include <stddef.h>
#include <svc.h>
#include "sys/time.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef int pthread_t;

enum {
/*
 * pthread_attr_{get,set}detachstate
 */
  PTHREAD_CREATE_JOINABLE       = 0,  /* Joinable thread, (default) */
  PTHREAD_CREATE_DETACHED       = 1,  /* Detached thread */

/*
 * pthread_setcancelstate paramters
 */
  PTHREAD_CANCEL_ENABLE         = 0,  /* Default */
  PTHREAD_CANCEL_DISABLE        = 1,

/*
 * pthread_setcanceltype parameters
 */
  PTHREAD_CANCEL_ASYNCHRONOUS   = 0,
  PTHREAD_CANCEL_DEFERRED       = 1, /* Default, !!! NOTE: Currently NOT SUPPORTED on Verix !!! */
};

//
// POSIX limits
//
#define PTHREAD_KEYS_MAX   64

/*
 * Mutex types.
 */
enum
{
  /* Compatibility with LinuxThreads */
  PTHREAD_MUTEX_FAST_NP,
  PTHREAD_MUTEX_RECURSIVE_NP,
  PTHREAD_MUTEX_ERRORCHECK_NP,
  // PTHREAD_MUTEX_TIMED_NP = PTHREAD_MUTEX_FAST_NP,     // currently not supported for Verix
  // PTHREAD_MUTEX_ADAPTIVE_NP = PTHREAD_MUTEX_FAST_NP,
  /* For compatibility with POSIX */
  PTHREAD_MUTEX_NORMAL = PTHREAD_MUTEX_FAST_NP,
  PTHREAD_MUTEX_RECURSIVE = PTHREAD_MUTEX_RECURSIVE_NP,
  PTHREAD_MUTEX_ERRORCHECK = PTHREAD_MUTEX_ERRORCHECK_NP,
  PTHREAD_MUTEX_DEFAULT = PTHREAD_MUTEX_NORMAL
};

typedef struct {
  int stacksize;   /**< stack size of the thread, default: 64k */
  unsigned char detachstate; /**< detach state of the thread */
} pthread_attr_t ;

typedef struct {
  unsigned char type; /**< mutex type, e.g. PTHREAD_MUTEX_NORMAL, PTHREAD_MUTEX_RECURSIVE ...  */
} pthread_mutexattr_t;


/** private data for structure pthread_mutex_t */
struct pthread_mutex_priv;

/** mutex variable structure */
typedef struct {
  sem_t critical;                  /**< semaphore used for protection of mutex variable */
  struct pthread_mutex_priv *priv; /**< private mutex data */
} pthread_mutex_t;

/** static initializers for mutex variable */
#define PTHREAD_MUTEX_INITIALIZER            {{0,0,1},(struct pthread_mutex_priv *)1}
#define PTHREAD_RECURSIVE_MUTEX_INITIALIZER  {{0,0,1},(struct pthread_mutex_priv *)2}
#define PTHREAD_ERRORCHECK_MUTEX_INITIALIZER {{0,0,1},(struct pthread_mutex_priv *)3}

typedef struct {
   clockid_t clk_id; /**< clock type used for pthread_cond_timedwait() */
   void *reserved;
} pthread_condattr_t;


/** private data for structure pthread_mutex_t */
struct pthread_cond_priv;

/** condition variable structure */
typedef struct {
  sem_t critical;                 /**< semaphore used for protection of condition variable */
  struct pthread_cond_priv *priv; /**< private condition data */
} pthread_cond_t;

/** static initializers for condition variable */
#define PTHREAD_COND_INITIALIZER {{0,0,1},0}

typedef struct {
  int inited; /**< init flag */
} pthread_once_t;

#define PTHREAD_ONCE_INIT {-1}

typedef int pthread_key_t;

int pthread_attr_init(pthread_attr_t *attr);
int pthread_attr_destroy(pthread_attr_t *attr);
int pthread_attr_setstacksize(pthread_attr_t *attr, int stacksize);
int pthread_attr_getstacksize(pthread_attr_t *attr, size_t *stacksize);
int pthread_attr_setdetachstate(pthread_attr_t *attr, int detachstate);
int pthread_attr_getdetachstate(pthread_attr_t *attr, int *detachstate);
int pthread_create(pthread_t * thread, 
                   const pthread_attr_t * attr,
                   void * (*start_routine)(void *), 
                   void *arg);
int pthread_join(pthread_t thread, void **retval);
int pthread_detach(pthread_t thread);
int pthread_cancel(pthread_t thread);
int pthread_setcancelstate(int state, int *oldstate);
int pthread_setcanceltype(int type, int *oldtype);
// void pthread_testcancel(void);  currently not supported
void pthread_exit(void *retval);
pthread_t pthread_self(void);
int pthread_equal(pthread_t t1, pthread_t t2);
int pthread_mutexattr_init(pthread_mutexattr_t *attr);
int pthread_mutexattr_destroy(pthread_mutexattr_t *attr);
int pthread_mutexattr_settype(pthread_mutexattr_t *attr, int type);
int pthread_mutexattr_gettype(const pthread_mutexattr_t *attr, int *type);
int pthread_mutex_init(pthread_mutex_t *mutex, const pthread_mutexattr_t *mutexattr);
int pthread_mutex_destroy(pthread_mutex_t *mutex);
int pthread_mutex_lock(pthread_mutex_t *mutex);
int pthread_mutex_trylock(pthread_mutex_t *mutex);
int pthread_mutex_unlock(pthread_mutex_t *mutex);
int pthread_condattr_init(pthread_condattr_t *attr);
int pthread_condattr_destroy(pthread_condattr_t *attr);
int pthread_cond_init(pthread_cond_t *cond, const pthread_condattr_t *attr);
int pthread_cond_destroy(pthread_cond_t *cond);
int pthread_condattr_getclock(const pthread_condattr_t *attr, clockid_t *clock_id);
int pthread_condattr_setclock(pthread_condattr_t *attr, clockid_t clock_id);
int pthread_cond_timedwait(pthread_cond_t *cond, pthread_mutex_t *mutex, const struct timespec *abstime);
int pthread_cond_wait(pthread_cond_t *cond, pthread_mutex_t *mutex);
int pthread_cond_signal(pthread_cond_t *cond);
int pthread_cond_broadcast(pthread_cond_t *cond);
int pthread_once(pthread_once_t *once_control, void (*init_routine)(void));
int pthread_key_create(pthread_key_t *key, void (*destructor)(void*));
int pthread_key_delete(pthread_key_t key);
void *pthread_getspecific(pthread_key_t key);
int pthread_setspecific(pthread_key_t key, const void *value);

// non-standard pthread functions, we additionally require for Verix

/** This function adds a handle \a fd to an internal list of file descriptors, which
 *  ownership are automatically transfered to the thread that is created with next call
 *  of pthread_create(). After calling pthread_create() the list is cleared.
 *  @note This function is required on Verix, since file descriptors are owned by thread
 *        that has called open(). By adding a fd to list, function pthread_create() will 
 *        transfer the ownership with set_owner() to the new thread. Transfering ownership
 *        will fail, if the function and pthread_create() are called by different threads
 *        or if the thread invoking pthread_create() is not the owner of the descriptor.
 *
 * \param[in] fd file descriptor, which ownership should be transfered to the new thread 
 */    
void pthread_add_to_fd_transition_list(int fd);

/** Same as pthread_add_to_fd_transition_list() used for sockets
 *
 * \param[in] sockfd socket file descriptor, which ownership should be transfered to the new thread 
 */    
void pthread_add_to_sock_transition_list(int sockfd);


#ifdef __cplusplus
} /* extern "C" */
#endif

#endif // PTHREAD_VX_H

#else // #if defined VERIXV || defined _VRXEVO

#ifdef __GNUC__
#include_next <pthread.h>
#endif

#endif
