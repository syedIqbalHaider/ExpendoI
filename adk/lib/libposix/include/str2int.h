#ifndef EMEAV_LIBC_STR2INT_H
#define EMEAV_LIBC_STR2INT_H

#if defined VERIXV || defined _VRXEVO

#ifdef __cplusplus
extern "C" {
#endif

int str2int(const char *buffer);

#ifdef __cplusplus
} /* extern "C" */
#endif

#else // #if defined VERIXV || defined _VRXEVO

#include <stdlib.h>
#define str2int atoi

#endif

#endif /* EMEAV_LIBC_STR2INT_H */

