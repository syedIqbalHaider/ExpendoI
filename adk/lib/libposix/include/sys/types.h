#if defined VERIXV || defined _VRXEVO

//
// types.h
//
// Basic type definitions
//
// Copyright (C) 2002 Michael Ringgaard. All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 
// 1. Redistributions of source code must retain the above copyright 
//    notice, this list of conditions and the following disclaimer.  
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.  
// 3. Neither the name of the project nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
// OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
// OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF 
// SUCH DAMAGE.
// 
#ifndef SYS_TYPES_H
#define SYS_TYPES_H

typedef unsigned short u_short;
typedef unsigned char u_char;
typedef unsigned long u_long;
typedef unsigned int u_int;

typedef u_char uchar;
#ifndef __VERIXVHEADERS_H // avoid collision with VerixV_HEADERS.h
typedef u_int  uint;
#endif

#ifndef _SIZE_T_DEFINED
#define _SIZE_T_DEFINED
#ifndef _SVC_NET_H // avoid collision with svc_net.h
typedef unsigned int size_t;
#endif
#endif

#ifndef _SSIZE_T_DEFINED
#define _SSIZE_T_DEFINED
#ifndef __VERIXVHEADERS_H // avoid collision with VerixV_HEADERS.h
typedef int ssize_t;
#endif
#endif

#ifndef _SUSECONDS_T_DEFINED
#define _SUSECONDS_T_DEFINED
typedef long suseconds_t;
#endif

#ifndef _USECONDS_T_DEFINED
#define _USECONDS_T_DEFINED
typedef unsigned int useconds_t;
#endif

#ifndef _INO_T_DEFINED
#define _INO_T_DEFINED
typedef unsigned int ino_t;
#endif

#ifndef _DEV_T_DEFINED
#define _DEV_T_DEFINED
typedef unsigned int dev_t;
#endif

#ifndef _MODE_T_DEFINED
#define _MODE_T_DEFINED
typedef unsigned short mode_t;
#endif

#ifndef _NLINK_T_DEFINED
#define _NLINK_T_DEFINED
typedef unsigned short nlink_t;
#endif

#ifndef _UID_T_DEFINED
#define _UID_T_DEFINED
typedef unsigned short uid_t;
#endif

#ifndef _GID_T_DEFINED
#define _GID_T_DEFINED
typedef unsigned short gid_t;
#endif

#ifndef _BLKNO_T_DEFINED
#define _BLKNO_T_DEFINED
typedef unsigned int blkno_t;
#endif

#ifndef _LOFF_T_DEFINED
#define _LOFF_T_DEFINED
typedef long loff_t;
#endif

#ifndef _OFF64_T_DEFINED
#define _OFF64_T_DEFINED
typedef __int64 off64_t;
#endif

#ifndef _OFF_T_DEFINED
#define _OFF_T_DEFINED
#ifdef LARGEFILES
typedef off64_t off_t;
#else
typedef loff_t off_t;
#endif
#endif

#ifndef _HANDLE_T_DEFINED
#define _HANDLE_T_DEFINED
typedef int handle_t;
#endif

#ifndef _TID_T_DEFINED
#define _TID_T_DEFINED
typedef unsigned long tid_t;
#endif

#ifndef _PID_T_DEFINED
#define _PID_T_DEFINED
typedef unsigned long pid_t;
#endif

#ifndef _HMODULE_T_DEFINED
#define _HMODULE_T_DEFINED
typedef void *hmodule_t;
#endif

#ifndef _TLS_T_DEFINED
#define _TLS_T_DEFINED
typedef unsigned long tls_t;
#endif

#ifndef _FSBLKCNT_DEFINED
#define _FSBLKCNT_DEFINED
typedef unsigned long fsblkcnt_t;
#endif

#ifndef _FSFILCNT_DEFINED
#define _FSFILCNT_DEFINED
typedef unsigned long fsfilcnt_t;
#endif


//#ifndef wchar_t
//typedef unsigned short wchar_t;
//#endif
/*
 *	@type ulong | unsigned long.
 */
#ifndef _ulong_defined_
#define _ulong_defined_
typedef unsigned long ulong;
#endif

typedef int port_t;
typedef int err_t;

#ifndef _T_SYSTIME
#define _T_SYSTIME
typedef __int64 systime_t;
#endif

#ifndef NULL
#ifdef __cplusplus
#define NULL    0
#else
#define NULL    ((void *)0)
#endif
#endif

#ifndef _ONLY_STD_TYPES

#ifndef osapi
#ifdef OS_LIB
#define osapi __declspec(dllexport)
#else
#ifdef KERNEL
#define osapi
#else
#define osapi __declspec(dllimport)
#endif
#endif
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE  1
#endif

#ifndef F_OK
#define F_OK 0
#endif

#ifndef IPC_STAT
#define  IPC_STAT   0 
#endif

#ifndef IPC_CREAT
#define  IPC_CREAT   0 
#endif

#ifndef IPC_NOWAIT
#define  IPC_NOWAIT   0 
#endif

#ifndef IPC_RMID
#define  IPC_RMID   0 
#endif

#ifndef IPC_EXCL
#define  IPC_EXCL   0 
#endif

#ifndef NOHANDLE
#define NOHANDLE ((handle_t) -1)
#endif

#ifndef _key_t_defined_
#define _key_t_defined_
typedef	long key_t;
#endif

#ifndef _byte_defined_
#define _byte_defined_
typedef unsigned char byte;
#endif

#ifndef _BYTE_defined_
#define _BYTE_defined_
typedef unsigned char BYTE;
#endif

#ifndef _WORD_defined_
#define _WORD_defined_
typedef unsigned short WORD;
#endif

#ifndef _DWORD_defined_
#define _DWORD_defined_
typedef unsigned long DWORD;
#endif

#ifndef _QWORD_defined_
#define _QWORD_defined_
typedef unsigned __int64 QWORD;
#endif

typedef	int		blksize_t;	/* used for block sizes */

typedef long		blkcnt_t;	/* count of file blocks */  
typedef u_long		fsblkcnt_t;	/* count of file system blocks */  
typedef u_long		fsfilcnt_t;	/* count of files */ 

#endif // #ifndef _ONLY_STD_TYPES

#endif // #ifndef SYS_TYPES_H


#else // #if defined VERIXV || defined _VRXEVO

#ifdef __GNUC__
#include_next<sys/types.h>
#endif

#endif


