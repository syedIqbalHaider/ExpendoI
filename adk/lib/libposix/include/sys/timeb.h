#if defined VERIXV || defined _VRXEVO

#ifndef _SYS_TIMEB_H
#define _SYS_TIMEB_H

#include <time.h>

#ifdef __cplusplus
extern "C" {
#endif

/* structure returned by the ftime() function.  */
struct timeb
{
  time_t time;		/* Seconds since epoch, as from time().  */
  unsigned short int millitm;	/* Additional milliseconds.  */
  short int timezone;		/* Minutes west of GMT.  */
  short int dstflag;		/* Nonzero if Daylight Savings Time used.  */
};

/** This function returns the current time as seconds and milliseconds
 *  since 1900 (Verix time base returned by SDK time()). The time is
 *  returned in parameter \a tp. 
 * \param[out] tp structure storing the current time
 * \return 0 in case of success, or -1 in case of failure.
 * \note Parameters timezone and dstflag of struct timeb are not supported.
 */
int ftime(struct timeb *tp);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif // #ifndef _SYS_TIMEB_H

#else  // #if defined VERIXV || defined _VRXEVO

#ifdef __GNUC__
#include_next<sys/timeb.h>
#endif

#endif


