#if defined VERIXV || defined _VRXEVO

#ifndef LIBC_SYS_TIME_H_
#define LIBC_SYS_TIME_H_

#include <time.h>
#include "../time2.h" // provide clock_gettime() for compatibility with old libc

#ifdef __cplusplus
extern "C"
{
#endif

#if !defined(timeval) && !defined(_SVC_NET_H) && !defined(_USE_TIMEVAL_FROM_SVC_NET_H_)
struct timeval
{
    unsigned long tv_sec;    /* Seconds since 00:00:00, 1 January 1900 (Verix time base returned by SDK time())
                              * timezone according environment variable DST */
    unsigned long tv_usec;   /* Additional micro seconds since tv_sec */
};
#endif

struct timezone {
    int tz_minuteswest; /* minutes W of Greenwich */
    int tz_dsttime;     /* type of dst correction */
};

/** read the time of day (seconds since 1900 (Verix time base returned by SDK time()). 
 * \param[out] tv timeval structure for returning the time
 * \param[in] tz not supported, should always be NULL
 * \return 0 in case of success, or -1 in case of failure.
 */
int gettimeofday(struct timeval *tv, struct timezone * tz);

/** set the time of day. Setting microseconds is not supported, tv_usec is ignored.
 *  As gettimeofday() returns, this functions awaits seconds since 1900.
 * \param[out] tv timeval structure for returning the time
 * \param[in] tz not supported, should always be NULL
 * \return 0 in case of success, or -1 in case of failure.
 */
int settimeofday(const struct timeval *tv , const struct timezone *tz);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* LIBC_SYS_TIME_H_ */

#else  // #if defined VERIXV || defined _VRXEVO

#ifdef __GNUC__
#include_next<sys/time.h>
#endif

#endif


