#if defined VERIXV || defined _VRXEVO

#ifndef SYS_VFSTAT_H
#define SYS_VFSTAT_H

#include <sys/types.h>

#ifndef _VFSTAT_DEFINED
#define _VFSTAT_DEFINED

struct statvfs {
    unsigned long  f_bsize;    /* file system block size */
    unsigned long  f_frsize;   /* fragment size */
    fsblkcnt_t     f_blocks;   /* size of fs in f_frsize units */
    fsblkcnt_t     f_bfree;    /* # free blocks */
    fsblkcnt_t     f_bavail;   /* # free blocks for unprivileged users */
    fsfilcnt_t     f_files;    /* # inodes */
    fsfilcnt_t     f_ffree;    /* # free inodes */
    fsfilcnt_t     f_favail;   /* # free inodes for unprivileged users */
    unsigned long  f_fsid;     /* file system ID */
    unsigned long  f_flag;     /* mount flags */
    unsigned long  f_namemax;  /* maximum filename length */
};
#endif

#ifndef ST_RDONLY_DEFINED
#define ST_RDONLY_DEFINED
    enum
    {
        ST_RDONLY = 1,
        ST_NOSUID = 2,
    };
#endif

#ifdef  __cplusplus
extern "C" {
#endif

int statvfs(const char *path, struct statvfs *buf);

#ifdef  __cplusplus
}
#endif

#endif // #ifndef SYS_VFSTAT_H

#else  // #if defined VERIXV || defined _VRXEVO

#ifdef __GNUC__
#include_next <sys/statvfs.h>
#endif

#endif

