// header file to ensure compatibility for EMEA applications
#ifndef _STRCASECMP_H_
#define _STRCASECMP_H_

#include "strings.h"
// ensure compatibility for EMEA applications (using stricmp() instead of strcasecmp())
#define stricmp strcasecmp

#endif
