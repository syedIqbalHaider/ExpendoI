#if defined VERIXV || defined _VRXEVO

#ifndef _STRING2_H_
#define _STRING2_H_

#include <string.h> // provide other Verix string functions strcpy() etc. with string2.h

#ifdef __cplusplus
extern "C" {
#endif

/** return a pointer to a new string which is a duplicate of the string \a s. Memory
 * for the new string is obtained with malloc(), and can be freed with free().
 * \param[in] s string to be duplicated
 * \return pointer to the duplicated string, or NULL if insufficient memory was available
 */
char *strdup(const char *s);

/** return a pointer to a new string which is a duplicate of the first \a n characters
 * of string \a s. Memory for the new string is obtained with malloc(), and can be freed with free().
 * \param[in] s string to be duplicated
 * \param[in] n maximum number of characters to be copied.
 * \return pointer to the duplicated string, or NULL if insufficient memory was available
 */
char *strndup(const char *s, unsigned n);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* _STRING2_H_ */

#else // #if defined VERIXV || defined _VRXEVO

#include <string.h>

#endif
