#ifndef LIBIPCPACKET_H
#define LIBIPCPACKET_H

#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 * 
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file         ipcpacket.hpp
 *
 * @author       Kamil Pawlowski
 * 
 * @brief        Helper module, wrapper for IPC library, allowing more easy communication between internal modules
 *
 *
 * @remarks      This file should be compliant with Verifone EMEA R&D C Coding   Standard 1.0.0 
 */


#include <string>
#include <vector>

#include <libipc/ipc.h>

#include <tlv-lite/TLVIterator.hpp>
#include <tlv-lite/ConstData.h>
#include <tlv-lite/TagLenVal.hpp>
#include <tlv-lite/SafeBuffer.hpp>


namespace com_verifone_ipcpacket
{

    /// Definition of tag data class - may be used to pass data to our class
    class TagData
    {
        public:
            TagData(const std::string & tag, const std::string & value): 
                m_tag(tag), m_value(value) {}
            TagData(const char * pTag, unsigned char tagLen, const char * pValue, int len): 
                m_tag(pTag, tagLen), m_value(pValue, len) {}
            TagData(const std::string & tag): 
                m_tag(tag) {}
                //{ m_value.clear(); }
            TagData(const char * pTag, unsigned char tagLen): 
                m_tag(pTag, tagLen) {}
                //{ m_value.clear(); }
            TagData(const com_verifone_TLVLite::ConstData & tag): 
                m_tag(static_cast<const char *>(tag.getBuffer()), tag.getSize()) {}
                //{ m_value.clear(); }
            TagData(const com_verifone_TLVLite::ConstData & tag, const com_verifone_TLVLite::ConstData & val): 
                m_tag(static_cast<const char *>(tag.getBuffer()), tag.getSize()), m_value(static_cast<const char *>(val.getBuffer()), val.getSize()) {}
            
            bool isEmpty() const { return m_tag.size() == 0 && m_value.size() == 0; }
            static TagData getEmpty() { return TagData(std::string(""), std::string("")); }
        protected:
            std::string m_tag;
            std::string m_value;
        private:
            friend class IPCPacket;
    };

    enum IPC_errors
    {
        IPCPACKET_SUCCESS = 0,
        IPCPACKET_ERROR = -1,
        IPCPACKET_NOTFOUND = -2,
        IPCPACKET_APPNAME_NOT_DEFINED = -3,
        IPCPACKET_TLV_EMPTY = -4
    };


    class IPCPacket
    {
        public:
            IPCPacket(const std::string & appName = "");

            virtual ~IPCPacket()
            {
                ClearData();
            }

            const std::string &  getAppName() {return toAppName;}

            const std::string &  getSenderName() {return senderName;}


            void                 setCmdCode(const unsigned char newCmdCode)
                                    { cmdCode = newCmdCode; }
            unsigned char        getCmdCode(void) {return cmdCode;}
            int                  setAppName(const std::string & appName);

            int                  addTag(const std::string & tag, const std::string & value, bool add_empty_tags = true);
            int                  addTag(const char * const pTag, const unsigned char tagLen, const char * const *pValue, const int len, bool add_empty_tags = true );
            int                  addTag(const com_verifone_TLVLite::ConstData & tag, const com_verifone_TLVLite::ConstData & val, bool add_empty_tags = true );

            int                  addTags(const TagData tags[], bool add_empty_tags = true);
            int                  addTags(const std::string & buf, bool add_empty_tags = true);
            int                  addTags(const char * const buf, const size_t len, bool add_empty_tags = true);

            int                  startTemplate(const std::string & tag);
            int                  endTemplate();


            int                  addBuffer(const std::string & buf);
            int                  addBuffer(const char * const buf, const size_t len);

            int                  getTag(const char * const pTag, const unsigned char pTagLen, char **ppValue, int &len ) const;
            int                  getTag(const std::string & tag, std::string & value) const;
            int                  getTag(const com_verifone_TLVLite::ConstData & tag, com_verifone_TLVLite::ConstData &val) const;
            int                  getTag(const com_verifone_TLVLite::ConstData & tag, char **ppValue, int &len) const;

            /**
             * Returns values for tag that may be included multiple times in the message;
             * Values are returned in order they appear in the message
             * @param tag - tag to be found
             * @param val - vector - placeholder for results - ale values are APPENDED to the vector
             */
            int                  getTags(const com_verifone_TLVLite::ConstData & tag, std::vector<com_verifone_TLVLite::ConstData> &val) const;

            void                 getBuffer(std::string & buf) const
                                    { buf = internalBuffer; }
            unsigned char        getCmdCode() const 
                                    { return cmdCode; }


            int                  send( );
            // receive without analysing, just plain stuff
            int                  receive( long timeout = 0 );
            // regular receive - checks default result tag (DF30) and returns it value
            int                  receiveCheckResult( int & moduleExitCode, long timeout = 0 );


            void                 showList();

        protected:
            unsigned char        cmdCode;
            std::string          toAppName;
            std::string          senderName;
            std::string          internalBuffer;
            std::string          internalTemplate;
            std::string          template_tag;
            bool                 isConnected;

            void ClearData();

    private: // noncopyable
            IPCPacket(IPCPacket & )
                { }
    };


}

#endif /* LIBIPCPACKET_H */
