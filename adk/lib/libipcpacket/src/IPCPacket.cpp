
#include <errno.h>


#include <liblog/logsys.h>

#include <libipcpacket/ipcpacket.hpp>
#include <libipc/ipc.h>
#include <libpml/pml.h>

// Private tags
#include <libtags/tags.h>

#include <tlv-lite/ValueConverter.hpp>

using namespace com_verifone_TLVLite;

namespace com_verifone_ipcpacket
{

//IPCPacket::IPCPacket(const std::string & appName): toAppName(appName), isConnected(false)
IPCPacket::IPCPacket(const std::string & appName): isConnected(false)
{
    ClearData();
    if (appName.size()) setAppName(appName);
}

int IPCPacket::setAppName(const std::string & appName)
{
	int result = IPCPACKET_ERROR;
	if(appName.size())
	{
    	result = com_verifone_ipc::connect_to(appName);
    	dlog_msg("Connection result %d", result);
    	// if connection succeeds or we're already connected
    	if (result == com_verifone_ipc::IPC_SUCCESS || result == com_verifone_ipc::IPC_ERROR_INVALID_PARAMETERS)
    	{
        	isConnected = true;
        	toAppName = appName;
        	result = IPCPACKET_SUCCESS;
    	}
    	else
    	{
        	isConnected = false;
        	result = IPCPACKET_ERROR;
    	}
	}
	else
	{
		result = IPCPACKET_APPNAME_NOT_DEFINED;
	}
    return result;
}

void IPCPacket::ClearData()
{
    cmdCode = 0;
    toAppName.clear();
    internalBuffer.clear();
    internalBuffer.append(1, ' '); // reserve space for command code
    // Do NOT close the connection, we will most likely reuse it in the future!
}

int IPCPacket::addTag(const std::string & tag, const std::string & value, bool add_empty_tags /* = true */)
{
    return addTag(ConstData(tag.data(), tag.size()), ConstData(value.data(), value.size()), add_empty_tags);
}

int IPCPacket::addTag(const char * const pTag, const unsigned char tagLen, const char * const *pValue, const int len, bool add_empty_tags /* = true */ )
{
    return addTag(ConstData(pTag, tagLen), ConstData(pValue, len), add_empty_tags);
}

int IPCPacket::addTag(const com_verifone_TLVLite::ConstData & tag, const com_verifone_TLVLite::ConstData & val, bool add_empty_tags /* = true */ )
{
    if (tag.getSize() == 0)
    {
        dlog_alert("Invalid tag!");
        return IPCPACKET_ERROR;
    }
    if (val.getSize() == 0 && !add_empty_tags)
    {
        dlog_alert("Empty tag!");
        return IPCPACKET_ERROR;
    }
    TagLenVal tlv(tag, val);
    // Optimise to avoid memory fragmentation for most cases... 
    int result = IPCPACKET_SUCCESS;
    char data[100];
    if (tlv.getLength() >= sizeof(data))
    {
        dlog_msg("--- ALLOC ---");
        char *tmp = new char [tlv.getLength()+1];
        assert(tmp);
        SafeBuffer buf(tmp, tlv.getLength()+1);
        buf.append(tlv);
        if (!buf.isOverflow())
        {
        	if(template_tag.size() > 0)
        	{
            	internalTemplate.append(reinterpret_cast<const char *>(buf.getBuffer()), buf.getLength());
        	}
			else
			{
            	internalBuffer.append(reinterpret_cast<const char *>(buf.getBuffer()), buf.getLength());
			}
        }
        else
        {
            dlog_error("Overflow!");
            result = IPCPACKET_ERROR;
        }
        delete [] tmp;
    }
    else
    {
        SafeBuffer buf(data, sizeof(data));
        buf.append(tlv);
        if (!buf.isOverflow())
        {
			if(template_tag.size() > 0)
			{
				internalTemplate.append(reinterpret_cast<const char *>(buf.getBuffer()), buf.getLength());
			}
			else
			{
				internalBuffer.append(reinterpret_cast<const char *>(buf.getBuffer()), buf.getLength());
			}
        }
        else
        {
            dlog_error("Overflow!");
            result = IPCPACKET_ERROR;
        }
    }
    return result;
}


int IPCPacket::addTags(const TagData tags[], bool add_empty_tags /* = true */)
{
    int i = 0;
    int result = 0;
    while (!tags[i].isEmpty())
    {
        result = addTag(tags[i].m_tag, tags[i].m_value, add_empty_tags);
        if (result != IPCPACKET_SUCCESS) break;
        ++i;
    }
    return result;
}

int IPCPacket::addTags(const char * const buf, const size_t len, bool add_empty_tags /* = true */)
{
    std::string localBuf(buf, len);
    return addTags(localBuf, add_empty_tags);
}

int IPCPacket::addTags(const std::string & buf, bool add_empty_tags /* = true */)
{
    if (!add_empty_tags)
    {
        // parse buf first, check if there are empty tags and ignore them
        TLVListWrapper tlvList(buf.c_str(), buf.size());
        for (TLVIterator it = tlvList.begin(); it != tlvList.end(); ++it)
        {
            TagLenVal tlv = *it;
            if (tlv.getData().getSize() == 0)
            {
                return IPCPACKET_TLV_EMPTY;
            }
        }
    }
    internalBuffer.append(buf);
    return IPCPACKET_SUCCESS;
}

int IPCPacket::addBuffer(const std::string & buf)
{
    //internalBuffer.append(buf);
    internalBuffer.assign(buf);
    return IPCPACKET_SUCCESS;
}
int IPCPacket::addBuffer(const char * const buf, const size_t len)
{
    std::string localBuf(buf, len);
    return addBuffer(localBuf);
}

/*
int IPCPacket::getTag_(const char * const pTag, const unsigned char pTagLen, char **ppValue, int &len ) const
{
	
    ConstData localTag(pTag, pTagLen);
    ConstData localVal(ConstData::getInvalid());
    int result = getTag(localTag, localVal);
	dlog_msg("Getting tag %X%X%X ret: %d", pTag[0], pTag[1], pTag[2], result);
    if (result == IPCPACKET_SUCCESS)
    {
        *ppValue = const_cast<char *>(static_cast<const char *>(localVal.getBuffer()));
        len = localVal.getSize();
    }
    return result;
}
*/

int IPCPacket::startTemplate(const std::string & tag)
{
    if(tag.size() > 0)
    {
        template_tag = tag;
        internalTemplate.clear();
        return 0;
    }
    // Invalid parameters
    return -1;
}

int IPCPacket::endTemplate()
{
    std::string tmpl_tag = template_tag;
    template_tag.clear();
    int ret = addTag(tmpl_tag, internalTemplate);
    internalTemplate.clear();

    return ret;
}


int IPCPacket::getTag(const char * const pTag, const unsigned char pTagLen, char **ppValue, int &len ) const
{
    ConstData localTag(pTag, pTagLen);
    ConstData localVal(ConstData::getInvalid());
    int result = getTag(localTag, localVal);
    dlog_msg("Getting tag %02X%02X%02X ret: %d", pTag[0], pTag[1], pTag[2], result);
    if (result == IPCPACKET_SUCCESS)
    {
        *ppValue = const_cast<char *>(static_cast<const char *>(localVal.getBuffer()));
        len = localVal.getSize();
    }
    return result;
}

int IPCPacket::getTag(const std::string & tag, std::string & value) const
{
    ConstData localTag(tag.c_str(), tag.size());
    ConstData localVal(ConstData::getInvalid());
    int result = getTag(localTag, localVal);
    if (result == IPCPACKET_SUCCESS)
    {
        value.clear();
        value.append(static_cast<const char *>(localVal.getBuffer()), localVal.getSize());
    }
    return result;
}

int IPCPacket::getTag(const com_verifone_TLVLite::ConstData & tag, char **ppValue, int &len) const
{
    com_verifone_TLVLite::ConstData localVal(com_verifone_TLVLite::ConstData::getInvalid());

    int result = getTag(tag, localVal);

    *ppValue = const_cast<char *>(static_cast<const char *>(localVal.getBuffer()));
    len = localVal.getSize();

    return result;
}

void IPCPacket::showList()
{
    ConstData localVal(ConstData::getInvalid());
    TLVListWrapper tlvList(internalBuffer.data()+1, internalBuffer.size()-1);
    int i=0;
    dlog_msg("Processing internalBuffer of size %d (%X:%X:%X)", internalBuffer.size(), internalBuffer[0], internalBuffer[1], internalBuffer[2]);
    for (TLVIterator it = tlvList.begin(); it != tlvList.end(); ++it)
    {
        i++;
        TagLenVal tlv = *it;
        ConstData localTag = tlv.getTag();
        if (getTag(localTag, localVal) == IPCPACKET_SUCCESS)
        {
            char * tag = const_cast<char *>(static_cast<const char *>(localTag.getBuffer()));
            if(tag != NULL)
            {
                dlog_msg("%d. TAG: %02X%02X%02X", i, tag[0], tag[1], tag[2]);
            }
        }
    }
}

int IPCPacket::getTag(const com_verifone_TLVLite::ConstData & tag, com_verifone_TLVLite::ConstData &val) const
{
    int result = IPCPACKET_NOTFOUND;
    TLVListWrapper tlvList(internalBuffer.data()+1, internalBuffer.size()-1);
    for (TLVIterator it = tlvList.begin(); it != tlvList.end(); ++it)
    {
        TagLenVal tlv = *it;
        if (tlv.getTag() == tag)
        {
            val = tlv.getData();
            result = IPCPACKET_SUCCESS;
            break;
        }
    }
    return result;
}

int IPCPacket::getTags(const com_verifone_TLVLite::ConstData & tag, std::vector<com_verifone_TLVLite::ConstData> &val) const
{
    int result = IPCPACKET_NOTFOUND;
    TLVListWrapper tlvList(internalBuffer.data()+1, internalBuffer.size()-1);
    for (TLVIterator it = tlvList.begin(); it != tlvList.end(); ++it)
    {
        TagLenVal tlv = *it;
        if (tlv.getTag() == tag)
        {
            val.push_back(tlv.getData());
            result = IPCPACKET_SUCCESS;
        }
    }
    return result;
}



int IPCPacket::send()
{
    if (!isConnected)
    {
        dlog_error("We are not connected!");
        return IPCPACKET_APPNAME_NOT_DEFINED;
    }
    // check if there is anything to be sent

	//checking if template is opened
	if(template_tag.size() > 0)
	{
		//if so, we must close template first
		endTemplate();
		
	}

    if ( cmdCode == 0 )
    {
        dlog_msg("invalid command");
        return IPCPACKET_NOTFOUND;
    }
    else
    {
        internalBuffer[0] = cmdCode;
    }

    // now send via IPC to destination task
    dlog_msg("Sending data to '%s'", toAppName.c_str());
    dlog_hex( internalBuffer.c_str(), internalBuffer.size(), "IPCPacket::send" );
    
    int ipc_ret = com_verifone_ipc::send( internalBuffer, toAppName );
    if (ipc_ret != com_verifone_ipc::IPC_SUCCESS )
    {
        dlog_msg( "ERROR %d sending msg to %s", ipc_ret, toAppName.c_str() );
        return IPCPACKET_ERROR;
    }

    // get the response
    return IPCPACKET_SUCCESS;
}

int IPCPacket::receive( long timeout /* = 0 */ )
{
    if (timeout)
    {
        com_verifone_pml::eventset_t pml_events;
        int eventRes = com_verifone_pml::event_wait( com_verifone_ipc::get_events_handler(toAppName)(), pml_events, timeout);
        if (eventRes <= 0)
        {
            dlog_alert("Timeout or error (%d)!", eventRes);
            return IPCPACKET_ERROR;
        }
        /*if (com_verifone_ipc::check_pending(toAppName) != com_verifone_ipc::IPC_SUCCESS)
        {
            dlog_error("Event but nothing pending from %s!", toAppName.c_str());
            return IPCPACKET_ERROR;
        }*/
    }

    int ipc_ret = com_verifone_ipc::receive( internalBuffer, toAppName, senderName );
    if( ipc_ret != com_verifone_ipc::IPC_SUCCESS )
    {
        dlog_error("ERROR %d receiving %s reply!", ipc_ret, senderName.c_str());
        return IPCPACKET_ERROR; 
    }

    if (internalBuffer.size() > 0)
    {
        cmdCode = internalBuffer[0];
        dlog_msg("Received %d bytes long reply from %s", internalBuffer.size(), senderName.c_str());
        return IPCPACKET_SUCCESS;
    }
    dlog_error("Invalid packet (0 length)!");
    return IPCPACKET_ERROR;
}

#if(0)
int IPCPacket::receiveCheckResult( int & moduleExitCode, long timeout /* = 0 */ )
{
    int result = receive(timeout);
    if (result != IPCPACKET_SUCCESS) return result;
    // Search packet for response code tag
    ConstData value(ConstData::getInvalid());
    if ( getTag( mapp_tags::TagCode, value ) != IPCPACKET_SUCCESS )
    {
        dlog_alert("ERROR getting response code!");
        if ( getTag( mapp_tags::TagExtResultCode, value ) != IPCPACKET_SUCCESS )
        {
            dlog_alert("ERROR getting response code!");
            moduleExitCode = -1;
            return IPCPACKET_NOTFOUND;
        }
    }
    ValueConverter<int> valueConv(value);
    moduleExitCode = valueConv.getValue(-1);
    dlog_msg("Response code: %X", moduleExitCode);
    return result;
}
#endif
}
