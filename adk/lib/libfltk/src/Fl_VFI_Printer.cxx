//
// "$Id: Fl_GDI_Printer.cxx 9325 2012-04-05 05:12:30Z fabien $"
//
// Support for WIN32 printing for the Fast Light Tool Kit (FLTK).
//
// Copyright 2010-2012 by Bill Spitzak and others.
//
// This library is free software. Distribution and use rights are outlined in
// the file "COPYING" which should have been included with this file.  If this
// file is missing or damaged, see the license at:
//
//     http://www.fltk.org/COPYING.php
//
// Please report all bugs and problems on the following page:
//
//     http://www.fltk.org/str.php
//

#ifdef VERIXV

#include <stdio.h>
#include <string.h>
#include <svc.h>
#include <errno.h>

#include <FL/Fl.H>
#include <FL/Fl_Printer.H>


#include <FL/fl_ask.H>
#include <FL/math.h>

//extern HWND fl_window;

static const int PORT_STATUS_BUF_SIZE=4;

 
class Fl_OffCreen_Xlib_Graphics_Driver : public Fl_Xlib_Graphics_Driver {

	public:
	Fl_OffCreen_Xlib_Graphics_Driver()
	{
	  _sw = 0;
	  pixmap = 0;
	}

	~Fl_OffCreen_Xlib_Graphics_Driver()
	{
		if(_sw != 0)
		{
			end_offscreen();
		}
		
		if(pixmap)
		{
			XFreePixmap(fl_display, pixmap);
		}	
	}

	Fl_Offscreen create_offscreen(int w, int h) 
	{
		  pixmap = XCreatePixmap(fl_display, (Fl_Surface_Device::surface() == Fl_Display_Device::display_device() ?  fl_window : fl_xid(Fl::first_window()) ), w, h, fl_visual->depth);
		  
		  return pixmap;
	}	  

	void begin_offscreen()
	{
	  if(pixmap == 0)
	  	return;
	
	  if(_sw != 0)
	  {
	  	if(fl_window == pixmap)
		  	return;
	  }	
	  else
	  {
		  _sw=fl_window; 
		  _ss = Fl_Surface_Device::surface(); 
	  }	  

	  fl_window=pixmap; 
	  Fl_Display_Device::display_device()->set_current(); 
	  fl_push_no_clip();
	}

	void end_offscreen()
	{
	  if(_sw != 0)
	  {
		  fl_pop_clip(); 
		  fl_window = _sw; 
		  _ss->set_current();
		  _sw = 0;
	  }
	} 

	private:
	Pixmap pixmap;
	Window _sw;
	Fl_Surface_Device *_ss;
};
 
 
Fl_System_Printer::Fl_System_Printer(void) : Fl_Paged_Device() {
  hPr = -1;
  
  x_offset = 0;
  y_offset = 0;
//  driver(Fl_Display_Device::display_device()->driver());
  driver(new Fl_OffCreen_Xlib_Graphics_Driver);

}

Fl_System_Printer::~Fl_System_Printer(void) {

  if (hPr > -1) end_job();
  
  delete driver();
}

void Fl_System_Printer::set_current(void)
{
    dbprintf("Fl_System_Printer::set_current\n");
    ((Fl_OffCreen_Xlib_Graphics_Driver *)driver())->begin_offscreen();
}

void Fl_System_Printer::origin(int *x, int *y)
{
  Fl_Paged_Device::origin(x, y);
}

int Fl_System_Printer::start_job (int pagecount, int *frompage, int *topage)
// returns 0 iff OK
{
    dbprintf("Fl_System_Printer::start_job\n");

	hPr = open(DEV_COM4, 0);
	
    if (hPr >= 0)
    {
		open_block_t parm;
		const char VFI_PRINTER_GRAPHICS_MODE[] = {0x1b, 'g'};

		
    	//Set the Comm Parameters
    	memset(&parm,0,sizeof(parm));
//    	parm.rate      = Rt_19200;		
    	parm.rate      = Rt_115200;
    	parm.format    = Fmt_A8N1 | Fmt_auto |Fmt_RTS;
    	parm.protocol  = P_char_mode;
	    parm.parameter = 0;
    	int res = set_opn_blk( hPr, &parm);
    	
    	while(write(hPr, VFI_PRINTER_GRAPHICS_MODE, sizeof(VFI_PRINTER_GRAPHICS_MODE)) <= 0 && errno == ENOSPC)
			SVC_WAIT(0);

		int w,h;
        printable_rect(&w, &h);
       
        ((Fl_OffCreen_Xlib_Graphics_Driver *)driver())->create_offscreen(w,h);
			
      	if (frompage) *frompage = 1;
      	if (topage) *topage = pagecount;
	    x_offset = 0;
    	y_offset = 0;
    }

	return (hPr == -1);  //0 means OK
}

void Fl_System_Printer::end_job (void)
{

	dbprintf("Fl_System_Printer::end_job\n");

	if(hPr > -1)
	{
	
    	while(write(hPr, "\x28", 1) <= 0 && errno == ENOSPC)
			SVC_WAIT(0);

	  char portStat[PORT_STATUS_BUF_SIZE];
			
	  while(get_port_status(hPr,portStat) != 0)
		SVC_WAIT(1);
			
		close(hPr);
		hPr = -1;
		
	  ((Fl_OffCreen_Xlib_Graphics_Driver *)driver())->end_offscreen();
	}

	
//  Fl_Display_Device::display_device()->set_current();
}

void Fl_System_Printer::absolute_printable_rect(int *x, int *y, int *w, int *h)
{
  *x = 0;
  *y = 0;
  
  //VFI printer page size
  *w = 384;
  *h = 1024;

  origin(x_offset, y_offset);
}

void Fl_System_Printer::margins(int *left, int *top, int *right, int *bottom)
{
  int x, y, w, h;
  absolute_printable_rect(&x, &y, &w, &h);
  if (left) *left = x;
  if (top) *top = y;
  if (right) *right = x;
  if (bottom) *bottom = y;
}

int Fl_System_Printer::printable_rect(int *w, int *h)
{
  int x, y;
  absolute_printable_rect(&x, &y, w, h);
  return 0;
}

int Fl_System_Printer::start_page (void)
{

  dbprintf("Fl_System_Printer::start_page\n");
  
  int  rsult = 0;
  
	if(hPr != -1)
	{
    	dbprintf("Fl_System_Printer::start_page x_offset=%d, y_offset=%d\n", x_offset, y_offset);

	    ((Fl_OffCreen_Xlib_Graphics_Driver *)driver())->begin_offscreen();
    	
		origin(x_offset, y_offset);
		
		maxW = 0;
		maxH = 0;
		
	}

  return rsult;
}

void Fl_System_Printer::origin (int deltax, int deltay)
{
  x_offset = deltax;
  y_offset = deltay;
}

void Fl_System_Printer::scale (float scalex, float scaley)
{

dbprintf("Fl_System_Printer::scale\n");

  if (scaley == 0.) scaley = scalex;
  int w, h;
#if 0
  SetWindowExtEx(fl_gc, (int)(720 / scalex + 0.5), (int)(720 / scaley + 0.5), NULL);
#endif  
  printable_rect(&w, &h);
  origin(0, 0);
}

void Fl_System_Printer::rotate (float rot_angle)
{
dbprintf("Fl_System_Printer::rotate %f\n", rot_angle);

#if 0
  XFORM mat;
  float angle;
  angle = (float) - (rot_angle * M_PI / 180.);
  mat.eM11 = cos(angle);
  mat.eM12 = sin(angle);
  mat.eM21 = - mat.eM12;
  mat.eM22 = mat.eM11;
  mat.eDx = mat.eDy = 0;
  SetWorldTransform(fl_gc, &mat);
#endif  
}

int Fl_System_Printer::end_page (void)
{
  dbprintf("Fl_System_Printer::end_page\n");


  int  rsult = 0, w, h, hh, curh;
  
  const int VFIPAGESIZE = 80;
  
	if(hPr != -1)
	{
        printable_rect(&w, &h);

       	if(maxW < w) w = maxW;
       	if(maxH < h) h = maxH;
       	
    	hh = h;

   if(w > 384)	w = 384;
		
  const int charsToWrite = w/6;
  const int addnewline = charsToWrite < 64 ? 1 : 0;

  uchar *src = new uchar[w*VFIPAGESIZE*3];
  

  curh = 0;
  
  do
  {
  
   h = hh - curh;
   if(h > VFIPAGESIZE) h = VFIPAGESIZE;
   
   dbprintf("^^^^^^^^^^^^^^^ printing w=%d, h=%d, curh=%d\n", w, h, curh);
  
   fl_read_image(src, 0, curh, w, h, 0);

   uchar *prnline = src;
   uchar *prnlineptr = prnline;
   
   uchar *srcptr = src;
   int 	  srcr, srcg, srcb;

   const int GLevel = 255/8;

   char portStat[PORT_STATUS_BUF_SIZE];
  
	int pixCnt = 0;  

    for (int y = h; y > 0; y--)
    {
	  int pixelNo = 0;
	  unsigned char pb = 0x40;

      for (int x = w; x > 0; x--)
      {
      
		srcr = *srcptr++;
		srcg = *srcptr++;
		srcb = *srcptr++;
		
        int nGray = ( 28 * srcb  + 77 * srcr + 151 * srcg ) >> 8;
        
		if (nGray > GLevel || nGray==0)
		{
			pb |= 1<<(5-pixelNo); 
			pixCnt++;
		}
		
		
		pixelNo++;
		
		if(pixelNo > 5)
		{
	      	*prnlineptr++ = pb;
	  		pb = 0x40;
	      	pixelNo = 0;
		}
//      	dbprintf("%d %d %d\n", srcr, srcg, srcb);
      }
      
      if(addnewline)
	      	*prnlineptr++ = 0x21;
  	}

	 int rc;
	    	
	 do
	 {	 
	   rc = write(hPr, (const char *)prnline, prnlineptr - prnline);
	   
   	 } while(rc <= 0 && errno == ENOSPC && SVC_WAIT(1) == 0);

	  while((rc = get_port_status(hPr,portStat)) != 0)
		SVC_WAIT(1);

/*
	  do
	  {
	  
	  	rc = get_port_status(hPr,portStat);
	  	
	  	dbprintf("get_port_status rc=%d, portStat: %d %d %d %d  line=%d\n", rc, portStat[0], portStat[1], portStat[2], portStat[3], h-y);
	  	
	  } while (rc > 0 && SVC_WAIT(1) == 0);
*/		

//	  	rc = get_port_status(hPr,portStat);
//	  	dbprintf("get_port_status rc=%d, portStat: %d %d %d %d  line=%d\n", rc, portStat[0], portStat[1], portStat[2], portStat[3], h-y);
  	
  	

	dbprintf("Fl_System_Printer::end_page printed %d points of %d\n", pixCnt, w*h);
	
	curh += h;
	
	} while(curh < hh);
	
	delete[] src;
	
	((Fl_OffCreen_Xlib_Graphics_Driver *)driver())->end_offscreen();
    }

  return 0;  
}

void Fl_System_Printer::print_widget(Fl_Widget* widget, int delta_x, int delta_y)
{
  dbprintf("&&&&&&&&&&&&&&&&& Fl_System_Printer::print_widget w=%d h=%d\n", widget->w(), widget->h());

  if(widget->w() > maxW) maxW = widget->w();
  if(widget->h() > maxH) maxH = widget->h();
  
  Fl_Paged_Device::print_widget(widget, delta_x, delta_y);
}


/*
static int translate_stack_depth = 0;
const int translate_stack_max = 5;
static int translate_stack_x[translate_stack_max];
static int translate_stack_y[translate_stack_max];
*/

static void do_translate(int x, int y)
{
#if 0
  XFORM tr;
  tr.eM11 = tr.eM22 = 1;
  tr.eM12 = tr.eM21 = 0;
  tr.eDx =  (FLOAT) x;
  tr.eDy =  (FLOAT) y;
  ModifyWorldTransform(fl_gc, &tr, MWT_LEFTMULTIPLY);
#endif
}

void Fl_System_Printer::translate (int x, int y)
{
/*
  do_translate(x, y);
  if (translate_stack_depth < translate_stack_max) {
    translate_stack_x[translate_stack_depth] = x;
    translate_stack_y[translate_stack_depth] = y;
    translate_stack_depth++;
    }
*/    
}

void Fl_System_Printer::untranslate (void)
{
/*
  if (translate_stack_depth > 0) {
    translate_stack_depth--;
    do_translate( - translate_stack_x[translate_stack_depth], - translate_stack_y[translate_stack_depth] );
    }
*/    
}

#endif // WIN32

//
// End of "$Id: Fl_GDI_Printer.cxx 9325 2012-04-05 05:12:30Z fabien $".
//
