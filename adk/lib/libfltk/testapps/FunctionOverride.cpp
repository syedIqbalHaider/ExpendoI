/*
 * FunctionOverride.cpp
 *
 *
 * This file contains all overwritten functions which enables fixing issues in 3rd party libraries.
 *
 *
 *  Created on: 13-02-2013
 *      Author: t_marcinp3
 */

/*****************************************************************************************/

#include <svc.h>

#define MEMORY_MANAGEMENT_DEBUG
// Memory management overrides very helpful for debugging
#ifdef MEMORY_MANAGEMENT_DEBUG
extern "C"
{
extern void* $Super$$malloc(size_t size);
extern void* $Super$$_Znwj(size_t size);
extern void* $Super$$_Znaj(size_t size);
extern void $Super$$free(void* p);
extern void $Super$$_ZdaPv(void* p);
extern void $Super$$_ZdlPv(void* p);

void* $Sub$$malloc(size_t size)
{
	unsigned int rc = __return_address();
	dbprintf("*** malloc() start (size: %d) return address: %p\n", size, rc);
	void* res = $Super$$malloc(size);
	dbprintf("*** malloc() for %p (size: %d) return address: %p\n", res, size, rc);
	return res;
}

void* $Sub$$_Znwj(size_t size)
{
	unsigned int rc = __return_address();
	void* res = $Super$$_Znwj(size);
	dbprintf("*** _Znwj() for %p (size: %d) return address: %p\n", res, size, rc);
	return res;
}

void* $Sub$$_Znaj(size_t size)
{
	unsigned int rc = __return_address();
	void* res = $Super$$_Znaj(size);
	dbprintf("*** _Znaj() for %p (size: %d) return address: %p\n", res, size, rc);
	return res;
}

void $Sub$$free(void* p)
{
	unsigned int rc = __return_address();
	dbprintf("*** free() for %p return address: %p\n", p, rc);
	$Super$$free(p);
}

void $Sub$$_ZdaPv(void* p)
{
	unsigned int rc = __return_address();
	dbprintf("*** _ZdaPv() for %p return address: %p\n", p, rc);
	$Super$$_ZdaPv(p);
}

void $Sub$$_ZdlPv(void* p)
{
	unsigned int rc = __return_address();
	dbprintf("*** _ZdlPv() for %p return address: %p\n", p, rc);
	$Super$$_ZdlPv(p);
}
}
#endif
