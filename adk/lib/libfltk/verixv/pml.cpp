#include <stdio.h>
#include <math.h>

#include <svc.h>

FILE *popen(const char *command, const char *type)
{
	return NULL;
}

int pclose(FILE *stream)
{
	return -1;
}

double rint(double x)
{
	return (x >= 0.0) ? floor(x + 0.5) : ceil(x - 0.5);
}

size_t wcstombs (char* dest, const wchar_t* src, size_t max)
{
	return snprintf(dest, max, "%s", src);
}

size_t mbstowcs (wchar_t* dest, const char* src, size_t max)
{
	return snprintf((char *)dest, max, "%s", src);
}

/*int access(const char * f, int p)
{
	return dir_get_attributes(filename) == -1;
}*/
