#ifndef __PML_H
#define __PML_H

#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>

FILE *popen(const char *command, const char *type);
int pclose(FILE *stream);
double rint(double x);

//int access(const char * f, int p);
//#define access(f, p) dir_get_attributes(f)

size_t wcstombs (char* dest, const wchar_t* src, size_t max);
size_t mbstowcs (wchar_t* dest, const char* src, size_t max);


#endif
