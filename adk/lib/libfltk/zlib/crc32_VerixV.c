/*
	This is a VerixV version of crc32 function that calculates same results as original one in crc32.c.
	It is way smaller (as it only calls OS function) and also is over 2.5 times faster (tested on Vx810)
*/

#include "zutil.h"      /* for STDC and FAR definitions */ 

#include <svc.h>


unsigned long ZEXPORT crc32(unsigned long crc, const unsigned char FAR * buf, unsigned len)
{ 
	if (!buf)
		return 0UL;
		
	crc = ~SVC_CRC_CRC32_L(buf, len, ~crc);
	return crc;
}
