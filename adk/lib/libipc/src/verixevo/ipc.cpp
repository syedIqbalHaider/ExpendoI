/*****************************************************************************
 * 
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file        Ipc.cpp
 *
 * @author      Kamil Pawlowski
 * 
 * @brief       Internal handler for internal communication process library (IPC)
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C Coding  
 *              Standard 1.0.0 
 */

#include <svc.h>

//#include <fmtlog/fmtlog.hpp>
#include <libipc/ipc.h>

#include "ThreadData.h"
#include "libver.h"

#if defined(__GNUC__)
#include <fcntl.h>
#endif

using namespace ipc_thread_data;

namespace internal_ipc
{
    const std::string upper_case(const std::string & task_name)
    {
        std::string result;
        std::transform(task_name.begin(), task_name.end(), std::back_inserter(result), ::toupper);
        return result;
    }
}
namespace com_verifone_ipc
{

    int init(const std::string &task_name, int timeout)
    {
        // Create static object
        // Will crash in case memory cannot be allocated!
        com_verifone_libver::register_library();
        ThreadData * ptr = ThreadData::getThreadData();
        return ptr->init(internal_ipc::upper_case(task_name), timeout);
    }

    int deinit(int timeout)
    {
        ThreadData * ptr = ThreadData::getThreadData();
        int result = ptr->deinit(timeout);
        delete ptr;
        return result;
    }

    int connect_to(const std::string &task_name, int timeout)
    {
        if (task_name.size())
        {
            ThreadData * ptr = ThreadData::getThreadData();
            std::string task = internal_ipc::upper_case(task_name);
            if (ptr->isChannelOpened(task))
            {
                //log_fmt_warn("Already connected to %1%", task_name);
                return IPC_SUCCESS;
            }
            return ptr->openChannel(task, timeout);
        }
        return IPC_ERROR_INVALID_PARAMETERS;
    }

    int disconnect(const std::string &task_name, int timeout)
    {
        if (task_name.size())
        {
            ThreadData * ptr = ThreadData::getThreadData();
            std::string task = internal_ipc::upper_case(task_name);
            if (ptr->isChannelOpened(task))
            {
                return ptr->closeChannel(task, timeout);
            }
            //log_fmt_warn("Not connected to %1%!", task_name);
            return IPC_ERROR_INVALID_PARAMETERS;
        }
        return IPC_ERROR_INVALID_PARAMETERS;
    }

    int send(const std::string &buffer, const std::string &destination_task)
    {
        if (destination_task.size() && buffer.size())
        {
            ThreadData * ptr = ThreadData::getThreadData();
            std::string task = internal_ipc::upper_case(destination_task);
            if (ptr->isChannelOpened(task))
            {
                // create buffer
                return ptr->send(buffer, task);
            }
            //log_fmt_warn("Not connected to %1%!", destination_task);
            return IPC_ERROR_INVALID_PARAMETERS;
        }
        return IPC_ERROR_INVALID_PARAMETERS;
    }

    int receive(std::string &buffer, const std::string &source_task, std::string &received_from)
    {
        ThreadData * ptr = ThreadData::getThreadData();
        if (ptr->checkPending(internal_ipc::upper_case(source_task), received_from) == IPC_SUCCESS) // sets received_from!
        {
            return ptr->receive(buffer, received_from);
        }
        return IPC_ERROR_NO_DATA_PENDING;
    }

    int check_pending(const std::string &source_task)
    {
        ThreadData * ptr = ThreadData::getThreadData();
        std::string from;
        return ptr->checkPending(internal_ipc::upper_case(source_task), from);
    }

    int is_task_available(const std::string &task)
    {
        return ThreadData::getThreadData()->getTaskPipeHandle(task) >= 0;
    }

    ipc_handle get_events_handler(const std::string &source_task /* = "" */)
    {
        return ThreadData::getThreadData()->getEventsHandler(source_task);
    }

} // namespace
