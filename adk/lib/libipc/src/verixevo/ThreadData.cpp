/*****************************************************************************
 * 
 * Copyright (C) 2007 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file        ThreadData.cpp
 *
 * @author      Kamil Pawlowski
 * 
 * @brief       Private IPC library class implementation
 *              This class handles all the IPC operations
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C Coding  
 *              Standard 1.0.0 
 */

#include <svc.h>
#include <errno.h>

#include <liblog/logsys.h>
#include <libipc/ipc.h>
#include <libpml/pml.h>

#include "ThreadData.h"

namespace ipc_internal
{
	static const int MAX_DATA = 2048;
	char tempBuffer[MAX_DATA];
}

namespace ipc_internal_pml
{
	using namespace com_verifone_pml;
	event_item getEventItem(int handle)
	{
		event_item evt;
		evt.event_id = events::ipc;
		evt.evt.com.fd = handle;
		evt.evt.com.flags = 0;
		dlog_msg("Event type %d, handle %d", evt.event_id, handle);
		return evt;
	}
}

// namespace ipc_constants
//{
	static const int MAX_QUEUE = 20; // taken from ACM
	static const size_t DEFAULT_RETRY_COUNT = 1000;
	static const size_t DEFAULT_RETRY_WAIT_TIME = 100;
	static const int INVALID_HANDLE = -1;
	enum IPC_COMMANDS { CMD_DATA = 0, CMD_OPEN_CONNECTION, CMD_CLOSE_CONNECTION };
	//static const unsigned char CMD_OPEN_CONNECTION = 1;
	//static const unsigned char CMD_CLOSE_CONNECTION = 2;
//}

namespace ipc_thread_data
{
    threadMapper ThreadData::threadsMap;

    ThreadData * ThreadData::getThreadData()
    {
        threadMapper::const_iterator itFinder = ThreadData::threadsMap.find(get_task_id());
        if (itFinder != threadsMap.end()) return itFinder->second;
        ThreadData * pTD = new ThreadData;
        assert(pTD);
        dlog_msg("Allocated new ptr %p for task %d", pTD, get_task_id());
        threadsMap.insert( std::make_pair( get_task_id(), pTD ) );
        return pTD;
    }


    // constructor
    ThreadData::ThreadData(): 
            sendRetryCount(DEFAULT_RETRY_COUNT), 
            sendRetryWaitTime(DEFAULT_RETRY_WAIT_TIME)
            //ourName(fillOutName(task_name)), ourHandle(openOurPipe()), 
    {
        // setup named pipe with our name
        //assert(ourName.size() > 0);
        //assert(ourHandle >= 0);
        dlog_msg("We're successfully initialised!");
    }

    // destructor
    ThreadData::~ThreadData()
    {
    
        dlog_msg("Destructor %p from task %d", this, get_task_id());
    
        for (handleMapper::iterator it = handleMap.begin(); it != handleMap.end(); ++it)
        {
            int hdl = it->second;
            std::string task = it->first;
            dlog_alert("Still open communication with %s!", task.c_str());
            closeChannel(task, com_verifone_ipc::CHANNEL_TIMEOUT);
            //close(hdl);
        }
        handleMap.clear();
        for (handleMapper::iterator it = eventHandleMap.begin(); it != eventHandleMap.end(); ++it)
        {
            int hdl = it->second;
            com_verifone_pml::event_close(hdl);
        }
        com_verifone_pml::event_close(eventHandler);
        close(ourHandle);

        for (threadMapper::iterator it = ThreadData::threadsMap.begin(); it != ThreadData::threadsMap.end(); ++it)
        {
            if(it->second == this)
            {
                dlog_msg("Removing from threadsMap");
                ThreadData::threadsMap.erase(it);
                break;
            }
        }
    }

    // inits the connection
    int ThreadData::init(const std::string & task, const int) // timeout is unused on Trident
    {
        ourName = fillOutName(task);
        // dlog_msg("IPC: Initialising task %s", ourName.c_str());
        eventHandler = com_verifone_pml::event_open();
        return openOurPipe();
    }
    
    int ThreadData::deinit(const int timeout)
    {
        int result = com_verifone_ipc::IPC_SUCCESS;
        for (handleMapper::iterator it = handleMap.begin(); it != handleMap.end(); ++it)
        {
            int hdl = it->second;
            std::string task = it->first;
            int res = closeChannel(task, timeout);
            if (res != com_verifone_ipc::IPC_SUCCESS)
            {
                dlog_msg("Cannot close channel with %s!", task.c_str());
                result = res;
            }
            else
            {
                handleMap.erase(it);
            }
        }
        return result;
    }

    // checks whether we're connected with given task
    bool ThreadData::isChannelOpened(const std::string & task) const
    {
        handleMapper::const_iterator itExec = handleMap.find(task);
        return itExec != handleMap.end();
    }

    // opens communication channel
    int ThreadData::openChannel(const std::string & task, const int timeout)
    {
        //if (!task.size()) return com_verifone_ipc::IPC_ERROR_INVALID_PARAMETERS;
        //if (isChannelOpened(task)) return com_verifone_ipc::IPC_ERROR_INVALID_PARAMETERS;
        int handle = -1;
        int result = connectToTask(task, CMD_OPEN_CONNECTION, -1, handle, timeout);
        if (result < 0)
        {
            return com_verifone_ipc::IPC_ERROR_CREATE_QUEUE;
        }

        do
        {
            std::string taskName;
            int taskHandle = 0;
            unsigned char cmd = 0;
            int recvHandle = ourHandle;
            if (pipe_pending(handle)) recvHandle = handle;
            dlog_msg("Using handle %d for connection", recvHandle);
            if (receiveTaskData(recvHandle, cmd, taskName, taskHandle) == com_verifone_ipc::IPC_SUCCESS)
            {
                if (taskName == task)
                {
                    // Cool - set it up
                    int res = pipe_connect(handle, taskHandle);
                    if (res == 0)
                    {
                        handleMap.insert( make_pair(taskName, handle) );
                        dlog_msg("Successfully connected to %s!", taskName.c_str());
                        addEventToLocalMap(taskName, handle);
                        return com_verifone_ipc::IPC_SUCCESS;
                    }
                    break;
                }
                else
                {
                    dlog_alert("Received handle from invalid task '%s'!", taskName.c_str());
                    result = createConnection(taskHandle, taskName);
                    if (result != com_verifone_ipc::IPC_SUCCESS)
                    {
                        dlog_error("Cannot create connection or invalid request!");
                    }

                        //int ThreadData::connectToTask(const std::string &task, const unsigned char cmd, const int currentHandle, int &handle, const int timeout)
                        //if (cmd == CMD_CLOSE_CONNECTION) res = waitForOtherSide(currentHandle, timeout, task);
                        //else res = waitForOtherSide(handle, timeout, task);
                    if (waitForOtherSide(taskHandle, timeout, taskName) >= 0)
                    {
                        dlog_msg("Connection successful, new handle %d!", handle);
                        if (waitForOtherSide(handle, timeout, task) < 0)
                        {
                            dlog_error("Waiting for task %s failed!", task.c_str());
                            return com_verifone_ipc::IPC_ERROR_CREATE_QUEUE;
                        }
                        // continue the loop
                    }
                    else
                    {
                        dlog_error("Waiting for task %s failed (timeout)!", task.c_str());
                        return com_verifone_ipc::IPC_ERROR_CREATE_QUEUE;
                    }
                }
            }
            else
            {
                dlog_error("Invalid data received!");
                return com_verifone_ipc::IPC_ERROR_CREATE_QUEUE;
            }
        } while ( true );
        return com_verifone_ipc::IPC_ERROR_CREATE_QUEUE;
    }

    int ThreadData::closeChannel(const std::string & task, const int timeout)
    {
        handleMapper::iterator itExec = handleMap.find(task);
        assert(itExec != handleMap.end());
        int currentHandle = itExec->second;
        int handle = -1;
        int result = connectToTask(task, CMD_CLOSE_CONNECTION, currentHandle, handle, timeout);
        if (handle != -1)
        {
            close(handle);
        }
        if (result >= 0)
        {
            std::string taskName;
            int taskHandle = 0;
            unsigned char cmd = 0;
            if (receiveTaskData(currentHandle, cmd, taskName, taskHandle) == com_verifone_ipc::IPC_SUCCESS)
            {
                handleMap.erase(itExec);
                removeEventFromLocalMap(taskName, currentHandle);
                close(currentHandle);
                dlog_msg("Connection with %s successfully closed (handle %d)!", task.c_str(), currentHandle);
                return com_verifone_ipc::IPC_SUCCESS;
            }
            else
            {
                dlog_error("Invalid data received!");
                return com_verifone_ipc::IPC_ERROR_DESTROY_QUEUE;
            }
        }
        return result;
    }

    int ThreadData::send(const std::string &buffer, const std::string & destination) const
    {
        int handle = getHandle(destination);
        assert(handle >= 0);
        dlog_msg("Sending %d bytes to %s, handle %d", buffer.size(), destination.c_str(), handle);
        const size_t chunkSub = 2 * sizeof(size_t);
        size_t dataSize = buffer.size();
        if (dataSize + chunkSub <= ipc_internal::MAX_DATA)
        {
            return sendChunk(handle, dataSize, buffer, destination);
        }

        // Send separate chunks
        size_t bytesWritten = 0;
        dlog_msg(">>> CHUNK MODE!");
        do
        {
            const size_t chunk_pcom_len = ((dataSize-bytesWritten) > (ipc_internal::MAX_DATA-chunkSub)) ? (ipc_internal::MAX_DATA-chunkSub):(dataSize-bytesWritten);
            dlog_msg("sending %d bytes", chunk_pcom_len);
            std::string send_data(buffer.substr(bytesWritten, chunk_pcom_len));
            int result = sendChunk(handle, dataSize, send_data, destination);
            if (result != com_verifone_ipc::IPC_SUCCESS)
            {
                dlog_error("Critical internal error sending chunk: error code %d, errno %d", result, errno);
                return com_verifone_ipc::IPC_ERROR_SEND;
            }
            bytesWritten += chunk_pcom_len;
        } while( bytesWritten < dataSize );
        return com_verifone_ipc::IPC_SUCCESS;
    }

    int ThreadData::checkPending(const std::string &source, std::string &from)
    {
        if (source.size())
        {
            // dlog_msg("Check pending: source %s", source.c_str());
            int handle = getHandle(source);
            if (handle >= 0 && pipe_pending(handle) > 0)
            {
                from = source;
                return com_verifone_ipc::IPC_SUCCESS;
            }
            /*assert(handle >= 0);
            if (pipe_pending(handle))
            {
                from = source;
                return com_verifone_ipc::IPC_SUCCESS; 
            } */
        }
        else
        {
            for (handleMapper::const_iterator it = handleMap.begin(); it != handleMap.end(); ++it)
            {
                int hdl = it->second;
                // dlog_msg("Checking pending from %s, handle %d", it->first.c_str(), hdl);
                //assert(hdl >= 0);
                if (hdl >= 0 && pipe_pending(hdl) > 0)
                {
                    // dlog_msg("message pending from %s", it->first.c_str());
                    from = it->first;
                    return com_verifone_ipc::IPC_SUCCESS;
                }
            }
        }
        // If we got here, this means nothing awaits on registered pipes. Check if someone wants to connect to us.
        return registerNewSource();
    }

    int ThreadData::receive(std::string &buffer, const std::string source) const
    {
        if (source.size())
        {
            dlog_msg("Receive from %s", source.c_str());
            int handle = getHandle(source);
            if (handle >= 0)
            {
                const size_t chunkSub = 2 * sizeof(size_t);
                int totalMsgSize = 0;
                buffer.clear();
                do
                {
                    if (pipe_pending(handle) > 0)
                    {
                        int result = read( handle, ipc_internal::tempBuffer, sizeof(ipc_internal::tempBuffer) );
                        if (result > 0)
                        {
                            if (result > chunkSub)
                            {
                                totalMsgSize = getInteger(ipc_internal::tempBuffer);
                                int currentSize = getInteger(ipc_internal::tempBuffer+sizeof(int));
                                dlog_msg("Total size %d, current %d", totalMsgSize, currentSize);
                                if (totalMsgSize < buffer.capacity())
                                {
                                    if (buffer.capacity() - buffer.size() + totalMsgSize > _heap_max())
                                    {
                                        dlog_error("Max heap %ld, current heap %ld, buffer capacity %d", _heap_max(), _heap_current(), buffer.capacity());
                                        dlog_error("Received too much data (we have only %ld heap bytes available)!", _heap_max());
                                        return com_verifone_ipc::IPC_ERROR_RECEIVE;
                                    }
                                    buffer.reserve(totalMsgSize);
                                }
                                buffer.append(ipc_internal::tempBuffer+chunkSub, result-chunkSub);
                            }
                            else
                            {
                                dlog_error("Invalid data received len %d (too short, minimum expected is %d)!", result, 2*sizeof(int)+1);
                                return com_verifone_ipc::IPC_ERROR_RECEIVE;
                            }
                        }
                        // error receiving, just wait for next iteration
                    }
                    SVC_WAIT(0);
                } while(totalMsgSize > buffer.size());
                return com_verifone_ipc::IPC_SUCCESS;
            }
            else
            {
                dlog_error("Handle for task %s cannot be obtained!", source.c_str());
                return com_verifone_ipc::IPC_ERROR_RECEIVE;
            }
        }
        dlog_msg("Invalid parameters!");
        return com_verifone_ipc::IPC_ERROR_INVALID_PARAMETERS;
    }

    com_verifone_ipc::ipc_handle ThreadData::getEventsHandler(const std::string &task) const
    {
        if (task.size())
        {
            handleMapper::const_iterator it = eventHandleMap.find(task);
            if (it != eventHandleMap.end())
            {
                com_verifone_ipc::ipc_handle ipch(it->second);
                return ipch;
            }
        }
        com_verifone_ipc::ipc_handle ipch(eventHandler);
        return ipch;
    }

    /****************************************************************************************************************************
     * PRIVATE
     ****************************************************************************************************************************/
    std::string ThreadData::fillOutName(const std::string & task_name)
    {
        if (task_name.size()) return task_name;
        struct task_info ti;
        int osRes = get_task_info(get_task_id(), &ti);
        assert(osRes > 0); //crash on failure!
        std::string result = ti.path;
        assert(result.size());
        // If this is file name, remove 'F:' or 'I:' from the beginning
        std::size_t found = result.find('/');
        if (found != std::string::npos)
        {
            // we have something like "F:15/TEST.OUT"
            result.erase(0, found+1);
        }
        else
        {
            // we still can have "F:TEST.OUT"...
            found = result.find(':');
            if (found != std::string::npos)
            {
                result.erase(0, found+1);
            }
        }
        // Ok, prefix (drive and GID) stripped. Find extension
        found = result.find(".OUT");
        if (found != std::string::npos)
        {
            result.erase(found, 4);
        }
        return result;
    }

    int ThreadData::getHandle(const std::string & task) const
    {
        handleMapper::const_iterator itExec = handleMap.find(task);
        if (itExec != handleMap.end()) return itExec->second;
        return INVALID_HANDLE;
        // assert(itExec != handleMap.end());
        //return itExec->second;
    }

    int ThreadData::openOurPipe()
    {
        std::string name("P:");
        name.append(ourName);
        // check if we're opened already (would mean name conflict!)

        int hdl = open( name.c_str(), O_RDWR );
        if (hdl >= 0)
        {
            int taskID = 0;
            if (get_owner(name.c_str(), &taskID) >= 0)
            {
                if (taskID != get_task_id())
                {
                    dlog_error("Name conflict! Named pipe with given name (%s) is already opened by task id %d (our id %d)!", name.c_str(), taskID, get_task_id());
                    return com_verifone_ipc::IPC_ERROR_INVALID_PARAMETERS;
                }
            }

            int res = pipe_init_msg(hdl, MAX_QUEUE);
            if (res == 0)
            {
                dlog_msg("Our pipe (%s) opened, handle %d (our task no %d)", ourName.c_str(), hdl, get_task_id());
                ourHandle = hdl;
                int eventRes = com_verifone_pml::event_ctl(eventHandler, com_verifone_pml::ctl::ADD, ipc_internal_pml::getEventItem(ourHandle));
                dlog_msg("Event result %d, added handle %d for task %s", eventRes, ourHandle, ourName.c_str()); // if < 0 -> error
                return com_verifone_ipc::IPC_SUCCESS;
            }
            else
            {
                dlog_error("Cannot initialise our pipe handle, error %d, errno %d!", res, errno);
            }
        }
        else
        {
            dlog_error("Cannot open our handle!");
        }
        return com_verifone_ipc::IPC_ERROR_CREATE_QUEUE;
    }

    int ThreadData::getInteger(const char * buf) const
    {
        int result = 0;
        int multiplier = 0x18;
        int index = 0;
        while (index < sizeof(int))
        {
            result += *(buf+index) << multiplier;
            multiplier -= 0x08;
            ++index;
        }
        return result;
    }

    int ThreadData::receiveTaskData(int handle, unsigned char & cmd, std::string & taskName, int & taskHandle)
    {
        if (pipe_pending(handle))
        {
            using namespace ipc_internal;
            memset(tempBuffer, 0, sizeof(tempBuffer));
            int readCount = read(handle, tempBuffer, sizeof(tempBuffer)-1);
            //if (readCount != cnt || strlen(tmpbuf) + sizeof(taskHandle) + 1 != readCount)
            if (strlen(tempBuffer) + sizeof(taskHandle) + sizeof(unsigned char) + 1 != readCount)
            {
                dlog_error("Register new source error: received %d bytes, should be max %d, errno %d", readCount, MAX_DATA, errno);
                dlog_hex(tempBuffer, readCount, "READ STUFF");
                return com_verifone_ipc::IPC_ERROR_RECEIVE;
            }
            // ok, we got the stuff
            taskName = tempBuffer;
            taskHandle = 0;
            int index = strlen(tempBuffer) + 1;
            if (index + 1 <= readCount)
            {
                cmd = *(tempBuffer+index);
                ++index;
                if (index + sizeof(int) <= readCount)
                {
                    taskHandle = getInteger(tempBuffer+index);
                    dlog_msg("Received info from task %s: cmd %02Xh, handle %d", taskName.c_str(), cmd, taskHandle);
                    if (handleMap.count(taskName) > 0)
                    {
                        dlog_alert("Task %s is already registered, handle %d - new %d!", taskName.c_str(), handleMap[taskName], taskHandle);
                        handleMap[taskName] = taskHandle;
                        return com_verifone_ipc::IPC_ERROR_NO_DATA_PENDING;
                    }
                    return com_verifone_ipc::IPC_SUCCESS;
                }
            }
            dlog_error("Message too short - %d more bytes required, only %d present!", sizeof(int)+sizeof(unsigned char), readCount-index);
            dlog_hex(tempBuffer, readCount, "READ STUFF");
            return com_verifone_ipc::IPC_ERROR_RECEIVE;
        }
        else
        {
            dlog_error("Nothing awaits on given handle!");
            return com_verifone_ipc::IPC_ERROR_NO_DATA_PENDING;
        }
    }

    int ThreadData::registerNewSource()
    {
        // Ok, now let's check - maybe someone wants to connect to US...
        int connResult = com_verifone_ipc::IPC_ERROR_NO_DATA_PENDING;
        // dlog_msg("Nothing on registered pipes and %d messages pending!", pipe_pending(ourHandle));
        while (pipe_pending(ourHandle) > 0)
        {
            // Seems so!
            dlog_msg("Nothing on registered pipes, but %d messages pending!", pipe_pending(ourHandle));
            std::string taskName;
            int taskHandle = 0;
            unsigned char cmd = 0;
            if (receiveTaskData(ourHandle, cmd, taskName, taskHandle) == com_verifone_ipc::IPC_SUCCESS)
            {
                if (cmd == CMD_OPEN_CONNECTION)
                {
                    connResult = createConnection(taskHandle, taskName);
                }
                else if (cmd == CMD_CLOSE_CONNECTION)
                {
                    connResult = closeConnection(taskHandle, taskName);
                }
            }
            else
            {
                dlog_error("Some invalid stuff found!");
            }
        }

        return com_verifone_ipc::IPC_ERROR_NO_DATA_PENDING;
    }

    int ThreadData::createConnection(const int taskHandle, const std::string & taskName)
    {
        int connResult = com_verifone_ipc::IPC_ERROR_CREATE_QUEUE;
        // Someone sent us his name and handle. Let's set it up.
        //dlog_msg("Connection request from %s, handle %d", taskName.c_str(), taskHandle);
        // New task
        int newHandle = open("P:", O_RDWR);
        if (newHandle >= 0)
        {
            int result = pipe_init_msg(newHandle, MAX_QUEUE);
            if (result == 0)
            {
                result = pipe_connect(newHandle, taskHandle);
                if (result == 0)
                {
                    //send back our pipe handle so that the caller can connect pipes
                    dlog_msg("Sending back our handle");
                    result = notifyOtherSide(newHandle, CMD_OPEN_CONNECTION);
                    if (result > 0)
                    {
                        handleMapper::const_iterator itExec = handleMap.find(taskName);
                        if (itExec != handleMap.end())
                        {
                            handleMap.insert( make_pair(taskName, newHandle) );
                            addEventToLocalMap(taskName, newHandle);
                        }
                        else
                        {
                            removeEventFromLocalMap(taskName, handleMap[taskName]);
                            handleMap[taskName] = newHandle;
                            addEventToLocalMap(taskName, newHandle);
                        }
                        dlog_msg("Success, we're now connected to %s!", taskName.c_str());
                        connResult = com_verifone_ipc::IPC_SUCCESS;
                        // return com_verifone_ipc::IPC_SUCCESS;
                    }
                    else
                    {
                        dlog_error("Cannot send data to %s: error %d, errno %d", taskName.c_str(), result, errno);
                        //return com_verifone_ipc::IPC_ERROR_CREATE_QUEUE;
                    }
                }
                else
                {
                    dlog_error("Cannot connect to new pipe: error %d, errno %d", result, errno);
                    //return com_verifone_ipc::IPC_ERROR_CREATE_QUEUE;
                }
            }
            else
            {
                dlog_error("Cannot initialize new pipe: error %d, errno %d", result, errno);
                //return com_verifone_ipc::IPC_ERROR_CREATE_QUEUE;
            }
        }
        else
        {
            dlog_error("Cannot open new pipe handle: error %d, errno %d", newHandle, errno);
            //return com_verifone_ipc::IPC_ERROR_CREATE_QUEUE;
        }
        return connResult;
    }

    int ThreadData::closeConnection(const int taskHandle, const std::string & taskName)
    {
        int connResult = com_verifone_ipc::IPC_ERROR_DESTROY_QUEUE;
        // Connection should be closed
        handleMapper::iterator it = handleMap.find(taskName);
        if (it != handleMap.end())
        {
            int hdl = it->second;
            dlog_msg("Sending back confirmation");
            int result = notifyOtherSide(hdl, CMD_CLOSE_CONNECTION);
            if (result > 0)
            {
                handleMap.erase(it);
                removeEventFromLocalMap(taskName, hdl);
                close(hdl);
                dlog_msg("Closed connection with task %s (handle %d)", taskName.c_str(), hdl);
                connResult = com_verifone_ipc::IPC_SUCCESS;
                //return com_verifone_ipc::IPC_SUCCESS;
            }
            else
            {
                dlog_error("Cannot send data to %s: error %d, errno %d", taskName.c_str(), result, errno);
                //return com_verifone_ipc::IPC_ERROR_CREATE_QUEUE;
            }
        }
        else
        {
            dlog_error("Cannot close connection to %s: we're not connected!", taskName.c_str());
            //return com_verifone_ipc::IPC_ERROR_DESTROY_QUEUE;
        }
        return connResult;
    }

    int ThreadData::sendChunk(int handle, const int fullSize, const std::string &buffer, const std::string & destination) const
    {
        size_t retries = sendRetryCount;
        int osRes;
        std::string send_data;
        int dataSize = buffer.size();
        send_data.append(reinterpret_cast<const char *>(&fullSize), sizeof(fullSize));
        send_data.append(reinterpret_cast<const char *>(&dataSize), sizeof(dataSize));
        send_data.append(buffer);
        assert(send_data.size() <= ipc_internal::MAX_DATA);
        do
        {
            osRes = write( handle, send_data.c_str(), send_data.size() );
            if (osRes != send_data.size())
            {
                int lerrno = errno;
                dlog_error("Cannot send message length %d to %s - error %d, errno %d", buffer.size(), destination.c_str(), osRes, errno);
                if (lerrno != ENOSPC)
                {
                    return com_verifone_ipc::IPC_ERROR_SEND;
                }
                SVC_WAIT(sendRetryWaitTime);
            }
            else
            {
                osRes = 0;
            }
        } while( osRes != 0 && --retries > 0 );
        return com_verifone_ipc::IPC_SUCCESS;
    }

    int ThreadData::notifyOtherSide(const int handle, const unsigned char cmd) const
    {
        std::string initBuffer(ourName);
        initBuffer.append(1, '\0');
        initBuffer.append(1, cmd);
        initBuffer.append(reinterpret_cast<const char *>(&handle), sizeof(handle));
        dlog_msg("Sending %d bytes, notification %02Xh, handle %d", initBuffer.size(), cmd, handle);
        dlog_hex(initBuffer.c_str(), initBuffer.size(), "REG_MSG");
        int result = write( handle, initBuffer.c_str(), initBuffer.size() );
        if (result == initBuffer.size()) return result;
        else if (result >= 0) return com_verifone_ipc::IPC_ERROR_SEND;
        else return result; // os error -1
    }

    int ThreadData::waitForOtherSide(const int handle, const int timeout, const std::string & task)
    {
        long currentTicks = read_ticks();
        do
        {
            SVC_WAIT(50);
            if (read_ticks() - currentTicks > timeout)
            {
                dlog_error("Waiting TIMEOUT - second task didn't answer!");
                return com_verifone_ipc::IPC_ERROR_NO_DATA_PENDING;
            }
            // Kamil_P1: Prevent registration deadlock
            if (pipe_pending(ourHandle))
            {
                dlog_alert("Some task is trying to connect to us! Check it to prevent deadlocks!");
                return com_verifone_ipc::IPC_SUCCESS;
            }
        } while (pipe_pending(handle) == 0);
        return com_verifone_ipc::IPC_SUCCESS;
    }

    int ThreadData::connectToTask(const std::string &task, const unsigned char cmd, const int currentHandle, int &handle, const int timeout)
    {
        // checks if such pipe is already opened
        int result;
        if (cmd == CMD_CLOSE_CONNECTION) result = com_verifone_ipc::IPC_ERROR_DESTROY_QUEUE;
        else result = com_verifone_ipc::IPC_ERROR_CREATE_QUEUE;
        int remoteHandle = getTaskPipeHandle(task);
        if (remoteHandle >= 0)
        {
            dlog_msg("Connecting to task %s", task.c_str());
            handle = open("P:", O_RDWR);
            if (handle >= 0)
            {
                int res = pipe_init_msg(handle, MAX_QUEUE);
                if (res == 0)
                {
                    res = pipe_connect(handle, remoteHandle);
                    if (res == 0)
                    {
                        // notify the other side this connection must be opened or closed!
                        int res = notifyOtherSide(handle, cmd);
                        if (res >= 0)
                        {
                            dlog_msg("%s channel to task %s success: now wait for response!", cmd == CMD_CLOSE_CONNECTION ? "Closing": "Opening", task.c_str());
                            // Wait for answer!!!!!
                            if (cmd == CMD_CLOSE_CONNECTION) res = waitForOtherSide(currentHandle, timeout, task);
                            else res = waitForOtherSide(handle, timeout, task);
                            if (res >= 0)
                            {
                                dlog_msg("Connection successful, new handle %d!", handle);
                                result = com_verifone_ipc::IPC_SUCCESS;
                            }
                            else
                            {
                                dlog_error("Waiting for task %s failed (timeout)!", task.c_str());
                            }
                        }
                        else
                        {
                            dlog_error("Cannot notify other side: error %d, errno %d", res, errno);
                        }
                    }
                    else
                    {
                        dlog_error("Error connecting pipes! Error %d, errno %d", res, errno);
                    }
                }
                else
                {
                    dlog_error("Error initialising the pipe! Error %d, errno %d", res, errno);
                }

                // Make sure we have successfully completed the task - if not, we have to close the handle (which is valid here)
                if (result != com_verifone_ipc::IPC_SUCCESS)
                {
                    dlog_msg("Closing unnecessary handle %d", handle);
                    close(handle);
                }

            }
            else
            {
                dlog_error("Cannot open communication channel for task %s - error %d, errno %d", task.c_str(), handle, errno);
            }
        }
        else
        {
            dlog_error("Remote handle for task %s doesn't exist (error %d, errno %d)", task.c_str(), remoteHandle, errno);
        }
        return result;
    }
    
    int ThreadData::addEventToLocalMap(const std::string & taskName, int handle)
    {
        using namespace com_verifone_pml;
        int eventRes = event_ctl(eventHandler, ctl::ADD, ipc_internal_pml::getEventItem(handle));
        dlog_msg("Event result %d, added handle %d for task %s", eventRes, handle, taskName.c_str()); // if < 0 -> error
        int singleEventHandle = event_open();
        eventHandleMap.insert( make_pair(taskName, singleEventHandle) );
        eventRes = event_ctl(singleEventHandle, ctl::ADD, ipc_internal_pml::getEventItem(handle));
        dlog_msg("Local event result %d, new handle %d", eventRes, singleEventHandle); // if < 0 -> error
        return 0;
    }
    
    int ThreadData::removeEventFromLocalMap(const std::string & taskName, int handle)
    {
        using namespace com_verifone_pml;
        int eventRes = event_ctl(eventHandler, ctl::DEL, ipc_internal_pml::getEventItem(handle));
        dlog_msg("Event result %d, deleted handle %d for task %s", eventRes, handle, taskName.c_str()); // if < 0 -> error
        handleMapper::iterator itFind = eventHandleMap.find(taskName);
        if (itFind != eventHandleMap.end())
        {
            int singleEventHandle = itFind->second;
            event_close(singleEventHandle);
            eventHandleMap.erase(itFind);
        }
        return 0;
    }

    int ThreadData::getTaskPipeHandle(const std::string &task) const
    {
        std::string name("P:");
        name.append(task);
        int taskID = 0;
        // checks if such pipe is already opened
        return get_owner(name.c_str(), &taskID);
    }

} // namespace


