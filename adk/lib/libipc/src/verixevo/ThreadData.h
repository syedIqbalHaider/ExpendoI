/*****************************************************************************
 * 
 * Copyright (C) 2007 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file        ThreadData.h
 *
 * @author      Kamil Pawlowski
 * 
 * @brief       Private IPC library class declaration
 *              This class will handle all the IPC operations
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C Coding  
 *              Standard 1.0.0 
 */


#ifndef _IPC_THREADDATA_H_
#define _IPC_THREADDATA_H_

#ifndef __cplusplus
#error "C++ only!"
#endif

#include <string>
#include <map>

#include <assert.h>

#include "libipc/ipc_handle.h"
// PML library

namespace ipc_thread_data
{
    class ThreadData;
    typedef std::map<std::string, int> handleMapper;
    typedef std::map<int, ThreadData *> threadMapper;

    // Singleton class!
    class ThreadData
    {
        public:
            static ThreadData * getThreadData();

            ~ThreadData();
            // Methods
            // const std::string & getMyName() const { return ourName; }
            int init(const std::string & task, const int timeout);
            int deinit(const int timeout);
            bool isChannelOpened(const std::string & task) const;
            int openChannel(const std::string & task, const int timeout);
            int closeChannel(const std::string & task, const int timeout);
            int send(const std::string & buffer, const std::string & destination) const;
            int checkPending(const std::string &source, std::string &from);
            int receive(std::string &buffer, const std::string source) const;
            void setSendRetryParameters(size_t newSendRetryCnt, size_t newSendRetryWaitTime) 
                { sendRetryCount = newSendRetryCnt; sendRetryWaitTime = newSendRetryWaitTime; }
            com_verifone_ipc::ipc_handle getEventsHandler(const std::string &task) const;
            int getTaskPipeHandle(const std::string &task) const;

        private:
            static threadMapper threadsMap;

            // Variables
            handleMapper handleMap;
            //const std::string ourName;
            //const int ourHandle;
            std::string ourName;
            int ourHandle;
            int eventHandler;
            handleMapper eventHandleMap;

            std::size_t sendRetryCount;
            std::size_t sendRetryWaitTime;

            // Methods
            ThreadData(); // Private constructor
            std::string fillOutName(const std::string &task_name);
            int openOurPipe();
            int getHandle(const std::string & task) const;
            int registerNewSource();
            int receiveTaskData(int handle, unsigned char & cmd, std::string & taskName, int & taskHandle);
            int sendChunk(int handle, const int fullSize, const std::string & buffer, const std::string & destination) const;
            int getInteger(const char * buf) const;
            int notifyOtherSide(const int handle, const unsigned char cmd) const;
            int waitForOtherSide(const int handle, const int timeout, const std::string & task);
            int connectToTask(const std::string &task, const unsigned char cmd, const int currentHandle, int &handle, const int timeout);
            int createConnection(const int taskHandle, const std::string & taskName);
            int closeConnection(const int taskHandle, const std::string & taskName);
            int addEventToLocalMap(const std::string & taskName, int handle);
            int removeEventFromLocalMap(const std::string & taskName, int handle);
    };
} // namespace


#endif /* _IPC_THREADDATA_H_ */

