/*
 * =====================================================================================
 *
 *       Filename:  ipc.cpp
 *
 *    Description: IPC procedure for MX platform
 *
 *        Version:  1.0
 *        Created:  21.01.2013 12:45:43
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lucjan Bryndza
 *   Organization:  Verifone
 *
 * =====================================================================================
 */

#include <libipc/ipc.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <cstring>
#include <errno.h>
#include <unistd.h>
#include <map>
#include <sys/epoll.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <fcntl.h>
#include <string>
#include <stdint.h>
#include <liblog/logsys.h>

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

using std::cout;
using std::endl;

/* ------------------------------------------------------------- */
namespace com_verifone_ipc
{

uint64_t gettid() {
    pthread_t ptid = pthread_self();
    uint64_t threadId = 0;
    memcpy(&threadId, &ptid, std::min(sizeof(threadId), sizeof(ptid)));
    return threadId;
}

/* ------------------------------------------------------------- */
namespace       //Unnamed namespace
{
    //Virtal socket path
    static const char vsock_path[] = "/tmp/vfi/";
    //MAX message size
    static const size_t max_message_size = 4 * 1024 * 1024;

	class IPCGlob
	{
		public:
		IPCGlob() : listen_sock(-1), epoll_fd(-1) {};
	    //Connected handles
    	int listen_sock;
	    //Epoll fd socked group
    	int epoll_fd;
    	
	    //Application task name
	    std::string my_app_name;
	    
	    //Store fd to name map
	    std::map<int, std::string> sock_fd_map;
    	std::map<std::string, int> sock_id_map;
	};
	
    //IPC header
    struct ipc_hdr
    {
        static const uint32_t MAGIC_HDR = 0x19790822;
        static const uint32_t START_HDR = 0x19790823;
        ipc_hdr(uint32_t _msg_len=0, uint32_t _msg_hdr=MAGIC_HDR)
            : msg_hdr(_msg_len>0?_msg_hdr:0), msg_len(_msg_len)
        {
        }
        uint32_t msg_hdr;
        uint32_t msg_len;
    };
    
	std::map<pid_t, boost::shared_ptr<IPCGlob> > IPCGlobMapper;
    
}   //NS end
/* ------------------------------------------------------------- */
namespace   //Function namespace
{
/* ------------------------------------------------------------- */
//Make socket nonblocking
int change_sock_flags( int fd, bool set, int flags )
{
    int ret;
    int gflags = fcntl( fd, F_GETFL, 0 );
    if( gflags == -1 )
    {
        return IPC_ERROR_SYSTEM;
    }
    if(set) flags |= flags;
    else flags &= ~flags;
    ret = fcntl( fd, F_SETFL, flags );
    if (ret == -1)
    {
        return IPC_ERROR_SYSTEM;
    }
    return IPC_SUCCESS;
}
/* ------------------------------------------------------------- */
/* Receive exactly required length */
int receive_len( int socket, void *buffer, size_t len, int flags = 0 )
{
    char *sptr = static_cast<char*>( buffer );
    int ret;
    size_t p;
    for( p=0; p<len; ) 
    {
        ret = ::recv( socket, sptr, len - p, flags );
        if( ret <=0 )
        {
            return ret;
        }
        else
        {
            p+=ret;
            sptr+=ret;
        }
    }
    return p;
}
/* ------------------------------------------------------------- */
//Handle connection destroy
int handle_disconnect( int fd )
{
    int ret = IPC_SUCCESS;
    if( epoll_ctl(IPCGlobMapper[gettid()]->epoll_fd, EPOLL_CTL_DEL, fd, NULL) == -1 )
    {
        ret = IPC_ERROR_SYSTEM;
    }
    close(fd);
    IPCGlobMapper[gettid()]->sock_id_map.erase(IPCGlobMapper[gettid()]->sock_fd_map[fd]);
    IPCGlobMapper[gettid()]->sock_fd_map.erase(fd);
    return ret;
}
/* ------------------------------------------------------------- */
//Handle connection (accept it)
int handle_connect()
{
    epoll_event event;
    int fd, ret;
    std::string app_name;
    if( (fd=accept(IPCGlobMapper[gettid()]->listen_sock, NULL, NULL)) == -1 )
    {
        return IPC_ERROR_CREATE_QUEUE;
    }
    ipc_hdr hdr;
    ret = receive_len(fd, &hdr , sizeof(hdr));
    if( ret <= 0 ) 
    {
        handle_disconnect( fd );
        return IPC_ERROR_CREATE_QUEUE;
    }
    if( hdr.msg_hdr != ipc_hdr::START_HDR )
    {
        handle_disconnect( fd );
        return IPC_ERROR_CREATE_QUEUE;
    }
    app_name.resize( hdr.msg_len );
    ret = receive_len( fd, &app_name[0], hdr.msg_len );
    if( ret <= 0 ) 
    {
        handle_disconnect( fd );
        return IPC_ERROR_CREATE_QUEUE;
    }
    event.data.fd = fd;
    event.events = EPOLLIN;
    if( epoll_ctl(IPCGlobMapper[gettid()]->epoll_fd, EPOLL_CTL_ADD, fd, &event ) == -1 )
    {
        close(fd);
        return IPC_ERROR_CREATE_QUEUE;
    }
    //Add name to socket map
    IPCGlobMapper[gettid()]->sock_fd_map[fd] = app_name;
    IPCGlobMapper[gettid()]->sock_id_map[app_name] = fd;
    return IPC_ERROR_NO_DATA_PENDING;   
}
/* ------------------------------------------------------------- */
int epoll_check_pending(const std::string &source_task, int *eventfd = NULL, std::string *pending_task = NULL )
{
    //Call poll with imadietly return value 
    int tret = IPC_ERROR_NO_DATA_PENDING;
    static const int max_events = 64;
    epoll_event events[max_events];
    const int ndescs = epoll_wait(IPCGlobMapper[gettid()]->epoll_fd, events, max_events, 0);
    if( ndescs == -1 )
    {
        dlog_error("Epoll error %i", ndescs);
        return IPC_ERROR_SYSTEM;
    }
    for(int n = 0; n < ndescs; ++n)
    {
       if( IPCGlobMapper[gettid()]->listen_sock == events[n].data.fd )
       {
            int ret;
            if( events[n].events & (EPOLLERR | EPOLLHUP) )
            {
               tret =  IPC_ERROR_SYSTEM;
            }
            if( (ret=handle_connect()) != IPC_ERROR_NO_DATA_PENDING )
            {
                if(tret!= IPC_SUCCESS) tret = ret;
                dlog_msg("Connect error %i", ret);
            }
       }
       else     //Events from other fds
       {    
            if( (events[n].events & (EPOLLERR|EPOLLHUP)) || !(events[n].events & EPOLLIN) )
            {
                int ret =  handle_disconnect( events[n].data.fd );
                if(tret!= IPC_SUCCESS && ret) tret = ret;
                dlog_msg("Disconnection event fd: [%02x]", unsigned(events[n].events));
            }
            else
            {
               if(source_task.empty())  //Any fdinput ok
               {
                   tret = IPC_SUCCESS;
                   if(eventfd) *eventfd = events[n].data.fd;
                   if(pending_task)
                   {
                        std::map<int,std::string>::const_iterator it = IPCGlobMapper[gettid()]->sock_fd_map.find(events[n].data.fd);
                        if( it != IPCGlobMapper[gettid()]->sock_fd_map.end() )
                            *pending_task = it->second;
                        else
                            dlog_error("Unable to find pending task %i", 0);
                   }
                   break;
               }
               else     //Check task from selected fdinput
               {
                    //If fd match
                    std::map<std::string, int>::const_iterator it = IPCGlobMapper[gettid()]->sock_id_map.find(source_task);
                    if( it != IPCGlobMapper[gettid()]->sock_id_map.end() )
                    {
                        if (it->second == events[n].data.fd)
                        {
                            tret = IPC_SUCCESS;
                            if(eventfd) *eventfd = it->second;
                            if(pending_task) *pending_task = it->first;
                            break;
                        }
                    }
                    else
                    {
                        tret = IPC_ERROR_SYSTEM;
                        dlog_error("Unable to find descriptor");
                    }
               }
               // break;
            }
       }
    }
    return tret;
}
/* ------------------------------------------------------------- */
}   //NS end
/* ------------------------------------------------------------- */
/*
* Sets up the library.
*
* Returns:
* 0 on success. Otherwise error code is returned.
*
* Parameters:
* In: task_name - The unique application name 
*
* Notes:
* Must be called only once, at application startup. 
*
*/

namespace
{
    bool delete_pid(pid_t pid)
    {
        std::map<pid_t, boost::shared_ptr<IPCGlob> >::iterator it = IPCGlobMapper.find(gettid());
        if (it != IPCGlobMapper.end())
        {
            close(IPCGlobMapper[pid]->listen_sock);
            close(IPCGlobMapper[pid]->epoll_fd);
            IPCGlobMapper[pid]->listen_sock = -1;
            IPCGlobMapper[pid]->epoll_fd = -1;
            IPCGlobMapper.erase(it);
            return true;
        }
        return false;
    }
}

int init(const std::string &task_name, int timeout)
{
    pid_t pid = gettid();

    dlog_msg("ipc_init name=%s pid=%ld", task_name.c_str(), pid);

    if( IPCGlobMapper.find( pid ) == IPCGlobMapper.end() )
    {
        IPCGlobMapper[pid] = boost::make_shared<IPCGlob>();
    }

    epoll_event event;
    sockaddr_un local;
    if( IPCGlobMapper[pid]->epoll_fd != -1 )
    {
        if( IPCGlobMapper[pid]->listen_sock != -1 )
            return IPC_SUCCESS;
        else
            return IPC_ERROR_SYSTEM;
    }
    if((IPCGlobMapper[pid]->epoll_fd = epoll_create1(0)) == -1)
    {
        return IPC_ERROR_SYSTEM;
    }
    if ((IPCGlobMapper[pid]->listen_sock = socket(AF_UNIX, SOCK_STREAM, 0)) == -1)
    {
        return IPC_ERROR_SYSTEM;
    }
    local.sun_path[0] = '\0';
    local.sun_family = AF_UNIX;
    std::strncpy(local.sun_path+1, vsock_path, sizeof(local.sun_path)-sizeof('\0')-sizeof('\0'));
    if( task_name.empty() )
        std::strncat(local.sun_path+1, program_invocation_short_name, sizeof(local.sun_path)-sizeof('\0')-sizeof('\0'));
    else
        std::strncat( local.sun_path+1, task_name.c_str(), sizeof(local.sun_path)-sizeof('\0')-sizeof('\0') );
	IPCGlobMapper[pid]->my_app_name = task_name.empty()?program_invocation_short_name:task_name;
    if( bind(IPCGlobMapper[pid]->listen_sock, reinterpret_cast<sockaddr*>(&local), sizeof(local.sun_family) + strlen(local.sun_path+1)+1) == -1 )
    {
        delete_pid(pid);
        return IPC_ERROR_SYSTEM;
    }
    if( listen(IPCGlobMapper[pid]->listen_sock, SOMAXCONN) == -1 )
    {
        delete_pid(pid);
        return IPC_ERROR_SYSTEM;
    }
    event.events = EPOLLIN;
    event.data.fd = IPCGlobMapper[pid]->listen_sock;
    if( epoll_ctl(IPCGlobMapper[pid]->epoll_fd, EPOLL_CTL_ADD, IPCGlobMapper[pid]->listen_sock, &event) == -1 )
    {
        delete_pid(pid);
        return IPC_ERROR_SYSTEM;
    }
    return IPC_SUCCESS;
}

int deinit(int timeout)
{
    // it may never be initialized, so we have to check if IPCGlobMapper contains entry for current pid
    delete_pid(gettid());
    return IPC_SUCCESS;
}


/* ------------------------------------------------------------- */
/*
 * Checks if there are pending messages waiting in the RX queue of current application.
 *
 * Parameters:
 * In: source_task: Name of task, sending the message. If empty, checks for messages from any task.
 *
 * Returns:
 * true on success, meaning there is at least one message waiting to be read. False otherwise. 
 *
 */
int check_pending(const std::string &source_task)
{
    return epoll_check_pending( source_task );
}

/* ------------------------------------------------------------- */
/*
 * Starts and sets up the pipe connection to a named application.
 *
 * Returns:
 * 0 on success. Otherwise error code is returned.
 *
 * Parameters:
 * In: task_name - The unique application name 
 *
 * Notes:
 * Must be called before an application attempts to communicate with given task name. 
 *
 */
int connect_to(const std::string &task_name, int timeout)
{
	pid_t pid = gettid();

    
    sockaddr_un remote;
    epoll_event event;
    
	
    if (IPCGlobMapper.find(pid) == IPCGlobMapper.end())
    {
    	// if this entry is missing, it is very likely that init function for this pid wasn't called
    	dlog_error("Did you forget to call com_verifone_ipc::init()?");
//    	cout<<"Did you forget to call com_verifone_ipc::init()?"<<endl;

    	return IPC_ERROR_SYSTEM;
    }
    
    if(IPCGlobMapper[pid]->sock_id_map.find(task_name) != IPCGlobMapper[pid]->sock_id_map.end())
    {
        return IPC_SUCCESS;
    }
    
    
    int fd  = socket(AF_UNIX, SOCK_STREAM, 0);
    
    
    if( fd == -1 )
    {
//    	cout<<"socket Error"<<endl;
        return IPC_ERROR_SYSTEM;
    }
    remote.sun_family = AF_UNIX;
    remote.sun_path[0] = '\0';
    strncpy(remote.sun_path+1, (vsock_path +  task_name).c_str(), sizeof(remote.sun_path)-1);
    
    
    if( connect(fd, reinterpret_cast<sockaddr*>(&remote),sizeof(remote.sun_family) + strlen(remote.sun_path+1)+1) == -1 )
    {
        close(fd);
//        cout<<"connect Error"<<endl;
//        cout<<task_name.c_str()<<endl;
        return IPC_ERROR_SYSTEM;
    }
    std::string &my_app_name = IPCGlobMapper[pid]->my_app_name;

    ipc_hdr hdr( my_app_name.size(), ipc_hdr::START_HDR );
    if( ::send(fd, &hdr, sizeof(hdr),MSG_MORE) != int(sizeof(hdr)) )
    {
//    	cout<<"send Error"<<endl;
        return IPC_ERROR_SYSTEM;
    }
    if( ::send(fd, my_app_name.c_str(), my_app_name.size(), 0 ) != int(my_app_name.size()) )
    {
//    	cout<<"send Error2"<<endl;
        return IPC_ERROR_SYSTEM;
    }
    
    event.data.fd = fd;
    event.events = EPOLLIN;
    if( epoll_ctl(IPCGlobMapper[pid]->epoll_fd, EPOLL_CTL_ADD, fd, &event) == -1 )
    {
        close(fd);
//        cout<<"epoll_ctl Error"<<endl;
        return IPC_ERROR_SYSTEM;
    }
    
    IPCGlobMapper[pid]->sock_fd_map[fd] = task_name;
    IPCGlobMapper[pid]->sock_id_map[task_name] = fd;
    // dlog_msg("Connected to task '%s', handle %d", task_name.c_str(), fd);
    return IPC_SUCCESS;
}
/* ------------------------------------------------------------- */
/*
 * Closes pipe connection to a named application.
 *
 * Returns:
 * 0 on success. Otherwise error code is returned.
 *
 * Parameters:
 * In: task_name - The unique application name 
 *
 *
 */
int disconnect(const std::string &task_name, int timeout)
{
    int ret = IPC_SUCCESS;
	pid_t pid = gettid();
    //If fd match
    std::map<std::string,int>::iterator it = IPCGlobMapper[pid]->sock_id_map.find(task_name);
    if( it == IPCGlobMapper[pid]->sock_id_map.end() )
    {
        return IPC_ERROR_INVALID_PARAMETERS;
    }
    if( epoll_ctl(IPCGlobMapper[pid]->epoll_fd, EPOLL_CTL_DEL, it->second, NULL) == -1 )
    {
        ret = IPC_ERROR_SYSTEM;
    }
    close(it->second);
    IPCGlobMapper[pid]->sock_fd_map.erase(it->second);
    IPCGlobMapper[pid]->sock_id_map.erase(it);
    return ret;
}
/* ------------------------------------------------------------- */

/*
 * Send a message to another application.
 * 
 * Returns:
 * 0 on success. Otherwise error code is returned.
 *
 * Parameters:
 * In: buffer - the message to be sent. Length must be nonzero. 
 *
 *
 * In: destination_task - destination application name
 * 
 * Notes:
 * Function simply sends data, passed in buffer object, to destination task. 
 * Successful send doesn't imply successful message delivery!
 *
 */
int send(const std::string &buffer, const std::string &destination_task)
{
    if( buffer.empty() )
    {
        dlog_msg("Buffer is empty. No data to send");
        return IPC_ERROR_SEND;
    }
    std::map<std::string,int>::iterator it = IPCGlobMapper[gettid()]->sock_id_map.find(destination_task);
	if( it == IPCGlobMapper[gettid()]->sock_id_map.end() )
    {
        dlog_error("Invalid destination task [%s]\n", destination_task.c_str());
        return IPC_ERROR_INVALID_PARAMETERS;
    }
    ipc_hdr hdr(  buffer.size() );
    dlog_msg("Message send to [%s] len [%i]", destination_task.c_str(), hdr.msg_len);
    if( send(it->second, &hdr ,sizeof(hdr), MSG_MORE) != sizeof(hdr) )
    {
        return IPC_ERROR_SYSTEM;
    }
    for(size_t p = 0; p < hdr.msg_len;)
    {
        const int ret = ::send(it->second, &buffer[p], hdr.msg_len-p, 0);
        if( ret == -1 )
        {
            dlog_error("Write error");
            return IPC_ERROR_SYSTEM;
        }
        p += ret;
    }
    return IPC_SUCCESS;
}
/* ------------------------------------------------------------- */

/*
 * Check for a message on the application's message queue. 
 * Returns the message received (if any) or error code.
 *
 * Returns:
 * 0 on success. Otherwise error code is returned.
 *
 * Parameters:
 * Out: buffer - received message
 *
 * In: source_task - Name of application which originated the message. If empty, function receives from any application. 
 *
 * Out: received_from - Originator of the message (application name)
 * 
 * Notes:
 * ! Function is non-blocking !
 * If some data is pending from requested application (defined by source_task data), it is returned. 
 * If source_data is empty, receives data from any application. 
 * In case no data is available, error code is returned. 
 *
 */
int receive(std::string &buffer, const std::string &source_task, std::string &received_from)
{

    int fd, toread;
    ipc_hdr hdr;
    int ret =  epoll_check_pending(source_task, &fd, &received_from );
    if( ret != IPC_SUCCESS )
    {
//        dlog_error("Check pending error [%i]", ret);
        return ret;
    }
   
    if( ioctl( fd, FIONREAD, &toread ) == -1 )
    {
        dlog_error("IOCTL error [%i]", errno);
        return IPC_ERROR_SYSTEM;
    }
    if( toread == 0  )
    {    
        dlog_msg("No pending data - requested len [%i]", toread);
        return IPC_ERROR_NO_DATA_PENDING;
    }
    
    ret = receive_len(fd, &hdr, sizeof(hdr));
    if( ret<=0)
    {
        handle_disconnect(fd);
        dlog_error("Nothing received, ret %i!!!", ret);
        return IPC_ERROR_RECEIVE;
    }
    if( hdr.msg_hdr != ipc_hdr::MAGIC_HDR )
    {
        dlog_error("Invalid header !!! %i", hdr.msg_hdr );
        return IPC_ERROR_RECEIVE;
    }
    if( hdr.msg_len == 0 || hdr.msg_len > max_message_size )
    {
        dlog_error("receive size error [%i] toread [%i]", errno, hdr.msg_len % toread );
        return IPC_ERROR_RECEIVE;
    }
    dlog_msg("Got message from [%s] with len [%i]",received_from.c_str(), hdr.msg_len);
    buffer.resize(hdr.msg_len);
    ret = receive_len(fd, &buffer[0], hdr.msg_len );
    if( ret <= 0)
    {
        dlog_error( "Receive failed [%i]", errno );
        return IPC_ERROR_RECEIVE;
    }
    return IPC_SUCCESS;
}
/* ------------------------------------------------------------- */
/* Get event handler. Handler is specific to the operation system event wait 
*   Parmaeters:
    Returns:
        OS specific event handler
*/
ipc_handle get_events_handler( const std::string &source_task )
{
    if( source_task.empty() )
    {
        return ipc_handle(IPCGlobMapper[gettid()]->epoll_fd);
    }
    else
    {
        std::map<std::string, int>::const_iterator it = IPCGlobMapper[gettid()]->sock_id_map.find( source_task );
        if( it == IPCGlobMapper[gettid()]->sock_id_map.end() )
        {
            return ipc_handle(IPC_ERROR_INVALID_PARAMETERS);
        }
        else
        {
            return ipc_handle(it->second);
        }
    }
}
/* ------------------------------------------------------------- */
/*
 * Checks whether an application is available for IPC communication
 * Parameters:
 * In: task: Name to be checked for availability
 *
 * Returns:
 * 1 if task is available, 0 when it isn't, negative value on error
 */
int is_task_available(const std::string &task)
{
	std::map<std::string,int>::iterator it = IPCGlobMapper[gettid()]->sock_id_map.find(task);
	if (it != IPCGlobMapper[gettid()]->sock_id_map.end())
		return 1;

    sockaddr_un local;
    int fd = -1;
    
    if ((fd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1)
    {
        return IPC_ERROR_SYSTEM;
    }
    local.sun_path[0] = '\0';
    local.sun_family = AF_UNIX;
    std::strncpy(local.sun_path+1, vsock_path, sizeof(local.sun_path)-sizeof('\0')-sizeof('\0'));
    std::strncat( local.sun_path+1, task.c_str(), sizeof(local.sun_path)-sizeof('\0')-sizeof('\0') );

    int ret = connect(fd, reinterpret_cast<sockaddr*>(&local),sizeof(local.sun_family) + strlen(local.sun_path+1)+1);
	
    close(fd);
    return ret != -1;
	
}
/* ------------------------------------------------------------- */
}

/* ------------------------------------------------------------- */


