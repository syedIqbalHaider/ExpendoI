 /*
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file      LibIPC.h
 *
 * @author    Kamil Pawlowski
 *
 * @brief     IPC (Internal Process Communication) library definitions
 *
 * @remarks   This file should be compliant with Verifone EMEA R&D C Coding
 *            Standard 1.0.0
 */


#ifndef LIBIPC_H_
#define LIBIPC_H_


#ifdef __cplusplus


#include <string>
#include <list>
#include "ipc_handle.h"

namespace com_verifone_ipc
{
    enum IPC_errors
    {
        IPC_SUCCESS = 0,
        IPC_ERROR_INVALID_PARAMETERS = -1,
        IPC_ERROR_NO_DATA_PENDING = -2,
        IPC_ERROR_CREATE_QUEUE = -3,
        IPC_ERROR_DESTROY_QUEUE = -4,
        IPC_ERROR_SEND = -5,
        IPC_ERROR_RECEIVE = -6,
        IPC_ERROR_SYSTEM  = -7
    };

    static const int CHANNEL_TIMEOUT = 10000;
	

    /*
     * Sets up the library.
     *
     * Returns:
     * 0 on success. Otherwise error code is returned.
     *
     * Parameters:
     * In: task_name    - The unique application name
     *     timeout      - Timeout (in milliseconds) for initialisation
     *
     * Notes:
     * Must be called only once, at application startup. 
     *
     */

    int init(const std::string &task_name, int timeout = CHANNEL_TIMEOUT);

    /*
     * Performs library cleanup. 
     * To be used only when task / thread exits. 
     *
     * Returns:
     * 0 on success. Otherwise error code is returned.
     *
     * Parameters:
     * In: timeout      - Timeout (in milliseconds) for initialisation
     *
     * Notes:
     * Must be called only once, at application end. 
     *
     */

    int deinit(int timeout = CHANNEL_TIMEOUT);

    /*
     * Starts and sets up the pipe connection to a named application.
     *
     * Returns:
     * 0 on success. Otherwise error code is returned.
     *
     * Parameters:
     * In: task_name    - The unique application name 
     *     timeout      - Timeout (in milliseconds) for connection establishment.      
     *                    In case the other side won't respond within that timeout, connection is not opened. 
     *                    The timeout prevents theoretical deadlock (badly written application may never respond). 
     *
     * Notes:
     * Must be called before an application attempts to communicate with given task name. 
     * Function opens communication channel between current application and application, identified as task_name. 
     *
     */
    int connect_to(const std::string &task_name, int timeout = CHANNEL_TIMEOUT);

    /*
     * Closes pipe connection to a named application.
     *
     * Returns:
     * 0 on success. Otherwise error code is returned.
     *
     * Parameters:
     * In: task_name - The unique application name 
     *
     *
     */
    int disconnect(const std::string &task_name, int timeout = CHANNEL_TIMEOUT);

    /*
     * Send a message to another application.
     *
     * Returns:
     * 0 on success. Otherwise error code is returned.
     *
     * Parameters:
     * In: buffer - the message to be sent. Length must be nonzero. 
     *     timeout      - Timeout (in milliseconds) for connection closure. 
     *                    In case the other side won't respond within that timeout, connection is not closed. 
     *                    The timeout prevents theoretical deadlock (badly written application may never respond). 
     *
     *
     * In: destination_task - destination application name
     * 
     * Notes:
     * Function simply sends data, passed in buffer object, to destination task. 
     * Successful send doesn't imply successful message delivery!
     *
     */
    int send(const std::string &buffer, const std::string &destination_task);

    /*
     * Check for a message on the application's message queue. 
     * Returns the message received (if any) or error code.
     *
     * Returns:
     * 0 on success. Otherwise error code is returned.
     *
     * Parameters:
     * Out: buffer - received message
     *
     * In: source_task - Name of application which originated the message. If empty, function receives from any application. 
     *
     * Out: received_from - Originator of the message (application name)
     * 
     * Notes:
     * ! Function is non-blocking !
     * If some data is pending from requested application (defined by source_task data), it is returned. 
     * If source_data is empty, receives data from any application. 
     * In case no data is available, error code is returned. 
     *
     */
    int receive(std::string &buffer, const std::string &source_task, std::string &received_from);

    /*
     * Checks if there are pending messages waiting in the RX queue of current application.
     *
     * Parameters:
     * In: source_task: Name of task, sending the message. If empty, checks for messages from any task.
     *
     * Returns:
     * true on success, meaning there is at least one message waiting to be read. False otherwise. 
     *
     */
    int check_pending(const std::string &source_task);

    /*
     * Checks whether an application is available for IPC communication
     * Parameters:
     * In: task: Name to be checked for availability
     *
     * Returns:
     * 1 if task is available, 0 when it isn't, negative value on error
     */
     int is_task_available(const std::string &task);


    /*
     * Get event handler (used for events handling routines, LIBPML) 
     *  Parameters:
     *
     * Returns:
     *  Event handler to be used with PML library
     */
     ipc_handle get_events_handler(const std::string &source_task = "");

} // namespace ends

#else

#error "C interface is not yet defined!"

#endif // __cplusplus

#endif // __LIBIPC_H__
