#ifndef _LIBIPC_IPC_HANDLE_H
#define _LIBIPC_IPC_HANDLE_H
namespace com_verifone_ipc
{

	/* PML handle wrapper */
	class ipc_handle
	{
	public:
		explicit ipc_handle( int handle )
			: m_handle( handle )
		{}
		int operator()() const
		{
			return m_handle;
		}
		int get_raw() const
		{
			return m_handle;
		}
		bool is_valid() const
		{
			return m_handle >= 0;
		}
		bool is_invalid() const
		{
			return !is_valid();
		}
	private:
		const int m_handle;
	};

}
#endif
