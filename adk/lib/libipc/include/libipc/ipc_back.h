 /*
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file      LibIPC_back.h
 *
 * @author    Kamil Pawlowski
 *
 * @brief     Backwards compatibility version of IPC (Internal Process Communication) library definitions
 *
 * @remarks   This file should be compliant with Verifone EMEA R&D C Coding
 *            Standard 1.0.0
 */


#ifndef LIBIPC_BACK_H_
#define LIBIPC_BACK_H_


#ifdef __cplusplus


#include "ipc.h"

inline int ipc_init(const std::string &task_name, int timeout = com_verifone_ipc::CHANNEL_TIMEOUT)
{
    return com_verifone_ipc::init(task_name, timeout);
}

inline int ipc_deinit(int timeout = com_verifone_ipc::CHANNEL_TIMEOUT)
{
    return com_verifone_ipc::deinit(timeout);
}

inline int ipc_connect_to(const std::string &task_name, int timeout = com_verifone_ipc::CHANNEL_TIMEOUT)
{
    return com_verifone_ipc::connect_to(task_name, timeout);
}

inline int ipc_disconnect(const std::string &task_name, int timeout = com_verifone_ipc::CHANNEL_TIMEOUT)
{
    return com_verifone_ipc::disconnect(task_name, timeout);
}

inline int ipc_send(const std::string &buffer, const std::string &destination_task)
{
    return com_verifone_ipc::send(buffer, destination_task);
}

inline int ipc_receive(std::string &buffer, const std::string &source_task, std::string &received_from)
{
    return com_verifone_ipc::receive(buffer, source_task, received_from);
}

inline int ipc_check_pending(const std::string &source_task)
{
    return com_verifone_ipc::check_pending(source_task);
}

inline int ipc_is_task_available(const std::string &task)
{
    return com_verifone_ipc::is_task_available(task);
}


inline int ipc_get_events_handler()
{
    return com_verifone_ipc::get_events_handler();
}

#else

#error "C interface is not yet defined!"

#endif // __cplusplus

#endif // LIBIPC_BACK_H_
