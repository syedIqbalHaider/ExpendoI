/* CiR - 13May2003. */

#if defined(VERIXV)
#include <svc.h>
#endif
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>

#include "libber-tlv/ber-tlv.h"
#include "liblog/logsys.h"

static TLV_CALLOC pfnMemCalloc = calloc;
static TLV_FREE pfnMemFree = free;


void vTLV_set_calloc(TLV_CALLOC pfnCalloc, TLV_FREE pfnFree)
{
	if (pfnCalloc && pfnFree)
	{
		pfnMemCalloc = pfnCalloc;
		pfnMemFree = pfnFree;
	}
}


/*
 * Interpret a string of bytes as an ISO8825 tag specifier.
 *
 * Returns,
 * Status of the interpretation.
 *
 * Parameters.
 * pstTLV Out.
 *   Tag-length-value-triplet-containing structure.  Tag length field used to
 *   indicate whether tag is valid.  It will be non-zero for a valid tag.
 *
 * pulnBufUsed Out.
 *  *pulnBufUsed is the number of bytes of the buffer used to make the tag.
 *  *pulnBufUsed is undefined if not successful.
 *
 * pbyBuf In.
 *   pbyBuf points to the start of the buffer.
 *
 * ulnBufLen In.
 *   Length of buffer (pointed to by pbyBuf) available.
 */
ERROR_CODE usnTLV_get_BER_tag
(
	BER_TLV *pstTLV,
	u_long *pulnBufUsed,
	u_char const *pbyBuf,
	u_long ulnBufLen
)
{
	ERROR_CODE usnEC = ERR_BER_TLV_BUFFER_OVERRUN;
	u_char byTagLen = 0;
	u_char bMore = TRUE;
	u_long ulnBufRemaining = ulnBufLen;

	/* Check if buffer has any data remaining. */
	if (ulnBufRemaining--)
	{
		/* Copy first byte. */
		pstTLV->abyTag[byTagLen++] = *pbyBuf++;

		/* If the five LSBs are set, the tag is extended. */
		if ((pstTLV->abyTag[0] & BER_TLV_TAG_MASK_NUMBER) ==
			BER_TLV_TAG_MASK_NUMBER)
		{
			/* Copy all extended tag bytes into the tag. */
			while (bMore &&
				   ulnBufRemaining-- &&
				   byTagLen < sizeof pstTLV->abyTag)
			{
				pstTLV->abyTag[byTagLen++] = *pbyBuf;
				bMore = (*pbyBuf++ & BER_TLV_TAG_MASK_MORE) != 0;
			}

			if (bMore)
			{
				if (byTagLen >= sizeof pstTLV->abyTag)
				{
					usnEC = ERR_BER_TLV_FORMAT;
				}
			}
			else
			{
				usnEC = SUCCESS;
			}
		}
		else
		{
			/* The tag is complete. */
			usnEC = SUCCESS;
		}
	}

	if (usnEC == SUCCESS)
	{
		*pulnBufUsed = ulnBufLen - ulnBufRemaining;

		pstTLV->byTagLen = byTagLen;
	}
	else
	{
		pstTLV->byTagLen = 0;
	}

	return usnEC;
}


/*
 * Interpret a string of bytes as an ISO8825 length specifier.
 *
 * Returns,
 * Status of the interpretation.
 *
 * Parameters.
 * pulnLen Out.
 *   Length, only if return code is LEN_OK, otherwise undefined.
 *
 * pulnBufUsed Out.
 *  *pulnBufUsed is the number of bytes of the buffer used to make the length.
 *  *pulnBufUsed is undefined if not successful.
 *
 * pbyBuf In.
 *   Points to the start of the buffer.
 *
 * ulnBufLen In.
 *   Length of buffer (pointed to by pbyBuf) available.
 */
static LENGTH_STATUS eGetBERLength
(
	u_long *pulnLen,
	u_long *pulnBufUsed,
	u_char const *pbyBuf,
	u_long ulnBufLen
)
{
	LENGTH_STATUS eRC = LEN_INVALID;
	u_char byCurr, byLenLen;
	u_long ulnTmpLen = 0, ulnBufRemaining = ulnBufLen;

	/* Check if buffer has any data remaining. */
	if (ulnBufRemaining--)
	{
		byCurr = *pbyBuf++;
		byLenLen = byCurr & BER_TLV_LEN_MASK_LENGTH;

		if (byCurr & BER_TLV_LEN_MASK_MULTI)
		{
			/* Check length of length. */
			if (byLenLen)
			{
				/*
				 * Length of length is specified so check if it will fit in
				 * space allowed and that there is enough valid data in the
				 * buffer remaining with which to construct it.
				 */
				if (byLenLen <= sizeof *pulnLen && byLenLen <= ulnBufRemaining)
				{
					ulnBufRemaining -= byLenLen;

					while (byLenLen--)
					{
						/* MSBy first. */
						ulnTmpLen <<= 8;
						ulnTmpLen += *pbyBuf++;
					}

					*pulnLen = ulnTmpLen;
					eRC = LEN_OK;
				}
			}
			else
			{
				/* Length is indefinite in the ISO8825 sense. */
				eRC = LEN_INDEFINITE;
			}
		}
		else
		{
			/* Simple length between 0 and 127. */
			*pulnLen = (u_long) byLenLen;
			eRC = LEN_OK;
		}
	}

	*pulnBufUsed = ulnBufLen - ulnBufRemaining;

	return eRC;
}


BER_TLV *pstTLV_make_element(void)
{
	BER_TLV *pstNew;

	pstNew = pfnMemCalloc(1, sizeof *pstNew);
	assert(pstNew);

	return pstNew;
}


static ERROR_CODE _usnTLV_parse_buffer
(
	BER_TLV *pstTLV,
	u_char const *pbyBuf,
	u_long ulnBufLen,
	char parseChild
)
{
	ERROR_CODE usnEC = SUCCESS;
	u_long ulnBufUsed;
	LENGTH_STATUS eLenStat;

	while (ulnBufLen != 0 && usnEC == SUCCESS)
	{
		usnEC = usnTLV_get_BER_tag(pstTLV, &ulnBufUsed, pbyBuf, ulnBufLen);
		ulnBufLen -= ulnBufUsed;
		pbyBuf += ulnBufUsed;

		if (usnEC == SUCCESS)
		{
			eLenStat = eGetBERLength(&pstTLV->ulnLen,
									 &ulnBufUsed,
									 pbyBuf,
									 ulnBufLen);
			ulnBufLen -= ulnBufUsed;
			pbyBuf += ulnBufUsed;

			if (eLenStat == LEN_OK)
			{
				if (pstTLV->ulnLen)
				{
					if (ulnBufLen >= pstTLV->ulnLen)
					{
						if (parseChild && bIsConstructed(pstTLV->abyTag[0]))
						{
							pstTLV->pstChild = pstTLV_make_element();
							usnEC = usnTLV_parse_buffer(pstTLV->pstChild,
														pbyBuf,
														pstTLV->ulnLen);
						}
						else
						{
							pstTLV->pbyValue = pfnMemCalloc(1, pstTLV->ulnLen);
							assert(pstTLV->pbyValue);
							memcpy(pstTLV->pbyValue, pbyBuf, pstTLV->ulnLen);
						}

						ulnBufLen -= pstTLV->ulnLen;
						pbyBuf += pstTLV->ulnLen;
					}
					else
					{
						usnEC = ERR_BER_TLV_BUFFER_OVERRUN;
					}
				}
			}
			else if (eLenStat == LEN_INDEFINITE)
			{
				/* Indefinite lengths not supported yet. */
				usnEC = ERR_BER_TLV_FORMAT;
			}
			else
			{
				usnEC = ERR_BER_TLV_FORMAT;
			}

			if (usnEC == SUCCESS)
			{
				/* If there's more in the buffer, set up space to store it. */
				if (ulnBufLen != 0)
				{
					pstTLV->pstNext = pstTLV_make_element();
					pstTLV = pstTLV->pstNext;
				}
			}
		}
	}

	return usnEC;
}

ERROR_CODE usnTLV_parse_buffer(BER_TLV *pstTLV, u_char const *pbyBuf, u_long ulnBufLen)
{
	return _usnTLV_parse_buffer(pstTLV, pbyBuf, ulnBufLen, 0);
}

ERROR_CODE usnTLV_parse_buffer_ext(BER_TLV *pstTLV, u_char const *pbyBuf, u_long ulnBufLen)
{
	return _usnTLV_parse_buffer(pstTLV, pbyBuf, ulnBufLen, 1);
}



void vTLV_destroy_element(BER_TLV *pstEl)
{
	if (pstEl)
	{
		if (pstEl->pstChild)
		{
			vTLV_destroy_element(pstEl->pstChild);
		}

		if (pstEl->pstNext)
		{
			vTLV_destroy_element(pstEl->pstNext);
		}

		pfnMemFree(pstEl->pbyValue);
		pfnMemFree(pstEl);
	}
}


static void vHexPrint(u_char *pbyInf, u_long ulnLen)
{
	while (ulnLen--)
	{
#if defined (VERIXV)
		dlog_msg("%02X", *pbyInf++);
#else
		printf("%02X", *pbyInf++);
#endif
	}
}


void vTLV_print_element(BER_TLV *pstEl, u_char byLvl, u_char bFollow)
{
	u_char byI;

	if (pstEl)
	{
#if !defined (VERIXV)
		for (byI = byLvl * 4; byI; byI--)
		{
			putchar(' ');
		}
#endif
		vHexPrint(pstEl->abyTag, pstEl->byTagLen);
#if defined (VERIXV)
		dlog_msg("length %d", pstEl->ulnLen);
#else
		printf(" %lX ", pstEl->ulnLen);
#endif
		if (pstEl->pbyValue)
		{
#if defined (VERIXV)
		    dlog_msg("value");
#endif
			vHexPrint(pstEl->pbyValue, pstEl->ulnLen);
		}
#if defined (VERIXV)
		putchar('\n');
#endif
		vTLV_print_element(pstEl->pstChild, byLvl + 1, TRUE);

		if (bFollow)
		{
			vTLV_print_element(pstEl->pstNext, byLvl, TRUE);
		}
	}
}


ERROR_CODE usnTLV_flatten_element
(
	u_char *pbyBuf,		/* Out: Buffer for flattened output.	*/
	u_long *pulnBufUsed,	/* Out: Number of bytes of buffer used.	*/
	u_long ulnBufLen,	/* In : Size of buffer available.		*/
	BER_TLV *pstEl,		/* In : TLV tree to be flattened.		*/
	u_char bFollow		/* In : All same-level elements flag.	*/
)
{
	ERROR_CODE usnEC = SUCCESS;
	u_char byLenLen;
	u_long ulnLen, ulnBufRemaining = ulnBufLen;

	if (pstEl && pstEl->byTagLen)
	{
		ulnLen = pstEl->ulnLen;

		if (ulnBufRemaining)
		{
			memcpy(pbyBuf, pstEl->abyTag, pstEl->byTagLen);
			pbyBuf += pstEl->byTagLen;
			ulnBufRemaining -= pstEl->byTagLen;
		}
		else
		{
			usnEC = ERR_BER_TLV_BUFFER_OVERRUN;
		}

		if (usnEC == SUCCESS)
		{
			if (pstEl->ulnLen < 0x80)
			{
				if (ulnBufRemaining--)
				{
					*pbyBuf++ = (u_char) pstEl->ulnLen;
				}
				else
				{
					usnEC = ERR_BER_TLV_BUFFER_OVERRUN;
				}
			}
			else
			{
				/* Get number of significant bytes. */
				for (byLenLen = 1; ulnLen >>= 8; byLenLen++);

				if (ulnBufRemaining >= byLenLen + 1)
				{
					*pbyBuf++ = byLenLen | 0x80;
					ulnBufRemaining -= byLenLen + 1;

					while (byLenLen--)
					{
						*pbyBuf++ = (u_char) (pstEl->ulnLen >> 8 * byLenLen);
					}
				}
				else
				{
					usnEC = ERR_BER_TLV_BUFFER_OVERRUN;
				}
			}
		}

		if (usnEC == SUCCESS)
		{
			if (/*!bIsConstructed(pstEl->abyTag[0]) &&*/ pstEl->pbyValue)
			{
				if (ulnBufRemaining >= pstEl->ulnLen)
				{
					memcpy(pbyBuf, pstEl->pbyValue, pstEl->ulnLen);
					pbyBuf += pstEl->ulnLen;
					ulnBufRemaining -= pstEl->ulnLen;
				}
				else
				{
					usnEC = ERR_BER_TLV_BUFFER_OVERRUN;
				}
			}
			else
			{
				usnEC = usnTLV_flatten_element(pbyBuf,
											   pulnBufUsed,
											   ulnBufRemaining,
											   pstEl->pstChild,
											   TRUE);
				ulnBufRemaining -= *pulnBufUsed;
				pbyBuf += *pulnBufUsed;
			}
		}

		if (usnEC == SUCCESS && bFollow)
		{
			usnEC = usnTLV_flatten_element(pbyBuf,
										   pulnBufUsed,
										   ulnBufRemaining,
										   pstEl->pstNext,
										   TRUE);
			ulnBufRemaining -= *pulnBufUsed;
		}
	}

	*pulnBufUsed = ulnBufLen - ulnBufRemaining;

	return usnEC;
}


static BER_TLV *_pstTLV_find_element(BER_TLV *pstEl, const u_char *pbyTag, u_char byTagLen, char searchChild)
{
	u_char bFound = FALSE;

	while (pstEl && !bFound)
	{
		if (byTagLen == pstEl->byTagLen &&
			!memcmp(pstEl->abyTag, pbyTag, byTagLen))
		{
			bFound = TRUE;
		}
		else
		{
			if (searchChild != 0 && pstEl->pstChild != NULL)
			{
				BER_TLV *pstElChild = _pstTLV_find_element(pstEl->pstChild, pbyTag, byTagLen, searchChild);
				if (pstElChild != NULL) return pstElChild;
			}
			pstEl = pstEl->pstNext;
		}
	}

	return pstEl;
}

BER_TLV *pstTLV_find_element(BER_TLV *pstEl, const u_char *pbyTag, u_char byTagLen)
{
	return _pstTLV_find_element(pstEl, pbyTag, byTagLen, 0);
}

BER_TLV *pstTLV_find_element_ext(BER_TLV *pstEl, const u_char *pbyTag, u_char byTagLen)
{
	return _pstTLV_find_element(pstEl, pbyTag, byTagLen, 1);
}




BER_TLV *pstTLV_insert_element(BER_TLV *pstEl)
{
	BER_TLV *pstNew;

	pstNew = pstTLV_make_element();

	pstNew->pstNext = pstEl->pstNext;
	pstEl->pstNext = pstNew;

	return pstNew;
}


BER_TLV *pstTLV_append_element(BER_TLV *pstEl)
{
	BER_TLV *pstNew;

	pstNew = pstTLV_make_element();

	if (pstEl)
	{
		while (pstEl->pstNext)
		{
			pstEl = pstEl->pstNext;
		}

		pstEl->pstNext = pstNew;
	}

	return pstNew;
}


void vTLV_set_up_primitive_element
(
	BER_TLV *pstEl,
	const u_char *pbyTag,
	u_char byTagLen,
	u_char *pbyValue,
	u_long ulnLen
)
{
	if (pstEl && byTagLen <= sizeof pstEl->abyTag)
	{
		memcpy(pstEl->abyTag, pbyTag, byTagLen);
		pstEl->byTagLen = byTagLen;
		pstEl->ulnLen = ulnLen;
        if( ulnLen != 0 )
        {
    		pstEl->pbyValue = pfnMemCalloc(1, ulnLen);
    		assert(pstEl->pbyValue);
    		memcpy(pstEl->pbyValue, pbyValue, ulnLen);
        }
	}
}


BER_TLV *pstTLV_make_primitive_element
(
	const u_char *pbyTag,
	u_char byTagLen,
	u_char *pbyValue,
	u_long ulnLen
)
{
	BER_TLV *pstEl;

	pstEl = pstTLV_make_element();
	vTLV_set_up_primitive_element(pstEl, pbyTag, byTagLen, pbyValue, ulnLen);

	return pstEl;
}


void vTLV_remove_element(BER_TLV *pstEl)
{
	BER_TLV *pstNext;

	if (pstEl)
	{
		/* Remove all of this object's sub-objects. */
		vTLV_destroy_element(pstEl->pstChild);

		pstNext = pstEl->pstNext;

		if (pstNext)
		{
			/* Move the next object into this space. */
			memcpy(pstEl->abyTag, pstNext->abyTag, pstNext->byTagLen);
			pstEl->byTagLen = pstNext->byTagLen;
			pstEl->pstNext = pstNext->pstNext;
			pstEl->pstChild = pstNext->pstChild;
			pstEl->ulnLen = pstNext->ulnLen;
			pfnMemFree(pstEl->pbyValue);
			pstEl->pbyValue = pstNext->pbyValue;
			pfnMemFree(pstNext);
		}
		else
		{
			memset(pstEl->abyTag, 0, sizeof pstEl->abyTag);
			pstEl->byTagLen = 0;
			pstEl->ulnLen = 0;
			pfnMemFree(pstEl->pbyValue);
			pstEl->pbyValue = NULL;
		}
	}
}


/*
 * Check that all the required elements in the list of allowed elements are
 * present in the command and that all the elements in the command are present
 * in the list.
 */
ERROR_CODE usnTLV_check_tags(BER_TLV *pstCmd, const ALLOW_ENTRY *pstList)
{
	ERROR_CODE usnEC = SUCCESS;
	BER_TLV *pstCurEl;
	const ALLOW_ENTRY *pstCurEnt;
	u_char bFound = TRUE;

	/* Check required tags. */
	for (pstCurEnt = pstList; bFound && pstCurEnt->pbyTag; pstCurEnt++)
	{
		if (pstCurEnt->eReqd == AT_REQUIRED)
		{
			for (pstCurEl = pstCmd, bFound = FALSE;
				 pstCurEl && !bFound;
				 pstCurEl = pstCurEl->pstNext)
			{
				if (pstCurEl->byTagLen == pstCurEnt->byTagLen &&
					!memcmp(pstCurEl->abyTag,
							pstCurEnt->pbyTag,
							pstCurEl->byTagLen))
				{
					bFound = TRUE;
				}
			}

			if (!bFound)
			{
				usnEC = ERR_BER_TLV_ELEMENT_NOT_PRESENT;
			}
		}
	}

	if (bFound)
	{
		/*
		 * Check that all the tags in the command are allowable (either required
		 * or optional).
		 */
		for (pstCurEl = pstCmd;
			 pstCurEl && pstCurEl->byTagLen && bFound;
			 pstCurEl = pstCurEl->pstNext)
		{
			for (pstCurEnt = pstList, bFound = FALSE;
				 !bFound && pstCurEnt->pbyTag;
				 pstCurEnt++)
			{
				if (pstCurEl->byTagLen == pstCurEnt->byTagLen &&
					!memcmp(pstCurEl->abyTag,
							pstCurEnt->pbyTag,
							pstCurEl->byTagLen))
				{
					bFound = TRUE;
				}
			}

			if (!bFound)
			{
				usnEC = ERR_BER_TLV_ELEMENT_NOT_EXPECTED;
			}
		}
	}

	return usnEC;
}


/**
 *  Rafal_P2:
 *  Check that every required element is present in the template
 *  and every element from template is defined in the list
 *
 *  @param pBufMissing  buffer to store all missing tags
 *  @param pLenMissing  [in]  pointer to pLenMissing buf. length
 *                      [out] length of returned data in bytes
 *  @param pBufNotExpd  buffer to store all not expected tags
 *  @param pLenNotExpd  [in]  pointer to pBufNotExpd buf. length
 *                      [out] length of returned data in bytes
 */
ERROR_CODE usnTLV_check_all_tags
(
	BER_TLV *pstCmd, const ALLOW_ENTRY *pstList,
	u_char *pBufMissing, u_long *pLenMissing,
	u_char *pBufNotExpd, u_long *pLenNotExpd
)
{
	ERROR_CODE usnEC = SUCCESS;
	BER_TLV *pstCurEl;
	const ALLOW_ENTRY *pstCurEnt;
	u_char bFound = TRUE;

	u_long ulLenMissing = 0;
	u_long ulLenNotExpd = 0;

	/* Check required tags. */
	for (pstCurEnt = pstList; pstCurEnt->pbyTag; pstCurEnt++)
	{
		if (pstCurEnt->eReqd == AT_REQUIRED)
		{
			for (pstCurEl = pstCmd, bFound = FALSE;
				 pstCurEl && !bFound;
				 pstCurEl = pstCurEl->pstNext)
			{
				if (pstCurEl->byTagLen == pstCurEnt->byTagLen &&
					!memcmp(pstCurEl->abyTag,
							pstCurEnt->pbyTag,
							pstCurEl->byTagLen))
				{
					bFound = TRUE;
				}
			}

			if (!bFound)
			{
				usnEC = ERR_BER_TLV_ELEMENT_NOT_PRESENT;

				if (pBufMissing && pLenMissing
				 && ulLenMissing + pstCurEnt->byTagLen <= *pLenMissing)
				{
					memcpy(	pBufMissing + ulLenMissing,
						pstCurEnt->pbyTag,
						pstCurEnt->byTagLen);

					ulLenMissing += pstCurEnt->byTagLen;
				}
			}
		}
	}

	if (bFound)
	{
		/*
		 * Check that all the tags in the command are allowable (either required
		 * or optional).
		 */
		for (pstCurEl = pstCmd;
			 pstCurEl && pstCurEl->byTagLen;
			 pstCurEl = pstCurEl->pstNext)
		{
			for (pstCurEnt = pstList, bFound = FALSE;
				 !bFound && pstCurEnt->pbyTag;
				 pstCurEnt++)
			{
				if (pstCurEl->byTagLen == pstCurEnt->byTagLen &&
					!memcmp(pstCurEl->abyTag,
							pstCurEnt->pbyTag,
							pstCurEl->byTagLen))
				{
					bFound = TRUE;
				}
			}

			if (!bFound)
			{
				usnEC = ERR_BER_TLV_ELEMENT_NOT_EXPECTED;

				if (pBufNotExpd && pLenNotExpd
				 && ulLenNotExpd + pstCurEl->byTagLen <= *pLenNotExpd)
				{
					memcpy(	pBufNotExpd + ulLenNotExpd,
						pstCurEl->abyTag,
						pstCurEl->byTagLen);

					ulLenNotExpd += pstCurEl->byTagLen;
				}
			}
		}
	}
	if (pLenMissing)
		*pLenMissing = ulLenMissing;

	if (pLenNotExpd)
		*pLenNotExpd = ulLenNotExpd;

	return usnEC;
}


/*
 * When using this function, make sure any memory allocated by the called
 * functions is freed after use in the calling function.
 */
ERROR_CODE usnTLV_fill_command_arguments
(
	void *pvVar,
	BER_TLV *pstCmd,
	const ALLOW_ENTRY *pstList
)
{
	ERROR_CODE usnEC = SUCCESS;
	const ALLOW_ENTRY *pstCurEnt;
	u_char bFound;

	for (; pstCmd && usnEC == SUCCESS; pstCmd = pstCmd->pstNext)
	{
		for (pstCurEnt = pstList, bFound = FALSE;
			 !bFound && pstCurEnt->pbyTag;
			 pstCurEnt++)
		{
			if (pstCmd->byTagLen == pstCurEnt->byTagLen &&
				!memcmp(pstCmd->abyTag, pstCurEnt->pbyTag, pstCmd->byTagLen))
			{
				if (pstCurEnt->pfnVarFn)
				{
					usnEC = (*pstCurEnt->pfnVarFn)(pvVar, pstCmd);
				}

				bFound = TRUE;
			}
		}
	}

	return usnEC;
}


static u_char bCompareTag
(
	BER_TLV *pstTag,
	const TAG_EQUIV *pstEntry,
	int bReverse
)
{
	u_char bRet = 0;

	if (bReverse)
	{
		bRet = (pstEntry->nTagEquivLen == pstTag->byTagLen &&
				!memcmp(pstEntry->pbyTagEquiv,
						pstTag->abyTag,
						pstEntry->nTagEquivLen));
	}
	else
	{
		bRet = (pstEntry->nTagLen == pstTag->byTagLen &&
				!memcmp(pstEntry->pbyTag,
						pstTag->abyTag,
				   		pstEntry->nTagLen));
	}

	return bRet;
}


static ERROR_CODE usnTagUpdate
(
	BER_TLV *pstTag,
	const u_char *pbyNewTag,
	u_char byNewTagLen
)
{
	ERROR_CODE usnErr = ERR_BER_TLV_BUFFER_OVERRUN;

	if (byNewTagLen <= sizeof pstTag->abyTag)
	{
		memcpy(pstTag->abyTag, pbyNewTag, byNewTagLen);
		pstTag->byTagLen = byNewTagLen;
		usnErr = SUCCESS;
	}

	return usnErr;
}


static ERROR_CODE usnCopyTag
(
	BER_TLV *pstTag,
	const TAG_EQUIV *pstEntry,
	u_char bReverse
)
{
	ERROR_CODE usnErr;

	if (bReverse)
	{
		usnErr = usnTagUpdate(pstTag, pstEntry->pbyTag, pstEntry->nTagLen);
	}
	else
	{
		usnErr = usnTagUpdate(pstTag,
							  pstEntry->pbyTagEquiv,
							  pstEntry->nTagEquivLen);
	}

	return usnErr;
}


static ERROR_CODE usnConvTag
(
	BER_TLV *pstTag,
	const TAG_EQUIV *pstTagEquivTable,
	u_char bReverse
)
{
	ERROR_CODE usnErr = SUCCESS;
	u_char bFound = FALSE;
	const TAG_EQUIV *pstEntry;

	for (pstEntry = pstTagEquivTable;
		 usnErr == SUCCESS && !bFound && pstEntry->pbyTag;
		 pstEntry++)
	{
		if (bCompareTag(pstTag, pstEntry, bReverse))
		{
			bFound = TRUE;

			usnErr = usnCopyTag(pstTag, pstEntry, bReverse);
		}
	}

	return usnErr;
}


/*
 * Convert the tag field of a BER_TLV structure according to the specified tag
 * equivalent table.
 *
 * Returns,
 * SUCCESS if conversion is successful. Oherwise an error code is returned.
 *
 * Parameters.
 * pbyTags Out.
 *   Tag-length-value-triplet-containing structure to be updated.
 *
 * pstTagEquivTable In.
 *   Table of BER-TLV tags with equivalent tags for conversion purposes.
 *
 *   Sample Table.
 *
 *   static const uint8_t abyDF01[] = { 0xDF, 0x01 };
 *   static const uint8_t abyDF02[] = { 0xDF, 0x02 };
 *   static const uint8_t abyDF8101[] = { 0xDF, 0x81, 0x01 };
 *   static const uint8_t abyDF8102[] = { 0xDF, 0x81, 0x02 };
 *
 *   static const TAG_EQUIV astTagConvTable[] =
 *   {
 *       { TAG_DET(abyDF01), TAG_DET(abyDF8101) },
 *       { TAG_DET(abyDF02), TAG_DET(abyDF8102) },
 *
 *       { NULL, 0, NULL, 0 } // NB: This entry is required.
 *   };
 *
 * bReverse In.
 *   If bReverse is FALSE, tags in the input BER_TLV string found in column 1
 *   of the conversion table will be converted to the equivalent tag from
 *   column 2. Setting bReverse will reverse the procedure.
 *
 * Example.
 *   Using the above table, the input string
 *
 *       DF01 01 AA DF03 01 BB DF02 02 CCDD
 *
 *   is converted to
 *
 *       DF8101 01 AA DF03 01 BB DF8102 02 CCDD
 *
 *	assuming bReverse is FALSE.
 *
 *  Settng bReverse will convert the output back to the original input again.
 *
 */
ERROR_CODE usnTLV_convert_BER_tags
(
	BER_TLV *pstTags,
	const TAG_EQUIV *pstTagEquivTable,
	u_char bReverse
)
{
	ERROR_CODE usnErr = SUCCESS;
	int nStartLen, nEndLen;

	for (; usnErr == SUCCESS && pstTags; pstTags = pstTags->pstNext)
	{
		// For constructed tags, must update the child first since this will
		// change the length of the tag itself.
		if (pstTags->pstChild)
		{
			nStartLen = pstTags->pstChild->byTagLen;

			// A recursive call here to update the child.
			usnErr = usnTLV_convert_BER_tags(pstTags->pstChild,
										 	 pstTagEquivTable,
											 bReverse);

			// Update the parent length.
			if (usnErr == SUCCESS)
			{
				nEndLen = pstTags->pstChild->byTagLen;

				if (nEndLen > nStartLen)
				{
					pstTags->ulnLen += nEndLen - nStartLen;
				}
				else
				{
					pstTags->ulnLen -= nStartLen - nEndLen;
				}
			}
		}

		// Finally update the current tag.
		if (usnErr == SUCCESS)
		{
			usnErr = usnConvTag(pstTags, pstTagEquivTable, bReverse);
		}
	}

	return usnErr;
}


/*
 * Convert the tag field of a BER_TLV string according to the specified tag
 * equivalent table.
 *
 * Returns,
 * SUCCESS if conversion is successful. Oherwise an error code is returned.
 *
 * Parameters.
 * pbyOutTags Out.
 *   The resulting BER-TLV string.
 *
 * pulnOutTagLen Out.
 *   The resulting BER-TLV string length.
 *
 * ulnMaxOutTagLen In.
 *   The maximum allowed output BER-TLV string length.
 *
 * pbyInTags In.
 *   The BER_TLV string to be converted.
 *
 * ulnInTagLen In.
 *   Length of the BER_TLV string to be converted.
 *
 * pstTagEquivTable In.
 *   Table of BER-TLV tags with equivalent tags for conversion purposes.
 *
 *   Sample Table.
 *
 *   static const uint8_t abyDF01[] = { 0xDF, 0x01 };
 *   static const uint8_t abyDF02[] = { 0xDF, 0x02 };
 *   static const uint8_t abyDF8101[] = { 0xDF, 0x81, 0x01 };
 *   static const uint8_t abyDF8102[] = { 0xDF, 0x81, 0x02 };
 *
 *   static const TAG_EQUIV astTagConvTable[] =
 *   {
 *       { TAG_DET(abyDF01), TAG_DET(abyDF8101) },
 *       { TAG_DET(abyDF02), TAG_DET(abyDF8102) },
 *
 *       { NULL, 0, NULL, 0 } // NB: This entry is required.
 *   };
 *
 * bReverse In.
 *   If bReverse is FALSE, tags in the input BER_TLV string found in column 1
 *   of the conversion table will be converted to the equivalent tag from
 *   column 2. Setting bReverse will reverse the procedure.
 *
 * Example.
 *   Using the above table, the input string
 *
 *       DF01 01 AA DF03 01 BB DF02 02 CCDD
 *
 *   is converted to
 *
 *       DF8101 01 AA DF03 01 BB DF8102 02 CCDD
 *
 *	assuming bReverse is FALSE.
 *
 *  Settng bReverse will convert the output back to the original input again.
 *
 */
u_short usnTLV_convert_tags
(
	u_char *pbyOutTags,
	u_long *pulnOutTagLen,
	u_long ulnMaxOutTagLen,
	u_char *pbyInTags,
	u_long ulnInTagLen,
	const TAG_EQUIV *pstTagEquivTable,
	u_char bReverse
)
{
	BER_TLV *pstTags;
	ERROR_CODE usnErr = SUCCESS;

	if (ulnInTagLen)
	{
		pstTags = pstTLV_make_element();

		usnErr = usnTLV_parse_buffer(pstTags, pbyInTags, ulnInTagLen);

		if (usnErr == SUCCESS)
		{
			usnErr = usnTLV_convert_BER_tags(pstTags,
											 pstTagEquivTable,
											 bReverse);

			if (usnErr == SUCCESS)
			{
				usnErr = usnTLV_flatten_element(pbyOutTags,
												pulnOutTagLen,
												ulnMaxOutTagLen,
												pstTags,
												TRUE);
			}
		}

		vTLV_destroy_element(pstTags);
	}

	return usnErr;
}

