//  (C) Copyright John Maddock 2002. 
//  Use, modification and distribution are subject to the 
//  Boost Software License, Version 1.0. (See accompanying file 
//  LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

//  See http://www.boost.org for most recent version.

#define BOOST_PLATFORM "VERIX"

#define BOOST_DISABLE_THREADS
#define BOOST_NO_CWCHAR
#define BOOST_NO_STD_WSTRING
#define BOOST_NO_INTRINSIC_WCHAR_T
//#define BOOST_NO_STRINGSTREAM
#define BOOST_NO_CWCTYPE
//#define BOOST_NO_IOSFWD
#define BOOST_NO_IOSTREAM
#define BOOST_NO_SWPRINT
//#define BOOST_NO_STD_LOCALE
#define BOOST_NO_STD_MESSAGES
#define BOOST_NO_STATIC_ASSERT
#define BOOST_NO_STD_WSTRING
#define BOOST_NO_STD_WSTREAMBUF
//#define BOOST_NO_TEMPLATED_IOSTREAMS
#define BOOST_LEXICAL_CAST_ASSUME_C_LOCALE
