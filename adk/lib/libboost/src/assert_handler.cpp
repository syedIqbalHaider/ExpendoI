/*
 * assert_handler.cpp
 *
 *  Created on: 17-02-2012
 *      Author: Lucjan_B1
 */

/* ----------------------------------------------------------------- */
#include <liblog/logsys.h>
#include <svc.h>
#include <stdio.h>
#include <stdlib.h>
#include <boost/cstdlib.hpp>
/* ----------------------------------------------------------------- */
namespace verix {
namespace handlers {

	__weak int stderr_printf( const char *fmt, ... );
}}


/* ----------------------------------------------------------------- */
namespace boost
  {
      //Boost assert handler
  	   void assertion_failed(char const * expr,
              char const * function, char const * file, long line)

    {
  		  using namespace verix::handlers;
  		  const int error_tone_interval = 1000;
		  error_tone();
		  stderr_printf("Boost assertion failed");
		  stderr_printf("File [%s:%i]", file, line );
		  stderr_printf("Function [%s]", function );
		  stderr_printf("Expression [%s]", expr );
		  stderr_printf("LR=%08x", __return_address());
  		  {
  			  volatile unsigned long *sp = reinterpret_cast<volatile unsigned long*>(__current_sp());
  			  for(int i=0; i<3; i++)
  				stderr_printf("SP[%d]=%08x", i, sp[i]);
  		  }
  		  for(int beeps = 0 ;beeps < 8; ++beeps)
  		  {
  			  error_tone();
  			  SVC_WAIT( error_tone_interval );
  		  }
  		 exit(boost::exit_failure);
    }
  } // namespace boost

/* ----------------------------------------------------------------- */

#undef VERIX_PRINT_ALL

/* ----------------------------------------------------------------- */
