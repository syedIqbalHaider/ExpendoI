/*
 * new_delete_verix.cpp
 *
 *  Created on: 22-02-2012
 *      Author: Lucjan_B1
 */
/* ----------------------------------------------------------------- */
#include <exception> // for std::bad_alloc
#include <new>
#include <stdlib.h> // for malloc() and free()
/* ----------------------------------------------------------------- */
//Global operator new
void* operator new (std::size_t size) throw (std::bad_alloc)
{
	if(size==0) size = 1; //Alloc 0 should return normal mem region in the ISO C++
	void *p=malloc( size );
	 if (p==0) // did malloc succeed?
		 throw std::bad_alloc(); // ANSI/ISO compliant behavior
	 return p;
}
/* ----------------------------------------------------------------- */
//Global operator delete
void operator delete (void *p) throw()
{
	if( p )		//delete NULL is valid in the ISO C++
		free(p);
}
 /* ----------------------------------------------------------------- */
//Global operator new
void* operator new[](std::size_t size) throw (std::bad_alloc)
{
	if(size==0) size = 1;	//Alloc 0 should return normal mem region in the ISO C++
	void *p=malloc( size );
	 if (p==0) // did malloc succeed?
		 throw std::bad_alloc(); // ANSI/ISO compliant behavior
	 return p;
}
/* ----------------------------------------------------------------- */
//Global operator delete
void operator delete[](void *p) throw()
{
	if( p )		//delete NULL is valid in the ISO C++
		free(p);
}
 /* ----------------------------------------------------------------- */
