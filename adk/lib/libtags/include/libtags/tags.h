#ifndef _LIB_LIBTAGS_TAGS_H
#define _LIB_LIBTAGS_TAGS_H

/*****************************************************************************
 *
 * Copyright (C) 2007 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file       	tags.h
 *
 * @author     	Nick tristram
 *
 * @brief      	Definition of class which serve generating mac command
 *
 *
 * @remarks    	This file should be compliant with Verifone EMEA R&D C++ Coding
 *             	Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/
// #include <stdint.h>
#include <tlv-lite/ConstData.h>


/*static const unsigned ADD_SCREEN_TEXT  = 16;
static const unsigned CODE_SIZE    =            1;
static const unsigned ACTION_CODE_SIZE =         1;
static const unsigned COMMAND_SIZE    =          1;
static const unsigned CONTRAST_SIZE	=		  1;
static const unsigned YPOS_SIZE		=		  1;
static const unsigned XPOS_SIZE		=		  1;
static const unsigned BACKLIGHT_SIZE	=		  1;
static const unsigned ICON_FROM_SIZE	= 1;
static const unsigned ICON_TO_SIZE		= 1;
static const unsigned BLOCK_RESP_SIZE	=		  1;
static const unsigned FONTCACHEFLAG_SIZE =		  1;
static const unsigned TEXT_SIZE		=		  512;
static const unsigned POS_TIMEOUT_SIZE   =       1;
static const unsigned RESP_CODE_SIZE   	=		1;
static const unsigned DISPLAY_CONTRAST_SIZE   =	 1;
static const unsigned KEY_VALUE_SIZE    		 = 1;*/

static const unsigned TAG_2BYTE       =  	   	 2;
static const unsigned TAG_3BYTE       = 	   	 3;


namespace guiapp_tags
{


/***************************************************************************
 * Preprocessor constant definitions
 **************************************************************************/
#define TLVLITE_EXP(tag) extern const ConstData_s tag

	using com_verifone_TLVLite::ConstData_s;
	TLVLITE_EXP(TagCode);
	TLVLITE_EXP(TagPosTimeout);
	TLVLITE_EXP(TagDisplayText);
	TLVLITE_EXP(TagDisplayXPos);
	TLVLITE_EXP(TagDisplayYPos);
	TLVLITE_EXP(TagBacklightFlag);
	TLVLITE_EXP(TagKbdBacklightFlag);
	TLVLITE_EXP(TagDisplayIconFrom);
	TLVLITE_EXP(TagDisplayContrast);
	TLVLITE_EXP(TagDisplayIconTo);
	TLVLITE_EXP(TagPromptFile);
	TLVLITE_EXP(TagPromptSection);
	TLVLITE_EXP(TagPromptIndex);
	TLVLITE_EXP(TagPromptIndex2);
	TLVLITE_EXP(TagPromptIndex3);
	TLVLITE_EXP(TagPromptIndex4);
	TLVLITE_EXP(TagPromptAnimation);
	TLVLITE_EXP(TagTimeoutMS);
	TLVLITE_EXP(TagKeyValue);
	TLVLITE_EXP(TagTextLine1);
	TLVLITE_EXP(TagTextLine2);
	TLVLITE_EXP(TagTextLine3);
	TLVLITE_EXP(TagPINClearText);
	TLVLITE_EXP(TagPINParams);
	TLVLITE_EXP(TagEntryParams);
	TLVLITE_EXP(TagDataInput);
	TLVLITE_EXP(TagSelectionItem);
	TLVLITE_EXP(TagSelectKeyText);
	TLVLITE_EXP(TagSelectedItem);
	TLVLITE_EXP(TagIconId);
	TLVLITE_EXP(TagIconLevel);
	TLVLITE_EXP(TagIconStatus);
	TLVLITE_EXP(TagGraphicFilename);
	TLVLITE_EXP(TagGraphicAlignment);
	TLVLITE_EXP(TagSelListMode);
	TLVLITE_EXP(TagBeeperInterval);
	TLVLITE_EXP(TagLineCount);
	TLVLITE_EXP(TagBlockResponse);
	TLVLITE_EXP(TagPrevListEnabled);
	TLVLITE_EXP(TagNextListEnabled);
	TLVLITE_EXP(TagLineLength);
	TLVLITE_EXP(TagReportItem);
	TLVLITE_EXP(TagMatchKeys);
	TLVLITE_EXP(TagFontFilename);
	TLVLITE_EXP(TagFontEncoding);
	TLVLITE_EXP(TagFontStyle);
	TLVLITE_EXP(TagFontSize);
	TLVLITE_EXP(TagFontFlags);
	TLVLITE_EXP(TagDefaultLang);
	TLVLITE_EXP(TagNumberOfLanguages);
	TLVLITE_EXP(TagURL);
	TLVLITE_EXP(TagStatus);
	TLVLITE_EXP(TagResponseEvtBitmap);
	TLVLITE_EXP(TagInputTerminator);
	TLVLITE_EXP(TagTaskId);
	TLVLITE_EXP(TagTaskGraphXPos);
	TLVLITE_EXP(TagTaskGraphYPos);
	TLVLITE_EXP(TagGraphForegroundColor);
	TLVLITE_EXP(TagGraphBackgroundColor);
	TLVLITE_EXP(TagMonitorKeys);
	TLVLITE_EXP(TagBoxBackgroundColor);
	TLVLITE_EXP(TagBoxBorderColor);
	TLVLITE_EXP(TagBoxForegroundColor);
	TLVLITE_EXP(TagBoxBorderSize);
	TLVLITE_EXP(TagBannerShow);
	TLVLITE_EXP(TagBannerFilename);
	TLVLITE_EXP(TagBannerReservedY);
	TLVLITE_EXP(TagBannerShowLeds);
	TLVLITE_EXP(TagBannerLedMaskOn);
	TLVLITE_EXP(TagBannerLedMaskOff);
	TLVLITE_EXP(TagIconBar);
	TLVLITE_EXP(TagReportPromptIndex);
	TLVLITE_EXP(TagImagesNum);
	TLVLITE_EXP(TagImagesRefreshTime);
	TLVLITE_EXP(TagLedsReservedY);
	TLVLITE_EXP(TagPINFlags);
	TLVLITE_EXP(TagwXMLName);
	TLVLITE_EXP(TagwXMLStrVal);
	TLVLITE_EXP(TagwXMLPromptVal);
	TLVLITE_EXP(TagwXMLGroup);
	TLVLITE_EXP(TagHTMLUrl);
	TLVLITE_EXP(TagHTMLKey);
	TLVLITE_EXP(TagHTMLValue);
	TLVLITE_EXP(TagHTMLRawValue);
	TLVLITE_EXP(TagHTMLResult);
	TLVLITE_EXP(TagHTMLDisplayConfirmation);
	TLVLITE_EXP(TagHTMLRegion);
}

namespace prnapp_tags
{

	using com_verifone_TLVLite::ConstData_s;
	TLVLITE_EXP(TagCode);
	TLVLITE_EXP(TagPosTimeout);
	TLVLITE_EXP(TagTaskId);
	TLVLITE_EXP(TagBeepOnLength);
	TLVLITE_EXP(TagBeepOffLength);
	TLVLITE_EXP(TagBeepCount);
	TLVLITE_EXP(TagPrintMessage);
	TLVLITE_EXP(TagPOSProtocol);
	TLVLITE_EXP(TagWaitForPrinting);
	TLVLITE_EXP(TagWaitForTail);
	TLVLITE_EXP(TagBarCode);
	TLVLITE_EXP(TagBarcodeHeight);
	TLVLITE_EXP(TagBarcodeWidth);
	TLVLITE_EXP(TagBarCodeEncoding);
	TLVLITE_EXP(TagBarCodeMargin);
	TLVLITE_EXP(TagBarCodeScale);
	TLVLITE_EXP(TagTemplateFilename);
	TLVLITE_EXP(TagTemplateNames);
	TLVLITE_EXP(TemplateAdditionalTags);
	TLVLITE_EXP(TagOutputFilename);
	TLVLITE_EXP(TagGraphicFilename);
	TLVLITE_EXP(TagGraphicAlignment);
	TLVLITE_EXP(TagTemplateErrorCol);
	TLVLITE_EXP(TagTemplateErrorRow);
	TLVLITE_EXP(TagTemplateErrorDesc);
	TLVLITE_EXP(TagStatus);
}

namespace mapp_tags
{
	using com_verifone_TLVLite::ConstData_s;
	TLVLITE_EXP(TagCode);
	TLVLITE_EXP(TemplateData);
	TLVLITE_EXP(TemplateRequestedElements);
	TLVLITE_EXP(TemplateDecisionRequired);
	TLVLITE_EXP(TemplateTC);
	TLVLITE_EXP(TemplateARQC);
	TLVLITE_EXP(TemplateAAC);
	TLVLITE_EXP(TemplateStatus);
	TLVLITE_EXP(TemplateContactlessMSD);
	TLVLITE_EXP(TagTranStatus);
	TLVLITE_EXP(TagTranStatusDesc);
	TLVLITE_EXP(TagStatusPINTryCounter);
	TLVLITE_EXP(TagTerminalID);
	TLVLITE_EXP(TagTerminalSerialNum);
	TLVLITE_EXP(TemplateConfigVersion);
	TLVLITE_EXP(TemplateOSVersion);
	TLVLITE_EXP(TemplateModuleVersion);
	TLVLITE_EXP(TagConfigFileName);
	TLVLITE_EXP(TagConfigFileVersion);
	TLVLITE_EXP(TagConfigFileChecksum);
	TLVLITE_EXP(TagLibraryFileName);
	TLVLITE_EXP(TagLibraryFileVersion);
	TLVLITE_EXP(TagDeviceArchitecture);
	TLVLITE_EXP(TagDeviceArchitectureVersion);
	TLVLITE_EXP(TagTamper);
	TLVLITE_EXP(TagSwitchTripped);
	TLVLITE_EXP(TagPlatformType);
	TLVLITE_EXP(TagNumericLanguageIndices);
	TLVLITE_EXP(TagNumberFormat);
	TLVLITE_EXP(TagNumericLanguageText);
	TLVLITE_EXP(TagNumericEdit);
	TLVLITE_EXP(TagAlphanumericEdit);
	TLVLITE_EXP(TagEntryMaxLength);
	TLVLITE_EXP(TagEntryMinLength);
	TLVLITE_EXP(TagPrefixSymbol);
	TLVLITE_EXP(TagSuffixSymbol);
	TLVLITE_EXP(TagUnmaskedCharactersCount);
	TLVLITE_EXP(TagAlphaModes);
	TLVLITE_EXP(TagAlphaInitMode);
	TLVLITE_EXP(TagBacklightMode);
	TLVLITE_EXP(TagKbdBacklightMode);
	TLVLITE_EXP(TagFontFilename);
	TLVLITE_EXP(TagFontSize);
	TLVLITE_EXP(TagDisplayContrast);
	TLVLITE_EXP(TagDisplayBitmapFileName);
	TLVLITE_EXP(TagDisplayBitmapCol);
	TLVLITE_EXP(TagDisplayBitmapRow);
	TLVLITE_EXP(TagCardStatus);
	TLVLITE_EXP(TagATR);
	TLVLITE_EXP(TagTrack1);
	TLVLITE_EXP(TagTrack2);
	TLVLITE_EXP(TagTrack3);
	TLVLITE_EXP(TagSwipeStatus);
	TLVLITE_EXP(TemplateFile);
	TLVLITE_EXP(TagFileSize);
	TLVLITE_EXP(TagFileSize1);
	TLVLITE_EXP(TagFileDescriptor);
	TLVLITE_EXP(TagFileIdentifier);
	TLVLITE_EXP(TagFileName);
	TLVLITE_EXP(TagFileEFId);
	TLVLITE_EXP(TagFileChecksum);
	TLVLITE_EXP(TagFileSecured);
	TLVLITE_EXP(TagFreeSpace);
	TLVLITE_EXP(TagDrive);
	TLVLITE_EXP(TagFileOffset);
	TLVLITE_EXP(TagFileReadSize);
	TLVLITE_EXP(TagKeyPress);
	TLVLITE_EXP(TagLanguage);
	TLVLITE_EXP(TagLanguageSelect);
	TLVLITE_EXP(TagBarcode);
	TLVLITE_EXP(TagBarcodeHeight);
	TLVLITE_EXP(TagBarcodeWidth);
	TLVLITE_EXP(TagBarcodeEncoding);
	TLVLITE_EXP(TagBarcodeMargin);
	TLVLITE_EXP(TagBarcodeScale);
	TLVLITE_EXP(TagPrintTemplateFilename);
	TLVLITE_EXP(TagPrintTemplateNames);
	TLVLITE_EXP(TemplatePrintAdditionalTags);
	TLVLITE_EXP(TagTemplateRow);
	TLVLITE_EXP(TagTemplateCol);
	TLVLITE_EXP(TagTemplateDesc);
	TLVLITE_EXP(TagBitmapFilename);
	TLVLITE_EXP(TagBitmapAlignment);
	TLVLITE_EXP(TagOptionID);
	TLVLITE_EXP(TagOptionText);
	TLVLITE_EXP(TagMenuTitle);
	TLVLITE_EXP(TagSelListMode);
	TLVLITE_EXP(TagSerialNumber);
	TLVLITE_EXP(TagMaxChainedSize);
	TLVLITE_EXP(TagSafeModeIndicator);
	TLVLITE_EXP(TagKeyType);
	TLVLITE_EXP(TagKeyData);
	TLVLITE_EXP(TagResultCode);
	TLVLITE_EXP(TagPinEntryType);
	TLVLITE_EXP(TagPosSecurityTimeout);
	TLVLITE_EXP(TagHostId);
	TLVLITE_EXP(TagPinBlockFormat);
	TLVLITE_EXP(TagKeyManagement);
	TLVLITE_EXP(TagPinAlgorithm);
	TLVLITE_EXP(TagDataAlgorithm);
	TLVLITE_EXP(TagMessageForMAC);
	TLVLITE_EXP(TagMessForEnc);
	TLVLITE_EXP(TagOutputData);
	TLVLITE_EXP(TagMaxPINLength);
	TLVLITE_EXP(TagMinPINLength);
	TLVLITE_EXP(TagOnlinePINCipher);
	TLVLITE_EXP(TagPan);
	TLVLITE_EXP(TagStan);
	TLVLITE_EXP(TagPinCancel);
	TLVLITE_EXP(TagPinEntryTimeout);
	TLVLITE_EXP(TagPinEntryFirstCharTimeout);
	TLVLITE_EXP(TagPinEntryInterCharTimeout);
	TLVLITE_EXP(TagPinTryFlag);
	TLVLITE_EXP(TagKSN);
	TLVLITE_EXP(TagScriptName);
	TLVLITE_EXP(TagAddScreenText);
	TLVLITE_EXP(TagFlags);
	TLVLITE_EXP(TagLEDOperation);
	TLVLITE_EXP(TagLEDDuration);
	TLVLITE_EXP(TagLEDRepeats);
	TLVLITE_EXP(TagBatteryStatus);
	TLVLITE_EXP(TagContinueFlag);
	TLVLITE_EXP(TagCashbackFlag);
	TLVLITE_EXP(TagAcquirerID);
	//TLVLITE_EXP(TagForceOnline);

	TLVLITE_EXP(TagMifareOperation);
	TLVLITE_EXP(TagMifareOperationID);
	TLVLITE_EXP(TagPinResult);
	TLVLITE_EXP(TagCardholderVerificationCompleted);
	TLVLITE_EXP(TagPinEntryStyle);
	TLVLITE_EXP(TagRadixSeparator);
	TLVLITE_EXP(TagPinpadSelectsApplication);
	TLVLITE_EXP(TagScripts71Results);
	TLVLITE_EXP(TagScripts72Results);
	TLVLITE_EXP(TagTACDefault);
	TLVLITE_EXP(TagTACDenial);
	TLVLITE_EXP(TagTACOnline);
	TLVLITE_EXP(TagForceOnline);
	TLVLITE_EXP(TagROSThreshold);
	TLVLITE_EXP(TagROSTargetRSPercent);
	TLVLITE_EXP(TagROSMaxTargetRSPercent);
	TLVLITE_EXP(TagDioneCashbackCheck);
	TLVLITE_EXP(TagReasonOnline);
	TLVLITE_EXP(TagSuppressDisplay);
	TLVLITE_EXP(TagPinEntryCurrencySymbol);
	TLVLITE_EXP(TagPinEntryCurrencyLoc);
	TLVLITE_EXP(TagPinEntryBeeper);
	TLVLITE_EXP(TagSimplifiedRefunds);
	TLVLITE_EXP(TagICCPublicKey);
	TLVLITE_EXP(TagICCPublicKeyLegacy);
	TLVLITE_EXP(TagShowIncorrectPIN);

	TLVLITE_EXP(TagItemID);
	TLVLITE_EXP(TagItemText);
	TLVLITE_EXP(TagItemInt);

	TLVLITE_EXP(TagMachineType);
	TLVLITE_EXP(TagCardReaderType);
	TLVLITE_EXP(TagAlternateInterval);
	TLVLITE_EXP(TagOverrideLabel);
	TLVLITE_EXP(TagHideIcons);

	TLVLITE_EXP(TagVSPStatus);
	TLVLITE_EXP(TagVSPFullStatus);
	TLVLITE_EXP(TagVSPVersion);
	TLVLITE_EXP(TagVSPExtStatus);
	TLVLITE_EXP(TagVSPDiags);
	TLVLITE_EXP(TagTGKStatus);
	TLVLITE_EXP(TagTGKFallbackStatus);

	TLVLITE_EXP(TagVSPEncryptionStatus);
	TLVLITE_EXP(TagVSPEParms);

	TLVLITE_EXP(TagTrackData);

	TLVLITE_EXP(TagExtResultCode);
	TLVLITE_EXP(TagBuzzerCtrl);

	TLVLITE_EXP(TemplateSRED);
	TLVLITE_EXP(TemplateSREDPAN);

	TLVLITE_EXP(TagTrack2Data);
	TLVLITE_EXP(TagManualPAN);
	TLVLITE_EXP(TagManualCVV2);
	TLVLITE_EXP(TagManualExpiryDate);
	TLVLITE_EXP(TagManualStartDate);
	TLVLITE_EXP(TagSREDMagstripeTrack1);
	TLVLITE_EXP(TagSREDMagstripeTrack2);
	TLVLITE_EXP(TagSREDMagstripeTrack3);
	TLVLITE_EXP(TagSREDWhitelistStatus);
	TLVLITE_EXP(TagSREDWhitelistHash);

	TLVLITE_EXP(TagSREDMaskingScheme);
	TLVLITE_EXP(TagSREDKeySlot);
	TLVLITE_EXP(TagSREDStatusIndicator);

	TLVLITE_EXP(TagSREDEncryptionStatus);
	TLVLITE_EXP(TagSREDEncryptedData);
	TLVLITE_EXP(TagSREDKSN);
	TLVLITE_EXP(TagSREDIV);
	TLVLITE_EXP(TagSREDEncryptedPAN);
	TLVLITE_EXP(TagSREDKSNPAN);
	TLVLITE_EXP(TagSREDIVPAN);
	TLVLITE_EXP(TagSREDEncryptionStatusPAN);

	TLVLITE_EXP(TagCardCheckIndicator);

	TLVLITE_EXP(TagARSPassword1);
	TLVLITE_EXP(TagARSPassword2);


	TLVLITE_EXP(TagOSCounterIndex);
	TLVLITE_EXP(TagOSCounterValue);
	TLVLITE_EXP(TagOSCounterName);
	TLVLITE_EXP(TagOSDevice);
	TLVLITE_EXP(TagOSPort);
	// TLVLITE_EXP(TagOSCounterGroup);
	TLVLITE_EXP(TemplateAdditionalTags);

	TLVLITE_EXP(TagHTMLKey);
	TLVLITE_EXP(TagHTMLValue);
	TLVLITE_EXP(TagHTMLResult);
	TLVLITE_EXP(TagHTMLUrl);
	TLVLITE_EXP(TagHTMLRawValue);
	TLVLITE_EXP(TagHTMLDisplayConfirmation);
	TLVLITE_EXP(TagHTMLRegion);
	
	TLVLITE_EXP(TagHTMLEcho);
	TLVLITE_EXP(TagHTMLEchoKey);
	TLVLITE_EXP(TagHTMLEchoValue);

}

namespace cardapp_tags
{
	using com_verifone_TLVLite::ConstData_s;
	TLVLITE_EXP(TagPosTimeout);
	TLVLITE_EXP(TagProcFlag);
	TLVLITE_EXP(TagATR);
	TLVLITE_EXP(TagInTLV);
	TLVLITE_EXP(TagInAPDU);
	TLVLITE_EXP(TagOutAPDU);
	TLVLITE_EXP(TagOutTLVlist);
	TLVLITE_EXP(TagOperations);
	TLVLITE_EXP(TagCode);
	TLVLITE_EXP(TagOutTLV);
	TLVLITE_EXP(TagProcStatus);
	TLVLITE_EXP(TagAuthResult);
	TLVLITE_EXP(TagPINtimeout);
	TLVLITE_EXP(TagPINfirstcharTimeout);
	TLVLITE_EXP(TagPINintercharTimeout);
	TLVLITE_EXP(TagEvent);
	TLVLITE_EXP(TagActionCode);
	TLVLITE_EXP(TagTrack1);
	TLVLITE_EXP(TagTrack2);
	TLVLITE_EXP(TagTrack3);
	TLVLITE_EXP(TagCardType);
	TLVLITE_EXP(TagCardBlacklisted);
	TLVLITE_EXP(TagOfflinePINEntryType);
	TLVLITE_EXP(TagPINEntryAtomic);
	TLVLITE_EXP(TagCardStatusWord);
	TLVLITE_EXP(TagPINEntryStatus);
	TLVLITE_EXP(TagLEDDuration);
	TLVLITE_EXP(TagLEDRepeats);
	TLVLITE_EXP(TagMemoryCardAddress);
	TLVLITE_EXP(TagMemoryCardByteCount);
	TLVLITE_EXP(TagMemoryCardData);
	TLVLITE_EXP(TagMemoryCardPIN);
	TLVLITE_EXP(TagShowIncorrectPIN);
	TLVLITE_EXP(TagEMVKernelChecksum);
	TLVLITE_EXP(TagEMVKernelVersion);
	TLVLITE_EXP(TagPacketType);
	TLVLITE_EXP(TagActiveInterfaces);
	TLVLITE_EXP(TagPartialSelection);
	TLVLITE_EXP(TagRecordApplicationName);
	TLVLITE_EXP(TagSecondTermAppVer);
	TLVLITE_EXP(TagROSThreshold);
	TLVLITE_EXP(TagROSTargetRSPercent);
	TLVLITE_EXP(TagROSMaxTargetRSPercent);
	TLVLITE_EXP(TagTACDefault);
	TLVLITE_EXP(TagTACDenial);
	TLVLITE_EXP(TagTACOnline);
	TLVLITE_EXP(TagDefaultTDOL);
	TLVLITE_EXP(TagDefaultDDOL);
	TLVLITE_EXP(TagAutoAppSelection);
	TLVLITE_EXP(TagMerchantCategoryCode);
	TLVLITE_EXP(TagCAPKModulus);
	TLVLITE_EXP(TagCAPKExponent);
	TLVLITE_EXP(TagCAPKChecksum);
	TLVLITE_EXP(TagCAPKExpiry);
	TLVLITE_EXP(TagCAPKFile);
	TLVLITE_EXP(TagFallback);
	TLVLITE_EXP(TagFlags);
	TLVLITE_EXP(TagPinEntryBeeper);
}

namespace authapp_tags
{
	using com_verifone_TLVLite::ConstData_s;
	TLVLITE_EXP(TagCB2_CAPK);
	TLVLITE_EXP(TagCB2_CAindex);
	TLVLITE_EXP(TagCB2_NRG);
	TLVLITE_EXP(TagCB2_OperCert);
	TLVLITE_EXP(TagCB2_OperExp);
	TLVLITE_EXP(TagCB2_SupplCert);
	TLVLITE_EXP(TagCB2_TermCert);
	TLVLITE_EXP(TagCB2_NRT);
	TLVLITE_EXP(TagCB2_NRG_ST);
	TLVLITE_EXP(TagCB2_NRT_SG);
	TLVLITE_EXP(TagCB2_SupplNdx);
	TLVLITE_EXP(TagCB2_File_CA);
	TLVLITE_EXP(TagCB2_File_CT);
	TLVLITE_EXP(TagCB2_File_ST);
	TLVLITE_EXP(TagCB2_File_MAN);
	TLVLITE_EXP(TagCB2_File_RND);
	TLVLITE_EXP(TagCB2_KSER_PT);
	TLVLITE_EXP(TagCB2_KCV_SG);
	TLVLITE_EXP(TagCB2_KIP_PT);
	TLVLITE_EXP(TagCB2_KCV_KIP_SG);
	TLVLITE_EXP(TagCB2_OperRem);
	TLVLITE_EXP(TagCB2_GTID);
	TLVLITE_EXP(TagCB2_CA_CG);
	TLVLITE_EXP(TagCB2_PF_exp);
	TLVLITE_EXP(TagCB2_PF_rem);
	TLVLITE_EXP(TagCB2_PT_exp);
	TLVLITE_EXP(TagCB2_PT_rem);
	TLVLITE_EXP(TagCB2_CA_ndx);
	TLVLITE_EXP(TagCB2_CF_ndx);
	TLVLITE_EXP(TagCB2_AdminOper);
	TLVLITE_EXP(TagCB2_VerApp);
	TLVLITE_EXP(TagCB2_VerVSS);
	TLVLITE_EXP(TagCB2_HomeDir);
	TLVLITE_EXP(TagCB2_KeysStatus);
	TLVLITE_EXP(TagCB2_EncData);
	TLVLITE_EXP(TagCB2_DecData);
	TLVLITE_EXP(TagCB2_OnlineTrID);
	TLVLITE_EXP(TagCB2_KeyIndex);
	TLVLITE_EXP(TagCB2_KSN);
	TLVLITE_EXP(TagCB2_TerminalID);
	TLVLITE_EXP(TagCB2_KeyType);
	TLVLITE_EXP(TagCB2_PAN);
	TLVLITE_EXP(TagCB2_PINblock);
	TLVLITE_EXP(TagCB2_KeyIndexAP);
	TLVLITE_EXP(TagCode);
}

namespace auth_SRED_tags
{
// SRED authentication
	using com_verifone_TLVLite::ConstData_s;
	TLVLITE_EXP(TagAuthCertLevel);
	TLVLITE_EXP(TagAuthCertData);
	TLVLITE_EXP(TagAuthCertFile);
	TLVLITE_EXP(TagAuthRandomClrRemote);
	TLVLITE_EXP(TagAuthRandomEncr);
	TLVLITE_EXP(TagAuthKeyStream);
	TLVLITE_EXP(TagAuthDataEncr);
	TLVLITE_EXP(TagAuthDataClr);
	TLVLITE_EXP(TagAuthFlags);
	TLVLITE_EXP(TagAuthSign);
	TLVLITE_EXP(TagAuthDID);
	TLVLITE_EXP(TagAuthCertReq);
	TLVLITE_EXP(TagAuthRSAsize);
	TLVLITE_EXP(TagAuthRandomClrOwn);
}


namespace scapp_tags
{
	using com_verifone_TLVLite::ConstData_s;
	TLVLITE_EXP(TagActionCode);
	TLVLITE_EXP(TagAddScreenText);
	TLVLITE_EXP(TagAddScreenText2);
	TLVLITE_EXP(TagScreenLines);
	TLVLITE_EXP(TagPromptFill);
	TLVLITE_EXP(TagCode);
	TLVLITE_EXP(TagDataAlgorithm);
	TLVLITE_EXP(TagHostId);
	TLVLITE_EXP(TagKeyData);
	TLVLITE_EXP(TagKeyManagement);
	TLVLITE_EXP(TagKeyType);
	TLVLITE_EXP(TagKSN);
	TLVLITE_EXP(TagLock);
	TLVLITE_EXP(TagMAC);
	TLVLITE_EXP(TagMaxPINLength);
	TLVLITE_EXP(TagMessForDec);
	TLVLITE_EXP(TagMessForEnc);
	TLVLITE_EXP(TagMessageForMAC);
	TLVLITE_EXP(TagMinPINLength);
	TLVLITE_EXP(TagOnlinePINCipher);
	TLVLITE_EXP(TagPan);
	TLVLITE_EXP(TagPinAlgorithm);
	TLVLITE_EXP(TagPinBlockFormat);
	TLVLITE_EXP(TagPinCancel);
	TLVLITE_EXP(TagPinEntryTimeout);
	TLVLITE_EXP(TagPinEntryType);
	TLVLITE_EXP(TagPinTryFlag);
	TLVLITE_EXP(TagPlainTxtPinBlock);
	TLVLITE_EXP(TagPosTimeout);
	TLVLITE_EXP(TagScriptName);
	TLVLITE_EXP(TagStan);
	TLVLITE_EXP(TagTransAmount);
	TLVLITE_EXP(TagTransCatExp);
	TLVLITE_EXP(TagTransCurrCode);
	TLVLITE_EXP(TagTransCurrExp);
	TLVLITE_EXP(TagFlags);
	TLVLITE_EXP(TagMessagePIN);
	TLVLITE_EXP(TagMessageAmount);
	TLVLITE_EXP(TagFontFile);
	TLVLITE_EXP(TagMonitorPeriod);
	TLVLITE_EXP(TagVSSname);
	TLVLITE_EXP(TagVSSslot);
	TLVLITE_EXP(TagVSSmacro);
	TLVLITE_EXP(TagVSSIn);
	TLVLITE_EXP(TagVSSOut);
	TLVLITE_EXP(TagVSSResult);
	TLVLITE_EXP(TagInput_EMV);
	TLVLITE_EXP(TagPINTimeout_EMV);
	TLVLITE_EXP(TagPinMaxLen);
	TLVLITE_EXP(TagPinMinLen);
	TLVLITE_EXP(TagPinOnlineEntryType);
	TLVLITE_EXP(TagPinOnlineCancel);
	TLVLITE_EXP(TagPinOnlineExtraMsg);
	TLVLITE_EXP(TagOperations_EMV);
	TLVLITE_EXP(TagDUKPTMaskPIN);
	TLVLITE_EXP(TagDUKPTMaskMAC);
	TLVLITE_EXP(TagDUKPTMaskENC);
	TLVLITE_EXP(TagPropData);
    TLVLITE_EXP(TagPinEntryBeeper);
}

namespace ebaapp_tags
{
	using com_verifone_TLVLite::ConstData_s;
	TLVLITE_EXP(TagCode);
	TLVLITE_EXP(TagDeviceStatus);
	TLVLITE_EXP(TagDeviceModelNo);
	TLVLITE_EXP(TagDeviceSerialNumber);
	TLVLITE_EXP(TagFirmwareVersion);
	TLVLITE_EXP(TagLastDetectionResult);
	TLVLITE_EXP(TagNotificationId);
	TLVLITE_EXP(TagCurrency);
	TLVLITE_EXP(TagDenomination);
	TLVLITE_EXP(TagProcessingError);
	TLVLITE_EXP(TagDeviceRejectionCode);
	TLVLITE_EXP(TagDeviceErrorCode);
	TLVLITE_EXP(TagFirmwareFileLocn);
	TLVLITE_EXP(TagDownloadTimeout);
}

namespace bcsapp_tags
{
	using com_verifone_TLVLite::ConstData_s;
	TLVLITE_EXP(TagCode);
	TLVLITE_EXP(TagBarcodeData);
}

namespace cb2if_tags
{
	using com_verifone_TLVLite::ConstData_s;
	TLVLITE_EXP(TagifOperation);
	TLVLITE_EXP(TagifTermID);
	TLVLITE_EXP(TagifTransAmount);
	TLVLITE_EXP(TagifConfirmAmount);
	TLVLITE_EXP(TagifPrintTicket);
	TLVLITE_EXP(TagifAddMessageToHost);
	TLVLITE_EXP(TagifPaymantType);
	TLVLITE_EXP(TagifMessageToPrinter);
	TLVLITE_EXP(TagifEcrId);
	TLVLITE_EXP(TagifSTAN);
	TLVLITE_EXP(TagifActionCode);
	TLVLITE_EXP(TagifPAN);
	TLVLITE_EXP(TagifTransType);
	TLVLITE_EXP(TagifTransDataTime);
	TLVLITE_EXP(TagifCardType);
	TLVLITE_EXP(TagifAcquirerID);
	TLVLITE_EXP(TagifOnlineOperation);
	TLVLITE_EXP(TagifApprovalCode);
	TLVLITE_EXP(TagifTransStatusEnable);
	TLVLITE_EXP(TagifGTID);
	TLVLITE_EXP(TagifLineType);
	TLVLITE_EXP(TagifNetType);
	TLVLITE_EXP(TagifHostIP);
	TLVLITE_EXP(TagifHostPort);
	TLVLITE_EXP(TagifDHCPEnable);
	TLVLITE_EXP(TagifIp);
	TLVLITE_EXP(TagifGateway1);
	TLVLITE_EXP(TagifGateway2);
	TLVLITE_EXP(TagifSubnetMask);
	TLVLITE_EXP(TagifDNS1);
	TLVLITE_EXP(TagifDNS2);
	TLVLITE_EXP(TagifTransportType);
	TLVLITE_EXP(TagifScreenEnable);
	TLVLITE_EXP(TagifPreResponseEnable);

	TLVLITE_EXP(TagifTransResult);
	TLVLITE_EXP(TagifHostErrorMessage);
	TLVLITE_EXP(TagifAppErrorMessage);
	TLVLITE_EXP(TagifScreenLine1ID);
	TLVLITE_EXP(TagifScreenLine2ID);
	TLVLITE_EXP(TagifScreenLine3ID);
	TLVLITE_EXP(TagifScreenLine4ID);
	TLVLITE_EXP(TagifScreenLine1Text);
	TLVLITE_EXP(TagifScreenLine2Text);
	TLVLITE_EXP(TagifScreenLine3Text);
	TLVLITE_EXP(TagifScreenLine4Text);

	TLVLITE_EXP(TagifTransStatus);

	TLVLITE_EXP(TagPosTimeout);
	TLVLITE_EXP(TagCode);

}

namespace iso8583_tags
{
	using com_verifone_TLVLite::ConstData_s;
	TLVLITE_EXP(TagPrimaryAccountNumber);						//ISO8583 field   2
	TLVLITE_EXP(TagProcessingCode);								//ISO8583 field   3
	TLVLITE_EXP(TagAmountTransaction);							//ISO8583 field   4
	TLVLITE_EXP(TagSystemsTraceAuditNumber);					//ISO8583 field  11
	TLVLITE_EXP(TagDateTimeHost);								//ISO8583 field  12
	TLVLITE_EXP(TagDateEffective);								//ISO8583 field  13
	TLVLITE_EXP(TagDateExpiration);								//ISO8583 field  14
	TLVLITE_EXP(TagPointOfServiceDataCode);						//ISO8583 field  22
	TLVLITE_EXP(TagCardSequenceNumber);							//ISO8583 field  23
	TLVLITE_EXP(TagFunctionCode);								//ISO8583 field  24
	TLVLITE_EXP(TagMessageReasonCode);							//ISO8583 field  25
	TLVLITE_EXP(TagCardAcceptorBusinessCode);					//ISO8583 field  26
	TLVLITE_EXP(TagAcquiringInstitutionsIdentificationCode);	//ISO8583 field  32
	TLVLITE_EXP(TagPrimaryAccountNumberPANExtended);			//ISO8583 field  34
	TLVLITE_EXP(TagTrack2data);									//ISO8583 field  35
	TLVLITE_EXP(TagApprovalCode);								//ISO8583 field  38
	TLVLITE_EXP(TagAC);											//ISO8583 field  39
	TLVLITE_EXP(TagAdditionalResponseData);						//ISO8583 field  44
	TLVLITE_EXP(TagAdditionalDataNational);						//ISO8583 field  47
	TLVLITE_EXP(TagAdditionalDataPrivate);						//ISO8583 field  48
	TLVLITE_EXP(TagCurrencyCode);								//ISO8583 field  49
	TLVLITE_EXP(TagPINData);									//ISO8583 field  52
	TLVLITE_EXP(TagSecurityRelatedControlInformation);			//ISO8583 field  53
	TLVLITE_EXP(TagICCRelatedData);								//ISO8583 field  55
	TLVLITE_EXP(TagOriginalDataElements);						//ISO8583 field  56
	TLVLITE_EXP(TagTransportData);								//ISO8583 field  59
	TLVLITE_EXP(TagMessageAuthenticationCode);					//ISO8583 field  64-128
	TLVLITE_EXP(TagDataRecord);									//ISO8583 field  72
	TLVLITE_EXP(TagAmountNetReconciliation);					//ISO8583 field  97
}

namespace posprxy_tags
{
    using com_verifone_TLVLite::ConstData_s;
    TLVLITE_EXP(TagCode);
    TLVLITE_EXP(TagLoginState);
    TLVLITE_EXP(TagFPState);
    TLVLITE_EXP(TagFPConfig);
    TLVLITE_EXP(TagDateTime);
    TLVLITE_EXP(TagDeviceID);
    TLVLITE_EXP(TagPumpNo);
    TLVLITE_EXP(TagNozzleNo);
    TLVLITE_EXP(TagTransSeqNo);
    TLVLITE_EXP(TagReleaseToken);
    TLVLITE_EXP(TagAmount);
    TLVLITE_EXP(TagVolume);
    TLVLITE_EXP(TagUnitPrice);
    TLVLITE_EXP(TagVolumeProduct1);
    TLVLITE_EXP(TagVolumeProduct2);
    TLVLITE_EXP(TagProductNo1);
    TLVLITE_EXP(TagProductNo2);
    TLVLITE_EXP(TagBlendRatio);
    TLVLITE_EXP(TagMIDLinesNarrow);
    TLVLITE_EXP(TagMIDLinesWide);
    TLVLITE_EXP(TagErrorCode);
    TLVLITE_EXP(TagDeviceState);
    TLVLITE_EXP(TagEmmergencyStop);
    TLVLITE_EXP(TagMaxTrxAmount);
    TLVLITE_EXP(TagMaxTrxVolume);
    TLVLITE_EXP(TagReceiptRequest);
    TLVLITE_EXP(TagPaymentTypeCard);
    TLVLITE_EXP(TagPaymentCardType);
    TLVLITE_EXP(TagCardPan);
    TLVLITE_EXP(TagCardAmount);
    TLVLITE_EXP(TagPaymentTypeCash);
    TLVLITE_EXP(TagCashDenomination);
    TLVLITE_EXP(TagCashNumber);
    TLVLITE_EXP(TagCashAmount);
    TLVLITE_EXP(TagPaymentTypeCoupon);
    TLVLITE_EXP(TagCouponType);
    TLVLITE_EXP(TagCouponCode);
    TLVLITE_EXP(TagCouponAmount);
    TLVLITE_EXP(TagPaymentTypeMagicNumber);
    TLVLITE_EXP(TagMagicNumberCode);
    TLVLITE_EXP(TagMagicNumberAmount);
    TLVLITE_EXP(TagLockFuelSale);
}

#undef TLVLITE_EXP

#endif // TAGS_H



