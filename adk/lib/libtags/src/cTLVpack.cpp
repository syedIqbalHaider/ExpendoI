///////////////////////////////////////////////////////////
//  cBaseInterface.cpp
//  Implementation of the Class cBaseInterface
//  Created on:      16-Oct-2007 09:10:56
//  Original author: Nick Tristram
///////////////////////////////////////////////////////////
#include "time.h"
#include "cTLVpack.h"
#include "libpml/pml_port.h"

int cTLVpack::AcmSendReceive( 
    char *pszFromAppName, 
    char const * pszToAppName, 
    unsigned char *pucMessage, 
    unsigned long ulLength, 
    int *piRxLength, 
    unsigned long ulTimeout, 
    short (*pfCVMCancellationFunction)(void *),
    void *pComm
    )
{
    unsigned long ulRxTimeout = read_ticks() + ulTimeout;
    unsigned char aucCancelMsg[] = { CMD_CANCEL_OPERATION };
    char szFromAppName[MAX_APP_NAME+1];

    int iRet = 0;
    dlog_msg("FROM-APP: %s TO-APP %s",pszFromAppName,pszToAppName);
    dlog_hex(pucMessage, ulLength, "SEND TO APP");
/* NKJT
    if( (iRet = pcom_send( pucMessage, ulLength, pszFromAppName, pszToAppName )) != 0 )
    {
        dlog_msg("error pcom_send ret %d", iRet);
    }
    else
    {
        //dlog_msg( "pcom_send ret %d, receiving from %s", iRet, pszDestAppName );

        memset(szFromAppName, 0, sizeof(szFromAppName));
        while( 1 )
        {
            // wait for response
            strncpy(szFromAppName, pszToAppName, sizeof(szFromAppName)-1);
            iRet = pcom_receive( pucMessage, piRxLength, szFromAppName, NULL, NULL, MAX_PCOM_APP_MSG_LEN );
            if( iRet != ERROR_PCOM_TIMEOUT ) // received okay or other error
            {
                dlog_msg("Not timeout code %d",iRet);
            	break;
            }

            // no data yet, check cancel callback fn if present
            if( pfCVMCancellationFunction && pfCVMCancellationFunction(pComm) != 0 )
            {
                // send a cancel to the other app
                if( (iRet = pcom_send( aucCancelMsg, 1, pszFromAppName, pszToAppName )) != 0 )
                {
                    dlog_msg( "error sending cancel, pcom_send ret %d", iRet );
                }
                dlog_msg("cancel MSG !!!");
                // don't wait for cancel response, this will come in the operation response
            }
            if( ulTimeout && read_ticks() >= ulRxTimeout ) //if ulTimeout == 0, no timeout!
            {
                dlog_msg( "receive timeout" );
                iRet = ERROR_PCOM_TIMEOUT;
                break;
            }
        }
    }
*/    
    dlog_msg("RETURN SUCCESS WITH CODE %d",iRet);
    return iRet;
}

int cTLVpack::ExtractTagData( u_char *pData, uint16_t iLength, const ALLOW_ENTRY *pstList )
{
	BER_TLV *pstTag;
	int usnEC = SUCCESS;

	pstTag = pstTLV_make_element();

    if (pstTag)
    {
        // Extract the TLV triplets from the message.
    	usnEC = usnTLV_parse_buffer( pstTag, pData, iLength );
    	if (usnEC == SUCCESS) 
    	{
         usnEC = usnTLV_check_tags( pstTag, pstList );	
    		if (usnEC == SUCCESS || usnEC == ERR_BER_TLV_ELEMENT_NOT_EXPECTED)
    		{
    			// Fill the command arguments
    			usnEC = usnTLV_fill_command_arguments( NULL, pstTag, pstList );
         }
      }
      vTLV_destroy_element(pstTag);
   }
   return usnEC;
}

BER_TLV *cTLVpack::GetFirstTag( BER_TLV *pstEl, const u_char *pbyTag, u_char byTagLen, u_char *pbyValue, u_long ulnLen )
{
	u_char bFound = FALSE;

	while (pstEl && !bFound)
	{
		if (byTagLen == pstEl->byTagLen &&
			!memcmp(pstEl->abyTag, pbyTag, byTagLen))
		{
			bFound = TRUE;
		}
		else
		{
			pstEl = pstEl->pstNext;
		}
	}

    if( bFound == FALSE )
    {
        if( byTagLen == 1 )
        {
            dlog_msg( "error finding tag %2.2X", pbyTag[0] );
        }
        else if( byTagLen == 2 )
        {
            dlog_msg( "error finding tag %2.2X%2.2X", pbyTag[0], pbyTag[1] );
        }
        else if( byTagLen == 3 )
        {
            dlog_msg( "error finding tag %2.2X%2.2X%2.2X", pbyTag[0], pbyTag[1], pbyTag[2] );
        }
        pstEl = NULL;
    }
    else
    {
        // found it, copy data
        if( pstEl->ulnLen <= ulnLen )
        {
            if( pbyValue )
            {
                memset( pbyValue, 0, ulnLen );
                memcpy( pbyValue, pstEl->pbyValue, pstEl->ulnLen );
            }
        }
    }
    return pstEl;
}

BER_TLV *cTLVpack::GetNextTag( BER_TLV *pstEl, const u_char *pbyTag, u_char byTagLen, u_char *pbyValue, u_long ulnLen )
{
    return GetFirstTag( pstEl->pstNext, pbyTag, byTagLen, pbyValue, ulnLen );
}

int cTLVpack::ExtractSingleTag( BER_TLV *pstEl, const u_char *pbyTag, u_char byTagLen, u_char *pbyValue, u_long ulnLen )
{
    u_char bFound = FALSE;
    int iRet = SUCCESS;

    if ( lastFound && byTagLen == lastFound->byTagLen && !memcmp( pbyTag, lastFound->abyTag, lastFound->byTagLen ) )
    {
        pstEl = lastFound->pstNext;
    }
    while (pstEl && !bFound)
    {
      if (byTagLen == pstEl->byTagLen &&
          !memcmp(pstEl->abyTag, pbyTag, byTagLen))
      {
          bFound = TRUE;
      }
      else
      {
        pstEl = pstEl->pstNext;
      }
    }

    if( bFound == FALSE )
    {
        if( byTagLen == 1 )
        {
            dlog_error("error finding tag %2.2X", pbyTag[0] );
        }
        else if( byTagLen == 2 )
        {
            dlog_error("error finding tag %2.2X%2.2X", pbyTag[0], pbyTag[1] );
        }
        else if( byTagLen == 3 )
        {
            dlog_error("error finding tag %2.2X%2.2X%2.2X", pbyTag[0], pbyTag[1], pbyTag[2] );
        }
        lastFound = 0;
        iRet = -1;
    }
    else
    {
        iRet = -2;
        // found it, copy data
        if( pstEl->ulnLen <= ulnLen )
        {
            if( pbyValue )
            {
                memset( pbyValue, 0, ulnLen );
                memcpy( pbyValue, pstEl->pbyValue, pstEl->ulnLen );
                iRet = SUCCESS;
            }
            lastFound = pstEl;
        }
    }
    return iRet;
}
