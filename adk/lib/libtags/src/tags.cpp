#ifndef __cplusplus
#  error "This file is for C++ only!"
#endif
/***************************************************************************
**
 *
 * Copyright (C) 2009 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 *
 **************************************************************************/

/**
 * @file    		tags.cpp
 *
 * @author  	Nick Tristram
 *
 * @brief   	TLV Parser definitions and utilities
 *
 * @remarks  	This file should be compliant with Verifone EMEA R&D C++ Coding
 *           		Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/
#include <svc.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <libtags/tags.h>
#include <tlv-lite/ConstData.h>

/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
namespace internal
{

	/**
	 * @addtogroup tags
	 * @{
	 */
	namespace
	{
		// general Tags
		const uint8_t TagCode[] = { 0xDF, 0x30 }; // command response code
		const uint8_t TagPosTimeout[] = { 0xDF, 0x1F };

		// display command
		const uint8_t TagDisplayText[] = { 0xDF, 0x81, 0x04 }; // display text
		const uint8_t TagDisplayXPos[] = { 0xDF, 0x81, 0x05 }; // display x position (column)
		const uint8_t TagDisplayYPos[] = { 0xDF, 0x81, 0x06 }; // display y position (row)
		const uint8_t TagBacklightFlag[] = { 0xDF, 0x81, 0x07 }; // display backlight flag
		const uint8_t TagDisplayIconFrom[] = { 0xDF, 0x81, 0x08 }; // display icon flag (from)
		const uint8_t TagDisplayContrast[] = { 0xDF, 0x81, 0x09 }; // display contrast
		const uint8_t TagDisplayIconTo[] = { 0xDF, 0x81, 0x0B }; // display icon flag (to)
		const uint8_t TagKbdBacklightFlag[] = { 0xDF, 0x81, 0x0A }; // keyboard backlight flag

		// display	command
		const uint8_t TagPromptFile[]				= { 0xDF, 0x81, 0x30}; // prompt file to be used when displaying prompts
		const uint8_t TagPromptSection[]		= { 0xDF, 0x81, 0x31}; // prompt section to search prompt in
		const uint8_t TagPromptIndex[]			= { 0xDF, 0x81, 0x32}; // prompt index to display
		const uint8_t TagPromptIndex2[]			= { 0xDF, 0x81, 0x33}; // prompt index to display (2)
		const uint8_t TagPromptIndex3[]			= { 0xDF, 0x81, 0x34}; // prompt index to display (3)
		const uint8_t TagPromptIndex4[]			= { 0xDF, 0x81, 0x35}; // prompt index to display (4)
		const uint8_t TagPromptAnimation[]	= { 0xDF, 0x81, 0x36}; // prompt index to display (4)
		// wait for keypress command
		const uint8_t TagTimeoutMS[] = { 0xDF, 0x81, 0x0C }; // timeout
		const uint8_t TagKeyValue[] = { 0xDF, 0x45 };       	// key value

		// get pin fields
		const uint8_t TagTextLine1[] = { 0xDF, 0x81, 0x0D }; // line1 text IN
		const uint8_t TagTextLine2[] = { 0xDF, 0x81, 0x0E }; // line2 text IN
		const uint8_t TagTextLine3[] = { 0xDF, 0x81, 0x0F }; // line3 text IN
		const uint8_t TagPINClearText[] = { 0xDF, 0x81, 0x10 }; // cleartext PIN OUT
		const uint8_t TagPINParams[] = { 0xDF, 0x81, 0x11 }; // pin entry parameter struct
		const uint8_t TagPINFlags[] = { 0xDF, 0x81, 0x1B }; // pin entry flags
		const uint8_t TagwXMLName[] = { 0xDF, 0x81, 0x1C };
		const uint8_t TagwXMLStrVal[] = { 0xDF, 0x81, 0x1D };
		const uint8_t TagwXMLPromptVal[] = { 0xDF, 0x81, 0x1E };
		const uint8_t TagwXMLGroup[] = { 0xDF, 0x81, 0x1F };
		// alpha/numeric fields
		const uint8_t TagEntryParams[] = { 0xDF, 0x81, 0x12 }; // entry parameters for alpha/numeric entry
		const uint8_t TagDataInput[] = { 0xDF, 0x33 };       // input from numeric/alphanumeric input

		// multi-item selection fields
		const uint8_t TagSelectionItem[] = { 0xDF, 0x81, 0x13 };
		const uint8_t TagSelectKeyText[] = { 0xDF, 0x81, 0x14 };
		const uint8_t TagSelectedItem[] = { 0xDF, 0x81, 0x15 };

		// update icon fields
		const uint8_t TagIconId[] = { 0xDF, 0x81, 0x16 };
		const uint8_t TagIconLevel[] = { 0xDF, 0x81, 0x17 };
		const uint8_t TagIconStatus[] = { 0xDF, 0x81, 0x18 };

		// display bitmap fields
		const uint8_t TagGraphicFilename[] = { 0xDF, 0x81, 0x19 };
		const uint8_t TagGraphicAlignment[] = { 0xDF, 0x81, 0x1A };

		// Selection list style moved to the config file gui.cfg
		const uint8_t TagSelListStyle[] = { 0xDF, 0x81, 0x20 };

		// beeper functionality
		const uint8_t TagBeeperInterval[] = { 0xDF, 0x81, 0x21 };

		// enhanced cmd to display the whole screen, rather than displaying single line
		const uint8_t TagLineCount[] = { 0xDF, 0x81, 0x22 };

		// block GuiApp from sending response back - for situations where command doesn't need to be ACKed
		const uint8_t TagBlockResponse[] = { 0xDF, 0x81, 0x23 };

		// multiple list selection
		const uint8_t TagPrevListEnabled[] = { 0xDF, 0x81, 0x24 };
		const uint8_t TagNextListEnabled[] = { 0xDF, 0x81, 0x25 };

		// Font style/size for predefined screens
		const uint8_t TagLineLength[] = { 0xDF, 0x81, 0x26 };

		//Tag report item
		const uint8_t TagReportItem[] = { 0xDF, 0x81, 0x27 };

		// Tag for matching keypress command
		const uint8_t TagMatchKeys[] = { 0xDF, 0x81, 0x28 };

		// Tag for keypress monitor status (command also uses TagMatchKeys)
		const uint8_t TagMonitorKeys[] = { 0xDF, 0x81, 0x2F };

		// Font filename
		const uint8_t TagFontFilename[] = { 0xDF, 0xA2, 0x10 };
		const uint8_t TagFontEncoding[] = { 0xDF, 0x81, 0x2B };
		const uint8_t TagFontStyle[] = { 0xDF, 0x81, 0x29 };
		const uint8_t TagFontAdd2Cache[] = { 0xDF, 0x81, 0x2A };
		const uint8_t TagFontSize[] = { 0xDF, 0x81, 0x2D };
		const uint8_t TagFontFlags[] = { 0xDF, 0x81, 0x2E };

		// VfNano
		const uint8_t TagDefaultLang[] = { 0xDF, 0x04};	// Default Language (2 bytes)
		const uint8_t TagNumberOfLanguages[] = { 0xDF, 0x81, 0x2C }; // Number of available languages
		const uint8_t TagURL[] = { 0xDF, 0x08};	// URL
		const uint8_t TagStatus[] = { 0xDF, 0x31};	// Status
		const uint8_t TagResponseEvtBitmap[] = { 0xDF, 0x32};	// Response event bitmap
		const uint8_t TagInputTerminator[] = { 0xDF, 0x45};	// Softkey/Input terminator
		const uint8_t TagTaskId[] = { 0xDF, 0x81, 0x55 };
		const uint8_t TagTaskGraphXPos[] = { 0xDF, 0x81, 0x56 };
		const uint8_t TagTaskGraphYPos[] = { 0xDF, 0x81, 0x57 };
		const uint8_t TagGraphForegroundColor[] = { 0xDF, 0x81, 0x58 };
		const uint8_t TagGraphBackgroundColor[] = { 0xDF, 0x81, 0x59 };
		const uint8_t TagBoxBackgroundColor[] = { 0xDF, 0x81, 0x5A };
		const uint8_t TagBoxBorderColor[] = { 0xDF, 0x81, 0x5B };
		const uint8_t TagBoxForegroundColor[] = { 0xDF, 0x81, 0x5C };
		const uint8_t TagBoxBorderSize[] = { 0xDF, 0x81, 0x5D };
		const uint8_t TagBannerShow[] = { 0xDF, 0x81, 0x5E };
		const uint8_t TagBannerFilename[] = { 0xDF, 0x81, 0x5F };
		const uint8_t TagBannerReservedY[] = { 0xDF, 0x81, 0x60 };
		const uint8_t TagBannerShowLeds[] = { 0xDF, 0x81, 0x61 };
		const uint8_t TagBannerLedMaskOn[] = { 0xDF, 0x81, 0x62 };
		const uint8_t TagBannerLedMaskOff[] = { 0xDF, 0x81, 0x63 };
		const uint8_t TagImagesNum[] =        { 0xDF, 0x81, 0x64 };
		const uint8_t TagImagesRefreshTime[]={ 0xDF, 0x81, 0x65 };
		const uint8_t TagLedsReservedY[] = { 0xDF, 0x81, 0x66 };
		// Printer
		const uint8_t TagBeepOnLength[]		= { 0xDF, 0x0C };	// Beep On Length (2 bytes)
		const uint8_t TagBeepOffLength[] 	= { 0xDF, 0x02 };	// Beep Off Length (2 bytes)
		const uint8_t TagBeepCount[] 		= { 0xDF, 0x03 };
		const uint8_t TagPrintMessage[] 	= { 0xDF, 0x2F };
		const uint8_t TagPOSProtocol[]		= { 0xDF, 0x3F };
		const uint8_t TagWaitForPrinting[]	= { 0xDF, 0x4F };
		const uint8_t TagWaitForTail[]		= { 0xDF, 0x5F };

		const uint8_t TagBarCode[] 			= { 0xDF, 0x81, 0x10 };
		const uint8_t TagBarCodeEncoding[]	= { 0xDF, 0x81, 0x11 };
		const uint8_t TagBarCodeMargin[]	= { 0xDF, 0x81, 0x12 };
		const uint8_t TagBarCodeScale[]		= { 0xDF, 0x81, 0x13 };
		const uint8_t TagTemplateFilename[] = { 0xDF, 0x81, 0x14 };
		const uint8_t TagTemplateNames[] = { 0xDF, 0x81, 0x15 };
		const uint8_t TagOutputFilename[] = { 0xDF, 0x81, 0x16 };
		const uint8_t TagIconBar[] = { 0xDF, 0x81, 0x7F };
		const uint8_t TagReportPromptIndex[] = { 0xDF, 0x81, 0x7E };
		const uint8_t TemplateAdditionalTags[] = { 0xFF, 0x7D }; // template to store additional tags

		const uint8_t TagTemplateErrorRow[] = { 0xDF, 0x81, 0x7D };
		const uint8_t TagTemplateErrorCol[] = { 0xDF, 0x81, 0x7C };
		const uint8_t TagTemplateErrorDesc[] = { 0xDF, 0x81, 0x7B };

		// MAPP tags and templates
		const uint8_t TemplateData[] = { 0xE0 };
		const uint8_t TemplateRequestedElements[] = { 0xE1 };
		const uint8_t TemplateDecisionRequired[] = { 0xE2 };
		const uint8_t TemplateTC[] = { 0xE3 };
		const uint8_t TemplateARQC[] = { 0xE4 };
		const uint8_t TemplateAAC[] = { 0xE5 };
		const uint8_t TemplateStatus[] = { 0xE6 };
		const uint8_t TemplateContactlessMSD[] = { 0xE7 };
		// Statuses
		const uint8_t TagTranStatus[] = { 0xC3 };
		const uint8_t TagTranStatusDesc[] = { 0xC4 };
		const uint8_t TagStatusPINTryCounter[] = { 0xC5 };
		const uint8_t TagTerminalID[] = { 0x9F, 0x1C };
		const uint8_t TagTerminalSerialNum[] = { 0x9F, 0x1E };

		// Tags for cards
		const uint8_t TagCardStatus[] = { 0x48 };
		const uint8_t TagATR[] = { 0x63 };
		const uint8_t TagTrack1[] = { 0x5F, 0x21 };
		const uint8_t TagTrack2[] = { 0x5F, 0x22 };
		const uint8_t TagTrack3[] = { 0x5F, 0x23 };
		const uint8_t TagSwipeStatus[] = { 0xDF, 0xDF, 0x6E };
		// Tags for files
		const uint8_t TemplateFile[] = { 0x6F };
		const uint8_t TagFileSize[] = { 0x80 };
		const uint8_t TagFileSize1[] = { 0x81 };
		const uint8_t TagFileDescriptor[] = { 0x82 };
		const uint8_t TagFileIdentifier[] = { 0x83 };
		const uint8_t TagFileName[] = { 0x84 };
		const uint8_t TagFileEFId[] = { 0x87 };
		const uint8_t TagFileChecksum[] = { 0x88 };
		const uint8_t TagFileSecured[] = { 0x89 };
		const uint8_t TagFreeSpace[] = { 0xDF, 0xDE, 0x7F }; // Free Space
		const uint8_t TagDrive[] = { 0xDF, 0xDE, 0x7E }; // Drive
		const uint8_t TagFileOffset[] = { 0xDF, 0xDE, 0x7D };
		const uint8_t TagFileReadSize[] = { 0xDF, 0xDE, 0x7C };
		// Config files versions
		const uint8_t TemplateConfigVersion[] = { 0xED };
		const uint8_t TemplateOSVersion[] = { 0xEE };
		const uint8_t TemplateModuleVersion[] = { 0xEF };
		const uint8_t TagConfigFileName[] = { 0xDF, 0x0D };
		const uint8_t TagConfigFileVersion[] = { 0xDF, 0x7F };
		const uint8_t TagConfigFileChecksum[] = { 0xDF, 0x7E };
		const uint8_t TagLibraryFileName[] = { 0xDF, 0x81, 0x06 };
		const uint8_t TagLibraryFileVersion[] = { 0xDF, 0x81, 0x07 };
		const uint8_t TagDeviceArchitecture[] = { 0xDF, 0x81, 0x08 };
		const uint8_t TagDeviceArchitectureVersion[] = { 0xDF, 0x81, 0x09 };
		const uint8_t TagTamper[] = { 0xDF, 0x81, 0x01 };
		const uint8_t TagSwitchTripped[] = { 0xDF, 0x81, 0x02 };
		const uint8_t TagPlatformType[] = { 0xDF, 0x81, 0x03 };

		// Entry
		const uint8_t TagNumericLanguageIndices[] = { 0xDF, 0xA2, 0x06 };
		const uint8_t TagNumberFormat[] = { 0xDF, 0xA2, 0x07 };
		const uint8_t TagNumericEdit[] = { 0xDF, 0xA2, 0x08 };
		const uint8_t TagNumericLanguageText[] = { 0xDF, 0xA2, 0x13 };
		const uint8_t TagAlphanumericEdit[] = { 0xDF, 0x83, 0x01 };
		const uint8_t TagEntryMaxLength[] = { 0xDF, 0x83, 0x05 };
		const uint8_t TagEntryMinLength[] = { 0xDF, 0x83, 0x06 };
		const uint8_t TagPrefixSymbol[] = { 0xDF, 0xB0, 0x02 };
		const uint8_t TagSuffixSymbol[] = { 0xDF, 0xB0, 0x03 };
		const uint8_t TagUnmaskedCharactersCount[] = { 0xDF, 0xB0, 0x04 };
		const uint8_t TagAlphaModes[] = { 0xDF, 0xB0, 0x05 };
		const uint8_t TagAlphaInitMode[] = { 0xDF, 0xB0, 0x06 };
		// Display
		const uint8_t TagBacklightMode[] = { 0xDF, 0xA2, 0x0F };
		const uint8_t TagKbdBacklightMode[] = { 0xDF, 0xA2, 0x0E };
		const uint8_t TagDisplayContrastMapp[] = { 0xDF, 0xA2, 0x0B };
		const uint8_t TagDisplayBitmapFileName[] = { 0xDF, 0xA3, 0x01 };
		const uint8_t TagDisplayBitmapCol[] = { 0xDF, 0xA3, 0x02 };
		const uint8_t TagDisplayBitmapRow[] = { 0xDF, 0xA3, 0x03 };
		// Keyboard
		const uint8_t TagKeyPress[] = { 0xDF, 0xA2, 0x05 };
		// language
		const uint8_t TagLanguage[] = { 0xDF, 0xA2, 0x20 };
		const uint8_t TagLanguageSelect[] = { 0xDF, 0xA2, 0x22 };
		// printing
		const uint8_t TagBarcode[] = { 0xDF, 0xA3, 0x10 };
		const uint8_t TagBarcodeHeight[] = { 0xDF, 0xA3, 0x12 };
		const uint8_t TagBarcodeWidth[] = { 0xDF, 0xA3, 0x13 };
		const uint8_t TagBarcodeEncoding[] = { 0xDF, 0x81, 0x11 }; // Barcode Encoding  (required)
		const uint8_t TagBarcodeMargin[] = { 0xDF, 0x81, 0x12 }; // Barcode Margin (optional)
		const uint8_t TagBarcodeScale[] = { 0xDF, 0x81, 0x13 }; // Barcode Scale (optional)
		const uint8_t TagPrintTemplateFilename[] = { 0xDF, 0x81, 0x20 }; // Print Template filename (mandatory)
		const uint8_t TagPrintTemplateNames[] = { 0xDF, 0x81, 0x21 }; // Print Template template names (optional)
		const uint8_t TemplatePrintAdditionalTags[] = { 0xDF, 0x81, 0x22 }; // Print Template tags (optional)
		const uint8_t TagBitmapFilename[] = { 0xDF, 0xA3, 0x01 }; // Print Bitmap - Filename
		const uint8_t TagBitmapAlignment[] = { 0xDF, 0xA3, 0x04 }; // Print Bitmap - Alignment
		const uint8_t TagTemplateRow[] = { 0xDF, 0xA2, 0x30 };
		const uint8_t TagTemplateCol[] = { 0xDF, 0xA2, 0x31 };
		const uint8_t TagTemplateDesc[] = { 0xDF, 0xA2, 0x32 };
		// Request Choice
		//const uint8_t TagOptionInt[] = { 0xDF, 0xA2, 0x01 };
		const uint8_t TagOptionID[] = { 0xDF, 0xA2, 0x02 };
		const uint8_t TagOptionText[] = { 0xDF, 0xA2, 0x03 };
		const uint8_t TagMenuTitle[] = { 0xDF, 0xA2, 0x11 };
		const uint8_t TagSelListMode[] = { 0xDF, 0xA2, 0x12 };
		// Reset device
		const uint8_t TagSerialNumber[] = { 0x9F, 0x1E };   // terminal serial number
		const uint8_t TagMaxChainedSize[] = { 0xDF, 0xA2, 0x1D }; // protocol properties supported
		const uint8_t TagSafeModeIndicator[] = { 0xDF, 0xA2, 0x1E }; // safe mode indicator
		// Security commands
		const uint8_t TagKeyType[] = { 0xDF, 0xEC, 0x46 };
		const uint8_t TagKeyData[] = { 0xDF, 0xEC, 0x2E };
		const uint8_t TagResultCode[] = { 0xDF, 0xEC, 0x30 }; // <- tagResult
		const uint8_t TagPinEntryType[] = { 0xDF, 0xEC, 0x7D };
		const uint8_t TagPosSecurityTimeout[] = { 0xDF, 0xEC, 0x1F };
		const uint8_t TagHostId[] = { 0xDF, 0xEC, 0x23 };
		const uint8_t TagPinBlockFormat[] = { 0xDF, 0xED, 0x08 };
		const uint8_t TagKeyManagement[] = { 0xDF, 0xED, 0x09 };
		const uint8_t TagPinAlgorithm[] = { 0xDF, 0xED, 0x0A };
		const uint8_t TagDataAlgorithm[] = { 0xDF, 0xED, 0x0B };
		const uint8_t TagMessageForMAC[] = { 0xDF, 0xEC, 0x0E };
		const uint8_t TagMessForEnc[] = { 0xDF, 0xEC, 0x0F };
		const uint8_t TagOutputData[] = { 0xDF, 0xEC, 0x7B };
		const uint8_t TagMaxPINLength[] = { 0xDF, 0xED, 0x05 };
		const uint8_t TagMinPINLength[] = { 0xDF, 0xED, 0x04 };
		const uint8_t TagOnlinePINCipher[] = { 0xDF, 0xED, 0x6C };
		const uint8_t TagPan[] = { 0x5A };
		const uint8_t TagStan[] = { 0xDF, 0xEC, 0x7C };
		const uint8_t TagPinCancel[] = { 0xDF, 0xED, 0x07 };
		const uint8_t TagPinEntryTimeout[] = { 0xDF, 0xA2, 0x0E };
		const uint8_t TagPinEntryFirstCharTimeout[] = { 0xDF, 0xB0, 0x0E };
		const uint8_t TagPinEntryInterCharTimeout[] = { 0xDF, 0xB0, 0x0F };
		const uint8_t TagPinTryFlag[] = { 0xDF, 0xEC, 0x05 };
		const uint8_t TagKSN[] = { 0xDF, 0xED, 0x03 };
		const uint8_t TagScriptName[] = { 0xDF, 0xED, 0x0C };
		const uint8_t TagAddScreenText[] = { 0xDF, 0xED, 0x02 };
		const uint8_t TagFlags[] = { 0xDF, 0xED, 0x0D };
		const uint8_t TagLEDOperation[] = { 0xDF, 0xED, 0x0E };
		const uint8_t TagLEDDuration[] = { 0xDF, 0xED, 0x0F };
		const uint8_t TagLEDRepeats[] = { 0xDF, 0xED, 0x10 };
		// Battery
		const uint8_t TagBatteryStatus[] = { 0xDF, 0xA2, 0x09 };
		// Contactless
		const uint8_t TagMifareOperation[] = { 0xDF, 0xA5, 0x01 }; // Read/Write/EPurse
		const uint8_t TagMifareOperationID[] = { 0xDF, 0xA5, 0x02 }; // ID, informative only
		// EMV VIPA
		const uint8_t TagContinueFlag[] = { 0xC0 };
		const uint8_t TagCashbackFlag[] = { 0xC1 };
		const uint8_t TagAcquirerID[] = { 0xC2 };
		//const uint8_t TagForceOnline[] = { 0xC3 };
		const uint8_t TagPinResult[] = { 0xDF, 0xA2, 0x0A };
		const uint8_t TagCardholderVerificationCompleted[]   = { 0xDF, 0xDF, 0x28 }; // was DF28!!
		const uint8_t TagPinEntryStyle[] = { 0xDF, 0xA2, 0x18 };
		const uint8_t TagRadixSeparator[] = { 0xDF, 0xA2, 0x17 };
		const uint8_t TagPinpadSelectsApplication[] = { 0xDF, 0xA2, 0x04 };
		const uint8_t TagScripts71Results[] = { 0xDF, 0xDF, 0x29 }; // was DF29!!
		const uint8_t TagScripts72Results[] = { 0xDF, 0xDF, 0x2A }; // was DF2A!!
		const uint8_t TagTACDefault[] = { 0xDF, 0xDF, 0x06 };
		const uint8_t TagTACDenial[] = { 0xDF, 0xDF, 0x07 };
		const uint8_t TagTACOnline[] = { 0xDF, 0xDF, 0x08 };
		const uint8_t TagROSThreshold[] = { 0xDF, 0xDF, 0x0A };
		const uint8_t TagROSTargetRSPercent[] = { 0xDF, 0xDF, 0x0B };
		const uint8_t TagROSMaxTargetRSPercent[] = { 0xDF, 0xDF, 0x0C };
		const uint8_t TagForceOnline[] = { 0xDF, 0xDF, 0x0D };
		const uint8_t TagDioneCashbackCheck[] =  { 0xDF, 0xA2, 0x1E };
		const uint8_t TagReasonOnline[] = { 0xDF, 0xDF, 0x25 };
		const uint8_t TagSuppressDisplay[] = { 0xDF, 0xA2, 0x14 };
		const uint8_t TagPinEntryCurrencySymbol[] = { 0xDF, 0xA2, 0x19 };
		const uint8_t TagPinEntryCurrencyLoc[] = { 0xDF, 0xA2, 0x1A };
		const uint8_t TagPinEntryBeeper[] = { 0xDF, 0xA2, 0x1B };
		const uint8_t TagSimplifiedRefunds[] = { 0xDF, 0xA2, 0x1F };
		const uint8_t TagICCPublicKey[] = { 0xDF, 0xDF, 0x0E };
		const uint8_t TagICCPublicKeyLegacy[] = { 0xDF, 0x0E };
		const uint8_t TagShowIncorrectPIN[] = {0xDF, 0xA2, 0x23};

		// GuiApp XMLs
		const uint8_t TagItemID[] = { 0xDF, 0xA5, 0x20 };
		const uint8_t TagItemText[] = { 0xDF, 0xA5, 0x21 };
		const uint8_t TagItemInt[] = { 0xDF, 0xA5, 0x22 };

		// from libtlvpool
		const uint8_t TagMachineType[] = { 0xDF, 0xA2, 0x0C };
		const uint8_t TagCardReaderType[] = { 0xDF, 0xA2, 0x0D };
		const uint8_t TagAlternateInterval[] = { 0xDF, 0xA2, 0x16 };
		const uint8_t TagOverrideLabel[] = { 0xDF, 0xA2, 0x1A };
		const uint8_t TagHideIcons[] = { 0xDF, 0xB0, 0x01 };

		// VSP
		const uint8_t TagVSPStatus[] = { 0xDF, 0xA2, 0x30 };
		const uint8_t TagVSPFullStatus[] = { 0xDF, 0xA2, 0x31 };
		const uint8_t TagVSPVersion[] = { 0xDF, 0xA2, 0x32 };
		const uint8_t TagVSPExtStatus[] = { 0xDF, 0xA2, 0x33 };
		const uint8_t TagVSPDiags[] = { 0xDF, 0xA2, 0x34 };
		const uint8_t TagTGKStatus[] = { 0xDF, 0xA2, 0x3A };
		const uint8_t TagTGKFallbackStatus[] = { 0xDF, 0xA2, 0x3B };

		// VIPA VSP flags
		const uint8_t TagVSPEncryptionStatus[] = { 0xDF, 0xDF, 0x6F };
		const uint8_t TagVSPEParms[] = { 0xDF, 0xDF, 0x70 };

		// Lib SRED for VIPA
		const uint8_t TemplateSRED[] = { 0xFF, 0x7F };
		const uint8_t TemplateSREDPAN[] = { 0xFF, 0x7E };
		const uint8_t TagTrack2Data[] = { 0x9F, 0x6B };
		const uint8_t TagManualPAN[] = { 0xDF, 0xDB, 0x01 };
		const uint8_t TagManualCVV2[] = { 0xDF, 0xDB, 0x02 };
		const uint8_t TagManualExpiryDate[] = { 0xDF, 0xDB, 0x03 };
		const uint8_t TagManualStartDate[] = { 0xDF, 0xDB, 0x04 };
		const uint8_t TagSREDMagstripeTrack1[] = { 0xDF, 0xDB, 0x05 };
		const uint8_t TagSREDMagstripeTrack2[] = { 0xDF, 0xDB, 0x06 };
		const uint8_t TagSREDMagstripeTrack3[] = { 0xDF, 0xDB, 0x07 };
		const uint8_t TagSREDWhitelistStatus[] = { 0xDF, 0xDB, 0x08 };
		const uint8_t TagSREDWhitelistHash[] = { 0xDF, 0xDB, 0x09 };

		const uint8_t TagSREDMaskingScheme[] = { 0xDF, 0xDB, 0x0C };
		const uint8_t TagSREDKeySlot[] = { 0xDF, 0xDB, 0x0D };
		const uint8_t TagSREDStatusIndicator[] = { 0xDF, 0xDB, 0x0E };

		const uint8_t TagSREDEncryptionStatus[] = { 0xDF, 0xDB, 0x0F };
		const uint8_t TagSREDEncryptedData[] = { 0xDF, 0xDF, 0x10 };
		const uint8_t TagSREDKSN[] = { 0xDF, 0xDF, 0x11 };
		const uint8_t TagSREDIV[] = { 0xDF, 0xDF, 0x12 };
		const uint8_t TagSREDEncryptedPAN[] = { 0xDF, 0xDF, 0x13 };
		const uint8_t TagSREDKSNPAN[] = { 0xDF, 0xDF, 0x14 };
		const uint8_t TagSREDIVPAN[] = { 0xDF, 0xDF, 0x15 };
		const uint8_t TagSREDEncryptionStatusPAN[] = { 0xDF, 0xDB, 0x16 };

		// SRED authentication
		const uint8_t TagAuthCertLevel[] = { 0xDF, 0x83, 0x10 };
		const uint8_t TagAuthCertData[] = { 0xDF, 0x83, 0x11 };
		const uint8_t TagAuthCertFile[] = { 0xDF, 0x83, 0x12 };
		const uint8_t TagAuthRandomClrRemote[] = { 0xDF, 0x83, 0x13 };
		const uint8_t TagAuthRandomEncr[] = { 0xDF, 0x83, 0x14 };
		const uint8_t TagAuthKeyStream[] = { 0xDF, 0x83, 0x15 };
		const uint8_t TagAuthDataEncr[] = { 0xDF, 0x83, 0x16 };
		const uint8_t TagAuthDataClr[] = { 0xDF, 0x83, 0x17 };
		const uint8_t TagAuthFlags[] = { 0xDF, 0x83, 0x18 };
		const uint8_t TagAuthSign[] = { 0xDF, 0x83, 0x19 };
		const uint8_t TagAuthDID[] = { 0xDF, 0x83, 0x1A };
		const uint8_t TagAuthCertReq[] = { 0xDF, 0x83, 0x1B };
		const uint8_t TagAuthRSAsize[] = { 0xDF, 0x83, 0x1C };
		const uint8_t TagAuthRandomClrOwn[] = { 0xDF, 0x83, 0x1D };
		const uint8_t TagCardCheckIndicator[] = { 0xDF, 0xDF, 0x05 };

		// GVR pass
		const uint8_t TagARSPassword2[] = { 0xDF, 0xEC, 0x2F };

		// CICAPP
		const uint8_t TagExtResultCode[] = { 0xDF, 0xDF, 0x30 };
		const uint8_t TagBuzzerCtrl[] = { 0xDF, 0xDF, 0x0C };

		// CARDAPP tags
		const uint8_t TagProcFlag[] = {0xDF, 0xCC, 0x58};
		const uint8_t TagATRCardapp[] = {0xDF, 0xCC, 0x61};
		const uint8_t TagInTLV[] = {0xFF, 0xCC, 0x10};
		const uint8_t TagInAPDU[] = {0xFF, 0xCC, 0x20};
		const uint8_t TagOutAPDU[] = {0xFF, 0xCC, 0x21};
		const uint8_t TagOutTLVlist	[] = {0xDF, 0xCC, 0x11};
		const uint8_t TagOperations	[] = {0xDF, 0xCC, 0x56};
		const uint8_t TagOutTLV[] = {0xFF, 0xCC, 0x11};
		const uint8_t TagProcStatus[] = {0xDF, 0xCC, 0x01};
		const uint8_t TagAuthResult[]	= {0xDF, 0xCC, 0x60};
		const uint8_t TagPINtimeout[] = {0xDF, 0xCC, 0x62};
		const uint8_t TagPINfirstcharTimeout[] = {0xDF, 0xCC, 0x65};
		const uint8_t TagPINintercharTimeout[] = {0xDF, 0xCC, 0x66};
		const uint8_t TagEvent[] = {0xDF, 0xCC, 0x63};
		const uint8_t TagActionCode[] = {0xDF, 0x2A};

		const uint8_t TagCardappTrack1[] = {0xDF, 0xCC, 0x41};
		const uint8_t TagCardappTrack2[] = {0xDF, 0xCC, 0x42};
		const uint8_t TagCardappTrack3[] = {0xDF, 0xCC, 0x43};

		const uint8_t TagCardType[] = {0xDF, 0xCC, 0x44};

		const uint8_t TagCardBlacklisted[] = {0xDF, 0xCC, 0x64};

		const uint8_t TagOfflinePINEntryType[] = {0xDF, 0xA2, 0x1C}; // Offline PIN type - Enciphered / Plaintext
		const uint8_t TagPINEntryAtomic[] = {0xDF, 0xA2, 0x1D}; // PIN entry type - Atomic / Normal
		const uint8_t TagCardStatusWord[] = { 0xDF, 0xDF, 0x09 };
		const uint8_t TagPINEntryStatus[] = {0xDF, 0xCC, 0x45};
		const uint8_t TagLEDDuration_CARDAPP[] = {0xDF, 0xCC, 0x46};
		const uint8_t TagLEDRepeats_CARDAPP[] = {0xDF, 0xCC, 0x47};

		const uint8_t TagMemoryCardAddress[] = {0xDF, 0xCC, 0x30};
		const uint8_t TagMemoryCardByteCount[] = {0xDF, 0xCC, 0x31};
		const uint8_t TagMemoryCardData[] = {0xDF, 0xCC, 0x32};
		const uint8_t TagMemoryCardPIN[] = {0xDF, 0xCC, 0x33};

		const uint8_t TagShowIncorrectPINCardapp[] = {0xDF, 0xCC, 0x34};

		// South Africans - new tags
		const uint8_t TagPacketType[] = { 0xDF, 0xAF, 0x01 };
		const uint8_t TagActiveInterfaces[] = { 0xDF, 0xAF, 0x03 };
		const uint8_t TagPartialSelection[] = { 0xDF, 0xAF, 0x11 };
		const uint8_t TagSecondTermAppVer[] = { 0xDF, 0xAF, 0x12 };
		const uint8_t TagRecordApplicationName[] = { 0xDF, 0xAF, 0x13 };
		const uint8_t TagROSThresholdCardapp[] = { 0xDF, 0xAF, 0x14 };
		const uint8_t TagROSTargetRSPercentCardapp[] = { 0xDF, 0xAF, 0x15 };
		const uint8_t TagROSMaxTargetRSPercentCardapp[] = { 0xDF, 0xAF, 0x16 };
		const uint8_t TagTACDefaultCardapp[] = { 0xDF, 0xAF, 0x17 };
		const uint8_t TagTACDenialCardapp[] = { 0xDF, 0xAF, 0x18 };
		const uint8_t TagTACOnlineCardapp[] = { 0xDF, 0xAF, 0x19 };
		const uint8_t TagDefaultTDOL[] = { 0xDF, 0xAF, 0x1A };
		const uint8_t TagDefaultDDOL[] = { 0xDF, 0xAF, 0x1B };
		const uint8_t TagFallback[] = { 0xDF, 0xAF, 0x1C };
		const uint8_t TagAutoAppSelection[] = { 0xDF, 0xAF, 0x1D };
		const uint8_t TagMerchantCategoryCode[] = { 0xDF, 0xAF, 0x1E };
		const uint8_t TagCAPKModulus[] = { 0xDF, 0xAF, 0x0E };
		const uint8_t TagCAPKExponent[] = { 0xDF, 0xAF, 0x0F };
		const uint8_t TagCAPKChecksum[] = { 0xDF, 0xAF, 0x10 };
		const uint8_t TagCAPKFile[] = { 0xDF, 0xAF, 0x20 };
		const uint8_t TagCAPKExpiry[] = { 0xDF, 0xAF, 0x1F };

		//c_auth
		const uint8_t TagCB2_CAPK[] = { 0xDF, 0x43 };
		const uint8_t TagCB2_CAindex[] = { 0xDF, 0x17 };
		const uint8_t TagCB2_NRG[] = { 0xDF, 0x20 };
		const uint8_t TagCB2_OperCert[] = { 0xDF, 0x0D };
		const uint8_t TagCB2_OperExp[] = { 0xDF, 0x15 };
		const uint8_t TagCB2_SupplCert[] = { 0xDF, 0x0C };
		const uint8_t TagCB2_TermCert[] = { 0xDF, 0x0E };
		const uint8_t TagCB2_NRT[] = { 0xDF, 0x1F };
		const uint8_t TagCB2_NRG_ST[] = {0xDF, 0x1D };
		const uint8_t TagCB2_NRT_SG[] = {0xDF, 0x1E };
		const uint8_t TagCB2_SupplNdx[] = { 0xDF, 0x19 };
		const uint8_t TagCB2_KSER_PT[] = { 0xDF, 0x1B };
		const uint8_t TagCB2_KIP_PT[] = { 0xDF, 0x1A };
		const uint8_t TagCB2_KCV_SG[] = { 0xDF, 0x0F };
		const uint8_t TagCB2_KCV_KIP_SG[] = { 0xDF, 0x10 };
		const uint8_t TagCB2_OperRem[] = { 0xDF, 0x2E };
		const uint8_t TagCB2_GTID[] = { 0xDF, 0x11};
		const uint8_t TagCB2_CA_CG[] = { 0xDF, 0x18 };
		const uint8_t TagCB2_PF_exp[] = { 0xDF, 0x14 };
		const uint8_t TagCB2_PF_rem[] = { 0xDF, 0x21 };
		const uint8_t TagCB2_PT_exp[] = { 0xDF, 0x16 };
		const uint8_t TagCB2_PT_rem[] = { 0xDF, 0x2F };
		const uint8_t TagCB2_CA_ndx[] = { 0xDF, 0x17 };
		const uint8_t TagCB2_CF_ndx[] = { 0xDF, 0x19 };

		const uint8_t TagCB2_AdminOper[] = { 0xDF, 0x68 };
		const uint8_t TagCB2_VerApp[] = { 0xDF, 0x67 };
		const uint8_t TagCB2_VerVSS[] = { 0xDF, 0x66 };
		const uint8_t TagCB2_HomeDir[] = {0xDF, 0x65 };
		const uint8_t TagCB2_KeysStatus[] = { 0xDF, 0x64};

		const uint8_t TagCB2_KSN[] = { 0xDF, 0x1C };

		const uint8_t TagCB2_File_CA[] = { 0xDF, 0x58 };
		const uint8_t TagCB2_File_CT[] = { 0xDF, 0x59 };
		const uint8_t TagCB2_File_ST[] = { 0xDF, 0x5A };
		const uint8_t TagCB2_File_MAN[] = { 0xDF, 0x5B };
		const uint8_t TagCB2_File_RND[] = { 0xDF, 0x5C };


		const uint8_t TagCB2_EncData[] = { 0xDF, 0x74 };
		const uint8_t TagCB2_DecData[] = { 0xDF, 0x75 };
		const uint8_t TagCB2_OnlineTrID[] = { 0xDF, 0x76 };
		const uint8_t TagCB2_KeyIndex[] = { 0xDF, 0x31 };
		const uint8_t TagCB2_TerminalID[] = { 0xDF, 0x32 };
		const uint8_t TagCB2_KeyType[] = { 0xDF, 0x77 };
		const uint8_t TagCB2_PAN[] = { 0x5A };
		const uint8_t TagCB2_PINblock[] = { 0xDF, 0x34 };
		const uint8_t TagCB2_KeyIndexAP[] = { 0xDF, 0x35 };

		// SCAPP
		const uint8_t TagAddScreenText1[] = { 0xDF, 0x81, 0x02 };
		const uint8_t TagAddScreenText2[] = { 0xDF, 0x81, 0x12 };
		const uint8_t TagScreenLines[] = { 0xDF, 0x81, 0x13 };
		const uint8_t TagPromptFill[] = { 0xDF, 0x81, 0x14 };
		const uint8_t TagHostIdScapp[] = { 0xDF, 0x23 };
		const uint8_t TagKeyDataScapp[] = { 0xDF, 0x2E };
		const uint8_t TagKeyManagementScapp[] = { 0xDF, 0x81, 0x09 };
		const uint8_t TagKeyTypeScapp[] = { 0xDF, 0x46 };
		const uint8_t TagKSNScapp[] = { 0xDF, 0x81, 0x03 };
		const uint8_t TagLock[] = { 0xDF, 0x81, 0x10 };
		const uint8_t TagMAC[] = { 0xDF, 0x7B };
		const uint8_t TagMaxPINLengthScapp[] = { 0xDF, 0x81, 0x05 };
		const uint8_t TagMessForDec[] = { 0xDF, 0x81, 0x11 };
		const uint8_t TagMessForEncScapp[] = { 0xDF, 0x0F };
		const uint8_t TagMessageForMACScapp[] = { 0xDF, 0x0F };
		const uint8_t TagMinPINLengthScapp[] = { 0xDF, 0x81, 0x04 };
		const uint8_t TagOnlinePINCipherScapp[] = { 0xDF, 0x6C };
		const uint8_t TagPanScapp[] = { 0xDF, 0x36 };
		const uint8_t TagPinAlgorithmScapp[] = { 0xDF, 0x81, 0x0A };
		const uint8_t TagPinBlockFormatScapp[] = { 0xDF, 0x81, 0x08 };
		const uint8_t TagPinCancelScapp[] = { 0xDF, 0x81, 0x07 };
		const uint8_t TagPinEntryTimeoutScapp[] = { 0xDF, 0x81, 0x06 };
		const uint8_t TagPinEntryTypeScapp[] = { 0xDF, 0x7D };
		const uint8_t TagPinTryFlagScapp[] = { 0xDF, 0x05 };
		const uint8_t TagPlainTxtPinBlock[] = { 0xDF, 0x81, 0x07 };
		const uint8_t TagScriptNameScapp[] = { 0xDF, 0x81, 0x0C };
		const uint8_t TagStanScapp[] = { 0xDF, 0x7C };
		const uint8_t TagTransAmount[] = { 0xDF, 0x17 };
		const uint8_t TagTransCatExp[] = { 0xDF, 0x1C };
		const uint8_t TagTransCurrCode[] = { 0xDF, 0x24 };
		const uint8_t TagTransCurrExp[] = { 0xDF, 0x1C };
		const uint8_t TagFlagsScapp[] = { 0xDF, 0x81, 0x0D };
		const uint8_t TagMessagePIN[] = {0xDF, 0x81, 0x1A};
		const uint8_t TagMessageAmount[] = {0xDF, 0x81, 0x1B};
		const uint8_t TagFontFile[] = {0xDF, 0x81, 0x1c};
		const uint8_t TagMonitorPeriod[] = {0xDF, 0x81, 0x1d};
		const uint8_t TagVSSname[] = {0xDF, 0xCC, 0x70};
		const uint8_t TagVSSslot[] = {0xDF, 0xCC, 0x71};
		const uint8_t TagVSSmacro[] = {0xDF, 0xCC, 0x72};
		const uint8_t TagVSSIn[] = {0xDF, 0xCC, 0x73};
		const uint8_t TagVSSOut[] = {0xDF, 0xCC, 0x74};
		const uint8_t TagVSSResult[] = {0xDF, 0xCC, 0x75};
		// EMV tags for online pin verification on Vx700 (passed to CARDAPP)
		const uint8_t TagInput_EMV[] = {0xFF, 0xCC, 0x10};
		const uint8_t TagPINTimeout_EMV[] = {0xDF, 0x81, 0x06};
		const uint8_t TagPinMaxLen[] = {0xDF, 0xA2, 0x1E};
		const uint8_t TagPinMinLen[] = {0xDF, 0xA2, 0x1F};
		const uint8_t TagPinOnlineEntryType[] = {0xDF, 0xA2, 0x20};
		const uint8_t TagPinOnlineCancel[] = {0xDF, 0xA2, 0x21};
		const uint8_t TagPinOnlineExtraMsg[] = {0xDF, 0xA2, 0x22};

		const uint8_t TagDUKPTMaskPIN[] = {0xDF, 0x81, 0x51};
		const uint8_t TagDUKPTMaskMAC[] = {0xDF, 0x81, 0x52};
		const uint8_t TagDUKPTMaskENC[] = {0xDF, 0x81, 0x53};

		const uint8_t TagEMVKernelChecksum[] = {0xDF, 0xDF, 0x05};
		const uint8_t TagEMVKernelVersion[] = {0xDF, 0xDF, 0x04};

		const uint8_t TagPropData[] = {0xDF, 0x81, 0x71};
		/// tags used by cmd_DisplayHTML and related
		const uint8_t TagHTMLUrl[] = {0xDF, 0xAA, 0x01};
		const uint8_t TagHTMLKey[] = {0xDF, 0xAA, 0x02};
		const uint8_t TagHTMLValue[] = {0xDF, 0xAA, 0x03};
		const uint8_t TagHTMLRawValue[] = {0xDF, 0xAA, 0x04};
		const uint8_t TagHTMLResult[] = {0xDF, 0xAA, 0x05};
		const uint8_t TagHTMLDisplayConfirmation[] = {0xDF, 0xAA, 0x06};
		const uint8_t TagHTMLRegion[] = {0xDF, 0xAA, 0x07};

		const uint8_t TagBluetoothName[] = { 0xD0, 0xB2 };

		// OS counters
		const uint8_t TagOSCounterIndex[] = { 0xDF, 0x81, 0x0A };
		const uint8_t TagOSCounterValue[] = { 0xDF, 0x81, 0x0B };
		const uint8_t TagOSCounterName[] = { 0xDF, 0x81, 0x0C };
		// Remote sysmode
		const uint8_t TagOSDevice[] = { 0xDF, 0x81, 0x0D };
		const uint8_t TagOSPort[] = { 0xDF, 0x81, 0x0E };
		// const uint8_t TagOSCounterGroup[] = { 0xDF, 0x81, 0x0D };

		// ECHO MODE TAGS
		const uint8_t TagHTMLEcho[] 	 = {0xDF, 0xAB, 0x01};
		const uint8_t TagHTMLEchoKey[] 	 = {0xDF, 0xAB, 0x02};
		const uint8_t TagHTMLEchoValue[] = {0xDF, 0xAB, 0x03};

		// EBAAPP
		const uint8_t TagDeviceStatus[] = {0xDF, 0xEB, 0x01};
		const uint8_t TagDeviceModelNo[] = {0xDF, 0xEB, 0x02};
		const uint8_t TagDeviceSerialNumber[] = {0xDF, 0xEB, 0x03};
		const uint8_t TagFirmwareVersion[] = {0xDF, 0xEB, 0x04};
		const uint8_t TagLastDetectionResult[] = {0xDF, 0xEB, 0x05};
		const uint8_t TagCurrency[] = {0xDF, 0xEB, 0x10};
		const uint8_t TagDenomination[] = {0xDF, 0xEB, 0x11};
		const uint8_t TagProcessingError[] = {0xDF, 0xEB, 0x12};
		const uint8_t TagDeviceRejectionCode[] = {0xDF, 0xEB, 0x13};
		const uint8_t TagDeviceErrorCode[] = {0xDF, 0xEB, 0x14};
		const uint8_t TagNotificationId[] = {0xDF, 0xEB, 0x15};
		const uint8_t TagFirmwareFileLocn[] = {0xDF, 0xEB, 0x20};
		const uint8_t TagDownloadTimeout[] = {0xDF, 0xEB, 0x21};

		// BCSAPP
		const uint8_t TagBarcodeData[] = {0xDF, 0xBC, 0x01};

		// CB2IF
		const uint8_t TagifOperation[] = {0x9F, 0x80, 0x01};
		const uint8_t TagifTermID[] = {0x9F, 0x80, 0x02};
		const uint8_t TagifTransAmount[] = {0x9F, 0x80, 0x03};
		const uint8_t TagifConfirmAmount[] = {0x9F, 0x80, 0x04};
		const uint8_t TagifPrintTicket[] = {0x9F, 0x80, 0x05};
		const uint8_t TagifAddMessageToHost[] = {0x9F, 0x80, 0x06};
		const uint8_t TagifPaymantType[] = {0x9F, 0x80, 0x07};
		const uint8_t TagifMessageToPrinter[] = {0x9F, 0x80, 0x08};
		const uint8_t TagifEcrId[] = {0x9F, 0x80, 0x09};
		const uint8_t TagifSTAN[] = {0x9F, 0x80, 0x0A};
		const uint8_t TagifActionCode[] = {0x9F, 0x80, 0x0B};
		const uint8_t TagifPAN[] = {0x9F, 0x80, 0x0C};
		const uint8_t TagifTransType[] = {0x9F, 0x80, 0x0D};
		const uint8_t TagifTransDataTime[] = {0x9F, 0x80, 0x0E};
		const uint8_t TagifCardType[] = {0x9F, 0x80, 0x0F};
		const uint8_t TagifAcquirerID[] = {0x9F, 0x80, 0x10};
		const uint8_t TagifOnlineOperation[] = {0x9F, 0x80, 0x11};
		const uint8_t TagifApprovalCode[] = {0x9F, 0x80, 0x12};
		const uint8_t TagifTransStatusEnable[] = {0x9F, 0x80, 0x13};
		const uint8_t TagifGTID[] = {0x9F, 0x80, 0x14};
		const uint8_t TagifLineType[] = {0x9F, 0x80, 0x15};
		const uint8_t TagifNetType[] = {0x9F, 0x80, 0x16};
		const uint8_t TagifHostIP[] = {0x9F, 0x80, 0x17};
		const uint8_t TagifHostPort[] = {0x9F, 0x80, 0x18};
		const uint8_t TagifDHCPEnable[] = {0x9F, 0x80, 0x19};
		const uint8_t TagifIp[] = {0x9F, 0x80, 0x1A};
		const uint8_t TagifGateway1[] = {0x9F, 0x80, 0x1B};
		const uint8_t TagifGateway2[] = {0x9F, 0x80, 0x1C};
		const uint8_t TagifSubnetMask[] = {0x9F, 0x80, 0x1D};
		const uint8_t TagifDNS1[] = {0x9F, 0x80, 0x1E};
		const uint8_t TagifDNS2[] = {0x9F, 0x80, 0x1F};
		const uint8_t TagifTransportType[] = {0x9F, 0x80, 0x20};
		const uint8_t TagifScreenEnable[] = {0x9F, 0x80, 0x21};
		const uint8_t TagifPreResponseEnable[] = {0x9F, 0x80, 0x22};

		const uint8_t TagifTransResult[] = {0x9F, 0x86, 0x01};
		const uint8_t TagifHostErrorMessage[] = {0x9F, 0x86, 0x02};
		const uint8_t TagifAppErrorMessage[] = {0x9F, 0x86, 0x03};
		const uint8_t TagifScreenLine1ID[] = {0x9F, 0x86, 0x04};
		const uint8_t TagifScreenLine2ID[] = {0x9F, 0x86, 0x05};
		const uint8_t TagifScreenLine3ID[] = {0x9F, 0x86, 0x06};
		const uint8_t TagifScreenLine4ID[] = {0x9F, 0x86, 0x07};
		const uint8_t TagifScreenLine1Text[] = {0x9F, 0x86, 0x08};
		const uint8_t TagifScreenLine2Text[] = {0x9F, 0x86, 0x09};
		const uint8_t TagifScreenLine3Text[] = {0x9F, 0x86, 0x0A};
		const uint8_t TagifScreenLine4Text[] = {0x9F, 0x86, 0x0B};

		const uint8_t TagifTransStatus[] = {0x9F, 0x86, 0x20};

		//iso8583
		const uint8_t TagProcessingCode[] = { 0xDF, 0x81, 0x12 };
		const uint8_t TagSystemsTraceAuditNumber[] = { 0xDF, 0x81, 0x13 };
		const uint8_t TagPointOfServiceDataCode[] = { 0xDF, 0x81, 0x14 };
		const uint8_t TagCardSequenceNumber[] = { 0xDF, 0x81, 0x15 };
		const uint8_t TagFunctionCode[] = { 0xDF, 0x81, 0x16 };
		const uint8_t TagMessageReasonCode[] = { 0xDF, 0x81, 0x17 };
		const uint8_t TagCardAcceptorBusinessCode[] = { 0xDF, 0x81, 0x18 };
		const uint8_t TagAcquiringInstitutionsIdentificationCode[] = { 0xDF, 0x81, 0x19 };
		const uint8_t TagPrimaryAccountNumberPANExtended[] = { 0xDF, 0x81, 0x1A };
		const uint8_t TagAdditionalDataNational[] = { 0xDF, 0x81, 0x1B };
		const uint8_t TagPINData[] = { 0xDF, 0x81, 0x1C };
		const uint8_t TagSecurityRelatedControlInformation[] = { 0xDF, 0x81, 0x1D };
		const uint8_t TagICCRelatedData[] = { 0xDF, 0x81, 0x1F };
		const uint8_t TagMessageAuthenticationCode[] = { 0xDF, 0x81, 0x20 };
		const uint8_t TagDateTimeHost[] = { 0xDF, 0x81, 0x21 };
		const uint8_t TagAdditionalDataPrivate[] = { 0xDF, 0x81, 0x22 };
		const uint8_t TagTransportData[] = { 0xDF, 0x81, 0x23 };
		const uint8_t TagAC[] = { 0xDF, 0x81, 0x24 };
		const uint8_t TagDataRecord[] = { 0xDF, 0x81, 0x25 };
		const uint8_t TagPrimaryAccountNumber[] = { 0xDF, 0x81, 0x26 };
		const uint8_t TagAmountTransaction[] = { 0xDF, 0x81, 0x27 };
		const uint8_t TagDateEffective[] = { 0xDF, 0x81, 0x28 };
		const uint8_t TagDateExpiration[] = { 0xDF, 0x81, 0x29 };
		const uint8_t TagAmountNetReconciliation[] = { 0xDF, 0x81, 0x2A };
		const uint8_t TagCurrencyCode[] = { 0xDF, 0x81, 0x2B };
		const uint8_t TagApprovalCode[] = { 0xDF, 0x81, 0x2C };
		const uint8_t TagOriginalDataElements[] = { 0xDF, 0x81, 0x2D };
		const uint8_t TagTrack2data[]= {0xDF, 0x81, 0x2E};
		const uint8_t TagAdditionalResponseData[]= {0xDF, 0x81, 0x2F};

#if 0
		//CB2 Proprietary Tag defined in SPE-DEF
		const uint8_t Tag_ManufacturerIdentifier[] = { 0xDF, 0x12 };
		const uint8_t Tag_TIN[] = { 0xDF, 0x30 };
		const uint8_t Tag_CAkey[] = { 0xDF, 0x43 };
		const uint8_t Tag_FunTmlEnabled[] = { 0xDF, 0x23 };
		const uint8_t Tag_NationCode[] = { 0x9F, 0x1A };
		const uint8_t Tag_CurrencyCode[] = { 0x5F, 0x2A };
		const uint8_t Tag_MerchCategory[] = { 0x9F, 0x15 };
		const uint8_t Tag_LogMode[] = { 0xDF, 0x28 };
		const uint8_t Tag_TmlTimeOut[] = { 0xDF, 0x29 };
		const uint8_t Tag_ConnectGtParams12[] = { 0xFF, 0x05 };
		const uint8_t Tag_ConnectGtParams3[] = { 0xFF, 0x06 };
		const uint8_t Tag_ConnectTMSParams[] = { 0xFF, 0x07 };
		const uint8_t Tag_ConnectTMSMode[] = { 0xFF, 0x08 };
		const uint8_t Tag_TMSMode[] = { 0xDF, 0x36};
		const uint8_t Tag_TMSMerchantMsg[] = { 0xDF, 0x37};
		const uint8_t Tag_AcquirerID[] = { 0x9F, 0x01 };
		const uint8_t Tag_AcquirerName[] = { 0xDF, 0x38 };
		const uint8_t Tag_TickeData[] = { 0xFF, 0x04 };
		const uint8_t Tag_MerchantID[] = { 0x9F, 0x16 };
		const uint8_t Tag_RangeOffline[] = { 0xDF, 0x3D };
		const uint8_t Tag_MsgAutOffline[] = { 0xDF, 0x3E };
		const uint8_t Tag_TelNumVoiceContact[] = { 0xDF ,0x3F };
		const uint8_t Tag_KeyIndexPinBlock[] = { 0xDF, 0x34 };
		const uint8_t Tag_KeyIndexAP[] = { 0xDF, 0x35 };
		const uint8_t Tag_KeyIndexFinancialMsg[] = { 0xDF, 0x31 };
		const uint8_t Tag_DDOL[] = { 0x9F, 0x49 };
		const uint8_t Tag_TDOL[] = { 0x97 };
		const uint8_t Tag_TipPercent[] = { 0xDF, 0x45 };
		const uint8_t Tag_CashBackParams[] = { 0xDF, 0x6D };
		const uint8_t Tag_PreauthAmount[] = { 0xDF ,0x6E };
		const uint8_t Tag_UpdateAcquirerProfileParams[] = { 0xDF, 0x6C };
		const uint8_t Tag_MagPagoBancomatParams[] = { 0xFF, 0x0D };
		const uint8_t Tag_MagInternationCardParams[] = { 0xFF, 0x0A };
		const uint8_t Tag_EMVapplParams[] = { 0xFF, 0x09 };
		const uint8_t Tag_TerminalAction[] = { 0xDF, 0x22 };

		const uint8_t Tag_1stRowTicket[] = { 0xDF, 0x24 };
		const uint8_t Tag_2ndRowTicket[] = { 0xDF, 0x25 };
		const uint8_t Tag_HeaderRowTicket[] = { 0xDF, 0x26 };
		const uint8_t Tag_FooterRowTicket[] = { 0xDF, 0x27 };
		const uint8_t Tag_3rdRowTicket[] = { 0xDF, 0x50 };
		const uint8_t Tag_MessageSuccessTransaction[] = { 0xDF, 0x51 };
		const uint8_t Tag_MessageFailTransaction[] = { 0xDF, 0x52 };
		const uint8_t Tag_4thRowTicket[] = { 0xDF, 0x6B };
		const uint8_t Tag_CourtesyMessage[] = { 0xDF, 0x40 };

		const uint8_t Tag_IDServizio[] = { 0xDF, 0x6A };
		const uint8_t Tag_NomeServizio[] = { 0xDF, 0x49 };
		const uint8_t Tag_ApplicationIDentifier[] = { 0x9F, 0x06 };
		const uint8_t Tag_BitmapChip[] = { 0xDF, 0x33 };
		const uint8_t Tag_ApplicationVersionNumber[] = { 0x9F, 0x09 };
		const uint8_t Tag_FloorLimit[] = { 0x9F, 0x1B };
		const uint8_t Tag_ParamSelezioneRandom[] = { 0xDF, 0x41 };
		const uint8_t Tag_TerminalActionCode[] = { 0xDF, 0x42 };
		const uint8_t Tag_ActionOnResult[] = { 0xDF, 0x47 };
		const uint8_t Tag_ApplicationLabel[] = { 0x50 };
		const uint8_t Tag_ApplicationPreferredName[] = { 0x9F, 0x12 };
		const uint8_t Tag_TransactionAmount[] = { 0x9F, 0x02 };
		const uint8_t Tag_TransactionDate[] = { 0x9A };
		const uint8_t Tag_TransactionTime[] = { 0x9F, 0x21 };
		const uint8_t Tag_TransactionType[] = { 0x9C };
		const uint8_t Tag_TransactionSequenceNumber[] = { 0x9F, 0x41 };
		const uint8_t Tag_ApplicationCryptogram[] = { 0x9F, 0x26 };
		const uint8_t Tag_AID[] = { 0x4F };
		const uint8_t Tag_ApplicationInterchangeProfile[] = { 0x82 };
		const uint8_t Tag_ApplicationTransactionCounter[] = { 0x9F, 0x36 };
		const uint8_t Tag_ApplicationUsageControl[] = { 0x9F, 0x07 };
		const uint8_t Tag_CryptogramInformationData[] = { 0x9F, 0x27 };
		const uint8_t Tag_IssuerApplicationData[] = { 0x9F, 0x10 };
		const uint8_t Tag_TerminalVerificationResults[] = { 0x95 };
		const uint8_t Tag_UnpredictableNumber[] = { 0x9F, 0x37 };
		const uint8_t Tag_ApplicationExpirationDate[] = { 0x5F, 0x24 };
		const uint8_t Tag_ApplicationEffectiveDate[] = { 0x5F, 0x25 };
		const uint8_t Tag_CardSequenceNumber[] = { 0x5F, 0x34 };
		const uint8_t Tag_Track2EquivalentData[] = { 0x57 };
		const uint8_t Tag_IssuerAuthenticationData[] = { 0x91 };
		const uint8_t Tag_IssuerScriptTemplate1[] = { 0x71 };
		const uint8_t Tag_IssuerScriptTemplate2[] = { 0x72 };
		const uint8_t Tag_AuthorisationResponseCode[] = { 0x8A };
		const uint8_t Tag_IssuerScriptResults[] = { 0x9F, 0x81, 0x08 };
		const uint8_t Tag_CardholderVerificationMethod[] = { 0x9F, 0x34 };
		const uint8_t Tag_MessageToPOS[] = { 0xDF, 0x46 };
		const uint8_t Tag_CountryCode[] = { 0x5F, 0x28 };
		const uint8_t Tag_AdditionalTerminalCapabilities[] = { 0x9F, 0x40 };
		const uint8_t Tag_TerminalCapabilities[] = { 0x9F, 0x33 };
		const uint8_t Tag_FloorLimit1[] = { 0xDF, 0x3B };
		const uint8_t Tag_RandomSelection[] = { 0xDF, 0x48 };
		const uint8_t Tag_BINTable[] = { 0xDF, 0x39 };
		const uint8_t Tag_BitmapMagnetic[] = { 0xDF, 0x57 };
		const uint8_t Tag_BitmapPagoBANCOMAT[] = { 0xDF, 0x3A };
		const uint8_t Tag_TransactionCertificate[] = { 0x9F, 0x26 };
		//
#endif

        // POSPRXY

        //UpdateLoginStatus
        const uint8_t TagLoginState[]   = { 0xDF, 0xBF, 0x01 };

        //UpdatePFState
        const uint8_t TagFPState[]      = { 0xDF, 0xBF, 0x02 };

        //UpdateDSPConfiguration
        const uint8_t TagFPConfig[]     = { 0xDF, 0xBF, 0x03 };

        //FuelSaleTrxComplete
        const uint8_t TagDateTime[]     = { 0xDF, 0xBF, 0x09 };
        const uint8_t TagDeviceID[]     = { 0xDF, 0xBF, 0x0A };
        const uint8_t TagPumpNo[]       = { 0xDF, 0xBF, 0x0B };
        const uint8_t TagNozzleNo[]     = { 0xDF, 0xBF, 0x0C };
        const uint8_t TagTransSeqNo[]   = { 0xDF, 0xBF, 0x0D };
        const uint8_t TagReleaseToken[] = { 0xDF, 0xBF, 0x0E };
        const uint8_t TagAmount[]       = { 0xDF, 0xBF, 0x0F };
        const uint8_t TagVolume[]       = { 0xDF, 0xBF, 0x10 };
        const uint8_t TagUnitPrice[]    = { 0xDF, 0xBF, 0x11 };
        const uint8_t TagVolumeProduct1[] = { 0xDF, 0xBF, 0x12 };
        const uint8_t TagVolumeProduct2[] = { 0xDF, 0xBF, 0x13 };
        const uint8_t TagProductNo1[]     = { 0xDF, 0xBF, 0x14 };
        const uint8_t TagProductNo2[]     = { 0xDF, 0xBF, 0x15 };
        const uint8_t TagBlendRatio[]     = { 0xDF, 0xBF, 0x16 };
        const uint8_t TagMIDLinesNarrow[] = { 0xDF, 0xBF, 0x17 };
        const uint8_t TagMIDLinesWide[]   = { 0xDF, 0xBF, 0x18 };


        //general
        const uint8_t TagErrorCode[]    = { 0xDF, 0xBF, 0x19 };
        const uint8_t TagDeviceState[]  = { 0xDF, 0xBF, 0x1A };

        //StartOPT
        //no tags defined

        //StopOPT
        const uint8_t TagEmmergencyStop[]  = { 0xDF, 0xBF, 0x1B };

        //AuthoriseFuelPoint
        const uint8_t TagMaxTrxAmount[]  = { 0xDF, 0xBF, 0x1C };
        const uint8_t TagMaxTrxVolume[]  = { 0xDF, 0xBF, 0x1D };
        const uint8_t TagReceiptRequest[]  = { 0xDF, 0xBF, 0x1E };
        //gap, check values
        const uint8_t TagPaymentTypeCard[]  = { 0xDF, 0xBF, 0x20 }; //this should be a template
        const uint8_t TagPaymentCardType[]  = { 0xDF, 0xBF, 0x21 };
        const uint8_t TagCardPan[]  = { 0xDF, 0xBF, 0x22 };
        const uint8_t TagCardAmount[]  = { 0xDF, 0xBF, 0x23 };
        //gap, check values
        const uint8_t TagPaymentTypeCash[]  = { 0xDF, 0xBF, 0x30 }; //this should be a template
        const uint8_t TagCashDenomination[]  = { 0xDF, 0xBF, 0x31 };
        const uint8_t TagCashNumber[]  = { 0xDF, 0xBF, 0x32 };
        const uint8_t TagCashAmount[]  = { 0xDF, 0xBF, 0x33};
        //gap, check values
        const uint8_t TagPaymentTypeCoupon[]  = { 0xDF, 0xBF, 0x40 };//this should be a template
        const uint8_t TagCouponType[]  = { 0xDF, 0xBF, 0x41 };
        const uint8_t TagCouponCode[]  = { 0xDF, 0xBF, 0x42 };
        const uint8_t TagCouponAmount[]  = { 0xDF, 0xBF, 0x43 };
        //gap, check values
        const uint8_t TagPaymentTypeMagicNumber[]  = { 0xDF, 0xBF, 0x50 };//this should be a template
        const uint8_t TagMagicNumberCode[]  = { 0xDF, 0xBF, 0x51 };
        const uint8_t TagMagicNumberAmount[]  = { 0xDF, 0xBF, 0x52 };
        //gap, check values
        const uint8_t TagLockFuelSale[]  = { 0xDF, 0xBF, 0x56 };

	}

	/**
	 * @}
	 */

	/***************************************************************************
	 * Module namspace: end
	 **************************************************************************/
}
#define TLVLITE_TAG(tag) const ConstData_s tag = CONST_DATA_ARRAY_INIT(internal::tag);
#define TLVLITE_TAG_RENAME(tag, tagInternal) const ConstData_s tag = CONST_DATA_ARRAY_INIT(internal::tagInternal);

namespace guiapp_tags
{
	using com_verifone_TLVLite::ConstData_s;
	using com_verifone_TLVLite::ConstData_t;
	TLVLITE_TAG(TagCode);
	TLVLITE_TAG(TagPosTimeout);
	TLVLITE_TAG(TagDisplayText);
	TLVLITE_TAG(TagDisplayXPos);
	TLVLITE_TAG(TagDisplayYPos);
	TLVLITE_TAG(TagBacklightFlag);
	TLVLITE_TAG(TagKbdBacklightFlag);
	TLVLITE_TAG(TagDisplayIconFrom);
	TLVLITE_TAG(TagDisplayContrast);
	TLVLITE_TAG(TagDisplayIconTo);
	TLVLITE_TAG(TagPromptFile);
	TLVLITE_TAG(TagPromptSection);
	TLVLITE_TAG(TagPromptIndex);
	TLVLITE_TAG(TagPromptIndex2);
	TLVLITE_TAG(TagPromptIndex3);
	TLVLITE_TAG(TagPromptIndex4);
	TLVLITE_TAG(TagPromptAnimation);
	TLVLITE_TAG(TagTimeoutMS);
	TLVLITE_TAG(TagKeyValue);
	TLVLITE_TAG(TagTextLine1);
	TLVLITE_TAG(TagTextLine2);
	TLVLITE_TAG(TagTextLine3);
	TLVLITE_TAG(TagPINClearText);
	TLVLITE_TAG(TagPINParams);
	TLVLITE_TAG(TagEntryParams);
	TLVLITE_TAG(TagDataInput);
	TLVLITE_TAG(TagSelectionItem);
	TLVLITE_TAG(TagSelectKeyText);
	TLVLITE_TAG(TagSelectedItem);
	TLVLITE_TAG(TagIconId);
	TLVLITE_TAG(TagIconLevel);
	TLVLITE_TAG(TagIconStatus);
	TLVLITE_TAG(TagGraphicFilename);
	TLVLITE_TAG(TagGraphicAlignment);
	TLVLITE_TAG(TagSelListMode);
	TLVLITE_TAG(TagBeeperInterval);
	TLVLITE_TAG(TagLineCount);
	TLVLITE_TAG(TagBlockResponse);
	TLVLITE_TAG(TagPrevListEnabled);
	TLVLITE_TAG(TagNextListEnabled);
	TLVLITE_TAG(TagLineLength);
	TLVLITE_TAG(TagReportItem);
	TLVLITE_TAG(TagMatchKeys);
	TLVLITE_TAG(TagFontFilename);
	TLVLITE_TAG(TagFontEncoding);
	TLVLITE_TAG(TagFontStyle);
	TLVLITE_TAG(TagFontSize);
	TLVLITE_TAG(TagFontFlags);
	TLVLITE_TAG(TagDefaultLang);
	TLVLITE_TAG(TagNumberOfLanguages);
	TLVLITE_TAG(TagURL);
	TLVLITE_TAG(TagStatus);
	TLVLITE_TAG(TagResponseEvtBitmap);
	TLVLITE_TAG(TagInputTerminator);
	TLVLITE_TAG(TagTaskId);
	TLVLITE_TAG(TagTaskGraphXPos);
	TLVLITE_TAG(TagTaskGraphYPos);
	TLVLITE_TAG(TagGraphForegroundColor);
	TLVLITE_TAG(TagGraphBackgroundColor);
	TLVLITE_TAG(TagMonitorKeys);
	TLVLITE_TAG(TagBoxBackgroundColor);
	TLVLITE_TAG(TagBoxBorderColor);
	TLVLITE_TAG(TagBoxForegroundColor);
	TLVLITE_TAG(TagBoxBorderSize);
	TLVLITE_TAG(TagBannerShow);
	TLVLITE_TAG(TagBannerFilename);
	TLVLITE_TAG(TagBannerReservedY);
	TLVLITE_TAG(TagBannerShowLeds);
	TLVLITE_TAG(TagBannerLedMaskOn);
	TLVLITE_TAG(TagBannerLedMaskOff);
	TLVLITE_TAG(TagIconBar);
	TLVLITE_TAG(TagReportPromptIndex);
	TLVLITE_TAG(TagImagesNum);
	TLVLITE_TAG(TagImagesRefreshTime);
	TLVLITE_TAG(TagLedsReservedY);
	TLVLITE_TAG(TagPINFlags);
	TLVLITE_TAG(TagwXMLName);
	TLVLITE_TAG(TagwXMLStrVal);
	TLVLITE_TAG(TagwXMLPromptVal);
	TLVLITE_TAG(TagwXMLGroup);
	TLVLITE_TAG(TagHTMLUrl);
	TLVLITE_TAG(TagHTMLKey);
	TLVLITE_TAG(TagHTMLValue);
	TLVLITE_TAG(TagHTMLRawValue);
	TLVLITE_TAG(TagHTMLResult);
	TLVLITE_TAG(TagHTMLDisplayConfirmation);
	TLVLITE_TAG(TagHTMLRegion);
}

namespace prnapp_tags
{
	using com_verifone_TLVLite::ConstData_s;
	using com_verifone_TLVLite::ConstData_t;
	TLVLITE_TAG(TagCode);
	TLVLITE_TAG(TagPosTimeout);
	TLVLITE_TAG(TagTaskId);
	TLVLITE_TAG(TagBeepOnLength);
	TLVLITE_TAG(TagBeepOffLength);
	TLVLITE_TAG(TagBeepCount);
	TLVLITE_TAG(TagPrintMessage);
	TLVLITE_TAG(TagPOSProtocol);
	TLVLITE_TAG(TagWaitForPrinting);
	TLVLITE_TAG(TagWaitForTail);
	TLVLITE_TAG(TagBarCode);
	TLVLITE_TAG(TagBarcodeHeight);
	TLVLITE_TAG(TagBarcodeWidth);
	TLVLITE_TAG(TagBarCodeEncoding);
	TLVLITE_TAG(TagBarCodeMargin);
	TLVLITE_TAG(TagBarCodeScale);
	TLVLITE_TAG(TagTemplateFilename);
	TLVLITE_TAG(TagTemplateNames);
	TLVLITE_TAG(TemplateAdditionalTags);
	TLVLITE_TAG(TagOutputFilename);
	TLVLITE_TAG(TagGraphicFilename);
	TLVLITE_TAG(TagGraphicAlignment);
	TLVLITE_TAG(TagTemplateErrorRow);
	TLVLITE_TAG(TagTemplateErrorCol);
	TLVLITE_TAG(TagTemplateErrorDesc);
	TLVLITE_TAG(TagStatus);
}

namespace mapp_tags
{
	using com_verifone_TLVLite::ConstData_s;
	using com_verifone_TLVLite::ConstData_t;
	TLVLITE_TAG(TagCode);
	TLVLITE_TAG(TemplateData);
	TLVLITE_TAG(TemplateRequestedElements);
	TLVLITE_TAG(TemplateDecisionRequired);
	TLVLITE_TAG(TemplateTC);
	TLVLITE_TAG(TemplateARQC);
	TLVLITE_TAG(TemplateAAC);
	TLVLITE_TAG(TemplateStatus);
	TLVLITE_TAG(TemplateContactlessMSD);
	TLVLITE_TAG(TemplateConfigVersion);
	TLVLITE_TAG(TemplateOSVersion);
	TLVLITE_TAG(TemplateModuleVersion);
	TLVLITE_TAG(TagConfigFileName);
	TLVLITE_TAG(TagConfigFileVersion);
	TLVLITE_TAG(TagConfigFileChecksum);
	TLVLITE_TAG(TagLibraryFileName);
	TLVLITE_TAG(TagLibraryFileVersion);
	TLVLITE_TAG(TagDeviceArchitecture);
	TLVLITE_TAG(TagDeviceArchitectureVersion);
	TLVLITE_TAG(TagTamper);
	TLVLITE_TAG(TagSwitchTripped);
	TLVLITE_TAG(TagPlatformType);
	TLVLITE_TAG(TagTranStatus);
	TLVLITE_TAG(TagTranStatusDesc);
	TLVLITE_TAG(TagStatusPINTryCounter);
	TLVLITE_TAG(TagTerminalID);
	TLVLITE_TAG(TagTerminalSerialNum);

	TLVLITE_TAG(TagNumericLanguageIndices);
	TLVLITE_TAG(TagNumberFormat);
	TLVLITE_TAG(TagNumericEdit);
	TLVLITE_TAG(TagNumericLanguageText);
	TLVLITE_TAG(TagAlphanumericEdit);
	TLVLITE_TAG(TagEntryMaxLength);
	TLVLITE_TAG(TagEntryMinLength);
	TLVLITE_TAG(TagPrefixSymbol);
	TLVLITE_TAG(TagSuffixSymbol);
	TLVLITE_TAG(TagUnmaskedCharactersCount);
	TLVLITE_TAG(TagAlphaModes);
	TLVLITE_TAG(TagAlphaInitMode);
	TLVLITE_TAG(TagBacklightMode);
	TLVLITE_TAG(TagKbdBacklightMode);
	TLVLITE_TAG(TagFontFilename);
	TLVLITE_TAG(TagFontSize);
	//TLVLITE_TAG(TagDisplayContrastMapp);
	TLVLITE_TAG_RENAME(TagDisplayContrast, TagDisplayContrastMapp);
	TLVLITE_TAG(TagDisplayBitmapFileName);
	TLVLITE_TAG(TagDisplayBitmapCol);
	TLVLITE_TAG(TagDisplayBitmapRow);
	TLVLITE_TAG(TagCardStatus);
	TLVLITE_TAG(TagATR);
	TLVLITE_TAG(TagTrack1);
	TLVLITE_TAG(TagTrack2);
	TLVLITE_TAG(TagTrack3);
	TLVLITE_TAG(TagSwipeStatus);
	TLVLITE_TAG(TemplateFile);
	TLVLITE_TAG(TagFileSize);
	TLVLITE_TAG(TagFileSize1);
	TLVLITE_TAG(TagFileDescriptor);
	TLVLITE_TAG(TagFileIdentifier);
	TLVLITE_TAG(TagFileName);
	TLVLITE_TAG(TagFileEFId);
	TLVLITE_TAG(TagFileChecksum);
	TLVLITE_TAG(TagFileSecured);
	TLVLITE_TAG(TagFreeSpace);
	TLVLITE_TAG(TagDrive);
	TLVLITE_TAG(TagFileOffset);
	TLVLITE_TAG(TagFileReadSize);
	TLVLITE_TAG(TagKeyPress);
	TLVLITE_TAG(TagLanguage);
	TLVLITE_TAG(TagLanguageSelect);
	TLVLITE_TAG(TagBarcode);
	TLVLITE_TAG(TagBarcodeHeight);
	TLVLITE_TAG(TagBarcodeWidth);
	TLVLITE_TAG(TagBarcodeEncoding);
	TLVLITE_TAG(TagBarcodeMargin);
	TLVLITE_TAG(TagBarcodeScale);
	TLVLITE_TAG(TagPrintTemplateFilename);
	TLVLITE_TAG(TagPrintTemplateNames);
	TLVLITE_TAG(TemplatePrintAdditionalTags);
	TLVLITE_TAG(TagTemplateRow);
	TLVLITE_TAG(TagTemplateCol);
	TLVLITE_TAG(TagTemplateDesc);
	TLVLITE_TAG(TagBitmapFilename);
	TLVLITE_TAG(TagBitmapAlignment);
	TLVLITE_TAG(TagOptionID);
	TLVLITE_TAG(TagOptionText);
	TLVLITE_TAG(TagMenuTitle);
	TLVLITE_TAG(TagSelListMode);
	TLVLITE_TAG(TagSerialNumber);
	TLVLITE_TAG(TagMaxChainedSize);
	TLVLITE_TAG(TagSafeModeIndicator);
	TLVLITE_TAG(TagKeyType);
	TLVLITE_TAG(TagKeyData);
	TLVLITE_TAG(TagResultCode);
	TLVLITE_TAG(TagPinEntryType);
	TLVLITE_TAG(TagPosSecurityTimeout);
	TLVLITE_TAG(TagHostId);
	TLVLITE_TAG(TagPinBlockFormat);
	TLVLITE_TAG(TagKeyManagement);
	TLVLITE_TAG(TagPinAlgorithm);
	TLVLITE_TAG(TagDataAlgorithm);
	TLVLITE_TAG(TagMessageForMAC);
	TLVLITE_TAG(TagMessForEnc);
	TLVLITE_TAG(TagOutputData);
	TLVLITE_TAG(TagMaxPINLength);
	TLVLITE_TAG(TagMinPINLength);
	TLVLITE_TAG(TagOnlinePINCipher);
	TLVLITE_TAG(TagPan);
	TLVLITE_TAG(TagStan);
	TLVLITE_TAG(TagPinCancel);
	TLVLITE_TAG(TagPinEntryTimeout);
	TLVLITE_TAG(TagPinEntryFirstCharTimeout);
	TLVLITE_TAG(TagPinEntryInterCharTimeout);
	TLVLITE_TAG(TagPinTryFlag);
	TLVLITE_TAG(TagKSN);
	TLVLITE_TAG(TagScriptName);
	TLVLITE_TAG(TagAddScreenText);
	TLVLITE_TAG(TagFlags);
	TLVLITE_TAG(TagLEDOperation);
	TLVLITE_TAG(TagLEDDuration);
	TLVLITE_TAG(TagLEDRepeats);
	TLVLITE_TAG(TagBatteryStatus);
	TLVLITE_TAG(TagMifareOperation);
	TLVLITE_TAG(TagMifareOperationID);

	TLVLITE_TAG(TagContinueFlag);
	TLVLITE_TAG(TagCashbackFlag);
	TLVLITE_TAG(TagAcquirerID);
	//TLVLITE_TAG(TagForceOnline);
	TLVLITE_TAG(TagPinResult);
	TLVLITE_TAG(TagCardholderVerificationCompleted);
	TLVLITE_TAG(TagPinEntryStyle);
	TLVLITE_TAG(TagRadixSeparator);
	TLVLITE_TAG(TagPinpadSelectsApplication);
	TLVLITE_TAG(TagScripts71Results);
	TLVLITE_TAG(TagScripts72Results);
	TLVLITE_TAG(TagTACDefault);
	TLVLITE_TAG(TagTACDenial);
	TLVLITE_TAG(TagTACOnline);
	TLVLITE_TAG(TagForceOnline);
	TLVLITE_TAG(TagROSThreshold);
	TLVLITE_TAG(TagROSTargetRSPercent);
	TLVLITE_TAG(TagROSMaxTargetRSPercent);
	TLVLITE_TAG(TagDioneCashbackCheck);
	TLVLITE_TAG(TagReasonOnline);
	TLVLITE_TAG(TagSuppressDisplay);
	TLVLITE_TAG(TagPinEntryCurrencySymbol);
	TLVLITE_TAG(TagPinEntryCurrencyLoc);
	TLVLITE_TAG(TagPinEntryBeeper);
	TLVLITE_TAG(TagSimplifiedRefunds);
	TLVLITE_TAG(TagICCPublicKey);
	TLVLITE_TAG(TagICCPublicKeyLegacy);
	TLVLITE_TAG(TagShowIncorrectPIN);

	TLVLITE_TAG(TagItemID);
	TLVLITE_TAG(TagItemText);
	TLVLITE_TAG(TagItemInt);

	TLVLITE_TAG(TagMachineType);
	TLVLITE_TAG(TagCardReaderType);
	TLVLITE_TAG(TagAlternateInterval);
	TLVLITE_TAG(TagOverrideLabel);
	TLVLITE_TAG(TagHideIcons);

	TLVLITE_TAG(TagVSPStatus);
	TLVLITE_TAG(TagVSPFullStatus);
	TLVLITE_TAG(TagVSPVersion);
	TLVLITE_TAG(TagVSPExtStatus);
	TLVLITE_TAG(TagVSPDiags);
	TLVLITE_TAG(TagTGKStatus);
	TLVLITE_TAG(TagTGKFallbackStatus);

	TLVLITE_TAG(TagVSPEncryptionStatus);
	TLVLITE_TAG(TagVSPEParms);

	TLVLITE_TAG(TagExtResultCode);
	TLVLITE_TAG(TagBuzzerCtrl);

	TLVLITE_TAG(TemplateSRED);
	TLVLITE_TAG(TemplateSREDPAN);

	TLVLITE_TAG(TagTrack2Data);
	TLVLITE_TAG(TagManualPAN);
	TLVLITE_TAG(TagManualCVV2);
	TLVLITE_TAG(TagManualExpiryDate);
	TLVLITE_TAG(TagManualStartDate);
	TLVLITE_TAG(TagSREDMagstripeTrack1);
	TLVLITE_TAG(TagSREDMagstripeTrack2);
	TLVLITE_TAG(TagSREDMagstripeTrack3);
	TLVLITE_TAG(TagSREDWhitelistStatus);
	TLVLITE_TAG(TagSREDWhitelistHash);

	TLVLITE_TAG(TagSREDMaskingScheme);
	TLVLITE_TAG(TagSREDKeySlot);
	TLVLITE_TAG(TagSREDStatusIndicator);

	TLVLITE_TAG(TagSREDEncryptionStatus);
	TLVLITE_TAG(TagSREDEncryptedData);
	TLVLITE_TAG(TagSREDKSN);
	TLVLITE_TAG(TagSREDIV);
	TLVLITE_TAG(TagSREDEncryptedPAN);
	TLVLITE_TAG(TagSREDKSNPAN);
	TLVLITE_TAG(TagSREDIVPAN);
	TLVLITE_TAG(TagSREDEncryptionStatusPAN);

	TLVLITE_TAG(TagCardCheckIndicator);

	TLVLITE_TAG_RENAME(TagARSPassword1, TagKeyData);
	TLVLITE_TAG(TagARSPassword2);


	TLVLITE_TAG(TagBluetoothName);

	TLVLITE_TAG(TagOSCounterIndex);
	TLVLITE_TAG(TagOSCounterValue);
	TLVLITE_TAG(TagOSCounterName);
	TLVLITE_TAG(TagOSDevice);
	TLVLITE_TAG(TagOSPort);

	// TLVLITE_TAG(TagOSCounterGroup);
	TLVLITE_TAG(TemplateAdditionalTags);


	TLVLITE_TAG(TagHTMLUrl);
	TLVLITE_TAG(TagHTMLKey);
	TLVLITE_TAG(TagHTMLValue);
	TLVLITE_TAG(TagHTMLResult);

	TLVLITE_TAG(TagHTMLEcho);
	TLVLITE_TAG(TagHTMLEchoKey);
	TLVLITE_TAG(TagHTMLEchoValue);
	TLVLITE_TAG(TagHTMLRegion);
}

namespace cardapp_tags
{
	using com_verifone_TLVLite::ConstData_s;
	using com_verifone_TLVLite::ConstData_t;
	TLVLITE_TAG(TagPosTimeout);
	TLVLITE_TAG(TagProcFlag);
	TLVLITE_TAG_RENAME(TagATR, TagATRCardapp);
	TLVLITE_TAG(TagInTLV);
	TLVLITE_TAG(TagInAPDU);
	TLVLITE_TAG(TagOutAPDU);
	TLVLITE_TAG(TagOutTLVlist);
	TLVLITE_TAG(TagOperations);
	TLVLITE_TAG(TagCode);
	TLVLITE_TAG(TagOutTLV);
	TLVLITE_TAG(TagProcStatus);
	TLVLITE_TAG(TagAuthResult);
	TLVLITE_TAG(TagPINtimeout);
	TLVLITE_TAG(TagPINfirstcharTimeout);
	TLVLITE_TAG(TagPINintercharTimeout);
	TLVLITE_TAG(TagEvent);
	TLVLITE_TAG(TagActionCode);
	TLVLITE_TAG_RENAME(TagTrack1, TagCardappTrack1);
	TLVLITE_TAG_RENAME(TagTrack2, TagCardappTrack2);
	TLVLITE_TAG_RENAME(TagTrack3, TagCardappTrack3);
	TLVLITE_TAG(TagCardType);
	TLVLITE_TAG(TagCardBlacklisted);
	TLVLITE_TAG(TagOfflinePINEntryType);
	TLVLITE_TAG(TagPINEntryAtomic);
	TLVLITE_TAG(TagCardStatusWord);
	TLVLITE_TAG(TagPINEntryStatus);
	TLVLITE_TAG_RENAME(TagLEDDuration, TagLEDDuration_CARDAPP);
	TLVLITE_TAG_RENAME(TagLEDRepeats, TagLEDRepeats_CARDAPP);
	TLVLITE_TAG(TagMemoryCardAddress);
	TLVLITE_TAG(TagMemoryCardByteCount);
	TLVLITE_TAG(TagMemoryCardData);
	TLVLITE_TAG(TagMemoryCardPIN);
	TLVLITE_TAG_RENAME(TagShowIncorrectPIN, TagShowIncorrectPINCardapp);
	TLVLITE_TAG(TagEMVKernelChecksum);
	TLVLITE_TAG(TagEMVKernelVersion);
	TLVLITE_TAG(TagPacketType);
	TLVLITE_TAG(TagActiveInterfaces);
	TLVLITE_TAG(TagPartialSelection);
	TLVLITE_TAG(TagRecordApplicationName);
	TLVLITE_TAG(TagSecondTermAppVer);
	TLVLITE_TAG_RENAME(TagROSThresholdSA, TagROSThresholdCardapp);
	TLVLITE_TAG_RENAME(TagROSTargetRSPercentSA, TagROSTargetRSPercentCardapp);
	TLVLITE_TAG_RENAME(TagROSMaxTargetRSPercentSA, TagROSMaxTargetRSPercentCardapp);
	TLVLITE_TAG_RENAME(TagTACDefaultSA, TagTACDefaultCardapp);
	TLVLITE_TAG_RENAME(TagTACDenialSA, TagTACDenialCardapp);
	TLVLITE_TAG_RENAME(TagTACOnlineSA, TagTACOnlineCardapp);
	TLVLITE_TAG(TagROSThreshold);
	TLVLITE_TAG(TagROSTargetRSPercent);
	TLVLITE_TAG(TagROSMaxTargetRSPercent);
	TLVLITE_TAG(TagTACDefault);
	TLVLITE_TAG(TagTACDenial);
	TLVLITE_TAG(TagTACOnline);

	TLVLITE_TAG(TagDefaultTDOL);
	TLVLITE_TAG(TagDefaultDDOL);
	TLVLITE_TAG(TagAutoAppSelection);
	TLVLITE_TAG(TagMerchantCategoryCode);
	TLVLITE_TAG(TagCAPKModulus);
	TLVLITE_TAG(TagCAPKExponent);
	TLVLITE_TAG(TagCAPKChecksum);
	TLVLITE_TAG(TagCAPKExpiry);
	TLVLITE_TAG(TagCAPKFile);
	TLVLITE_TAG(TagFallback);
	TLVLITE_TAG_RENAME(TagFlags, TagFlagsScapp);
	TLVLITE_TAG(TagPinEntryBeeper);
}

namespace authapp_tags
{
	using com_verifone_TLVLite::ConstData_s;
	using com_verifone_TLVLite::ConstData_t;
	TLVLITE_TAG(TagCB2_CAPK);
	TLVLITE_TAG(TagCB2_CAindex);
	TLVLITE_TAG(TagCB2_NRG);
	TLVLITE_TAG(TagCB2_OperCert);
	TLVLITE_TAG(TagCB2_OperExp);
	TLVLITE_TAG(TagCB2_SupplCert);
	TLVLITE_TAG(TagCB2_TermCert);
	TLVLITE_TAG(TagCB2_NRT);
	TLVLITE_TAG(TagCB2_NRG_ST);
	TLVLITE_TAG(TagCB2_NRT_SG);
	TLVLITE_TAG(TagCB2_SupplNdx);
	TLVLITE_TAG(TagCB2_File_CA);
	TLVLITE_TAG(TagCB2_File_CT);
	TLVLITE_TAG(TagCB2_File_ST);
	TLVLITE_TAG(TagCB2_File_MAN);
	TLVLITE_TAG(TagCB2_File_RND);
	TLVLITE_TAG(TagCB2_KSER_PT);
	TLVLITE_TAG(TagCB2_KIP_PT);
	TLVLITE_TAG(TagCB2_KCV_SG);
	TLVLITE_TAG(TagCB2_KCV_KIP_SG);
	TLVLITE_TAG(TagCB2_OperRem);
	TLVLITE_TAG(TagCB2_GTID);
	TLVLITE_TAG(TagCB2_CA_CG);
	TLVLITE_TAG(TagCB2_PF_exp);
	TLVLITE_TAG(TagCB2_PF_rem);
	TLVLITE_TAG(TagCB2_PT_exp);
	TLVLITE_TAG(TagCB2_PT_rem);
	TLVLITE_TAG(TagCB2_CA_ndx);
	TLVLITE_TAG(TagCB2_CF_ndx);
	TLVLITE_TAG(TagCB2_AdminOper);
	TLVLITE_TAG(TagCB2_VerApp);
	TLVLITE_TAG(TagCB2_VerVSS);
	TLVLITE_TAG(TagCB2_HomeDir);
	TLVLITE_TAG(TagCB2_KeysStatus);
	TLVLITE_TAG(TagCB2_EncData);
	TLVLITE_TAG(TagCB2_DecData);
	TLVLITE_TAG(TagCB2_OnlineTrID);
	TLVLITE_TAG(TagCB2_KeyIndex);
	TLVLITE_TAG(TagCB2_KSN);
	TLVLITE_TAG(TagCB2_TerminalID);
	TLVLITE_TAG(TagCB2_KeyType);
	TLVLITE_TAG(TagCB2_PAN);
	TLVLITE_TAG(TagCB2_PINblock);
	TLVLITE_TAG(TagCB2_KeyIndexAP);
	TLVLITE_TAG(TagCode);
}

namespace auth_SRED_tags
{
// SRED authentication
	TLVLITE_TAG(TagAuthCertLevel);
	TLVLITE_TAG(TagAuthCertData);
	TLVLITE_TAG(TagAuthCertFile);
	TLVLITE_TAG(TagAuthRandomClrRemote);
	TLVLITE_TAG(TagAuthRandomEncr);
	TLVLITE_TAG(TagAuthKeyStream);
	TLVLITE_TAG(TagAuthDataEncr);
	TLVLITE_TAG(TagAuthDataClr);
	TLVLITE_TAG(TagAuthFlags);
	TLVLITE_TAG(TagAuthSign);
	TLVLITE_TAG(TagAuthDID);
	TLVLITE_TAG(TagAuthCertReq);
	TLVLITE_TAG(TagAuthRSAsize);
	TLVLITE_TAG(TagAuthRandomClrOwn);
}

namespace scapp_tags
{
	using com_verifone_TLVLite::ConstData_s;
	using com_verifone_TLVLite::ConstData_t;
	TLVLITE_TAG(TagActionCode);
	TLVLITE_TAG_RENAME(TagAddScreenText, TagAddScreenText1);
	TLVLITE_TAG(TagAddScreenText2);
	TLVLITE_TAG(TagScreenLines);
	TLVLITE_TAG(TagPromptFill);
	TLVLITE_TAG(TagCode);
	TLVLITE_TAG(TagDataAlgorithm);
	TLVLITE_TAG_RENAME(TagHostId, TagHostIdScapp);
	TLVLITE_TAG_RENAME(TagKeyData, TagKeyDataScapp);
	TLVLITE_TAG_RENAME(TagKeyManagement, TagKeyManagementScapp);
	TLVLITE_TAG_RENAME(TagKeyType, TagKeyTypeScapp);
	TLVLITE_TAG_RENAME(TagKSN, TagKSNScapp);
	TLVLITE_TAG(TagLock);
	TLVLITE_TAG(TagMAC);
	TLVLITE_TAG_RENAME(TagMaxPINLength, TagMaxPINLengthScapp);
	TLVLITE_TAG(TagMessForDec);
	TLVLITE_TAG_RENAME(TagMessForEnc, TagMessForEncScapp);
	TLVLITE_TAG_RENAME(TagMessageForMAC, TagMessageForMACScapp);
	TLVLITE_TAG_RENAME(TagMinPINLength, TagMinPINLengthScapp);
	TLVLITE_TAG_RENAME(TagOnlinePINCipher, TagOnlinePINCipherScapp);
	TLVLITE_TAG_RENAME(TagPan, TagPanScapp);
	TLVLITE_TAG_RENAME(TagPinAlgorithm, TagPinAlgorithmScapp);
	TLVLITE_TAG_RENAME(TagPinBlockFormat, TagPinBlockFormatScapp);
	TLVLITE_TAG_RENAME(TagPinCancel, TagPinCancelScapp);
	TLVLITE_TAG_RENAME(TagPinEntryTimeout, TagPinEntryTimeoutScapp);
	TLVLITE_TAG_RENAME(TagPinEntryType, TagPinEntryTypeScapp);
	TLVLITE_TAG_RENAME(TagPinTryFlag, TagPinTryFlagScapp);
	TLVLITE_TAG(TagPlainTxtPinBlock);
	TLVLITE_TAG(TagPosTimeout);
	TLVLITE_TAG_RENAME(TagScriptName, TagScriptNameScapp);
	TLVLITE_TAG_RENAME(TagStan, TagStanScapp);
	TLVLITE_TAG(TagTransAmount);
	TLVLITE_TAG(TagTransCatExp);
	TLVLITE_TAG(TagTransCurrCode);
	TLVLITE_TAG(TagTransCurrExp);
	TLVLITE_TAG_RENAME(TagFlags, TagFlagsScapp);
	TLVLITE_TAG(TagMessagePIN);
	TLVLITE_TAG(TagMessageAmount);
	TLVLITE_TAG(TagFontFile);
	TLVLITE_TAG(TagMonitorPeriod);
	TLVLITE_TAG(TagVSSname);
	TLVLITE_TAG(TagVSSslot);
	TLVLITE_TAG(TagVSSmacro);
	TLVLITE_TAG(TagVSSIn);
	TLVLITE_TAG(TagVSSOut);
	TLVLITE_TAG(TagVSSResult);
	TLVLITE_TAG(TagInput_EMV);
	TLVLITE_TAG(TagPINTimeout_EMV);
	TLVLITE_TAG(TagPinMaxLen);
	TLVLITE_TAG(TagPinMinLen);
	TLVLITE_TAG(TagPinOnlineEntryType);
	TLVLITE_TAG(TagPinOnlineCancel);
	TLVLITE_TAG(TagPinOnlineExtraMsg);
	TLVLITE_TAG(TagDUKPTMaskPIN);
	TLVLITE_TAG(TagDUKPTMaskMAC);
	TLVLITE_TAG(TagDUKPTMaskENC);

	TLVLITE_TAG(TagPropData);
	TLVLITE_TAG(TagPinEntryBeeper);
}

namespace ebaapp_tags
{
	TLVLITE_TAG(TagCode);
	TLVLITE_TAG(TagDeviceStatus);
	TLVLITE_TAG(TagDeviceModelNo);
	TLVLITE_TAG(TagDeviceSerialNumber);
	TLVLITE_TAG(TagFirmwareVersion);
	TLVLITE_TAG(TagLastDetectionResult);
	TLVLITE_TAG(TagNotificationId);
	TLVLITE_TAG(TagCurrency);
	TLVLITE_TAG(TagDenomination);
	TLVLITE_TAG(TagProcessingError);
	TLVLITE_TAG(TagDeviceRejectionCode);
	TLVLITE_TAG(TagDeviceErrorCode);
	TLVLITE_TAG(TagFirmwareFileLocn);
	TLVLITE_TAG(TagDownloadTimeout);
}

namespace bcsapp_tags
{
	TLVLITE_TAG(TagCode);
	TLVLITE_TAG(TagBarcodeData);
}

namespace cb2if_tags
{
	TLVLITE_TAG(TagifOperation);
	TLVLITE_TAG(TagifTermID);
	TLVLITE_TAG(TagifTransAmount);
	TLVLITE_TAG(TagifConfirmAmount);
	TLVLITE_TAG(TagifPrintTicket);
	TLVLITE_TAG(TagifAddMessageToHost);
	TLVLITE_TAG(TagifPaymantType);
	TLVLITE_TAG(TagifMessageToPrinter);
	TLVLITE_TAG(TagifEcrId);
	TLVLITE_TAG(TagifSTAN);
	TLVLITE_TAG(TagifActionCode);
	TLVLITE_TAG(TagifPAN);
	TLVLITE_TAG(TagifTransType);
	TLVLITE_TAG(TagifTransDataTime);
	TLVLITE_TAG(TagifCardType);
	TLVLITE_TAG(TagifAcquirerID);
	TLVLITE_TAG(TagifOnlineOperation);
	TLVLITE_TAG(TagifApprovalCode);
	TLVLITE_TAG(TagifTransStatusEnable);
	TLVLITE_TAG(TagifGTID);
	TLVLITE_TAG(TagifLineType);
	TLVLITE_TAG(TagifNetType);
	TLVLITE_TAG(TagifHostIP);
	TLVLITE_TAG(TagifHostPort);
	TLVLITE_TAG(TagifDHCPEnable);
	TLVLITE_TAG(TagifIp);
	TLVLITE_TAG(TagifGateway1);
	TLVLITE_TAG(TagifGateway2);
	TLVLITE_TAG(TagifSubnetMask);
	TLVLITE_TAG(TagifDNS1);
	TLVLITE_TAG(TagifDNS2);
	TLVLITE_TAG(TagifTransportType);
	TLVLITE_TAG(TagifScreenEnable);
	TLVLITE_TAG(TagifPreResponseEnable);

	TLVLITE_TAG(TagifTransResult);
	TLVLITE_TAG(TagifHostErrorMessage);
	TLVLITE_TAG(TagifAppErrorMessage);
	TLVLITE_TAG(TagifScreenLine1ID);
	TLVLITE_TAG(TagifScreenLine2ID);
	TLVLITE_TAG(TagifScreenLine3ID);
	TLVLITE_TAG(TagifScreenLine4ID);
	TLVLITE_TAG(TagifScreenLine1Text);
	TLVLITE_TAG(TagifScreenLine2Text);
	TLVLITE_TAG(TagifScreenLine3Text);
	TLVLITE_TAG(TagifScreenLine4Text);

	TLVLITE_TAG(TagifTransStatus);

	TLVLITE_TAG(TagPosTimeout);
	TLVLITE_TAG(TagCode);
}

namespace iso8583_tags
{
	TLVLITE_TAG(TagPrimaryAccountNumber);						//ISO8583 field   2
	TLVLITE_TAG(TagProcessingCode);								//ISO8583 field   3
	TLVLITE_TAG(TagAmountTransaction);							//ISO8583 field   4
	TLVLITE_TAG(TagSystemsTraceAuditNumber);					//ISO8583 field  11
	TLVLITE_TAG(TagDateTimeHost);								//ISO8583 field  12
	TLVLITE_TAG(TagDateEffective);								//ISO8583 field  13
	TLVLITE_TAG(TagDateExpiration);								//ISO8583 field  14
	TLVLITE_TAG(TagPointOfServiceDataCode);						//ISO8583 field  22
	TLVLITE_TAG(TagCardSequenceNumber);							//ISO8583 field  23
	TLVLITE_TAG(TagFunctionCode);								//ISO8583 field  24
	TLVLITE_TAG(TagMessageReasonCode);							//ISO8583 field  25
	TLVLITE_TAG(TagCardAcceptorBusinessCode);					//ISO8583 field  26
	TLVLITE_TAG(TagAcquiringInstitutionsIdentificationCode);	//ISO8583 field  32
	TLVLITE_TAG(TagPrimaryAccountNumberPANExtended);			//ISO8583 field  34
	TLVLITE_TAG(TagTrack2data);									//ISO8583 field  35
	TLVLITE_TAG(TagApprovalCode);								//ISO8583 field  38
	TLVLITE_TAG(TagAC);											//ISO8583 field  39
	TLVLITE_TAG(TagAdditionalResponseData);						//ISO8583 field  44
	TLVLITE_TAG(TagAdditionalDataNational);						//ISO8583 field  47
	TLVLITE_TAG(TagAdditionalDataPrivate);						//ISO8583 field  48
	TLVLITE_TAG(TagCurrencyCode);								//ISO8583 field  49
	TLVLITE_TAG(TagPINData);									//ISO8583 field  52
	TLVLITE_TAG(TagSecurityRelatedControlInformation);			//ISO8583 field  53
	TLVLITE_TAG(TagICCRelatedData);								//ISO8583 field  55
	TLVLITE_TAG(TagOriginalDataElements);						//ISO8583 field  56
	TLVLITE_TAG(TagTransportData);								//ISO8583 field  59
	TLVLITE_TAG(TagMessageAuthenticationCode);					//ISO8583 field  64-128
	TLVLITE_TAG(TagDataRecord);									//ISO8583 field  72
	TLVLITE_TAG(TagAmountNetReconciliation);					//ISO8583 field  97
}


namespace posprxy_tags
{
    TLVLITE_TAG(TagCode);

    TLVLITE_TAG(TagLoginState);
    TLVLITE_TAG(TagFPState);
    TLVLITE_TAG(TagFPConfig);
    TLVLITE_TAG(TagDateTime);
    TLVLITE_TAG(TagDeviceID);
    TLVLITE_TAG(TagPumpNo);
    TLVLITE_TAG(TagNozzleNo);
    TLVLITE_TAG(TagTransSeqNo);
    TLVLITE_TAG(TagReleaseToken);
    TLVLITE_TAG(TagAmount);
    TLVLITE_TAG(TagVolume);
    TLVLITE_TAG(TagUnitPrice);
    TLVLITE_TAG(TagVolumeProduct1);
    TLVLITE_TAG(TagVolumeProduct2);
    TLVLITE_TAG(TagProductNo1);
    TLVLITE_TAG(TagProductNo2);
    TLVLITE_TAG(TagBlendRatio);
    TLVLITE_TAG(TagMIDLinesNarrow);
    TLVLITE_TAG(TagMIDLinesWide);
    TLVLITE_TAG(TagErrorCode);
    TLVLITE_TAG(TagDeviceState);
    TLVLITE_TAG(TagEmmergencyStop);
    TLVLITE_TAG(TagMaxTrxAmount);
    TLVLITE_TAG(TagMaxTrxVolume);
    TLVLITE_TAG(TagReceiptRequest);
    TLVLITE_TAG(TagPaymentTypeCard);
    TLVLITE_TAG(TagPaymentCardType);
    TLVLITE_TAG(TagCardPan);
    TLVLITE_TAG(TagCardAmount);
    TLVLITE_TAG(TagPaymentTypeCash);
    TLVLITE_TAG(TagCashDenomination);
    TLVLITE_TAG(TagCashNumber);
    TLVLITE_TAG(TagCashAmount);
    TLVLITE_TAG(TagPaymentTypeCoupon);
    TLVLITE_TAG(TagCouponType);
    TLVLITE_TAG(TagCouponCode);
    TLVLITE_TAG(TagCouponAmount);
    TLVLITE_TAG(TagPaymentTypeMagicNumber);
    TLVLITE_TAG(TagMagicNumberCode);
    TLVLITE_TAG(TagMagicNumberAmount);
    TLVLITE_TAG(TagLockFuelSale);


}

