///////////////////////////////////////////////////////////
//  cBaseInterface.h
//  Implementation of the Class cBaseInterface
//  Created on:      16-Oct-2007 09:10:56
//  Original author: Nick Tristram
///////////////////////////////////////////////////////////

#if !defined(CTLVPACK__INCLUDED_)
#define CTLVPACK__INCLUDED_

#ifndef __cplusplus
#error "This file is for C++ only!"
#endif
/*****************************************************************************
 *
 * Copyright (C) 2007 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/


/***************************************************************************
 * Includes
 **************************************************************************/
//#include <svc.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>

#include <stdint.h>

#include <liblog/logsys.h>
#include <libipc/ipc.h>

#define _WCHAR_T_DEFINED
#include <libber-tlv/ber-tlv.h>


/**
  * @brief
  *         Base Interface class
  *
  *
  */

/***************************************************************************
 * Preprocessor constant definitions
 **************************************************************************/
#define MAX_PCOM_APP_MSG_LEN     	1024

// return codes - These must not be changed or should be consistent with other apps
#define PCOM_OKAY                    (  0)
#define PCOM_INIT_ERROR              ( -4)
#define PCOM_ERROR               	( -6)

#ifndef CMD_CANCEL_OPERATION
#define CMD_CANCEL_OPERATION        (83)	//	@define CMD_CANCEL_OPERATION	 		| 0x83 | Cancel current operation.
#endif

#define MAX_APP_NAME 30

class cTLVpack
{
private:
	unsigned char aucMessage[MAX_PCOM_APP_MSG_LEN];
	unsigned long ulLength;
	unsigned char ucCommandType;
	int iRxLength;
	BER_TLV *pstTag;
	bool bFirstField;
	bool bConnected;
	char szSrcAppName[MAX_APP_NAME+1];
	char szDstAppName[MAX_APP_NAME+1];
	// for ExtractSingleTag
	BER_TLV *lastFound;

	int AcmSendReceive(
	    char *pszFromAppName,
	    char const *pszToAppName,
	    unsigned char *pucMessage,
	    unsigned long ulLength,
	    int *piRxLength,
	    unsigned long ulTimeout,
	    short (*pfCVMCancellationFunction)(void *),
	    void *pComm
	    );

	int ExtractTagData( u_char *pData, uint16_t iLength, const ALLOW_ENTRY *pstList );
	int ExtractSingleTag( BER_TLV *pstEl, const u_char *pbyTag, u_char byTagLen, u_char *pbyValue, u_long ulnLen );
	BER_TLV *GetFirstTag( BER_TLV *pstEl, const u_char *pbyTag, u_char byTagLen, u_char *pbyValue, u_long ulnLen );
	BER_TLV *GetNextTag( BER_TLV *pstEl, const u_char *pbyTag, u_char byTagLen, u_char *pbyValue, u_long ulnLen );

protected:
	void setTags(void)
	{
	    if( bFirstField )
    	{
        	ulLength = 1;
    	}
    	else
    	{
        	usnTLV_flatten_element( aucMessage+1, &ulLength, sizeof(aucMessage)-1, pstTag, TRUE);
        	ulLength++;
        	vTLV_destroy_element(pstTag);
    	}
	};

	int makeTags(void)
	{
		pstTag = pstTLV_make_element();
	    if (!pstTag)
	        return PCOM_ERROR;

		if( usnTLV_parse_buffer( pstTag, aucMessage+1, iRxLength-1 ) != SUCCESS )
	    {
	        vTLV_destroy_element(pstTag);
	        return PCOM_ERROR;
	    }
		return PCOM_OKAY;
	};

public:
   	cTLVpack()
   	{
    	bFirstField = true;
      memset(szSrcAppName, 0 , sizeof(szSrcAppName));
      memset(szDstAppName, 0 , sizeof(szDstAppName));
      memset(aucMessage, 0, sizeof(aucMessage));
      lastFound = 0;
      pstTag = 0;
      ulLength = 0;
   	};

	~cTLVpack()
	{
		vTLV_destroy_element(pstTag);
	};

	virtual void setConnection (bool connection)
	{
		bConnected = connection;
	};

	void setAppSrcDst(char const *AppSrc, char const *AppDst)
	{
		memset(szSrcAppName, 0, sizeof(szSrcAppName));
		strncpy(szSrcAppName, AppSrc, sizeof(szSrcAppName)-1);
		memset(szDstAppName, 0, sizeof(szDstAppName));
		strncpy(szDstAppName, AppDst, sizeof(szDstAppName)-1);
	};

	virtual int mInitMessage (unsigned char cmd)
	{
		return mInitMessage (cmd, true);
	};

	virtual int mInitMessage (unsigned char cmd, bool connection)
	{
		aucMessage[0] = cmd;
	    ucCommandType = cmd;

	   	if( !connection)
	   	{
		   dlog_msg("app not connected, ignoring request" );
		   return PCOM_INIT_ERROR;
	   	}
		return PCOM_OKAY;
	};

   	virtual int mSendPCOM()
   	{
   	/* NKJT
	    if( pcom_send( aucMessage, ulLength, szSrcAppName, szDstAppName ) != 0 )
	        return PCOM_ERROR;
	*/
		return PCOM_OKAY;
   	};

   	virtual int mSendReceivePCOM(unsigned long timeout, short (*pfCVMCancellationFunction)(void *), void *pComm)
   	{
   		dlog_error(":::::PCOM send receive error SRC=%s DST=%s SLEN=%d RLEN=%d",
   				szSrcAppName,szDstAppName,ulLength,iRxLength);
   		setTags();
		if( AcmSendReceive( szSrcAppName, szDstAppName, aucMessage, ulLength, &iRxLength, timeout, pfCVMCancellationFunction, pComm ) != 0 )
		{
			dlog_error(":::::PCOM send receive error");
			return PCOM_ERROR;
		}

		if( (ucCommandType | 0x80) != aucMessage[0] )
		{
			dlog_msg ("error: message type mismatch %02x!=%02x",ucCommandType, aucMessage[0]);
			dlog_hex(aucMessage, iRxLength, "ALE-BRYNDZA");
			return PCOM_ERROR;
		}
		return makeTags();
   	};

   	virtual int mSendReceivePCOMCancel(unsigned long timeout, short (*pfCVMCancellationFunction)(void *), void *pComm)
   	{
   		dlog_msg(":::::PCOM send receive CANCEL");
   		setTags();
	    if( AcmSendReceive( szSrcAppName, szDstAppName, aucMessage, ulLength, &iRxLength, timeout, pfCVMCancellationFunction, pComm ) != 0 )
        	return PCOM_ERROR;

	    if( (ucCommandType | 0x80) != aucMessage[0] )
	    {
	        dlog_msg( "error: message type mismatch. Sending CANCEL. " );
	        aucMessage[0] = (char)CMD_CANCEL_OPERATION;
	        AcmSendReceive( szSrcAppName, szDstAppName, aucMessage, 1, &iRxLength, timeout, pfCVMCancellationFunction, pComm );
	        return PCOM_ERROR;
	    }
		return makeTags();
   	};

   	virtual int mGetField(uint8_t const * tag, uint8_t tagsize, uint8_t * buffer, u_long bufferSize = 1 )
   	{
    	return ExtractSingleTag( pstTag, tag, tagsize, (u_char *)buffer, bufferSize );
   	};

	virtual void mAddField(uint8_t const * tag, uint8_t tagsize, uint8_t* data, uint8_t datasize)
	{
	    if( &data != 0 )
	    {
	        if( bFirstField )
	        {
	        	pstTag = pstTLV_make_primitive_element( tag, tagsize, (u_char *)data, datasize );
	            bFirstField = false;
	        }
	        else
	        {
				vTLV_set_up_primitive_element( pstTLV_append_element(pstTag), tag, tagsize, (u_char *)data, datasize );
	        }
	    }
	};


};
#endif // !defined(CTLVPACK__INCLUDED_)
