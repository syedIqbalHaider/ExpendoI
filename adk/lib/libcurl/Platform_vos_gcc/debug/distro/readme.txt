This is a bare minimum, 'Hello world' type application for the Mx platform to demonstrate the process of signing and packaging a bundle to be downloaded to a terminal.

It consists of an application called hello.out, that uses a framebuffer library to output some text to the display.

Perform the following steps to get the bundle built and signed:
1. Open the do_package shell script in a text editor and change the terminal's IP address to the right address of your terminal.
2. Put the terminal in the netloader mode, ready to accept a download.
3. Execute the do_package shell script, which ultimtely builds a download bundle that is signed properly and download it to the terminal using mxdownload.
4. Telnet into the terminal.
5. Run ps command to list running processes and look for a process called /home/sys4/sysmode. In the left most column, next to it, is the process id.
6. run kill xxx where xxx is the process id obtained in step 5.
7. Do a su usr1 to change to the user id to usr1.
8. Enter cd /home/usr1
9. Execute the the application by entering ./hello.out.
10. This should result in the 'hello world' text being displayed on the screen.
