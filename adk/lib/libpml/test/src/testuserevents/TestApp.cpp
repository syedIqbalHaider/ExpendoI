/******************************************************************************
      Copyright (C) 2002 by VeriFone Inc. All rights reserved.

 No part of this software may be used, stored, compiled, reproduced,
 modified, transcribed, translated, transmitted, or transferred, in any form 
 or by any means whether electronic, mechanical, magnetic, optical, 
 or otherwise, without the express prior written permission of VeriFone, Inc.
******************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <svc.h>
#include <errno.h>

#include <pthread.h>

#include <liblog/logsys.h>

#include <libipc/ipc.h>
#include <libpml/pml.h>
#include <libpml/pml_abstracted_api.h>

const char *g_apLogicalName = "TESTAPP";

const char *g_apLogicalNameThread_1 = "TESTAPP/1";

const char *g_apLogicalNameThread_1_1 = "TESTAPP/1/1";



static int thread1Entry(int param);
static int thread2Entry(int param);


/*
* Function name		:	main
* Inputs			:   int argc, char *argv[]		
* Outputs			:	Return value - Success, Failure		
* Description		:	main of the sample
* Globals modified	:	None
* Design doc. section being implemented or pseudocode: 
* Change record		: 
* Date				Changed by					Details
*/
int main (int argc, char *argv[])
{
	// Log init
	dlog_scope lscope(g_apLogicalName);
	dlog_msg("hello world from %s taskno=%d!", g_apLogicalName, get_task_id());

	
	if (com_verifone_ipc::init(g_apLogicalName) != com_verifone_ipc::IPC_SUCCESS)
	{
		dlog_error("Cannot initialise internal communication library (IPC)!!");
		return 0;
	}

	if (com_verifone_pml::pml_init(g_apLogicalName) != 0)
	{
		dlog_error("Cannot initialise event system (PML)!");
		return 0;
	}

	
	{
		// start thread
		pthread_attr_t tattr;
		pthread_attr_init(&tattr);
		// 4k should be enough for everybody ;-)
		pthread_attr_setstacksize(&tattr, 4 * 1024);
		int slaveThreadId_;
		
	    int rc = pthread_create (&slaveThreadId_, &tattr, (void* (*)(void*))thread1Entry, NULL);
	}
	

	
	
	dlog_msg("Starting main loop, taskno=%d", get_task_id());
	
	for(;;)
	{
		SVC_WAIT(8000);
		
		com_verifone_pml::event_item ev;
		ev.event_id = com_verifone_pml::events::user1;
		ev.evt.user.flags = 0x01;
		int res = com_verifone_pml::event_raise( g_apLogicalNameThread_1,  ev );
		dlog_msg("Raising user1 event, flags 0x01 to %s, taskno=%d, res %d", g_apLogicalNameThread_1, get_task_id(), res);
	}

	
    return 1;
}


static int thread1Entry(int param)
{
	dlog_msg("%s started taskno=%d!", g_apLogicalNameThread_1, get_task_id());

	if (com_verifone_ipc::init(g_apLogicalNameThread_1) != com_verifone_ipc::IPC_SUCCESS)
	{
		dlog_error("Cannot initialise internal communication library (IPC)!!");
		return 0;
	}

	if (com_verifone_pml::pml_init(g_apLogicalNameThread_1) != 0)
	{
		dlog_error("Cannot initialise event system (PML)!");
		return 0;
	}

	{
		// start thread
		pthread_attr_t tattr;
		pthread_attr_init(&tattr);
		// 4k should be enough for everybody ;-)
		pthread_attr_setstacksize(&tattr, 4 * 1024);
		int slaveThreadId_;
		
	    int rc = pthread_create (&slaveThreadId_, &tattr, (void* (*)(void*))thread2Entry, NULL);
	}


	int eventsHandler = com_verifone_pml::event_open();
	com_verifone_pml::event_ctl(eventsHandler, com_verifone_pml::ctl::ADD, com_verifone_ipc::get_events_handler());

	com_verifone_pml::event_item userevent;
	userevent.event_id = com_verifone_pml::events::user0;
	com_verifone_pml::event_ctl(eventsHandler, com_verifone_pml::ctl::ADD, userevent);

	userevent.event_id = com_verifone_pml::events::user1;
	com_verifone_pml::event_ctl(eventsHandler, com_verifone_pml::ctl::ADD, userevent);
	
	dlog_msg("%s starting loop, taskno=%d", g_apLogicalNameThread_1, get_task_id());
	
	for(;;)
	{
        com_verifone_pml::eventset_t pml_events;
        int eventsCount = com_verifone_pml::event_wait(eventsHandler, pml_events);

        dlog_msg("%s taskno=%d got event(s) size=%d", g_apLogicalNameThread_1, get_task_id(), pml_events.size());
	            
        for(int i = 0, s = pml_events.size(); i < s; ++i)
        {
        	switch(pml_events[i].event_id) {
        	
        		case com_verifone_pml::events::user0:
			        dlog_msg("%s taskno=%d i=%d got USER0 event, flags=0x%x", g_apLogicalNameThread_1, get_task_id(), i, pml_events[i].evt.user.flags);
			        break;
        		case com_verifone_pml::events::user1:
			        dlog_msg("%s taskno=%d i=%d got USER1 event, flags=0x%x", g_apLogicalNameThread_1, get_task_id(), i, pml_events[i].evt.user.flags);
			        break;
			    defualt:
			        dlog_msg("%s taskno=%d i=%d got %d event", g_apLogicalNameThread_1, get_task_id(), i, pml_events[i].event_id);
			        break;
			}
        }	
	}
	
}




static int thread2Entry(int param)
{
	dlog_msg("%s started taskno=%d!", g_apLogicalNameThread_1_1, get_task_id());

	if (com_verifone_ipc::init(g_apLogicalNameThread_1_1) != com_verifone_ipc::IPC_SUCCESS)
	{
		dlog_error("Cannot initialise internal communication library (IPC)!!");
		return 0;
	}

	if (com_verifone_pml::pml_init(g_apLogicalNameThread_1_1) != 0)
	{
		dlog_error("Cannot initialise event system (PML)!");
		return 0;
	}

	dlog_msg("%s starting loop, taskno=%d", g_apLogicalNameThread_1_1, get_task_id());
	
	for(;;)
	{
		SVC_WAIT(5000);
		
		com_verifone_pml::event_item ev;
		ev.event_id = com_verifone_pml::events::user0;
		ev.evt.user.flags = 0x100;
		int res = com_verifone_pml::event_raise( g_apLogicalNameThread_1,  ev );
		dlog_msg("Raising user0 event, flags 0x100 to %s, taskno=%d, res %d", g_apLogicalNameThread_1, get_task_id(), res);
	}
	
}
