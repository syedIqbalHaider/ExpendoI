/*
 * testvos.cpp
 *
 *  Created on: 26-02-2013
 *      Author: lucck
 */



/* ---------------------------------------------------------------- */
#include "libpml/pml.h"
#include "libpml/pml_abstracted_api.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/epoll.h>
#include <termios.h>
#include <sys/wait.h>
#  include <fcntl.h>
#include <boost/exception/all.hpp>
#include <boost/function.hpp>
#include <boost/foreach.hpp>
#include <boost/array.hpp>
/* ---------------------------------------------------------------- */
namespace pml = com_verifone_pml;
/* ---------------------------------------------------------------- */

void toggle_canonical(bool on) 
{
    struct termios terminal_settings;

    // Get the existing terminal settings
    tcgetattr(0 /* stdin */, &terminal_settings);
    if(on) {
        // Disable canonical mode
        terminal_settings.c_lflag &= ~ICANON;
        // Read at least one character.
        terminal_settings.c_cc[VMIN] = 1; 
    } else {
        // Enable canonical mode 
        terminal_settings.c_lflag |= ICANON;
    }
    tcsetattr(0 /* stdin */, TCSANOW, &terminal_settings);

    return;
}
/* ---------------------------------------------------------------- */
#if 0		//File descriptor test
//Main test function
int main()
{
    toggle_canonical(true);   
    /*  Test manual  */
    if(0)
    {
        ::epoll_event ev;
        ev.events = EPOLLIN; 
        ev.data.fd = 0;
        int epc = epoll_create1( 0 );
        if( epoll_ctl(epc, EPOLL_CTL_ADD, 0, &ev ) )
        {
            printf("FAIL\n");
            exit(-1);
        }
        for(;;)
        {
            char c;
            printf( "EPOLL RET=%i\n",epoll_wait( epc , &ev, 1, -1 ) );
            read(0, &c, sizeof(c));
            printf("Char is=%c\n", c);
        }
    }
    
    printf( "Initialize lalala [%d]\n", pml::pml _init( NULL ) );

    int hwnd = pml::event_open();
	if( hwnd < 0 )
	{
		perror("Event handle");
		exit(-1);
	}
	pml::event_item ev;
    ev.event_id = pml::events::ipc;
    ev.evt.com.fd = STDIN_FILENO;
    ev.evt.com.flags = EPOLLIN |EPOLLPRI;
    printf("EVent add %i\n", 
        pml::event_ctl(hwnd , pml::ctl::ADD, ev )
    );
    pml::eventset_t revents;
    {
        //Cascade test
        int hwnd2 = pml::event_open(); 
        printf("EVent add2 %i\n",  pml::event_ctl(hwnd2 , pml::ctl::ADD, ev )  );
        printf("EVent add3 %i\n",  pml::event_ctl(hwnd, pml::ctl::ADD, hwnd2 ) );
    }
    char c;
    for(;;)
    {
        int ret = pml::event_wait(hwnd, revents );
        if( revents.empty() )
        {
            printf("No events %i\n",ret);
            sleep(2);
            break;
        }
        printf("RetWait=%i VSIZE=%lu\n", ret, revents.size() );
        for(unsigned xe=0; xe<revents.size(); xe++)
        {
            if( revents[xe].event_id == pml::events::ipc )
            {
                if( read( revents[xe].evt.com.fd, &c, sizeof(c)) != 1 )
                {
                    printf("ERROR READ\n"); 
                    break;
                }
                else
                {
                    printf("GOT CHAR [%c] ON HANDLE [%i], flags [%lu]\n", c, revents[xe].evt.com.fd, revents[xe].evt.com.flags);   
                }
            }
            else
            {
                printf("MAMEVENTA=%lu HWND=%i\n", revents[xe].event_id, revents[xe].evt.cascade.fd );
            }
        } 
    }
	pml::event_close( hwnd );
}
#elif 0				//User event test

/* ---------------------------------------------------------------- */
//Parent process execution
void parent_main()
{

	 printf( "Initialize PML library (parent) [%d] PID [%i]\n", pml::pml_init( NULL ),getpid() );
	 int hwnd = pml::event_open();
	 printf( "Event open result (parent) [%i]\n", hwnd );
	 if( hwnd < 0 )
	 {
		perror("Event handle");
		return;
	 }
	pml::event_item ev;
    ev.event_id = pml::events::user0;
    printf("EVent add %i\n",
        pml::event_ctl(hwnd , pml::ctl::ADD, ev ) 
    );
    pml::eventset_t revents;
    for(;;)
    {
    	 const int ret = pml::event_wait(hwnd, revents );
    	 if( ret < 0 )
    	 {
    		 perror("PML wait problem");
    		 return;
    	 }
    	 if( revents.empty() )
    	 {
    	    printf("No events %i\n",ret);
    	    sleep(2);
    	    break;
    	 }
         for(unsigned xe=0; xe<revents.size(); xe++)
         {
        	 printf("Got event %lu FLAGS: %lu\n", revents[xe].event_id, revents[xe].evt.user.flags );
         }
    }
}
/* ---------------------------------------------------------------- */
//Child process execution
void child_main()
{
	const std::string iname = std::string( program_invocation_short_name) + "_p1";
	printf( "Initialize PML library (child) [%d] PID [%i]\n", pml::pml_init( iname.c_str() ), getpid() );
	for( int i = 0; i< 100; i++ )
	{
		//Try to raise the event
		pml::event_item ev;
		ev.event_id =  pml::events::user0;
		ev.evt.user.flags = i;
		ev.evt.user.ext_flags = i+1000;
		printf("#LOOP %i # Invoke event result %i\n", i,
				pml::event_raise( program_invocation_short_name,  ev )
		);
		sleep(1);
	}
}

/* ---------------------------------------------------------------- */
//Main core function
int main( )
{ 
    const int fpid = fork();
    if( fpid < 0 )
    {
    	perror("Fork failed");
    	return EXIT_FAILURE;
    }
    else if( fpid == 0 )
    {
    	child_main();
    }
    else
    {
    	int status;
    	parent_main();
    	if( ::waitpid( fpid, &status, 0 ) < 0 )
    	{
    		perror("Wait pid failed");
    		return EXIT_FAILURE;FF, 0, false, false>  crc_ccitt2;
    crc_ccitt2 = std::for_each( data, data + data_len, crc_ccitt2 );
    	}
    }
	return 0; 
}
#elif 0		//Timer test
int main( )
{

	 printf( "Initialize PML library (parent) [%d] PID [%i]\n", pml::pml_init( NULL ),getpid() );
	 int hwnd = pml::event_open();
	 printf( "Event open result (parent) [%i]\n", hwnd );
	 if( hwnd < 0 )
	 {
		perror("Event handle");
		return -1;
	 }
	 {
		 pml::event_item ev;
		 int res = pml::set_timer( ev, 1000, true );
		 if( res < 0 )
		 {
			perror("Event handle");
			return -1;
		 }
		 printf("EVent add %i\n",
				pml::event_ctl(hwnd , pml::ctl::ADD, ev )
			);
		 printf(" #1 HWND %li TYPE %li\n", ev.evt.tmr.id, ev.event_id );
	 }
	 {
	 		 pml::event_item ev;
	 		 int res = pml::set_timer( ev, 2500, true );
	 		 if( res < 0 )
	 		 {
	 			perror("Event handle 2");
	 			return -1;
	 		 }
	 		 printf("EVent 2 add %i\n",
	 				pml::event_ctl(hwnd , pml::ctl::ADD, ev )
	 			);
	 		printf(" #2 HWND %li TYPE %li\n", ev.evt.tmr.id, ev.event_id );
	 	 }
	 pml::eventset_t revents;
	 for(;;)
	 {
		 const int ret = pml::event_wait(hwnd, revents );
	     if( ret < 0 )
	     {
	    	perror("PML wait problem");
	    	return -1;
	     }
	     if( revents.empty() )
	     {
	    	 printf("No events %i\n",ret);
	    	 sleep(2);
	    	 break;
	     }
	     for(unsigned xe=0; xe<revents.size(); xe++)
	     {
	    	 printf("Got event %lu MISSED: %lu ID %li\n", revents[xe].event_id, revents[xe].evt.tmr.num_expires, revents[xe].evt.tmr.id );
	     }
	 }
	 return 0;
}
#elif 0 /* Abstract test */
    int main( )
    {
        pml::dir_search search;
        char buf[256] = { '\0' };
        printf( "FIRST %i , NAME %s\n", search.get_first("*a*", buf, sizeof(buf) ), buf );
        while( search.get_next(buf, sizeof(buf) ) ==  pml::dir_search::err_found )
        {
           printf("NEXT %s\n", buf );
        }
        printf("RENAME %i ERRNO %i\n", pml::rename_file("analanalq.tst", "analanal.tst" ), errno);
        printf("DELETE %i ERRNO %i\n", pml::delete_file("analanal.tst" ), errno);
        //Test FOR the CRCS
        const char test_vec[] =  "0123456789";
        const unsigned long vec_len = sizeof(test_vec) - sizeof('\0');
        printf( "LRC=0x%lx\n", pml::svcCrcCalc( pml::lrc, 0, test_vec, vec_len ) );
        printf( "CRC16LSB=0x%lx\n", pml::svcCrcCalc( pml::crc16_lsb, 0, test_vec, vec_len ) );
        printf( "CRC16MSB=0x%lx\n", pml::svcCrcCalc( pml::crc16_msb, 0, test_vec, vec_len ) );
        printf( "CCIT_LSB=0x%lx\n", pml::svcCrcCalc( pml::ccitt_lsb, 0, test_vec, vec_len ) );
        printf( "CCIT_MSB=0x%lx\n", pml::svcCrcCalc( pml::ccitt_msb, 0, test_vec, vec_len ) );
        printf( "CRC32=0x%lx\n", pml::svcCrcCalc( pml::crc32, 0, test_vec, vec_len ) );
        printf( "CRC32_INV=0x%lx\n", pml::svcCrcCalc( pml::crc32_inverted, 0,test_vec, vec_len ) );
        printf( "CRC16=0x%lx\n", pml::svcCrcCalc( pml::crc16, 0, test_vec, vec_len ) );
        printf( "CCIT16=0x%lx\n", pml::svcCrcCalc( pml::ccitt16, 0, test_vec, vec_len ) );
        {
            const char in[] = "1A2B";
            char xout[32];
            const int x = pml::svcDsp2Hex( in, sizeof(in)-1, xout, sizeof(xout) );
            printf("RET=%i\n", x );
            for( size_t i=0;i<(sizeof(in)-1)/2;i++ )
            {
                printf("%02x", xout[i] );
            }
            printf("\n");
        }
        {
            const char in[] = "\x11\x22\x33\xAA";
            char xout[32];
            const int x = pml::svcHex2Dsp( in, sizeof(in)-1, xout, sizeof(xout) );
            printf("RET2=%i [%s]\n", x, xout );
        }
        printf("RAMSIZE=%li\n", pml::svcRAMSize());
        printf("FlashSIZE=%li\n", pml::svcFlashSize() );
        //Info tests
        typedef boost::function< void(char*, const size_t) > info_fun_t;
        const boost::array<info_fun_t, 9 > array = 
        {
                pml::svcInfoPtid,
                pml::svcInfoEprom,
                pml::svcInfoModelNum,
                pml::svcInfoManufacturingData,
                pml::svcInfoCountry,
                pml::svcInfoPartNumber,
                pml::svcInfoHardwareVersion,
                pml::svcInfoSerialNumber,   
                pml::svcInfoLotNumber
        };
        char ret_buf[256] = { '\0' };
        const size_t bsize = sizeof(ret_buf);
        BOOST_FOREACH(info_fun_t f, array)
        {
            f( ret_buf, bsize);
            printf("Ret buf %s\n", ret_buf);
        }
}
#elif 0
//Main test function
int main()
{
    toggle_canonical(true);   
    
    printf( "Initialize lalala [%d]\n", pml::pml_init( NULL ) );

    int hwnd = pml::event_open();
	if( hwnd < 0 )
	{
		perror("Event handle");
		exit(-1);
	}
	
	int thread_filedes[2];
	
    if (pipe(thread_filedes)==-1) 
	{
		perror("pipe error");
		exit(-1);
	}
	
    // Make the write side of the pipe non-blocking to avoid deadlock
    // conditions (STR #1537)
    fcntl(thread_filedes[1], F_SETFL,
          fcntl(thread_filedes[1], F_GETFL) | O_NONBLOCK);

	pml::event_item ev;
    ev.event_id = pml::events::comm;
    ev.evt.com.fd = thread_filedes[0];
    ev.evt.com.flags = EPOLLIN |EPOLLPRI;
    
    printf("EVent add %i\n", 
        pml::event_ctl(hwnd , pml::ctl::ADD, ev )
    );
    
    char c = 'x';
    write(thread_filedes[1], &c, sizeof(char));
    
    pml::eventset_t revents;
      	
    for(;;)
    {
    	printf("waiting for events...\n");
    	
        int ret = pml::event_wait(hwnd, revents );
        if( revents.empty() )
        {
            printf("No events %i\n",ret);
            sleep(2);
            break;
        }
        printf("RetWait=%i VSIZE=%lu\n", ret, revents.size() );
        for(unsigned xe=0; xe<revents.size(); xe++)
        {
            if( revents[xe].event_id == pml::events::comm )
            {
                if( read( revents[xe].evt.com.fd, &c, sizeof(c)) != 1 )
                {
                    printf("ERROR READ\n"); 
                    break;
                }
                else
                {
                    printf("GOT CHAR [%c] ON HANDLE [%i], flags [%lu]\n", c, revents[xe].evt.com.fd, revents[xe].evt.com.flags);   
                }
            }
        } 
    }
	pml::event_close( hwnd );
}
#elif 0
//Main test function
int main()
{
    toggle_canonical(true);   
    
    printf( "Initialize lalala [%d]\n", pml::pml_init( NULL ) );

    int hwnd = pml::event_open();
	if( hwnd < 0 )
	{
		perror("Event handle");
		exit(-1);
	}
	
	pml::event_item ev;
    ev.event_id = pml::events::comm;
    ev.evt.com.flags = EPOLLIN |EPOLLPRI;

	int fd1=open("/dev/scdrv",O_RDONLY | O_NONBLOCK);
	printf("/dev/scdrv handle=%i\n", fd1);	
    ev.evt.com.fd = fd1;
    printf("EVent add %i, errno=%i\n\n", 
        pml::event_ctl(hwnd , pml::ctl::ADD, ev ),
        errno
    );


	int fd2=open("/dev/amsr",O_RDONLY | O_NONBLOCK);
	printf("/dev/amsr handle=%i\n", fd2);	
    ev.evt.com.fd = fd2;
    printf("EVent add %i, errno=%i\n\n", 
        pml::event_ctl(hwnd , pml::ctl::ADD, ev ),
        errno
    );

	int fd3=open("/dev/keypad",O_RDONLY | O_NONBLOCK);
	printf("/dev/keypad handle=%i\n", fd3);	
    ev.evt.com.fd = fd3;
    printf("EVent add %i, errno=%i\n\n", 
        pml::event_ctl(hwnd , pml::ctl::ADD, ev ),
        errno
    );

	int fd4=open("/dev/scr",O_RDONLY | O_NONBLOCK);
	printf("/dev/scr handle=%i\n", fd4);	
    ev.evt.com.fd = fd4;
    printf("EVent add %i, errno=%i\n\n", 
        pml::event_ctl(hwnd , pml::ctl::ADD, ev ),
        errno
    );
    
    
    char c = 'x';
    
    pml::eventset_t revents;
      	
    for(;;)
    {
    	printf("waiting for events...\n");
    	
        int ret = pml::event_wait(hwnd, revents );
        if( revents.empty() )
        {
            printf("No events %i\n",ret);
            sleep(2);
            break;
        }
        printf("RetWait=%i VSIZE=%lu\n", ret, revents.size() );
        for(unsigned xe=0; xe<revents.size(); xe++)
        {
	        printf("revent.com.fd=%i\n", revents[xe].evt.com.fd );
	        
            if( revents[xe].event_id == pml::events::comm )
            {
                if( read( revents[xe].evt.com.fd, &c, sizeof(c)) != 1 )
                {
                    printf("ERROR READ\n"); 
                    break;
                }
                else
                {
                    printf("GOT CHAR [%x] ON HANDLE [%i], flags [%lu]\n", c, revents[xe].evt.com.fd, revents[xe].evt.com.flags);   
                }
            }
        } 
    }
	pml::event_close( hwnd );
}
#elif 1				//User event test

/* ---------------------------------------------------------------- */
//Child process execution
void threadEntry(void *)
{
	const std::string iname = std::string( program_invocation_short_name) + "_p1";
	printf( "Initialize PML library (child) [%d] PID [%i]\n", pml::pml_init( iname.c_str() ), getpid() );
	
	 int hwnd = pml::event_open();
	 printf( "CHILD Event open result (parent) [%i]\n", hwnd );
	 if( hwnd < 0 )
	 {
		perror("Event handle");
		return;
	 }
	pml::event_item ev;
    ev.event_id = pml::events::user0;
    printf("CHILD EVent add %i\n",
        pml::event_ctl(hwnd , pml::ctl::ADD, ev ) 
    );
	
	for( int i = 0; i< 100; i++ )
	{
		//Try to raise the event
		pml::event_item ev;
		ev.event_id =  pml::events::user0;
		ev.evt.user.flags = i;
		ev.evt.user.ext_flags = i+1000;
		printf("CHILD #LOOP %i # Invoke event result %i\n", i,
				pml::event_raise( program_invocation_short_name,  ev )
		);

	     pml::eventset_t revents;
   		 const int ret = pml::event_wait(hwnd, revents );
   		 printf("CHILD event_wait ret=%d revents.size=%d\n", ret, revents.size());
   		 if( ret < 0 )
    	 {
   			 perror("CHILD PML wait problem");
   			 return;
    	 }
    	 if( revents.empty() )
   		 {
    	    printf("CHILD No events %i\n",ret);
   		    sleep(2);
    	    break;
   		 }
    		 
         for(unsigned xe=0; xe<revents.size(); xe++)
   	     {
       		 printf("CHILD got event %lu FLAGS: %lu\n", revents[xe].event_id, revents[xe].evt.user.flags );
         }
		
		
		sleep(1);
	}
}

/* ---------------------------------------------------------------- */
//Main core function
int main( )
{ 
	 printf( "Initialize PML library (parent) [%d] PID [%i]\n", pml::pml_init( program_invocation_short_name ),getpid() );
	 int hwnd = pml::event_open();
	 printf( "Event open result (parent) [%i]\n", hwnd );
	 if( hwnd < 0 )
	 {
		perror("Event handle");
		return EXIT_FAILURE;
	 }
	pml::event_item ev;
    ev.event_id = pml::events::user0;
    printf("EVent add %i\n",
        pml::event_ctl(hwnd , pml::ctl::ADD, ev ) 
    );

	// start thread
	pthread_attr_t tattr;
	pthread_attr_init(&tattr);
	// 4k should be enough for everybody ;-)
	pthread_attr_setstacksize(&tattr, 4 * 1024);
			
	pthread_t childThreadId_;
    int rc = pthread_create (&childThreadId_, &tattr, (void* (*)(void*))threadEntry, NULL);

    if(rc <0 )
    {
    		perror(" pthread_create failed");
    		return EXIT_FAILURE;
    }

	const std::string iname = std::string( program_invocation_short_name) + "_p1";

    pml::eventset_t revents;
    for(;;)
    {
    	 const int ret = pml::event_wait(hwnd, revents );
   		 printf("PARENT event_wait ret=%d revents.size=%d\n", ret, revents.size());
    	 if( ret < 0 )
    	 {
    		 perror("PARENT PML wait problem");
    		 return EXIT_FAILURE;
    	 }
    	 if( revents.empty() )
    	 {
    	    printf("PARENT No events %i\n",ret);
    	    sleep(2);
    	    break;
    	 }
         for(unsigned xe=0; xe<revents.size(); xe++)
         {
        	 printf("PARENT got event %lu FLAGS: %lu\n", revents[xe].event_id, revents[xe].evt.user.flags );

			//Try to raise the event
			pml::event_item ev;
			ev.event_id =  pml::events::user0;
			ev.evt.user.flags = revents[xe].evt.user.flags + 20000;
			ev.evt.user.ext_flags = revents[xe].evt.user.flags+20000;
			printf("PARENT Invoke response to %i result %i\n", revents[xe].evt.user.flags,
				pml::event_raise( iname.c_str(),  ev )
			);
         }
    }
    
	return 0; 
}
#endif
/* ---------------------------------------------------------------- */

