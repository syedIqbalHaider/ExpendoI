/******************************************************************************
      Copyright (C) 2002 by VeriFone Inc. All rights reserved.

 No part of this software may be used, stored, compiled, reproduced,
 modified, transcribed, translated, transmitted, or transferred, in any form 
 or by any means whether electronic, mechanical, magnetic, optical, 
 or otherwise, without the express prior written permission of VeriFone, Inc.
******************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <svc.h>
#include <errno.h>

#include "logsys.h"

#include "PMLEventHandler.h"

const char *g_apLogicalName = "TESTAPP";

/*
* Function name		:	main
* Inputs			:   int argc, char *argv[]		
* Outputs			:	Return value - Success, Failure		
* Description		:	main of the sample
* Globals modified	:	None
* Design doc. section being implemented or pseudocode: 
* Change record		: 
* Date				Changed by					Details
*/
int main (int argc, char *argv[])
{
	using namespace pml_verifone_interface;
	
	// Log init
	dlog_scope lscope(g_apLogicalName);
	dlog_msg("hello world from %s!", g_apLogicalName );

	
	int p1 = open("P:", 0);
	pipe_init_msg(p1, 10);

	int p2 = open("P:", 0);
	pipe_init_msg(p2, 10);


	unsigned char dummy = 0;
	pipe_connect(p1, p2);
	write(p1, (const char *)&dummy, sizeof (dummy));

	int hConsole = open(DEV_CONSOLE, 0);
	int hmag = open(DEV_CARD, 0);
	int hicc = open(DEV_ICC1, 0);

	int ourEventHandler = epoll_create();
	//epoll_ctl(ourEventHandler, CTL_ADD, pollitem_t(-1, 0xFFFFFFFF, 0, "All events"));
	epoll_ctl(ourEventHandler, CTL_ADD, pollitem_t(EVT_KBD, "Keyboard"));
	epoll_ctl(ourEventHandler, CTL_ADD, pollitem_t(EVT_MAG, "Swipe"));
	epoll_ctl(ourEventHandler, CTL_ADD, pollitem_t(EVT_ICC1_INS | EVT_ICC1_REM, "Chip"));
	epoll_ctl(ourEventHandler, CTL_ADD, pollitem_t(p1, EVT_PIPE, "Pipe P1"));
	epoll_ctl(ourEventHandler, CTL_ADD, pollitem_t(p2, EVT_PIPE, "Pipe P2"));
    int anotherEventHandler = epoll_create();
	epoll_ctl(anotherEventHandler, CTL_ADD, pollitem_t(EVT_KBD, "Keyboard"));
	epoll_ctl(anotherEventHandler, CTL_ADD, pollitem_t(EVT_MAG, "Swipe"));


	pollset_t pollEvents;
	int tmp = epoll_peek(ourEventHandler, pollEvents);
	dlog_msg("Poll return %d events", tmp);
	handle_t ourHandles;
    ourHandles.push_back(ourEventHandler);
    ourHandles.push_back(anotherEventHandler);
    
    int mergedEvents = epoll_create(ourHandles);
	for(;;)
	{
		dlog_msg("waiting for events...");

		pollset_t events;
		//int evtCnt = epoll_wait(ourHandles, events, 60000);
        int evtCnt = epoll_wait(mergedEvents, events, 60000);
		
		dlog_msg("received %d events", evtCnt);
		
		for(int i = 0, cnt = events.size(); i < cnt; ++i)
		{
			pollitem_t evt = events[i];
			
			dlog_msg("received event %X for '%s'", evt.revents, evt.description.c_str());
			
			if(strcmp(evt.description.c_str(), "Pipe P1") == 0)
			{
			   	int rc = read(p1, (char *)&dummy, sizeof (dummy));
				dlog_msg("Read %d bytes from p1", rc);
			}
			else if(strcmp(evt.description.c_str(), "Pipe P2") == 0)
			{
			   	int rc = read(p2, (char *)&dummy, sizeof (dummy));
				dlog_msg("Read %d bytes from p2", rc);
			}
			else if(strcmp(evt.description.c_str(), "Keyboard") == 0)
			{
			   	int rc;
			   	do
			   	{
				   	rc = read(hConsole, (char *)&dummy, sizeof (dummy));
					if (rc > 0) 
						dlog_msg("Read %d from console", dummy);
				} while (rc > 0);
			}
			
		}
		
		if(evtCnt == 0)
		{
				dlog_msg("Writing data to P1");
			    pipe_connect(p2, p1);
			   	write(p2, (const char *)&dummy, sizeof (dummy));
		}
	}

	
    return 1;
}

