/*
 * =====================================================================================
 *
 *       Filename:  pml_os_core.h
 *
 *    Description: PML abstraction API core header
 *
 *        Version:  1.0
 *        Created:  27.02.2015 09:48:51
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lucjan Bryndza (LB), lucck
 *   Organization:  
 *
 * =====================================================================================
 */


#ifndef _COM_VERIFONE_PML_OS_CORE_H
#define _COM_VERIFONE_PML_OS_CORE_H

#include <stddef.h>
#include <cstddef>
#include <string>
#include <list>
#include <vector>
#include <map>
#include "safe_string.h"

namespace com_verifone_pml
{
	enum pml_return_codes {
		PML_SUCCESS = 0,
		PML_FAILURE = -1
	};
	/************************************************************************************************************************************************/
	/* File system calls (RQAP-001) */
	/************************************************************************************************************************************************/
	class dir_search
	{
	public:
		enum { err_system = -1, err_found = 1, err_not_found = 0 };
		/* Default constructor */
		dir_search();
		/* Destructor */
		~dir_search();
		/*
		 * Gets name of first file, which fits given mask. Up to bufSize characters of found file name is copied into buffer.
		*/
		int get_first(const char *mask, char * buf, std::size_t bufSize);
		/*
		 * If mask is set using dir_get_first(), this returns next file fitting given mask.
		 */
		int get_next(char * buf, size_t bufSize);
	private:
		class internal_state;
		internal_state * const m_state;
	};

	/*
	 * Copies src file to dst file
	 */
	int copy_file(const char * src, const char * dst);
	/*
	 * Moves src file to dst file
	 */
	int move_file(const char * src, const char * dst);
	/*
	 * Deletes file, represented by name
	 */
	int delete_file(const char *name);
	/*
	* Renames file - oldname is replaced by newname
	 */
	int rename_file(const char *oldname, const char *newname);

	int set_working_directory(const char *directory);

	/************************************************************************************************************************************************/
	/* Service calls (RQAP-002) */
	/************************************************************************************************************************************************/
	/*
	 * Gets 8 characters long terminal identification number
	 */
	void svcInfoPtid(char * buffer, const std::size_t bufferSize);

	/*
	 * Gets 8 characters long firmware version
	 */
	void svcInfoEprom(char * buffer, const std::size_t bufferSize);

	/*
	 * Gets 12 characters long model number
	 */
	void svcInfoModelNum(char * buffer, const size_t bufferSize);

	/*
	 * Gets 30 characters long factory defined manufacturing data
	 */
	void svcInfoManufacturingData(char * buffer, const size_t bufferSize);

	/*
	 * Gets 12 characters long country variant data
	 */
	void svcInfoCountry(char * buffer, const size_t bufferSize);

	/*
	 * Gets 12 characters long part number
	 */
	void svcInfoPartNumber(char * buffer, const size_t bufferSize);

	/*
	 * Gets 2 characters long hardware version
	 */
	void svcInfoHardwareVersion(char * buffer, const size_t bufferSize);

	/*
	 * Gets 11 characters long serial number
	 */
	void svcInfoSerialNumber(char * buffer, const size_t bufferSize);
	// Returns serial number without dashes, just digitWiększego gością z taką ciągotą do rządzejs
	void svcInfoBareSerialNumber(char * buffer, const size_t bufferSize);

	/*
	 * Gets 6 characters long lot number
	 */
	void svcInfoLotNumber(char * buffer, const size_t bufferSize);

	/*
	 * Gets RAM size in kBytes
	 */
	long svcRAMSize();

	/*
	 * Gets Flash size in kBytes
	 */
	long svcFlashSize();

	/*
	 * Gets SBI version
	 */
	void svcInfoSBIVersion(char * buffer, const size_t bufferSize);

	/*
	 * Gets CIB ID
	 */
	void svcInfoCIBID(char * buffer, const size_t bufferSize);

	/*
	 * Gets CIB Version
	 */
	void svcInfoCIBVersion(char * buffer, const size_t bufferSize);

	/*
	 * Gets removal switches and tamper status
	 */
	int svcGetRemovalSwitchStatus();
	int svcGetTamperStatus();

	/************************************************************************************************************************************************/
	/* Calls (RQAP-003) */
	/************************************************************************************************************************************************/
	/*
	 * Converts data, stored in dsp buffer, to hex equivalent. Dsp_len is length of dsp buffer (zero-termination is not required), max_hex_len is size of hex buffer.
	 * Example: svcDsp2Hex("1A2B", hex, 4, 2) stores "\x1A\x2B" in hex buffer.
	 * Buffers hex and dsp should NOT overlap!
	 */
	int svcDsp2Hex(const char *dsp, size_t dsp_len, char *hex, const size_t max_hex_len);
	/*
	 * Converts data, stored in hex buffer, to display (ASCII) equivalent. Hex_len is length of hex buffer, max_dsp_len is size of display buffer (the buffer will be zero-terminated on exit).
	 * Example: svcHex2Dsp("\x1A\x2B", dsp, 2, 5) stores ASCIIZ string "1A2B" in display buffer.
	 * Buffers hex and dsp should NOT overlap!
	 */
	int svcHex2Dsp(const char *hex, size_t hex_len, char *dsp, const size_t max_dsp_len);

	/************************************************************************************************************************************************/
	/* CRCs and checksums calls (RQAP-004) */
	/************************************************************************************************************************************************/
	typedef enum _crc_type
	{
		lrc,
		crc16_lsb,
		crc16_msb,
		ccitt_lsb,
		ccitt_msb,
		crc32,
		crc32_inverted,
		crc16,
		ccitt16,
	} CRC_TYPES;
	unsigned long svcCrcCalc(const CRC_TYPES type, const unsigned long seed, const void *msg, const size_t size);

	struct SHA1_OUTPUT
	{
		unsigned char shabuf[20];
	};
	int SHA1(const unsigned char * input_buffer, size_t input_length, SHA1_OUTPUT & output);
	struct SHA256_OUTPUT
	{
		unsigned char shabuf[32];
	};
	int SHA256(const unsigned char * input_buffer, size_t input_length, SHA256_OUTPUT & output);

	struct MD5_OUTPUT
	{
		unsigned char md5buf[16];
	};
	class MD5
	{
		public:
			MD5();
			void update(const void * input_buffer, size_t input_length);
			int finalize(MD5_OUTPUT & output);
	};

	/************************************************************************************************************************************************/
	/* System Handling(RQDV-004) */
	/************************************************************************************************************************************************/
	void svcRestart();
	void svcRestartApplication();
	void sleep(int seconds);
	void usleep(int microseconds);


	void datetime2seconds(const char *yyyymmddhhmmss, unsigned long *seconds);

	int luhn(const std::string &pan_ascii);
	bool isFileSigned(const std::string &file);
	std::string getFlashRoot(int gid = 0);
	std::string getRAMRoot(int gid = 0);
	std::string getLogsDir();
	int openConsole();

	int get_env(const char *key, char *buf, int size);
	int put_env(const char *key, const char *buf, int size);

	int getkey (const char *key, char *buf, int size, const char *file);
	int putkey (const char *key, const char *buf, int size, const char *file);

	int alpha_shift(int c);

	int normal_tone();
	int error_tone();
	int sound (int note, int msec);

	int read_clock(char *yyyymmddhhmmssw, size_t size);
	int set_clock(const char * yyyymmddhhmmss);
	long get_uptime();

	int set_backlight_level(int percent); // 0 - 100
	int set_backlight(bool enable);
	int get_backlight_level();

	// Battery
	// 0 - battery present, 1 - battery present and charging, -1 - no battery
	int get_battery_status();
	enum battery_conditions {
		BATTERY_FULL,
		BATTERY_HIGH,
		BATTERY_LOW,
		BATTERY_CRITICAL,
		BATTERY_CHARGING,
		BATTERY_ABSENT
	};
	int get_battery_condition();
	// -1 - no battery, 0-100 - percentage
	int get_battery_percentage();
	enum battery_values {
		BATTERY_FULLCHARGE = 0,
		BATTERY_REMAININGCHARGE = 1,
		BATTERY_VOLTAGE = 2,
		BATTERY_AVAIL = 3,
		BATTERY_TEMP = 9,
		BATTERY_CURRENT = 10,
		BATTERY_CHARGER_STATUS = 13
	};
	int get_battery_value(battery_values reqValue);
	// 0 - undocked, 1 - docked, -1 - no battery
	int get_dock_status();

	int set_keyboard_backlight(int enabled); // 1 - enable, 0 - disable
	int get_keyboard_backlight();

	int key_beeps(bool enable);

	int get_first_file_group();
	int get_last_file_group();

	int svcProcessUpgradeFile(const char * file, int * reboot = 0);

	int svcAuthenticateFile(const char * file);

	char * get_serial_port_name(char * buf, size_t size, int port_no);

	bool has_base_station();
	bool base_station_info(std::string & firmware_version, std::map<std::string, std::string> * base_info = NULL);

	// Gilbarco
	// out_file is OUT only!
	int dump_logs(std::string & out_file);

	namespace apm {
		enum MODES {
			HIBERNATE = 0,
			SUSPEND,
			STANDBY
		};
		int init(unsigned long wakeDevices);
		int init(const std::vector<std::string> & wakeDevices);
		int activate(MODES mode);
	};

	//! Communication capabilities structure
	struct communication_capabilities {
		communication_capabilities() 
			:is_wifi(false), is_ethernet(false), 
			 is_bluetooth(false), is_gprs(false)
		{}
		bool is_wifi;
		bool is_ethernet;
		bool is_bluetooth;
		bool is_gprs;
	};

	/* Get terminal communiction capabilities
	 * @param[in] communication capabilities
	 * @return[out] error
	 */
	int get_communication_capabilities( communication_capabilities& cap );

	std::string get_invocation_name();
}


#endif   /* ----- #ifndef pml_os_core_INC  ----- */

