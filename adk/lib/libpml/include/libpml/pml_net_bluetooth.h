/*
 * =====================================================================================
 *
 *       Filename:  pml_bluetooth.h
 *
 *    Description:  PML net bluetooth abstracted API
 *
 *        Version:  1.0
 *        Created:  27.02.2015 09:38:38
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lucjan Bryndza (LB), lucck
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef _COM_VERIFONE_PML_NET_BLUETOOTH_H
#define _COM_VERIFONE_PML_NET_BLUETOOTH_H

#include <vector>
#include <string>

namespace com_verifone_pml {
	namespace net {
		namespace bluetooth 
		{
			const unsigned int SSP_WITHOUT_PASSKEY = 0;
			const unsigned int SSP_WITH_PASSKEY = 1;
			const unsigned int LEGACY_WITH_PASSKEY = 2;

			//! Ethernet connect with selected type of interface
			struct conn_type {
				enum _enum_ {
					profile,		//! Use profile settings
					ethernet,		//! Ethernet connection
					dialup,			//! Dialup connection
					serial          //! Serial connection 
				};
			};
			
			//! Pairing method
			struct pair_method {
				enum _enum_ {
					legacy = 0,
					ssp = 1,
					ssp_passkey = 2
				};
			};

			//! Bluetooth device scan
			class device {
			public:
				template<typename Archive> 
				void serialize( Archive& ar, const unsigned int /*file_version */) {
					ar & m_name & m_address & m_btclass & m_serial 
					   & m_ethernet & m_dial;
				}

				device( const char *address, const char *name, const unsigned char *bt_class )
					: m_name( name ), m_address( address ), 
					  m_btclass( (unsigned(bt_class[0])<<24) | (unsigned(bt_class[1])<<16)|
							 	 unsigned(bt_class[2]) ), 
					  m_serial(false), m_ethernet(false), m_dial(false)
				{}
				device( const char* address, const char* name ) 
					: m_name( name ), m_address( address ), m_btclass(0),
					  m_serial(false), m_ethernet(false), m_dial(false)
				{
				}
				device( const std::string& address=std::string(), const std::string& name=std::string() ) 
					: m_name( name ), m_address( address ), m_btclass(0),
					  m_serial(false), m_ethernet(false), m_dial(false)
				{
				}
				const std::string& name() const {
					return m_name;
				}
				const std::string& address() const {
					return m_address;
				}
				void address( const std::string& addr ) {
					m_address = addr;
				}
				unsigned btclass() const {
					return m_btclass;
				}
				void serial( bool en ) {
					m_serial = en;
				}
				void ethernet( bool en ) {
					m_ethernet = en;
				}
				void dial( bool en ) {
					m_dial = en;
				}
				bool serial() const {
					return m_serial;
				}
				bool ethernet() const {
					return m_ethernet;
				}
				bool dial() const {
					return m_dial;
				}
				bool operator==( const device& dev ) const {
					return m_address == dev.m_address;
				}
				bool empty() const {
					return m_address.empty();
				}
			private:
				std::string m_name;
				std::string m_address;
				unsigned m_btclass;
				bool m_serial;
				bool m_ethernet;
				bool m_dial;
			};

			//BT profile
			class network_profile {
			public:
				network_profile( const device& cfg, int profile_id, bool isdefault )
				: m_cfg(cfg), m_profile_id(profile_id), m_isdefault(isdefault)
				{}
				network_profile(): m_profile_id(-1), m_isdefault(false)
				{}
				const device& get_device() const {
				return m_cfg;
				}
				int get_profile_id() const {
					return m_profile_id;
				}
				bool is_default() const {
					return m_isdefault;
				}
			private:
				device m_cfg;
				int m_profile_id;
				bool m_isdefault;
			};

			//! Enable or disable bluetooth device
			int enable( bool enable );

			/** Function establish Bluetooth connection with selected type
			 * @param[in] link Connection type of the device
			 * @param[in] profile_id Select profile id
			 * @return Error -1 if failed and errno set
			 */
			int connect( int pair_index, conn_type::_enum_ link=conn_type::profile);

			/** Destroy the bluetooth link
			 * @return error code */
			int disconnect( conn_type::_enum_ link=conn_type::profile );
				
			/**  Scan the available devices in range 
			 *  @param[out] dev_list Return device list in the application
				@return Number of count of the device or -1 and errno will be set
			*/
			int scan( std::vector<device> &dev_list );

			/** Pair with the selected device from the list
			 *  NOTE: Pairing also is used to add data to the profile
			 * @param[in] dev Input device for pair
			 * @param[in] pin Pin to pair
			 * @param[in] method Pairing ethod @see pair_method struct
			 * @return 0 on success -1 if fail errno will be set
			 */
			int pair( const device& dev, const char pin[], 
					pair_method::_enum_ method = pair_method::legacy );

			/** Unpair with the selected device from the list
			 * @param[in] paired_index Unpair the device using paired index
			 * @return 0 on success -1 if fail errno will be set
			 */
			int unpair( int paired_index );
			
			/** is_paired with the selected device from the list
			 * @param[in] dev Input device for pair
			 * @return 0 on success -1 if fail errno will be set
			 */
			int is_paired( int pair_index );
			
			/** Get the paired device list 
			 * @param[out] Paired device list
			 * @param[out] Errno
			 */
			int paired_list( std::vector<network_profile>& dev );
			int paired_list( std::vector<device>& dev );

			/** 
			 * Get paired devices count
			 * @param[in] count Get paired profiles count
			 * @return error code
			 */
			int get_paired_count( int& count );

			/** 
			 * Find paired device as an address
			 * @param[in] device Get paired profiles count
			 * @param[out] pair_index Paired index
			 * @return error code
			 */
			int find_paired( const device &device, int& pair_index );

			/** 
			 *  Set device as paired defaults
			 *	@param[in] pair_index Pairing device index
			 *	@return Error code
			 */
			int set_paired_default( int pair_index );


			/** Get device as defualt paired
			 * @param[out] Get paired device info
			 * @return Error code
			 */
			int get_default_paired( device& cfg );
			
			/** Get paired device profile id
			 * @param[out] profile_id Paired profile index returned
			 * @return Error code 
			 */
			int get_default_paired_id( int &pair_index );

			/** Get connected list of devices
			 * @param[in] List of connected devices
			 * @return error code
			 */
			int get_connected( std::vector<device>& m_list );
		}
	}
}

#endif
