/******************************************************************************

  FILE: ux.h

  DESC: Ux-100 device handling functions

******************************************************************************/

#ifndef UX_H
#define UX_H

#ifdef __cplusplus

#include <cstddef>
using std::size_t;

class ux
{
	public:
		ux();
		~ux();
		bool is_ux()
			{ return initialized; }
		bool have_keyboard()
			{ return haveKeyboard; }
		bool have_display()
			{ return haveDisplay; }
		int pinStartPinEntry(unsigned char  pinLenMin, unsigned char pinLenMax, int timeout);
		int pinAbort();
		int pinDelete(int /*cnt*/);
		int pinSendEncPinBlockToVault();
		int pinGetNbCurrDigitEntered();
		int pinEntryMode();
		int keybdInit();
		int keybdGetKey(unsigned int timeout);
		int dispSetBacklightMode(int mode, unsigned int time);
		int getVersions(char * id, size_t idSize,
			char * family, size_t familySize,
			char * version, size_t versionSize);
		int getVersion(char * ver, size_t verSize);
		int getSerialNo(char * sn, size_t snSize);
		int getBareSerialNo(char * sn, size_t snSize);
		int getPartNo(char * pn, size_t pnSize);
		int getHardwareVer(char * hw, size_t hwSize);
		int getModelNumber(char *mn, size_t mnSize);
		int getPTID(char *ptid, size_t ptidSize);
		int getRemovalSwitchStatus();
		int getKbdStatus(unsigned char *secure_mode, unsigned char *nb_function_key, unsigned char *nb_normal_key, unsigned char *nb_row, unsigned char *nb_col, unsigned char *key_map);
		int isKbdSecure();
		int setKeyboardBeep(bool enable);
		int setContrast(int value);
		int setBacklight(int value)
			{ return dispSetBacklightMode(value == 0 ? 0 : 1, 0); }
		int getTamperStatus();
		bool hasKeyboard() { return haveKeyboard; }
		bool hasDisplay() { return haveDisplay; }
	private:
		bool initialized;
		bool haveKeyboard, haveDisplay;
};

#else
		// Only functions needed by NanoX
		int is_ux();
		int ux_have_keyboard();
		int ux_have_display();
		int ux_keybdInit();
#endif

#endif



