/*
 * =====================================================================================
 *
 *       Filename:  pml_net_data.hpp
 *
 *    Description:  PML net data header
 *
 *        Version:  1.0
 *        Created:  27.02.2015 11:11:14
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lucjan Bryndza (LB), lucck
 *   Organization:  
 *
 * =====================================================================================
 */


#ifndef  _COM_VERIFONE_pml_net_data_HPP
#define  _COM_VERIFONE_pml_net_data_HPP


#include <string>

namespace com_verifone_pml {
namespace net {

		//! Interface information
		struct interface_info {
			std::string name;		//!Interface name
			std::string addr;		//!Interface address
			std::string bdaddr;		//! Interface netmask
			std::string netmask;	//! Netmask address
			std::string hwaddr;		//! Hardware address if available
			int metric;				//! Interface metric
			int mtu;				//! MTU
			bool up;				//! Interface is UP
			bool point_to_point;	//! Point to point interface
			bool multicast;			//! Support multicast
			bool loopback;			//! It is a loopback interface
			bool dhcp;			//! Iface uses DHCP
		};
		//! Interface config
		struct interface_config {
			std::string addr;		//! Network address
			std::string netmask;	//! Network mask
			std::string gateway;	//! Gateway
			std::string dns1;		//! DNS1
			std::string dns2;		//! DNS2
		};

}}


#endif   /* ----- #ifndef pml_net_data_INC  ----- */
