/*
 * =====================================================================================
 *
 *       Filename:  pml_net_wifi.h
 *
 *    Description: PML net wifi abstracted API
 *
 *        Version:  1.0
 *        Created:  27.02.2015 10:01:50
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lucjan Bryndza (LB), lucck
 *   Organization:  
 *
 * =====================================================================================
 */


#ifndef _COM_VERIFONE_PML_NET_WIFI_H
#define _COM_VERIFONE_PML_NET_WIFI_H

#include <string>
#include <vector>
#include <list>
#include "pml_net_data.hpp"

namespace com_verifone_pml {
namespace net {
namespace wifi {

		/** WiFi network structure
		 */
		//! Wifi anthentication mode
		enum auth {
			auth_unknown,
			auth_none,
			auth_wep,
			auth_wep_shared,
			auth_wpa_psk,
			auth_wpa,
			wpa_none,
			auth_wpa2_psk,
		};
		//! Wifi algo enim
		enum algo {
			algo_unknown,		//! Unknown algorithm
			algo_none,			//! No algo open
			algo_default,		//! Autodetect based on auth
			algo_wep1,			//! Wep algo
			algo_tkip,			//! Tkip algo
			algo_wep128,		//! WEP 128 algo
			algo_aes_ccm
		};
		//! WIfo config
		class medium_config {
		public:
		template<typename Archive> 
			void serialize( Archive& ar, const unsigned int /*file_version */) {
				ar & m_bssid & m_ssid & m_auth & m_algo & m_passwd;
			}
			medium_config( const std::string &ssid, auth auth, const std::string& bssid=std::string(),
				algo algo=algo_default ) 
			: m_bssid(bssid), m_ssid(ssid), m_auth(auth), m_algo(algo)
			{}
			medium_config(): m_auth(auth_unknown), m_algo(algo_unknown)
			{}
			//! Passphrase initial key
			void set_passphrase( const std::string& pass ) {
				m_passwd.push_back( pass );
			}
			//! Set wep keys
			void set_wep_keys( const std::string& k1, const std::string& k2,
					const std::string& k3, const std::string& k4 )
			{
				m_passwd.push_back( k1 );	
				m_passwd.push_back( k2 );	
				m_passwd.push_back( k3 );	
				m_passwd.push_back( k4 );	
			}

			const std::string& get_bssid() const {
				return m_bssid;
			}
			const std::string& get_ssid() const {
				return m_ssid;
			}
			auth get_auth() const {
				return m_auth;
			}
			algo get_algo() const {
				return m_algo;
			}
			const std::vector<std::string>& get_passwords() const {
				return m_passwd;
			}
			bool operator==(const medium_config & rhs) const
			{
				if (m_bssid == rhs.m_bssid && m_ssid == rhs.m_ssid && m_auth == rhs.m_auth && m_algo == rhs.m_algo && m_passwd.size() == rhs.m_passwd.size())
				{
					for (size_t i = 0; i < m_passwd.size(); ++i)
					{
						if (m_passwd[i] != rhs.m_passwd[i]) return false;
					}
					return true;
				}
				return false;
			}
		private:
			std::string m_bssid;
			std::string m_ssid;
			auth m_auth;
			algo m_algo;
			//Wep shared 4 keys
			std::vector<std::string> m_passwd;
		};

		//! WIFI info
		class medium_info {
		public:
			medium_info( const std::string& bssid, const std::string& ssid, int rssi, 
					const char* auth_mode, const char* algo_mode )
				: m_bssid(bssid), m_ssid(ssid), m_rssi(rssi), m_algo_desc(algo_mode), m_auth_desc(auth_mode)
			{
				str_to_mode( auth_mode, algo_mode );
			}
			medium_info( const std::string& bssid, const std::string& ssid, int rssi, 
					auth auth_mode=auth_unknown, algo algo_mode=algo_unknown)
				: m_bssid(bssid), m_ssid(ssid), m_rssi(rssi), m_algo(algo_mode), m_auth(auth_mode)
			{
				mode_to_str( auth_mode, algo_mode );
			}
			medium_info(): m_rssi(0), m_algo(algo_unknown), m_auth(auth_unknown)
			{
				mode_to_str( m_auth, m_algo );
			}

			const std::string& get_bssid() const {
				return m_bssid;
			}
			const std::string& get_ssid() const {
				return m_ssid;
			}
			int get_rssi() const {
				return m_rssi;
			}
			algo get_algo() const {
				return m_algo;
			}
			auth get_auth() const {
				return m_auth;
			}
			const std::string get_algo_desc() const {
				return m_algo_desc;
			}
			const std::string get_auth_desc() const {
				return m_auth_desc;
			}
		private:
			void str_to_mode( const char *auth, const char* algo );
			void mode_to_str( auth auth, algo algo );
		private:
			std::string m_bssid;	//! BSSID 
			std::string m_ssid;		//! SSID
			int m_rssi;				//! RSSI
			algo m_algo;		//! Malgo
			auth m_auth;		//! Mauth
			std::string m_algo_desc;		//! Algo description
			std::string m_auth_desc;		//! Auth description
		};

		//! Enable or disable bluetooth device
		int enable( bool enable );

		/** Gets available Access Points
		 *  @param[out] ap_list Vector of NETWORK structures is returned, one for each AP found
		 *  @return Error status
		 */
		int get_available_ap_list( const std::string& ifc_name, std::vector<medium_info> & ap_list );

		/** Connect to wifi network
		 *  @param[in] ap wifi AP config
		 *  @param[in] input interface config if null pointer DHCP will be used
		 *  @return Error status
		 */
		int connect ( const std::string& ifc_name, const medium_config& cfg, const interface_config* ifc );
		/** TODO: Add profile management structures and functions here!
		*/
		int get_profiles_count( const std::string& ifc_name, int& profiles_count );
		int find_profile(const std::string& ifc_name, const std::string &SSID, int& profile_id);
		int set_default_profile( const std::string& ifc_name, int profile_id );
		int get_default_profile( const std::string& ifc_name, medium_config& cfg );
		int get_default_profile_id( const std::string& ifc_name, int &profile_id);
		int add_profile( const std::string& ifc_name, const medium_config& cfg, int &profile_id );
		int connect( const std::string& ifc_name, int profile_id );

		//Wifi network profile task
		class network_profile {
		public:
			network_profile( const medium_config& cfg, int profile_id, bool isdefault )
				: m_cfg(cfg), m_profile_id(profile_id), m_isdefault(isdefault)
			{}
			network_profile(): m_profile_id(-1), m_isdefault(false)
			{}
			const medium_config& get_config() const {
				return  m_cfg;
			}
			int get_profile_id() const {
				return m_profile_id;
			}
			bool is_default() const {
				return m_isdefault;

			}
		private:
			medium_config m_cfg;
			int m_profile_id;
			bool m_isdefault;
		};
		int get_profile_info( const std::string& ifc_name, network_profile & prof, int profile_id = -1 );
		int get_profile_info( const std::string& ifc_name, std::vector<network_profile> & prof );


		int get_current_ap_info( const std::string& ifc_name, medium_info &ap );
		int delete_profile( const std::string& ifc_name, int profile_id );
/* ------------------------------------------------------------------ */ 
} } }



#endif   /* ----- #ifndef pml_net_INC  ----- */
