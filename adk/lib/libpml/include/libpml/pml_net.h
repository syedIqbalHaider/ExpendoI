/*
 * =====================================================================================
 *
 *       Filename:  pml_net.h
 *
 *    Description:  PML net interface
 *
 *        Version:  1.0
 *        Created:  27.02.2015 09:30:59
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lucjan Bryndza (LB), lucck
 *   Organization:  
 *
 * =====================================================================================
 */


#ifndef _COM_VERIFONE_PML_NET_H
#define _COM_VERIFONE_PML_NET_H

#include <string>
#include <vector>
#include <list>
#include "pml_net_data.hpp"

namespace com_verifone_pml {


	namespace net {
		namespace detail {

			std::string calc_broadcast( const std::string& addr, const std::string& netmask );
		}
		/** Configure network interface using non dhcp mode
		 *  if nullptr is assigned it use DHCP mode
		 * */
		int config_interface( const std::string & ifcn, const interface_config* ifc ) ;
		int config_interface( const interface_config* ifc );

		/** Checks if network interface is up
		 * */
		int is_interface_up( const std::string & ifcn );
		int is_interface_up();

		/** Brings network interface up
		 * */
		int interface_up( const std::string & ifcn );
		int interface_up();

		/** Brings network interface down
		 * */
		int interface_down( const std::string & ifcn );
		int interface_down();

		/** Gets  network interface configuration
		 * */
		int get_interface_config( const std::string & ifc_name, interface_config & ifc ) ;

		/** Enum available network interface
		* @param[out] if_names Interface list names
		* @return Network interface count
		*/
		int enum_interfaces ( std::list<std::string>& if_names );

		/** Function gets interface detailed information by name
		 * @param[in] ifc_name Interface input name
		 * @param[out] info Returned interface information
		 * @return Error code if successfull */
		int get_interface_info( const std::string& ifc_name, interface_info& info );

		/** Return the link interface state */
		enum link_state {
			link_state_up = 10,		//! Link is UP
			link_state_down			//! Link is Down
		};
		/** Function return if not local network interfcace is ready
		* and configured. It can be used for determine connection state
		* @param[in] Interface name. If name is null autodetection is made
		* @return if negative error or @see link_state for link status
		*/
		int link_is_up( const char *ifc_name = NULL );


	}	//namespace net

}
#endif   /* ----- #ifndef pml_net_INC  ----- */
