/******************************************************************************

  FILE: pml_os.h

  DESC: Platform-specific OS-specific functionality
        Defined here so that pml_abstracted_api.h is not cluttered with platform #ifdef-s

******************************************************************************/

#ifndef PML_OS_H
#define PML_OS_H



// Workaround for OS header issues - only works in case of C
#ifdef __cplusplus
#define __VAULT__
#define __KERNEL__

namespace com_verifone_pml
{
    namespace pml_os
    {
#endif

#ifdef VFI_PLATFORM_VOS
#include <linux/types.h>

// This defines list of counters as diag_counter_index_number enum
#include <verifone/diag_counters_API.h>
#endif

struct OS_COUNTER {
    char name[20];
    int value;
    unsigned int flags;
    unsigned int group;
};

#ifdef __cplusplus
extern "C" {
#endif

// Place C functions here

int get_counter(int index, struct OS_COUNTER * counter);
int is_counter_defined(int index);
int is_custom_counter_defined(int index, const char * name);
int set_counter(int index, const struct OS_COUNTER * counter);
int increase_counter(int index, int count);
inline int increment_counter(int index)
    { return increase_counter(index, 1); }
int decrease_counter(int index, int count);
inline int decrement_counter(int index)
    { return decrease_counter(index, 1); }

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus
#undef __KERNEL__
#undef __VAULT__
    } // namespace pml_os
} // namespace com_verifone_pml
#endif


#endif // PML_OS_H
