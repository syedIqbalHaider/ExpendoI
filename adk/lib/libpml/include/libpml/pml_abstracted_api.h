#ifndef _COM_VERIFONE_PML_ABSTRACTED_API_H
#define _COM_VERIFONE_PML_ABSTRACTED_API_H


#include <libpml/pml_os_core.h>
#include <libpml/pml_net.h>
#include <libpml/pml_net_bluetooth.h>
#include <libpml/pml_net_wifi.h>
#include <libpml/pml_net_gsm.h>

#endif // _COM_VERIFONE_PML_ABSTRACTED_API_H
