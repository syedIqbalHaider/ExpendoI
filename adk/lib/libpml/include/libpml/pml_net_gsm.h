/*
 * =====================================================================================
 *
 *       Filename:  pml_net_gsm.h
 *
 *    Description:  PML GSM modems abstration layer
 *
 *        Version:  1.0
 *        Created:  27.02.2015 11:49:16
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lucjan Bryndza (LB), lucck
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef  _COM_VERIFONE_GSM_pml_net_gsm_H
#define  _COM_VERIFONE_GSM_pml_net_gsm_H 
/* ------------------------------------------------------------------ */ 
#include <libpml/pml_net_data.hpp>
/* ------------------------------------------------------------------ */ 
namespace com_verifone_pml {
namespace net {
namespace gsm {
/* ------------------------------------------------------------------ */ 
//! Authentication mode 
	enum auth_mode {
		auth_auto,
		auth_pap,
		auth_chap
	};
/* ------------------------------------------------------------------ */ 
//! Medium configuration structure
struct medium_config {
public:
	medium_config( const std::string& apn, const std::string& phoneno=std::string(), 
			const std::string& username=std::string(), const std::string& passwd=std::string(), 
			auth_mode auth = auth_auto ):
				m_auth(auth)
				, m_apn(apn)
				, m_username(username)
				, m_passwd(passwd)
				, m_phoneno(phoneno)
	{
	}
	auth_mode get_auth() const {
		return m_auth;
	}
	const std::string& get_apn() const {
		return m_apn;
	}
	const std::string& get_username() const {
		return m_username;
	}
	const std::string& get_passwd() const {
		return m_passwd;
	}
	const std::string& get_phoneno() const {
		return m_phoneno;
	}
private:
	auth_mode m_auth;
	std::string m_apn;
	std::string m_username;
	std::string m_passwd;
	std::string m_phoneno;
};
/* ------------------------------------------------------------------ */ 
/**  Register to gsm network and enable GSM module
 *  @param[in] pin Pin for network registration
 *  @return Gprs error code < 0 
 */
int register_to_network( const std::string &pin );
/* ------------------------------------------------------------------ */ 
/** Deregister from GSM network and disable module
 * @return gprs error code < 0 
 */
int deregister_from_network();
/* ------------------------------------------------------------------ */ 
/** 
 * Connect to gsm network using GSM module 
 * @param[in] cfg Medium configuration
 * @param[in] interface configuration if none automatic DHCP mode will be used
 * @return error code if failed system code or gsm error returned
 */
int connect( const medium_config& cfg, const interface_config* ifc = NULL ); 
/* ------------------------------------------------------------------ */ 
/** Disconnect from the GSM modem network 
 * @return error code if failed system code or gsm error code returned
 */
int disconnect( );

/* ------------------------------------------------------------------ */ 
/** Function return signal strength or error if failed
 * @return signal strength or error code if failed
 */
int get_signal_strength();

/* ------------------------------------------------------------------ */ 
}}}
/* ------------------------------------------------------------------ */ 
#endif   /* ----- #ifndef pml_net_gsm_INC  ----- */
