/******************************************************************************

  FILE: ux.h

  DESC: Vx600 device handling functions
  
******************************************************************************/

#ifndef VX600_H
#define VX600_H

#include <string>

namespace pml600
{
	static const int RES_BARCODE_TIMEOUT = 0;
	static const int RES_BARCODE_NOOPERATION = 1;
	static const int RES_BARCODE_NOMEMORY = 2;
	static const int RES_BARCODE_DATAREADY = 3;

	class vx600
	{
		public:
			vx600();
			~vx600();
			bool is_vx600()
				{ return initialized; }

			/*
			 * Enables keyboard for Vx600
			 */
			void enableKeyboard();
			void disableKeyboard();
			int BarCodeOpen();
			void BarCodeClose();
			int BarCodeRead(std::string & buf);
			void BarCodeSetMonitoringMode(bool val);
			bool BarCodeIsOpened();
			bool BarCodeIsMonitoringMode();
			void BarCodeBeep(int B1Freq, int B1Dur, int Pause, int B2Freq, int B2Dur);

		private:
			bool initialized;
	};
}

#endif


