/******************************************************************************

  FILE: pml_os_security.h

  DESC: Platform-specific OS functionality - Needham-Schroeder security, PIN passing
        Defined here so that pml_abstracted_api.h is not cluttered with platform #ifdef-s

******************************************************************************/

#ifndef PML_OS_SECURITY_H
#define PML_OS_SECURITY_H

#include <string>

namespace com_verifone_pml
{
	namespace needham_schroeder {
		namespace {
			// Channel is stored on bits 28-29
			const int CHANNEL_0 = 0 << 28;
			const int CHANNEL_1 = 1 << 28;
			const int CHANNEL_2 = 2 << 28;
			const int CHANNEL_3 = 3 << 28;

			const int USE_KLD_KEY = 1 << 24;

			// Encryption mode is stored on bit 0
			const int KEY_ENCR_MODE_DES_ECB = 0;
			const int KEY_ENCR_MODE_DES_CBC = 1;
		}

		int init(int mode, std::string & output);
		int resolve(int mode, const std::string & input, std::string & output);
		int finalize(int mode, const std::string & input, std::string & output);
		int verify(int mode, const std::string & input);

		enum OPERATION_TYPE {
			OPER_PIN = 0,
			OPER_PASSWORD = 1
		};
		int get_session_key(int mode, OPERATION_TYPE key_type, std::string & key);
		inline int get_pin_session_key(int mode, std::string & key)
			{ return get_session_key(mode, OPER_PIN, key); }
		inline int get_password_session_key(int mode, std::string & key)
			{ return get_session_key(mode, OPER_PASSWORD, key); }
		int load_encrypted_block(int mode, OPERATION_TYPE key_type, const std::string & block);
		inline int load_encrypted_pin_block(int mode, const std::string & block)
			{ return load_encrypted_block(mode, OPER_PIN, block); }
		inline int load_encrypted_password_block(int mode, const std::string & block)
			{ return load_encrypted_block(mode, OPER_PASSWORD, block); }
		int ux_set_ars_password(int mode, int password_id, const std::string & password);
		int ux_reset_ars(int mode, int password_id, const std::string & password);

		int get_certificate(int pki_type, int cert_level, std::string & cert);
		int store_certificate(int pki_type, int cert_level, const std::string & cert);
		int clear_certificate_chain(int pki_type);
		inline int get_verifone_certificate(int cert_level, std::string & cert)
			{ return get_certificate( 0, cert_level, cert); }
		inline int store_user_certificate(int cert_level, const std::string & cert)
			{ return store_certificate( 1, cert_level, cert ); }
		inline int clear_user_certificate_chain()
			{ return clear_certificate_chain( 1 ); }
		inline int init_user_certificate_chain()
			{ return store_certificate( 1, 0, std::string() ); }


	} // namespace needham_schroeder
} // namespace com_verifone_pml

#endif // PML_OS_SECURITY_H
