/*
 * =====================================================================================
 *
 *       Filename:  safe_string.h
 *
 *    Description:  Safe string to remove later
 *
 *        Version:  1.0
 *        Created:  04.03.2015 11:36:29
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lucjan Bryndza (LB), lucck
 *   Organization:  
 *
 * =====================================================================================
 */
//TODO: To be removed later ( normal string instead of safe string )
//TODO: It should be defined as std::string with custom allocator

#ifndef  _COM_VERIFONE_PML_safe_string_INC
#define  _COM_VERIFONE_PML_safe_string_INC
#include <string>
namespace com_verifone_pml {


	class safe_string
	{
		public:
			explicit safe_string() {
				ptr = new std::string();
			}
			explicit safe_string( std::string & s ) {
				ptr = new std::string(s);
			}
			~safe_string() {
				this->clear();
				delete ptr;
			}
			void clear() {
				//BOOST_FOREACH( std::string::value_type &v, *ptr ) {
				for (std::string::iterator it = ptr->begin(); it != ptr->end(); ++it) {
					*it = 0;
				}
				ptr->clear();
			}

			operator std::string() const {
				return *ptr;
			}

			std::string & operator=(const std::string &out) {
				ptr->assign(out);
				return *ptr;
			}
			std::string & operator*() {
				return *ptr;
			}
			std::string * operator->() {
				return ptr;
			}
			const std::string & operator*() const {
				return *ptr;
			}
			const std::string * operator->() const {
				return ptr;
			}
		private:
			// noncopyable
			safe_string( const safe_string& other );
		private:
			std::string * ptr;
	};

}


#endif   /* ----- #ifndef safe_string_INC  ----- */
