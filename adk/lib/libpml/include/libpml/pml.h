#ifndef _COM_VERIFONE_PML_PML_H
#define _COM_VERIFONE_PML_PML_H

#include <vector>
#include <string>
#ifndef VERIXV
	#include <sys/epoll.h>
#else

char ToUpper (char c);
char ToLower (char c);


enum EPOLL_EVENTS
{
#define EPOLLIN 0
#define EPOLLPRI 0
#define EPOLLOUT 0
#define EPOLLRDNORM 0
#define EPOLLRDBAND 0
#define EPOLLWRNORM 0
#define EPOLLWRBAND 0
#define EPOLLMSG 0
#define EPOLLERR 0
#define EPOLLHUP 0
#define EPOLLRDHUP 0
#define EPOLLONESHOT 0
#define EPOLLET 0
};


typedef unsigned long pid_t;

#endif

namespace com_verifone_ipc
{
	class ipc_handle;
}


namespace com_verifone_pml
{
	struct events
	{
		enum evt
		{
			cascade,		/* Cascade poll event */
			clock,			/* Clock event */
			comm,			/* Communication event */
			timer,			/* User timer event */
			ipc,			/* IPC event */
			keyboard,		/* KBD event */
			icc,			/* ICC event */
			mag,			/* Magstripe event */
			touch,			/* Touch event */
			case_removal,	/* Case removal event (unattended devices only) */
			usb_device,		/* USB device connected / disconnected */
			dock,			/* Terminal dock/undock event (battery powered devices only) */
			barcode,		/* Bar code reader event */
			shutdown,		/* Shutdown event (battery powered devices only */
			switch_sys,		/* Sys switch event */
			netlink,		/* Network link */
			/* Verix eVo only events */
			activate,		/* Activate event, transfers console between tasks */
							// Add Verix specific events here
			/* VOS only events */
							// Add VOS specific events here
			/* System events */
			battery,		/* Battery status reporting for battery terminals */
			tamper,		/* Tamper event */
			switch_tripped,		/* Removal switch tripped (unattended only) */
			signal_strength, //! Signal strength WIFI/BT/GPRS etc
			//Add system events here
			user0,			// User events
			user1,
			user2,
			user3,
			user4,
			user5,
			user6,
			user7,
			user8,
			user9,
			user10,
			user11,
			user12,
			user13,
			user14,
			user15,
			end_of_events
		};
	};

	static const long INFINITE_TIMEOUT = -1;

	typedef events::evt event_id_t;

	// Comm item object, used for internal and external comms
	struct event_com_item
	{
		static const int any = -1;
		int fd;
		unsigned long flags;	//Especially flags from the EPOLL (OS specific)
	};
	//Cascade event
	struct event_cascade
	{
		int fd;
	};
	//Battery event
	static const int BATT_CHARGING = 1;
	static const int BATT_DOCKED = 2;
	struct event_battery
	{
		int chg_percent;		// input only; battery percent difference to trigger the event
		int percent;		// output only; current battery percentage
		int status;		// output only; can have BATT_CHARGING / BATT_DOCKED flags
	};
	struct signal_strength {
		int value;				//INPUt output on create treshold value on update signal percent
	};
	//Tamper event
	struct event_tamper
	{
		int is_tampered;		// output - if nonzero, the device is tampered
	};
	struct event_timer
	{
		long id;							//Timer ID
		unsigned long num_expires;			//Missed events
	};
	// User event
	struct user_event_item
	{
		unsigned long flags;
		unsigned long ext_flags;
	};
	//Net link event item
	struct event_netlink
	{
		int _fd;				//!Handle intifier
		int n_ifcs;				//Number of intefaces changed
		struct ifcinfo {
			char name[32];			//!Network interface name
			bool isup;				//!Link is UP
		} ifc[4];					//!One message can contains few evts
	};
	//! Sys switch 
	struct event_sysswitch 
	{
		int _fd;
	};
	//Version 1 event item t
	struct event_item
	{
		events::evt event_id;
		union evt_t
		{
			event_com_item com;
			user_event_item user;
			event_cascade cascade;
			event_battery batt;
			event_timer tmr;
			event_tamper tamper;
			event_netlink netlink;
			event_sysswitch sysswitch;
			signal_strength signal;
		} evt;
		// event_item(event_id_t t): event_id(t) {}
	};
	typedef std::vector <event_item> eventset_t;

	struct ctl
	{
		enum op_t 
		{
			ADD,
			DEL
		};
	};

	/* Return codes -1 when failed and errno */
	/* 
	* Function initializes PML library
	*/
	int pml_init(const char * name);
	int pml_deinit(const char * name);
	
	/* Return codes -1 when failed and errno */
	/* 
	* Function creates internal object, which manages events
	*/
	int event_open( );
	
	/*
	* Destroys internal object, associated with handle
	*/
	int event_close(int handle);
	
	/*
	* Adds / removes / updates events, associated with handle
	*/
	int event_ctl( int handle, const ctl::op_t operation, const event_item & event );
	int event_ctl( int handle, const ctl::op_t operation, const eventset_t & events );
	int event_ctl( int handle, const ctl::op_t operation, const com_verifone_ipc::ipc_handle& ipchwnd );
	/*
	* As above, but works with second object, represented by add_handle
	* Object related to add_handle is always untouched.
	* ADD combines events from both object and puts them in first one, associated with handle. 
	*     Handle object will consists of events present in both objects. 
	* MOD updates events present in object associated to handle so that they match those from add_handle.
	*     Events present in object add_handle but absent in object handle are not copied
	* DEL deletes events present in object add_handle from object handle. Other events are left untouched.
	*/
	int event_ctl( int handle, const ctl::op_t operation, int add_handle );
	
	/*
	* Waits for events, represented by object associated with handle. 
	* Timeout: 0 for read event, -1 - wait forever, positive value - wait amount of time in milliseconds
	*/
	int event_wait( int handle, eventset_t & revents , long timeout = INFINITE_TIMEOUT );
	inline int event_read( int handle, eventset_t & revents )
		{ return event_wait(handle, revents, 0); }

	/* Raise an user event and OS specific event
	 * @param[in] handle Input handle
	 * @param[in] event Event handle input
	 * @return error code
	 */
	int event_raise( const char *name, const event_item & event );
	int event_raise( pid_t tid, const event_item & event );
	
	
	/* Create new timer
	 * @param[in] timeout Timeout in milliseconds
	 * @param[in] repeat Boolean flag, configuring repeatable timer
	 * @return timer ID
	 */
	int set_timer( event_item & event, long milliseconds, bool repeat = false );
	
	/* Checks timer status
	 * @param[in] timer_id Timer id, returned by set_timer
	 * @return 0 if timer pending, > 0 number of timer expirations
	 */
	int check_timer( const event_item & timer_id );
	/* Clears (cancels) previously created timer
	 * @param[in] timer_id Timer id, returned by set_timer
	 * @return true on success
	 */
	int clr_timer(event_item & timer_id);
	
	/* Change timer settings
	 * @param[in] timeout Timeout in milliseconds
	 * @param[in] repeat Boolean flag, configuring repeatable timer
	 * @return timer ID
	 */
	int change_timer( event_item &event, long milliseconds, bool repeat = false );
	
	
	
	namespace appver
	{
		/* Application list type */
		typedef std::vector<std::pair<std::string, std::string > > applist_t;
		
		/**
		 * Add application  to the app list
		 * @param[in] version application version
		 * @return PML error code
		 */
		int add_app_version( const char version[] );
		/**
		 * Add library to the app
		 * @param[in] libname application name 
		 * @param[in] version application version
		 */
		void register_library( const char libname[], const char version[] );
		
		
		/** 
		 * Get application list 
		 * @param[out] apps application list*
		 */
		void get_apps_list( applist_t &apps );
		
		/**
		 * Get libraries list
		 * @param[out] liblist libraries list Input applicatoin name 
		 * @param[in]  appname Input applicatoin name
		 * @return PML return code status
		 */
		void get_lib_list( applist_t& liblist, const char *appname = NULL );
		
		/**
		 * Get current application version 
		 * @return  Application version 
		 */
		std::string get_app_version();
	}

	/** Infinite timeout */
	static const int time_infinite = -1;

	/** 
	 * Notify application launcher to complete initialization 
	 * @return PML error code status 
	 */
	int app_notify_initialization_complete();

	/**
	 * Add notify that all apps are initialized
	 */
	int app_wait_for_all( int timeout = time_infinite );

	/** Network PML part  */
	namespace net {
		
		/** Create netif listener for network events
		 *  @param[in,out] netif_event Netif event for listen
		 *  @return Error status
		 */
		int netif_events_listener_create( event_item& netif_event );
		/** Destroy netif listener for network events
		 *  @param[in,out] netif_event Netif event for listen
		 *  @return Error status
		 */
		int netif_events_listener_destroy( event_item& netif_event );
	}
	namespace latch {
		/** PML special security devices
		* @param[in,out] latch_event Latch event object
		* @return Error status
		*/
		int sysswitch_events_listener_create( event_item& event );

		/** Destrroy latch object
		 * @param[in,out] latch_event Latch object 
		 * @return Error status
		 */
		int sysswitch_events_listener_destroy( event_item& event );
		
	}
}
#endif

