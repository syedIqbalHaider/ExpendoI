#ifndef _COM_VERIFONE_PML_PORT_H
#define _COM_VERIFONE_PML_PORT_H

#ifdef VERIXV
#include <svc.h>
#include <svc_sec.h>

#else

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>

#include <liblog/logsys.h>

#include <fcntl.h>
#include <sys/sendfile.h>
#include <sys/stat.h>
#include <sys/types.h>

inline int get_group() {return 0;}
inline int set_group(int group_id) {return group_id;}

inline void play_RTTTL(const char * /*music*/) { fprintf(stderr, "play_RTTTL not implemented\n");}
inline int kbd_pending_count (void){ fprintf(stderr, "kbd_pending_count not implemented\n"); return 0;}

inline int get_font (char * /*font_name*/) { fprintf(stderr, "get_font not implemented\n"); return 0;}
inline int set_font (const char * /*font_name*/) { fprintf(stderr, "set_font not implemented\n"); return 0;}

inline int setcontrast (int /*flag*/) { fprintf(stderr, "setcontrast not implemented\n"); return 0;}
inline int getcontrast (void) { fprintf(stderr, "getcontrast not implemented\n"); return 0;}

inline int get_task_id (void) {return getpid();}

inline int get_console (int /*clear_keys*/) {return 0;}
inline int   get_owner (const char * /*id*/, int *task_id)  {*task_id = getpid(); return 0;}
inline int   set_owner (int /*handle*/, int /*task_id*/) {return 0;}
inline int activate_task (int /*task_id*/){ fprintf(stderr, "activate_task not implemented\n"); return 0;}

inline int  SVC_WAIT (unsigned int msec) {return usleep(msec*1000);}
inline int SVC_RESTART (const char * /*file*/) { fprintf(stderr, "SVC_RESTART not implemented\n"); return 0;}

inline int gotoxy (int /*x*/,int /*y*/){ fprintf(stderr, "gotoxy not implemented\n"); return 0;}
#undef clrscr
inline int clrscr (void){ fprintf(stderr, "clrscr not implemented\n"); return 0;}
inline int setscrollmode (int /*mode*/){ fprintf(stderr, "setscrollmode not implemented\n"); return 0;}

inline int set_display_color(int /*type*/, int /*color*/){ fprintf(stderr, "set_display_color not implemented\n"); return 0;}
inline int get_display_color(int /*type*/){ fprintf(stderr, "get_display_color not implemented\n"); return 0;}
inline int set_display_coordinate_mode(int /*mode*/){ fprintf(stderr, "set_display_coordinate_mode not implemented\n"); return 0;}
inline int get_display_coordinate_mode(void){ fprintf(stderr, "get_display_coordinate_mode not implemented\n"); return 0;}
inline int window (int /*startX*/, int /*startY*/, int /*endX*/, int /*endY*/){ fprintf(stderr, "window not implemented\n"); return 0;}
#define TICKS_PER_SEC 1000
//inline unsigned long read_ticks (void) {return ((long long)clock())*TICKS_PER_SEC/CLOCKS_PER_SEC;}
inline unsigned long read_ticks(void)
{
 struct timeval tv; 
 if (gettimeofday(&tv, 0) == 0) return tv.tv_sec * TICKS_PER_SEC + tv.tv_usec / TICKS_PER_SEC; 
 else return 0;
}
inline int get_battery_sts (void) { fprintf(stderr, "get_battery_sts not implemented\n"); return -1;}
inline int get_battery_value (int /*type*/)  {fprintf(stderr, "get_battery_value not implemented\n"); return -1;}
inline int BatteryRegs (char* /*buf*/)  {fprintf(stderr, "BatteryRegs not implemented\n"); return -1;}

inline int get_dock_sts (void)  { fprintf(stderr, "get_dock_sts not implemented\n"); return 0;}

/* Color Type definitions for SET_DISPLAY_COLOR and GET_DISPLAY_COLOR */
#define FOREGROUND_COLOR    0
#define BACKGROUND_COLOR    1
#define   WHITE               0
#define   LIGHTGRAY           2
#define   DARKGRAY            3
#define   BLACK               1
#define CURRENT_PALETTE     2
#define   RGB                 1
#define   MONOCHROME          2
#define   GRAY4               4
#define AVAILABLE_PALETTE   3

/* Mode constants for set_display_coordinate_mode() and get_display_coordinate_mode() */
#define CHARACTER_MODE      0
#define PIXEL_MODE          1


/* Event Codes returned by wait_event() */
#define EVT_KBD         (1L<<0 )    /* keyboard input */
#define EVT_CONSOLE     (1L<<1 )    /* display output complete */
#define EVT_CLK         (1L<<2 )    /* one second clock ticks */
#define EVT_TIMER       (1L<<3 )    /* user-defined timer */
#define EVT_PIPE        (1L<<4 )    /* pipe I/O */
#define EVT_SOKT        (1L<<5 )    /* socket I/O */
#define EVT_NETWORK     (1L<<5 )    /* compatibility */
#define EVT_COM1        (1L<<6 )    /* com 1 I/O */
#define EVT_COM2        (1L<<7 )    /* com 2 I/O */
#define EVT_COM3        (1L<<8 )    /* com 3 I/O */
#define EVT_COM4        (1L<<9 )    /* com 4 I/O */
#define EVT_COM5        (1L<<10)    /* com 5 I/O */
#define EVT_COM8        (1L<<11)    /* com 8 I/O */
#define EVT_MAG         (1L<<12)    /* mag card swipe */
#define EVT_ICC1_INS    (1L<<13)    /* customer smart card I/O */
#define EVT_USB_CLIENT   (1L<<15)    /* USB-slave events */
#define EVT_ACTIVATE    (1L<<16)    /* now can use console handle */
#define EVT_DEACTIVATE  (1L<<17)    /* now cannot use console handle */
#define EVT_BAR         (1L<<18)    /* Barcode */
#define EVT_ICC1_REM    (1L<<19)    /* customer smart card removed */
#define EVT_REMOVED     (1L<<20)    /* UPT case removal event */
#define EVT_IFD_READY   (1L<<21)    /* smart card response available */
#define EVT_IFD_TIMEOUT (1L<<22)    /* smart card response timed out */
#define EVT_USB         (1L<<23)    /* any (usually, "the") USB device */
#define EVT_COM6        (1L<<24)    /* com 6 I/O */
#define EVT_COM7        (1L<<25)    /* com 7 I/O */
#define EVT_WLN         (1L<<26)    /* Wireless LAN WiFi */
#define EVT_SDIO        (1L<<27)    /* SD/SDIO device */
#define EVT_BIO         (1L<<28)    /* MSO300 biometric device */
#define EVT_USER        (1L<<29)    /* post_user_event summary bit */
#define EVT_SHUTDOWN    (1L<<30)    /* powering off soon: all awake! */
#define EVT_SYSTEM      ((long)(1UL<<31)) /* dock, unzip completion, ... */

/* get_battery_value arguments */
#define FULLCHARGE      0
#define REMAININGCHARGE 1
#define BATTERYVOLTAGE  2
#define BATTERYAVAIL    3
#define BATTERYTEMP     9
#define BATTERYCURRENT 10
#define CHARGERSTATUS  13
/* set_battery_value argument */
#define LOWBATTLEVEL   11

/* Rate Codes for Opn_Blk.rate */
#define Rt_300      0
#define Rt_600      1
#define Rt_1200     2
#define Rt_2400     3
#define Rt_4800     4
#define Rt_9600     5
#define Rt_19200    6
#define Rt_38400    7
#define Rt_57600    8
#define Rt_115200   9
#define Rt_12000    10
#define Rt_14400    11
#define Rt_28800    12
#define Rt_33600    13
#define Rt_MAX      13  /* highest code, not highest baud rate! */
#define Rt_user_defined 0xFF    // Specify actual rate in trailer.words.w[0]

/* Format codes for Opn_Blk.format */
#define Fmt_A7E1    0       /* async, 7 data bits, even parity, one stop */
#define Fmt_A7N1    1       /* async, 7 data bits,   no parity, one stop */
#define Fmt_A7O1    2       /* async, 7 data bits,  odd parity, one stop */
#define Fmt_A8E1    3       /* async, 8 data bits, even parity, one stop */
#define Fmt_A8N1    4       /* async, 8 data bits,   no parity, one stop */
#define Fmt_A8O1    5       /* async, 8 data bits,  odd parity, one stop */
#define Fmt_A9N1    6       /* async, 9 data bits,   no parity, one stop, requires MDB dongle */
#define Fmt_SDLC    7       /* SDLC,  8 data bits,   no parity           */
#define Fmt_2stp    0x80    /* (SIO) Add, if two stop bits desired       */
#define Fmt_AFC     0x40    /* True AFC RTS not accessable by application*/
#define Fmt_auto    0x20    /* (SIO) Add, if Auto-enables for flow ctl   */
#define Fmt_DTR     0x10    /* (SIO) Add, if DTR to be asserted          */
#define Fmt_RTS     0x08    /* (SIO) Add, if RTS to be asserted          */

#define P_char_mode      0

/* Account Data Encryption */

// ADE Encryption Algorithms
#define ADE_ALG_TDEA    0

// ADE Encryption Modes of Operation
#define ADE_MODE_ECB    0
#define ADE_MODE_CBC    1

// ADE IV Settings
#define ADE_IV_NONE     0
#define ADE_IV_ZERO     1
#define ADE_IV_RAND     2

// ADE Padding Schemes
#define ADE_PAD_NONE    0
#define ADE_PAD_PKCS7   1
#define ADE_PAD_X923    2
#define ADE_PAD_ISO7816 3

// ADE Return Codes
#define ADE_SUCCESS 0
#define ADE_ERR_PM_PTR  -1
#define ADE_ERR_PM_LEN  -2
#define ADE_ERR_PM_KEY  -3
#define ADE_ERR_PM_ALG  -4
#define ADE_ERR_PM_MODE -5
#define ADE_ERR_PM_IV   -6
#define ADE_ERR_PM_PAD  -7
#define ADE_ERR_NO_KEY  -11
#define ADE_ERR_OFF     -12
#define ADE_ERR_GENERIC -99

// ADE Status Codes (bitmap)
#define ADE_STS_ENG0    1<<0
#define ADE_STS_ENG1    1<<1
#define ADE_STS_ENG2    1<<2
#define ADE_STS_ENG3    1<<3
#define ADE_STS_ENG4    1<<4
#define ADE_STS_ENG5    1<<5
#define ADE_STS_ENG6    1<<6
#define ADE_STS_ENG7    1<<7
#define ADE_STS_ENG8    1<<8
#define ADE_STS_ENG9    1<<9
#define ADE_STS_ON      1<<10
#define ADE_STS_ENABLED 1<<11
#define ADE_STS_ENG_ALL 0x03FF

#endif

#endif
