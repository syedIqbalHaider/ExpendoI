/* ---------------------------------------------------------------- */
/*
 * pml_task_mngr.cpp
 *
 *  Created on: 13-03-2013
 *      Author: lucck
 */

/* ---------------------------------------------------------------- */

#include "pml_task_mngr.hpp"
#include <cstring>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include <boost/function.hpp>
#ifndef NDEBUG
#include <boost/foreach.hpp>
#include <iostream>
#endif
#include <liblog/logsys.h>

/* ---------------------------------------------------------------- */
namespace com_verifone_pml {

pid_t gettid();


namespace internal {
/* ---------------------------------------------------------------- */
namespace ipc = boost::interprocess;


/* ---------------------------------------------------------------- */
namespace
{
	//IPC task list shared memory segment list
	const char ipc_name[] = "com_verifone_pml_tasks_list";
}
/* ---------------------------------------------------------------- */
//Constructor
pml_task_mngr::pml_task_mngr()
	:  m_shm( ipc::open_or_create, ipc_name,
	   C_max_tasks * sizeof( value_type ) + C_header_size )
	,  m_shm_allocator(m_shm.get_segment_manager())
	,  m_tasks( m_shm.find_or_construct<shared_map_t>("TasksList")( comparable_type() ,m_shm_allocator) )
{

}

/* ---------------------------------------------------------------- */
//Check if task is alive and exists
bool pml_task_mngr::exists( const value_type &v ) const
{
	//this line is crashing!!
	//bool isalive = pthread_kill(v.second.pid, 0) != ESRCH;
//	dlog_msg("pml_task_mngr::eists pid=%ld isalive=%d", v.second.pid, isalive);
	bool isalive = true;
	return isalive;
}

/* ---------------------------------------------------------------- */
//Insert the task into the queue
void pml_task_mngr::insert( const char* name )
{
	app_info info = { gettid(),"", "" };
	std::strncpy( info.name, name, sizeof info.name );
	(*m_tasks)[ name ] = info; 
}
/* ---------------------------------------------------------------- */
//Insert version to the task
int pml_task_mngr::insert_version( const char* name, const char* version )
{
	iterator item = find(name);
	if( item != end() )
	{
		std::strncpy( item->second.ver, version, sizeof app_info::ver ); 
        item->second.ver[ sizeof app_info::ver - 1 ] = '\0';
        std::strncpy( item->second.name, name, sizeof app_info::name );
        item->second.name[ sizeof app_info::name - 1 ] = '\0';
		return 0;
	}
	else
	{
		return -1;
	}
}
/* ---------------------------------------------------------------- */
#ifndef NDEBUG
	//Print registered tasks
void pml_task_mngr::print_tasks( ) const
{
	BOOST_FOREACH( const shared_map_t::value_type &v, *m_tasks )
	{
		std::cout << std::string(v.first) << " = [ PID: " << v.second.pid << " VER: "<< v.second.ver << " ]\n";
	}
}
#endif
/* ---------------------------------------------------------------- */
} /* namespace internal */
} /* namespace com_verifone_pml */

/* ---------------------------------------------------------------- */
