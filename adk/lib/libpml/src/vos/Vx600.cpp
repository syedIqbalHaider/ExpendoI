#include <svc.h>

#include <liblog/logsys.h>

#include <libpml/vx600.h> // pml header

namespace pml600
{
	vx600::vx600(): initialized(false)
	{
		// nothing
	}

	vx600::~vx600()
	{
		// nothing
	}
	
	void vx600::enableKeyboard()
	{
		// nothing
	}

	void vx600::disableKeyboard()
	{
		// nothing
	}

	/************************************************************************************************************************************************/
	/* Vx600 barcode reader */
	/************************************************************************************************************************************************/
	int vx600::BarCodeOpen()
	{
		return RES_BARCODE_NOOPERATION;
	}

	void vx600::BarCodeClose()
	{
		// nothing
	}

	int vx600::BarCodeRead(std::string & /*buf*/)
	{
		return RES_BARCODE_NOOPERATION;
	}

	void vx600::BarCodeSetMonitoringMode(bool /*val*/)
	{
		// nothing
	}
	
	bool vx600::BarCodeIsOpened()
	{
		return false;
	}
	
	bool vx600::BarCodeIsMonitoringMode()
	{
		return false;
	}
	
	void vx600::BarCodeBeep(int /*B1Freq*/, int /*B1Dur*/, int /*Pause*/, int /*B2Freq*/, int /*B2Dur*/)
	{
		// nothing
	}

} //namespace

