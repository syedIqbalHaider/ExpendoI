/*
 * =====================================================================================
 *
 *       Filename:  wpa_supplicant_generator.cpp
 *
 *    Description:  WPA supplicant config generator
 *
 *        Version:  1.0
 *        Created:  16.02.2015 12:24:32
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lucjan Bryndza (LB), lucck
 *   Organization:  
 *
 * =====================================================================================
 */
#include "wpa_supplicant_generator.hpp"
#include <libpml/pml_abstracted_api.h>
#include <liblog/logsys.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fstream>
/* ------------------------------------------------------------------ */ 
namespace com_verifone_pml {
namespace net {
namespace wifi {
namespace detail {
/* ------------------------------------------------------------------ */
namespace {
namespace wpa {
/* ------------------------------------------------------------------ */ 
//! Generate plain encrypted file
void plain( std::ofstream& f )
{
	f << "\tkey_mgmt=NONE" << std::endl;
}
/* ------------------------------------------------------------------ */
void wpa_psk( std::ofstream& f, const medium_config& wcfg )
{
	f << "\tkey_mgmt=WPA-PSK IEEE8021X" << std::endl;
	f << "\tpairwise=CCMP TKIP" << std::endl;
	f << "\tgroup=CCMP TKIP" << std::endl;
	auto& pass = wcfg.get_passwords();
	if( pass.size() > 0 ) {
		f << "\tpsk=\"" << pass[0] << "\"" << std::endl;
	}
}
/* ------------------------------------------------------------------ */ 
void wep( std::ofstream& f, const medium_config& wcfg )
{
	f << "\tkey_mgmt=NONE" << std::endl;
	auto& pass = wcfg.get_passwords();
	for( std::size_t s=0; s<pass.size(); ++s ) {
		f << "\twep_key" << s << "=" << pass[s] << std::endl;
	}
}
/* ------------------------------------------------------------------ */ 
}}
/* ------------------------------------------------------------------ */ 
//! Generate WPA supplicant file based on the initial input configutation
int wpa_supplicant_generator( const std::string& ifc_name, 
		const medium_config& cfg, const char* outfile )
{	
	umask(022);
	using ffl = std::ofstream;
	std::ofstream of( outfile, ffl::out|ffl::trunc );
	if( !of.good() ) {
		return -1;
	}
	//Generate header
	of << "ctrl_interface=/var/run/wpa_supplicant" << std::endl;
	of << "update_config=1\n" << std::endl;
	of << "network={" << std::endl;
	of << "\tssid=\"" << cfg.get_ssid() << "\"" << std::endl;
	if( !cfg.get_bssid().empty() ) {
		of << "\tbssid=" << cfg.get_bssid() << std::endl;
	}
	switch( cfg.get_auth() ) {
	case auth_none:
		wpa::plain( of ); 
		break;
	case auth_wep:
		wpa::wep( of, cfg );
		break;
	case auth_wpa_psk:
	case auth_wpa:
	case wpa_none:
	case auth_wpa2_psk:
		wpa::wpa_psk( of, cfg );
		break;
	default:
	case auth_wep_shared:
		dlog_error("WIFI unsuported auth mode %d", cfg.get_auth() );
		errno = EINVAL;
		return -1;
	}
	of << "}" << std::endl;
	return 0;
}
/* ------------------------------------------------------------------ */ 
}}}}
