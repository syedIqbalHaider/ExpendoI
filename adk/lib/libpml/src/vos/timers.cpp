/*
 * timers.cpp
 *
 *  Created on: 29-03-2013
 *      Author: lucck
 */
/* ---------------------------------------------------------------- */
#include <sys/timerfd.h>
#include <time.h>
#include <unistd.h>
#include <poll.h>
#include <cerrno>
#include "libpml/pml.h"

/* ---------------------------------------------------------------- */
namespace com_verifone_pml {
/* ---------------------------------------------------------------- */
namespace {
namespace _internal {
/* ---------------------------------------------------------------- */
	//Miliseconds constants
	const long C_MS = 1000;
	const long C_NS_MS = 1000 * 1000;
	const long C_NS = 1000000000;
	const int  C_TIMER_CREATE = -1;
	/* ---------------------------------------------------------------- */
	/* Create new timer
	 * @param[in] timeout Timeout in milliseconds
	 * @param[in] repeat Boolean flag, configuring repeatable timer
	 * @return timer ID
	 */
	long set_timer( long milliseconds, bool repeat, int fd = C_TIMER_CREATE )
	{
		::itimerspec new_value, old_value;
		if( fd == C_TIMER_CREATE )
		{
			fd = ::timerfd_create(CLOCK_MONOTONIC, 0);
			if( fd < 0 )
			{
				return fd;
			}
		}

		
		if (milliseconds < 0)
		{
			milliseconds = 0;
		}
		
		new_value.it_value.tv_sec = milliseconds / C_MS;
		new_value.it_value.tv_nsec = (milliseconds % C_MS) * C_NS_MS;

		if (!repeat || milliseconds==0)
		{
			new_value.it_interval.tv_sec = 0;
			new_value.it_interval.tv_nsec = 0;
		}
		else
		{
			new_value.it_interval.tv_sec = milliseconds / C_MS;
			new_value.it_interval.tv_nsec = (milliseconds % C_MS) * C_NS_MS;
		}
		
		// NOTE: flags=0 sets the relative timer
		if ( ::timerfd_settime(fd, 0, &new_value, &old_value) == -1 )
		{
			::close( fd );
			return -1;
		}
		

		return fd;
	}

	/* ---------------------------------------------------------------- */
	/* Checks timer status
	 * @param[in] timer_id Timer id, returned by set_timer
	 * @return 0 if timer pending, > 0 number of timer expirations
	 */
	int check_timer( long timer_id )
	{
		::pollfd pfd = { int(timer_id), POLLIN, 0 };
		if( ::poll(&pfd, 1, 0) == -1 )
		{
			return -1;
		}
		return ( pfd.revents & POLLIN )?(1):(0);
	}

	/* ---------------------------------------------------------------- */
	/* Clears (cancels) previously created timer
	 * @param[in] timer_id Timer id, returned by set_timer
	 * @return true on success
	 */
	inline int clr_timer( long timer_id )
	{
		return ::close( int(timer_id) );
	}

/* ---------------------------------------------------------------- */
}} /* Namespace internal END */

/* ---------------------------------------------------------------- */
/* Create new timer
 * @param[in] timeout Timeout in milliseconds
 * @param[in] repeat Boolean flag, configuring repeatable timer
 * @return timer ID
 */
int set_timer( event_item &event, long milliseconds, bool repeat )
{
	int timerHandle = _internal::C_TIMER_CREATE;
	if (event.event_id == events::timer)
		timerHandle = event.evt.tmr.id;
		
	const int tim_h = _internal::set_timer(milliseconds, repeat, timerHandle);
	if (tim_h >=0)
	{
		event.event_id = events::timer;
		event.evt.tmr.id = tim_h;
		event.evt.tmr.num_expires = 0;
	}
	return tim_h;
}

/* Checks timer status
 * @param[in] timer_id Timer id, returned by set_timer
 * @return 0 if timer pending, > 0 number of timer expirations
 */
 int check_timer( const event_item &timer_id )
{
	if( timer_id.event_id == events::timer )
		return _internal::check_timer( timer_id.evt.tmr.id );
	{
		errno = EINVAL;
		return -1;
	}
}
/* Clears (cancels) previously created timer
 * @param[in] timer_id Timer id, returned by set_timer
 * @return true on success
 */
int clr_timer(event_item &timer_id)
{
	if (timer_id.event_id == events::timer)
	{
		int res = _internal::clr_timer( timer_id.evt.tmr.id );
		timer_id.evt.tmr.id = -1;
		return res;
	}
	
	errno = EINVAL;
	return -1;
}
/* ---------------------------------------------------------------- */
/* Change timer settings
 * @param[in] timeout Timeout in milliseconds
 * @param[in] repeat Boolean flag, configuring repeatable timer
 * @return timer ID
 */
int change_timer( event_item &timer_id, long milliseconds, bool repeat )
{
	if(  timer_id.event_id == events::timer && timer_id.evt.tmr.id > 0 )
		return _internal::set_timer( milliseconds, repeat, timer_id.evt.tmr.id );
	{
		errno = EINVAL;
		return -1;
	}
}
/* ---------------------------------------------------------------- */
}

/* ---------------------------------------------------------------- */
