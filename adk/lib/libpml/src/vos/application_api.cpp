/*
 * =====================================================================================
 *
 *       Filename:  application_api.cpp
 *
 *    Description:  Application API for VOS 
 *
 *        Version:  1.0
 *        Created:  12/11/2013 01:54:26 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lucjan Bryndza (lb), Lucjan_B1@verifone.com
 *   Organization:  VERIFONE
 *
 * =====================================================================================
 */
#include <boost/interprocess/sync/named_semaphore.hpp>
#include <liblog/logsys.h>
#include <boost/thread/thread_time.hpp>
#include <libpml/pml.h>

namespace com_verifone_pml {


/** 
 * Notify application launcher to complete initialization 
 * @return PML error code status 
 */
int app_notify_initialization_complete() {
#ifdef USE_VOS_SEMAPHORES
	boost::interprocess::named_semaphore app_sem ( 
			boost::interprocess::open_or_create, "vfi_app_launcher_sem_from_app" , 1
	);
	dlog_msg("App Initialization complete called()");
	app_sem.post();
	return 0;
#else
	return 0;
#endif
}


/**
	* Add notify that all apps are initialized
	*/
int app_wait_for_all( int timeout )
{
#ifdef USE_VOS_SEMAPHORES
	boost::interprocess::named_semaphore notify_sem(
			boost::interprocess::open_or_create, "vfi_app_launcher_complete" , 0
		);
	dlog_msg( "Wait for initialization complete" );
	if( timeout != time_infinite ) {
		boost::system_time const stout = boost::get_system_time() +  
			boost::posix_time::milliseconds( timeout );
		if( notify_sem.timed_wait( stout ) ) {
			return -1;
		} else {
			return 0;
		}
	} else {
		notify_sem.wait();
		return 0;
	}
#else
	return 0;
#endif
}

}	//Namepsace end

