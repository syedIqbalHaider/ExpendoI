/*
 * userevents.h
 *
 *  Created on: 26-02-2013
 *      Author: lucck
 */

/* ---------------------------------------------------------------- */
#ifndef COM_VERIFONE_PML_EPOLL_USER_HPP_
#define COM_VERIFONE_PML_EPOLL_USER_HPP_

/* ---------------------------------------------------------------- */
#include "libpml/pml.h"
#include "libpml/pml_os_core.h"
#include "epoll_comm.hpp"
#include <boost/weak_ptr.hpp>
#include <boost/scoped_ptr.hpp>
#include <set>
#include <boost/function.hpp>
#include <boost/thread.hpp>
#include <atomic>
/* ---------------------------------------------------------------- */
namespace com_verifone_pml {
namespace internal {

/* ---------------------------------------------------------------- */
//Fwd decls
class pml_task_mngr;
class events_queue;

typedef boost::function<bool (const event_item&, const event_item&)> comparator_type;
typedef std::set<event_item, comparator_type> events_thread_container;
typedef std::map<int, events_thread_container> events_container_type;

/* ---------------------------------------------------------------- */
class epoll_user : public epoll_comm
{
public:
	//User event constructor
	epoll_user( int epoll_hwnd, boost::shared_ptr<events_queue> queue, boost::shared_ptr<events_container_type> req_events );
	//Destructor
	virtual ~epoll_user() {
	}
private:
	//Private control function special for event update
	virtual int ctl( const event_item &event, int op );
	//Update the event
	virtual int update_event( event_item& event, const ::epoll_event &sys_event );
	const int m_epoll_hwnd;
	boost::weak_ptr<events_container_type> m_req_events;
	boost::weak_ptr<events_queue> m_queue;
	// Stuff for tamper and switch_tripped events
	boost::scoped_ptr<boost::thread> m_thread;
	mutable boost::mutex m_thread_mutex; // Controls parameters passing
	bool m_thread_tamper_checks;
	bool m_thread_switch_checks;
	// battery
	boost::scoped_ptr<boost::thread> m_battery_thread;
	mutable boost::mutex m_battery_thread_mutex; // Controls parameters passing
	int m_battery_thread_percent;
	int m_battery_thread_status;
	// Range
	std::atomic<int> m_signal_strength {};
	std::atomic<int> m_signal_tresh {};
	boost::scoped_ptr<boost::thread> m_signal_thread;
	pid_t m_tid;
	communication_capabilities m_comm_capab;
	void thread_func();
	void battery_thread_func();
	void signal_thread_func();
};
/* ---------------------------------------------------------------- */
} /* namespace internal */
} /* namespace com_verifone_pml */

/* ---------------------------------------------------------------- */
#endif /* USEREVENTS_H_ */
/* ---------------------------------------------------------------- */

