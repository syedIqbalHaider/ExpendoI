/*
 * =====================================================================================
 *
 *       Filename:  netlink_helper.hpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  02/18/2014 12:34:46 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lucjan Bryndza (lb), Lucjan_B1@verifone.com
 *   Organization:  VERIFONE
 *
 * =====================================================================================
 */


#ifndef  netlink_helper_INC
#define  netlink_helper_INC
/* ------------------------------------------------------------------ */
#include <utility>
#include <string>
#include <list>
#include <boost/function.hpp>
/* ------------------------------------------------------------------ */ 
struct nlmsghdr;
/* ------------------------------------------------------------------ */ 
namespace com_verifone_pml {
namespace net {
namespace netlink_utils {

	static const int INVALID_HANDLE = -1;
	typedef boost::function<void( const char*,bool )> netlink_callback_t;
	//! Start monitoring
	int create_netlink_listener();
	//! Close netlink listener
	int close_netlink_listener( int handle );
	//! Parse netlink message
	int parse_and_receive_netlink_msg( int handle, netlink_callback_t ev_callback );
/* ------------------------------------------------------------------ */ 
}	//detail
}	//net
}	//com_verifone_pml


#endif   /* ----- #ifndef netlink_helper_INC  ----- */
