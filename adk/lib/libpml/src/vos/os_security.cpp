#include <cerrno>
#include <cstdlib>
#include <string>
#include <svcmgr/svc_security.h>

#include <libpml/pml_os_security.h>

#include <liblog/logsys.h>

#include "dynlib_loader.hpp"


namespace com_verifone_pml
{
	namespace needham_schroeder {
		#ifdef SDK_SUPPORTS_NS
		int init(int mode, std::string & output)
		{
			int result = 0;
			dlog_msg("InitiateNS, mode %Xh", mode);
			securityDataBuffer out_buffer = PML_DYNAMIC_CALL( "libsvc_security.so", security_ux_initiateNS, mode );
			dlog_msg("InitiateNS: result errno %d, buffer data len %d", errno, out_buffer.data_count);
			if (errno == 0)
			{
				if (out_buffer.data)
				{
					output.assign( static_cast<char *>(out_buffer.data), out_buffer.data_count );
				}
				else
				{
					errno = EIO;
					result = -1;
				}
			}
			else
			{
				result = -1;
			}
			if (out_buffer.data) free(out_buffer.data);
			return result;
		}
		int resolve(int mode, const std::string & input, std::string & output)
		{
			int result = 0;
			securityDataBuffer out_buffer = { NULL, 0 };
			// Kamil_P1: OS API stupidity, const_cast is required here
			securityDataBuffer in_buffer = { const_cast<char *>(input.data()), static_cast<int>(input.size()) };
			dlog_msg("ResolveNS, mode %Xh, input data size %d", mode, input.size());
			out_buffer = PML_DYNAMIC_CALL( "libsvc_security.so", security_ux_resolveNS, mode, in_buffer );
			dlog_msg("ResolveNS: result errno %d, buffer data len %d", errno, out_buffer.data_count);
			if (errno == 0)
			{
				if (out_buffer.data)
				{
					output.assign( static_cast<char *>(out_buffer.data), out_buffer.data_count );
				}
				else
				{
					errno = EIO;
					result = -1;
				}
			}
			else
			{
				result = -1;
			}
			if (out_buffer.data) free(out_buffer.data);
			return result;
		}
		int finalize(int mode, const std::string & input, std::string & output)
		{
			int result = 0;
			dlog_msg("FinalizeNS, mode %Xh, input data size %d", mode, input.size());
			// Kamil_P1: OS API stupidity, const_cast is required here
			securityDataBuffer in_buffer = { const_cast<char *>(input.data()), static_cast<int>(input.size()) };
			securityDataBuffer out_buffer = PML_DYNAMIC_CALL( "libsvc_security.so", security_ux_resolveNS, mode, in_buffer );
			dlog_msg("FinalizeNS: result errno %d, buffer data len %d", errno, out_buffer.data_count);
			if (errno == 0)
			{
				if (out_buffer.data)
				{
					output.assign( static_cast<char *>(out_buffer.data), out_buffer.data_count );
				}
				else
				{
					errno = EIO;
					result = -1;
				}
			}
			else
			{
				result = -1;
			}
			if (out_buffer.data) free(out_buffer.data);
			return result;
		}
		int verify(int mode, const std::string & input)
		{
			// Kamil_P1: OS API stupidity, const_cast is required here
			securityDataBuffer in_buffer = { const_cast<char *>(input.data()), static_cast<int>(input.size()) };
			dlog_msg("VerifyNS, mode %Xh, input data size %d", mode, input.size());
			int result = PML_DYNAMIC_CALL( "libsvc_security.so", security_ux_verifyNS, mode, in_buffer );
			dlog_msg("VerifyNS: result %d, errno %d", result, errno);
			return result;
		}
		int get_session_key(int mode, OPERATION_TYPE key_type, std::string & key)
		{
			int result = 0;
			dlog_msg("GetSessionKeyNS, mode %Xh, key type %d", mode, key_type);
			securityDataBuffer out_buffer = PML_DYNAMIC_CALL( "libsvc_security.so", security_ux_get_session_key, mode, key_type );
			dlog_msg("GetSessionKeyNS: result errno %d, buffer data len %d", errno, out_buffer.data_count);
			if (errno == 0)
			{
				if (out_buffer.data)
				{
					key.assign( static_cast<char *>(out_buffer.data), out_buffer.data_count );
				}
				else
				{
					errno = EIO;
					result = -1;
				}
			}
			else
			{
				result = -1;
			}
			if (out_buffer.data) free(out_buffer.data);
			return result;
		}
		int load_encrypted_block(int mode, OPERATION_TYPE key_type, const std::string & block)
		{
			// Kamil_P1: OS API stupidity, const_cast is required here
			securityDataBuffer in_buffer = { const_cast<char *>(block.data()), static_cast<int>(block.size()) };
			dlog_msg("LoadBlockNS, mode %Xh, key type %d, block size %d", mode, key_type, block.size());
			int result = PML_DYNAMIC_CALL( "libsvc_security.so", security_ux_load_cipher_block, mode, key_type, in_buffer );
			dlog_msg("LoadBlockNS: result %d, errno %d", result, errno);
			return result;
		}
		int ux_set_ars_password(int mode, int password_id, const std::string & password)
		{
			dlog_msg("Set Ux ARS password, mode %Xh, password id %d", mode, password_id);
			// Kamil_P1: OS API stupidity, const_cast is required here
			int result = PML_DYNAMIC_CALL( "libsvc_security.so", security_setUxPassword, mode, password_id, const_cast<char *>(password.c_str()) );
			dlog_msg("Set Ux ARS password: result %d, errno %d", result, errno);
			return result;
		}
		int ux_reset_ars(int mode, int password_id, const std::string & password)
		{
			dlog_msg("Verify Ux ARS password, mode %Xh, password id %d", mode, password_id);
			// Kamil_P1: OS API stupidity, const_cast is required here
			int result = PML_DYNAMIC_CALL( "libsvc_security.so", security_resetUxARS, mode, password_id, const_cast<char *>(password.c_str()) );
			dlog_msg("Verify Ux ARS password: result %d, errno %d", result, errno);
			return result;
		}
		int get_certificate(int pki_type, int cert_level, std::string & cert)
		{
			int result = 0;
			dlog_msg("ReadCertificate, PKI type %d, level %d", pki_type, cert_level);
			securityDataBuffer out_cert = PML_DYNAMIC_CALL( "libsvc_security.so", security_ux_read_certificate, pki_type, cert_level );
			dlog_msg("ReadCertificate: result errno %d, certificate data len %d", errno, out_cert.data_count);
			if (errno == 0)
			{
				if (out_cert.data)
				{
					cert.assign( static_cast<char *>(out_cert.data), out_cert.data_count );
				}
				else
				{
					errno = EIO;
					result = -1;
				}
			}
			else
			{
				result = -1;
			}
			if (out_cert.data) free(out_cert.data);
			return result;
		}
		int store_certificate(int pki_type, int cert_level, const std::string & cert)
		{
			// Kamil_P1: OS API stupidity, const_cast is required here
			securityDataBuffer in_cert = { const_cast<char *>(cert.data()), static_cast<int>(cert.size()) };
			dlog_msg("StoreCertificate, PKI type %d, level %d, cert size %u", pki_type, cert_level, cert.size());
			int result = PML_DYNAMIC_CALL( "libsvc_security.so", security_ux_load_certificate, pki_type, cert_level, in_cert );
			dlog_msg("StoreCertificate: result %d, errno %d", result, errno);
			return result;
		}
		int clear_certificate_chain(int pki_type)
		{
			securityDataBuffer in_cert = { NULL, 0 };
			dlog_msg("ClearCertificate: PKI %d", pki_type);
			int result = PML_DYNAMIC_CALL( "libsvc_security.so", security_ux_load_certificate, pki_type, -1, in_cert );
			dlog_msg("ClearCertificate: result %d, errno %d", result, errno);
			return result;
		}
		#else
		// For old SDKs
		int init(int /*mode*/, std::string & /*output*/)
		{
			errno = ENXIO;
			return -1;
		}
		int resolve(int /*mode*/, const std::string & /*input*/, std::string & /*output*/)
		{
			errno = ENXIO;
			return -1;
		}
		int finalize(int /*mode*/, const std::string & /*input*/, std::string & /*output*/)
		{
			errno = ENXIO;
			return -1;
		}
		int verify(int /*mode*/, const std::string & /*input*/)
		{
			errno = ENXIO;
			return -1;
		}
		int get_session_key(int /*mode*/, OPERATION_TYPE /*key_type*/, std::string & /*key*/)
		{
			errno = ENXIO;
			return -1;
		}
		int load_encrypted_block(int /*mode*/, OPERATION_TYPE /*key_type*/, const std::string & /*block*/)
		{
			errno = ENXIO;
			return -1;
		}
		int get_certificate(int /*pki_type*/, int /*cert_level*/, std::string & /*cert*/)
		{
			errno = ENXIO;
			return -1;
		}
		int store_certificate(int /*pki_type*/, int /*cert_level*/, const std::string & /*cert*/)
		{
			errno = ENXIO;
			return -1;
		}
		int clear_certificate_chain(int /*pki_type*/)
		{
			errno = ENXIO;
			return -1;
		}
		int ux_set_ars_password(int /*mode*/, int /*password_id*/, const std::string & /*password*/)
		{
			errno = ENXIO;
			return -1;
		}
		int ux_reset_ars(int /*mode*/, int /*password_id*/, const std::string & /*password*/)
		{
			errno = ENXIO;
			return -1;
		}
		#endif
	} // namespace needham_schroeder
}

// os_security
