/*
 * =====================================================================================
 *
 *       Filename:  bletooth.cpp
 *
 *    Description:  Bluetooth support for the PML
 *
 *        Version:  1.0
 *        Created:  12.02.2015 12:50:05
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lucjan Bryndza (LB), lucck
 *   Organization:  
 *
 * =====================================================================================
 */
#include <cstring>
#include <liblog/logsys.h>
#include <libpml/pml_abstracted_api.h>


/* ------------------------------------------------------------------ */
namespace com_verifone_pml {
namespace net {
namespace bluetooth {
/* ------------------------------------------------------------------ */
//! Enable or disable bluetooth device
int enable( bool )
{
	dlog_error("Bluetooth is not supported by SDK API");
	errno = ENOTSUP;
	return -1;
}
/* ------------------------------------------------------------------ */ 
/** Function establish Bluetooth connection with selected type
	* @param[in] link Connection type of the device
	* @return Error -1 if failed and errno set
	*/
int connect( int, conn_type::_enum_ )
{
	dlog_error("Bluetooth is not supported by SDK API");
	errno = ENOTSUP;
	return -1;
}
/* ------------------------------------------------------------------ */
/** Destroy the bluetooth link
	* @return error code */
int disconnect( conn_type::_enum_ )
{
	dlog_error("Bluetooth is not supported by SDK API");
	errno = ENOTSUP;
	return -1;
}
/* ------------------------------------------------------------------ */
/**  Scan the available devices in range 
	*  @param[out] dev_list Return device list in the application
	@return Number of count of the device or -1 and errno will be set
*/
int scan( std::vector<device> &)
{
	dlog_error("Bluetooth is not supported by SDK API");
	errno = ENOTSUP;
	return -1;
}
/* ------------------------------------------------------------------ */
/** Pair with the selected device from the list
	* @param[in] dev Input device for pair
	* @return 0 on success -1 if fail errno will be set
	*/
int pair( const device&, const char [], pair_method::_enum_ )
{
	dlog_error("Bluetooth is not supported by SDK API");
	errno = ENOTSUP;
	return -1;
}
/* ------------------------------------------------------------------ */
/** Pair with the selected device from the list
	* @param[in] dev Input device for pair
	* @return 0 on success -1 if fail errno will be set
	*/
int is_paired( int )
{
	dlog_error("Bluetooth is not supported by SDK API");
	errno = ENOTSUP;
	return -1;
}
/* ------------------------------------------------------------------ */
/** Unpair with the selected device from the list
	* @param[in] dev Input device for pair
	* @return 0 on success -1 if fail errno will be set
	*/
int unpair( int )
{
	dlog_error("Bluetooth is not supported by SDK API");
	errno = ENOTSUP;
	return -1;
}
/* ------------------------------------------------------------------ */ 
/** Get the paired device list 
	* @param[out] Paired device list
	* @param[out] Errno
	*/
int paired_list( std::vector<device>& )
{
	dlog_error("Bluetooth is not supported by SDK API");
	errno = ENOTSUP;
	return -1;
}
int paired_list( std::vector<network_profile>& )
{
	dlog_error("Bluetooth is not supported by SDK API");
	errno = ENOTSUP;
	return -1;
}

/* ------------------------------------------------------------------ */ 
/** 
* Get paired devices count
* @param[in] count Get paired profiles count
* @return error code
*/
int get_paired_count( int& )
{
	dlog_error("Bluetooth is not supported by SDK API");
	errno = ENOTSUP;
	return -1;
}
/* ------------------------------------------------------------------ */
/** 
* Find paired device as an address
* @param[in] device Get paired profiles count
* @param[out] pair_index Paired index
* @return error code
*/
int find_paired( const device &, int& )
{
	dlog_error("Bluetooth is not supported by SDK API");
	errno = ENOTSUP;
	return -1;
}
/* ------------------------------------------------------------------ */ 
/** 
	*  Set device as paired defaults
	*	@param[in] pair_index Pairing device index
	*	@return Error code
	*/
int set_paired_default( int )
{
	dlog_error("Bluetooth is not supported by SDK API");
	errno = ENOTSUP;
	return -1;
}
/* ------------------------------------------------------------------ */
/** Get device as defualt paired
* @param[out] Get paired device info
* @return Error code
*/
int get_default_paired( device& )
{
	dlog_error("Bluetooth is not supported by SDK API");
	errno = ENOTSUP;
	return -1;
}
/* ------------------------------------------------------------------ */ 
/** Get paired device profile id
	* @param[out] profile_id Paired profile index returned
	* @return Error code 
	*/
int get_default_paired_id( int & )
{
	dlog_error("Bluetooth is not supported by SDK API");
	errno = ENOTSUP;
	return -1;
}
/* ------------------------------------------------------------------ */ 
/** Get connected list of devices
	* @param[in] List of connected devices
	* @return error code
	*/
int get_connected( std::vector<device>& )
{
	dlog_error("Bluetooth is not supported by SDK API");
	errno = ENOTSUP;
	return -1;
}
/* ------------------------------------------------------------------ */ 

} } }
/* ------------------------------------------------------------------ */ 

