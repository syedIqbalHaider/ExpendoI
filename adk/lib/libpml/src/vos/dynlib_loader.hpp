/*
 * =====================================================================================
 *
 *       Filename:  dynlib_loader.hpp
 *
 *    Description: Dynamic symbol library loader
 *
 *        Version:  1.0
 *        Created:  24.02.2015 12:01:14
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lucjan Bryndza (LB), lucck
 *   Organization:  
 *
 * =====================================================================================
 */
#include <functional>
#include <iostream>
#include <map>
#include <memory>
#include <list>
#include <cassert>

/* ------------------------------------------------------------------ */ 
namespace com_verifone_pml {
namespace internal {
/* ------------------------------------------------------------------ */
class dl_loader {
	// Dl loader
	dl_loader( dl_loader& ) = delete;
	dl_loader& operator=( dl_loader& ) = delete;
	dl_loader() {
	}
public:
	~dl_loader() {
		if (_instance_) {
			delete _instance_; _instance_ = nullptr;
		}
	}
public:
	static dl_loader * get_instance();
	//Resolve name by svc_net.so.1:name
	template <typename Func, typename ...Args>
	typename std::function<Func>::result_type call( const char* name, Args && ...args ) 
	{
		Func* fptr = reinterpret_cast<Func*>( find_or_load_symbol( name ) );
		assert(fptr);
		return fptr( args ... );
	}
	template <typename Func>
	typename std::function<Func>::result_type call( const char* name ) 
	{
		Func* fptr = reinterpret_cast<Func*>( find_or_load_symbol( name ) );
		assert(fptr);
		return fptr( );
	}
private:
	//! Find or load symbol from the dynamic library
	void* find_or_load_symbol( const char *name );
private:
	std::map<std::string, void*> m_funcs;	//Function loaded names
	using lib_ptr_t = std::unique_ptr<void,std::function<void(void*)>>;
	std::map<std::string, lib_ptr_t> m_libs;	//Libs loaded
	static dl_loader * _instance_;
};
/* ------------------------------------------------------------------ */ 
}
}
/* ------------------------------------------------------------------ */ 

#define _PML_INTERNAL_STRINGIZE_NX(A) #A
#define _PML_INTERNAL_STRINGIZE(A) _PML_INTERNAL_STRINGIZE_NX(A)
#define PML_DYNAMIC_CALL( libname, funcname, ...) \
	com_verifone_pml::internal::dl_loader::get_instance()->call<decltype(funcname)>(libname ":" _PML_INTERNAL_STRINGIZE(funcname) , ##__VA_ARGS__ )
/* ------------------------------------------------------------------ */
