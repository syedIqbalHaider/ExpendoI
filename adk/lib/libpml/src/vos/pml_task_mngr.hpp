/*
 * pml_task_mngr.hpp
 *
 *  Created on: 14-03-2013
 *      Author: lucck
 */
/* ---------------------------------------------------------------- */
#ifndef COM_VERIFONE_PML_PML_TASK_MNGR_HPP_
#define COM_VERIFONE_PML_PML_TASK_MNGR_HPP_
/* ---------------------------------------------------------------- */
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/allocators/allocator.hpp>
#include <boost/interprocess/containers/map.hpp>
#include <boost/noncopyable.hpp>
#include <cstring>
#include "evt_key.hpp"

/* ---------------------------------------------------------------- */
namespace com_verifone_pml {
namespace internal {

/* ---------------------------------------------------------------- */
//PLM task manager creator
class pml_task_mngr : private boost::noncopyable
{
public:
	struct app_info
	{
		pid_t pid;				//Application PID
		char ver[16];			//Application version
		char name[16];
	};
	typedef evt_key key_type;
	typedef app_info mapped_type;
	typedef std::pair< const key_type, mapped_type > value_type;
private:
	struct comparable_type : public std::binary_function<evt_key, evt_key, bool>
	{
	   bool operator()(evt_key const& n1, evt_key const& n2) const
	   {
	       return std::memcmp( n1.id, n2.id, sizeof evt_key::id) < 0;
	   }
	};
	typedef boost::interprocess::allocator<value_type, boost::interprocess::managed_shared_memory::segment_manager> shm_allocator_type;

public:
	typedef boost::interprocess::map<key_type, mapped_type ,comparable_type ,shm_allocator_type> shared_map_t;
	typedef shared_map_t::iterator iterator;
	typedef shared_map_t::const_iterator const_iterator;
	typedef shared_map_t::reference reference;
	typedef shared_map_t::const_reference const_reference;
	iterator begin() { return m_tasks->begin(); }
	const_iterator begin() const { return m_tasks->begin(); }
	iterator end() { return m_tasks->end(); }
	const_iterator end() const { return m_tasks->end(); }
	const_iterator cbegin() const { return m_tasks->begin(); }
	const_iterator cend() const { return m_tasks->end(); }
private:
	static const std::size_t C_header_size = 4096;
	static const std::size_t C_max_tasks = 128;
public:
	//Constructor
	pml_task_mngr();
	//Register task
	void insert( const char* name );
	//Insert version to the task
	int insert_version( const char* name, const char* version );
	//Operator for finding the tasks
	iterator find( const key_type& k )
	{
		iterator it = m_tasks->find( k );
		if( it != m_tasks->end() )
		{
			if( exists( *it ) )return it;
			else return m_tasks->end();
		}
		else
		{
			return it;
		}
	}
	//Operator for finding the task struct
	const_iterator find( const key_type& k ) const
	{
		const_iterator it = m_tasks->find( k );
		if( it != m_tasks->end() )
		{
			if( exists( *it ) )return it;
			else return m_tasks->end();
		}
		else
		{
			return it;
		}	
	}
#ifndef NDEBUG
	//Print registered tasks
	void print_tasks( ) const;
#endif
private:
	//Check if task is alive and exists
	bool exists( const value_type &v ) const;
private:
	boost::interprocess::managed_shared_memory m_shm;
	const shm_allocator_type m_shm_allocator;
	shared_map_t * const m_tasks;
};
/* ---------------------------------------------------------------- */
} /* namespace internal */
} /* namespace com_verifone_pml */
/* ---------------------------------------------------------------- */
#endif /* PML_TASK_MNGR_HPP_ */
/* ---------------------------------------------------------------- */
