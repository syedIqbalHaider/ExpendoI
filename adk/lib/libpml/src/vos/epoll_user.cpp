/*
 * userevents.cpp
 *
 *  Created on: 26-02-2013
 *      Author: lucck
 */
/* ---------------------------------------------------------------- */

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/eventfd.h>
#include <boost/foreach.hpp>
#include <boost/lambda/lambda.hpp>
#include <boost/lambda/bind.hpp>
#include <errno.h>
#include "epoll_user.hpp"
#include "libpml/pml.h"
#include "libpml/ux.h"
#include "events_queue.hpp"
#include <liblog/logsys.h>
#include "libpml/pml_abstracted_api.h"
#include "bluetooth_utility.hpp"

extern "C" {    //Extern C ugly hack VFI sdk doesn't provide C++ guard
#include <svcmgr/svc_security.h>
#include <svcmgr/svc_net.h>
}
#include "dynlib_loader.hpp"
/* ---------------------------------------------------------------- */
namespace com_verifone_pml {
pid_t gettid();
namespace internal {

/* ---------------------------------------------------------------- */
//TODO: Implementation is required
//User event constructor
epoll_user::epoll_user( int epoll_hwnd, boost::shared_ptr<events_queue> queue, boost::shared_ptr<events_container_type> req_events )
	:	epoll_comm( epoll_hwnd )
	,	m_epoll_hwnd( epoll_hwnd )
	,	m_req_events( req_events )
	,	m_queue( queue )
	,	m_thread_tamper_checks(false)
	,	m_thread_switch_checks(false)
	,	m_battery_thread_percent(0)
	,	m_battery_thread_status(0)
	,	m_tid(gettid())

{
	if( get_communication_capabilities( m_comm_capab ) ) {
		dlog_error("Unable to get communication capabilities err %i", errno );
	} else {
		dlog_msg("Has wifi %i Has gprs %i Has bluetooth %i", m_comm_capab.is_wifi, m_comm_capab.is_gprs, m_comm_capab.is_bluetooth );
	}
}
/* ----------------------------------------------------------------- */
//Thread main
void epoll_user::thread_func()
{
	pml_init( "__PML_INTERNAL_TAMPER_THREAD__" );
	ux uxHandler;
	for(;;)
	{
	//Note lock should be limited only to real access data
		// dlog_msg("Tamper check tick");
	{
			boost::mutex::scoped_lock lock( m_thread_mutex );
			if (m_thread_tamper_checks)
			{
				bool tampered = false;
				attackStatus tamper = PML_DYNAMIC_CALL("libsvc_security.so",security_getAttackStatus);
				if (errno == 0) tampered = tamper.isAttacked;
				if (!tampered && uxHandler.is_ux())
				{
					tampered = uxHandler.getTamperStatus() == 1;
				}
				if (tampered)
				{
					dlog_error("We're being attacked!");
					// notify
					event_item ev;
					ev.event_id =  events::tamper;
					event_raise( m_tid,  ev );
					break; // We're done!
				}
			}
			if (m_thread_switch_checks)
			{
				bool switch_tripped = PML_DYNAMIC_CALL("libsvc_security.so",security_readServiceSwitch)> 0;
				if (!switch_tripped && uxHandler.is_ux()) switch_tripped = uxHandler.getRemovalSwitchStatus() > 0;
				if (switch_tripped)
				{
					dlog_error("We're removed!");
					// notify
					event_item ev;
					ev.event_id =  events::switch_tripped;
					dlog_msg("Raising event for tid %d", m_tid);
					event_raise( m_tid,  ev );
					break; // We're done!
				}
			}
	}
	boost::this_thread::sleep( boost::posix_time::seconds(10) );
	}
}
/* ---------------------------------------------------------------- */
namespace
{
	int get_from_system_file(const char * file)
	{
		int result = -1;
		int hdl = open(file, O_RDONLY);
		if (hdl >= 0)
		{
			char buf[10];
			memset(buf, 0, sizeof(buf));
			if (read(hdl, buf, sizeof(buf)-1) > 0)
			{
				if (sscanf(buf, "%d", &result) != 1)
				{
					dlog_error("Invalid data read (%s)", buf);
					result = 0;
				}
			}
			close(hdl);
		}
		return result;
	}
	int get_from_system_file(const char * file, char * buf, size_t size)
	{
		int result = -1;
		int hdl = open(file, O_RDONLY);
		if (hdl >= 0)
		{
			memset(buf, 0, size);
			result = read(hdl, buf, size-1);
			for (int i = 0; i < result; ++i) *(buf+i) = tolower(*(buf+i));
			close(hdl);
		}
		return result;
	}
}
/* ---------------------------------------------------------------- */
// Battery thread
void epoll_user::battery_thread_func()
{
	pml_init( "__PML_INTERNAL_BATTERY_THREAD__" );
	// Get current percentage
	//int curr_percent = com_verifone_pml::get_battery_percentage();
	//int docked = com_verifone_pml::get_dock_status() == 1 ? BATT_DOCKED : 0;
	//int charging = com_verifone_pml::get_battery_status() == 1 ? BATT_CHARGING : 0;
	int curr_percent = -1;

	for(;;)
	{
		//NOTE: scope of lock should be limited to the access data only not in sleep
		{
			boost::mutex::scoped_lock lock( m_battery_thread_mutex );
			// Read stuff, check params
			// dlog_msg("Battery check tick %d", curr_capacity);
			int percentage = com_verifone_pml::get_battery_percentage();
			int docked = com_verifone_pml::get_dock_status() == 1 ? BATT_DOCKED : 0;
			int charging = com_verifone_pml::get_battery_status() == 1 ? BATT_CHARGING : 0;
			int batt_state = docked | charging;
			if (m_battery_thread_status != batt_state)
			{
				dlog_msg("Battery: status change (to %Xh), %% = %d", batt_state, percentage);
				m_battery_thread_status = batt_state;
				event_item ev;
				ev.event_id =  events::battery;
				ev.evt.batt.status = batt_state;
				ev.evt.batt.percent = percentage;
				event_raise( m_tid,  ev );
			}
			else
			{
				int percDiff = percentage - curr_percent;
				if (percDiff < 0) percDiff = curr_percent - percentage;
				if (percDiff >= m_battery_thread_percent)
				{
					dlog_msg("Battery: percentage change (from %d%% to %d%%), charging %d, trigger %d, was %d",
							curr_percent, percentage, charging, m_battery_thread_percent, percDiff );
					curr_percent = percentage;
					event_item ev;
					ev.event_id =  events::battery;
					ev.evt.batt.status = batt_state;
					ev.evt.batt.percent = percentage;
					event_raise( m_tid,  ev );
				}
			}
		}
		boost::this_thread::sleep( boost::posix_time::seconds( 3 ) );
	}
}
/* ---------------------------------------------------------------- */
//! Wifi thread function ( lock free access )
//! Any locking is not needed structure is updated only from this thread
//  and current signal strenght is not changed from the another thread
void epoll_user::signal_thread_func() 
{
	pml_init( "__PML_INTERNAL_SIGNAL_THREAD__" );
	for(;;) 
	{
		int sig_val {};
		errno = 0;
		if( m_comm_capab.is_wifi 
#ifdef SDK_HAS_BLUETOOTH
			|| m_comm_capab.is_bluetooth
#endif
			)
		{
#ifdef SDK_HAS_BLUETOOTH
			std::string bt_conn_addr;
			const auto bt_conn_stat = m_comm_capab.is_bluetooth?
				net::bluetooth::detail::first_connected( bt_conn_addr ):0;
			// dlog_msg("Checking BT signal strength %d", bt_conn_stat);
			if( bt_conn_stat > 0 )
			{
				const auto sig_dbm = PML_DYNAMIC_CALL("libsvc_net.so", net_bluetoothGetRssi,
					const_cast<char*>( bt_conn_addr.c_str() ) );
				//if (sig_dbm == -1) sig_val = 0;
				if (sig_dbm <= -50) sig_val = 0;
				else if (sig_dbm >= 0) sig_val = 100;
				else sig_val = 2 * (sig_dbm + 50);
			}
			else
#endif
			{
				const auto info =  PML_DYNAMIC_CALL( "libsvc_net.so", net_wifiInfo, const_cast<char*>("wlan0") );
				int sig_dbm = info.signalStrength;
				if (errno == ENETDOWN)
				{
					sig_val = 0;
					errno = 0;
				}
				else if (sig_dbm <= -100) sig_val = 0;
				else if (sig_dbm >= -50) sig_val = 100;
				else sig_val = 2 * (sig_dbm + 100);
			}
		} 
		else if( m_comm_capab.is_gprs ) 
		{
			const auto rssi  = PML_DYNAMIC_CALL("libsvc_net.so", net_gprsGetRssi );
			dlog_msg("GPRS RSSI %d, errno %d", rssi, errno);
			//NOTE: GSM strength to dbm conversion
			//NOTE: RXQUAL values are defined in the GSM 05.08 spec table values (0 to 99) - http://m2msupport.net/m2msupport/atcsq-signal-quality/
			// Value 	RSSI dBm 	Condition
			//2 	-109 	Marginal
			//3 	-107 	Marginal
			//4 	-105 	Marginal
			//5 	-103 	Marginal
			//6 	-101 	Marginal
			//7 	-99 	Marginal
			//8 	-97 	Marginal
			//9 	-95 	Marginal
			//10 	-93 	OK
			//11 	-91 	OK
			//12 	-89 	OK
			//13 	-87 	OK
			//14 	-85 	OK
			//15 	-83 	Good
			//16 	-81 	Good
			//17 	-79 	Good
			//18 	-77 	Good
			//19 	-75 	Good
			//20 	-73 	Excellent
			//21 	-71 	Excellent
			//22 	-69 	Excellent
			//23 	-67 	Excellent
			//24 	-65 	Excellent
			//25 	-63 	Excellent
			//26 	-61 	Excellent
			//27 	-59 	Excellent
			//28 	-57 	Excellent
			//29 	-55 	Excellent
			//30 	-53 	Excellent
			if (rssi.rssi <= 0)
			{
				sig_val = 0;
			}
			else if (rssi.rssi >= 99)
			{
				sig_val = 0; //unknown
			}
			else
			{
				//sig_val = -113 + rssi.rssi * 2;
				//sig_val = 2 * (sig_val + 100);
				sig_val = 3 * rssi.rssi + 3;
			}
			if (errno)
			{
				const auto gprsStat = PML_DYNAMIC_CALL("libsvc_net.so", net_gprsGetStatus );
				dlog_msg("GPRS status %d", gprsStat);
				if (gprsStat >= 0)
				{
					errno = 0; // Force signal update
				}
			}
		}
		// dlog_msg("Errno %d: Signal %d: ori %d, threshold %d", errno, sig_val, m_signal_strength.load(), m_signal_tresh.load());
		if( !errno && (std::abs(m_signal_strength.load()-sig_val) >= m_signal_tresh.load() || m_signal_strength.load() < 0 ))
		{
			event_item ev;
			ev.event_id = events::signal_strength;
			ev.evt.signal.value = sig_val;
			event_raise( m_tid, ev );
			m_signal_strength = sig_val;
			dlog_msg( "Signal strength updated to %i", sig_val );
		}
		boost::this_thread::sleep( boost::posix_time::seconds( 3 ) );
	}
}
/* ---------------------------------------------------------------- */
//Private control function special for event update
int epoll_user::ctl( const event_item &event, int op )
{
	if( event.event_id <= events::activate )
		return epoll_comm::ctl( event, op );
	else
	{
		if (event.event_id == events::tamper || event.event_id == events::switch_tripped)
		{
			// Handle thread here
			dlog_msg("Tamper / switch event!");
			// Lock mutex, change params
			boost::mutex::scoped_lock lock( m_thread_mutex );
			if (event.event_id == events::tamper) m_thread_tamper_checks = true;
			else m_thread_switch_checks = true;
			if (m_thread_switch_checks || m_thread_tamper_checks)
			{
				if (!m_thread) 
					m_thread.reset (
						new boost::thread(boost::bind(&epoll_user::thread_func, this))
					);
			}
			else
			{
				if (m_thread != 0) { 
					m_thread->interrupt();
					m_thread->join();
				}
			}
		}
		else if (event.event_id == events::battery)
		{
			dlog_msg("Battery event!");
			if (op == EPOLL_CTL_ADD)
			{
				boost::mutex::scoped_lock lock( m_battery_thread_mutex );
				m_battery_thread_percent = event.evt.batt.percent;
				m_battery_thread_status = event.evt.batt.status;
				if (!m_battery_thread)
					m_battery_thread.reset(
						new boost::thread(boost::bind(&epoll_user::battery_thread_func, this))
					);
			}
			else
			{
				if (m_battery_thread) {
					m_battery_thread->interrupt();
					m_battery_thread->join();
				}
			}
		}
		else if( event.event_id == events::signal_strength ) 
		{
			if( !m_comm_capab.is_gprs && !m_comm_capab.is_wifi && !m_comm_capab.is_bluetooth ) 
			{
				dlog_error("Wifi or gsm not present");
				errno = ENXIO;
				return -1;
			}
			if( op == EPOLL_CTL_ADD ) 
			{
				dlog_msg("Signal strenght added and chg percent is %i", event.evt.signal.value );
				m_signal_tresh = event.evt.signal.value;
				m_signal_strength = -1;
				if( !m_signal_thread ) {
					m_signal_thread.reset( 
						new boost::thread( boost::bind(&epoll_user::signal_thread_func, this ))
					);
				}
			} 
			else 
			{
				if( m_signal_thread ) {
					m_signal_thread->interrupt();
					m_signal_thread->join();
				}
			}
		}
		boost::shared_ptr<events_queue> queue = m_queue.lock();
		::epoll_event epev;
		boost::shared_ptr<events_container_type> req_events_glob = m_req_events.lock();
		events_container_type::iterator itglob = req_events_glob->find( m_epoll_hwnd );
		if (itglob == req_events_glob->end())
		{
					itglob = req_events_glob->insert( std::make_pair( m_epoll_hwnd, 
							events_thread_container (
								boost::lambda::ret<bool>(
										boost::lambda::bind( &event_item::event_id, boost::lambda::_1 ) <
										boost::lambda::bind( &event_item::event_id, boost::lambda::_2 )
								)
							)
						)
					).first;
		}
		events_thread_container & req_events = itglob->second;
		if( op == EPOLL_CTL_ADD )
		{
			epev.data.fd = queue->get_fd();
			epev.events  = EPOLLIN;
			dlog_msg("epoll_user::ctl ADD %d, to handle %d", epev.data.fd, m_epoll_hwnd);
			if( epoll_ifc::do_ctl( epev, event, op ) < 0 )
			{
				if (errno != EEXIST) return -1;
			}
			req_events.insert( event );
		}
		else if( op == EPOLL_CTL_DEL )
		{
			events_thread_container::iterator it =  req_events.find( event );
			if( it != req_events.end() )
			{
				req_events.erase( it );
			}
			if( req_events.empty() )
			{
				epev.data.fd = queue->get_fd();
				epev.events  = EPOLLIN;
				dlog_msg("epoll_user::ctl DEL %d, from handle %d", epev.data.fd, m_epoll_hwnd);
				if( epoll_ifc::do_ctl( epev, event, op ) < 0 )
				{
					return -1;
				}
			}
		}
		return 0;
	}
}
/* ---------------------------------------------------------------- */
//Update event
int epoll_user::update_event( event_item& event, const ::epoll_event &sys_event )
{
//	dlog_msg("epoll_user::update_event event.event_id=%d", event.event_id);

	if( event.event_id <= events::activate )
	{
//		dlog_msg("calling epoll_comm::update_event");
		epoll_comm::update_event( event, sys_event );
	}
	else
	{
		boost::shared_ptr<events_queue> queue = m_queue.lock();
//		dlog_msg("epoll_user::update_event sys_event.events=%d, sys_event.data.fd=%d queue->get_fd()=%d", sys_event.events, sys_event.data.fd, queue->get_fd());
		
		if( sys_event.events == EPOLLIN && sys_event.data.fd == queue->get_fd() )
		{
			
//			dlog_msg("calling queue->pop");
			if(queue->pop( event ))
			{
//				dlog_msg("return res_empty_event");
				return res_empty_event;
			}
			else
			{
//				dlog_msg("Got event type %d", event.event_id);
				boost::shared_ptr<events_container_type> req_events_glob = m_req_events.lock();
				events_container_type::iterator itglob = req_events_glob->find( m_epoll_hwnd );
				if (itglob == req_events_glob->end())
				{
					itglob = req_events_glob->insert( std::make_pair( m_epoll_hwnd, 
							events_thread_container (
								boost::lambda::ret<bool>(
										boost::lambda::bind( &event_item::event_id, boost::lambda::_1 ) <
										boost::lambda::bind( &event_item::event_id, boost::lambda::_2 )
								)
							)
						)
					).first;
				}
				events_thread_container & req_events = itglob->second;
				events_thread_container::iterator it =  req_events.find( event );
				if (it != req_events.end())
				{
//					dlog_msg("return res_data_event");
					return res_data_event;
				}
				else
				{
//					dlog_msg("return res_empty_event");
					return res_empty_event;
				}
			}
		}
	}
//	dlog_msg("return res_data_event 2");
	return res_data_event;
}

/* ---------------------------------------------------------------- */
} /* namespace internal */
} /* namespace com_verifone_pml */
/* ---------------------------------------------------------------- */


