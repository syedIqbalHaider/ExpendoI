/*
 * =====================================================================================
 *
 *       Filename:  netlink_helper.cpp
 *
 *    Description:  Netlink helper class
 *
 *        Version:  1.0
 *        Created:  02/18/2014 12:34:04 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lucjan Bryndza (lb), Lucjan_B1@verifone.com
 *   Organization:  VERIFONE
 *
 * =====================================================================================
 */
#include "netlink_helper.hpp"
#include <stdio.h>
#include <sys/socket.h>
#include <linux/rtnetlink.h>
#include <linux/netlink.h>
#include <asm/types.h>
#include <net/if.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <liblog/logsys.h>
/* ------------------------------------------------------------------ */
namespace com_verifone_pml {
namespace net {
namespace netlink_utils {
/* ------------------------------------------------------------------ */
namespace {
/* ------------------------------------------------------------------ */
//!Parse single netlink message
int parse_netlink_message
	( const struct nlmsghdr* nlm, netlink_callback_t ev_callback )
{
	static const int IFF_LOWER_UP = 0x10000;
	int len;
	char ifname[IF_NAMESIZE + 1];
	ifinfomsg *ifi = nullptr;
	/* Do some error handling. */
	if (nlm->nlmsg_type == RTM_NEWLINK) {
		len = nlm->nlmsg_len - sizeof(*nlm);
		if (size_t(len) < sizeof(*ifi)) {
			errno = EBADMSG;
			return -1;
		}
		ifi = reinterpret_cast<ifinfomsg*>(NLMSG_DATA(nlm));
		if(!ifi) {
			dlog_error("IFI IS EMPTY");
			return -1;
		}
		if (ifi->ifi_flags & IFF_LOOPBACK)
			return -1;

		rtattr * rta = reinterpret_cast<rtattr *>(reinterpret_cast<char *>(ifi) + NLMSG_ALIGN(sizeof(*ifi)));
		len = NLMSG_PAYLOAD(nlm, sizeof(*ifi));
		*ifname = '\0';
		while (RTA_OK(rta, len)) {
			switch (rta->rta_type) {
			case IFLA_IFNAME:
				strncpy(ifname, reinterpret_cast<char*>(RTA_DATA(rta)), sizeof(ifname));
				break;
			}
			rta = RTA_NEXT(rta, len);
		}
	}
	if( !ifi ) {
		dlog_error("IFI pointer is null");
		return -1;
	}
	if (nlm->nlmsg_type == RTM_NEWLINK) {
		ifi->ifi_change == ~0U ? 1 : 0;
	}
	const bool is_up =  (ifi->ifi_flags&IFF_LOWER_UP)==IFF_LOWER_UP;
	ev_callback( ifname, is_up );
	return 0;
}
/* ------------------------------------------------------------------ */
}	//Unnamed ns end
/* ------------------------------------------------------------------ */
//!Close
int close_netlink_listener( int handle )
{
	if( handle != INVALID_HANDLE ) {
		return close( handle );
	}
}
/* ------------------------------------------------------------------ */
//! Start netlink monitoring
int create_netlink_listener()
{
	int m_handle  = socket(AF_NETLINK, SOCK_RAW,  NETLINK_ROUTE);
	if( m_handle < 0 ) {
		dlog_error("Unable to create netlink socket errno %i", errno );
		return -1;
	}
	struct sockaddr_nl nladdr;
	memset( &nladdr, 0, sizeof(nladdr) );
	nladdr.nl_family = AF_NETLINK;
	nladdr.nl_pid = getpid();
	nladdr.nl_groups = RTMGRP_LINK;
	int yes = 1;
	if( ::setsockopt( m_handle, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int) ) == -1) {
		dlog_error("Unable to setsockopt %i", errno);
	}
	if( ::bind(  m_handle,  reinterpret_cast<sockaddr*>(&nladdr), sizeof(nladdr) ) == -1  ) {
		dlog_error("Unable to bind errno %i", errno );
		if( errno != EADDRINUSE ) {
			close( m_handle );
			return -1;
		}
	}
	return m_handle;
}
/* ------------------------------------------------------------------ */
//! Parse netlink message
int parse_and_receive_netlink_msg( int handle, netlink_callback_t ev_callback )
{
	if( !handle ) {
		dlog_error("Invalid socket handle");
		return -1;
	}
	char buffer[ 4096 ];
	int bytes = recv( handle, buffer, sizeof buffer, 0 );
	if( bytes <= 0 ) {
		dlog_error("Invalid netlink data %i", errno );
		return -1;
	}
	nlmsghdr *nlm;
	for (nlm = (struct nlmsghdr *)buffer;
		NLMSG_OK(nlm, (size_t)bytes);
		nlm = NLMSG_NEXT(nlm, bytes))
	{
		parse_netlink_message( nlm, ev_callback );
	}
	return 0;
}
}	//detail
}	//net
}	//com_verifone_pml
