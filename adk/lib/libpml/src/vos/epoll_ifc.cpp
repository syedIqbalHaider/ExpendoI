/*
 * epoll_ifc.cpp
 *
 *  Created on: 07-03-2013
 *      Author: lucck
 */
/* ---------------------------------------------------------------- */
#include "epoll_ifc.hpp"
#include <errno.h>
#include <sys/epoll.h>
#include <liblog/logsys.h>

/* ---------------------------------------------------------------- */
namespace com_verifone_pml {
namespace internal {

/* ---------------------------------------------------------------- */
//Do control for cascade handle
int epoll_ifc::do_ctl( const event_item& event, int op )
{
	if( event.event_id == events::cascade )
	{
		if( event.evt.cascade.fd  != event_com_item::any )
		{
			::epoll_event epev;
			epev.data.fd = event.evt.cascade.fd;
			epev.events  = EPOLLIN;
			return do_ctl( epev, event, op );
		}
		else
		{
			errno = EINVAL;
			return -1;
		}
	}
	else
	{
		return ctl( event, op );
	}
}
/* ---------------------------------------------------------------- */
//Cascaded file descriptor manager
int epoll_ifc::do_ctl( ::epoll_event &ev, const event_item &event ,int op )
{
	// dlog_msg("epoll_ifc::do_ctl");
	std::map<int, event_item>::iterator find_ev = m_events_map.find( ev.data.fd );
	if( op == EPOLL_CTL_ADD )
	{
		if( find_ev != m_events_map.end() )
		{
			errno = EEXIST;
			return -1;
		}
		m_events_map[ ev.data.fd ] = event;
	}
	else if( op == EPOLL_CTL_DEL )
	{
		if( find_ev == m_events_map.end() )
		{
				errno = EINVAL;
				return -1;
		}
		m_events_map.erase( find_ev );
	}
	else
	{
		errno = EINVAL;
		return -1;
	}
	return epoll_ctl( m_epoll_fd, op, ev.data.fd , &ev );
}

/* ---------------------------------------------------------------- */
//Wait for event
int epoll_ifc::wait( eventset_t& revents, long timeout )
{
    int ret = -1;
    revents.clear();
    ::epoll_event epoll_ev[ m_events_map.size() ];
//    dlog_msg("epoll_ifc::wait");
    
    if( ( ret = ::epoll_wait( m_epoll_fd, epoll_ev, m_events_map.size(), timeout ) ) > 0 )
    {
        for( int s = 0; s < ret; ++s )
        {
            std::map<int, event_item>::iterator it = m_events_map.find( epoll_ev[s].data.fd );
            if( it != m_events_map.end() )
            {
                //dlog_msg("epoll_ifc::wait, hdl=%d", epoll_ev[s].data.fd);
                const int eret = update_event(it->second, epoll_ev[s] );
                //dlog_msg("epoll_ifc::wait, eret=%d", eret);

                if( eret == epoll_ifc::res_data_event )
                    revents.push_back( it->second );
                else if( eret < 0 )
                    return -1;
            }
        }
    }
    if( revents.empty() && ret > 0 )
    {
        wait( revents, timeout );
        ret = 0;
    }
    return ret;
}
/* ---------------------------------------------------------------- */
} /* namespace internal */
} /* namespace com_verifone_pml */
/* ---------------------------------------------------------------- */
