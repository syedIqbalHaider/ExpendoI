/******************************************************************************

  FILE: Ux.cpp

  DESC: This file is used to handle UX100 unattended device
******************************************************************************/

#include <liblog/logsys.h>
#include <dlfcn.h>
#include <svcsec.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>

#include "libpml/ux.h"


namespace
{
    static int (*__ux100_pinStartPinEntry)( unsigned char  pinLenMin, unsigned char pinLenMax, int timeout ) = 0;
    static int (*__ux100_pinAbort)( void ) = 0;
    static int (*__ux100_pinDelete)( int cnt ) = 0;
    static int (*__ux100_pinSendEncPinBlockToVault)( void ) = 0;
    static int (*__ux100_pinGetNbCurrDigitEntered)( void ) = 0;
    static int (*__ux100_pinEntryMode)( void ) = 0;

    static int (*__ux100_keybdInit)( void ) = 0;
    static int (*__ux100_keybdGetKey)(unsigned int timeout) = 0;
    static int (*__ux100_dispSetBacklightMode)(int mode, unsigned int time) = 0;

	struct ux100KeybdInfoStruct
	{
		unsigned char secure_mode; /** Is the keyboard in secure mode ? */
		unsigned char nb_function_key; /** Number of function keys on the keyboard */
		unsigned char nb_normal_key; /** Number of normal keys on the keyboard */
		unsigned char nb_row; /** Number of normal key rows */
		unsigned char nb_col; /** Number of normal key columns */
		unsigned char key_map[3*5]; /** Keyboard map */
	};
	static struct ux100KeybdInfoStruct (*__ux100_keybdGetInfo)(void) = 0;

    struct ux100SecureVersionStruct
    {
        char id_mode[50];
        char family[10];
        char version[12];
    };
    static struct ux100SecureVersionStruct (*__ux100_infoGetSecureVersion)(void) = 0;

    struct version
    {
        int major;
        int minor;
        int maint;
        char build[16];
    };
    static struct version (*__ux100_getVersion)(void) = 0;

    struct ux100SerialNumberStruct
    {
        char serial_number[11+1]; // ux100 serial number
    };
    static struct ux100SerialNumberStruct (*__ux100_infoGetSerialNumber)(void) = 0;

    struct ux100HardVerStruct
    {
        char hardware_version[15+1]; // ux100 hardware version.
    };
    static struct ux100HardVerStruct (*__ux100_infoGetHardwareVersion)(void) = 0;

    struct ux100PartNumberStruct
    {
        char part_number[32+1]; // ux100 part number
    };
    static struct ux100PartNumberStruct (*__ux100_infoGetPartNumber)(void) = 0;

    struct ux100ModelNumberStruct
    {
        char model_number[12+1]; // ux100 model number
    };
    static struct ux100ModelNumberStruct (*__ux100_infoGetModelNumber)(void) = 0;

    struct ux100PermanentTerminalIDStruct
    {
        char permanent_terminal_id[10 + 1]; /**< ux100 Permanent Terminal ID */
    };
    static struct ux100PermanentTerminalIDStruct (*__ux100_infoGetPermanentTerminalID)(void) = 0;

    static int (*__ux100_infoGetRemSwStatus)( void ) = 0;

    static int (*__ux100_infoGetPairingStatus)(void) = 0;

    static int (*__ux100_infoGetUnitType)(void) = 0;

    static int (*__ux100_buzzerSetKeyboardBeep)(unsigned int frequency, unsigned int time, unsigned int volume) = 0;
    struct ux100DisplayInfoStruct
    {
        int currentBacklightMode;		// curent backlight setting of the display
        int currentBklTime;			// current backlight timer in ms
        int currentContrast;			// current contrast of the display
        int MaxBacklight;			// maximum backlight setting for the display
        int MaxContrast;			// maximum contast setting for the display
        int MinBacklight;			// minimum backlight setting for the display
        int MinContrast;			// minumim contrast setting for the display
        int ScreenHeight;			// Heigth of the screen in pixels
        int ScreenWidth;			// Width of the screen in pixels
        int ScreenColorDepth;		//Depth of the screen color if any
    };
    static struct ux100DisplayInfoStruct (*__ux100_dispGetInfo)(void) = 0;
    static int (*__ux100_dispSetContrast) ( int level ) = 0;
    static int (*__ux100_infoGetTamperStatus) ( void ) = 0;

    static int status = -1;
    static void * handle = NULL;

    static const int FLAG_HAVE_DISPLAY = 0x04;
    static const int FLAG_HAVE_KEYBOARD = 0x02;

    char *rtrim(char *str)
    {
        // Trim trailing space
        char * end = str + strlen(str) - 1;
        while(end > str && isspace(*end)) end--;

        // Write new null terminator
        *(end+1) = 0;

        return str;
    }
}

ux::ux(): initialized(false), haveKeyboard(false), haveDisplay(false)
{
    if (status == -1)
    {
        // try initialising
        status = 0;
        dlog_msg("Detecting terminal type...");
        if (handle) dlclose(handle);
        handle = dlopen("/usr/local/lib/svcmgr/libsvc_ux100.so", RTLD_LAZY);
        if(handle)
        {
            __ux100_pinStartPinEntry          =(int (*)(unsigned char,unsigned char, int))       dlsym(handle,"ux100_pinStartPinEntry");
            __ux100_pinDelete                 =(int (*)(int))                                    dlsym(handle,"ux100_pinDelete");
            __ux100_pinAbort                  =(int (*)())                                       dlsym(handle,"ux100_pinAbort");
            __ux100_pinSendEncPinBlockToVault =(int (*)())                                       dlsym(handle,"ux100_pinSendEncPinBlockToVault");
            __ux100_pinGetNbCurrDigitEntered  =(int (*)())                                       dlsym(handle,"ux100_pinGetNbCurrDigitEntered");
            __ux100_pinEntryMode              =(int (*)())                                       dlsym(handle,"ux100_pinEntryMode");

            __ux100_keybdInit                 =(int (*)())                                       dlsym(handle,"ux100_keybdInit");
            __ux100_keybdGetKey               =(int (*)(unsigned int))                           dlsym(handle,"ux100_keybdGetKey");
            __ux100_dispSetBacklightMode      =(int (*)(int, unsigned int))                      dlsym(handle,"ux100_dispSetBacklightMode");
            __ux100_keybdGetInfo		      =(struct ux100KeybdInfoStruct(*)())                dlsym(handle,"ux100_keybdGetInfo");

            __ux100_infoGetSecureVersion      =(struct ux100SecureVersionStruct (*)())           dlsym(handle, "ux100_infoGetSecureVersion");
            __ux100_getVersion                =(struct version (*)())                            dlsym(handle, "ux100_getVersion");
            __ux100_infoGetSerialNumber       =(struct ux100SerialNumberStruct (*)())            dlsym(handle, "ux100_infoGetSerialNumber");
            __ux100_infoGetHardwareVersion    =(struct ux100HardVerStruct (*)())                 dlsym(handle, "ux100_infoGetHardwareVersion");
            __ux100_infoGetPartNumber         =(struct ux100PartNumberStruct (*)())              dlsym(handle, "ux100_infoGetPartNumber");
            __ux100_infoGetModelNumber        =(struct ux100ModelNumberStruct (*)())             dlsym(handle, "ux100_infoGetModelNumber");
            __ux100_infoGetPermanentTerminalID=(struct ux100PermanentTerminalIDStruct (*)())     dlsym(handle, "ux100_infoGetPermanentTerminalID");

            __ux100_infoGetRemSwStatus        =(int (*)())                                       dlsym(handle, "ux100_infoGetRemSwStatus");
            __ux100_infoGetPairingStatus      =(int (*)())                                       dlsym(handle, "ux100_infoGetPairingStatus");
            __ux100_infoGetUnitType           =(int (*)())                                       dlsym(handle, "ux100_infoGetUnitType");

            __ux100_buzzerSetKeyboardBeep     =(int (*)(unsigned int, unsigned int, unsigned int)) dlsym(handle, "ux100_buzzerSetKeyboardBeep");
            __ux100_dispGetInfo               =(struct ux100DisplayInfoStruct (*)())             dlsym(handle, "ux100_dispGetInfo");
            __ux100_dispSetContrast           =(int (*) ( int ))                                 dlsym(handle, "ux100_dispSetContrast");
            __ux100_infoGetTamperStatus       =(int (*) ())                                      dlsym(handle, "ux100_infoGetTamperStatus");


            //__ux100_secGetRemovalSwitchChallenge=(struct uxByteBuffer (*)())     dlsym(handle,"ux100_secGetRemovalSwitchChallenge");
            //__ux100_secRemovalSwitchReset       =(int (*)( struct uxByteBuffer)) dlsym(handle,"ux100_secRemovalSwitchReset");
            //__ux100_infoGetRemSwStatus          =(int (*)())                     dlsym(handle,"ux100_infoGetRemSwStatus");
            //__ux100_secPerformParing            =(int (*)(int))                  dlsym(handle,"ux100_secPerformParing");
            //__ux100_infoGetOperationalMode      =(int (*)())                     dlsym(handle,"ux100_infoGetOperationalMode");

            if(!__ux100_pinStartPinEntry || !__ux100_pinDelete || !__ux100_pinAbort || !__ux100_pinSendEncPinBlockToVault
               || !__ux100_pinGetNbCurrDigitEntered || !__ux100_pinEntryMode || !__ux100_keybdInit || !__ux100_keybdGetKey
               || !__ux100_dispSetBacklightMode || !__ux100_infoGetSecureVersion || !__ux100_getVersion
               || !__ux100_infoGetSerialNumber || !__ux100_infoGetHardwareVersion || !__ux100_infoGetPartNumber
               || !__ux100_infoGetModelNumber || !__ux100_infoGetRemSwStatus || !__ux100_infoGetPairingStatus
               || !__ux100_infoGetUnitType || !__ux100_buzzerSetKeyboardBeep || !__ux100_infoGetTamperStatus
               //   || !__ux100_secGetRemovalSwitchChallenge || !__ux100_secRemovalSwitchReset  || !__ux100_infoGetRemSwStatus
               //   || !__ux100_secPerformParing || !__ux100_infoGetOperationalMode
              )
            {
                __ux100_pinStartPinEntry              =0;
                __ux100_pinDelete                     =0;
                __ux100_pinAbort                      =0;
                __ux100_pinSendEncPinBlockToVault     =0;
                __ux100_pinGetNbCurrDigitEntered      =0;
                __ux100_pinEntryMode                  =0;
                __ux100_keybdInit                     =0;
                __ux100_keybdGetKey                   =0;
                __ux100_dispSetBacklightMode          =0;
                __ux100_infoGetSecureVersion          =0;
                __ux100_getVersion                    =0;
                __ux100_infoGetSerialNumber           =0;
                __ux100_infoGetHardwareVersion        =0;
                __ux100_infoGetPartNumber             =0;
                __ux100_infoGetModelNumber            =0;
                __ux100_infoGetRemSwStatus            =0;
                __ux100_infoGetPairingStatus          =0;
                __ux100_infoGetUnitType               =0;
                __ux100_buzzerSetKeyboardBeep         =0;
                __ux100_dispGetInfo                   =0;
                __ux100_dispSetContrast               =0;
                __ux100_infoGetTamperStatus           =0;
                //__ux100_secGetRemovalSwitchChallenge=0;
                //__ux100_secRemovalSwitchReset       =0;
                //__ux100_infoGetRemSwStatus          =0;
                //__ux100_secPerformParing            =0;
                //__ux100_infoGetOperationalMode      =0;
                dlclose(handle); handle = NULL;
                // Leave status as 0, this is NOT UX100!
                dlog_msg("This is NOT UX device!");
            }
            else
            {
                ux100SecureVersionStruct secVersion;
                int unitType = __ux100_infoGetUnitType();
                dlog_msg("Detected UX300 device, Ux100/110 device type %d", unitType);
                status = 1;
                if (unitType == 100 || unitType == 110)
                {
                    dlog_msg("Ux%d presence detected", unitType);
                    status |= FLAG_HAVE_KEYBOARD;
                    if (unitType == 100)
                    {
                        status |= FLAG_HAVE_DISPLAY;
                    }
                    secVersion = __ux100_infoGetSecureVersion();
                    if (errno == 0)
                    {
                        dlog_msg("id:%s, family:%s, ver:%s ", secVersion.id_mode, secVersion.family, secVersion.version);
                    }
                    else
                    {
                        dlog_error("Cannot identify Ux100!");
                    }
                    int pairing_status = __ux100_infoGetPairingStatus();
                    if (errno == 0)
                    {
                        dlog_msg("Pairing status: %d", pairing_status);
                    }
                    else
                    {
                        dlog_error("Cannot get pairing status!");
                    }
                }
                else if (unitType == 0)
                {
                    dlog_msg("Unknown keyboard / display unit!");
                }
                else if (unitType < 0)
                {
                    dlog_error("No Ux100/110 connected!");
                }
                else
                {
                    dlog_alert("No keyboard / display unit!");
                }
            }
        }
    }
    if (status > 0)
    {
        initialized = true;
        if (status & FLAG_HAVE_KEYBOARD) haveKeyboard = true;
        if (status & FLAG_HAVE_DISPLAY) haveDisplay = true;
    }
    dlog_msg("Ux initialized: %d", initialized);
    if (initialized) dlog_msg("Features: keyboard %d, display %d", haveKeyboard, haveDisplay);
}

ux::~ux()
{
    //if (handle != NULL) dlclose(handle);
}

int ux::pinStartPinEntry(unsigned char pinLenMin, unsigned char pinLenMax, int timeout)
{
    if (initialized)
    {
        if (__ux100_pinStartPinEntry) return __ux100_pinStartPinEntry(pinLenMin, pinLenMax, timeout);
        dlog_error("pinStartPinEntry is not available on ux100");
    }
    return -1;
}
int ux::pinAbort()
{
    if (initialized)
    {
        if (__ux100_pinAbort) return __ux100_pinAbort();
        dlog_error("pinAbort is not available on ux100");
    }
    return -1;
}
int ux::pinDelete(int cnt)
{
    if (initialized)
    {
        if (__ux100_pinDelete) return __ux100_pinDelete(cnt);
        dlog_error("pinDelete is not available on ux100");
    }
    return -1;
}
int ux::pinSendEncPinBlockToVault()
{
    if (initialized)
    {
        if (__ux100_pinSendEncPinBlockToVault) return __ux100_pinSendEncPinBlockToVault();
        dlog_error("pinSendEncPinBlockToVault is not available on ux100");
    }
    return -1;
}
int ux::pinGetNbCurrDigitEntered()
{
    if (initialized)
    {
        if (__ux100_pinGetNbCurrDigitEntered) return __ux100_pinGetNbCurrDigitEntered();
        dlog_error("pinGetNbCurrDigitEntered is not available on ux100");
    }
    return -1;
}
int ux::pinEntryMode()
{
    if (initialized)
    {
        if (__ux100_pinEntryMode) return __ux100_pinEntryMode();
        dlog_error("pinEntryMode is not available on ux100");
    }
    return -1;
}
int ux::keybdInit()
{
    if (initialized)
    {
        //if (haveKeyboard && __ux100_keybdInit) return __ux100_keybdInit();
        //dlog_error("keybdInit is not available");
        if (haveKeyboard && __ux100_keybdInit)
        {
            int res = __ux100_keybdInit();
            dlog_msg("Keybd init result %d, errno %d", res, errno);
            return res;
        }
    }
    return -1;
}
int ux::keybdGetKey(unsigned int timeout)
{
    if (initialized)
    {
        if (haveKeyboard && __ux100_keybdGetKey) return __ux100_keybdGetKey(timeout);
        dlog_error("keybdGetKey is not available");
    }
    return -1;
}
int ux::dispSetBacklightMode(int mode, unsigned int time)
{
    if (initialized)
    {
        if (haveDisplay && __ux100_dispSetBacklightMode) return __ux100_dispSetBacklightMode(mode, time);
        dlog_error("dispSetBacklightMode is not available on ux110");
    }
    return -1;
}
int ux::getVersions(char * id, size_t idSize,
                    char * family, size_t familySize,
                    char * version, size_t versionSize)
{
    if (initialized && __ux100_infoGetSecureVersion)
    {
        ux100SecureVersionStruct secVersion = __ux100_infoGetSecureVersion();
        if (errno == 0)
        {
            if (id)
            {
                strncpy(id, secVersion.id_mode, idSize);
                rtrim(id);
            }
            if (family)
            {
                strncpy(family, secVersion.family, familySize);
                rtrim(family);
            }
            if (version)
            {
                strncpy(version, secVersion.version, versionSize);
                rtrim(version);
            }
            return 0;
        }
        dlog_error("Errno %d", errno);
    }
    return -1;
}

int ux::getVersion(char * ver, size_t verSize)
{
    if (initialized && __ux100_getVersion)
    {
        version vers = __ux100_getVersion();
        if (errno == 0)
        {
            if (ver) snprintf(ver, verSize, "%03d%03d%03d%s",vers.major,vers.minor,vers.maint,vers.build);
            return 0;
        }
        dlog_error("Errno %d", errno);
    }
    return -1;

}

int ux::getSerialNo(char * sn, size_t snSize)
{
    if (initialized && __ux100_infoGetSerialNumber)
    {
        ux100SerialNumberStruct serial = __ux100_infoGetSerialNumber();
        if (errno == 0)
        {
            if (sn) strncpy(sn, serial.serial_number, snSize);
            return 0;
        }
        dlog_error("Errno %d", errno);
    }
    return -1;
}

int ux::getBareSerialNo(char * sn, size_t snSize)
{
    if (initialized && __ux100_infoGetSerialNumber)
    {
        ux100SerialNumberStruct serial = __ux100_infoGetSerialNumber();
        if (errno == 0)
        {
            if (sn)
            {
                size_t len = strlen( serial.serial_number);
                for (size_t i = 0; i < len && i < snSize-1; ++i)
                {
                    if (isdigit(*((serial.serial_number)+i))) *sn++ = *((serial.serial_number)+i);
                }
                *sn = 0;
            }
            return 0;
        }
        dlog_error("Errno %d", errno);
    }
    return -1;
}

int ux::getPartNo(char * pn, size_t pnSize)
{
    if (initialized && __ux100_infoGetPartNumber)
    {
        ux100PartNumberStruct part = __ux100_infoGetPartNumber();
        if (errno == 0)
        {
            if (pn) strncpy(pn, part.part_number, pnSize);
            return 0;
        }
        dlog_error("Errno %d", errno);
    }
    return -1;
}

int ux::getHardwareVer(char * hw, size_t hwSize)
{
    if (initialized && __ux100_infoGetHardwareVersion)
    {
        ux100HardVerStruct hardware = __ux100_infoGetHardwareVersion();
        if (errno == 0)
        {
            if (hw) strncpy(hw, hardware.hardware_version, hwSize);
            return 0;
        }
        dlog_error("Errno %d", errno);
    }
    return -1;
}

int ux::getModelNumber(char *mn, size_t mnSize)
{
    if (initialized && __ux100_infoGetHardwareVersion)
    {
        ux100ModelNumberStruct model = __ux100_infoGetModelNumber();
        if (errno == 0)
        {
            if (mn)
            {
                strncpy(mn, model.model_number, mnSize);
                rtrim(mn);
            }
            return 0;
        }
        dlog_error("Errno %d", errno);
    }
    return -1;
}

int ux::getPTID(char *ptid, size_t ptidSize)
{
    if (initialized && __ux100_infoGetPermanentTerminalID)
    {
        ux100PermanentTerminalIDStruct id = __ux100_infoGetPermanentTerminalID();
        if (errno == 0)
        {
            if (ptid) strncpy(ptid, id.permanent_terminal_id, ptidSize);
            return 0;
        }
        dlog_error("Errno %d", errno);
    }
    return -1;
}

int ux::getRemovalSwitchStatus()
{
    if (initialized)
    {
        if (__ux100_infoGetRemSwStatus)
        {
            int result = __ux100_infoGetRemSwStatus();
            if (errno == 0) return result;
            else return -1;
        }
        dlog_error("infoGetRemSwStatus is not available on ux100");
    }
    return -1;
}

int ux::getKbdStatus(unsigned char *secure_mode, unsigned char *nb_function_key, unsigned char *nb_normal_key, unsigned char *nb_row, unsigned char *nb_col, unsigned char *key_map)
{
	if (initialized)
	{
		if (__ux100_keybdGetInfo)
		{
			ux100KeybdInfoStruct kbdInfo = __ux100_keybdGetInfo();
			if (errno == 0)
			{
				if (secure_mode) *secure_mode = kbdInfo.secure_mode;
				if (nb_function_key) *nb_function_key = kbdInfo.nb_function_key;
				if (nb_normal_key) *nb_normal_key = kbdInfo.nb_normal_key;
				if (nb_row) *nb_row = kbdInfo.nb_row;
				if (nb_col) *nb_col = kbdInfo.nb_col;
				if (key_map) strncpy((char *)key_map, (const char*)kbdInfo.key_map, 3*5);
				return 0;
			}
			dlog_error("Errno %d", errno);
		}
		else
		{
			dlog_error("keybdGetInfo is not available on ux100");
		}
	}
	return -1;
}

int ux::isKbdSecure()
{
	if (initialized)
	{
		if (__ux100_keybdGetInfo)
		{
			ux100KeybdInfoStruct kbdInfo = __ux100_keybdGetInfo();
			if (errno == 0)
			{
				return kbdInfo.secure_mode;
			}
			dlog_error("Errno %d", errno);
					
		}
	   	dlog_error("keybdGetInfo is not available on ux100");
	}
	return -1;

}

int ux::setKeyboardBeep(bool enable)
{
    if (initialized)
    {
        if (haveKeyboard && __ux100_buzzerSetKeyboardBeep)
        {
            const int frequency = 1245;
            int time = 0;
            if (enable) time = 50;
            int volume = 5; // unused anyway
            return __ux100_buzzerSetKeyboardBeep(frequency, time, volume);
        }
        dlog_error("buzzerSetKeyboardBeep is not available");
    }
    return -1;
}

int ux::setContrast(int percent)
{
    if (initialized)
    {
        if (haveDisplay && __ux100_dispSetContrast && __ux100_dispGetInfo )
        {
            // perform checks whether value is in allowed range here
            // get max contrast
            struct ux100DisplayInfoStruct dispInfo = __ux100_dispGetInfo();
            if (errno)
            {
                dlog_error("Cannot get display info, error %d", errno);
                return -1;
            }
            dlog_msg("Setting contrast to level %d%%", percent);
            dlog_msg("Max contrast %d, min contrast %d", dispInfo.MaxContrast, dispInfo.MinContrast);
            percent = dispInfo.MinContrast + (percent * (dispInfo.MaxContrast-dispInfo.MinContrast)) / 100;
            dlog_msg("Setting contrast to value %d", percent);
            return __ux100_dispSetContrast(percent);
        }
        dlog_error("dispSetContrast is not available on ux110");
    }
    return -1;
}

int ux::getTamperStatus()
{
    if (initialized)
    {
        if (__ux100_infoGetTamperStatus )
        {
            int result = __ux100_infoGetTamperStatus();
            if (errno == 0) return result;
            else return -1;
        }
        dlog_error("infoGetTamperStatus is not available on ux100");
    }
    return -1;
}

// C functions, needed by libnanox
extern "C"
{
    int is_ux()
    {
        ux uxHandler;
        return uxHandler.is_ux();
    }

    bool ux_have_keyboard()
    {
        ux uxHandler;
        if (uxHandler.is_ux())
        {
                if (uxHandler.have_keyboard()) return 1;
                return 0;
        }
        return -1;
    }

    bool ux_have_display()
    {
        ux uxHandler;
        if (uxHandler.is_ux())
        {
                if (uxHandler.have_display()) return 1;
                return 0;
        }
        return -1;
    }

    int ux_keybdInit()
    {
        ux uxHandler;
        if (uxHandler.is_ux()) return uxHandler.keybdInit();
        return -1;
    }
} // extern


