/*
 * epollevent.hpp
 *
 *  Created on: 27-02-2013
 *      Author: lucck
 */
/* ---------------------------------------------------------------- */
#ifndef COM_VERIFONE_PML_EPOLLCOMM_H_
#define COM_VERIFONE_PML_EPOLLCOMM_H_
/* ---------------------------------------------------------------- */
#include "epoll_ifc.hpp"

/* ---------------------------------------------------------------- */
namespace com_verifone_pml {
namespace internal {

/* ---------------------------------------------------------------- */
/* Epoll system events controler */
class epoll_comm : public epoll_ifc
{
public:
    //Constructor
    explicit epoll_comm( int handle )
        : epoll_ifc(handle)
    {}
    //Destructor
    virtual ~epoll_comm()
    {}
protected:
    //Private control function special for event update
    virtual int ctl( const event_item &event, int op );
    //On complete event update
    virtual int update_event( event_item& event, const ::epoll_event &sys_event );
};

/* ---------------------------------------------------------------- */
} /* namespace internal */
} /* namespace com_verifone_pml */

/* ---------------------------------------------------------------- */
#endif /* COM_VERIFONE_PML_EPOOLEVENT_H_ */
/* ---------------------------------------------------------------- */
