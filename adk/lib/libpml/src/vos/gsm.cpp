/*
 * =====================================================================================
 *
 *       Filename:  gsm.cpp
 *
 *    Description:  GSM library
 *
 *        Version:  1.0
 *        Created:  27.02.2015 14:00:34
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lucjan Bryndza (LB), lucck
 *   Organization:  
 *
 * =====================================================================================
 */
#include <libpml/pml_net_gsm.h>
#include <liblog/logsys.h>
#include "dynlib_loader.hpp"
#include <svcmgr/svc_net.h>
#include <cstring>
#include <libpml/pml_net_data.hpp>
#include <libpml/pml_net.h>
/* ------------------------------------------------------------------ */ 
namespace com_verifone_pml {
namespace net {
namespace gsm {

/* ------------------------------------------------------------------ */ 
/**  Register to gsm network and enable GSM module
 *  @param[in] pin Pin for network registration
 *  @return Gprs error code < 0 
 */
int register_to_network( const std::string &pin )
{
	auto ret = PML_DYNAMIC_CALL("libsvc_net.so",net_gprsPowerOnModule );
	if( ret )  {
		dlog_error( "Unable to poweron GSM module ret: %i  errno: %i errnostr %i",
			ret, errno, std::strerror( errno ) );
		return ret;
	}
	auto gsm_status = PML_DYNAMIC_CALL("libsvc_net.so", net_gprsGetStatus );
	if (gsm_status == NET_GSM_STT_SIM_AUTH_NEEDED) {
		ret = PML_DYNAMIC_CALL("libsvc_net.so", net_gprsVerifyPin, const_cast<char*>(pin.c_str()) );
		if (ret) {
			dlog_error( "Unable to getstatus GSM module ret: %i  errno: %i errnostr %i",
				ret, errno, std::strerror( errno ) );
		}
	}
	return ret;
}
/* ------------------------------------------------------------------ */ 
/** Deregister from GSM network and disable module
 * @return gprs error code < 0 
 */
int deregister_from_network()
{
	auto ret = PML_DYNAMIC_CALL("libsvc_net.so", net_gprsPowerOffModule );
	if (ret) {
		dlog_error( "Unable to deregister GSM module ret: %i  errno: %i errnostr %i",
			ret, errno, std::strerror( errno ) );
	}
	return ret;
}
/* ------------------------------------------------------------------ */ 
/** 
 * Connect to gsm network using GSM module 
 * @param[in] cfg Medium configuration
 * @param[in] interface configuration if none automatic DHCP mode will be used
 * @return error code if failed system code or gsm error returned
 */
int connect( const medium_config& cfg, const interface_config* ifc ) 
{
	netGprsInfo gprscfg {};
	std::strncpy( gprscfg.APN, cfg.get_apn().c_str(), sizeof(gprscfg.APN)-1);
	std::strncpy( gprscfg.number, cfg.get_phoneno().c_str(), sizeof(gprscfg.number)-1);
	gprscfg.reg_mode = NET_GPRS_REG_AUTO;
	dlog_msg( "GSM connect APN: %s phone_number: %s", gprscfg.APN, gprscfg.number );
	auto ret = PML_DYNAMIC_CALL("libsvc_net.so", net_interfaceGprsSet, gprscfg );
	if( ret ) {
		dlog_error( "Unable to GSM interface set ret: %i  errno: %i errnostr %i",
			ret, errno, std::strerror( errno ) );
		return ret;
	}
	//! Basic interface config
	netIfconfig ifcfg {};
	if( ifc == nullptr ) {
		ifcfg.usedhcp = true;
		ifcfg.activate = true;
	} else {
		std::strncpy( ifcfg.local_ip, ifc->addr.c_str(), sizeof ifcfg.local_ip );
		std::strncpy( ifcfg.netmask,  ifc->netmask.c_str(), sizeof ifcfg.netmask );
		std::strncpy( ifcfg.gateway,  ifc->gateway.c_str(), sizeof ifcfg.gateway );
		std::strncpy( ifcfg.dns1,  ifc->dns1.c_str(), sizeof ifcfg.dns1 );
		std::strncpy( ifcfg.dns2,  ifc->dns2.c_str(), sizeof ifcfg.dns2 );
		std::strncpy( ifcfg.broadcast, net::detail::calc_broadcast( ifc->addr, ifc->netmask ).c_str(), 
				sizeof ifcfg.broadcast );
		ifcfg.usedhcp = false;
		ifcfg.activate = true;
	}
	//! Configure rest of the GPRS params
	std::strncpy( ifcfg.interface, NET_PPP_GPRS_DEFAULT, sizeof ifcfg.interface-1 );
	std::strncpy( ifcfg.user, cfg.get_username().c_str(), sizeof ifcfg.interface-1 );
	std::strncpy( ifcfg.password, cfg.get_passwd().c_str(), sizeof ifcfg.password-1 );
	std::strncpy( ifcfg.pppPort, NET_GPRS_LAYER,  sizeof ifcfg.pppPort-1 );
	switch( cfg.get_auth() ) {
	case auth_auto:
	default:
		std::strncpy( ifcfg.auth,"auto", sizeof ifcfg.auth-1 );
		break;
	case auth_pap:
		std::strncpy( ifcfg.auth,"PAP", sizeof ifcfg.auth-1 );
		break;
	case auth_chap:
		std::strncpy( ifcfg.auth,"CHAP", sizeof ifcfg.auth-1 );
		break;
	}
	ret = PML_DYNAMIC_CALL("libsvc_net.so", net_interfaceSet, ifcfg );
	if( ret ) {
		dlog_error( "Unable to GSM interface_2 set ret: %i  errno: %i errnostr %i",
			ret, errno, std::strerror( errno ) );
		return ret;
	}
	ret = PML_DYNAMIC_CALL("libsvc_net.so", 
			net_interfaceUp, const_cast<char *>("ppp1"), NET_IP_V4 );

	if (ret != 0) {
		dlog_error( "Unable to disconnect GSM module ret: %i  errno: %i errnostr %i",
			ret, errno, std::strerror( errno ) );
		return ret;
	}
	return ret;
}
/* ------------------------------------------------------------------ */ 
/** Disconnect from the GSM modem network 
 * @return error code if failed system code or gsm error code returned
 */
int disconnect( )
{
	// FIXME: OS API issue
	auto ret = PML_DYNAMIC_CALL("libsvc_net.so", 
			net_interfaceDown, const_cast<char *>("ppp1"), NET_IP_V4 );

	if (ret != 0) {
		dlog_error( "Unable to disconnect GSM module ret: %i  errno: %i errnostr %i",
			ret, errno, std::strerror( errno ) );
	}

	return ret;
}
/* ------------------------------------------------------------------ */
/** Function return signal strength or error if failed
 * @return signal strength or error code if failed
 */
int get_signal_strength() 
{
	const auto rssi  = PML_DYNAMIC_CALL("libsvc_net.so", net_gprsGetRssi );
	return rssi.rssi;
}
/* ------------------------------------------------------------------ */ 
}}}
/* ------------------------------------------------------------------ */
