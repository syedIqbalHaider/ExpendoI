/*
 * =====================================================================================
 *
 *       Filename:  bluetooth_utility.cpp
 *
 *    Description:  Bluetooth extra utility
 *
 *        Version:  1.0
 *        Created:  16.03.2015 13:33:19
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lucjan Bryndza (LB), lucck
 *   Organization:  
 *
 * =====================================================================================
 */
#include <fstream>
#include <cstdio>
#include <bluetooth/bluetooth.h>
#include "dynlib_loader.hpp"
#include "bluetooth_utility.hpp"
#include <algorithm>
/* ------------------------------------------------------------------ */ 
namespace com_verifone_pml {
namespace net {
namespace bluetooth {
namespace detail {
/* ------------------------------------------------------------------ */ 
namespace {
	//L2cap file
	static constexpr auto l2cap_info = "/sys/class/bluetooth/l2cap";
	// Parse one line 
	std::string parse_one( const std::string &line, unsigned chckstate ) 
	{
		char daddr[18], saddr[18];
		unsigned state, psm, dcid, scid, imtu, omtu, sec_level;
		int num;
		num = std::sscanf(line.c_str(), "%17s %17s %d %d 0x%04x 0x%04x %d %d %d",
			daddr, saddr, &state, &psm, &dcid, &scid, &imtu, &omtu, &sec_level);
		if( num < 9 ) return std::string();
		if( state != chckstate ) return std::string();
		bdaddr_t addr;
		PML_DYNAMIC_CALL( "libbluetooth.so", str2ba, saddr, &addr );
		std::reverse( addr.b, addr.b+sizeof(addr.b) );
		PML_DYNAMIC_CALL( "libbluetooth.so", ba2str, &addr, saddr );
		return saddr;
	}
}
/* ------------------------------------------------------------------ */ 
/** Get list L2CAP connected devices 
	* @param[out] List of devices ids
	* @return error code
	*/
int list_connected( std::vector<std::string>& devices ) noexcept
{
	std::string line;
	std::ifstream in( l2cap_info );
	if( !in.good() ) return -1;
	while( std::getline( in, line ) ) 
	{
		auto str = parse_one( line, BT_CONNECTED );
		if( !str.empty() ) {
			devices.push_back( str );
		}
	}
	return 0;
}
/* ------------------------------------------------------------------ */ 
/** Return first connected bluetooth device
	* @param[out] dev Output device
	* @return error code
	*/
int first_connected( std::string &dev ) noexcept
{
	std::string line;
	std::ifstream in( l2cap_info );
	if( !in.good() ) return -1;
	if( std::getline( in, line ) ) 
	{
		dev = parse_one( line, BT_CONNECTED );
		if (dev.size()) return true;
	}
	return false;
}
/* ------------------------------------------------------------------ */ 
/**  Device is connected */
int is_connected( const std::string& device ) noexcept
{	
	std::string line;
	std::ifstream in( l2cap_info );
	if( !in.good() ) return -1;
	while( std::getline( in, line ) ) 
	{
		auto str = parse_one( line, BT_CONNECTED );
		if( str == device )
			return true;
	}
	return false;
}
/* ------------------------------------------------------------------ */ 

}}}}
