#include <string.h>
#include <stdio.h>

#include <libpml/pml_os.h>

int get_counter(int index, struct OS_COUNTER * counter)
{
    int result = -1;
    if (counter != NULL)
    {
        struct diag_counter_info info;
        int res = diag_counter_get_info((short)index, &info);
        if (res == 0)
        {
            strcpy(counter->name, info.text);
            counter->value = info.value;
            counter->group = info.groups;
            // counter->index = info.index;
            result = 0;
        }
    }
    else
    {
        errno = EINVAL;
    }
    return result;
}

int is_counter_defined(int index)
{
    struct diag_counter_info info;
    if (diag_counter_get_info((short)index, &info) == 0)
    {
        char defName[20];
        snprintf(defName, sizeof(defName)-1, "App Counter %03d", index);
        if (strcmp(info.text, defName)) return 1;
    }
    return 0;
}

int is_custom_counter_defined(int index, const char * name)
{
    if (name && strlen(name))
    {
        struct diag_counter_info info;
        if (diag_counter_get_info((short)index, &info) == 0)
        {
            if (!strcmp(info.text, name)) return 1;
        }
        return 0;
    }
    return is_counter_defined(index);
}

int set_counter(int index, const struct OS_COUNTER * counter)
{
    int result = -1;
    if (counter != NULL)
    {
        struct diag_counter_info info;
        memset(&info, 0, sizeof(info));
        strncpy(info.text, counter->name, sizeof(info.text)-1);
        info.value = counter->value;
        info.groups = counter->group;
        result = diag_counter_set_info((short)index, &info);
    }
    else
    {
        errno = EINVAL;
    }
    return result;
}

int increase_counter(int index, int count)
{
    return diag_counter_add(index, count);
}

int decrease_counter(int index, int count)
{
    return diag_counter_sub(index, count);
}

