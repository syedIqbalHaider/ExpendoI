/*
 * =====================================================================================
 *
 *       Filename:  net_api.cpp
 *
 *    Description:  NET abstracted API for VOS platform
 *
 *        Version:  1.0
 *        Created:  02/17/2014 12:30:08 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lucjan Bryndza (lb), Lucjan_B1@verifone.com
 *   Organization:  VERIFONE
 *
 * =====================================================================================
 */
#include <libpml/pml_abstracted_api.h>
#include <libpml/pml.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/netdevice.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <net/if_arp.h>
#include <errno.h>
#include <unistd.h>
#include <cstring>
#include <cstdio>
#include <cstring>
#include <liblog/logsys.h>
#include "netlink_helper.hpp"
#include <svcmgr/svc_net.h>
#include <arpa/inet.h>
#include <iostream>
#include "dynlib_loader.hpp"
/* ------------------------------------------------------------------ */
namespace com_verifone_pml {
namespace net {
/* ------------------------------------------------------------------ */
namespace {
	//! Max interfaces used in API
	const int CONFIG_MAX_INTERFACES = 32;
}
/* ------------------------------------------------------------------ */ 
namespace {
	static const char * DEFAULT_IFC = "eth0";
	bool get_iface_config(const std::string & ifcn, netIfconfig & cfg)
	{
		const char *ifc_name = NULL;
		if (ifcn.size()) ifc_name = ifcn.c_str();
		else ifc_name = DEFAULT_IFC;
		memset(&cfg, 0, sizeof cfg);
		//FIXME: Stupid API should be const char in defs not char (OSTeam CRAP)
		cfg = PML_DYNAMIC_CALL("libsvc_net.so", net_interfaceGet, const_cast<char*>(ifc_name) );
		if (errno)
		{
			dlog_alert( "Unable to get ifc config fallback for %s, errno %d, fallback to %s", ifc_name, errno, DEFAULT_IFC );
			#if 0
			ifc_name = DEFAULT_IFC;
			std::memset( &cfg, 0 , sizeof cfg );
			//FIXME: Stupid API should be const char in defs not char (OSTeam CRAP)
			cfg = PML_DYNAMIC_CALL( "libsvc_net.so", net_interfaceGet, const_cast<char *>(ifc_name) );
			if (errno)
			{
				dlog_error("Still cannot get ifc config for default %s", DEFAULT_IFC);
				return false;
			}
			#else
			return false;
			#endif
		}
		return true;
	}
	/*
	int check_ifc_name(const std::string & ifc_name)
	{
		struct netIfconfigList if_list = net_interfaceStatusList();
		if( errno ) {
			dlog_error("Cannot get network iface list, error %d", errno);
			return -1;
		}
		dlog_msg("Looking for network iface %s", ifc_name.c_str();
		bool found = false;
		for (int i = 0; i < if_list.ifconfig_count; ++i)
		{
			if (!strnicmp(if_list.ifconfig[i].interface, ifc_name.c_str())) {
				found = true;
				break;
			}
		}
		if (if_list.ifconfig_count) free (if_list.ifconfig);
		if (if_list.ifconfigIpv6_count) free (if_list.ifconfigIpv6);
	}
	*/
}
/* ------------------------------------------------------------------ */
namespace detail {
	//! Calculate netmask from bd address
	std::string calc_broadcast( const std::string& addr, const std::string& netmask ) {
		std::string ret;
		in_addr host, mask, broadcast;
		if( inet_pton( AF_INET, addr.c_str(), &host ) == 1 &&
				 inet_pton( AF_INET, netmask.c_str(), &mask ) == 1 ) {
			broadcast.s_addr = host.s_addr | ~mask.s_addr;
			ret.resize( INET_ADDRSTRLEN );
			if( inet_ntop( AF_INET, &broadcast, &ret[0], INET_ADDRSTRLEN ) == NULL ) {
				ret.resize(0);
			}
		}
		dlog_msg("Broadcast addr calculation based on ip and mask %s", ret.c_str() );
		return ret;
	}
}
/* ------------------------------------------------------------------ */
/** Enum available network interface
* @param[out] if_names Interface list names
* @return Network interface count
*/
int enum_interfaces ( std::list<std::string>& if_names )
{
	int sock = socket( AF_INET, SOCK_STREAM, 0 );
	if( sock < 0 ) {
		return PML_FAILURE;
	}
	struct ifconf ifconf;
	struct ifreq ifreq[CONFIG_MAX_INTERFACES];
	int interfaces;
	ifconf.ifc_buf = reinterpret_cast<char *> ( ifreq );
	ifconf.ifc_len = sizeof ifreq;

	if (ioctl(sock, SIOCGIFCONF, &ifconf) == -1) {
		close( sock );
		return PML_FAILURE;
	}
	interfaces = ifconf.ifc_len / sizeof(ifreq[0]);
	for (int i = 0; i < interfaces; i++) {
		if_names.push_back( ifreq[i].ifr_name );
	}
	close( sock );
	return interfaces;
}
/* ------------------------------------------------------------------ */
/** Function gets interface detailed information by name
	* @param[in] ifc_name Interface input name
	* @param[out] info Returned interface information
	* @return Error code if successfull */
int get_interface_info( const std::string& ifc_name, interface_info& info )
{
	std::string ifcn(ifc_name);
	if( ifcn.empty() ) {
		ifcn = DEFAULT_IFC;
	}
	int sock = socket( AF_INET, SOCK_STREAM, 0 );
	if( sock < 0 ) {
		return PML_FAILURE;
	}
	ifreq ifr;
	std::memset( &ifr, 0, sizeof ifr );
	std::strncpy( ifr.ifr_name, ifcn.c_str(), IFNAMSIZ );
	if( ioctl( sock, SIOCGIFFLAGS, &ifr ) == -1 ) {
		close(sock);
		return PML_FAILURE;
	}
	info.up = (ifr.ifr_flags & (IFF_UP|IFF_RUNNING)) == (IFF_UP|IFF_RUNNING);
	info.point_to_point = ifr.ifr_flags & IFF_POINTOPOINT;
	info.multicast = ifr.ifr_flags & IFF_MULTICAST;
	info.loopback = ifr.ifr_flags & IFF_LOOPBACK;
	if( ioctl( sock, SIOCGIFMETRIC, &ifr ) == -1 ) {
		close(sock);
		return PML_FAILURE;
	}
	info.metric = ifr.ifr_metric;
	if( ioctl( sock, SIOCGIFMTU, &ifr ) == -1 ) {
		close(sock);
		return PML_FAILURE;
	}
	info.mtu = ifr.ifr_mtu;
	if( ioctl( sock, SIOCGIFHWADDR, &ifr ) == -1 ) {
		close(sock);
		return PML_FAILURE;
	}
	{
		if( ifr.ifr_hwaddr.sa_family == ARPHRD_ETHER ) {
			char mac[32];
			const unsigned char* bmac = reinterpret_cast<const unsigned char*>( ifr.ifr_hwaddr.sa_data );
			std::snprintf( mac, sizeof mac, "%02X:%02X:%02X:%02X:%02X:%02X",
					bmac[0],bmac[1],bmac[2],bmac[3],bmac[4],bmac[5] );
			info.hwaddr = mac;
		}
	}

	char ip[INET_ADDRSTRLEN];
	info.name = ifr.ifr_name;

	if( ioctl( sock, SIOCGIFADDR, &ifr ) == -1 ) {
		close(sock);
		return PML_FAILURE;
	}
	{
		sockaddr_in *address = reinterpret_cast<sockaddr_in*>( &ifr.ifr_addr );
		if( inet_ntop( AF_INET, &address->sin_addr, ip, sizeof ip ) ) {
			info.addr = ip;
		}
	}
	if( ioctl( sock, SIOCGIFBRDADDR, &ifr ) == -1 ) {
		close(sock);
		return PML_FAILURE;
	}
	{
		sockaddr_in *address = reinterpret_cast<sockaddr_in*>( &ifr.ifr_broadaddr );
		if( inet_ntop( AF_INET, &address->sin_addr, ip, sizeof ip ) ) {
			info.bdaddr = ip;
		}
	}
	if( ioctl( sock, SIOCGIFNETMASK, &ifr ) == -1 ) {
		close(sock);
		return PML_FAILURE;
	}
	{
		sockaddr_in *address = reinterpret_cast<sockaddr_in*>( &ifr.ifr_netmask );
		if( inet_ntop( AF_INET, &address->sin_addr, ip, sizeof ip ) ) {
			info.netmask = ip;
		}
	}

	close( sock );

	netIfconfig cfg;
	if (!get_iface_config(ifcn, cfg)) {
		return PML_FAILURE;
	}
	info.dhcp = cfg.usedhcp;

	return PML_SUCCESS;
}
/* ------------------------------------------------------------------ */
/** Function return if not local network interfcace is ready
	* and configured. It can be used for determine connection state
	* @param[in] Interface name. If name is null autodetection is made
	* @return if negative error or @see link_state for link status
	*/
int link_is_up( const char *ifc_name )
{
	int sock = socket( AF_INET, SOCK_STREAM, 0 );
	if( sock < 0 ) {
		return PML_FAILURE;
	}
	struct ifreq ifreq[CONFIG_MAX_INTERFACES];
	int interfaces;

	if( ifc_name ) {
		interfaces = 1;
		std::memset( &ifreq[0], 0, sizeof ifreq[0] );
		std::strncpy( ifreq[0].ifr_name, ifc_name, IFNAMSIZ );
	} else {
		struct ifconf ifconf;
		ifconf.ifc_buf = reinterpret_cast<char *>(ifreq );
		ifconf.ifc_len = sizeof ifreq;
		if (ioctl(sock, SIOCGIFCONF, &ifconf) == -1) {
			close( sock );
			return PML_FAILURE;
		}
		interfaces = ifconf.ifc_len / sizeof(ifreq[0]);
	}

	//Walk through interfaces
	for( int i = 0; i < interfaces; ++i ) {
		if( ioctl( sock, SIOCGIFFLAGS, &ifreq[i] ) == -1 ) {
			close( sock );
			return PML_FAILURE;
		}
		dlog_msg("Interface '%s', flags %Xh", ifreq[i].ifr_name, ifreq[i].ifr_flags);
		if( !(ifreq[i].ifr_flags & IFF_LOOPBACK) &&
			( (ifreq[i].ifr_flags & (IFF_UP|IFF_RUNNING)) == (IFF_UP|IFF_RUNNING ) )
		) {
			close( sock );
			return link_state_up;
		}
	}
	close( sock );
	return link_state_down;
}
/* ------------------------------------------------------------------ */

/** Create netif listener for network events
	*  @param[in,out] netif_event Netif event for listen
	*  @return Error status
	*/
int netif_events_listener_create( event_item& netif_event )
{
	const int netif_h = netlink_utils::create_netlink_listener();
	if( netif_h > 0 ) {
		netif_event.event_id =  events::netlink;
		netif_event.evt.netlink._fd = netif_h;
		netif_event.evt.netlink.n_ifcs = 0;
		return PML_SUCCESS;
	} else {
		return PML_FAILURE;
	}
}
/* ------------------------------------------------------------------ */
/** Destroy netif listener for network events
	*  @param[in,out] netif_event Netif event for listen
	*  @return Error status
	*/
int netif_events_listener_destroy( event_item& netif_event )
{
	if( netif_event.event_id == events::netlink ) {
		netif_event.evt.netlink.n_ifcs = 0;
		const auto fd = netif_event.evt.netlink._fd;
		netif_event.evt.netlink._fd = -1;
		return netlink_utils::close_netlink_listener( fd );
	}
	else {
		dlog_error("Error this is not a netlink socket");
		errno = EINVAL;
		return PML_FAILURE;
	}
}
/* ------------------------------------------------------------------ */
/** Configure network interface using non dhcp mode
	*  if nullptr is assigned it use DHCP mode
	* */
int config_interface( const interface_config* ifc )
{
	return config_interface( std::string(""), ifc);
}
int config_interface( const std::string & ifcn, const interface_config* ifc )
{
	netIfconfig cfg;
	int ret = -1;
	if (!get_iface_config(ifcn, cfg)) {
		return ret;
	}
	ret = PML_DYNAMIC_CALL( "libsvc_net.so", net_interfaceDown ,cfg.interface, NET_IP_V4 );
	dlog_msg("Interface down result %d, error %d", ret, errno);
	if( ifc == NULL ) {
		cfg.usedhcp = true;
		cfg.activate = true;
	} else {
		std::strncpy( cfg.local_ip, ifc->addr.c_str(), sizeof cfg.local_ip );
		std::strncpy( cfg.netmask,  ifc->netmask.c_str(), sizeof cfg.netmask );
		std::strncpy( cfg.gateway,  ifc->gateway.c_str(), sizeof cfg.gateway );
		std::strncpy( cfg.dns1,  ifc->dns1.c_str(), sizeof cfg.dns1 );
		std::strncpy( cfg.dns2,  ifc->dns2.c_str(), sizeof cfg.dns2 );
		std::strncpy( cfg.broadcast, detail::calc_broadcast( ifc->addr, ifc->netmask ).c_str(), sizeof cfg.broadcast );
		cfg.usedhcp = false;
		cfg.activate = true;
	}
	if( (ret=PML_DYNAMIC_CALL("libsvc_net.so", net_interfaceSet,  cfg ) ) != 0 ) {
		dlog_error( "Unable to set interface, error %d, errno %d", ret, errno );
		return ret;
	}
	if( (ret=PML_DYNAMIC_CALL( "libsvc_net.so", net_interfaceUp, cfg.interface, NET_IP_V4 ) ) != 0 ) {
		dlog_error( "Unable to set interface up, error %d, errno %d", ret, errno );
	}
	return ret;
}
/* ------------------------------------------------------------------ */
int is_interface_up()
{
	return is_interface_up( std::string("") );
}
int is_interface_up( const std::string & ifcn )
{
	netIfconfig cfg;
	if (!get_iface_config(ifcn, cfg)) {
		return -1;
	}
	cfg = PML_DYNAMIC_CALL("libsvc_net.so", net_interfaceStatus, cfg.interface );
	if (errno)
	{
		// Network is down!
		dlog_alert("Iface is down!");
		return 0;
	}
	dlog_msg("Iface is up!");
	return 1;
}
/* ------------------------------------------------------------------ */ 
int interface_up()
{
	return interface_up( std::string("") );
}
int interface_up( const std::string & ifcn )
{
	if (!is_interface_up( ifcn )) {
		netIfconfig cfg;
		int ret = -1;
		if (!get_iface_config(ifcn, cfg)) {
			return ret;
		}
		if (!cfg.activate) {
			cfg.activate = true;
			if( (ret=PML_DYNAMIC_CALL("libsvc_net.so", net_interfaceSet,  cfg ) ) != 0 ) {
				dlog_error( "Unable to set interface, error %d, errno %d", ret, errno );
				return ret;
			}
			ret = PML_DYNAMIC_CALL( "libsvc_net.so", net_interfaceUp,cfg.interface, NET_IP_V4 );
			dlog_msg("Interface up result %d, error %d", ret, errno);
			return ret;
		}
	}
	return 0;
}

/* ------------------------------------------------------------------ */ 
int interface_down()
{
	return interface_down( std::string("") );
}
int interface_down( const std::string & ifcn )
{
	if (is_interface_up( ifcn ))
	{
		netIfconfig cfg;
		int ret = -1;
		if (!get_iface_config(ifcn, cfg)) {
			return ret;
		}
		ret = PML_DYNAMIC_CALL( "libsvc_net.so", net_interfaceDown ,cfg.interface, NET_IP_V4 );
		dlog_msg("Interface down result %d, error %d", ret, errno);
		return ret;
	}
	return 0;
}
/* ------------------------------------------------------------------ */ 
int get_interface_config( const std::string & ifc_name, interface_config & ifc )
{
	const char * ifcn = NULL;
	if (ifc_name.size()) ifcn = ifc_name.c_str();
	else ifcn = DEFAULT_IFC;
	//FIXME: Stupid API should be const char in defs not char ( OSTeam CRAP )
	netIfconfig cfg = PML_DYNAMIC_CALL("libsvc_net.so", net_interfaceStatus, const_cast<char *>(ifcn) );
	if (errno != 0)
	{
		dlog_error("Cannot get interface info, errno %d", errno);
		return PML_FAILURE;
	}
	ifc.addr.assign(cfg.local_ip);
	ifc.netmask.assign(cfg.netmask);
	ifc.gateway.assign(cfg.gateway);
	ifc.dns1.assign(cfg.dns1);
	ifc.dns2.assign(cfg.dns2);
	return 0;
}
/* ------------------------------------------------------------------ */
}	// namespace net
}	//namespace com_verifone_pml
