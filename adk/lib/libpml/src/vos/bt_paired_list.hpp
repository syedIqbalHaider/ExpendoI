/*
 * =====================================================================================
 *
 *       Filename:  bt_paired_list.hpp
 *
 *    Description:  Paired list implementation for VOS
 *
 *        Version:  1.0
 *        Created:  11.03.2015 10:08:00
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lucjan Bryndza (LB), lucck
 *   Organization:  
 *
 * =====================================================================================
 */

#pragma once

#include <boost/serialization/base_object.hpp>
#include <boost/tuple/tuple.hpp>
#include <list>
#include <string>
#include <algorithm>
#include <libpml/pml_net_bluetooth.h>
#include "generic_profile_store.hpp"

namespace com_verifone_pml {
namespace net {
namespace bluetooth {
namespace internal {

	//! Bluetooth paired list storage
	class bt_paired_list : public net::detail::generic_profile_store<device> {
	public:
		bt_paired_list() 
			: generic_profile_store( "bt_pair_list.oarch" )
		{}
		//Check if contains thge name
		bool contains( const std::string& addr ) const 
		{
			const auto it = std::find_if( m_cfg.begin(), m_cfg.end(), 
				[&]( const container::value_type& s1 ) { return s1.second.address() == addr; } );
			return it!=m_cfg.end();
		}
	};

}}}}


