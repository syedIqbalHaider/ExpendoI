/*
 * evt_key.hpp
 *
 *  Created on: 14-03-2013
 *      Author: lucck
 */
/* ---------------------------------------------------------------- */
#ifndef COM_VERIFONE_PML_EVENTS_EVT_KEY_HPP_
#define COM_VERIFONE_PML_EVENTS_EVT_KEY_HPP_

/* ---------------------------------------------------------------- */
#include <openssl/md5.h>
#include <cstring>
#include <string>
#include <sstream>
#include <iomanip>


/* ---------------------------------------------------------------- */
namespace com_verifone_pml {
namespace internal {

/* ---------------------------------------------------------------- */
//Event pid identifer
struct evt_key
{
	//Constructor
	evt_key( const char *name )
	{
		::MD5( reinterpret_cast<const unsigned char*>(name), std::strlen(name), id );
	}
	evt_key( const unsigned char *key )
	{
		std::memcpy( id, key, MD5_DIGEST_LENGTH );
	}
	//Digest shortuct
	unsigned char id[MD5_DIGEST_LENGTH];
	//Get string representation
	operator std::string() const
	{
		using namespace std;
		//using namespace boost::lambda;
		ostringstream os;
		os << hex << setfill( '0' );
		for( size_t i=0; i< sizeof(id); ++i )
			os << setw(2) << int(id[i]);
		return os.str();
	}
};

/* ---------------------------------------------------------------- */
}}
/* ---------------------------------------------------------------- */

#endif /* EVT_KEY_HPP_ */
