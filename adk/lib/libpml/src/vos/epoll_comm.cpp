/*
 * =====================================================================================
 *
 *       Filename:  epoolevent.cpp
 *
 *    Description:  EPOLL system event implemenation
 *
 *        Version:  1.0
 *        Created:  02/27/2013 10:05:36 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lucjan Bryndza <lucjan_b1@verifone.com>
 *   Organization:  VERIFONE
 :
 * =====================================================================================
 */

#include "epoll_comm.hpp"
#include <boost/foreach.hpp>
#include <liblog/logsys.h>
#include <sys/epoll.h>
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include "netlink_helper.hpp"
#include <boost/bind.hpp>
/* ---------------------------------------------------------------- */
namespace com_verifone_pml {
namespace internal {
namespace {
	void netlink_notify_callback( event_item& ev, const char* if_name, bool link ) 	  {
		if( ev.evt.netlink.n_ifcs < sizeof(ev.evt.netlink.ifc)/sizeof(ev.evt.netlink.ifc[0]) )
		{
			std::strncpy( ev.evt.netlink.ifc[ ev.evt.netlink.n_ifcs ].name, if_name, 
				sizeof ev.evt.netlink.ifc[0].name );
			ev.evt.netlink.ifc[ ev.evt.netlink.n_ifcs++ ].isup = link;
		}
		else {
			dlog_error("Link events overflowed");
		}
	}
}
/* ---------------------------------------------------------------- */
//Control event handler
int epoll_comm::ctl( const event_item &event, int op )
{
    ::epoll_event epev;
    if( event.event_id > events::activate )
    {
        errno = EINVAL;
        return -1;
    }
    if( event.evt.com.fd == event_com_item::any )
    {
        errno = EINVAL;
        return -1;
    }
    epev.data.fd = event.evt.com.fd;
    if( event.event_id == events::timer )
    {
        epev.events = EPOLLIN;
    }
    else if( event.event_id == events::netlink ) 
    {
        epev.events = EPOLLIN;
    }
    else if( event.event_id == events::switch_sys ) 
    {
        epev.events = EPOLLIN;
    }
    else if( event.event_id == events::switch_tripped )
    {
        epev.events = EPOLLIN;
    }
    else
    {
      epev.events  = event.evt.com.flags;
    }
    return epoll_ifc::do_ctl( epev, event, op );
}

/* ---------------------------------------------------------------- */
/** Update the PML event according to the System event
    @param[out] event Updated system even
  	@param[in] sys_event Incoming system event
  	@return None
*/
int epoll_comm::update_event( event_item& event, const ::epoll_event &sys_event )
{
	if( event.event_id == events::timer )
	{
		if( sys_event.events & EPOLLIN )
		{
			uint64_t nexpires = 0;
			if( ::read( event.evt.tmr.id, &nexpires, sizeof nexpires) != sizeof(nexpires) )
			{
				return -1;
			}
		}
		else
		{
			errno = EINVAL;
			return -1;
		}
	}
	else if( event.event_id == events::netlink ) 
	{
		if( sys_event.events & EPOLLIN ) 
		{
			using namespace net::netlink_utils;
			using boost::bind;
			event.evt.netlink.n_ifcs = 0;
			if( 
			parse_and_receive_netlink_msg(
				event.evt.netlink._fd,
				boost::bind( netlink_notify_callback, boost::ref(event), _1, _2 )
			) < 0 ) {
				return -1;
			}
		}
	}
	else if( event.event_id == events::switch_sys ) 
	{
		if( sys_event.events & EPOLLIN )  {
			char dummy;
			if( ::read( event.evt.sysswitch._fd, &dummy, sizeof(dummy) ) != sizeof(dummy) ) {
				return -1;
			}
		}
	}
	else
	{
		event.evt.com.flags = sys_event.events;
	}
	return 0;
}
/* ---------------------------------------------------------------- */
}   /* internal  */
}   /* com_verifone_pml  */

