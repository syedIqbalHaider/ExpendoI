/*
 * events_queue.cpp
 *
 *  Created on: 12-03-2013
 *      Author: lucck
 */
/* ---------------------------------------------------------------- */
#include "events_queue.hpp"
#include <sys/epoll.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <boost/noncopyable.hpp>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <liblog/logsys.h>

/* ---------------------------------------------------------------- */
namespace com_verifone_pml {
namespace internal {
/* ---------------------------------------------------------------- */


/* ---------------------------------------------------------------- */
namespace
{
	//IPC task list shared memory segment list
	const char sock_prefix[] = "/tmp/vfi/pml/";
}
/* ---------------------------------------------------------------- */
//Constructor
events_queue::events_queue( const evt_key &key, bool server_mode )
	: m_conn_key(key), m_conn_fd(-1), m_fd(std::make_pair(-1,-1)), m_server(server_mode)
{
	if( server_mode )
		m_fd = create_unnamed_wait_sock( m_conn_key );

}

/* ---------------------------------------------------------------- */
int events_queue::receive_equal( int socket, void *buffer, size_t len, int flags )
{
//	dlog_msg("events_queue::receive_equal at socket %d", socket);
	
	char *sptr = static_cast<char*>( buffer );
	size_t p;
	for( p=0; p<len; )
	{
		int ret = ::recv( socket, sptr, len-p, flags );
		if( ret <=0 ) { 
//			dlog_msg("events_queue::return ret=%d", ret);
			return ret; 
		}
		else { p+=ret; sptr+=ret; }
	}
	
//	dlog_msg("events_queue::return p=%d", p);
	
	return p;
}


/* ---------------------------------------------------------------- */
//Create connection server
int events_queue::create_connection( const std::string& sock_name )
{
	sockaddr_un remote;
	const int fd = socket( AF_UNIX, SOCK_STREAM, 0 );
	if( fd < 0 ) return fd;
	remote.sun_family = AF_UNIX;
	remote.sun_path[0] = '\0';
//	std::strncpy(remote.sun_path+1,  sock_prefix , sizeof(remote.sun_path)-1);
//	std::strncat(remote.sun_path+1,  sock_name.c_str() , sizeof(remote.sun_path)-1);
	snprintf(remote.sun_path+1, sizeof(remote.sun_path)-1, "%s%s", sock_prefix, sock_name.c_str());
	
	if( ::connect(fd, reinterpret_cast<sockaddr*>(&remote),sizeof(remote.sun_family) + strlen(remote.sun_path+1)+1) == -1 )
	{
		close(fd);
	}
	return fd;
}

/* ---------------------------------------------------------------- */
// Create waiting server
events_queue::sockets_pair events_queue::create_unnamed_wait_sock( const std::string& sock_name )
{
	epoll_event event;
	sockaddr_un local;
	std::pair <int, int> ret_socks( -1, -1 );
	do
	{
		if((ret_socks.first = epoll_create1(0)) == -1)
		{
			break;
		}

		if ((ret_socks.second = socket(AF_UNIX, SOCK_STREAM, 0)) == -1)
		{
			break;
		}
		local.sun_path[0] = '\0';
		local.sun_family = AF_UNIX;
//		std::strncpy(local.sun_path+1, sock_prefix, sizeof(local.sun_path)-sizeof('\0')-sizeof('\0'));
//		std::strncat( local.sun_path+1, sock_name.c_str(), sizeof(local.sun_path)-sizeof('\0')-sizeof('\0') );
		snprintf(local.sun_path+1, sizeof(local.sun_path)-1, "%s%s", sock_prefix, sock_name.c_str());
		
		if( bind(ret_socks.second, reinterpret_cast<sockaddr*>(&local), sizeof(local.sun_family) + strlen(local.sun_path+1)+1) == -1 )
		{
			close(ret_socks.second);
			close(ret_socks.first);
			break;
		}
		if( listen(ret_socks.second, SOMAXCONN) == -1 )
		{
			close(ret_socks.second);
			close(ret_socks.first);
			break;
		}
		event.events = EPOLLIN;
		event.data.fd = ret_socks.second;
		if( epoll_ctl(ret_socks.first, EPOLL_CTL_ADD, ret_socks.second, &event) == -1 )
		{
			close(ret_socks.second);
			close(ret_socks.first);
			break;
		}
	} while(0);
	
//   dlog_msg("events_queue::create_unnamed_wait_sock, hdl=%d", ret_socks.first);
	
	return ret_socks;
}


/* ---------------------------------------------------------------- */
//Destructor
events_queue::~events_queue()
{
	if( m_fd.first > 0) ::close(m_fd.second);
	if( m_fd.second > 0) ::close(m_fd.first);
	if( m_conn_fd ) ::close( m_conn_fd );
}

/* ---------------------------------------------------------------- */
//POP the data
bool events_queue::pop( event_item &event )
{
	if( !m_server ) return true;

//	dlog_msg("events_queue::pop m_conn_fd=%d", m_conn_fd);

	if( m_conn_fd < 0 )
	{
		::epoll_event ev;
		m_conn_fd = ::accept( m_fd.second, NULL, NULL );
		if( m_conn_fd < 0 ) return true;
		ev.data.fd = m_conn_fd; ev.events = EPOLLIN|EPOLLERR;
		if( ::epoll_ctl( m_fd.first, EPOLL_CTL_ADD, m_conn_fd, &ev ) < 0 )
		{
			::close( m_conn_fd );
			m_conn_fd = -1;
			return true;
		}
	}
	if( m_conn_fd > 0 )
	{
		const int ret = receive_equal( m_conn_fd, &event, sizeof(event_item) );
		if( ret <= 0 )
		{
//			dlog_error("events_queue::pop receive_equal failed! ret=%d", ret);
			::epoll_ctl( m_fd.first, EPOLL_CTL_DEL, m_conn_fd, NULL );
			::close( m_conn_fd );
			m_conn_fd = -1;
			return true;
		}
		return false;
	}
	return true;
}
/* ---------------------------------------------------------------- */
//Insert value into the queue
bool events_queue::insert( const event_item &ev )
{
	//Only in client mode
	if( m_server) return true;
	if( m_conn_fd <= 0 )
	{
		m_conn_fd = create_connection( m_conn_key );
	}
	if( m_conn_fd  <= 0 )
	{
//		dlog_error("Cannot create connection, error %d, errno %d", m_conn_fd, errno);
		return true;
	}
	if( ::send( m_conn_fd, &ev, sizeof(ev), 0 ) != sizeof(ev) )
	{
		close( m_conn_fd );
		m_conn_fd = -1;
//		dlog_error("Cannot send, errno %d", errno);
		return true;
	}
	return false;
}

/* ---------------------------------------------------------------- */
//Check if the queue is empty
bool events_queue::empty( ) const
{
	if( !m_server ) return true;
	if( m_conn_fd <= 0 ) return true;
	static const std::size_t n_evt = 2;
	epoll_event evt[n_evt];
	const int ret = ::epoll_wait( m_fd.first, evt, n_evt, 0 );
	for( int e = 0; e < ret; ++e )
	{
		if( evt[e].data.fd == m_conn_fd  && (evt[e].events & EPOLLIN) )
		{
			return false;
		}
	}
	return true;
}
/* ---------------------------------------------------------------- */
} /* namespace internal */
} /* namespace com_verifone_pml */
/* ---------------------------------------------------------------- */
