/*
 * =====================================================================================
 *
 *       Filename:  wifi.cpp
 *
 *    Description:  WIFI PML implementation cpp file
 *
 *        Version:  1.0
 *        Created:  27.02.2015 10:48:49
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lucjan Bryndza (LB), lucck
 *   Organization:  
 *
 * =====================================================================================
 */

#include <libpml/pml_net_wifi.h>
#include <libpml/pml_net.h>
#include <liblog/logsys.h>
#include "wpa_supplicant_generator.hpp"
#include "wifi_profile_store.hpp"
#include "dynlib_loader.hpp"
#include <cstring>
#include <svcmgr/svc_net.h>
/* ------------------------------------------------------------------ */ 
namespace com_verifone_pml {
namespace net {
namespace wifi {
/* ------------------------------------------------------------------ */ 
namespace {

	//WIFI profile store
	detail::wifi_profile_store m_store;

}	//Unammed ns end

/* ------------------------------------------------------------------ */
void medium_info::str_to_mode( const char *auth, const char* algo )
{	
	//FIXME: Implement all available modes
	//FIXME: VOS api doesn't provide proper info about all wifi params
	if( !strcmp(auth,"WPA-PSK") && !strcmp(algo,"RSN") ) {
		m_algo = algo_aes_ccm;
		m_auth = auth_wpa2_psk;
	} else if( !strcmp(auth,"WPA") && !strcmp(algo,"RSN") ) {
		m_algo = algo_aes_ccm;
		m_auth = auth_wpa2_psk;
	} else if( !strcmp(auth,"WPA-PSK") && !strcmp(algo,"WPA") ) {
		m_algo = algo_aes_ccm;
		m_auth = auth_wpa_psk;
	} else if( !strcmp(auth,"WEP") && !strcmp(algo,"WEP1") ) {
		m_algo = algo_wep1;
		m_auth = auth_wep;
	} else if( !strcmp(auth,"WPA") && !strcmp(algo,"TKIP") ) {
		m_algo = algo_tkip;
		m_auth = auth_wpa_psk;
	} else {
		dlog_msg("Unknown auth %s unknown algo %s\n", auth, algo );
		m_algo = algo_unknown;
		m_auth = auth_unknown;
	}
}

/* ------------------------------------------------------------------ */
void medium_info::mode_to_str( auth auth, algo algo )
{
		//FIXME: Implement all available modes
	//FIXME: VOS api doesn't provide proper info about all wifi params
	switch (auth)
	{
		case auth_unknown: m_auth_desc.assign("Unknown"); break;
		case auth_none: m_auth_desc.assign("None"); break;
		case auth_wep: m_auth_desc.assign("WEP"); break;
		case auth_wep_shared: m_auth_desc.assign("WEP Shared"); break;
		case auth_wpa_psk: m_auth_desc.assign("WPA-PSK"); break;
		case auth_wpa: m_auth_desc.assign("WPA-EAP"); break;
		case wpa_none: m_auth_desc.assign("WPA"); break;
		case auth_wpa2_psk: m_auth_desc.assign("WPA2-PSK"); break;
	}
	switch (algo)
	{
		case algo_unknown: m_algo_desc.assign("Unknown"); break;
		case algo_none: m_algo_desc.assign("None"); break;
		case algo_default: m_algo_desc.assign("Autodetect"); break;
		case algo_wep1: m_algo_desc.assign("WEP1"); break;
		case algo_tkip: m_algo_desc.assign("TKIP"); break;
		case algo_wep128: m_algo_desc.assign("WEP"); break;
		case algo_aes_ccm: m_algo_desc.assign("CCM"); break;
	}
}

/* ------------------------------------------------------------------ */
int connect ( const std::string& ifc_name, const medium_config& wcfg, const interface_config* ifc )
{
	int ret {};
	netIfconfig cfg {};
	ret = PML_DYNAMIC_CALL( "libsvc_net.so",net_wifiStart, 0 );
	if( ret ) {
		dlog_error("net_wifiStart error %d, errno %d", ret, errno);
		return ret;
	}
	if( ifc == nullptr ) {
		cfg.usedhcp = true;
		cfg.activate = true;
	} else {
		std::strncpy( cfg.local_ip, ifc->addr.c_str(), sizeof cfg.local_ip );
		std::strncpy( cfg.netmask,  ifc->netmask.c_str(), sizeof cfg.netmask );
		std::strncpy( cfg.gateway,  ifc->gateway.c_str(), sizeof cfg.gateway );
		std::strncpy( cfg.dns1,  ifc->dns1.c_str(), sizeof cfg.dns1 );
		std::strncpy( cfg.dns2,  ifc->dns2.c_str(), sizeof cfg.dns2 );
		std::strncpy( cfg.broadcast, net::detail::calc_broadcast( ifc->addr, ifc->netmask ).c_str(), 
				sizeof cfg.broadcast );
		cfg.usedhcp = false;
		cfg.activate = true;
	}
	constexpr auto tmp_wpa_conf = "/tmp/pml_wpa_supplicant.conf";
	ret = detail::wpa_supplicant_generator( ifc_name, wcfg, tmp_wpa_conf );
	if( ret ) {
		dlog_error("wpa_generator error %d, errno %d", ret, errno);
		return ret;
	}
	std::strncpy(cfg.interface, ifc_name.c_str(), sizeof cfg.interface );
	std::strncpy( cfg.suppliconf, tmp_wpa_conf, sizeof cfg.suppliconf );
	ret = PML_DYNAMIC_CALL( "libsvc_net.so", net_interfaceSet, cfg );
	if( ret ) {
		dlog_error("net_interfaceSet error %d, errno %d", ret, errno);
		return ret;
	}
	if( (ret=PML_DYNAMIC_CALL("libsvc_net.so", net_interfaceUp, cfg.interface, NET_IP_V4 ) ) != 0 ) {
		if (errno != ENETDOWN)
		{
			dlog_error( "Unable to set interface up, error %d, errno %d", ret, errno );
			return ret;
		}
		// Ignore ENETDOWN, it's okay
	}
	ret = PML_DYNAMIC_CALL( "libsvc_net.so", net_wifiStart,  1 );
	if( ret ) {
		dlog_error("net_wifiStart error %d, errno %d", ret, errno);
	}
	// Wait until the interface is started...
	struct netIfconfig ifstate {};
	size_t numAttempts = 5;
	ret = -1;
	do {
		ifstate = PML_DYNAMIC_CALL( "libsvc_net.so", net_interfaceStatus, const_cast<char*>(ifc_name.c_str() ));
		if (errno) {
			dlog_error("net_interfaceStatus error %d", errno);
			if (errno != ENETDOWN && errno != ENONET) {
				break;
			}
			usleep(5000000);
		} else {
			ret = 0;
			break;
		}
	} while (--numAttempts > 0);
	return ret;
}
/* ------------------------------------------------------------------ */
int get_profiles_count( const std::string& /*ifc_name*/, int & profiles_count )
{
	profiles_count = m_store.get_count();
	return 0;
}
/* ------------------------------------------------------------------ */
int find_profile(const std::string& /*ifc_name*/,  const std::string &SSID, int &profile_id)
{
	const auto ret = m_store.find_profile( SSID );
	if( ret < 0 ) return ret;
	else { profile_id = ret; return 0; }
}
/* ------------------------------------------------------------------ */
int set_default_profile( const std::string& /*ifc_name,*/, int profile_id )
{
	auto ret =  m_store.set_default( profile_id );
	if( !ret ) m_store.save();
	return ret;
}
/* ------------------------------------------------------------------ */
int get_default_profile( const std::string& ifc_name, medium_config& cfg )
{
	auto def_profile = m_store.get_default();
	if (def_profile < 0) return def_profile;
	auto ret = m_store.get( def_profile, cfg );
	return 0;
}
/* ------------------------------------------------------------------ */
int get_default_profile_id( const std::string& ifc_name, int &profile_id)
{
	profile_id = m_store.get_default();
	if (profile_id < 0) return -1;
	return 0;
}
/* ------------------------------------------------------------------ */
int add_profile( const std::string& /*ifc_name*/, const medium_config& wcfg, int &profile_id )
{
	profile_id = m_store.add( wcfg );
	if (profile_id >= 0)
	{
		m_store.save();
		return 0;
	}
	return -1;
}
/* ------------------------------------------------------------------ */
int connect( const std::string& ifc_name, int profile_id )
{
	medium_config wcfg;
	auto ret = m_store.get( profile_id, wcfg );
	if( ret < 0 ) return ret;
	//!TODO: Add IP configuration param
	ret = connect( ifc_name, wcfg, nullptr );
	return ret;
}
/* ------------------------------------------------------------------ */
int get_profile_info( const std::string& /*ifc_name*/, network_profile& prof, int profile_id /*= -1*/ )
{
	medium_config wcfg;
	auto ret = m_store.get( profile_id, wcfg );
	if( ret ) return ret;
	prof = network_profile( wcfg, profile_id, profile_id==m_store.get_default() );
	return ret;
}
/* ------------------------------------------------------------------ */
int get_profile_info( const std::string& /*ifc_name*/, std::vector<network_profile> & prof )
{
	int cnt = m_store.get_count();
	int profile_id = 0;
	medium_config wcfg;
	while (cnt > 0) {
		if (!m_store.get( profile_id, wcfg )) {
			prof.push_back( network_profile( wcfg, profile_id, profile_id==m_store.get_default() ) );
			--cnt;
		}
		++profile_id;
	}
	return 0;
}
/* ------------------------------------------------------------------ */
int get_current_ap_info( const std::string& ifc_name, medium_info &ap )
{
	errno = 0;
	const auto ret = PML_DYNAMIC_CALL("libsvc_net.so", net_wifiInfo, const_cast<char*>(ifc_name.c_str() ));
	if( errno ) {
		return -1;
	}
	ap = medium_info( ret.bssid, ret.ssid, ret.signalStrength );
	return 0;
}
/* ------------------------------------------------------------------ */
int delete_profile( const std::string& /*ifc_name*/, int profile_id )
{
	auto ret =  m_store.remove( profile_id );
	if( ret < 0 ) return ret;
	m_store.save();
	return ret;
}
/* ------------------------------------------------------------------ */ 
int get_available_ap_list( const std::string& ifc_name, std::vector<medium_info> & ap_list )
{
	const auto survc = PML_DYNAMIC_CALL("libsvc_net.so", net_wifiSiteSurvey);
	if( errno ) {
		dlog_error("net_wifiSiteSurvey errno %d", errno);
		if (errno == ENETDOWN)
		{
			const auto rc = PML_DYNAMIC_CALL("libsvc_net.so", net_wifiStart, 0 ); // start wifi iface if not started yet
			if( rc != 0 ) {
				dlog_error("net_wifiStart result %d, errno %d", rc, errno);
				return -1;
			}
			return get_available_ap_list( ifc_name, ap_list );
		}
		return -1;
	}
	for( int c=0; c < survc.siteSurvey_count; ++c ) {
		const auto& item = survc.siteSurvey[c];
		medium_info winfo (item.bssid, item.ssid, item.signalStrength, item.authMode, item.encrypType ); 
		ap_list.push_back( winfo );
	}
	if (survc.siteSurvey && survc.siteSurvey_count > 0) free(survc.siteSurvey);
	return 0;
}
/* ------------------------------------------------------------------ */
int enable( bool enable )
{
	if (enable) return PML_DYNAMIC_CALL( "libsvc_net.so",net_wifiStart, 0 ); // Only power on the module
	else return PML_DYNAMIC_CALL( "libsvc_net.so",net_wifiStop, 1 ); // Disable the module and WPA supplicant
}
/* ------------------------------------------------------------------ */
}}}
