/*
 * =====================================================================================
 *
 *       Filename:  security_latch.cpp
 *
 *    Description:  Latch support in the PML
 *
 *        Version:  1.0
 *        Created:  05.11.2014 12:25:14
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lucjan Bryndza (LB), lucck
 *   Organization:  
 *
 * =====================================================================================
 */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <libpml/pml.h>
#include <libpml/pml_abstracted_api.h>
#include <liblog/logsys.h>
/* ------------------------------------------------------------------ */ 
namespace com_verifone_pml {
namespace latch {
/* ------------------------------------------------------------------ */
namespace {
	static constexpr auto SERVICE_SWITCH_VALUE = "/sys/devices/platform/sysmode-button/wake_apps";
}
/* ------------------------------------------------------------------ */ 
/** PML special security devices
* @param[in,out] latch_event Latch event object
* @return Error status
*/
int sysswitch_events_listener_create( event_item& event ) 
{
	const auto fd = open( SERVICE_SWITCH_VALUE, O_RDONLY );	
	if( fd < 0 ) {
		return PML_FAILURE;
	}
	event.event_id = events::switch_sys;
	event.evt.sysswitch._fd = fd;
	return PML_SUCCESS;
}
/* ------------------------------------------------------------------ */ 
/** Destrroy latch object
	* @param[in,out] latch_event Latch object 
	* @return Error status
	*/
int sysswitch_events_listener_destroy( event_item& event )
{
	const auto fd = event.evt.netlink._fd;
	event.evt.netlink._fd = -1;
	if( event.event_id == events::switch_sys ) {
		if( ::close( fd ) < 0 ) {
			return PML_FAILURE;
		}
		return PML_SUCCESS;
	}
	dlog_error("Error this is not sys switch socket");
	errno = EINVAL;
	return PML_FAILURE;
}
/* ------------------------------------------------------------------ */ 
}
}
