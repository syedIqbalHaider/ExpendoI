/*
 * =====================================================================================
 *
 *       Filename:  wpa_supplicant_generator.hpp
 *
 *    Description:  Wpa supplicant config file generator
 *
 *        Version:  1.0
 *        Created:  16.02.2015 12:27:44
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lucjan Bryndza (LB), lucck
 *   Organization:  
 *
 * =====================================================================================
 */
#include <string>
/* ------------------------------------------------------------------ */ 
namespace com_verifone_pml {
namespace net {
namespace wifi {
/* ------------------------------------------------------------------ */ 
class medium_config;
/* ------------------------------------------------------------------ */
namespace detail {
/* ------------------------------------------------------------------ */ 
/** This function generate WPA supplicant config file used for wifi connection
 *	 @param[in] network input configuration
 *	 @param[in] outfile Ouptut filename used for the wpa supplicant config
 *	 @return -1 if errno 0 on success + errno
 */
int wpa_supplicant_generator( const std::string& ifc_name, 
		const medium_config& cfg, const char* outfile );
/* ------------------------------------------------------------------ */ 
}}}}

