/*
 * =====================================================================================
 *
 *       Filename:  generic_profile_store.hpp
 *
 *    Description:  Generic profile store via template 
 *
 *        Version:  1.0
 *        Created:  16.03.2015 10:25:12
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lucjan Bryndza (LB), lucck
 *   Organization:  
 *
 * =====================================================================================
 */

#pragma once

#include <boost/serialization/base_object.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/assume_abstract.hpp>
#include <boost/filesystem.hpp>
#include <libpml/pml_abstracted_api.h>
#include <map>
#include <fstream>

namespace com_verifone_pml {
namespace net {
namespace detail {
	template <class T>
	class generic_profile_store 
	{
		friend class boost::serialization::access;
		generic_profile_store( generic_profile_store& ) = delete;
		generic_profile_store& operator=( generic_profile_store ) = delete;
	protected:
		static constexpr auto inval = -1;
		using container = std::map<int,T>;
	private:
		//! Save and restore public methods
		static void restore( const char *filename, generic_profile_store& store ) 
		{
			std::ifstream ifs( filename );
			boost::archive::text_iarchive ia(ifs);
			ia >> store;
		}
		//Save the profile
		static void save( const char *filename, generic_profile_store& store ) 
		{

			std::ofstream ofs( filename );
			boost::archive::text_oarchive oa(ofs);
			oa << store;
		}
	protected:
		//! Generic profile store private constructor
		generic_profile_store( const char filename[] ) 
		{

			boost::filesystem::path file( getRAMRoot() );
			file /= filename;
			if( boost::filesystem::exists(file) ) {
				restore( file.string().c_str(), *this );
			} else {
				save( file.string().c_str(), *this );
			}
			m_file = file.string();
		}
	public:
		//! Save config data
		void save() 
		{
			if(!m_file.empty() ) 
			{
				save(m_file.c_str(), *this);
			}
		}
		//! serialization method
		template<typename archive> 
			void serialize( archive& ar, const unsigned int /*file_version */) {
				ar & m_cfg & m_default;
			}
		//! Get profile count
		int get_count() const 
		{
			return m_cfg.size();
		}
		//! Get defalt profile
		int get_default() const 
		{
			return m_default;
		}
		//! Find profile
		int find_profile( const T &prof ) const 
		{
			const auto it = std::find_if( m_cfg.cbegin(), m_cfg.cend(), 
				[&]( const typename container::value_type& cfg ) { return cfg.second==prof; } );
			return (it==m_cfg.cend())?(inval):(it->first);
		}
		//! Set default profile
		int set_default( int id ) 
		{
			if( m_cfg.find(id)!=m_cfg.cend() ) m_default = id;
			else return -1;
			return 0;
		}
		//! Add profile
		void add( int id, const T& cfg ) 
		{
			m_cfg[id] = cfg;
		}
		//Add profile
		int add( const T& cfg ) 
		{
			auto index = find_profile(cfg);
			if (index != inval)
			{
				errno = EEXIST;
				return inval;
			}
			index = 0;
			while (m_cfg.find( index ) != m_cfg.cend()) ++index;
			add( index, cfg );
			return index;
		}
		//Get profile
		int get( int id, T& cfg ) 
		{
			if (id == -1) 
			{
				const auto first = m_cfg.cbegin();
				if (first == m_cfg.cend()) 
				{
					return inval;
				}
				cfg = first->second;
				return 0;
			}
			const auto el = m_cfg.find( id );
			if( el == m_cfg.cend() ) 
			{
				return inval;
			}
			cfg = el->second;
			return 0;
		}
		//! Erase
		int remove( int id ) 
		{
			return m_cfg.erase(id)>0?0:inval;
		}
		//Begin
		typename container::const_iterator begin() const {
			return m_cfg.begin();
		}
		//End
		typename container::const_iterator end() const {
			return m_cfg.end();
		}
		//! Get profile
	protected:
		container m_cfg;
	private:
		int m_default { inval };
		std::string m_file;
	};
	
}
}
}
