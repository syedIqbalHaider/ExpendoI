/*
 * events_queue.hpp
 *
 *  Created on: 12-03-2013
 *      Author: lucck
 */

/* ---------------------------------------------------------------- */
#ifndef COM_VERIFONE_PML_EVENTS_QUEUE_HPP_
#define COM_VERIFONE_PML_EVENTS_QUEUE_HPP_
/* ---------------------------------------------------------------- */

#include <string>
#include <boost/noncopyable.hpp>
#include "evt_key.hpp"
#include "libpml/pml.h"
/* ---------------------------------------------------------------- */
namespace com_verifone_pml {
namespace internal {

/* ---------------------------------------------------------------- */
//Global events queue for tasks
class events_queue : private boost::noncopyable
{
public:
	//Queueue event constructor
	events_queue( const evt_key &key, bool server_mode );
	//Events queue destructor
	~events_queue();
	//Insert int the queue
	bool insert( const event_item &ev );
	//Get event
	bool pop( event_item &ev );
	//NOTE: Especialy not const method (event handle can change object state !
	int get_fd() const
	{
		return m_fd.first;
	}
	bool is_valid() const
	{
		return m_fd.first != -1;
	}
	bool empty( ) const;
private:
	typedef std::pair<int, int> sockets_pair;
	static sockets_pair create_unnamed_wait_sock( const std::string& sock_name );
	static int receive_equal( int socket, void *buffer, size_t len, int flags = 0 );
	static int create_connection( const std::string& sock_name );
private:
	evt_key m_conn_key;
	int m_conn_fd;
	sockets_pair m_fd;
	bool m_server;
};

/* ---------------------------------------------------------------- */
} /* namespace internal */
} /* namespace com_verifone_pml */
/* ---------------------------------------------------------------- */
#endif /* EVENTS_QUEUE_HPP_ */
/* ---------------------------------------------------------------- */
