/*
 * =====================================================================================
 *
 *       Filename:  wifi_profile_store.hpp
 *
 *    Description: WIFI profile store for vos
 *
 *        Version:  1.0
 *        Created:  19.02.2015 10:13:27
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lucjan Bryndza (LB), lucck
 *   Organization:  
 *
 * =====================================================================================
 */

#pragma once

#include <map>
#include <string>
#include "generic_profile_store.hpp"
#include <libpml/pml_net_wifi.h>

namespace com_verifone_pml {
namespace net {
namespace wifi {

namespace detail {

class wifi_profile_store : public net::detail::generic_profile_store<medium_config> 
{
public:
	//Constructor 
	wifi_profile_store() 
		:generic_profile_store( "wifi_profile.oarch" )
	{};

	//Extra option find profile by SSID 
	int find_profile( const std::string &ssid ) const {
		const auto it = std::find_if( m_cfg.cbegin(), m_cfg.cend(), 
			[&]( const container::value_type& cfg ) { return cfg.second.get_ssid()==ssid; } );
		return (it==m_cfg.cend())?(inval):(it->first);
	}
};


} } } }


