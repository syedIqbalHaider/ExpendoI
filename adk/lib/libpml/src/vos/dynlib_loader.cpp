/*
 * =====================================================================================
 *
 *       Filename:  dynlib_loader.cpp
 *
 *    Description:  Dynamic library loader PML vos implementation
 *
 *        Version:  1.0
 *        Created:  24.02.2015 15:40:41
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lucjan Bryndza (LB), lucck
 *   Organization:  
 *
 * =====================================================================================
 */
#include "dynlib_loader.hpp"
#include <dlfcn.h>
#include <cstring>
#include <stdexcept>

/* ------------------------------------------------------------------ */ 
namespace com_verifone_pml {
namespace internal {
/* ------------------------------------------------------------------ */
dl_loader * dl_loader::_instance_ = nullptr;
/* ------------------------------------------------------------------ */ 
dl_loader * dl_loader::get_instance() 
{
	if (!_instance_) {
		_instance_ = new dl_loader;
	}
	return _instance_;
}
/* ------------------------------------------------------------------ */
//! Find or load symbol from the dynamic library
void* dl_loader::find_or_load_symbol( const char *name )
{
	const auto f_it = m_funcs.find( name );
	if( f_it == m_funcs.end() ) 
	{
		const auto sep = std::strstr( name, ":" );
		if( !sep ) 
		{
			throw std::logic_error("Invalid syntax token : not found");
		}
		const auto name_len = sep - name;
		char library_name[ name_len + sizeof('\0') ];
		std::memcpy( library_name, name, name_len ); 
		library_name[name_len] = '\0';
		const auto symbol_name = sep + 1;
		const auto l_it = m_libs.find( library_name );
		void* lib_hwnd {};
		if( l_it == m_libs.end() ) 
		{
			const auto handle = dlopen( library_name, RTLD_LAZY );
			if( !handle ) 
			{
				throw std::logic_error( std::string("Unable to find library: ") + 
						library_name );
			}
			m_libs[ library_name ] = lib_ptr_t( handle, [](void*d){ dlclose(d); } );
			lib_hwnd = handle;
		} 
		else 
		{
			lib_hwnd = l_it->second.get();
		}
		//Try to open function name from library
		const auto fn_ptr = dlsym( lib_hwnd, symbol_name );
		if( fn_ptr ) 
		{
			m_funcs[ name ] = fn_ptr;
			return fn_ptr;
		} 
		else 
		{
			throw  std::logic_error( std::string("Unable to find symbol: ") +
				symbol_name + " in library: " +  library_name );
		}
	} 
	else 
	{
		return f_it->second;
	}
	return nullptr;
}
/* ------------------------------------------------------------------ */
}}
