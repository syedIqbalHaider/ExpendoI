/*
 * =====================================================================================
 *
 *       Filename:  bluetooth_utility.hpp
 *
 *    Description:  Bluetooth utility API
 *
 *        Version:  1.0
 *        Created:  16.03.2015 13:38:51
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lucjan Bryndza (LB), lucck
 *   Organization:  
 *
 * =====================================================================================
 */

#pragma once

#include <vector>
#include <string>

namespace com_verifone_pml {
namespace net {
namespace bluetooth {
namespace detail {

	/** Get list L2CAP connected devices 
	 * @param[out] List of devices ids
	 * @return error code
	 */
	int list_connected( std::vector<std::string>& devices ) noexcept;

	/** Return first connected bluetooth device
	 * @param[out] dev Output device
	 * @return error code
	 */
	int first_connected( std::string &dev ) noexcept;

	/**  Device is connected 
	 * @param[in] device Bluetooth device class
	 * @return error code
	 */
	int is_connected( const std::string& device ) noexcept;


}}}}
