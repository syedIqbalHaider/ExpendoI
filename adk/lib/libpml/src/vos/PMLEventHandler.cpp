/* ---------------------------------------------------------------- */
/*
 * testvos.cpp
 *
 *  Created on: 26-02-2013
 *      Author: lucck
 */

/* ---------------------------------------------------------------- */
#include <libipc/ipc_handle.h>
#include "epoll_user.hpp"
#include <sys/epoll.h>
#include <unistd.h>
#include <map>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <utility>
#include <errno.h>
#include <boost/foreach.hpp>
#include "libpml/pml.h"
#include "events_queue.hpp"
#include "pml_task_mngr.hpp"
#include "lib_list.hpp"
#include <liblog/logsys.h>
#include "libpml/pml_port.h"
//#include "libver.h"
/* ---------------------------------------------------------------- */


char ToUpper (char c) { return ::toupper(c); }
char ToLower (char c) { return ::tolower(c); }

namespace com_verifone_pml
{

pid_t gettid() {
    pthread_t ptid = pthread_self();
    pid_t threadId = 0;
    memcpy(&threadId, &ptid, std::min(sizeof(threadId), sizeof(ptid)));
    return threadId;
 }

/* ---------------------------------------------------------------- */
namespace
{
	//TODO: Change to inheritance 
	//Internal event implementation 
	typedef internal::epoll_user internal_event_t;
	typedef std::map<int, boost::shared_ptr< internal_event_t> > root_map_t;

	//Internal map for root handling
	root_map_t g_object_map;
	//Check if exists
	bool handle_exists( int handle )
	{
		root_map_t::const_iterator it = g_object_map.find( handle );
		return (g_object_map.end() != it );
	}
	//Class for manage the events
	boost::shared_ptr< internal::pml_task_mngr > g_task_mngr;
	//Object for lib list
	boost::shared_ptr< internal::lib_list> g_libs_db;

	struct PMLGlob
	{
		//Object for the task queue
		boost::shared_ptr< internal::events_queue > g_evt_queue;
		// Object for user events
		boost::shared_ptr< internal::events_container_type > g_req_events;
		//Store pml object name
		std::string g_pml_name;
	};
	
	std::map<pid_t, boost::shared_ptr<PMLGlob> > PMLGlobMapper;
	std::string g_pml_name;
}

/* ---------------------------------------------------------------- */
/*
* Function initializes PML library
*/
int pml_init(const char *name)
{
	pid_t pid = gettid();

	dlog_msg("pml_init name=%s pid=%ld", name, pid);
	
	if( PMLGlobMapper.find( pid ) != PMLGlobMapper.end() )
	{
		dlog_error("PMLGlobMapper is initialized for pid %ld", pid);
		errno = EEXIST;
		return -1;
	}
	else
	{
		PMLGlobMapper[pid] = boost::make_shared<PMLGlob>();
		const char *mname = (name == NULL || *name == '\0')?(program_invocation_short_name):(name);
		if( g_pml_name.empty() )
			g_pml_name = mname;
		if(!g_task_mngr)
		{
			g_task_mngr = boost::make_shared<internal::pml_task_mngr>();
			g_libs_db = boost::make_shared<internal::lib_list>();
		}
		g_task_mngr->insert( mname );
		
		PMLGlobMapper[pid]->g_evt_queue = boost::make_shared<internal::events_queue>( mname, true );
		PMLGlobMapper[pid]->g_req_events = boost::make_shared<internal::events_container_type>();
		PMLGlobMapper[pid]->g_pml_name = mname;
	}
	return 0;
}

int pml_deinit(const char * name)
{
	PMLGlobMapper.erase(PMLGlobMapper.find(gettid()));
	
	return 0;
}

/* ---------------------------------------------------------------- */
/* Return codes -1 when failed and errno */
/*
* Function creates internal object, which manages events
*/
int event_open()
{
    const int ehwnd = epoll_create1(0);
    if( ehwnd < 0 )
        return -1;
    g_object_map[ ehwnd ] = boost::make_shared<internal_event_t>( ehwnd, PMLGlobMapper[gettid()]->g_evt_queue, PMLGlobMapper[gettid()]->g_req_events );
    return ehwnd;
}
/* ---------------------------------------------------------------- */
/*
* Destroys internal object, associated with handle
*/
int event_close( int handle )
{
    root_map_t::iterator it = g_object_map.find( handle );
    if( it == g_object_map.end() )
    {
        errno = ENOENT;
        return -1;
    }
    g_object_map.erase( it );
    return close( handle );
}
/* ---------------------------------------------------------------- */
/*
* Adds / removes / updates events, associated with handle
*/
int event_ctl( int handle, const ctl::op_t operation, const event_item & event )
{
    if( !handle_exists( handle ) )
    {
        errno = ENOENT;
        return -1;
    }
    {
        if( operation == ctl::ADD )
            return g_object_map[handle]->add( event );
        else if( operation == ctl::DEL )
            return g_object_map[handle]->del( event );
        else
        {
            errno = ENOTSUP;
            return -1;
        }
    }
    return 0;
}
/* ---------------------------------------------------------------- */
int event_ctl( int handle, const ctl::op_t operation, const eventset_t & events )
{
    if( !handle_exists( handle ) )
    {
        errno = ENOENT;
        return -1;
    }
 
    BOOST_FOREACH( const eventset_t::value_type &ev, events )
    {
        if( event_ctl( handle, operation, ev ) < 0 )
        {
            return -1;
        }
    }
    return 0;
}
/* ---------------------------------------------------------------- */
int event_ctl( int handle, const ctl::op_t operation, const com_verifone_ipc::ipc_handle& ipchwnd )
{
	event_item ev;
	ev.event_id = events::ipc;
	ev.evt.com.fd = ipchwnd.get_raw();
	//ev.evt.com.flags = EPOLLIN |EPOLLPRI | EPOLLOUT |EPOLLERR;
	ev.evt.com.flags = EPOLLIN | EPOLLPRI | EPOLLERR;
	return event_ctl( handle ,operation, ev );
}
/* ---------------------------------------------------------------- */
/*
* As above, but works with second object, represented by add_handle
* Object related to add_handle is always untouched.
* ADD combines events from both object and puts them in first one, associated with handle. 
*     Handle object will consists of events present in both objects. 
* MOD updates events present in object associated to handle so that they match those from add_handle.
*     Events present in object add_handle but absent in object handle are not copied
* DEL deletes events present in object add_handle from object handle. Other events are left untouched.
*/
int event_ctl( int handle, const ctl::op_t operation, int add_handle )
{
    if( !handle_exists( handle ) )
    {
        errno = ENOENT;
        return -1;
    }
    if( operation == ctl::ADD )
        return g_object_map[handle]->add( add_handle );
    else if( operation == ctl::DEL )
        return g_object_map[handle]->del( add_handle );
    else
    {
        errno = ENOTSUP;
        return -1;
    }
    return 0;
}
/* ---------------------------------------------------------------- */
/*
* Waits for events, represented by object associated with handle. 
* Timeout: 0 for peek event, -1 - wait forever, positive value - wait amount of time in milliseconds
*/
int event_wait( int handle, eventset_t & revents , long timeout )
{
    if( !handle_exists( handle ) )
    {
        // Kamil_P1: VerixV compatibility
        // Treat the handle as existing epoll handle
        bool isError = true;
        int ret = -1;
        int hdl = event_open();
        if (hdl != -1)
        {
            if (event_ctl(hdl, ctl::ADD, com_verifone_ipc::ipc_handle(handle)) != -1)
            {
                if ( (ret = event_wait(hdl, revents, timeout)) != -1)
                {
                    isError = false;
                }
                event_ctl(hdl, ctl::DEL, com_verifone_ipc::ipc_handle(handle));
            }
            event_close(hdl);
        }
        if (isError)
        {
            errno = ENOENT;
            return -1;
        }
        return ret;
    }
    return g_object_map[handle]->wait( revents ,timeout );
}

/* ---------------------------------------------------------------- */
/* Raise an user event and OS specific event
 * @param[in] handle Input handle
 * @param[in[ event Event handle input
 * @return error code
 */
int event_raise( const char *name, const event_item &event )
{
	//g_task_mngr->print_tasks();
	if( name && *name != '\0' )
	{
		internal::pml_task_mngr::const_iterator it = g_task_mngr->find( name );
		if( it == g_task_mngr->end() )
		{
			dlog_error("Task %s cannot be found!", name);
			errno = ENOENT;
			return -1;
		}
		else
		{
			dlog_msg("Inserting event %d to task %s queue", event.event_id, name);
			internal::events_queue task_queue( name, false );
			bool res = task_queue.insert( event );
			return res == false ? 0 : -1;
		}
	}
	else
	{
		BOOST_FOREACH( const internal::pml_task_mngr::value_type &v, *g_task_mngr )
		{
			internal::events_queue task_queue( v.first, false );
			bool res = task_queue.insert( event );
			return res == false ? 0 : -1;
		}
	}
	return 0;
}
/* ---------------------------------------------------------------- */
int event_raise( pid_t pid, const event_item &event )
{
	BOOST_FOREACH( const internal::pml_task_mngr::value_type &v, *g_task_mngr )
	{
		if (v.second.pid == pid)
		{
			dlog_msg("Inserting event %d to task %s queue", event.event_id, v.second.name);
			internal::events_queue task_queue( v.first, false );
			bool res = task_queue.insert( event );
			return res == false ? 0 : -1;
		}
	}
	return -1;
}
/* ---------------------------------------------------------------- */
//Application version handling
namespace appver
{
	/* ---------------------------------------------------------------- */
	/**
	 * Add application  to the app list
	 * @param[in] version application version
	 * @return PML error code
	 */
	int add_app_version( const char version[] )
	{
		return g_task_mngr->insert_version( g_pml_name.c_str(), version );
	}
	/* ---------------------------------------------------------------- */
	/** Get current application version 
	 * @return  Application version 
	 */
	std::string get_app_version()
	{
		internal::pml_task_mngr::const_iterator it = g_task_mngr->find( g_pml_name.c_str() );
		if( it != g_task_mngr->end() )
		{
			return it->second.ver;
		}
		else
		{
			return "";
		}
	}
	/* ---------------------------------------------------------------- */
	/** Get application list 
	 @param[out] apps application list
	 */
	void get_apps_list( applist_t &apps )
	{
		BOOST_FOREACH( const internal::pml_task_mngr::value_type &item, *g_task_mngr )
		{
			apps.push_back( std::make_pair( item.second.name, item.second.ver ) );
		}
	}
	/* ---------------------------------------------------------------- */
	/** Get libraries list
	 * @param[out] liblist libraries list Input applicatoin name 
	 * @param[in]  appname Input applicatoin name 
	 * @return PML return code status
	 */
	void get_lib_list( applist_t& liblist, const char *appname )
	{
		BOOST_FOREACH( const internal::lib_list::value_type &item, *g_libs_db)
		{
			if( appname==NULL || !std::strcmp( appname, item.first.app ) )
			{
				liblist.push_back( std::make_pair( item.first.name, item.second.ver ) );
			}
		}
	}
	/* ---------------------------------------------------------------- */
	/**
	 * Add library to the app
	 * @param[in] libname application name 
	 * @param[in] version application version
	 * @return PML error code
	 */
	void register_library( const char libname[], const char version[] )
	{
		g_libs_db->add_library( libname, version,  g_pml_name.c_str() );
	}

	/* ---------------------------------------------------------------- */
}
/* ---------------------------------------------------------------- */
}
/* ---------------------------------------------------------------- */
