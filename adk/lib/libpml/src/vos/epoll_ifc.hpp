/*
 * epoll_ifc.hpp
 *
 *  Created on: 07-03-2013
 *      Author: lucck
 */
/* ---------------------------------------------------------------- */
#ifndef COM_VERIFONE_PML_EPOLL_IFC_HPP_
#define COM_VERIFONE_PML_EPOLL_IFC_HPP_
/* ---------------------------------------------------------------- */
#include <boost/noncopyable.hpp>
#include <map>
#include <libpml/pml.h>
#include <sys/epoll.h>

/* ---------------------------------------------------------------- */
namespace com_verifone_pml {
namespace internal {

/* ---------------------------------------------------------------- */
class epoll_ifc : private boost::noncopyable
{
private:
	  //Cascade event internal handling
	int do_ctl( int handle, int op )
	{
		event_item evi;
		evi.event_id = events::cascade;
		evi.evt.com.fd = handle;
		//evi.event_id = events::comm;
		//evi.evt.com.flags = EPOLLIN | EPOLLPRI;
		//evi.evt.com.fd = handle;
		return do_ctl( evi, op );
	}
	 //Do control for cascade handle
	int do_ctl( const event_item& event, int op );
public:
	//Enum status
	enum update_res { res_data_event, res_empty_event };
    //Constructor
    explicit epoll_ifc( int handle )
        : m_epoll_fd(handle)
    {
    }
    //Destructor
    virtual ~epoll_ifc()
    {}
    //Add event
    int add( const event_item &events )
    {
        return do_ctl( events, EPOLL_CTL_ADD );
    }
    int add( int handle )
    {
        return do_ctl( handle, EPOLL_CTL_ADD );
    }
    //Delete events
    int del( const event_item &events )
    {
        return  do_ctl( events, EPOLL_CTL_DEL );
    }
    int del( int handle )
    {
        return do_ctl( handle, EPOLL_CTL_DEL );
    }
    //Wait for event
    int wait( eventset_t& revents, long timeout );
protected:
    //Cascaded file descriptor manager
    int do_ctl( ::epoll_event &ev, const event_item &event ,int op );
private:
    /** Internal system function should fill the PML event
    * @param[in] event
    * @param[in] EPOLL operation event control
    * @return 0 If ok / -1 if fail and set errno like os func
    */
    virtual int ctl( const event_item& event, int op ) = 0;

    /** Update the PML event according to the System event
     * @param[out] event Updated system even
     * @param[in] sys_event Incoming system event
     * @return None
     */
    virtual int update_event( event_item& event, const ::epoll_event &sys_event) = 0;
private:
    int m_epoll_fd;
    std::map<int, event_item> m_events_map;
};
/* ---------------------------------------------------------------- */

} /* namespace internal */
} /* namespace com_verifone_pml */

/* ---------------------------------------------------------------- */
#endif /* EPOLL_IFC_HPP_ */
/* ---------------------------------------------------------------- */
