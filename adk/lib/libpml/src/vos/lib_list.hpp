/*
 * =====================================================================================
 *
 *       Filename:  lib_list.hpp
 *
 *    Description:  Library list version shared memory container
 *
 *        Version:  1.0
 *        Created:  07/19/2013 10:08:40 AM
 *       Revision:  
 *       Compiler:  gcc
 *
 *         Author:  Lucjan Bryndza
 *   Organization:  VERIFONE
 *
 * =====================================================================================
 */
#ifndef COM_VERIFONE_PML_LIB_LIST_HPP
#define COM_VERIFONE_PML_LIB_LIST_HPP
/* ---------------------------------------------------------------- */

#include <boost/noncopyable.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/allocators/allocator.hpp>
#include <boost/interprocess/containers/map.hpp>
#include <cstring>

/* ---------------------------------------------------------------- */

namespace com_verifone_pml {
namespace internal {

/* ---------------------------------------------------------------- */
class lib_list : private boost::noncopyable
{
	static const std::size_t C_header_size = 4096;
	static const std::size_t C_max_items = 512;
public:
    struct lib_key
    {
        char name[16];  //!  Application name
        char app[16];   //! Application which use it
    };
    struct lib_ver
    {
        char ver[16]; //Library version
    };
	typedef lib_key key_type;
	typedef lib_ver mapped_type;
    typedef std::pair< const key_type, mapped_type > value_type;
private:
   	struct comparable_type : public std::binary_function<lib_key, lib_key, bool>
	{
	   bool operator()( lib_key const& n1, lib_key const& n2 ) const
	   {
	       return (std::strncmp( n1.name, n2.name, sizeof(lib_key::name) )<0) ||
                  (std::strncmp( n1.app, n2.app, sizeof(lib_key::app) )<0) ;
	   }
	};
    typedef boost::interprocess::allocator<value_type, boost::interprocess::managed_shared_memory::segment_manager> shm_allocator_type;
public:
    typedef boost::interprocess::map<key_type, mapped_type ,comparable_type ,shm_allocator_type> shared_map_t;
    typedef shared_map_t::iterator iterator;
	typedef shared_map_t::const_iterator const_iterator;
	typedef shared_map_t::reference reference;
	typedef shared_map_t::const_reference const_reference;
	iterator begin() { return m_libs->begin(); }
	const_iterator begin() const { return m_libs->begin(); }
	iterator end() { return m_libs->end(); }
	const_iterator end() const { return m_libs->end(); }
	const_iterator cbegin() const { return m_libs->begin(); }
	const_iterator cend() const { return m_libs->end(); }
public:         // PUBLIC API
    //Constructor default
    lib_list();
    // Register library
    void add_library( const char name[], const char ver[], const char app[] );
private:
    boost::interprocess::managed_shared_memory m_shm;
	const shm_allocator_type m_shm_allocator;    
    shared_map_t *m_libs;
};

/* ---------------------------------------------------------------- */
}}
/* ---------------------------------------------------------------- */
#endif


