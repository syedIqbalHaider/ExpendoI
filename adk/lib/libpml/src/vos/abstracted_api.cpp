/*
 * abstracted_api.cpp
 *
 *  Created on: 08-04-2013
 *      Author: lucck
 */
/* ---------------------------------------------------------------- */
#include <cstddef>
#ifdef BOOST_FILE_BROWSER
#include <boost/checked_delete.hpp>
#include <boost/filesystem.hpp>
#include <boost/regex.hpp>
#include <boost/algorithm/string/replace.hpp>
#endif

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/optional.hpp>
#include <fstream>
#include <string>
#include <cstring>
#include <liblog/logsys.h>
#include <fcntl.h>
#include <dirent.h>
#include <sys/sendfile.h>
#include <libpml/pml_abstracted_api.h>
#include <libpml/ux.h>
#if !(defined(__x86_64) || defined(__i386))
extern "C" {    //Extern C ugly hack VFI sdk doesn't provide C++ guard
#include <svcInfoAPI.h>
#include <platforminfo_api.h>
#include <buzzer.h>
#include <svcsec.h>
#include <svcmgr/svc_utility.h>
#include <svcmgr/svc_security.h>
#include <svcmgr/svc_powermngt.h>
#include <libsecins.h>
#undef max
#undef min
}
#endif
#include <unistd.h>
#include <sys/types.h>
#include <sys/param.h>
#include <sys/stat.h>
#include <sys/sysinfo.h>
#include <boost/algorithm/hex.hpp>
#include <boost/crc.hpp>
#include <algorithm>
#include <openssl/sha.h>
#include <openssl/md5.h>
#include <cstdlib>
#include "dynlib_loader.hpp"

#ifdef VFI_SUBPLATFORM_RAPTOR
    #define LIBVFISEC_SUFFIX ".1"
#else
    #define LIBVFISEC_SUFFIX
#endif

/* ---------------------------------------------------------------- */
namespace com_verifone_pml
{

namespace {
    int get_from_system_file(const char * file, char * buf, size_t size)
    {
        int result = -1;
        int hdl = open(file, O_RDONLY);
        if (hdl >= 0)
        {
            memset(buf, 0, size);
            result = read(hdl, buf, size-1);
            close(hdl);
        }
        return result;
    }

    int get_from_system_file(const char * file)
    {
        char buf[10];
        int result = get_from_system_file(file, buf, sizeof(buf));
        if (result > 0)
        {
            if (sscanf(buf, "%d", &result) != 1)
            {
                dlog_error("Invalid data read (%s)", buf);
                result = 0;
            }
        }
        return result;
    }
}

#ifdef BOOST_FILE_BROWSER
/* ---------------------------------------------------------------- */
/* Internal state for dir searching */
class dir_search::internal_state
{
private:
    static void escape_regexp( std::string &regex )
    {
        boost::replace_all(regex, "\\", "\\\\");
        boost::replace_all(regex, "^", "\\^");
        boost::replace_all(regex, ".", "\\.");
        boost::replace_all(regex, "$", "\\$");
        boost::replace_all(regex, "|", "\\|");
        boost::replace_all(regex, "(", "\\(");
        boost::replace_all(regex, ")", "\\)");
        boost::replace_all(regex, "[", "\\[");
        boost::replace_all(regex, "]", "\\]");
        boost::replace_all(regex, "*", "\\*");
        boost::replace_all(regex, "+", "\\+");
        boost::replace_all(regex, "?", "\\?");
        boost::replace_all(regex, "/", "\\/");
    }
    static boost::regex regexp_string( const char *pattern )
    {
        std::string p( pattern );
        escape_regexp( p );
        boost::replace_all(p, "\\?", ".");
        boost::replace_all(p, "\\*", ".*");
        return boost::regex(p, boost::regex::normal);
    }
public:
     boost::filesystem::directory_iterator it;
     void set_pattern( const char *pattern, const char *path )
     {
        m_pattern =  regexp_string( pattern );
        it = boost::filesystem::directory_iterator( (path==NULL||*path=='\0')?("."):(path)  );
     }
     bool operator()() const
     {
        return boost::regex_match( std::string((it)->path().filename().string()), m_pattern );
     }
private:
      boost::regex m_pattern;
};

/* ---------------------------------------------------------------- */
/* Default constructor */
dir_search::dir_search()
    : m_state(new internal_state)
{
}
/* ---------------------------------------------------------------- */
/* Destructor */
dir_search::~dir_search()
{
    boost::checked_delete( m_state );
}

/* ---------------------------------------------------------------- */
/*
 * Gets name of first file, which fits given mask. Up to bufSize characters of found file name is copied into buffer.
 */
int dir_search::get_first(const char *mask, char * buf, std::size_t bufSize)
{
    m_state->set_pattern( mask, buf );
    return get_next( buf, bufSize );
}

/* ---------------------------------------------------------------- */
/*
 * If mask is set using dir_get_first(), this returns next file fitting given mask.
 */
int dir_search::get_next(char *buf, size_t bufSize)
{
    const boost::filesystem::directory_iterator end;
    if( m_state->it == end )
    {
        return err_not_found;
    }
    if( (*m_state)() )
    {
        //std::strncpy(buf, m_state->it->path().filename().c_str(), bufSize );
        std::strncpy(buf, m_state->it->path().c_str(), bufSize );
        ++m_state->it;
        return err_found;
    }
    else
    {
        if( m_state->it != end )
        {
            ++m_state->it;
            return  get_next( buf, bufSize );
        }
        else
            return err_not_found;
    }
}


/* ---------------------------------------------------------------- */
/*
 * Deletes file, represented by name
 */
int delete_file(const char  *name)
{
    boost::system::error_code err;
    const bool exists = boost::filesystem::remove( name, err );
    return (err!=0||!exists )?(-1):(0);
}
/* ---------------------------------------------------------------- */
/*
 * Renames file - oldname is replaced by newname
 */
int rename_file(const char * oldname, const char * newname)
{
    boost::system::error_code err;
    boost::filesystem::rename( oldname, newname, err );
    return err!=0?(-1):(0);
}
/* ---------------------------------------------------------------- */
#else // BOOST_FILE_BROWSER
// The below DOES NOT use boost
/* ---------------------------------------------------------------- */
/* Internal state for dir searching */
class dir_search::internal_state
{
public:
    internal_state(): dirp(NULL)
    {
        memset(m_mask, 0, sizeof(m_mask));
        memset(m_path, 0, sizeof(m_path));
    }
    ~internal_state()
    {
        if (dirp) closedir(dirp);
    }
    int get_first(const char * mask, char * buf, std::size_t bufSize);
    int get_next(char * buf, size_t bufSize);
private:
    bool file_matches_mask(const char * mask, const char *fname);
    int get_next_matching_file(char * buf, size_t bufSize);
    void create_output(const char * fname, char * buf, size_t bufSize);
private:
    static const int MAX_FILE_NAME = 128+1; // ASCIIZ string
    static const int MAX_SUBDIR_NAME = 256+1; // Subdirectory string

    char m_mask[MAX_FILE_NAME];
    char m_path[MAX_SUBDIR_NAME];
    DIR * dirp;
};
/* ---------------------------------------------------------------- */
int dir_search::internal_state::get_first(const char * mask, char * buf, std::size_t bufSize)
{
    if (!mask || *mask == '\0')
    {
        strcpy(m_mask, "*");
    }
    else
    {
        strncpy(m_mask, mask, sizeof(m_mask)-1);
    }
    strncpy(m_path, buf, sizeof(m_path)-1);
    if (m_path && strlen(m_path))
    {
        dirp = opendir( m_path );
    }
    else
    {
        dirp = opendir( "." );
    }
    if (!dirp)
    {
        #ifdef DIR_SEARCH_LOGGING
        dlog_error("DS: Cannot open directory %s", m_path ? m_path : ".");
        #endif
        return -1;
    }
    // Find files
    dlog_msg("DS: Looking for files matching %s in %s", m_mask, m_path ? m_path : ".");
    return get_next_matching_file(buf, bufSize);
}
/* ---------------------------------------------------------------- */
int dir_search::internal_state::get_next(char * buf, size_t bufSize)
{
    if (!dirp)
    {
        #ifdef DIR_SEARCH_LOGGING
        dlog_error("DS: Call get_first!");
        #endif
        errno = EINVAL; // ctmp
        return -1;
    }
    return get_next_matching_file(buf, bufSize);
}
/* ---------------------------------------------------------------- */
int dir_search::internal_state::get_next_matching_file(char * buf, size_t bufSize)
{
    struct dirent *dp;
    errno = 0;
    while ( (dp = readdir(dirp)) != NULL )
    {
        errno = 0;
        if (strcmp(dp->d_name, ".") && strcmp(dp->d_name, "..") && file_matches_mask(m_mask, dp->d_name))
        {
            create_output( dp->d_name, buf, bufSize );
            return err_found;
        }
    }
    if (errno != 0)
    {
        #ifdef DIR_SEARCH_LOGGING
        dlog_error("DS: error reading directory, errno %d", errno);
        #endif
        return err_system;
    }
    #ifdef DIR_SEARCH_LOGGING
    dlog_alert("DS: next file not found");
    #endif
    return err_not_found;
}
/* ---------------------------------------------------------------- */
void dir_search::internal_state::create_output(const char * fname, char * buf, size_t bufSize)
{
    size_t pathSize = strlen(m_path);
    *buf = 0;
    if (pathSize)
    {
        strncpy(buf, m_path, bufSize);
        bufSize -= pathSize;
        const char lastChar = *(m_path+pathSize-1);
        if (lastChar != '/')
        {
            strncat(buf, "/", bufSize);
            --bufSize;
        }
    }
    strncat(buf, fname, bufSize);
    dlog_msg("DS: Found file: '%s'", buf);
}
/* ---------------------------------------------------------------- */
bool dir_search::internal_state::file_matches_mask(const char * mask, const char *fname)
{
    int i;
    bool star = false;
    const char * pat = mask;
    #ifdef DIR_SEARCH_LOGGING
    dlog_msg("DS: Comparing fname %s with mask %s", fname, mask);
    #endif
    do
    {
      bool nextLoop = false;
      for (i = 0; fname[i]; i++)
      {
        if (pat[i] == '?')
        {
          if (fname[i] == '.')
          {
            if (!star) return false;
            fname++;
            nextLoop = true;
            break; // for, next loop
          }
        }
        else if (pat[i] == '*')
        {
          star = true;
          fname += i, pat += i;
          do { ++pat; } while (*pat == '*');
          if (!*pat) return true;
          nextLoop = true;
          break; // for, next loop
        }
        else
        {
          // Case insensitive??
          //if (tolower(fname[i]) != tolower(pat[i]))
          if (fname[i] != pat[i])
          {
            if (!star) return false;
            fname++;
            nextLoop = true;
            break; // for, next loop
          }
        }
      } /* endfor */
      if (nextLoop) continue;
      else break;
    } while(true);
    while (pat[i] == '*') ++i;
    #ifdef DIR_SEARCH_LOGGING
    dlog_msg("DS: Match result %d", !pat[i]);
    #endif
    return (!pat[i]);
}
/* ---------------------------------------------------------------- */
/* Default constructor */
dir_search::dir_search(): m_state(new internal_state)
{
}
/* ---------------------------------------------------------------- */
/* Destructor */
dir_search::~dir_search()
{
    delete m_state;
}
/* ---------------------------------------------------------------- */
/*
 * Gets name of first file, which fits given mask. Up to bufSize characters of found file name is copied into buffer.
 */
int dir_search::get_first(const char *mask, char * buf, std::size_t bufSize)
{
    assert(m_state);
    return m_state->get_first(mask, buf, bufSize);
}
/* ---------------------------------------------------------------- */
/*
 * If mask is set using dir_get_first(), this returns next file fitting given mask.
 */
int dir_search::get_next(char *buf, size_t bufSize)
{
    assert(m_state);
    return m_state->get_next(buf, bufSize);
}
/* ---------------------------------------------------------------- */
/*
 * Deletes file, represented by name
 */
int delete_file(const char  *name)
{
    return unlink(name);
}
/* ---------------------------------------------------------------- */
/*
 * Renames file - oldname is replaced by newname
 */
int rename_file(const char * oldname, const char * newname)
{
    return rename(oldname, newname);
}

/* ---------------------------------------------------------------- */
#endif // BOOST_FILE_BROWSER
/* ---------------------------------------------------------------- */
/*
 * Copies the file
 */

int copy_file (const char *source, const char *target)
{
    int result = -1;
    int read_fd = -1;
    int write_fd = -1;
    struct stat stat_buf;
    off_t offset = 0;
    ssize_t copied = -1;

    /* Open the input file. */
    read_fd = open (source, O_RDONLY);
    if(read_fd < 0)  return -1;

    /* Stat the input file to obtain its size. */
    if (fstat(read_fd, &stat_buf) != 0) goto cleanup;

    /* Open the output file for writing, with the same permissions as the source file. */
    write_fd = open (target, O_WRONLY | O_CREAT, stat_buf.st_mode);
    if(write_fd < 0) goto cleanup;

    /* Blast the bytes from one file to the other. */
    copied = sendfile (write_fd, read_fd, &offset, stat_buf.st_size);
    if (copied < 0)
    {
        dlog_alert("sendfile(%s -> %s) failed: %d, errno %d (%s)! Try manual copy",
            source, target, copied, errno, strerror(errno));
        // Error, probably Kernel is too old. Copy manually...
        char buffer[32768];
        int readSize;
        while ( (readSize = read(read_fd, buffer, sizeof buffer)) > 0)
        {
            int writeSize = write(write_fd, buffer, readSize);
            if (writeSize != readSize)
            {
                dlog_error("Write(%s) error: wrote %d, should write %d, errno %d (%s)",
                    target, writeSize, readSize, errno, strerror(errno));
                goto cleanup;
            }
        }
        if (readSize < 0)
        {
            dlog_error("read(%s) failed: %d, errno %d (%s)!",
                source, readSize, errno, strerror(errno));
            goto cleanup;
        }
    }

    result = 0;
    dlog_msg("Copy success (%s -> %s)", source, target);
    
    {
        std::string sig(source);
        sig.append(".p7s");
        if (int error = access(sig.c_str(), 0))
        {
            dlog_msg("Skip copying of signature file: %s, error %d, errno %d (%s)",
                sig.c_str(), error, errno, strerror(errno));
        }
        else
        {
            std::string dst(target);
            dst.append(".p7s");
            dlog_msg("Copy file signature (%s -> %s)", sig.c_str(), dst.c_str());
            result = copy_file(sig.c_str(), dst.c_str());
        }
    }
    
cleanup:
    if (read_fd >= 0) close(read_fd);
    if (write_fd >= 0) close(write_fd);
    return result;
}

/* ---------------------------------------------------------------- */
/*
 * Moves the file
 */
int move_file(const char * src, const char * dst)
{
    int err = link( src, dst );
    if (err < 0)
    {
        // didn't work, fallback to copy / remove
        if (copy_file(src, dst) < 0)
        {
            // dlog_error("Copy file failed!");
            return -1;
        }
    }
    err = unlink( src );
    if (err < 0)
    {
        // dlog_error("Fast move failed!");
        return -1;
    }
    return 0;
}
/* ##################### PC emulated version ####################### */
#if defined(__x86_64) || defined(__i386)
/* ---------------------------------------------------------------- */
/*
 * Gets 8 characters long terminal identification number
*/
void svcInfoPtid(char * buffer, const std::size_t bufferSize)
{
    std::strncpy(buffer,"x86PCOMP", bufferSize );
}

/* ---------------------------------------------------------------- */
/*
 * Gets 8 characters long firmware version
 */
void svcInfoEprom(char * buffer, const std::size_t bufferSize)
{
    std::strncpy(buffer,"verPCx86",sizeof(bufferSize));
}
/* ---------------------------------------------------------------- */
/*
 * Gets 12 characters long model number
 */
void svcInfoModelNum(char * buffer, const size_t bufferSize)
{
    std::strncpy( buffer, "modPCx86xxxx", bufferSize );
}

/* ---------------------------------------------------------------- */
/*
 * Gets 30 characters long factory defined manufacturing data
 */
void svcInfoManufacturingData(char * buffer, const size_t bufferSize)
{
    if( bufferSize > 0 ) buffer[0] = '\0';
}

/* ---------------------------------------------------------------- */
/*
 * Gets 12 characters long country variant data
 */
void svcInfoCountry(char * buffer, const size_t bufferSize)
{
   std::strncpy( buffer, "CountryNo", bufferSize );
}

/* ---------------------------------------------------------------- */
/*
 * Gets 12 characters long part number
 */
void svcInfoPartNumber(char * buffer, const size_t bufferSize)
{
    std::strncpy( buffer, "000000000000", bufferSize );
}

/* ---------------------------------------------------------------- */
/*
 * Gets 2 characters long hardware version
 */
void svcInfoHardwareVersion(char * buffer, const size_t bufferSize)
{
    std::strncpy( buffer, "00", bufferSize );
}

/* ---------------------------------------------------------------- */
/*
 * Gets 11 characters long serial number
 */
void svcInfoSerialNumber(char * buffer, const size_t bufferSize)
{
    std::strncpy( buffer, "00000000000", sizeof(bufferSize) );
}

void svcInfoBareSerialNumber(char * buffer, const size_t bufferSize)
{
    svcInfoSerialNumber(buffer, bufferSize);
}

/* ---------------------------------------------------------------- */
/*
 * Gets 6 characters long lot number
 */
void svcInfoLotNumber(char * buffer, const size_t bufferSize)
{
    std::strncpy( buffer, "000000", sizeof(bufferSize) );
}


/* ---------------------------------------------------------------- */
/*
 * Gets Flash size in kBytes
 */
long svcFlashSize()
{
    return 0;
}

/* ---------------------------------------------------------------- */
/*
 * Gets SBI version
 */
void svcInfoSBIVersion(char * buffer, const size_t bufferSize)
{
    std::strncpy(buffer, "00", bufferSize);
}

/* ---------------------------------------------------------------- */
/*
 * Gets CIB ID
 */
void svcInfoCIBID(char * buffer, const size_t bufferSize)
{
    std::strncpy(buffer, "00", bufferSize);
}

/* ---------------------------------------------------------------- */
/*
 * Gets CIB Version
 */
void svcInfoCIBVersion(char * buffer, const size_t bufferSize)
{
    std::strncpy(buffer, "00", bufferSize);
}

/* ---------------------------------------------------------------- */
int SHA1(const unsigned char * input_buffer, size_t input_length, SHA1_OUTPUT & output)
{
    return -1;
}
/* ---------------------------------------------------------------- */
int SHA256(const unsigned char * input_buffer, size_t input_length, SHA256_OUTPUT & output)
{
    return -1;
}

/* ##################### Real MX  version ####################### */
#else /*defined(__x86_64) || defined(__i386)*/

/* ---------------------------------------------------------------- */
namespace {
    //Private MIB structure bleh
    struct mib {
    unsigned long hdr_signature;
    unsigned long hdr_fileSize;
    char hdr_dateTime[14];
    char hdr_version[4];
    char hdr_spare[38];
    char mfg_block[30];
    char model_num[12];
    char country[12];
    char part_number[32];
    char hdwr_version[4];
    char lot_number[8];
    char serial_number[16];
    char PTID[8];
    char ETH_MAC_addr[12];
    char WiFi_MAC_addr[12];
    char security_option[1];
    char BT_MAC_addr[12];
    char reserved[64];
    char undefined[733];
    unsigned char crc[4];
};
    //Get mib block //false if ok
    inline bool get_mib_block( mib &output )
    {
        unsigned long rlen = 0;
        return ( PML_DYNAMIC_CALL( "libvfiplatforminfo.so", platforminfo_get, ::PI_MIB, &output, sizeof(mib), &rlen ) != PI_OK || rlen != sizeof(mib) );
    }

    int get_platforminfo_string(unsigned long code, size_t localSize, char * buffer, size_t bufferSize)
    {
        char local_buf[localSize+1];
        unsigned long rlen = 0;
        local_buf[localSize] = 0;
        if (!PML_DYNAMIC_CALL( "libvfiplatforminfo.so", platforminfo_get, code, (char*)local_buf, localSize, &rlen ) )
        {
            std::strncpy(buffer, local_buf, bufferSize);
            return 0;
        }
        return -1;
    }
}
/* ---------------------------------------------------------------- */
int SHA1(const unsigned char * input_buffer, size_t input_length, SHA1_OUTPUT & output)
{
    // VOS OS call requires the cast below...
    ::SHA1( const_cast<unsigned char *>(input_buffer), input_length, output.shabuf );
    return 0;
}
/* ---------------------------------------------------------------- */
int SHA256(const unsigned char * input_buffer, size_t input_length, SHA256_OUTPUT & output)
{
    // VOS OS call requires the cast below...
    ::SHA256( const_cast<unsigned char *>(input_buffer), input_length, output.shabuf );
    return 0;
}
/* ---------------------------------------------------------------- */
namespace md5_internals
{
    MD5_CTX ctx;
};
MD5::MD5()
    { MD5_Init(&md5_internals::ctx); }
void MD5::update(const void * input_buffer, size_t input_length)
    { MD5_Update(&md5_internals::ctx, input_buffer, input_length); }
int MD5::finalize(MD5_OUTPUT & output)
    { MD5_Final(output.md5buf, &md5_internals::ctx); return 0; }

/* ---------------------------------------------------------------- */
void svcInfoPtid(char * buffer, const std::size_t bufferSize)
{
    get_platforminfo_string(PI_UNIT_ID, PI_UNIT_ID_SIZE_NULL_TERM, buffer, bufferSize);
}
/* ---------------------------------------------------------------- */
/*
 * Gets 8 characters long firmware version
 */
void svcInfoEprom(char * buffer, const std::size_t bufferSize)
{
    get_platforminfo_string(PI_VERSION_RELEASE, PI_VERSION_RELEASE_SIZE, buffer, bufferSize);
}
/* ---------------------------------------------------------------- */
/*
 * Gets 12 characters long model number
 */
void svcInfoModelNum(char * buffer, const size_t bufferSize)
{
    //get_platforminfo_string(PI_MIB_MODEL_NUM, PI_MIB_MODEL_NUM_SIZE, buffer, bufferSize);
    get_platforminfo_string(PI_CIB_MODEL_ID_STR, PI_MIB_MODEL_NUM_SIZE, buffer, bufferSize);
}
/* ---------------------------------------------------------------- */
/*
 * Gets 30 characters long factory defined manufacturing data
 */
void svcInfoManufacturingData(char * buffer, const size_t bufferSize)
{
    mib mb;
    if( !get_mib_block( mb ) )
    {
        const size_t l = std::min( bufferSize, sizeof mb.mfg_block );
        std::memcpy( buffer, mb.mfg_block, l ); buffer[l] = '\0';
    }
    else
    {
        if( bufferSize > 0 ) buffer[0] = '\0';
    }
}
/* ---------------------------------------------------------------- */
/*
 * Gets 12 characters long country variant data
 */
void svcInfoCountry(char * buffer, const size_t bufferSize)
{
    mib mb;
    if( !get_mib_block( mb ) )
    {
        const size_t l = std::min( bufferSize, sizeof mb.country );
        std::memcpy( buffer, mb.country, l ); buffer[l] = '\0';
    }
    else
    {
        if( bufferSize > 0 ) buffer[0] = '\0';
    }
}

/* ---------------------------------------------------------------- */
/*
 * Gets 12 characters long part number
 */
void svcInfoPartNumber(char * buffer, const size_t bufferSize)
{
    mib mb;
    if( !get_mib_block( mb ) )
    {
        const size_t l = std::min( bufferSize, sizeof mb.part_number );
        std::memcpy( buffer, mb.part_number, l ); buffer[l] = '\0';
    }
    else
    {
        if( bufferSize > 0 ) buffer[0] = '\0';
    }
}

/* ---------------------------------------------------------------- */
/*
 * Gets 2 characters long hardware version
 */
void svcInfoHardwareVersion(char * buffer, const size_t bufferSize)
{
    mib mb;
    if( !get_mib_block( mb ) )
    {
        const size_t l = std::min( bufferSize, sizeof mb.hdwr_version );
        std::memcpy( buffer, mb.hdwr_version, l ); buffer[l] = '\0';
    }
    else
    {
        if( bufferSize > 0 ) buffer[0] = '\0';
    }
}

/* ---------------------------------------------------------------- */
/*
 * Gets 11 characters long serial number
 */
void svcInfoSerialNumber(char * buffer, const size_t bufferSize)
{
    mib mb;
    if( !get_mib_block( mb ) )
    {
        const size_t l = std::min( bufferSize, sizeof mb.serial_number );
        std::memcpy( buffer, mb.serial_number, l ); buffer[l] = '\0';
    }
    else
    {
        if( bufferSize > 0 ) buffer[0] = '\0';
    }
}

void svcInfoBareSerialNumber(char * buffer, const size_t bufferSize)
{
    char localBuffer[bufferSize];
    svcInfoSerialNumber(localBuffer, sizeof(localBuffer));
    size_t len = strlen(localBuffer);
    for (size_t i = 0; i < len && i < bufferSize-1; ++i)
    {
        if (isdigit(*(localBuffer+i))) *buffer++ = *(localBuffer+i);
    }
    *buffer = 0;
}

/* ---------------------------------------------------------------- */
/*
 * Gets 6 characters long lot number
 */
void svcInfoLotNumber(char * buffer, const size_t bufferSize)
{
    mib mb;
    if( !get_mib_block( mb ) )
    {
        const size_t l = std::min( bufferSize, sizeof mb.lot_number );
        std::memcpy( buffer, mb.lot_number, l ); buffer[l] = '\0';
    }
    else
    {
        if( bufferSize > 0 ) buffer[0] = '\0';
    }
}
/* ---------------------------------------------------------------- */
/*
 * Gets Flash size in kBytes
 */
long svcFlashSize()
{
    ::PI_flash_info_st flashinfo;
    unsigned long rlen = 0;
    if (!PML_DYNAMIC_CALL( "libvfiplatforminfo.so",platforminfo_get, PI_FLASH_INFO, &flashinfo, sizeof(flashinfo), &rlen ))
    {
        return flashinfo.total;
    }
    return -1;
}

/* ---------------------------------------------------------------- */
/*
 * Gets SBI version
 */
void svcInfoSBIVersion(char * buffer, const size_t bufferSize)
{
    get_platforminfo_string(PI_VERSION_SBI, PI_VERSION_SBI_SIZE_NULL_TERM, buffer, bufferSize);
}

/* ---------------------------------------------------------------- */
/*
 * Gets CIB ID
 */
void svcInfoCIBID(char * buffer, const size_t bufferSize)
{
    get_platforminfo_string(PI_CIB_PRODUCT_NUMBER_STR, PI_CIB_PRODUCT_NUMBER_SIZE, buffer, bufferSize);
}

/* ---------------------------------------------------------------- */
/*
 * Gets CIB Version
 */
void svcInfoCIBVersion(char * buffer, const size_t bufferSize)
{
    get_platforminfo_string(PI_VERSION_CIB, PI_VERSION_CIB_SIZE_NULL_TERM, buffer, bufferSize);
}

/*
 * Gets removal switches and tamper status
 */
int svcGetRemovalSwitchStatus()
{
    return PML_DYNAMIC_CALL( "libsvc_security.so", security_readServiceSwitch );
}
int svcGetTamperStatus()
{
    auto tamper = PML_DYNAMIC_CALL( "libsvc_security.so", security_getAttackStatus );
    return tamper.isAttacked;
}
/* ---------------------------------------------------------------- */
#endif /*defined(__x86_64) || defined(__i386)*/

/* ##################### COMMON Part  ####################### */

/* ---------------------------------------------------------------- */
/*
 * Gets RAM size in kBytes
 */
long svcRAMSize()
{
      return long(::sysconf( _SC_PHYS_PAGES )) * long(::sysconf( _SC_PAGE_SIZE )/1024);
}
/* ---------------------------------------------------------------- */
/*
 * Converts data, stored in dsp buffer, to hex equivalent. Dsp_len is length of dsp buffer (zero-termination is not required), max_hex_len is size of hex buffer.
 * Example: svcDsp2Hex("1A2B", 4, hex, 2) stores "\x1A\x2B" in hex buffer.
 * Buffers hex and dsp should NOT overlap!
 */
int svcDsp2Hex(const char *dsp, size_t dsp_len, char *hex, const size_t max_hex_len)
{
    namespace algo = boost::algorithm;
    int end = (dsp_len>2*max_hex_len)?(max_hex_len*2):(dsp_len);
    const char * const end_iterator =  dsp + end;
    try {
        algo::unhex( dsp, end_iterator, hex );
    }
    catch( algo::hex_decode_error& ) {
        return -1;
    }
    return end >> 1;
    // return ( dsp_len > 2*max_hex_len );
}

/* ---------------------------------------------------------------- */
/*
 * Converts data, stored in hex buffer, to display (ASCII) equivalent. Hex_len is length of hex buffer, max_dsp_len is size of display buffer (the buffer will be zero-terminated on exit).
 * Example: svcHex2Dsp("\x1A\x2B", dsp, 2, 5) stores ASCIIZ string "1A2B" in display buffer.
 * Buffers hex and dsp should NOT overlap!
 */
int svcHex2Dsp(const char *hex, size_t hex_len, char *dsp, const size_t max_dsp_len)
{
    namespace algo = boost::algorithm;
    const char * const end_iterator = hex + ((max_dsp_len/2<hex_len)?(max_dsp_len/2):(hex_len));
    try {
        char* fout = algo::hex( hex, end_iterator, dsp );
        *fout = '\0';
    }
    catch( algo::hex_decode_error& ) {
       return -1;
    }
    return strlen(dsp);
    //return ( max_dsp_len/2 < hex_len );
}
/* ---------------------------------------------------------------- */
//DSP algorithm
namespace
{
        unsigned long reverse(register unsigned long x)
        {
            x = (((x & 0xaaaaaaaa) >> 1) | ((x & 0x55555555) << 1));
            x = (((x & 0xcccccccc) >> 2) | ((x & 0x33333333) << 2));
            x = (((x & 0xf0f0f0f0) >> 4) | ((x & 0x0f0f0f0f) << 4));
            x = (((x & 0xff00ff00) >> 8) | ((x & 0x00ff00ff) << 8));
            return((x >> 16) | (x << 16));
        }
}

unsigned long svcCrcCalc(const CRC_TYPES type, const unsigned long seed, const void *_msg, const size_t size)
{
    const char * const msg = reinterpret_cast<const char*>(_msg);
    unsigned long ret = 0;
    switch( type )
    {
    case lrc:
    {
       boost::crc_optimal<8, 1, 0, 0, 0> crc_lrc(seed);
       crc_lrc = std::for_each( msg, msg + size, crc_lrc );
       ret = crc_lrc();
       break;
    }
    case crc16_lsb:
    {
        boost::crc_optimal<16, 0xA001, 0, 0, 0> c16_lsb(seed);
        c16_lsb = std::for_each( msg, msg + size, c16_lsb );
        ret = c16_lsb();
        break;
    }
    case crc16_msb:
    {
        boost::crc_optimal<16, 0x8005, 0, 0, 0>  c16_msb(seed);
        c16_msb = std::for_each( msg, msg + size, c16_msb );
        ret = c16_msb();
        break;
    }
    case ccitt_lsb:
    {
        boost::crc_optimal<16, 0x8408, 0xFFFF, 0, 0> c16_clsb(seed);
        c16_clsb = std::for_each( msg, msg + size, c16_clsb );
        ret = c16_clsb();
        break;
    }
    case ccitt_msb:
    {
        boost::crc_optimal<16, 0x1021, 0xFFFF, 0, 0> c16_mlsb(seed);
        c16_mlsb = std::for_each( msg, msg + size, c16_mlsb );
        ret = c16_mlsb();
        break;
    }
    case crc32:
    {
        boost::crc_optimal<32, 0x04C11DB7, 0, 0, true, true> c32(reverse(seed));
        c32 = std::for_each( msg, msg + size, c32 );
        ret = c32();
        break;
    }
    case crc32_inverted:
    {
        boost::crc_optimal<32, 0x04C11DB7, 0xFFFFFFFF, true, true> c32i(reverse(seed));
        c32i = std::for_each( msg, msg + size, c32i );
        ret = c32i() ^ 0xFFFFFFFF;
        break;
    }
    case crc16:
    {
        boost::crc_optimal<16, 0x8005, 0 , true, true> c16(seed);
        c16 = std::for_each( msg, msg + size, c16 );
        ret = c16();
        break;
    }
    case ccitt16:
    {
        boost::crc_optimal<16, 0x1021, 0xFFFF , true, true> c16(seed);
        c16 = std::for_each( msg, msg + size, c16 );
        ret = c16();
        break;
    }
    } // switch
    return ret;
}

/* ---------------------------------------------------------------- */

void datetime2seconds(const char *yyyymmddhhmmss, unsigned long *seconds)
{
    struct tm t = {0};  // Initalize to all 0's
    static const size_t TMP_BUFLEN = 5;
    char tbuf[TMP_BUFLEN];
    tbuf[TMP_BUFLEN-sizeof('\0')] = '\0';
    std::strncpy(tbuf, &yyyymmddhhmmss[0], 4 );  tbuf[4] = '\0';
    t.tm_year = std::atoi(tbuf) - 1900;     // This is year-1900, so 112 = 2012
    std::strncpy(tbuf, &yyyymmddhhmmss[4], 2 );  tbuf[2] = '\0';
    t.tm_mon = std::atoi(tbuf) - 1;         // Month in struct tm is 0-11, not 1-12!
    std::strncpy(tbuf, &yyyymmddhhmmss[6], 2 );
    t.tm_mday = std::atoi(tbuf);
    std::strncpy(tbuf, &yyyymmddhhmmss[8], 2 );
    t.tm_hour = std::atoi(tbuf);
    std::strncpy(tbuf, &yyyymmddhhmmss[10], 2 );
    t.tm_min = std::atoi(tbuf);
    std::strncpy(tbuf, &yyyymmddhhmmss[12], 2 );
    t.tm_sec = std::atoi(tbuf);
    *seconds = mktime(&t);
}

/* ---------------------------------------------------------------- */
void svcRestart()
{
    PML_DYNAMIC_CALL( "libsecins.so", Secins_reboot );
}
/* ---------------------------------------------------------------- */
void svcRestartApplication()
{
    dlog_msg("Restarting VIPA");
    PML_DYNAMIC_CALL( "libsecins.so", Secins_start_user_apps );
}
/* ---------------------------------------------------------------- */
// Returns equivalent of SVC_MOD_CK() eVo call
int luhn(const std::string &pan_asciiz)
{
    int pan_size = pan_asciiz.size();
    if (pan_size > 0)
    {
        if (tolower(pan_asciiz[pan_size-1]) == 'f') --pan_size;
        if (strspn(pan_asciiz.c_str(), "1234567890") == pan_size)
        {
            const int m[] = {0,2,4,6,8,1,3,5,7,9}; // mapping for rule 3
            bool is_odd_dgt = true;
            int sum = 0;
            for (int i = pan_size-1; i >= 0; --i, is_odd_dgt = !is_odd_dgt)
            {
                int digit = pan_asciiz[i] - '0';
                if (is_odd_dgt) sum += digit;
                else sum += m[digit];
            }
            int digit = ( 10 - sum % 10 ) % 10;
            dlog_msg("Sum %d, res %d, digit %d", sum, (sum % 10) == 0, digit);
            int result = ((sum % 10) == 0) << 8 | (digit & 0xFF);
            return result;
            //return ((sum % 10) == 0) << 8 ;
        }
    }
    return 0;
}
/* ---------------------------------------------------------------- */
bool isFileSigned(const std::string &file)
{
    struct stat buf;
    if (stat(file.c_str(), &buf) == 0)
    {
        //return true; // signed for now
        // Note from devs: When specifying a filename, ensure that the full path is included.
        // Make sure this is the case
        std::string locFile(file);
        if (locFile.find(getFlashRoot()) == std::string::npos && locFile.find(getRAMRoot()) == std::string::npos)
        {
            locFile = getRAMRoot();
            locFile.append(1, '/');
            locFile.append(file);
        }
        int res = PML_DYNAMIC_CALL("libvfisec.so" LIBVFISEC_SUFFIX, authFile, locFile.c_str());
        dlog_msg("Checking file auth: '%s', result %d", locFile.c_str(), res);
        if (res == 1) return true;
    }
    return false;
}
/* ---------------------------------------------------------------- */
namespace
{
	std::string get_home_dir()
	{
		return std::getenv("HOME");
	}
}
/* ---------------------------------------------------------------- */
std::string getFlashRoot(int /*unused*/)
{
    return std::string(get_home_dir() + "/flash/flash");
}
/* ---------------------------------------------------------------- */
std::string getRAMRoot(int /*unused*/)
{
    return std::string(get_home_dir() + "/flash");
}
/* ---------------------------------------------------------------- */
std::string getLogsDir()
{
    return std::string(get_home_dir() + "/logs");
}
/* ---------------------------------------------------------------- */
int set_working_directory(const char *directory)
{
	if (directory) return chdir(directory);
	else return chdir(get_home_dir().c_str());
}
/* ---------------------------------------------------------------- */
int openConsole()
{
    // No need to open anything here
    return 0;
}

/* ---------------------------------------------------------------- */
void sleep(int seconds)
{
    ::sleep(seconds);
}
/* ---------------------------------------------------------------- */
void usleep(int microseconds)
{
    ::usleep(microseconds);
}
/* ---------------------------------------------------------------- */
namespace
{

std::string get_config_filename(std::string fileName)
{
	return getRAMRoot(0) + "/" + fileName;
}

}
/* ---------------------------------------------------------------- */
int get_env(const char *key, char *buf, int size)
{
	return getkey(key, buf, size, ".config.sys");
}
/* ---------------------------------------------------------------- */
int put_env(const char *key, const char *buf, int size)
{
	return putkey(key, buf, size, ".config.sys");
}

/* ---------------------------------------------------------------- */
int getkey(const char *key, char *buf, int size, const char *fileName)
{
	using boost::property_tree::ptree;
	ptree pt;
	std::ifstream ifl(get_config_filename(fileName).c_str());
	if( !ifl.good() )
		return -1;
	read_ini( ifl , pt );

	ptree::path_type keyPath(key, '/');
	boost::optional<std::string> val = pt.get_optional<std::string>(keyPath);
	if( val )
		std::strncpy(buf, val.get().c_str(), size );
	else
		return 0;
	return val.get().size();
}

/* ---------------------------------------------------------------- */
int putkey(const char *key, const char *buf, int size, const char *fileName)
{
	using boost::property_tree::ptree;
	ptree pt;
	std::string configFileName = get_config_filename(fileName);

	{
		std::ifstream ifl(configFileName.c_str());
		if( ifl.good() )
			read_ini( ifl , pt );
	}

	// Indicate "/" as path separator instead of default '.'!
	ptree::path_type keyPath(key, '/');
	pt.put(keyPath, std::string(buf, size));

	write_ini( configFileName, pt );
	return 0;
}

/* ---------------------------------------------------------------- */
int alpha_shift(int c)
{
	static const char table[255] = {
	//	0     1     2     3     4     5     6     7     8     9     A     B     C     D     E     F
		0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   ,
		0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   ,
		'+' , ':' , '*' , '!' , 0   , 0   , 0   , '\"', 0   , 0   , ',' , '0' , '\'', ' ' , '1' , 0   ,
		'-' , 'Q' , 'A' , 'D' , 'G' , 'J' , 'M' , 'P' , 'T' , 'W' , ';' , '@' , 0   , 0   , 0   , 0   ,
		'#' , 'B' , 'C' , '2' , 'E' , 'F' , '3' , 'H' , 'I' , '4' , 'K' , 'L' , '5' , 'N' , 'O' , '6' ,
		'R' , 'Z' , 'S' , '7' , 'U' , 'V' , '8' , 'X' , 'Y' , '9' , '.' , 0   , 0   , 0   , 0   , 0   ,
		0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   ,
		0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   ,
		0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   ,
		0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   ,
		0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   ,
		0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   ,
		0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   ,
		0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   ,
		0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   , 0   ,
	};
	unsigned char curr = static_cast<unsigned char>(c);
	if (table[curr] != 0) return table[curr];
	return c;
}

/* ---------------------------------------------------------------- */
int normal_tone()
{
	int hnd = PML_DYNAMIC_CALL( "libvfibuzzer.so", Buzzer_Open );
	if (hnd >= 0)
	{
		// PML_DYNAMIC_CALL( "libvfibuzzer.so", Buzzer_SetVolume, 100 );
		PML_DYNAMIC_CALL( "libvfibuzzer.so", Buzzer_Play, 1245, 50, 10 );
		usleep(50000);
		PML_DYNAMIC_CALL( "libvfibuzzer.so", Buzzer_Close, hnd );
		return 0;
	}
	return -1;
}
/* ---------------------------------------------------------------- */
int error_tone()
{
	int hnd = PML_DYNAMIC_CALL( "libvfibuzzer.so", Buzzer_Open );
	if (hnd >= 0)
	{
		// PML_DYNAMIC_CALL( "libvfibuzzer.so", Buzzer_SetVolume, 100 );
		PML_DYNAMIC_CALL( "libvfibuzzer.so", Buzzer_Play, 880, 100, 10 );
		usleep(100000);
		PML_DYNAMIC_CALL( "libvfibuzzer.so", Buzzer_Close, hnd );
		return 0;
	}
	return -1;
}

int sound (int note, int msec)
{
		float soundfr[] = {
			55.00,
			58.27,
			61.74,
			65.41,
			69.30,
			73.42,
			77.78,
			82.41,
			87.31,
			92.50,
			98.00,
			103.83,
			110.00,
			116.54,
			123.47,
			130.81,
			138.59,
			146.83,
			155.56,
			164.81,
			174.61,
			185.00,
			196.00,
			207.65,
			220.00,
			233.08,
			246.94,
			261.63,
			277.18,
			293.66,
			311.13,
			329.63,
			349.23,
			369.99,
			392.00,
			415.30,
			440.00,
			466.16,
			493.88,
			523.25,
			554.37,
			587.33,
			622.25,
			659.26,
			698.46,
			739.99,
			783.99,
			830.61,
			880,
			932,
			988,
			1047,
			1109,
			1175,
			1245,
			1319,
			1397,
			1480,
			1568,
			1661,
			1760,
			1865,
			1976,
			2093,
			2217,
			2349,
			2489,
			2637,
			2794,
			2960,
			3136,
			3322,
			3520,
			3729,
			3951,
			4186,
			4435,
			4699,
			4978,
			5274,
			5588,
			5920,
			6272,
			6645,
			7040,
			7459,
			7902,
			8372,
			8870,
			9397,
			9956,
			10548,
			11175,
			11840,
			12544,
			13290
		};

	int hnd = PML_DYNAMIC_CALL( "libvfibuzzer.so", Buzzer_Open );
	if (hnd >= 0)
	{
		// PML_DYNAMIC_CALL( "libvfibuzzer.so", Buzzer_SetVolume, 100 );
		PML_DYNAMIC_CALL( "libvfibuzzer.so", Buzzer_Play, note >= 0 && note < sizeof(soundfr)/soundfr[0] ? soundfr[note] : soundfr[0], msec, 10 );
		usleep(msec * 1000);
		PML_DYNAMIC_CALL( "libvfibuzzer.so", Buzzer_Close, hnd );
	}
	return 0;
}

/* ---------------------------------------------------------------- */
int read_clock (char *yyyymmddhhmmssw, size_t size)
{
	if (size >= 15)
	{
		time_t rawtime;
		struct tm * timeinfo;
		time (&rawtime);
		timeinfo = localtime (&rawtime);
		return strftime (yyyymmddhhmmssw, size, "%Y%m%d%H%M%S%w", timeinfo);
	}
	return -1;
}
/* ---------------------------------------------------------------- */
int set_clock(const char * yyyymmddhhmmss)
{
	int result = -1;

	struct utilityDateTime yourtime;
	memset(&yourtime, 0, sizeof(yourtime));
	if (std::sscanf(yyyymmddhhmmss, "%4d%02d%02d%02d%02d%02d", &yourtime.tm_year, &yourtime.tm_mon, &yourtime.tm_mday,
		&yourtime.tm_hour, &yourtime.tm_min, &yourtime.tm_sec) == 6)
	{
		result = PML_DYNAMIC_CALL( "libsvc_utility.so", utility_settime, &yourtime );
		dlog_msg("Settime result %d", result);
	}
	return result;
}
/* ---------------------------------------------------------------- */
long get_uptime()
{
	struct sysinfo info;
	if (sysinfo(&info)) return -1;
	return info.uptime;
}
/* ---------------------------------------------------------------- */
namespace backlight
{
    static const char * BACKLIGHT_FILE = "/sys/class/backlight/bcm5892-bl/brightness";
    static int percent_max = 0;
    int get_percent_max()
    {
        if (percent_max > 0) return percent_max;
        percent_max = get_from_system_file("/sys/class/backlight/bcm5892-bl/max_brightness");
        return percent_max;
    }
}
// 0 - 100
int set_backlight_level(int percent)
{
    if (percent >= 0 && percent <= 100)
    {
        static const int MIN_BACKLIGHT = 5; // it is 0, but we don't want to disable backlight completely
        ux uxHandler;
        if (uxHandler.is_ux()) return uxHandler.setBacklight(percent);
        // read through file since API doesn't allow checking the level (!!!)
        dlog_msg("Setting backlight to level %d%%", percent);
        percent = (percent * backlight::get_percent_max()) / 100;
        if (percent == 0) percent = MIN_BACKLIGHT;
        dlog_msg("Setting backlight to value %d", percent);
        int hdl = open(backlight::BACKLIGHT_FILE, O_WRONLY | O_TRUNC);
        if (hdl >= 0)
        {
            char buf[10];
            snprintf(buf, sizeof(buf), "%d", percent);
            size_t size = strlen(buf);
            if (write(hdl, buf, size) != size)
            {
                // error
            }
            close(hdl);
            return 0;
        }
    }
    return -1;
}
/* ---------------------------------------------------------------- */
int set_backlight(bool enable)
{
    if (enable) return set_backlight_level(100);
    else return set_backlight_level(0);
}
/* ---------------------------------------------------------------- */
int get_backlight_level()
{
    int result = get_from_system_file(backlight::BACKLIGHT_FILE);
    if (result >= 0)
    {
        // Scale to 1-100
        result = (result * 100) / backlight::get_percent_max();
    }
    return result;
}
/* ---------------------------------------------------------------- */
int get_battery_status()
{
    svc_apm_info battInfo;
    if (!PML_DYNAMIC_CALL( "libsvc_powermngt.so", powermngt_GetInfo, &battInfo ))
    {
        if (!(battInfo.battery_flags & SVC_BATTERY_FLAGS_ABSENT))
        {
            return battInfo.battery_flags & SVC_BATTERY_FLAGS_CHARGING;
        }
    }
    errno = ENODEV;
    return -1;
}
/* ---------------------------------------------------------------- */
int get_battery_condition()
{
    svc_apm_info battInfo;
    if (!PML_DYNAMIC_CALL( "libsvc_powermngt.so", powermngt_GetInfo, &battInfo ))
    {
        if (!(battInfo.battery_flags & SVC_BATTERY_FLAGS_ABSENT))
        {
            switch (battInfo.battery_status)
            {
                case SVC_BATTERY_STATUS_HIGH: return BATTERY_HIGH;
                case SVC_BATTERY_STATUS_LOW: return BATTERY_LOW;
                case SVC_BATTERY_STATUS_CRITICAL: return BATTERY_CRITICAL;
                case SVC_BATTERY_STATUS_CHARGING: return BATTERY_CHARGING;
                case SVC_BATTERY_STATUS_ABSENT: return BATTERY_ABSENT;
                #ifdef NEW_SDK
                case SVC_BATTERY_STATUS_FULL: return BATTERY_FULL;
                #endif
                default:
                    dlog_error("Invalid battery status %d", battInfo.battery_status);
                    break; // just return -1
            }
        }
    }
    errno = ENODEV;
    return -1;
}
/* ---------------------------------------------------------------- */
int get_battery_percentage()
{
    svc_apm_info battInfo;
    if (!PML_DYNAMIC_CALL( "libsvc_powermngt.so", powermngt_GetInfo, &battInfo ))
    {
        if (!(battInfo.battery_flags & SVC_BATTERY_FLAGS_ABSENT))
        {
            return battInfo.battery_percentage;
        }
    }
    errno = ENODEV;
    return -1;
}
/* ---------------------------------------------------------------- */
int get_dock_status()
{
    svc_apm_info battInfo;
    if (!PML_DYNAMIC_CALL( "libsvc_powermngt.so", powermngt_GetInfo, &battInfo ))
    {
        // dlog_msg("Battery flags %Xh, battery status %Xh, AC stat %Xh", battInfo.battery_flags, battInfo.battery_status, battInfo.ac_line_status);
        if (!(battInfo.battery_flags & SVC_BATTERY_FLAGS_ABSENT))
        {
            return battInfo.ac_line_status == SVC_AC_LINE_STATUS_ON;
            // return battInfo.battery_flags & SVC_BATTERY_FLAGS_CHARGING;
        }
    }
    errno = ENODEV;
    return -1;
}
/* ---------------------------------------------------------------- */
int get_battery_value(battery_values reqValue)
{
    svc_apm_info battInfo;
    if (!PML_DYNAMIC_CALL( "libsvc_powermngt.so", powermngt_GetInfo, &battInfo ))
    {
        if (!(battInfo.battery_flags & SVC_BATTERY_FLAGS_ABSENT))
        {
            int res;
            switch (reqValue) {
                case BATTERY_FULLCHARGE:
                    res = get_from_system_file("/sys/class/power_supply/adc_battery/charge_full_design");
                    break;
                case BATTERY_REMAININGCHARGE:
                    res = get_from_system_file("/sys/class/power_supply/adc_battery/voltage_max_design") - get_from_system_file("/sys/class/power_supply/adc_battery/voltage_now");
                    break;
                case BATTERY_VOLTAGE:
                    res = get_from_system_file("/sys/class/power_supply/adc_battery/voltage_now");
                    break;
                default:
                case BATTERY_AVAIL:
                case BATTERY_TEMP:
                case BATTERY_CHARGER_STATUS:
                    res = -1;
                    break;
            }
            // eVo compatibility, -2 indicates battery error
            if (res == -1) res = -2;
            return res;
        }
    }
    errno = ENODEV;
    return -1;
}
/* ---------------------------------------------------------------- */
int set_keyboard_backlight(int /*enabled*/) // 1 - enabled, 0 - disabled
{
    // todo implement!
    return 0;
}
/* ---------------------------------------------------------------- */
int get_keyboard_backlight()
{
    // todo implement!
    return 0;
}
/* ---------------------------------------------------------------- */
int key_beeps(bool enable)
{
    // ux100
    ux uxHandler;
    if (uxHandler.is_ux())
    {
        return uxHandler.setKeyboardBeep(enable);
    }
    return PML_DYNAMIC_CALL( "libvfibuzzer.so", Keypad_Buzzer_On_Off, enable );
}
/* ---------------------------------------------------------------- */
int get_first_file_group()
{
    return 1;
}
/* ---------------------------------------------------------------- */
int get_last_file_group()
{
    return 1;
}
/* ---------------------------------------------------------------- */
int svcProcessUpgradeFile(const char * file, int * reboot /*= 0*/)
{
	static const char * UPGRADES_DIR = "/mnt/flash/install/dl/";
	std::string dstFile(UPGRADES_DIR);
	std::string srcFile(file);
	size_t index = srcFile.find_last_of("/");
	if (index != std::string::npos) dstFile.append(srcFile.substr(index+1));
	else dstFile.append(file);
	// first of all, move the file to correct dir
	if (move_file(file, dstFile.c_str()) < 0)
	{
		dlog_error("Cannot move the file!");
		return -1;
	}
	// Maybe the bundle already exists? Try removing it. First, determine bundle name based on filename
	std::string bundleName(file);
	// Assume package filename format is some.prefix.pkg_name.ext
	// so look between last two dots.
	size_t nameEnd = bundleName.rfind('.');
	if (nameEnd != std::string::npos)
	{
		size_t nameStart = bundleName.rfind('.', nameEnd - 1);
		if (nameStart == std::string::npos)
			nameStart = bundleName.find_last_of(":/");
		nameStart = (nameStart != std::string::npos) ? nameStart + 1 : 0;
		bundleName = bundleName.substr(nameStart, nameEnd - nameStart);
	}
	int tmpr = PML_DYNAMIC_CALL("libsecins.so",Secins_remove_user_bundle, bundleName.c_str());
	dlog_msg("Bundle name %s, removal result %d", bundleName.c_str(), tmpr);
	// TODO: What should we do in case removal fails?
	// Ok, now call upgrade function
	int tmp = 0;
	return PML_DYNAMIC_CALL( "libsecins.so", Secins_install_pkgs, reboot ? reboot : &tmp );
}
/* ---------------------------------------------------------------- */
int svcAuthenticateFile(const char * file)
{
	std::string fname(file);
	fname.append(".p7s");
	int res = PML_DYNAMIC_CALL( "libvfisec.so" LIBVFISEC_SUFFIX,authFile,const_cast<char *>(fname.c_str()));

	dlog_msg("Auth result %d, errno %d", res, errno);
	if (res == 1) return 0;
	return -1;
}
/* ---------------------------------------------------------------- */
char * get_serial_port_name(char * buf, size_t size, int port_no)
{
	snprintf(buf, size, "/dev/ttyAMA%d", port_no-1);
	return buf;
}
/* ---------------------------------------------------------------- */
	bool has_base_station()
	{
		char model[16] = {0};
		svcInfoModelNum(model, sizeof model);
		if (strstr(model, "VX675"))
		{
			// Check if /dev/ttyUSB0 can be opened
			int fd = open("/dev/ttyUSB0", O_RDWR | O_NOCTTY | O_NDELAY);
			if (fd != -1)
			{
				close(fd);
				return true;
			}
		}
		else if (strstr(model, "VX820"))
		{
			// TODO: How to detect base for 820 V/OS?
		}
		return false;
	}
/* ---------------------------------------------------------------- */
bool base_station_info(std::string & firmware_version, std::map<std::string, std::string> * base_info /*= NULL*/)
{
	// TODO: Implement when possible
	// This is for Vx675 full feature base and Vx820 Duet base
	return false;
}
/* ---------------------------------------------------------------- */
int dump_logs(std::string & out_file)
{
	#ifdef NEW_SDK
	utilityDumpLogStatus dls = PML_DYNAMIC_CALL( "libsvc_utility.so", utility_dumplogs, 0 );
	if (dls.status)
	{
		dlog_error("Error %d, Errno %d, error msg '%s'", dls.status, errno, dls.file_msg);
		return dls.status;
	}
	out_file.assign(dls.file_msg);
	return 0;
	#else
	errno = ENXIO;
	return -1;
	#endif
}
/* ------------------------------------------------------------------ */
/* Get terminal communiction capabilities
	* @param[in] communication capabilities
	* @return[out] error
	*/
int get_communication_capabilities( communication_capabilities& cap )
{
	{
		::PI_ethernet_info_st ethInfo;
		unsigned long rlen = 0;
		if (! PML_DYNAMIC_CALL( "libvfiplatforminfo.so",platforminfo_get, PI_ETHERNET_INFO, &ethInfo, sizeof(ethInfo), &rlen ))
		{
			cap.is_ethernet =  ethInfo.exist != 0;
		} else {
			dlog_error("Unable to get ethinfo %i", errno );
			return -1;
		}
	}
	{
		// Only works with SDK 3019x
		#ifdef NEW_SDK
		::PI_gprs_info_st gprsInfo;
		unsigned long rlen = 0;
		if (! PML_DYNAMIC_CALL("libvfiplatforminfo.so", platforminfo_get, PI_GPRS_INFO, &gprsInfo, sizeof(gprsInfo), &rlen ))
		{
			cap.is_gprs =  gprsInfo.exist != 0;
		} else {
			dlog_error("Unable to get gprs %i", errno );
			return -1;
		}
		#else
		unsigned long rlen = 0;
		unsigned int value = 0;
		if (!PML_DYNAMIC_CALL("libvfiplatforminfo.so", platforminfo_get, PI_IO_MODULE_CONFIG, &value, sizeof(value), &rlen ))
		{
			cap.is_gprs =  value & PI_IO_MODULE_GPRS;
		} else {
			dlog_error("Unable to get gprsinfo %i", errno );
			return -1;
		}
		#endif
	}
	{

		// Only works with SDK 3019x
		#ifdef NEW_SDK
		::PI_wifi_info_st wifiInfo;
		unsigned long rlen = 0;
		if (!PML_DYNAMIC_CALL("libvfiplatforminfo.so",platforminfo_get, PI_WIFI_INFO, &wifiInfo, sizeof(wifiInfo), &rlen ))
		{
			cap.is_wifi =  wifiInfo.exist != 0;
		} else {
			return -1;
		}
		#else
		unsigned long rlen = 0;
		unsigned int value = 0;
		int res = PML_DYNAMIC_CALL("libvfiplatforminfo.so", platforminfo_get, PI_IO_MODULE_CONFIG, &value, sizeof(value), &rlen );
		if (!res)
		{
			dlog_msg("Platforminfo value %u", value);
			cap.is_wifi =  value & PI_IO_MODULE_WIFI;
		}
		else
		{
			dlog_error("Unable to get wifi info errno %i", res);
			return -1;
		}
		#endif
	}
	{
		// Only works with SDK 3019x
		#ifdef NEW_SDK
		::PI_bluetooth_info_st btInfo;
		unsigned long rlen = 0;
		if (!PML_DYNAMIC_CALL("libvfiplatforminfo.so", platforminfo_get, PI_BLUETOOTH_INFO, &btInfo, sizeof(btInfo), &rlen ))
		{
			cap.is_bluetooth =  btInfo.exist != 0;
		} else {

			dlog_error("Unable to get bluetooth info errno %i", errno);
			return -1;
		}
		#else
		unsigned long rlen = 0;
		unsigned int value = 0;
		if (!PML_DYNAMIC_CALL("libvfiplatforminfo.so", platforminfo_get, PI_IO_MODULE_CONFIG, &value, sizeof(value), &rlen ))
		{
			cap.is_bluetooth =  value & PI_IO_MODULE_BLUETOOTH;
		} else {
			dlog_error("Unable to get bluetooth info errno %i", errno);
			return -1;
		}
		#endif
	}
	return 0;
}
/* ---------------------------------------------------------------- */
namespace apm {
	namespace {
		bool initialized = false;
		const unsigned long DEFAULT_DEVS = PM_WAKE_DEVICE_MCU_CARDIN | PM_WAKE_DEVICE_UART0 | PM_WAKE_DEVICE_UART1 | PM_WAKE_DEVICE_UART2 | PM_WAKE_DEVICE_UART3 | PM_WAKE_DEVICE_TOUCH_SCREEN | PM_WAKE_DEVICE_KEYPAD;
	}
	int init(unsigned long wakeDevices)
	{
		if (initialized) return 0;
		// Get config
		apm_config cfg;
		int status = PML_DYNAMIC_CALL( "libsvc_powermngt.so", powermngt_get_config, &cfg );
		if (status)
		{
			dlog_error("APM Get Config error %d, errno %d", status, errno);
			return -1;
		}
		//Try to set up the power management
		cfg.Size = sizeof(cfg);
		cfg.apm_on = true;
		// cfg.Standby_timeout = 30;
		cfg.communication_on = true;
		status = PML_DYNAMIC_CALL( "libsvc_powermngt.so", powermngt_set_config, & cfg );
		if (status)
		{
			dlog_error("APM Set Config error %d, errno %d", status, errno);
			//Kamil_P1: The API returns -1 on success (!!!)
			//return -1;
		}
		unsigned int wakeupdevs = 0;
		status = PML_DYNAMIC_CALL( "libsvc_powermngt.so", powermngt_get_wakeupdevices, &wakeupdevs );
		if (status)
		{
			dlog_error("APM Get Wake Devices error %d, errno %d", status, errno);
			return -1;
		}
		status = PML_DYNAMIC_CALL( "libsvc_powermngt.so", powermngt_set_wakeupdevices, wakeupdevs | wakeDevices );
		if (status)
		{
			dlog_error("APM Set Wake Devices error %d, errno %d", status, errno);
			return -1;
		}
		initialized = true;
		return 0;
	}
	int init(const std::vector<std::string> & wakeDevices)
	{
		ux uxHandler;
		unsigned long wakeDevs = 0;
		for (std::vector<std::string>::const_iterator it = wakeDevices.begin(); it != wakeDevices.end(); ++it)
		{
			std::string dev(*it);
			if (dev == "keyboard")
			{
				if (!uxHandler.is_ux()) wakeDevs |= PM_WAKE_DEVICE_KEYPAD;
				#ifdef NEW_SDK
				else wakeDevs |= PM_WAKE_DEVICE_KEYPAD_FUNC;
				#endif
			}
			else if (dev == "touchscreen")
			{
				// check 820 here!!
				wakeDevs |= PM_WAKE_DEVICE_TOUCH_SCREEN;
			}
			else if (dev == "contactless")
			{
				wakeDevs |= PM_WAKE_DEVICE_MCU_PROXIMITY;
			}
			else if (dev == "icc")
			{
				wakeDevs |= PM_WAKE_DEVICE_MCU_CARDIN;
			}
			else if (dev == "serial")
			{
				wakeDevs |= PM_WAKE_DEVICE_UART0;
			}
			else if (dev == "svc_btn" && uxHandler.is_ux())
			{
				wakeDevs |= PM_WAKE_DEVICE_EXTGRP5;
			}
		}
		dlog_msg("Detected wake devices: %lu", wakeDevs);
		if (wakeDevs == 0) wakeDevs = DEFAULT_DEVS;
		return init(wakeDevs);
	}
	int activate(MODES mode)
	{
		if (!initialized)
		{
			if (init(DEFAULT_DEVS)) return -1;
		}
		switch (mode)
		{
			case SUSPEND:
				return PML_DYNAMIC_CALL( "libsvc_powermngt.so", powermngt_Susspend );
			case HIBERNATE:
				#ifdef SDK_SUPPORTS_NS
				return PML_DYNAMIC_CALL( "libsvc_powermngt.so", powermngt_Hibernate );
				#else
				dlog_error("Hibernate is not supported!");
				return -1;
				#endif
			case STANDBY:
				return PML_DYNAMIC_CALL( "libsvc_powermngt.so", powermngt_Standby );
			default:
				dlog_error("Unknown APM mode %d", mode);
				return -1;
		}
		// never gets here...
		return -1;
	}
	
} // namespace apm

std::string get_invocation_name()
{
	return std::string(program_invocation_short_name);
}

/* ---------------------------------------------------------------- */
} // namespace com_verifone_pml
