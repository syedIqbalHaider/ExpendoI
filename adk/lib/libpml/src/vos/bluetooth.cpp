/*
 * =====================================================================================
 *
 *       Filename:  bletooth.cpp
 *
 *    Description:  Bluetooth support for the PML
 *
 *        Version:  1.0
 *        Created:  12.02.2015 12:50:05
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lucjan Bryndza (LB), lucck
 *   Organization:  
 *
 * =====================================================================================
 */
#include <cstring>
#include <liblog/logsys.h>
#include <libpml/pml.h>
#include <libpml/pml_abstracted_api.h>
#include <svcmgr/svc_net.h>
#include <sys/socket.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include <unistd.h>
#include "dynlib_loader.hpp"
extern "C" {
//! Crappy workarround for sdk1
#include <svcmgr/svc_bluetooth.h>
}
#include "bt_paired_list.hpp"
#include "bluetooth_utility.hpp"

/** FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME
 * This is a crapy bluetooth implementation becase the OS doesn't provide
 * API for the list of paired but not available devices, so it is implemented
 * using additional temporary list saved in the local application directory
 */
/* ------------------------------------------------------------------ */
namespace com_verifone_pml {
namespace net {
namespace bluetooth {
/* ------------------------------------------------------------------ */
namespace {
	internal::bt_paired_list m_paired;
}
/* ------------------------------------------------------------------ */
//! Enable or disable bluetooth device
int enable( bool enable )
{
	return PML_DYNAMIC_CALL( "libsvc_net.so",net_bluetoothPower, enable?NET_BT_PWR_UP:NET_BT_PWR_DOWN );
}
/* ------------------------------------------------------------------ */ 
/** Function establish Bluetooth connection with selected type
	* @param[in] link Connection type of the device
	* @return Error -1 if failed and errno set
	*/
int connect( int pair_index, conn_type::_enum_ link )
{
	device cdev;
	int error = enable(true);
	if( error < 0 )
	{
		dlog_error("Unable to power on Bluetooth, errno %d", errno);
		return error;
	}
	error = m_paired.get( pair_index, cdev );
	if( error < 0 ) 
	{
		dlog_error("Unable to get pair index %i", pair_index );
		return error;
	}
	bool is_pan_connected {};
	bool is_dun_connected {};
	/** Check if device is already connected or not  */
	if( detail::is_connected(cdev.address()) ) 
	{
		//Determine connection type
		auto status = PML_DYNAMIC_CALL( "libsvc_net.so",  net_bluetoothGetStatus, 
			const_cast<char*>(cdev.address().c_str()) );
		is_pan_connected = status.is_pan_connected;
		is_dun_connected = status.is_dun_connected;
		dlog_msg("We're connected: PAN %d, DUN %d", is_pan_connected, is_dun_connected);
	}
	/** Trying to pair with selected media and trying to recognize
	 * the media device to connect with it */
	if( !is_pan_connected && (cdev.ethernet()||link==conn_type::ethernet) )
	{
		error = PML_DYNAMIC_CALL( "libsvc_net.so", net_bluetoothConnectPAN,
			const_cast<char*>(cdev.address().c_str() ) );
		dlog_msg("PAN connection result %d, errno %d", error, errno);
		if( error==0 ) 
		{
			dlog_msg( "Trying to pan connect with dev %s", cdev.address().c_str() );
			error = PML_DYNAMIC_CALL("libsvc_net.so", 
					net_interfaceUp, const_cast<char *>("bnep0"), NET_IP_V4 );
			dlog_msg( "Iface up %d (%d)", error, errno );
			if( error<0 && errno!=ENETDOWN && errno!=ESRCH )
			{
				dlog_error("Net interface UP failed " );
				error = -1;
			}
			else error = 0;
		}
	}
	if( !is_dun_connected && (cdev.dial()||link==conn_type::dialup) ) 
	{
		error = PML_DYNAMIC_CALL( "libsvc_net.so", net_bluetoothConnectDUN, 
			const_cast<char*>(cdev.address().c_str() ) );
		if( error < 0 ) 
		{
			dlog_error("Connect dun failed");
			error = -1;
		}
	}
	return error;
}
/* ------------------------------------------------------------------ */
/** Destroy the bluetooth link
	* @return error code */
int disconect( conn_type::_enum_ link )
{
	std::vector<std::string> m_conns;
	detail::list_connected( m_conns );
	int err {};
	for( const auto& e : m_conns ) 
	{
		device cdev( e );
		auto prid = m_paired.find_profile(cdev);
		if( prid<0 ) 
		{
			dlog_msg("Connected %s but not on list", e.c_str() );
			continue;
		}
		m_paired.get( prid, cdev );
		//Get connection type
		auto status = PML_DYNAMIC_CALL( "libsvc_net.so",  net_bluetoothGetStatus, 
			const_cast<char*>(cdev.address().c_str()) );
		if( status.is_pan_connected && (cdev.ethernet()||link==conn_type::ethernet ) )
		{
			auto ret = PML_DYNAMIC_CALL("libsvc_net.so", net_bluetoothDisconnectPAN,
					const_cast<char*>(cdev.address().c_str()) );
			dlog_error("Disconnect pan status %i errno %i", ret, errno );
			if( ret < 0 ) err = ret;
		}
		if( status.is_dun_connected && (cdev.dial()||link==conn_type::dialup) )
		{
			auto ret = PML_DYNAMIC_CALL("libsvc_net.so", net_bluetoothDisconnectDUN,
					const_cast<char*>(cdev.address().c_str()) );
			dlog_error("Disconnect dun status %i errno %i", ret, errno );
			if( ret < 0 ) err = ret;
		}
	}
	return err;
}
/* ------------------------------------------------------------------ */
/**  Scan the available devices in range 
	*  @param[out] dev_list Return device list in the application
	@return Number of count of the device or -1 and errno will be set
*/
int scan( std::vector<device> &dev_list )
{
    inquiry_info *ii = NULL;
    int max_rsp, num_rsp;
    int dev_id, sock, len, flags;
    char addr[19] = { 0 };
    char name[248] = { 0 };

    errno = 0;
    dev_list.clear();
    dev_id = PML_DYNAMIC_CALL("libbluetooth.so", hci_get_route, nullptr );
    if (dev_id < 0)
    {
        int error = enable(true);
        if( error < 0 )
        {
          dlog_error("Unable to power on Bluetooth, errno %d", errno);
          return error;
        }
        dev_id = PML_DYNAMIC_CALL("libbluetooth.so", hci_get_route, nullptr );
    }
    sock = PML_DYNAMIC_CALL("libbluetooth.so",hci_open_dev, dev_id );
    if (dev_id < 0 || sock < 0) {
        dlog_error("opening socket failed: dev_id %d, sock %d, errno %d", dev_id, sock, errno);
        return -1;
    }

    len  = 8;
    max_rsp = 255;
    flags = IREQ_CACHE_FLUSH;
    ii = (inquiry_info*)malloc(max_rsp * sizeof(inquiry_info));
    
    num_rsp = PML_DYNAMIC_CALL("libbluetooth.so",hci_inquiry, dev_id, len, max_rsp, nullptr, &ii, flags);
    if( num_rsp < 0 )
    {
        PML_DYNAMIC_CALL("libbluetooth.so",hci_close_dev, dev_id );
        free( ii );
        return num_rsp;
    }

    for (auto i = 0; i < num_rsp; i++) 
    {
        PML_DYNAMIC_CALL("libbluetooth.so",ba2str, &(ii+i)->bdaddr, addr );
        memset(name, 0, sizeof(name));
        if (PML_DYNAMIC_CALL("libbluetooth.so",hci_read_remote_name,sock, &(ii+i)->bdaddr, sizeof(name), 
            name, 0) < 0)
        strcpy(name, "[unknown]");
        device dev( addr, name, ii->dev_class );
        dev_list.push_back( dev );
    }
    free( ii );
    close( sock );
    return 0;
}
/* ------------------------------------------------------------------ */
/** Pair with the selected device from the list
	* @param[in] dev Input device for pair
	* @param[in] pin Pin to pair
	* @param[in] method Pairing ethod @see pair_method struct
	* @return 0 on success -1 if fail errno will be set
	*/
int pair( const device& dev, const char pin[], pair_method::_enum_ method )
{
	if( m_paired.contains(dev.address()) ) 
	{
		dlog_error("Already paired");
		errno = EEXIST;
		return -1;
	}
	//FIXME: Archaic VOS SDK api without const requre const casting
	dlog_error(" PIN %s ADDR %s", pin, dev.address().c_str() );
	if( PML_DYNAMIC_CALL("libsvc_net.so", net_bluetoothSetPairingMethod, method ) ) {
		dlog_error("Unable to set pairing method: errno %d", errno);
		return -1;
	}
	if( PML_DYNAMIC_CALL("libsvc_net.so", net_bluetoothPair,
		const_cast<char*>(pin), const_cast<char*>(dev.address().c_str()) ) ) {
		dlog_error("Unable to pair: errno %d", errno);
		return -1;
	}
	{
		//Temporary workarround 
		if( m_paired.add( dev ) < 0 ) 
		{
			if (errno == EEXIST) dlog_error("Unable to add to storage, already exists");
			else dlog_error("Unable to add to storage");
			// return -1;
		}
		m_paired.save();
	}
	//Trying to setup internal structure what is a OS CRAP!!!
	{
		netBluetoothInfo bi {};
		std::strncpy( bi.name, dev.name().c_str(), sizeof(bi.name)-1 );
		std::strncpy( bi.address, dev.address().c_str(), sizeof(bi.address)-1 );
		bi.pan_profile = true;
		if( PML_DYNAMIC_CALL("libsvc_net.so", net_bluetoothSetInfo, 
					const_cast<char*>(dev.address().c_str()), bi ) ) {
			if (errno != EINVAL) {
				dlog_error("Unable to set bt dev info: errno %d", errno);
				return -1;
			}
			// Ignore EINVAL errno
		}
	}
	return 0;
}
/* ------------------------------------------------------------------ */
/** Pair with the selected device from the list
	* @param[in] dev Input device for pair
	* @return 0 on success -1 if fail errno will be set
	*/
int is_paired( int pair_index )
{
	device dev;
	return m_paired.get( pair_index, dev )>=0;
}
/* ------------------------------------------------------------------ */
/** Unpair with the selected device from the list
	* @param[in] dev Input device for pair
	* @return 0 on success -1 if fail errno will be set
	*/
int unpair( int paired_index )
{
	device dev;
	auto ret = m_paired.get( paired_index, dev ); 
	if( ret < 0 ) return ret;
	//FIXME: Archaic VOS SDK api without const requre const casting
	ret =  PML_DYNAMIC_CALL("libsvc_net.so", net_bluetoothUnpair,
			const_cast<char*>(dev.address().c_str()) );
	if( ret < 0 ) {
		if (errno != ENETDOWN) return ret;
		// else - we have the device on our list, but OS doesn't. Remove it.
	}
	ret = m_paired.remove( paired_index );
	if( ret < 0 ) return ret;
	m_paired.save();
	return ret;
}
/* ------------------------------------------------------------------ */ 
/** Get the paired device list 
	* @param[out] Paired device list
	* @param[out] Errno
	*/
int paired_list( std::vector<device>& dev )
{
	dev.clear();
	for( const auto& it : m_paired ) 
	{
		dev.push_back( it.second );
	}
	return 0;
}
/* ------------------------------------------------------------------ */ 
int paired_list( std::vector<network_profile>& dev )
{
	dev.clear();
	int cnt = m_paired.get_count();
	int profile_id = 0;
	device wcfg;
	while (cnt > 0) {
		if (!m_paired.get( profile_id, wcfg )) {
			dev.push_back( network_profile( wcfg, profile_id, profile_id==m_paired.get_default() ) );
			--cnt;
		}
		++profile_id;
	}
	return 0;
}
/* ------------------------------------------------------------------ */ 
/** 
* Get paired devices count
* @param[in] count Get paired profiles count
* @return error code
*/
int get_paired_count( int& count )
{
	count = m_paired.get_count();
	return 0;
}
/* ------------------------------------------------------------------ */
/** 
* Find paired device as an address
* @param[in] device Get paired profiles count
* @param[out] pair_index Paired index
* @return error code
*/
int find_paired( const device &device, int& pair_index )
{
	const auto ret = m_paired.find_profile( device );
	if( ret < 0 ) return ret;
	else { pair_index = ret; return 0; }
}
/* ------------------------------------------------------------------ */ 
/** 
	*  Set device as paired defaults
	*	@param[in] pair_index Pairing device index
	*	@return Error code
	*/
int set_paired_default( int pair_index )
{
	auto ret =  m_paired.set_default( pair_index );
	if( !ret ) m_paired.save();
	return ret;
}
/* ------------------------------------------------------------------ */
/** Get device as defualt paired
* @param[out] Get paired device info
* @return Error code
*/
int get_default_paired( device& cfg )
{
	auto pair_index = m_paired.get_default();
	if (pair_index < 0) return pair_index;
	auto ret = m_paired.get( pair_index, cfg );
	return 0;
}
/* ------------------------------------------------------------------ */ 
/** Get paired device profile id
	* @param[out] profile_id Paired profile index returned
	* @return Error code 
	*/
int get_default_paired_id( int &pair_index )
{
	pair_index = m_paired.get_default();
	if (pair_index < 0) {
		errno = ENOENT;
		return -1;
	}
	return 0;
}
/* ------------------------------------------------------------------ */ 
/** Get connected list of devices
	* @param[in] List of connected devices
	* @return error code
	*/
int get_connected( std::vector<device>& m_list )
{
	std::vector<std::string> raw_conns;
	int ret = detail::list_connected( raw_conns );
	if( ret < 0 ) return ret;
	for( const auto& r: raw_conns ) 
	{
		device dev( r );
		//Determine connection type
		errno = 0;
		auto status = PML_DYNAMIC_CALL( "libsvc_net.so", net_bluetoothGetStatus, 
			const_cast<char*>(r.c_str()) );
		if( errno ) { ret = -1; break; }
		dev.ethernet( status.is_pan_connected );
		dev.dial( status.is_dun_connected );
		m_list.push_back( dev );
	}
	return ret;
}
/* ------------------------------------------------------------------ */ 
} } }
/* ------------------------------------------------------------------ */ 

