/*
 * =====================================================================================
 *
 *       Filename:  lib_list.cpp
 *
 *    Description:  Library list version shared memory container
 *
 *        Version:  1.0
 *        Created:  07/19/2013 10:08:40 AM
 *       Revision:  
 *       Compiler:  gcc
 *
 *         Author:  Lucjan Bryndza
 *   Organization:  VERIFONE
 *
 * =====================================================================================
 */
#include "lib_list.hpp"
#include <cstring>

/* ---------------------------------------------------------------- */
namespace com_verifone_pml {
namespace internal {
/* ---------------------------------------------------------------- */
namespace ipc = boost::interprocess;
namespace {
	
    //IPC task list shared memory segment list
	const char lib_shm_name[] = "com_verifone_pml_lib_versions";    
}
/* ---------------------------------------------------------------- */
//Constructor
lib_list::lib_list()
    :  m_shm( ipc::open_or_create, lib_shm_name,
	   C_max_items * sizeof( value_type ) + C_header_size )
	,  m_shm_allocator(m_shm.get_segment_manager())
	,  m_libs( m_shm.find_or_construct<shared_map_t>("LibList")( comparable_type(),m_shm_allocator ) )
{
    
}
/* ---------------------------------------------------------------- */
//Register library
void lib_list::add_library( const char name[], const char ver[], const char app[] )
{
    lib_key key;
    lib_ver aver;
    std::strncpy( key.name, name, sizeof key.name );
    key.name[ sizeof lib_key::name -1 ] = '\0';
    std::strncpy( key.app, app, sizeof key.app );
    key.app[ sizeof lib_key::app -1 ] = '\0';

    std::strncpy( aver.ver, ver, sizeof aver.ver );
    aver.ver[ sizeof lib_ver::ver -1 ] = '\0';
    (*m_libs)[key] = aver;
}
/* ---------------------------------------------------------------- */
}}
/* ---------------------------------------------------------------- */
