/*****************************************************************************
 * 
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file        PMLInternal.cpp
 *
 * @author      Kamil Pawlowski
 * 
 * @brief       Implementation of internal class handling PML on Verix (Trident) devices
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C Coding  
 *              Standard 1.0.0 
 */

#include "PMLInternals.hpp"
#include "VxOS.hpp"
#include "SoftTimer.h"
#include "VxSharedMem.hpp"

#include <time.h>

#include <svc.h>
#include <svc_sec.h>
#include <errno.h>

#include <liblog/logsys.h>

// Workarounds for OS problems
/*
 * This define enables get_port_status() workaround for /dev/usbd
 * Said OS call always returns No data status, regardless of actual input pending count
 */
#define USB_PORT_STATUS_WORKAROUND


using namespace com_verifone_pml;
// using namespace com_verifone_pml_events;

namespace
{
	int validate_device_access(const char * device)
	{
		int ownerTask = 0;
		if (get_owner(device, &ownerTask) >= 0)
		{
			// dlog_msg("%s device is owned by %d, our id %d", device, ownerTask, get_task_id());
			//if (ownerTask == 0) return true;
			if (ownerTask == get_task_id()) return true;
			return false;
		}
		dlog_error("Get owner has failed, errno %d!", errno);
		return false;
	}
	
	unsigned long get_event_flag(const com_verifone_pml::events::evt event_id)
	{
		unsigned long result = 0;
		int eventNo = event_id - events::user0;
		if (eventNo >= 0 && eventNo <= 15)
		{
			result = 1;
			result <<= eventNo;
		}
		dlog_msg("Event flag %ld", result);
		return result;
	}
	
	class user_events
	{
		public:
			static user_events * get_instance()
			{
				if (!m_ptr)
				{
					m_ptr = new user_events;
					assert(m_ptr);
				}
				return m_ptr;
			}
			unsigned long generate_event()
			{
				//static unsigned long current_event = 1UL << 31;
				//static const unsigned long EVENT_LIMIT = 1UL << 15;
				//unsigned long event = current_event;
				//current_event >>= 1;
				//assert(current_event > EVENT_LIMIT);
				unsigned long event = find_first_free();
				assert(event != 0);
				return event;
			}
			void clear_event(unsigned long event)
			{
				for (int i=0; i<15; --i)
				{
					if (event & (1UL << (31-i)))
					{
						m_timers[i] = false;
						dlog_msg("Cleared user event id %d (%lu)", i, event);
						break;
					}
				}
				return;
			}
		private:
			static user_events * m_ptr;
			bool m_timers[31-15];
			user_events()
			{
				for (int i=0; i<sizeof(m_timers)/sizeof(m_timers[0]); ++i) m_timers[i] = false;
			}
			unsigned long find_first_free()
			{
				for (int i=0; i<sizeof(m_timers)/sizeof(m_timers[0]); ++i)
				{
					if (m_timers[i] == false)
					{
						m_timers[i] = true;
						unsigned long result = 1UL << (31-i);
						dlog_msg("Generated user event id %d (%Xh)", i, result);
						return result;
					}
				}
				return 0UL;
			}
			// noncopyable
	};
	user_events * user_events::m_ptr = 0;
}

namespace com_verifone_pml_events_internals
{
	bool operator==(const com_verifone_pml::event_item & lhs, const com_verifone_pml::event_item & rhs)
	{
		if (lhs.event_id != rhs.event_id) return false;
		if (lhs.event_id == events::comm || lhs.event_id == events::ipc)
		{
			//dlog_msg("Comm / IPC event: lhs hdl %d, rhs hdl %d", lhs.evt.com.fd, rhs.evt.com.fd);
			return lhs.evt.com.any == rhs.evt.com.any && lhs.evt.com.fd == rhs.evt.com.fd && lhs.evt.com.flags == rhs.evt.com.flags;
		}
		/*else if (lhs.event_id == events::ipc)
		{
			return lhs.evt.com.fd == rhs.evt.com.fd && lhs.evt.com.flags == rhs.evt.com.flags;
		} */
		else if (lhs.event_id >= events::user0 && lhs.event_id <= events::user15)
		{
			return lhs.event_id == rhs.event_id && lhs.evt.user.flags == rhs.evt.user.flags && lhs.evt.user.ext_flags == rhs.evt.user.ext_flags;
		}
		else if (lhs.event_id == events::timer)
		{
			// dlog_msg("Timer event: lhs id %d, rhs id %d", lhs.evt.tmr.id, rhs.evt.tmr.id);
			return lhs.evt.tmr.id == rhs.evt.tmr.id;
		}
		else if (lhs.event_id == events::signal_strength)
		{
			return lhs.evt.signal.value == rhs.evt.signal.value;
		}
		else if (lhs.event_id == events::netlink)
		{
			return lhs.evt.netlink._fd == rhs.evt.netlink._fd;
		}
		// Add other event types here
		return true;
	}
	bool operator!=(const com_verifone_pml::event_item & lhs, const com_verifone_pml::event_item & rhs)
	{
		return !operator==(lhs, rhs);
	}

	/**********************************************************************************************
	** PML event handler classes
	**********************************************************************************************/
	
	// ----------------------------- Virtual class for generic events -----------------------------
	PMLInternalHandler::PMLInternalHandler(const com_verifone_pml::event_item & evt):
		m_handle(INVALID_HANDLE),
		m_event(0),
		m_event_item(evt)
	{
		// empty
	}
	
	PMLInternalHandler::~PMLInternalHandler()
	{
		// empty
	}
	
	int PMLInternalHandler::check_event(long event) const
	{
		return (get_event_mask() & event);
	}
	
	
	// ----------------------------- Virtual class - notification handling (so for flags only) -----------------------------
	PMLInternalHandlerNotification::~PMLInternalHandlerNotification()
	{
		// empty
	}
	
	int PMLInternalHandlerNotification::check_pending_event() const
	{
		if (event_pending)
		{
			event_pending = false;
			return 1;
		}
		return 0;
	}
	
	
	// ----------------------------- Virtual class - thread handling (for notification events which must be ran as thread) -----------------------------
	//PMLInternalHandlerThread::PMLInternalHandlerThread(const com_verifone_pml::event_item & evt, unsigned long notify, thread_routine_t thread_routine, void * thread_params, int thread_stack /* = 1024 */): 
	//		PMLInternalHandlerNotification(evt), notification_flag(notify), thread_handle(fire_thread(thread_routine, thread_params, thread_stack))
	PMLInternalHandlerThread::PMLInternalHandlerThread(const com_verifone_pml::event_item & evt, unsigned long notify): PMLInternalHandlerNotification(evt), notification_flag(notify), thread_handle(INVALID_HANDLE)
	{
		// empty
	}
	
	PMLInternalHandlerThread::~PMLInternalHandlerThread()
	{
		// stop the thread here
		if (is_handle_valid())
		{
			dlog_msg("Stopping thread %d", thread_handle);
			post_user_event(thread_handle, notification_flag);
		}
	}
	
	bool PMLInternalHandlerThread::fire_thread(thread_routine_t thread_routine, void * thread_params, int thread_stack /* = 1024 */)
	{
		if (!is_handle_valid())
		{
			thread_handle = run_thread(reinterpret_cast<int>(thread_routine), reinterpret_cast<int>(thread_params), thread_stack);
			return thread_handle != INVALID_HANDLE;
		}
		return true; // Already running
	}
	
	
	// ----------------------------- Clock handling event class -----------------------------
	PMLClockEventHandler::PMLClockEventHandler(const com_verifone_pml::event_item & evt):
		PMLInternalHandlerNotification(evt), m_has_clock(validate_device_access(DEV_CLOCK))
	{
		m_event = EVT_CLK;
	}
	
	PMLClockEventHandler::~PMLClockEventHandler()
	{
		// empty
	}
	
	int PMLClockEventHandler::check_event(long event) const
	{
		// No need to validate clock access if the event came
		int res = PMLInternalHandler::check_event(event);
		if (res > 0) set_pending_event();
		return res;
	}
	
	
	// ----------------------------- Internal communication (IPC) handling event class -----------------------------
	PMLInternalCommsEventHandler::PMLInternalCommsEventHandler(const com_verifone_pml::event_item & evt):
		PMLInternalHandler(evt), event_pending(false)
	{
		m_event = EVT_PIPE;
		m_handle = evt.evt.com.fd;
	}
	
	PMLInternalCommsEventHandler::~PMLInternalCommsEventHandler()
	{
		// empty
	}
	
	int PMLInternalCommsEventHandler::check_pending_event() const
	{
		if (m_handle != INVALID_HANDLE)
		{
			// dlog_msg("Checking handle %d", m_handle);
			return pipe_pending(m_handle);
		}
		// Invalid handle handler
		if (event_pending)
		{
			// dlog_msg("PIPE event pending, clearing stat");
			event_pending = false;
			return 1;
		}
		return 0;
	}
	
	int PMLInternalCommsEventHandler::check_event(long event) const
	{
		if (m_event & event)
		{
			if (m_handle != INVALID_HANDLE)
			{
				return pipe_pending(m_handle);
			}
			// dlog_msg("Invalid handle, assume IPC event pending!");
			event_pending = true;
			return 1;
		}
		return 0;
	}
	
	bool PMLInternalCommsEventHandler::is_valid() const
	{
		if (m_handle == INVALID_HANDLE)
		{
			return true;
		}
		// seems legit, try to do somethign with it
		if (pipe_pending(m_handle) == -1)
		{
			if (errno == EBADF) return false;
		}
		return true;
	}

	
	
	// ----------------------------- External communication (serial ports / USB) handling event class -----------------------------
	PMLExternalCommsEventHandler::PMLExternalCommsEventHandler(const com_verifone_pml::event_item & evt):
		PMLInternalHandler(evt), m_workaround(false)
	{
		m_handle = evt.evt.com.fd;
		if (m_handle != INVALID_HANDLE)
		{
			m_event = com_verifone_VxOS::get_event_bit(m_handle);
			if (m_event != -1)
			{
				dlog_msg("External comms handle valid (%d), set event %ld", m_handle, m_event);
			}
			else
			{
				dlog_alert("Cannot detect device type!");
				m_event = 0;
			}
		}
		else
		{
			dlog_alert("Invalid handle!");
		}
	}
	
	PMLExternalCommsEventHandler::~PMLExternalCommsEventHandler()
	{
		// empty
	}
	
	int PMLExternalCommsEventHandler::check_pending_event() const
	{
		// External comms (serial / USB) - check port status
		#ifdef USB_PORT_STATUS_WORKAROUND
		if (m_workaround)
		{
			if (m_event == EVT_USB_CLIENT) dlog_alert("Workaround - get_port_status() call below does NOT work!");
			m_workaround = false;
			return 1;
		}
		#endif
		if (m_handle != INVALID_HANDLE)
		{
			char buf_tmp[4] = { 0 };
			get_port_status(m_handle, buf_tmp);
			//int stat = get_port_status(m_handle, buf_tmp);
			//dlog_hex(buf_tmp, 4, "PORT STATUS");
			//dlog_msg("%d bytes awaits on port, status result %d (handle %d)", buf_tmp[0], stat, m_handle);
			return buf_tmp[0];
		}
		return 0;
	}
	
	int PMLExternalCommsEventHandler::check_event(long event) const
	{
		if (m_event & event)
		{
			#ifdef USB_PORT_STATUS_WORKAROUND
			if (m_event == EVT_USB_CLIENT)
			{
				m_workaround = true;
			}
			#endif
			if (m_event == EVT_SOKT)
			{
				m_workaround = true;
			}
			return 1;
		}
		return 0;
	}
	
	
	// ----------------------------- Timer handling event class -----------------------------
	PMLTimerEventHandler::PMLTimerEventHandler(const com_verifone_pml::event_item & evt)
		: PMLInternalHandlerNotification(evt), m_timer_valid(validate_timer(evt)), m_timerId(evt.evt.tmr.id), m_counter(0)
	{
		m_event = com_verifone_SoftTimer::SoftTimerId::getInstance()->getSysEventMask();
		dlog_msg("Timer event: valid %i, id %i", m_timer_valid, m_timerId);
	}
	
	PMLTimerEventHandler::~PMLTimerEventHandler()
	{
		// empty
	}
	
	int PMLTimerEventHandler::check_event(long event) const
	{
		// Timer
		if (m_timer_valid)
		{
			using namespace com_verifone_SoftTimer;
			SoftTimerId * ptr = SoftTimerId::getInstance();
			if (event & m_event)
			{
				if (ptr->isTimerFired(m_timerId))
				{
					//dlog_msg("Timer fired!");
					++m_counter;
					set_pending_event();
					ptr->updateTimer(m_timerId);
					return 1;
				}
			}
		}
		return 0;
	}
	
	com_verifone_pml::event_item PMLTimerEventHandler::get_item()
	{
		m_event_item.evt.tmr.num_expires = m_counter;
		m_counter = 0;
		// dlog_msg(">>> Expired timer %lu: %d times", m_mask, m_event_item.evt.tmr.num_expires);
		return m_event_item;
	}
	
	bool PMLTimerEventHandler::validate_timer(const com_verifone_pml::event_item & evt)
	{
		using namespace com_verifone_SoftTimer;
		SoftTimerId * ptr = SoftTimerId::getInstance();
		int timerId = evt.evt.tmr.id;
		
		if (!ptr->isValid(timerId))
			return false;
		
		if (ptr->isTimerFired(timerId))
		{
			set_pending_event();
			ptr->updateTimer(timerId);
		}
		
		return true;
	}


#if 0
	bool PMLEOSHandler::monitor_netlink = false;
	bool PMLEOSHandler::monitor_signal = false;
	bool PMLEOSHandler::m_unregister = false;
	bool PMLEOSHandler::m_disable_event_notifications = false;
	bool PMLEOSHandler::m_disable_signal_notifications = false;
	bool PMLEOSHandler::m_initialized = false;
	bool PMLEOSHandler::m_has_network = false;
	int PMLEOSHandler::m_timer_id = -1;
	PMLEOSHandler::IFACE_NAMES PMLEOSHandler::interface_names;
	PMLEOSHandler::item_queue PMLEOSHandler::netlink_queue;
	PMLEOSHandler::item_queue PMLEOSHandler::signal_queue;

	PMLEOSHandler::PMLEOSHandler(const com_verifone_pml::event_item & evt):
		PMLInternalHandler(evt)
	{
		dlog_msg("Creating EOS Handler");
		m_event = EVT_PIPE;
		if (!m_initialized)
		{
			m_has_network = check_network();
			m_initialized = true;
		}
		if (interface_names.empty()) interface_names = setup_network_interfaces();
	}
	
	PMLEOSHandler::~PMLEOSHandler()
	{
		// Ignore errors here
		if (m_initialized && !monitor_netlink && !monitor_signal)
		{
			if (m_disable_signal_notifications) ceSetSignalNotification(CE_SF_OFF);
			if (m_disable_event_notifications) ceDisableEventNotification();
			if (m_unregister) ceUnregister();
			m_initialized = false;
		}
	}
	
	PMLEOSHandler::IFACE_NAMES PMLEOSHandler::setup_network_interfaces()
	{
		IFACE_NAMES inames;
		/*int NICount = ceGetNWIFCount();
		if (NICount == ECE_NOTREG)
		{
			int ret = ceRegister();
			if (ret != ECE_SUCCESS)
			{
				dlog_error("Thread %d, cannot register!", get_task_id());
				return inames;
			}
		} */
		// Get the total number of network interface from this terminal (NWIF)
		int NICount = ceGetNWIFCount();
		if (NICount <= 0)
		{
			dlog_error("There are no devices!");
			return inames;
		}
		stNIInfo * NIInfo = new stNIInfo[NICount];
		assert(NIInfo);
		unsigned int count = 0;
		if (ceGetNWIFInfo ( (stNIInfo*) NIInfo, NICount, &count) == ECE_SUCCESS)
		{
			dlog_msg("We have %d interfaces", NICount);
			for (int i = 0; i < NICount; ++i)
			{
				std::string name;
				if (strlen(NIInfo[i].niDeviceName)) name.assign(NIInfo[i].niDeviceName);
				inames.insert(std::make_pair(NIInfo[i].niHandle, name));
			}
		}
		delete [] NIInfo; NIInfo = 0;
		return inames;
	}

	bool PMLEOSHandler::check_network()
	{
		m_unregister = false;
		int NICount = ceGetNWIFCount();
		if (NICount == ECE_NOTREG)
		{
			m_unregister = true;
			int ret = ceRegister();
			if (ret != ECE_SUCCESS)
			{
				dlog_error("Network register failure %d!", ret);
				return false;
			}
		}
		NICount = ceGetNWIFCount();
		if (NICount > 0)
		{
			int ret = ceEnableEventNotification();
			dlog_msg("Enable network events result %d", ret);
			if (ret == ECE_SUCCESS || ret == ECE_EVENTS_AGAIN)
			{
				m_disable_event_notifications = (ret == ECE_SUCCESS);
				ret = ceSetSignalNotification(CE_SF_ON);
				dlog_msg("Enable signal notifications result %d", ret);
				m_disable_signal_notifications = (ret == ECE_SUCCESS);
				return true;
			}
		}
		else
		{
			dlog_error("No network interfaces!");
		}
		return false;
	}

	int PMLEOSHandler::check_eos_event() const
	{
		if (ceGetEventCount())
		{
			stceNWEvt NotificationEvt;
			char NotificationMsg[CEIF_EVT_DATA_SZ];
			int NotificationMsgLen;
			size_t index = 0;
			com_verifone_pml::event_item m_event_netlink_item;
			m_event_netlink_item.event_id = events::netlink;
			m_event_netlink_item.evt.netlink._fd = -1;
			m_event_netlink_item.evt.netlink.n_ifcs = 0;
			com_verifone_pml::event_item m_event_signal_item;
			m_event_signal_item.event_id = events::signal_strength;
			m_event_signal_item.evt.signal.value = -1;
			dlog_msg("EOS Handler: processing %d events", ceGetEventCount());
			while (ceGetEventCount() > 0 && index < sizeof(m_event_netlink_item.evt.netlink.ifc)/sizeof(m_event_netlink_item.evt.netlink.ifc[0]))
			{
				int retVal = ceGetEvent (&NotificationEvt, sizeof(NotificationMsg), NotificationMsg, &NotificationMsgLen);
				if(retVal == ECE_SUCCESS)
				{
					dlog_msg("Network CE Event:");
					dlog_msg("  niHandle:%u", NotificationEvt.niHandle);
					dlog_msg("  neEvt:%u",    NotificationEvt.neEvt);
					dlog_msg("  neParam1:%d", NotificationEvt.neParam1);
					dlog_msg("  neParam2:%d", NotificationEvt.neParam2);
					dlog_msg("  neParam3:%d", NotificationEvt.neParam3);
					if (NotificationMsgLen) dlog_hex(NotificationMsg, NotificationMsgLen, "[NotificationMsg]");
					dlog_msg("");
					bool validNetlinkItem = false;
					switch (NotificationEvt.neEvt)
					{
						case CE_EVT_STOP_CLOSE:
						case CE_EVT_STOP_NW:
							// Network is down - fall forward
						case CE_EVT_NET_OUT:
							// Cable is out
							m_event_netlink_item.evt.netlink.ifc[index].isup = false;
							validNetlinkItem = true;
							break;
						case CE_EVT_NET_UP:
							// Network is up - fall forward
						case CE_EVT_NET_RES:
							// Cable is back in
							m_event_netlink_item.evt.netlink.ifc[index].isup = true;
							validNetlinkItem = true;
							break;
						// WiFi events
						case CE_EVT_SIGNAL:
							if(NotificationEvt.neParam1 < 0)
							{
								m_event_signal_item.evt.signal.value = 0;
							}
							else if(NotificationEvt.neParam1 > 100)
							{
								m_event_signal_item.evt.signal.value = 100;
							}
							else
							{
								m_event_signal_item.evt.signal.value = NotificationEvt.neParam1;
							}
							break;
						case CE_EVT_NET_POOR:
							// Assume no network
							m_event_signal_item.evt.signal.value = 1;
							break;
						default:
							break;
					}
					if (validNetlinkItem)
					{
						// Set name
						m_event_netlink_item.evt.netlink.ifc[index].name[0] = 0;
						for (IFACE_NAMES::const_iterator it = interface_names.begin(); it != interface_names.end(); ++it)
						{
							if (it->first == NotificationEvt.niHandle)
							{
								strncpy(m_event_netlink_item.evt.netlink.ifc[index].name, it->second.c_str(), sizeof m_event_netlink_item.evt.netlink.ifc[index].name);
								break;
							}
						}
						++index;
						m_event_netlink_item.evt.netlink.n_ifcs = index;
					}
				}
			}
			if (m_event_signal_item.evt.signal.value != -1 && monitor_signal)
			{
				dlog_alert("Signal event (%d)!", m_event_signal_item.evt.signal.value);
				signal_queue.push(m_event_signal_item);
			}
			if (m_event_netlink_item.evt.netlink.n_ifcs > 0 && monitor_netlink)
			{
				dlog_alert("Netlink event (%d)!", m_event_netlink_item.evt.netlink.n_ifcs);
				netlink_queue.push(m_event_netlink_item);
			}
			return !signal_queue.empty() || !netlink_queue.empty();
		}
		return 0;
	}

	int PMLEOSHandler::initial_status(bool send_netlink, bool send_signal)
	{
		// Get device handle
		bool gen_event = false;
		if (send_netlink)
		{
			for (IFACE_NAMES::const_iterator it = interface_names.begin(); it != interface_names.end(); ++it)
			{
				stNI_NWIFState ifaceState;
				unsigned int pLen = 0;
				int rc = ceGetNWParamValue (it->first, NWIF_STATE, &ifaceState, sizeof(ifaceState), &pLen);
				if (rc == ECE_SUCCESS)
				{
					com_verifone_pml::event_item m_event_netlink_item;
					m_event_netlink_item.event_id = events::netlink;
					m_event_netlink_item.evt.netlink._fd = -1;
					m_event_netlink_item.evt.netlink.n_ifcs = 1;
					m_event_netlink_item.evt.netlink.ifc[0].isup = ifaceState.nsCurrentState >= NWIF_CONN_STATE_LINK;
					strncpy(m_event_netlink_item.evt.netlink.ifc[0].name, it->second.c_str(), sizeof m_event_netlink_item.evt.netlink.ifc[0].name);
					netlink_queue.push(m_event_netlink_item);
					dlog_msg("Generated initial netlink event");
				}
				else
				{
					dlog_error("Cannot push network interface %s state, error %d", it->second.c_str(), rc);
				}
			}
			gen_event = !netlink_queue.empty();
		}
		if (send_signal)
		{
			for (IFACE_NAMES::const_iterator it = interface_names.begin(); it != interface_names.end(); ++it)
			{
				unsigned int len;
				char buf[CEIF_PARAM_VALUE_SZ];
				int rc = ceGetDDParamValue(it->first, IOCTL_GET_RSSI, sizeof(buf), buf, &len);
				if (rc == ECE_SUCCESS)
				{
					*(buf+len) = 0;
					com_verifone_pml::event_item m_event_signal_item;
					m_event_signal_item.event_id = events::signal_strength;
					if (sscanf(buf, "%d", &m_event_signal_item.evt.signal.value) == 1)
					{
						signal_queue.push(m_event_signal_item);
						dlog_msg("Generated initial signal event");
					}
					else
					{
						dlog_error("Invalid value returned (%s)", buf);
						dlog_hex(buf, len, "DUMP");
					}
				}
				else
				{
					dlog_error("Cannot get signal info for interface %s, error %d", it->second.c_str(), rc);
				}
			}
			gen_event = !signal_queue.empty();
		}
		if (gen_event)
		{
			//m_timer_id = set_timer(10, EVT_PIPE);
			return 1;
		}
		return 0;
	}
#endif

	// ----------------------------- Netlink handling event class -----------------------------
	PMLNetlinkEventHandler::PMLNetlinkEventHandler(const com_verifone_pml::event_item & evt):
		PMLEOSHandler(evt)
	{
		monitor_netlink = true;
		initial_status(true, false);
	}

	PMLNetlinkEventHandler::~PMLNetlinkEventHandler()
	{
		monitor_netlink = false;
	}

	int PMLNetlinkEventHandler::check_event(long event) const
	{
		if (event & m_event)
		{
			check_eos_event();
			return netlink_queue.size();
		}
		return 0;
	}
	
	com_verifone_pml::event_item PMLNetlinkEventHandler::get_item()
	{
		m_event_item = netlink_queue.front();
		netlink_queue.pop();
		return m_event_item;
	}

	// ----------------------------- Signal handling event class -----------------------------
	PMLSignalEventHandler::PMLSignalEventHandler(const com_verifone_pml::event_item & evt):
		PMLEOSHandler(evt)
	{
		monitor_signal = true;
		initial_status(false, true);
	}
	
	PMLSignalEventHandler::~PMLSignalEventHandler()
	{
		monitor_signal = false;
	}
	
	int PMLSignalEventHandler::check_event(long event) const
	{
		if (event & m_event)
		{
			check_eos_event();
			return signal_queue.size();
		}
		return 0;
	}
	
	com_verifone_pml::event_item PMLSignalEventHandler::get_item()
	{
		m_event_item = signal_queue.front();
		signal_queue.pop();
		return m_event_item;
	}
	
	// ----------------------------- Keyboard handling event class -----------------------------
	PMLKeyboardEventHandler::PMLKeyboardEventHandler(const com_verifone_pml::event_item & evt):
		PMLInternalHandler(evt), m_has_kbd(validate_device_access(DEV_CONSOLE))
	{
		m_event = EVT_KBD;
	}
	
	PMLKeyboardEventHandler::~PMLKeyboardEventHandler()
	{
		// empty
	}
	
	int PMLKeyboardEventHandler::check_pending_event() const
	{
		return kbd_pending_count();
	}
	
	int PMLKeyboardEventHandler::check_event(long event) const
	{
		// Keyboard
		return PMLInternalHandler::check_event(event);
	}
	
	
	// ----------------------------- ICC handling event class -----------------------------
	PMLICCEventHandler::PMLICCEventHandler(const com_verifone_pml::event_item & evt):
		PMLInternalHandler(evt), m_has_icc(validate_device_access(DEV_ICC1))
	{
		m_event = EVT_ICC1_INS | EVT_ICC1_REM;
	}
	
	PMLICCEventHandler::~PMLICCEventHandler()
	{
		// empty
	}
	
	int PMLICCEventHandler::check_pending_event() const
	{
		return 0;
	}
	
	int PMLICCEventHandler::check_event(long event) const
	{
		return PMLInternalHandler::check_event(event);
	}
	
	
	// ----------------------------- Magstripe handling event class -----------------------------
	PMLMagEventHandler::PMLMagEventHandler(const com_verifone_pml::event_item & evt):
		PMLInternalHandler(evt), m_has_mag(validate_mag_presence())
	{
		m_event = EVT_MAG;
	}
	
	PMLMagEventHandler::~PMLMagEventHandler()
	{
		// empty
	}
	
	int PMLMagEventHandler::check_pending_event() const
	{
		return card_pending() == 1;
	}
	
	int PMLMagEventHandler::check_event(long event) const
	{
		return PMLInternalHandler::check_event(event);
		//if (m_event & event) return check_pending_event();
		//return 0;
	}
	
	bool PMLMagEventHandler::validate_mag_presence() const
	{
		char buf[2];
		SVC_INFO_MAG(buf);
		if (buf[0] == 0) return false;
		return card_pending() != -1;
	}
	
	
	// ----------------------------- Touch handling event class -----------------------------
	PMLTouchEventHandler::PMLTouchEventHandler(const com_verifone_pml::event_item & evt):
		PMLInternalHandlerNotification(evt)
	{
		m_event = EVT_BAR;
	}
	
	PMLTouchEventHandler::~PMLTouchEventHandler()
	{
		// empty
	}
	
	int PMLTouchEventHandler::check_event(long event) const
	{
		int res = PMLInternalHandler::check_event(event);
		if (res > 0) set_pending_event();
		return res;
	}
	
	
	// ----------------------------- Case removal handling event class -----------------------------
	PMLCaseRemovalEventHandler::PMLCaseRemovalEventHandler(const com_verifone_pml::event_item & evt):
		PMLInternalHandlerNotification(evt)
	{
		m_event = EVT_REMOVED;
	}
	
	PMLCaseRemovalEventHandler::~PMLCaseRemovalEventHandler()
	{
		// empty
	}
	
	int PMLCaseRemovalEventHandler::check_event(long event) const
	{
		int res = PMLInternalHandler::check_event(event);
		if (res > 0) set_pending_event();
		return res;
	}
	
	// ----------------------------- Case removal handling event class -----------------------------
	PMLActivateEventHandler::PMLActivateEventHandler(const com_verifone_pml::event_item & evt):
		PMLInternalHandlerNotification(evt)
	{
		m_event = EVT_ACTIVATE;
	}
	
	PMLActivateEventHandler::~PMLActivateEventHandler()
	{
		// empty
	}
	
	int PMLActivateEventHandler::check_event(long event) const
	{
		int res = PMLInternalHandler::check_event(event);
		if (res > 0) set_pending_event();
		return res;
	}
	
	

	// ----------------------------- USB device notification handling event class -----------------------------
	PMLUSBDeviceEventHandler::PMLUSBDeviceEventHandler(const com_verifone_pml::event_item & evt):
		PMLInternalHandlerNotification(evt)
	{
		m_event = EVT_USB;
	}
	
	PMLUSBDeviceEventHandler::~PMLUSBDeviceEventHandler()
	{
		// empty
	}
	
	int PMLUSBDeviceEventHandler::check_event(long event) const
	{
		int res = PMLInternalHandler::check_event(event);
		if (res > 0) set_pending_event();
		return res;
	}
	
	// ----------------------------- Tamper handling event class (thread!) -----------------------------
	namespace tamper_thread
	{
		struct tamper_params
		{
			struct event_tamper event;
			int thread_timer;
			unsigned long user_event;
			int parent_id;
		};
		
		int thread_routine(struct tamper_params * params)
		{
			assert(params);
			long interestingEvents = EVT_TIMER | EVT_USER;
			int state = isAttacked();
			dlog_msg("Thread %d started, timer %d, will report tamper", get_task_id(), params->thread_timer);
			dlog_msg("Thread %d, state %d", get_task_id(), state);
			if (state != 0)
			{
				int res = post_user_event(params->parent_id, params->user_event);
				if (res == -1) error_tone();
				dlog_msg("Thread %d, attacked initially, terminating", get_task_id());
				return 0;
			}
			int timerID = set_timer(params->thread_timer, EVT_TIMER);
			do
			{
				long events = wait_evt(interestingEvents);
				// dlog_msg("Thread %d, events %lu", get_task_id(), events);
				if (events & EVT_TIMER)
				{
					// check tamper
					state = isAttacked();
					clr_timer(timerID);
					if (state != 0)
					{
						dlog_alert("Thread %d, terminal is attacked, terminating", get_task_id());
						int res = post_user_event(params->parent_id, params->user_event);
						if (res == -1) error_tone();
						// We're done
						break;
					}
					timerID = set_timer(params->thread_timer, EVT_TIMER);
				}
				if (events & EVT_USER)
				{
					unsigned long flags = read_user_event();
					dlog_msg("Thread %d, user event %lu", get_task_id(), flags);
					if (flags == params->user_event)
					{
						dlog_alert("Thread %d, we are done!", get_task_id());
						// we're done!
						break;
					}
				}
			} while(true);
			clr_timer(timerID);
			return 0;
		}
	}
	PMLTamperEventHandler::PMLTamperEventHandler(const com_verifone_pml::event_item & evt): 
			PMLInternalHandlerThread(evt, user_events::get_instance()->generate_event()), thread_params(NULL)
			//PMLInternalHandlerThread(evt, generate_event()
	{
		fire_thread((PMLInternalHandlerThread::thread_routine_t)tamper_thread::thread_routine, generate_thread_params(evt));
		m_event = EVT_USER;
	}
	
	PMLTamperEventHandler::~PMLTamperEventHandler()
	{
		if (thread_params)
		{
			tamper_thread::tamper_params * ptr = static_cast<tamper_thread::tamper_params *>(thread_params);
			delete ptr;
			user_events::get_instance()->clear_event(notification_flag);
		}
	}
	
	int PMLTamperEventHandler::check_event(long event) const
	{
		int result = 0;
		if (event & EVT_USER)
		{
			unsigned long userEvent = read_user_event();
			unsigned long flags = get_notification_flag();
			if (userEvent & flags)
			{
				result = 1;
				// set event
				set_pending_event();
				userEvent &= ~flags; // clear the flag
			}
			// Are there some flags to be set?
			if (userEvent)
			{
				post_user_event(get_task_id(), userEvent);
			}
		}
		return result;
	}
	
	bool PMLTamperEventHandler::is_valid() const
	{
		return is_handle_valid();
	}
	
	void * PMLTamperEventHandler::generate_thread_params(const com_verifone_pml::event_item & evt)
	{
		tamper_thread::tamper_params * ptr = new tamper_thread::tamper_params;
		assert(ptr);
		ptr->event = evt.evt.tamper;
		ptr->thread_timer = 10000; // check every 10 seconds
		ptr->user_event  = get_notification_flag();
		ptr->parent_id = get_task_id();
		thread_params = ptr;
		return ptr;
	}
	
	com_verifone_pml::event_item PMLTamperEventHandler::get_item()
	{
		// Tamper state
		m_event_item.evt.tamper.is_tampered = isAttacked();
		return m_event_item;
	}
	
	// ----------------------------- Battery handling event class (thread!) -----------------------------
	namespace battery_thread
	{
		struct battery_params
		{
			struct event_battery event;
			int thread_timer;
			unsigned long user_event;
			int parent_id;
		};
		
		int thread_routine(struct battery_params * params)
		{
			assert(params);
			long interestingEvents = EVT_TIMER | EVT_USER | EVT_SYSTEM;
			int timerID = 0;
			const int batteryFull = get_battery_value(FULLCHARGE);
			int batteryLevel = (get_battery_value(REMAININGCHARGE) * 100) / batteryFull;
			if (batteryLevel == 0) batteryLevel = 1;
			bool isDocked = get_dock_sts() == 0;
			bool isCharging = get_battery_value(CHARGERSTATUS) == 3;
			dlog_msg("Thread %d started, timer %d, will report changes by %d percent", get_task_id(), params->thread_timer, params->event.percent);
			dlog_msg("Thread %d, docked %d, level %d", get_task_id(), isDocked, batteryLevel);
			dlog_msg("Thread %d: Setting timer (%lu)", get_task_id(), params->thread_timer);
			post_user_event(params->parent_id, params->user_event); //Post initial event so that the app can initialize battery meters
			timerID = set_timer(params->thread_timer, EVT_TIMER);
			do
			{
				long events = wait_evt(interestingEvents);
				// dlog_msg("Thread %d, events %lu", get_task_id(), events);
				if ((events & EVT_TIMER) || (events & EVT_SYSTEM))
				{
					// check battery
					int currentBatteryLevel = (get_battery_value(REMAININGCHARGE) * 100) / batteryFull;
					if (currentBatteryLevel == 0) currentBatteryLevel = 1;
					int battPercentDiff = (batteryLevel * 100) / currentBatteryLevel;
					if (batteryLevel < currentBatteryLevel) battPercentDiff = 100 - battPercentDiff;
					else battPercentDiff -= 100;
					bool currentIsDocked = get_dock_sts() == 0;
					bool currentIsCharging = get_battery_value(CHARGERSTATUS) == 3;
					dlog_msg("Thread %d, tick, docked %d, charging %d, level %d, diff %d", get_task_id(), currentIsDocked, currentIsCharging, currentBatteryLevel, battPercentDiff);
					if (battPercentDiff >= params->event.percent || isDocked != currentIsDocked || isCharging != currentIsCharging)
					{
						// signal!
						//post_user_event(params->parent_id, params->user_event);
						// ctmp uncomment the above, remove the below
						batteryLevel = currentBatteryLevel;
						isDocked = currentIsDocked;
						isCharging = currentIsCharging;
						int res = post_user_event(params->parent_id, params->user_event);
						if (res == -1) error_tone();
						// ctmp
					}
					clr_timer(timerID);
					timerID = set_timer(params->thread_timer, EVT_TIMER);
				}
				if (events & EVT_USER)
				{
					unsigned long flags = read_user_event();
					dlog_msg("Thread %d, user event %lu", get_task_id(), flags);
					if (flags == params->user_event)
					{
						// we're done!
						break;
					}
				}
			} while(true);
			clr_timer(timerID);
			return 0;
		}
	}
	PMLBatteryEventHandler::PMLBatteryEventHandler(const com_verifone_pml::event_item & evt): 
			PMLInternalHandlerThread(evt, user_events::get_instance()->generate_event()), thread_params(NULL)
			//PMLInternalHandlerThread(evt, generate_event()
	{
		if (get_battery_sts() >= 0)
		{
			fire_thread((PMLInternalHandlerThread::thread_routine_t)battery_thread::thread_routine, generate_thread_params(evt));
			m_event = EVT_USER;
		}
	}
	
	PMLBatteryEventHandler::~PMLBatteryEventHandler()
	{
		if (thread_params)
		{
			battery_thread::battery_params * ptr = static_cast<battery_thread::battery_params *>(thread_params);
			delete ptr;
			user_events::get_instance()->clear_event(notification_flag);
		}
	}
	
	int PMLBatteryEventHandler::check_event(long event) const
	{
		int result = 0;
		if (event & EVT_USER)
		{
			unsigned long userEvent = read_user_event();
			unsigned long flags = get_notification_flag();
			if (userEvent & flags)
			{
				result = 1;
				// set event
				set_pending_event();
				userEvent &= ~flags; // clear the flag
			}
			// Are there some flags to be set?
			if (userEvent)
			{
				post_user_event(get_task_id(), userEvent);
			}
		}
		return result;
	}
	
	bool PMLBatteryEventHandler::is_valid() const
	{
		return ((get_battery_sts() >= 0) && is_handle_valid());
	}
	
	void * PMLBatteryEventHandler::generate_thread_params(const com_verifone_pml::event_item & evt)
	{
		battery_thread::battery_params * ptr = new battery_thread::battery_params;
		assert(ptr);
		ptr->event = evt.evt.batt;
		//ptr->thread_timer = 1000;
		//ptr->thread_timer = 60000; // one minute!
		ptr->thread_timer = 10000; // check every ten seconds
		ptr->user_event  = get_notification_flag();
		ptr->parent_id = get_task_id();
		thread_params = ptr;
		return ptr;
	}
	
	com_verifone_pml::event_item PMLBatteryEventHandler::get_item()
	{
		//  battery percent
		int batteryLevel = get_battery_value(REMAININGCHARGE) * 100;
		int batteryFull = get_battery_value(FULLCHARGE);
		//dlog_msg( "batt val=%d, full=%d", batteryLevel, batteryFull );
		if( batteryFull != 0 )
		{
			m_event_item.evt.batt.percent = batteryLevel/batteryFull;
		}
		else
		{
			m_event_item.evt.batt.percent = 0;
		}
		// dock status
		m_event_item.evt.batt.status = 0;
		if (!get_dock_sts()) m_event_item.evt.batt.status |= BATT_DOCKED;
		if (get_battery_value(CHARGERSTATUS) == 3) m_event_item.evt.batt.status |= BATT_CHARGING;
		return m_event_item;
	}


	// ----------------------------- Barcode handling event class -----------------------------
	PMLBarcodeEventHandler::PMLBarcodeEventHandler(const com_verifone_pml::event_item & evt):
		PMLInternalHandlerNotification(evt)
	{
		m_event = EVT_BAR;
	}
	
	PMLBarcodeEventHandler::~PMLBarcodeEventHandler()
	{
		// empty
	}
	
	int PMLBarcodeEventHandler::check_event(long event) const
	{
		return PMLInternalHandler::check_event(event);

		/*dlog_msg("PMLBarcodeEventHandler::check_event 0x%X", event);
		if (m_event & event)
		{
			return 1;
		}
		
		return 0;*/
	}


	// ----------------------------- User events handling event class -----------------------------
	PMLUserEventHandler::PMLUserEventHandler(const com_verifone_pml::event_item & evt):
		PMLInternalHandlerNotification(evt), flag(get_event_flag(evt.event_id))
	{
		m_event = EVT_USER;
	}
	
	PMLUserEventHandler::~PMLUserEventHandler()
	{
		// empty
	}
	
	int PMLUserEventHandler::check_event(long event) const
	{
		int result = 0;
		if (event & EVT_USER)
		{
			unsigned long userEvent = read_user_event();
			if (userEvent & flag)
			{
				result = 1;
				set_pending_event();
				userEvent &= ~flag; // clear the flag
			}
			// Are there some flags to be set?
			if (userEvent)
			{
				post_user_event(get_task_id(), userEvent);
			}
		}
		return result;
	}
	
	event_item PMLUserEventHandler::get_item() // gets updated item
	{
		int flags = 0, ext_flags = 0;
		if (com_verifone_VxSharedMemory::get_value( get_task_id(), flags, ext_flags ) == 0)
		{
			dlog_msg("User event flags %d, ext flags %d", flags, ext_flags);
			m_event_item.evt.user.flags = flags;
			m_event_item.evt.user.ext_flags = ext_flags;
		}
		else
		{
			m_event_item.evt.user.flags = 0;
		}
		return m_event_item;
	}
	

	/**********************************************************************************************
	** PML Event class
	**********************************************************************************************/
	PMLEvent::PMLEvent(const com_verifone_pml::event_item & evt): 
		//item(evt), m_handler(0)
		m_handler(create_handler(evt))
	{
		// empty
	}
	PMLInternalHandler * PMLEvent::create_handler(const com_verifone_pml::event_item & evt)
	{
		PMLInternalHandler * local_handler = 0;
		switch (evt.event_id)
		{
			case events::comm:
				//dlog_msg("Serial comms");
				local_handler = new PMLExternalCommsEventHandler(evt);
				assert(local_handler);
				// add tcp comms handling (we must know the event!)
				break;
			case events::ipc:
				//dlog_msg("Internal comms");
				local_handler = new PMLInternalCommsEventHandler(evt);
				assert(local_handler);
				break;
			case events::timer:
				local_handler = new PMLTimerEventHandler(evt);
				assert(local_handler);
				break;
			case events::netlink:
				local_handler = new PMLNetlinkEventHandler(evt);
				assert(local_handler);
				break;
			case events::signal_strength:
				local_handler = new PMLSignalEventHandler(evt);
				assert(local_handler);
				break;
			case events::keyboard:
				local_handler = new PMLKeyboardEventHandler(evt);
				assert(local_handler);
				break;
			case events::icc:
				local_handler = new PMLICCEventHandler(evt);
				assert(local_handler);
				break;
			case events::mag:
				local_handler = new PMLMagEventHandler(evt);
				assert(local_handler);
				break;
			case events::touch:
				local_handler = new PMLTouchEventHandler(evt);
				assert(local_handler);
				break;
			case events::clock:
				local_handler = new PMLClockEventHandler(evt);
				assert(local_handler);
				break;
			case events::case_removal:
				local_handler = new PMLCaseRemovalEventHandler(evt);
				assert(local_handler);
				break;
			case events::activate:
				local_handler = new PMLActivateEventHandler(evt);
				assert(local_handler);
				break;
			case events::usb_device:
				local_handler = new PMLUSBDeviceEventHandler(evt);
				assert(local_handler);
				break;
			case events::battery:
				local_handler = new PMLBatteryEventHandler(evt);
				assert(local_handler);
				break;
			case events::barcode:
				local_handler = new PMLBarcodeEventHandler(evt);
				assert(local_handler);
				break;
			case events::tamper:
				local_handler = new PMLTamperEventHandler(evt);
				assert(local_handler);
				break;
			case events::user0:
			case events::user1:
			case events::user2:
			case events::user3:
			case events::user4:
			case events::user5:
			case events::user6:
			case events::user7:
			case events::user8:
			case events::user9:
			case events::user10:
			case events::user11:
			case events::user12:
			case events::user13:
			case events::user14:
			case events::user15:
				local_handler = new PMLUserEventHandler(evt);
				assert(local_handler);
				break;
			// Add other event types here
			
			default:
				dlog_error("Unsupported event type %ld!", evt.event_id);
				break;
		}
		if (local_handler && !local_handler->is_valid())
		{
			dlog_alert("Invalid event!");
			delete local_handler; local_handler = 0;
		}
		return local_handler;
	}

	PMLEvent::~PMLEvent()
	{
		delete m_handler;
	}

	int PMLEvent::our_event(long event, bool read) const
	{
		if (is_valid())
		{
			int result;
			if (read) result = m_handler->read_event(event);
			else result = m_handler->check_event(event);
			//int result = m_handler->check_event(event);
			if (result >= 0) return result;
		}
		return 0;
	}
	
	int PMLEvent::check_pending_event() const
	{
		if (is_valid())
		{
			int result = m_handler->check_pending_event();
			if (result >= 0) return result;
		}
		return 0;
	}
	
	bool operator==(const PMLEvent & lhs, const PMLEvent & rhs)
	{
		if (lhs.read_item() != rhs.read_item()) return false;
		if (lhs.is_valid() != rhs.is_valid()) return false;
		if (lhs.is_valid())
		{
			if (lhs.m_handler->get_handle() != rhs.m_handler->get_handle()) return false;
			if (lhs.m_handler->get_event_mask() != rhs.m_handler->get_event_mask()) return false;
		}
		return true;
	}
	
	
	/**********************************************************************************************
	** PML Collection class
	**********************************************************************************************/
	PMLCollection::PMLCollection(int handle): eventMask_internal(0), ourHandle(handle), fHasPendingEvents(false)
	{
		//
	}
	PMLCollection::~PMLCollection()
	{
		while (!eventSet.empty())
		{
			PMLEvent * ptr = eventSet.back();
			eventSet.pop_back();
			delete ptr;
		}
	}

	int PMLCollection::ctl(ctl::op_t op, const event_item & event)
	{
		switch(op)
		{
			case ctl::ADD:
			{
				PMLEvent * evt = new PMLEvent(event);
				if (!evt->is_valid())
				{
					return -1;
				}
				if (!we_have_event(evt))
				{
					eventSet.push_back(evt);
					eventMask_internal |= evt->get_event_mask();
					dlog_msg("Event %ld appended (Vx event %Xh)! Our internal events now %Xh", evt->get_event_id(), evt->get_event_mask(), eventMask_internal);
				}
				else
				{
					dlog_alert("Event type %d already added!", event.event_id);
					dlog_hex(&event, sizeof(event_item), "DUMP");
					delete evt;
				}
				break;
			}
			case ctl::DEL:
			{
				eventMask_internal = 0;
				for (eventset_t::iterator it = eventSet.begin (); it != eventSet.end ();)
				{
					if ( (*it)->read_item() == event )
					{
						dlog_msg("Removing event");
						PMLEvent * ptr = *it;
						// dlog_hex(&event, sizeof(event), "EVENT DUMP");
						it = eventSet.erase (it);
						delete ptr;
					}
					else
					{
						eventMask_internal |= (*it)->get_event_mask();
						++it;
					}
				}
				break;
			}
			default:
				return 1;
		}
		return 0;
	}
	
	int PMLCollection::ctl(ctl::op_t op, PMLCollection * const ptr_add)
	{
		assert(ptr_add);
		switch(op)
		{
			case ctl::ADD:
				if (!we_have_collection(ptr_add))
				{
					collectionSet.push_back(ptr_add);
				}
				else
				{
					dlog_alert("Event already added!");
				}
				break;
			case ctl::DEL:
				for (collectionset_t::iterator it = collectionSet.begin (); it != collectionSet.end ();)
				{
					if (*it == ptr_add)
					{
						it = collectionSet.erase (it);
					}
					else
					{
						++it;
					}
				}
				break;
			default:
				return 1;
		}
		return 0;

	}


	int PMLCollection::our_events(const long lnEvent, com_verifone_pml::eventset_t & retEvents, bool read) const
	{
		//for (eventset_t::size_type i = 0, iend = eventSet.size(); i<iend; i++) 
		fHasPendingEvents = false;
		for (eventset_t::const_iterator it = eventSet.begin(); it != eventSet.end(); ++it)
		{
			int res;
			if (read) res = (*it)->read_event(lnEvent);
			else res = (*it)->check_event(lnEvent);
			if (res)
			{
				// dlog_msg("Match: event %d (received %d). Reading %d", (*it)->get_event_mask(), lnEvent, read);
				if (read) retEvents.push_back((*it)->get_item());
				else retEvents.push_back((*it)->read_item());
				fHasPendingEvents = true;
			}
		}
		com_verifone_pml::eventset_t localEvents;
		for (collectionset_t::const_iterator it = collectionSet.begin(); it != collectionSet.end(); ++it)
		{
			// always check events in cascade!
			if ((*it)->check_our_events(lnEvent, localEvents))
			{
				com_verifone_pml::event_item item;
				item.event_id = com_verifone_pml::events::cascade;
				item.evt.cascade.fd = (*it)->get_handle();
				retEvents.push_back(item);
				localEvents.clear();
			}
		}

		return retEvents.size();
	}

	bool PMLCollection::has_pending_events() const
	{
		if (fHasPendingEvents) return true;
		for (eventset_t::const_iterator it = eventSet.begin(); it != eventSet.end(); ++it)
		{
			if ((*it)->check_pending_event() > 0)
			{
				dlog_msg("Pending event %d", (*it)->get_event_mask());
				return true;
			}
		}
		for (collectionset_t::const_iterator it = collectionSet.begin(); it != collectionSet.end(); ++it)
		{
			if ((*it)->has_pending_events())
			{
				return true;
			}
		}
		return false;
	}

	int PMLCollection::check_pending_events(com_verifone_pml::eventset_t & retEvents) const
	{
		//for (eventset_t::size_type i = 0, iend = eventSet.size(); i<iend; i++) 
		fHasPendingEvents = false;
		for (eventset_t::const_iterator it = eventSet.begin(); it != eventSet.end(); ++it)
		{
			if ((*it)->check_pending_event() > 0)
			{
				dlog_msg("Pending event %Xh", (*it)->get_event_mask());
				retEvents.push_back((*it)->get_item());
			}
		}
		/*com_verifone_pml::eventset_t localEvents;
		for (collectionset_t::const_iterator it = collectionSet.begin(); it != collectionSet.end(); ++it)
		{
			if ((*it)->check_pending_events(localEvents))
			{
				com_verifone_pml::event_item item;
				item.event_id = com_verifone_pml::events::cascade;
				item.evt.cascade.fd = (*it)->get_handle();
				retEvents.push_back(item);
				localEvents.clear();
			}
		} */
		for (collectionset_t::const_iterator it = collectionSet.begin(); it != collectionSet.end(); ++it)
		{
			if ((*it)->has_pending_events())
			{
				com_verifone_pml::event_item item;
				item.event_id = com_verifone_pml::events::cascade;
				item.evt.cascade.fd = (*it)->get_handle();
				retEvents.push_back(item);
			}
		}

		return retEvents.size();
	}

	bool PMLCollection::we_have_event(PMLEvent const * event) const
	{
		for (eventset_t::const_iterator ourIt = eventSet.begin(); ourIt != eventSet.end(); ++ourIt)
		{
			if (*event == *(*ourIt)) return true;
		}
		return false;
	}

	bool PMLCollection::we_have_collection(PMLCollection * const add_ptr) const
	{
		for (collectionset_t::const_iterator it = collectionSet.begin(); it != collectionSet.end(); ++it)
		{
			if (*it == add_ptr) return true;
		}
		return false;
	}

	int PMLCollection::close(PMLCollection const * ptr)
	{
		for (collectionset_t::iterator it = collectionSet.begin(); it != collectionSet.end(); ++it)
		{
			if (*it == ptr)
			{
				collectionSet.erase(it);
				return 1;
			}
		}
		return 0;
	}
	
	long PMLCollection::get_event_mask() const
	{
		long eventMask_external = 0;
		for (collectionset_t::const_iterator it = collectionSet.begin(); it != collectionSet.end(); ++it)
		{
			eventMask_external |= (*it)->get_event_mask();
		}
		return eventMask_internal | eventMask_external;
	}

	std::size_t PMLCollection::get_all_timers(timer_id_list_t & timerIdList) const
	{
		fHasPendingEvents = false;
		for (eventset_t::const_iterator it = eventSet.begin(); it != eventSet.end(); ++it)
		{
			com_verifone_pml::event_item ev = (*it)->read_item();
			
			if (ev.event_id == events::timer)
				timerIdList.push_back(ev.evt.tmr.id);
		}
		
		for (collectionset_t::const_iterator it = collectionSet.begin(); it != collectionSet.end(); ++it)
		{
			(*it)->get_all_timers(timerIdList);
		}
		
		return timerIdList.size();
	}
	
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////

	PMLEventHandler * PMLEventHandler::ptr = 0;

	PMLEventHandler::PMLEventHandler()
	{
		srand(time(0));
	}

	PMLEventHandler::~PMLEventHandler()
	{
		for (mapEvents::iterator it = myMap.begin(); it != myMap.end(); ++it)
		{
			PMLCollection * ptr = it->second;
			if (ptr) delete ptr;
		}
		myMap.clear();
	}

	int PMLEventHandler::create()
	{
		int randNo = 0;
		do
		{
			randNo = rand();
			if (myMap.find(randNo) == myMap.end())
			{
				break;
			}
			SVC_WAIT(0);
		} while(true);

		PMLCollection * ptr = new PMLCollection(randNo);
		if (ptr == 0) return -1;
		myMap.insert( std::make_pair (randNo, ptr) );
		return randNo;
	}

	bool PMLEventHandler::is_handle_valid(const int handle) const
	{
		mapEvents::const_iterator itExec = myMap.find(handle);
		return itExec != myMap.end();
	}

	PMLCollection * PMLEventHandler::getEventsHandler(const int handle) const
	{
		mapEvents::const_iterator itExec = myMap.find(handle);
		assert(itExec != myMap.end());
		return itExec->second;
	}

	int PMLEventHandler::ctl(const int handle, const ctl::op_t op, const eventset_t & event)
	{
		PMLCollection * ptr = getEventsHandler(handle);
		int result = 0;
		for (eventset_t::const_iterator it = event.begin(); it != event.end(); ++it)
		{
			int intRes = ptr->ctl(op, *it);
			if (intRes != 0) result = intRes;
		}
		return result;
	}

	int PMLEventHandler::ctl(const int handle, const com_verifone_pml::ctl::op_t op, const int add_handle)
	{
		PMLCollection * ptr = getEventsHandler(handle);
		PMLCollection * ptr_add = getEventsHandler(add_handle);
		//if (ptr != INVALID_HANDLE && ptr_add != INVALID_HANDLE)
		return ptr->ctl(op, ptr_add);
	}

	int PMLEventHandler::wait(const int handle, eventset_t & retEvents, long timeout)
	{
		using namespace com_verifone_SoftTimer;
		//com_verifone_VxOS::TimerOS idleTimer(EVT_TIMER);
		long idleTimer = SoftTimerId::INVALID_ID;
		PMLCollection * ptr = getEventsHandler(handle);
		SoftTimerId * timer = SoftTimerId::getInstance();
		long lnEvent = 0;
		long eventMask = ptr->get_event_mask();
		int eventCount = 0;

		retEvents.clear();
		if (!eventMask) return -1;

		// first check whether there are some events pending
		eventCount = ptr->check_pending_events(retEvents);
		if (eventCount > 0)
		{
			dlog_alert("There are %d pending events!", eventCount);
			// read_evt(eventMask);
			return eventCount;
		}

		// timeout 0 means poll only (no timeout)
		if (timeout == 0)
		{
			lnEvent = peek_event();
			lnEvent &= eventMask; // only our events are interesting!
			// dlog_msg("Peek event result %d", lnEvent);

			if(lnEvent == 0)
				return 0;
			read_evt(lnEvent); // clear pending events
			return ptr->read_our_events(lnEvent, retEvents);
		}

		// Try reading events, maybe there are some already pending
		dlog_msg("Event mask 0x%08lx", eventMask);
		lnEvent = read_evt(eventMask) & eventMask;
		if (lnEvent)
		{
			dlog_alert("read_evt=0x%08x!", lnEvent);
			eventCount = ptr->read_our_events(lnEvent, retEvents);
			if (eventCount > 0) return eventCount;
		}

		// Nothing yet, so we have to wait for events now
		dlog_msg("Timeout %ld milliseconds", timeout);
		bool timedOut = false;

		
		// Get the list of all timers in collection
		timer_id_list_t timerIdList;
		ptr->get_all_timers(timerIdList);

		
		if(timeout != INFINITE_TIMEOUT)
		{
			idleTimer = timer->setTimer(timeout);
			timerIdList.push_back(idleTimer);
			
			dlog_msg("Timer id %i", idleTimer);
			eventMask |= timer->getSysEventMask();
		}
		
		do
		{
			timer->setSysTimer(timerIdList);
			
			dlog_msg("Waiting for 0x%08x", eventMask);
			lnEvent = wait_evt(eventMask);
			if (lnEvent & timer->getSysEventMask())
			{
				if (idleTimer >= 0)
				{
					timedOut = timer->isTimerFired(idleTimer);
					if (timedOut)
					{
						timer->clrTimer(idleTimer);
						idleTimer = SoftTimerId::INVALID_ID;
					}
				}
			}
			dlog_msg("Event 0x%08x, timedout %i", lnEvent, timedOut);

			if (lnEvent != 0) // else - wait_evt returned nothing, wait again
			{
				eventCount += ptr->read_our_events(lnEvent, retEvents);
				dlog_msg("We have %i events", eventCount);
			}

#if 0
			if (timer->haveExpiredTimers())
			{
				dlog_alert("There are pending unhandled timers!");
				// Check if we should report timer events
				timer_id_list_t tmr = timer->getExpiredTimers();
				for (timer_id_list_t::const_iterator it = tmr.begin(); it != tmr.end(); ++it)
				{
					expiredTimers_t::iterator expiredTimer = expiredTimers.find(*it);
					if (expiredTimer != expiredTimers.end())
					{
						++(expiredTimer->second);
						dlog_msg("Timer ID %d expired for %d time", *it, expiredTimer->second);
					}
					else
					{
						expiredTimers.insert(std::make_pair(*it, 1));
						dlog_msg("Timer ID %d expired for %d time", *it, 1);
					}
					timer->clearTimerMask(*it);
					if (!timer->isRunning(*it)) timer->clrTimer(*it);
				}
				dlog_msg("Expired timers list has %d elements", expiredTimers.size());
			}
#endif
			
			if (timedOut)
			{
				// dlog_alert("Timed out!");
				break;
			}
		} while(eventCount == 0);
		
		if (idleTimer >= 0)
			timer->clrTimer(idleTimer);
		timer->clrSysTimer();
		
		return eventCount;
	}

	int PMLEventHandler::close(const int handle)
	{
		mapEvents::iterator itExec = myMap.find(handle);
		if (itExec != myMap.end())
		{
			PMLCollection const * ptr = itExec->second;
			for (mapEvents::iterator it = myMap.begin(); it != myMap.end(); ++it)
			{
				it->second->close(ptr);
			}
			myMap.erase(itExec);
			delete ptr;
			return 1;
		}
		return 0;
	}

	int PMLEventHandler::init(const char * const name)
	{
		if (name && strlen(name))
		{
			if (!com_verifone_VxSharedMemory::add_name(name)) return -1;
			com_verifone_VxSharedMemory::register_user_event( get_task_id() );
			ourName = name;
			return 0;
		}
		return -1;
	}

	int PMLEventHandler::deinit(const char * const name)
	{
		if (!com_verifone_VxSharedMemory::delete_name(name)) return -1;
		return 0;
	}

	int PMLEventHandler::raise(const char * name, const com_verifone_pml::event_item & event)
	{
		if (name)
		{
			int task_id = com_verifone_VxSharedMemory::get_task_id(name);
			if (task_id == -1)
			{
				dlog_error("Task %s cannot be found!", name);
				return -1;
			}
			com_verifone_VxSharedMemory::pass_value( task_id, event.evt.user.flags, event.evt.user.ext_flags );
			unsigned long user_event = get_event_flag(event.event_id);
			int result = post_user_event(task_id, user_event);
			dlog_msg("Post event result %d", result);
			return result;
		}
		// to everyone
		com_verifone_VxSharedMemory::id_set_t all_ids;
		if (com_verifone_VxSharedMemory::get_all_tasks_ids(all_ids) > 0)
		{
			dlog_msg("We have %d IDs", all_ids.size());
			int result = 0;
			unsigned long user_event = get_event_flag(event.event_id);
			while (!all_ids.empty())
			{
				int id = all_ids.back();
				all_ids.pop_back();
				com_verifone_VxSharedMemory::pass_value( id, event.evt.user.flags, event.evt.user.ext_flags );
				int localResult = post_user_event(id, user_event);
				if (localResult != 0)
				{
					dlog_alert("Error posting event to task id %d, error %d, errno %d!", id, localResult, errno);
					result = localResult;
				}
			}
			return result;
		}
		return -1;
	}
	int PMLEventHandler::raise(int tid, const com_verifone_pml::event_item & event)
	{
		unsigned long user_event = get_event_flag(event.event_id);
		com_verifone_VxSharedMemory::pass_value( tid, event.evt.user.flags, event.evt.user.ext_flags );
		int result = post_user_event(tid, user_event);
		dlog_msg("Post event result %d", result);
		return result;
	}

	/*
	 * VERSIONING
	 */
	int PMLEventHandler::set_version(const char version [])
	{
		if (version && strlen(version) && ourName.size())
		{
			if (!com_verifone_VxSharedMemory::set_version(ourName.c_str(), version)) return -1;
			return 0;
		}
		return -1;

	}

	int PMLEventHandler::register_library( const char libname[], const char version[] )
	{
		if (libname && strlen(libname) && version && strlen(version))
		{
			if (!com_verifone_VxSharedMemory::register_library(ourName.c_str(), libname, version)) return -1;
			return 0;
		}
		return -1;
	}

	int PMLEventHandler::get_apps_list( com_verifone_pml::appver::applist_t &apps )
	{
		return com_verifone_VxSharedMemory::get_apps_list(apps);
	}

	int PMLEventHandler::get_lib_list( com_verifone_pml::appver::applist_t& liblist, const char *appname /* = NULL */ )
	{
		return com_verifone_VxSharedMemory::get_lib_list(liblist, appname);
	}
	
	std::string PMLEventHandler::get_app_version(const char appName [])
	{
		std::string appver;
		com_verifone_VxSharedMemory::get_app_version(appName, appver);
		return appver;
	}

} // namespace
