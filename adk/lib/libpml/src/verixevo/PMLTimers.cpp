/*****************************************************************************
 * 
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file        PMLTimers.cpp
 *
 * @author      Kamil Pawlowski
 * 
 * @brief       Internal handler for Timers
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C Coding  
 *              Standard 1.0.0 
 */

#include <map>

#include <libpml/pml.h>

#include "SoftTimer.h"


using namespace com_verifone_SoftTimer;

namespace com_verifone_pml
{
	int set_timer(event_item & event, long milliseconds, bool repeat /* = false */)
	{
		//if (milliseconds > 0)
		{
			SoftTimerId * ptr = SoftTimerId::getInstance();
			long timerID = ptr->setTimer(milliseconds, repeat);
			if (timerID != -1)
			{
				event.event_id = events::timer;
				event.evt.tmr.id = timerID;
				return static_cast<int>(timerID);
			}
		}
		return -1;
	}

	int check_timer(const event_item & timer_id)
	{
		SoftTimerId * ptr = SoftTimerId::getInstance();
		//long timerID = getID(timer_id.evt.tmr.id);
		long timerID = timer_id.evt.tmr.id;
		if (ptr->isValid(timerID))
		{
			int result = ptr->isTimerFired(timerID);
			if (result)
			{
				// timer is expired
				ptr->updateTimer(timerID);
			}

			// we return true for pending and false for expired timers
			return result;
		}
		return -1;
	}
	
	/*int stop_timer(const event_item & timer_id)
	{
		SoftTimer * ptr = SoftTimer::getInstance();
		long timerID = getID(timer_id.evt.tmr.id);
		if (ptr->isValidMask(timerID))
		{
			ptr->stopTimer(timerID);
			return 0;
		}
		return -1;
	}*/
	
	int clr_timer(event_item & timer_id)
	{
		SoftTimerId * ptr = SoftTimerId::getInstance();
		long timerID = timer_id.evt.tmr.id;

		if (ptr->isValid(timerID))
		{
			ptr->clrTimer(timerID);
			timer_id.evt.tmr.id = 0;
			return 0; //always
		}
		return -1;
	}
	
	/* Change timer settings
	 * @param[in] timeout Timeout in milliseconds
	 * @param[in] repeat Boolean flag, configuring repeatable timer
	 * @return timer ID
	 */
	int change_timer( event_item &event, long milliseconds, bool repeat )
	{
		SoftTimerId * ptr = SoftTimerId::getInstance();
		long timerID = event.evt.tmr.id;
		return ptr->changeTimer(timerID, milliseconds, repeat);
	}
}


