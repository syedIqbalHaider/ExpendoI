#include "PMLInternals.hpp"

#include <ceif.h>
#include <vos_ioctl.h>
#include <vos_ddi_ini.h>

#include <liblog/logsys.h>

using namespace com_verifone_pml;

namespace com_verifone_pml_events_internals
{

bool PMLEOSHandler::monitor_netlink = false;
bool PMLEOSHandler::monitor_signal = false;
bool PMLEOSHandler::m_unregister = false;
bool PMLEOSHandler::m_disable_event_notifications = false;
bool PMLEOSHandler::m_disable_signal_notifications = false;
bool PMLEOSHandler::m_initialized = false;
bool PMLEOSHandler::m_has_network = false;
int PMLEOSHandler::m_timer_id = -1;
PMLEOSHandler::IFACE_NAMES PMLEOSHandler::interface_names;
PMLEOSHandler::item_queue PMLEOSHandler::netlink_queue;
PMLEOSHandler::item_queue PMLEOSHandler::signal_queue;

PMLEOSHandler::PMLEOSHandler(const com_verifone_pml::event_item & evt):
	PMLInternalHandler(evt)
{
	dlog_msg("Creating EOS Handler");
	m_event = EVT_PIPE;
	if (!m_initialized)
	{
		m_has_network = check_network();
		m_initialized = true;
	}
	if (interface_names.empty()) interface_names = setup_network_interfaces();
}

PMLEOSHandler::~PMLEOSHandler()
{
	// Ignore errors here
	if (m_initialized && !monitor_netlink && !monitor_signal)
	{
		if (m_disable_signal_notifications) ceSetSignalNotification(CE_SF_OFF);
		if (m_disable_event_notifications) ceDisableEventNotification();
		if (m_unregister) ceUnregister();
		m_initialized = false;
	}
}


PMLEOSHandler::IFACE_NAMES PMLEOSHandler::setup_network_interfaces()
{
	IFACE_NAMES inames;
	/*int NICount = ceGetNWIFCount();
	if (NICount == ECE_NOTREG)
	{
		int ret = ceRegister();
		if (ret != ECE_SUCCESS)
		{
			dlog_error("Thread %d, cannot register!", get_task_id());
			return inames;
		}
	} */
	// Get the total number of network interface from this terminal (NWIF)
	int NICount = ceGetNWIFCount();
	if (NICount <= 0)
	{
		dlog_error("There are no devices!");
		return inames;
	}
	stNIInfo * NIInfo = new stNIInfo[NICount];
	assert(NIInfo);
	unsigned int count = 0;
	if (ceGetNWIFInfo ( (stNIInfo*) NIInfo, NICount, &count) == ECE_SUCCESS)
	{
		dlog_msg("We have %d interfaces", NICount);
		for (int i = 0; i < NICount; ++i)
		{
			std::string name;
			if (strlen(NIInfo[i].niDeviceName))
				name.assign(NIInfo[i].niDeviceName);

			inames.insert(std::make_pair(NIInfo[i].niHandle, name));
		}
	}
	delete [] NIInfo; NIInfo = 0;
	return inames;
}

bool PMLEOSHandler::check_network()
{
	m_unregister = false;
	int NICount = ceGetNWIFCount();
	if (NICount == ECE_NOTREG)
	{
		m_unregister = true;
		int ret = ceRegister();
		if (ret != ECE_SUCCESS)
		{
			dlog_error("Network register failure %d!", ret);
			return false;
		}
	}
	NICount = ceGetNWIFCount();
	if (NICount > 0)
	{
		int ret = ceEnableEventNotification();
		dlog_msg("Enable network events result %d", ret);
		if (ret == ECE_SUCCESS || ret == ECE_EVENTS_AGAIN)
		{
			m_disable_event_notifications = (ret == ECE_SUCCESS);
			ret = ceSetSignalNotification(CE_SF_ON);
			dlog_msg("Enable signal notifications result %d", ret);
			m_disable_signal_notifications = (ret == ECE_SUCCESS);
			return true;
		}
	}
	else
	{
		dlog_error("No network interfaces!");
	}
	return false;
}

int PMLEOSHandler::check_eos_event() const
{
	if (ceGetEventCount())
	{
		stceNWEvt NotificationEvt;
		char NotificationMsg[CEIF_EVT_DATA_SZ];
		int NotificationMsgLen;
		size_t index = 0;
		com_verifone_pml::event_item m_event_netlink_item;
		m_event_netlink_item.event_id = events::netlink;
		m_event_netlink_item.evt.netlink._fd = -1;
		m_event_netlink_item.evt.netlink.n_ifcs = 0;
		com_verifone_pml::event_item m_event_signal_item;
		m_event_signal_item.event_id = events::signal_strength;
		m_event_signal_item.evt.signal.value = -1;
		m_event_signal_item.evt.signal.cable_connected = 0;
		dlog_msg("EOS Handler: processing %d events", ceGetEventCount());
		while (ceGetEventCount() > 0 && index < sizeof(m_event_netlink_item.evt.netlink.ifc)/sizeof(m_event_netlink_item.evt.netlink.ifc[0]))
		{
			int retVal = ceGetEvent (&NotificationEvt, sizeof(NotificationMsg), NotificationMsg, &NotificationMsgLen);
			if(retVal == ECE_SUCCESS)
			{
				dlog_msg("Network CE Event:");
				dlog_msg("	niHandle:%u", NotificationEvt.niHandle);
				dlog_msg("	neEvt:%u",	  NotificationEvt.neEvt);
				dlog_msg("	neParam1:%d", NotificationEvt.neParam1);
				dlog_msg("	neParam2:%d", NotificationEvt.neParam2);
				dlog_msg("	neParam3:%d", NotificationEvt.neParam3);
				if (NotificationMsgLen) dlog_hex(NotificationMsg, NotificationMsgLen, "[NotificationMsg]");
				dlog_msg("");
				bool validNetlinkItem = false;
				switch (NotificationEvt.neEvt)
				{
					case CE_EVT_STOP_CLOSE:
					case CE_EVT_STOP_NW:
						// Network is down - fall forward
					case CE_EVT_NET_OUT:
						// Cable is out
						m_event_netlink_item.evt.netlink.ifc[index].isup = false;
						m_event_signal_item.evt.signal.value = -1;
						validNetlinkItem = true;
						break;
					case CE_EVT_NET_UP:
						// Network is up - fall forward
					case CE_EVT_NET_RES:
						// Cable is back in
						m_event_netlink_item.evt.netlink.ifc[index].isup = true;
						m_event_signal_item.evt.signal.value = 100;
						validNetlinkItem = true;
						break;
					// WiFi events
					case CE_EVT_SIGNAL:
						if(NotificationEvt.neParam1 < 0)
						{
							m_event_signal_item.evt.signal.value = 0;
						}
						else if(NotificationEvt.neParam1 > 100)
						{
							m_event_signal_item.evt.signal.value = 100;
						}
						else
						{
							m_event_signal_item.evt.signal.value = NotificationEvt.neParam1;
						}
						break;
					case CE_EVT_NET_POOR:
						// Assume no network
						m_event_signal_item.evt.signal.value = 1;
						break;
					default:
						break;
				}
				if (validNetlinkItem)
				{
					// Set name
					m_event_netlink_item.evt.netlink.ifc[index].name[0] = 0;
					for (IFACE_NAMES::const_iterator it = interface_names.begin(); it != interface_names.end(); ++it)
					{
						if (it->first == NotificationEvt.niHandle)
						{
							strncpy(m_event_netlink_item.evt.netlink.ifc[index].name, it->second.c_str(), sizeof m_event_netlink_item.evt.netlink.ifc[index].name);
							break;
						}
					}
					++index;
					m_event_netlink_item.evt.netlink.n_ifcs = index;
				}
			}
		}
		if (m_event_signal_item.evt.signal.value != -1 && monitor_signal)
		{
			dlog_alert("Signal event (%d)!", m_event_signal_item.evt.signal.value);
			signal_queue.push(m_event_signal_item);
		}
		if (m_event_netlink_item.evt.netlink.n_ifcs > 0 && monitor_netlink)
		{
			dlog_alert("Netlink event (%d)!", m_event_netlink_item.evt.netlink.n_ifcs);
			netlink_queue.push(m_event_netlink_item);
		}
		return !signal_queue.empty() || !netlink_queue.empty();
	}
	return 0;
}

int PMLEOSHandler::initial_status(bool send_netlink, bool send_signal)
{
	// Get device handle
	bool gen_event = false;
	if (send_netlink)
	{
		for (IFACE_NAMES::const_iterator it = interface_names.begin(); it != interface_names.end(); ++it)
		{
			stNI_NWIFState ifaceState;
			unsigned int pLen = 0;
			int rc = ceGetNWParamValue (it->first, NWIF_STATE, &ifaceState, sizeof(ifaceState), &pLen);
			if (rc == ECE_SUCCESS)
			{
				com_verifone_pml::event_item m_event_netlink_item;
				m_event_netlink_item.event_id = events::netlink;
				m_event_netlink_item.evt.netlink._fd = -1;
				m_event_netlink_item.evt.netlink.n_ifcs = 1;
				m_event_netlink_item.evt.netlink.ifc[0].isup = ifaceState.nsCurrentState > NWIF_CONN_STATE_LINK;
				strncpy(m_event_netlink_item.evt.netlink.ifc[0].name, it->second.c_str(), sizeof m_event_netlink_item.evt.netlink.ifc[0].name);
				#if 1
				stNI_IPConfig ipConfig;
				rc = ceGetNWParamValue (it->first, IP_CONFIG, &ipConfig, sizeof (stNI_IPConfig), &pLen);
				if (rc == ECE_SUCCESS)
				{
					m_event_netlink_item.evt.netlink.ifc[0].isup = ipConfig.ncIsConnected;
				}
				#endif
				netlink_queue.push(m_event_netlink_item);
				dlog_msg("Generated initial netlink event, state %d, up %d", ifaceState.nsCurrentState, m_event_netlink_item.evt.netlink.ifc[0].isup);
			}
			else
			{
				dlog_error("Cannot push network interface %s state, error %d", it->second.c_str(), rc);
			}
		}
		gen_event = !netlink_queue.empty();
	}
	if (send_signal)
	{
		for (IFACE_NAMES::const_iterator it = interface_names.begin(); it != interface_names.end(); ++it)
		{
			unsigned int len;
			char buf[CEIF_PARAM_VALUE_SZ];
			int rc;

			if ((rc = ceGetDDParamValue(it->first, IOCTL_GET_RSSI, sizeof(buf), buf, &len)) == ECE_SUCCESS)
			{
				*(buf + len) = 0;
				com_verifone_pml::event_item m_event_signal_item;
				m_event_signal_item.event_id = events::signal_strength;
				if (sscanf(buf, "%d", &m_event_signal_item.evt.signal.value) == 1)
				{
					signal_queue.push(m_event_signal_item);
					dlog_msg("Generated initial signal event");
				}
				else
				{
					dlog_error("Invalid value returned (%s)", buf);
					dlog_hex(buf, len, "DUMP");
				}
			}
			else if (rc == ECE_PARAM_NOT_FOUND)
			{
				//Bluetooth
				ddi_bt_base_info *btBaseInfo = reinterpret_cast<ddi_bt_base_info*>(buf);

				dlog_msg("Firmware version - %s", btBaseInfo->fw_ver);
				dlog_msg("PAN connection type - %d", btBaseInfo->pan_type);
				dlog_msg("Ethernet cable state - %d", btBaseInfo->eth_cable_stat);
				dlog_msg("Router IPv4 address - %s", btBaseInfo->ip_addr);
				dlog_msg("Serial number - %s", btBaseInfo->serial_no);

				com_verifone_pml::event_item m_event_signal_item;
				m_event_signal_item.event_id = events::signal_strength;
				m_event_signal_item.evt.signal.value = 100;
				signal_queue.push(m_event_signal_item);

				dlog_msg("Generated initial signal event");

				/*if ((rc = ceGetDDParamValue(it->first, IOCTL_GET_BTBASE_INFO, sizeof(buf), buf, &len)) == ECE_SUCCESS)
				{
				}
				else
				{
					dlog_error("Cannot get BT base info for interface %s, error %d", it->second.c_str(), rc);
				}*/
			}
			else
			{
				dlog_error("Cannot get signal info for interface %s, error %d", it->second.c_str(), rc);
			}
		}
		gen_event = !signal_queue.empty();
	}
	if (gen_event)
	{
		//m_timer_id = set_timer(10, EVT_PIPE);
		return 1;
	}
	return 0;
}

}
