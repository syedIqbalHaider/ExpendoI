/*
 * =====================================================================================
 *
 *       Filename:  net_api.cpp
 *
 *    Description:  NET abstracted API for verixevo platform
 *
 *        Version:  1.0
 *        Created:  02/17/2014 12:30:08 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lucjan Bryndza (lb), Lucjan_B1@verifone.com
 *   Organization:  VERIFONE
 *
 * =====================================================================================
 */
#include <svc_net.h>

#include <liblog/logsys.h>
#include <libpml/pml.h>
#include <libpml/pml_abstracted_api.h>

#include <strings.h>

#include <ceifConst.h>
#include <ceif.h>
#include <vos_ioctl.h>
#include <vos_ddi_ini.h>
#include <vos_ddi_app_evt.h>

/* ------------------------------------------------------------------ */ 
namespace com_verifone_pml {
namespace net {
/* ------------------------------------------------------------------ */ 
static const int INVALID_INTERFACE_HANDLE = -1;
namespace internal_fun {
	static int NICount = 0;
	static stNIInfo * NIInfo = 0;

	int getInterfaceHandle(const std::string & name, interface_info * info = NULL)
	{
		if (NICount == 0 || NIInfo == NULL)
		{
			// Get the total number of network interface from this terminal (NWIF)
			NICount = ceGetNWIFCount();
			if (NICount == ECE_NOTREG)
			{
				int ret = ceRegister();
				if (ret != ECE_SUCCESS)
				{
					dlog_error("Cannot register!");
					return INVALID_INTERFACE_HANDLE;
				}
				dlog_msg("Registered successfully");
				NICount = ceGetNWIFCount();
			}
			dlog_msg("We have %d interfaces", NICount);
			if (NICount <= 0)
			{
				dlog_error("There are no devices!");
				return INVALID_INTERFACE_HANDLE;
			}
			delete [] NIInfo;
			NIInfo = new stNIInfo[NICount];
			assert(NIInfo);
			unsigned int count = 0;
			//if (ceGetNWIFInfo ( (stNIInfo*) NIInfo, NICount, &count) != ECE_SUCCESS)
			if (ceGetNWIFInfo ( (stNIInfo*) NIInfo, NICount, &count) != ECE_SUCCESS)
			{
				delete [] NIInfo; NIInfo = 0;
				NICount = 0;
			}
			for (int i = 0; i < NICount; ++i)
			{
				dlog_msg("%d: '%s'", i, NIInfo[i].niDeviceName);
				dlog_msg("Dev: '%s'", NIInfo[i].niCommTech);
				dlog_msg("State: %d", NIInfo[i].niRunState);
				dlog_msg("Handle : %d", NIInfo[i].niHandle);
			}
		}
		if (NICount > 0 && NIInfo != NULL)
		{
			for (int i = 0; i < NICount; ++i)
			{

				if (name.empty() || !strcasecmp(NIInfo[i].niDeviceName, name.c_str()))
				{
					if (info)
					{
						stNI_NWIFState ifaceState;
						unsigned int pLen = 0;
						int rc = ceGetNWParamValue (NIInfo[i].niHandle, NWIF_STATE, &ifaceState, sizeof(ifaceState), &pLen);
						if (rc == ECE_SUCCESS)
						{
							dlog_msg("Interface handle %i", NIInfo[i].niHandle);
							dlog_msg("Interface tech %s", NIInfo[i].niCommTech);
							dlog_msg("Interface name %s", NIInfo[i].niDeviceName);
							dlog_msg("Interface state: %d", NIInfo[i].niRunState);
							dlog_msg("Link state: %d", ifaceState.nsCurrentState);
							info->name.assign(NIInfo[i].niDeviceName);
							info->up = ifaceState.nsCurrentState > NWIF_CONN_STATE_LINK;
							// TODO: Fixme!
							info->metric = 0;
							info->mtu = 0;
							info->point_to_point = false;
							info->multicast = false;
							info->loopback = false;
						}
						#if 1
						// Workaround for WEINTRET-526: EOS returns Network up, but the IP is not yet acquired...
						stNI_IPConfig ipConfig;
						int cnt = 3; // 2 attempts
						while (--cnt > 0)
						{
							rc = ceGetNWParamValue (NIInfo[i].niHandle, IP_CONFIG, &ipConfig, sizeof (stNI_IPConfig), &pLen);
							if (rc == ECE_SUCCESS)
							{
								dlog_msg("Iter %i: Res %i", cnt, ipConfig.ncIsConnected);
								if (ipConfig.ncIsConnected) break;
								else SVC_WAIT(0);
							}
							else
							{
								break;
							}
						}
						if (rc == ECE_SUCCESS)
						{
							info->up = ipConfig.ncIsConnected;
						}
						#endif
						#if 0
						stNI_IPConfig ipConfig;
						char ip[3*4+3+1];
						rc = ceGetNWParamValue (NIInfo[i].niHandle, IP_CONFIG, &ipConfig, sizeof (stNI_IPConfig), &pLen);
						if (rc == ECE_SUCCESS)
						{
							memset(ip, 0, sizeof(ip));
							if (inet_ntoa(ipConfig.ncIPAddr, ip))
							{
								info->addr.assign(ip);
							}
							dlog_msg("Interface name %s", NIInfo[i].niDeviceName);
							dlog_msg("IP %s", info->addr.c_str());
							info->name.assign(NIInfo[i].niDeviceName);
							info->up = (NIInfo[i].niRunState == CE_RUN_STATE_RUNNING) && (ipConfig.ncIsConnected == 1);
							dlog_msg("Link state: %d", NIInfo[i].niRunState);
							// TODO: Fixme!
							info->metric = 0;
							info->mtu = 0;
							info->point_to_point = false;
							info->multicast = false;
							info->loopback = false;
							if (!strcmp(NIInfo[i].niCommTech, CE_COMM_TECH_WIFI))
							{
								ddi_bss_info dbi;
								memset(&dbi, 0, sizeof (ddi_bss_info));
								rc = ceGetNWParamValue(NIInfo[i].niHandle, IOCTL_GET_BSSINFO, &dbi, sizeof(dbi), &pLen);
								if (rc == ECE_SUCCESS)
								{
									dlog_msg("IOCTL_GET_BSSINFO [%d] [%s] [%s]", rc, dbi.SSID, dbi.NWAuth);
									stSignalLevel sl;
									rc = ceGetNWParamValue(NIInfo[i].niHandle, SIGNAL_LEVEL, &sl, sizeof(sl), &pLen);
									if (rc == ECE_SUCCESS)
									{
										info->up = sl.iSignal1 > 0;
									}
									else
									{
										info->up = false;
									}
								}
								else
								{
									info->up = false;
								}
							}
							else if (!strcmp(NIInfo[i].niCommTech, CE_COMM_TECH_ETHBT))
							{
							}
						}
						#endif
						else // (rc != ECE_SUCCESS)
						{
							dlog_error("Cannot get interface information: %d!", rc);
							info->addr.clear();
							info->name.clear();
							info->up = false;
							info->metric = 0;
							info->mtu = 0;
							info->point_to_point = false;
							info->multicast = false;
							info->loopback = false;
						}
					}
					return NIInfo[i].niHandle;
				}
			}
		}
		return INVALID_INTERFACE_HANDLE;
	}

	inline void resetNWIFState()
	{
		NICount = 0;
	}

	typedef std::vector<unsigned int> EVT_VEC;
	struct stceNWEvtEx {
		stceNWEvt evt;
		size_t extDataSize;
		char extData[CEIF_EVT_DATA_SZ];
		stceNWEvtEx(): extDataSize(0) { memset(&evt, 0, sizeof evt); }
	};
	typedef std::vector<stceNWEvtEx> EVT_VEC_OUT;
	int waitForNetworkEvent(const EVT_VEC &events, EVT_VEC_OUT & outEvents, unsigned long timeout = 0)
	{
		// int networkEventsNo = ceGetEventCount();
		bool waiting = true;
		long interestingEvents = EVT_PIPE;
		int tmrHandle = -1;
		for (EVT_VEC::const_iterator it = events.begin(); it != events.end(); ++it)
		{
			dlog_msg("Waiting for network event: %u", *it);
		}
		if (timeout)
		{
			dlog_msg("With timeout %u", timeout);
			interestingEvents |= EVT_TIMER;
			tmrHandle = ::set_timer(timeout, EVT_TIMER);
		}
		outEvents.clear();
		while (waiting)
		{
			long firedEvents = wait_evt(interestingEvents);
			if (firedEvents & EVT_PIPE)
			{
				int networkEventsNo = ceGetEventCount();
				dlog_msg("We have %d network events", networkEventsNo);
				if (networkEventsNo > 0)
				{
					stceNWEvt NotificationEvt;
					char NotificationMsg[CEIF_EVT_DATA_SZ];
					int NotificationMsgLen;
					int retVal = 0;
					while(ceGetEventCount() > 0)
					{
						retVal = ceGetEvent(&NotificationEvt, sizeof(NotificationMsg), NotificationMsg, &NotificationMsgLen);
						if(retVal == 0)
						{
							dlog_msg("Network event:%u", NotificationEvt.neEvt);
							for (EVT_VEC::const_iterator it = events.begin(); it != events.end(); ++it)
							{
								if (NotificationEvt.neEvt == *it)
								{
									stceNWEvtEx evt;
									evt.evt = NotificationEvt;
									evt.extDataSize = NotificationMsgLen;
									if (NotificationMsgLen) std::memcpy(evt.extData, NotificationMsg, NotificationMsgLen);
									outEvents.push_back(evt);
									break;
								}
							}
							if (outEvents.size()) waiting = false;
							// Always carry on and read all the pending events
						}
					}
				}
				else
				{
					dlog_msg("No network event");
				}
			}
			else if (firedEvents & EVT_TIMER)
			{
				dlog_alert("Wait timed out!");
				waiting = false;
			}
		}
		if (tmrHandle >= 0) ::clr_timer(tmrHandle);
		return outEvents.size();
	}
	int waitForNetworkEvent(const EVT_VEC &events, unsigned long timeout = 0)
	{
		internal_fun::EVT_VEC_OUT outEvents;
		return waitForNetworkEvent(events, outEvents, timeout);
	}


	int initNWIF( int handle )
	{
		int rc = ceGetNWIFStartMode(handle);
		dlog_msg("ceGetNWIFStartMode %d", rc);
		if (rc != CE_SM_MANUAL)
		{
			rc = ceSetNWIFStartMode(handle, CE_SM_MANUAL);
			if (rc != ECE_SUCCESS)
			{
				dlog_error("ceSetNWIFStartMode failed, result %d", rc);
				return rc;
			}
		}
		// Now start the interface
		while(true)
		{
			rc = ceStartNWIF(handle, CE_OPEN);
			dlog_msg("ceStartNWIF for handle %d result %d", handle, rc);
			if (rc == ECE_SUCCESS) break;
			else
			{
				if (rc == ECE_NO_CE_EVENT)
				{
					rc = ceEnableEventNotification();
					if (rc == ECE_SUCCESS)
					{
						dlog_msg("Events enabled successfully");
						continue;
					}
				}
				else if (rc == ECE_STATE_ERR)
				{
					// assume already correct state
					return ECE_SUCCESS;
				}
				else if (rc == ECE_START_STOP_BUSY)
				{
					dlog_alert("NWIF is busy!");
					SVC_WAIT(500);
				}
				else
				{
					dlog_error("ceStartNWIF failed, result %d", rc);
					return rc;
				}
			}
		}
		internal_fun::resetNWIFState();
		internal_fun::EVT_VEC events;
		events.push_back(CE_EVT_START_OPEN);
		events.push_back(CE_EVT_START_FAIL);
		int networkEventsNo = internal_fun::waitForNetworkEvent(events);
		if (networkEventsNo <= 0)
		{
			rc = ECE_REGFAILED;
		}
		return rc;
	}

	int getConfiguredAPCount( int handle, int & count )
	{
		unsigned int len = 0;
		char buf[CEIF_PARAM_VALUE_SZ];
		int rc = ceGetDDParamValue(handle, IOCTL_GET_NW_CNT, sizeof(buf), buf, &len);
		dlog_msg("ceGetDDParamValue(NW_CNT) = %d", rc);
		if (rc == ECE_SUCCESS)
		{
			*(buf + len) = 0;
			count = atoi(buf);
		}
		return rc;
	}

	int getConfiguredAPList( int handle, std::vector<ddi_nw_list> & apList )
	{
		int cnt = 0;
		int rc = getConfiguredAPCount(handle, cnt);
		if (rc != ECE_SUCCESS)
		{
			return rc;
		}
		dlog_msg("We have %d network profiles", cnt);
		if (cnt <= 0)
		{
			return 0;
		}
		unsigned int len = 0;
		apList.resize(cnt);
		rc = ceGetDDParamValue(handle, IOCTL_GET_NW_PROFILE_LIST, sizeof(ddi_nw_list) * cnt, apList.begin(), &len);
		if(rc != ECE_SUCCESS)
		{
			dlog_msg("%s() : could not get profile list", __func__);
			apList.clear();
			return rc;
		}
		for(int i = 0; i < cnt; ++i)
		{
			apList[i].SSID[apList[i].ssid_len] = 0;
			dlog_msg("=========== NW profile #%d ==========", i);
			dlog_msg("    network_id: %d", apList[i].network_id);
			dlog_msg("    SSID:       %s", apList[i].SSID);
			dlog_msg("    BSSID:      %02X:%02X:%02X:%02X:%02X:%02X", 
				apList[i].BSSID[0], apList[i].BSSID[1], apList[i].BSSID[2], apList[i].BSSID[3], apList[i].BSSID[4], apList[i].BSSID[5]);
			dlog_msg("    bssid_len:  %d", apList[i].bssid_len);
			dlog_msg("    ssid_len:   %d", apList[i].ssid_len);
			dlog_msg("=====================================");
		}
		return ECE_SUCCESS;
	}

	int getAvailableNetworkList( int handle, std::vector<ddi_bss_info> & networkList)
	{
		char getCEIFValBuf[CEIF_PARAM_VALUE_SZ];
		unsigned int getCEIFValLen = 0;
		int rc = ceGetDDParamValue(handle, IOCTL_GET_SEARCH_CNT, sizeof(getCEIFValBuf), getCEIFValBuf, &getCEIFValLen);
		if (rc != 0)
		{
			dlog_error("Unable to IOCTL_GET_SEARCH_CNT for WiFi devices: %d", rc);
			return rc;
		}
		dlog_hex(getCEIFValBuf, getCEIFValLen, "[IOCTL_GET_SEARCH_CNT]");
		getCEIFValBuf[getCEIFValLen] = 0;
		int cntAP = atoi(getCEIFValBuf);
		networkList.resize(cntAP);
		rc = ceGetDDParamValue(handle, 
								IOCTL_GET_SEARCH_RESULT, 
								sizeof(ddi_bss_info) * cntAP, 
								networkList.begin(),
								&getCEIFValLen);
		if (rc == ECE_SUCCESS)
		{
			dlog_msg("We have %d (%d) networks", cntAP, getCEIFValLen / sizeof(ddi_bss_info));
			for (size_t i = 0; i < networkList.size(); ++i)
			{
				networkList[i].SSID[networkList[i].SSID_len] = 0;
				dlog_msg("------ AP #%d ------", i);
				dlog_msg("    BSSID: %02X:%02X:%02X:%02X:%02X:%02X", networkList[i].BSSID[0], networkList[i].BSSID[1], networkList[i].BSSID[2], networkList[i].BSSID[3], networkList[i].BSSID[4], networkList[i].BSSID[5]);
				dlog_msg("    SSID: %s", networkList[i].SSID);
				dlog_msg("    freq: %d", networkList[i].freq);
				dlog_msg("    RSSI: %d", networkList[i].RSSI);
				dlog_msg("    NWAuth: %s", networkList[i].NWAuth);
				dlog_msg("    DataEncrypt: %s", networkList[i].DataEncrypt);
				dlog_msg("    bss_type: %d", networkList[i].bss_type);
				dlog_msg("    SSID_len: %d", networkList[i].SSID_len);
				dlog_msg("--------------------");
				dlog_msg("");
			}
		}
		return rc;
	}

	int setDefaultProfile( int handle, int idx )
	{
		char buf[10];
		sprintf(buf, "%d", idx);
		int rc = ceSetDDParamValue(handle, IOCTL_SET_DEFAULT_PROFILE, buf, strlen(buf)+1);
		dlog_msg("IOCTL_SET_DEFAULT_PROFILE (%d) result: %d", idx, rc);
		return rc;
	}

	int getDefaultProfile( int handle, int & defaultProfile)
	{
		char getCEIFValBuf[CEIF_PARAM_VALUE_SZ];
		unsigned int getCEIFValLen = 0;
		int rc = ceGetDDParamValue(handle, IOCTL_GET_DEFAULT_PROFILE, sizeof(getCEIFValBuf), getCEIFValBuf, &getCEIFValLen);
		if (rc != 0)
		{
			dlog_error("Unable to IOCTL_GET_DEFAULT_PROFILE for WiFi devices: %d", rc);
			return rc;
		}
		getCEIFValBuf[getCEIFValLen] = 0;
		defaultProfile = atoi(getCEIFValBuf);
		return rc;
	}

	int deleteProfile(int handle, int idx)
	{
		int rc;
		dlog_msg("Deleting profile (%d)", idx);

		// profile to be deleted must be converted to network id as string
		char buf[10];
		sprintf(buf, "%d", idx);
		rc = ceSetDDParamValue(handle, IOCTL_DEL_NW_PROFILE, buf, strlen(buf)+1);
		dlog_msg("IOCTL_DEL_NW_PROFILE result %d", rc);
		return rc;
	}

	std::string conv_hex(const unsigned char * hex, size_t hex_size)
	{
		std::string res;
		char buf[5];
		for (size_t i = 0; i < hex_size; ++i)
		{
			sprintf(buf, "%02X:", *(hex + i));
			res.append(buf);
		}
		return res;
	}

	int setupNewProfile( int handle, const ddi_bss_info * pAPDescriptor, const wifi::medium_config & apPass )
	{
		char profNameBuf[32];
		unsigned int getCEIFValLen = 0;
		int profileIndex = 0;
		int rc;

		rc = ceGetDDParamValue(handle, IOCTL_ADD_NW_PROFILE,  sizeof(profNameBuf),  profNameBuf,  &getCEIFValLen);

		dlog_msg("IOCTL_ADD_NW_PROFILE result: %d", rc);

		if(rc == 0)
		{
			dlog_hex(profNameBuf, getCEIFValLen, "[profNameBuf]");
			//remove delimiter '0A'
			profNameBuf[1] = 0;
			profileIndex = atoi(profNameBuf);
		}
		else
		{
			dlog_error("Cannot add nw profile: error: %d", rc);
			return rc;
		}

		// Set profile data
		dlog_msg("Setting network profile: #%d", profileIndex);
		ddi_nw_profile dnp;

		memcpy(dnp.BSSID,		pAPDescriptor->BSSID, 6);
		strcpy(dnp.ssid,		pAPDescriptor->SSID);

		dnp.network_id			= profileIndex;
		dnp.ssid_len			= pAPDescriptor->SSID_len;
		dnp.bssid_len			= 6;
		
		dnp.bss_type			= pAPDescriptor->bss_type;		//Infrastructure
		dnp.channel				= 0;							//0 default (will be set up automatically)

		//Detect authentication and data encryption mode
		dlog_msg("WiFi network encryption: %s", pAPDescriptor->NWAuth);
		const std::vector<std::string> passwords(apPass.get_passwords());
		if (!strcmp(pAPDescriptor->NWAuth, "WPA-PSK"))
		{
			dnp.auth_type = AUTH_WPA_PSK;
			dnp.uKey.wpa_auth.eap_type = NO_EAP;
			if (passwords.size() >= 1)
			{
				strcpy(dnp.uKey.wpa_psk.key, passwords.front().c_str());
				dnp.uKey.wpa_psk.key_len = passwords.front().size();
			}
			else
			{
				dnp.uKey.wpa_psk.key_len = 0;
			}

			dlog_msg("WPA-PSK Key: '%s'", dnp.uKey.wpa_psk.key);
		}
		else if (!strcmp(pAPDescriptor->NWAuth, "WPA2-PSK"))
		{
			dnp.auth_type = AUTH_WPA2_PSK;
			dnp.uKey.wpa_auth.eap_type = NO_EAP;
			if (passwords.size() >= 1)
			{
				strcpy(dnp.uKey.wpa_psk.key, passwords.front().c_str());
				dnp.uKey.wpa_psk.key_len = passwords.front().size();
			}
			else
			{
				dnp.uKey.wpa_psk.key_len = 0;
			}
			dlog_msg("WPA2-PSK Key: '%s'", dnp.uKey.wpa_psk.key);
		}
		else if (!strcmp(pAPDescriptor->NWAuth, "WPA-EAP"))
		{
			dnp.auth_type = AUTH_WPA_EAP;
			//TODO: fill dnp.uKey.wpa_auth for WPA-EAP
			dnp.uKey.wpa_auth.eap_type = NO_EAP;
		}
		else if (!strcmp(pAPDescriptor->NWAuth, "WPA2-EAP"))
		{
			dnp.auth_type		= AUTH_WPA2_EAP;
			//TODO: fill dnp.uKey.wpa_auth for WPA2-EAP
			dnp.uKey.wpa_auth.eap_type = NO_EAP;
		}

		dlog_msg("WiFi network encryption: %s", pAPDescriptor->DataEncrypt);
		if (strstr(pAPDescriptor->DataEncrypt, "CCMP"))
		{
			dlog_msg("WiFi network encrypt: CCMP");
			dnp.wsec = ALGO_AES_CCM; //CCMP
		}
		else if (!strcmp(pAPDescriptor->DataEncrypt, "TKIP"))
		{
			dlog_msg("WiFi network encrypt: TKIP");
			dnp.wsec = ALGO_TKIP; //TKIP
		}
		else if (!strcmp(pAPDescriptor->DataEncrypt, "WEP"))
		{
			if (apPass.get_auth() == wifi::auth_wep_shared)
			{
				dlog_msg("WiFi network: WEP/WEP128 Shared");
				dnp.auth_type = AUTH_NONE_WEP_SHARED; //WEP Shared
			}
			else
			{
				dlog_msg("WiFi network: WEP/WEP128 Open");
				dnp.auth_type = AUTH_NONE_WEP; //WEP none
			}

			dnp.wsec = ALGO_WEP128;   //WEP128

			for (size_t i = 0; i < 4; ++i) 
				*(dnp.uKey.wep_auth.key[i]) = '\0';

			for (size_t i = 0; i < passwords.size(); ++i)
			{
				strcpy(dnp.uKey.wep_auth.key[i], passwords[i].c_str());
			}

			dnp.uKey.wep_auth.index = 0;
			dnp.uKey.wep_auth.key_len = strlen(dnp.uKey.wep_auth.key[dnp.uKey.wep_auth.index]);
			dlog_msg("WEP key: '%s'", dnp.uKey.wep_auth.key[dnp.uKey.wep_auth.index]);
		}
		else
		{
			dlog_msg("WiFi network: not protected");
			dnp.auth_type = AUTH_NONE_OPEN;     //open none
			dnp.wsec = ALGO_OFF;                //algo off
		}

		//dnp.bss_type = INFRASTRUCTURE;

		dlog_msg("ddi_nw_profile:");
		dlog_msg("\t:network_id -\t%d", dnp.network_id);
		dlog_msg("\t:BSSID -\t%s", conv_hex(dnp.BSSID, dnp.bssid_len).c_str());
		dlog_msg("\t:network_id -\t%s", dnp.ssid);
		dlog_msg("\t:flags -\t%s", conv_hex(reinterpret_cast<const unsigned char*>(dnp.flags), 63).c_str());
		dlog_msg("\t:ssid_len -\t%d", dnp.ssid_len);
		dlog_msg("\t:bssid_len -\t%d", dnp.bssid_len);
		dlog_msg("\t:auth_type -\t%d", dnp.auth_type);
		dlog_msg("\t:wsec -\t%d", dnp.wsec);
		dlog_msg("\t:bss_type -\t%d", dnp.bss_type);
		dlog_msg("\t:channel -\t%d", dnp.channel);
		dlog_msg("\t:uKey.wpa_psk -\t%s", dnp.uKey.wpa_psk.key );

		dlog_msg("\t:handle -\t%d", handle);

		rc = ceSetDDParamValue(handle, IOCTL_SET_NW_PROFILE, (void*)&dnp, sizeof(ddi_nw_profile));

		dlog_msg("IOCTL_SET_NW_PROFILE result: %d", rc);
		return rc;
	}

	bool is_profile_id_valid( int handle, int profile_id )
	{
		int cnt = 0;
		int rc = getConfiguredAPCount(handle, cnt);
		if (profile_id >= 0 && profile_id < cnt) return true;
		return false;
	}

		/*
		int configureAP( int handle, std::vector<ddi_nw_list> & apList, const WIFI_NETWORK & ap, const WIFI_NETWORK_AUTHENTICATION & apPass )
		{
			// Check if given AP exists on the list
			int rc = ECE_SUCCESS;
			int profileIndex = -1;
			for (int i = 0; i < apList.size(); ++i)
			{
				if (!strcmp(apList[i].SSID, ap.SSID.c_str()))
				{
					profileIndex = i;
					break;
				}
			}
			if (profileIndex < 0)
			{
				dlog_msg("Network '%s' not found, creating new profile", ap.SSID.c_str());
				char profNameBuf[32];
				unsigned int getCEIFValLen = 0;
				rc = ceGetDDParamValue(handle, 
										IOCTL_ADD_NW_PROFILE, 
										sizeof(profNameBuf), 
										profNameBuf, 
										&getCEIFValLen);
				dlog_msg("IOCTL_ADD_NW_PROFILE result: %d", rc);
				if(rc == 0)
				{
					dlog_hex(profNameBuf, getCEIFValLen, "[profNameBuf]");
					//remove delimiter '0A'
					profNameBuf[1] = 0;
					profileIndex = atoi(profNameBuf);
				}
				else
				{
					profileIndex = 0;
				}
			}
			else
			{
				dlog_msg("Matching NW profile '%s' found at #%d", ap.SSID.c_str(), profileIndex);
			}

			// Set profile data
			dlog_msg("Setting network profile: #%d", profileIndex);
			ddi_nw_profile dnp;
			dnp.network_id = profileIndex;
			memcpy(dnp.BSSID, ap.BSSID.c_str(), ap.BSSID.size());
			dnp.bssid_len = ap.BSSID.size();
			strcpy(dnp.ssid, ap.SSID.c_str());
			dnp.ssid_len = ap.SSID.size();
			dnp.bss_type = ap.bss_type;   //Infrastructure
			dnp.channel = 0;              //0 default (will be set up automatically)
			//Detect authentication and data encryption mode
			dlog_msg("WiFi network encryption: %s", ap.NWAuth.c_str());
			if(ap.NWAuth == "WPA-PSK")
			{
				dnp.auth_type = AUTH_WPA_PSK;
				dnp.uKey.wpa_auth.eap_type = NO_EAP;
				strcpy(dnp.uKey.wpa_psk.key, apPass.wpakey.c_str());
				dnp.uKey.wpa_psk.key_len = apPass.wpakey.size();
			}
			else if(ap.NWAuth == "WPA2-PSK")
			{
				dnp.auth_type = AUTH_WPA2_PSK;
				dnp.uKey.wpa_auth.eap_type = NO_EAP;
				strcpy(dnp.uKey.wpa_psk.key, apPass.wpakey.c_str());
				dnp.uKey.wpa_psk.key_len = apPass.wpakey.size();
			}
			else if(ap.NWAuth == "WPA-EAP")
			{
				dnp.auth_type		= AUTH_WPA_EAP;
				//TODO: fill dnp.uKey.wpa_auth for WPA-EAP
				dnp.uKey.wpa_auth.eap_type = NO_EAP;
			}
			else if(ap.NWAuth == "WPA2-EAP")
			{
				dnp.auth_type		= AUTH_WPA2_EAP;
				//TODO: fill dnp.uKey.wpa_auth for WPA2-EAP
				dnp.uKey.wpa_auth.eap_type = NO_EAP;
			}

			if(ap.DataEncrypt.find("CCMP") != std::string::npos)
			{
				dlog_msg("WiFi network encrypt: CCMP");
				dnp.wsec = ALGO_AES_CCM; //CCMP
			}
			else if(ap.DataEncrypt.find("TKIP") != std::string::npos)
			{
				dlog_msg("WiFi network encrypt: TKIP");
				dnp.wsec = ALGO_TKIP; //TKIP
			}
			else if(ap.DataEncrypt.find("WEP") != std::string::npos)
			{
				if(apPass.is_wep_shared)
				{
					dlog_msg("WiFi network: WEP/WEP128 Shared");
					dnp.auth_type = AUTH_NONE_WEP_SHARED; //WEP Shared
				}
				else
				{
					dlog_msg("WiFi network: WEP/WEP128 Open");
					dnp.auth_type = AUTH_NONE_WEP; //WEP none
				}
				dnp.wsec = ALGO_WEP128;   //WEP128

				strcpy(dnp.uKey.wep_auth.key[0], apPass.wepkey[0].c_str());
				strcpy(dnp.uKey.wep_auth.key[1], apPass.wepkey[1].c_str());
				strcpy(dnp.uKey.wep_auth.key[2], apPass.wepkey[2].c_str());
				strcpy(dnp.uKey.wep_auth.key[3], apPass.wepkey[3].c_str());
				dnp.uKey.wep_auth.index = 0;
				dnp.uKey.wep_auth.key_len = strlen(dnp.uKey.wep_auth.key[dnp.uKey.wep_auth.index]);
				dlog_msg("WEP key: '%s'", dnp.uKey.wep_auth.key[dnp.uKey.wep_auth.index]);
			}
			else
			{
				dlog_msg("WiFi network: not protected");
				dnp.auth_type = AUTH_NONE_OPEN;     //open none
				dnp.wsec = ALGO_OFF;                //algo off
			}

			//Write the profile data
			rc = ceSetDDParamValue(handle,
									IOCTL_SET_NW_PROFILE,
									&dnp,
									sizeof(ddi_nw_profile));
			dlog_msg("IOCTL_SET_NW_PROFILE result: %d", rc);
			if(rc == ECE_SUCCESS)
			{
				dlog_msg("Network profile '%d' successfully set!", dnp.network_id);
				// Set it as default
				char cszNetworkId[32];
				printf(cszNetworkId, "%d", dnp.network_id);
				//Set default profile
				rc = ceSetDDParamValue(handle,
										IOCTL_SET_DEFAULT_PROFILE,
										cszNetworkId,
										strlen(cszNetworkId));
				dlog_msg("IOCTL_SET_DEFAULT_PROFILE result: %d", rc);
			}
			return rc;
		}
		*/

	using namespace com_verifone_pml::net::wifi;
	std::string auth_desc(const auth wauth)
	{
		std::string result;
		switch (wauth)
		{
			case auth_none: result.assign("None"); break;
			case auth_wep: result.assign("WEP"); break;
			case auth_wep_shared: result.assign("WEP Shared"); break;
			case auth_wpa_psk: result.assign("WPA-PSK"); break;
			case auth_wpa: result.assign("WPA-EAP"); break;
			case auth_wpa2_psk: result.assign("WPA2-PSK"); break;
			case wpa_none: result.assign("WPA-None"); break;
		}
		return result;
	}


	std::string algo_desc(const algo walgo)
	{
		std::string result;
		switch (walgo)
		{
			case algo_unknown: break; // ??
			case algo_none: result.assign("Off"); break;
			case algo_wep1: result.assign("WEP1"); break;
			case algo_tkip: result.assign("TKIP"); break;
			case algo_wep128: result.assign("WEP128"); break;
			case algo_aes_ccm: result.assign("AES-CCMP"); break;
		}
		return result;
	}

	auth convert_auth(int auth_type)
	{
		auth result;
		switch(auth_type)
		{
			case AUTH_NONE_OPEN: result = auth_none; break;
			case AUTH_NONE_WEP: result = auth_wep; break;
			case AUTH_NONE_WEP_SHARED: result = auth_wep_shared; break;
			case AUTH_WPA_PSK: result = auth_wpa_psk; break;
			case AUTH_WPA2_PSK: result = auth_wpa2_psk; break;
			case AUTH_WPA_NONE: result = wpa_none; break;
			case AUTH_WPA_EAP: result = auth_wpa; break;
			default: result = auth_unknown; break;
		}
		return result;
	}

	algo convert_algo(int wsec)
	{
		algo result;
		switch (wsec)
		{
			case ALGO_OFF: result = algo_none; break;
			case ALGO_WEP1: result = algo_wep1; break;
			case ALGO_TKIP: result = algo_tkip; break;
			case ALGO_WEP128: result = algo_wep128; break;
			case ALGO_AES_CCM: result = algo_aes_ccm; break;
			// algo_wep1
			default: result = algo_unknown; break;
		}
		return result;
	}

	std::string conv_bssid(const unsigned char * bssid, size_t bssid_size)
	{
		std::string res;
		char buf[5];
		dlog_hex(bssid, bssid_size, "BSSID");
		for (size_t i = 0; i < bssid_size; ++i)
		{
			sprintf(buf, "%02X:", *(bssid+i));
			res.append(buf);
		}
		if (!res.empty()) res.resize(res.size()-1); // strip last ':'
		dlog_msg("Conv to %s", res.c_str());
		return res;
	}

	std::string conv_bt_addr(const char * bd_addr)
	{
		return std::string(bd_addr);
	}

	int enable(const std::string &ifname, bool enable)
	{
		interface_info info;
		int handle = getInterfaceHandle(ifname, &info);
		if (handle != INVALID_INTERFACE_HANDLE)
		{
			if (info.up == enable) return PML_SUCCESS;
			// We need to change interface state
			int rc;
			if (enable)
			{
				rc = initNWIF(handle);
			}
			else
			{
				// We need to stop NWIF
				ceEnableEventNotification(); // ignore errors here
				// Bring the interface down
				rc = ceStopNWIF(handle, CE_DISCONNECT);
				if (rc == ECE_SUCCESS)
				{
					internal_fun::resetNWIFState();
					internal_fun::EVT_VEC events;
					events.push_back(CE_EVT_STOP_NW);
					events.push_back(CE_EVT_STOP_CLOSE);
					int networkEventsNo = internal_fun::waitForNetworkEvent(events);
					if (networkEventsNo <= 0)
					{
						dlog_error("Waiting for network events failed, error %d", networkEventsNo);
						rc = ECE_EX_CMD_FAILED;
					}
				}
			}
			dlog_msg("Result %d", rc);
			return rc == ECE_SUCCESS ? PML_SUCCESS : PML_FAILURE;
		}
		return PML_FAILURE;;
	}

} // namespace internal_fun
	/* ------------------------------------------------------------------ */ 

	/** Enum available network interface
	* @param[out] if_names Interface list names
	* @return Network interface count
	*/
	int enum_interfaces ( std::list<std::string>& if_names ) 
	{
		// Get the total number of network interface from this terminal (NWIF)
		internal_fun::getInterfaceHandle(std::string(""), NULL); // Init
		int NICount = ceGetNWIFCount ();
		dlog_msg("NWIFCount %d", NICount);
		if (NICount <= 0)
		{
			return 0;
		}

		stNIInfo * NIInfo = new stNIInfo[NICount];
		unsigned int dummy = 0;

		int retVal = ceGetNWIFInfo ( (stNIInfo*) NIInfo, NICount, &dummy);
		dlog_msg("ceGetNWIFInfo %d", retVal);
		if (retVal == ECE_SUCCESS)
		{
			for (int i = 0; i < NICount; ++i)
			{
				std::string dev(NIInfo[i].niDeviceName);
				if_names.push_back(dev);
			}
		}
		else
		{
			NICount = 0;
		}
		delete [] NIInfo;
		return NICount;
	}
	/* ------------------------------------------------------------------ */ 
	/** Function gets interface detailed information by name 
		* @param[in] ifc_name Interface input name
		* @param[out] info Returned interface information 
		* @return Error code if successfull */
	int get_interface_info( const std::string& ifc_name, interface_info& info )
	{
		int handle = internal_fun::getInterfaceHandle(ifc_name, &info);
		if (handle != INVALID_INTERFACE_HANDLE)
		{
			return PML_SUCCESS;
		}
		return PML_FAILURE;
	}
	/* ------------------------------------------------------------------ */ 
	/** Function return if not local network interfcace is ready
		* and configured. It can be used for determine connection state
		* @param[in] Interface name. If name is null autodetection is made 
		* @return Enumera
		*/
	int link_is_up( const char *ifc_name )
	{
		interface_info info;
		std::string ifname;
		if (ifc_name && strlen(ifc_name)) ifname.assign(ifc_name);
		int handle = internal_fun::getInterfaceHandle(ifname, &info);
		if (handle != INVALID_INTERFACE_HANDLE)
		{
			dlog_msg("link is up: %d", info.up);
			if (info.up) return net::link_state_up;
			else return net::link_state_down;
		}
		return PML_FAILURE;
	}
	/* ------------------------------------------------------------------ */ 
	/** Create netif listener for network events
		*  @param[in,out] netif_event Netif event for listen
		*  @return Error status
		*/
	int netif_events_listener_create( event_item& netif_event ) 
	{
		netif_event.event_id =  events::netlink;
		netif_event.evt.netlink._fd = -1;
		netif_event.evt.netlink.n_ifcs = 0;
		return PML_SUCCESS;
	}
	/* ------------------------------------------------------------------ */ 
	/** Destroy netif listener for network events
		*  @param[in,out] netif_event Netif event for listen
		*  @return Error status
		*/
	int netif_events_listener_destroy( event_item& /* netif_event */ )
	{
		return PML_SUCCESS;
	}
	/* ------------------------------------------------------------------ */
	/** Configure network interface using non dhcp mode
		*  if nullptr is assigned it use DHCP mode
		* */
	int config_interface( const interface_config* ifc )
	{
		return config_interface(std::string(""), ifc);
	}
	int config_interface( const std::string & ifname, const interface_config* ifc )
	{
		interface_info info;
		int handle = internal_fun::getInterfaceHandle(ifname, &info);
		if (handle != INVALID_INTERFACE_HANDLE)
		{
			stNI_IPConfig ipc;
			bool needToRestartInterface = false;
			unsigned int retSize;
			int res = ceGetNWParamValue(handle, IP_CONFIG, &ipc, sizeof(stNI_IPConfig), &retSize);
			if (res == ECE_SUCCESS)
			{
				if (ifc == 0)
				{
					if (ipc.ncDHCP != 1)
					{
						ipc.ncDHCP = 1;
						needToRestartInterface = true;
					}
				}
				else
				{
					if (ipc.ncDHCP != 0)
					{
						ipc.ncDHCP = 0;
						needToRestartInterface = true;
					}
					unsigned long ipAddr = 0;
					if (inet_aton(ifc->addr.c_str(), &ipAddr))
					{
						if (ipc.ncIPAddr != ipAddr)
						{
							ipc.ncIPAddr = ipAddr;
							needToRestartInterface = true;
						}
					}
					if (inet_aton(ifc->netmask.c_str(), &ipAddr))
					{
						if (ipc.ncSubnet != ipAddr)
						{
							ipc.ncSubnet = ipAddr;
							needToRestartInterface = true;
						}
					}
					if (inet_aton(ifc->gateway.c_str(), &ipAddr))
					{
						if (ipc.ncGateway != ipAddr)
						{
							ipc.ncGateway = ipAddr;
							needToRestartInterface = true;
						}
					}
					if (inet_aton(ifc->dns1.c_str(), &ipAddr))
					{
						if (ipc.ncDNS1 != ipAddr)
						{
							ipc.ncDNS1 = ipAddr;
							needToRestartInterface = true;
						}
					}
					if (inet_aton(ifc->dns2.c_str(), &ipAddr))
					{
						if (ipc.ncDNS2 != ipAddr)
						{
							ipc.ncDNS2 = ipAddr;
							needToRestartInterface = true;
						}
					}
				}
				if (needToRestartInterface)
				{
					dlog_alert("We need to restart network interface!");
					res = ceSetNWParamValue(handle, IP_CONFIG, &ipc, sizeof(stNI_IPConfig));
					if (res == ECE_SUCCESS)
					{
						// We need to restart NWIF
						ceEnableEventNotification(); // ignore errors here
						// Bring the interface down
						res = ceStopNWIF(handle, CE_DISCONNECT);
						if (res == ECE_SUCCESS)
						{
							internal_fun::resetNWIFState();
							internal_fun::EVT_VEC events;
							events.push_back(CE_EVT_STOP_NW);
							events.push_back(CE_EVT_STOP_CLOSE);
							int networkEventsNo = internal_fun::waitForNetworkEvent(events);
							if (networkEventsNo > 0)
							{
								dlog_msg("Network interface stopped, starting it");
								res = ceStartNWIF(handle, CE_CONNECT);
								if (res == ECE_SUCCESS)
								{
									internal_fun::resetNWIFState();
									events.clear();
									events.push_back(CE_EVT_START_NW);
									events.push_back(CE_EVT_NET_OUT);
									networkEventsNo = internal_fun::waitForNetworkEvent(events);
									if (networkEventsNo > 0)
									{
										// Success, we're done!
										dlog_msg("Network interface started successfully");
										return PML_SUCCESS;
									}
									else
									{
										dlog_error("Waiting for network events failed, error %d", networkEventsNo);
									}
								}
								else
								{
									dlog_error("Cannot start network interface, error %d", res);
								}
							}
							else
							{
								dlog_error("Waiting for network events failed, error %d", networkEventsNo);
							}
						}
						else
						{
							dlog_error("Cannot stop network interface, error %d", res);
						}
						// We're done!
					}
					else
					{
						dlog_error("Cannot set new network configuration, result %d!", res);
					}
				}
				else
				{
					if (!info.up)
					{
						dlog_msg("We need to start the interface");
						res = internal_fun::initNWIF( handle );
						if (res == ECE_SUCCESS)
						{
							if (info.name.find("ETH") != std::string::npos)
							{
								dlog_msg("Fully start Ethernet device");
								res = ceStartNWIF(handle, CE_CONNECT);
								if (res == ECE_SUCCESS)
								{
									internal_fun::resetNWIFState();
									internal_fun::EVT_VEC events;
									events.push_back(CE_EVT_START_NW);
									events.push_back(CE_EVT_NET_OUT);
									int networkEventsNo = internal_fun::waitForNetworkEvent(events);
									if (networkEventsNo > 0)
									{
										// Success, we're done!
										dlog_msg("Network interface started successfully");
										return PML_SUCCESS;
									}
									else
									{
										dlog_error("Waiting for network events failed, error %d", networkEventsNo);
									}
								}
								else
								{
									dlog_error("Cannot start network interface, error %d", res);
								}
							}
							return res;
						}
						else
						{
							dlog_error("Cannot start network interface, error %d", res);
						}
					}
					else
					{
						// Nothing to do
						return PML_SUCCESS;
					}
				}
			}
			else
			{
				dlog_error("Cannot get current network configuration, result %d!", res);
			}
		}
		return PML_FAILURE;
	}
	/* ------------------------------------------------------------------ */
	int is_interface_up()
	{
		return is_interface_up( std::string("") );
	}

	int is_interface_up( const std::string & ifname )
	{
		interface_info info;
		int handle = internal_fun::getInterfaceHandle(ifname, &info);
		if (handle != INVALID_INTERFACE_HANDLE)
		{
			return info.up;
		}
		return 0;
	}
	/* ------------------------------------------------------------------ */
	/** Configure network interface using non dhcp mode
		*  if nullptr is assigned it use DHCP mode
		* */
	int interface_up()
	{
		return interface_up(std::string(""));
	}
	int interface_up( const std::string & ifname )
	{
		interface_info info;
		int res;
		int handle = internal_fun::getInterfaceHandle(ifname, &info);
		if (handle != INVALID_INTERFACE_HANDLE)
		{
			if (!info.up)
			{
				dlog_msg("We need to start the interface");
				int res = internal_fun::initNWIF( handle );
				if (res == ECE_SUCCESS)
				{
					dlog_msg("Fully start network device");

					while (true)
					{
						res = ceStartNWIF(handle, CE_CONNECT);
						dlog_msg("interface_up: ceStartNWIF - %d");
						
						if ((res == ECE_SUCCESS) || (res == ECE_STATE_ERR))
						{
							internal_fun::resetNWIFState();
							internal_fun::EVT_VEC events;
							events.push_back(CE_EVT_START_NW);
							events.push_back(CE_EVT_NET_OUT);
							int networkEventsNo = internal_fun::waitForNetworkEvent(events);
							if (networkEventsNo > 0)
							{
								// Success, we're done!
								dlog_msg("Network interface started successfully");
								return PML_SUCCESS;
							}
							else
							{
								dlog_error("Waiting for network events failed, error %d", networkEventsNo);
								return PML_FAILURE;
							}
						}
						else
						{
							if (res == ECE_NO_CE_EVENT)
							{
								res = ceEnableEventNotification();
								if (res == ECE_SUCCESS)
								{
									dlog_msg("Events enabled successfully");
									continue;
								}
							}
							else if (res == ECE_START_STOP_BUSY)
							{
								dlog_alert("NWIF is busy!");
								SVC_WAIT(500);
							}
							else
							{
								dlog_error("Cannot start network interface, error %d", res);
								return PML_FAILURE;
							}
						}
					}
				}
				else
				{
					dlog_error("Cannot start network interface, error %d", res);
				}
			}
			else
			{
				// Nothing to do
				return PML_SUCCESS;
			}
		}
		return PML_FAILURE;
	}

	/* ------------------------------------------------------------------ */ 
	int get_interface_config( const std::string & ifc_name, interface_config & ifc )
	{
		int handle = internal_fun::getInterfaceHandle(ifc_name);
		if (handle != INVALID_INTERFACE_HANDLE)
		{
			stNI_IPConfig ipConfig;
			unsigned int pLen = 0;
			int rc = ceGetNWParamValue (handle, IP_CONFIG, &ipConfig, sizeof (stNI_IPConfig), &pLen);
			if (rc == ECE_SUCCESS)
			{
				// Convert all the IP addresses
				dlog_msg("IP %Xh (%d)", ipConfig.ncIPAddr, ipConfig.ncIPAddr);
				char ip[3*4+3+1];
				memset(ip, 0, sizeof(ip));
				if (inet_ntoa(ipConfig.ncIPAddr, ip) == 0)
				{
					ifc.addr.assign(ip);
				}
				ip[0] = 0;
				if (inet_ntoa(ipConfig.ncSubnet, ip) == 0)
				{
					ifc.netmask.assign(ip);
				}
				ip[0] = 0;
				if (inet_ntoa(ipConfig.ncGateway, ip) == 0)
				{
					ifc.gateway.assign(ip);
				}
				ip[0] = 0;
				if (inet_ntoa(ipConfig.ncDNS2, ip) == 0)
				{
					ifc.dns1.assign(ip);
				}
				ip[0] = 0;
				if (inet_ntoa(ipConfig.ncDNS2, ip) == 0)
				{
					ifc.dns2.assign(ip);
				}
				return 0;
			}
			else
			{
				dlog_error("Cannot get IP, error %i", rc);
			}
		}
		return PML_FAILURE;
	}

	/* ------------------------------------------------------------------ */

	/////////////////////////////////////////////////////////////////////////////////////////////////
	namespace wifi
	{
		namespace {
			static const std::string WIFI_DEVICE("/DEV/WLN1");
		}
		std::string auth_desc(const auth wauth)
		{
			std::string result;
			switch (wauth)
			{
				case auth_none: result.assign("None"); break;
				case auth_wep: result.assign("WEP"); break;
				case auth_wep_shared: result.assign("WEP Shared"); break;
				case auth_wpa_psk: result.assign("WPA-PSK"); break;
				case auth_wpa: result.assign("WPA-EAP"); break;
				case auth_wpa2_psk: result.assign("WPA2-PSK"); break;
				case wpa_none: result.assign("WPA-None"); break;
			}
			return result;
		}


		std::string algo_desc(const algo walgo)
		{
			std::string result;
			switch (walgo)
			{
				case algo_unknown: break; // ??
				case algo_none: result.assign("Off"); break;
				case algo_wep1: result.assign("WEP1"); break;
				case algo_tkip: result.assign("TKIP"); break;
				case algo_wep128: result.assign("WEP128"); break;
				case algo_aes_ccm: result.assign("AES-CCMP"); break;
			}
			return result;
		}

		auth convert_auth(int auth_type)
		{
			auth result;
			switch(auth_type)
			{
				case AUTH_NONE_OPEN: result = auth_none; break;
				case AUTH_NONE_WEP: result = auth_wep; break;
				case AUTH_NONE_WEP_SHARED: result = auth_wep_shared; break;
				case AUTH_WPA_PSK: result = auth_wpa_psk; break;
				case AUTH_WPA2_PSK: result = auth_wpa2_psk; break;
				case AUTH_WPA_NONE: result = wpa_none; break;
				case AUTH_WPA_EAP: result = auth_wpa; break;
				default: result = auth_unknown; break;
			}
			return result;
		}
		
		algo convert_algo(int wsec)
		{
			algo result;
			switch (wsec)
			{
				case ALGO_OFF: result = algo_none; break;
				case ALGO_WEP1: result = algo_wep1; break;
				case ALGO_TKIP: result = algo_tkip; break;
				case ALGO_WEP128: result = algo_wep128; break;
				case ALGO_AES_CCM: result = algo_aes_ccm; break;
				// algo_wep1
				default: result = algo_unknown; break;
			}
			return result;
		}

		/* ------------------------------------------------------------------ */
		void medium_info::str_to_mode( const char * auth, const char * algo )
		{
			// ctmp fixme - may not work correctly!
			dlog_msg("WiFi auth: %s", auth);
			if (!strcmp(auth, "WPA-PSK"))
			{
				m_auth = auth_wpa_psk;
			}
			else if (!strcmp(auth, "WPA2-PSK"))
			{
				m_auth = auth_wpa2_psk;
			}
			else if (!strcmp(auth, "WPA-EAP"))
			{
				m_auth = auth_wpa; // ??
			}
			else if (!strcmp(auth, "WPA2-EAP"))
			{
				m_auth = auth_wpa;
			}
			dlog_msg("WiFi network encryption: %s", algo);
			if (strstr(auth, "CCMP"))
			{
				m_algo = algo_aes_ccm;
			}
			else if (!strcmp(algo, "TKIP"))
			{
				m_algo = algo_tkip;
			}
			else if (!strcmp(algo, "WEP"))
			{
				// No way to detect WEP shared here
				m_algo = algo_wep1;
			}
			else
			{
				m_algo = algo_none;
			}
		}
		/* ------------------------------------------------------------------ */
		void medium_info::mode_to_str( auth wauth, algo walgo )
		{
			m_algo_desc = internal_fun::algo_desc( walgo );
			m_auth_desc = internal_fun::auth_desc( wauth );
		}
		/* ------------------------------------------------------------------ */
		int get_available_ap_list( const std::string& ifc_name, std::vector<medium_info> & ap_list )
		{
			int handle = internal_fun::getInterfaceHandle(ifc_name);
			if (handle != INVALID_INTERFACE_HANDLE)
			{
				do
				{
					int rc = internal_fun::initNWIF( handle );
					if (rc != ECE_SUCCESS)
					{
						dlog_error("Unable to initialize NWIF: %d", rc);
						break;
					}
					// Get available networks list
					unsigned char inIoctlSearch = 1;
					rc = ceSetDDParamValue(handle, IOCTL_SEARCH, &inIoctlSearch, 1);
					if (rc != ECE_SUCCESS)
					{
						dlog_error("Unable to IOCTL_SEARCH for WiFi devices: %d", rc);
						break;
					}
					// Wait for event
					internal_fun::EVT_VEC events;
					events.push_back(CE_EVT_DDI_APPL);
					int networkEventsNo = internal_fun::waitForNetworkEvent(events);
					if (networkEventsNo <= 0)
					{
						break;
					}
					std::vector<ddi_bss_info> networkList;
					rc = internal_fun::getAvailableNetworkList( handle, networkList );
					if (rc != ECE_SUCCESS)
					{
						dlog_error("Unable to get available network list: %d", rc);
						break;
					}
					for (std::vector<ddi_bss_info>::const_iterator it = networkList.begin(); it != networkList.end(); ++it)
					{
						medium_info ap(internal_fun::conv_bssid(it->BSSID, sizeof(it->BSSID)), it->SSID, it->RSSI, it->NWAuth, it->DataEncrypt);
						ap_list.push_back(ap);
					}
					return PML_SUCCESS;
				} while(false);
			}
			return PML_FAILURE;
		}
		/* ------------------------------------------------------------------ */ 
		int connect ( const std::string& ifc_name, const medium_config& wcfg, const interface_config* ifc )
		{
			// Check if interface is available
			int handle = internal_fun::getInterfaceHandle(ifc_name);
			if (handle != INVALID_INTERFACE_HANDLE)
			{
				do
				{
					int rc = internal_fun::initNWIF( handle );
					if (rc != ECE_SUCCESS)
					{
						dlog_error("Unable to initialize NWIF: %d", rc);
						break;
					}
					// Get list of configured WiFi AP
					std::vector<ddi_nw_list> apList;
					rc = internal_fun::getConfiguredAPList( handle, apList );
					if (rc != ECE_SUCCESS)
					{
						dlog_error("Cannot get list of configured AP, error %d", rc);
						break;
					}
					internal_fun::EVT_VEC events;
					internal_fun::EVT_VEC_OUT outEvents;
					// Find if our AP is on that list
					int profileIndex = -1;
					for (int i = 0; i < apList.size(); ++i)
					{
						if (!strcmp(apList[i].SSID, wcfg.get_ssid().c_str()))
						{
							profileIndex = i;
							break;
						}
					}
					if (profileIndex != -1)
					{
						// Set appropriate profile as default
						rc = internal_fun::setDefaultProfile(handle, profileIndex);
					}
					else
					{
						if (wcfg.get_auth() == auth_unknown)
						{
							// Get available networks list
							unsigned char inIoctlSearch = 1;
							rc = ceSetDDParamValue(handle, IOCTL_SEARCH, &inIoctlSearch, 1);
							if (rc != 0)
							{
								dlog_error("Unable to IOCTL_SEARCH for WiFi devices: %d", rc);
								break;
							}
							// Wait for event
							events.clear();
							events.push_back(CE_EVT_DDI_APPL);
							int networkEventsNo = internal_fun::waitForNetworkEvent(events);
							if (networkEventsNo <= 0)
							{
								rc = -1;
								break;
							}
							std::vector<ddi_bss_info> networkList;
							rc = internal_fun::getAvailableNetworkList( handle, networkList );
							if (rc != ECE_SUCCESS)
							{
								dlog_error("Unable to get available network list: %d", rc);
								break;
							}
							// Find if our SSID is on the list
							for (size_t i = 0; i < networkList.size(); ++i)
							{
								if (wcfg.get_ssid() == networkList[i].SSID)
								{
									profileIndex = i;
									break;
								}
							}
							if (profileIndex != -1)
							{
								dlog_msg("Found network SSID '%s' at index %d, setting up new profile", networkList[profileIndex].SSID, profileIndex);
								rc = internal_fun::setupNewProfile( handle, &networkList[profileIndex], wcfg );
							}
						}
						else
						{
							// Just setup new profile
							ddi_bss_info profile;
							memset(&profile, 0, sizeof(ddi_bss_info));
							memcpy(profile.BSSID, wcfg.get_bssid().c_str(), std::min( wcfg.get_bssid().size(), sizeof profile.BSSID ));
							profile.SSID_len = std::min( wcfg.get_ssid().size(), sizeof profile.SSID );
							strncpy(profile.SSID, wcfg.get_ssid().c_str(), profile.SSID_len);
							// Convert !
							std::string tmp = internal_fun::auth_desc(wcfg.get_auth());
							strncpy(profile.NWAuth, tmp.c_str(), std::min( tmp.size(), sizeof profile.NWAuth ));
							tmp = internal_fun::algo_desc(wcfg.get_algo());
							strncpy(profile.DataEncrypt, tmp.c_str(), std::min( tmp.size(), sizeof profile.DataEncrypt ));
							profile.bss_type = 0;
							profile.RSSI = 0;
							rc = internal_fun::setupNewProfile( handle, &profile, wcfg );
						}
					}

					if (rc != ECE_SUCCESS)
					{
						dlog_error("Cannot connect to network SSID '%s', error %d!", wcfg.get_ssid().c_str(), rc);
						break;
					}
					// Finally, send network up
					rc = ceStartNWIF(handle, CE_CONNECT);
					// Wait for appropriate events here
					if (rc == ECE_SUCCESS)
					{
						internal_fun::resetNWIFState();
						events.clear();
						internal_fun::EVT_VEC_OUT outEvents;
						events.push_back(CE_EVT_NET_FAILED);
						events.push_back(CE_EVT_NET_UP);
						// events.push_back(CE_EVT_START_NW);
						int networkEventsNo = internal_fun::waitForNetworkEvent(events, outEvents);
						if (networkEventsNo > 0)
						{
							bool isOkay = true;
							for (internal_fun::EVT_VEC_OUT::const_iterator it = outEvents.begin(); it != outEvents.end(); ++it)
							{
								if (it->evt.neEvt == CE_EVT_NET_FAILED)
								{
									isOkay = false;
									break;
								}
							}
							if (!isOkay)
							{
								dlog_error("Unable to bring up network for SSID '%s'!", wcfg.get_ssid().c_str());
								rc = ceStartNWIF(handle, CE_OPEN);
								// Try reactivating NWIF
								// rc = ceStopNWIF(handle, CE_LINK);
								if (rc == ECE_SUCCESS)
								{
									internal_fun::resetNWIFState();
									events.clear();
									events.push_back(CE_EVT_START_OPEN);
									int networkEventsNo = internal_fun::waitForNetworkEvent(events);
									if (networkEventsNo > 0)
									{
										// Try deleting current profile and re-try everything to create new profile
										int defaultProfile = -1;
										rc = internal_fun::getDefaultProfile(handle, defaultProfile);
										if (rc != ECE_SUCCESS)
										{
											dlog_error("Cannot get default profile, error %d", rc);
											break;
										}
										rc = internal_fun::deleteProfile(handle, defaultProfile);
										continue; // Retry everything, a new profile will be created
									}
								}
								else
								{
									dlog_error("Cannot re-start NWIF: %d", rc);
								}
				#if 0
				// Get extended error
				char buf[100];
				unsigned int len;
				rc = ceGetDDParamValue(handle, IOCTL_GET_EXTENDED_ERROR, sizeof(buf), buf, &len);
				if (rc == ECE_SUCCESS) dlog_error("ExtErr: %s", buf);
				else dlog_error("Cannot get extended error: %d", rc);
				#endif
							}
							else
							{
								return PML_SUCCESS;
							}
						}
					}
					break;
				} while(true);
			}
			return PML_FAILURE;
		}
		/* ------------------------------------------------------------------ */ 
		int find_profile(const std::string& ifc_name, const std::string &SSID, int &profile_id)
		{
			int handle = internal_fun::getInterfaceHandle(ifc_name);
			if (handle != INVALID_INTERFACE_HANDLE)
			{
				do
				{
					int rc = internal_fun::initNWIF( handle );
					if (rc != ECE_SUCCESS)
					{
						dlog_error("Unable to initialize NWIF: %d", rc);
						break;
					}
					// Get list of configured WiFi AP
					std::vector<ddi_nw_list> apList;
					rc = internal_fun::getConfiguredAPList( handle, apList );
					if (rc != ECE_SUCCESS)
					{
						dlog_error("Cannot get list of configured AP, error %d", rc);
						break;
					}
					// Find if our AP is on that list
					profile_id = -1;
					for (int i = 0; i < apList.size(); ++i)
					{
						if (!strcmp(apList[i].SSID, SSID.c_str()))
						{
							profile_id = i;
							dlog_msg("Profile found at [%d]", i);
							return PML_SUCCESS;
						}
					}
				} while(false);
			}
			return PML_FAILURE;
		}
		/* ------------------------------------------------------------------ */
		int set_default_profile( const std::string& ifc_name, int profile_id )
		{
			int handle = internal_fun::getInterfaceHandle(ifc_name);
			if (handle != INVALID_INTERFACE_HANDLE)
			{
				do
				{
					int rc = internal_fun::initNWIF( handle );
					if (rc != ECE_SUCCESS)
					{
						dlog_error("Unable to initialize NWIF: %d", rc);
						break;
					}
					if (!internal_fun::is_profile_id_valid(handle, profile_id))
					{
						dlog_error("Invalid profile ID %d", profile_id);
						break;
					}
					// Check default profile first
					int defaultProfile = -1;
					rc = internal_fun::getDefaultProfile(handle, defaultProfile);
					if (rc != ECE_SUCCESS)
					{
						dlog_error("Cannot get default profile, error %d", rc);
						break;
					}
					if (defaultProfile != profile_id)
					{
						rc = internal_fun::setDefaultProfile(handle, profile_id);
						if (rc != ECE_SUCCESS)
						{
							dlog_error("Unable to set default profile: %d", rc);
							break;
						}
					}
					return PML_SUCCESS;
				} while(false);
			}
			return PML_FAILURE;
		}
		/* ------------------------------------------------------------------ */
		int get_default_profile( const std::string& ifc_name, medium_config& cfg )
		{
			int handle = internal_fun::getInterfaceHandle(ifc_name);
			if (handle != INVALID_INTERFACE_HANDLE)
			{
				do
				{
					int profile_id = 0;
					int rc = internal_fun::getDefaultProfile(handle, profile_id);
					if (rc != ECE_SUCCESS)
					{
						dlog_error("Cannot get default profile, error %d", rc);
						break;
					}
					dlog_msg("Getting information for profile %d", profile_id);
					// Set current profile, get it
					char buf[11];
					sprintf(buf, "%d", profile_id);
					rc = ceSetNWParamValue(handle, IOCTL_SET_NW_ID, buf, strlen(buf) + 1);
					if (rc != ECE_SUCCESS)
					{
						dlog_error("Cannot select profile %d: %d", profile_id, rc);
						break;
					}
					ddi_nw_profile p;
					unsigned int len = 0;
					rc = ceGetNWParamValue(handle, IOCTL_GET_NW_PROFILE, (void *)&p, sizeof (ddi_nw_profile), &len);
					if (rc != ECE_SUCCESS)
					{
						dlog_error("Cannot get profile information: %d", rc);
						break;
					}
					if (p.BSSID[0] == 0 && p.BSSID[1] == 0)
					{
						dlog_alert("No BSSID, trying to get it elsewhere");
						ddi_bss_info b;
						rc = ceGetNWParamValue(handle, IOCTL_GET_BSSINFO, (void *)&b, sizeof (ddi_bss_info), &len);
						if (rc == ECE_SUCCESS)
						{
							memcpy(p.BSSID, b.BSSID, sizeof b.BSSID);
							p.bssid_len = sizeof b.BSSID;
						}
					}
					medium_config new_cfg(std::string(p.ssid, p.ssid_len), internal_fun::convert_auth(p.auth_type), internal_fun::conv_bssid(p.BSSID, p.bssid_len), internal_fun::convert_algo(p.wsec));
					cfg = new_cfg;
					return PML_SUCCESS;
				} while(false);
			}
			return PML_FAILURE;
		}
		/* ------------------------------------------------------------------ */
		int get_default_profile_id( const std::string& ifc_name, int &profile_id)
		{
			int handle = internal_fun::getInterfaceHandle(ifc_name);
			if (handle != INVALID_INTERFACE_HANDLE)
			{
				int rc = internal_fun::getDefaultProfile(handle, profile_id);
				if (rc == ECE_SUCCESS) return PML_SUCCESS;
				dlog_error("Cannot get default profile, error %d", rc);
			}
			return PML_FAILURE;
		}
		/* ------------------------------------------------------------------ */
		int add_profile( const std::string& ifc_name, const medium_config& wcfg, int & profile_id )
		{
			int handle = internal_fun::getInterfaceHandle(ifc_name);

			if (handle != INVALID_INTERFACE_HANDLE)
			{
				do
				{
					if (find_profile(ifc_name, wcfg.get_ssid(), profile_id) == PML_SUCCESS && profile_id != -1)
					{
						dlog_msg("Profile already exists (%d)!", profile_id);
						int rc = internal_fun::setDefaultProfile(handle, profile_id);

						return (rc == ECE_SUCCESS) ? PML_SUCCESS : PML_FAILURE;
					}

					int rc = internal_fun::initNWIF( handle );

					if (rc != ECE_SUCCESS)
					{
						dlog_error("Unable to initialize NWIF: %d", rc);
						break;
					}

					// We're here, so profile for given SSID doesn't exist for sure. 
					if (wcfg.get_auth() == auth_unknown || wcfg.get_auth() == auth_none ||
						wcfg.get_algo() == algo_unknown || wcfg.get_algo() == algo_none || wcfg.get_algo() == algo_default)
					{
						// Get available networks list
						unsigned char inIoctlSearch = 1;
						rc = ceSetDDParamValue(handle, IOCTL_SEARCH, &inIoctlSearch, 1);
						if (rc != 0)
						{
							dlog_error("Unable to IOCTL_SEARCH for WiFi devices: %d", rc);
							break;
						}

						// Wait for event
						internal_fun::EVT_VEC events;
						events.push_back(CE_EVT_DDI_APPL);
						int networkEventsNo = internal_fun::waitForNetworkEvent(events);
						if (networkEventsNo <= 0)
						{
							rc = -1;
							break;
						}

						std::vector<ddi_bss_info> networkList;
						rc = internal_fun::getAvailableNetworkList( handle, networkList );

						if (rc != ECE_SUCCESS)
						{
							dlog_error("Unable to get available network list: %d", rc);
							break;
						}

						// Find if our SSID is on the list
						profile_id = -1;
						for (size_t i = 0; i < networkList.size(); ++i)
						{
							if (wcfg.get_ssid() == networkList[i].SSID)
							{
								profile_id = i;
								break;
							}
						}

						if (profile_id != -1)
						{
							dlog_msg("Found network SSID '%s' at index %d, setting up new profile", networkList[profile_id].SSID, profile_id);
							rc = internal_fun::setupNewProfile( handle, &networkList[profile_id], wcfg );
						}
					}
					else
					{
						// Just setup new profile
						ddi_bss_info profile;
						memset(&profile, 0, sizeof(ddi_bss_info));
						memcpy(profile.BSSID, wcfg.get_bssid().c_str(), std::min( wcfg.get_bssid().size(), sizeof profile.BSSID ));
						profile.SSID_len = std::min( wcfg.get_ssid().size(), sizeof profile.SSID );
						strncpy(profile.SSID, wcfg.get_ssid().c_str(), profile.SSID_len);

						// Convert !
						std::string tmp = internal_fun::auth_desc(wcfg.get_auth());
						strncpy(profile.NWAuth, tmp.c_str(), std::min( tmp.size(), sizeof profile.NWAuth ));
						tmp = internal_fun::algo_desc(wcfg.get_algo());
						strncpy(profile.DataEncrypt, tmp.c_str(), std::min( tmp.size(), sizeof profile.DataEncrypt ));

						profile.bss_type = 0;
						profile.RSSI = 0;

						rc = internal_fun::setupNewProfile( handle, &profile, wcfg );
					}

					if (rc != ECE_SUCCESS)
					{
						dlog_error("Cannot create profile for network SSID '%s', error %d!", wcfg.get_ssid().c_str(), rc);
						break;
					}

					return PML_SUCCESS;

				} while(false);
			}
			return PML_FAILURE;
		}

		/* ------------------------------------------------------------------ */
		int enable( bool enable )
		{
			return internal_fun::enable(WIFI_DEVICE, enable);
		}
		/* ------------------------------------------------------------------ */
		int connect( const std::string& ifc_name, int profile_id )
		{
			// Check if interface is available
			int handle = internal_fun::getInterfaceHandle(ifc_name);
			if (handle != INVALID_INTERFACE_HANDLE)
			{
				int currentDefaultProfile = -1;
				int rc = internal_fun::getDefaultProfile(handle, currentDefaultProfile);
				if (rc == ECE_SUCCESS)
				{
					if (currentDefaultProfile != profile_id)
					{
						// Try to set up default profile
						rc = internal_fun::setDefaultProfile(handle, profile_id);
					}
					// Connect
					
					// Finally, send network up
					rc = ceStartNWIF(handle, CE_CONNECT);
					dlog_msg("Connection result %d", rc);
					// Wait for appropriate events here
					if (rc == ECE_SUCCESS)
					{
						internal_fun::resetNWIFState();
						internal_fun::EVT_VEC events;
						events.push_back(CE_EVT_NET_FAILED);
						events.push_back(CE_EVT_NET_UP);
						// events.push_back(CE_EVT_START_NW);
						int networkEventsNo = internal_fun::waitForNetworkEvent(events);
						if (networkEventsNo > 0)
						{
							dlog_msg("Connected to WiFi profile %d", profile_id);
							return PML_SUCCESS;
						}
					}
				}
			}
			return PML_FAILURE;
		}

		/* ------------------------------------------------------------------ */
		int get_profiles_count( const std::string& ifc_name, int & cnt )
		{
			// Check if interface is available
			int handle = internal_fun::getInterfaceHandle(ifc_name);
			if (handle != INVALID_INTERFACE_HANDLE)
			{
				cnt = 0;
				int rc = internal_fun::getConfiguredAPCount(handle, cnt);
				if (rc == ECE_SUCCESS) return PML_SUCCESS;
			}
			return PML_FAILURE;
		}

		/* ------------------------------------------------------------------ */
		int get_profile_info( const std::string& ifc_name, network_profile & prof, int profile_id /*= -1*/ )
		{
			// Check if interface is available
			int handle = internal_fun::getInterfaceHandle(ifc_name);
			if (handle != INVALID_INTERFACE_HANDLE)
			{
				bool is_default = false;
				int rc = ECE_SUCCESS;
				do
				{
					if (profile_id == -1)
					{
						rc = internal_fun::getDefaultProfile(handle, profile_id);
						if (rc != ECE_SUCCESS)
						{
							dlog_error("Cannot get default profile, error %d", rc);
							break;
						}
						is_default = true;
					}
					dlog_msg("Getting information for profile %d", profile_id);
					// Set current profile, get it
					char buf[11];
					sprintf(buf, "%d", profile_id);
					rc = ceSetNWParamValue(handle, IOCTL_SET_NW_ID, buf, strlen(buf) + 1);
					if (rc != ECE_SUCCESS)
					{
						dlog_error("Cannot select profile: %d", rc);
						break;
					}
					ddi_nw_profile p;
					unsigned int length = 0;
					rc = ceGetNWParamValue(handle, IOCTL_GET_NW_PROFILE, (void *)&p, sizeof (ddi_nw_profile), &length);
					if (rc != ECE_SUCCESS)
					{
						dlog_error("Cannot get profile information: %d", rc);
						break;
					}
					if (p.bssid_len == 0)
					{
						ddi_bss_info dbi;
						memset(&dbi, 0, sizeof (ddi_bss_info));
						rc = ceGetNWParamValue(handle, IOCTL_GET_BSSINFO, &dbi, sizeof(dbi), &length);
						dlog_msg("IOCTL_GET_BSSINFO [%d] [%s] [%s]", rc, dbi.SSID, dbi.NWAuth);
						if (rc == ECE_SUCCESS)
						{
							p.bssid_len = sizeof dbi.BSSID;
							memcpy(p.BSSID, dbi.BSSID, p.bssid_len);
						}
					}
					medium_config cfg(std::string(p.ssid, p.ssid_len), internal_fun::convert_auth(p.auth_type), internal_fun::conv_bssid(p.BSSID, p.bssid_len), internal_fun::convert_algo(p.wsec));

					network_profile result(cfg, profile_id, is_default);
					if (!result.is_default())
					{
						int default_profile = -1;
						rc = internal_fun::getDefaultProfile(handle, default_profile);
						if (rc == ECE_SUCCESS) // Ignore error here, worst thing is we won't set this profile as default
						{
							// prof.is_default = profile_id == default_profile;
						}
					}
					prof = result;
					return PML_SUCCESS;
				} while(false);
			}
			return PML_FAILURE;
		}
		/* ------------------------------------------------------------------ */
		int get_profile_info( const std::string& ifc_name, std::vector<network_profile> & prof )
		{
			// Check if interface is available
			int handle = internal_fun::getInterfaceHandle(ifc_name);
			if (handle != INVALID_INTERFACE_HANDLE)
			{
				int rc = ECE_SUCCESS;
				do
				{
					unsigned int len = 0;
					char buf[CEIF_PARAM_VALUE_SZ];
					int count = 0;
					int rc = ceGetDDParamValue(handle, IOCTL_GET_NW_CNT, sizeof(buf), buf, &len);
					dlog_msg("ceGetDDParamValue(NW_CNT) = %d",  rc);

					if (rc == ECE_DD_NOT_READY)
					{
						dlog_error("WiFi driver is not ready - WAIT 100mls");
						SVC_WAIT(100);
						continue;
					}
					else if (rc != ECE_SUCCESS)
					{
						dlog_error("Cannot get profile count!");
						break;
					}

					*(buf + len) = 0;
					count = atoi(buf);
					
					int default_profile_id = 0;
					// Get default profile ID
					rc = internal_fun::getDefaultProfile(handle, default_profile_id);
					if (rc != ECE_SUCCESS)
					{
						dlog_error("Cannot get default profile, error %d", rc);
						break;
					}
					for (int i = 0; i < count; ++i)
					{
						dlog_msg("Getting information for profile %d", i);
						// Set current profile, get it
						char buf[11];
						sprintf(buf, "%d", i);
						rc = ceSetNWParamValue(handle, IOCTL_SET_NW_ID, buf, strlen(buf) + 1);
						if (rc != ECE_SUCCESS)
						{
							dlog_error("Cannot select profile %d: %d", i, rc);
							break;
						}
						ddi_nw_profile p;
						rc = ceGetNWParamValue(handle, IOCTL_GET_NW_PROFILE, (void *)&p, sizeof (ddi_nw_profile), &len);
						if (rc != ECE_SUCCESS)
						{
							dlog_error("Cannot get profile information: %d", rc);
							break;
						}
						if (p.bssid_len == 0)
						{
							ddi_bss_info dbi;
							memset(&dbi, 0, sizeof (ddi_bss_info));
							rc = ceGetNWParamValue(handle, IOCTL_GET_BSSINFO, &dbi, sizeof(dbi), &len);
							dlog_msg("IOCTL_GET_BSSINFO [%d] [%s] [%s]", rc, dbi.SSID, dbi.NWAuth);
							if (rc == ECE_SUCCESS)
							{
								p.bssid_len = sizeof dbi.BSSID;
								memcpy(p.BSSID, dbi.BSSID, p.bssid_len);
							}
						}
						medium_config cfg(std::string(p.ssid, p.ssid_len), internal_fun::convert_auth(p.auth_type), internal_fun::conv_bssid(p.BSSID, p.bssid_len), internal_fun::convert_algo(p.wsec));
						prof.push_back(network_profile(cfg, i, i == default_profile_id));
					}
					return PML_SUCCESS;
				} while(true);
			}
			return PML_FAILURE;
		}
		/* ------------------------------------------------------------------ */
		int get_current_ap_info( const std::string& ifc_name, medium_info &ap )
		{
			// Check if interface is available
			int handle = internal_fun::getInterfaceHandle(ifc_name);
			if (handle != INVALID_INTERFACE_HANDLE)
			{
				int rc = ECE_SUCCESS;
				do
				{
					unsigned int length = 0;
					ddi_bss_info dbi;
					memset(&dbi, 0, sizeof (ddi_bss_info));
					rc = ceGetNWParamValue(handle, IOCTL_GET_BSSINFO, &dbi, sizeof(dbi), &length);
					dlog_msg("IOCTL_GET_BSSINFO [%d] [%s] [%s]", rc, dbi.SSID, dbi.NWAuth);
					if (rc != ECE_SUCCESS)
					{
						dlog_error("Cannot get BSS info: %d", rc);
						break;
					}
					medium_info result( internal_fun::conv_bssid(dbi.BSSID, sizeof dbi.BSSID),
							std::string(dbi.SSID, dbi.SSID_len), dbi.RSSI, dbi.NWAuth, dbi.DataEncrypt );
					ap = result;
					return PML_SUCCESS;
				} while(false);
			}
			return PML_FAILURE;
		}
		/* ------------------------------------------------------------------ */
		int delete_profile( const std::string& ifc_name, int profile_id )
		{
				// Check if interface is available
			int handle = internal_fun::getInterfaceHandle(ifc_name);
			if (handle != INVALID_INTERFACE_HANDLE)
			{
				int rc = ECE_SUCCESS;
				do
				{
					if (!internal_fun::is_profile_id_valid(handle, profile_id))
					{
						dlog_error("Invalid profile ID %d", profile_id);
						break;
					}
					rc = internal_fun::deleteProfile(handle, profile_id);
					dlog_msg("Profile removal: %d", rc);
					if (rc == ECE_SUCCESS) return PML_SUCCESS;
				} while(false);
			}
			return PML_FAILURE;
		}
		/* ------------------------------------------------------------------ */
	} // namespace wifi
	/* ------------------------------------------------------------------ */
	/* ------------------------------------------------------------------ */
	/* ------------------------------------------------------------------ */
	namespace gsm
	{
		int register_to_network( const std::string &pin )
		{
			errno = ENXIO;
			return -1;
		}
		/* ------------------------------------------------------------------ */
		int deregister_from_network()
		{
			errno = ENXIO;
			return -1;
		}
		/* ------------------------------------------------------------------ */
		int connect( const medium_config& cfg, const interface_config* ifc /*= NULL*/ )
		{
			errno = ENXIO;
			return -1;
		}
		/* ------------------------------------------------------------------ */
		int disconnect()
		{
			errno = ENXIO;
			return -1;
		}
		/* ------------------------------------------------------------------ */
		int get_signal_strength()
		{
			errno = ENXIO;
			return -1;
		}
		/* ------------------------------------------------------------------ */
		int get_connection_info( medium_config& cfg, const interface_config* ifc /*= NULL*/ )
		{
			errno = ENXIO;
			return -1;
		}
		/* ------------------------------------------------------------------ */
	}

/**************************************************************************************************
 **************************************************************************************************
 BLUETOOTH
 **************************************************************************************************
 **************************************************************************************************/

namespace bluetooth {
	/* ------------------------------------------------------------------ */ 
	/** Get connected list of devices
		* @param[in] List of connected devices
		* @return error code
		*/

	namespace {
		// Workaround: EOS doesn't offer any API to check whether we are connected to some device and which one...

		static const char * BT_DEFAULT_DEVICE = "I:15/btconndev.txt";
		bool read_bt_device(ddi_bt_device & dev)
		{
			bool result = false;
			int hdl = open(BT_DEFAULT_DEVICE, O_RDONLY);
			if (hdl >= 0)
			{
				memset(&dev, 0, sizeof(dev));
				char * bufs[] = { dev.bd_addr, dev.bt_class, dev.name, dev.port_bt };
				char buf[100];
				int rdCount;
				for (size_t i = 0; i < sizeof(bufs)/sizeof(bufs[0]); ++i)
				{
					rdCount = read(hdl, buf, sizeof(buf)-1);
					if (rdCount < 0) break;
					buf[rdCount] = 0;
					strncpy(bufs[i], buf, sizeof(dev.bd_addr));
				}
				if (rdCount > 0)
				{
					rdCount = read(hdl, buf, sizeof(buf)-1);
					if (rdCount > 0)
					{
						buf[rdCount] = 0;
						sscanf(buf, "%d", &dev.port_bt_bit);
						result = true;
					}
					dlog_msg("Read current device: address %s, class %s, name %s, port %s, port bits %d", dev.bd_addr, dev.bt_class, dev.name, dev.port_bt, dev.port_bt_bit);
				}
				close(hdl);
			}
			return result;
		}

		bool write_bt_device(const ddi_bt_device & dev)
		{
			bool result = false;
			int hdl = open(BT_DEFAULT_DEVICE, O_WRONLY | O_CREAT);
			if (hdl >= 0)
			{
				const char * bufs[] = { dev.bd_addr, dev.bt_class, dev.name, dev.port_bt };
				int wCount;
				for (size_t i = 0; i < sizeof(bufs)/sizeof(bufs[0]); ++i)
				{
					wCount = write(hdl, bufs[i], strlen(bufs[i]));
					if (wCount < strlen(bufs[i])) break;
				}
				if (wCount > 0)
				{
					char buf[100];
					sprintf(buf, "%d", dev.port_bt_bit);
					wCount = write(hdl, buf, strlen(buf));
					if (wCount > 0)
					{
						result = true;
					}
					dlog_msg("Wrote current device: address %s, class %s, name %s, port %s, port bits %d", dev.bd_addr, dev.bt_class, dev.name, dev.port_bt, dev.port_bt_bit);
				}
				close(hdl);
			}
			return result;
		}

		ddi_bt_device init_ddi_bt_device()
		{
			ddi_bt_device ddibtdev;
			memset(&ddibtdev, 0, sizeof(ddi_bt_device));
			// try reading, ignore errors, default device may not be configured yet
			read_bt_device(ddibtdev);
			return ddibtdev;
		}
		static ddi_bt_device m_connected = init_ddi_bt_device();

		static const std::string BT_DEVICE("/DEV/BLUETOOTH");
		static const char * BT_DEFAULT_ID = "I:15/btdef.txt";

		bool read_default_paired_id(int & id)
		{
			bool result = false;
			int hdl = open(BT_DEFAULT_ID, O_RDONLY);
			if (hdl >= 0)
			{
				char buf[10];
				int rdCount = read(hdl, buf, sizeof(buf));
				if (rdCount > 0)
				{
					buf[rdCount] = 0;
					if (sscanf(buf, "%d", &id) == 1)
					{
						dlog_msg("Default paired ID is %d", id);
						result = true;
					}
				}
				close(hdl);
			}
			return result;
		}

		bool write_default_paired_id(int id)
		{
			int result = false;
			int hdl = open(BT_DEFAULT_ID, O_WRONLY | O_CREAT);
			if (hdl >= 0)
			{
				char buf[10];
				sprintf(buf, "%d", id);
				int slen = strlen(buf);
				int wCount = write(hdl, buf, slen);
				if (wCount == slen) result = true;
				close(hdl);
			}
			return result;
		}

		bool delete_default_paired_id()
		{
			return _remove(BT_DEFAULT_ID) == 0;
		}

		bool is_dione_base(const char * btAddr)
		{
			return strncmp(btAddr, "008098", 6) == 0;
		}

		static std::vector<ddi_bt_device> m_paired;
		static bool m_paired_read = false;
		int read_paired_list()
		{
			if (!m_paired_read)
			{
				int handle = internal_fun::getInterfaceHandle(BT_DEVICE, NULL);
				if (handle != INVALID_INTERFACE_HANDLE)
				{
					do
					{
						// get paired devices count
						char getCEIFValBuf[CEIF_PARAM_VALUE_SZ];
						unsigned int getCEIFValLen;
						int rc;
						
						while (true)
						{
							rc = ceGetDDParamValue(handle, IOCTL_GET_BT_PAIR_CNT, sizeof(getCEIFValBuf), getCEIFValBuf, &getCEIFValLen);

							if (rc == ECE_DD_NOT_READY)
							{
								dlog_error("ceGetDDParamValue():IOCTL_GET_BT_PAIR_CNT - error %d - device may still be processing initialization", rc);
								SVC_WAIT(100);
								continue;
							}

							break;
						}

						if(rc < 0)
						{
							dlog_error("ceGetDDParamValue():IOCTL_GET_BT_PAIR_CNT error %d", rc);
							break;
						}

						unsigned int iBTPairCount = atoi(getCEIFValBuf);
						dlog_msg("Bluetooth paired devices count: %d", iBTPairCount);
						if (iBTPairCount == 0) return PML_SUCCESS; // No paired devices

						// print paired devices
						ddi_bt_device * stBtDev = new ddi_bt_device[iBTPairCount];
						const size_t stBtDevSize = sizeof (ddi_bt_device) * iBTPairCount;
						assert(stBtDev);
						memset(stBtDev, 0, stBtDevSize);
						rc = ceGetDDParamValue(handle, IOCTL_GET_BT_PAIRED_DEVICES, stBtDevSize, stBtDev, &getCEIFValLen);
						if(rc<0)
						{
							dlog_error("ceGetDDParamValue():IOCTL_GET_BT_PAIRED_DEVICES error %d", rc);
							delete [] stBtDev;
							break;
						}

						// check if matches with the given device
						for (unsigned int i=0; i<iBTPairCount; i++)
						{
							dlog_msg(":::::::::: paired BT device index:%d", i);
							dlog_msg(":::: stBtDev:bd_addr:%s", stBtDev[i].bd_addr);
							dlog_msg(":::: stBtDev:bt_class:%s", stBtDev[i].bt_class);
							dlog_msg(":::: stBtDev:name:%s", stBtDev[i].name);
							dlog_msg(":::: stBtDev:port_bt:%s", stBtDev[i].port_bt);
							dlog_msg(":::: stBtDev:port_bt_bit:%08X", stBtDev[i].port_bt_bit);

							// ctmp check the below!!!
							m_paired.push_back( stBtDev[i] );
						}
						delete [] stBtDev;
						m_paired_read = true;
						return PML_SUCCESS;
					} while(true);
				}
				return PML_FAILURE;
			}
			return PML_SUCCESS;
		}

		bool set_connected(const std::string & mac)
		{
			if (read_paired_list()) return false;
			for (std::vector<ddi_bt_device>::const_iterator it = m_paired.begin(); it != m_paired.end(); ++it)
			{
				if (mac == internal_fun::conv_bt_addr(it->bd_addr))
				{
					m_connected = *it;
					write_bt_device(m_connected);
					return true;
				}
			}
			dlog_error("Cannot find connected device (%s) on the list!", mac.c_str());
			return false;
		}

	}

	/* ------------------------------------------------------------------ */
	//! Enable or disable bluetooth device
	int enable( bool enable )
	{
		return internal_fun::enable(BT_DEVICE, enable);
	}

	/* ------------------------------------------------------------------ */
	int paired_list( std::vector<device>& m_list )
	{
		if (!m_paired_read)
		{
			int res = read_paired_list();
			if (res != PML_SUCCESS) return res;
		}
		for (std::vector<ddi_bt_device>::const_iterator it = m_paired.begin(); it != m_paired.end(); ++it)
		{
			m_list.push_back( device(internal_fun::conv_bt_addr(it->bd_addr), it->name) );
		}
		return PML_SUCCESS;
	}

	int paired_list( std::vector<network_profile>& dev )
	{
		if (!m_paired_read)
		{
			int res = read_paired_list();
			if (res != PML_SUCCESS) return res;
		}
		int profile_id = 0;
		int default_paired = -1;
		read_default_paired_id(default_paired);
		for (std::vector<ddi_bt_device>::const_iterator it = m_paired.begin(); it != m_paired.end(); ++it)
		{
			device device(internal_fun::conv_bt_addr(it->bd_addr), it->name);
			dev.push_back( network_profile( device, profile_id++, profile_id==default_paired ) );
		}
		return PML_SUCCESS;
	}

	/* ------------------------------------------------------------------ */ 
	/** 
	* Get paired devices count
	* @param[in] count Get paired profiles count
	* @return error code
	*/
	int get_paired_count( int& count )
	{
		int handle = internal_fun::getInterfaceHandle(BT_DEVICE, NULL);
		if (handle != INVALID_INTERFACE_HANDLE)
		{
			do
			{
				// get paired devices count
				char getCEIFValBuf[CEIF_PARAM_VALUE_SZ];
				unsigned int getCEIFValLen;
				int rc = ceGetDDParamValue(handle, IOCTL_GET_BT_PAIR_CNT, sizeof(getCEIFValBuf), getCEIFValBuf, &getCEIFValLen);
				if(rc<0)
				{
					dlog_error("ceGetDDParamValue():IOCTL_GET_BT_PAIR_CNT error %d", rc);
					break;
				}
				count = atoi(getCEIFValBuf);
				dlog_msg("Bluetooth paired devices count: %d", count);
				return PML_SUCCESS;
			} while(false);
		}
		return PML_FAILURE;
	}

	/* ------------------------------------------------------------------ */
	/**  Scan the available devices in range 
		*  @param[out] dev_list Return device list in the application
		@return Number of count of the device or -1 and errno will be set
	*/
	int scan( std::vector<device> &dev_list )
	{
		int handle = internal_fun::getInterfaceHandle(BT_DEVICE, NULL);

		if (handle == INVALID_INTERFACE_HANDLE)
		{
			dlog_error("getInterfaceHandle - INVALID_INTERFACE_HANDLE - return PML_FAILURE");
			return PML_FAILURE;
		}
		
		int rc;
		unsigned int iResultLen = 0;
		int iSearchCount = 0;
		int btStatus = 0;
		int index = 0;
		char chBuffer[2048] = { '\0' };
		char *pVx680 = NULL;

		ddi_bt_device*	g_bt_scan_results = NULL;// used during device searching 
		stceNWEvt ceEvt;
		unsigned char ucEventData[CEIF_EVT_DATA_SZ];
		int evtDataLen;

		{
			// search class
			int arg = 0; // todo: for VFI devices only change this to 1
			rc = ceSetDDParamValue(handle, INI_SRCH_VFI_BASE, &arg, sizeof(arg));
			if (rc < 0)
			{
				dlog_error("ceSetDDParamValue():INI_SRCH_VFI_BASE error %d", rc);
				return PML_FAILURE;
			}

			// search limit
			arg = 16; // todo: Pass it here somehow???
			rc = ceSetDDParamValue(handle, INI_SRCH_MAX, &arg, sizeof(arg));
			if (rc < 0)
			{
				dlog_error("ceSetDDParamValue():INI_SRCH_MAX error %d", rc);
				return PML_FAILURE;
			}


			// search timeout
			arg = 30000; // todo: hardcoded timeout 30 seconds, we need to pass the timeout here somehow???
			rc = ceSetDDParamValue(handle, INI_SRCH_TIMEOUT, &arg, sizeof(arg));
			if (rc < 0)
			{
				dlog_error("ceSetDDParamValue():INI_SRCH_TIMEOUT error %d", rc);
				return PML_FAILURE;
			}

			//Searching for devices...
			rc = ceSetDDParamValue(handle, IOCTL_SEARCH, chBuffer, sizeof(chBuffer));
			if (rc != ECE_SUCCESS)
			{
				dlog_error("ceSetDDParamValue():IOCTL_SEARCH error %d", rc);
				return PML_FAILURE;
			}
		}
		
		dlog_msg("IOCTL: BT device search, rc: [%d]", rc);

		// Wait for search event
		internal_fun::resetNWIFState();
		internal_fun::EVT_VEC events;
		internal_fun::EVT_VEC_OUT outEvents;
		events.push_back(CE_EVT_DDI_APPL);
		bool bt_searching = true;
		int result = PML_SUCCESS;

		do
		{
			int networkEventsNo = internal_fun::waitForNetworkEvent(events, outEvents);
			if (networkEventsNo <= 0)
			{
				dlog_error("Error waiting for Bluetooth events");
				break;
			}
			while (!outEvents.empty())
			{
				internal_fun::stceNWEvtEx event = outEvents.back();
				outEvents.pop_back();
				switch (event.evt.neParam1)
				{
				case BT_SEARCH_INIT:
					dlog_msg("Bluetooth search initialized (BT_SEARCH_INIT)");
					break;
				case BT_SEARCH_DONE:
					dlog_msg("Bluetooth search done (BT_SEARCH_DONE), start device name query");
					break;
				case BT_SEARCH_FAIL:
					dlog_error("Bluetooth search failed (BT_SEARCH_FAIL)!");
					result = PML_FAILURE;
					bt_searching = false;
					break;
				case BT_QUERY_REMOTE_NAME_FOUND:
					dlog_msg("BT search new device found (BT_QUERY_REMOTE_NAME_FOUND), codes: %d/%d/%d", event.evt.neParam1, event.evt.neParam2, event.evt.neParam3);
					break;

				case BT_QUERY_REMOTE_NAME_DONE:
					{
						dlog_msg("Bluetooth search finished (BT_QUERY_REMOTE_NAME_DONE)");

						memset(chBuffer, 0, sizeof(chBuffer));
						rc = ceGetDDParamValue(handle, IOCTL_GET_SEARCH_CNT, sizeof(chBuffer), chBuffer, &iResultLen);

						iSearchCount = atoi(chBuffer);
						dlog_msg("IOCTL_GET_SEARCH_CNT result: [%d] count: [%d]", rc, iSearchCount);

						if (iSearchCount > 0)
						{
							rc = ceGetDDParamValue(handle, IOCTL_GET_SEARCH_RESULT, sizeof(chBuffer), chBuffer, &iResultLen);
							dlog_msg("IOCTL_GET_SEARCH_RESULT: BT Search Result rc: [%d]", rc);

							g_bt_scan_results = reinterpret_cast<ddi_bt_device*>(chBuffer);

							for (int iter = 0; iter < iSearchCount; iter++)
							{
								dev_list.push_back(device(g_bt_scan_results[iter].bd_addr,
									g_bt_scan_results[iter].name,
									reinterpret_cast<unsigned char *>(g_bt_scan_results[iter].bt_class)));

								dlog_msg("Device %d --> BD Address:[%s] BD Name:[%s] Class:[%s]",
									iter, g_bt_scan_results[iter].bd_addr, g_bt_scan_results[iter].name, g_bt_scan_results[iter].bt_class);
							}
						}

						bt_searching = false;
						break;
					}
				}
			}
		} while (bt_searching);

		return result;
	}

/* ------------------------------------------------------------------ */ 
/** Get connected list of devices
	* @param[in] List of connected devices
	* @return error code
	*/
int get_connected( std::vector<device>& m_list )
{
	// Read this from file - only one device will be stored...
	if (strlen(m_connected.bd_addr))
	{
		m_list.push_back( device( m_connected.bd_addr, m_connected.name, reinterpret_cast<unsigned char *>(m_connected.bt_class) ) );
		return PML_SUCCESS;
	}
	return PML_FAILURE;
}

/* ------------------------------------------------------------------ */
/** Get device as defualt paired
* @param[out] Get paired device info
* @return Error code
*/
int get_default_paired( device& cfg )
{
	int pair_index;
	if (read_default_paired_id(pair_index))
	{
		if (!m_paired_read)
		{
			int res = read_paired_list();
			if (res != PML_SUCCESS) return res;
		}
		if (pair_index < m_paired.size())
		{
			cfg = device( internal_fun::conv_bt_addr(m_paired[pair_index].bd_addr), m_paired[pair_index].name );
			return PML_SUCCESS;
		}
		else
		{
			dlog_error("Invalid paired index %d, only %u paired devices!", pair_index, m_paired.size());
			delete_default_paired_id();
		}
	}
	else
	{
		dlog_error("Cannot determine default paired index");
	}
	return PML_FAILURE;
}
/* ------------------------------------------------------------------ */ 
/** Get paired device profile id
	* @param[out] profile_id Paired profile index returned
	* @return Error code 
	*/
int get_default_paired_id( int &pair_index )
{
	if (read_default_paired_id(pair_index)) return PML_SUCCESS;
	return PML_FAILURE;
}

/* ------------------------------------------------------------------ */ 
/** Function establish Bluetooth connection with selected type
	* @param[in] link Connection type of the device
	* @return Error -1 if failed and errno set
	*/
int connect(int pair_index, conn_type::_enum_ link)
{
	int handle = internal_fun::getInterfaceHandle(BT_DEVICE, NULL);
	if (handle != INVALID_INTERFACE_HANDLE)
	{
		if (!m_paired_read)
		{
			int res = read_paired_list();
			if (res != PML_SUCCESS) return res;
		}
		if (pair_index < m_paired.size())
		{
			do
			{
				ddi_bt_device stBtDevSet;
				strncpy(stBtDevSet.bd_addr, m_paired[pair_index].bd_addr, sizeof(stBtDevSet.bd_addr));
				strcpy(stBtDevSet.port_bt, CE_DEV_ETH_BT);
				dlog_msg("Bluetooth: Connecting to %s/%s\n", stBtDevSet.bd_addr, stBtDevSet.port_bt);
				int rc = ceSetDDParamValue(handle, IOCTL_SET_CONNECT_INFO, (void *)&stBtDevSet, sizeof(stBtDevSet));
				if (rc<0)
				{
					dlog_error("ceSetDDParamValue():IOCTL_SET_CONNECT_INFO error %d", rc);
					break;
				}
				if (is_dione_base(stBtDevSet.bd_addr))
				{
					dlog_msg("Configuring Dione base station ...");
					stNI_BTConfig btCfg;
					strcpy(btCfg.bcDevPort, "/DEV/BT_SERIAL");
					rc = ceSetNWParamValue(handle, BT_CONFIG, (void*)&btCfg, sizeof(btCfg));
					if (rc != ECE_SUCCESS)
					{
						dlog_error("Bluetooth config returned %d", rc);
						break;
					}
				}

				/*rc = ceSetDDParamValue(handle, IOCTL_EX_SELECT_REMOTE_BT_AP, stBtDevSet.bd_addr, sizeof(stBtDevSet.bd_addr));
				dlog_msg("ceSetDDParamValue - IOCTL_EX_SELECT_REMOTE_BT_AP - %d", rc);*/

				dlog_msg("Device [%s] selected", stBtDevSet.bd_addr);
				// Try bringing interface up, see what happens :)
				int ciapciap = interface_up(CE_DEV_ETH_BT);
				dlog_msg("Iface up 5d", ciapciap);
				return PML_SUCCESS;
			} while (false);
		}
		else
		{
			dlog_error("Not paired with device id %d (max %u)", pair_index, m_paired.size());
		}
	}
	return PML_FAILURE;
}


/* ------------------------------------------------------------------ */
/** Unpair with the selected device from the list
	* @param[in] dev Input device for pair
	* @return 0 on success -1 if fail errno will be set
	*/
int unpair( int pair_index )
{
	int handle = internal_fun::getInterfaceHandle(BT_DEVICE, NULL);
	if (handle != INVALID_INTERFACE_HANDLE)
	{
		if (!m_paired_read)
		{
			int res = read_paired_list();
			if (res != PML_SUCCESS) return res;
		}
		if (pair_index < m_paired.size())
		{
			do
			{
				// do BT pair removal
				ddi_bt_pair removeReq;
				strncpy(removeReq.bd_addr, m_paired[pair_index].bd_addr, sizeof(removeReq.bd_addr));
				strncpy(removeReq.name, m_paired[pair_index].name, sizeof(removeReq.name));
				dlog_msg("Attempting to remove paired device addr: \"%s\", name \"%s\"", removeReq.bd_addr, removeReq.name);
				int rc = ceSetDDParamValue(handle, IOCTL_EX_BT_REMOVE_PAIR, (void *)&removeReq, sizeof(removeReq));
				if (rc < 0)
				{
					dlog_msg("ceSetDDParamValue():IOCTL_EX_BT_REMOVE_PAIR error %d", rc);
					break;
				}
				m_paired.erase(m_paired.begin()+pair_index);
				return PML_SUCCESS;
			}
			while(false);
		}
		else
		{
			dlog_error("Not paired with device id %d (max %u)", pair_index, m_paired.size());
		}
	}
	return PML_FAILURE;
}

/* ------------------------------------------------------------------ */
/** Pair with the selected device from the list
	* @param[in] dev Input device for pair
	* @return 0 on success -1 if fail errno will be set
	*/
int pair( const device& dev, const char pin[], pair_method::_enum_ method )
{
	int handle = internal_fun::getInterfaceHandle(BT_DEVICE, NULL);

	if (handle != INVALID_INTERFACE_HANDLE)
	{
		if (!m_paired_read)
		{
			int res = read_paired_list();
			if (res != PML_SUCCESS) return res;
		}
		do
		{
			for (std::vector<ddi_bt_device>::const_iterator it = m_paired.begin(); it != m_paired.end(); ++it)
			{
				if (internal_fun::conv_bt_addr(it->bd_addr) == dev.address())
				{
					dlog_error("Already paired");
					errno = EEXIST;
					break;
				}
			}

			ddi_bt_pair pairReq;

			strncpy(pairReq.bd_addr, dev.address().c_str(), sizeof(pairReq.bd_addr));
			// 0 - SSP without passkey, 1 - SSP with passkey, 2 - legacy pair
			if (method == pair_method::legacy) pairReq.method = LEGACY_WITH_PASSKEY;
			else if (method == pair_method::ssp) pairReq.method = SSP_WITHOUT_PASSKEY;
			else if (method == pair_method::ssp_passkey) pairReq.method = SSP_WITH_PASSKEY;

			if (is_dione_base(pairReq.bd_addr))
			{
				dlog_alert("Dione base, enforcing pairing method!");
				pairReq.method = 2;
			}

			if (!dev.name().empty())
			{
				size_t copySize = std::min(dev.name().size(), (sizeof pairReq.name)-1);
				strncpy(pairReq.name, dev.name().c_str(), copySize);
				pairReq.name[copySize] = 0;
			}
			else
			{
				pairReq.name[0] = 0;
			}

			pairReq.keycode[0] = 0;

			 if (pin && strlen(pin)) 
				 strncpy(pairReq.keycode, pin, strlen(pin));

			dlog_msg("Attempting to pair device addr: \"%s\", name \"%s\", method=%d", pairReq.bd_addr, pairReq.name, pairReq.method);

			if (pairReq.method == SSP_WITH_PASSKEY)
			{
				dlog_msg("BT keycode - %s", pin);
			}

			int rc = ceSetDDParamValue(handle, IOCTL_BT_PAIR, reinterpret_cast<void*>(&pairReq), sizeof(ddi_bt_pair));

			if (rc < 0)
			{
				dlog_error("ceSetDDParamValue():IOCTL_BT_PAIR error %d", rc);
				break;
			}

			if (pairReq.method == SSP_WITHOUT_PASSKEY)
			{
				unsigned int iResultLen = 0;
				ddi_bt_pair stBtPairDevice;

				memset(&stBtPairDevice, 0x00, sizeof(stBtPairDevice));
				rc = ceGetDDParamValue(handle, IOCTL_BT_GET_PAIR_KEY, sizeof(stBtPairDevice), &stBtPairDevice, &iResultLen);

				dlog_msg("IOCTL_BT_GET_PAIR_KEY:: %d", rc);
				dlog_msg("IOCTL_BT_GET_PAIR_KEY: Device keycode: %s", stBtPairDevice.keycode);

				rc = ceSetDDParamValue(handle, IOCTL_BT_CONFIRM_PAIR, "YES", sizeof("YES"));
				dlog_msg("IOCTL_BT_CONFIRM_PAIR:: %d", rc);
			}

			// ctmp todo write this
			m_paired_read = false;
			set_connected(dev.address());
			return PML_SUCCESS;
		}
		while(false);
	}
	return PML_FAILURE;
}

/* ------------------------------------------------------------------ */
/** 
* Find paired device as an address
* @param[in] device Get paired profiles count
* @param[out] pair_index Paired index
* @return error code
*/
int find_paired( const device &device, int& pair_index )
{
	if (!m_paired_read)
	{
		int res = read_paired_list();
		if (res != PML_SUCCESS) return res;
	}
	int index = 0;
	for (std::vector<ddi_bt_device>::const_iterator it = m_paired.begin(); it != m_paired.end(); ++it, ++index)
	{
		if (internal_fun::conv_bt_addr(it->bd_addr) == device.address())
		{
			pair_index = index;
			return PML_SUCCESS;
		}
	}
	errno = ENODEV;
	return PML_FAILURE;
}
/* ------------------------------------------------------------------ */ 
/** 
	*  Set device as paired defaults
	*	@param[in] pair_index Pairing device index
	*	@return Error code
	*/
int set_paired_default( int pair_index )
{
	if (!m_paired_read)
	{
		int res = read_paired_list();
		if (res != PML_SUCCESS) return res;
	}
	if (pair_index < m_paired.size())
	{
		write_default_paired_id(pair_index);
		return PML_SUCCESS;
	}
	errno = ENODEV;
	return PML_FAILURE;
}

/* ------------------------------------------------------------------ */ 
}	// namespace bluetooth
}	// namespace net
}	//namespace com_verifone_pml
