#include "VxOS.hpp"

#include <cstddef>
#include <cstring>

#include <svc.h>


namespace com_verifone_VxOS
{
	namespace
	{
		struct deviceEventMask
		{
			char const * name;
			long mask;
		};

		deviceEventMask const deviceEventMaskMap[] =
		{
			{ DEV_COM1, EVT_COM1 },
			{ DEV_COM2, EVT_COM2 },
			{ DEV_COM3, EVT_COM3 },
			{ DEV_COM4, EVT_COM4 },
			{ DEV_COM5, EVT_COM5 },
			{ DEV_COM6, EVT_COM6 },
			{ DEV_COM7, EVT_COM7 },
			{ DEV_COM8, EVT_COM8 },
			{ DEV_USBD, EVT_USB_CLIENT } // UNTESTED, may not work!!!
			
		};
		const std::size_t deviceEventMaskMapCount = sizeof(deviceEventMaskMap) / sizeof(deviceEventMaskMap[0]);

	}

	long get_event_bit(int handle)
	{
		long eventMask = ::get_event_bit(handle);

		if (eventMask < 0)
		{
			// OS does not want to tell us so we have to do it on our own..,
			char deviceName[20];
			int result = ::get_name(handle, deviceName);
			if (result < 0)
			{
				eventMask = result;
			}
			else
			{
				eventMask = -1;

				if (deviceName[0] != '\0')
				{
					// we have a proper name - seek it
					// Slow loop with strings comparison
					for (std::size_t idx = 0; idx < deviceEventMaskMapCount; ++idx)
					{
						deviceEventMask const & entry = deviceEventMaskMap[idx];
						if (std::strcmp(entry.name, deviceName) == 0)
						{
							eventMask = entry.mask;
							break;
						}
					}
				}
			}
		}

		return eventMask;
	}
}

