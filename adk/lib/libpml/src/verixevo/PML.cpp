/*****************************************************************************
 * 
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file        PML.cpp
 *
 * @author      Kamil Pawlowski
 * 
 * @brief       Internal handler for Events
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C Coding  
 *              Standard 1.0.0 
 */

#include <libpml/pml.h>
#include <libipc/ipc_handle.h>

#include "PMLInternals.hpp"
#include "libver.h"

char ToUpper (char c) { return ::toupper(c); }
char ToLower (char c) { return ::tolower(c); }




using namespace com_verifone_pml_events_internals;


namespace com_verifone_pml
{
	int event_open()
	{
		return PMLEventHandler::getInstance()->create();
	}

	int event_close(const int handle)
	{
		//return PMLEventHandler::getInstance()->close(handle);
		PMLEventHandler * ptr = PMLEventHandler::getInstance();
		if (ptr->is_handle_valid(handle)) return ptr->close(handle);
		return -1;
	}

	int event_ctl(const int handle, const ctl::op_t operation, const event_item & event)
	{
		eventset_t events;
		events.push_back(event);
		return event_ctl(handle, operation, events);
	}

	int event_ctl(const int handle, const ctl::op_t operation, const eventset_t & events)
	{
		PMLEventHandler * ptr = PMLEventHandler::getInstance();
		if (ptr->is_handle_valid(handle)) return ptr->ctl(handle, operation, events);
		return -1;
	}

	int event_ctl( int handle, const ctl::op_t operation, const com_verifone_ipc::ipc_handle& ipchwnd )
	{
		PMLEventHandler * ptr = PMLEventHandler::getInstance();
		int add_handle = ipchwnd.get_raw();
		if (ptr->is_handle_valid(handle) && ptr->is_handle_valid(add_handle)) return ptr->ctl(handle, operation, add_handle);
		return -1;
	}

	int event_ctl( int handle, const ctl::op_t operation, int add_handle )
	{
		PMLEventHandler * ptr = PMLEventHandler::getInstance();
		if (ptr->is_handle_valid(handle))
		{
			if (ptr->is_handle_valid(add_handle)) return ptr->ctl(handle, operation, add_handle);
			// dlog_alert("Passed handle, no idea what to do with it...");
			event_item event;
			event.event_id = events::comm;
			event.evt.com.fd = add_handle;
			event.evt.com.flags = 0;
			return event_ctl(handle, operation, event);
		}
		return -1;
	}

	int event_wait(int handle, eventset_t & events, long timeout)
	{
		PMLEventHandler * ptr = PMLEventHandler::getInstance();
		if (ptr->is_handle_valid(handle)) return ptr->wait(handle, events, timeout);
		return -1;
	}

	int event_raise(const char * name, const event_item & event)
	{
		return PMLEventHandler::getInstance()->raise(name, event);
	}
	int event_raise(pid_t tid, const event_item & event)
	{
		return PMLEventHandler::getInstance()->raise(tid, event);
	}

	int pml_init(const char * name)
	{
		com_verifone_libver::register_library();
		return PMLEventHandler::getInstance()->init(name);
	}
	int pml_deinit(const char * name)
	{
		return PMLEventHandler::getInstance()->deinit(name);
	}
	
	namespace appver
	{
		int add_app_version( const char version[] )
		{
			return PMLEventHandler::getInstance()->set_version(version);
		}
		
		void register_library( const char libname[], const char version[] )
		{
			PMLEventHandler::getInstance()->register_library(libname, version);
		}
		
		void get_apps_list( applist_t &applist )
		{
			PMLEventHandler::getInstance()->get_apps_list(applist);
		}
		
		void get_lib_list( applist_t& liblist, const char *appname /* = NULL */ )
		{
			PMLEventHandler::getInstance()->get_lib_list(liblist, appname);
		}
		
		std::string get_app_version()
		{
			return PMLEventHandler::getInstance()->get_our_version();
		}
	}
}


