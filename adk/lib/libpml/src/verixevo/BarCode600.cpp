#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 *
 * Copyright (C) 2008 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file          BarCode600.cpp
 *
 * @author        Arkadiusz_A1
 *
 * @brief         Definition of Vx600 bar code reader class
 *
 * @remarks       This file should be compliant with Verifone EMEA R&D C Coding Standard 1.0.0
 */
/***************************************************************************
 * Includes
 **************************************************************************/
#include <string.h>
#include <stdio.h>
#include <svc.h>
#include <errno.h>
#include <liblog/logsys.h>
#include "BarCode600.h"
#include <libpml/pml.h>
//#include <new>          // std::bad_alloc

/***************************************************************************
 * Using
 **************************************************************************/
/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
namespace com_verifone_barcode
{
	using namespace com_verifone_pml;
	

	/**
	 * @addtogroup Comm
	 * @{
	 */

	CBarCodeReader * CBarCodeReader::pInstance = NULL;

	CBarCodeReader::CBarCodeReader():
        inHndl(-1), m_mode(mode_oneshot)
	{
	}

	CBarCodeReader * CBarCodeReader::getInstance()
	{
		if(pInstance == NULL)
		{
			pInstance = new CBarCodeReader();
		}

		return pInstance;
	}

	void CBarCodeReader::deleteInstance()
	{
		if(pInstance != NULL)
		{
			delete pInstance;
			pInstance = NULL;
		}
	}

	void CBarCodeReader::Init()
	{

	}

	int CBarCodeReader::Open()
	{
		int inRet = -1;
		if (inHndl != -1)
		{
			inRet = 1;
		}
		else
		{
			inHndl = open(DEV_BAR, O_RDWR);
			
			if(inHndl < 0)
			{
				dlog_msg("Failed to open %s: %d errno %d", DEV_BAR, inHndl, errno);
			}
			else
			{
				dlog_msg("BarCode device %s opened: %d", DEV_BAR, inHndl);

				char buf[2];
				buf[0] = 0x11;
				buf[1] = 0x01;
				int inWr = write(inHndl, buf, 2);
				dlog_msg("written: %d bytes to %d", inWr, inHndl);
				if(errno == ENOSPC)
				{
					dlog_error("Battery charge is not enough to enable the barcode scanner");
				}
				else
				{
					inRet = 1;
				}
			}
		}
		
		return inRet;
	}

	void CBarCodeReader::Close()
	{
		if(inHndl >= 0)
		{
			close(inHndl);
			inHndl = -1;
		}
	}

	int CBarCodeReader::Read(std::string & buf)
	{
		char bufLocal[128];
		size_t bufSize = sizeof(bufLocal);
		int inRet = 0;
		do
		{
			inRet = read(inHndl, bufLocal, bufSize);
			if (inRet > 0)
			{
				buf.append(bufLocal, inRet);
				SVC_WAIT(50);
			}
		} while (inRet > 0);
		
		dlog_msg("read: %d from %d", inRet, inHndl);
		
		return buf.size();
	}

	void CBarCodeReader::setMode(CBarCodeReader::t_mode mode)
    {
        m_mode = mode;
    }

	CBarCodeReader::t_mode CBarCodeReader::getMode() const
    {
        return m_mode;
    }

	bool CBarCodeReader::isOpened() const
    {
        return (inHndl >= 0);
    }

	/***************************************************************************
	 * Exportable function definitions
	 **************************************************************************/


	/***************************************************************************
	 * Module namspace: end
	 **************************************************************************/
}
