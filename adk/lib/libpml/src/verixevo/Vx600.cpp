#include <svc.h>
#include <stdio.h>
#include <math.h>

#include <string>

#include <liblog/logsys.h>

#include <libpml/vx600.h> // pml header
#include "BarCode600.h"

namespace pml600
{
	vx600::vx600(): initialized(false)
	{
		char modelNo[13];
		memset(modelNo,0,sizeof(modelNo));
		SVC_INFO_MODELNO(modelNo);
		// dlog_msg("Model no %s",modelNo);

		if ((strstr(modelNo,"Vx60")) || (strstr(modelNo,"VX60")))
		{
			initialized = true;
		}
	}

	vx600::~vx600()
	{
		// nothing
	}
	
	void vx600::enableKeyboard()
	{
		if (initialized)
		{
			//NKJT - GEN2.5 : Must Wake the Keypad up, as it is powered off by default
			SVC_WAIT(200);
			dlog_msg("Wake keyboard up for Vx600");
			cs_set_sleep_state(0);
			#if 0
			int handle = open(DEV_COM1E,0);
			if (handle>=0)
			{
				dlog_msg("IAP status: %d", get_iap_state(handle));
				char buf[10];
				memset(buf, 0, sizeof(buf));
				if (iap_get_keypad_info(handle, buf) == 0)
				{
					dlog_msg("IAP get keypad info: %02X, %02X", buf[0], buf[1]);
				}
				close(handle);
			}
			#endif
		}
	}

	void vx600::disableKeyboard()
	{
		if (initialized)
		{
			//NKJT - GEN2.5 : Must Wake the Keypad up, as it is powered off by default
			dlog_msg("keyboard sleep for Vx600");
			cs_set_sleep_state(1);
			#if 0
			int handle = open(DEV_COM1E,0);
			if (handle>=0)
			{
				dlog_msg("IAP status: %d", get_iap_state(handle));
				char buf[10];
				memset(buf, 0, sizeof(buf));
				if (iap_get_keypad_info(handle, buf) == 0)
				{
					dlog_msg("IAP get keypad info: %02X, %02X", buf[0], buf[1]);
				}
				close(handle);
			}
			#endif
		}
	}

	/************************************************************************************************************************************************/
	/* Vx600 barcode reader */
	/************************************************************************************************************************************************/
	int vx600::BarCodeOpen()
	{
		if (initialized)
		{
			com_verifone_barcode::CBarCodeReader * pI = com_verifone_barcode::CBarCodeReader::getInstance();
			if(pI && (pI->Open() >= 0))
			return 1;
		}
		return 0;
	}

	void vx600::BarCodeClose()
	{
		if (initialized)
		{
			com_verifone_barcode::CBarCodeReader * pI = com_verifone_barcode::CBarCodeReader::getInstance();
			if(pI)
			{
					pI->Close();
			}
		}
	}

	int vx600::BarCodeRead(std::string & buf)
	{
		int inRet = RES_BARCODE_NOOPERATION;
		if (initialized)
		{
			com_verifone_barcode::CBarCodeReader * pI = com_verifone_barcode::CBarCodeReader::getInstance();
			if(pI)
			{
				pI->Read(buf);
				inRet = RES_BARCODE_DATAREADY;
			}
			else
			{
				dlog_error("Unable to get CBarCodeReader instance!");
				inRet = RES_BARCODE_NOMEMORY;
			}
		}
		return inRet;
	}

	void vx600::BarCodeSetMonitoringMode(bool val)
	{
		if (initialized)
		{
			com_verifone_barcode::CBarCodeReader * pI = com_verifone_barcode::CBarCodeReader::getInstance();
			if(pI)
			{
					(val == true) ? pI->setMode(com_verifone_barcode::CBarCodeReader::mode_monitoring) : pI->setMode(com_verifone_barcode::CBarCodeReader::mode_oneshot);
			}
		}
	}
	
	bool vx600::BarCodeIsOpened()
	{
		if (initialized)
		{
			com_verifone_barcode::CBarCodeReader * pI = com_verifone_barcode::CBarCodeReader::getInstance();
			return pI->isOpened();
		}
		return false;
	}
	
	bool vx600::BarCodeIsMonitoringMode()
	{
		if (initialized)
		{
			com_verifone_barcode::CBarCodeReader * pI = com_verifone_barcode::CBarCodeReader::getInstance();
			return (pI->getMode() == com_verifone_barcode::CBarCodeReader::mode_monitoring);
		}
		return false;
	}
	
	void vx600::BarCodeBeep(int B1Freq, int B1Dur, int Pause, int B2Freq, int B2Dur)
	{
		if (initialized)
		{
			const int MAX_PAUSE = 500;
			if(Pause > MAX_PAUSE) Pause = MAX_PAUSE;

			if(B1Freq > 0)
			{
				long n1 = 40L * log10(static_cast<double>(B1Freq)/55);
				dlog_msg("B1Freq:%d, n1:%ld", B1Freq, n1);
				sound(n1, B1Dur);
				SVC_WAIT(Pause);
			}

			if(B2Freq > 0)
			{
				long n2 = 40L * log10(static_cast<double>(B2Freq)/55);
				dlog_msg("B2Freq:%d, n2:%ld", B2Freq, n2);
				sound(n2, B2Dur);
			}
		}
	}

} //namespace

