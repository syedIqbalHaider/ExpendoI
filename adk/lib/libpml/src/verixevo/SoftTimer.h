#ifndef EMEA_SOFT_TIMER_H
#define EMEA_SOFT_TIMER_H

#ifndef __cplusplus 
#  error "This file is for C++ only!"
#endif 

/***************************************************************************
** 
 *  
 * Copyright (C) 2006 by VeriFone, Inc. 
 * 
 * All rights reserved.  No part of this software may be reproduced, 
 * transmitted, transcribed, stored in a retrieval system, or translated 
 * into any language or computer language, in any form or by any means, 
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise, 
 * without the prior written permission of VeriFone, Inc.  
 *        
 **************************************************************************/ 
 
/** 
 * @file   SoftTimer.h
 * 
 * @author  Tomasz Saniawa
 *  
 * @brief   Library for software timers over single hardware timer
 * 
 * @remarks  This file should be compliant with Verifone EMEA R&D C++ Coding   
 *           Standard 1.0.x  
 */ 
 
/*************************************************************************** 
 * Includes 
 **************************************************************************/ 
#include "mutex.hpp"

#include <svc.h>

#include <limits>
#include <map>
#include <vector>
#include <list>

//#include "SoftTimerC.h"


/*************************************************************************** 
 * Module namespace: begin 
 **************************************************************************/ 

namespace com_verifone_SoftTimer
{
/** 
 * @addtogroup SoftTimer
 * @{  
 */ 

	/** 
	 * Data type definitions 
	 **/ 

	typedef unsigned long SysEventMask_t;
	typedef int TimerId_t;
	

	namespace Internal
	{
		typedef int TimerHandle_t;
		typedef long TimeDiff_t;
		
		class TimeStamp
		{
		public:
			typedef unsigned long TimerCounter_t;	// to match the type returned by read_ticks()
		
		private:
			//const TimeDiff_t maxDiffInPast = std::numeric_limits<TimeDiff_t>::max() / 4;
		
		public:
			TimeStamp(TimerCounter_t ticks = 0)
				: value_(ticks)
			{}
			
			TimerCounter_t get() const { return value_; }
			bool operator == (TimeStamp const & rhs) const { return value_ == rhs.value_; }
			
			// NOTE: comparison is tricky as the counter rolls over!
			// Assumption is that the t1 is in the past if (t2-t1) < (MAX_RANGE/2)
			bool operator <= (TimeStamp const & rhs) const 
			{
				TimeDiff_t diff = value_ - rhs.value_;
				return diff <= 0;
			}
			
			bool operator < (TimeStamp const & rhs) const 
			{
				TimeDiff_t diff = value_ - rhs.value_;
				return diff < 0;
			}

			bool operator > (TimeStamp const & rhs) const 
			{
				TimeDiff_t diff = value_ - rhs.value_;
				return diff > 0;
			}
			
			static TimeStamp now() { return TimeStamp(::read_ticks()); }
			
			TimeDiff_t operator -(TimeStamp const & rhs) const 
			{ 
				TimeDiff_t diff = value_ - rhs.value_;
				return diff;
			}
			
			TimeStamp & operator+=(TimeDiff_t diff)
			{
				value_ += diff;
				return *this;
			}
			
		private:
			TimerCounter_t value_;
		};
		typedef TimeStamp TimeStamp_t;
		
		
		/**
		 * @brief
		 *		Helper class to hold state for single software timer
		 *
		 * @note
		 *		This class is internal to SoftwareTimers
		 */
		class TimerState
		{
		public:

			 /** 
			  * @brief 
			  *       TimerState constructor
			  */ 
			TimerState()
				: m_fireTime(0), m_repeatTime(0), m_timer_runs(false)
			{}
			
		
			/** 
			  * @brief 
			  *         Sets the timer parameters
			  * 
			  * @param[in]   milliseconds Time to fire in milliseconds
			  * @param[in]  repeat  if true the timer will repeat every milliseconds, otherwise it will be one-shot 
			  */ 
			void setTimer(TimeDiff_t milliseconds, bool repeat = false);
			
			/** 
			  * @brief 
			  *         Stops (deactivates timer)
			  */ 
			void stopTimer()
			{
				m_fireTime = 0;
				m_timer_runs = false;
			}

			/** 
			  * @brief 
			  *         Get the time when timer should fire
			  * 
			  * @return    Fire time in system ticks
			  */ 
			TimeStamp_t getFireTime() const { return m_fireTime; }

			/** 
			  * @brief 
			  *         Returns whether timer is active   
			  * 
			  * @return    true if timer is active
			  */ 
			bool isRunning() const { return m_timer_runs; }


			/** 
			  * @brief 
			  *         Updates the timer for the next shot i.e. updates fire time to next slot for 
			  *			repeatable timers and deactivates one-shot ones
			  * 
			  * @return    returns activity status
			  */ 
			bool update()
			{
				if (m_repeatTime > 0)
				{
					m_fireTime += m_repeatTime;
				}
				else
				{
					m_timer_runs = false;
				}
				
				return m_timer_runs;
			}


			/** 
			  * @brief 
			  *         Checks whether timer should fire already
			  * 
			  * @param[in]   now	System ticks in present moment.  Used to avoid comparing with slightly different ticks every time.
			  * 
			  * @return    true if timer should or was fired 
			  * 
			  * @note    The timer is considered to be in the past also if it is not active.
			  */ 
			bool isFired(TimeStamp_t now = TimeStamp_t::now()) const;
			
		private:
			TimeStamp_t m_fireTime;
			TimeDiff_t m_repeatTime;
			bool m_timer_runs;
		};
	}
	


	typedef std::vector<TimerId_t> timer_id_list_t;
	
	class SoftTimerId
	{
	// Types
	public:
		typedef Internal::TimeStamp_t TimeStamp_t;

		// Helper list of running timers ordered by a fire time (nearest time to fire is first)
		typedef struct
		{
			TimeStamp_t fireTime;
			TimerId_t timerId;
		} timer_order_info_t;
		typedef std::list<timer_order_info_t> timer_fire_list_t;
		
		// Map of timer id to timer state
		typedef struct
		{
			Internal::TimerState state;
			timer_fire_list_t::iterator fireIterator;
		} timer_context_t;
		typedef std::map<TimerId_t, timer_context_t> timer_collection_t;
		
		// Map of taskId to system timer id
		typedef std::map<int, int> sys_timer_collection_t;

		
	// Const
	public:
		static const int INVALID_ID = -1;
		
		
	// Methods
	public:
		static SoftTimerId * getInstance();

		TimerId_t setTimer(long milliseconds, bool repeat = false);
		int changeTimer(TimerId_t timerID, long milliseconds, bool repeat = false);
		void clrTimer(TimerId_t timerId);
		void stopTimer(TimerId_t timerId);

		bool isValid(TimerId_t timerId) const;
		bool isRunning(TimerId_t timerId);
		bool isTimerFired(TimerId_t timerId);
		bool updateTimer(TimerId_t timerId);
		
		timer_id_list_t getFiredTimers();
		
	
		// Call setSysTimer() before wait_event() and clrSysTimer() after.
		// NOTE: it is needed as the timers may be exchanged between tasks
		bool setSysTimer(timer_id_list_t timerList);
		void clrSysTimer();
		
		// NOTE: For now only one timer event
		SysEventMask_t getSysEventMask() { return EVT_TIMER; }
		
	private:
		SoftTimerId();
		SoftTimerId(const SoftTimerId &); // noncopyable
		SoftTimerId & operator=(const SoftTimerId &); // noncopyable
		
		//TimerMask_t getTimerMask(TimerId_t timerId);
		TimerId_t createNewId() const;
		timer_collection_t::iterator getTimerIterator(TimerId_t timerId);
		timer_collection_t::mapped_type * getTimerObj(TimerId_t timerId);

		void insertToFireList(TimerId_t timerId, timer_collection_t::mapped_type & timerCtx);
		void removeFromFireList(timer_collection_t::mapped_type & timerCtx);
		
		void clrSysTimer(int taskId);
		
	// Attributes
	private:
		timer_fire_list_t fireList_;
		timer_collection_t timerCollection_;
		mutable com_verifone_VIPAParser::Mutex mutex_;
		sys_timer_collection_t sysTimerCollection_;
		
	private:
		static SoftTimerId * ptr;
	};

/** 
 * @} 
 */ 
}


/***************************************************************************  
 * Module namespace: end   
**************************************************************************/ 
#endif /* EMEA_SOFT_TIMER_H */

