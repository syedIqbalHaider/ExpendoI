/*****************************************************************************
 * 
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file        PMLClasses.hpp
 *
 * @author      Kamil Pawlowski
 * 
 * @brief       Internal classes implementing events on Verix (Trident) devices
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C Coding  
 *              Standard 1.0.0 
 */

#include <pml/pml.h>

namespace com_verifone_pml_events
{

	class PMLGenericEvent: public event_item
	{
		public:
			PMLGenericEvent(const com_verifone_pml::event_item & evt);
			virtual ~PMLGenericEvent();
			virtual int check_event(long event) = 0;
			long get_event() const 
				{ return m_event; }
			friend bool operator==(const PMLGenericEvent & lhs, const PMLGenericEvent & rhs) const
				{ return lhs.compare(rhs); }
		protected:
			virtual bool compare(PMLGenericEvent & rhs) const
				{ return m_event == rhs.m_event; }
		private:
			long m_event;
	};
	
	class PMLEvent: public PMLGenericEvents
	{
		public:
			PMLEvent(const com_verifone_pml::event_item & evt);
			virtual ~PMLEvent();
			virtual int check_event(long event);
		protected:
			virtual bool compare(PMLGenericEvent & rhs) const;
	};
	
	class PMLEventHandle: public PMLGenericEvents
	{
		public:
			PMLEventHandle(const com_verifone_pml::event_item & evt);
			virtual ~PMLEventHandle();
			virtual int check_event(long event);
		protected:
			virtual bool compare(PMLGenericEvent & rhs) const;
		private:
			int m_handle;
	};

} // namespace
