#include <errno.h>
#include <libpml/pml_os.h>
#include <libpml/pml_os_security.h>


namespace com_verifone_pml
{
	namespace pml_os
	{

		// Function is not supported
		int get_counter(int /*index*/, struct OS_COUNTER * /*counter*/)
		{
			errno = ENXIO;
			return -1;
		}

		int is_counter_defined(int /*index*/)
		{
			errno = ENXIO;
			return -1;
		}

		int is_custom_counter_defined(int /*index*/, const char * /*name*/)
		{
			errno = ENXIO;
			return -1;
		}

		int define_counter(int /*index*/, const struct OS_COUNTER * /*counter*/)
		{
			errno = ENXIO;
			return -1;
		}

		int set_counter(int /*index*/, const struct OS_COUNTER * /*counter*/)
		{
			errno = ENXIO;
			return -1;
		}

		int increase_counter(int /*index*/, int /*count*/)
		{
			errno = ENXIO;
			return -1;
		}
		int decrease_counter(int /*index*/, int /*count*/)
		{
			errno = ENXIO;
			return -1;
		}
	} // namespace pml_os

	namespace needham_schroeder {
		int init(int /*mode*/, std::string & /*output*/)
		{
			errno = ENXIO;
			return -1;
		}
		int resolve(int /*mode*/, const std::string & /*input*/, std::string & /*output*/)
		{
			errno = ENXIO;
			return -1;
		}
		int finalize(int /*mode*/, const std::string & /*input*/, std::string & /*output*/)
		{
			errno = ENXIO;
			return -1;
		}
		int verify(int /*mode*/, const std::string & /*input*/)
		{
			errno = ENXIO;
			return -1;
		}
		int get_session_key(int /*mode*/, OPERATION_TYPE /*key_type*/, std::string & /*key*/)
		{
			errno = ENXIO;
			return -1;
		}
		int load_encrypted_block(int /*mode*/, OPERATION_TYPE /*key_type*/, const std::string & /*block*/)
		{
			errno = ENXIO;
			return -1;
		}
		int ux_set_ars_password(int /*mode*/, int /*password_id*/, const std::string & /*password*/)
		{
			errno = ENXIO;
			return -1;
		}
		int ux_reset_ars(int /*mode*/, int /*password_id*/, const std::string & /*password*/)
		{
			errno = ENXIO;
			return -1;
		}
		int get_certificate(int /*pki_type*/, int /*cert_level*/, std::string & /*cert*/)
		{
			errno = ENXIO;
			return -1;
		}
		int store_certificate(int /*pki_type*/, int /*cert_level*/, const std::string & /*cert*/)
		{
			errno = ENXIO;
			return -1;
		}
		int clear_certificate_chain(int /*pki_type*/)
		{
			errno = ENXIO;
			return -1;
		}
	} // namespace needham_schroeder
} // namespace com_verifone_pml

