#include <svc.h>
#include <errno.h>
#include <string.h>

#include <liblog/logsys.h>

#include "VxSharedMem.hpp"
#include "VxSharedMemMgr.hpp"

// #define EXTRA_LOGGING

using namespace com_verifone_VxSharedMemoryMgr;

namespace com_verifone_VxSharedMemory
{
	namespace
	{
		static const std::string SHARED_MEMORY_NAME_APPS("PML_APPS");
		static const std::string SHARED_MEMORY_NAME_VERSION("PML_VERSIONS");
		static const std::string SHARED_MEMORY_USER_VALUES("PML_USERVAL");
		static const int MAX_NAME = 32; // includes terminating '\0' !
		struct task_structure
		{
			char task_name[MAX_NAME];
			int task_id;
			task_structure(const char * const name, int tid): task_id(tid)
				{ strncpy(task_name, name, MAX_NAME); }
		};
		
		task_structure * get_pointer(void * mem)
		{
			void * tmp_ptr = reinterpret_cast<void *>(static_cast<char *>(mem) + sizeof(int));          // skip number of elements
			return static_cast<task_structure *>(tmp_ptr);
		}
		const task_structure * get_pointer(const void * mem)
		{
			const void * tmp_ptr = reinterpret_cast<const void *>(static_cast<const char *>(mem) + sizeof(int));    // skip number of elements
			return static_cast<const task_structure *>(tmp_ptr);
		}
		
		const task_structure * getTaskPointer(const char * const name, const void * mem, int count)
		{
			const task_structure * ptr = get_pointer(mem);
			for (int i=0; i<count; ++i)
			{
				dlog_msg("Checking task %s", ptr->task_name);
				if (!strncmp(ptr->task_name, name, MAX_NAME))
				{
					return ptr;
				}
				++ptr;
			}
			return 0;
		}
		
		int getTaskId(const char * const name, const void * mem, int count)
		{
			const task_structure * ptr = getTaskPointer(name, mem, count);
			if (ptr)
			{
				dlog_msg("Task %s is already registered", ptr->task_name);
				return ptr->task_id;
			}
			return -1;
		}
		
		bool appendToTheList(const char * const name, void * const mem, int count)
		{
			task_structure * ptr = get_pointer(mem);
			ptr += count;
			task_structure newStruct(name, ::get_task_id());
			*ptr = newStruct;
			int * pInt = static_cast<int *>(mem);
			*pInt = ++count;
			return true;
		}
		
		bool deleteFromTheList(void * const mem, void * const ptr, int count)
		{
			//int moveSize = count - ((char *)ptr-(char *)mem-sizeof(int)) * sizeof(task_structure);
			int skipCnt = (static_cast<const char *>(ptr) - (static_cast<const char *>(mem) + sizeof(int))) / sizeof(task_structure);
			dlog_msg("Moving element - skipping %d first", skipCnt);
			int moveSize = (count - skipCnt - 1) * sizeof(task_structure); // -1 because we have to clear the last one
			dlog_msg("Moving %d bytes", moveSize);
			
			memmove(ptr, static_cast<char *>(ptr) + sizeof(task_structure), moveSize);
			memset(static_cast<char *>(ptr) + moveSize, 0, sizeof(task_structure));
			int * pInt = static_cast<int *>(mem);
			*pInt = --count;
			return true;
		}
		
		int * get_memory(void * mem, int task_id)
		{
			int count = *(static_cast<int *>(mem));
			int * pTaskMem = static_cast<int *>(mem) + sizeof(int);          // skip number of elements
			for (int i = 0; i < count; ++i)
			{
				if (*pTaskMem == task_id)
				{
					return pTaskMem+1;
				}
				pTaskMem += 3;
			}
			return NULL;
		}
	}

	bool add_name(const char * const name)
	{
		CSharedMemoryManager * ptr = CSharedMemoryManager::getInstance(SHARED_MEMORY_NAME_APPS);
		void * mem = 0;
		bool res = false;
		int result;
		do
		{
			result = ptr->get_shared_memory(&mem);
			if (result < 0)
			{
				dlog_error("Cannot get shared memory!");
				break;
			}
			// ok, got shared memory
			int count = *(static_cast<int *>(mem));
			dlog_msg("We have %d tasks", count);
			void * taskPtr = mem;
			if (getTaskId(name, taskPtr, count) != -1)
			{
				dlog_error("Name already exists!");
			}
			else
			{
				appendToTheList(name, mem, count);
				dlog_msg("Appended %s to the list!", name);
				// dlog_hex(mem, (count+1)*sizeof(task_structure)+sizeof(int), "MEMORY");
				res = true;
			}
		} while(false);
		#ifdef EXTRA_LOGGING
		dlog_hex(mem, 128, "FIRST SHARED BYTES");
		#endif
		result = ptr->close_shared_memory(mem);
		dlog_msg("Shared memory free result %d", result);
		dlog_msg("Returning %d", res);
		return res;
	}
	
	bool delete_name(const char * const name)
	{
		CSharedMemoryManager * ptr = CSharedMemoryManager::getInstance(SHARED_MEMORY_NAME_APPS);
		void * mem = 0;
		bool res = false;
		int result;
		do
		{
			result = ptr->get_shared_memory(&mem);
			if (result < 0)
			{
				dlog_error("Cannot get shared memory!");
				break;
			}
			// ok, got shared memory
			int count = *(static_cast<int *>(mem));
			dlog_msg("We have %d tasks", count);
			task_structure * taskPtr = const_cast<task_structure *>(getTaskPointer(name, mem, count));
			if ( taskPtr != 0)
			{
				deleteFromTheList(mem, taskPtr, count);
				res = true;
				// dlog_hex(mem, count*sizeof(task_structure)+sizeof(int), "MEMORY");
			}
			else
			{
				dlog_error("Name (%s) doesn't exist!", name);
			}
		} while(false);
		result = ptr->close_shared_memory(mem);
		dlog_msg("Shared memory free result %d", result);
		dlog_msg("Returning %d", res);
		return res;
	}
	
	int get_task_id(const char * const name)
	{
		CSharedMemoryManager * ptr  = CSharedMemoryManager::getInstance(SHARED_MEMORY_NAME_APPS);
		void * mem = 0;
		int id = -1;
		int result;
		do
		{
			result = ptr->get_shared_memory(&mem);
			if (result < 0)
			{
				dlog_error("Cannot get shared memory!");
				break;
			}
			// ok, got shared memory
			#ifdef EXTRA_LOGGING
			dlog_hex(mem, 128, "FIRST SHARED BYTES");
			#endif
			int count = *(static_cast<int *>(mem));
			void * taskPtr = mem;
			id = getTaskId(name, taskPtr, count);
		} while(false);
		result = ptr->close_shared_memory(mem);
		dlog_msg("Shared memory free result %d", result);
		dlog_msg("Task ID %d (-1 means not found)", id);
		return id;
	}
	
	int get_all_tasks_ids(id_set_t & id_list, bool exclude_ourself /* = true */)
	{
		CSharedMemoryManager * ptr  = CSharedMemoryManager::getInstance(SHARED_MEMORY_NAME_APPS);
		void * mem = 0;
		int result;
		do
		{
			result = ptr->get_shared_memory(&mem);
			if (result < 0)
			{
				dlog_error("Cannot get shared memory!");
				break;
			}
			// ok, got shared memory
			int count = *(static_cast<int *>(mem));
			task_structure * ptr = get_pointer(mem);
			dlog_msg("We have %d items", count);
			for (int i = 0; i < count; ++i, ++ptr)
			{
				if (exclude_ourself && (ptr->task_id != ::get_task_id()))
				{
					dlog_msg("Task %s, id %d", ptr->task_name, ptr->task_id);
					id_list.push_back(ptr->task_id);
				}
				else
				{
					dlog_msg("Skipping ourselves (task %s, id %d)", ptr->task_name, ptr->task_id);
				}
			}
		} while(false);
		result = ptr->close_shared_memory(mem);
		dlog_msg("Shared memory free result %d", result);
		dlog_msg("Found %d task IDs", id_list.size());
		return id_list.size();
	}
	
	
	
	namespace internal_versioning
	{
		static const int NAME_SIZE = 15;
		static const int VER_SIZE = 15;
		typedef struct _ver
		{
			char name[NAME_SIZE];
			char libname[NAME_SIZE];
			char ver[VER_SIZE];
			_ver(const char *_n, const char * _l, const char * _v)
			{
				memset(name, 0, sizeof(name));
				if (_n) strncpy(name, _n, sizeof(name));
				memset(libname, 0, sizeof(libname));
				if (_l) strncpy(libname, _l, sizeof(libname));
				memset(ver, 0, sizeof(ver));
				if (_v) strncpy(ver, _v, sizeof(ver));
			}
			bool operator==(const _ver & rhs) const
			{
				return !strcmp(name, rhs.name) && !strcmp(libname, rhs.libname) && !strcmp(ver, rhs.ver);
			}
		} VerStruct;
		typedef std::vector<VerStruct> Versions;

		VerStruct * get_pointer(void * mem)
		{
			void * tmp_ptr = reinterpret_cast<void *>(static_cast<char *>(mem) + sizeof(int));          // skip number of elements
			return static_cast<VerStruct *>(tmp_ptr);
		}
		const VerStruct * get_pointer(const void * mem)
		{
			const void * tmp_ptr = reinterpret_cast<const void *>(static_cast<const char *>(mem) + sizeof(int));    // skip number of elements
			return static_cast<const VerStruct *>(tmp_ptr);
		}

		int deserialize_versions(const void * mem, Versions &apps)
		{
			int count = *(static_cast<const int *>(mem));
			const VerStruct * ptr = get_pointer(mem);
			dlog_msg("We have %d items", count);
			for (int i = 0; i < count; ++i, ++ptr)
			{
				apps.push_back(*ptr);
			}
			#ifdef EXTRA_LOGGING
			dlog_hex(mem, sizeof(VerStruct)*count+sizeof(int), "DATA");
			#endif
			return apps.size();
		}
		
		int serialize_versions(void * mem, const Versions & apps)
		{
			VerStruct * ptr = get_pointer(mem);
			dlog_msg("Serializing %d items", apps.size());
			for (Versions::const_iterator it = apps.begin(); it != apps.end(); ++it)
			{
				VerStruct item = *it;
				*ptr = item;
				++ptr;
			}
			int * pInt = static_cast<int *>(mem);
			*pInt = apps.size();
			#ifdef EXTRA_LOGGING
			dlog_hex(mem, sizeof(VerStruct)*apps.size()+sizeof(int), "DATA");
			#endif
			return *pInt;
		}
		
		int find_version(const Versions & apps, const VerStruct & ver)
		{
			int index = 0;
			for (Versions::const_iterator it = apps.begin(); it != apps.end(); ++it)
			{
				if (ver == *it) return index;
				++index;
			}
			return -1;
		}
	}
	
	int set_version(const char name [], const char version [])
	{
		using namespace internal_versioning;
		assert(name != NULL);
		assert(version != NULL);
		CSharedMemoryManager * ptr  = CSharedMemoryManager::getInstance(SHARED_MEMORY_NAME_VERSION);
		void * mem = 0;
		int result = -1;
		int res = -1;
		do
		{
			result = ptr->get_shared_memory(&mem);
			if (result < 0)
			{
				dlog_error("Cannot get shared memory!");
				break;
			}
			// ok, got shared memory
			dlog_msg("Setting version %s for app name %s", version, name);
			Versions apps;
			if (deserialize_versions(mem, apps) >= 0)
			{
				VerStruct ver(name, 0, version);
				int index = find_version(apps, ver);
				if (index == -1)
				{
					apps.push_back(ver);
				}
				else
				{
					apps[index] = ver;
				}
				if (serialize_versions(mem, apps) < 0)
				{
					dlog_error("Cannot serialize!");
					break;
				}
				res = 0;
			}
		} while(false);
		result = ptr->close_shared_memory(mem);
		dlog_msg("Shared memory free result %d", result);
		return res;
	}
	
	int register_library( const char name[], const char libname[], const char version[] )
	{
		using namespace internal_versioning;
		assert(name != NULL);
		assert(version != NULL);
		CSharedMemoryManager * ptr  = CSharedMemoryManager::getInstance(SHARED_MEMORY_NAME_VERSION);
		void * mem = 0;
		int result = -1;
		int res = -1;
		do
		{
			result = ptr->get_shared_memory(&mem);
			if (result < 0)
			{
				dlog_error("Cannot get shared memory!");
				break;
			}
			// ok, got shared memory
			dlog_msg("Registering library %s version %s for app %s", libname, version, name);
			Versions apps;
			if (deserialize_versions(mem, apps) >= 0)
			{
				VerStruct ver(name, libname, version);
				int index = find_version(apps, ver);
				if (index == -1)
				{
					apps.push_back(ver);
				}
				else
				{
					apps[index] = ver;
				}
				if (serialize_versions(mem, apps) < 0)
				{
					dlog_error("Cannot serialize!");
					break;
				}
				res = 0;
			}
		} while(false);
		result = ptr->close_shared_memory(mem);
		dlog_msg("Shared memory free result %d", result);
		return res;
	}
	
	int get_apps_list( com_verifone_pml::appver::applist_t & applist )
	{
		using namespace internal_versioning;
		CSharedMemoryManager * ptr  = CSharedMemoryManager::getInstance(SHARED_MEMORY_NAME_VERSION);
		void * mem = 0;
		int result = -1;
		applist.clear();
		do
		{
			result = ptr->get_shared_memory(&mem);
			if (result < 0)
			{
				dlog_error("Cannot get shared memory!");
				break;
			}
			// ok, got shared memory
			Versions apps;
			if (deserialize_versions(mem, apps) >= 0)
			{
				for (Versions::const_iterator it = apps.begin(); it != apps.end(); ++it)
				{
					VerStruct ver = *it;
					if (strlen(ver.libname) == 0)
					{
						applist.push_back(make_pair(std::string(ver.name), std::string(ver.ver)));
					}
				}
			}
		} while(false);
		result = ptr->close_shared_memory(mem);
		dlog_msg("Shared memory free result %d", result);
		return applist.size();
	}
	
	int get_lib_list( com_verifone_pml::appver::applist_t& liblist, const char *appname /* = NULL */ )
	{
		using namespace internal_versioning;
		CSharedMemoryManager * ptr  = CSharedMemoryManager::getInstance(SHARED_MEMORY_NAME_VERSION);
		void * mem = 0;
		int result = -1;
		liblist.clear();
		do
		{
			result = ptr->get_shared_memory(&mem);
			if (result < 0)
			{
				dlog_error("Cannot get shared memory!");
				break;
			}
			// ok, got shared memory
			Versions apps;
			if (deserialize_versions(mem, apps) >= 0)
			{
				for (Versions::const_iterator it = apps.begin(); it != apps.end(); ++it)
				{
					VerStruct ver = *it;
					if (strlen(ver.libname) != 0)
					{
						if (appname)
						{
							// This gets all the libraries, used by app, passed as parameter
							if (!strcmp(ver.name, appname))
							{
								liblist.push_back(make_pair(std::string(ver.libname), std::string(ver.ver)));
							}
						}
						else
						{
							// This gets all the libraries used in the system
							std::pair<std::string, std::string> libinfo(make_pair(std::string(ver.libname), std::string(ver.ver)));
							if (std::find(liblist.begin(), liblist.end(), libinfo) == liblist.end())
							{
								liblist.push_back(libinfo);
							}
						}
					}
					
				}
			}
		} while(false);
		result = ptr->close_shared_memory(mem);
		dlog_msg("Shared memory free result %d", result);
		return liblist.size();
	}

	int get_app_version( const char name[], std::string & appver )
	{
		using namespace internal_versioning;
		assert(name != NULL);
		CSharedMemoryManager * ptr  = CSharedMemoryManager::getInstance(SHARED_MEMORY_NAME_VERSION);
		void * mem = 0;
		int result = -1;
		int res = -1;
		do
		{
			result = ptr->get_shared_memory(&mem);
			if (result < 0)
			{
				dlog_error("Cannot get shared memory!");
				break;
			}
			// ok, got shared memory
			Versions apps;
			if (deserialize_versions(mem, apps) >= 0)
			{
				for (Versions::const_iterator it = apps.begin(); it != apps.end(); ++it)
				{
					VerStruct ver = *it;
					if (strlen(ver.libname) == 0 && !strcmp(ver.name, name))
					{
						appver.assign(ver.ver);
						res = 0;
						break;
					}
				}
			}
		} while(false);
		result = ptr->close_shared_memory(mem);
		dlog_msg("Shared memory free result %d", result);
		return res;
	}

	/***********************************************************************************************/
	int register_user_event(int task_id)
	{
		CSharedMemoryManager * ptr  = CSharedMemoryManager::getInstance(SHARED_MEMORY_USER_VALUES);
		void * mem = 0;
		int * pTaskMem = 0;
		int result = -1;
		int res = -1;
		do
		{
			result = ptr->get_shared_memory(&mem);
			if (result < 0)
			{
				dlog_error("Cannot get shared memory!");
				break;
			}
			res = 0;
			int count = *(static_cast<const int *>(mem));
			bool found = false;
			pTaskMem = static_cast<int *>(mem) + sizeof(int);          // skip number of elements
			for (int i = 0; i < count; ++i)
			{
				if (*pTaskMem == task_id)
				{
					found = true;
					break;
				}
				pTaskMem += 3;
			}
			*pTaskMem = task_id;
			*(pTaskMem+1) = 0;
			if (!found)
			{
				int * pCount = static_cast<int *>(mem);
				(*pCount)++;
			}
			#ifdef DEBUG
			int * pCount = static_cast<int *>(mem);
			dlog_msg("We have %d registered user tasks", *pCount);
			#endif
		} while(false);
		result = ptr->close_shared_memory(mem);
		dlog_msg("Shared memory free result %d", result);
		return res;
	}
	
	int pass_value(int task_id, int value, int ext_value)
	{
		CSharedMemoryManager * ptr  = CSharedMemoryManager::getInstance(SHARED_MEMORY_USER_VALUES);
		void * mem = 0;
		int result = -1;
		int res = -1;
		do
		{
			result = ptr->get_shared_memory(&mem);
			if (result < 0)
			{
				dlog_error("Cannot get shared memory!");
				break;
			}
			int * pTaskMem = get_memory(mem, task_id);
			if (!pTaskMem)
			{
				dlog_error("Cannot get memory to store data!");
				break;
			}
			dlog_msg("Passing value %d to task %d", value, task_id);
			*pTaskMem++ = value;
			*pTaskMem = ext_value;
			res = 0;
		} while(false);
		result = ptr->close_shared_memory(mem);
		dlog_msg("Shared memory free result %d", result);
		return res;
	}
	
	int get_value(int task_id, int &value, int &ext_value)
	{
		CSharedMemoryManager * ptr  = CSharedMemoryManager::getInstance(SHARED_MEMORY_USER_VALUES);
		void * mem = 0;
		int result = -1;
		int res = -1;
		do
		{
			result = ptr->get_shared_memory(&mem);
			if (result < 0)
			{
				dlog_error("Cannot get shared memory!");
				break;
			}
			int * pTaskMem = get_memory(mem, task_id);
			if (!pTaskMem)
			{
				dlog_error("Cannot get memory to store data!");
				break;
			}
			dlog_msg("Task %d got user value %d", task_id, value);
			value = *pTaskMem;
			*pTaskMem++ = 0; // cleanup
			ext_value = *pTaskMem;
			*pTaskMem = 0;
			res = 0;
		} while(false);
		result = ptr->close_shared_memory(mem);
		dlog_msg("Shared memory free result %d", result);
		return res;
	}

}

