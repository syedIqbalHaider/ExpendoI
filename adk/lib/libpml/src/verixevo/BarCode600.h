#ifndef BARCODE600_H
#define BARCODE600_H


#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 *
 * Copyright (C) 2007 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file          BarCode600.h
 *
 * @author        Arkadiusz_A1
 *
 * @brief         Definition of Vx600 bar code reader class
 *
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C Coding
 *           Standard 1.0.0
 */

/***************************************************************************
 * Includes
 **************************************************************************/

#include <string>

/***************************************************************************
 * Using
 **************************************************************************/
 
/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
namespace com_verifone_barcode
{


	/**
	 * @addtogroup Comm
	 * @{
	 */

	/***************************************************************************
	 * Preprocessor constant definitions
	 **************************************************************************/


	/***************************************************************************
	 * Macro definitions
	 **************************************************************************/

	/***************************************************************************
	 * Data type definitions
	 **************************************************************************/

	class CBarCodeReader
	{
	public:
		typedef enum
		{
			mode_oneshot,
			mode_monitoring
		} t_mode;
		private:
			int inHndl;
			t_mode m_mode;

			static CBarCodeReader * pInstance;

		protected:
		
		public:
			CBarCodeReader();
			void Init();
			int Open();
			void Close();
			int Read(std::string & buf);
			void setMode(t_mode mode);
			t_mode getMode() const;
			bool isOpened() const;

			static CBarCodeReader * getInstance();
			static void deleteInstance();
	};


	/***************************************************************************
	 * Exported variable declarations
	 **************************************************************************/

	/***************************************************************************
	 * Exported function declarations
	 **************************************************************************/

	/***************************************************************************
	 * Exported class declarations
	 **************************************************************************/



	/**
	 * @}
	 */

	/***************************************************************************
	 * Module namspace: end
	 **************************************************************************/
}

#endif /* BARCODE600_H */
