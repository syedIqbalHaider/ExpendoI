/*****************************************************************************
 *
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file        AbstractedAPI.cpp
 *
 * @author      Kamil Pawlowski
 *
 * @brief       Abstracted API - common API for Verix OS calls
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C Coding
 *              Standard 1.0.0
 */

#include <svc.h>
#include <svc_sec.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <assert.h>
#include <errno.h>


#include <libpml/pml.h>
#include <libpml/pml_abstracted_api.h>

#include <liblog/logsys.h>

// #define DIR_SEARCH_LOGGING

namespace
{
	// service functions

	typedef void (*VOID_OS_SVC_FUN_BUF) (char *);
	void get_svc_buf( VOID_OS_SVC_FUN_BUF os_fun, char * buffer, const size_t bufferSize )
	{
		char info[100];
		os_fun(info);
		info[info[0]] = 0;
		strncpy(buffer, info+1, bufferSize);
	}

	typedef int (*INT_OS_SVC_FUN_BUF) (char *);
	void get_svc_buf( INT_OS_SVC_FUN_BUF os_fun, char * buffer, const size_t bufferSize )
	{
		char info[100];
		memset(info, 0, sizeof(info));
		os_fun(info);
		for (int i = strlen(info)-1; i >= 0; --i)
		{
			if (isspace(*(info+i))) *(info+i) = '\0';
			else break;
		}
		strncpy(buffer, info, bufferSize);
	}

	typedef int (*INT_EXT_OS_SVC_FUN_BUF) (char *, int);
	void get_svc_buf( INT_EXT_OS_SVC_FUN_BUF os_fun, char * buffer, const size_t bufferSize )
	{
		#if 0
		memset(buffer, 0, bufferSize);
		os_fun(buffer, static_cast<int>(bufferSize));
		#else
		char info[100];
		memset(info, 0, sizeof(info));
		os_fun(info, static_cast<int>(sizeof(info)));
		for (int i = strlen(info)-1; i >= 0; --i)
		{
			if (isspace(*(info+i))) *(info+i) = '\0';
			else break;
		}
		strncpy(buffer, info, bufferSize);
		#endif
	}

}

namespace com_verifone_pml
{
	// private dir functions
	#ifndef PATHNAME_MAX
	#define UNDEF_PATHNAME_MAX
	#define PATHNAME_MAX 256+1
	#define SUBDIRNAME_MAX 32+1
	#endif
	class dir_search::internal_state
	{
	public:
		/* Default constructor */
		internal_state(): m_group(get_group()), m_search_flash(true), m_search_ram(true), m_state(stat_init_ram)
		{
			memset(m_mask, 0, sizeof(m_mask));
			memset(m_ori_mask, 0, sizeof(m_ori_mask));
			memset(m_fname, 0, sizeof(m_fname));
			memset(m_subdir, 0, sizeof(m_subdir));
			memset(m_ori_path, 0, sizeof(m_ori_path));
		}

		int get_first(const char *mask, char * buf, size_t bufSize);
		int get_next(char * buf, size_t bufSize);
		/*void set_mask(const char * const mask) { strncpy(m_mask, mask, sizeof(m_mask)-1); }
		const char * get_mask() { return g_mask; }

		void set_fname(const char * const fname) { strncpy(m_fname, fname, sizeof(m_fname)-1); }
		const char * get_fname() { return g_fname; } */

		//bool file_matches_mask(const char * mask, const char *fname);
	private:
		static const int MAX_FILE_NAME = PATHNAME_MAX; // ASCIIZ string
		static const int MAX_SUBDIR_NAME = SUBDIRNAME_MAX; // Subdirectory string
		static const char * RAM; // = "I:";
		static const char * FLASH; // = "F:";
		char m_mask[MAX_SUBDIR_NAME];
		char m_ori_mask[MAX_SUBDIR_NAME];
		char m_fname[MAX_FILE_NAME];
		char m_subdir[MAX_SUBDIR_NAME];
		char m_ori_path[MAX_SUBDIR_NAME];
		int m_group;
		bool m_search_ram, m_search_flash;

		enum lookup_states { stat_init_ram, stat_search_ram_first, stat_search_ram_next, stat_init_flash, stat_search_flash_first, stat_search_flash_next, stat_not_found };
		lookup_states m_state;

		bool file_matches_mask(const char * mask, const char *fname);
		int parse_filename(const char *src_filename);
		int get_next_matching_file();
		int find_first();
		int find_next();
		void create_output(char * buf, size_t bufSize)
		{
			char drive = 'I';
			size_t start = 0;
			size_t oriPathSize = strlen(m_ori_path);
			*buf = 0;
			if (oriPathSize)
			{
				strncpy(buf, m_ori_path, bufSize);
				bufSize -= oriPathSize;
				const char lastChar = *(m_ori_path+oriPathSize-1);
				if (lastChar != '/' && lastChar != ':')
				{
					strncat(buf, "/", bufSize);
					--bufSize;
				}
			}
			if( sscanf( m_fname, "%c:", &drive ) == 1 ) start = 2;
			strncat(buf, m_fname+start, bufSize);
			dlog_msg("Output: '%s'", buf);
		}
		bool setGroup(int group)
		{
			if (group >= 1 && group <= 15)
			{
				m_group = group;
				return true;
			}
			return false;
		}
		bool setDrive(char drive)
		{
			if (drive == 'I' || drive == 'i')
			{
				m_search_ram = true;
				m_search_flash = false;
				return true;
			}
			else if (drive == 'F' || drive == 'f')
			{
				m_search_ram = false;
				m_search_flash = true;
				return true;
			}
			return false;
		}
	};

	#ifdef UNDEF_PATHNAME_MAX
	#undef UNDEF_PATHNAME_MAX
	#undef PATHNAME_MAX
	#undef SUBDIRNAME_MAX
	#endif

	//int internal_state::MAX_FILE_NAME = 32+1;
	const char * dir_search::internal_state::RAM = "I:";
	const char * dir_search::internal_state::FLASH = "F:";


	int dir_search::internal_state::parse_filename( const char *src_filename )
	{
		int ii, iNumCommas;
		char drive = 'I';
		int group = get_group();
		int result = -1;

		// false loop
		do
		{
			if(!src_filename || src_filename[0] == '\0')
			{
				dlog_msg("DS: No params given, leaving defaults");
				result = 0;
				break;
			}
			int slen = strlen(src_filename);
			while (src_filename[slen-1] == '/') --slen;

			// count separators
			for(ii=0, iNumCommas=0; ii<slen; ii++)
			{
				if(src_filename[ii] == ':') iNumCommas++;
				else if(src_filename[ii] == '/') iNumCommas++;
			}

			//dlog_msg("DS: Num commas: %d", iNumCommas); SVC_WAIT(1000);

			// read parameters
			if( iNumCommas == 0 )
			{
				dlog_msg("DS: No commas, just subdir %s", src_filename);
				m_search_flash = false; // ignore flash, ram is the default
			}
			else if( iNumCommas == 1 )
			{
				if( sscanf( src_filename, "%2d/", &group ) == 1 )
				{
					dlog_msg( "DS: File GID %d", group );
					if (!setGroup(group))
					{
						dlog_alert("DS: Invalid group!");
						break;
					}
					m_search_flash = false; // ignore flash, lookup only in RAM if no drive is given
				}
				else if( sscanf( src_filename, "%c:", &drive ) == 1 )
				{
					dlog_msg( "DS: Found drive %c", drive );
					if (!setDrive(drive))
					{
						dlog_alert("DS: Invalid drive!");
						break;
					}
					setGroup(get_group()); // use current group if only drive is given
				}
				else
				{
					dlog_alert("DS: scanning parameters into given filename failed!");
					break;
				}
			}
			else if( iNumCommas >= 2 )
			{
				if( sscanf( src_filename, "%c:%2d:", &drive, &group ) == 2 || sscanf( src_filename, "%c:%2d/", &drive, &group ) == 2 )
				{
					if (!setDrive(drive) || !setGroup(group))
					{
						dlog_alert("DS: Invalid data!");
						break;
					}
					iNumCommas = 2;
				}
				else if( sscanf( src_filename, "%c:", &drive ) == 1 )
				{
					if (!setDrive(drive))
					{
						dlog_alert("DS:Invalid data!");
						break;
					}
					iNumCommas = 1;
				}
				else
				{
					// fix to make it possible to perform searches inside subdirectories
					// previously for iNumCommas > 2 function returned invalid path name for correct paths
					iNumCommas = 0;
				}
			}
			// find the separator
			for( ii=0; iNumCommas!=0; ++ii )
			{
				if( src_filename[ii] == ':' ) iNumCommas--;
				else if ( src_filename[ii] == '/' ) iNumCommas--;
			}
			if (src_filename[ii] == ':' || src_filename[ii] == '/') ++ii;
			// construct the real filename
			int copyLen = std::min(static_cast<unsigned int>(slen - ii), sizeof(m_subdir)-1);
			strncpy(m_subdir, src_filename + ii, copyLen);
			result = 1;
		} while (false);

	return result;
}

	bool dir_search::internal_state::file_matches_mask(const char * mask, const char *fname)
	{
		int i;
		bool star = false;
		const char * pat = mask;
		//static const char RAM[] = "I:";
		//static const char FLASH[] = "F:";

		/*if (!strstr(mask, RAM))
		{
			if (strstr(fname, RAM)) fname = strstr(fname, RAM) + strlen(RAM);
		}
		if (!strstr(mask, FLASH))
		{
			if (strstr(fname, FLASH)) fname = strstr(fname, FLASH) + strlen(FLASH);
		}*/
		#ifdef DIR_SEARCH_LOGGING
		dlog_msg("DS: Comparing fname %s with mask %s", fname, mask);
		#endif
		do
		{
			bool nextLoop = false;
			for (i = 0; fname[i]; i++)
			{
				if (pat[i] == '?')
				{
					if (fname[i] == '.')
					{
						if (!star) return false;
						fname++;
						nextLoop = true;
						break; // for, next loop
					}
				}
				else if (pat[i] == '*')
				{
					star = true;
					fname += i, pat += i;
					do { ++pat; } while (*pat == '*');
					if (!*pat) return true;
					nextLoop = true;
					break; // for, next loop
				}
				else
				{
					if (tolower(fname[i]) != tolower(pat[i]))
					//if (fname[i] != pat[i])
					{
						if (!star) return false;
						fname++;
						nextLoop = true;
						break; // for, next loop
					}
				}
			} /* endfor */
			if (nextLoop) continue;
			else break;
		} while(true);
		while (pat[i] == '*') ++i;
		#ifdef DIR_SEARCH_LOGGING
		dlog_msg("DS: Match result %d", !pat[i]);
		#endif
		return (!pat[i]);
	}

	int dir_search::internal_state::find_first()
	{
		int res = ::dir_get_first(m_fname);
		if (res == 0)
		{
			do
			{
				if (file_matches_mask(m_mask, m_fname))
				{
					return 1;
				}
			} while ( ( res = ::dir_get_next(m_fname)) == 0);
		}
		if (res == -1 && errno == ENOENT) res = 0;
		return res;
	}

	int dir_search::internal_state::find_next()
	{
		while (::dir_get_next(m_fname) == 0)
		{
			if (file_matches_mask(m_mask, m_fname))
			{
				return 1;
			}
		}
		return 0;
	}

	int dir_search::internal_state::get_next_matching_file()
	{
		int res = 0;
		char curDir[MAX_SUBDIR_NAME] = { '\0' };
		int curGroup = get_group();
		do
		{
			res = set_group(m_group);
			if (res == -1)
			{
				dlog_error("DS: Invalid group!");
				break;
			}
			if (strlen(m_subdir))
			{
				if ( (res = getcwd(curDir, sizeof(curDir))) == 0)
				{
					if ( ( res = chdir(m_subdir)) != 0)
					{
						//if (errno == ENOENT || errno == EPERM)
						dlog_error("DS: Invalid directory %s! (error %d, errno %d)", m_subdir, res, errno);
					}
				}
			}
		} while(false);
		if (res == -1)
		{
			set_group(curGroup);
			chdir(curDir);
			return res;
		}

		do
		{
			#ifdef DIR_SEARCH_LOGGING
			dlog_msg("DS: Internal state %d", m_state);
			#endif
			switch (m_state)
			{
				case stat_init_ram:
					// init
					strcpy(m_fname, RAM);
					snprintf(m_mask, sizeof(m_mask), "%s%s", RAM, m_ori_mask);
					m_state = stat_search_ram_first;
					continue;

				case stat_search_ram_first:
					if (!m_search_ram)
					{
						m_state = stat_init_flash;
						continue;
					}
					res = find_first();
					m_state = stat_search_ram_next;
					if (res == 1) break;
					else if (res == 0) continue;
					m_state = stat_not_found;
					break;

				case stat_search_ram_next:
					res = find_next();
					if (res == 1) break;
					else if (res == 0)
					{
						m_state = stat_init_flash;
						continue;
					}
					break;

				case stat_init_flash:
					// init
					strcpy(m_fname, FLASH);
					snprintf(m_mask, sizeof(m_mask), "%s%s", FLASH, m_ori_mask);
					m_state = stat_search_flash_first;
					continue;

				case stat_search_flash_first:
					if (!m_search_flash)
					{
						m_state = stat_not_found;
						continue;
					}
					res = find_first();
					m_state = stat_search_flash_next;
					if (res == 1) break;
					else if (res == 0) continue;
					m_state = stat_not_found;
					break;

				case stat_search_flash_next:
					res = find_next();
					if (res == 1) break;
					else if (res == 0)
					{
						m_state = stat_not_found;
						continue;
					}
					 break;

				case stat_not_found:
					res = 0;
					break;
			}
			break;
		} while(true);
		#ifdef DIR_SEARCH_LOGGING
		dlog_msg("DS: Out of the loop, %d", res);
		#endif
		set_group(curGroup);
		chdir(curDir);
		return res;
	}




	int dir_search::internal_state::get_first(const char *mask, char * buf, size_t bufSize)
	{
		if (!mask || *mask == '\0')
		{
			strcpy(m_ori_mask, "*");
		}
		else
		{
			strncpy(m_ori_mask, mask, sizeof(m_ori_mask)-1);
		}
		strncpy(m_ori_path, buf, sizeof(m_ori_path)-1);
		if (parse_filename(buf) < 0 || strlen(m_ori_mask) == 0)
		{
			dlog_error("DS: Invalid parameters!");
			return -1;
		}
		dlog_msg("DS: Group %d, check RAM %d, check Flash %d, mask %s", m_group, m_search_ram, m_search_flash, m_ori_mask);
		dlog_msg("DS: Subdir %s", m_subdir);
		m_state = stat_init_ram;
		int res = get_next_matching_file();
		if (res == 1)
		{
			create_output(buf, bufSize);
			//strncpy(buf, m_fname, bufSize);
		}
		return res;
	}

	int dir_search::internal_state::get_next(char * buf, size_t bufSize)
	{
		#ifdef DIR_SEARCH_LOGGING
		dlog_msg("DS: Current mask %s", m_mask);
		#endif
		if (strlen(m_mask) && strlen(m_fname))
		{
			int res = get_next_matching_file();
			if (res == 1)
			{
				create_output(buf, bufSize);
				//strncpy(buf, m_fname, bufSize);
			}
			return res;
		}
		return -1;
	}
}

// "public"

namespace com_verifone_pml
{
	/************************************************************************************************************************************************/
	/* File system calls (RQAP-001) */
	/************************************************************************************************************************************************/
	dir_search::dir_search(): m_state(new internal_state)
	{
		//empty
		assert(m_state);
	}

	dir_search::~dir_search()
	{
		if (m_state) delete m_state;
	}

	int dir_search::get_first(const char *mask, char * buf, std::size_t bufSize)
	{
		return m_state->get_first(mask, buf, bufSize);
	}

	int dir_search::get_next(char * buf, size_t bufSize)
	{
		return m_state->get_next(buf, bufSize);
	}


	int copy_file(const char * src, const char * dst)
	{
		int res = file_copy(src, dst);
		if (res == 0 && isFileSigned(src))
		{
			res = file_copy_auth_bit(src, dst);
		}
		return res;
	}
	int move_file(const char * src, const char * dst)
	{
		if (file_copy(src, dst) < 0) return -1;
		return _remove(src);
	}
	int delete_file(const char *name)
	{
		return _remove(name);
	}
	int rename_file(const char *oldname, const char *newname)
	{
		return _rename(oldname, newname);
	}

	int set_working_directory(const char * /* directory */)
	{
		return 0; // do nothing here
	}

	/************************************************************************************************************************************************/
	/* Service calls (RQAP-002) */
	/************************************************************************************************************************************************/
	void svcInfoPtid(char * buffer, const size_t bufferSize)
	{
		get_svc_buf(::SVC_INFO_PTID, buffer, bufferSize);
	}

	void svcInfoEprom(char * buffer, const size_t bufferSize)
	{
		get_svc_buf(::SVC_INFO_EPROM, buffer, bufferSize);
	}

	void svcInfoModelNum(char * buffer, const size_t bufferSize)
	{
		get_svc_buf(::SVC_INFO_MODELNO, buffer, bufferSize);
	}

	void svcInfoManufacturingData(char * buffer, const size_t bufferSize)
	{
		get_svc_buf(::SVC_INFO_MFG_BLK, buffer, bufferSize);
	}

	void svcInfoCountry(char * buffer, const size_t bufferSize)
	{
		get_svc_buf(::SVC_INFO_COUNTRY, buffer, bufferSize);
	}

	void svcInfoPartNumber(char * buffer, const size_t bufferSize)
	{
		get_svc_buf(::SVC_INFO_PARTNO_EXT, buffer, bufferSize);
	}

	void svcInfoHardwareVersion(char * buffer, const size_t bufferSize)
	{
		get_svc_buf(::SVC_INFO_HW_VERS, buffer, bufferSize);
	}

	void svcInfoSerialNumber(char * buffer, const size_t bufferSize)
	{
		get_svc_buf(::SVC_INFO_SERLNO, buffer, bufferSize);
	}

	void svcInfoBareSerialNumber(char * buffer, const size_t bufferSize)
	{
		char localBuffer[12];
		svcInfoSerialNumber(localBuffer, sizeof(localBuffer));
		int len = strlen(localBuffer);
		for (int i = 0; i < len; ++i)
		{
			//if (*(localBuffer+i) != '-') *buffer++ = *(localBuffer+i);
			if (isdigit(*(localBuffer+i))) *buffer++ = *(localBuffer+i);
		}
		*buffer = 0;
	}

	void svcInfoLotNumber(char * buffer, const size_t bufferSize)
	{
		get_svc_buf(::SVC_INFO_LOTNO, buffer, bufferSize);
	}

	long svcRAMSize()
	{
		return ::SVC_RAM_SIZE();
	}

	long svcFlashSize()
	{
		return ::SVC_FLASH_SIZE();
	}

	/* ---------------------------------------------------------------- */
	/*
	 * Gets SBI version
	 */
	void svcInfoSBIVersion(char * buffer, const size_t bufferSize)
	{
		SVC_INFO_SBI_VER(buffer, bufferSize);
	}

	/* ---------------------------------------------------------------- */
	/*
	 * Gets CIB ID
	 */
	void svcInfoCIBID(char * buffer, const size_t bufferSize)
	{
		SVC_INFO_CIB_ID(buffer, bufferSize);
	}

	/* ---------------------------------------------------------------- */
	/*
	 * Gets CIB Version
	 */
	void svcInfoCIBVersion(char * buffer, const size_t bufferSize)
	{
		SVC_INFO_CIB_VER(buffer, bufferSize);
	}

	/*
	 * Gets removal switches and tamper status
	 */
	int svcGetRemovalSwitchStatus()
        {
                // Unsupported on current devices
                return -1;
        }
        int svcGetTamperStatus()
        {
                return isAttacked();
        }

	/************************************************************************************************************************************************/
	/* Calls (RQAP-003) */
	/************************************************************************************************************************************************/
	int svcDsp2Hex(const char *dsp, size_t dsp_len, char *hex, const size_t max_hex_len)
	{
		if (!dsp || !hex || dsp == hex || dsp_len == 0) return -1;
		if (dsp_len % 2) --dsp_len; // skip one byte
		dsp_len >>= 1;
		int conv_len = (dsp_len > max_hex_len ? max_hex_len : dsp_len);
		SVC_DSP_2_HEX(dsp, hex, conv_len);
		return conv_len;
	}

	int svcHex2Dsp(const char *hex, size_t hex_len, char *dsp, const size_t max_dsp_len)
	{
		if (!hex || !dsp || hex == dsp || hex_len == 0) return -1;
		int max_dsp = max_dsp_len >> 1;
		int conv_len = max_dsp < hex_len ? max_dsp : hex_len;
		SVC_HEX_2_DSP(hex, dsp, conv_len);
		conv_len <<= 1;
		if (conv_len < max_dsp_len) *(dsp+conv_len) = 0;
		return conv_len;
	}

	/************************************************************************************************************************************************/
	/* Service calls (RQAP-004) */
	/************************************************************************************************************************************************/
	unsigned long svcCrcCalc(const CRC_TYPES crc_type, const unsigned long seed, const void *msg, const size_t size)
	{
		switch (crc_type)
		{
			case lrc:
				return SVC_LRC_CALC(msg, size, seed);
			case crc16_lsb:
				return SVC_CRC_CRC16_L(msg, size, seed);
			case crc16_msb:
				return SVC_CRC_CRC16_M(msg, size, seed);
			case ccitt_lsb:
				return SVC_CRC_CCITT_L(msg, size, seed);
			case ccitt_msb:
				return SVC_CRC_CCITT_M(msg, size, seed);
			case crc32:
				return SVC_CRC_CRC32_L(msg, size, seed);
			case crc32_inverted:
			case crc16:
			case ccitt16:
			default:
				dlog_alert("Either unsupported or invalid type %d!", crc_type);
				return 0;
		}
	}

	int SHA1(const unsigned char * input_buffer, size_t input_length, SHA1_OUTPUT & output)
	{
		// Verix OS requires the cast below...
		return ::SHA1( 0, const_cast<unsigned char *>(input_buffer), input_length, output.shabuf );
	}
	int SHA256(const unsigned char * input_buffer, size_t input_length, SHA256_OUTPUT & output)
	{
		// Verix OS requires the cast below...
		return ::SHA256( 0, const_cast<unsigned char *>(input_buffer), input_length, output.shabuf );
	}

	/************************************************************************************************************************************************/
	/* System Handling(RQDV-004) */
	/************************************************************************************************************************************************/
	void svcRestart()
	{
		SVC_RESTART("");
	}
	void svcRestartApplication()
	{
		// There is no equivalent, just perform reboot
		SVC_RESTART("");
	}
	void sleep(int seconds)
	{
		SVC_WAIT(seconds * 1000);
	}
	void usleep(int microseconds)
	{
		SVC_WAIT(microseconds / 1000);
	}
	void datetime2seconds(const char *yyyymmddhhmmss, unsigned long *seconds)
	{
		::datetime2seconds(yyyymmddhhmmss, seconds);
	}
	int luhn(const std::string &pan_asciiz)
	{
		if (pan_asciiz.size() && strspn(pan_asciiz.c_str(), "1234567890Ff") == pan_asciiz.size())
		{
			char sPan[20];
			// dlog_msg("PAN: %s", pan_asciiz.c_str());
			SVC_AZ2CS(sPan, pan_asciiz.c_str());
			return SVC_MOD_CK(sPan);
		}
		return 0;
	}
	bool isFileSigned(const std::string &file)
	{
		int attrib = dir_get_attributes(file.c_str());
		if (attrib != -1)
		{
			if (attrib & ATTR_NOT_AUTH)
			{
				// Try checking signature
				if (svcAuthenticateFile(file.c_str()) == 0)
				{
					attrib = dir_get_attributes(file.c_str());
					return !(attrib & ATTR_NOT_AUTH); // 0 if attribute set (not signed), 1 otherwise (signed)
				}
				return false;
			}
			return true;
		}
		else return false; // file doesn't exist or other error
	}
	namespace
	{
		std::string get_gid_str(int gid)
		{
			if (gid >= 1 && gid <= 15)
			{
				char buf[5];
				snprintf(buf, sizeof(buf), "%d", gid);
				return std::string(buf);
			}
			return std::string("");
		}
	}

	std::string getFlashRoot(int gid /* = -1*/)
	{
		return std::string("F:") + get_gid_str(gid);
	}
	std::string getRAMRoot(int gid /* = -1*/)
	{
		return std::string("I:") + get_gid_str(gid);
	}
	std::string getLogsDir()
	{
		return std::string("F:1/logs");
	}
	int openConsole()
	{
		return ::open(DEV_CONSOLE, 0);
	}
	int get_env(const char *key, char *buf, int size)
	{
		return ::get_env(key, buf, size);
	}
	int put_env(const char *key, const char *buf, int size)
	{
		return ::put_env(key, buf, size);
	}
	int getkey (const char *key, char *buf, int size, const char *file)
	{
		return ::getkey(key, buf, size, file);
	}
	int putkey (const char *key, const char *buf, int size, const char *file)
	{
		return ::putkey(key, buf, size, file);
	}

	int alpha_shift(int c)
	{
		return ::alpha_shift(c);
	}
	int normal_tone()
	{
		::normal_tone();
		return 0;
	}
	int error_tone()
	{
		::error_tone();
		return 0;
	}
 	int sound (int note, int msec)
 	{
		::sound (note, msec);
 		return 0;
 	}

	int read_clock(char *yyyymmddhhmmssw, size_t size)
	{
		if (size >= 15)
		{
			yyyymmddhhmmssw[15] = 0; // zero terminate, OS may not do that
			::read_clock(yyyymmddhhmmssw);
			return strlen(yyyymmddhhmmssw);
		}
		return -1;
	}
	long get_uptime()
	{
		return static_cast<long>(read_ticks() / TICKS_PER_SEC);
	}

	// 0 - 100
	int set_backlight_level(int percent)
	{
		return ::set_backlight_level(percent);
	}
	int set_backlight(bool enable)
	{
		//return ::set_backlight(static_cast<int>(enable));
		if (enable) return set_backlight_level(100);
		else return set_backlight_level(5);
	}
	int get_backlight_level()
	{
		return ::get_backlight_level();
	}

	int get_battery_status()
	{
		int res = ::get_battery_sts();
		if (res >= 0)
		{
			// We have battery
			int chrg = ::get_battery_value(CHARGERSTATUS);
			if (chrg == 3) res = 1;
			else res = 0;
		}
		return res;
	}
	int get_battery_condition()
	{
		if (::get_battery_sts() >= 0)
		{
			if (get_battery_status() == 1) return com_verifone_pml::BATTERY_CHARGING;
			int perc = get_battery_percentage();
			// dlog_msg("Battery percentage %d", perc);
			if (perc >= 100) return com_verifone_pml::BATTERY_FULL;
			else if (perc <= 10) return com_verifone_pml::BATTERY_CRITICAL;
			else if (perc <= 20) return com_verifone_pml::BATTERY_LOW;
			return com_verifone_pml::BATTERY_HIGH;
		}
		return com_verifone_pml::BATTERY_ABSENT;
	}
	int get_battery_percentage()
	{
		if (::get_battery_sts() >= 0)
		{
			const int batteryFull = ::get_battery_value(FULLCHARGE);
			return (::get_battery_value(REMAININGCHARGE) * 100) / batteryFull;
		}
		return -1;
	}
	// 0 - undocked, 1 - docked, -1 - no battery
	int get_dock_status()
	{
		if (::get_battery_sts() >= 0)
		{
			if (!::get_dock_sts()) return 1;
			return 0;
		}
		// No battery
		return -1;
	}
	int get_battery_value(battery_values reqValue)
	{
		return ::get_battery_value(static_cast<int>(reqValue));
	}


	int set_keyboard_backlight(int enabled) // 1 - enabled, 0 - disabled
	{
		switch (enabled)
		{
			case 0: set_kb_backlight(0); break;
			default: // fall back to next case
			case 1: set_kb_backlight(1); break;
		}
		return 0;
	}
	int get_keyboard_backlight()
	{
		return get_kb_backlight();
	}

	int key_beeps(bool enable)
	{
		return ::key_beeps(enable);
	}


	int set_clock(const char * yyyymmddhhmmss)
	{
		int result = -1;
		int len = strlen(yyyymmddhhmmss);
		if (len == 14 || len == 15)
		{
			int clockHandle = open(DEV_CLOCK, 0);
			if (clockHandle >= 0)
			{
				result = write(clockHandle, yyyymmddhhmmss, 14);
				close(clockHandle);
				if (result > 0) result = 0;
			}
		}
		return result;
	}

	namespace
	{
		#if 0
		bool authenticateP7S()
		{
			char signatureFilename[50];
			int ret = 0;
			bool result = true;
			int curGroup = get_group();
			int attr;
			dir_search ds;

			dlog_msg("Authenticating P7S files");
			for (int group = 1; group <= 15; ++group)
			{
				set_group(group);
				signatureFilename[0] = '\0';
				int found = ds.get_first("*.P7S", signatureFilename, sizeof(signatureFilename));
				while (found > 0)
				{
					attr = dir_get_attributes(signatureFilename);
					// dlog_msg("File %s, attr %d", signatureFilename, attr);
					if (attr != -1 && !(attr & ATTR_READONLY))
					{
						ret = authenticate(signatureFilename);
						dlog_msg("Authenticate result on file %s is %d, errno %d", signatureFilename, ret, errno);
						/*if (ret == 1)
						{
							attr = dir_get_attributes(signatureFilename);
							if (!(attr & ATTR_READONLY))
							{
								dlog_msg("Setting ReadOnly attribute");
								dir_set_attributes(signatureFilename, ATTR_READONLY);
							}
						}
						else */
						if (ret != 1)
						{
							// TODO: Should we react on authentication error?
							result = false;
						}
					}
					found = ds.get_next(signatureFilename, sizeof(signatureFilename));
				}
			}
			set_group(curGroup);
			dlog_msg("Authentication completed");

			return result;
		}
		#endif
	}

	int get_first_file_group()
	{
		return 1;
	}

	int get_last_file_group()
	{
		return 15;
	}


	int svcProcessUpgradeFile(const char * file, int * reboot /*= 0*/)
	{
		const char * UNZIP_VAR = "UNZIP";
		int result = -1;
		// First of all, check if the file is signed
		if (!isFileSigned(std::string(file)))
		{
			dlog_error("File '%s' doesn't exist or is not authorised!", file);
			errno = EACCES; // set to no permission :)
			return result;
		}
		// clear "UNZIP" variable to make sure we'll read the real one
		char empty[] = "\0";
		::put_env(UNZIP_VAR, empty, 0);
		if (unzip(file) == 0)
		{
			// wait for unzip completion
			// TODO: EVT_SYSTEM may indicate another event, like dock - check "UNZIP" presence and loop in case it's missing!
			::wait_evt(EVT_SYSTEM);
			dlog_msg("Unzip completed!");
			char zip_ok;
			int zip_res = get_env(UNZIP_VAR, &zip_ok, 1);
			if (zip_res != 1 || zip_ok != '1')
			{
				dlog_error("Unzip has failed, error %d", zip_ok);
			}
			else
			{
				result = 0; // okay!
				// authenticateP7S(); // Perform once, after ALL the upgrades are processed, to save time!!!
			}
		}
		else
		{
			dlog_alert("Unzip has failed with errno %d", errno);
		}
		if (result == -1)
		{
			const char * UNZIP_FILE = "*UNZIP";
			int len = strlen(file);
			dlog_msg("Forcing reboot");
			if (put_env(UNZIP_FILE, file, len) == len)
			{
				// result = 0;
				if (reboot) *reboot = 1;
				else SVC_RESTART(""); // we have no choice here... reboot is needed to unzip properly
			}
			else
			{
				dlog_alert("Cannot force reboot!");
			}
		}
		// Shall we reboot?
		if (result == 0 && reboot)
		{
			*reboot = 0;
			static const char * REBOOT_FILES[] =
				{ "vfi.ped", "config.$$$", "*.rkl" };
			com_verifone_pml::dir_search ds;
			char filename[33];
			for (int i = 0; i < sizeof(REBOOT_FILES)/sizeof(REBOOT_FILES[0]); ++i)
			{
				strcpy(filename, "I:");
				if (ds.get_first(REBOOT_FILES[i], filename, sizeof(filename)))
				{
					// Yes!
					dlog_alert("We should reboot!");
					*reboot = 1;
					break;
				}
			}
		}
		return result;
	}

	int svcAuthenticateFile(const char * file)
	{
		char filename[33];
		snprintf(filename, sizeof filename, "%s.p7s", file);
		int ret = authenticate(filename);
		if (ret == 1) return 0;
		return -1;
	}


	char * get_serial_port_name(char * buf, size_t size, int port_no)
	{
		snprintf(buf, size, "/dev/com%d", port_no);
		return buf;
	}

	bool has_base_station()
	{
		return get_usb_device_bits() & UDB_IBHUB;
	}

	bool base_station_info(std::string & firmware_version, std::map<std::string, std::string> * base_info /*= NULL*/)
	{
		// Try detecting 820 Duet
		bool result = has_base_station();
		if (result)
		{
			static std::map<std::string, std::string> duetVersion;
			static std::string fw;
			// get firmware info etc. 
			if (duetVersion.empty() || fw.empty())
			{
				// Check Duet firmware version
				int printerHandle = open(DEV_COM4, 0);
				if (printerHandle >= 0)
				{
					// Fake loop, avoid gotos
					do
					{
						dlog_msg("Getting Duet firmware version [%d]", printerHandle);
						const int NUM_TRIES = 3;

						char tempbuf[512];
						for (int i = 0; i < NUM_TRIES; i++)
						{
							memset(tempbuf, 0, sizeof tempbuf);
							tempbuf[0] = '\x1B';
							strcat(tempbuf,"CS;");
							int status = ::write(printerHandle, tempbuf, strlen(tempbuf));
							dlog_msg("Write status: %d/%d (%s)", status, errno, strerror(errno));
							if (status > 0) break;
							SVC_WAIT(500);
						}

						memset(tempbuf, 0, sizeof tempbuf);
						int len = 0;
						for (int i=0; i < NUM_TRIES; i++)
						{
							int status = ::read(printerHandle, &tempbuf[len], (sizeof tempbuf) - len - 1);
							if (status > 0)
							{
								/* compare with control data */
								//dlog_msg("FW Version[%s], len=%d", USBbuf, ret);
								len += status;
							}
							SVC_WAIT(500);
						}
						if (len <= 0)
						{
							dlog_error("No response from COM4");
							break;
						}
						// dlog_msg("Duet Response [%s], len [%d]", tempbuf, strlen(tempbuf));
						const char * TOKENS = "\r\n";
						const char * DUET_VERSION = "Version";
						char * temp = strtok(tempbuf, TOKENS);
						std::vector<std::string> data;
						while (temp)
						{
							if (strlen(temp))
							{
								data.push_back(temp);
							}
							temp = strtok(NULL, TOKENS);
						}
						while (!data.empty())
						{
							std::string it = data.back();
							data.pop_back();
							size_t pos = it.find_first_of(":");
							if (pos != std::string::npos)
							{
								std::string first(it.substr(0, pos));
								std::string second(it.substr(pos+1));
								first.erase(first.begin(), std::find_if(first.begin(), first.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
								first.erase(std::find_if(first.rbegin(), first.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), first.end());
								second.erase(second.begin(), std::find_if(second.begin(), second.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
								second.erase(std::find_if(second.rbegin(), second.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), second.end());
								dlog_msg("Adding [%s] -> [%s]", first.c_str(), second.c_str());
								duetVersion.insert( std::make_pair(first, second) );
								if (first.find(DUET_VERSION) != std::string::npos)
								{
									fw = second;
									dlog_msg("Firmware [%s]", fw.c_str());
								}
							}
						}
					} while(false);
					close(printerHandle);
				}
				else
				{
					dlog_error("Impossible to get Duet firmware version!");
				}
			}
			if (base_info)
			{
				// todo: make this compatible with vos
				*base_info = duetVersion;
			}
			firmware_version = fw;
		}
		return result;
	}

	// Unsupported on eVo platform
	int dump_logs(std::string &/*out_file*/)
	{
		errno = ENXIO;
		return -1;
	}

	namespace apm {
		int init(unsigned long)
		{
			errno = ENXIO;
			return -1;
		}
		int init(const std::vector<std::string> &)
		{
			errno = ENXIO;
			return -1;
		}
		int activate(MODES)
		{
			errno = ENXIO;
			// SVC_SLEEP();
			return -1;
		}
	};

	std::string get_invocation_name()
	{
		std::string ourName;
		for (int id = 2; ; ++id) // Start from 2nd task, skip OS task
		{
			struct task_info info;
			int result = get_task_info(id, &info);
			if (result <= 0)
				break;

			if (info.id == get_task_id())
			{
				if (info.path && strlen(info.path))
				{
					ourName.assign(info.path);
					std::transform(ourName.begin(), ourName.end(), ourName.begin(), ToLower);
				}
				break;
			}
		}
		return ourName;
	}
}


