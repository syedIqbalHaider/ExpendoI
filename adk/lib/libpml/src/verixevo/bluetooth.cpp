/*
 * =====================================================================================
 *
 *       Filename:  bletooth.cpp
 *
 *    Description:  Bluetooth support for the PML
 *
 *        Version:  1.0
 *        Created:  12.02.2015 12:50:05
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lucjan Bryndza (LB), lucck
 *   Organization:  
 *
 * =====================================================================================
 */
#include <errno.h>

#include <liblog/logsys.h>
#include <libpml/pml.h>
#include <libpml/pml_abstracted_api.h>

/* ------------------------------------------------------------------ */
namespace com_verifone_pml {
namespace net {
namespace bluetooth {
/* ------------------------------------------------------------------ */
//! Enable or disable bluetooth device
int enable( bool enable )
{
	static_cast<void>(enable);
	errno = ENODEV;
	return -1;
}
/* ------------------------------------------------------------------ */ 
/** Function establish Bluetooth connection with selected type
	* @param[in] link Connection type of the device
	* @return Error -1 if failed and errno set
	*/
int connect( int pair_index, conn_type::_enum_ link )
{
	static_cast<void>(pair_index);
	static_cast<void>(link);
	errno = ENODEV;
	return -1;
}
/* ------------------------------------------------------------------ */
/** Destroy the bluetooth link
	* @return error code */
int disconect( conn_type::_enum_ link )
{
	static_cast<void>(link);
	errno = ENODEV;
	return -1;
}
/* ------------------------------------------------------------------ */
/**  Scan the available devices in range 
	*  @param[out] dev_list Return device list in the application
	@return Number of count of the device or -1 and errno will be set
*/
int scan( std::vector<device> &dev_list )
{
	static_cast<void>(dev_list);
	errno = ENODEV;
	return -1;
}
/* ------------------------------------------------------------------ */
/** Pair with the selected device from the list
	* @param[in] dev Input device for pair
	* @return 0 on success -1 if fail errno will be set
	*/
int pair( const device& dev, const char pin[], pair_method::_enum_ method )
{
	static_cast<void>(dev);
	static_cast<void>(pin);
	static_cast<void>(method);
	errno = ENODEV;
	return -1;
}
/* ------------------------------------------------------------------ */
/** Pair with the selected device from the list
	* @param[in] dev Input device for pair
	* @return 0 on success -1 if fail errno will be set
	*/
int is_paired( int pair_index )
{
	static_cast<void>(pair_index);
	errno = ENODEV;
	return -1;
}
/* ------------------------------------------------------------------ */
/** Unpair with the selected device from the list
	* @param[in] dev Input device for pair
	* @return 0 on success -1 if fail errno will be set
	*/
int unpair( int paired_index )
{
	static_cast<void>(paired_index);
	errno = ENODEV;
	return -1;
}
/* ------------------------------------------------------------------ */
/** Unpair with the selected device from the list
	* @param[in] dev Input device for pair
	* @return 0 on success -1 if fail errno will be set
	*/
int paired_list( std::vector<device>& dev )
{
	static_cast<void>(dev);
	errno = ENODEV;
	return -1;
}

int paired_list( std::vector<network_profile>& dev )
{
	static_cast<void>(dev);
	errno = ENODEV;
	return -1;
}
/* ------------------------------------------------------------------ */ 
/** 
* Get paired devices count
* @param[in] count Get paired profiles count
* @return error code
*/
int get_paired_count( int& count )
{
	static_cast<void>(count);
	errno = ENODEV;
	return -1;
}
/* ------------------------------------------------------------------ */
/** 
* Find paired device as an address
* @param[in] device Get paired profiles count
* @param[out] pair_index Paired index
* @return error code
*/
int find_paired( const device &device, int& pair_index )
{
	static_cast<void>(device);
	static_cast<void>(pair_index);
	errno = ENODEV;
	return -1;
}
/* ------------------------------------------------------------------ */ 
/** 
	*  Set device as paired defaults
	*	@param[in] pair_index Pairing device index
	*	@return Error code
	*/
int set_paired_default( int pair_index )
{
	static_cast<void>(pair_index);
	errno = ENODEV;
	return -1;
}
/* ------------------------------------------------------------------ */
/** Get device as defualt paired
* @param[out] Get paired device info
* @return Error code
*/
int get_default_paired( device& cfg )
{
	static_cast<void>(cfg);
	errno = ENODEV;
	return -1;
}
/* ------------------------------------------------------------------ */ 
/** Get paired device profile id
	* @param[out] profile_id Paired profile index returned
	* @return Error code 
	*/
int get_default_paired_id( int &pair_index )
{
	static_cast<void>(pair_index);
	errno = ENODEV;
	return -1;
}
/* ------------------------------------------------------------------ */ 
/** Get connected list of devices
	* @param[in] List of connected devices
	* @return error code
	*/
int get_connected( std::vector<device>& m_list )
{
	static_cast<void>(m_list);
	errno = ENODEV;
	return -1;
}
/* ------------------------------------------------------------------ */ 
} } }
/* ------------------------------------------------------------------ */ 

