#ifndef EMEA_CICAPP_VXOS_HPP
#define EMEA_CICAPP_VXOS_HPP

#ifndef __cplusplus
#  error "This file is for C++ only!"
#endif

/***************************************************************************
**
 *
 * Copyright (C) 2006 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 *
 **************************************************************************/

/**
 * @file   VxOS.hpp
 *
 * @author  Tomasz Saniawa (tomasz_s1@verifone.com)
 *
 * @brief   Helper layer on top of standard VerixV calls
 *
 * @remarks  This file should be compliant with Verifone EMEA R&D C++ Coding
 *           Standard 1.0.2
 */

/***************************************************************************
 * Includes
 **************************************************************************/
#include <svc.h>

/***************************************************************************
 * Module namespace: begin
 **************************************************************************/
namespace com_verifone_VxOS
{
	long get_event_bit(int handle);
		
} //namespace
/***************************************************************************
 * Module namespace: end
**************************************************************************/

#endif // EMEA_CICAPP_VXOS_HPP
