#include <libpml/pml_abstracted_api.h>

#include <openssl/md5.h>

/* ---------------------------------------------------------------- */
namespace com_verifone_pml {


namespace md5_internals
{
	MD5_CTX ctx;
};
MD5::MD5()
	{ MD5_Init(&md5_internals::ctx); }
void MD5::update(const void * input_buffer, size_t input_length)
	{ MD5_Update(&md5_internals::ctx, input_buffer, input_length); }
int MD5::finalize(MD5_OUTPUT & output)
	{ MD5_Final(output.md5buf, &md5_internals::ctx); return 0; }

/* ---------------------------------------------------------------- */
} // namespace com_verifone_pml

