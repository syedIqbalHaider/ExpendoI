#include <svc.h>
#include <errno.h>
#include <assert.h>
#include <string.h>

#include <liblog/logsys.h>

#include "VxSharedMemMgr.hpp"


/* 
 * This simple class implements shared memory management for VerixV architecture
 * It will share one memory block, size 4 kBytes, between all users.
 * First call to get_shared_memory() gets that memory chunk from the OS. 
 * Subsequent calls open it for read/write. 
 * 
 * Memory chunk, retrieved during first call, cannot be freed because freeing it would cause OS to reclaim that memory!
 * If an application really want to free that memory, it has to be done manually (pointer to memory, received during first get_shared_memory() access, can be used to free the chunk). 
 */
namespace com_verifone_VxSharedMemoryMgr
{
	namespace
	{
		// static const char * const SHARED_MEMORY_NAME = "PML_SHARED_MEM";
		static const int SHARED_MEMORY_SIZE = 4096; // 4 kBytes of shared memory should be enough for everything
		
		
	}
	
	threadMapper CSharedMemoryManager::threadsMap;
	
	CSharedMemoryManager * CSharedMemoryManager::getInstance(const std::string & sharedMemName)
	{
		SharedMemId id(sharedMemName, get_task_id());
		dlog_msg("Getting shared mem, name %s, for task %d", sharedMemName.c_str(), get_task_id());
		//threadMapper::const_iterator itFinder = CSharedMemoryManager::threadsMap.find(get_task_id());
		threadMapper::const_iterator itFinder = CSharedMemoryManager::threadsMap.find(id);
		if (itFinder != threadsMap.end()) return itFinder->second;
		CSharedMemoryManager * pTD = new CSharedMemoryManager(sharedMemName);
		assert(pTD);
		dlog_msg("Allocated new ptr %p for task %d, shared mem name %s", pTD, get_task_id(), sharedMemName.c_str());
		threadsMap.insert( std::make_pair( id, pTD ) );
		return pTD;
	}
	
	CSharedMemoryManager::CSharedMemoryManager(const std::string & sharedMemName): SemaphoreName(makeSemaphoreName(sharedMemName)), SharedMemoryName(sharedMemName), g_memory(0), g_semaphore(0)
	{
		// empty...
	}
	
	#if 0
	CSharedMemoryManager::~CSharedMemoryManager()
	{
		if (g_semaphore != 0)
		{
			int semRes = sem_post(g_semaphore);
			dlog_msg("Semaphore post result %d (errno %d)", semRes, errno);
			semRes = sem_close(g_semaphore);
			dlog_msg("Semaphore close result %d (errno %d)", semRes, errno);
		}
	}
	#endif
	
	std::string CSharedMemoryManager::makeSemaphoreName(const std::string & sharedMemName)
	{
		static const std::string SHARED_SEMAPHORE_NAME = "_SEM";
		std::string result(sharedMemName);
		result.append(SHARED_SEMAPHORE_NAME);
		dlog_msg("New semaphore, name %s", result.c_str());
		return result;
	}
	
	int CSharedMemoryManager::get_shared_memory(void ** ptr)
	{
		int result = -1;
		if (g_semaphore == 0)
		{
			g_semaphore = sem_open(const_cast<char *>(SemaphoreName.c_str()), 0); // OS function requirement...
			if (reinterpret_cast<int>(g_semaphore) == -1)
			{
				dlog_error("Cannot create global semaphore name %s, errno %d!", SemaphoreName.c_str(), errno);
				g_semaphore = 0;
				return result;
			}
			else
			{
				dlog_msg("Shared semaphore created, ptr %p", g_semaphore);
			}
		}
		int semRes = sem_wait(g_semaphore);
		if (semRes == 0)
		{
			do
			{
				if (g_memory)
				{
					*ptr = g_memory;
					result = 0;
					break;
				}
				// Try to create the memory first
				void * memory = shm_open(SharedMemoryName.c_str(), O_EXCL | O_CREAT | O_RDWR, SHARED_MEMORY_SIZE);
				if (reinterpret_cast<int>(memory) == -1)
				{
					if (errno == EEXIST || errno == EBADF || errno == ENODEV)
					{
						dlog_msg("Shared memory exists (errno %d)", errno);
						memory = shm_open(SharedMemoryName.c_str(), O_RDWR, SHARED_MEMORY_SIZE);
						if (reinterpret_cast<int>(memory) == -1)
						{
							dlog_error("Cannot open shared memory '%s', errno %d!", SharedMemoryName.c_str(), errno);
							break;
						}
					}
					else
					{
						dlog_error("Error accessing shared memory '%s', errno %d!", SharedMemoryName.c_str(), errno);
						break;
					}
				}
				else
				{
					dlog_msg("Clearing shared memory");
					memset(memory, 0, SHARED_MEMORY_SIZE); // clear the memory as it's unclear whether OS does it
					g_memory = memory;
				}
				*ptr = memory;
				result = 0;
				dlog_msg("Shared memory '%s' successfully acquired", SharedMemoryName.c_str());
			} while(false);
			//semRes = sem_post(g_semaphore);
			//dlog_msg("Semaphore post result %d (errno %d)", semRes, errno);
		}
		else
		{
			dlog_error("Waiting for semaphore has failed, errno %d! Semaphore %p", errno, g_semaphore);
		}
		return result;
	}
	
	int CSharedMemoryManager::close_shared_memory(void * ptr)
	{
		int result = 0;
		if (!g_memory)
		{
			result = shm_close(ptr);
			dlog_msg("Shared memory close result %d, errno %d", result, errno);
		}
		// Else ignore it - the creator cannot close access to shared memory as the OS then reclaims it
		int semRes = sem_post(g_semaphore);
		dlog_msg("Semaphore post result %d (errno %d)", semRes, errno);
		semRes = sem_close(g_semaphore);
		dlog_msg("Semaphore close result %d (errno %d)", semRes, errno);
		g_semaphore = 0;
		return result;
	}
}

