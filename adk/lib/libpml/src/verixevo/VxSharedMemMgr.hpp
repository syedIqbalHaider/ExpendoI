#ifndef EMEA_SHAREDMEM_MANAGER_VXOS_HPP
#define EMEA_SHAREDMEM_MANAGER_VXOS_HPP

#ifndef __cplusplus
#  error "This file is for C++ only!"
#endif

/***************************************************************************
**
 *
 * Copyright (C) 2006 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 *
 **************************************************************************/

/**
 * @file   VxSharedMem.hpp
 *
 * @author  Kamil Pawlowski (Kamil_P1@verifone.com)
 *
 * @brief   Helper layer on top of standard VerixV calls
 *          Implements shared memory access, synchronized using semaphores
 *
 * @remarks  This file should be compliant with Verifone EMEA R&D C++ Coding
 *           Standard 1.0.2
 */

#include <svc.h>

#include <string>
#include <map>

namespace com_verifone_VxSharedMemoryMgr
{
	class CSharedMemoryManager;
	typedef struct _smid
	{
		std::string sharedMemName;
		int pid;
		_smid(const std::string &smn, int piid): sharedMemName(smn), pid(piid) {}
		bool operator<(const _smid & rhs) const { if (this->pid < rhs.pid) return true; else if (this->pid > rhs.pid) return false; else return sharedMemName.compare(rhs.sharedMemName) < 0; }
		//bool operator>(const _smid & rhs) const { return this->pid > rhs.pid; }
		bool operator>(const _smid & rhs) const { if (this->pid > rhs.pid) return true; else if (this->pid < rhs.pid) return false; else return sharedMemName.compare(rhs.sharedMemName) > 0; }
		bool operator==(const _smid & rhs) const { return this->pid == rhs.pid && this->sharedMemName == rhs.sharedMemName; }
	} SharedMemId;
	//typedef std::map<int, CSharedMemoryManager *> threadMapper;
	typedef std::map<SharedMemId, CSharedMemoryManager *> threadMapper;
	class CSharedMemoryManager
	{
		public:
			static CSharedMemoryManager * getInstance(const std::string & sharedMemName);
			//~CSharedMemoryManager();
			/*
			* Gets pointer to read/write shared memory
			* Size is constant, declared in CPP file
			*/
			int get_shared_memory(void ** ptr);
			int close_shared_memory(void * ptr);
			//int destroy_shared_memory();
		private:
			static threadMapper threadsMap;
			CSharedMemoryManager(const std::string & sharedMemName); // noncopyable
			CSharedMemoryManager(const CSharedMemoryManager &); // noncopyable
			CSharedMemoryManager & operator=(const CSharedMemoryManager &); // noncopyable
			
			std::string makeSemaphoreName(const std::string & sharedMemName);
			const std::string SemaphoreName;
			
			const std::string SharedMemoryName;
			
			void * g_memory;
			sem_t * g_semaphore;
	};
} //namespace

#endif // EMEA_SHAREDMEM_MANAGER_VXOS_HPP
