/*
 * =====================================================================================
 *
 *       Filename:  application_api.cpp
 *
 *    Description:  Application API for eVo
 *
 *        Version:  1.0
 *        Created:  12/11/2013 01:54:26 PM
 *       Revision:  none
 *       Compiler:  armcc
 *
 *         Author:  Lucjan Bryndza (lb), Lucjan_B1@verifone.com
 *   Organization:  VERIFONE
 *
 * =====================================================================================
 */
#include <svc.h>
#include <errno.h>

#include <liblog/logsys.h>

namespace com_verifone_pml {


namespace
{
    static char TASK_SEMAPHORE[] = "L_TASK_INIT";
    static char TASK_LOCK_SEMAPHORE[] = "L_TASK_READY";
}

/** 
 * Notify application launcher to complete initialization 
 * @return PML error code status 
 */
int app_notify_initialization_complete()
{
    sem_t * g_semaphore = sem_open(TASK_SEMAPHORE, 0);
    if (reinterpret_cast<int>(g_semaphore) == -1)
    {
        dlog_error("Cannot open global semaphore %s, errno %d", TASK_SEMAPHORE, errno);
        return -1;
    }
    sem_post(g_semaphore);
    sem_close(g_semaphore);

    // dlog_msg("Task initialized");
    return 0;
}


/** 
 * Notify application launcher to complete initialization 
 * @return PML error code status 
 */
int app_wait_for_all( int timeout ) 
{
/*
    sem_t * g_semaphore = sem_open(TASK_LOCK_SEMAPHORE, 0);
    if (reinterpret_cast<int>(g_semaphore) == -1)
    {
        dlog_error("Cannot open global semaphore %s, errno %d", TASK_LOCK_SEMAPHORE, errno);
        return -1;
    }
    sem_wait(g_semaphore);
    sem_close(g_semaphore);

    dlog_msg("Launcher is done!");
*/
    return 0;
}


}
