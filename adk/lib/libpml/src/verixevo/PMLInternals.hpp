/*****************************************************************************
 * 
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file        PMLInternal.hpp
 *
 * @author      Kamil Pawlowski
 * 
 * @brief       Internal class implementing PML on Verix (Trident) devices
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C Coding  
 *              Standard 1.0.0 
 */

#include <vector>
#include <map>
#include <queue>

#include <libpml/pml.h>

namespace com_verifone_pml_events_internals
{
	static const int INVALID_HANDLE = -1;

	bool operator==(const com_verifone_pml::event_item & lhs, const com_verifone_pml::event_item & rhs);
	bool operator!=(const com_verifone_pml::event_item & lhs, const com_verifone_pml::event_item & rhs);

	/* 
	 * Base event handling classes declarations
	 */
	// Pure virtual base event handling class
	// A base for most events handler, like comms event
	class PMLInternalHandler
	{
		public:
			PMLInternalHandler(const com_verifone_pml::event_item & evt);
			virtual ~PMLInternalHandler();
			virtual int read_event(long event) const
				{ return check_event(event); }
			virtual int check_event(long event) const = 0;
			virtual int check_pending_event() const = 0;
			virtual bool is_valid() const = 0;
			int get_event_mask() const
				{ return m_event; }
			int get_handle() const
				{ return m_handle; }
			com_verifone_pml::event_item read_item()
				{ return m_event_item; }
			virtual com_verifone_pml::event_item get_item() // gets updated item
				{ return m_event_item; }
		protected:
			// virtual bool compare(const PMLEvent & rhs) const = 0;
			int m_event;
			int m_handle;
			com_verifone_pml::event_item m_event_item;
	};
	
	// Pure virtual notification event handling class
	// (Notification event means that event just occurs, like terminal dock or clock tick)
	class PMLInternalHandlerNotification: public PMLInternalHandler
	{
		public:
			PMLInternalHandlerNotification(const com_verifone_pml::event_item & evt): PMLInternalHandler(evt), event_pending(false)
				{ }
			virtual ~PMLInternalHandlerNotification();
			virtual int read_event(long event) const
				{ int res = check_event(event); if (res) read_evt(m_event); event_pending = false; return res; }
			virtual int check_event(long event) const = 0;
			virtual int check_pending_event() const;
			virtual bool is_valid() const = 0;
			void set_pending_event() const { event_pending = true; }
		private:
			mutable bool event_pending;
	};
	
	// Pure virtual notification event handling class, who require dedicated thread to perform some check periodically
	// This is basically implemented to compensate Verix architecture deficiencies
	// An example is battery level event - Verix doesn't offer one
	class PMLInternalHandlerThread: public PMLInternalHandlerNotification
	{
		public:
			typedef int (*thread_routine_t)(void *);
			//PMLInternalHandlerThread(const com_verifone_pml::event_item & evt, unsigned long notify, thread_routine_t thread_routine, void * thread_params, int thread_stack = 1024);
			PMLInternalHandlerThread(const com_verifone_pml::event_item & evt, unsigned long notify);
			virtual ~PMLInternalHandlerThread();
			virtual int check_event(long event) const = 0;
			virtual bool is_valid() const = 0;
			bool is_handle_valid() const
				{ return thread_handle != INVALID_HANDLE; }
			unsigned long get_notification_flag() const
				{ return notification_flag; }
		private:
			int thread_handle;
		protected:
			bool fire_thread(thread_routine_t thread_routine, void * thread_params, int thread_stack = 1024);
			const unsigned long notification_flag;
	};
	
	
	/*
	 * Clock event handler
	 */
	class PMLClockEventHandler: public PMLInternalHandlerNotification
	{
		public:
			PMLClockEventHandler(const com_verifone_pml::event_item & evt);
			virtual ~PMLClockEventHandler();
			virtual int check_event(long event) const;
			//virtual int check_pending_event() const;
			virtual bool is_valid() const
				{ return m_has_clock; }
		private:
			const bool m_has_clock;
	};
	
	/*
	 * Internal communication event handler (via IPC library) 
	 */
	class PMLInternalCommsEventHandler: public PMLInternalHandler
	{
		public:
			PMLInternalCommsEventHandler(const com_verifone_pml::event_item & evt);
			virtual ~PMLInternalCommsEventHandler();
			virtual int check_event(long event) const;
			virtual int check_pending_event() const;
			virtual bool is_valid() const;
			virtual int read_event(long event) const
				{ int res = check_event(event); if (res) read_evt(m_event); event_pending = false; return res; }
		private:
			mutable bool event_pending;
	};
	
	/*
	 * External communication (Serial ports only!) event handler
	 */
	class PMLExternalCommsEventHandler: public PMLInternalHandler
	{
		public:
			PMLExternalCommsEventHandler(const com_verifone_pml::event_item & evt);
			virtual ~PMLExternalCommsEventHandler();
			virtual int check_event(long event) const;
			virtual int check_pending_event() const;
			virtual bool is_valid() const
				{ return m_handle != INVALID_HANDLE && m_event != 0; }
		private:
			mutable bool m_workaround;
	};
	
	/*
	 * Timer event handler
	 */
	class PMLTimerEventHandler: public PMLInternalHandlerNotification
	{
		public:
			PMLTimerEventHandler(const com_verifone_pml::event_item & evt);
			virtual ~PMLTimerEventHandler();
			virtual int check_event(long event) const;
			//virtual int check_pending_event() const;
			virtual bool is_valid() const
				{ return m_timer_valid; }
			virtual com_verifone_pml::event_item get_item();
		private:
			mutable bool m_timer_valid;
			const int m_timerId;
			mutable int m_counter;
			bool validate_timer(const com_verifone_pml::event_item & evt);
	};
	
	/*
	 * EOS event handler (cable removal, signal strength etc.)
	 */
	class PMLEOSHandler: public PMLInternalHandler
	{
		public:
			PMLEOSHandler(const com_verifone_pml::event_item & evt);
			virtual ~PMLEOSHandler();
			virtual int check_event(long event) const = 0;
			virtual bool is_valid() const
				{ return m_has_network; }
			virtual com_verifone_pml::event_item get_item() = 0;
			virtual int check_pending_event() const = 0;
		protected:
			static bool monitor_netlink;
			static bool monitor_signal;
			typedef std::queue<com_verifone_pml::event_item> item_queue;
			static item_queue netlink_queue;
			static item_queue signal_queue;
			int process_eos_events();
			int initial_status(bool send_netlink, bool send_signal);
			int check_eos_event() const;

		private:
			static bool m_unregister;
			static bool m_disable_event_notifications;
			static bool m_disable_signal_notifications;
			static bool m_has_network;
			static bool m_initialized;
			static int m_timer_id;
			bool check_network();

			typedef std::map<int, std::string> IFACE_NAMES;
			IFACE_NAMES setup_network_interfaces();
			static IFACE_NAMES interface_names;

	};
	
	/*
	 * Netlink event handler (cable removal)
	 */
	class PMLNetlinkEventHandler: public PMLEOSHandler
	{
		public:
			PMLNetlinkEventHandler(const com_verifone_pml::event_item & evt);
			virtual ~PMLNetlinkEventHandler();
			virtual int check_event(long event) const;
			virtual com_verifone_pml::event_item get_item();
			virtual int check_pending_event() const
				{ return netlink_queue.size() > 0; }
	};
	
	/*
	 * Signal strength event handler
	 */
	class PMLSignalEventHandler: public PMLEOSHandler
	{
		public:
			PMLSignalEventHandler(const com_verifone_pml::event_item & evt);
			virtual ~PMLSignalEventHandler();
			virtual int check_event(long event) const;
			virtual com_verifone_pml::event_item get_item();
			virtual int check_pending_event() const
				{ return signal_queue.size() > 0; }
	};
	
	/*
	 * Keyboard event handler
	 */
	class PMLKeyboardEventHandler: public PMLInternalHandler
	{
		public:
			PMLKeyboardEventHandler(const com_verifone_pml::event_item & evt);
			virtual ~PMLKeyboardEventHandler();
			virtual int check_event(long event) const;
			virtual int check_pending_event() const;
			virtual bool is_valid() const
				{ return m_has_kbd; }
		private:
			const bool m_has_kbd;
	};
	
	/*
	 * ICC event handler (only supports internal readers)
	 */
	class PMLICCEventHandler: public PMLInternalHandler
	{
		public:
			PMLICCEventHandler(const com_verifone_pml::event_item & evt);
			virtual ~PMLICCEventHandler();
			virtual int check_event(long event) const;
			virtual int check_pending_event() const;
			virtual bool is_valid() const
				{ return m_has_icc; }
		private:
			const bool m_has_icc;
	};
	
	/*
	 * Magstripe event handler
	 */
	class PMLMagEventHandler: public PMLInternalHandler
	{
		public:
			PMLMagEventHandler(const com_verifone_pml::event_item & evt);
			virtual ~PMLMagEventHandler();
			virtual int check_event(long event) const;
			virtual int check_pending_event() const;
			virtual bool is_valid() const
				{ return m_has_mag; }
		private:
			const bool m_has_mag;
			bool validate_mag_presence() const;
	};
	
	/*
	 * Touch event handler (simulated; mapped to Verix event)
	 */
	class PMLTouchEventHandler: public PMLInternalHandlerNotification
	{
		public:
			PMLTouchEventHandler(const com_verifone_pml::event_item & evt);
			virtual ~PMLTouchEventHandler();
			virtual int check_event(long event) const;
			//virtual int check_pending_event() const;
			virtual bool is_valid() const
				{ return true; }
	};
	
	/*
	 * Case removal event handler (cannot be tested, no Verix eVo terminal supports it)
	 */
	class PMLCaseRemovalEventHandler: public PMLInternalHandlerNotification
	{
		public:
			PMLCaseRemovalEventHandler(const com_verifone_pml::event_item & evt);
			virtual ~PMLCaseRemovalEventHandler();
			virtual int check_event(long event) const;
			virtual bool is_valid() const
				{ return true; }
	};
	
	/*
	 * Activate event handler
	 */
	class PMLActivateEventHandler: public PMLInternalHandlerNotification
	{
		public:
			PMLActivateEventHandler(const com_verifone_pml::event_item & evt);
			virtual ~PMLActivateEventHandler();
			virtual int check_event(long event) const;
			virtual bool is_valid() const
				{ return true; }
	};
	
	/*
	 * USB slave device event handler (reports USB device connection / disconnection)
	 */
	class PMLUSBDeviceEventHandler: public PMLInternalHandlerNotification
	{
		public:
			PMLUSBDeviceEventHandler(const com_verifone_pml::event_item & evt);
			virtual ~PMLUSBDeviceEventHandler();
			virtual int check_event(long event) const;
			virtual bool is_valid() const
				{ return true; }
	};
	
	
	
	/*
	 * Battery event handler (thread one)
	 */
	class PMLBatteryEventHandler: public PMLInternalHandlerThread
	{
		public:
			PMLBatteryEventHandler(const com_verifone_pml::event_item & evt);
			virtual ~PMLBatteryEventHandler();
			virtual int check_event(long event) const;
			virtual bool is_valid() const;
			virtual com_verifone_pml::event_item get_item();
		private:
			void * generate_thread_params(const com_verifone_pml::event_item & evt);
			void * thread_params;
	};
	
	
	/*
	 * Tamper event handler (thread one)
	 */
	class PMLTamperEventHandler: public PMLInternalHandlerThread
	{
		public:
			PMLTamperEventHandler(const com_verifone_pml::event_item & evt);
			virtual ~PMLTamperEventHandler();
			virtual int check_event(long event) const;
			virtual bool is_valid() const;
			virtual com_verifone_pml::event_item get_item();
		private:
			void * generate_thread_params(const com_verifone_pml::event_item & evt);
			void * thread_params;
	};

	/*
	 * Barcode event handler
	 */
	class PMLBarcodeEventHandler: public PMLInternalHandlerNotification
	{
		public:
			PMLBarcodeEventHandler(const com_verifone_pml::event_item & evt);
			virtual ~PMLBarcodeEventHandler();
			virtual int check_event(long event) const;
			virtual bool is_valid() const
				{ return true; }
	};


	
	/*
	 * User event handler
	 */
	class PMLUserEventHandler: public PMLInternalHandlerNotification
	{
		public:
			PMLUserEventHandler(const com_verifone_pml::event_item & evt);
			virtual ~PMLUserEventHandler();
			virtual int check_event(long event) const;
			virtual bool is_valid() const
				{ return flag != 0; }
			virtual com_verifone_pml::event_item get_item(); // gets updated item
		private:
			const unsigned long flag;
	};
	
	
	
	/*
	Class representing an event for events collection
	*/
	class PMLEvent
	{
		public:
			PMLEvent(const com_verifone_pml::event_item & evt);
			~PMLEvent();

			int read_event(long event) const
				{ return our_event(event, true); }
			int check_event(long event) const
				{ return our_event(event, false); }
			int check_pending_event() const;
			long get_event_mask() const
				{ if (is_valid()) return m_handler->get_event_mask(); else return 0; }
			com_verifone_pml::events::evt get_event_id() const
				{ return m_handler->read_item().event_id; }
			bool is_valid() const
				{ return m_handler != 0 && m_handler->is_valid(); }
			com_verifone_pml::event_item read_item() const
				{ return m_handler->read_item(); }
			com_verifone_pml::event_item get_item() const
				{ return m_handler->get_item(); }
			friend bool operator==(const PMLEvent & lhs, const PMLEvent & rhs);

		private:
			PMLInternalHandler * m_handler;
			//com_verifone_pml::event_item item;
			int our_event(long event, bool read) const;
			PMLInternalHandler * create_handler(const com_verifone_pml::event_item & evt);

			PMLEvent(const PMLEvent & _pmlEvent); // copy constructor
			PMLEvent & operator=(PMLEvent & ev); // assignment operator

	};

	/*
	Class representing event collection (handles returned outside refer to this object)
	*/
	class PMLCollection
	{
		public:
			typedef std::vector<int> timer_id_list_t;
			
		public:
			PMLCollection(int handle);
			~PMLCollection();
			int ctl(const com_verifone_pml::ctl::op_t, const com_verifone_pml::event_item & event);
			int ctl(const com_verifone_pml::ctl::op_t, PMLCollection * const add_ptr);
			long get_event_mask() const;
			int read_our_events(const long lnEvents, com_verifone_pml::eventset_t & events) const
				{ return our_events(lnEvents, events, true); }
			int check_our_events(const long lnEvents, com_verifone_pml::eventset_t & events) const
				{ return our_events(lnEvents, events, false); }
			int check_pending_events(com_verifone_pml::eventset_t & events) const;
			bool has_pending_events() const;
			int close(PMLCollection const * ptr);
			//int wait(com_verifone_pml::eventset_t & retEvents, long timeout);
			bool is_valid() { return get_event_mask() != 0; }
			int get_handle() const { return ourHandle; }
			
			std::size_t get_all_timers(timer_id_list_t & timerIdList) const;

		private:
			typedef std::vector<PMLEvent *> eventset_t;
			eventset_t eventSet;
			typedef std::vector<PMLCollection *> collectionset_t;
			collectionset_t collectionSet;
			long eventMask_internal;
			const int ourHandle;
			mutable bool fHasPendingEvents;
		
		private:
			bool we_have_event(PMLEvent const * event) const;
			bool we_have_collection(PMLCollection * const add_ptr) const;
			long update_mask();
			int our_events(const long lnEvents, com_verifone_pml::eventset_t & events, bool read) const;
	};


	class PMLEventHandler
	{
		public:
			static PMLEventHandler * getInstance()
			{
				if (!ptr)
				{
					ptr = new PMLEventHandler();
					assert(ptr);
				}
				return ptr;
			}
			int create();
			//int create(const com_verifone_pml::handle_set_t & handleList);
			int close(const int handle);
			int init(const char * const name);
			int deinit(const char * const name);

			bool is_handle_valid(const int handle) const;
			int ctl(const int handle, const com_verifone_pml::ctl::op_t op, const com_verifone_pml::eventset_t & event);
			int ctl(const int handle, const com_verifone_pml::ctl::op_t op, const int add_handle);
			int wait(const int handle, com_verifone_pml::eventset_t &revents, long timeuot);
			int raise(const char * name, const com_verifone_pml::event_item & event);
			int raise(int tid, const com_verifone_pml::event_item & event);

			// versions
			int set_version(const char version []);
			int register_library( const char libname[], const char version[] );
			int get_apps_list( com_verifone_pml::appver::applist_t &apps );
			int get_lib_list( com_verifone_pml::appver::applist_t& liblist, const char *appname = NULL );
			std::string get_app_version(const char appName []);
			std::string get_our_version() { return get_app_version(ourName.c_str()); }

		private:
			static PMLEventHandler *ptr;

			PMLEventHandler();
			~PMLEventHandler();

			typedef std::map<int, PMLCollection *> mapEvents;
			mapEvents myMap;

			PMLCollection * getEventsHandler(const int handle) const;

			std::string ourName;
	};

} // namespace
