/*
	This is an implementation for library for software timers.
	It allows developer to use a single system timer mask and
	use software dispatcher to multiply the number
*/

#include "SoftTimer.h"

#include <svc.h>
#include <assert.h>
#include <liblog/logsys.h>

#include <algorithm>
#include <functional>

// Uncomment this to have a detailed logs for timer internal actions
#define SOFT_TIMER_DEBUG

#ifdef SOFT_TIMER_DEBUG
	#define local_dlog_msg(...)		dlog_msg(__VA_ARGS__)
#else
	#define local_dlog_msg(...)		do {} while(0)
#endif

namespace com_verifone_SoftTimer
{
	namespace Internal
	{
		static const long MAX_ALLOWED_TIMER = 86400000; // one day - maximum allowed timer period

		bool TimerState::isFired(TimeStamp_t now) const
		{
			TimeStamp_t diff = now - getFireTime();
			bool isFired = isRunning() && (getFireTime() <= now);
			local_dlog_msg("isFired=%i (%lu<=%lu, run=%i)", isFired, getFireTime().get(), now.get(), isRunning());

			return isFired;
		}
		
		void TimerState::setTimer(TimeDiff_t milliseconds, bool repeat)
		{ 
			local_dlog_msg("TimerState::setTimer:time:%li", milliseconds);

			m_fireTime = TimeStamp_t(::read_ticks() + milliseconds); 
			m_repeatTime = (repeat ? milliseconds : 0); 
			
			// NOTE: milliseconds == 0 means a STOP. 
			m_timer_runs = (milliseconds != 0);
		}
	} // namespace Internal


	
	// Singleton ptr
	SoftTimerId* SoftTimerId::ptr = 0;
	
	SoftTimerId* SoftTimerId::getInstance()
	{
		if (ptr == 0)
		{
			ptr = new SoftTimerId;
			assert(ptr);
		}
		return ptr;
	}

	SoftTimerId::SoftTimerId()
	{
	}
	
	TimerId_t SoftTimerId::createNewId() const
	{
		TimerId_t id = 0;
		
		do
		{
			id = rand();
			if (id == INVALID_ID) 
				continue;
			
			if (timerCollection_.find(id) == timerCollection_.end())
				break;
			
			SVC_WAIT(0);
		} while(true);
		
		// new unique id found
		return id;
	}
	
	bool SoftTimerId::isValid(TimerId_t timerId) const
	{
		if (timerId != INVALID_ID)
		{
			com_verifone_VIPAParser::ScopedLock lock(mutex_);
			
			timer_collection_t::const_iterator it = timerCollection_.find( timerId );
			if (it != timerCollection_.end())
			{
				// In the collection - it is valid
				return true;
			}
		}
		return false;
	}
	
	SoftTimerId::timer_collection_t::iterator SoftTimerId::getTimerIterator(TimerId_t timerId)
	{
		if (timerId != INVALID_ID)
		{
			// NOTE: Intentionally no lock here as this is internal method - should be locked above
			timer_collection_t::iterator it = timerCollection_.find(timerId);
			return it;
		}

		return timerCollection_.end();
	}
	
	SoftTimerId::timer_collection_t::mapped_type * SoftTimerId::getTimerObj(TimerId_t timerId)
	{
		// NOTE: Intentionally no lock here as this is internal method - should be locked above
		timer_collection_t::iterator it = getTimerIterator(timerId);

		if (it != timerCollection_.end())
		{
			return &(it->second);
		}
		
		return 0;
	}
	
	
	namespace
	{
		class FireListItemGreaterThanTimestamp
		{
		public:
			FireListItemGreaterThanTimestamp(SoftTimerId::TimeStamp_t refTimestamp)
			: timestamp_(refTimestamp)
			{}
			
			bool operator()(SoftTimerId::timer_fire_list_t::const_reference fireListItem)
			{
				return fireListItem.fireTime > timestamp_;
			}
			
		private:
			SoftTimerId::TimeStamp_t timestamp_;
		};
		
	}
	
	void SoftTimerId::insertToFireList(TimerId_t timerId, timer_collection_t::mapped_type & timerCtx)
	{	
		// insert to the list if running!
		if (timerCtx.state.isRunning())
		{
			timer_fire_list_t::value_type timerOrdering;
			timerOrdering.fireTime = timerCtx.state.getFireTime();
			timerOrdering.timerId = timerId;

			FireListItemGreaterThanTimestamp pred(timerOrdering.fireTime);
			timer_fire_list_t::iterator it = std::find_if(fireList_.begin(), fireList_.end(), pred);

			timerCtx.fireIterator = fireList_.insert(it, timerOrdering);
		}
		else
		{
			timerCtx.fireIterator = fireList_.end();
		}
	}
	
	void SoftTimerId::removeFromFireList(timer_collection_t::mapped_type & timerCtx)
	{
		if (timerCtx.fireIterator != fireList_.end())
			fireList_.erase(timerCtx.fireIterator);
	}
	
	TimerId_t SoftTimerId::setTimer(long milliseconds, bool repeat /* = false */)
	{
		com_verifone_VIPAParser::ScopedLock lock(mutex_);
		
		TimerId_t timerId = createNewId();

		timer_collection_t::mapped_type timerCtx;
		timerCtx.state.setTimer(milliseconds, repeat);
		insertToFireList(timerId, timerCtx);

		timerCollection_.insert(std::make_pair(timerId, timerCtx));
		
		return timerId;
	}
	
	
	int SoftTimerId::changeTimer(TimerId_t timerId, long milliseconds, bool repeat /* = false */)
	{
		com_verifone_VIPAParser::ScopedLock lock(mutex_);
		
		timer_collection_t::mapped_type * pTimerCtx = getTimerObj(timerId);
		local_dlog_msg("tmr(%i)=%p", timerId, pTimerCtx);

		
		if (pTimerCtx)
		{
			timer_collection_t::mapped_type & timerCtx = *pTimerCtx;
			
			removeFromFireList(timerCtx);
			timerCtx.state.setTimer(milliseconds, repeat);
			insertToFireList(timerId, timerCtx);

			return timerId;
		}

		return -1;
	}

	void SoftTimerId::clrTimer(TimerId_t timerId)
	{
		com_verifone_VIPAParser::ScopedLock lock(mutex_);
		
		timer_collection_t::iterator it = getTimerIterator(timerId);

		if (it != timerCollection_.end())
		{
			timer_collection_t::mapped_type & timerCtx = it->second;
			
			removeFromFireList(timerCtx);
			timerCollection_.erase(it);
			
			local_dlog_msg("Cleared timer id=%i", timerId);
		}
	}
	
	void SoftTimerId::stopTimer(TimerId_t timerId)
	{
		com_verifone_VIPAParser::ScopedLock lock(mutex_);
		
		timer_collection_t::mapped_type * pTimerCtx = getTimerObj(timerId);
		
		if (pTimerCtx)
		{
			timer_collection_t::mapped_type & timerCtx = *pTimerCtx;
			removeFromFireList(timerCtx);
			timerCtx.state.setTimer(0, false);
		}
	}
	
	
	bool SoftTimerId::isRunning(TimerId_t timerId)
	{
		com_verifone_VIPAParser::ScopedLock lock(mutex_);
		
		timer_collection_t::mapped_type * pTimerCtx = getTimerObj(timerId);
		
		if (pTimerCtx)
		{
			timer_collection_t::mapped_type & timerCtx = *pTimerCtx;
			return timerCtx.state.isRunning();
		}
		
		return false;
	}
	
	bool SoftTimerId::isTimerFired(TimerId_t timerId)
	{
		com_verifone_VIPAParser::ScopedLock lock(mutex_);
		
		timer_collection_t::mapped_type * pTimerCtx = getTimerObj(timerId);
		
		if (pTimerCtx)
		{
			timer_collection_t::mapped_type & timerCtx = *pTimerCtx;
			
			// TODO: Should in fact look only in fireList_!
			return timerCtx.state.isFired();
		}
		
		return false;
	}
	
	bool SoftTimerId::updateTimer(TimerId_t timerId)
	{
		com_verifone_VIPAParser::ScopedLock lock(mutex_);

		timer_collection_t::mapped_type * pTimerCtx = getTimerObj(timerId);
		
		if (pTimerCtx)
		{
			timer_collection_t::mapped_type & timerCtx = *pTimerCtx;
			
			timer_fire_list_t::iterator itFire = timerCtx.fireIterator;
			if (itFire != fireList_.end())
			{
				fireList_.erase(itFire);
				timerCtx.fireIterator = fireList_.end();
				
				if (timerCtx.state.update())
				{
					// timer still running - add it back to the list
					insertToFireList(timerId, timerCtx);
				}
				return true;
			}
		}
		
		return false;
	}
	
	// NOTE: This function gets ALL the fired timers created (for all the threads) so it seems it should be removed
	timer_id_list_t SoftTimerId::getFiredTimers()
	{
		com_verifone_VIPAParser::ScopedLock lock(mutex_);
		timer_id_list_t expiredTimers;
		
		TimeStamp_t now = TimeStamp_t::now();
		
		// NOTE: This is ordered list so the first one not fired stops the search. Also only running timers are here
		for (timer_fire_list_t::const_iterator it = fireList_.begin(); it != fireList_.end(); ++it)
		{
			if (it->fireTime < now)
			{
				expiredTimers.push_back(it->timerId);
			}
			else
				break;
		}
		
		local_dlog_msg("We have %lu fired timer events", expiredTimers.size());
		return expiredTimers;
	}

	
	
	namespace
	{
		class CompareFireListItemToIdPred
		{
		public:
			bool operator()(SoftTimerId::timer_fire_list_t::const_reference fireListItem, timer_id_list_t::value_type timerId)
			{
				return fireListItem.timerId == timerId;
			}
		};
	}
	
	bool SoftTimerId::setSysTimer(timer_id_list_t timerList)
	{
		local_dlog_msg("timerList.size()=%lu", timerList.size());
		if (timerList.size() == 0)
			return false;
		
		// Only one timerMask for all threads
		SysEventMask_t timerMask = EVT_TIMER;
		com_verifone_VIPAParser::ScopedLock lock(mutex_);
		
		int taskId = get_task_id();
		clrSysTimer(taskId);
		
		// find nearest timer for the given list
		timer_fire_list_t::const_iterator it = std::find_first_of(fireList_.begin(), fireList_.end(), timerList.begin(), timerList.end(), CompareFireListItemToIdPred());

		TimeStamp_t now = TimeStamp_t::now();
		Internal::TimeDiff_t milliseconds = it->fireTime - now;
		local_dlog_msg("nearest timer id=%i, time=%lu, now=%lu, diff=%li", it->timerId, it->fireTime.get(), now.get(), milliseconds);
		
		// If the timer managed to be in the past - trigger the event as soon as possible
		if (milliseconds < 0)
			milliseconds = 0;
			
		int sysTimerId = ::set_timer(milliseconds, timerMask);
		local_dlog_msg("sys set_timer(%lu, %08x)=%i", milliseconds, timerMask);
		
		if (sysTimerId < 0)
			return false;
		
		sysTimerCollection_.insert(std::make_pair(taskId, sysTimerId));
		return true;
	}
	
	void SoftTimerId::clrSysTimer()
	{
		int taskId = get_task_id();

		com_verifone_VIPAParser::ScopedLock lock(mutex_);
		clrSysTimer(taskId);
	}
	
	void SoftTimerId::clrSysTimer(int taskId)
	{
		sys_timer_collection_t::iterator it = sysTimerCollection_.find(taskId);
		if (it != sysTimerCollection_.end())
		{
			int sysTimerId = it->second;
			::clr_timer(sysTimerId);
			sysTimerCollection_.erase(it);
		}
	}

} // namespace com_verifone_SoftTimer
