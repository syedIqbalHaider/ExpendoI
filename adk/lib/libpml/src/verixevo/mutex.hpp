/*
 * mutex.hpp
 *
 *  Created on: 22 Jun 2011
 *      Author: T_Stanislaw_P1
 */

#ifndef MUTEX_HPP_
#define MUTEX_HPP_

#ifdef VFI_PLATFORM_VERIXEVO
	#include <svc.h>
#else
	#include <semaphore.h>
#endif

namespace com_verifone_VIPAParser
{
	class Mutex{
			sem_t sem;
		public:
			Mutex(bool enabled = true){
#ifdef VFI_PLATFORM_VERIXEVO
				sem_init (&sem, enabled);
#else
				sem_init (&sem, 0, enabled);
#endif				
			}
			~Mutex(){
#ifndef VFI_PLATFORM_VERIXEVO
			  sem_destroy(&sem);
#endif			  
			}
			void wait(){
				sem_wait(&sem);
			}
			void post(){
				sem_post(&sem);
			}
		};
		class ScopedLock{
			Mutex &mx;
		public:
			ScopedLock(Mutex &mx):mx(mx){
				this->mx.wait();
			}
			~ScopedLock(){
				mx.post();
			}
	};
}
#endif /* MUTEX_HPP_ */
