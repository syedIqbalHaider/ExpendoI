/******************************************************************************

  FILE: Ux.cpp

  DESC: This file is used to handle UX100 unattended device
******************************************************************************/

#include <liblog/logsys.h>

#include "libpml/ux.h"

ux::ux(): initialized(false), haveKeyboard(false), haveDisplay(false)
{
    // do nothing on eVo
}

ux::~ux()
{
    //if (handle != NULL) dlclose(handle);
}

int ux::pinStartPinEntry(unsigned char /*pinLenMin*/, unsigned char /*pinLenMax*/, int /*timeout*/)
{
    return -1;
}
int ux::pinAbort()
{
    return -1;
}
int ux::pinDelete(int /*cnt*/)
{
    return -1;
}
int ux::pinSendEncPinBlockToVault()
{
    return -1;
}
int ux::pinGetNbCurrDigitEntered()
{
    return -1;
}
int ux::pinEntryMode()
{
    return -1;
}
int ux::keybdInit()
{
    return -1;
}
int ux::keybdGetKey(unsigned int /*timeout*/)
{
    return -1;
}
int ux::dispSetBacklightMode(int /*mode*/, unsigned int /*time*/)
{
    return -1;
}
int ux::getVersions(char * /*id*/, size_t /*idSize*/, 
                    char * /*family*/, size_t /*familySize*/, 
                    char * /*version*/, size_t /*versionSize*/)
{
    return -1;
}
int ux::getVersion(char * /*ver*/, size_t /*verSize*/)
{
    return -1;
}
int ux::getSerialNo(char * /*sn*/, size_t /*snSize*/)
{
    return -1;
}
int ux::getBareSerialNo(char * /*sn*/, size_t /*snSize*/)
{
    return -1;
}
int ux::getPartNo(char * /*pn*/, size_t /*pnSize*/)
{
    return -1;
}
int ux::getHardwareVer(char * /*hw*/, size_t /*hwSize*/)
{
    return -1;
}
int ux::getModelNumber(char *mn, size_t mnSize)
{
    return -1;
}
int ux::getPTID(char *ptid, size_t ptidSize)
{
    return -1;
}
int ux::getRemovalSwitchStatus()
{
    return -1;
}
int ux::getKbdStatus(unsigned char *secure_mode, unsigned char *nb_function_key, unsigned char *nb_normal_key, unsigned char *nb_row, unsigned char *nb_col, unsigned char *key_map)
{
    return -1;
}	

int ux::isKbdSecure()
{
	return -1;
}

int ux::setKeyboardBeep(bool /*enable*/)
{
    return -1;
}
int ux::setContrast(int /*value*/)
{
    return -1;
}
int ux::getTamperStatus()
{
    return -1;
}


