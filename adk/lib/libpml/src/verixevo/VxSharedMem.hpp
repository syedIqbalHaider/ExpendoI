#ifndef EMEA_SHAREDMEM_VXOS_HPP
#define EMEA_SHAREDMEM_VXOS_HPP

#ifndef __cplusplus
#  error "This file is for C++ only!"
#endif

/***************************************************************************
**
 *
 * Copyright (C) 2006 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 *
 **************************************************************************/

/**
 * @file   VxSharedMem.hpp
 *
 * @author  Kamil Pawlowski (Kamil_P1@verifone.com)
 *
 * @brief   Helper layer on top of standard VerixV calls
 *          Implements shared memory access, synchronized using semaphores
 *
 * @remarks  This file should be compliant with Verifone EMEA R&D C++ Coding
 *           Standard 1.0.2
 */

#include <svc.h>
#include <string>
#include <vector>

#include <libpml/pml.h>

namespace com_verifone_VxSharedMemory
{
	typedef std::vector<int> id_set_t;
	// Task names management
	bool add_name(const char * const name);
	bool delete_name(const char * const name);
	int get_task_id(const char * const name);
	int get_all_tasks_ids(id_set_t & id_list, bool exclude_ourself = true);
	
	// Versioning
	int set_version(const char name [], const char version []);
	int register_library( const char name[], const char libname[], const char version[] );
	int get_apps_list( com_verifone_pml::appver::applist_t &apps );
	int get_lib_list( com_verifone_pml::appver::applist_t& liblist, const char *appname = NULL );
	int get_app_version( const char name[], std::string & appver );
	
	//User events
	int register_user_event(int task_id);
	int pass_value(int task_id, int value, int ext_value);
	int get_value(int task_id, int &value, int &ext_value);

	// TODO: Add virtual memory management for user events
	
	
} //namespace

#endif // EMEA_SHAREDMEM_VXOS_HPP
