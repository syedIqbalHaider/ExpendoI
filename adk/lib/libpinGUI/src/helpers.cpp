
#include <algorithm>

#include <liblog/logsys.h>
#include "helpers.h"

namespace helpers
{
      size_t str_explode(const std::string & str, const std::string & delim, std::vector<std::string> & exploded)
      {
          size_t end = 0;
          exploded.clear();
          while (end < str.size())
          {
              size_t start = end;
              while (start < str.size() && (delim.find(str[start]) != std::string::npos))
              {
                  start++;  // skip initial whitespace
              }
              end = start;
              while (end < str.size() && (delim.find(str[end]) == std::string::npos))
              {
                  end++; // skip to end of word
              }
              if (end-start != 0)
              {  // just ignore zero-length strings.
                  exploded.push_back(std::string(str, start, end-start));
              }
          }
          #ifdef DEBUG
          dlog_msg("Exploded: '%s'", str.c_str());
          for (std::vector<std::string>::const_iterator it = exploded.begin(); it != exploded.end(); ++it)
          {
              dlog_msg("Found '%s'", it->c_str());
          }
          #endif
          return exploded.size();
      }

      namespace check {
          int isvalid(int c)
          {
              if (std::isspace(c) || c == '\'' || c == '\"') return 1;
              return 0;
          }
      }

      // trim from start
      std::string &ltrim(std::string &s)
      {
          s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(check::isvalid))));
          return s;
      }

      // trim from end
      std::string &rtrim(std::string &s)
      {
          s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(check::isvalid))).base(), s.end());
          return s;
      }

      // trim from both ends
      std::string &str_trim(std::string &s)
      {
          return ltrim(rtrim(s));
      }

}



