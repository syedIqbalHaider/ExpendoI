
#include <liblog/logsys.h>
#include <libipc/ipc.h>
#include <libpml/pml_os_core.h>

#include "app_selection.h"

#ifdef VFI_GUI_GUIAPP
#include <libpml/pml.h>
#include "GuiApp.h"
using namespace com_verifone_guiapi;
#endif

#ifdef VFI_GUI_DIRECTGUI
#ifdef DIRECTGUI_PROXY
#include <libguiclient/guiclient.h>
#include <libguiclient/jsonObj.h>
#else
#ifdef DIRECTGUI2
#  include <html/gui.h>
#  include <html/jsobject.h>
#else
#  include <gui/gui.h>
#  include <gui/jsobject.h>
#endif
#endif
#include <libminini/minIni.h>
#endif

#include "capacitive_kbd.h"

#include <errno.h>
#include <algorithm>

using namespace com_verifone_pml;

namespace
{
	break_ret_t checkBreak_loc()
	{
		return NoData;
	}
	bool isICCpresent_loc()
	{
		return true;
	}
}

/* Base class */
cAppSelDisplay::cAppSelDisplay(): CheckBreak(checkBreak_loc), isICCpresent(isICCpresent_loc)
{
	//
}

/* Console class */
int cAppSelDisplayConsole::displayApplicationSelection(const char * app_list[], size_t app_list_count, int milliseconds)
{
	return 0;
}

#ifdef VFI_GUI_GUIAPP
/* GuiApp class */
namespace
{
	int procEvents(eventset_t pml_events, cAppSelDisplay * pObj)
	{
		int result = -1;
		while (!pml_events.empty())
		{
			event_item event = pml_events.back();
			pml_events.pop_back();
			if (event.event_id == events::cascade)
			{
				eventset_t cascadeEvents;
				if (event_read(event.evt.cascade.fd, cascadeEvents) > 0)
				{
					result = procEvents(cascadeEvents, pObj);
				}
			}
			else if (event.event_id == events::icc)
			{
				dlog_alert("Card removed!");
				//g_CEMV.SetStatusDetail(CARD_APP_STATUS_D_REMOVED);
				//g_CEMV.SetStatusMain(CARD_APP_STATUS_M_STOP);
				result = 1; // card removed
				break;
			}
			else if (event.event_id == events::timer)
			{
				if (pObj)
				{
					if (!pObj->isICCpresent())
					{
						dlog_alert("Card removed!");
						result = 1;
						break;
					}
					switch (pObj->CheckBreak())
					{
						case GuiAppData:
							dlog_msg("GuiApp answer");
							result = 0;
							break;
						case Cancelled:
							dlog_alert("App selection cancelled, exiting");
							result = 1;
							break;
						default:
							result = 0;
							break;
					}
					break;
				}
			}
			else if (event.event_id == events::ipc)
			{
				if (pObj)
				{
					switch (pObj->CheckBreak())
					{
						case GuiAppData:
							dlog_msg("GuiApp answer");
							result = 0;
							break;
						case Cancelled:
							dlog_alert("App selection cancelled, exiting");
							result = 1;
							break;
						default:
							result = 0;
							break;
					}
					break;
				}
				/*
				CCardAppConfig::cmd_break_t break_status = g_CardAppConfig.checkPipeStatus();
				if(break_status == CCardAppConfig::CMD_BREAK_CANCEL_E)
				{
					dlog_alert("App selection cancelled, exiting...");
					//g_CEMV.SetStatusDetail(CARD_APP_STATUS_D_CANCELLED);
					//g_CEMV.SetStatusMain(CARD_APP_STATUS_M_STOP);
					result = 1; // cancelled
					break;
				}
				else if (break_status == CCardAppConfig::CMD_BREAK_NO_BREAK_E)
				{
					dlog_msg("GUIAPP answer");
					result = 0;
					break;
				}
				result = 0; // ctmp!!
				break;
				*/
			}
		}
		return result;
	}

	int EntryCancelled(void * ptr)
	{
		//dlog_msg("Selection Cancelled!");
		//PML implementation here
		unsigned long ulEventValue = 0;
		int result = 1;
		bool waiting = true;
		int events_mon = event_open();
		eventset_t pml_events;
		int num_events = 0;
		event_item item_event;
		bool timerSet = false;
		cAppSelDisplay * pObj = static_cast<cAppSelDisplay *>(ptr);
		long pml_timeout = 500;

		if(events_mon > 0)
		{
			event_item event;
			event_item localTimer;
			event.event_id = events::icc;
			event_ctl(events_mon, ctl::ADD, event);
			event_ctl(events_mon, ctl::ADD, com_verifone_ipc::get_events_handler());
			if (set_timer(localTimer, 500) >= 0)
			{
				timerSet = true;
				event_ctl(events_mon, ctl::ADD, localTimer);
			}

			do
			{
				dlog_msg("App Selection - Wait for events");
				int cnt = event_wait(events_mon, pml_events);
				if (cnt > 0)
				{
					result = procEvents(pml_events, pObj);
					if (result >= 0) waiting = false;
				}
			} while(waiting);
			if (timerSet) clr_timer(localTimer);
			event_close(events_mon);
		}
		return (result);
	}
} //namespace internal

cAppSelDisplayGuiApp::cAppSelDisplayGuiApp(): cAppSelDisplay(), connected_to_gui(connect()), result(APP_SEL_OK)
{
	//nothing here
}
cAppSelDisplayGuiApp::~cAppSelDisplayGuiApp()
{
	if (connected_to_gui) GuiApp_Disconnect();
}

bool cAppSelDisplayGuiApp::connect()
{
	static const char *CARD_APP_NAME = "CARDAPP";
	if (GuiApp_Connect( (char *)CARD_APP_NAME, 10000 ) == GUI_OKAY) return true;
	return false;
}

int cAppSelDisplayGuiApp::displayApplicationSelection(const char * app_list[], size_t app_list_count, int milliseconds)
{
	if (!connected_to_gui)
	{
		dlog_error("We are not connected to GuiApp!");
		return APP_SEL_ERROR;
	}
	// Ok, we're connected! Check input data
	if (app_list == 0 || app_list_count <= 0)
	{
		dlog_error("Invalid parameters (%p and %d)", app_list, app_list_count);
		return APP_SEL_INVALID_PARAMETERS;
	}
	// Process
	int iRet = 0;

	dlog_msg("MenuFunc called for %d items!", app_list_count);

	//Check for Kernel bug - after application selection, Kernel always calls this function, even if it's not necessary ...
	if (app_list_count == 2)
	{
		if (!strcmp(app_list[0], " ")) return 2;
		else if (!strcmp(app_list[1], " ")) return 1;
	}

	// g_CEMVtrns.send_notify(CARD_APP_EVT_APP_SELECT);

	for(unsigned i= 0; i< app_list_count; i++)
	{
		dlog_msg("Label %d - %s", i+1, app_list[i]);
	}

	// TODO: Make application selection timeout configurable via some private tag!
	dlog_msg("Select application with timeout %d", milliseconds);
	iRet = GuiApp_SelectOption("", "", app_list, app_list_count, milliseconds, EntryCancelled, this);

	if (iRet < 0 || iRet >= app_list_count)
	{
		dlog_alert("Select application failed, error %d", iRet);
		if (!isICCpresent())
		{
			dlog_alert("Card removed!");
			result = APP_SEL_CARD_REMOVED;
		}
		else if (iRet == GUI_CANCELLED)
		{
			//g_CEMV.SetStatusDetail(CARD_APP_STATUS_D_CANCELLED);
			//g_CEMV.SetStatusMain(CARD_APP_STATUS_M_STOP);
			dlog_alert("Cancelled by Cardholder!");
			iRet = APP_SEL_CANCELLED;
		}
		else if (iRet == GUI_POS_CANCELLED)
		{
			dlog_alert("Cancelled by POS!");
			iRet = APP_SEL_POS_CANCELLED;
		}
	}

	GuiApp_ClearDisplay(0);

	// g_CEMVtrns.send_notify(CARD_APP_EVT_APP_SELECT_DONE);
	dlog_msg("Selected option or error: %d", iRet);

	return iRet;
}

#endif

#ifdef VFI_GUI_DIRECTGUI
/* DirectGUI class */

#define MAX_AID_FOR_SELECTION 10

cAppSelDisplayDirectGUI::cAppSelDisplayDirectGUI(const std::string & aResourcePath, const std::string & anAppSelectionScreen)
	: cAppSelDisplay(),
	  resourcePath(aResourcePath),
	  appSelectionScreen(anAppSelectionScreen)
{
	if (resourcePath.length() > 0 )
	{
		// appending / if needed
		if (*(resourcePath.rbegin()) != '/')
		{
			resourcePath += "/";
		}
	}
}

#ifdef DIRECTGUI_PROXY
int cAppSelDisplayDirectGUI::displayApplicationSelection(const char * app_list[], size_t app_list_count, int milliseconds)
{
    vfiguiproxy::JSONObj menuObj;
    int result = 0, guiResult = 0;
    int max_items = (app_list_count > MAX_AID_FOR_SELECTION)?(MAX_AID_FOR_SELECTION):(app_list_count);
    int timeout_msec = 300;

    for (int i = 0; i < max_items; ++i)
    {
        menuObj[i]("name") = app_list[i];
        menuObj[i]("index") = i + 1;
    }

    if (max_items > 0)
    {
        menuObj[0]("selected") = "selected";
        menuObj[0]("focus") = "autofocus";
    }

    std::map<std::string, std::string> params;
    params["json_array"] = menuObj.dump();

    minIni ini("gui_common.ini");
    params["logo_image"] = ini.gets("", "logo", "");

    activateCapacitiveKbd();

    std::string htmlPath(com_verifone_pml::get_invocation_name());
    size_t has_ext = htmlPath.find(".out");
    if (has_ext == std::string::npos) has_ext = htmlPath.find(".vsa");
    if (has_ext != std::string::npos) htmlPath.resize(has_ext);
    htmlPath.append("/");
    if (resourcePath.size()) htmlPath.append(resourcePath);
    htmlPath.append(appSelectionScreen);
    dlog_msg("GuiProxy: HTML %s", htmlPath.c_str());

    vfiguiproxy::GUIClient::instance().displayURLAsync(htmlPath, params);

    do
    {
        dlog_msg("App selection step");
        guiResult = vfiguiproxy::GUIClient::instance().pollAsyncResult(timeout_msec);

        if(!isICCpresent())
        {
            dlog_msg("APP_SEL_CARD_REMOVED");
            result = APP_SEL_CARD_REMOVED;
            vfiguiproxy::GUIClient::instance().cancel();
            break;
        }

        if(CheckBreak() == Cancelled)
        {
            dlog_msg("APP_SEL_CANCELLED");
            result = APP_SEL_CANCELLED;
            vfiguiproxy::GUIClient::instance().cancel();
            break;
        }
    } while (guiResult == vfiguiproxy::result::POLL_TIMEOUT);

    // 0 means OK, all items are therfore enumerated from 1
    // but outside this function OK is not relevant, so we map the result back to 0..N-1
    if (guiResult > 0) 
    {
        result = (guiResult - 1);
    }
    else if (guiResult == vfiguiproxy::result::ABORT && result == 0)
    {
        result = APP_SEL_CANCELLED;
    }

    deactivateCapacitiveKbd();

    dlog_msg("app selection result: %d", result);
    return result;
}
#else
int cAppSelDisplayDirectGUI::displayApplicationSelection(const char * app_list[], size_t app_list_count, int milliseconds)
{
	using namespace vfigui;

	vfigui::JSObject menuObj;
	int iRet = 0;
	int max_items = (app_list_count > MAX_AID_FOR_SELECTION)?(MAX_AID_FOR_SELECTION):(app_list_count);
	int request_id = 0;
	int timeout_msec = 300;
	int total_timeout = 0;

	for (int i = 0; i < max_items; ++i)
	{
		menuObj[i]("name") = app_list[i];
		menuObj[i]("index") = i + 1;
	}

	if (max_items > 0)
	{
		menuObj[0]("selected") = "selected";
		menuObj[0]("focus") = "autofocus";
	}

	std::map<std::string, std::string> params;
	params["json_array"] = menuObj.dump();
	
	// loading language settings and setting language catalogue
	minIni ini("gui_common.ini");
	std::string language = ini.gets("","lang", "");
	if (!language.empty()){
		vfigui::uiSetCatalog(language);
	}
    	params["logo_image"] = ini.gets("", "logo", "");

	activateCapacitiveKbd();

	request_id = vfigui::uiInvokeURLAsync(params, resourcePath + appSelectionScreen);
	dlog_msg("uiInvokeURLAsync reqid: %d", request_id);
	do
	{
		if(!isICCpresent())
		{
			dlog_msg("APP_SEL_CARD_REMOVED");
			iRet = APP_SEL_CARD_REMOVED;
			break;
		}
		if((iRet = vfigui::uiInvokeWait(request_id, timeout_msec)) == vfigui::UI_ERR_WAIT_TIMEOUT)
		{
			dlog_msg("uiInvokeWait UI_ERR_WAIT_TIMEOUT, iRet %d", iRet);
			if(CheckBreak() == Cancelled)
			{
				dlog_msg("APP_SEL_CANCELLED");
				iRet = APP_SEL_CANCELLED;
				break;
			}
		}
		else
		{
			dlog_msg("break");
			if (iRet == -1 )
			{
				dlog_msg("Selection was cancelled");
				iRet = APP_SEL_CANCELLED;
			}
			iRet-=1;	//indexing must be 0..n-1
			break;
		}
		dlog_msg("loop, request_id %d", request_id);
	}
	while(1);

	deactivateCapacitiveKbd();

	dlog_msg("after loop: iret %d", iRet);
	return iRet;
}
#endif // DIRECTGUI_PROXY
#endif // VFI_GUI_DIRECTGUI

