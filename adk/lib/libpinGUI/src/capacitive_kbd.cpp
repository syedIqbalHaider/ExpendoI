#include "capacitive_kbd.h"

#if defined(VFI_PLATFORM_VERIXEVO) && defined(VFI_GUI_DIRECTGUI)

#ifdef DIRECTGUI_PROXY
#include <libguiclient/guiclient.h>
#include <libguiclient/properties.h>
#else
#  ifdef DIRECTGUI2
#    include <html/gui.h>
#  else
#    include <gui/gui.h>
#  endif
#endif
#include <liblog/logsys.h>

void activateCapacitiveKbd(){
	int hasCapacitiveKeypad = 0;

	//workaround for e355 terminal, GUI lib thinks that e355 has capacitive keypad - which is wrong!
	const char * MODEL_E355 = "E355";
	char model_number[12+1];

	SVC_INFO_MODELNO (model_number);
	model_number[12]=0;

	if(memcmp(model_number, MODEL_E355, strlen(MODEL_E355)) == 0)
		return;

	#ifdef DIRECTGUI_PROXY
	vfiguiproxy::GUIClient::instance().getProperty(vfiguiproxy::property::DEVICE_HAS_CAP_TOUCH_KEYPAD, hasCapacitiveKeypad);
	#else
	vfigui::uiGetPropertyInt(vfigui::UI_DEVICE_HAS_CAP_TOUCH_KEYPAD,&hasCapacitiveKeypad);
	#endif
	dlog_msg("UI_DEVICE_HAS_CAP_TOUCH_KEYPAD [%d]",hasCapacitiveKeypad);
	if(hasCapacitiveKeypad) {
		int handle=open(DEV_COM1E,0);
		dlog_msg("DEV_COM1E [%d]",handle);
		if(handle>=0) {
			if(iap_control_function(handle,IAP_CONTROL_KEYPAD_WAKE)>=0) {
				SVC_WAIT(1000);
				dlog_msg("iap_control_function (WAKE): success");
			} else {
				dlog_msg("iap_control_function (WAKE): failure");
			}
			close(handle);
		}
	}
}

void deactivateCapacitiveKbd() {

	//workaround for e355 terminal, GUI lib thinks that e355 has capacitive keypad - which is wrong!
	const char * MODEL_E355 = "E355";
	char model_number[12+1];

	SVC_INFO_MODELNO (model_number);
	model_number[12]=0;

	if(memcmp(model_number, MODEL_E355, strlen(MODEL_E355)) == 0)
		return;

	int hasCapacitiveKeypad = 0;
	#ifdef DIRECTGUI_PROXY
	vfiguiproxy::GUIClient::instance().getProperty(vfiguiproxy::property::DEVICE_HAS_CAP_TOUCH_KEYPAD, hasCapacitiveKeypad);
	#else
	vfigui::uiGetPropertyInt(vfigui::UI_DEVICE_HAS_CAP_TOUCH_KEYPAD,&hasCapacitiveKeypad);
	#endif
	dlog_msg("UI_DEVICE_HAS_CAP_TOUCH_KEYPAD [%d]",hasCapacitiveKeypad);
	if(hasCapacitiveKeypad) {
		int handle=open(DEV_COM1E,0);
		dlog_msg("DEV_COM1E [%d]",handle);
		if(handle>=0) {
			if(iap_control_function(handle,IAP_CONTROL_KEYPAD_SLEEP)>=0) {
				dlog_msg("iap_control_function (SLEEP): success");
			} else {
				dlog_msg("iap_control_function (SLEEP): failure");
			}
			close(handle);
		}
	}
}

#else

void activateCapacitiveKbd(){}
void deactivateCapacitiveKbd(){}

#endif
