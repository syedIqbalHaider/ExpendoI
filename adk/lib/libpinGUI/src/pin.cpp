/******************************************************************************

  FILE: pin.cpp

  DESC: This file is used to generate a library containing all the PIN entry
        functions for the SC5000 ADPU application.
        This library must contain a embedded version number that indicates
        the version of PIN entry module.
        This library can not be modified with recertification of the POS 
        system with EMV Co.
        Please refer to:
        1) "EMVCo Type Approval Terminal Level 1 Administrative Process"
        2) "EMVCo Type Approval Bulletin No.11"
        
        

******************************************************************************/

#include <string>

#include <errno.h>
#include "Defines.h"
//#include "command.h"
#include <liblog/logsys.h>

#ifdef VFI_PLATFORM_VOS
#include <svcsec.h>
#undef max
#undef min
#endif
#ifdef VFI_PLATFORM_VERIXEVO
#include <svc_sec.h>
#endif

// #include "emvproto.h"

#include <libfoundation/HWInfo.h>
#include <libvoy.h>
#include <libpml/pml_port.h>
#include <libpml/pml_os_security.h>
#include <libpml/pml_os_core.h>
#include "PIN.h"

#ifdef VFI_GUI_DIRECTGUI
#include "helpers.h"
#include <libminini/minIni.h>
#ifdef DIRECTGUI_PROXY
#include <libguiclient/guiclient.h>
#include <libguiclient/jsonObj.h>
#else
#ifdef DIRECTGUI2
#  include <html/gui.h>
#  include <html/jsobject.h>
#else
#  include <gui/gui.h>
#  include <gui/jsobject.h>
#endif
#endif
#endif

/***************************************************************************
 * Definitions
 **************************************************************************/
#define LOGGING



using namespace com_verifone_util;

/******************************************************************************
  External variables
******************************************************************************/

/******************************************************************************
  External functions from SC5KEMV
******************************************************************************/
extern unsigned short getUsrPin(unsigned char *);
extern void usEMVDisplayErrorPrompt(unsigned short errorID);
extern int iPC_GenerateRandom( unsigned char * );

/******************************************************************************
  Static Global data
******************************************************************************/
const char PINModuleVersion[] = PINGUI_VERSION;




c_encPIN::c_encPIN(void)
{
	
	memset(c_encPIN::modulus, 0, MAX_MODULUS_LENGTH);
	c_encPIN::modulus_length = 0;
	memset(c_encPIN::exponent, 0, sizeof(c_encPIN::exponent));
	c_encPIN::exponent_length = 0;
	memset(c_encPIN::ICC_random, 0, sizeof(c_encPIN::ICC_random));
	c_encPIN::external_pin_len = 0;
	memset(c_encPIN::external_pin, 0, sizeof(c_encPIN::external_pin));
	external_pin_mode = com_verifone_pml::needham_schroeder::CHANNEL_2;
}



//this function makes init and presets parameters for selected GUI type



/******************************************************************************
  Defines for secure script
******************************************************************************/
#define RSA_SLOT 0
#define RSA_MACRO1  0x10
#define RSA_MACRO2  0x11
#define RSA_CORRECT 100
#define RSA_FAILED 101
#define RSA_INVALID_SCRIPT 102
#define RSA_SCRIPT_NOT_PRESENT 103
#define RSA_SYSTEM_ERROR 104


#define RSA_BUFFER_SIZE 300
/*-----------------------------------------------------------------------------
  Get PIN Module Version
-----------------------------------------------------------------------------*/
void GetPINModuleVersion( void )
{
  char buf[20];

  memcpy(buf, "PV", 2);
  buf[2] = strlen(PINModuleVersion) / 0x100;
  buf[3] = strlen(PINModuleVersion) % 0x100;
  memcpy(&buf[4], PINModuleVersion, strlen(PINModuleVersion));

  //send_response((byte *)PIN_MODULE_VERSION_REQUEST, strlen(PINModuleVersion)+4, 
//      (byte *)buf, (byte *)"00");

  return;
}


#ifdef DIRECTGUI_PROXY
namespace {
    std::string makeHtmlName(const std::string &fname)
    {
        std::string result(com_verifone_pml::get_invocation_name());
        size_t has_ext = result.find(".out");
        if (has_ext == std::string::npos) has_ext = result.find(".vsa");
        if (has_ext != std::string::npos) result.resize(has_ext);
        result.append("/");
        result.append(fname);
        dlog_msg("GuiProxy: HTML %s", result.c_str());
        return result;
    }
}
#endif


/*-----------------------------------------------------------------------------
  RSA
-----------------------------------------------------------------------------*/

int c_encPIN::VerifyPIN()
{
	if (external_pin_len > 0)
	{
		using namespace com_verifone_pml::needham_schroeder;
		int res = iPS_SelectPINAlgo(M_PIN_EMV);
		if (res)
		{
			dlog_msg("Error setting PIN Algo ret: %X - 1st try", res);
			if(res == E_KM_BAD_SEQUENCE)
			{
				iPS_CancelPIN();
				res = iPS_SelectPINAlgo(M_PIN_EMV);
				if (res != 0)
				{
					dlog_msg("Error setting PIN Algo ret: %X - 2nd try", res);
				}
			}
			// ctmp todo: proper error value
			if(res) return PIN_VERIFY_ABORT;
		}
		// ctmp todo: pass mode variable properly
		res = load_encrypted_pin_block(external_pin_mode, std::string(external_pin, external_pin_len));
		dlog_msg("External PIN load: %d, %d", res, errno);
		if (res != 0)
		{
			return PIN_VERIFY_ERROR;
		}
	}

	RESPONSECODE ret;
	unsigned char ICC_response[MAX_APDU_BUFFER_SIZE];
	int exp2 = -1;
	int SW1SW2 = 0;
	//checking if data for enciphered pin is set - if yes, we assume enciphered pin

	if(modulus_length > 0 && exponent_length > 0)
	{
		if(exponent_length == 1)
		{
			if (exponent[0] == 0x03) exp2 = 1;
			//else if (exponent[0] == 0x02) exp2 = 0;  //Kamil_P1: Disabled legacy exponent
		}
		else if(exponent_length == 3)
		{
			if (exponent[0] == 0x01 && exponent[1] == 0x00 && exponent[2] == 0x01) exp2 = 16;
		}
		if (exp2 == -1)
		{
			dlog_error("Invalid exponent defined: length %d, three bytes: %02X:%02X:%02Xh", exponent_length, exponent[0], exponent[1], exponent[2]);
			return SW1SW2;
		}
		ret = IFD_PresentEncryptEMVPIN(ICC_random, modulus, modulus_length, exp2, ICC_response);
		dlog_msg("Encrypted PIN verification ret: %d", ret);
	}
	else
	{
		ret = IFD_PresentClearTextPIN(ICC_response);
		dlog_msg("Clear text PIN verification ret: %d", ret);
	}

	if(ret == IFD_Success)
	{
		if (((ICC_response[0] << 8) + ICC_response[1]) > 0)
		{
			dlog_msg("PIN VERIFICATION SW1:SW2 %X:%X", ICC_response[2], ICC_response[3]);
			SW1SW2 = (int)(ICC_response[3] + ICC_response[2]*0x100);
		}
		displayNotification(ICC_response, 2);
	}

	return SW1SW2;
}

void c_encPIN::displayNotification(unsigned char * ICC_response, int offset /* = 0 */)
{
		#ifdef VFI_GUI_DIRECTGUI
		if (ICC_response[offset+0] == 0x90 && ICC_response[offset+1] == 00 && !PIN_html_ok.empty())
		{
			#ifdef DIRECTGUI_PROXY
			vfiguiproxy::GUIClient::instance().displayURL(makeHtmlName(PIN_html_ok), values_pin_ok);
			#else
			vfigui::uiInvokeURL(values_pin_ok, PIN_html_ok);
			#endif
		}
		else if (ICC_response[offset+0] == 0x63)
		{
			if (ICC_response[offset+1] > 0xC1 && !PIN_html_retry.empty())
			{
				#ifdef DIRECTGUI_PROXY
				vfiguiproxy::GUIClient::instance().displayURL(makeHtmlName(PIN_html_retry), values_pin_retry);
				#else
				vfigui::uiInvokeURL(values_pin_retry, PIN_html_retry);
				#endif
			}
			else if (ICC_response[offset+1] == 0xC1 && !PIN_html_last_try.empty())
			{
				#ifdef DIRECTGUI_PROXY
				vfiguiproxy::GUIClient::instance().displayURL(makeHtmlName(PIN_html_last_try), values_pin_last_try);
				#else
				vfigui::uiInvokeURL(values_pin_last_try, PIN_html_last_try);
				#endif
			}
		}
		#else
		#warning PIN HTML notifications are not supported for non-DirectGUI builds!
		#endif // Only for DirectGUI / GuiProxy
}

int c_encPIN::addModulus(unsigned char *mod, int m_len)
{
	if(m_len <= sizeof(modulus))
	{
		#if 0
		int offset = 0;
		if (m_len % 2)
		{
			dlog_alert("Odd modulus len (%d)!", m_len);
			modulus[0] = 0;
			offset = 1;
		}
		memcpy(modulus+offset, mod, m_len);
		modulus_length = m_len+offset;
		dlog_hex(modulus, modulus_length, "MODULUS");
		return m_len;
		#else
		memcpy(modulus, mod, m_len);
		modulus_length = m_len;
		return m_len;
		#endif
	}
	else
	{
		return 0;
	}
}

int c_encPIN::getModulus(unsigned char *mod, int *m_len)
{
	memcpy(mod, modulus, modulus_length);
	*m_len = modulus_length;

	return modulus_length;

}

int c_encPIN::addExponent(unsigned char *exp, int e_len)
{
	if(e_len <= sizeof(exponent))
	{
		memcpy(exponent, exp, e_len);
		exponent_length = e_len;
		return e_len;
	}
	else
	{
		return 0;
	}

}

int c_encPIN::getExponent(unsigned char *exp, int *e_len)
{
	memcpy(exp, exponent, exponent_length);
	*e_len = exponent_length;
	
	return exponent_length;

}


int c_encPIN::addICCrandom(unsigned char *rand, int r_len)
{
	if(r_len <= sizeof(ICC_random))
	{
		memcpy(ICC_random, rand, r_len);
		return r_len;
	}
	else
	{
		return 0;
	}
}

int c_encPIN::getICCrandom(unsigned char *rand, int *r_len)
{
	memcpy(rand, ICC_random, sizeof(ICC_random));
	*r_len = sizeof(ICC_random);
	
	return *r_len;

}

int c_encPIN::addExternalPIN(const char * ext_pin, int p_len)
{
	if (p_len < sizeof(external_pin))
	{
		external_pin_len = p_len;
		memcpy(external_pin, ext_pin, external_pin_len);
		return external_pin_len;
	}
	else
	{
		return 0;
	}
}

int c_encPIN::addExternalPINMode(int p_mode)
{
	if (p_mode == com_verifone_pml::needham_schroeder::CHANNEL_0 ||
			p_mode == com_verifone_pml::needham_schroeder::CHANNEL_1 ||
			p_mode == com_verifone_pml::needham_schroeder::CHANNEL_2 ||
			p_mode == com_verifone_pml::needham_schroeder::CHANNEL_3)
	{
		external_pin_mode = p_mode;
		return 1;
	}
	else
	{
		return 0;
	}
}


void c_encPIN::set_map_values(std::map<std::string, std::string> & values, const std::map<std::string, std::string> & vals)
{
    for (std::map<std::string, std::string>::const_iterator it = vals.begin(); it != vals.end(); ++it)
    {
        std::string val(it->second);
        #ifdef VFI_GUI_DIRECTGUI
        if (val.find(',') != std::string::npos)
        {
            #ifdef DIRECTGUI_PROXY
            vfiguiproxy::JSONObj lineObjects;
            #else
				#ifdef DIRECTGUI2
    	        vfihtml::JSObject lineObjects;
				#else
				vfigui::JSObject lineObjects;
        	    #endif
			#endif
            std::vector<std::string> val_params;
            helpers::str_explode(val, std::string(","), val_params);
            //for (std::vector<std::string>::const_iterator it = val_params.begin(); it != val_params.end(); ++it)
            for (size_t i = 0; i < val_params.size(); ++i)
            {
                lineObjects[i]("value") = helpers::str_trim(val_params[i]);
            }
            val = lineObjects.dump();
            dlog_msg("Parameter converted to (%s)", val.c_str());
        }
        #endif
        values.insert(std::make_pair(it->first, val));
    }
}

