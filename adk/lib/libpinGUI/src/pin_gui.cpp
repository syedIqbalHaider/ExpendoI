#include "Pin_gui.h"
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include "Defines.h"
//#include "command.h"
#include <liblog/logsys.h>

#include <E2E_EMV_CT_Serialize.h>

#ifdef VFI_PLATFORM_VOS
#include <sys/time.h>
#include <svcsec.h>
#undef max
#undef min
#endif

#ifdef VFI_PLATFORM_VERIXEVO
#include <svc_net.h>
#include <svc_sec.h>
#include <pthread.h>
#define _VRXEVO
#endif

// #include "emvproto.h"
#include <libfoundation/HWInfo.h>
#include <libvoy.h>
#include "libpml/pml_port.h"
#include "libpml/pml_abstracted_api.h"
#include "libpml/ux.h"

#include "capacitive_kbd.h"

#ifdef VFI_GUI_GUIAPP
#include "GuiApp.h"
using namespace com_verifone_guiapi;
#endif

#ifdef VFI_GUI_DIRECTGUI
#include <libminini/minIni.h>
#ifdef DIRECTGUI_PROXY
#include <libguiclient/guiclient.h>
#else
#ifdef DIRECTGUI2
#  include <html/gui.h>
#  include <html/jsobject.h>
#else
#  include <gui/gui.h>
#  include <gui/jsobject.h>
#endif
#endif
#endif

#include <algorithm>

#ifndef MAX_LABEL_LEN
    #define MAX_LABEL_LEN 16
#endif

#define PIN_ENTRY_STATUS_BAD_PIN                7
#define PIN_ENTRY_STATUS_LAST_TRY               8
#define PIN_ENTRY_STATUS_BLOCKED                9

const char *CARD_APP_NAME = "CARDAPP";
const char *GUI_APP_NAME_ = "GUIAPP";
#define ADD_SCREEN_TEXT 16
#define DISPLAY_TEXT             (2*ADD_SCREEN_TEXT) // should be able to accomodate 2 full lines of text
#define CARD_APP_GUI_S_PROMPTS      "prompts"
#define CARD_APP_GUI_S_EMV          "emv"
#define CARD_APP_GUI_S_PIN          "pin"

#define M_MAX_PIN_LENGTH_MAX 12
#define M_MIN_PIN_LENGTH_MIN 4
#define WINDOW_MAX 0xFFFFFFFF

#define GUI_CONSOLE 0
#define GUI_APP 1


#define CURR_CODE_LEN 8
#define CURRENCY_CODE_NULL {0, {'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0'}}
#define CARD_APP_DEF_MAX_TLV_LEN    256

#define UX100_KEY_NUM 240
#define UX100_KEY_DOWN 136
#define UX100_KEY_UP 134
#define UX100_KEY_CAN 129
#define UX100_KEY_COR 8
#define UX100_KEY_INFO 140
#define UX100_KEY_OK 13


typedef struct currency_code_s
{
      unsigned short code; /**< Numeric currency code  */
      char name[CURR_CODE_LEN+1]; /**< Currency code name */
} currency_code_t;


const currency_code_t M_CURRENCY_CODES[] =
    {
        {9999, "   "}, {784, "AED"}, {971, "AFN"}, {8, "ALL"}, {51, "AMD"}, {532, "ANG"},
        {973, "AOA"}, {32, "ARS"}, {36, "AUD"}, {533, "AWG"}, {944, "AZN"},
        {977, "BAM"}, {52, "BBD"}, {50, "BDT"}, {975, "BGN"}, {48, "BHD"},
        {108, "BIF"}, {60, "BMD"}, {96, "BND"}, {68, "BOB"}, {984, "BOV"},
        {986, "BRL"}, {44, "BSD"}, {64, "BTN"}, {72, "BWP"}, {974, "BYR"},
        {84, "BZD"}, {124, "CAD"}, {976, "CDF"}, {947, "CHE"}, {756, "CHF"},
        {948, "CHW"}, {990, "CLF"}, {152, "CLP"}, {156, "CNY"}, {170, "COP"},
        {970, "COU"}, {188, "CRC"}, {192, "CUP"}, {132, "CVE"}, {203, "CZK"},
        {262, "DJF"}, {208, "DKK"}, {214, "DOP"}, {12, "DZD"}, {233, "EEK"},
        {818, "EGP"}, {232, "ERN"}, {230, "ETB"}, {978, "EUR"}, {242, "FJD"},
        {238, "FKP"}, {826, "GBP"}, {981, "GEL"}, {936, "GHS"}, {292, "GIP"},
        {270, "GMD"}, {324, "GNF"}, {320, "GTQ"}, {328, "GYD"}, {344, "HKD"},
        {340, "HNL"}, {191, "HRK"}, {332, "HTG"}, {348, "HUF"}, {360, "IDR"},
        {376, "ILS"}, {356, "INR"}, {368, "IQD"}, {364, "IRR"}, {352, "ISK"},
        {388, "JMD"}, {400, "JOD"}, {392, "JPY"}, {404, "KES"}, {417, "KGS"},
        {116, "KHR"}, {174, "KMF"}, {408, "KPW"}, {410, "KRW"}, {414, "KWD"},
        {136, "KYD"}, {398, "KZT"}, {418, "LAK"}, {422, "LBP"}, {144, "LKR"},
        {430, "LRD"}, {426, "LSL"}, {440, "LTL"}, {428, "LVL"}, {434, "LYD"},
        {504, "MAD"}, {498, "MDL"}, {969, "MGA"}, {807, "MKD"}, {104, "MMK"},
        {496, "MNT"}, {446, "MOP"}, {478, "MRO"}, {480, "MUR"}, {462, "MVR"},
        {454, "MWK"}, {484, "MXN"}, {979, "MXV"}, {458, "MYR"}, {943, "MZN"},
        {516, "NAD"}, {566, "NGN"}, {558, "NIO"}, {578, "NOK"}, {524, "NPR"},
        {554, "NZD"}, {512, "OMR"}, {590, "PAB"}, {604, "PEN"}, {598, "PGK"},
        {608, "PHP"}, {586, "PKR"}, {985, "PLN"}, {600, "PYG"}, {634, "QAR"},
        {946, "RON"}, {941, "RSD"}, {643, "RUB"}, {646, "RWF"}, {682, "SAR"},
        {90, "SBD"}, {690, "SCR"}, {938, "SDG"}, {752, "SEK"}, {702, "SGD"},
        {654, "SHP"}, {703, "SKK"}, {694, "SLL"}, {706, "SOS"}, {968, "SRD"},
        {678, "STD"}, {760, "SYP"}, {748, "SZL"}, {764, "THB"}, {972, "TJS"},
        {795, "TMM"}, {788, "TND"}, {776, "TOP"}, {949, "TRY"}, {780, "TTD"},
        {901, "TWD"}, {834, "TZS"}, {980, "UAH"}, {800, "UGX"}, {840, "USD"},
        CURRENCY_CODE_NULL
    };

// Based on this table: http://www.xe.com/symbols.php
// Some symbols were not listed and are left as codes
const currency_code_t M_CURRENCY_SYMBOLS[] =
     {
        {9999, "   "},                     {784, "AED"},                       {971, "\xD8\x8B"},
        {8, "\x4C\x65\x6B"},               {51, "AMD"},                        {532, "\xC6\x92"},
        {973, "AOA"},                      {32, "$"},                          {36, "$"},
        {533, "\xC6\x92"},                 {944, "\xD0\xBC\xD0\xB0\xD0\xBD"},
        {977, "\x4B\x4D"},                 {52, "$"},                          {50, "BDT"},
        {975, "\xD0\xBB\xD0\xB2"},         {48, "BHD"},
        {108, "BIF"},                      {60, "$"},                          {96, "$"},
        {68, "$b"},                        {984, "BOV"},
        {986, "R$"},                       {44, "$"},                          {64, "BTN"},
        {72, "P"},                         {974, "p."},
        {84, "BZ$"},                       {124, "$"},                         {976, "CDF"},
        {947, "CHE"},                      {756, "CHF"},
        {948, "CHW"},                      {990, "CLF"},                       {152, "$"},
        {156, "\xC2\xA5"},                 {170, "$"},
        {970, "COU"},                      {188, "\xE2\x82\xA1"},              {192, "\xE2\x82\xB1"},
        {132, "CVE"},                      {203, "\x4B\xC4\x8D"},
        {262, "DJF"},                      {208, "kr"},                        {214, "RD$"},
        {12, "DZD"},                       {233, "kr"},
        {818, "\xC2\xA3"},                 {232, "ERN"},                       {230, "ETB"},
        {978, "\xE2\x82\xAC"},             {242, "$"},
        {238, "\xC2\xA3"},                 {826, "\xC2\xA3"},                  {981, "GEL"},
        {936, "GHS"},                      {292, "\xC2\xA3"},
        {270, "GMD"},                      {324, "GNF"},                       {320, "Q"},
        {328, "$"},                        {344, "$"},
        {340, "L"},                        {191, "kn"},                        {332, "HTG"},
        {348, "Ft"},                       {360, "Rp"},
        {376, "\xE2\x82\xAA"},             {356, "INR"},                       {368, "IQD"},
        {364, "\xEF\xB7\xBC"},             {352, "ISK"},
        {388, "J$"},                       {400, "JOD"},                       {392, "\xC2\xA5"},
        {404, "KES"},                      {417, "\xD0\xBB\xD0\xB2"},
        {116, "\xE1\x9F\x9B"},             {174, "KMF"},                       {408, "\xE2\x82\xA9"},
        {410, "\xE2\x82\xA9"},             {414, "KWD"},
        {136, "$"},                        {398, "\xD0\xBB\xD0\xB2"},          {418, "\xE2\x82\xAD"},
        {422, "\xC2\xA3"},                 {144, "\xE2\x82\xA8"},
        {430, "$"},                        {426, "LSL"},                       {440, "Lt"},
        {428, "Ls"},                       {434, "LYD"},
        {504, "MAD"},                      {498, "MDL"},                       {969, "MGA"},
        {807, "\xD0\xB4\xD0\xB5\xD0\xBD"}, {104, "MMK"},
        {496, "\xE2\x82\xAE"},             {446, "MOP"},                       {478, "MRO"},
        {480, "\xE2\x82\xA8"},             {462, "MVR"},
        {454, "MWK"},                      {484, "$"},                         {979, "MXV"},
        {458, "RM"},                       {943, "MT"},
        {516, "$"},                        {566, "\xE2\x82\xA6"},              {558, "C$"},
        {578, "kr"},                       {524, "\xE2\x82\xa8"},
        {554, "$"},                        {512, "\xEF\xB7\xBC"},              {590, "B/."},
        {604, "S/."},                      {598, "PGK"},
        {608, "\xE2\x82\xB1"},             {586, "\xE2\x82\xA8"},              {985, "z\xC5\x82"},
        {600, "Gs"},                       {634, "\xEF\xB7\xBC"},
        {946, "lei"},                      {941, "\xD0\x94\xD0\xB8\xD0\xBD."},
        {643, "\xD1\x80\xD1\x83\xD0\xB1"}, {646, "RWF"},                       {682, "\xEF\xB7\xBC"},
        {90, "$"},                         {690, "\xE2\x82\xA8"},              {938, "SDG"},
        {752, "kr"},                       {702, "$"},
        {654, "\xC2\xA3"},                 {703, "SKK"},                       {694, "SLL"},
        {706, "S"},                        {968, "$"},
        {678, "STD"},                      {760, "\xC2\xA3"},                  {748, "SZL"},
        {764, "\xE0\xB8\xBF"},             {972, "TJS"},
        {795, "TMM"},                      {788, "TND"},                       {776, "TOP"},
        {949, "TL"},                       {780, "TT$"},
        {901, "NT$"},                      {834, "TZS"},                       {980, "\xE2\x82\xB4"},
        {800, "UGX"},                      {840, "$"},
        CURRENCY_CODE_NULL
     };
#ifdef DIRECTGUI_PROXY
const int UI_REGION_KBD_MONITORING = 100;
#else
const int UI_REGION_KBD_MONITORING = 101;
#endif


#ifdef DIRECTGUI_PROXY
namespace {
    std::string makeHtmlName(const std::string &fname)
    {
        std::string result(com_verifone_pml::get_invocation_name());
        size_t has_ext = result.find(".out");
        if (has_ext == std::string::npos) has_ext = result.find(".vsa");
        if (has_ext != std::string::npos) result.resize(has_ext);
        result.append("/");
        result.append(fname);
        dlog_msg("GuiProxy: HTML %s", result.c_str());
        return result;
    }
}
#endif

int GetTLVData_loc(int tag, unsigned char *val, int *len)
{
    val[0] = 0;
    *len = 0;
    return 0;
}
int SetTLVData_loc(int tag, unsigned char *val, int len)
{
    return 0;
}
break_ret_t checkBreak_loc()
{
    return NoData;
}

bool isICCpresent_loc()
{
    return true;
}


int EntryCallBack_loc(int num_digits, int status, char* filename, char* filename_warning)
{
    std::string empty;
    int iRet = 0;

    dlog_msg("Dummy callback: number of valid PIN digits: %d, entry status: %d", num_digits, status);

    return iRet;
}

#ifdef VFI_GUI_DIRECTGUI
class BeeperThread{
public:
    /**
     * @param timeout - delay between repeats of the beep
     */
    BeeperThread(long timeout);
    ~BeeperThread();

private:
    static void * runThreadHelper(void * thisObj);
    void run();

private:
    long timeout;   // ms
    pthread_t threadId;
};
/////////////////////////////////////////////////////////////////////////////////////////////////

BeeperThread::BeeperThread(long _timeout) :
    timeout(_timeout), threadId(-1)
{
    if (timeout > 0){
        #if defined DIRECTGUI_PROXY || defined DIRECTGUI2
        pthread_create(&threadId, NULL, BeeperThread::runThreadHelper, static_cast<void *>(this));
        #else
//        vfigui::uiThreadCreate(&threadId, NULL, BeeperThread::runThreadHelper, static_cast<void *>(this));
        pthread_create(&threadId, NULL, BeeperThread::runThreadHelper, static_cast<void *>(this));
        #endif
    }
}

BeeperThread::~BeeperThread(){
    if(threadId != -1) {
        pthread_cancel(threadId);
        pthread_join(threadId, NULL);
    }
}

void BeeperThread::run()
{
    while(true){
        com_verifone_pml::normal_tone();
        SVC_WAIT(timeout);
    }
}

void * BeeperThread::runThreadHelper(void * thisObj){
    (static_cast<BeeperThread*>(thisObj))->run();
    return NULL;
}
#endif

cPINdisplay::cPINdisplay(void)
{
    extra_msg.clear();
    amount_line = 0;
    display_curr_symbol = true;
    PINDisplayCurrencySymbolLeft = false;
    radixSep = '.';
    x = 0;
    y = 0;
    msg_enter_pin = "ENTER PIN";
    msg_incorrect_pin = "INCORRECT PIN";
    msg_last_pin_try = "LAST PIN TRY";
    GetTLVData = GetTLVData_loc;
    SetTLVData = SetTLVData_loc;
    CheckBreak = checkBreak_loc;
    isICCpresent = isICCpresent_loc;
    EntryCallBack = EntryCallBack_loc;

    bypassKey = 0xFF;
    PINtimeout = 60000; // 60 seconds
    PINfirstchartimeout = 0;
    PINnextchartimeout = 0;

    pin_progress = 0;
    pin_mode = 0x0A;

    cancel_reason = 0;
    lng_prefix.clear();

    open_close_crypto = true;
    VSS_macro = -1;
    VSS_script = -1;
}

#if 0
int cPINdisplayConsole::Display()
{
    const currency_code_t *pCurrCode = &M_CURRENCY_CODES[0];
    char strPIN[ADD_SCREEN_TEXT+1];
    strPIN[0]=0;
    uint16_t trn_curr = 0;
    std::string amtDsp;
    std::string extDsp;


    amtDsp = BuildAmtMsg();
    dlog_msg("Amount text: %s", amtDsp.c_str());
    extDsp = BuildExtraMsg();
    dlog_msg("Extra text: %s", extDsp.c_str());
    strPIN[0]=0;

    char tempText[DISPLAY_TEXT+1];
    tempText[DISPLAY_TEXT] = 0;
    strcpy(tempText, "\\c");
    strncpy(tempText+2, extDsp.c_str(), DISPLAY_TEXT-4);
    //strcat(tempText, "\n");
    get_console_handle();
    DisplayText(tempText, strlen(tempText), 1, 1, true);
    strncpy(tempText+2, amtDsp.c_str(), DISPLAY_TEXT-4);
    //strcat(tempText, "\n");
    DisplayText(tempText, strlen(tempText), 1, 2, false);
    {
         char * pText = tempText + 2;
         userPromptLite prompt;
         if (prompt.getPrompt(CARD_APP_GUI_S_PIN, g_CardAppConfig.pinScreenTitle, pText, DISPLAY_TEXT-2 ) <= 0) // -2 to ensure beginning '\c' fits
         {
             dlog_error("Cannot get PIN prompt (number %d), assuming default (Enter PIN)!", g_CardAppConfig.pinScreenTitle);
             if(getPromptLocal(g_CardAppConfig.pinScreenTitle, pText) == 0)
                strcpy(pText, "ENTER PIN");
         }
    }
    DisplayText(tempText, strlen(tempText), 1, 3, false);

    return 1;
}

bool cPINdisplayConsole::ComputeEntryXY()
{
    const int maxX = getScreenMaxX();
    const int maxY = getScreenMaxY();
    x = (maxX >> 1) - (M_MAX_PIN_LENGTH_MAX >> 1);
	y = 4;
	
    dlog_msg("Screen size (x,y): %d:%d coordinates: %d:%d", maxX, maxY, x, y);
    return true;
}
#endif

#if defined(VFI_GUI_DIRECTGUI)

static bool isKeyboardBeeper(const minIni& gui_common)
{
    #if defined VFI_PLATFORM_VOS && defined VFI_SUBPLATFORM_RAPTOR
        // Not working keyboard beeper (VOSTRT-389) is fixed on V/OS2 Raptor.
        return false;
    #else
        return gui_common.getbool("", "keyboard_beeper", false);
    #endif
}

// if this method returns false, pin entry is cancelled by DirectGUI
bool pinEntryCallback(void * data){
    cPINdisplayDirectGUIBase * pingui = static_cast<cPINdisplayDirectGUIBase *>(data);

    #ifndef DIRECTGUI_PROXY
    SVC_WAIT(100);
    #endif

    if (pingui->should_cancel()) {
        dlog_msg("pinEntryCallback: cancelled");
        return false;
    }

    if (pingui->pinEntryTimeExpired()) {
        dlog_msg("pinEntryCallback: pin entry time  expired");
        return false;
    }

    if (pingui->pinEntryKeyboardTimeExpired()) {
        dlog_msg("pinEntryCallback : pin entry keyboard time expired");
        return false;
    }

    return true;
}

cPINdisplayDirectGUIBase * g_ourObj = NULL;

static void callback_PIN_entry_general(char value, bool beep)
{
	if((value & 0xF0) == 0xF0)
	{
		if (beep) com_verifone_pml::error_tone();
		if (g_ourObj) g_ourObj->storePinEntryKeyTime();
	}
	if(beep && (value & 0x70) == 0x70)
	{
		com_verifone_pml::normal_tone();
	}
	if((value & 0x03))
	{
		// a key pressed
		if (g_ourObj)
		{
			if ((value & 0x01) == 0x01)
			{
				g_ourObj->setNextKey();
			}
			g_ourObj->storePinEntryKeyTime();
			int num_digits = g_ourObj->getPIN_num_digits();
			g_ourObj->setPIN_num_digits(++num_digits);
		}
	}
	dlog_msg("PIN event (callback): 0x%X (%sBEEP)", value, beep ? "" : "NO ");
}

static void callback_PIN_entry_BEEP(char value) { callback_PIN_entry_general(value, true); }
static void callback_PIN_entry(char value) { callback_PIN_entry_general(value, false); }

cPINdisplayDirectGUI::cPINdisplayDirectGUI(long timeout): cPINdisplayDirectGUIBase(timeout)
{
    cancel_reason = 0;
    open_close_crypto = true;

    //setSecurePINDisplayParameters(NULL, (void *)callback_PIN_entry);
    #ifdef DIRECTGUI_PROXY
    // vfiguiproxy::GUIClient::instance().displayHTML("");
    vfiguiproxy::GUIClient::instance().clearMissedNotifications();
    #else
    vfigui::uiInvoke("");
    #endif
	dlog_msg("GUI object created");
}

cPINdisplayDirectGUI::~cPINdisplayDirectGUI()
{
    // clear screen
    #ifdef DIRECTGUI_PROXY
    // vfiguiproxy::GUIClient::instance().displayHTML("");
    vfiguiproxy::GUIClient::instance().clearMissedNotifications();
    #else
    vfigui::uiInvoke("");
    #endif
}

#if defined(VFI_PLATFORM_VOS) || defined(EMV_ADK_VRXEVO)


int cPINdisplayDirectGUI::GetPIN(c_encPIN encPIN)
{
    int ret = 0;
    std::map<std::string, std::string> value;
    ux uxHandler;


    dlog_msg("cPINdisplayDirectGUI::GetPIN, pin_mode: %X", pin_mode);
	/*
    #ifdef VFI_PLATFORM_VERIXEVO
    int hdl = -1;
    if(open_close_crypto)
    {
        int owner = 0;
        hdl = get_owner("/dev/crypto", &owner);
        if (owner != get_task_id())
        {
            hdl = open("/dev/crypto", 0);
        }
        if (hdl < 0)
        {
            dlog_error("Cannot open crypto device!");
            return PIN_VERIFY_ABORT;
        }
    }
    #endif
    */

    std::string amtDsp = BuildAmtMsg();
    value["amount"]=amtDsp;
    std::string extDsp = BuildExtraMsg();
    value["extra"]=extDsp;

    char buf[32] = {'\0'};
    if (sprintf(buf, "%d", pKeypadSetup.ucMin) < 0) return PIN_VERIFY_ABORT;
    value["pinminlength"] = buf;
    if (sprintf(buf, "%d", pKeypadSetup.ucMax) < 0) return PIN_VERIFY_ABORT;
    value["pinmaxlength"] = buf;

	#if defined(VFI_PLATFORM_VOS)

	ret=iPS_SetPINParameter(&pKeypadSetup);
    if (ret)
    {
        dlog_msg("iPS_SetPINParameter error");
        switch(ret)
        {
            case E_KM_OUT_OF_RANGE:
               dlog_msg("E_KM_OUT_OF_RANGE");
               break;
            case E_KM_SYSTEM_ERROR:
               dlog_msg("E_KM_SYSTEM_ERROR");
               break;
            case EBADF:
               dlog_msg("EBADF, we don't own crypto!");
               break;
            default:
               dlog_msg("GENERIC ERROR");
               break;
        }
        return -1;
    }
	#endif

    #ifdef DIRECTGUI_PROXY
    // modify this values to change default behaviour of PIN entry
    vfiguiproxy::GUIClient::instance().setProperty(vfiguiproxy::property::PASSWORD_CHAR, (int)pKeypadSetup.ucEchoChar);
    vfiguiproxy::GUIClient::instance().setProperty(vfiguiproxy::property::PIN_AUTO_ENTER, (pKeypadSetup.ucOption & 0x01) ? 1 : 0);   // 1 -> enable
    vfiguiproxy::GUIClient::instance().setProperty(vfiguiproxy::property::PIN_CLEAR_ALL, (pKeypadSetup.ucOption & 0x08) ? 0 : 1);    // 1 -> enable
    vfiguiproxy::GUIClient::instance().setProperty(vfiguiproxy::property::PIN_BYPASS_KEY, (pKeypadSetup.ucOption & 0x02) ? bypassKey : 0);     // 13 -> enter key, 8 -> clear key
    vfiguiproxy::GUIClient::instance().setProperty(vfiguiproxy::property::PIN_ALGORITHM, pin_mode);

    minIni ini("gui_common.ini");
    value["logo_image"] = ini.gets("", "logo", "");
    bool keyBeeper = isKeyboardBeeper(ini);

	#if defined(VFI_PLATFORM_VOS)
    setSecurePINDisplayParameters(NULL, keyBeeper ? (void *)callback_PIN_entry_BEEP : (void *)callback_PIN_entry);
	#endif

    //ret = vfigui::uiInvoke(value, "PIN < input type='pin' name='input' action='return 0'");

    #else
    // modify this values to change default behaviour of PIN entry
    uiSetPropertyInt(vfigui::UI_PROP_PASSWORD_CHAR,'*');
    uiSetPropertyInt(vfigui::UI_PROP_PIN_AUTO_ENTER,0);  // 1 -> enable
    uiSetPropertyInt(vfigui::UI_PROP_PIN_CLEAR_ALL,0);   // 1 -> enable
    uiSetPropertyInt(vfigui::UI_PROP_PIN_BYPASS_KEY, bypassKey);  // 13 -> enter key, 8 -> clear key
    uiSetPropertyInt(vfigui::UI_PROP_PIN_ALGORITHM,pin_mode);

    // kamilm: client must be able to restore empty "" prefix
    std::string currentPrefix;
    vfigui::uiGetPropertyString(vfigui::UI_PROP_FILE_PREFIX, currentPrefix);
    if(currentPrefix.size() > 0)
    {
        // as it was prefix set by pin gui, we need to remove trailing '-' that was added
        currentPrefix = currentPrefix.substr(0, currentPrefix.size() - 1);
    }
    if(lng_prefix != currentPrefix)
    {
        if (lng_prefix.size() > 0)  // do not add '-' to empty prefix!
        {
            lng_prefix+='-';
        }
        vfigui::uiSetPropertyString(vfigui::UI_PROP_FILE_PREFIX, lng_prefix);
    }

    // loading language settings and setting language catalog
    minIni ini("gui_common.ini");
    std::string language = ini.gets("","lang", "");
    if (!language.empty()){
        vfigui::uiSetCatalog(language);
    }
    value["logo_image"] = ini.gets("", "logo", "");
    #endif

    BeeperThread beeper(pinBeeperTimeout);

    int try_cntr = 1;

    storePinEntryStartTime();

    activateCapacitiveKbd();    //activates capacitive keyboard support (device e315 etc.)

    cancel_reason = 0;

    std::string htmlName;
    if(pin_progress == PIN_ENTRY_STATUS_LAST_TRY)
    {
        htmlName = "LastPIN.html";
    }
    else if(pin_progress == PIN_ENTRY_STATUS_BAD_PIN)
    {
        htmlName = "RetryPIN.html";
    }
    else
    {
        htmlName = "EnterPIN.html";
    }

	dlog_msg("STARTING PIN ENTRY using %s", htmlName.c_str());
	
    do
    {
        #ifdef DIRECTGUI_PROXY
		vfiguiproxy::GUIClient::instance().clearMissedNotifications();
		dlog_msg("Using GUIproxy for PIN entry");
        ret = vfiguiproxy::GUIClient::instance().displayURLAsync(makeHtmlName(htmlName), value);
        if (ret < 0)
        {
            dlog_error("Gui error %d", ret);
            break;
        }
        int pinStatus = 0;
        PINRESULT pinOUTData;
        unsigned char currentKeyCount = 0;
        g_ourObj = this;

        do
        {
            if (pinEntryCallback(this) == false) 
            {
                dlog_msg("cancelling pin entry due to callback");
                vfiguiproxy::GUIClient::instance().cancel_wait();
                ret = vfiguiproxy::result::ABORT;
                break;
            }
            if (uxHandler.is_ux())
            {
                pinOUTData.nbPinDigits = uxHandler.pinGetNbCurrDigitEntered();
                dlog_msg("pinGetNbCurrDigitEntered entered digits: %d", pinOUTData.nbPinDigits);
                if (pinOUTData.nbPinDigits != currentKeyCount)
                {
                    if (keyBeeper) com_verifone_pml::normal_tone();
                    currentKeyCount = pinOUTData.nbPinDigits;
                    storePinEntryKeyTime();
                    PIN_first_key = false;
                    EntryCallBack(currentKeyCount, 0xFF, NULL, NULL);
                }
            }
            ret = vfiguiproxy::GUIClient::instance().pollAsyncResult(value, 100);
        } while (ret == vfiguiproxy::result::POLL_TIMEOUT);
        #else
        ret = vfigui::uiInvokeURL(value, htmlName, pinEntryCallback, this);
        #endif
        dlog_msg("GUI ret: %d", ret);
        for(std::map<std::string, std::string>::iterator it = value.begin(); it!=value.end(); ++it)
        {
        	dlog_msg("[%s] = [%s]", it->first.c_str(), it->second.c_str());
            if((it->first == "pin") && (it->second == "bypass"))
            {
            #ifdef DIRECTGUI_PROXY
                ret = vfiguiproxy::result::BACK;
            #else
                ret = vfigui::UI_ERR_BACK;
            #endif
                dlog_msg("'pin' is 'bypass', set return code to %d", ret);
            }
        }
        g_ourObj = NULL;
    }
    #ifdef DIRECTGUI_PROXY
    while((ret == vfiguiproxy::result::GUIPROXY_REGION_BUSY) && (--try_cntr));
    #else
    while((ret == vfigui::UI_ERR_CANCELLED) && (--try_cntr));
    #endif

    deactivateCapacitiveKbd();

    #ifdef DIRECTGUI_PROXY
    switch(ret)
    {
        case vfiguiproxy::result::OK:
            ret = PIN_VERIFY_OK;
            break;

        // TODO handling result cancelled by callback
        // case vfiguiproxy::result::CANCELLED:
        case vfiguiproxy::result::ABORT:
            if (PIN_entry_timeout)
                ret = PIN_VERIFY_TIMEOUT;
            else if (cancel_reason)
                ret = cancel_reason;
            else
                ret = PIN_VERIFY_CANCEL;
            break;
        case vfiguiproxy::result::BACK:
            ret = PIN_VERIFY_BYPASS;
            break;
        case vfiguiproxy::result::TIMEOUT:
            ret = PIN_VERIFY_TIMEOUT;
            break;
        default:
            ret = PIN_VERIFY_ERROR;
            break;
    }
    #else
    switch(ret)
    {
        case vfigui::UI_ERR_OK:
            ret = PIN_VERIFY_OK;
            break;
        case vfigui::UI_ERR_CANCELLED:
        case vfigui::UI_ERR_ABORT:
            if (PIN_entry_timeout)
                ret = PIN_VERIFY_TIMEOUT;
            else if (cancel_reason)
                ret = cancel_reason;
            else
                ret = PIN_VERIFY_CANCEL;
            break;
        case vfigui::UI_ERR_BACK:
            ret = PIN_VERIFY_BYPASS;
            break;
        case vfigui::UI_ERR_TIMEOUT:
            ret = PIN_VERIFY_TIMEOUT;
            break;
        default:
            ret = PIN_VERIFY_ERROR;
            break;
    }
    #endif
    if(ret == PIN_VERIFY_OK)
    {
        if((VSS_script >=0) && (VSS_macro >= 0))
        {
			dlog_msg("Trying to perform VSS macro after PIN... (script: 0x%X, macro:0x%X)", VSS_script, VSS_macro);
            unsigned char unused[2];
            unsigned short outSize;
            int res = iPS_ExecuteScript(VSS_script, VSS_macro, 0, unused, 0, &outSize, unused);
            dlog_msg("iPS_ExecuteScript(Script: 0x%X macro 0x%X - res:0x%X outSize:%d", VSS_script, VSS_macro, res, outSize);
        }
		
    }
	dlog_msg("getPIN result: %d", ret);
	/*
    #ifdef VFI_PLATFORM_VERIXEVO
    if(open_close_crypto)
    {
        if (hdl >= 0) close(hdl);
    }
    #endif
    */

    return ret;
}
#endif

int cPINdisplayDirectGUI::Clear()
{
    int ret = 0;
    #ifdef VFI_PLATFORM_VERIXEVO
    #ifdef DIRECTGUI_PROXY
    ret = vfiguiproxy::GUIClient::instance().releaseConsole();
    #else
    ret = vfigui::uiReleaseConsole();
    #endif
    dlog_msg("directGUI uiReleaseConsole ret: %d", ret);

    // added for e315, but should be required for every device with cap. kbd
    deactivateCapacitiveKbd();
    #endif
    return ret;
}

#if defined(VFI_PLATFORM_VERIXEVO) && !defined(EMV_ADK_VRXEVO)
int cPINdisplayDirectGUI::GetPIN(c_encPIN encPIN)
{
    Display(encPIN);
    ComputeEntryXY();
	
    dlog_msg("Stars at: %d, %d", x, y);
    ::write_at("", 0, x, y);
    // We have console, so let's just perform PIN entry...
    int hdl = -1;
    if(open_close_crypto)
    {
        int owner = 0;
        hdl = get_owner("/dev/crypto", &owner);
        if (owner != get_task_id())
        {
            hdl = open("/dev/crypto", 0);
        }
        if (hdl < 0)
        {
            dlog_error("Cannot open crypto device!");
            return PIN_VERIFY_ABORT;
        }
    }

    dlog_msg("cPINdisplayDirectGUI::GetPIN, pin_mode: %X", pin_mode);

    int ret=iPS_SetPINParameter(&pKeypadSetup);
    if (ret)
    {
        dlog_msg("iPS_SetPINParameter error");
        switch(ret)
        {
            case E_KM_OUT_OF_RANGE:
               dlog_msg("E_KM_OUT_OF_RANGE");
               break;
            case E_KM_SYSTEM_ERROR:
               dlog_msg("E_KM_SYSTEM_ERROR");
               break;
            case EBADF:
               dlog_msg("EBADF, we don't own crypto!");
               break;
            default:
               dlog_msg("GENERIC ERROR");
               break;
        }
        return -1;
    }
    if (bypassKey != 0xFF)
    {
        dlog_msg("Setting PIN Bypass Key to %02Xh", bypassKey);
        iPS_SetPINBypassKey(bypassKey);
    }
    if((ret = iPS_SelectPINAlgo(pin_mode)))
    {
        dlog_msg("Error setting PIN Algo ret: %X - 1st try", ret);
        if(ret == E_KM_BAD_SEQUENCE)
        {
            iPS_CancelPIN();
            ret = iPS_SelectPINAlgo(pin_mode);
            if(ret != 0)
            {
                dlog_msg("Error setting PIN Algo ret: %X - 2nd try", ret);
            }
        }
        else
        {
            return -1;
        }
    }

    dlog_msg("Trying to call directGUI for PIN entry");

    BeeperThread beeper(pinBeeperTimeout);

    int try_cntr = 1;

    storePinEntryStartTime();

    cancel_reason = 0;

    unsigned char dummy[1];
    int retVal = iPS_RequestPINEntry(1, dummy);
    dlog_msg("Starting PIN entry...");
    if (retVal)
    {
        dlog_msg("iPS_RequestPINEntry error ret: %X", retVal);
        switch(retVal)
        {
          case E_KM_NO_ALGO_SELECTED:
              dlog_msg("E_KM_NO_ALGO_SELECTED");
              break;
          case E_KM_BAD_SEQUENCE:
              dlog_msg("E_KM_BAD_SEQUENCE");
              break;
          case E_KM_SYSTEM_ERROR:
              dlog_msg("E_KM_SYSTEM_ERROR");
              break;
          case E_KM_ACCESS_DENIED:
              dlog_msg("E_KM_ACCESS_DENIED"); // This indicates leaky bucket error, PIN limit exhausted
              break;
          case E_KM_OUT_OF_RANGE:
              dlog_msg("E_KM_OUT_OF_RANGE"); // This indicates leaky bucket error, PIN limit exhausted
              break;
          default:
              dlog_msg("OTHER ERROR");
              break;
        }
        if (open_close_crypto)
        {
            if (hdl >= 0) close(hdl);
        }
        return PIN_VERIFY_ABORT;
    }

    if (pinEntryCallback(this) == false) 
    {
        dlog_msg("cancelling pin entry due to callback");
        iPS_CancelPIN();
        if (PIN_entry_timeout)
            return PIN_VERIFY_TIMEOUT;
        else if (cancel_reason)
            return cancel_reason;
        else
            return PIN_VERIFY_CANCEL;
    }

    long timecurrent = read_ticks();
    long timeout = timecurrent + getPINtimeout();
    long firstchar = timecurrent + getPINfirstchartimeout();
    long nextchar = timecurrent + getPINnextchartimeout();
    bool firstKey = true;
    unsigned char currentKeyCount = 0;
    int pinStatus = 0;
    PINRESULT pinOUTData;
    int nbPinDigits = 0;
    char display_text[M_MAX_PIN_LENGTH_MAX+1];
    break_ret_t breakRes = NoData;
    const int DEFAULT_WAIT_PERIOD = 100; // 100 milliseconds, default
    int waitPeriod;
    dlog_msg("PIN Entry timeout global: %d, first char: %d, interchar: %d", getPINtimeout(), getPINfirstchartimeout(), getPINnextchartimeout());
    dlog_msg("PIN Entry Max size %d", pKeypadSetup.ucMax);
    dlog_msg("PIN entry expected number of digits %d", expPINdigits);
    dlog_msg("PIN entry def char %c echo char %c", pKeypadSetup.ucDefChar, pKeypadSetup.ucEchoChar);

    int result = PIN_VERIFY_ERROR;

    do
    {
        waitPeriod = DEFAULT_WAIT_PERIOD;
        if(!isICCpresent())
        {
            dlog_msg("Card removed!!! - canceling");
            iPS_CancelPIN();
            result = PIN_VERIFY_CARD_REMOVED;
            break;
        }
        else
        {
            dlog_msg("Card in reader, continuing PIN entry... (%d valid digits)", currentKeyCount);
        }
        retVal = iPS_GetPINResponse(&pinStatus, &pinOUTData);
        dlog_msg("iPS_GetPINResponse ret: %d, status: %d, entered digits: %d", retVal, pinStatus, pinOUTData.nbPinDigits);
        if(retVal)
        {
            dlog_msg("Incorrect response from iPS_GetPINResponse: %X", retVal);
            iPS_CancelPIN();
            result = PIN_VERIFY_ERROR;
            break;
        }

        if( (breakRes = CheckBreak()) != NoData)
        {
            int cancel_reason = PIN_VERIFY_CANCEL;
            switch (breakRes)
            {
            case Cancelled:
                dlog_alert("PIN entry cancelled!");
                cancel_reason = PIN_VERIFY_POS_CANCEL;
                break;
            case Bypassed:
                dlog_alert("PIN entry bypassed!");
                cancel_reason = PIN_VERIFY_POS_BYPASS;
                break;
            default:
                dlog_msg("External PIN cancel requested");
            }
            iPS_CancelPIN();
            result = cancel_reason;
            break;
        }
        if (pinOUTData.nbPinDigits != currentKeyCount)
        {
            firstKey = false;
            currentKeyCount = pinOUTData.nbPinDigits;
            nextchar = read_ticks() + getPINnextchartimeout();
        }
        if(getPINfirstchartimeout() && firstKey)
        {
            if (timecurrent > firstchar)
            {
                iPS_CancelPIN();
                dlog_msg("first char timeout");
                result = PIN_VERIFY_TIMEOUT;
                break;
            }
            else if (DEFAULT_WAIT_PERIOD + timecurrent > firstchar) waitPeriod = firstchar - timecurrent;
        }
        if(getPINnextchartimeout() && !firstKey)
        {
            if (timecurrent > nextchar)
            {
                iPS_CancelPIN();
                dlog_msg("next char timeout");
                result = PIN_VERIFY_TIMEOUT;
                break;
            }
            else if (DEFAULT_WAIT_PERIOD + timecurrent > nextchar) waitPeriod = nextchar - timecurrent;
        }
        if(getPINtimeout())
        {
            if (timecurrent > timeout)
            {
                iPS_CancelPIN();
                dlog_msg("local pin entry timeout");
                result = PIN_VERIFY_TIMEOUT;
                break;
            }
            else if (DEFAULT_WAIT_PERIOD + timecurrent > timeout) waitPeriod = timeout - timecurrent;
        }
        switch(pinStatus)
        {
            case 0x00:      //PIN done
                dlog_msg("%X - PIN ready - %d digits", pinStatus, pinOUTData.nbPinDigits);
                result = PIN_VERIFY_OK;
                break;
            case 0x01:      //unit idle
                dlog_msg("%X - global PIN entry timeout", pinStatus);
                result = PIN_VERIFY_TIMEOUT;
                break;
            case 0x02:      //in progress
                dlog_msg("%X - PIN entry in progress: %d digits already entered", pinStatus, pinOUTData.nbPinDigits);
                if(pinOUTData.nbPinDigits != nbPinDigits)
                {
                    #if defined(VFI_PLATFORM_VOS)
                    memset(display_text, 0, M_MAX_PIN_LENGTH_MAX+1);
                    memset(display_text, '*', pinOUTData.nbPinDigits);
                    dlog_msg("updating PIN entry screen (x=%d y=%d) %s", x, y, display_text);
                    nbPinDigits = pinOUTData.nbPinDigits;
                    #endif
                }
                break;
            case 0x05:      //aborted by user
                dlog_msg("%X - PIN entry cancelled", pinStatus);
                result = PIN_VERIFY_CANCEL;
                break;
            case 0x06:      //PIN bypassed
                dlog_msg("%X - PIN entry bypass", pinStatus);
                result = PIN_VERIFY_BYPASS;
                break;
            case 0x0A:      //PIN bypassed
                dlog_msg("%X - PIN entry bypass", pinStatus);
                result = PIN_VERIFY_BYPASS;
                break;
            case 0x0C:      //pin timeout
                dlog_msg("%X - global PIN entry timeout", pinStatus);
                result = PIN_VERIFY_TIMEOUT;
                break;
            default:
                dlog_msg("%X - Unknown PIN entry error", pinStatus);
                result = PIN_VERIFY_ERROR;
                break;
        }

        timecurrent = read_ticks();
        // dlog_msg("Waiting for %d ms", waitPeriod);
        SVC_WAIT(waitPeriod);
    } while(pinStatus == 0x02);

    if(ret == PIN_VERIFY_OK)
    {
        dlog_msg("Trying to perform VSS macro after PIN... (script: 0x%X, macro:0x%X)", VSS_script, VSS_macro);
        if((VSS_script >=0) && (VSS_macro >= 0))
        {
            unsigned char unused[2];
            unsigned short outSize;
            int res = iPS_ExecuteScript(VSS_script, VSS_macro, 0, unused, 0, &outSize, unused);
            dlog_msg("iPS_ExecuteScript(Script: 0x%X macro 0x%X - res:0x%X outSize:%d", VSS_script, VSS_macro, res, outSize);
        }
    }

    if (open_close_crypto)
    {
        if (hdl >= 0) close(hdl);
    }
    return result;
}
#endif

#if defined(VFI_PLATFORM_VERIXEVO) && !defined(EMV_ADK_VRXEVO)
int cPINdisplayDirectGUI::Display(c_encPIN encPIN)
{
    int ret = 0;
    std::map<std::string, std::string> value;

    std::string amtDsp;
    std::string extDsp;

    amtDsp = BuildAmtMsg();
    dlog_msg("Amount text: %s", amtDsp.c_str());
    value["amount"]=amtDsp;

    extDsp = BuildExtraMsg();
    dlog_msg("Extra text: %s", extDsp.c_str());
    value["extra"]=extDsp;

    #ifdef DIRECTGUI_PROXY
    ret = vfiguiproxy::GUIClient::instance().releaseConsole();
    #else
    ret = vfigui::uiReleaseConsole();
    #endif
    dlog_msg("directGUI uiReleaseConsole ret: %d", ret);
    //ret = vfigui::uiInvoke(value, "PIN < input type='pin' name='input' action='return 0'");

    // loading language settings and setting language catalogue
    minIni ini("gui_common.ini");
    #ifndef DIRECTGUI_PROXY
    std::string language = ini.gets("","lang", "");
    if (!language.empty()){
        vfigui::uiSetCatalog(language);
    }
    #endif
    value["logo_image"] = ini.gets("", "logo", "");


    std::string htmlName;
    if(pin_progress == PIN_ENTRY_STATUS_LAST_TRY)
        htmlName = "LastPINscr.html";
    else if(pin_progress == PIN_ENTRY_STATUS_BAD_PIN)
        htmlName = "RetryPINscr.html";
    else
        htmlName = "EnterPINscr.html";

    #ifdef DIRECTGUI_PROXY
    ret = vfiguiproxy::GUIClient::instance().displayURL(makeHtmlName(htmlName), value);
    #else
    ret = vfigui::uiInvokeURL(value, htmlName);
    #endif
    dlog_msg("directGUI uiInvoke ret: %d", ret);

    #ifdef DIRECTGUI_PROXY
    if(ret == vfiguiproxy::result::OK)
    #else
    if(ret == vfigui::UI_ERR_OK)
    #endif
        ret = PIN_VERIFY_OK;
    else
        ret = PIN_VERIFY_ERROR;

    #ifdef DIRECTGUI_PROXY
    ret = vfiguiproxy::GUIClient::instance().obtainConsole();
    #else
    ret = vfigui::uiObtainConsole();
    #endif

    dlog_msg("directGUI uiObtainConsole ret: %d", ret);

    // activating keyboard for cap. devices so pin entry will be possible
    activateCapacitiveKbd();

    // also for e315 overlay scan needs to be performed...
    // this is taken from get_console_handle method above, not sure why this works at all...


    const int OVERLAY_SENSE_SCAN = 0x02;
    const int FW_CHECK_IN_PROGRESS = 0x02;
    const char * MODEL_E315 = "e315";

    char model_number[12+1];

    SVC_INFO_MODELNO (model_number);
    model_number[12]=0;
    dlog_msg("MODEL NUMBER: %s", model_number);

    if(memcmp(model_number, MODEL_E315, strlen(MODEL_E315)) == 0)
    {
        ret = cs_overlay_scan(OVERLAY_SENSE_SCAN);
        dlog_msg("cs_overlay_scan(0x02) RET: %X", ret);
        int i = 0;
        while(i++ < 4 && ret != 0)
        {
            if((ret = cs_overlay_scan(OVERLAY_SENSE_SCAN)) < 0)
            break;
            SVC_WAIT(100);
        }

        do
        {
            if(((ret = is_keypad_secure()) & FW_CHECK_IN_PROGRESS) == 0)
            break;

            SVC_WAIT(100); // Be battery-friendly
        }while (ret & FW_CHECK_IN_PROGRESS);
        dlog_msg("is_keypad_secure() RET: %X", ret);

        ret = cs_set_sleep_state(0);
        dlog_msg("cs_set_sleep_state(0) RET: %d", ret);
    }

    return ret;
}
#endif


#if defined(VFI_PLATFORM_VOS) || defined(EMV_ADK_VRXEVO)
int cPINdisplayDirectGUI::Display(c_encPIN encPIN)
{
    dlog_msg("cPINdisplayDirectGUI::Display, NOT IMPLEMENTED");

    return 0;
}
#endif

bool cPINdisplayDirectGUI::ComputeEntryXY()
{
	dlog_msg("cPINdisplayDirectGUI::ComputeEntryXY - Enter");
    bool result = true;

    #if defined(VFI_PLATFORM_VERIXEVO)
    const char * MODEL_E315 = "e315";
	const char * MODEL_E355 = "E355";
	const char * MODEL_Vx520 = "VX520";
    char model_number[12+1];
    SVC_INFO_MODELNO (model_number);
    model_number[12]=0;

	dlog_msg("cPINdisplayDirectGUI::ComputeEntryXY - Terminal - %s", model_number);

    // looks like this x,y is position in pixels - at least on e315...
    // this is the proper position as long as client won't change html template used...
    if(memcmp(model_number, MODEL_E315, strlen(MODEL_E315)) == 0)
    {
        x = 50;
        y = 24;
    }
	else if(memcmp(model_number, MODEL_E355, strlen(MODEL_E355)) == 0)
	{
		x = 140;
		y = 150;
	}
	else if(memcmp(model_number, MODEL_Vx520, strlen(MODEL_Vx520)) == 0)
	{
		x = 30;
		y = 42;
	}
    else
    // for some reason it was set like this for other devices... this is certainly not true for directgui
    {
        int error = 0;
        int width = -1, height = -1;
        #ifdef DIRECTGUI_PROXY
        vfiguiproxy::GUIClient& gui = vfiguiproxy::GUIClient::instance();
        if ( (error = gui.getProperty(vfiguiproxy::property::DEVICE_WIDTH, width)) ||
            (error = gui.getProperty(vfiguiproxy::property::DEVICE_HEIGHT, height)) )
        #else
        if ( (error = vfigui::uiGetPropertyInt(vfigui::UI_DEVICE_WIDTH , &width)) ||
            (error = vfigui::uiGetPropertyInt(vfigui::UI_DEVICE_HEIGHT , &height)) )
        #endif
        {
            dlog_msg("Could not get screen size, error %d", error);
            return false;
        }
        x = (width / 2); //- 20;  // screen center minus some offset to account for char width
        y = height / 2;
    }
    #else
    x = 8;
    y = 4;
    #endif
    dlog_msg("Entry x, y: %d, %d", x, y);

	dlog_msg("cPINdisplayDirectGUI::ComputeEntryXY - End with result - %x", result);
    return result;
}

void cPINdisplayDirectGUIBase::storePinEntryStartTime(){
    struct timeval tv;
    gettimeofday(&tv, NULL);
    pinEntryStartTime = tv.tv_sec;
    pinEntryKeyTime = tv.tv_sec;
}

void cPINdisplayDirectGUIBase::storePinEntryKeyTime(){
    struct timeval tv;
    gettimeofday(&tv, NULL);
    pinEntryKeyTime = tv.tv_sec;
}


bool cPINdisplayDirectGUIBase::pinEntryTimeExpired(){
    struct timeval tv;
    gettimeofday(&tv, NULL);
    if ((tv.tv_sec - pinEntryStartTime) * 1000 > PINtimeout )
    {   // since PIN timeout is in ms
        PIN_entry_timeout = true;
        return true;
    }
    return false;
}

bool cPINdisplayDirectGUIBase::pinEntryKeyboardTimeExpired()
{
    long timeout = PIN_first_key ? getPINfirstchartimeout() : getPINnextchartimeout();
    if (timeout > 0)
    {
        struct timeval tv;
        gettimeofday(&tv, NULL);
        if ((tv.tv_sec - pinEntryKeyTime) * 1000 > timeout )
        {
            PIN_entry_timeout = true;
            return true;
        }
    }
    return false;
}

#endif


#if defined(VFI_GUI_GUIAPP) && defined(VFI_PLATFORM_VOS)

cPINdisplayGuiApp::cPINdisplayGuiApp(): cPINdisplay()
{
    using namespace com_verifone_guiapi;

    if( GuiApp_Connect( (char *)CARD_APP_NAME, 10000 ) == GUI_OKAY )
    {
        guiConnected = true;
        dlog_msg( "Connected from %s to %s", CARD_APP_NAME, GUI_APP_NAME_ );
    }
    else
    {
        guiConnected = false;
    }
    pin_mode = M_PIN_EMV;
}
cPINdisplayGuiApp::~cPINdisplayGuiApp()
{
    // restore console
    using namespace com_verifone_guiapi;
    dlog_msg("Destroying GuiApp PIN object");
    if (guiConnected) com_verifone_guiapi::GuiApp_Disconnect();
}

int cPINdisplayGuiApp::Clear()
{
    return GuiApp_ClearDisplay(0);
}

int cPINdisplayGuiApp::Display(c_encPIN encPIN)
{
    using namespace com_verifone_guiapi;
    const currency_code_t *pCurrCode = &M_CURRENCY_CODES[0];
    char strPIN[ADD_SCREEN_TEXT+1];
    strPIN[0]=0;
    uint16_t trn_curr = 0;
    std::string amtDsp;
    std::string extDsp;
    int retGUI = GUI_CMD_FAIL;

    amtDsp = BuildAmtMsg();
    dlog_msg("Amount text: %s", amtDsp.c_str());
    extDsp = BuildExtraMsg();
    dlog_msg("Extra text: %s", extDsp.c_str());
    strPIN[0]=0;

    if(amtDsp.size() == 0 )
    {
        amtDsp.assign("==============");
    }

    if(extDsp.size() == 0)
    {
        extDsp.assign("ENTER YOUR PIN");
    }

    int prompt_id = 1;

    if(pin_progress == PIN_ENTRY_STATUS_LAST_TRY)
        prompt_id = 3;
    else if(pin_progress == PIN_ENTRY_STATUS_BAD_PIN)
        prompt_id = 2;
    else
        prompt_id = 1;

    if (guiConnected)
    {
        dlog_msg("Getting PIN in CARDAPP");
        if((retGUI = GuiApp_DisplayPINPromptEx( prompt_id, const_cast<char *>(extDsp.c_str()), const_cast<char *>(amtDsp.c_str()), &x, &y, CARD_APP_GUI_S_PIN)) != GUI_OKAY)
        {
            dlog_msg("ERROR displaying PIN prompt, EXITING... ret: %d", retGUI);
            return retGUI;
        }
    }
    else
    {
        dlog_msg("ERROR! GUIAPP not connected!");
        return GUI_INIT_ERROR;
    }

    return GUI_OKAY;
}
bool cPINdisplayGuiApp::ComputeEntryXY()
{
    using namespace com_verifone_guiapi;

    long x_pos = 0, y_pos = 0, x_font = 0, y_font = 0;
    int m_Console = -1;
    bool result = false;
    /*
    if (GuiApp_GetPINBoxCoordinates(&x_pos, &y_pos) == GUI_OKAY)
    {

        y_font = 8;
        x_font = 8;
        dlog_msg("Font size: %d:%d", x_font, y_font);
        x = x_pos/x_font-M_MAX_PIN_LENGTH_MAX/2;
        y = y_pos/y_font;
        dlog_msg("Received PIN box coords: %d x %d", x_pos, y_pos);
        dlog_msg("PIN boox coordinates: %d x %d", x, y);
        result = true;

    }
    else
    {
        dlog_msg("ERROR getting PIN box coordinates");
        y_font = 8;
        x_font = 8;
        dlog_msg("Font size: %d:%d", x_font, y_font);
        x = x/x_font-M_MAX_PIN_LENGTH_MAX/2;
        y = y/y_font+2;
        dlog_msg("APPROX PIN boox coordinates: %d x %d", x, y);
    }
    */

    x = 8;
    y = 4;
    return result;
}

#endif

#if defined(VFI_GUI_GUIAPP)
int gui_cancel_func(void * ptr)
{
    cPINdisplay * obj = reinterpret_cast<cPINdisplay *>(ptr);
    if (obj && obj->should_cancel()) return 1;
    return 0;
}
#if defined(VFI_PLATFORM_VOS)
int cPINdisplayGuiApp::GetPIN(c_encPIN encPIN)
{
    if (guiConnected)
    {
        int prompt_id = 1;

        if(pin_progress == PIN_ENTRY_STATUS_LAST_TRY)
        {
            prompt_id = 3;
        }
        else if(pin_progress == PIN_ENTRY_STATUS_BAD_PIN)
        {
            prompt_id = 2;
        }
        else
        {
            prompt_id = 1;
        }

        uint16_t trn_curr = 0;
        int retGUI = GUI_CMD_FAIL;

        std::string amtDsp = BuildAmtMsg();
        dlog_msg("Amount text: %s", amtDsp.c_str());
        std::string extDsp = BuildExtraMsg();
        dlog_msg("Extra text: %s", extDsp.c_str());

        /*
        //temporary - must be read from resources
        switch(prompt_id)
        {
          case 1:
            extDsp = "ENTER PIN";
            //extDsp = g_CardAppConfig.GetEnterPINMsg();
            break;
          case 2:
            extDsp = "INCORRECT PIN";
            //extDsp = g_CardAppConfig.GetIncorrectPINMsg();
            break;
          case 3:
            extDsp = "LAST PIN TRY";
            //extDsp = g_CardAppConfig.GetLastPINMsg();
            break;
        }
        */
        char pindigits[12];
        srPINParams p_params;
        dlog_msg("Switching into GUIAPP for PIN entry");
        p_params.ulWaitTime = getPINtimeout();
        if (p_params.ulWaitTime == 0) p_params.ulWaitTime = GUI_TIMEOUT_INFINITE;
        p_params.ulFirstKeyTimeOut = getPINfirstchartimeout();
        if (p_params.ulFirstKeyTimeOut == 0) p_params.ulFirstKeyTimeOut = GUI_TIMEOUT_INFINITE;
        p_params.ulInterCharTimeOut = getPINnextchartimeout();
        if (p_params.ulInterCharTimeOut == 0) p_params.ulInterCharTimeOut = GUI_TIMEOUT_INFINITE;
        p_params.ucEchoChar = pKeypadSetup.ucEchoChar;
        p_params.ucDefChar = pKeypadSetup.ucDefChar;
        p_params.ucDspLine = p_params.ucDspCol = 0;
        p_params.ucOption = pKeypadSetup.ucOption;
        p_params.ucMin = pKeypadSetup.ucMin;
        p_params.ucMax = pKeypadSetup.ucMax;
        p_params.abortOnPINEntry = 1;
        p_params.ucPINBypassKey = bypassKey;

        cancel_reason = 0;
        if((retGUI = GuiApp_GetPinExPrompts(const_cast<char *>(extDsp.c_str()) , 0, const_cast<char *>(amtDsp.c_str()), prompt_id, pindigits, &p_params, pin_mode, gui_cancel_func, this, NULL, "pin")) < GUI_OKAY)
        {
            dlog_msg("ERROR getting PIN in GUIAPP, EXITING... ret: %d (GUI %d)", cancel_reason, retGUI);
            if (cancel_reason) return cancel_reason;
            switch (retGUI)
            {
                case GUI_TIMEOUT: return PIN_VERIFY_TIMEOUT;
                case GUI_CANCELLED: return PIN_VERIFY_CANCEL;
                case GUI_POS_CANCELLED: return PIN_VERIFY_POS_CANCEL;
                case GUI_PIN_BYPASSED_NOPWD: // fall back to next case
                case GUI_PIN_BYPASSED: return PIN_VERIFY_BYPASS;
                default:
                case GUI_CMD_FAIL: return PIN_VERIFY_ABORT;
            }
            // return PIN_VERIFY_ERROR;
        }
        return PIN_VERIFY_OK;
    }
    else
    {
        dlog_msg("ERROR! GUIAPP not connected!");
        return PIN_VERIFY_ERROR;
    }

}
#endif // VFI_PLATFORM_VOS

#if defined(VFI_PLATFORM_VERIXEVO)
int cPINdisplayGuiApp::GetPIN(c_encPIN encPIN)
{
    // Display first
    if (Display(encPIN) != 1)
    {
        dlog_error("Display error");
        return PIN_VERIFY_ABORT;
    }
    int owner = 0;
    int hdl = get_owner("/dev/crypto", &owner);
    if (owner != get_task_id())
    {
        hdl = open("/dev/crypto", 0);
    }
    if (hdl < 0)
    {
        dlog_error("Cannot open crypto device!");
        return PIN_VERIFY_ABORT;
    }
    PINRESULT pinOUTData;
    int retVal;

    retVal=iPS_SetPINParameter(&pKeypadSetup);
    if(retVal)
    {
        dlog_msg("iPS_SetPINParameter error ret: %X", retVal);
        switch(retVal)
        {
            case E_KM_OUT_OF_RANGE:
               dlog_msg("E_KM_OUT_OF_RANGE");
               break;
            case E_KM_SYSTEM_ERROR:
               dlog_msg("E_KM_SYSTEM_ERROR");
               break;
            default:
               dlog_msg("GENERIC ERROR");
               break;
        }
        return PIN_VERIFY_ABORT;
    }

    if (bypassKey != 0xFF)
    {
        dlog_msg("Setting PIN Bypass Key to %02Xh ucOptions: %.02X", bypassKey, pKeypadSetup.ucOption);
		
        #ifdef VFI_PLATFORM_VERIXEVO
        iPS_SetPINBypassKey(bypassKey);
        #else
        iPS_SetPinBypassKey(bypassKey);
        #endif

    }

	
	dlog_msg("Setting PIN ucOptions: %.02X", pKeypadSetup.ucOption);

    if((retVal = iPS_SelectPINAlgo(pin_mode)))
    {
        dlog_msg("Error setting PIN Algo ret: %X - 1st try", retVal);
        if(retVal == E_KM_BAD_SEQUENCE)
        {
            iPS_CancelPIN();
            retVal = iPS_SelectPINAlgo(pin_mode);
            if(retVal != 0)
            {
                dlog_msg("Error setting PIN Algo ret: %X - 2nd try", retVal);
            }
        }
        else
        {
            return PIN_VERIFY_ABORT;
        }
    }

    unsigned char dummy[1];
    int pinStatus = 0;
    retVal=iPS_RequestPINEntry(0, dummy);
    dlog_msg("Starting PIN entry...");
    if(retVal)
    {
         dlog_msg("iPS_RequestPINEntry error ret: %X", retVal);
         switch(retVal)
         {
            case E_KM_NO_ALGO_SELECTED:
                dlog_msg("E_KM_NO_ALGO_SELECTED");
                break;
            case E_KM_BAD_SEQUENCE:
                dlog_msg("E_KM_BAD_SEQUENCE");
                break;
            case E_KM_SYSTEM_ERROR:
                dlog_msg("E_KM_SYSTEM_ERROR");
                break;
            case E_KM_ACCESS_DENIED:
                dlog_msg("E_KM_ACCESS_DENIED"); // This indicates leaky bucket error, PIN limit exhausted
                break;
            default:
                dlog_msg("OTHER ERROR");
                break;
        }
        return PIN_VERIFY_ABORT;


    }

    long timecurrent = read_ticks();
    long timeout = timecurrent + getPINtimeout();
    long firstchar = timecurrent + getPINfirstchartimeout();
    long nextchar = timecurrent + getPINnextchartimeout();
    bool firstKey = true;
    unsigned char currentKeyCount = 0;
    pinStatus = 0;
    int nbPinDigits = 0;
    char display_text[M_MAX_PIN_LENGTH_MAX+1];
    break_ret_t breakRes = NoData;
    const int DEFAULT_WAIT_PERIOD = 100; // 100 milliseconds, default
    int waitPeriod;
    dlog_msg("PIN Entry timeout global: %d, first char: %d, interchar: %d", getPINtimeout(), getPINfirstchartimeout(), getPINnextchartimeout());
	
    do
    {
        waitPeriod = DEFAULT_WAIT_PERIOD;
        if(!isICCpresent())
        {
            dlog_msg("Card removed!!! - canceling");
            iPS_CancelPIN();
            return PIN_VERIFY_CARD_REMOVED;
        }
        retVal = iPS_GetPINResponse(&pinStatus, &pinOUTData);
        dlog_msg("iPS_GetPINResponse ret: %d, status: %d, entered digits: %d", retVal, pinStatus, pinOUTData.nbPinDigits);

		
			
        if(retVal)
        {
            iPS_CancelPIN();
            return PIN_VERIFY_ERROR;
        }
        if( (breakRes = CheckBreak()) != NoData)
        {
            int cancel_reason = PIN_VERIFY_CANCEL;
            switch (breakRes)
            {
            case Cancelled:
                dlog_alert("PIN entry cancelled!");
                cancel_reason = PIN_VERIFY_POS_CANCEL;
                break;
            case Bypassed:
                dlog_alert("PIN entry bypassed!");
                cancel_reason = PIN_VERIFY_POS_BYPASS;
                break;
            default:
                dlog_msg("External PIN cancel requested");
            }
            iPS_CancelPIN();
            return cancel_reason;
        }
        if (pinOUTData.nbPinDigits != currentKeyCount)
        {
            firstKey = false;
            currentKeyCount = pinOUTData.nbPinDigits;
            nextchar = read_ticks() + getPINnextchartimeout();
        }
        if(getPINfirstchartimeout() && firstKey)
        {
            if (timecurrent > firstchar)
            {
                iPS_CancelPIN();
                dlog_msg("first char timeout");
                return PIN_VERIFY_TIMEOUT;
            }
            else if (DEFAULT_WAIT_PERIOD + timecurrent > firstchar) waitPeriod = firstchar - timecurrent;
        }
        if(getPINnextchartimeout() && !firstKey)
        {
            if (timecurrent > nextchar)
            {
                iPS_CancelPIN();
                dlog_msg("next char timeout");
                return PIN_VERIFY_TIMEOUT;
            }
            else if (DEFAULT_WAIT_PERIOD + timecurrent > nextchar) waitPeriod = nextchar - timecurrent;
        }
        if(getPINtimeout())
        {
            if (timecurrent > timeout)
            {
                iPS_CancelPIN();
                dlog_msg("local pin entry timeout");
                return PIN_VERIFY_TIMEOUT;
            }
            else if (DEFAULT_WAIT_PERIOD + timecurrent > timeout) waitPeriod = timeout - timecurrent;
        }
        switch(pinStatus)
        {
            case 0x00:      //PIN done
                dlog_msg("%X - PIN ready - %d digits", pinStatus, pinOUTData.nbPinDigits);
                return PIN_VERIFY_OK;
            case 0x01:      //unit idle
                dlog_msg("%X - global PIN entry timeout", pinStatus);
                return PIN_VERIFY_TIMEOUT;
            case 0x02:      //in progress
                dlog_msg("%X - PIN entry in progress: %d digits already entered", pinStatus, pinOUTData.nbPinDigits);
                if(pinOUTData.nbPinDigits != nbPinDigits)
                {
                    #if defined(VFI_PLATFORM_VOS)
                    memset(display_text, 0, M_MAX_PIN_LENGTH_MAX+1);
                    memset(display_text, '*', pinOUTData.nbPinDigits);
                    dlog_msg("updating PIN entry screen (x=%d y=%d) %s", x, y, display_text);
                    if (guiConnected) GuiApp_DisplayText(display_text, x, y, 0, 0);
                    nbPinDigits = pinOUTData.nbPinDigits;
                    #endif
                }
                break;
            case 0x05:      //aborted by user
                dlog_msg("%X - PIN entry cancelled", pinStatus);
                return PIN_VERIFY_CANCEL;
            case 0x06:      //PIN bypassed
                dlog_msg("%X - PIN entry bypass", pinStatus);
                return PIN_VERIFY_BYPASS;
            case 0x0A:      //PIN bypassed
                dlog_msg("%X - PIN entry bypass", pinStatus);
                return PIN_VERIFY_BYPASS;
            case 0x0C:      //pin timeout
                dlog_msg("%X - global PIN entry timeout", pinStatus);
                return PIN_VERIFY_TIMEOUT;
            default:
                dlog_msg("%X - Unknown PIN entry error", pinStatus);
                return PIN_VERIFY_ERROR;
        }

        timecurrent = read_ticks();
        // dlog_msg("Waiting for %d ms", waitPeriod);
        SVC_WAIT(waitPeriod);
    }while(pinStatus == 0x02);

    return PIN_VERIFY_ERROR;
}
#endif
#endif //VFI_GUI_GUIAPP

#if defined(VFI_GUI_GUIAPP) && defined(VFI_PLATFORM_VERIXEVO)

#define OVERLAY_SENSE_SCAN          0x02
#define FW_CHECK_IN_PROGRESS        0x02
#define MODEL_VX600     "VX600"
#define CARD_APP_GUI_S_PROMPTS      "prompts"
#define CARD_APP_GUI_S_EMV          "emv"
#define CARD_APP_GUI_S_PIN          "pin"


int get_console_handle()
{
    int m_console = get_console(1);
    int iRet = 0;
    char model_number[12+1];

    SVC_INFO_MODELNO (model_number);
    model_number[12]=0;
    dlog_msg("MODEL NUMBER: %s", model_number);

    if (m_console < 0)
    {
        dlog_alert("get_console() failed, errno %d", errno);
        int consoleOwner = 0;
        m_console = get_owner(DEV_CONSOLE, &consoleOwner);
        dlog_msg("Console owner task id %d, our id %d, handle %d", consoleOwner, get_task_id(), m_console);
        if (consoleOwner == 0)
        {
            m_console = open(DEV_CONSOLE, 0);
            dlog_msg("Console open result %d", m_console);
        }
        else if(consoleOwner != get_task_id())
        {
          //console is not available.. checking for 3 times more...
          int i = 0;
          m_console = -1;
          while(i++ < 4 && m_console < 0)
          {
              if((m_console = open(DEV_CONSOLE, 0)) != -1)
                break;
              SVC_WAIT(100);
          }
        }
        if(m_console >= 0)
        {
            iRet = cs_set_sleep_state(0);
            dlog_msg("Vx600: cs_set_sleep_state(0) RET: %d", iRet);
        }
    }
    if(m_console >= 0)
    {
        if(memcmp(model_number, MODEL_VX600, strlen(MODEL_VX600)) == 0)
        {
            iRet = cs_overlay_scan(OVERLAY_SENSE_SCAN);
            dlog_msg("vx600: cs_overlay_scan(0x02) RET: %X", iRet);
            int i = 0;
            while(i++ < 4 && iRet != 0)
            {
                if((iRet = cs_overlay_scan(OVERLAY_SENSE_SCAN)) < 0)
                    break;
                SVC_WAIT(100);
            }
            //this code has problems on Vx600 Gen2.5

            //if(iRet == 0)
            //{
            //    do
            //    {
            //        iRet = cs_spi_cmd_status();
            //       SVC_WAIT(100); // Be battery-friendly
            //    } while (iRet == 0);
            //}
            //else
            //{
            //    return -1;
            //}

            do
            {
                if(((iRet = is_keypad_secure()) & FW_CHECK_IN_PROGRESS) == 0)
                    break;

                SVC_WAIT(100); // Be battery-friendly
            } while (iRet & FW_CHECK_IN_PROGRESS);
            dlog_msg("Vx600: is_keypad_secure() RET: %X", iRet);

            iRet = cs_set_sleep_state(0);
            dlog_msg("Vx600: cs_set_sleep_state(0) RET: %d", iRet);
        }
    }

    return m_console;
}



cPINdisplayGuiApp::cPINdisplayGuiApp(): cPINdisplay(), guiConnected(false)
{
    pin_progress = 0;
    using namespace com_verifone_guiapi;
    //const char * const CARD_APP_NAME_ = "CARDAPP";
    //const char * const GUI_APP_NAME_ = "GUIAPP";
    if( GuiApp_Connect( (char *)CARD_APP_NAME, 10000 ) == GUI_OKAY )
    {
        guiConnected = true;
        dlog_msg( "Connected from %s to %s", CARD_APP_NAME, GUI_APP_NAME_ );
    }

}
cPINdisplayGuiApp::~cPINdisplayGuiApp()
{
    // restore console
    using namespace com_verifone_guiapi;
    dlog_msg("Destroying GuiApp PIN object");
    if (consoleClaimed) GuiApp_RestoreConsole();
    if (guiConnected) GuiApp_Disconnect();
    //if (!pinExecuted) g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_DONE);
}
int cPINdisplayGuiApp::Display(c_encPIN encPIN)
{
    using namespace com_verifone_guiapi;
    const currency_code_t *pCurrCode = &M_CURRENCY_CODES[0];
    char strPIN[ADD_SCREEN_TEXT+1];
    strPIN[0]=0;
    uint16_t trn_curr = 0;
    std::string amtDsp;
    std::string extDsp;
    int iRet = 0;
    int retGUI = GUI_CMD_FAIL;

    int prompt_id = 1;

    if(pin_progress == PIN_ENTRY_STATUS_LAST_TRY)
        prompt_id = 3;
    else if(pin_progress == PIN_ENTRY_STATUS_BAD_PIN)
        prompt_id = 2;
    else
        prompt_id = 1;

    pinExecuted = true;
    amtDsp = BuildAmtMsg();
    dlog_msg("Amount text: %s", amtDsp.c_str());
    extDsp = BuildExtraMsg();
    dlog_msg("Extra text: %s", extDsp.c_str());
    strPIN[0]=0;


    //if (consoleClaimed)
    if(1)
    {
        // We have to restore the console!!!
        iRet = GuiApp_RestoreConsole();
        dlog_msg("Restoring console ret: %d", iRet);
        consoleClaimed = false;
    }
    dlog_msg("pin progress %d, prompt id %d", pin_progress, prompt_id);
    iRet = GuiApp_DisplayPINPromptEx( prompt_id, const_cast<char *>(extDsp.c_str()), const_cast<char *>(amtDsp.c_str()), &x, &y, CARD_APP_GUI_S_PIN);
    dlog_msg("GuiApp_DisplayPINPromptEx, ret: %X, x:%d, y:%d prompt_id: %d", iRet, x, y, prompt_id);
    // transfer console
    iRet = GuiApp_TransferConsole();
    dlog_msg("GuiApp_TransferConsole, ret: %X", iRet);
    if(iRet == 0)
    {
        consoleClaimed = true;
    }

    //SB: get_console_handle must be called for Vx600
    //TODO: check if this call will be ok for Vx820

    //if (get_console(1) >= 0)

    if(get_console_handle()>0)
    {
        set_display_coordinate_mode(CHARACTER_MODE);
        dlog_msg("SB:console opened successfully");
        int x_font, y_font;
        switch(getgrid())
        {
          case 0:
            x_font = 8;
            y_font = 16;
            break;
          case 2:
            x_font = 6;
            y_font = 8;
            break;
          default:
            y_font = 8;
            x_font = 8;
            break;
        }
        x = x/x_font-M_MAX_PIN_LENGTH_MAX/2;
        y = y/y_font+2;
        dlog_msg("SB:console opened successfully (x:%d y:%d)", x, y);
        write_at(" ", 1, x, y);
    } else {
        dlog_msg("Could not get console handle");
    }

    //write_at("", 0, 9, 14);
    //write_at("", 0, x_pos/x_font+M_MAX_PIN_LENGTH_MAX/2, y_pos/y_font+2);
    return 1;
}
bool cPINdisplayGuiApp::ComputeEntryXY()
{
    using namespace com_verifone_guiapi;

    long x_pos = 0, y_pos = 0, x_font = 0, y_font = 0;
    int m_Console = -1;
    bool result = false;
    if (GuiApp_GetPINBoxCoordinates(&x_pos, &y_pos) == GUI_OKAY)
    {
        GuiApp_TransferConsole();
        if((m_Console = get_console(1)) > 0)
        {
            dlog_msg("Getting console handle %d", m_Console);
            switch(getgrid())
            {
                case 0:
                    x_font = 8;
                    y_font = 16;
                    break;
                case 2:
                    x_font = 6;
                    y_font = 8;
                    break;
                default:
                    y_font = 8;
                    x_font = 8;
                    break;
            }
            //screen_size(xy_screen);
            dlog_msg("Font size: %d:%d", x_font, y_font);
            //write_at("", 0, ((x_screen/x_font)/4)+((M_MAX_PIN_LENGTH_MAX-maxPINLength)/2), y_pos/y_font+2);
            //write_at("", 0, x_pos/x_font+M_MAX_PIN_LENGTH_MAX/2, y_pos/y_font+2);
            x = x_pos/x_font-M_MAX_PIN_LENGTH_MAX/2;
            y = y_pos/y_font+2;
            dlog_msg("Received PIN box coords: %d x %d", x_pos, y_pos);
            dlog_msg("PIN boox coordinates: %d x %d", x, y);
            GuiApp_RestoreConsole();
            result = true;
        }
    }
    return result;
}



int cPINdisplayGuiApp::Clear()
{
    if (consoleClaimed)
    {
        // We have to restore the console!!!
        GuiApp_RestoreConsole();
        consoleClaimed = false;
    }
    return GuiApp_ClearDisplay(0);
}

/*
int cPINdisplayGuiApp::GetPIN(c_encPIN encPIN)
{
    return PIN_VERIFY_OK;

}
*/
#endif

/* For PIN Library only */

//implementation of FLexi method using directGUI for screen preparation and own PIN collection - currently for VOS only!

#ifdef VFI_GUI_DIRECTGUI
#ifdef VFI_PLATFORM_VOS

cPINdisplayFlexi::cPINdisplayFlexi(long timeout): cPINdisplayDirectGUIBase(timeout)
{
    cancel_reason = 0;
    dlog_msg("registering PIN entry callback");
}

cPINdisplayFlexi::~cPINdisplayFlexi()
{
    // clear screen
    //dlog_msg("Destroying cPINdisplay object");
	#if 0
    #ifdef DIRECTGUI_PROXY
    //vfiguiproxy::GUIClient::instance().displayHTML("");
    vfiguiproxy::GUIClient::instance().clearMissedNotifications();
    ux uxHandler;
    if (uxHandler.is_ux())
    {
        if (vfiguiproxy::GUIClient::instance().cancel(UI_REGION_KBD_MONITORING) == vfiguiproxy::result::OK)
        {
            std::map<std::string, std::string> ignored;
            vfiguiproxy::GUIClient::instance().receiveResult(ignored);
        }
    }
    #else
    vfigui::uiInvoke("");
    #endif
	#endif

	dlog_msg("cPINdisplay object succesfully destroyed!");
}

int cPINdisplayFlexi::Display(c_encPIN encPIN)
{
    dlog_msg("cPINdisplayFlexi::Display, NOT IMPLEMENTED");

    return 1;
}

std::string buildPINbox(int num_dig, int max_digits, int exp_digits, char bg, char star)
{
    std::string pin_box;
    int i = 0;

    if((num_dig > max_digits) || (max_digits < exp_digits))
        return pin_box;

    while(i < num_dig)
    {
        pin_box+=star;
        i++;
    }
    while((i < max_digits) && (i < exp_digits))
    {
        pin_box+=bg;
        i++;
    }

    return pin_box;

}

int cPINdisplayFlexi::GetPIN(c_encPIN encPIN)
{
    ux uxHandler;
    std::map<std::string, std::string> value;

    char html_file[50];
    char html_warning[50];

	
	int inprogress = 0;
	int count = 0;
	int lastNonNumericKey = 0;
	
    // Display first
    if (Display(encPIN) != 1)
    {
        dlog_error("Display error");
        return PIN_VERIFY_ABORT;
    }

    #ifdef VFI_PLATFORM_VERIXEVO
    int owner = 0;
    int hdl = get_owner("/dev/crypto", &owner);
    if (owner != get_task_id())
    {
        hdl = open("/dev/crypto", 0);
    }
    if (hdl < 0)
    {
        dlog_error("Cannot open crypto device!");
        return PIN_VERIFY_ABORT;
    }
    #endif

    PINRESULT pinOUTData;
    int retVal;

    retVal=iPS_SetPINParameter(&pKeypadSetup);
    if(retVal)
    {
        dlog_msg("iPS_SetPINParameter error ret: %X", retVal);
        switch(retVal)
        {
            case E_KM_OUT_OF_RANGE:
               dlog_msg("E_KM_OUT_OF_RANGE");
               break;
            case E_KM_SYSTEM_ERROR:
               dlog_msg("E_KM_SYSTEM_ERROR");
               break;
            default:
               dlog_msg("GENERIC ERROR");
               break;
        }
        return PIN_VERIFY_ABORT;
    }
    dlog_msg("Setting PIN parameters ret: %X", retVal);

    if (bypassKey != 0xFF)
    {
        dlog_msg("Setting PIN Bypass Key to %02Xh", bypassKey);
        #ifdef VFI_PLATFORM_VERIXEVO
        iPS_SetPINBypassKey(bypassKey);
        #else
        iPS_SetPinBypassKey(bypassKey);
        #endif

    }

    if((retVal = iPS_SelectPINAlgo(pin_mode)))
    {
        dlog_msg("Error setting PIN Algo ret: %X - 1st try", retVal);
        if(retVal == E_KM_BAD_SEQUENCE)
        {
            iPS_CancelPIN();
            retVal = iPS_SelectPINAlgo(pin_mode);
            if(retVal != 0)
            {
                dlog_msg("Error setting PIN Algo ret: %X - 2nd try", retVal);
            }
        }
        else
        {
            return PIN_VERIFY_ABORT;
        }
    }
    dlog_msg("Setting PIN entry mode to %X ret:%X", pin_mode, retVal);

    value["pinline"]= buildPINbox(0, pKeypadSetup.ucMax, expPINdigits, pKeypadSetup.ucDefChar, pKeypadSetup.ucEchoChar);
    std::string amtDsp = BuildAmtMsg();
    dlog_msg("Amount text: %s", amtDsp.c_str());
    value["amount"]=amtDsp;

    std::string extDsp = BuildExtraMsg();
    dlog_msg("Extra text: %s", extDsp.c_str());
    value["extra"]=extDsp;

    if(pin_progress == PIN_ENTRY_STATUS_LAST_TRY)
        value["enter_pin"] = "last_pin_try";
    else if(pin_progress == PIN_ENTRY_STATUS_BAD_PIN)
        value["enter_pin"] = "retry_pin";
    else
        value["enter_pin"] = "enter_pin";


    for(std::map<std::string, std::string>::iterator it = HTML_labels.begin(); it!=HTML_labels.end(); ++it)
    {
        value[(it->first)] = (it->second);
    }

    minIni ini("gui_common.ini");
    #ifndef DIRECTGUI_PROXY
    std::string language = ini.gets("","lang", "");
    if (!language.empty()){
        int tmp = vfigui::uiSetCatalog(language);
        dlog_msg("Set lang to %s: %d", language.c_str(), tmp);
    }
    #endif


    cancel_reason = 0;

    EntryCallBack(0, 0x02, html_file, html_warning);
    if(strlen(html_warning) && warning_msg.size())
    {
        value["warning"]=warning_msg;
        dlog_msg("Displaying PIN status screen: %s msg: %s", html_warning, warning_msg.c_str());
        #ifdef DIRECTGUI_PROXY
        vfiguiproxy::GUIClient::instance().displayURL(makeHtmlName(html_warning), value);
        #else
        vfigui::uiInvokeURL(value, html_warning);
        #endif
        SVC_WAIT(1500);
    }

    BeeperThread beeper(pinBeeperTimeout);

    //checking terminal type (is it Ux?)
    if (uxHandler.is_ux())
    {
        // This is Ux, return 0 and wait for events
        retVal = uxHandler.pinStartPinEntry(pKeypadSetup.ucMin, pKeypadSetup.ucMax, getPINtimeout());
        if (retVal)
        {
            dlog_error("Start PIN entry on UX100 failed, error %d", retVal);
            return PIN_VERIFY_ERROR;
        }
        unsigned char secure_mode; 
        unsigned char nb_function_key;
        unsigned char nb_normal_key;
        unsigned char nb_row;
        unsigned char nb_col;
        unsigned char key_map[3*5];
        uxHandler.getKbdStatus(&secure_mode, &nb_function_key, &nb_normal_key, &nb_row, &nb_col, key_map);
        uxHandler.pinDelete(0);
        int key_pressed = 0;
        int num_digits = 0;
        int pre_digits = 0;
        int entry_mode;
        const int DEFAULT_WAIT_PERIOD = 100; // 100 milliseconds, default - another 100 ms is enforced by pinEntryCallback

        storePinEntryStartTime();
        EntryCallBack(num_digits, key_pressed, html_file, html_warning);
        dlog_msg("Starting UX100 PIN entry...");
        dlog_msg("Start keyboard monitoring in region: %d", UI_REGION_KBD_MONITORING);

        int guiRet = 0;

        do
        {
            //key_pressed = vfigui::uiInvokeURL(UI_REGION_KBD_MONITORING, html_file);
            #ifdef DIRECTGUI_PROXY
            guiRet = vfiguiproxy::GUIClient::instance().displayURL(makeHtmlName(html_file), value);
            dlog_msg("START WAITING FOR KEY PRESS kbd secure: %d", uxHandler.pinEntryMode());
            std::map<std::string, std::string> params;
            key_pressed = vfiguiproxy::GUIClient::instance().displayURLAsync(UI_REGION_KBD_MONITORING, makeHtmlName("kbd_monitor.html"), params);
            if (key_pressed < 0 && key_pressed != vfiguiproxy::result::GUIPROXY_REGION_BUSY)
            {
                dlog_error("Error displaying HTML %d", key_pressed);
                uxHandler.pinAbort();
                return PIN_VERIFY_ABORT;
            }

            do
            {
                if (pinEntryCallback(this) == false) 
                {
                    dlog_msg("cancelling pin entry due to callback");
                    vfiguiproxy::GUIClient::instance().cancel(UI_REGION_KBD_MONITORING);
                    uxHandler.pinAbort();
                    key_pressed = vfiguiproxy::result::ABORT;
                    break;
                }
                key_pressed = vfiguiproxy::GUIClient::instance().pollAsyncResult(params, DEFAULT_WAIT_PERIOD);
                if (vfiguiproxy::GUIClient::instance().getMissedNotificationsCount())
                {
                    int region, res;
                    while ( (res = vfiguiproxy::GUIClient::instance().getLastMissedNotification(params, region)) != vfiguiproxy::result::ERROR)
                    {
                        if (region == UI_REGION_KBD_MONITORING) key_pressed = res;
                    }
                }
            } while (key_pressed == vfiguiproxy::result::POLL_TIMEOUT);


            dlog_msg("GUI ret: %d", key_pressed);


            #else
            vfigui::uiInvokeURL(value, html_file);
            key_pressed = vfigui::uiInvokeURL(UI_REGION_KBD_MONITORING, "kbd_monitor.html", pinEntryCallback, this);
            #endif
            //key_pressed = uxHandler.keybdGetKey(0xFFFFFFFF);
            dlog_msg("Got KeyPressed event: %d (0x%X) kbd secure: %d", key_pressed, key_pressed, uxHandler.pinEntryMode());

            if ((bypassKey != 0xFF) && (pKeypadSetup.ucOption & 0x02))
            {
                if((key_pressed == bypassKey) && (num_digits == 0))
                {
                    dlog_msg("PIN bypass!");
                    uxHandler.pinAbort();
                    return PIN_VERIFY_BYPASS;
                }
            }

            switch(key_pressed)
            {
                case UX100_KEY_NUM:
                    dlog_msg("Numeric key pressed!");
                    PIN_first_key = false;
                    storePinEntryKeyTime();
                    break;
                case UX100_KEY_OK:
                    if((num_digits == 0) && (pKeypadSetup.ucOption & 0x02))
                    {
                        dlog_msg("PIN bypass!");
                        uxHandler.pinAbort();
                        return PIN_VERIFY_BYPASS;
                    }
                    else if((num_digits >= 4) && (pKeypadSetup.ucMax >= num_digits) && (pKeypadSetup.ucMin <= num_digits))
                    {
                        dlog_msg("PIN entry completed sending to vault!");
                        if(uxHandler.pinSendEncPinBlockToVault() == 0)
                        {
                            dlog_msg("PIN entered correctly");
                            uxHandler.pinDelete(0);
                            return PIN_VERIFY_OK;
                        }
                        else
                        {
                            dlog_msg("PIN entry FAILED!");
                            uxHandler.pinAbort();
                            return PIN_VERIFY_ERROR;
                        }
                    }
                    dlog_msg("PIN too short or too long and bypass not allowed");
					if(pKeypadSetup.ucMax < num_digits)
					{
						uxHandler.pinDelete(1);
					}
                    break;
                case UX100_KEY_CAN:
                    uxHandler.pinAbort();
                    dlog_msg("PIN entry cancelled");
                    return PIN_VERIFY_CANCEL;

                case UX100_KEY_COR:
                    if (pKeypadSetup.ucOption & 0x08) uxHandler.pinDelete(1);
                    else uxHandler.pinDelete(0);
                    storePinEntryKeyTime();
                    dlog_msg("Corr key pressed");
                    break;

                case UX100_KEY_DOWN:
                case UX100_KEY_UP:
                case UX100_KEY_INFO:
                    dlog_msg("Unsupported key pressed, ignoring...");
                    break;

                #ifdef DIRECTGUI_PROXY
                case vfiguiproxy::result::ABORT:
                    dlog_msg("PIN entry aborted");
                    uxHandler.pinAbort();
                    if (PIN_entry_timeout)
                        return PIN_VERIFY_TIMEOUT;
                    else if (cancel_reason)
                        return cancel_reason;
                    return PIN_VERIFY_CANCEL;
                    break;
                /*
                case vfiguiproxy::result::TIMEOUT:
                    dlog_msg("PIN entry timeout");
                    uxHandler.pinAbort();
                    return PIN_VERIFY_TIMEOUT;
                case vfiguiproxy::result::BACK:
                    dlog_alert("PIN entry bypassed!");
                    cancel_reason = PIN_VERIFY_POS_BYPASS;
                    return PIN_VERIFY_BYPASS;
                */
                #else
                case vfigui::UI_ERR_TIMEOUT:
                    dlog_msg("PIN entry timeout");
                    uxHandler.pinAbort();
                    return PIN_VERIFY_TIMEOUT;

                case vfigui::UI_ERR_CANCELLED:
                    dlog_msg("Cancelled");
                    uxHandler.pinAbort();
                    if (cancel_reason) return cancel_reason;
                    else return PIN_VERIFY_CANCEL;
                #endif

                default:
                    uxHandler.pinAbort();
                    dlog_msg("Undefined key %d", key_pressed);
                    return PIN_VERIFY_ERROR;
            }
            num_digits = uxHandler.pinGetNbCurrDigitEntered();
            dlog_msg("We have %d PIN digits", num_digits);
            if(num_digits != pre_digits || key_pressed > 0)
            {
                EntryCallBack(num_digits, key_pressed, html_file, html_warning);
                value["pinline"]= buildPINbox(num_digits, pKeypadSetup.ucMax, expPINdigits, pKeypadSetup.ucDefChar, pKeypadSetup.ucEchoChar);
                #ifdef DIRECTGUI_PROXY
                vfiguiproxy::GUIClient::instance().displayURL(makeHtmlName(html_file), value);
                #else
                vfigui::uiInvokeURL(value, html_file);
                #endif
                pre_digits = num_digits;
            }
        }
        while(1);
    }
    else
    {
        #ifdef VFI_PLATFORM_VERIXEVO
        #ifdef DIRECTGUI_PROXY
        vfiguiproxy::GUIClient::instance().obtainConsole();
        #else
        vfigui::uiObtainConsole();
        #endif
        #endif
        unsigned char dummy[1];
        retVal=iPS_RequestPINEntry(1, dummy);
        dlog_msg("Starting PIN entry...");
        if(retVal)
        {
             dlog_msg("iPS_RequestPINEntry error ret: %X", retVal);
             switch(retVal)
             {
                case E_KM_NO_ALGO_SELECTED:
                    dlog_msg("E_KM_NO_ALGO_SELECTED");
                    break;
                case E_KM_BAD_SEQUENCE:
                    dlog_msg("E_KM_BAD_SEQUENCE");
                    break;
                case E_KM_SYSTEM_ERROR:
                    dlog_msg("E_KM_SYSTEM_ERROR");
                    break;
                case E_KM_ACCESS_DENIED:
                    dlog_msg("E_KM_ACCESS_DENIED"); // This indicates leaky bucket error, PIN limit exhausted
                    break;
                case E_KM_OUT_OF_RANGE:
                    dlog_msg("E_KM_OUT_OF_RANGE"); // This indicates leaky bucket error, PIN limit exhausted
                    break;
                default:
                    dlog_msg("OTHER ERROR");
                    break;
            }
            #ifdef VFI_PLATFORM_VERIXEVO
            if (hdl >= 0) close(hdl);
            #endif
            return PIN_VERIFY_ABORT;
        }

        long timecurrent = read_ticks();
        long timeout = timecurrent + getPINtimeout();
        long firstchar = timecurrent + getPINfirstchartimeout();
        long nextchar = timecurrent + getPINnextchartimeout();
        bool firstKey = true;
        unsigned char currentKeyCount = 0;
        int pinStatus = 0;
        int nbPinDigits = 0;
        char display_text[M_MAX_PIN_LENGTH_MAX+1];
        break_ret_t breakRes = NoData;
        const int DEFAULT_WAIT_PERIOD = 100; // 100 milliseconds, default
        int waitPeriod;
        dlog_msg("PIN Entry timeout global: %d, first char: %d, interchar: %d", getPINtimeout(), getPINfirstchartimeout(), getPINnextchartimeout());
        dlog_msg("PIN Entry Max size %d", pKeypadSetup.ucMax);
        dlog_msg("PIN entry expected number of digits %d", expPINdigits);
        dlog_msg("PIN entry def char %c echo char %c", pKeypadSetup.ucDefChar, pKeypadSetup.ucEchoChar);

        bool keyBeeper = isKeyboardBeeper(ini);
        value["logo_image"] = ini.gets("", "logo", "");

        setSecurePINDisplayParameters(NULL, keyBeeper ? (void *)callback_PIN_entry_BEEP : (void *)callback_PIN_entry);

        EntryCallBack(0, 0x02, html_file, html_warning);

        value["pinline"]= buildPINbox(0, pKeypadSetup.ucMax, expPINdigits, pKeypadSetup.ucDefChar, pKeypadSetup.ucEchoChar);
        #ifdef DIRECTGUI_PROXY
        #ifdef VFI_PLATFORM_VERIXEVO
        vfiguiproxy::GUIClient::instance().releaseConsole();
        #endif
        vfiguiproxy::GUIClient::instance().displayURL(makeHtmlName(html_file), value);
        #ifdef VFI_PLATFORM_VERIXEVO
        vfiguiproxy::GUIClient::instance().obtainConsole();
        #endif
        #else
        #ifdef VFI_PLATFORM_VERIXEVO
        vfigui::uiReleaseConsole();
        #endif
        vfigui::uiInvokeURL(value, html_file);
        #ifdef VFI_PLATFORM_VERIXEVO
        vfigui::uiObtainConsole();
        #endif
        #endif
        int result = PIN_VERIFY_ERROR;

        do
        {
            waitPeriod = DEFAULT_WAIT_PERIOD;
            if(!isICCpresent())
            {
                dlog_msg("Card removed!!! - canceling");
                iPS_CancelPIN();
                result = PIN_VERIFY_CARD_REMOVED;
                break;
            }
            else
            {
              dlog_msg("Card in reader, continuing PIN entry... (%d valid digits)", currentKeyCount);
            }
            retVal = iPS_GetPINResponse(&pinStatus, &pinOUTData);
            dlog_msg("iPS_GetPINResponse ret: %d, status: %d, entered digits: %d", retVal, pinStatus, pinOUTData.nbPinDigits);
            if(retVal)
            {
                dlog_msg("Incorrect response from iPS_GetPINResponse: %X", retVal);
                iPS_CancelPIN();
                result = PIN_VERIFY_ERROR;
                break;
            }
            
            inprogress = ippPinEntryStatus(&count, &lastNonNumericKey);
            dlog_msg("PIN status: %d, num digits: %d lastKey: %X", inprogress, count, lastNonNumericKey);
            
            if( (breakRes = CheckBreak()) != NoData)
            {
                int cancel_reason = PIN_VERIFY_CANCEL;
                switch (breakRes)
                {
                case Cancelled:
                    dlog_alert("PIN entry cancelled!");
                    cancel_reason = PIN_VERIFY_POS_CANCEL;
                    break;
                case Bypassed:
                    dlog_alert("PIN entry bypassed!");
                    cancel_reason = PIN_VERIFY_POS_BYPASS;
                    break;
                default:
                    dlog_msg("External PIN cancel requested");
                }
                iPS_CancelPIN();
                result = cancel_reason;
                 break;
            }
            if (pinOUTData.nbPinDigits != currentKeyCount)
            {
                firstKey = false;
                currentKeyCount = pinOUTData.nbPinDigits;
                EntryCallBack(pinOUTData.nbPinDigits, pinStatus, html_file, html_warning);
                value["pinline"]= buildPINbox(pinOUTData.nbPinDigits, pKeypadSetup.ucMax, expPINdigits, pKeypadSetup.ucDefChar, pKeypadSetup.ucEchoChar);
                #ifdef DIRECTGUI_PROXY
                #ifdef VFI_PLATFORM_VERIXEVO
                vfiguiproxy::GUIClient::instance().releaseConsole();
                #endif
                vfiguiproxy::GUIClient::instance().displayURL(makeHtmlName(html_file), value);
                #ifdef VFI_PLATFORM_VERIXEVO
                vfiguiproxy::GUIClient::instance().obtainConsole();
                #endif
                #else
                #ifdef VFI_PLATFORM_VERIXEVO
                vfigui::uiReleaseConsole();
                #endif
                vfigui::uiInvokeURL(value, html_file);
                #ifdef VFI_PLATFORM_VERIXEVO
                vfigui::uiObtainConsole();
                #endif
                #endif
                nextchar = read_ticks() + getPINnextchartimeout();
            }
            if(getPINfirstchartimeout() && firstKey)
            {
                if (timecurrent > firstchar)
                {
                    iPS_CancelPIN();
                    dlog_msg("first char timeout");
                    result = PIN_VERIFY_TIMEOUT;
                    break;
                }
                else if (DEFAULT_WAIT_PERIOD + timecurrent > firstchar) waitPeriod = firstchar - timecurrent;
            }
            if(getPINnextchartimeout() && !firstKey)
            {
                if (timecurrent > nextchar)
                {
                    iPS_CancelPIN();
                    dlog_msg("next char timeout");
                    result = PIN_VERIFY_TIMEOUT;
                    break;
                }
                else if (DEFAULT_WAIT_PERIOD + timecurrent > nextchar) waitPeriod = nextchar - timecurrent;
            }
            if(getPINtimeout())
            {
                if (timecurrent > timeout)
                {
                    iPS_CancelPIN();
                    dlog_msg("local pin entry timeout");
                    result = PIN_VERIFY_TIMEOUT;
                    break;
                }
                else if (DEFAULT_WAIT_PERIOD + timecurrent > timeout) waitPeriod = timeout - timecurrent;
            }
            switch(pinStatus)
            {
                case 0x00:      //PIN done
                    dlog_msg("%X - PIN ready - %d digits", pinStatus, pinOUTData.nbPinDigits);
                    result = PIN_VERIFY_OK;
                    break;
                case 0x01:      //unit idle
                    dlog_msg("%X - global PIN entry timeout", pinStatus);
                    result = PIN_VERIFY_TIMEOUT;
                    break;
                case 0x02:      //in progress
                    dlog_msg("%X - PIN entry in progress: %d digits already entered", pinStatus, pinOUTData.nbPinDigits);
                    if(pinOUTData.nbPinDigits != nbPinDigits)
                    {
                        #if defined(VFI_PLATFORM_VOS)
                        memset(display_text, 0, M_MAX_PIN_LENGTH_MAX+1);
                        memset(display_text, '*', pinOUTData.nbPinDigits);
                        dlog_msg("updating PIN entry screen (x=%d y=%d) %s", x, y, display_text);
                        nbPinDigits = pinOUTData.nbPinDigits;
                        #endif
                    }
                    break;
                case 0x05:      //aborted by user
                    dlog_msg("%X - PIN entry cancelled", pinStatus);
                    result = PIN_VERIFY_CANCEL;
                    break;
                case 0x06:      //PIN bypassed
                    dlog_msg("%X - PIN entry bypass", pinStatus);
                    result = PIN_VERIFY_BYPASS;
                    break;
                case 0x0A:      //PIN bypassed
                    dlog_msg("%X - PIN entry bypass", pinStatus);
                    result = PIN_VERIFY_BYPASS;
                    break;
                case 0x0C:      //pin timeout
                    dlog_msg("%X - global PIN entry timeout", pinStatus);
                    result = PIN_VERIFY_TIMEOUT;
                    break;
                default:
                    dlog_msg("%X - Unknown PIN entry error", pinStatus);
                    result = PIN_VERIFY_ERROR;
                    break;
            }

            timecurrent = read_ticks();
            // dlog_msg("Waiting for %d ms", waitPeriod);
            SVC_WAIT(waitPeriod);
        } while(pinStatus == 0x02);
        #ifdef VFI_PLATFORM_VERIXEVO
        #ifdef DIRECTGUI_PROXY
        vfiguiproxy::GUIClient::instance().releaseConsole();
        #else
        vfigui::uiReleaseConsole();
        #endif
        if (hdl >= 0) close(hdl);
        #endif
        return result;
    }

    return PIN_VERIFY_ERROR;
}


int cPINdisplayFlexi::Clear()
{
    int ret = 0;
    #ifdef VFI_PLATFORM_VERIXEVO
    #ifdef DIRECTGUI_PROXY
    ret = vfiguiproxy::GUIClient::instance().releaseConsole();
    #else
    ret = vfigui::uiReleaseConsole();
    #endif
    dlog_msg("directGUI uiReleaseConsole ret: %d", ret);

    // added for e315, but should be required for every device with cap. kbd
    deactivateCapacitiveKbd();

    #endif
    return ret;
}

bool cPINdisplayFlexi::ComputeEntryXY()
{
    bool result = true;

    long x_pos = 0, y_pos = 0, x_font = 0, y_font = 0;

    x = 8;
    y = 4;

    return result;
}

#endif

#ifdef VFI_PLATFORM_VERIXEVO

cPINdisplayFlexi::cPINdisplayFlexi(long timeout): cPINdisplayDirectGUIBase(timeout)
{
    cancel_reason = 0;
    dlog_msg("registering PIN entry callback");
}

cPINdisplayFlexi::~cPINdisplayFlexi()
{
    
	dlog_msg("cPINdisplay object succesfully destroyed!");
}

int cPINdisplayFlexi::Display(c_encPIN encPIN)
{
    dlog_msg("cPINdisplayFlexi::Display, NOT IMPLEMENTED");

    return 1;
}

int cPINdisplayFlexi::Clear()
{
    int ret = 0;
    #ifdef VFI_PLATFORM_VERIXEVO
    #ifdef DIRECTGUI_PROXY
    ret = vfiguiproxy::GUIClient::instance().releaseConsole();
    #else
    ret = vfigui::uiReleaseConsole();
    #endif
    dlog_msg("directGUI uiReleaseConsole ret: %d", ret);

    // added for e315, but should be required for every device with cap. kbd
    deactivateCapacitiveKbd();

    #endif
    return ret;
}

int cPINdisplayFlexi::GetPIN(c_encPIN encPIN)
{
	dlog_msg("cPINdisplayFlexi::Display, NOT IMPLEMENTED");

	return 1;
}

bool cPINdisplayFlexi::ComputeEntryXY()
{
    bool result = true;

    long x_pos = 0, y_pos = 0, x_font = 0, y_font = 0;

    x = 8;
    y = 4;

    return result;
}


#endif


#endif


std::string cPINdisplay::BuildExtraMsg(void)
{
    if (extra_msg.size() == 0)
    {
        // extra_msg.assign("=====");
        char szAppLabel[MAX_LABEL_LEN+1] = {0}; // 16 bytes plus zero termination
        unsigned short usAppLabelLen = sizeof(szAppLabel) - 1; // = EMV_APP_LABEL_MAX_LEN

        dlog_msg("Extra PIN message not set, trying to display Application Preferred Name");
        if ( (usAppLabelLen=GetTLVData( TAG_9F12_APP_PREFERRED_NAME, (unsigned char *)szAppLabel, (int *)&usAppLabelLen )) > 0 )
        {
            dlog_msg("Preferred App. label %s, len: %d", szAppLabel, usAppLabelLen);
            // Check whether Issuer Code Table Index is present and supported
            unsigned char codeTable = 0;
            int tempLen = 0;
            if ( (tempLen = GetTLVData( TAG_9F11_ISS_CODE_TABLE_ID, (unsigned char *)&codeTable, &tempLen )) > 0 )
            {
                dlog_msg("Code table %d (len %d)", codeTable, tempLen);
                if (codeTable >= 1 && codeTable <= 10)
                {
                    unsigned char tempBuf[10];
                    memset(tempBuf, 0, sizeof(tempBuf));
                    tempLen = sizeof(tempBuf)-1;
                    if ( (tempLen=GetTLVData( TAG_9F40_ADD_TRM_CAP, (unsigned char *)tempBuf, &tempLen )) > 0 )
                    {
                        bool isSupported = false;
                        switch (codeTable)
                        {
                                case 1 : isSupported = tempBuf[4] & 0x01; break;
                                case 2 : isSupported = tempBuf[4] & 0x02; break;
                                case 3 : isSupported = tempBuf[4] & 0x04; break;
                                case 4 : isSupported = tempBuf[4] & 0x08; break;
                                case 5 : isSupported = tempBuf[4] & 0x10; break;
                                case 6 : isSupported = tempBuf[4] & 0x20; break;
                                case 7 : isSupported = tempBuf[4] & 0x40; break;
                                case 8 : isSupported = tempBuf[4] & 0x80; break;
                                case 9 : isSupported = tempBuf[3] & 0x01; break;
                                case 10: isSupported = tempBuf[3] & 0x02; break;
                        }
                        dlog_msg("Add term caps (2 last bytes) %02X %02X. IsSupported %d", tempBuf[2], tempBuf[3], isSupported);
                        if (isSupported)
                        {

                            extra_msg.assign(szAppLabel, usAppLabelLen );
                        }
                    }
                }
            }
        }
        if (extra_msg.size()==0)
        {
            memset(szAppLabel, 0, sizeof(szAppLabel));
            usAppLabelLen = sizeof(szAppLabel) - 1;
            if ( (usAppLabelLen=GetTLVData( TAG_50_APP_LABEL, (unsigned char *)szAppLabel, (int *)&usAppLabelLen )) > 0 )
            {
                dlog_msg("App. label %s, len: %d", szAppLabel, usAppLabelLen);
                extra_msg.assign(szAppLabel, (int)usAppLabelLen );
            }
        }
        if (extra_msg.size()==0)
        {
            dlog_error("App. label cannot be retrieved");
            extra_msg.clear();
        }
    }
    return extra_msg;

}


std::string cPINdisplay::BuildAmtMsg(void)
{
    if (amt_msg.size() == 0)
    {
        uint16_t trn_curr = 0;
        int dLen =0;
        char amtDsp[ADD_SCREEN_TEXT+2]; // 12 digits tops, so we need 18 bytes here
        unsigned char amt_num[6];
        unsigned char currExp = 2;
        const currency_code_t *pCurrCode = &M_CURRENCY_CODES[0];
        bool codesUsed = false;

        if(GetTLVData(TAG_9F02_NUM_AMOUNT_AUTH, amt_num, &dLen) > 0)
        {
            //SVC_HEX_2_DSP((const char *)amt_num, amtDsp, dLen);
            if(dLen > ADD_SCREEN_TEXT/2)
                dLen = ADD_SCREEN_TEXT/2;
            amtDsp[0] = '0';
            com_verifone_pml::svcHex2Dsp((const char *)amt_num, dLen, amtDsp+1, dLen*2+1);
            amtDsp[dLen*2+1] = 0;
            dlog_msg("amount read: %s", amtDsp);
            if(atol(amtDsp) == 0)
            {
              dlog_msg("Amount 0 => no amount line");
              amt_msg.clear();
              return amt_msg;
            }
            if(GetTLVData(TAG_5F2A_TRANS_CURRENCY, amt_num, &dLen) > 0)
            {
                char strPIN[ADD_SCREEN_TEXT+1];
                //
                //SVC_HEX_2_DSP((const char *)amt_num, strPIN, dLen);
                com_verifone_pml::svcHex2Dsp((const char *)amt_num, dLen, strPIN, dLen*2+1);
                trn_curr = atoi(strPIN);
                int curr_ind=0;
                if (display_curr_symbol)
                {
                    while((M_CURRENCY_SYMBOLS[curr_ind].code != trn_curr) && (M_CURRENCY_SYMBOLS[curr_ind].code != 0))
                    {
                        curr_ind++;
                    }
                    if (M_CURRENCY_SYMBOLS[curr_ind].code == 0) curr_ind = 0;
                    else pCurrCode = &M_CURRENCY_SYMBOLS[curr_ind];
                }
                if (curr_ind == 0)
                {
                    while((M_CURRENCY_CODES[curr_ind].code != trn_curr) && (M_CURRENCY_CODES[curr_ind].code != 0))
                    {
                        curr_ind++;
                    }
                    pCurrCode = &M_CURRENCY_CODES[curr_ind];
                    codesUsed = true;
                }
                dlog_msg("Transaction currency code: %d - '%s'", trn_curr, pCurrCode->name);
                dlog_msg("Curr code, first three bytes: 0x%02X 0x%02X 0x%02X", pCurrCode->name[0], pCurrCode->name[1], pCurrCode->name[2]);

            }

            dLen = 1;
            if(GetTLVData(TAG_5F36_TRANS_CURRENCY_EXP, &currExp, &dLen) <= 0)
            {
                dlog_alert("No currency exponent, assuming default");
                currExp = 2;
            }

            int ind_i = 0, ind_j = 0, am_len = 0;
            am_len = strlen(amtDsp);
            while((amtDsp[ind_i] == '0') && (ind_i < am_len-(currExp+1)))
                ind_i++;
            while(amtDsp[ind_i] != 0)
            {
                if(am_len - currExp  == ind_i)
                {
                    //amtDsp[ind_j++] = g_CardAppConfig.GetPINRadixSeparator();
                    amtDsp[ind_j++] = radixSep;
                }
                if((amtDsp[ind_i] >= '0') && (amtDsp[ind_i] <= '9'))
                {
                    amtDsp[ind_j++] = amtDsp[ind_i];
                }
                ind_i++;
            }
            if (PINDisplayCurrencySymbolLeft)
            {
                // ind_j consists of amtDsp length
                int currCodeLen = strlen(pCurrCode->name);
                //assert(ind_j + currCodeLen < sizeof(amtDsp));
                memmove( amtDsp+currCodeLen+1, amtDsp, ind_j );
                memcpy(amtDsp, pCurrCode->name, currCodeLen);
                amtDsp[currCodeLen] = ' ';
                amtDsp[ind_j+currCodeLen+1] = 0;
            }
            else
            {
                int currCodeLen = strlen(pCurrCode->name);
                amtDsp[ind_j++] = ' ';
                memcpy(amtDsp+ind_j, pCurrCode->name, currCodeLen);
                amtDsp[ind_j+currCodeLen] = 0;
            }
            amt_msg.assign(amtDsp);
        }
    }
	else
	{
		dlog_msg("Amount already set: %s", amt_msg.c_str());
	}
    return amt_msg;

}


int cPINdisplay::Display(c_encPIN encPIN)
{
    #if 0
    const currency_code_t *pCurrCode = &M_CURRENCY_CODES[0];
    char strPIN[ADD_SCREEN_TEXT+1];
    strPIN[0]=0;
    uint16_t trn_curr = 0;
    std::string amtDsp;
    std::string extDsp;


    amtDsp = BuildAmtMsg(encPIN);
    dlog_msg("Amount text: %s", amtDsp.c_str());
    extDsp = BuildExtraMsg(encPIN);
    dlog_msg("Extra text: %s", extDsp.c_str());
    strPIN[0]=0;

    char tempText[DISPLAY_TEXT+1];
    tempText[DISPLAY_TEXT] = 0;
    strcpy(tempText, "\\c");
    strncpy(tempText+2, extDsp.c_str(), DISPLAY_TEXT-4);
    //strcat(tempText, "\n");
    get_console_handle();
    DisplayText(tempText, strlen(tempText), 1, 1, true);
    strncpy(tempText+2, amtDsp.c_str(), DISPLAY_TEXT-4);
    //strcat(tempText, "\n");
    DisplayText(tempText, strlen(tempText), 1, 2, false);
    {
         char * pText = tempText + 2;
         userPromptLite prompt;
         if (prompt.getPrompt(CARD_APP_GUI_S_PIN, g_CardAppConfig.pinScreenTitle, pText, DISPLAY_TEXT-2 ) <= 0) // -2 to ensure beginning '\c' fits
         {
             dlog_error("Cannot get PIN prompt (number %d), assuming default (Enter PIN)!", g_CardAppConfig.pinScreenTitle);
             if(getPromptLocal(g_CardAppConfig.pinScreenTitle, pText) == 0)
                strcpy(pText, "ENTER PIN");
         }
    }
    DisplayText(tempText, strlen(tempText), 1, 3, false);
    #endif
    return 1;
}


void cPINdisplay::setPINparams(PINPARAMETER pinparams, unsigned char bKey /* = 0xFF */, int PINdigits /* = 4*/)
{
    pKeypadSetup = pinparams;
    bypassKey = bKey;
    expPINdigits = PINdigits;
}

bool cPINdisplay::should_cancel() // for GuiApp callback
{
    if(!isICCpresent())
    {
        dlog_msg("Card removed!!! - canceling");
        cancel_reason = PIN_VERIFY_CARD_REMOVED;
        // iPS_CancelPIN();
        return 1;
    }
    break_ret_t breakRes = CheckBreak();
    if (breakRes != NoData)
    {
        switch (breakRes)
        {
        case GuiAppData:
        case Timeout:
            return 0;
        case Cancelled:
            dlog_alert("PIN entry cancelled!");
            cancel_reason = PIN_VERIFY_POS_CANCEL;
            break;
        case Bypassed:
            dlog_alert("PIN entry bypassed!");
            cancel_reason = PIN_VERIFY_POS_BYPASS;
            break;
        }
        // iPS_CancelPIN();
        return 1;
    }
    return 0;
}
