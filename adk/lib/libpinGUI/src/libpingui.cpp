#include "libpingui.h"
#include <libpml/pml.h>

namespace com_verifone_pin_gui
{
    void init()
    {
        com_verifone_pml::appver::register_library( PINGUI_NAME , PINGUI_VERSION );
    }
}

