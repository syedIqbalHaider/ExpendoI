/******************************************************************************

  FILE: pin.h

  DESC: functions contained within the PIN Module
  
******************************************************************************/

#ifndef PIN_H
#define PIN_H

// #include <stdint.h>
#include <string>
#include <map>


#define PIN_VERIFY_OK              0      // Correct PIN entered
#define PIN_VERIFY_TIMEOUT         1      // PIN entry timed out
#define PIN_VERIFY_CANCEL          2      // PIN entry cancelled
#define PIN_VERIFY_ERROR           3      // Incorrect PIN entered
#define PIN_VERIFY_BYPASS          4      // PIN entry bypassed
#define PIN_VERIFY_BLOCKED         5      // PIN try limit exhausted, card may be blocked
// #define PIN_VERIFY_LAST_TRY       6      // Last PIN try
#define PIN_VERIFY_CARD_REMOVED    7      // PIN entry aborted, card removed
#define PIN_VERIFY_POS_CANCEL      8      // PIN entry cancelled by POS
#define PIN_VERIFY_POS_BYPASS      9      // PIN entry bypassed by POS
#define PIN_VERIFY_GET_CHALLENGE  10      // PIN encrypted - GetChallenge failed
#define PIN_VERIFY_ABORT          20      // Unknown error occurred, abort PIN entry

#define MAX_MODULUS_LENGTH        248


const unsigned char M_PIN_ALGO = 0x0B;
const unsigned char M_PIN_EMV = 0x0A;



void GetPINModuleVersion( void );
void get_pin(unsigned char *data, unsigned short data_length);
void load_pin_key(unsigned char *data, unsigned short data_length);

class c_encPIN
{
	public:
		c_encPIN(void);
		int VerifyPIN(void);
		int addModulus(unsigned char *mod, int m_len);
		int getModulus(unsigned char *mod, int *m_len);
		int addExponent(unsigned char *exp, int e_len);
		int getExponent(unsigned char *exp, int *e_len);
		int addICCrandom(unsigned char *rand, int r_len);
		int getICCrandom(unsigned char *rand, int *r_len);
		void setGUItype(int GUItype);
		int addExternalPIN(const char * ext_pin, int p_len);
		int addExternalPINMode(int p_mode);
		
		void displayNotification(unsigned char * ICC_response, int offset = 0);
		
		inline void SetHTML_PIN_OK(const std::string & html, const std::map<std::string, std::string> &vals)
			{ PIN_html_ok = html; set_map_values(values_pin_ok, vals); }
		inline void SetHTML_PIN_Retry(const std::string & html, const std::map<std::string, std::string> &vals)
			{ PIN_html_retry = html; set_map_values(values_pin_retry, vals); }
		inline void SetHTML_PIN_Last_Try(const std::string & html, const std::map<std::string, std::string> &vals)
			{ PIN_html_last_try = html; set_map_values(values_pin_last_try, vals); }
		
	private:
		unsigned char modulus[MAX_MODULUS_LENGTH];
		int modulus_length;
		unsigned char exponent[4];
		int exponent_length;
		unsigned char ICC_random[8];
		char external_pin[32]; // cryptogram
		int external_pin_len;
		int external_pin_mode;
		
		std::string PIN_html_ok;
		std::string PIN_html_retry;
		std::string PIN_html_last_try;
		std::map<std::string, std::string> values_pin_ok;
		std::map<std::string, std::string> values_pin_retry;
		std::map<std::string, std::string> values_pin_last_try;

		//int (*GetTLVData)(int tagNum, unsigned char *value, int *Len);
		//int (*SetTLVData)(int tagNum, unsigned char *value, int Len);
		void set_map_values(std::map<std::string, std::string> & values, const std::map<std::string, std::string> & vals);
};


#endif



