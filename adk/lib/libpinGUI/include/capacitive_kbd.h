#ifndef CAPACITIVE_KBD_H
#define CAPACITIVE_KBD_H

/**
 * Following methods activate/deactivate capacitive keyboard (if present).
 *
 * Code taken from directgui doc.
 */
void activateCapacitiveKbd();
void deactivateCapacitiveKbd();

#endif //CAPACITIVE_KBD_H
