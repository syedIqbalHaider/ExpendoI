#ifndef DEFINES_H
#define DEFINES_H

//#define MAX_PIN_LENGTH         12
//#define MIN_PIN_LENGTH         4


//#define MAX_REC_PACKET_LENGTH   511
//#define MAX_SEND_PACKET_LENGTH  500
//#define MAX_PAYLOAD_LENGTH      502
#define MAX_APDU_BUFFER_SIZE    257  // CVN 255 bytes data + 2 bytes SW1SW2


typedef int (*GetTLVData_t)(int, unsigned char*, int*);
typedef int (*SetTLVData_t)(int, unsigned char*, int);

typedef enum
{
    NoData = 0,
    GuiAppData = 1,
    Cancelled = 2,
    Bypassed = 3,
    Timeout = 4
} break_ret_t;


typedef break_ret_t (*checkBreak_t)(void);
typedef bool (*isICCpresent_t)(void);

typedef int (*EntryCallBack_t)(int, int, char*, char*);	//this function is called after each key press - parameters are: number of valid pin digits, pin entry status it may return filename of new html requested to be displayed


#endif
