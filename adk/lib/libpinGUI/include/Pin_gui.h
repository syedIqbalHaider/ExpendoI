#ifndef PINGUI_H
#define PINGUI_H

#include "PIN.h"
#include "Defines.h"

#include <svc.h>
#include <string>

#include <map>


#ifdef VFI_PLATFORM_VERIXEVO
#include <svc_sec.h>
#else
#include <svcsec.h>
#endif

//#define TAG_9F12_APP_PREFERRED_NAME 0x9F12
//#define TAG_9F11_ISS_CODE_TABLE_ID 0x9F11
//#define TAG_9F40_ADD_TRM_CAP 0x9F40
//#define TAG_50_APP_LABEL 0x5000
//#define TAG_9F02_NUM_AMOUNT_AUTH 0x9F02
//#define TAG_5F2A_TRANS_CURRENCY 0x5F2A
//#define TAG_5F36_TRANS_CURRENCY_EXP 0x5F36

typedef std::map<std::string, std::string> HTML_vars;


int get_console_handle();

class cPINdisplay
{
	public:

		cPINdisplay(void);
		virtual ~cPINdisplay()
			{ }
		inline void SetEntryXY(long x_val, long y_val) {x=x_val; y=y_val;}
		inline long GetEntryX() { return x; }
		inline long GetEntryY() { return y; }
		inline void SetExtraMsg(const std::string & msg) {extra_msg = msg;}
		inline void SetAmtMsg(const std::string & amt) {amt_msg = amt;}
		inline void SetWarningMsg(const std::string & msg) {warning_msg = msg;}
		inline void SetDisplayAmount(int disp_line) {amount_line = disp_line;}
		inline void SetLanguage(const std::string & lng){ lng_prefix = lng;}
		virtual int Display(c_encPIN encPIN) = 0;
		virtual bool ComputeEntryXY() = 0;

		std::string BuildAmtMsg();
		std::string BuildExtraMsg();
		void setRadixSeparator(char radix) {radixSep = radix;}
		void setPINparams(PINPARAMETER pinparams, unsigned char bypassKey = 0xFF, int expPINdigits = 4);
		inline PINPARAMETER getPINparams(void) {return pKeypadSetup; }
		/// @param timeout timeout in ms
		inline void setPINtimeout(long timeoutMs) {PINtimeout = timeoutMs;}
		/// All timeouts are in ms!
		inline void setPINtimeouts(long firstcharMs, long nextcharMs, long timeoutMs) {PINfirstchartimeout=firstcharMs; PINnextchartimeout = nextcharMs; PINtimeout=timeoutMs; }
		inline long getPINtimeout(void) {return PINtimeout; }
		inline long getPINfirstchartimeout(void) { return PINfirstchartimeout; }
		inline long getPINnextchartimeout(void) { return PINnextchartimeout; }
		inline void setCurrSymbolLeft(bool curr_left) {PINDisplayCurrencySymbolLeft = curr_left;}
		inline bool getCurrSymbolLeft(void) {return PINDisplayCurrencySymbolLeft; }
		inline void setDispCurrSymbol(bool curr_symbol) {display_curr_symbol = curr_symbol;}
		inline bool getDispCurrSymbol(void) {return display_curr_symbol; }
		inline void setEnterPINmsg(char *msg){msg_enter_pin.assign(msg);}
		inline std::string getEnterPINmsg(void){return msg_enter_pin;}
		inline void setIncorrectPINmsg(char *msg){msg_incorrect_pin.assign(msg);}
		inline std::string getIncorrectPINmsg(void){return msg_incorrect_pin;}
		inline void setLastPINmsg(char *msg){msg_last_pin_try.assign(msg);}
		inline std::string getLastPINmsg(void) {return msg_last_pin_try;}
		inline void setTLVget(GetTLVData_t lGetTLVData) {GetTLVData = lGetTLVData;}
		inline void setTLVset(SetTLVData_t lSetTLVData) {SetTLVData = lSetTLVData;}
		inline void setCheckBreak(checkBreak_t lCheckBreak) {CheckBreak = lCheckBreak;}
		inline void setEntryCallBack(EntryCallBack_t lEntryCallBack) {EntryCallBack = lEntryCallBack;}
		inline void setICCpresent(isICCpresent_t lIsICCpresent) {isICCpresent = lIsICCpresent;}

		virtual int GetPIN(c_encPIN encPIN) = 0;
		virtual int Clear() = 0;

		inline void SetPINprogress(int pin_p) {pin_progress = pin_p;}
		inline void SetPINmode(int pin_m) {pin_mode = pin_m;}

		inline void SetPINhtml(std::string PIN_html) { PIN_prompt_html = PIN_html;}
		inline void SetOpenCloseCrypto(bool flag) {open_close_crypto = flag; }
		inline void SetPerformVSSscript(int data) {VSS_script = data;}
		inline void SetPerformVSSmacro(int data) {VSS_macro= data;}
		inline void AddHTMLvars(HTML_vars vars) { HTML_labels = vars;}
		bool should_cancel(); // for GuiApp callback

	protected:
		std::string extra_msg;
		std::string amt_msg;
		std::string warning_msg;
		int amount_line;
		long x;
		long y;
		bool display_curr_symbol;
		bool PINDisplayCurrencySymbolLeft;
		char radixSep;
		PINPARAMETER pKeypadSetup;
		unsigned char bypassKey;
		int expPINdigits;
		long PINtimeout;
		long PINfirstchartimeout;
		long PINnextchartimeout;
		std::string msg_enter_pin;
		std::string msg_incorrect_pin;
		std::string msg_last_pin_try;
		std::string lng_prefix;

		int pin_progress;
		int pin_mode;
		bool open_close_crypto;

		GetTLVData_t GetTLVData;
		SetTLVData_t SetTLVData;
		checkBreak_t CheckBreak;
		isICCpresent_t isICCpresent;
		EntryCallBack_t EntryCallBack;

		std::string PIN_prompt_html;

		int cancel_reason;
		int VSS_script;
		int VSS_macro;
		HTML_vars HTML_labels;
		
};

class cPINdisplayConsole: public cPINdisplay
{
	public:
		cPINdisplayConsole(): cPINdisplay()
			{}
		virtual ~cPINdisplayConsole()
			{ }

		int GetPIN(c_encPIN encPIN);
		virtual int Display(c_encPIN encPIN);
		virtual bool ComputeEntryXY();

		virtual int Clear();
};

class cPINdisplayGuiApp: public cPINdisplay
{
	public:
		cPINdisplayGuiApp();
		virtual ~cPINdisplayGuiApp();
		int GetPIN(c_encPIN encPIN);
		virtual int Display(c_encPIN encPIN);
		virtual int Clear();
		virtual bool ComputeEntryXY();

	private:
		bool guiConnected;
		bool consoleClaimed;
		bool pinExecuted;
};

class cPINdisplayDirectGUIBase: public cPINdisplay
{
	public:
		cPINdisplayDirectGUIBase(long timeout = 0): pinEntryStartTime(0), pinEntryKeyTime(0), pinBeeperTimeout(timeout), PIN_entry_timeout(false), PIN_first_key(true), cPINdisplay(), PIN_num_digits(0)
			{}
		// checks if current time minus time stored in pinEntryStartTime
		// is greater than total pin timeout (timeout for the whole process, not
		// interchar timeouts! - those are handled by DG)
		bool pinEntryTimeExpired();
		bool pinEntryKeyboardTimeExpired();
		void setNextKey() { PIN_first_key = false; }
		void storePinEntryKeyTime();
		inline void setPIN_num_digits(int PIN_num_digits_param) {PIN_num_digits = PIN_num_digits_param;}
		inline int getPIN_num_digits(void) {return PIN_num_digits;}

	protected:
		// measuring total time of pin entry -
		void storePinEntryStartTime();

		unsigned long pinEntryStartTime;    // seconds since 1900, saved using storePinEntryStartTime method
		unsigned long pinEntryKeyTime;      // as above
		long pinBeeperTimeout;  // France requirement to beep every pinBeeperTimeout during pin entry...
		bool PIN_entry_timeout;
		bool PIN_first_key;
		int PIN_num_digits;
};

class cPINdisplayDirectGUI: public cPINdisplayDirectGUIBase
{
	public:
		cPINdisplayDirectGUI(long pinBeeperTimeout = 0);
		virtual ~cPINdisplayDirectGUI();
		int GetPIN(c_encPIN encPIN);
		virtual int Display(c_encPIN encPIN);

		virtual int Clear();
		virtual bool ComputeEntryXY();
};

class cPINdisplayFlexi: public cPINdisplayDirectGUIBase
{
	public:
		cPINdisplayFlexi(long pinBeeperTimeout = 0);
		virtual ~cPINdisplayFlexi();
		int GetPIN(c_encPIN encPIN);
		virtual int Display(c_encPIN encPIN);
		virtual int Clear();
		virtual bool ComputeEntryXY();

	private:
		bool guiConnected;
		bool consoleClaimed;
		bool pinExecuted;
};



#endif


