/******************************************************************************

  FILE: helpers.h

  DESC: helper functions
  
******************************************************************************/

#ifndef HELPER_FUNCTIONS_H
#define HELPER_FUNCTIONS_H

// #include <stdint.h>
#include <string>
#include <vector>

namespace helpers {

size_t str_explode(const std::string & str, const std::string & delim, std::vector<std::string> & exploded);
std::string &ltrim(std::string &s);
std::string &rtrim(std::string &s);
std::string &str_trim(std::string &s);

}

#endif



