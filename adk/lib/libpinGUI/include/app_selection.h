#ifndef APPSELECTION_H
#define APPSELECTION_H

//#include <svc.h>
//#include <libpml/pml.h>

#ifdef VFI_GUI_DIRECTGUI
#include <string>
#endif

#include "Defines.h"

#define APP_SEL_OK 0
#define APP_SEL_CANCELLED -1
#define APP_SEL_CARD_REMOVED -2
#define APP_SEL_POS_CANCELLED -3
#define APP_SEL_INVALID_PARAMETERS -4
#define APP_SEL_ERROR -254


class cAppSelDisplay
{
	public:
		cAppSelDisplay();
		virtual ~cAppSelDisplay()
			{}
		
		virtual int displayApplicationSelection(const char * app_list[], size_t app_list_count, int milliseconds) = 0;
		inline void setCheckBreak(checkBreak_t lCheckBreak) {CheckBreak = lCheckBreak;}
		inline void setIsICCpresent(isICCpresent_t lICCPresent) {isICCpresent = lICCPresent;}
	// protected:
		checkBreak_t CheckBreak;
		isICCpresent_t isICCpresent;
};

class cAppSelDisplayConsole: public cAppSelDisplay
{
	public:
		cAppSelDisplayConsole(): cAppSelDisplay()
			{}
		virtual ~cAppSelDisplayConsole()
			{}
		virtual int displayApplicationSelection(const char * app_list[], size_t app_list_count, int milliseconds);
};

#ifdef VFI_GUI_GUIAPP
class cAppSelDisplayGuiApp: public cAppSelDisplay
{
	public:
		cAppSelDisplayGuiApp();
		virtual ~cAppSelDisplayGuiApp();
		virtual int displayApplicationSelection(const char * app_list[], size_t app_list_count, int milliseconds);
	private:
		bool connect();
		const bool connected_to_gui;
		int result;
		
		//int EntryCancelled(void * unused);
		//int procEvents(com_verifone_pml::eventset_t & pml_events);
};
#endif

#ifdef VFI_GUI_DIRECTGUI
class cAppSelDisplayDirectGUI: public cAppSelDisplay
{
	public:
		/**
		 * Directgui uses resource path and app selection screen to display html prompt.
		 * Path to the prompt is merged  like this: resourcePath + appSelectionScreen.
		 *
		 * @see displayApplicationSelection code
		 */
		cAppSelDisplayDirectGUI(const std::string & aResourcePath, const std::string & anAppSelectionScreen);
		virtual ~cAppSelDisplayDirectGUI()
			{}
		virtual int displayApplicationSelection(const char * app_list[], size_t app_list_count, int milliseconds);
	private:
		std::string resourcePath;
		std::string appSelectionScreen;
};
#endif



#endif


