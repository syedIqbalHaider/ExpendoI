/*
 * =====================================================================================
 *
 *       Filename:  fmtlog.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  02/19/2013 11:25:12 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lucjan Bryndza <Lucjan_B1@verifone.com>
 *   Organization:  VERIFONE
 *
 * =====================================================================================
 */

#include "fmtlog/fmtlog.hpp"
#include <cstdio>
#include <errno.h>
#include <syslog.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <unistd.h>

static char LOGDEV = 'O';

/* ------------------------------------------------------------------------------------ */
namespace com_verifone_log
{

/* ------------------------------------------------------------------------------------ */
namespace _internal {
namespace _handlers {

    // Internal output handler function
    void output_string( loglevel level, const char* file, unsigned line, const std::string& message )
    {
    	#define MAX_BUFFER_LENGTH 1024
		static int loginited = false;
		int fd1 = -1;
		
		if(!loginited)
		{
			if(LOGDEV == 'S')
			{
				openlog(program_invocation_short_name, LOG_PID|LOG_CONS, LOG_USER);
			} 
			else if (LOGDEV == 'C')
			{
			
				fd1=open("/dev/ttyS0", O_RDWR | O_NOCTTY | O_NDELAY);

				if (fd1 == -1 )
				{
					perror("open_port: Unable to open /dev/ttyS0  ");
				}
				else
				{
					fcntl(fd1, F_SETFL,0);
					printf("Port 1 has been sucessfully opened and %d is the file description\n",fd1);
				}
			}	
			loginited = true;
		}
		
		
		switch(LOGDEV) {
			case 'O':
			        std::fprintf(stderr, "%s:%s|L:%05u|%s\n", program_invocation_short_name, file, line, message.c_str() );
			        break;
			case 'S':
			    	syslog(LOG_DEBUG, "%s:%s|L:%05u|%s\n", program_invocation_short_name, file, line, message.c_str() );
			case 'C':
					{
						char disp_buf[MAX_BUFFER_LENGTH];
				        int ll = snprintf(disp_buf, sizeof(disp_buf), "%s:%s|L:%05u|%s\n", program_invocation_short_name, file, line, message.c_str() );
				        write(fd1, disp_buf, ll);
				    }
			        break;
	    }
	}
}
}

/* ------------------------------------------------------------------------------------ */
}   //namespace com_verifone_log

/* ------------------------------------------------------------------------------------ */


