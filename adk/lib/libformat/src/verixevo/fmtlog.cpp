/*
 * =====================================================================================
 *
 *       Filename:  fmtlog.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  02/19/2013 11:25:12 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lucjan Bryndza <Lucjan_B1@verifone.com>
 *   Organization:  VERIFONE
 *
 * =====================================================================================
 */

#include "fmtlog/fmtlog.hpp"
#include <cstdio>
#include <SVC.h>
#include <string>

/* ------------------------------------------------------------------------------------ */
namespace com_verifone_log
{
/* ------------------------------------------------------------------------------------ */
namespace
{
    std::string app_name;
}

/* ------------------------------------------------------------------------------------ */
namespace _internal {
namespace _handlers {

    // Internal output handler function
    void output_string( loglevel level, const char* file, unsigned line, const std::string& message )
    {
        if( app_name.empty() )
        {
            const int id = get_task_id();
            task_info t_info;
            get_task_info(id, &t_info);
            app_name = t_info.path;
        }
        dbprintf("%s:%s|L:%05u|%s\n",app_name.c_str(), file, line, message.c_str() );
    }
}}

/* ------------------------------------------------------------------------------------ */
}   //namespace com_verifone_log

/* ------------------------------------------------------------------------------------ */


