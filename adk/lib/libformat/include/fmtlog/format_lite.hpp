#ifndef FMTLOG_FORMAT_LITE_VERSION_H_
#define FMTLOG_FORMAT_LITE_VERSION_H_
/* ------------------------------------------------------------------------------------ */
#include <string>
#include <cstring>
#include <cstdio>


/* ------------------------------------------------------------------------------------ */

namespace boost {


/* ------------------------------------------------------------------------------------ */

class format
{
private:
	template <typename T>
	void replace( T sval )
	{
		const char FMTC = '%';
		const size_t parsed_pos1 = m_fmt.find_first_of( FMTC );
		if( parsed_pos1 != std::string::npos )
		{
			const size_t parsed_pos2 = m_fmt.find_first_of( FMTC, parsed_pos1+1 );
			if ( parsed_pos2 != std::string::npos )
			{
 				m_fmt.replace( parsed_pos1, parsed_pos2 - parsed_pos1 + 1 , sval );
			}
		}
	}
public:
    format( const std::string &fmt )
		: m_fmt( fmt )
    {
		
    }
    const char* str() const
    {
		return m_fmt.c_str();
    }
    format& operator%(int x)
    {	
		char tbuf[16];
		std::snprintf(tbuf, sizeof(tbuf), "%i", x );
		replace( tbuf );
		return *this;
    }
    format& operator%(unsigned x)
	{	
		char tbuf[16];
		std::snprintf(tbuf, sizeof(tbuf), "%u", x );
		replace( tbuf );
		return *this;
	}
	format& operator%(void *x)
	{	
		char tbuf[32];
		std::snprintf(tbuf, sizeof(tbuf), "%p", x );
		replace( tbuf );
		return *this;
	}
	format& operator%(char ch)
	{	
		char tbuf[2] = { ch, '\0' };
		replace( tbuf );
		return *this;
	}
	format& operator%(const char *str)
	{	
		replace( str );
		return *this;
	}
	format& operator%(const std::string &str )
	{	
		replace( str );
		return *this;
	}
private:
    std::string m_fmt;
};
/* ------------------------------------------------------------------------------------ */

/* ------------------------------------------------------------------------------------ */
}
/* ------------------------------------------------------------------------------------ */
#endif
