/*
 * =====================================================================================
 *
 *       Filename:  fmtlog.cpp
 *
 *    Description: LOG FORMAT LIBRARY
 *
 *        Version:  1.0
 *        Created:  02/19/2013 09:50:35 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lucjan Bryndza <lucjan_B1@verifone.com>
 *   Organization:  VERIFONE
 *
 * =====================================================================================
 */

/* ------------------------------------------------------------------------------------ */
#ifndef _COM_VERIFONE_LOG_FMTLOG_HPP_
#define _COM_VERIFONE_LOG_FMTLOG_HPP_
/* ------------------------------------------------------------------------------------ */
#ifdef __cplusplus
/* ------------------------------------------------------------------------------------ */
#include <string>

/* ------------------------------------------------------------------------------------ */
namespace com_verifone_log
{

enum loglevel
{
    loglevel_info = 1,
    loglevel_warn,
    loglevel_error,
    loglevel_trace
};

/* ------------------------------------------------------------------------------------ */
namespace _internal {
namespace _handlers {

    // Internal output handler function
    void output_string( loglevel level, const char* file, unsigned line, const std::string& message );
}}


}   //namespace com_verifone_log

/* ------------------------------------------------------------------------------------ */
#ifdef DEBUG    /* DEBUG version */

#ifndef CONFIG_FMTLOG_DONT_USE_BOOST_FORMAT
#include <boost/format.hpp>
#else
#include "format_lite.hpp"
#endif
/* ------------------------------------------------------------------------------------ */

#define com_verifone_log_internal_arg1(level, message) \
    com_verifone_log::_internal::_handlers::output_string( level ,__FILE__,__LINE__, message ) 

#define com_verifone_log_internal_arg2( level, fmt, args ) \
   com_verifone_log::_internal::_handlers::output_string( level ,__FILE__,__LINE__, (boost::format((fmt))%args).str() ) 

#define com_verifone_log_internal_get_4th_arg(arg1, arg2, arg3, arg4, ...) arg4

#define com_verifone_log_internal_macro_chooser(...) \
    com_verifone_log_internal_get_4th_arg( __VA_ARGS__, com_verifone_log_internal_arg2, com_verifone_log_internal_arg1, )

#define log_fmt_level( ... ) com_verifone_log_internal_macro_chooser(__VA_ARGS__)( __VA_ARGS__)
#define log_fmt_info( ... )  log_fmt_level( com_verifone_log::loglevel_info, __VA_ARGS__)
#define log_fmt_warn( ... )  log_fmt_level( com_verifone_log::loglevel_warn, __VA_ARGS__)
#define log_fmt_error( ... ) log_fmt_level( com_verifone_log::loglevel_error, __VA_ARGS__)
#define log_fmt_trace( ... ) log_fmt_level( com_verifone_log::loglevel_trace, __VA_ARGS__)

/* ------------------------------------------------------------------------------------ */
#else      /* DEBUG */

#define log_fmt_level( ... )       do {} while(0)
#define log_fmt_info( ... )  do {} while(0)
#define log_fmt_warn( ... )  do {} while(0)
#define log_fmt_error( ... ) do {} while(0)
#define log_fmt_trace( ... ) do {} while(0)

/* ------------------------------------------------------------------------------------ */
#endif  /* DEBUG */
/* ------------------------------------------------------------------------------------ */

/** Example usage 
    log_fmt_err(" Ala %1% ma kota %2% ",  zmiena1 % zmienna2 )
*/
/* ------------------------------------------------------------------------------------ */
#else /* __cplusplus */
#error C++ only log header
#endif /* __cplusplus */


/* ------------------------------------------------------------------------------------ */
#endif  /* _COM_VERIFONE_LOG_FMTLOG_HPP_ */

