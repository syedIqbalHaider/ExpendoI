#ifndef _LIBSECCMD_H_
#define _LIBSECCMD_H_

/**  @file libseccmd.h
*
*   @brief ADK Security Service API
*
*   This header file contains information about the ADK security service API functions.
*/

/** @addtogroup  com_verifone_seccmd */
/** @{ */

/***************************************************************************
 * Includes
 **************************************************************************/
#include <stdlib.h>
#include "sec.h"
#include <stdint.h>

#ifdef _VRXEVO
#include <unistd.h>
#include <string>
#endif

#ifdef _VOS
#include <string>
#endif

#if (defined _VRXEVO || defined _WIN32)
#  if   defined VFI_SEC_DLL_EXPORT
#    define DllSpecSEC __declspec(dllexport)
#  elif defined VFI_SEC_DLL_IMPORT && defined _VRXEVO
#    define DllSpecSEC __declspec(dllimport) // dllimport not required for Windows
#  else
#    define DllSpecSEC
#  endif
#elif defined __GNUC__ && defined VFI_SEC_DLL_EXPORT
#  define DllSpecSEC  __attribute__((visibility ("default")))
#else
#  define DllSpecSEC
#endif

/***************************************************************************
 * Namespace
 **************************************************************************/
using namespace com_verifone_host;

/***************************************************************************
 * Preprocessor constant definitions
 **************************************************************************/
#define FLAG_BYPASS_KSN_INCR     0x0001
#define FLAG_PIN_CONFIRMATION    0x0010


/***************************************************************************
 * Global variable declarations
 **************************************************************************/
namespace com_verifone_seccmd
{
  /** Defines */

  #define SCRIPT_NAME_LENGTH         10

  /** Communication timeouts */
  #define MIN_COMM_TIMEOUT        30000
  #define DEFAULT_COMM_TIMEOUT       -1 /** A negative timeout means: wait forever */

  /** PIN entry timeouts */
  #define MAX_PIN_TIMEOUT        300000
  #define DEFAULT_PIN_TIMEOUT     30000

  /** Error codes */
  enum SecCmd_errors
  {
    SC_SUCCESS = 0,         /**<  0*/
    SC_ERROR = -1,          /**< -1 */
    SC_WRONG_PAR = -2,      /**< -2 */
    SC_SEND_FAILED = -3,    /**< -3 */
    SC_RECBUF_SMALL = -4,   /**< -4 */
    SC_TIMER_ERROR = -5     /**< -5 */
  };

  /** Data container for encryption/decryption functions */
  struct EncDecData
  {
    uint8_t *pInData; /**< input buffer*/
    u_long uiInLen;   /**< length of input buffer */
    uint8_t *pOutData;/**< output buffer*/
    u_long uiOutLen;  /**< length of output buffer */
    uint8_t *stan;    /**< up to 6 bytes, NULL terminated ASCII data, optional, NULL value as default*/
    uint8_t *tid;     /**< up to 8 bytes, NULL terminated ASCII data, optional, NULL value as default*/
  };

  /** Data container for encryption/decryption functions */
  struct EncDecDataIV
  {
    uint8_t *pInData; /**< input buffer*/
    u_long uiInLen;   /**< length of input buffer */
    uint8_t *pOutData;/**< output buffer*/
    u_long uiOutLen;  /**< length of output buffer */
    uint8_t *IV;      /**< IV, optional, NULL value as default {0}*/
    u_long uiIVLen;   /**< length of IV buffer */
  };

  /** Data container for MAC calculation/verification */
  struct MACData
  {
    uint8_t *pData; /**< input buffer */
    u_long   uiLen; /**< length of input buffer */
  };

  /** MAC container for MAC calculation/verification */
  struct genMAC
  {
    uint8_t *pMAC;    /**< MAC buffer */
    u_char  ucMACLen; /**< size of MAC data */
  };

  /** Data container for additional data using by PIN encryption */
  struct TransAmnt_t
  {
    u_char  *pTAData;
    u_long   uiTALen;
  };

  /** Parameter container for PIN block using in PIN entry/retrieving functions */
  struct PINBlockParams
  {
    u_char *pPAN;   /**< pointer to NULL terminated string containing PAN (19 digits max)*/
    u_char *pSTAN;  /**< pointer to NULL terminated string containing STAN (6 digits max)*/
    u_char PinBlockFormat;
    u_char TransAmount[TRANS_AMOUNT_SIZE]; /**< Used only for Sec_RetrieveEncryptedPIN - 6 bytes BCD transaction amount */
  };

  /** Dialog parameters for PIN entry dialogs */
  struct PINParams
  {
    u_char      *pPinTextHtmlPath; /**< pointer to NULL terminated string (128 characters max)*/
    u_char       TransAmount[TRANS_AMOUNT_SIZE];
    u_char       TransCurrCode[TRANS_CURR_CODE_SIZE];
    u_char       TransCurrExp[TRANS_CURR_EXPONENT_SIZE];
    u_char       PinEntryType;
    u_char       PinTryFlag;
    u_char       FontFileSelect[SEC_FILENAME_SIZE];
    u_char       PINMessageText[ADD_SCREEN_TEXT];
    u_char       AmountText[ADD_SCREEN_TEXT];
    u_char       AddScreenText[ADD_SCREEN_TEXT];
    u_char       AddScreenText2[ADD_SCREEN_TEXT];
    u_char       MinPINLength;
    u_char       MaxPINLength;
    u_char       PinCancel;
  };

  /** Dialog parameters for PIN entry dialogs v.2 */
  struct PINParams_v2
  {
    u_char       *pPinTextHtmlPath; /**< pointer to NULL terminated string (128 characters max)*/
    u_char        TransAmount[TRANS_AMOUNT_SIZE];
    u_char        PinEntryType;
    u_char        PinTryFlag;
    u_char       *PINMessageText;   /**< pointer to NULL terminated string */
    u_char       *AmountText;       /**< pointer to NULL terminated string */
    u_char       *AddScreenText;    /**< pointer to NULL terminated string */
    u_char       *AddScreenText2;   /**< pointer to NULL terminated string */
    u_char        MinPINLength;
    u_char        MaxPINLength;
    u_char        PinCancel;
  };

  /** Dialog parameters for PIN entry dialogs v.3 */
  struct PINParams_v3
  {
    u_char       *pPinTextHtmlPath; /**< pointer to NULL terminated string (128 characters max)*/
    TransAmnt_t   TransAmount;
    u_char        PinEntryType;
    u_char        PinTryFlag;
    u_char       *PINMessageText;   /**< pointer to NULL terminated string */
    u_char       *AmountText;       /**< pointer to NULL terminated string */
    u_char       *AddScreenText;    /**< pointer to NULL terminated string */
    u_char       *AddScreenText2;   /**< pointer to NULL terminated string */
    u_char        MinPINLength;
    u_char        MaxPINLength;
    u_char        PinCancel;
  };

  /** Proprietary data passed to VSS scripts */
  struct PropData
  {
    uint8_t *pPropData;
    u_long   uiPropDataLen;
  };

  /** Online PIN Cipher Block container */
  struct EncPINBlock
  {
    uint8_t *pPINBlock;       /**< pointer to data buffer */
    u_char   ucPINBlockLen;   /**< [in] maximal buffer size, [out] length of data stored in pPINBlock buffer */
  };

  /** structure of security configuration */
  struct SecConfig
  {
    u_char PinEntryType;    /**< PIN entry type, 0 = PIN Mandatory; 1 = PIN Optional;  2 = PIN Optional - 0-Length PIN Encryption;  3 = 0-Length PIN */
    u_char PinBlockFormat;  /**< PIN block format, 0 = ISO-0; 1 = ISO-1; 2 = ISO-2;  3 = ISO-3 */
    u_char KeyManagement;   /**< key management, 1 = MSK; 2 = DUKPT */
    u_char PINAlgo;         /**< PIN algorithm, 1 = Single DES; 2 = 3 DES; 3 = AES */
    u_char DataAlgo;        /**< data algorithm, 1 = Single DES; 2 = 3 DES; 3 = AES */
    u_char ScriptIdent[SCRIPT_NAME_LENGTH];  /**< script identifier */
  };

  /** Parameter container for DUKPT Key Serial Number (KSN) */
  struct Ksn
  {
    u_char KSN[KSN_SIZE];
  };

  /** Parameter container for DUKPT Key Serial Number (KSN), version 2 */
  struct Ksn_v2
  {
    u_char *KSN;
    u_long KSNLen;
  };

  /** Parameter container for DUKPT Key masking */
  struct DUKPTmask
  {
    u_char *maskPIN;  /**< mask for PIN */
    u_char *maskMAC;  /**< mask for MAC */
    u_char *maskENC;  /**< mask for encryption */
    u_char sizeP;     /**< size of mask for PIN */
    u_char sizeM;     /**< size of mask for MAC */
    u_char sizeE;     /**< size of mask for encryption */
  };

/***************************************************************************
 * Exported function declarations
 **************************************************************************/

  /** Generic interface to establish an IPC communication to Security Service
   *
   * @param[in] appName     - optional parameter defined application name
   *                          to display in trace/syslog. Null terminated string.
   * @return 0 in success case, other value in error case (see #SecCmd_errors)
   */
DllSpecSEC int Sec_Init( u_char *appName = NULL );

  /** Generic interface to close IPC connection to Security Service
   *
   */
DllSpecSEC void Sec_Destroy( );


/***************************************************************************
 * Exported function declarations
 **************************************************************************/

  /** Generic interface to open Encryption Module based on Host Name
   *
   * @param[in] schemaName    - name of encryption scheme
   * @param[out] errCode     - error code sent from SC
   * @return                 a handle to encryption scheme
   *                         0 - error case
   */
DllSpecSEC u_char Sec_Open( const std::string &schemaName, uint8_t *errCode );


  /** Generic interface to open Encryption Module based on Host Name
   *
   * @param[in] schemaName    - name of encryption scheme. Null terminated string.
   * @param[out] errCode     - error code sent from SC
   * @return                 a handle to encryption scheme
   *                         0 - error case
   */
DllSpecSEC u_char Sec_Open( u_char * schemaName, uint8_t *errCode );


  /** Generic interface to close Encryption Module
   *
   * @param[in] hdl         - handle to encryption scheme
   * @param[out] errCode     - error code sent from SC
   * @return 0 in success case, other value in error case (see #SecCmd_errors)
   */
DllSpecSEC int Sec_Close(u_char hdl, uint8_t *errCode);


  /** SecApp, Selecting host configuration
   * @param[in] SHC_HostId   - Host Id to set
   * @param[in] timeout      - timeout for receiving answer (in milliseconds) - minimum value: 30000 (is automatically
   *                           set if value is smaller), default value: -1 (wait forever)
   * @param[out] errCode     - error code sent from SC
   *
   * @return 0 in success case, other value in error case (see #SecCmd_errors)
   */
DllSpecSEC int Sec_SelectHostConf(u_char SHC_HostId, uint8_t *errCode, long timeout = DEFAULT_COMM_TIMEOUT);


#ifndef DOXYGEN_SHOULD_SKIP_THIS
/** SecApp, Get Key Data
   * @param[in] GKD_HostId    - Host Id to set
   * @param[in] GKD_KeyType   - key type
   *                    4 - TCU RSA Public Key
   *                    12 - Encrypted KI (Format 0)
   *                    13 - Encrypted KI (Format 1)
   *                    14 - Encrypted KI (Format 2)
   *                    15 - Encrypted PPID
   *                    16 - Encrypted PPASN
   *                    17 - KVC of KIA
   *                    18 - KVC of KEK1
   *                    19 - KVC of KT
   * @param[in]  uiOutBufLen  - maximal buffer length
   * @param[in] timeout       - timeout for receiving answer (in milliseconds) - minimum value: 30000 (is automatically
   *                            set if value is smaller), default value: -1 (wait forever)
   * @param[out] errCode      - error code sent from SC
   * @param[out] aOutBuf      - Key data, specific for each key type
   * @param[out] uiOutBufLen  - data length
   *
   * @return 0 in success case, other value in error case (see #SecCmd_errors)
   */
DllSpecSEC int Sec_GetKeyData(u_char GKD_HostId, u_char GKD_KeyType, char *aOutBuf, u_long *uiOutBufLen,
      uint8_t *errCode, long timeout = DEFAULT_COMM_TIMEOUT);


/** SecApp, Get Key Data
   * @param[in] GKD_HostId    - Host Id to set
   * @param[in] GKD_KeyType   - key type
   *                    4 - TCU RSA Public Key
   *                    12 - Encrypted KI (Format 0)
   *                    13 - Encrypted KI (Format 1)
   *                    14 - Encrypted KI (Format 2)
   *                    15 - Encrypted PPID
   *                    16 - Encrypted PPASN
   *                    17 - KVC of KIA
   *                    18 - KVC of KEK1
   *                    19 - KVC of KT
   * @param[in] kekFlag       - indicates use of KEK1 or KEK2 for Key Type 11
   * @param[in]  uiOutBufLen  - maximal buffer length
   * @param[in] timeout       - timeout for receiving answer (in milliseconds) - minimum value: 30000 (is automatically
   *                            set if value is smaller), default value: -1 (wait forever)
   * @param[out] errCode      - error code sent from SC
   * @param[out] aOutBuf      - Key data, specific for each key type
   * @param[out] uiOutBufLen  - data length
   *
   * @return 0 in success case, other value in error case (see #SecCmd_errors)
   */
DllSpecSEC int Sec_GetKeyData(u_char GKD_HostId, u_char GKD_KeyType, u_char kekFlag, char *aOutBuf,
        u_long *uiOutBufLen, uint8_t *errCode, long timeout = DEFAULT_COMM_TIMEOUT);

#endif /* DOXYGEN_SHOULD_SKIP_THIS */



  /** SecApp, update key
   * @deprecated This function should not be used anymore as it might not be supported in future releases
   * @param[in] UpK_HostId   - Host Id to set
   * @param[in] UpK_KeyType  - key type (IPP relevant: KEY_TYPE_PPK, KEY_TYPE_MGK)
   * @param[in] aKeyData     - key data to store in SC (NULL is also allowed)
   * @param[in] uiKeyDataLen - length of the key data
   * @param[in] timeout      - timeout for receiving answer (in milliseconds) - minimum value: 30000 (is automatically
   *                           set if value is smaller), default value: -1 (wait forever)
   * @param[out] errCode     - error code sent from SC
   *
   * @return 0 in success case, other value in error case (see #SecCmd_errors)
   */
DllSpecSEC int Sec_UpdateKey(u_char UpK_HostId, u_char UpK_KeyType, char *aKeyData, u_long uiKeyDataLen,
      uint8_t *errCode, long timeout = DEFAULT_COMM_TIMEOUT);


  /** SecApp, update key
   * @deprecated This function should not be used anymore as it might not be supported in future releases
   * @param[in] UpK_HostId   - Host Id to set
   * @param[in] UpK_KeyType  - key type (IPP relevant: KEY_TYPE_PPK, KEY_TYPE_MGK)
   * @param[in] kekFlag      - indicates use of KEK1 or KEK2 for Key Type 11
   * @param[in] aKeyData     - key data to store in SC (NULL is also allowed)
   * @param[in] uiKeyDataLen - length of the key data
   * @param[in] timeout      - timeout for receiving answer (in milliseconds) - minimum value: 30000 (is automatically
   *                           set if value is smaller), default value: -1 (wait forever)
   * @param[out] errCode     - error code sent from SC
   *
   * @return 0 in success case, other value in error case (see #SecCmd_errors)
   */
DllSpecSEC int Sec_UpdateKey(u_char UpK_HostId, u_char UpK_KeyType, u_char kekFlag, char *aKeyData,
      u_long uiKeyDataLen, uint8_t *errCode, long timeout = DEFAULT_COMM_TIMEOUT);


  /**
   * SecApp, update key
   * @deprecated This function should not be used anymore as it might not be supported in future releases
   * @param[in] UpK_HostId   - Host Id to set
   * @param[in] UpK_KeyType  - key type (IPP relevant: KEY_TYPE_PPK, KEY_TYPE_MGK)
   * @param[in] kekFlag      - indicates use of KEK1 or KEK2 for Key Type 11
   * @param[in] aKeyData     - key data to store in SC (NULL is also allowed). For IPP usage data have to be provided in ASCII format.
   * @param[in] uiKeyDataLen - length of the key data
   * @param[in] pKSN         - Key Serial Number for DUKPT key (optional for other key types). For IPP usage data have to be provided in BIN format.
   * @param[in] timeout      - timeout for receiving answer (in milliseconds) - minimum value: 30000 (is automatically
   *                           set if value is smaller), default value: -1 (wait forever)
   * @param[out] errCode     - error code sent from SC
   *
   * @return 0 in success case, other value in error case (see #SecCmd_errors)
   */
DllSpecSEC int Sec_UpdateKey(u_char UpK_HostId, key_type_t UpK_KeyType, u_char kekFlag,
                    char *aKeyData, u_long uiKeyDataLen, Ksn *pKSN, uint8_t *errCode, long timeout = DEFAULT_COMM_TIMEOUT);


   /** SecApp, update key
   * @param[in] UpK_HostId   - Host Id to set
   * @param[in] UpK_KeyType  - key type (IPP relevant: KEY_TYPE_PPK, KEY_TYPE_MGK)
   * @param[in] kekFlag      - indicates use of KEK1 or KEK2 for Key Type 11
   * @param[in] aKeyData     - key data to store in SC (NULL is also allowed). For IPP usage data have to be provided in ASCII format.
   * @param[in] uiKeyDataLen - length of the key data
   * @param[in] KSN          - KSN for new DUKPT key (variable length). For IPP usage data have to be provided in BIN format.
   * @param[in] timeout      - timeout for receiving answer (in milliseconds) - minimum value: 30000 (is automatically
   *                           set if value is smaller), default value: -1 (wait forever)
   * @param[out] errCode     - error code sent from SC
   *
   * @return 0 in success case, other value in error case (see #SecCmd_errors)
   */
DllSpecSEC int Sec_UpdateKey(u_char UpK_HostId, key_type_t UpK_KeyType, u_char kekFlag,
                    char *aKeyData, u_long uiKeyDataLen, Ksn_v2 KSN,
                    uint8_t *errCode, long timeout = DEFAULT_COMM_TIMEOUT);


  /** SecApp, encrypt data
   * @deprecated This function should not be used anymore as it might not be supported in future releases
   * @param[in]     EncD_HostId   - Host Id to set
   * @param[in]     EncD_EncMode  - DES encryption mode; 0 - CBC, 1 - OFB with SV "0123456789ABCDEF", 2 - OFB with SV set to the XOR of the STAN
   * @param[in]     pData         - structure with inp/output parameters ([length in]:data size / [length out]:maximal buffer size),
   *                                6 bytes STAN and 8 bytes TID are optional
   * @param[out]    pData
   * @param[out]    errCode       - error code sent from SC
   * @param[in] timeout           - timeout for receiving answer (in milliseconds) - minimum value: 30000 (is automatically
   *                                set if value is smaller), default value: -1 (wait forever)
   *
   * @return 0 in success case, other value in error case (see #SecCmd_errors)
   */
DllSpecSEC int Sec_EncryptData(u_char EncD_HostId, u_char EncD_EncMode, EncDecData *pData, uint8_t *errCode, long timeout = DEFAULT_COMM_TIMEOUT);


  /** SecApp, encrypt data
   * @deprecated This function should not be used anymore as it might not be supported in future releases
   * @param[in]     EncD_HostId   - Host Id to set
   * @param[in]     pData         - structure with inp/output parameters ([length in]:data size / [length out]:maximal buffer size)
   * @param[out]    pData
   * @param[out]    errCode       - error code sent from SC
   * @param[in] timeout           - timeout for receiving answer (in milliseconds) - minimum value: 30000 (is automatically
   *                                set if value is smaller), default value: -1 (wait forever)
   *
   * @return 0 in success case, other value in error case (see #SecCmd_errors)
   */
DllSpecSEC int Sec_EncryptData(u_char EncD_HostId, EncDecDataIV *pData, uint8_t *errCode, long timeout = DEFAULT_COMM_TIMEOUT);


  /** SecApp, encrypt data
   * @param[in]     EncD_HostId   - Host Id to set
   * @param[in]     pData         - structure with inp/output parameters, IV is optional
   * @param[out]    pData
   * @param[out]    pKSN          - structure of KSN
   * @param[out]    errCode       - error code sent from SC
   * @param[in]     timeout      - timeout for receiving answer (in milliseconds) - minimum value: 30000, default value: -1 (wait forever)
   *
   * @return 0 in success case, other value in error case (see #SecCmd_errors)
   */
DllSpecSEC int Sec_EncryptData(u_char EncD_HostId, EncDecDataIV *pData, Ksn *pKSN, uint8_t *errCode, long timeout = DEFAULT_COMM_TIMEOUT);


  /** SecApp, encrypt data
   * @param[in]     EncD_HostId   - Host Id to set
   * @param[in]     EncD_EncMode  - encryption mode, useb by encryption module.
   *                                ADE: 0 - ECB mode, 1 - CBC mode
   * @param[in]     pData         - structure with inp/output parameters, IV is optional
   * @param[out]    pData
   * @param[out]    pKSN          - structure of KSN
   * @param[out]    errCode       - error code sent from SC
   * @param[in] timeout           - timeout for receiving answer (in milliseconds) - minimum value: 30000 (is automatically
   *                                set if value is smaller), default value: -1 (wait forever)
   *
   * @return 0 in success case, other value in error case (see #SecCmd_errors)
   */
DllSpecSEC int Sec_EncryptData(u_char EncD_HostId, u_char EncD_EncMode, EncDecDataIV *pData, Ksn *pKSN, uint8_t *errCode, long timeout = DEFAULT_COMM_TIMEOUT);


  /** SecApp, decrypt data
   * @deprecated This function should not be used anymore as it might not be supported in future releases
   * @param[in] DecD_HostId   - Host Id to set
   * @param[in] DecD_DecMode  - DES encryption mode
   *                    0 - CBC
   *                    1 - OFB with SV "0123456789ABCDEF"
   *                    2 - OFB with SV set to the XOR of the STAN
   * @param[in]  pData       - structure with inp/output parameters ([length in]:data size / [length out]:maximal buffer size),
   *                           6 bytes STAN and 8 bytes TID are optional
   * @param[out] pData
   * @param[out] errCode     - error code sent from SC
   * @param[in] timeout      - timeout for receiving answer (in milliseconds) - minimum value: 30000 (is automatically
   *                           set if value is smaller), default value: -1 (wait forever)
   *
   * @return 0 in success case, other value in error case (see #SecCmd_errors)
   */
DllSpecSEC int Sec_DecryptData(u_char DecD_HostId, u_char DecD_DecMode, EncDecData *pData, uint8_t *errCode,
      long timeout = DEFAULT_COMM_TIMEOUT);


  /** SecApp, decrypt data
   * @deprecated This function should not be used anymore as it might not be supported in future releases
   * @param[in] DecD_HostId   - Host Id to set
   * @param[in] pData        - structure with inp/output parameters ([length in]:data size / [length out]:maximal buffer size),
   *                           IV is optional and will be set to {0} vector
   * @param[out] pData       - structure with inp/output parameters
   * @param[out] errCode     - error code sent from SC
   * @param[in] timeout      - timeout for receiving answer (in milliseconds) - minimum value: 30000 (is automatically
   *                           set if value is smaller), default value: -1 (wait forever)
   *
   * @return 0 in success case, other value in error case (see #SecCmd_errors)
   */
DllSpecSEC int Sec_DecryptData(u_char DecD_HostId, EncDecDataIV *pData, uint8_t *errCode, long timeout = DEFAULT_COMM_TIMEOUT);


  /** SecApp, decrypt data
   * @param[in] DecD_HostId   - Host Id to set
   * @param[in] pData        - structure with inp/output parameters, IV is optional
   * @param[out] pData       - structure with inp/output parameters
   * @param[out] pKSN        - structure of KSN
   * @param[out] errCode     - error code sent from SC
   * @param[in] timeout      - timeout for receiving answer (in milliseconds) - minimum value: 30000 (is automatically
   *                           set if value is smaller), default value: -1 (wait forever)
   *
   * @return 0 in success case, other value in error case (see #SecCmd_errors)
   */
DllSpecSEC int Sec_DecryptData(u_char DecD_HostId, EncDecDataIV *pData, Ksn *pKSN, uint8_t *errCode, long timeout = DEFAULT_COMM_TIMEOUT);


#ifndef DOXYGEN_SHOULD_SKIP_THIS

/** SecApp, Get Key Data
   * @param[in] AS2805_HostId     - Host Id to set
   * @param[in] AS2805_AS2805Func - AS2805 Function
   * @param[in] aKeyData          - Key Data
   * @param[in] uiKeyDataLen      - Key Data Length
   * @param[in] uiMaxKeyDataLen   - Maximal Key Data Length
   * @param[in] timeout           - timeout for receiving answer (in milliseconds) - minimum value: 30000 (is automatically
   *                                set if value is smaller), default value: -1 (wait forever)
   * @param[out] errCode          - error code sent from SC
   *
   * @return 0 in success case, other value in error case (see #SecCmd_errors)
   */
DllSpecSEC int Sec_AS2805KeyMgmnt(u_char AS2805_HostId, u_char AS2805_AS2805Func, char *aKeyData,
    u_long *uiKeyDataLen, u_long uiMaxKeyDataLen, uint8_t *errCode, long timeout = DEFAULT_COMM_TIMEOUT);

#endif /* DOXYGEN_SHOULD_SKIP_THIS */


  /** SecApp, GenerateMAC
   * @deprecated This function should not be used anymore as it might not be supported in future releases
   * @param[in] GMAC_HostId        - Host Id to set
   * @param[in] GMAC_UseDefaultCV  - use default card value flag --- paramter is currently not used!
   *                                    0 - no (default if not provided)
   *                                    1 - yes
   * @param[in] GMAC_MACMode       - MAC mode
   *                                    0 - AS2804.6.2 MAC (default if not present)
   *                                    1 - AS2804.6.4 MAC
   *                                    5 - IPP, ASCII Data
   *                                    7 - IPP, Binary Data
   * @param[in]  pData            - structure of data to be MAC-ed
   * @param[in] pMAC              - structure of MAC and MAC length - maximal buffer size
   * @param[out] pMAC             - structure of MAC and MAC size
   * @param[out] errCode          - error code sent from SC
   * @param[in] timeout           - timeout for receiving answer (in milliseconds) - minimum value: 30000 (is automatically
   *                                set if value is smaller), default value: -1 (wait forever)
   *
   * @return 0 in success case, other value in error case (see #SecCmd_errors)
   */
DllSpecSEC int Sec_GenerateMAC(u_char GMAC_HostId, u_char GMAC_UseDefaultCV, u_char GMAC_MACMode,
                      MACData *pData, genMAC *pMAC, uint8_t *errCode, long timeout = DEFAULT_COMM_TIMEOUT);


  /** SecApp, GenerateMAC
   * @param[in] GMAC_HostId        - Host Id to set
   * @param[in] GMAC_UseDefaultCV  - use default card value flag --- paramter is currently not used!
   *                                    0 - no (default if not provided)
   *                                    1 - yes
   * @param[in] GMAC_MACMode       - MAC mode
   *                                  Not IPP:
   *                                    paramter is not used!
   *                                  IPP:
   *                                    5 - IPP, ASCII Data
   *                                    7 - IPP, Binary Data
   * @param[in]  pData            - structure of data to be MAC-ed
   * @param[out] pMAC             - structure of MAC length and MAC (Always in ASCII format if IPP is used)
   * @param[out] pKSN             - structure of KSN
   * @param[out] errCode          - error code sent from SC
   * @param[in] timeout           - timeout for receiving answer (in milliseconds) - minimum value: 30000 (is automatically
   *                                set if value is smaller), default value: -1 (wait forever)
   *
   * @return 0 in success case, other value in error case (see #SecCmd_errors)
   */
DllSpecSEC int Sec_GenerateMAC(u_char GMAC_HostId, u_char GMAC_UseDefaultCV, u_char GMAC_MACMode,
                      MACData *pData, genMAC *pMAC, Ksn *pKSN, uint8_t *errCode, long timeout = DEFAULT_COMM_TIMEOUT);


  /** SecApp, VerifyMAC
   * @deprecated This function should not be used anymore as it might not be supported in future releases
   * @param[in] VMAC_HostId       - Host Id to set
   * @param[in] VMAC_UseDefaultCV - use default card value flag --- paramter is currently not used!
   *                         0 - no (default if not provided)
   *                         1 - yes
   * @param[in] VMAC_MACMode    - MAC mode
   *                            0 - AS2804.6.2 MAC (default if not present)
   *                            1 - AS2804.6.4 MAC
   * @param[in] pData         - structure of data to be MAC-ed
   * @param[in] MAC              - structure of MAC length and MAC to verify
   * @param[out] errCode         - error code sent from SC
   * @param[in] timeout          - timeout for receiving answer (in milliseconds) - minimum value: 30000 (is automatically
   *                               set if value is smaller), default value: -1 (wait forever)
   *
   * @return 0 in success case, other value in error case (see #SecCmd_errors)
   */
DllSpecSEC int Sec_VerifyMAC(u_char VMAC_HostId, u_char VMAC_UseDefaultCV, u_char VMAC_MACMode,
                    MACData *pData, genMAC MAC, uint8_t *errCode, long timeout = DEFAULT_COMM_TIMEOUT);


  /** SecApp, VerifyMAC
   * @param[in] VMAC_HostId       - Host Id to set
   * @param[in] VMAC_UseDefaultCV - use default card value flag --- paramter is currently not used!
   *                         0 - no (default if not provided)
   *                         1 - yes
   * @param[in] VMAC_MACMode     - MAC mode --- paramter is not used!
   * @param[in] pData            - structure of data to be MAC-ed
   * @param[in] MAC              - structure of MAC length and MAC
   * @param[out] pKSN            - structure of KSN
   * @param[out] errCode         - error code sent from SC
   * @param[in] timeout          - timeout for receiving answer (in milliseconds) - minimum value: 30000 (is automatically
   *                               set if value is smaller), default value: -1 (wait forever)
   *
   * @return 0 in success case, other value in error case (see #SecCmd_errors)
   */
DllSpecSEC int Sec_VerifyMAC(u_char VMAC_HostId, u_char VMAC_UseDefaultCV, u_char VMAC_MACMode,
                    MACData *pData, genMAC MAC, Ksn *pKSN, uint8_t *errCode, long timeout = DEFAULT_COMM_TIMEOUT);


  /** SecApp, EnterAndEncryptPIN
   * @deprecated This function should not be used anymore as it might not be supported in future releases
   * @param[in] EEP_HostId        - Host Id to set
   * @param[in] pEEP_PINParams    - structure of PIN data:
   *                             - pPinTextHtmlPath  - HTML file name path for PIN entry text
   *                             - TransAmount[7]    - transaction amount
   *                             - TransCurrCode     - transaction currency code (parameter is not used)
   *                             - TransCurrExp      - transaction currency exponent (parameter is not used)
   *                             - PinEntryType      - PIN entry type
   *                                                   0 = PIN Mandatory
   *                                                   1 = PIN Optional
   *                                                   2 = PIN Optional - 0-Length PIN Encryption
   *                                                   3 = 0-Length PIN
   *                             - PinTryFlag        - PIN try flag (controls display output - only effective if pPinTextHtmlPath==NULL)
   *                                                   0 = 1st Try
   *                                                   1 = PIN Retry
   *                                                   2 = Last PIN Try
   *                             - FontFileSelect    - font file name (parameter has no effect)
   *                             - PINMessageText    - message to be displayed
   *                             - AmountText        - amount text to be displayed
   *                             - AddScreenText     - display of an additional line of text
   *                             - AddScreenText2    - display of a second additional line of text
   *                             - MinPINLength      - minimal PIN length: 4 - 12 Characters
   *                             - MaxPINLength      - maximal PIN length: 4 - 12 Characters
   *                             - PinCancel         - PIN cancelling flag
   *                                                   bit0 = 0 - clears all PIN digits when CLEAR key is pressed
   *                                                   bit0 = 1 - clears one PIN digit when CLEAR key is pressed
   *                                                   bit1 = 0 - cancel by CLEAR key not allowed
   *                                                   bit1 = 1 - cancel by CLEAR key allowed
   * @param[in] pEEP_PINBlockParams   - structure of PIN block parameter:
   *                             - pPAN              - pointer to PAN (NULL terminated string, 20 digits max).
   *                                                   Only the last 12 digits, except the last digit (Luhn value), are used (Luhn value is not checked)
   *                             - pSTAN             - pointer to STAN (NULL terminated string, 12 digits max), mandatory if PIN block format ISO-1 is used
   *                             - PinBlockFormat    - PIN block format
   *                                                   0 = ISO-0
   *                                                   1 = ISO-1
   *                                                   2 = ISO-2
   *                                                   3 = ISO-3
   *                             - TransAmount       - not used
   * @param[in] EEP_Flags             - flags
   *                                   - Bit 4 = request PIN confirmation (same PIN must be entered again) (=0x10)
   * @param[in] EEP_PropData          - proprietary data passed to VSS scripts
   * @param[in] timeout               - timeout for operator to enter PIN and response to be received (0-300000 msec), default value 30000
   * @param[in] timeoutToFirstDigit   - timeout for operator to enter the first PIN digit (0-300000 msec) --- paramter is currently not used!
   * @param[in] timeoutToDigits       - timeout for operator to enter subsequent PIN digits (0-300000 msec) --- paramter is currently not used!
   * @param[out] pEEP_encPIN          - online PIN Cipher Block
   * @param[out] errCode              - error code sent from SC
   *
   * @return 0 in success case, other value in error case (see #SecCmd_errors)
   */
DllSpecSEC int Sec_EnterAndEncryptPIN( u_char          EEP_HostId,
                             PINParams      *pEEP_PINParams,
                             PINBlockParams *pEEP_PINBlockParams,
                             u_char          EEP_Flags,
                             PropData        EEP_PropData,
                             EncPINBlock    *pEEP_encPIN,
                             uint8_t        *errCode,
                             long            timeout = DEFAULT_PIN_TIMEOUT,
                             long            timeoutToFirstDigit = DEFAULT_PIN_TIMEOUT,
                             long            timeoutToDigits = DEFAULT_PIN_TIMEOUT);


  /** SecApp, EnterAndEncryptPIN
   * @deprecated This function should not be used anymore as it might not be supported in future releases
   * @param[in] EEP_HostId       - Host Id to set
   * @param[in] pEEP_PINParams   - structure of PIN data v.2:
   *                             - pPinTextHtmlPath  - HTML file name path for PIN entry text, NULL terminated string
   *                             - TransAmount[7]    - transaction amount
   *                             - PinEntryType      - PIN entry type
   *                                                   0 = PIN Mandatory
   *                                                   1 = PIN Optional
   *                                                   2 = PIN Optional - 0-Length PIN Encryption
   *                                                   3 = 0-Length PIN
   *                             - PinTryFlag        - PIN try flag (controls display output - only effective if pPinTextHtmlPath==NULL)
   *                                                   0 = 1st Try
   *                                                   1 = PIN Retry
   *                                                   2 = Last PIN Try
   *                             - PINMessageText    - message to be displayed, NULL terminated string
   *                             - AmountText        - amount text to be displayed, NULL terminated string
   *                             - AddScreenText     - display of an additional line of text, NULL terminated string
   *                             - AddScreenText2    - display of a second additional line of text, NULL terminated string
   *                             - MinPINLength      - minimal PIN length: 4 - 12 Characters
   *                             - MaxPINLength      - maximal PIN length: 4 - 12 Characters
   *                             - PinCancel         - PIN cancelling flag
   *                                                   bit0 = 0 - clears all PIN digits when CLEAR key is pressed
   *                                                   bit0 = 1 - clears one PIN digit when CLEAR key is pressed
   *                                                   bit1 = 0 - cancel by CLEAR key not allowed
   *                                                   bit1 = 1 - cancel by CLEAR key allowed
   * @param[in] pEEP_PINBlockParams   - structure of PIN block parameter:
   *                             - pPAN              - pointer to PAN (NULL terminated string, 20 digits max).
   *                                                   Only the last 12 digits, except the last digit (Luhn value), are used (Luhn value is not checked)
   *                             - pSTAN             - pointer to STAN (NULL terminated string, 12 digits max), mandatory if PIN block format ISO-1 is used
   *                             - PinBlockFormat    - PIN block format
   *                                                   0 = ISO-0
   *                                                   1 = ISO-1
   *                                                   2 = ISO-2
   *                                                   3 = ISO-3
   *                             - TransAmount       - not used
   * @param[in] EEP_Flags             - flags
   *                                   - Bit 0 = bypass KSN incrementation in case of DUKPT support (=0x0001)
   *                                   - Bit 4 = request PIN confirmation (same PIN must be entered again) (=0x0010)
   * @param[in] EEP_PropData          - proprietary data passed to VSS scripts
   * @param[in] timeout               - timeout for operator to enter PIN and response to be received (0-300000 msec), default value 30000
   * @param[in] timeoutToFirstDigit   - timeout for operator to enter the first PIN digit (0-300000 msec) --- paramter is currently not used!
   * @param[in] timeoutToDigits       - timeout for operator to enter subsequent PIN digits (0-300000 msec) --- paramter is currently not used!
   * @param[out] pEEP_encPIN          - online PIN Cipher Block
   * @param[out] pEEP_KSN             - structure of KSN
   * @param[out] errCode              - error code sent from SC
   *
   * @return 0 in success case, other value in error case (see #SecCmd_errors)
   */
DllSpecSEC int Sec_EnterAndEncryptPIN(u_char          EEP_HostId,
                             PINParams_v2   *pEEP_PINParams,
                             PINBlockParams *pEEP_PINBlockParams,
                             uint16_t        EEP_Flags,
                             PropData        EEP_PropData,
                             EncPINBlock    *pEEP_encPIN,
                             Ksn            *pEEP_KSN,
                             uint8_t        *errCode,
                             long            timeout = DEFAULT_PIN_TIMEOUT,
                             long            timeoutToFirstDigit = DEFAULT_PIN_TIMEOUT,
                             long            timeoutToDigits = DEFAULT_PIN_TIMEOUT);


  /** SecApp, EnterAndEncryptPIN
   * @li not IPP relevant:
   * @param[in] EEP_HostId       - Host Id to set
   * @param[in] pEEP_PINParams   - structure of PIN data v.3:
   *                             - pPinTextHtmlPath  - HTML file name path for PIN entry text, NULL terminated string
   *                                                   In case if bit 4 of EEP_Flags is set, the HTML file name path will be used for second PIN entry dialog as well.
   *                                                   To define different HTML file for the second dialog the file path can be added to the string via ";" separator
   *                             - TransAmount       - Transaction Amount - 6 bytes BCD coded
   *                             - PinEntryType      - PIN entry type
   *                                                   0 = PIN Mandatory
   *                                                   1 = PIN Optional
   *                                                   2 = PIN Optional - 0-Length PIN Encryption
   *                                                   3 = 0-Length PIN
   *                             - PinTryFlag        - PIN try flag (controls display output - only effective if pPinTextHtmlPath==NULL)
   *                                                   0 = 1st Try
   *                                                   1 = PIN Retry
   *                                                   2 = Last PIN Try
   *                             - PINMessageText    - message to be displayed, NULL terminated string
   *                             - AmountText        - amount text to be displayed, NULL terminated string
   *                             - AddScreenText     - display of an additional line of text, NULL terminated string
   *                             - AddScreenText2    - display of a second additional line of text, NULL terminated string
   *                             - MinPINLength      - minimal PIN length: 4 - 12 Characters
   *                             - MaxPINLength      - maximal PIN length: 4 - 12 Characters
   *                             - PinCancel         - PIN cancelling flag
   *                                                   bit0 = 0 - clears all PIN digits when CLEAR key is pressed
   *                                                   bit0 = 1 - clears one PIN digit when CLEAR key is pressed
   *                                                   bit1 = 0 - cancel by CLEAR key not allowed
   *                                                   bit1 = 1 - cancel by CLEAR key allowed
   * @param[in] pEEP_PINBlockParams   - structure of PIN block parameter:
   *                             - pPAN              - pointer to PAN (NULL terminated string, 20 digits max).
   *                                                   Only the last 12 digits, except the last digit (Luhn value), are used (Luhn value is not checked)
   *                             - pSTAN             - pointer to STAN (NULL terminated string, 12 digits max), mandatory if PIN block format ISO-1 is used
   *                             - PinBlockFormat    - PIN block format
   *                                                   0 = ISO-0
   *                                                   1 = ISO-1
   *                                                   2 = ISO-2
   *                                                   3 = ISO-3
   *                             - TransAmount       - not used
   * @param[in] EEP_Flags             - flags
   *                                   - Bit 0 = bypass KSN incrementation in case of DUKPT support (=0x0001)
   *                                   - Bit 4 = retry PIN entry (same PIN must be entered again) (=0x0010)
   * @param[in] EEP_PropData          - proprietary data passed to VSS scripts
   * @param[in] timeout               - timeout for operator to enter PIN and response to be received (0-300000 msec)
   * @param[in] timeoutToFirstDigit   - timeout for operator to enter the first PIN digit --- paramter is currently not used!
   * @param[in] timeoutToDigits       - timeout for operator to enter subsequent PIN digits --- paramter is currently not used!
   * @param[out] pEEP_encPIN          - online PIN Cipher Block
   * @param[out] pEEP_KSN             - structure v2 of KSN - not relevant if KSN incrementation is bypassed (see EEP_Flags)
   * @param[out] errCode              - error code sent from SC
   * @li IPP relevant:
   * @param[in] EEP_HostId       - Host Id to set
   * @param[in] pEEP_PINParams   - structure of PIN data v.3:
   *                             - TransAmount       - not used
   *                             - PinEntryType      - PIN entry type
   *                                                   0 = PIN Mandatory
   *                                                   1 = PIN Optional
   *                             - PINMessageText    - message to be displayed
   *                             - AmountText        - amount text to be displayed
   *                             - AddScreenText     - display of an additional line of text
   *                             - AddScreenText2    - display of a second additional line of text
   *                             - MinPINLength      - minimal PIN length: 4 - 12 Characters
   *                             - MaxPINLength      - maximal PIN length: 4 - 12 Characters
   * @param[out] pEEP_encPIN          - online PIN Cipher Block. Data are provided in ASCII or BIN format according configuration.
   *                                    Output data depend on key management scheme:
   *                                      - MSK:   20 Bytes (PINLength[2], PINFormat[2], PINBlock[16])
   *                                      - DUKPT: 16 Bytes PINBlock
   * @param[out] pEEP_KSN             - structure v2 of KSN. Data are provided in ASCII or BIN format according configuration.
   * @param[out] errCode              - error code sent from SC
   *                                    Note:
   *                                    In case of IPP usage error codes provided by the OS IPP component are returned (see 'MS Packet 71'
   *                                    or 'DUKPT Packet 75' in 'Verix eVo Volume I:Operations Systems, Programmers Guide, Appendix D: IPP Communications Packets')
   *
   * @return 0 in success case, other value in error case (see #SecCmd_errors)
   */
DllSpecSEC int Sec_EnterAndEncryptPIN(u_char          EEP_HostId,
                             PINParams_v3   *pEEP_PINParams,
                             PINBlockParams *pEEP_PINBlockParams,
                             uint16_t        EEP_Flags,
                             PropData        EEP_PropData,
                             EncPINBlock    *pEEP_encPIN,
                             Ksn_v2         *pEEP_KSN,
                             uint8_t        *errCode,
                             long            timeout = DEFAULT_PIN_TIMEOUT,
                             long            timeoutToFirstDigit = DEFAULT_PIN_TIMEOUT,
                             long            timeoutToDigits = DEFAULT_PIN_TIMEOUT);

  /** SecApp, EnterAndHoldPIN
   * @deprecated This function should not be used anymore as it might not be supported in future releases
   * @param[in] EHP_HostId            - Host Id to set
   * @param[in] pEHP_PINParams        - structure of PIN data:
   *                                     - pPinTextHtmlPath  - HTML file name path for PIN entry text
   *                                     - TransAmount       - not used
   *                                     - TransCurrCode     - transaction currency code (parameter is not used)
   *                                     - TransCurrExp      - transaction currency exponent (parameter is not used)
   *                                     - PinTryFlag        - PIN try flag (controls display output - only effective if pPinTextHtmlPath==NULL)
   *                                                           0 = PIN Mandatory
   *                                                           1 = PIN Optional
   *                                                           2 = PIN Optional - 0-Length PIN Encryption
   *                                                           3 = 0-Length PIN
   *                                     - PinTryFlag        - PIN try flag (only effective if pPinTextHtmlPath==NULL)
   *                                                           0 = 1st Try
   *                                                           1 = PIN Retry
   *                                                           2 = Last PIN Try
   *                                     - FontFileSelect    - font file name (parameter has no effect)
   *                                     - PINMessageText    - message to be displayed
   *                                     - AmountText        - amount text to be displayed
   *                                     - AddScreenText     - display of an additional line of text
   *                                     - AddScreenText2    - display of a second additional line of text
   *                                     - MinPINLength      - minimal PIN length: 4 - 12 Characters
   *                                     - MaxPINLength      - maximal PIN length: 4 - 12 Characters
   *                                     - PinCancel         - PIN cancelling flag
   *                                                           bit0 = 0 - clears all PIN digits when CLEAR key is pressed
   *                                                           bit0 = 1 - clears one PIN digit when CLEAR key is pressed
   *                                                           bit1 = 0 - cancel by CLEAR key not allowed
   *                                                           bit1 = 1 - cancel by CLEAR key allowed
   * @param[in] EHP_Flags             - flags                 - not used
   * @param[in] timeout               - timeout for operator to enter PIN and response to be received (0-300000 msec), default value 30000
   * @param[in] timeoutToFirstDigit   - timeout for operator to enter the first PIN digit (0-300000 msec) --- paramter is currently not used!
   * @param[in] timeoutToDigits       - timeout for operator to enter subsequent PIN digits (0-300000 msec) --- paramter is currently not used!
   * @param[out] errCode              - error code sent from SC
   *
   * @return 0 in success case, other value in error case (see #SecCmd_errors)
   */
DllSpecSEC int Sec_EnterAndHoldPIN(u_char       EHP_HostId,
                          PINParams   *pEHP_PINParams,
                          u_char       EHP_Flags,
                          uint8_t     *errCode,
                          long         timeout = DEFAULT_PIN_TIMEOUT,
                          long         timeoutToFirstDigit = DEFAULT_PIN_TIMEOUT,
                          long         timeoutToDigits = DEFAULT_PIN_TIMEOUT);

  /** SecApp, EnterAndHoldPIN
   * @param[in] EHP_HostId            - Host Id to set
   * @param[in] pEHP_PINParams        - structure of PIN data v.2:
   *                                     - pPinTextHtmlPath  - HTML file name path for PIN entry text, NULL terminated string
   *                                     - TransAmount       - not used
   *                                     - PinEntryType      - PIN entry type
   *                                                           0 = PIN Mandatory
   *                                                           1 = PIN Optional
   *                                                           2 = PIN Optional - 0-Length PIN Encryption
   *                                                           3 = 0-Length PIN
   *                                     - PinTryFlag        - PIN try flag (controls display output - only effective if pPinTextHtmlPath==NULL)
   *                                                           0 = 1st Try
   *                                                           1 = PIN Retry
   *                                                           2 = Last PIN Try
   *                                     - PINMessageText    - message to be displayed, NULL terminated string
   *                                     - AmountText        - amount text to be displayed, NULL terminated string
   *                                     - AddScreenText     - display of an additional line of text, NULL terminated string
   *                                     - AddScreenText2    - display of a second additional line of text, NULL terminated string
   *                                     - MinPINLength      - minimal PIN length: 4 - 12 Characters
   *                                     - MaxPINLength      - maximal PIN length: 4 - 12 Characters
   *                                     - PinCancel         - PIN cancelling flag
   *                                                           bit0 = 0 - clears all PIN digits when CLEAR key is pressed
   *                                                           bit0 = 1 - clears one PIN digit when CLEAR key is pressed
   *                                                           bit1 = 0 - cancel by CLEAR key not allowed
   *                                                           bit1 = 1 - cancel by CLEAR key allowed
   * @param[in] EHP_Flags             - flags                 - not used
   * @param[in] timeout               - timeout for operator to enter PIN and response to be received (0-300000 msec), default value 30000
   * @param[in] timeoutToFirstDigit   - timeout for operator to enter the first PIN digit (0-300000 msec) --- paramter is currently not used!
   * @param[in] timeoutToDigits       - timeout for operator to enter subsequent PIN digits (0-300000 msec) --- paramter is currently not used!
   * @param[out] errCode              - error code sent from SC
   *
   * @return 0 in success case, other value in error case (see #SecCmd_errors)
   */
DllSpecSEC int Sec_EnterAndHoldPIN(u_char        EHP_HostId,
                         PINParams_v2 *pEHP_PINParams,
                         uint16_t      EHP_Flags,
                         uint8_t      *errCode,
                         long          timeout = DEFAULT_PIN_TIMEOUT,
                         long          timeoutToFirstDigit = DEFAULT_PIN_TIMEOUT,
                         long          timeoutToDigits = DEFAULT_PIN_TIMEOUT);


  /** SecApp, RetrieveEncryptedPIN
   * @deprecated This function should not be used anymore as it might not be supported in future releases
   * @param[in] REP_HostId          - Host Id to set
   * @param[in] pREP_PINBlockParams - structure of PIN block parameter:
   *                                   - pPAN              - pointer to PAN (NULL terminated string, 20 digits max).
   *                                                         Only the last 12 digits, except the last digit (Luhn value), are used (Luhn value is not checked)
   *                                   - pSTAN             - pointer to STAN (NULL terminated string, 12 digits max), mandatory if PIN block format ISO-1 is used
   *                                   - PinBlockFormat    - PIN block format
   *                                                         0 = ISO-0
   *                                                         1 = ISO-1
   *                                                         2 = ISO-2
   *                                                         3 = ISO-3
   *                                   - TransAmount       - Transaction Amount - 6 bytes BCD coded
   * @param[in] REP_Flags           - flags                 - not used
   * @param[in] REP_PropData        - proprietary data passed to VSS scripts
   * @param[in] timeout             - timeout for receiving answer (in milliseconds) - minimum value: 30000 (is automatically
   *                                  set if value is smaller), default value: -1 (wait forever)
   * @param[out] pREP_encPIN        - online PIN Cipher Block
   * @param[out] errCode            - error code sent from SC
   *
   * @return 0 in success case, other value in error case (see #SecCmd_errors)
   */
DllSpecSEC int Sec_RetrieveEncryptedPIN(u_char          REP_HostId,
                               PINBlockParams *pREP_PINBlockParams,
                               u_char          REP_Flags,
                               PropData        REP_PropData,
                               EncPINBlock    *pREP_encPIN,
                               uint8_t        *errCode,
                               long            timeout = DEFAULT_COMM_TIMEOUT);


  /** SecApp, RetrieveEncryptedPIN
   * @deprecated This function should not be used anymore as it might not be supported in future releases
   * @param[in] REP_HostId          - Host Id to set
   * @param[in] pREP_PINBlockParams - structure of PIN block parameter:
   *                                   - pPAN              - pointer to PAN (NULL terminated string, 20 digits max).
   *                                                         Only the last 12 digits, except the last digit (Luhn value), are used (Luhn value is not checked)
   *                                   - pSTAN             - pointer to STAN (NULL terminated string, 12 digits max), mandatory if PIN block format ISO-1 is used
   *                                   - PinBlockFormat    - PIN block format
   *                                                         0 = ISO-0
   *                                                         1 = ISO-1
   *                                                         2 = ISO-2
   *                                                         3 = ISO-3
   *                                   - TransAmount       - Transaction Amount - 6 bytes BCD coded
   * @param[in] REP_Flags           - flags
   *                                   - Bit 0 = bypass KSN incrementation in case of DUKPT support (=0x0001)
   * @param[in] REP_PropData        - proprietary data passed to VSS scripts
   * @param[in] timeout             - timeout for receiving answer (in milliseconds) - minimum value: 30000 (is automatically
   *                                  set if value is smaller), default value: -1 (wait forever)
   * @param[out] pREP_encPIN        - online PIN Cipher Block
   * @param[out] pREP_KSN           - structure of KSN
   * @param[out] errCode            - error code sent from SC
   *
   * @return 0 in success case, other value in error case (see #SecCmd_errors)
   */
DllSpecSEC int Sec_RetrieveEncryptedPIN(u_char         REP_HostId,
                              PINBlockParams *pREP_PINBlockParams,
                              uint16_t        REP_Flags,
                              PropData        REP_PropData,
                              EncPINBlock    *pREP_encPIN,
                              Ksn            *pREP_KSN,
                              uint8_t        *errCode,
                              long            timeout = DEFAULT_COMM_TIMEOUT);


  /** SecApp, RetrieveEncryptedPIN
   * @param[in] REP_HostId          - Host Id to set
   * @param[in] pREP_PINBlockParams - structure of PIN block parameter:
   *                                   - pPAN              - pointer to PAN (NULL terminated string, 20 digits max).
   *                                                         Only the last 12 digits, except the last digit (Luhn value), are used (Luhn value is not checked)
   *                                   - pSTAN             - pointer to STAN (NULL terminated string, 12 digits max), mandatory if PIN block format ISO-1 is used
   *                                   - PinBlockFormat    - PIN block format
   *                                                         0 = ISO-0
   *                                                         1 = ISO-1
   *                                                         2 = ISO-2
   *                                                         3 = ISO-3
   *                                   - TransAmount       - Transaction Amount - 6 bytes BCD coded
   * @param[in] REP_Flags           - flags
   *                                   - Bit 0 = bypass KSN incrementation in case of DUKPT support (=0x0001)
   * @param[in] REP_PropData        - proprietary data passed to VSS scripts
   * @param[in] timeout             - timeout for receiving answer (in milliseconds) - minimum value: 30000 (is automatically
   *                                  set if value is smaller), default value: -1 (wait forever)
   * @param[out] pREP_encPIN        - online PIN Cipher Block
   * @param[out] pREP_KSN           - structure of KSN
   * @param[out] errCode            - error code sent from SC
   * @li IPP relevant:
   * @param[in] REP_HostId           - Host Id to set
   * @param[out] pREP_encPIN          - online PIN Cipher Block. Data are provided in ASCII or BIN format according configuration.
   *                                    Output data depend on key management scheme:
   *                                      - MSK:   20 Bytes (PINLength[2], PINFormat[2], PINBlock[16])
   *                                      - DUKPT: 16 Bytes PINBlock
   * @param[out] pREP_KSN             - structure v2 of KSN. Data are provided in ASCII or BIN format according configuration.
   * @param[out] errCode              - error code sent from SC
   *                                    Note:
   *                                    In case of IPP usage error codes provided by the OS IPP component are returned (see 'MS Packet 71'
   *                                    or 'DUKPT Packet 75' in 'Verix eVo Volume I:Operations Systems, Programmers Guide, Appendix D: IPP Communications Packets')
   *
   * @return 0 in success case, other value in error case (see #SecCmd_errors)
   */
DllSpecSEC int Sec_RetrieveEncryptedPIN(u_char         REP_HostId,
                              PINBlockParams *pREP_PINBlockParams,
                              uint16_t        REP_Flags,
                              PropData        REP_PropData,
                              EncPINBlock    *pREP_encPIN,
                               Ksn_v2         *pREP_KSN,
                              uint8_t        *errCode,
                              long            timeout = DEFAULT_COMM_TIMEOUT);


  /** SecApp, CancelPIN (Returns error if no PIN entry process is running)
   * @param[in] CP_HostId    - Host Id to set
   * @param[in] timeout      - timeout for receiving answer (in milliseconds) - minimum value: 30000 (is automatically
   *                           set if value is smaller), default value: -1 (wait forever)
   * @param[out] errCode    - error code sent from SC (if PIN not available: error code != 0)
   *
   * @return 0 in success case, other value in error case (see #SecCmd_errors)
   */
DllSpecSEC int Sec_CancelPIN(u_char   CP_HostId,
                    uint8_t *errCode,
                    long     timeout = DEFAULT_COMM_TIMEOUT);


   /** SecApp, Set Security Configuration
   * @param[in] SSC_HostId     - Host Id to set
   * @param[in] SSC_PropData   - proprietary data passed to VSS scripts
   * @param[in] timeout        - timeout for receiving answer (in milliseconds) - minimum value: 30000 (is automatically
   *                             set if value is smaller), default value: -1 (wait forever)
   * @param[out] pSSC_SecConfig  - structure of security configuration:
   *                               - PinEntryType   - PIN entry type
   *                                                  0 = PIN Mandatory
   *                                                  1 = PIN Optional
   *                                                  2 = PIN Optional - 0-Length PIN Encryption
   *                                                  3 = 0-Length PIN
   *                               - PinBlockFormat - PIN block format
   *                                                  0 = ISO-0
   *                                                  1 = ISO-1
   *                                                  2 = ISO-2
   *                                                  3 = ISO-3
   *                               - KeyManagement  - key management
   *                                                  1 = MSK
   *                                                  2 = DUKPT
   *                               - PINAlgo        - PIN algorithm
   *                                                  1 = Single DES
   *                                                  2 = 3 DES
   *                                                  3 = AES
   *                               - DataAlgo       - data algorithm
   *                                                  1 = Single DES
   *                                                  2 = 3 DES
   *                                                  3 = AES
   *                               - ScriptIdent    - script identifier
   * @param[out] errCode         - error code sent from SC
   *
   * @return 0 in success case, other value in error case (see #SecCmd_errors)
   */
DllSpecSEC int Sec_SetSecurityConfig(u_char     SSC_HostId,
                            PropData   SSC_PropData,
                            SecConfig *pSSC_SecConfig,
                            uint8_t   *errCode,
                            long       timeout = DEFAULT_COMM_TIMEOUT);


  /** SecApp, Get Security Configuration
   * @param[in] GSC_HostId    - Host Id to set
   * @param[in] GSC_PropData  - proprietary data passed to VSS scripts
   * @param[in] timeout       - timeout for receiving answer (in milliseconds) - minimum value: 30000 (is automatically
   *                            set if value is smaller), default value: -1 (wait forever)
   * @param[out] pGSC_SecConfig  - structure of security configuration:
   *                               - PinEntryType   - PIN entry type
   *                                                  0 = PIN Mandatory
   *                                                  1 = PIN Optional
   *                                                  2 = PIN Optional - 0-Length PIN Encryption
   *                                                  3 = 0-Length PIN
   *                               - PinBlockFormat - PIN block format
   *                                                  0 = ISO-0
   *                                                  1 = ISO-1
   *                                                  2 = ISO-2
   *                                                  3 = ISO-3
   *                               - KeyManagement  - key management
   *                                                  1 = MSK
   *                                                  2 = DUKPT
   *                               - PINAlgo        - PIN algorithm
   *                                                  1 = Single DES
   *                                                  2 = 3 DES
   *                                                  3 = AES
   *                               - DataAlgo       - data algorithm
   *                                                  1 = Single DES
   *                                                  2 = 3 DES
   *                                                  3 = AES
   *                               - ScriptIdent    - script identifier
   * @param[out] errCode         - error code sent from SC
   *
   * @return 0 in success case, other value in error case (see #SecCmd_errors)
   */
DllSpecSEC int Sec_GetSecurityConfig(u_char     GSC_HostId,
                            PropData   GSC_PropData,
                            SecConfig *pGSC_SecConfig,
                            uint8_t   *errCode,
                            long       timeout = DEFAULT_COMM_TIMEOUT);


  /** SecApp, get components versions
   * @param[in] timeout    - timeout for receiving answer (in milliseconds) - minimum value: 30000 (is automatically
   *                         set if value is smaller), default value: -1 (wait forever)
   * @param[out] pVersions - version string in format (component separator:';'):
   *                             API:MAJOR.MINOR.BUILD;SC:MAJOR.MINOR.BUILD;HOSTIDn:name_n.vso;...;HOSTIDm:name_m.vso;
   *                             (where n,m are from 1,2, ..., k),
   *                             key words are:
   *                                 API    - API library
   *                                 SC     - security component
   *                                 HOSTID - host id
   * @param[out] errCode   - error code sent from SC
   *
   * @return 0 in success case, other value in error case (see #SecCmd_errors)
   */
DllSpecSEC int Sec_GetVersions(std::string *pVersions, uint8_t *errCode, long timeout = DEFAULT_COMM_TIMEOUT);


  /** SecApp, get API library version
   *
   * @return version string: \<major>.\<minor>.\<patch>-\<build>
   */
DllSpecSEC const char *Sec_GetVersion(void);


  /** SecApp, get security service version
   *
   * @return version string: \<major>.\<minor>.\<patch>-\<build>
   */
DllSpecSEC const char *Sec_GetSvcVersion(void);


  /** SecApp, Set key set ID - used in
  * - VSS for UpdateKey, Enc/Decryption, MAC Gen/Verification operations
  * - IPP for UpdateKey, MAC Generation, PIN Encryption operations
  *  @param[in] HostId   - Host Id to set
  *  @param[in] ksid     - key set id
  *                        - 1-8   for VSS
  *                        - 1-10  for IPPMSK
  *                        - 1-3   for IPPDUKPT
  *
  * @return 0 in success case, other value in error case (see #SecCmd_errors)
  */
DllSpecSEC int Sec_SetKSId(u_char HostId, uint8_t ksid);


   /** SecApp, set DUKPT mask
   * @param[in]  HostId       - Host Id to set
   * @param[in]  pDUKPTMask   - masking of PIN, MAC and ENC DUKPT key
   * @param[in] timeout       - timeout for receiving answer (in milliseconds) - minimum value: 30000 (is automatically
   *                            set if value is smaller), default value: -1 (wait forever)
   * @param[out] errCode      - error code sent from SC
   *
   * @return 0 in success case, other value in error case (see #SecCmd_errors)
   */
DllSpecSEC int Sec_SetDUKPTMask(u_char      HostId,
                        DUKPTmask   *pDUKPTMask,
                        uint8_t     *errCode,
                        long        timeout = DEFAULT_COMM_TIMEOUT);


   /** SecApp, Increment KSN (Key Serial Number)
   * @param[in]  HostId       - Host Id to set
   * @param[in]  timeout      - timeout for receiving answer (in milliseconds) - minimum value: 30000, default value: -1 (wait forever)
   * @param[out] pKSN	        - Key Serial Number
   * @param[out] errCode      - error code sent from SC
   *
   * @return 0 in success case, other value in error case (see #SecCmd_errors)
   */
DllSpecSEC int Sec_IncrementKSN(	u_char   HostId,
                        Ksn      *pKSN,
                        uint8_t  *errCode,
                        long     timeout = DEFAULT_COMM_TIMEOUT);

}
/** @} */

#endif //_LIBSECCMD_H_
