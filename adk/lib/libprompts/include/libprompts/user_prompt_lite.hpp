/*
 * user_prompt_lite.hpp
 *
 *  Created on: 27-04-2011
 *      Author: Lucjan_B1
 */

#ifndef USER_PROMPT_LITE_HPP_
#define USER_PROMPT_LITE_HPP_
/* ------------------------------------------------------------- */
#include <cstddef>
/* ------------------------------------------------------------- */
namespace com_verifone_guiapi
{
/* ------------------------------------------------------------- */
class userPromptLite
{
public:
    userPromptLite();
    userPromptLite(int gui_gid);

    int getPrompt( const char *section, const char *key, char *pbuf, std::size_t pbuf_len );

    int getPrompt(  const char *section, const unsigned int  index, char *pbuf, std::size_t pbuf_len );

    int getPrompt(  const char *key, char *pbuf, std::size_t pbuf_len )
    {
    	return getPrompt( current_section, key, pbuf, pbuf_len );
    }

    int getPrompt(const unsigned int  index,char *pbuf, std::size_t pbuf_len)
    {
    	return getPrompt( current_section, index, pbuf, pbuf_len );
    }

	void setSection( const char *section);
private:
	int updateLang();
	void updateLangFin();
private:
	char current_section[128];
	int current_group;
	int gui_group;
	char filename[128];
};

/* ------------------------------------------------------------- */
}	//com_verifone_guiapi
/* ------------------------------------------------------------- */
#endif /* USER_PROMPT_LITE_HPP_ */
/* ------------------------------------------------------------- */
