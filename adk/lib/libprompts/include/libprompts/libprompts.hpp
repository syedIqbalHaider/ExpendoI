/*****************************************************************************
 * Copyright (C) 2010 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file       libprompts.hpp
 * @author     Maciej 'karp' Karpiuk
 * @brief      Definition of prompt file manager class
 */

#ifndef LIBPROMPTS_HPP_
#define LIBPROMPTS_HPP_

#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

#include <string>
#include <vector>

class Prompts
{
    public:
                                    /**
                                     * normal class constructor
                                     * @param prompts_filename      filename to get prompts from
                                     *                              If no filename extension found, ".MSG" will be used.
                                     *                              Language files will be searched by inserting 2 byte
                                     *                              language code between the file root and extension,
                                     *                              i.e. "LANG.FR.MSG". Language code is in
                                     *                              ISO639-2 format.
                                     *                              If no argument given, default file will
                                     *                              be used: LANG.MSG
                                     */
                                    Prompts(std::string       prompts_filename = "LANG.MSG");

                                    /**
                                     * default destructor
                                     */
                                   ~Prompts();

                                   /**
                                    * change filaname used by the Prompt class
                                    * @param prompts_filename      filename to get prompts from
                                    *                              If no filename extension found, ".MSG" will be used.
                                    *                              Language files will be searched by inserting 2-4 byte
                                    *                              language code between the file root and extension,
                                    *                              i.e. "LANG.FR.MSG". Language code is in
                                    *                              ISO639-2 format.
                                    *                              If no argument given, default file will
                                    *                              be used: LANG.MSG
                                    * @return                      true if file exists, false for an error
                                    */
        bool                        setFilename(std::string prompts_filename = "LANG.MSG");

                                   /**
                                    * tell if class is set with a valid, authenticated prompts file
                                    *
                                    * @return                      true if class has a valid file,
                                    *                              false if not
                                    */
        bool                        hasValidFile() const { return check_prompts_authentication(); }

        /* --------------------------------- prompts --------------------------------- */
                                    /**
                                     * get prompt for given section and key
                                     * @param section               section to look for prompts into
                                     * @param key                   prompt key in the given section
                                     * @return                      string with prompt value or empty string
                                     *                              if prompt not found
                                     */
        std::string                 getPrompt(  const std::string & section,
                                                const std::string & key);

                                    /**
                                     * get prompt for given section and index
                                     * @param section               section to look for prompts into
                                     * @param index                 prompt index in the given section
                                     * @return                      string with prompt value or empty string
                                     *                              if prompt not found
                                     */
        std::string                 getPrompt(  const std::string & section,
                                                const unsigned int  index);
                                    /**
                                     * get prompt for actual section and key
                                     * @param key                   prompt key in actual section
                                     * @return                      string with prompt value or empty string
                                     *                              if prompt not found
                                     */
        std::string                 getPrompt(const std::string & key);
                                    /**
                                     * get prompt for actual section and index
                                     * @param index                 index of prompt in actual section
                                     * @return                      string with prompt value or empty string
                                     *                              if prompt not found
                                     */
        std::string                 getPrompt(const unsigned int  index);
                                    /**
                                     * set actual section
                                     * @param section               new actual section name
                                     * @return                      nothing
                                     */
        void                        setActualSection(const std::string & section) { m_section = section; }
        void                        setDefaultSection() { m_section="prompts"; }

        /* --------------------------------- parsing prompts --------------------------------- */

                                    /**
                                     * do some basic prompt parsing.
                                     * *** PLACEHOLDER *** \n is not currently supported by minini
                                     * @param base                  base prompt to parse
                                     * @param [out] return_table    vector of strings
                                     * @return                      number of items returned in vector
                                     */
        unsigned int                basicPromptParsing(std::string                  base,
                                                       std::vector<std::string> &   return_table) const;

                                    /*
                                     * remove all formatting chars
                                     * @param base                  base prompt to parse
                                     *
                                     * @return                      string without formatting
                                     */
        static std::string          stripPromptParsing(std::string                  base);

        /* --------------------------------- language support --------------------------------- */
                                    /**
                                     * get list of available prompt files basing onto actually set
                                     * filename maks
                                     * @param [out] return_list     vector of strings with available
                                     *                              ISO639-2 language codes
                                     * @return                      number of items returned in vector
                                     */
        unsigned int                getAvailableLanguages(std::vector<std::string> & return_list) const;

                                    /**
                                     * get current language selection
                                     * @return                      string with ISO639-2 language format or empty,
                                     *                              if such information cannot be extracted from
                                     *                              actually used file
                                     */
        std::string                 getCurrentLanguage() const;

                                    /**
                                     * set current language
                                     * @param new_language          ISO639-2 language code to be set or empty
                                     *                              string to revert to default
                                     * @return                      true if such language was present and was set,
                                     *                              false for any error or no lang file present
                                     */
        bool                        setCurrentLanguage(const std::string &  new_lang);
        bool                        setDefaultLanguage() { return setCurrentLanguage(""); }

        /* --------------------------------- font support --------------------------------- */

                                    /**
                                     * tell if a font was properly described in currently selected
                                     * prompt file
                                     * @return                      true if font definition is available
                                     */
        bool                        isFontDescribed() const { return m_fontidentified; }
                                    /**
                                     * return selected font's name
                                     * @return                      font name
                                     */
        std::string                 getFontname() const { return m_fontname; }
                                    /**
                                     * return selected font's size
                                     * @return                      font size
                                     */
        unsigned int                getFontsize() const { return m_fontsize; }
                                    /**
                                     * return selected font's style
                                     * @return                      font style
                                     */
        std::string                 getFontstyle() const { return m_fontstyle; }

        /* --------------------------------- helpers --------------------------------- */
        // extract root, language and extension from given filename. Files with no extension
        // will have extension ".MSG" added
        static bool                 filename_extract_fields(std::string     main_filename,
                                                            std::string &   root,
                                                            std::string &   lang,
                                                            std::string &   extension);
    private:
        // explode filename, take '.' as separator
        static unsigned int         string_explode( std::string                 base,
                                                    const char                  separator,
                                                    std::vector<std::string> &  retval);
        // extract font parameters
        void                        promptfile_extract_font(const std::string   filename,
                                                            std::string  &      fontname,
                                                            unsigned int &      fontsize,
                                                            std::string  &      fontstyle) const;
        // get actual filename
        std::string                 filename_glue() const;
        // get base filename
        std::string                 filename_base_glue() const;
        // push actual prompt file font to class internals
        void                        font_push();
        // push base prompt file font to class internals
        void                        font_base_push();
        // flush internal font info
        void                        font_flush() { m_fontidentified = false; }
        // return true if authenticated, false if security violation
        bool                        check_prompts_authentication() const;
        // extract information about preferable font
        void                        extract_font_info(std::string  fontname);
        // is a custom language set?
        bool                        is_custom_lang_set() const;
        std::string                 m_def_language;
        std::string                 m_root;
        std::string                 m_extension;
        std::string                 m_section;
        std::string                 m_language;
        bool                        m_fontidentified;
        std::string                 m_fontname;
        unsigned int                m_fontsize;
        std::string                 m_fontstyle;
};

#endif /* LIBPROMPTS_HPP_ */
