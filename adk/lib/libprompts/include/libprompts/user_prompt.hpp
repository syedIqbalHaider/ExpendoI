/* ------------------------------------------------------------- */
/*
 * user_prompt.hpp
 *
 *  Created on: 08-04-2011
 *      Author: Lucjan_B1
 */
/* ------------------------------------------------------------- */
#ifndef USER_PROMPT_HPP_
#define USER_PROMPT_HPP_
/* ------------------------------------------------------------- */
#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/* ------------------------------------------------------------- */
#include <string>
class Prompts;

/* ------------------------------------------------------------- */
namespace com_verifone_guiapi
{
/* ------------------------------------------------------------- */
class userPrompt
{
public:
	userPrompt();

	~userPrompt();

	userPrompt(const userPrompt &up);

    const userPrompt& operator=( const userPrompt& );

    /**
     * get prompt for given section and key
     * @param section               section to look for prompts into
     * @param key                   prompt key in the given section
     * @return                      string with prompt value or empty string
     *                              if prompt not found
     */
std::string                 getPrompt(  const std::string & section, const std::string & key);

    /**
     * get prompt for given section and index
     * @param section               section to look for prompts into
     * @param index                 prompt index in the given section
     * @return                      string with prompt value or empty string
     *                              if prompt not found
     */
std::string                 getPrompt(  const std::string & section, const unsigned int  index);
    /**
     * get prompt for actual section and key
     * @param key                   prompt key in actual section
     * @return                      string with prompt value or empty string
     *                              if prompt not found
     */
std::string                 getPrompt(const std::string & key);
    /**
     * get prompt for actual section and index
     * @param index                 index of prompt in actual section
     * @return                      string with prompt value or empty string
     *                              if prompt not found
     */
std::string                 getPrompt(const unsigned int  index);
    /**
     * set actual section
     * @param section               new actual section name
     * @return                      nothing
     */
	 /** Set prompt section
	  * @param section Current section
	  */
	void setSection(const std::string & section)
	{
		curr_section = section;
	}
private:
	void updateLang();
	void updateLangFin();
	static int ref_cnt;
	static Prompts *prompt;
	std::string curr_section;
	int curr_group;

};
/* ------------------------------------------------------------- */
}

/* ------------------------------------------------------------- */
#endif /* USER_PROMPT_HPP_ */
