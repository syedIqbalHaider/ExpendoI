/*****************************************************************************
 * Copyright (C) 2010 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file       libprompts.cpp
 * @author     Maciej 'karp' Karpiuk
 * @brief      Implementation of prompt file manager class
 */

#include <libminini/minIni.h>
#include <libprompts/libprompts.hpp>
#include <libfoundation/DirScanner.h>
#include <libfoundation/ISO639_2.h>

#include <libpml/pml_abstracted_api.h>

#include <sstream>
#include <algorithm>

#ifdef VERIXV
#include <liblog/logsys.h>
#define report_log(...)   dlog_msg(__VA_ARGS__)
#else
#define report_log(...)   printf(__VA_ARGS__);
#endif

using namespace com_verifone_util;

/**
* normal class constructor
* @param prompts_filename      filename to get prompts from
*                              If no filename extension found, ".MSG" will be used.
*                              Language files will be searched by inserting 2 byte
*                              language code between the file root and extension,
*                              i.e. "LANG.FR.MSG". Language code is in
*                              ISO639-2 format.
*                              If no argument given, default file will
*                              be used: LANG.MSG
*/
Prompts::Prompts(std::string       prompts_filename) : m_fontidentified(false)
{
    setDefaultSection();
    setFilename(prompts_filename);
}

/**
* default destructor
*/
Prompts::~Prompts()
{

}

/**
 * change filaname used by the Prompt class
 * @param prompts_filename      filename to get prompts from
 *                              If no filename extension found, ".MSG" will be used.
 *                              Language files will be searched by inserting 2-4 byte
 *                              language code between the file root and extension,
 *                              i.e. "LANG.FR.MSG". Language code is in
 *                              ISO639-2 format.
 *                              If no argument given, default file will
 *                              be used: LANG.MSG
 * @return                      true if file exists, false for an error
 */
bool                        Prompts::setFilename(std::string prompts_filename)
{
    // explode filename
    if(prompts_filename.empty()) prompts_filename="LANG.MSG";
    m_language.clear();
    filename_extract_fields( prompts_filename, m_root, m_def_language, m_extension);
    font_push();
    return true;
}

// extract information about preferable font
void                        Prompts::extract_font_info(std::string  fontname)
{
    // extract font info
    std::string c_fontname, c_fontstyle;
    unsigned int c_fontsize;
    font_flush();
    m_fontname.clear(); m_fontstyle.clear(); m_fontsize=0;
    promptfile_extract_font(fontname, c_fontname, c_fontsize, c_fontstyle);
    // extract font info
    if(!c_fontname.empty() && c_fontsize>0) {
        m_fontidentified = true;
        m_fontname = c_fontname;
        m_fontsize = c_fontsize;
        if(!c_fontstyle.empty()) m_fontstyle = c_fontstyle;
    }
}

/* --------------------------------- prompts --------------------------------- */
/**
* get prompt for given section and key
* @param section               section to look for prompts into
* @param key                   prompt key in the given section
* @return                      string with prompt value or empty string
*                              if prompt not found
*/
std::string                 Prompts::getPrompt( const std::string & section,
                                                const std::string & key)
{
    std::string retval;
    font_flush();
    minIni  ini_reader(filename_glue());
    retval = ini_reader.gets(section, key);
    font_push(); // fallback font change disabled (#5200)
    if(retval.empty()!=true) {
        //font_push(); fallback font change disabled (#5200)
        return retval;
    }
    // fallback if needed
    if(is_custom_lang_set()) {
        minIni  ini_top_reader(filename_base_glue());
        retval = ini_top_reader.gets(section, key);
        if(retval.empty()!=true) {
            //font_base_push(); fallback font change disabled (#5200)
            return retval;
        }
    }
    return retval;
}

/**
* get prompt for given section and index
* @param section               section to look for prompts into
* @param index                 prompt index in the given section
* @return                      string with prompt value or empty string
*                              if prompt not found
*/
std::string                 Prompts::getPrompt( const std::string & section,
                                                const unsigned int  index)
{
    char num_buf[16];
    sprintf(num_buf, "%03i", index);
    std::string key = num_buf;
    return getPrompt(section, key);
}

/**
* get prompt for actual section and key
* @param key                   prompt key in actual section
* @return                      string with prompt value or empty string
*                              if prompt not found
*/
std::string                 Prompts::getPrompt(const std::string & key)
{
    return getPrompt(m_section, key);
}

/**
* get prompt for actual section and index
* @param index                 index of prompt in actual section
* @return                      string with prompt value or empty string
*                              if prompt not found
*/
std::string                 Prompts::getPrompt(const unsigned int  index)
{
    return getPrompt(m_section, index);
}


/* --------------------------------- language support --------------------------------- */
/**
* get list of available prompt files basing onto actually set
* filename maks
* @param [out] return_list     vector of strings with available
*                              ISO639-2 language codes
* @return                      number of items returned in vector
*/
unsigned int                Prompts::getAvailableLanguages(std::vector<std::string> & return_list) const
{
    return_list.clear();
    // create dirscanner using filename mask and look for available languages
    std::string mask = m_root + ".*." + m_extension;
    char        fname[MAX_FILENAME+1];
    // add default language if not on the list above
    if(m_def_language.empty())
    {
        std::string def_fname = m_root + "." + m_extension;
        if (com_verifone_pml::isFileSigned(def_fname))
        {
            return_list.push_back("def");
        }
    }
    // report_log("prompts> scanning for lang files, mask %s\n", mask.c_str());
    DirScanner  scanner(mask.c_str());
    while( scanner.getNext(fname) )
    {
        report_log("prompts> file candidate: %s\n", fname);
        if (com_verifone_pml::isFileSigned(std::string(fname)))
        {
            std::string root, lang, extension;
            if(filename_extract_fields(fname, root, lang, extension))
            {
                // check if in the ISO639_2 list
                if(lang.size() && ISO639_2::valid_item(lang)) return_list.push_back(lang);
            }
        }
        else
        {
            report_log("prompts warning> file candidate %s not authenticated!\n", fname);
        }
    }
    return return_list.size();
}

/**
* get current language selection
* @return                      string with ISO639-2 language format or empty,
*                              if such information cannot be extracted from
*                              actually used file
*/
std::string                 Prompts::getCurrentLanguage() const
{
    if(m_language.size()) return m_language;
    else return m_def_language;
}

/**
* set current language
* @param new_language          ISO639-2 language code to be set or empty
*                              string to revert to default
* @return                      true if such language was present and was set,
*                              false for any error or no lang file present
*/
bool                        Prompts::setCurrentLanguage(const std::string & new_lang)
{
    std::string local_lang = new_lang;
    std::transform(local_lang.begin(), local_lang.end(), local_lang.begin(), tolower);
    std::vector<std::string> available_languages;
    if(local_lang.empty())
    {
        if(!m_def_language.empty()) return false;
        local_lang="def";
    }
    if(getAvailableLanguages(available_languages)==0) return false;
    // check if item present on the list
    std::vector<std::string>::iterator  iter;
    for(iter=available_languages.begin();iter!=available_languages.end();++iter)
    {
        if((*iter).compare(local_lang)==0) {
            m_language = local_lang;
            if(m_language.compare("def")==0) m_language.clear();
            report_log("Selected language %s", m_language.c_str());
            font_push();
            return true;
        }
    }
    return false;
}

// is a custom language set?
bool                        Prompts::is_custom_lang_set() const
{
    if(m_language.empty()) return false;
    if(m_language.compare(m_def_language)==0) return false;
    return true;
}


/**
 * do some basic prompt parsing.
 * currently '\n' separator is supported.
 * @param base                  base prompt to parse
 * @param [out] return_table    vector of strings
 * @return                      number of items returned in vector
 */
unsigned int                Prompts::basicPromptParsing(std::string                  base,
                                                        std::vector<std::string> &   return_table) const
{
    if(base.empty()) return 0;
    return string_explode(base, '\n', return_table);
}

/*
 * remove all formatting chars
 * @param base                  base prompt to parse
 *
 * @return                      string without formatting
 */
std::string                 Prompts::stripPromptParsing(std::string                  base)
{
    // check if center line
    std::size_t pos = base.find("\\c");
    if(pos!=std::string::npos) {
        base.erase(pos, strlen("\\c"));
    }
    return base;
}

// ----------------- private section
// explode filename, take '.' as separator
unsigned int                Prompts::string_explode(std::string                 base,
                                                    const char                  separator,
                                                    std::vector<std::string> &  retval)
{
    retval.clear();
    std::size_t found;
    found = base.find_first_of(separator);
    while(found != std::string::npos){
        if(found>0) retval.push_back(base.substr(0,found));
        base = base.substr(found+1);
        found = base.find_first_of(separator);
    }
    if(base.length() > 0) retval.push_back(base);
    return retval.size();
}

// extract root, language and extension from given filename. Files with no extension
// will have extension ".MSG" added
bool                        Prompts::filename_extract_fields(   std::string     main_filename,
                                                                std::string &   root,
                                                                std::string &   lang,
                                                                std::string &   extension)
{
    if(!main_filename.size()) return false;
    std::vector<std::string> explode;
    std::transform(main_filename.begin(), main_filename.end(), main_filename.begin(), tolower);
    string_explode(main_filename, '.', explode);
    // attach default extension if extension not found
    if(explode.size()==1) explode.push_back("msg");
    // create mask
    unsigned int    last_field_index = explode.size()-1; // min.1
    // extract extension
    extension = explode[last_field_index];
    // check if language present
    last_field_index--;
    if(last_field_index==0)
    {
        // language extension not present
        lang.clear();
        root = explode[last_field_index];
    }
    else
    {
        // language extension present
        lang = explode[last_field_index--];
        unsigned int glue;
        root.clear();
        for(glue=0;glue<=last_field_index;glue++)
        {
            if(glue>0) root+=".";
            root+=explode[glue];
        }
    }
    report_log( "prompts> file root:\"%s\", extension:\"%s\", def language:\"%s\"\n",
                root.c_str(), extension.c_str(), lang.c_str());
    return true;
}

// extract font parameters
void                        Prompts::promptfile_extract_font(   const std::string   filename,
                                                                std::string  &      fontname,
                                                                unsigned int &      fontsize,
                                                                std::string  &      fontstyle) const
{
    minIni  ini_reader(filename);
    fontname = ini_reader.gets("font", "font_name");
    fontsize = static_cast<unsigned int>(ini_reader.getl("font", "font_size"));
    fontstyle = ini_reader.gets("font", "font_style");
}

// push actual prompt file font to class internals
void                        Prompts::font_push()
{
    extract_font_info(filename_glue());
}
// push base prompt file font to class internals
void                        Prompts::font_base_push()
{
    extract_font_info(filename_base_glue());
}

// get actual filename
std::string                 Prompts::filename_glue() const
{
    std::string retval;
    if(m_root.empty()) return retval;
    retval += m_root + '.';
    if(!m_language.empty()) retval += m_language + '.';
    else if(!m_def_language.empty()) retval += m_def_language + '.';
    retval += m_extension;
    std::transform(retval.begin(), retval.end(), retval.begin(), tolower);
    return retval;
}
// get base filename
std::string                 Prompts::filename_base_glue() const
{
    std::string retval;
    if(m_root.empty()) return retval;
    retval += m_root + '.';
    if(!m_def_language.empty()) retval += m_def_language + '.';
    retval += m_extension;
    std::transform(retval.begin(), retval.end(), retval.begin(), tolower);
    return retval;
}

// return true if authenticated, false if security violation
bool                        Prompts::check_prompts_authentication() const
{
#ifdef VERIXV
    std::string final_fname = filename_glue();
    int attrib = dir_get_attributes(final_fname.c_str());
    if(attrib & ATTR_NOT_AUTH) return false;
    return true;
#else
    return true;
#endif
}
