/*
 * user_prompt_lite.cpp
 *
 *  Created on: 27-04-2011
 *      Author: Lucjan_B1
 */

/* ------------------------------------------------------------- */
#include <cstring>
#include <cstdio>

#include <libprompts/user_prompt_lite.hpp>
#include <libminini/minIni.h>
#include <libpml/pml_abstracted_api.h>
#include <libpml/pml_port.h>
/* ------------------------------------------------------------- */
#ifdef VERIXV
#include <svc.h>
#endif

#include <liblog/logsys.h>
#define report_log(...)   dlog_msg(__VA_ARGS__);
#define report_error(...)   dlog_error(__VA_ARGS__);
/* ------------------------------------------------------------- */
// using namespace com_verifone_pml;

namespace com_verifone_guiapi
{
/* ------------------------------------------------------------- */
namespace	//string defs
{
	const char def_lang_preffix[] = "lang.";
	const char def_lang_suffix[] = ".msg";
	const char lang_env[] = "LANG";
}
/* ------------------------------------------------------------- */
//Default constructor
userPromptLite::userPromptLite() : current_group(get_group()), gui_group(get_group())
{
	current_section[0] = '\0';
	filename[0] = '\0';
}
/* ------------------------------------------------------------- */
userPromptLite::userPromptLite(int gui_gid) : current_group(get_group()), gui_group(gui_gid)
{
	current_section[0] = '\0';
	filename[0] = '\0';
}
/* ------------------------------------------------------------- */
//Update language private function
int userPromptLite::updateLang()
{
	char env[32];
	if (current_group != gui_group)
		set_group(gui_group);
	int ret = com_verifone_pml::get_env( lang_env, env, sizeof(env) );
	if(ret>0)
	{
		env[ret] = '\0';
		std::strncpy(filename, def_lang_preffix, sizeof(filename) );
		std::strncat(filename, env, sizeof(filename) );
		std::strncat(filename, def_lang_suffix, sizeof(filename) );
		filename[sizeof(filename)-sizeof('\0')] = '\0';
	}
	else
	{
		report_error("Unable to get current language");
		return -1;
	}
	return 0;
}
/* ------------------------------------------------------------- */
//Get user prompt
int userPromptLite::getPrompt( const char *section, const char *key, char *pbuf, std::size_t pbuf_len )
{
	int ret = 0;
	do
	{
		if((ret=updateLang())<0) break;
		if( (ret=ini_gets( section, key, "", pbuf, pbuf_len, filename ))<0) break;
	} while(0);
	updateLangFin();
	return ret;
}
/* ------------------------------------------------------------- */
//Get user prompt 2
int userPromptLite::getPrompt(  const char *section, const unsigned int  index, char *pbuf, std::size_t pbuf_len )
{
	char key[32];
	std::snprintf(key,sizeof(key),"%03d", index );
	return getPrompt( section, key, pbuf, pbuf_len );
}
/* ------------------------------------------------------------- */
void userPromptLite::updateLangFin()
{
	if(get_group()!=current_group)
	{
		set_group(current_group);
	}
}
/* ------------------------------------------------------------- */
//Set section
void userPromptLite::setSection( const char *section)
{
	std::strncpy( current_section, section,  sizeof(current_section) );
	current_section[sizeof(current_section)-sizeof('\0')] = '\0';
}
/* ------------------------------------------------------------- */
}
/* ------------------------------------------------------------- */
