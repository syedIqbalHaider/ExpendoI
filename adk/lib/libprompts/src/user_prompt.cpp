/* ------------------------------------------------------------- */
/*
 * user_prompt.cpp
 *
 *  Created on: 08-04-2011
 *      Author: Lucjan_B1
 */
/* ------------------------------------------------------------- */
#include <stdio.h>
#include <stdlib.h>
//#include <svc.h>
#include <assert.h>

#include "libprompts/user_prompt.hpp"
#include "libprompts/libprompts.hpp"

#include <libpml/pml_abstracted_api.h>
#include <libpml/pml_port.h>

/* ------------------------------------------------------------- */
#ifdef VERIXV
#include <liblog/logsys.h>
#define report_log(...)   dlog_msg(__VA_ARGS__);
#define report_error(...)   dlog_error(__VA_ARGS__);
#else
#define report_log(...)   printf(__VA_ARGS__);
#define report_error(...) fprintf(stderr,__VA_ARGS__);
#endif
/* ------------------------------------------------------------- */
namespace com_verifone_guiapi
{
/* ------------------------------------------------------------- */
int userPrompt::ref_cnt = 0;
Prompts* userPrompt::prompt = NULL;

namespace
{
	const char lang_env[] = "LANG";
}

/* ------------------------------------------------------------- */
//Normal contructor
userPrompt::userPrompt()
{
	if(ref_cnt==0)
	{
		delete prompt;
		prompt = new Prompts;
		if( !prompt )
		{
			report_error("Can't create prompt object. Not enough mem");
			exit(-1);
		}
	}
	ref_cnt++;
}
/* ------------------------------------------------------------- */
//Copy constructor
userPrompt::userPrompt(const userPrompt &up)
{
	ref_cnt++;
	curr_section = up.curr_section;
	curr_group = up.curr_group;
}
/* ------------------------------------------------------------- */
//Copy operator
const userPrompt& userPrompt::operator=( const userPrompt& up )
{
	curr_section = up.curr_section;
	curr_group = up.curr_group;
	ref_cnt++;
	return *this;
}

/* ------------------------------------------------------------- */
userPrompt::~userPrompt()
{
	if(ref_cnt==1)
	{
		delete prompt;
		prompt = NULL;
	}
	ref_cnt--;
}

/* ------------------------------------------------------------- */
void userPrompt::updateLang()
{
	char buf[64];
	int ret = com_verifone_pml::get_env( lang_env, buf, sizeof(buf) );
	if(ret>0)
	{
		buf[ret] = '\0';
		prompt->setCurrentLanguage(buf);
	}
	else
	{
		report_error("Unable to get current language");
	}
	curr_group = get_group();
	if(curr_group!=1)
		set_group(1);
}
/* ------------------------------------------------------------- */
void userPrompt::updateLangFin()
{
	if(get_group()!=curr_group)
	{
		set_group(curr_group);
	}
}
/* ------------------------------------------------------------- */
std::string userPrompt::getPrompt(  const std::string & section, const std::string & key)
{
	updateLang();
	std::string ret = prompt->getPrompt( section, key );
	updateLangFin();
	return ret;
}
/* ------------------------------------------------------------- */
std::string userPrompt::getPrompt(  const std::string & section, const unsigned int  index)
{
	updateLang();
	std::string ret = prompt->getPrompt( section, index );
	updateLangFin();
	return ret;
}
/* ------------------------------------------------------------- */
std::string  userPrompt::getPrompt(const std::string & key)
{
	updateLang();
	std::string ret = prompt->getPrompt( curr_section, key );
	updateLangFin();
	return ret;
}
/* ------------------------------------------------------------- */
std::string  userPrompt::getPrompt(const unsigned int  index)
{
	updateLang();
	std::string ret = prompt->getPrompt( curr_section, index );
	updateLangFin();
	return ret;
}
/* ------------------------------------------------------------- */
} //com_verifone_guiapi

/* ------------------------------------------------------------- */
