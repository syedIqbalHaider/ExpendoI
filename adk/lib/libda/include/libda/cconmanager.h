#ifndef CCONMANAGER_H_
#define CCONMANAGER_H_
#include <string>
#include "sqlite3.h"
#include <vector>
#include <map>
#include <sstream>
#include "cuser.h"
#include "ccommssettings.h"
#include "cversionsfile.h"
#include "cbatchrec.h"
#include "cbatch.h"
#include "cpaninfo.h"

#define SSTR( x ) dynamic_cast< std::ostringstream & >( std::ostringstream() << std::dec << x ).str()

#define DB_FAIL 		0
#define DB_OK   		1
#define DB_NO_ACTION	2	// Used to indicate no update was available

using namespace com_verifone_user;
using namespace com_verifone_commssettings;
using namespace com_verifone_versionsfile;
using namespace com_verifone_batchrec;
using namespace com_verifone_batch;
using namespace com_verifone_paninfo;

namespace com_verifone_conmanager {
	class CConManager {
		public:
			CConManager(std::string path):dbPath(path){sqldb=NULL;}
			CConManager();
				int selectFromDB(std::string sql, std::vector<std::vector<std::string> > &records);
				int selectFromDB(std::string sql, std::vector<std::vector<std::string> > &records,bool closeConnection);
				int writeToDB(std::string sql, bool closeConnection); //insert an update
				int writeToDB(std::string sql);

				int getColumnValue(std::string columName, std::string &value, std::vector<std::vector<std::string> > &records, int rowNum);

				int insertTerminalConfig(std::string name, std::string value);
				int updateTerminalConfig(std::string name, std::string value);

				int insertUser(CUser &user);
				int updateUser(CUser &user);

				int insertBatchRec(CBatchRec &batchRec);
				int insertBatch(CBatch &batch);

				int insertPan(CPanInfo &panInfo);

				int insertFileWithVersion(std::string appName, CVersionsFile file);

				int insertCommsParameters(std::string tableName,CCommsSettings &comsSettings);

				int updateStatus(std::string name, std::string value);
				int insertStatus(std::string name, std::string value);

				int insertAidOverride(std::string aid, std::string cardProductId,std::string accountProfileId,std::string totalsGroup);
				int insertBin(std::string bin,std::string len,std::string cardProfileIndex,std::string issuedName);
				int insertCardProfile(std::string index,std::string active,std::string checkServiceCode,std::string checkExpiryDate,std::string pinLenMin,std::string pinLenMax,std::string checkLuhn,std::string approvalText);
				int insertAttachedAccounts(std::string cardProfileIndex,std::string accountProfileIndex,std::string totalsGroupName);
				int insertAccountProfile(std::string accProfileIndex, std::string skimCardValue,std::string checkCardValue, std::string code,std::string allowVoiceAproval,std::string draftCaptureCode,std::string requireCardPinEntry,std::string emvFallBackIndex,std::string localLimitIndex,std::string OfflineLimitIndex,std::string requireSignature,std::string allowManualEntry,std::string allowBudget,std::string budgetAmtMin,std::string accountProfileHostId,std::string contactlessIndex);
				int insertAccountProfileCurrency2(std::string accProfileIndex, std::string skimCardValue,std::string checkCardValue, std::string code,std::string allowVoiceAproval,std::string draftCaptureCode,std::string requireCardPinEntry,std::string emvFallBackIndex,std::string localLimitIndex,std::string OfflineLimitIndex,std::string requireSignature,std::string allowManualEntry,std::string allowBudget,std::string budgetAmtMin,std::string accountProfileHostId,std::string contactlessIndex);
				int insertAccountDescription(std::string code, std::string description);
				int insertAlowedTransactions(std::string accProfileIndex, std::string code);
				int insertLimits(std::string cashbackAmtMax,std::string floorLimtAmt,std::string txAmtMax ,std::string txAmtMin,std::string limitsIndex);
				int updateLimits(std::string cashbackAmtMax,std::string floorLimtAmt,std::string txAmtMax ,std::string txAmtMin,std::string limitsIndex);
				int insertSuperTotals(std::string totalsGroup,std::string superTotalsGroup);

				int insertPublicKey(std::string rid, std::string publicKeyIndex,std::string hashAlgorithmIndicator,std::string publicKeyAlgorithmIndicator,std::string modulus,std::string exponent,std::string checkSum,std::string activation,std::string expiry);
				int insertAid(std::string aid,std::string emvCode,std::string accountProfileHostId,std::string totalsGroupName,std::string appVersion,std::string ddol,std::string tdol,std::string percentageMax,std::string percentage,std::string amount,std::string tacDefault,std::string tacDecline,std::string tacOnline,std::string cardDataInputMode);

				int commsLog(std::string type, std::string status, std::string timestamp, std::string ip, std::string port, std::string seconds, std::string tx_count, std::string rx_count);

				void close();

				// Below is for low level direct database access
				std::string getDBFirstField(std::string sql, std::string def, bool mustClose = false);
				int getDBRecords(std::string sql, std::vector<std::vector<std::string> > &records, int maxNr, bool mustClose = false);
				sqlite3 * getDbConnection();


			private:
				void close(sqlite3 **db);
				std::string& getDbPath();
				void setDbPath( std::string dbPath);
				sqlite3 *sqldb;
				static int getFirstRecField(void *vRecEnv, int nrColumns, char **argv, char **azColName);
				static int getRecordFields(void *vRecEnv, int nrColumns, char **argv, char **azColName);
				std::string dbPath;
				void toHex(void *const data,const size_t dataLength,std::string &dest);

	};
}
#endif /* CCONMANAGER_H_ */
