#ifndef CVERSIONS_H_
#define CVERSIONS_H_
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <set>
#include <map>
#include <libda/cversionsfile.h>

using namespace com_verifone_versionsfile;

namespace com_verifone_versions {
	class CVersions {
		public:
			CVersions();

			int getApps(std::set<std::string> &apps);
			int getFiles(std::string appName, std::map<int,CVersionsFile> &files);

			int deleteApp(std::string appName);
			int addFile(std::string appName, CVersionsFile file);
			int deleteFile(std::string appName,std::string fileName);

		private:
	};
}
#endif /* CVERSIONS_H_ */
