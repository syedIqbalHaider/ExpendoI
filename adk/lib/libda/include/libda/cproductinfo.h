
#ifndef CPRODUCTINFO_H_
#define CPRODUCTINFO_H_
#include <stdio.h>
#include <stdlib.h>
#include <string>

namespace com_verifone_productinfo {
	class CProductInfo {
		public:
			CProductInfo();
			std::string& getExpiryDateDf33() ;
			void setExpiryDateDf33( std::string expiryDateDf33);
			std::string& getItemattrDf21() ;
			void setItemattrDf21( std::string itemattrDf21);
			std::string& getItemCodeDDf12() ;
			void setItemCodeDDf12( std::string itemCodeDDf12);
			std::string& getItemMessageDf38() ;
			void setItemMessageDf38( std::string itemMessageDf38);
			std::string& getItemTypeDf13() ;
			void setItemTypeDf13( std::string itemTypeDf13);
			std::string& getMerchantNumDf14() ;
			void setMerchantNumDf14( std::string merchantNumDf14);
			std::string& getNetworkNameDf36() ;
			void setNetworkNameDf36( std::string networkNameDf36);
			std::string& getPrepaidIdCodeDf11() ;
			void setPrepaidIdCodeDf11( std::string prepaidIdCodeDf11);
			std::string& getProductNameDf37() ;
			void setProductNameDf37( std::string productNameDf37);
			std::string& getProviderDf22() ;
			void setProviderDf22( std::string providerDf22);
			std::string& getSerialNumberDf23() ;
			void setSerialNumberDf23( std::string serialNumberDf23);
			std::string& getTrackTwoDf15() ;
			void setTrackTwoDf15( std::string trackTwoDf15);
			std::string& getVoucherPinDf31() ;
			void setVoucherPinDf31( std::string voucherPinDf31);
			std::string& getVoucherSerialDf32() ;
			void setVoucherSerialDf32( std::string voucherSerialDf32);

		private:
			std::string prepaidIdCode_DF11;
			std::string itemCodeD_DF12;
			std::string itemType_DF13;
			std::string merchantNum_DF14;
			std::string trackTwo_DF15;
			std::string itemattr_DF21;
			std::string provider_DF22;
			std::string serialNumber_DF23;
			std::string voucherPin_DF31;
			std::string voucherSerial_DF32;
			std::string expiryDate_DF33;
			std::string networkName_DF36;
			std::string productName_DF37;
			std::string itemMessage_DF38;
	};
}

#endif /* CPRODUCTINFO_H_ */
