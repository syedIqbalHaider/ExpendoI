
#ifndef CCARDDATA_H_
#define CCARDDATA_H_
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <map>
#include <libda/ccardprofile.h>
#include <libda/caccountprofile.h>
#include <stdexcept>

using namespace com_verifone_cardprofile;
using namespace com_verifone_accountprofile;

namespace com_verifone_carddata {
	class CCardData {
		public:
			CCardData();

			std::map<std::string, CAccountProfile>& getAccounts() ;
			CAccountProfile & getAccountAt(int index);
			int getAccountsCount();
			void setAccounts(std::map<std::string, CAccountProfile> accounts);
			std::string& getBin() ;
			void setBin( std::string bin);
			std::string& getCardIssuedName() ;
			void setCardIssuedName( std::string cardIssuedName);
			int getCardLength() ;
			void setCardLength(int cardLength);
			CCardProfile& getCardProfile() ;
			void setCardProfile(CCardProfile cardProfile);
			bool isExcluded();
			void setExcluded(bool excluded);
			int getCardProductId();
			void setCardProductId(int cardProductId);

		private:
			std::string bin;
			int cardLength;
			int cardProductId;
			std::string cardIssuedName;
			bool excluded;
			CCardProfile cardProfile;
			std::map<std::string,CAccountProfile> accounts;

	};
}
#endif /* CCARDDATA_H_ */
