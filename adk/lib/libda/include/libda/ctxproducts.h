
#ifndef CTXPRODUCTS_H_
#define CTXPRODUCTS_H_
#include <stdio.h>
#include <stdlib.h>
#include <string>

namespace com_verifone_txproducts {
	class CTxProducts {
		public:
			CTxProducts();
			long getAmount() ;
			void setAmount(long amount);
			std::string& getAttribute() ;
			void setAttribute( std::string attribute);
			std::string& getDescription() ;
			void setDescription( std::string description);
			long getUnitPrice() ;
			void setUnitPrice(long unitPrice);
			long getUnits() ;
			void setUnits(long units);

		private:
			std::string description;
			std::string attribute;
			long unitPrice;
			long units;
			long amount;
	};
}
#endif /* CTXPRODUCTS_H_ */
