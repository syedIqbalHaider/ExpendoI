
#ifndef CLIMITS_H_
#define CLIMITS_H_
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <map>

namespace com_verifone_limits {
	class CLimits {
		public:

			CLimits();
			std::string& getCashbackAmtMax() ;
			void setCashbackAmtMax( std::string cashbackAmtMax);
			std::string& getFloorLimitAmt() ;
			void setFloorLimitAmt( std::string floorLimitAmt);
			std::string& getTransactionAmtMax() ;
			void setTransactionAmtMax( std::string transactionAmtMax);
			std::string& getTransactionAmtMin() ;
			void setTransactionAmtMin( std::string transactionAmtMin);

		private:
			std::string cashbackAmtMax;
			std::string floorLimitAmt;
			std::string transactionAmtMax;
			std::string transactionAmtMin;
	};
}
#endif /* CLIMITS_H_ */
