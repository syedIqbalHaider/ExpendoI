
#ifndef CAID_H_
#define CAID_H_
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include "caccountprofile.h"
#include "cpublickey.h"

using namespace com_verifone_accountprofile;
using namespace com_verifone_publickey;

namespace com_verifone_emvdata {
	class CEmvData {
		public:
			CEmvData();

			CAccountProfile& getAccountProfile() ;
			void setAccountProfile( CAccountProfile& accountProfile);
			std::string& getAid() ;
			void setAid( std::string& aid);
			std::string& getApplicationVersion() ;
			void setApplicationVersion( std::string applicationVersion);
			std::string& getDefaultDdol() ;
			void setDefaultDdol( std::string defaultDdol);
			std::string& getDefaultTdol() ;
			void setDefaultTdol( std::string defaultTdol);
			std::string& getEmvSchemeCode() ;
			void setEmvSchemeCode( std::string emvSchemeCode);
			std::map<std::string, CPublicKey>& getPublicKeys();
			void setPublicKeys(std::map<std::string, CPublicKey> publicKeys);
			std::string& getTacDecline() ;
			void setTacDecline( std::string tacDecline);
			std::string& getTacDefault() ;
			void setTacDefault( std::string tacDefault);
			std::string& getTacOnline() ;
			void setTacOnline( std::string tacOnline);
			std::string& getTargetPercentage() ;
			void setTargetPercentage( std::string targetPercentage);
			std::string& getTargetPercentageMax() ;
			void setTargetPercentageMax( std::string targetPercentageMax);
			std::string& getThresholdAmt() ;
			void setThresholdAmt( std::string thresholdAmt);
			std::string& getTotalsGroupName() ;
			void setTotalsGroupName( std::string totalsGroupName);
			std::string& getSuperTotalsGroupName() ;
			void setSuperTotalsGroupName( std::string superTotalsGroupName);
			std::string& getCardDataInputMode() ;
			void setCardDataInputMode( std::string cardDataInputMode);
			std::string getLatestApplicationVersion();
			void setLatestApplicationVersion( std::string latestApplicationVersion);
			bool getPartialMatchingAllowed();
			void setPartialMatchingAllowed(bool partialMatchingAllowed);

		private:
			std::string aid;
			std::string applicationVersion;
			std::string defaultDdol;
			std::string defaultTdol;
			std::string targetPercentageMax;
			std::string targetPercentage;
			std::string thresholdAmt;
			std::string emvSchemeCode;
			std::string totalsGroupName;
			std::string superTotalsGroupName;
			std::string tacDefault;
			std::string tacDecline;
			std::string tacOnline;
			std::string cardDataInputMode;
			std::string latestApplicationVersion;
			bool partialMatchingAllowed;
			CAccountProfile accountProfile;
			std::map<std::string, CPublicKey> publicKeys;


	};
}
#endif /* CAID_H_ */
