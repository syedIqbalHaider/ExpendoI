#ifndef CVELOCITYCHECK_H_
#define CVELOCITYCHECK_H_
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <string>
#include <map>
#include "libda/cpaninfo.h"

using namespace com_verifone_paninfo;

namespace com_verifone_velocitycheck {
	class CVelocityCheck {
		public:
			CVelocityCheck();

			int checkPan(CPanInfo &panInfo);
			int removePan(std::string pan);
			int updatePan(CPanInfo &panInfo);
			int insertPan(CPanInfo &panInfo);
			int getAllPans(std::map<std::string,CPanInfo> &pans);

		private:

	};
}
#endif /* CVELOCITYCHECK_H_ */
