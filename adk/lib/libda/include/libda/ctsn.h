
#ifndef CTSN_H_
#define CTSN_H_
#include <stdio.h>
#include <stdlib.h>
#include <string>

namespace com_verifone_tsn {
	class CTsn {
		public:
			CTsn();
			int getNextTsn();
			int getCurrentTsn();
			void resetTsn(int tsn);
		private:
	};
}
#endif /* CTSN_H_ */
