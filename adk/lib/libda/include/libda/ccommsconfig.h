
#ifndef CCOMMSCONFIG_H_
#define CCOMMSCONFIG_H_
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include "ccommssettings.h"
#include "himdef.h"

using namespace com_verifone_commssettings;
using namespace com_verifone_himdef;


namespace com_verifone_commsconfig {
	class CCommsConfig {
		public:

			CCommsConfig();

			int importCommsConfig(std::string path, HIM him);

			int getPriAuthCommsSettings(CCommsSettings &commsSettings);
			int getPriSettleCommsSettings(CCommsSettings &commsSettings);
			int getPriParamCommsSettings(CCommsSettings &commsSettings);
			int getPriSoftwareDwlndCommsSettings(CCommsSettings &commsSettings);
			int getSecAuthCommsSettings(CCommsSettings &commsSettings);
			int getSecSettleCommsSettings(CCommsSettings &commsSettings);
			int getSecParamCommsSettings(CCommsSettings &commsSettings);
			int getSecSoftwareDwlndCommsSettings(CCommsSettings &commsSettings);

			int setPriParamCommsSettings(CCommsSettings &commsSettings);
			int setSecParamCommsSettings(CCommsSettings &commsSettings);

			int getVersion(std::string &version);
//				iqbal_audi_240217 make following functions public
			int setPriSettleCommsSettings(CCommsSettings &commsSettings);
			int setSecSettleCommsSettings(CCommsSettings &commsSettings);
			int setPriAuthCommsSettings(CCommsSettings &commsSettings);
			int setSecAuthCommsSettings(CCommsSettings &commsSettings);


		private:
			int poplateCommsParameters(CCommsSettings &commsSettings, std::vector<std::vector<std::string> > &records);
			int processAlienoHimFiles(std::string path);
			int processAlienoHimLine(std::string line);
			int processIsoHimFiles(std::string path);
			int processIsoHimLine(std::string line);

//			int setPriSettleCommsSettings(CCommsSettings &commsSettings);
//			int setSecSettleCommsSettings(CCommsSettings &commsSettings);
//			int setPriAuthCommsSettings(CCommsSettings &commsSettings);
//			int setSecAuthCommsSettings(CCommsSettings &commsSettings);
			int setPriSoftwareCommsSettings(CCommsSettings &commsSettings);
			int setSecSoftwareCommsSettings(CCommsSettings &commsSettings);

	};
}
#endif /* CCOMMSCONFIG_H_ */
