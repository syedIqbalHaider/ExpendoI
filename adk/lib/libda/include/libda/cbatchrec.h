#ifndef CBATCHREC_H_
#define CBATCHREC_H_
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <set>
#include "himdef.h"
#include <libda/conlinestatus.h>
#include <libda/cfraudindicator.h>
#include <libda/cproductinfo.h>
#include <libda/ctxproducts.h>

using namespace com_verifone_onlinestatus;
using namespace com_verifone_fraudindicator;
using namespace com_verifone_productinfo;
using namespace com_verifone_txproducts;

namespace com_verifone_batchrec {

	enum PAN_ENTRY_MODE {
		UNKNOWN=0,
		SWIPED,
		ENTERED,
		INSERTED,
		TAPPED,
		FALLBACK
	};

	enum DRAFT_CAPTURE_MODE {
		ONLINE,
		OFFLINE
	};

	enum AUTH_MODE {
		TERMINAL,
		HOST,
		VOICE,
		DEF_ANALYSIS
	};

	enum CARD_AUTH_METHOD {
		PIN,
		SIGNATURE
	};

	enum CAPTURE_FLAG {
		ALREADY_CAPTURED,
		NOT_CAPTURED,
		COMPLETION_TO_BE_SEND,
		REVERSAL
	};


	class CBatchRec {
		public:
			CBatchRec();
			std::string& getAccProfIndex() ;
			void setAccProfIndex( std::string accProfIndex);
			std::string& getAccType() ;
			void setAccType( std::string accType);
			bool isApproveOnline() ;
			void setApproveOnline(bool approveOnline);
			std::string& getAuthCode() ;
			void setAuthCode( std::string authCode);
			AUTH_MODE getAuthMode() ;
			void setAuthMode(AUTH_MODE authMode);
			unsigned int getBatchDbId() ;
			void setBatchDbId(unsigned int batchNo);
			unsigned int getBatchTxNo() ;
			void setBatchTxNo(unsigned int batchTxNo);
			std::string& getBudgetPeriod() ;
			void setBudgetPeriod( std::string budgetPeriod);
			std::string& getCanceledReceiptNo() ;
			void setCanceledReceiptNo( std::string canceledReceiptNo);
			std::string& getCanceledTxType() ;
			void setCanceledTxType( std::string canceledTxType);
			std::string& getCancelledTsn() ;
			void setCancelledTsn(std::string cancelledTsn);
			CARD_AUTH_METHOD getCardhAuthMethod() ;
			void setCardhAuthMethod(CARD_AUTH_METHOD cardhAuthMethod);
			std::string& getCardholderName() ;
			void setCardholderName( std::string cardholderName);
			long getCashAmount() ;
			void setCashAmount(long cashAmount);
			bool isCashExcludeFlag() ;
			void setCashExcludeFlag(bool cashExcludeFlag);
			std::string& getCashierId() ;
			void setCashierId( std::string cashierId);
			std::string& getCashierName() ;
			void setCashierName( std::string cashierName);
			bool isCashierPinVerified() ;
			void setCashierPinVerified(bool cashierPinVerified);
			std::string& getCompletionCode() ;
			void setCompletionCode( std::string completionCode);
			std::string& getCvv() ;
			void setCvv( std::string cvv);
			bool isDemoTx() ;
			void setDemoTx(bool demoTx);
			DRAFT_CAPTURE_MODE getDraftCapMode() ;
			void setDraftCapMode(DRAFT_CAPTURE_MODE draftCapMode);
			std::string& getEmvTags() ;
			void setEmvTags( std::string emvTags);
			std::string& getExp() ;
			void setExp( std::string exp);
			bool isFleetCard() ;
			void setFleetCard(bool fleetCard);
			std::string& getFleetCardUsage() ;
			void setFleetCardUsage( std::string fleetCardUsage);
			CFraudIndicator& getFraudInd();
			void setFraudInd(CFraudIndicator fraudInd);
			bool isIccFlag() ;
			void setIccFlag(bool iccFlag);
			std::string& getInvoiceNo() ;
			void setInvoiceNo( std::string invoiceNo);
			long getLitres() ;
			void setLitres(long litres);
			std::string& getNonIsoRespCode() ;
			void setNonIsoRespCode( std::string nonIsoRespCode);
			std::string& getOdoReading() ;
			void setOdoReading( std::string odoReading);
			COnlineStatus& getOnlStatus() ;
			void setOnlStatus( COnlineStatus onlStatus);
			std::string& getOrigTxDateTime() ;
			void setOrigTxDateTime( std::string origTxDateTime);
			std::string& getPan() ;
			void setPan(std::string pan);
			PAN_ENTRY_MODE getPanEntryMode() ;
			void setPanEntryMode(PAN_ENTRY_MODE panEntryMode);
			bool isPayRequest() ;
			void setPayRequest(bool payRequest);
			CProductInfo& getProductInfo() ;
			void setProductInfo(CProductInfo productInfo);
			bool isRcsCard() ;
			void setRcsCard(bool rcsCard);
			std::string& getReceiptNo() ;
			void setReceiptNo( std::string receiptNo);
			std::string& getEcrNo();		//iqbal_audi_301216
			void setEcrNo( std::string ecrNo);
			std::string& getRespMsg();		//iqbal_audi_060317
			void setRespMsg(std::string respMsg);
			std::string& getRefNo1() ;
			void setRefNo1( std::string refNo1);
			std::string& getRefNo2() ;
			void setRefNo2( std::string refNo2);
			unsigned int getRepeatTsn() ;
			void setRepeatTsn(unsigned int repeatTsn);
			std::string& getRespCode() ;
			void setRespCode( std::string respCode);
			std::string& getRrn() ;
			void setRrn( std::string rrn);
			bool isSignatureRequired() ;
			void setSignatureRequired(bool signatureRequired);
			std::string& getSpdhSeqNo() ;
			void setSpdhSeqNo( std::string spdhSeqNo);
			std::string& getSpdhTsn() ;
			void setSpdhTsn( std::string spdhTsn);
			std::string& getSrc() ;
			void setSrc( std::string src);
			std::string& getSupervisorId() ;
			void setSupervisorId( std::string supervisorId);
			std::string& getSupervisorName() ;
			void setSupervisorName( std::string supervisorName);
			std::string& getTableNo() ;
			void setTableNo( std::string tableNo);
			bool isTimeoutReversal() ;
			void setTimeoutReversal(bool timeoutReversal);
			long getTipAmount() ;
			void setTipAmount(long tipAmount);
			std::string& getTotalsGroup() ;
			void setTotalsGroup( std::string totalsGroup);
			std::string& getTrack2() ;
			void setTrack2( std::string track2);
			unsigned int getTransmissionNum() ;
			void setTransmissionNum(unsigned int transmissionNum);
			unsigned int getTsn() ;
			void setTsn(unsigned int tsn);
			long getTxAmount() ;
			void setTxAmount(long txAmount);
			bool isTxCanceled() ;
			void setTxCanceled(bool txCanceled);
			bool isTxVoided() ;
			void setTxVoided(bool txVoided);
			std::string& getTxCode() ;
			void setTxCode( std::string txCode);
			std::string& getTxDateTime() ;
			void setTxDateTime( std::string txDateTime);
			bool isTxForceOffline() ;
			void setTxForceOffline(bool txForceOffline);
			bool isTxForceOnline() ;
			void setTxForceOnline(bool txForceOnline);
			std::string& getTxType() ;
			void setTxType( std::string txType);
			std::string& getWaiterId() ;
			void setWaiterId( std::string waiterId);
			CAPTURE_FLAG getCaptureFlag();
			void setCaptureFlag(CAPTURE_FLAG captureFlag);
			bool isInProgress();
			void setInProgress(bool inProgress);
			long getCardFees();
			void setCardFees(long cardFees);
			long getLoadFees();
			void setLoadFees(long loadFees);
			bool isApproved();
			void setApproved(bool approved);
			bool isDeclined();
			void setDeclined(bool declined);
			std::string getReason();
			void setReason(std::string reason);
			std::string getTransCurrency();
			void setTransCurrency(std::string currency);

		private:
			std::string supervisorId;
			std::string supervisorName;
			std::string cashierId;
			std::string cashierName;
			bool cashierPinVerified;
			std::string waiterId;
			std::string tableNo;
			std::string src;
			bool iccFlag;
			std::string txDateTime;
			std::string origTxDateTime;
			PAN_ENTRY_MODE panEntryMode;            // Manual or swiped
			AUTH_MODE authMode;                     // Online or offline
			CARD_AUTH_METHOD cardhAuthMethod;       // Pin, signature
			bool signatureRequired;
			bool txForceOnline;
			bool txForceOffline;
			DRAFT_CAPTURE_MODE draftCapMode;                // 0-online ; 1-offline
			bool txCanceled;                       // Flag indicating that the transaction was canceled
			bool txVoided;
			bool approveOnline;                // Will only be set when tx is not approved offline
			bool timeoutReversal;              // Flag indicating that this is a timeout reversal
			CAPTURE_FLAG captureFlag;
			std::string completionCode;
			bool cashExcludeFlag;
			std::string accType;
			std::string txType;
			std::string canceledTxType;              // Transaction type that was canceled
			std::string txCode;
			std::string budgetPeriod;
			std::string  rrn;
			std::string authCode;
			std::string respCode;
			std::string nonIsoRespCode;
			std::string cardholderName;
			std::string track2;
			std::string accProfIndex;
			std::string pan;
			std::string exp;
			std::string CVV;
			std::string totalsGroup;
			std::string invoiceNo;
			std::string refNo1;
			std::string refNo2;
			bool rcsCard;                      // 1 - Identifies this transaction as RCS
			bool fleetCard;                    // 1 - Identifies this transaction as Fleet
			unsigned int tsn;
			unsigned int repeatTsn;
			unsigned int transmissionNum;
			std::string spdhTsn;
			std::string spdhSeqNo;
			std::string cancelledTsn;
			std::string receiptNo;
			std::string ecrNo;	//iqbal_audi_301216
			std::string respMsg; //iqbal_audi_060317
			std::string canceledReceiptNo;
			unsigned int batchNo;
			unsigned int batchTxNo;
			long txAmount;
			long cashAmount;
			long tipAmount;
			long litres;
			COnlineStatus onlStatus;          // Bitmask containing different reasons to go online
			CFraudIndicator fraudInd;         // Indicate a possible fraudulant card
			CProductInfo productInfo;          // Hold the information for a product/prepaid voucher
			std::string  emvTags;     // emv tlv, 0x00 terminated, changed to 1000 for iso which supports LLLVAR in DE55 (999 plus null terminator)
			std::string odoReading;
			std::string fleetCardUsage;
			bool demoTx;                   // When this flag is 1 it indicates that this is a demo transaction that should NEVER reach the bank!!!
			bool payRequest;               // This flag indicates that a transaction was initiated by a third application, typically a VAS, to request payment
			bool inProgress;
			long cardFees;
			long loadFees;
			bool approved;
			bool declined;
			std::string reason;
			std::string transCurrency;

	};
}

#endif /* CBATCHREC_H_ */
