
#ifndef CRECEIPTNO_H_
#define CRECEIPTNO_H_
#include <stdio.h>
#include <stdlib.h>
#include <string>

namespace com_verifone_receiptno {
	class CReceiptNo {
		public:
			CReceiptNo();
			int getNextReceiptNo();
			int getCurrentReceiptNo();
			void resetReceiptNo(int receiptNo);
		private:
	};

	//iqbal_audi_301216
	class CEcrNumber {
			public:
				CEcrNumber();
				int getEcrNo();
				void setEcrNo(int ecrNo);
			private:
		};
}


#endif /* CRECEIPTNO_H_ */
