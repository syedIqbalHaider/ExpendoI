
#ifndef CCARDPROFILE_H_
#define CCARDPROFILE_H_
#include <stdio.h>
#include <stdlib.h>
#include <string>


namespace com_verifone_cardprofile {
	class CCardProfile {
		public:
			CCardProfile();

			bool isActive() ;
			void setActive(bool active);
			bool isCheckExpiryDate() ;
			void setCheckExpiryDate(bool checkExpiryDate);
			bool isCheckLuhn() ;
			void setCheckLuhn(bool checkLuhn);
			bool isCheckServiceCode() ;
			void setCheckServiceCode(bool checkServiceCode);
			bool isForeignCard() ;
			void setForeignCard(bool foreignCard);
			int getPinLenMax() ;
			void setPinLenMax(int pinLenMax);
			int getPinLenMin() ;
			void setPinLenMin(int pinLenMin);
			std::string& getVoiceApprovalText() ;
			void setVoiceApprovalText( std::string voiceApprovalText);
			int getIndex();
			void setIndex(int index);

		private:
			bool active;
			bool foreignCard;
			bool checkServiceCode;
			bool checkExpiryDate;
			bool checkLuhn;
			int pinLenMin;
			int pinLenMax;
			std::string voiceApprovalText;
			int index;

	};
}
#endif /* CCARDPROFILE_H_ */
