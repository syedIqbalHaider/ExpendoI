
#ifndef CHOTCARDS_H_
#define CHOTCARDS_H_
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include "himdef.h"

using namespace com_verifone_himdef;

namespace com_verifone_hotcard {
	class CHotcard {
		public:
			CHotcard();

			int isHotCard(std::string pan);
			int deleteHotCard(std::string pan);
			int addHotCard(std::string pan);

			int importHotcards(std::string path, HIM him, std::string hcFileName);
			int getRecordCount();
			int getVersion(std::string &version);

		private:
			int processAlienoHotcardFile(std::string path, std::string hcFileName);
			int processIsoHotcardFile(std::string path, std::string hcFileName);

			int processPan(std::string bcdPan, bool add);

			bool bcdToAscii(const unsigned char* bcdPan, const int length, std::string& asciiPan);
	};
}
#endif /* CHOTCARDS_H_ */
