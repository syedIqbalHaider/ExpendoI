#ifndef CCARDCONFIG_H_
#define CCARDCONFIG_H_
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <set>
#include "ccarddata.h"
#include "cemvdata.h"
#include "himdef.h"
#include "cpublickey.h"

using namespace com_verifone_carddata;
using namespace com_verifone_emvdata;
using namespace com_verifone_himdef;
using namespace com_verifone_publickey;


namespace com_verifone_cardconfig {
	class CCardConfig {
		public:
			CCardConfig();

			int lookupCardConfig(std::string pan, CCardData &cardData, std::string currency);
			int lookupEmvConfig(std::string aid, CEmvData &emvData, std::string currency);
			int lookupAidOverride(std::string bin, std::string aid, CEmvData &emvData, std::string currency);

			int importCardConfig(std::string path, HIM him);
			int importEmvConfig(std::string path, HIM him);

			std::set<std::string> getAids();
			std::map<int,CPublicKey> getCapks();

			int getVersion(std::string &version);

		private:
			std::string insertString;

			int populateCardProfile(int cardProductId,CCardData &cardData);
			int populateAccounts(int cardProductId,std::map<std::string, CAccountProfile> &accounts);
			int populateAccountsCurrency2(int cardProductId,std::map<std::string, CAccountProfile> &accounts);
			int populateEmvAccountProfile(int accountProfileHostId,CAccountProfile &accountProfile);
			int populateEmvAccountProfileCurrency2(int accountProfileHostId,CAccountProfile &accountProfile);
			int poplateLimits(int limitsId, CLimits &limits);
			int populateSuperTotalsGroup(std::string totalsGroupName,std::string &supertotalsGroupName);
			int populateAllowedTransactions(int accountProfileId,std::set<std::string> &allowedtransactions);
			int populatePublicKey(std::string rid, std::map<std::string, CPublicKey> &publicKeys);
			int populateAccountDescription(std::string code, std::string &description);

			int processAlienoHimCardFiles(std::string path);
			int processAlienoHimEmvFiles(std::string path);
			int processAlienoBinFile(std::string path);
			int processAlienoBinLine(std::string line);
			int processAlienoCardProfileFile(std::string path);
			int processAlienoCardProfileLine(std::string line);
			int processAlienoForeignCard(std::string path);
			int processAlienoForeignCardLine(std::string line);
			int processAlienoAttachedAccountsFile(std::string path);
			int processAlienoAttachedAccountsLine(std::string line);
			int processAlienoAccountProfileFile(std::string path);
			int processAlienoAccountProfileLine(std::string line);
			int processAlienoAccountDescriptionFile(std::string path);
			int processAlienoAccountDescriptionLine(std::string line);
			int processAlienoAllowedTransactionsFile(std::string path);
			int processAlienoAllowedTransactionsLine(std::string line);
			int processAlienoLimitsFile(std::string path);
			int processAlienoLimitsLine(std::string line);
			int processAlienoSuperTotalsFile(std::string path);
			int processAlienoSuperTotalsLine(std::string line);
			int processAlienoPublicKeyFile(std::string path);
			int processAlienoPublicKeyLine(std::string line);
			int processAlienoAidFile(std::string path);
			int processAlienoAidLine(std::string line);

			int processIsoHimCardFiles(std::string path);
			int processIsoHimEmvFiles(std::string path);
			int processIsoBinFile(std::string path);
			int processIsoBinLine(std::string line,std::string &sql, bool first);
			int processIsoCardProfileFile(std::string path);
			int processIsoCardProfileLine(std::string line,std::string &sql, bool first);
			int processIsoCardProductsFile(std::string path);
			int processIsoCardProductsLine(std::string line,std::string &sql, bool first);
			int processIsoAttachedAccountsFile(std::string path);
			int processIsoAttachedAccountsLine(std::string line,std::string &sql, bool first);
			int processIsoAccountProfileFile(std::string path);
			int processIsoAccountProfileLine(std::string line,std::string &sql, bool first);
			int processIsoAccountDescriptionFile(std::string path);
			int processIsoAccountDescriptionLine(std::string line,std::string &sql, bool first);
			int processIsoAllowedTransactionsFile(std::string path);
			int processIsoAllowedTransactionsLine(std::string line,std::string &sql, bool first);
			int processIsoLimitsFile(std::string path);
			int processIsoLimitsLine(std::string line,std::string &sql, bool first);
			int processIsoSuperTotalsFile(std::string path);
			int processIsoSuperTotalsLine(std::string line);
			int processIsoPublicKeyFile(std::string path);
			int processIsoPublicKeyLine(std::string line);
			int processIsoAidOverrideFile(std::string path);
			int processIsoAidOverrideLine(std::string line);
			int processIsoAidFile(std::string path);
			int processIsoAidLine(std::string line,std::string &sql, bool first);
			std::string &rtrim(std::string &s);

	};
}
#endif /* CCARDCONFIG_H_ */
