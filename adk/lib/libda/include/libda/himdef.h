
#ifndef HIMDEF_H_
#define HIMDEF_H_

#define DB_COMMSLOG_PATH "/home/usr1/flash/CommsLog.sqlite"
#define DB_PATH "/home/usr1/flash/ExpendoI.sqlite"
#define DB_PATH_ORG "/home/usr1/ExpendoI.sqlite.master"
#define TERM_PARM_PATH "/home/usr1/flash"
#define COMMS_PARM_PATH "/home/usr1/flash"
#define CARD_PARM_PATH "/home/usr1/flash"
#define EMV_PARM_PATH "/home/usr1/flash"
#define HOTCARD_PATH "/home/usr1/flash"

#define ALIENO_COMMS_FILE_NAME "COMMS.DAT"
#define ALIENO_BIN_FILE_NAME "cp_bin.dat"
#define ALIENO_CARD_PROFILE_FILE_NAME "cp_card_prof.dat"
#define ALIENO_TERMINAL_FILE_NAME "TERMPARM.DAT"
#define ALIENO_FOREIGN_CARD_FILE_NAME "cp_card.dat"
#define ALIENO_ATTACHED_ACC_FILE_NAME "cp_att_acc.dat"
#define ALIENO_ALLOWED_TX_FILE_NAME "cp_all_tx.dat"
#define ALIENO_ACC_DESCR_FILE_NAME "cp_acc_descr.dat"
#define ALIENO_ACC_PROFILE_FILE_NAME "cp_acc_prof.dat"
#define ALIENO_LIMITS_FILE_NAME "cp_limits.dat"
#define ALIENO_SUPER_TOTALS_FILE_NAME "cp_sup_totals.dat"
#define ALIENO_AID_FILE_NAME "cp_aid.dat"
#define ALIENO_PUBLIC_KEY_FILE_NAME "cp_pub_key.dat"
#define ALIENO_HOTCARD_FILE_NAME "hotcards.dat"


#define ISO_COMMS_FILE_NAME "CM.dat"
#define ISO_BIN_FILE_NAME "CP_bin.dat"
#define ISO_CARD_PROFILE_FILE_NAME "CP_crd_prf.dat"
#define ISO_TERMINAL_FILE_NAME "TP.dat"
#define ISO_FOREIGN_CARD_FILE_NAME "CP_crd_prd.dat"
#define ISO_ATTACHED_ACC_FILE_NAME "CP_att_acc.dat"
#define ISO_ALLOWED_TX_FILE_NAME "CP_all_tra.dat"
#define ISO_ACC_DESCR_FILE_NAME "CP_acc_dsc.dat"
#define ISO_ACC_PROFILE_FILE_NAME "CP_acc_prf.dat"
#define ISO_LIMITS_FILE_NAME "CP_lim_tbl.dat"
#define ISO_SUPER_TOTALS_FILE_NAME "SG.dat"
#define ISO_AID_FILE_NAME "CP_aid_prm.dat"
#define ISO_PUBLIC_KEY_FILE_NAME "CP_pub_key.dat"
#define ISO_HOTCARD_FILE_NAME "hotcards.dat"
#define ISO_MESSAGE_TO_MERCHANT_FILE_NAME "MM.dat"
#define ISO_APPLICATION_BITMAP_FILE_NAME "AB.dat"
#define ISO_CUSTOM_PARAMETERS_FILE_NAME "CT.dat"
#define ISO_AID_OVERRIDE_FILE_NAME "CP_aid_over.dat"

#define VERSIONS_PARM_APP_NAME "ParmVersions"
#define VERSIONS_PARM_TERM_FILE_NAME "TermConfig"
#define VERSIONS_PARM_CARD_FILE_NAME "CardConfig"
#define VERSIONS_PARM_COMMS_FILE_NAME "CommsConfig"
#define VERSIONS_PARM_HOTC_FILE_NAME "Hotcards"

#define MAX_PROD_COUNT 4

namespace com_verifone_himdef
{
	enum HIM {
		ALIENO,
		SPDH,
		ISO
	};
}
#endif /* HIMDEF_H_ */
