
#ifndef CBATCHMANAGER_H_
#define CBATCHMANAGER_H_
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <set>
#include "himdef.h"
#include <libda/cbatch.h>

using namespace com_verifone_batch;

namespace com_verifone_batchmanager {
	class CBatchManager {
		public:
			CBatchManager();

			int getBatch(CBatch &batch);
			int getPreviousBatch(CBatch &batch);
			int resetBatchNo(int batchNo);
			int closeBatch();

			int getNoRecordsInBatch();
			int getNoRecordsInPreviousBatch();

		private:
			std::string getCurrentDateTime();

			int updateBatchRec(int prevDbBatchId, int newDbBatchId);
	};
}
#endif /* CBATCHMANAGER_H_ */
