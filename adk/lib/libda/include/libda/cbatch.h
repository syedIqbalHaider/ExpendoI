#ifndef CBATCH_H_
#define CBATCH_H_
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <map>
#include "himdef.h"
#include <libda/cbatchrec.h>


using namespace com_verifone_batchrec;

namespace com_verifone_batch {

	enum BATCH_STATE {
		OPEN,
		CLOSED
	};

	class CBatch {
		public:
			CBatch();
			int getBatchNo() ;
			void setBatchNo(int batchNo);
			std::string& getDateTimeClosed() ;
			void setDateTimeClosed( std::string dateTimeClosed); // does not persist to db
			std::string& getDateTimeOpened() ;
			void setDateTimeOpened( std::string dateTimeOpened);  // does not persist to db
			BATCH_STATE getState() ;
			void setState(BATCH_STATE state);  // does not persist to db

			int writeRecToBatch(CBatchRec &batchRec); // persist batch rec to database
			int delLastRecFromBatchIfIncomplete();	// Deletes last record if in_progress == 1
			int readLastRecFromBatch(CBatchRec &batchRec); // retrieves last batch_rec added to database
			int readRecFromBatch(CBatchRec &batchRec, int tsn); // retrieves batch_rec by tsn

			int getRecords(std::map<unsigned int, CBatchRec> &batchRecs);
			int getRecordCount();
			int getBatchDbId();
			void setBatchDbId(int batchDbId);

		private:
			int batchNo;
			int batchDbId;
			std::string dateTimeOpened;
			std::string dateTimeClosed;
			BATCH_STATE state;

			std::string getCurrentDateTime();
			void fromHex(
					const std::string &in,              //!< Input hex string
					void *const data                    //!< Data store
					);
	};
}
#endif /* CBATCH_H_ */
