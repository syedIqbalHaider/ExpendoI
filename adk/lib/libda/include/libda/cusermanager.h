#ifndef CUSERMANAGER_H_
#define CUSERMANAGER_H_
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <map>
#include <set>
#include "cuser.h"
#include "cconmanager.h"

using namespace com_verifone_user;

namespace com_verifone_usermanager{
	class CUserManager {
		public:
			CUserManager();
			int addUser(CUser &user);
			int updateUser(CUser &user);

			int getUserByName(std::string name,CUser &user);
			int getUserById(std::string id,CUser &user);

			int deleteUserByName(std::string name);
			int deleteUserById(std::string id);

			bool verifyPinById(std::string id, std::string pin);
			bool verifyPinByname(std::string name, std::string pin);

			int getAllUsers(std::map<std::string,CUser> &users);
			int getAllSupervisors(std::map<std::string,CUser> &users);
			int getAllManagers(std::map<std::string,CUser> &users);
			int getAllCashiers(std::map<std::string,CUser> &users);

			std::string getCurrentDateTime();

		private:
	};
}
#endif /* CUSERMANAGER_H_ */
