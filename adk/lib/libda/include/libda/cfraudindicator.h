
#ifndef CFRAUDINDICATOR_H_
#define CFRAUDINDICATOR_H_

namespace com_verifone_fraudindicator {

	class CFraudIndicator {
		public:
			CFraudIndicator();
			bool isHotcard() ;
			void setHotcard(bool hotcard);
			bool isSkimmedCard();
			void setSkimmedCard(bool skimmedCard);

		private:
			bool skimmedCard;
			bool hotcard;
	};
}
#endif /* CFRAUDINDICATOR_H_ */
