
#ifndef CONLINESTATUS_H_
#define CONLINESTATUS_H_

namespace com_verifone_onlinestatus {
	class COnlineStatus {
		public:
			COnlineStatus();
			bool isEmvFallbackToMag() ;
			void setEmvFallbackToMag(bool emvFallbackToMag);
			bool isFloorLimit() ;
			void setFloorLimit(bool floorLimit);
			bool isManualEntry() ;
			void setManualEntry(bool manualEntry);
			bool isNotHotcard() ;
			void setNotHotcard(bool notHotcard);
			bool isSrcCode() ;
			void setSrcCode(bool srcCode);
			bool isVelocity() ;
			void setVelocity(bool velocity);

		private:
			bool velocity;
			bool floorLimit;
			bool manualEntry;
			bool notHotcard;
			bool srcCode;
			bool emvFallbackToMag;
	};
}
#endif /* CONLINESTATUS_H_ */
