#ifndef CCOMMSSETTINGS_H_
#define CCOMMSSETTINGS_H_
#include <stdio.h>
#include <stdlib.h>
#include <string>


namespace com_verifone_commssettings {
	class CCommsSettings {
		public:

			enum COMMS_TYPE {
				ETHERNET,  // 0
				DIRECTDIAL,// 1
				SPEEDLINK, // 2
				QUICKCONNECT, //3
				RADIOPAD,  // 4
				GPRS,      // 5
				FNBDIRECT, // 6
			};

			CCommsSettings();

			std::string& getIp() ;
			void setIp( std::string ip);
			std::string& getPort() ;
			void setPort( std::string port);
			std::string& getIp2() ;
			void setIp2( std::string ip);
			std::string& getPort2() ;
			void setPort2( std::string port);
			bool isUseSsl() ;
			void setUseSsl(bool useSsl);
			std::string& getDte() ;
			void setDte( std::string dte);
			std::string& getNui() ;
			void setNui( std::string nui);
			std::string& getPhoneNumber() ;
			void setPhoneNumber( std::string phoneNumber);
			COMMS_TYPE getCommsType() ;
			void setCommsType(COMMS_TYPE commsType);

		private:
			std::string ip;
			std::string port;
			std::string ip2;
			std::string port2;
			bool useSSL;
			std::string dte;
			std::string phoneNumber;
			std::string nui;
			COMMS_TYPE commsType;
	};
}
#endif /* CCOMMSSETTINGS_H_ */
