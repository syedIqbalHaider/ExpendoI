#ifndef CPANINFO_H_
#define CPANINFO_H_
#include <stdio.h>
#include <stdlib.h>
#include <string>

namespace com_verifone_paninfo {
	class CPanInfo {
		public:
			CPanInfo();

			int getCounter() ;
			void setCounter(int counter);
			long getLastUsedTimestamp() ;
			void setLastUsedTimestamp(long lastUsedTimestamp);
			std::string& getPan() ;
			void setPan( std::string pan);
			long getTimeStamp() ;
			void setTimeStamp(long timeStamp);

		private:
			std::string pan;
			int counter;
			long timeStamp;
			long lastUsedTimestamp;
	};
}
#endif /* CPANINFO_H_ */
