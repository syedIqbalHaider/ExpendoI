
#ifndef CUSER_H_
#define CUSER_H_
#include <stdio.h>
#include <stdlib.h>
#include <string>


namespace com_verifone_user{
	class CUser {
		public:
			enum USER_TYPE {
				CASHIER,
				MANAGER,
				SUPERVISOR
			};
			CUser();
			bool isBankingAllowed() ;
			void setBankingAllowed(bool bankingAllowed);
			std::string& getDateAdded() ;
			void setDateAdded( std::string dateAdded);
			std::string& getId() ;
			void setId( std::string id);
			std::string& getName() ;
			void setName( std::string name);
			std::string& getPin() ;
			void setPin( std::string pin);
			bool isPinRequired() ;
			void setPinRequired(bool pinRequired);
			bool isRefundsAllowed() ;
			void setRefundsAllowed(bool refundsAllowed);
			USER_TYPE getUserType() ;
			void setUserType(USER_TYPE userType);

		private:
			std::string name;
			std::string id;
			std::string pin;
			std::string dateAdded;
			bool pinRequired;
			bool bankingAllowed;
			bool refundsAllowed;
			USER_TYPE userType;
	};
}
#endif /* USER_H_ */
