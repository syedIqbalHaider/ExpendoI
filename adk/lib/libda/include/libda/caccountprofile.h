
#ifndef CACCOUNTPROFILE_H_
#define CACCOUNTPROFILE_H_
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <set>
#include <libda/climits.h>

using namespace com_verifone_limits;

namespace com_verifone_accountprofile {
	class CAccountProfile {
		public:
			CAccountProfile();

			bool isAllowBudget() ;
			void setAllowBudget(bool allowBudget);
			std::set<std::string>& getAllowedTransactions() ;
			void setAllowedTransactions(
			std::set<std::string>& allowedTransactions);
			bool isAllowManualEntry() ;
			void setAllowManualEntry(bool allowManualEntry);
			bool isAllowVoiceApproval() ;
			void setAllowVoiceApproval(bool allowVoiceApproval);
			std::string& getBudgetAmountMin() ;
			void setBudgetAmountMin( std::string budgetAmountMin);
			bool isCheckCardValue() ;
			void setCheckCardValue(bool checkCardValue);
			std::string& getCode() ;
			void setCode( std::string& code);
			CLimits& getContactLessLimits() ;
			void setContactLessLimits( CLimits contactLessLimits);
			std::string& getDescription() ;
			void setDescription( std::string description);
			bool isDraftCaptureMode() ;
			void setDraftCaptureMode(bool draftCaptureMode);
			CLimits& getEmvFallBackLimits() ;
			void setEmvFallBackLimits( CLimits emvFallBackLimits);
			CLimits& getLocalLimits() ;
			void setLocalLimits( CLimits localLimits);
			CLimits& getOfflineLimits() ;
			void setOfflineLimits( CLimits offlineLimits);
			bool isRequirePinEntry() ;
			void setRequirePinEntry(bool requirePinEntry);
			bool isRequireSignature() ;
			void setRequireSignature(bool requireSignature);
			bool isSkimCardValue() ;
			void setSkimCardValue(bool skimCardValue);
			std::string& getSuperTotalsGroupName() ;
			void setSuperTotalsGroupName( std::string superTotalsGroupName);
			std::string& getTotalsGroupName() ;
			void setTotalsGroupName( std::string totalsGroupName);

		private:

			std::string code;
			std::string description;
			bool draftCaptureMode;
			bool allowVoiceApproval;
			bool allowManualEntry;
			bool requirePinEntry;
			bool requireSignature;
			bool checkCardValue;
			bool skimCardValue;
			bool allowBudget;
			std::string budgetAmountMin;
			CLimits emvFallBackLimits;
			CLimits localLimits;
			CLimits offlineLimits;
			CLimits contactLessLimits;
			std::set<std::string> allowedTransactions;
			std::string totalsGroupName;
			std::string superTotalsGroupName;
	};
}
#endif /* CACCOUNTPROFILE_H_ */
