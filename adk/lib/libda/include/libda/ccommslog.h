#ifndef _COMMSLOG_H_
#define _COMMSLOG_H_

namespace com_verifone_commslog{

//using namespace std;

using std::stringstream;
using std::string;

	class CCommsLog{
		public:
			typedef enum{
				CT_AUTH,
				CT_PARAM,
				CT_SOFT
			}CommsType;
			
			typedef enum{
				CS_DISCONNECT,
				CS_TIMEOUT,
				CS_NO_CONNECT
			}CommsStatus;
			
			CCommsLog(){};
			
			static int Log(CommsType type, CommsStatus status, string timestamp, string ip, string port, unsigned int seconds=0, unsigned int tx_count=0, unsigned int rx_count=0);
			
			int getLog();
		private:
	};
}
#endif // _COMMSLOG_H_
