#ifndef CTERMINALCONFIG_H_
#define CTERMINALCONFIG_H_
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include "himdef.h"

using namespace com_verifone_himdef;

namespace com_verifone_terminalconfig {
	class CTerminalConfig {
		public:

			CTerminalConfig();

			int importTerminalConfig(std::string path, HIM him);
			bool isAllowChequeVerification() ;
			void setAllowChequeVerification(bool allowChequeVerification);
			bool isAllowManualBanking() ;
			void setAllowManualBanking(bool allowManualBanking);
			bool isAllowmanualPan() ;
			void setAllowmanualPan(bool allowmanualPan);
			bool isAttendantsEnabled() ;
			void setAttendantsEnabled(bool attendantsEnabled);
			int getBatchMax() ;
			void setBatchMax(int batchMax);
			bool isBlindDial() ;
			void setBlindDial(bool blindDial);
			int getConnectTimeout() ;
			void setConnectTimeout(int connectTimeout);
			std::string getCurrencyCode() ;
			void setCurrencyCode( std::string currencyCode);
			std::string getCurrencySymbol() ;
			void setCurrencySymbol( std::string currencySymbol);
			std::string getDailType() ;
			void setDailType( std::string dailType);
			std::string getLogicalTermId() ;
			void setLogicalTermId( std::string logicalTermId);
			int getMaxSettleConnectionAttempts() ;
			void setMaxSettleConnectionAttempts(int maxSettleConnectionAttempts);
			int getMaxParmConnectionAttempts() ;
			void setMaxParmConnectionAttempts(int maxParmonnectionAttempts);
			std::string getMenuType() ;
			void setMenuType( std::string menuType);
			std::string getMerchantCat() ;
			void setMerchantCat( std::string merchantCat);
			std::string getMerchantName() ;
			void setMerchantName( std::string merchantName);
			std::string getMerchantNo() ;
			void setMerchantNo( std::string merchantNo);
			long getOilMax() ;
			void setOilMax(long oilMax);
			std::string getParamDnldTime() ;
			void setParamDnldTime( std::string paramDnldTime);
			int getResponseTimeout() ;
			void setResponseTimeout(int responseTimeout);
			std::string getSessionKey() ;
			void setSessionKey( std::string sessionKey);
			std::string getSettlementTime() ;
			void setSettlementTime( std::string settlementTime);
			std::string getSpdhTerminalId() ;
			void setSpdhTerminalId( std::string spdhTerminalId);
			std::string getSwitchBoardCode() ;
			void setSwitchBoardCode( std::string switchBoardCode);
			std::string getTerminalName() ;
			void setTerminalName( std::string terminalName);
			std::string getTerminalNo() ;
			void setTerminalNo( std::string terminalNo);
			std::string getTrailerText() ;
			void setTrailerText( std::string trailerText);
			bool isUseSwitchBoard() ;
			void setUseSwitchBoard(bool useSwitchBoard);
			int getVelocityCnt() ;
			void setVelocityCnt(int velocityCnt);
			int getVelocityPeriod() ;
			void setVelocityPeriod(int velocityPeriod);
			bool isWaitersEnabled() ;
			void setWaitersEnabled(bool waitersEnabled);
			bool isSetupComplete() ;
			void setSetupComplete(bool setupComplete);
			int getWaitTime() ;
			void setWaitTime(int waitTime);
			int getTerminalSeqNumber();
			void setTerminalSeqNumber(int seqNumber);
			std::string getBusinessCode();
			void setBusinessCode(std::string businessCode);
			void setNUI(std::string nui);
			std::string getNUI();
			void setMessageToMerchant(std::string message);
			std::string getMessageToMerchant();
			std::string getEmvCapabilityBitMap();
			void setEmvCapabilityBitMap(std::string emvCapabilityBitmap);
			std::string getTerminalFunctionBitMap();
			void setTerminalFunctionBitMap(std::string terminalFunctionBitMap);
			std::string getTerminalMenuBitMap();
			void setTerminalMenuBitMap(std::string terminalMenuBitMap);
			std::string getReverselTsn();
			void setReverselTsn(int tsn);
			std::string getCustomParameters();
			void setCustomParameters(std::string customParameters);

			//iqbal_audi_291116
			std::string getTpdu() ;
			void setTpdu( std::string tpdu);
			std::string getF49Enable() ;
			void setF49Enable( std::string isEnable);
			std::string getSSLEnable() ;
			void setSSLEnable( std::string isEnable);
			std::string getIsHexHeader() ;
			void setIsHexHeader( std::string isEnable);
			std::string getTpk() ;
			void setTpk( std::string tpk);
			std::string getIsfallbackAllowed();
			void setIsfallbackAllowed( std::string isEnable);
			std::string getEmvKernelVersion();
			void setEmvKernelVersion( std::string emv_kernel_ver);

			std::string getSdkVersion();
			std::string getAdkVersion();

			std::string getAppVersion();


			int getVersion(std::string &version);

		private:

			int getTerminalConfigValue(std::string name,std::string& value);
			void setTerminalConfigValue(std::string name,std::string value);

			int processAlienoHimTerminalFiles(std::string path);
			int processAlienoTerminalFile(std::string path);
			int processAlienoTerminalLine(std::string line);

			int processIsoHimTerminalFiles(std::string path);
			int processIsoTerminalFile(std::string path);
			int processIsoTerminalLine(std::string line);

			int processMessageToMerchant(std::string message);
			int processApplicationBitMap(std::string message);
			int processCustomParameters(std::string message);
	};
}
#endif /* CTERMINALCONFIG_H_ */

