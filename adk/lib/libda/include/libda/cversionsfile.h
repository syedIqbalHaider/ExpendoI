
#ifndef CVERSIONSFILE_H_
#define CVERSIONSFILE_H_
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <set>
#include <map>

namespace com_verifone_versionsfile {
	class CVersionsFile {
		public:
			CVersionsFile();
			std::string& getName() ;
			void setName( std::string name);
			std::string& getPath() ;
			void setPath( std::string path);
			std::string& getVersion() ;
			void setVersion( std::string version);

		private:
			std::string name;
			std::string path;
			std::string version;
	};
}
#endif /* CVERSIONSFILE_H_ */
