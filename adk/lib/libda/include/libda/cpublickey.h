
#ifndef CPUBLICKEY_H_
#define CPUBLICKEY_H_
#include <stdio.h>
#include <stdlib.h>
#include <string>

namespace com_verifone_publickey {
	class CPublicKey {
		public:
			CPublicKey();

			std::string& getActivation() ;
			void setActivation( std::string activation);
			std::string& getCheckSum() ;
			void setCheckSum( std::string checkSum);
			std::string& getExpiry() ;
			void setExpiry( std::string expiry);
			std::string& getExponent() ;
			void setExponent( std::string exponent);
			std::string& getHashAlgorithIndicator() ;
			void setHashAlgorithIndicator( std::string hashAlgorithIndicator);
			std::string& getKeyAlgorithmIndicator() ;
			void setKeyAlgorithmIndicator( std::string keyAlgorithmIndicator);
			std::string& getModulus() ;
			void setModulus( std::string modulus);
			std::string& getPublicKeyIndex() ;
			void setPublicKeyIndex( std::string publicKeyIndex);
			std::string& getRid() ;
			void setRid( std::string rid);

		private:
			std::string rid;
			std::string publicKeyIndex;
			std::string hashAlgorithIndicator;
			std::string modulus;
			std::string exponent;
			std::string checkSum;
			std::string activation;
			std::string expiry;
			std::string keyAlgorithmIndicator;
	};
}
#endif /* CPUBLICKEY_H_ */
