#include <stdio.h>
#include <stdlib.h>
#include "libda/conlinestatus.h"


namespace com_verifone_onlinestatus
{
	COnlineStatus::COnlineStatus():emvFallbackToMag(false),floorLimit(false),manualEntry(false),notHotcard(false),srcCode(false),velocity(false){}

	bool COnlineStatus::isEmvFallbackToMag()  {
		return emvFallbackToMag;
	}

	void COnlineStatus::setEmvFallbackToMag(bool emvFallbackToMag) {
		this->emvFallbackToMag = emvFallbackToMag;
	}

	bool COnlineStatus::isFloorLimit()  {
		return floorLimit;
	}

	void COnlineStatus::setFloorLimit(bool floorLimit) {
		this->floorLimit = floorLimit;
	}

	bool COnlineStatus::isManualEntry()  {
		return manualEntry;
	}

	void COnlineStatus::setManualEntry(bool manualEntry) {
		this->manualEntry = manualEntry;
	}

	bool COnlineStatus::isNotHotcard()  {
		return notHotcard;
	}

	void COnlineStatus::setNotHotcard(bool notHotcard) {
		this->notHotcard = notHotcard;
	}

	bool COnlineStatus::isSrcCode()  {
		return srcCode;
	}

	void COnlineStatus::setSrcCode(bool srcCode) {
		this->srcCode = srcCode;
	}

	bool COnlineStatus::isVelocity()  {
		return velocity;
	}

	void COnlineStatus::setVelocity(bool velocity) {
		this->velocity = velocity;
	}
}
