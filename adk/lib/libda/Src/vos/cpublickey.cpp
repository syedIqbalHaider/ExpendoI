#include <stdio.h>
#include <stdlib.h>
#include "libda/cconmanager.h"
#include "libda/cpublickey.h"

using namespace com_verifone_conmanager;

namespace com_verifone_publickey
{
	CConManager conManager;

	CPublicKey::CPublicKey() {}

	std::string& CPublicKey::getActivation()  {
		return activation;
	}

	void CPublicKey::setActivation( std::string activation) {
		this->activation = activation;
	}

	std::string& CPublicKey::getCheckSum()  {
		return checkSum;
	}

	void CPublicKey::setCheckSum( std::string checkSum) {
		this->checkSum = checkSum;
	}

	std::string& CPublicKey::getExpiry()  {
		return expiry;
	}

	void CPublicKey::setExpiry( std::string expiry) {
		this->expiry = expiry;
	}

	std::string& CPublicKey::getExponent()  {
		return exponent;
	}

	void CPublicKey::setExponent( std::string exponent) {
		this->exponent = exponent;
	}

	std::string& CPublicKey::getHashAlgorithIndicator()  {
		return hashAlgorithIndicator;
	}

	void CPublicKey::setHashAlgorithIndicator(
			 std::string hashAlgorithIndicator) {
		this->hashAlgorithIndicator = hashAlgorithIndicator;
	}

	std::string& CPublicKey::getKeyAlgorithmIndicator()  {
		return keyAlgorithmIndicator;
	}

	void CPublicKey::setKeyAlgorithmIndicator(
			 std::string keyAlgorithmIndicator) {
		this->keyAlgorithmIndicator = keyAlgorithmIndicator;
	}

	std::string& CPublicKey::getModulus()  {
		return modulus;
	}

	void CPublicKey::setModulus( std::string modulus) {
		this->modulus = modulus;
	}

	 std::string& CPublicKey::getPublicKeyIndex()  {
		return publicKeyIndex;
	}

	void CPublicKey::setPublicKeyIndex( std::string publicKeyIndex) {
		this->publicKeyIndex = publicKeyIndex;
	}

	 std::string& CPublicKey::getRid()  {
		return rid;
	}

	void CPublicKey::setRid( std::string rid) {
		this->rid = rid;
	}
}
