#include <stdio.h>
#include <stdlib.h>
#include "libda/cuser.h"
#include "libda/cconmanager.h"
#include "libda/cusermanager.h"


using namespace com_verifone_user;
using namespace com_verifone_conmanager;

namespace com_verifone_usermanager
{
	CConManager conManager;

	CUserManager::CUserManager(){}

	int CUserManager::addUser(CUser &user) {
		user.setDateAdded(getCurrentDateTime());
		return conManager.insertUser(user);
	}
	int CUserManager::updateUser(CUser &user) {

		return conManager.updateUser(user);
	}

	int CUserManager::getUserByName(std::string name,CUser &user) {
		std::string sql = "SELECT * FROM user WHERE name = '"+name+"';";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1) {
			return DB_FAIL;
		}

		std::string val;
		int ret = conManager.getColumnValue("id",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			user.setId(val);

		ret = conManager.getColumnValue("name",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			user.setName(val);

		ret = conManager.getColumnValue("pin",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			user.setPin(val);

		ret = conManager.getColumnValue("type",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			user.setUserType((CUser::USER_TYPE)atoi(val.c_str()));

		ret = conManager.getColumnValue("date_added",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			user.setDateAdded(val);

		ret = conManager.getColumnValue("banking_allowed",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else {
			if (val.compare("0") == 0)
				user.setBankingAllowed(false);
			else
				user.setBankingAllowed(true);
		}

		ret = conManager.getColumnValue("pin_required",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else {
			if (val.compare("0") == 0)
				user.setPinRequired(false);
			else
				user.setPinRequired(true);
		}

		ret = conManager.getColumnValue("refunds_allowed",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else {
			if (val.compare("0") == 0)
				user.setRefundsAllowed(false);
			else
				user.setRefundsAllowed(true);
		}

		return DB_OK;
	}

	int CUserManager::getUserById(std::string id,CUser &user) {

		std::string sql = "SELECT * FROM user WHERE id = '"+id+"';";

		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1)
			return DB_FAIL;

		std::string val;
		int ret = conManager.getColumnValue("id",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			user.setId(val);

		ret = conManager.getColumnValue("name",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			user.setName(val);

		ret = conManager.getColumnValue("pin",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			user.setPin(val);

		ret = conManager.getColumnValue("type",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			user.setUserType((CUser::USER_TYPE)atoi(val.c_str()));

		ret = conManager.getColumnValue("date_added",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			user.setDateAdded(val);

		ret = conManager.getColumnValue("banking_allowed",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else {
			if (val.compare("0") == 0)
				user.setBankingAllowed(false);
			else
				user.setBankingAllowed(true);
		}

		ret = conManager.getColumnValue("pin_required",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else {
			if (val.compare("0") == 0)
				user.setPinRequired(false);
			else
				user.setPinRequired(true);
		}

		ret = conManager.getColumnValue("refunds_allowed",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else {
			if (val.compare("0") == 0)
				user.setRefundsAllowed(false);
			else
				user.setRefundsAllowed(true);
		}

		return DB_OK;
	}

	int CUserManager::deleteUserByName(std::string name) {

		std::string sql = "DELETE FROM user WHERE name = '"+name+"';";
		conManager.writeToDB(sql.c_str());

		return DB_OK;
	}
	int CUserManager::deleteUserById(std::string id) {

		std::string sql = "DELETE FROM user WHERE id = '"+id+"';";
		conManager.writeToDB(sql.c_str());

		return DB_OK;
	}

	bool CUserManager::verifyPinById(std::string id, std::string pin){

		std::string sql = "SELECT pin FROM user WHERE id = '"+id+"';";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1)
			return DB_FAIL;

		std::string val;
		int ret = conManager.getColumnValue("pin",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else {
			if (val.compare(pin) == 0)
				return DB_OK;
			else
				return DB_FAIL;
		}
	}
	bool CUserManager::verifyPinByname(std::string name,std::string pin){

		std::string sql = "SELECT pin FROM user WHERE name = '"+name+"';";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1)
			return DB_FAIL;

		std::string val;
		int ret = conManager.getColumnValue("pin",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else {
			if (val.compare(pin) == 0)
				return DB_OK;
			else
				return DB_FAIL;
		}
	}

	int CUserManager::getAllUsers(std::map<std::string,CUser> &users) {
		std::string sql = "SELECT * FROM user;";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1)
			return DB_OK; // no users

		std::string val;
		for (int i=1;i<records.size();i++) {
			CUser user;
			int ret = conManager.getColumnValue("id",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				user.setId(val);

			ret = conManager.getColumnValue("name",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				user.setName(val);

			ret = conManager.getColumnValue("pin",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				user.setPin(val);

			ret = conManager.getColumnValue("type",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				user.setUserType((CUser::USER_TYPE)atoi(val.c_str()));

			ret = conManager.getColumnValue("date_added",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				user.setDateAdded(val);

			ret = conManager.getColumnValue("banking_allowed",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else {
				if (val.compare("0") == 0)
					user.setBankingAllowed(false);
				else
					user.setBankingAllowed(true);
			}

			ret = conManager.getColumnValue("pin_required",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else {
				if (val.compare("0") == 0)
					user.setPinRequired(false);
				else
					user.setPinRequired(true);
			}

			ret = conManager.getColumnValue("refunds_allowed",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else {
				if (val.compare("0") == 0)
					user.setRefundsAllowed(false);
				else
					user.setRefundsAllowed(true);
			}

			users[user.getId()] = user;
		}

		return DB_OK;
	}
	int CUserManager::getAllSupervisors(std::map<std::string,CUser> &users) {
		std::string sql = "SELECT * FROM user WHERE type = '"+SSTR(CUser::SUPERVISOR)+"';";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1)
			return DB_OK; // no records

		std::string val;
		for (int i=1;i<records.size();i++) {
			CUser user;
			int ret = conManager.getColumnValue("id",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				user.setId(val);

			ret = conManager.getColumnValue("name",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				user.setName(val);

			ret = conManager.getColumnValue("pin",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				user.setPin(val);

			ret = conManager.getColumnValue("type",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				user.setUserType((CUser::USER_TYPE)atoi(val.c_str()));

			ret = conManager.getColumnValue("date_added",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				user.setDateAdded(val);

			ret = conManager.getColumnValue("banking_allowed",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else {
				if (val.compare("0") == 0)
					user.setBankingAllowed(false);
				else
					user.setBankingAllowed(true);
			}

			ret = conManager.getColumnValue("pin_required",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else {
				if (val.compare("0") == 0)
					user.setPinRequired(false);
				else
					user.setPinRequired(true);
			}

			ret = conManager.getColumnValue("refunds_allowed",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else {
				if (val.compare("0") == 0)
					user.setRefundsAllowed(false);
				else
					user.setRefundsAllowed(true);
			}

			users[user.getId()] = user;
		}

		return DB_OK;
	}
	int CUserManager::getAllManagers(std::map<std::string,CUser> &users) {

		std::string sql = "SELECT * FROM user WHERE type = '"+SSTR(CUser::MANAGER)+"';";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1)
			return DB_OK; // no records

		std::string val;
		for (int i=1;i<records.size();i++) {
			CUser user;
			int ret = conManager.getColumnValue("id",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				user.setId(val);

			ret = conManager.getColumnValue("name",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				user.setName(val);

			ret = conManager.getColumnValue("pin",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				user.setPin(val);

			ret = conManager.getColumnValue("type",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				user.setUserType((CUser::USER_TYPE)atoi(val.c_str()));

			ret = conManager.getColumnValue("date_added",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				user.setDateAdded(val);

			ret = conManager.getColumnValue("banking_allowed",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else {
				if (val.compare("0") == 0)
					user.setBankingAllowed(false);
				else
					user.setBankingAllowed(true);
			}

			ret = conManager.getColumnValue("pin_required",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else {
				if (val.compare("0") == 0)
					user.setPinRequired(false);
				else
					user.setPinRequired(true);
			}

			ret = conManager.getColumnValue("refunds_allowed",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else {
				if (val.compare("0") == 0)
					user.setRefundsAllowed(false);
				else
					user.setRefundsAllowed(true);
			}

			users[user.getId()] = user;
		}

		return DB_OK;
	}
	int CUserManager::getAllCashiers(std::map<std::string,CUser> &users) {
		std::string sql = "SELECT * FROM user WHERE type = '"+SSTR(CUser::CASHIER)+"';";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1)
			return DB_OK; // no records

		std::string val;
		for (int i=1;i<records.size();i++) {
			CUser user;
			int ret = conManager.getColumnValue("id",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				user.setId(val);

			ret = conManager.getColumnValue("name",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				user.setName(val);

			ret = conManager.getColumnValue("pin",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				user.setPin(val);

			ret = conManager.getColumnValue("type",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				user.setUserType((CUser::USER_TYPE)atoi(val.c_str()));

			ret = conManager.getColumnValue("date_added",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				user.setDateAdded(val);

			ret = conManager.getColumnValue("banking_allowed",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else {
				if (val.compare("0") == 0)
					user.setBankingAllowed(false);
				else
					user.setBankingAllowed(true);
			}

			ret = conManager.getColumnValue("pin_required",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else {
				if (val.compare("0") == 0)
					user.setPinRequired(false);
				else
					user.setPinRequired(true);
			}

			ret = conManager.getColumnValue("refunds_allowed",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else {
				if (val.compare("0") == 0)
					user.setRefundsAllowed(false);
				else
					user.setRefundsAllowed(true);
			}

			users[user.getId()] = user;
		}

		return DB_OK;

	}

	std::string CUserManager::getCurrentDateTime() {
	    time_t     now = time(0);
	    struct tm  tstruct;
	    char       buf[80];
	    tstruct = *localtime(&now);
	    strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);

	    return buf;
	}
}
