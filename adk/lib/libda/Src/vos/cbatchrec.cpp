#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <errno.h>
#include "libda/cbatchrec.h"
#include "libda/cconmanager.h"
#include "libda/himdef.h"

namespace com_verifone_batchrec
{
	CBatchRec::CBatchRec():inProgress(true),cashExcludeFlag(false),repeatTsn(0),cashierPinVerified(false),batchNo(0),txCanceled(false),txForceOnline(false),
			txForceOffline(false),cardhAuthMethod(PIN),approveOnline(false),litres(0),batchTxNo(0),captureFlag(NOT_CAPTURED),cashAmount(0),timeoutReversal(false),
			signatureRequired(false),draftCapMode(OFFLINE),rcsCard(false),panEntryMode(SWIPED),transmissionNum(0),iccFlag(false),tsn(0),tipAmount(0),
			demoTx(false),fleetCard(false),authMode(TERMINAL),payRequest(false),txAmount(0),cardFees(0),loadFees(0),approved(false),declined(false),txVoided(false){}

	std::string& CBatchRec::getAccProfIndex()  {
		return accProfIndex;
	}

	void CBatchRec::setAccProfIndex( std::string accProfIndex) {
		this->accProfIndex = accProfIndex;
	}

	std::string& CBatchRec::getAccType()  {
		return accType;
	}

	void CBatchRec::setAccType( std::string accType) {
		this->accType = accType;
	}

	bool CBatchRec::isApproveOnline()  {
		return approveOnline;
	}

	void CBatchRec::setApproveOnline(bool approveOnline) {
		this->approveOnline = approveOnline;
	}

	std::string& CBatchRec::getAuthCode()  {
		return authCode;
	}

	void CBatchRec::setAuthCode( std::string authCode) {
		this->authCode = authCode;
	}

	AUTH_MODE CBatchRec::getAuthMode()  {
		return authMode;
	}

	void CBatchRec::setAuthMode(AUTH_MODE authMode) {
		this->authMode = authMode;
	}

	unsigned int CBatchRec::getBatchDbId()  {
		return batchNo;
	}

	void CBatchRec::setBatchDbId(unsigned int batchNo) {
		this->batchNo = batchNo;
	}

	unsigned int CBatchRec::getBatchTxNo()  {
		return batchTxNo;
	}

	void CBatchRec::setBatchTxNo(unsigned int batchTxNo) {
		this->batchTxNo = batchTxNo;
	}

	std::string& CBatchRec::getBudgetPeriod()  {
		return budgetPeriod;
	}

	void CBatchRec::setBudgetPeriod( std::string budgetPeriod) {
		this->budgetPeriod = budgetPeriod;
	}

	std::string& CBatchRec::getCanceledReceiptNo()  {
		return canceledReceiptNo;
	}

	void CBatchRec::setCanceledReceiptNo( std::string canceledReceiptNo) {
		this->canceledReceiptNo = canceledReceiptNo;
	}

	std::string& CBatchRec::getCanceledTxType()  {
		return canceledTxType;
	}

	void CBatchRec::setCanceledTxType( std::string canceledTxType) {
		this->canceledTxType = canceledTxType;
	}

	std::string& CBatchRec::getCancelledTsn()  {
		return cancelledTsn;
	}

	void CBatchRec::setCancelledTsn(std::string cancelledTsn) {
		this->cancelledTsn = cancelledTsn;
	}

	CARD_AUTH_METHOD CBatchRec::getCardhAuthMethod()  {
		return cardhAuthMethod;
	}

	void CBatchRec::setCardhAuthMethod(CARD_AUTH_METHOD cardhAuthMethod) {
		this->cardhAuthMethod = cardhAuthMethod;
	}

	std::string& CBatchRec::getCardholderName()  {
		return cardholderName;
	}

	void CBatchRec::setCardholderName( std::string cardholderName) {
		this->cardholderName = cardholderName;
	}

	long CBatchRec::getCashAmount()  {
		return cashAmount;
	}

	void CBatchRec::setCashAmount(long cashAmount) {
		this->cashAmount = cashAmount;
	}

	bool CBatchRec::isCashExcludeFlag()  {
		return cashExcludeFlag;
	}

	void CBatchRec::setCashExcludeFlag(bool cashExcludeFlag) {
		this->cashExcludeFlag = cashExcludeFlag;
	}

	std::string& CBatchRec::getCashierId()  {
		return cashierId;
	}

	void CBatchRec::setCashierId( std::string cashierId) {
		this->cashierId = cashierId;
	}

	std::string& CBatchRec::getCashierName()  {
		return cashierName;
	}

	void CBatchRec::setCashierName(std::string cashierName) {
		this->cashierName = cashierName;
	}

	bool CBatchRec::isCashierPinVerified()  {
		return cashierPinVerified;
	}

	void CBatchRec::setCashierPinVerified(bool cashierPinVerified) {
		this->cashierPinVerified = cashierPinVerified;
	}

	std::string& CBatchRec::getCompletionCode()  {
		return completionCode;
	}

	void CBatchRec::setCompletionCode( std::string completionCode) {
		this->completionCode = completionCode;
	}

	std::string& CBatchRec::getCvv()  {
		return CVV;
	}

	void CBatchRec::setCvv( std::string cvv) {
		CVV = cvv;
	}

	bool CBatchRec::isDemoTx()  {
		return demoTx;
	}

	void CBatchRec::setDemoTx(bool demoTx) {
		this->demoTx = demoTx;
	}

	DRAFT_CAPTURE_MODE CBatchRec::getDraftCapMode()  {
		return draftCapMode;
	}

	void CBatchRec::setDraftCapMode(DRAFT_CAPTURE_MODE draftCapMode) {
		this->draftCapMode = draftCapMode;
	}

	std::string& CBatchRec::getEmvTags()  {
		return emvTags;
	}

	void CBatchRec::setEmvTags( std::string emvTags) {


		this->emvTags = emvTags;
	}

	std::string& CBatchRec::getExp()  {
		return exp;
	}

	void CBatchRec::setExp( std::string exp) {
		this->exp = exp;
	}

	bool CBatchRec::isFleetCard()  {
		return fleetCard;
	}

	void CBatchRec::setFleetCard(bool fleetCard) {
		this->fleetCard = fleetCard;
	}

	std::string& CBatchRec::getFleetCardUsage()  {
		return fleetCardUsage;
	}

	void CBatchRec::setFleetCardUsage( std::string fleetCardUsage) {
		this->fleetCardUsage = fleetCardUsage;
	}

	CFraudIndicator& CBatchRec::getFraudInd()  {
		return fraudInd;
	}

	void CBatchRec::setFraudInd(CFraudIndicator fraudInd) {
		this->fraudInd = fraudInd;
	}

	bool CBatchRec::isIccFlag()  {
		return iccFlag;
	}

	void CBatchRec::setIccFlag(bool iccFlag) {
		this->iccFlag = iccFlag;
	}

	std::string& CBatchRec::getInvoiceNo()  {
		return invoiceNo;
	}

	void CBatchRec::setInvoiceNo( std::string invoiceNo) {
		this->invoiceNo = invoiceNo;
	}

	long CBatchRec::getLitres()  {
		return litres;
	}

	void CBatchRec::setLitres(long litres) {
		this->litres = litres;
	}

	std::string& CBatchRec::getNonIsoRespCode()  {
		return nonIsoRespCode;
	}

	void CBatchRec::setNonIsoRespCode( std::string nonIsoRespCode) {
		this->nonIsoRespCode = nonIsoRespCode;
	}

	std::string& CBatchRec::getOdoReading()  {
		return odoReading;
	}

	void CBatchRec::setOdoReading( std::string odoReading) {
		this->odoReading = odoReading;
	}

	COnlineStatus& CBatchRec::getOnlStatus()  {
		return onlStatus;
	}

	void CBatchRec::setOnlStatus( COnlineStatus onlStatus) {
		this->onlStatus = onlStatus;
	}

	std::string& CBatchRec::getOrigTxDateTime()  {
		return origTxDateTime;
	}

	void CBatchRec::setOrigTxDateTime( std::string origTxDateTime) {
		this->origTxDateTime = origTxDateTime;
	}

	std::string& CBatchRec::getPan()  {
		return pan;
	}

	void CBatchRec::setPan(std::string pan) {
		this->pan = pan;
	}

	PAN_ENTRY_MODE CBatchRec::getPanEntryMode()  {
		return panEntryMode;
	}

	void CBatchRec::setPanEntryMode(PAN_ENTRY_MODE panEntryMode) {
		this->panEntryMode = panEntryMode;
	}

	bool CBatchRec::isPayRequest()  {
		return payRequest;
	}

	void CBatchRec::setPayRequest(bool payRequest) {
		this->payRequest = payRequest;
	}

	CProductInfo& CBatchRec::getProductInfo()  {
		return productInfo;
	}

	void CBatchRec::setProductInfo(CProductInfo productInfo) {
		this->productInfo = productInfo;
	}

	bool CBatchRec::isRcsCard()  {
		return rcsCard;
	}

	void CBatchRec::setRcsCard(bool rcsCard) {
		this->rcsCard = rcsCard;
	}

	std::string& CBatchRec::getReceiptNo()  {
		return receiptNo;
	}

	void CBatchRec::setReceiptNo( std::string receiptNo) {
		this->receiptNo = receiptNo;
	}

//iq_audi_301216 start
	std::string& CBatchRec::getEcrNo()  {
		return ecrNo;
	}

	void CBatchRec::setEcrNo( std::string ecrNo) {
		this->ecrNo = ecrNo;
	}
//	iq_audi_301216 end

	std::string& CBatchRec::getRespMsg()  {
		return respMsg;
	}

	void CBatchRec::setRespMsg( std::string respMsg) {
		this->respMsg = respMsg;
	}

	std::string& CBatchRec::getRefNo1()  {
		return refNo1;
	}

	void CBatchRec::setRefNo1( std::string refNo1) {
		this->refNo1 = refNo1;
	}

	std::string& CBatchRec::getRefNo2()  {
		return refNo2;
	}

	void CBatchRec::setRefNo2( std::string refNo2) {
		this->refNo2 = refNo2;
	}

	unsigned int CBatchRec::getRepeatTsn()  {
		return repeatTsn;
	}

	void CBatchRec::setRepeatTsn(unsigned int repeatTsn) {
		this->repeatTsn = repeatTsn;
	}

	std::string& CBatchRec::getRespCode()  {
		return respCode;
	}

	void CBatchRec::setRespCode( std::string respCode) {
		this->respCode = respCode;
	}

	std::string& CBatchRec::getRrn()  {
		return rrn;
	}

	void CBatchRec::setRrn( std::string rrn) {
		this->rrn = rrn;
	}

	bool CBatchRec::isSignatureRequired()  {
		return signatureRequired;
	}

	void CBatchRec::setSignatureRequired(bool signatureRequired) {
		this->signatureRequired = signatureRequired;
	}

	std::string& CBatchRec::getSpdhSeqNo()  {
		return spdhSeqNo;
	}

	void CBatchRec::setSpdhSeqNo( std::string spdhSeqNo) {
		this->spdhSeqNo = spdhSeqNo;
	}

	std::string& CBatchRec::getSpdhTsn()  {
		return spdhTsn;
	}

	void CBatchRec::setSpdhTsn( std::string spdhTsn) {
		this->spdhTsn = spdhTsn;
	}

	std::string& CBatchRec::getSrc()  {
		return src;
	}

	void CBatchRec::setSrc( std::string src) {
		this->src = src;
	}

	std::string& CBatchRec::getSupervisorId()  {
		return supervisorId;
	}

	void CBatchRec::setSupervisorId( std::string supervisorId) {
		this->supervisorId = supervisorId;
	}

	std::string& CBatchRec::getSupervisorName()  {
		return supervisorName;
	}

	void CBatchRec::setSupervisorName( std::string supervisorName) {
		this->supervisorName = supervisorName;
	}

	std::string& CBatchRec::getTableNo()  {
		return tableNo;
	}

	void CBatchRec::setTableNo( std::string tableNo) {
		this->tableNo = tableNo;
	}

	bool CBatchRec::isTimeoutReversal()  {
		return timeoutReversal;
	}

	void CBatchRec::setTimeoutReversal(bool timeoutReversal) {
		this->timeoutReversal = timeoutReversal;
	}

	long CBatchRec::getTipAmount()  {
		return tipAmount;
	}

	void CBatchRec::setTipAmount(long tipAmount) {
		this->tipAmount = tipAmount;
	}

	std::string& CBatchRec::getTotalsGroup()  {
		return totalsGroup;
	}

	void CBatchRec::setTotalsGroup( std::string totalsGroup) {
		this->totalsGroup = totalsGroup;
	}

	std::string& CBatchRec::getTrack2()  {
		return track2;
	}

	void CBatchRec::setTrack2( std::string track2) {
		this->track2 = track2;
	}

	unsigned int CBatchRec::getTransmissionNum()  {
		return transmissionNum;
	}

	void CBatchRec::setTransmissionNum(unsigned int transmissionNum) {
		this->transmissionNum = transmissionNum;
	}

	unsigned int CBatchRec::getTsn()  {
		return tsn;
	}

	void CBatchRec::setTsn(unsigned int tsn) {
		this->tsn = tsn;
	}

	long CBatchRec::getTxAmount()  {
		return txAmount;
	}

	void CBatchRec::setTxAmount(long txAmount) {
		this->txAmount = txAmount;
	}

	bool CBatchRec::isTxCanceled()  {
		return txCanceled;
	}

	void CBatchRec::setTxCanceled(bool txCanceled) {
		this->txCanceled = txCanceled;
	}

	bool CBatchRec::isTxVoided()  {
		return txVoided;
	}

	void CBatchRec::setTxVoided(bool txVoided) {
		this->txVoided = txVoided;
	}

	std::string& CBatchRec::getTxCode()  {
		return txCode;
	}

	void CBatchRec::setTxCode( std::string txCode) {
		this->txCode = txCode;
	}

	std::string& CBatchRec::getTxDateTime()  {
		return txDateTime;
	}

	void CBatchRec::setTxDateTime( std::string txDateTime) {
		this->txDateTime = txDateTime;
	}

	bool CBatchRec::isTxForceOffline()  {
		return txForceOffline;
	}

	void CBatchRec::setTxForceOffline(bool txForceOffline) {
		this->txForceOffline = txForceOffline;
	}

	bool CBatchRec::isTxForceOnline()  {
		return txForceOnline;
	}

	void CBatchRec::setTxForceOnline(bool txForceOnline) {
		this->txForceOnline = txForceOnline;
	}

	std::string& CBatchRec::getTxType()  {
		return txType;
	}

	void CBatchRec::setTxType( std::string txType) {
		this->txType = txType;
	}

	std::string& CBatchRec::getWaiterId()  {
		return waiterId;
	}

	void CBatchRec::setWaiterId( std::string waiterId) {
		this->waiterId = waiterId;
	}

	CAPTURE_FLAG CBatchRec::getCaptureFlag() {
		return captureFlag;
	}

	void CBatchRec::setCaptureFlag(CAPTURE_FLAG captureFlag) {
		this->captureFlag = captureFlag;
	}

	bool CBatchRec::isInProgress() {
		return inProgress;
	}

	void CBatchRec::setInProgress(bool inProgress) {
		this->inProgress = inProgress;
	}

	long CBatchRec::getCardFees() {
		return cardFees;
	}

	void CBatchRec::setCardFees(long cardFees) {
		this->cardFees = cardFees;
	}

	long CBatchRec::getLoadFees() {
		return loadFees;
	}

	void CBatchRec::setLoadFees(long loadFees) {
		this->loadFees = loadFees;
	}

	bool CBatchRec::isApproved() {
		return approved;
	}

	void CBatchRec::setApproved(bool approved) {
		this->approved = approved;
	}

	bool CBatchRec::isDeclined() {
		return declined;
	}

	void CBatchRec::setDeclined(bool declined) {
		this->declined = declined;
	}

	std::string CBatchRec::getReason()
	{
		return reason;
	}

	void CBatchRec::setReason(std::string reason)
	{
		this->reason = reason;
	}

	void CBatchRec::setTransCurrency(std::string currency)
	{
		this->transCurrency = currency;
	}

	std::string CBatchRec::getTransCurrency()
	{
		return transCurrency;
	}

}
