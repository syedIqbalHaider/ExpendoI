#include <stdio.h>
#include <stdlib.h>
#include "libda/ccarddata.h"


namespace com_verifone_carddata
{
	CCardData::CCardData():cardLength(0),excluded(false){}

	std::map<std::string, CAccountProfile>& CCardData::getAccounts()  {
		return accounts;
	}

	CAccountProfile & CCardData::getAccountAt(int index){
		int count=0;
		for(std::map<std::string, CAccountProfile>::iterator it=accounts.begin();it!=accounts.end();++it){
			if(count == index)
				return it->second;

			count++;
		}

		throw std::out_of_range("index out of Account Profile range");
	}

	int CCardData::getAccountsCount(){
		return accounts.size();
	}

	void CCardData::setAccounts( std::map<std::string, CAccountProfile> accounts) {
		this->accounts = accounts;
	}

	 std::string& CCardData::getBin()  {
		return bin;
	}

	void CCardData::setBin( std::string bin) {
		this->bin = bin;
	}

	 std::string& CCardData::getCardIssuedName()  {
		return cardIssuedName;
	}

	void CCardData::setCardIssuedName(std::string cardIssuedName) {
		this->cardIssuedName = cardIssuedName;
	}

	int CCardData::getCardLength()  {
		return cardLength;
	}

	void CCardData::setCardLength(int cardLength) {
		this->cardLength = cardLength;
	}

	 CCardProfile& CCardData::getCardProfile()  {
		return cardProfile;
	}

	void CCardData::setCardProfile(CCardProfile cardProfile) {
		this->cardProfile = cardProfile;
	}

	bool CCardData::isExcluded() {
		return excluded;
	}

	void CCardData::setExcluded(bool excluded) {
		this->excluded = excluded;
	}

	int CCardData::getCardProductId() {
		return cardProductId;
	}

	void CCardData::setCardProductId(int cardProductId) {
		this->cardProductId = cardProductId;
	}
}
