#include <stdio.h>
#include <stdlib.h>
#include <libda/cfraudindicator.h>


namespace com_verifone_fraudindicator
{
	CFraudIndicator::CFraudIndicator():hotcard(false),skimmedCard(false){}

	bool CFraudIndicator::isHotcard()  {
		return hotcard;
	}

	void CFraudIndicator::setHotcard(bool hotcard) {
		this->hotcard = hotcard;
	}

	bool CFraudIndicator::isSkimmedCard()  {
		return skimmedCard;
	}

	void CFraudIndicator::setSkimmedCard(bool skimmedCard) {
		this->skimmedCard = skimmedCard;
	}
}
