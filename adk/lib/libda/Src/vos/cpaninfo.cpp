#include <stdio.h>
#include <stdlib.h>
#include "libda/cpaninfo.h"
#include "libda/cconmanager.h"


using namespace com_verifone_conmanager;

namespace com_verifone_paninfo
{

	CConManager conManager;

	CPanInfo::CPanInfo():lastUsedTimestamp(0),counter(0),timeStamp(0) {}

	int CPanInfo::getCounter()
	{
		return counter;
	}

	void CPanInfo::setCounter(int counter)
	{
		this->counter = counter;
	}

	long CPanInfo::getLastUsedTimestamp()
	{
		return lastUsedTimestamp;
	}

	void CPanInfo::setLastUsedTimestamp(long lastUsedTimestamp)
	{
		this->lastUsedTimestamp = lastUsedTimestamp;
	}

	 std::string& CPanInfo::getPan()
	{
		return pan;
	}

	void CPanInfo::setPan( std::string pan)
	{
		this->pan = pan;
	}

	long CPanInfo::getTimeStamp()
	{
		return timeStamp;
	}

	void CPanInfo::setTimeStamp(long timeStamp)
	{
		this->timeStamp = timeStamp;
	}

}
