#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <string>

#include <iostream>
#include <string>
#include <sstream>
#include <iomanip>

#include "libda/cconmanager.h"
#include "libda/sqlite3.h"
#include "libda/himdef.h"
#include "libda/cuser.h"
#include "libda/ccommssettings.h"
#include "libda/cversionsfile.h"
#include "libda/cbatchrec.h"
#include "libda/conlinestatus.h"
#include "libda/cfraudindicator.h"

using namespace com_verifone_user;
using namespace com_verifone_commssettings;
using namespace com_verifone_versionsfile;
using namespace com_verifone_onlinestatus;
using namespace com_verifone_fraudindicator;

namespace com_verifone_conmanager
{

	CConManager::CConManager(){
		sqldb = NULL;
		setDbPath(DB_PATH);
	}

	std::string& CConManager::getDbPath()  {
		return dbPath;
	}

	void CConManager::setDbPath( std::string dbPath) {
		this->dbPath = dbPath;
	}

	sqlite3 * CConManager::getDbConnection() {
		if (sqldb == NULL) {
			 int rc = sqlite3_open(getDbPath().c_str(), &sqldb);
			 if( rc ){
			   printf("Can't open database %d, nm=%s, msg=: %s\n",rc,getDbPath().c_str(), sqlite3_errmsg(sqldb));
			   sqlite3_close(sqldb);
			   sqldb = NULL;
			 } else {
				 //printf("Using database %s\n",getDbPath().c_str());
			 }
		}
		return sqldb;
	}

	void CConManager::close(sqlite3 **db) {
		if ((db != NULL) && (*db != NULL)) {
			int rc = sqlite3_close(*db);
			if (rc)
				printf("Can't close database, code=%d, nm=%s, msg=: %s\n",rc,getDbPath().c_str(), sqlite3_errmsg(*db));
			else
				*db = NULL;
		}
	}

	void CConManager::close() {
		close (&sqldb);
	}

	int CConManager::getFirstRecField(void *vRecEnv, int nrColumns, char **argv, char **azColName){
		std::vector<std::string> &fields = *((std::vector<std::string> *)vRecEnv);
		if (fields.empty() && nrColumns > 0) {
			fields.push_back(argv[0] ? argv[0] : "NULL");
		}
		return 1; // ensure ABORT
	}

	int CConManager::getRecordFields(void *vRecEnv, int nrColumns, char **argv, char **azColName){

		if (nrColumns == 0)
			return 1;

		// Note: We are assuming that it is ok for the inner vector to be declared in this scope and that it will
		// only be freed at the right time by the vector's destructors. May be wrong assumption!

		std::vector<std::vector<std::string> > &records = *((std::vector<std::vector<std::string> > *)vRecEnv);
		std::string sMax = (records[0].size() == 1) ? records[0][0] : records[0][records[0].size()-1];
		long maxNr = strtol(sMax.c_str(),NULL,10);
		//std::vector<std::string> *thisRec = new std::vector<std::string>(nrColumns);
		if (records.size() == 1) {
			// First time - add column names
			std::vector<std::string> colNames(nrColumns);
			for(int i=0; i<nrColumns; i++){
				colNames[i] = (azColName[i] ? azColName[i] : "");
			}
			colNames.push_back(sMax);
			records.clear();
			records.push_back(colNames);
		}
		if (maxNr != 0) {
			std::vector<std::string> thisRec(nrColumns);
			for(int i=0; i<nrColumns; i++){
				thisRec[i] = (argv[i] ? argv[i] : "NULL");
			}
			records.push_back(thisRec);
		}
		if ((maxNr+1) == (long)records.size())
			return 1; // ensure abort
		else
			return 0;
	}


	/**
	 * Returns the first field in the first record; use default value provided if none or NULL.
	 * @param sql
	 * @param def
	 * @return
	 */
	std::string CConManager::getDBFirstField(std::string sql, std::string def, bool mustClose) {
		 std::vector<std::string> fields;
		 char *errMsg = 0;
		 std::string res = def;
		 sqlite3 *db = getDbConnection();

		 int rc = sqlite3_exec(db,sql.c_str(), getFirstRecField, &fields, &errMsg);
		 if( rc!=SQLITE_OK && rc!=SQLITE_ABORT){
				printf("SQL error: %s\nQry was %s", errMsg,sql.c_str());
				sqlite3_free(errMsg);
		 } else {
			 if (fields.size()!=0 && !fields[0].empty() && (fields[0].compare("NULL")!=0))
				 res = fields[0];
		 }
		 if (mustClose) close();
		 return res;
	}

	int CConManager::selectFromDB(std::string sql, std::vector<std::vector<std::string> > &records, bool closeConnection) {
		int nrCols = getDBRecords(sql,records,1000,closeConnection);
		return ((nrCols>=0) ? DB_OK : DB_FAIL);
	}
	/**
	 * Return the first 1000 records - see getDBRecords for finer control
	 * @param sql
	 * @param records
	 * @return
	 */
	int CConManager::selectFromDB(std::string sql, std::vector<std::vector<std::string> > &records) {
		int nrCols = getDBRecords(sql,records,1000,true);
		return ((nrCols>=0) ? DB_OK : DB_FAIL);

//		 int maxNr = 1000;
//		std::vector<std::string> fields;
//		 char *errMsg = 0;
//		 int nrCols = -1;
//		 sqlite3 *db = getDbConnection();
//
//		 // We save maxnr in the first field of the first record - will be replaced right at the end
//		 if (maxNr >= 0) records.reserve(maxNr+1);
//		 // Note that fields will be freeed when this routine is exited - but we reallocate anyway so we are safe
//		 fields.push_back(SSTR(maxNr));
//		 records.push_back(fields);
//
//		 int rc = sqlite3_exec(db,sql.c_str(), getRecordFields, &records, &errMsg);
//
//		/*for (uint i=0;i<records.size();i++) {
//			std::string rowStr = "Row " + SSTR(i) + ":";
//			for (uint c=0;c<records[i].size();c++) {
//				rowStr +="," + records[i][c];
//			}
//			printf("getDbRec %d,%s\n",records[i].size(),rowStr.c_str());
//		}*/
//
//		 records[0].pop_back(); // remove the last field of first record which contained maxNr
//
//		 if( rc!=SQLITE_OK && rc!=SQLITE_ABORT){
//				printf("SQL error: %s\nQry was %s", errMsg,sql.c_str());
//				sqlite3_free(errMsg);
//				close();
//				return DB_FAIL;
//		 }
//
//		 close();
//		 return DB_OK;
	}

	/**
	 * Populate records (a vector of vectors) with first a vector of the fieldnames then a vector for each record in the
	 * query result, upto maxNr records. Note that all fields have a string representation.
	 * @param sql
	 * @param records - must be empty initially
	 * @param maxNr - if 0 will just return the field names, if <0 return fieldnames plus all records found;
	 *        if >0 return fieldnames plus maxNr records (Note that you may want to use LIMIT maxNr in your original qry)
	 * @return nr of columns in a record; -1 on failure
	 */
	int CConManager::getDBRecords(std::string sql, std::vector<std::vector<std::string> > &records,
								  int maxNr, bool mustClose) {
		 std::vector<std::string> fields;
		 char *errMsg = 0;
		 int nrCols = -1;
		 sqlite3 *db = getDbConnection();

		 // We save maxnr in the first field of the first record - will be replaced right at the end
		 if (maxNr >= 0) records.reserve(maxNr+1);
		 // Note that fields will be freeed when this routine is exited - but we reallocate anyway so we are safe
		 fields.push_back(SSTR(maxNr));
		 records.push_back(fields);

		 int rc = sqlite3_exec(db,sql.c_str(), getRecordFields, &records, &errMsg);
/*
for (uint i=0;i<records.size();i++) {
	std::string rowStr = "Row " + SSTR(i) + ":";
	for (uint c=0;c<records[i].size();c++) {
		rowStr +="," + records[i][c];
	}
	printf("getDbRec %d,%s",records[i].size(),rowStr.c_str());
}
*/
		 records[0].pop_back(); // remove the last field of first record which contained maxNr

		 if( rc!=SQLITE_OK && rc!=SQLITE_ABORT){
				printf("SQL error: %s\nQry was %s", errMsg,sql.c_str());
				sqlite3_free(errMsg);
		 } else {
			nrCols = records[0].size();
		 }
		 return nrCols;

		 if (mustClose) close();
	}


	int CConManager::writeToDB(std::string sql) {
		return writeToDB(sql,true);
	}
    int CConManager::writeToDB(std::string sql, bool closeConnection) {
		std::vector<std::string> fields;
		char *errMsg = 0;
		std::string res;
		sqlite3 *db = getDbConnection();

		int rc = sqlite3_exec(db, sql.c_str(), NULL, NULL, &errMsg);
		if (rc != SQLITE_OK) {
			//printf("SQL error: %s\nQuery was %s", errMsg, sql.c_str());
			printf("SQL error: %s\n", errMsg);
			sqlite3_free(errMsg);
			close();
			return DB_FAIL;
		}

		if (closeConnection)
			close();
		return DB_OK;
    }
    int CConManager::insertStatus(std::string name, std::string value) {
    	sqlite3_stmt *stmt;
    	sqlite3 *db = getDbConnection();
    	std::string sql = "INSERT INTO status (app_name,value) VALUES (?,?);";
    	int ret = sqlite3_prepare_v2(db, sql.c_str(),-1,&stmt, 0);
    	if (ret == SQLITE_OK) {
    		sqlite3_bind_text(stmt, 1, name.c_str(),-1,0);
			sqlite3_bind_text(stmt, 2, value.c_str(),-1,0);
			do
			{
			    ret = sqlite3_step( stmt);
			    switch( ret )
			    {
			        case SQLITE_DONE:
			            break;

			        case SQLITE_ROW:
			        	//returned data
			        	break;

			        default:
			            break;
			    }
			}while(ret == SQLITE_ROW );
    	} else {
    		printf("Error = %d\n",ret);
    		sqlite3_finalize(stmt);
    		close();
    		return DB_FAIL;
    	}

    	sqlite3_reset(stmt);
    	sqlite3_finalize(stmt);
    	close();
    	return  DB_OK;
    }

    int CConManager::insertTerminalConfig(std::string name, std::string value) {
    	sqlite3_stmt *stmt;
    	sqlite3 *db = getDbConnection();
    	std::string sql = "INSERT INTO terminal_config (name,value) VALUES (?,?);";
    	int ret = sqlite3_prepare_v2(db, sql.c_str(),-1,&stmt, 0);
    	if (ret == SQLITE_OK) {
    		sqlite3_bind_text(stmt, 1, name.c_str(),-1,0);
			sqlite3_bind_text(stmt, 2, value.c_str(),-1,0);
			do
			{
			    ret = sqlite3_step( stmt);
			    switch( ret )
			    {
			        case SQLITE_DONE:
			            break;

			        case SQLITE_ROW:
			        	//returned data
			        	break;

			        default:
			            break;
			    }
			}while(ret == SQLITE_ROW );
    	} else {
    		printf("Error = %d\n",ret);
    		sqlite3_finalize(stmt);
    		close();
    		return DB_FAIL;
    	}

    	sqlite3_reset(stmt);
    	sqlite3_finalize(stmt);
    	close();
    	return  DB_OK;
    }

    int CConManager::updateUser(CUser &user) {
    	sqlite3_stmt *stmt;
		sqlite3 *db = getDbConnection();
		std::string sql = "UPDATE user SET name = ?, pin = ?,pin_required =?,banking_allowed = ?,refunds_allowed =?,date_added =?,type = ? WHERE id = ?";
		int ret = sqlite3_prepare_v2(db, sql.c_str(),-1,&stmt, 0);
		if (ret == SQLITE_OK) {
			sqlite3_bind_text(stmt, 1, user.getName().c_str(),-1,0);
			sqlite3_bind_text(stmt, 2, user.getPin().c_str(),-1,0);
			sqlite3_bind_text(stmt, 3, user.isPinRequired()?"1":"0",-1,0);
			sqlite3_bind_text(stmt, 4, user.isBankingAllowed()?"1":"0",-1,0);
			sqlite3_bind_text(stmt, 5, user.isRefundsAllowed()?"1":"0",-1,0);
			sqlite3_bind_text(stmt, 6, user.getDateAdded().c_str(),-1,0);
			sqlite3_bind_text(stmt, 7, SSTR(user.getUserType()).c_str(),-1,0);
			sqlite3_bind_text(stmt, 8, user.getId().c_str(),-1,0);
			do
			{
				ret = sqlite3_step( stmt);
				switch( ret )
				{
					case SQLITE_DONE:
						break;

					case SQLITE_ROW:
						//returned data
						break;

					default:
						break;
				}
			}while(ret == SQLITE_ROW );
		} else {
			sqlite3_finalize(stmt);
			close();
			return DB_FAIL;
		}

		sqlite3_reset(stmt);
		sqlite3_finalize(stmt);
		close();
		return  DB_OK;
    }

    int CConManager::insertAid(std::string aid,std::string emvCode,std::string accountProfileHostId,std::string totalsGroupName,std::string appVersion,std::string ddol,std::string tdol,std::string percentageMax,std::string percentage,std::string amount,std::string tacDefault,std::string tacDecline,std::string tacOnline,std::string cardDataInputMode) {
    	sqlite3_stmt *stmt;
		sqlite3 *db = getDbConnection();
		std::string sql = "INSERT INTO aid (aid,application_version,default_ddol,default_tdol,target_percentage_max,target_percentage,threshold_amount,emv_scheme_code,totals_group_name,tac_default,tac_decline,tac_online,card_data_input_mode,account_profile_host_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
		int ret = sqlite3_prepare_v2(db, sql.c_str(),-1,&stmt, 0);
		if (ret == SQLITE_OK) {
			sqlite3_bind_text(stmt, 1, aid.c_str(),-1,0);
			sqlite3_bind_text(stmt, 2, appVersion.c_str(),-1,0);
			sqlite3_bind_text(stmt, 3, ddol.c_str(),-1,0);
			sqlite3_bind_text(stmt, 4, tdol.c_str(),-1,0);
			sqlite3_bind_text(stmt, 5, percentageMax.c_str(),-1,0);
			sqlite3_bind_text(stmt, 6, percentage.c_str(),-1,0);
			sqlite3_bind_text(stmt, 7, amount.c_str(),-1,0);
			sqlite3_bind_text(stmt, 8, emvCode.c_str(),-1,0);
			sqlite3_bind_text(stmt, 9, totalsGroupName.c_str(),-1,0);
			sqlite3_bind_text(stmt, 10, tacDefault.c_str(),-1,0);
			sqlite3_bind_text(stmt, 11, tacDecline.c_str(),-1,0);
			sqlite3_bind_text(stmt, 12, tacOnline.c_str(),-1,0);
			sqlite3_bind_text(stmt, 13, cardDataInputMode.c_str(),-1,0);
			sqlite3_bind_text(stmt, 14, accountProfileHostId.c_str(),-1,0);

			do
			{
				ret = sqlite3_step( stmt);
				switch( ret )
				{
					case SQLITE_DONE:
						break;

					case SQLITE_ROW:
						//returned data
						break;

					default:
						break;
				}
			}while(ret == SQLITE_ROW );
		} else {
			printf("SQL Error = %d\n",ret);
			sqlite3_finalize(stmt);
			close();
			return DB_FAIL;
		}

		sqlite3_reset(stmt);
		sqlite3_finalize(stmt);

		//close();
		return  DB_OK;
    }

    int CConManager::insertPublicKey(std::string rid, std::string publicKeyIndex,std::string hashAlgorithmIndicator,std::string publicKeyAlgorithmIndicator,std::string modulus,std::string exponent,std::string checkSum,std::string activation,std::string expiry) {
    	sqlite3_stmt *stmt;
		sqlite3 *db = getDbConnection();
		std::string sql = "INSERT INTO public_key (rid,public_key_index,hash_algorithm_indicator,public_key_modulus,public_key_exponent,public_key_check_sum,key_activation,key_expiry,key_algorithm_indicator) VALUES (?,?,?,?,?,?,?,?,?);";
		int ret = sqlite3_prepare_v2(db, sql.c_str(),-1,&stmt, 0);
		if (ret == SQLITE_OK) {
			sqlite3_bind_text(stmt, 1, rid.c_str(),-1,0);
			sqlite3_bind_text(stmt, 2, publicKeyIndex.c_str(),-1,0);
			sqlite3_bind_text(stmt, 3, hashAlgorithmIndicator.c_str(),-1,0);
			sqlite3_bind_text(stmt, 4, modulus.c_str(),-1,0);
			sqlite3_bind_text(stmt, 5, exponent.c_str(),-1,0);
			sqlite3_bind_text(stmt, 6, checkSum.c_str(),-1,0);
			sqlite3_bind_text(stmt, 7, activation.c_str(),-1,0);
			sqlite3_bind_text(stmt, 8, expiry.c_str(),-1,0);
			sqlite3_bind_text(stmt, 9, publicKeyAlgorithmIndicator.c_str(),-1,0);
			do
			{
				ret = sqlite3_step( stmt);
				switch( ret )
				{
					case SQLITE_DONE:
						break;

					case SQLITE_ROW:
						//returned data
						break;

					default:
						break;
				}
			}while(ret == SQLITE_ROW );
		} else {
			printf("SQL Error = %d\n",ret);
			sqlite3_finalize(stmt);
			close();
			return DB_FAIL;
		}

		sqlite3_reset(stmt);
		sqlite3_finalize(stmt);

		//close();
		return  DB_OK;
    }

    int CConManager::insertAidOverride(std::string aid, std::string cardProductId,std::string accountProfileId,std::string totalsGroup) {
		sqlite3_stmt *stmt;
		sqlite3 *db = getDbConnection();
		std::string sql = "INSERT INTO aid_override (card_product_id,aid,account_profile_id,totals_group) VALUES (?,?,?,?);";
		int ret = sqlite3_prepare_v2(db, sql.c_str(),-1,&stmt, 0);
		if (ret == SQLITE_OK) {
			sqlite3_bind_text(stmt, 1, cardProductId.c_str(),-1,0);
			sqlite3_bind_text(stmt, 2, aid.c_str(),-1,0);
			sqlite3_bind_text(stmt, 3, accountProfileId.c_str(),-1,0);
			sqlite3_bind_text(stmt, 4, totalsGroup.c_str(),-1,0);
			do
			{
				ret = sqlite3_step( stmt);
				switch( ret )
				{
					case SQLITE_DONE:
						break;

					case SQLITE_ROW:
						//returned data
						break;

					default:
						break;
				}
			}while(ret == SQLITE_ROW );
		} else {
			printf("SQL Error = %d\n",ret);
			sqlite3_finalize(stmt);
			close();
			return DB_FAIL;
		}

		sqlite3_reset(stmt);
		sqlite3_finalize(stmt);

		//close();
		return  DB_OK;
	}

    int CConManager::insertCardProfile(std::string index,std::string active,std::string checkServiceCode,std::string checkExpiryDate,std::string pinLenMin,std::string pinLenMax,std::string checkLuhn,std::string approvalText) {
    	sqlite3_stmt *stmt;
		sqlite3 *db = getDbConnection();
		std::string sql = "INSERT INTO card_profile (active,check_service_code,check_expiry_date,check_luhn,pin_length_min,pin_length_max,voice_approval_text,card_profile_index) VALUES (?,?,?,?,?,?,?,?);";
		int ret = sqlite3_prepare_v2(db, sql.c_str(),-1,&stmt, 0);
		if (ret == SQLITE_OK) {
			sqlite3_bind_text(stmt, 1, active.c_str(),-1,0);
			sqlite3_bind_text(stmt, 2, checkServiceCode.c_str(),-1,0);
			sqlite3_bind_text(stmt, 3, checkExpiryDate.c_str(),-1,0);
			sqlite3_bind_text(stmt, 4, checkLuhn.c_str(),-1,0);
			sqlite3_bind_text(stmt, 5, pinLenMin.c_str(),-1,0);
			sqlite3_bind_text(stmt, 6, pinLenMax.c_str(),-1,0);
			sqlite3_bind_text(stmt, 7, approvalText.c_str(),-1,0);
			sqlite3_bind_text(stmt, 8, index.c_str(),-1,0);
			do
			{
				ret = sqlite3_step( stmt);
				switch( ret )
				{
					case SQLITE_DONE:
						break;

					case SQLITE_ROW:
						//returned data
						break;

					default:
						break;
				}
			}while(ret == SQLITE_ROW );
		} else {
			printf("SQL Error = %d\n",ret);
			sqlite3_finalize(stmt);
			close();
			return DB_FAIL;
		}

		sqlite3_reset(stmt);
		sqlite3_finalize(stmt);

		//close();
		return  DB_OK;
    }

    int CConManager::insertSuperTotals(std::string totalsGroup,std::string superTotalsGroup) {
    	sqlite3_stmt *stmt;
		sqlite3 *db = getDbConnection();
		std::string sql = "INSERT INTO super_totals (totals_group_name,super_totals_group_name) VALUES (?,?);";
		int ret = sqlite3_prepare_v2(db, sql.c_str(),-1,&stmt, 0);
		if (ret == SQLITE_OK) {
			sqlite3_bind_text(stmt, 1, totalsGroup.c_str(),-1,0);
			sqlite3_bind_text(stmt, 2, superTotalsGroup.c_str(),-1,0);
			do
			{
				ret = sqlite3_step( stmt);
				switch( ret )
				{
					case SQLITE_DONE:
						break;

					case SQLITE_ROW:
						//returned data
						break;

					default:
						break;
				}
			}while(ret == SQLITE_ROW );
		} else {
			printf("SQL Error = %d\n",ret);
			sqlite3_finalize(stmt);
			close();
			return DB_FAIL;
		}

		sqlite3_reset(stmt);
		sqlite3_finalize(stmt);

		int id = sqlite3_last_insert_rowid(db);


		//close();
		return  DB_OK;
    }

    int CConManager::insertLimits(std::string cashbackAmtMax,std::string floorLimtAmt,std::string txAmtMax,std::string txAmtMin, std::string limitsIndex) {
    	sqlite3_stmt *stmt;
		sqlite3 *db = getDbConnection();
		std::string sql = "INSERT INTO limits (cashback_amt_max,floor_limit_amt,transaction_amt_max,transaction_amt_min,limits_index) VALUES (?,?,?,?,?);";
		int ret = sqlite3_prepare_v2(db, sql.c_str(),-1,&stmt, 0);
		if (ret == SQLITE_OK) {
			sqlite3_bind_text(stmt, 1, cashbackAmtMax.c_str(),-1,0);
			sqlite3_bind_text(stmt, 2, floorLimtAmt.c_str(),-1,0);
			sqlite3_bind_text(stmt, 3, txAmtMax.c_str(),-1,0);
			sqlite3_bind_text(stmt, 4, txAmtMin.c_str(),-1,0);
			sqlite3_bind_text(stmt, 5, limitsIndex.c_str(),-1,0);
			do
			{
				ret = sqlite3_step( stmt);
				switch( ret )
				{
					case SQLITE_DONE:
						break;

					case SQLITE_ROW:
						//returned data
						break;

					default:
						break;
				}
			}while(ret == SQLITE_ROW );
		} else {
			printf("SQL Error = %d\n",ret);
			sqlite3_finalize(stmt);
			close();
			return DB_FAIL;
		}

		sqlite3_reset(stmt);
		sqlite3_finalize(stmt);

		//close();
		return DB_OK;
    }

    int CConManager::updateLimits(std::string cashbackAmtMax,std::string floorLimtAmt,std::string txAmtMax,std::string txAmtMin,std::string limitsIndex) {
    	sqlite3_stmt *stmt;
		sqlite3 *db = getDbConnection();
		std::string sql = "UPDATE limits SET cashback_amt_max = ?, floor_limit_amt=?, transaction_amt_max=?, transaction_amt_min=? WHERE limits_index = ?;";

		int ret = sqlite3_prepare_v2(db, sql.c_str(),-1,&stmt, 0);
		if (ret == SQLITE_OK) {
			sqlite3_bind_text(stmt, 1, cashbackAmtMax.c_str(),-1,0);
			sqlite3_bind_text(stmt, 2, floorLimtAmt.c_str(),-1,0);
			sqlite3_bind_text(stmt, 3, txAmtMax.c_str(),-1,0);
			sqlite3_bind_text(stmt, 4, txAmtMin.c_str(),-1,0);
			sqlite3_bind_text(stmt, 5, limitsIndex.c_str(),-1,0);
			do
			{
				ret = sqlite3_step( stmt);
				switch( ret )
				{
					case SQLITE_DONE:
						break;

					case SQLITE_ROW:
						//returned data
						break;

					default:
						break;
				}
			}while(ret == SQLITE_ROW );
		} else {
			printf("SQL Error = %d\n",ret);
			sqlite3_finalize(stmt);
			close();
			return DB_FAIL;
		}

		sqlite3_reset(stmt);
		sqlite3_finalize(stmt);

		close();
		return DB_OK;
    }

    int CConManager::insertAlowedTransactions(std::string accProfileIndex, std::string code) {
    	sqlite3_stmt *stmt;
		sqlite3 *db = getDbConnection();
		std::string sql = "INSERT INTO allowed_transactions (account_profile_index,code) VALUES (?,?);";
		int ret = sqlite3_prepare_v2(db, sql.c_str(),-1,&stmt, 0);
		if (ret == SQLITE_OK) {
			sqlite3_bind_text(stmt, 1, accProfileIndex.c_str(),-1,0);
			sqlite3_bind_text(stmt, 2, code.c_str(),-1,0);
			do
			{
				ret = sqlite3_step( stmt);
				switch( ret )
				{
					case SQLITE_DONE:
						break;

					case SQLITE_ROW:
						//returned data
						break;

					default:
						break;
				}
			}while(ret == SQLITE_ROW );
		} else {
			printf("SQL Error = %d\n",ret);
			sqlite3_finalize(stmt);
			close();
			return DB_FAIL;
		}

		sqlite3_reset(stmt);
		sqlite3_finalize(stmt);

		//close();
		return  DB_OK;
    }

    int CConManager::insertAccountDescription(std::string code, std::string description) {
    	sqlite3_stmt *stmt;
		sqlite3 *db = getDbConnection();
		std::string sql = "INSERT INTO account_description (code,name) VALUES (?,?);";
		int ret = sqlite3_prepare_v2(db, sql.c_str(),-1,&stmt, 0);
		if (ret == SQLITE_OK) {
			sqlite3_bind_text(stmt, 1, code.c_str(),-1,0);
			sqlite3_bind_text(stmt, 2, description.c_str(),-1,0);
			do
			{
				ret = sqlite3_step( stmt);
				switch( ret )
				{
					case SQLITE_DONE:
						break;

					case SQLITE_ROW:
						//returned data
						break;

					default:
						break;
				}
			}while(ret == SQLITE_ROW );
		} else {
			printf("SQL Error = %d\n",ret);
			sqlite3_finalize(stmt);
			close();
			return DB_FAIL;
		}

		sqlite3_reset(stmt);
		sqlite3_finalize(stmt);


		//close();
		return  DB_OK;
    }

    int CConManager::insertAccountProfile(std::string accProfileIndex, std::string skimCardValue,std::string checkCardValue, std::string code,std::string allowVoiceAproval,std::string draftCaptureCode,std::string requireCardPinEntry,std::string emvFallBackIndex,std::string localLimitIndex,std::string OfflineLimitIndex,std::string requireSignature,std::string allowManualEntry,std::string allowBudget,std::string budgetAmtMin,std::string accountProfileHostId,std::string contactlessIndex) {
    	sqlite3_stmt *stmt;
		sqlite3 *db = getDbConnection();
		std::string sql = "INSERT INTO account_profile (code,draft_capture_mode,allow_voice_app,allow_manual_entry,require_pin_entry,require_signature,check_card_value,skim_card_value,allow_budget,budget_amount_min,emv_fallback_limit_index,local_limit_index,offline_limit_index,contactless_limit_index,account_profile_host_id,account_profile_index) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
		int ret = sqlite3_prepare_v2(db, sql.c_str(),-1,&stmt, 0);
		if (ret == SQLITE_OK) {
			sqlite3_bind_text(stmt, 1, code.c_str(),-1,0);
			sqlite3_bind_text(stmt, 2, draftCaptureCode.c_str(),-1,0);
			sqlite3_bind_text(stmt, 3, allowVoiceAproval.c_str(),-1,0);
			sqlite3_bind_text(stmt, 4, allowManualEntry.c_str(),-1,0);
			sqlite3_bind_text(stmt, 5, requireCardPinEntry.c_str(),-1,0);
			sqlite3_bind_text(stmt, 6, requireSignature.c_str(),-1,0);
			sqlite3_bind_text(stmt, 7, checkCardValue.c_str(),-1,0);
			sqlite3_bind_text(stmt, 8, skimCardValue.c_str(),-1,0);
			sqlite3_bind_text(stmt, 9, allowBudget.c_str(),-1,0);
			sqlite3_bind_text(stmt, 10, budgetAmtMin.c_str(),-1,0);
			sqlite3_bind_text(stmt, 11, emvFallBackIndex.c_str(),-1,0);
			sqlite3_bind_text(stmt, 12, localLimitIndex.c_str(),-1,0);
			sqlite3_bind_text(stmt, 13, OfflineLimitIndex.c_str(),-1,0);
			sqlite3_bind_text(stmt, 14, contactlessIndex.c_str(),-1,0);
			if (accountProfileHostId.empty())
				sqlite3_bind_text(stmt, 15, accProfileIndex.c_str(),-1,0);
			else
				sqlite3_bind_text(stmt, 15, accountProfileHostId.c_str(),-1,0);
			sqlite3_bind_text(stmt, 16, accProfileIndex.c_str(),-1,0);
			do
			{
				ret = sqlite3_step( stmt);
				switch( ret )
				{
					case SQLITE_DONE:
						break;

					case SQLITE_ROW:
						//returned data
						break;

					default:
						break;
				}
			}while(ret == SQLITE_ROW );
		} else {
			printf("SQL Error = %d\n",ret);
			sqlite3_finalize(stmt);
			close();
			return DB_FAIL;
		}

		sqlite3_reset(stmt);
		sqlite3_finalize(stmt);

		//close();
		return  DB_OK;
    }

    int CConManager::insertAccountProfileCurrency2(std::string accProfileIndex, std::string skimCardValue,std::string checkCardValue, std::string code,std::string allowVoiceAproval,std::string draftCaptureCode,std::string requireCardPinEntry,std::string emvFallBackIndex,std::string localLimitIndex,std::string OfflineLimitIndex,std::string requireSignature,std::string allowManualEntry,std::string allowBudget,std::string budgetAmtMin,std::string accountProfileHostId,std::string contactlessIndex) {
    	sqlite3_stmt *stmt;
		sqlite3 *db = getDbConnection();
		std::string sql = "INSERT INTO account_profile_curr2 (code,draft_capture_mode,allow_voice_app,allow_manual_entry,require_pin_entry,require_signature,check_card_value,skim_card_value,allow_budget,budget_amount_min,emv_fallback_limit_index,local_limit_index,offline_limit_index,contactless_limit_index,account_profile_host_id,account_profile_index) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
		int ret = sqlite3_prepare_v2(db, sql.c_str(),-1,&stmt, 0);
		if (ret == SQLITE_OK) {
			sqlite3_bind_text(stmt, 1, code.c_str(),-1,0);
			sqlite3_bind_text(stmt, 2, draftCaptureCode.c_str(),-1,0);
			sqlite3_bind_text(stmt, 3, allowVoiceAproval.c_str(),-1,0);
			sqlite3_bind_text(stmt, 4, allowManualEntry.c_str(),-1,0);
			sqlite3_bind_text(stmt, 5, requireCardPinEntry.c_str(),-1,0);
			sqlite3_bind_text(stmt, 6, requireSignature.c_str(),-1,0);
			sqlite3_bind_text(stmt, 7, checkCardValue.c_str(),-1,0);
			sqlite3_bind_text(stmt, 8, skimCardValue.c_str(),-1,0);
			sqlite3_bind_text(stmt, 9, allowBudget.c_str(),-1,0);
			sqlite3_bind_text(stmt, 10, budgetAmtMin.c_str(),-1,0);
			sqlite3_bind_text(stmt, 11, emvFallBackIndex.c_str(),-1,0);
			sqlite3_bind_text(stmt, 12, localLimitIndex.c_str(),-1,0);
			sqlite3_bind_text(stmt, 13, OfflineLimitIndex.c_str(),-1,0);
			sqlite3_bind_text(stmt, 14, contactlessIndex.c_str(),-1,0);
			if (accountProfileHostId.empty())
				sqlite3_bind_text(stmt, 15, accProfileIndex.c_str(),-1,0);
			else
				sqlite3_bind_text(stmt, 15, accountProfileHostId.c_str(),-1,0);
			sqlite3_bind_text(stmt, 16, accProfileIndex.c_str(),-1,0);
			do
			{
				ret = sqlite3_step( stmt);
				switch( ret )
				{
					case SQLITE_DONE:
						break;

					case SQLITE_ROW:
						//returned data
						break;

					default:
						break;
				}
			}while(ret == SQLITE_ROW );
		} else {
			printf("SQL Error = %d\n",ret);
			sqlite3_finalize(stmt);
			close();
			return DB_FAIL;
		}

		sqlite3_reset(stmt);
		sqlite3_finalize(stmt);

		//close();
		return  DB_OK;
    }

    int CConManager::insertAttachedAccounts(std::string cardProfileIndex,std::string accountProfileIndex,std::string totalsGroupName) {
    	sqlite3_stmt *stmt;
		sqlite3 *db = getDbConnection();
		std::string sql = "INSERT INTO attached_account (totals_group_name,card_profile_index,account_profile_index) VALUES (?,?,?);";
		int ret = sqlite3_prepare_v2(db, sql.c_str(),-1,&stmt, 0);
		if (ret == SQLITE_OK) {
			sqlite3_bind_text(stmt, 1, totalsGroupName.c_str(),-1,0);
			sqlite3_bind_text(stmt, 2, cardProfileIndex.c_str(),-1,0);
			sqlite3_bind_text(stmt, 3, accountProfileIndex.c_str(),-1,0);
			do
			{
				ret = sqlite3_step( stmt);
				switch( ret )
				{
					case SQLITE_DONE:
						break;

					case SQLITE_ROW:
						//returned data
						break;

					default:
						break;
				}
			}while(ret == SQLITE_ROW );
		} else {
			printf("SQL Error = %d\n",ret);
			sqlite3_finalize(stmt);
			close();
			return DB_FAIL;
		}

		sqlite3_reset(stmt);
		sqlite3_finalize(stmt);

		//close();
		return  DB_OK;
    }

    int CConManager::insertBin(std::string bin,std::string len,std::string cardProfileIndex,std::string issuedName) {
    	sqlite3_stmt *stmt;
		sqlite3 *db = getDbConnection();
		std::string sql = "INSERT INTO bin (bin,card_length,card_profile_index,card_issued_name) VALUES (?,?,?,?);";
		int ret = sqlite3_prepare_v2(db, sql.c_str(),-1,&stmt, 0);
		if (ret == SQLITE_OK) {
			sqlite3_bind_text(stmt, 1, bin.c_str(),-1,0);
			sqlite3_bind_text(stmt, 2, len.c_str(),-1,0);
			sqlite3_bind_text(stmt, 3, cardProfileIndex.c_str(),-1,0);
			sqlite3_bind_text(stmt, 4, issuedName.c_str(),-1,0);
			do
			{
				ret = sqlite3_step( stmt);
				switch( ret )
				{
					case SQLITE_DONE:
						break;

					case SQLITE_ROW:
						//returned data
						break;

					default:
						break;
				}
			}while(ret == SQLITE_ROW );
		} else {
			printf("SQL Error = %d\n",ret);
			sqlite3_finalize(stmt);
			close();
			return DB_FAIL;
		}

		sqlite3_reset(stmt);
		sqlite3_finalize(stmt);

		//close();
		return  DB_OK;

    }

    int CConManager::insertCommsParameters(std::string tableName,CCommsSettings &comsSettings) {
    	sqlite3_stmt *stmt;
		sqlite3 *db = getDbConnection();
		std::string sql = "INSERT INTO "+tableName+" (ip,port, phone_number,DTE,NUI,ip2, port2) VALUES (?,?,?,?,?,?,?);";
		int ret = sqlite3_prepare_v2(db, sql.c_str(),-1,&stmt, 0);
		if (ret == SQLITE_OK) {
			sqlite3_bind_text(stmt, 1, comsSettings.getIp().c_str(),-1,0);
			sqlite3_bind_text(stmt, 2, comsSettings.getPort().c_str(),-1,0);
			sqlite3_bind_text(stmt, 3, comsSettings.getPhoneNumber().c_str(),-1,0);
			sqlite3_bind_text(stmt, 4, comsSettings.getDte().c_str(),-1,0);
			sqlite3_bind_text(stmt, 5, comsSettings.getNui().c_str(),-1,0);
			sqlite3_bind_text(stmt, 6, comsSettings.getIp2().c_str(),-1,0);
			sqlite3_bind_text(stmt, 7, comsSettings.getPort2().c_str(),-1,0);

		do
			{
				ret = sqlite3_step( stmt);
				switch( ret )
				{
					case SQLITE_DONE:
						break;

					case SQLITE_ROW:
						//returned data
						break;

					default:
						break;
				}
			}while(ret == SQLITE_ROW );
		} else {
			printf("SQL Error = %d\n",ret);
			sqlite3_finalize(stmt);
			close();
			return -1;
		}

		sqlite3_reset(stmt);
		sqlite3_finalize(stmt);

		int id = sqlite3_last_insert_rowid(db);


		close();
		return  id;
    }
    int CConManager::insertFileWithVersion(std::string appName, CVersionsFile file) {
    	sqlite3_stmt *stmt;
		sqlite3 *db = getDbConnection();
		std::string sql = "INSERT INTO versions (app_name,file_name,path,version) VALUES (?,?,?,?);";
		int ret = sqlite3_prepare_v2(db, sql.c_str(),-1,&stmt, 0);
		if (ret == SQLITE_OK) {
			sqlite3_bind_text(stmt, 1, appName.c_str(),-1,0);
			sqlite3_bind_text(stmt, 2, file.getName().c_str(),-1,0);
			sqlite3_bind_text(stmt, 3, file.getPath().c_str(),-1,0);
			sqlite3_bind_text(stmt, 4, file.getVersion().c_str(),-1,0);
			{
				ret = sqlite3_step( stmt);
				switch( ret )
				{
					case SQLITE_DONE:
						break;

					case SQLITE_ROW:
						//returned data
						break;

					default:
						break;
				}
			}while(ret == SQLITE_ROW );
		} else {
			printf("SQL Error = %d\n",ret);
			sqlite3_finalize(stmt);
			close();
			return DB_FAIL;
		}

		sqlite3_reset(stmt);
		sqlite3_finalize(stmt);
		close();
		return  DB_OK;
    }

    int CConManager::insertPan(CPanInfo &panInfo) {
    	sqlite3_stmt *stmt;
		sqlite3 *db = getDbConnection();
		std::string sql = "INSERT INTO velocity (pan,counter,timestamp,last_used_timestamp) VALUES (?,?,?,?);";
		std::string counter = SSTR(panInfo.getCounter());
		std::string timestamp = SSTR(panInfo.getTimeStamp());
		std::string lastUsedTimestamp = SSTR(panInfo.getLastUsedTimestamp());
		int ret = sqlite3_prepare_v2(db, sql.c_str(),-1,&stmt, 0);
		if (ret == SQLITE_OK) {
			sqlite3_bind_text(stmt, 1, panInfo.getPan().c_str(),-1,0);
			sqlite3_bind_text(stmt, 2, counter.c_str(),-1,0);
			sqlite3_bind_text(stmt, 3, timestamp.c_str(),-1,0);
			sqlite3_bind_text(stmt, 4, lastUsedTimestamp.c_str(),-1,0);
			do
			{
				ret = sqlite3_step( stmt);
				switch( ret )
				{
					case SQLITE_DONE:
						break;

					case SQLITE_ROW:
						//returned data
						break;

					default:
						break;
				}
			}while(ret == SQLITE_ROW );
		} else {
			printf("SQL Error = %d\n",ret);
			sqlite3_finalize(stmt);
			close();
			return DB_FAIL;
		}

		sqlite3_reset(stmt);
		sqlite3_finalize(stmt);
		close();
		return  DB_OK;
    }

    int CConManager::insertBatch(CBatch &batch) {
    	sqlite3_stmt *stmt;
		sqlite3 *db = getDbConnection();
		std::string sql = "INSERT INTO batch (batch_no,datetime_opened,datetime_closed,state) VALUES (?,?,?,?);";
		std::string batchNo = SSTR(batch.getBatchNo());
		int ret = sqlite3_prepare_v2(db, sql.c_str(),-1,&stmt, 0);
		if (ret == SQLITE_OK) {
			sqlite3_bind_text(stmt, 1, batchNo.c_str(),-1,0);
			sqlite3_bind_text(stmt, 2, batch.getDateTimeOpened().c_str(),-1,0);
			sqlite3_bind_text(stmt, 3, batch.getDateTimeClosed().c_str(),-1,0);
			sqlite3_bind_text(stmt, 4, SSTR(batch.getState()).c_str(),-1,0);
			do
			{
				ret = sqlite3_step( stmt);
				switch( ret )
				{
					case SQLITE_DONE:
						break;

					case SQLITE_ROW:
						//returned data
						break;

					default:
						break;
				}
			}while(ret == SQLITE_ROW );
		} else {
			printf("SQL Error = %d\n",ret);
			sqlite3_finalize(stmt);
			close();
			return DB_FAIL;
		}

		sqlite3_reset(stmt);
		sqlite3_finalize(stmt);
		close();
		return  DB_OK;
    }

    int CConManager::insertBatchRec(CBatchRec &batchRec) {

    	sqlite3_stmt *stmt;
		sqlite3 *db = getDbConnection();
		std::string sql = "INSERT INTO batch_rec (supervisor_id,supervisor_name,cashier_id,cashier_name,cashier_pin_verified,waiter_id,table_no,src,icc_flag,tx_date_time,orig_tx_date_time,"
				"pan_entry_mode,auth_mode,card_auth_method,signature_required,tx_force_online,tx_force_offline,draft_capture_mode,tx_cancelled,approve_online,timeout_reversal,capture_flag,completion_code,"
				"cash_exclude_flag,acc_type,tx_type,cancelled_tx_type,tx_code,budget_period,rrn,auth_code,resp_code,non_iso_resp_code,card_holder_name,track2,acc_prof_index,pan,exp,cvv,totals_group,"
				"invoice_no,ref_no1,ref_no2,rcs_card,fleet_card,tsn,repeat_tsn,transmission_num,spdh_tsn,spdh_seq_no,cancelled_tsn,receipt_no,cancelled_receipt_no,batch_no,batch_txn_no,tx_amount,cash_amount,"
				"tip_amount,litres,onl_status,fraud_ind,product_info,emv_tags,odo_reading,fleet_card_usage,demo_tx,pay_request,in_progress,load_fees,card_fees,approved,declined,reason,ecr_no,resp_msg,tx_voided,trans_currency) "
				"VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		int ret = sqlite3_prepare_v2(db, sql.c_str(),-1,&stmt, 0);
		if (ret == SQLITE_OK) {
			sqlite3_bind_text(stmt, 1, batchRec.getSupervisorId().c_str(),-1,0);
			sqlite3_bind_text(stmt, 2, batchRec.getSupervisorName().c_str(),-1,0);
			sqlite3_bind_text(stmt, 3, batchRec.getCashierId().c_str(),-1,0);
			sqlite3_bind_text(stmt, 4, batchRec.getCashierName().c_str(),-1,0);
			sqlite3_bind_text(stmt, 5, batchRec.isCashierPinVerified()?"1":"0",-1,0);
			sqlite3_bind_text(stmt, 6, batchRec.getWaiterId().c_str(),-1,0);
			sqlite3_bind_text(stmt, 7, batchRec.getTableNo().c_str(),-1,0);
			sqlite3_bind_text(stmt, 8, batchRec.getSrc().c_str(),-1,0);
			sqlite3_bind_text(stmt, 9, batchRec.isIccFlag()?"1":"0",-1,0);
			sqlite3_bind_text(stmt, 10, batchRec.getTxDateTime().c_str(),-1,0);
			sqlite3_bind_text(stmt, 11, batchRec.getOrigTxDateTime().c_str(),-1,0);
			std::string panEntryMode = SSTR(batchRec.getPanEntryMode());
			sqlite3_bind_text(stmt, 12, panEntryMode.c_str(),-1,0);
			std::string authMode = SSTR(batchRec.getAuthMode());
			sqlite3_bind_text(stmt, 13, authMode.c_str(),-1,0);
			std::string cardAuthMethod = SSTR(batchRec.getCardhAuthMethod());
			sqlite3_bind_text(stmt, 14, cardAuthMethod.c_str(),-1,0);
			sqlite3_bind_text(stmt, 15, batchRec.isSignatureRequired()?"1":"0",-1,0);
			sqlite3_bind_text(stmt, 16, batchRec.isTxForceOnline()?"1":"0",-1,0);
			sqlite3_bind_text(stmt, 17, batchRec.isTxForceOffline()?"1":"0",-1,0);
			std::string draftCapMode = SSTR(batchRec.getDraftCapMode());
			sqlite3_bind_text(stmt, 18, draftCapMode.c_str(),-1,0);
			sqlite3_bind_text(stmt, 19, batchRec.isTxCanceled()?"1":"0",-1,0);
			sqlite3_bind_text(stmt, 20, batchRec.isApproveOnline()?"1":"0",-1,0);
			sqlite3_bind_text(stmt, 21, batchRec.isTimeoutReversal()?"1":"0",-1,0);
			std::string captureFlag = SSTR(batchRec.getCaptureFlag());
			sqlite3_bind_text(stmt, 22, captureFlag.c_str(),-1,0);
			sqlite3_bind_text(stmt, 23, batchRec.getCompletionCode().c_str(),-1,0);
			sqlite3_bind_text(stmt, 24, batchRec.isCashExcludeFlag()?"1":"0",-1,0);
			sqlite3_bind_text(stmt, 25, batchRec.getAccType().c_str(),-1,0);
			sqlite3_bind_text(stmt, 26, batchRec.getTxType().c_str(),-1,0);
			sqlite3_bind_text(stmt, 27, batchRec.getCanceledTxType().c_str(),-1,0);
			sqlite3_bind_text(stmt, 28, batchRec.getTxCode().c_str(),-1,0);
			sqlite3_bind_text(stmt, 29, batchRec.getBudgetPeriod().c_str(),-1,0);
			sqlite3_bind_text(stmt, 30, batchRec.getRrn().c_str(),-1,0);
			sqlite3_bind_text(stmt, 31, batchRec.getAuthCode().c_str(),-1,0);
			sqlite3_bind_text(stmt, 32, batchRec.getRespCode().c_str(),-1,0);
			sqlite3_bind_text(stmt, 33, batchRec.getNonIsoRespCode().c_str(),-1,0);
			sqlite3_bind_text(stmt, 34, batchRec.getCardholderName().c_str(),-1,0);
			sqlite3_bind_text(stmt, 35, batchRec.getTrack2().c_str(),-1,0);
			sqlite3_bind_text(stmt, 36, batchRec.getAccProfIndex().c_str(),-1,0);
			sqlite3_bind_text(stmt, 37, batchRec.getPan().c_str(),-1,0);
			sqlite3_bind_text(stmt, 38, batchRec.getExp().c_str(),-1,0);
			sqlite3_bind_text(stmt, 39, batchRec.getCvv().c_str(),-1,0);
			sqlite3_bind_text(stmt, 40, batchRec.getTotalsGroup().c_str(),-1,0);
			sqlite3_bind_text(stmt, 41, batchRec.getInvoiceNo().c_str(),-1,0);
			sqlite3_bind_text(stmt, 42, batchRec.getRefNo1().c_str(),-1,0);
			sqlite3_bind_text(stmt, 43, batchRec.getRefNo2().c_str(),-1,0);
			sqlite3_bind_text(stmt, 44, batchRec.isRcsCard()?"1":"0",-1,0);
			sqlite3_bind_text(stmt, 45, batchRec.isFleetCard()?"1":"0",-1,0);
			std::string tsn = SSTR(batchRec.getTsn());
			sqlite3_bind_text(stmt, 46, tsn.c_str(),-1,0);
			std::string repeatTsn = SSTR(batchRec.getRepeatTsn());
			sqlite3_bind_text(stmt, 47, repeatTsn.c_str(),-1,0);
			std::string transmissionNum = SSTR(batchRec.getTransmissionNum());
			sqlite3_bind_text(stmt, 48, transmissionNum.c_str(),-1,0);
			sqlite3_bind_text(stmt, 49, batchRec.getSpdhTsn().c_str(),-1,0);
			sqlite3_bind_text(stmt, 50, batchRec.getSpdhSeqNo().c_str(),-1,0);
			sqlite3_bind_text(stmt, 51, batchRec.getCancelledTsn().c_str(),-1,0);
			sqlite3_bind_text(stmt, 52, batchRec.getReceiptNo().c_str(),-1,0);
			sqlite3_bind_text(stmt, 53, batchRec.getCanceledReceiptNo().c_str(),-1,0);
			std::string batchNo = SSTR(batchRec.getBatchDbId());
			sqlite3_bind_text(stmt, 54, batchNo.c_str(),-1,0);
			std::string batchTxNo = SSTR(batchRec.getBatchTxNo());
			sqlite3_bind_text(stmt, 55, batchTxNo.c_str(),-1,0);
			std::string txAmount = SSTR(batchRec.getTxAmount());
			sqlite3_bind_text(stmt, 56, txAmount.c_str(),-1,0);
			std::string cashAmount = SSTR(batchRec.getCashAmount());
			sqlite3_bind_text(stmt, 57, cashAmount.c_str(),-1,0);
			std::string tipAmount = SSTR(batchRec.getTipAmount());
			sqlite3_bind_text(stmt, 58, tipAmount.c_str(),-1,0);
			std::string litres = SSTR(batchRec.getLitres());
			sqlite3_bind_text(stmt, 59, litres.c_str(),-1,0);
			COnlineStatus status =batchRec.getOnlStatus();
			std::string onlStatus = std::string(status.isEmvFallbackToMag()?"1":"0").append(status.isFloorLimit()?"1":"0").append(status.isManualEntry()?"1":"0").append(status.isNotHotcard()?"1":"0").append(status.isSrcCode()?"1":"0").append(status.isVelocity()?"1":"0");
			sqlite3_bind_text(stmt, 60, onlStatus.c_str(),-1,0);
			CFraudIndicator indicator = batchRec.getFraudInd();
			std::string fraudIndicator = std::string(indicator.isHotcard()?"1":"0").append(indicator.isSkimmedCard()?"1":"0");
			sqlite3_bind_text(stmt, 61, fraudIndicator.c_str(),-1,0);
			sqlite3_bind_text(stmt, 62, "",-1,0);
			std::string hex;
			toHex((void *)batchRec.getEmvTags().data(),batchRec.getEmvTags().length(),hex);
			sqlite3_bind_text(stmt, 63, hex.c_str(),-1,0);
			sqlite3_bind_text(stmt, 64, batchRec.getOdoReading().c_str(),-1,0);
			sqlite3_bind_text(stmt, 65, batchRec.getFleetCardUsage().c_str(),-1,0);
			sqlite3_bind_text(stmt, 66, batchRec.isDemoTx()?"1":"0",-1,0);
			sqlite3_bind_text(stmt, 67, batchRec.isPayRequest()?"1":"0",-1,0);
			sqlite3_bind_text(stmt, 68, batchRec.isInProgress()?"1":"0",-1,0);
			std::string loadFees = SSTR(batchRec.getLoadFees());
			sqlite3_bind_text(stmt, 69, loadFees.c_str(),-1,0);
			std::string cardFees = SSTR(batchRec.getCardFees());
			sqlite3_bind_text(stmt, 70, cardFees.c_str(),-1,0);
			sqlite3_bind_text(stmt, 71, batchRec.isApproved()?"1":"0",-1,0);
			sqlite3_bind_text(stmt, 72, batchRec.isDeclined()?"1":"0",-1,0);
			std::string reason = SSTR(batchRec.getReason());
			sqlite3_bind_text(stmt, 73, reason.c_str(),-1,0);
			sqlite3_bind_text(stmt, 74, batchRec.getEcrNo().c_str(),-1,0);	//iq_audi_020117
			sqlite3_bind_text(stmt, 75, batchRec.getRespMsg().c_str(),-1,0);	//iq_audi_020117
			sqlite3_bind_text(stmt, 76, batchRec.isTxVoided()?"1":"0",-1,0);
			sqlite3_bind_text(stmt, 77, batchRec.getTransCurrency().c_str(),-1,0);

			do
			{
				ret = sqlite3_step( stmt);
				switch( ret )
				{
					case SQLITE_DONE:
						break;

					case SQLITE_ROW:
						//returned data
						break;

					default:
						break;
				}
			}while(ret == SQLITE_ROW );
		} else {
			printf("SQL Error = %d\n",ret);
			sqlite3_finalize(stmt);
			close();
			return DB_FAIL;
		}

		sqlite3_reset(stmt);
		sqlite3_finalize(stmt);
		close();
		return  DB_OK;


    }

    int CConManager::insertUser(CUser &user) {
		sqlite3_stmt *stmt;
		sqlite3 *db = getDbConnection();
		std::string sql = "INSERT INTO user (name,id,pin,pin_required,banking_allowed,refunds_allowed,date_added,type) VALUES (?,?,?,?,?,?,?,?);";
		int ret = sqlite3_prepare_v2(db, sql.c_str(),-1,&stmt, 0);
		if (ret == SQLITE_OK) {
			sqlite3_bind_text(stmt, 1, user.getName().c_str(),-1,0);
			sqlite3_bind_text(stmt, 2, user.getId().c_str(),-1,0);
			sqlite3_bind_text(stmt, 3, user.getPin().c_str(),-1,0);
			sqlite3_bind_text(stmt, 4, user.isPinRequired()?"1":"0",-1,0);
			sqlite3_bind_text(stmt, 5, user.isBankingAllowed()?"1":"0",-1,0);
			sqlite3_bind_text(stmt, 6, user.isRefundsAllowed()?"1":"0",-1,0);
			sqlite3_bind_text(stmt, 7, user.getDateAdded().c_str(),-1,0);
			sqlite3_bind_text(stmt, 8, SSTR(user.getUserType()).c_str(),-1,0);
			do
			{
				ret = sqlite3_step( stmt);
				switch( ret )
				{
					case SQLITE_DONE:
						break;

					case SQLITE_ROW:
						//returned data
						break;

					default:
						break;
				}
			}while(ret == SQLITE_ROW );
		} else {
			printf("SQL Error = %d\n",ret);
			sqlite3_finalize(stmt);
			close();
			return DB_FAIL;
		}

		sqlite3_reset(stmt);
		sqlite3_finalize(stmt);
		close();
		return  DB_OK;
	}

    int CConManager::updateTerminalConfig(std::string name, std::string value) {
		sqlite3_stmt *stmt;
		sqlite3 *db = getDbConnection();
		std::string sql = "UPDATE terminal_config SET value = ? WHERE name = ?;";
		int ret = sqlite3_prepare_v2(db, sql.c_str(),-1,&stmt, 0);
		if (ret == SQLITE_OK) {
			sqlite3_bind_text(stmt, 1, value.c_str(),-1,0);
			sqlite3_bind_text(stmt, 2, name.c_str(),-1,0);
			do
			{
				ret = sqlite3_step( stmt);
				switch( ret )
				{
					case SQLITE_DONE:
						break;

					case SQLITE_ROW:
						//returned data
						break;

					default:
						break;
				}
			}while(ret == SQLITE_ROW );
		} else {
			sqlite3_finalize(stmt);
			close();
			return DB_FAIL;
		}

		sqlite3_reset(stmt);
		sqlite3_finalize(stmt);
		close();
		return  DB_OK;
	}

    int CConManager::updateStatus(std::string name, std::string value) {
		sqlite3_stmt *stmt;
		sqlite3 *db = getDbConnection();
		std::string sql = "UPDATE status SET value = ? WHERE app_name = ?;";
		int ret = sqlite3_prepare_v2(db, sql.c_str(),-1,&stmt, 0);
		if (ret == SQLITE_OK) {
			sqlite3_bind_text(stmt, 1, value.c_str(),-1,0);
			sqlite3_bind_text(stmt, 2, name.c_str(),-1,0);
			do
			{
				ret = sqlite3_step( stmt);
				switch( ret )
				{
					case SQLITE_DONE:
						break;

					case SQLITE_ROW:
						//returned data
						break;

					default:
						break;
				}
			}while(ret == SQLITE_ROW );
		} else {
			sqlite3_finalize(stmt);
			close();
			return DB_FAIL;
		}

		sqlite3_reset(stmt);
		sqlite3_finalize(stmt);
		close();
		return  DB_OK;
	}

    int CConManager::commsLog(std::string type, std::string status, std::string timestamp, std::string ip, std::string port, std::string seconds, std::string tx_count, std::string rx_count){
		sqlite3_stmt *stmt;
		sqlite3 *db = getDbConnection();

		std::string sql = "INSERT INTO Log (Timestamp,IP,Port,Type,Duration,BytesTx,BytesRx,Status) VALUES (?,?,?,?,?,?,?,?);";
		int ret = sqlite3_prepare_v2(db, sql.c_str(),-1,&stmt, 0);

		if (ret == SQLITE_OK) {
			sqlite3_bind_text(stmt, 1, timestamp.c_str(),	-1,0);
			sqlite3_bind_text(stmt, 2, ip.c_str(),			-1,0);
			sqlite3_bind_text(stmt, 3, port.c_str(),		-1,0);
			sqlite3_bind_text(stmt, 4, type.c_str(),		-1,0);
			sqlite3_bind_text(stmt, 5, seconds.c_str(),		-1,0);
			sqlite3_bind_text(stmt, 6, tx_count.c_str(),	-1,0);
			sqlite3_bind_text(stmt, 7, rx_count.c_str(),	-1,0);
			sqlite3_bind_text(stmt, 8, status.c_str(),		-1,0);

			do
			{
				ret = sqlite3_step( stmt);
				switch( ret )
				{
					case SQLITE_DONE:
						break;

					case SQLITE_ROW:
						//returned data
						break;

					default:
						break;
				}
			}while(ret == SQLITE_ROW );

		} else {
			printf("Error = %d\n",ret);
			sqlite3_finalize(stmt);
			close();
			return DB_FAIL;
		}

		sqlite3_reset(stmt);
		sqlite3_finalize(stmt);
		close();

		return  DB_OK;
	}

    int CConManager::getColumnValue(std::string columName, std::string &value, std::vector<std::vector<std::string> > &records, int rowNum) {
		int colNumValue = -1;

		// Get column index
		for (uint c=0;c<records[0].size();c++) {
			if (records[0][c].compare(columName) == 0)
				colNumValue = c;
		}

		if (colNumValue != -1) {
			value = records[rowNum][colNumValue];
			return DB_OK;
		} else
			return DB_FAIL;
	}

    void CConManager::toHex(void *const data,const size_t dataLength,std::string &dest)
	{
		unsigned char       *byteData = reinterpret_cast<unsigned char*>(data);
		std::stringstream   hexStringStream;

		hexStringStream << std::hex << std::setfill('0');
		for(size_t index = 0; index < dataLength; ++index)
			hexStringStream << std::setw(2) << static_cast<int>(byteData[index]);
		dest = hexStringStream.str();
	}
}
