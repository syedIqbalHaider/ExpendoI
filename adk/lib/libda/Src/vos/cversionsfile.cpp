#include <stdio.h>
#include <stdlib.h>
#include "libda/cversionsfile.h"


namespace com_verifone_versionsfile
{

	CVersionsFile::CVersionsFile() {}
	std::string& CVersionsFile::getName()
	{
		return name;
	}

	void CVersionsFile::setName( std::string name)
	{
		this->name = name;
	}

	 std::string& CVersionsFile::getPath()
	{
		return path;
	}

	void CVersionsFile::setPath( std::string path)
	{
		this->path = path;
	}

	 std::string& CVersionsFile::getVersion()
	{
		return version;
	}

	void CVersionsFile::setVersion( std::string version)
	{
		this->version = version;
	}

}
