#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <errno.h>
#include <string>     // std::string, std::stoi
#include "libda/cbatch.h"
#include "libda/cconmanager.h"
#include "libda/himdef.h"

using namespace com_verifone_conmanager;

namespace com_verifone_batch
{
	CConManager conManager;

	CBatch::CBatch():batchNo(0),state(CLOSED),batchDbId(0) {
	}

	int CBatch::getBatchNo()  {
		return batchNo;
	}

	void CBatch::setBatchNo(int batchNo) {
		this->batchNo = batchNo;
	}

	 std::string& CBatch::getDateTimeClosed()  {
		return dateTimeClosed;
	}

	void CBatch::setDateTimeClosed( std::string dateTimeClosed) {
		this->dateTimeClosed = dateTimeClosed;
	}

	 std::string& CBatch::getDateTimeOpened()  {
		return dateTimeOpened;
	}

	void CBatch::setDateTimeOpened( std::string dateTimeOpened) {
		this->dateTimeOpened = dateTimeOpened;
	}

	BATCH_STATE CBatch::getState()  {
		return state;
	}

	void CBatch::setState(BATCH_STATE state) {
		this->state = state;
	}

	int CBatch::writeRecToBatch(CBatchRec &batchRec) {
		std::vector<std::vector<std::string> > records;
		std::string sql = "DELETE FROM batch_rec WHERE tsn = '"+SSTR(batchRec.getTsn())+"';";
		int ret = conManager.writeToDB(sql);
		if (ret == DB_FAIL)
			return ret;

		batchRec.setBatchDbId(this->batchDbId);
		if (batchRec.getTxDateTime().empty())
			batchRec.setTxDateTime(getCurrentDateTime());

		ret = conManager.insertBatchRec(batchRec);

		return ret;
	}

	int CBatch::delLastRecFromBatchIfIncomplete() {
		std::vector<std::vector<std::string> > records;
		std::string sql = "DELETE FROM batch_rec WHERE batch_rec_id=(SELECT MAX(CAST(batch_rec_id as int)) FROM batch_rec ) AND in_progress = '1'";
		int ret = conManager.writeToDB(sql);

		return ret;
	}

	int CBatch::readLastRecFromBatch(CBatchRec &batchRec) {

		std::string sql = "SELECT * FROM batch_rec WHERE tsn = (SELECT MAX(CAST(tsn as int)) FROM batch_rec);";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1) {
			return DB_FAIL;
		}

		std::string val;
		int ret = conManager.getColumnValue("supervisor_id",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setSupervisorId(val);

		ret = conManager.getColumnValue("supervisor_name",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setSupervisorName(val);

		ret = conManager.getColumnValue("cashier_id",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setCashierId(val);

		ret = conManager.getColumnValue("cashier_name",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setCashierName(val);

		ret = conManager.getColumnValue("cashier_pin_verified",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setCashierPinVerified(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("waiter_id",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setWaiterId(val);

		ret = conManager.getColumnValue("table_no",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setTableNo(val);

		ret = conManager.getColumnValue("src",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setSrc(val);

		ret = conManager.getColumnValue("icc_flag",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setIccFlag(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("tx_date_time",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setTxDateTime(val);

		ret = conManager.getColumnValue("orig_tx_date_time",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setOrigTxDateTime(val);

		ret = conManager.getColumnValue("pan_entry_mode",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setPanEntryMode((PAN_ENTRY_MODE)atoi(val.c_str()));

		ret = conManager.getColumnValue("auth_mode",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setAuthMode((AUTH_MODE)atoi(val.c_str()));

		ret = conManager.getColumnValue("card_auth_method",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setCardhAuthMethod((CARD_AUTH_METHOD)atoi(val.c_str()));

		ret = conManager.getColumnValue("signature_required",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setSignatureRequired(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("tx_force_online",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setTxForceOnline(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("tx_force_offline",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setTxForceOffline(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("draft_capture_mode",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setDraftCapMode((DRAFT_CAPTURE_MODE)atoi(val.c_str()));

		ret = conManager.getColumnValue("tx_cancelled",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setTxCanceled(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("tx_voided",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setTxVoided(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("approve_online",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setApproveOnline(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("timeout_reversal",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setTimeoutReversal(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("capture_flag",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setCaptureFlag((CAPTURE_FLAG)atoi(val.c_str()));

		ret = conManager.getColumnValue("completion_code",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setCompletionCode(val);

		ret = conManager.getColumnValue("cash_exclude_flag",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setCashExcludeFlag(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("acc_type",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setAccType(val);

		ret = conManager.getColumnValue("tx_type",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setTxType(val);

		ret = conManager.getColumnValue("cancelled_tx_type",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setCanceledTxType(val);

		ret = conManager.getColumnValue("tx_code",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setTxCode(val);

		ret = conManager.getColumnValue("budget_period",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setBudgetPeriod(val);

		ret = conManager.getColumnValue("rrn",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setRrn(val);

		ret = conManager.getColumnValue("auth_code",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setAuthCode(val);

		ret = conManager.getColumnValue("resp_code",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setRespCode(val);

		ret = conManager.getColumnValue("non_iso_resp_code",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setNonIsoRespCode(val);

		ret = conManager.getColumnValue("card_holder_name",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setCardholderName(val);

		ret = conManager.getColumnValue("track2",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setTrack2(val);

		ret = conManager.getColumnValue("acc_prof_index",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setAccProfIndex(val);

		ret = conManager.getColumnValue("pan",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setPan(val);

		ret = conManager.getColumnValue("exp",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setExp(val);

		ret = conManager.getColumnValue("cvv",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setCvv(val);

		ret = conManager.getColumnValue("totals_group",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setTotalsGroup(val);

		ret = conManager.getColumnValue("invoice_no",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setInvoiceNo(val);

		ret = conManager.getColumnValue("ref_no1",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setRefNo1(val);

		ret = conManager.getColumnValue("ref_no2",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setRefNo2(val);

		ret = conManager.getColumnValue("rcs_card",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setRcsCard(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("fleet_card",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setFleetCard(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("tsn",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setTsn(atoi(val.c_str()));

		ret = conManager.getColumnValue("repeat_tsn",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setRepeatTsn(atoi(val.c_str()));

		ret = conManager.getColumnValue("transmission_num",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setTransmissionNum(atoi(val.c_str()));

		ret = conManager.getColumnValue("spdh_tsn",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setSpdhTsn(val.c_str());

		ret = conManager.getColumnValue("spdh_seq_no",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setSpdhSeqNo(val.c_str());

		ret = conManager.getColumnValue("cancelled_tsn",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setCancelledTsn(val.c_str());

		ret = conManager.getColumnValue("receipt_no",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setReceiptNo(val.c_str());

		//iq_audi_301216
		ret = conManager.getColumnValue("ecr_no",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setEcrNo(val.c_str());

		//iq_audi_070317
		ret = conManager.getColumnValue("resp_msg",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setRespMsg(val.c_str());

		ret = conManager.getColumnValue("cancelled_receipt_no",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setCanceledReceiptNo(val.c_str());

		ret = conManager.getColumnValue("batch_no",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setBatchDbId(atoi(val.c_str()));

		ret = conManager.getColumnValue("batch_txn_no",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setBatchTxNo(atoi(val.c_str()));

		ret = conManager.getColumnValue("tx_amount",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setTxAmount(atol(val.c_str()));

		ret = conManager.getColumnValue("cash_amount",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setCashAmount(atol(val.c_str()));

		ret = conManager.getColumnValue("tip_amount",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setTipAmount(atol(val.c_str()));

		ret = conManager.getColumnValue("litres",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setLitres(atol(val.c_str()));

		ret = conManager.getColumnValue("onl_status",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else {
			COnlineStatus onlineStatus;
			onlineStatus.setEmvFallbackToMag(val[0] == '1'?true:false);
			onlineStatus.setFloorLimit(val[1] == '1'?true:false);
			onlineStatus.setManualEntry(val[2] == '1'?true:false);
			onlineStatus.setNotHotcard(val[3] == '1'?true:false);
			onlineStatus.setSrcCode(val[4] == '1'?true:false);
			onlineStatus.setVelocity(val[5] == '1'?true:false);

			batchRec.setOnlStatus(onlineStatus);
		}

		ret = conManager.getColumnValue("fraud_ind",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else {
			CFraudIndicator fraudIndicator;
			fraudIndicator.setHotcard(val[0] == '1'?true:false);
			fraudIndicator.setSkimmedCard(val[1] == '1'?true:false);

			batchRec.setFraudInd(fraudIndicator);
		}

		ret = conManager.getColumnValue("emv_tags",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else {
			unsigned char bin[val.length()/2];
			fromHex(val,bin);
			std::string my_std_string(reinterpret_cast<const char *>(bin), val.length()/2);
			batchRec.setEmvTags(my_std_string);
		}

		ret = conManager.getColumnValue("odo_reading",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setOdoReading(val.c_str());


		ret = conManager.getColumnValue("product_info",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else {
			// todo
		}

		ret = conManager.getColumnValue("fleet_card_usage",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setFleetCardUsage(val.c_str());

		ret = conManager.getColumnValue("demo_tx",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setDemoTx(val.compare("1") == 0?true:false);


		ret = conManager.getColumnValue("pay_request",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setPayRequest(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("in_progress",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setInProgress(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("card_fees",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setCardFees(atol(val.c_str()));

		ret = conManager.getColumnValue("load_fees",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setLoadFees(atol(val.c_str()));

		ret = conManager.getColumnValue("approved",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setApproved(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("declined",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setDeclined(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("reason",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setReason(val);

		ret = conManager.getColumnValue("trans_currency",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setTransCurrency(val);


		return DB_OK;
	}

	int CBatch::getRecordCount() {
		std::map<unsigned int, CBatchRec> batchRecs;
		getRecords(batchRecs);
		return batchRecs.size();
	}

	int CBatch::getRecords(std::map<unsigned int, CBatchRec> &batchRecs) {
		std::vector<std::vector<std::string> > records;
		std::string sql = "SELECT * FROM batch_rec where batch_no = '"+SSTR(this->batchDbId)+"';";
		conManager.selectFromDB(sql.c_str(),records,true);
		if (records.size() <= 1)
			return DB_FAIL;

		for (int i=1; i<records.size(); i++) {
			CBatchRec batchRec;
			std::string val;
			int ret = conManager.getColumnValue("supervisor_id",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setSupervisorId(val);

			ret = conManager.getColumnValue("supervisor_name",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setSupervisorName(val);

			ret = conManager.getColumnValue("cashier_id",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setCashierId(val);

			ret = conManager.getColumnValue("cashier_name",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setCashierName(val);

			ret = conManager.getColumnValue("cashier_pin_verified",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setCashierPinVerified(val.compare("1") == 0?true:false);

			ret = conManager.getColumnValue("waiter_id",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setWaiterId(val);

			ret = conManager.getColumnValue("table_no",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setTableNo(val);

			ret = conManager.getColumnValue("src",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setSrc(val);

			ret = conManager.getColumnValue("icc_flag",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setIccFlag(val.compare("1") == 0?true:false);

			ret = conManager.getColumnValue("tx_date_time",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setTxDateTime(val);

			ret = conManager.getColumnValue("orig_tx_date_time",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setOrigTxDateTime(val);

			ret = conManager.getColumnValue("pan_entry_mode",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setPanEntryMode((PAN_ENTRY_MODE)atoi(val.c_str()));

			ret = conManager.getColumnValue("auth_mode",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setAuthMode((AUTH_MODE)atoi(val.c_str()));

			ret = conManager.getColumnValue("card_auth_method",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setCardhAuthMethod((CARD_AUTH_METHOD)atoi(val.c_str()));

			ret = conManager.getColumnValue("signature_required",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setSignatureRequired(val.compare("1") == 0?true:false);

			ret = conManager.getColumnValue("tx_force_online",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setTxForceOnline(val.compare("1") == 0?true:false);

			ret = conManager.getColumnValue("tx_force_offline",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setTxForceOffline(val.compare("1") == 0?true:false);

			ret = conManager.getColumnValue("draft_capture_mode",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setDraftCapMode((DRAFT_CAPTURE_MODE)atoi(val.c_str()));

			ret = conManager.getColumnValue("tx_cancelled",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setTxCanceled(val.compare("1") == 0?true:false);

			ret = conManager.getColumnValue("tx_voided",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setTxVoided(val.compare("1") == 0?true:false);

			ret = conManager.getColumnValue("approve_online",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setApproveOnline(val.compare("1") == 0?true:false);

			ret = conManager.getColumnValue("timeout_reversal",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setTimeoutReversal(val.compare("1") == 0?true:false);

			ret = conManager.getColumnValue("capture_flag",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setCaptureFlag((CAPTURE_FLAG)atoi(val.c_str()));

			ret = conManager.getColumnValue("completion_code",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setCompletionCode(val);

			ret = conManager.getColumnValue("cash_exclude_flag",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setCashExcludeFlag(val.compare("1") == 0?true:false);

			ret = conManager.getColumnValue("acc_type",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setAccType(val);

			ret = conManager.getColumnValue("tx_type",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setTxType(val);

			ret = conManager.getColumnValue("cancelled_tx_type",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setCanceledTxType(val);

			ret = conManager.getColumnValue("tx_code",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setTxCode(val);

			ret = conManager.getColumnValue("budget_period",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setBudgetPeriod(val);

			ret = conManager.getColumnValue("rrn",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setRrn(val);

			ret = conManager.getColumnValue("auth_code",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setAuthCode(val);

			ret = conManager.getColumnValue("resp_code",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setRespCode(val);

			ret = conManager.getColumnValue("non_iso_resp_code",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setNonIsoRespCode(val);

			ret = conManager.getColumnValue("card_holder_name",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setCardholderName(val);

			ret = conManager.getColumnValue("track2",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setTrack2(val);

			ret = conManager.getColumnValue("acc_prof_index",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setAccProfIndex(val);

			ret = conManager.getColumnValue("pan",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setPan(val);

			ret = conManager.getColumnValue("exp",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setExp(val);

			ret = conManager.getColumnValue("cvv",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setCvv(val);

			ret = conManager.getColumnValue("totals_group",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setTotalsGroup(val);

			ret = conManager.getColumnValue("invoice_no",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setInvoiceNo(val);

			ret = conManager.getColumnValue("ref_no1",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setRefNo1(val);

			ret = conManager.getColumnValue("ref_no2",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setRefNo2(val);

			ret = conManager.getColumnValue("rcs_card",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setRcsCard(val.compare("1") == 0?true:false);

			ret = conManager.getColumnValue("fleet_card",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setFleetCard(val.compare("1") == 0?true:false);

			ret = conManager.getColumnValue("tsn",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setTsn(atoi(val.c_str()));

			ret = conManager.getColumnValue("repeat_tsn",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setRepeatTsn(atoi(val.c_str()));

			ret = conManager.getColumnValue("transmission_num",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setTransmissionNum(atoi(val.c_str()));

			ret = conManager.getColumnValue("spdh_tsn",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setSpdhTsn(val.c_str());

			ret = conManager.getColumnValue("spdh_seq_no",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setSpdhSeqNo(val.c_str());

			ret = conManager.getColumnValue("cancelled_tsn",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setCancelledTsn(val.c_str());

			ret = conManager.getColumnValue("receipt_no",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setReceiptNo(val.c_str());

			ret = conManager.getColumnValue("cancelled_receipt_no",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setCanceledReceiptNo(val.c_str());

			ret = conManager.getColumnValue("batch_no",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setBatchDbId(atoi(val.c_str()));

			ret = conManager.getColumnValue("batch_txn_no",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setBatchTxNo(atoi(val.c_str()));

			ret = conManager.getColumnValue("tx_amount",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setTxAmount(atol(val.c_str()));

			ret = conManager.getColumnValue("cash_amount",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setCashAmount(atol(val.c_str()));

			ret = conManager.getColumnValue("tip_amount",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setTipAmount(atol(val.c_str()));

			ret = conManager.getColumnValue("litres",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setLitres(atol(val.c_str()));

			ret = conManager.getColumnValue("onl_status",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else {
				COnlineStatus onlineStatus;
				onlineStatus.setEmvFallbackToMag(val[0] == '1'?true:false);
				onlineStatus.setFloorLimit(val[1] == '1'?true:false);
				onlineStatus.setManualEntry(val[2] == '1'?true:false);
				onlineStatus.setNotHotcard(val[3] == '1'?true:false);
				onlineStatus.setSrcCode(val[4] == '1'?true:false);
				onlineStatus.setVelocity(val[5] == '1'?true:false);

				batchRec.setOnlStatus(onlineStatus);
			}

			ret = conManager.getColumnValue("fraud_ind",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else {
				CFraudIndicator fraudIndicator;
				fraudIndicator.setHotcard(val[0] == '1'?true:false);
				fraudIndicator.setSkimmedCard(val[1] == '1'?true:false);

				batchRec.setFraudInd(fraudIndicator);
			}

			ret = conManager.getColumnValue("emv_tags",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else {
				unsigned char bin[val.length()/2];
				fromHex(val,bin);
				std::string my_std_string(reinterpret_cast<const char *>(bin), val.length()/2);
				batchRec.setEmvTags(my_std_string);
			}

			ret = conManager.getColumnValue("odo_reading",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setOdoReading(val.c_str());


			ret = conManager.getColumnValue("product_info",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else {
				// todo
			}

			ret = conManager.getColumnValue("fleet_card_usage",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setFleetCardUsage(val.c_str());

			ret = conManager.getColumnValue("demo_tx",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setDemoTx(val.compare("1") == 0?true:false);


			ret = conManager.getColumnValue("pay_request",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setPayRequest(val.compare("1") == 0?true:false);

			ret = conManager.getColumnValue("in_progress",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setInProgress(val.compare("1") == 0?true:false);

			ret = conManager.getColumnValue("card_fees",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setCardFees(atol(val.c_str()));

			ret = conManager.getColumnValue("load_fees",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setLoadFees(atol(val.c_str()));

			ret = conManager.getColumnValue("approved",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setApproved(val.compare("1") == 0?true:false);

			ret = conManager.getColumnValue("declined",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setDeclined(val.compare("1") == 0?true:false);

			ret = conManager.getColumnValue("reason",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setReason(val);

			//iq_audi_301216
			ret = conManager.getColumnValue("ecr_no",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setEcrNo(val.c_str());

			ret = conManager.getColumnValue("trans_currency",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				batchRec.setTransCurrency(val);

			batchRecs[batchRec.getBatchTxNo()] = batchRec;
		}

		return DB_OK;
	}

	int CBatch::readRecFromBatch(CBatchRec &batchRec, int tsn) {

		std::string sql = "SELECT * FROM batch_rec WHERE tsn = '"+SSTR(tsn)+"' LIMIT 1;";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1) {
			return DB_FAIL;
		}


		std::string val;
		int ret = conManager.getColumnValue("supervisor_id",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setSupervisorId(val);

		ret = conManager.getColumnValue("supervisor_name",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setSupervisorName(val);

		ret = conManager.getColumnValue("cashier_id",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setCashierId(val);

		ret = conManager.getColumnValue("cashier_name",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setCashierName(val);

		ret = conManager.getColumnValue("cashier_pin_verified",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setCashierPinVerified(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("waiter_id",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setWaiterId(val);

		ret = conManager.getColumnValue("table_no",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setTableNo(val);

		ret = conManager.getColumnValue("src",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setSrc(val);

		ret = conManager.getColumnValue("icc_flag",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setIccFlag(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("tx_date_time",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setTxDateTime(val);

		ret = conManager.getColumnValue("orig_tx_date_time",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setOrigTxDateTime(val);

		ret = conManager.getColumnValue("pan_entry_mode",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setPanEntryMode((PAN_ENTRY_MODE)atoi(val.c_str()));

		ret = conManager.getColumnValue("auth_mode",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setAuthMode((AUTH_MODE)atoi(val.c_str()));

		ret = conManager.getColumnValue("card_auth_method",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setCardhAuthMethod((CARD_AUTH_METHOD)atoi(val.c_str()));

		ret = conManager.getColumnValue("signature_required",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setSignatureRequired(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("tx_force_online",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setTxForceOnline(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("tx_force_offline",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setTxForceOffline(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("draft_capture_mode",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setDraftCapMode((DRAFT_CAPTURE_MODE)atoi(val.c_str()));

		ret = conManager.getColumnValue("tx_cancelled",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setTxCanceled(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("tx_voided",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setTxVoided(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("approve_online",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setApproveOnline(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("timeout_reversal",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setTimeoutReversal(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("capture_flag",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setCaptureFlag((CAPTURE_FLAG)atoi(val.c_str()));

		ret = conManager.getColumnValue("completion_code",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setCompletionCode(val);

		ret = conManager.getColumnValue("cash_exclude_flag",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setCashExcludeFlag(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("acc_type",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setAccType(val);

		ret = conManager.getColumnValue("tx_type",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setTxType(val);

		ret = conManager.getColumnValue("cancelled_tx_type",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setCanceledTxType(val);

		ret = conManager.getColumnValue("tx_code",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setTxCode(val);

		ret = conManager.getColumnValue("budget_period",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setBudgetPeriod(val);

		ret = conManager.getColumnValue("rrn",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setRrn(val);

		ret = conManager.getColumnValue("auth_code",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setAuthCode(val);

		ret = conManager.getColumnValue("resp_code",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setRespCode(val);

		ret = conManager.getColumnValue("non_iso_resp_code",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setNonIsoRespCode(val);

		ret = conManager.getColumnValue("card_holder_name",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setCardholderName(val);

		ret = conManager.getColumnValue("track2",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setTrack2(val);

		ret = conManager.getColumnValue("acc_prof_index",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setAccProfIndex(val);

		ret = conManager.getColumnValue("pan",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setPan(val);

		ret = conManager.getColumnValue("exp",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setPan(val);

		ret = conManager.getColumnValue("cvv",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setCvv(val);

		ret = conManager.getColumnValue("totals_group",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setTotalsGroup(val);

		ret = conManager.getColumnValue("invoice_no",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setInvoiceNo(val);

		ret = conManager.getColumnValue("ref_no1",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setRefNo1(val);

		ret = conManager.getColumnValue("ref_no2",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setRefNo2(val);

		ret = conManager.getColumnValue("rcs_card",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setRcsCard(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("fleet_card",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setFleetCard(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("tsn",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setTsn(atoi(val.c_str()));

		ret = conManager.getColumnValue("repeat_tsn",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setRepeatTsn(atoi(val.c_str()));

		ret = conManager.getColumnValue("transmission_num",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setTransmissionNum(atoi(val.c_str()));

		ret = conManager.getColumnValue("spdh_tsn",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setSpdhTsn(val.c_str());

		ret = conManager.getColumnValue("spdh_seq_no",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setSpdhSeqNo(val.c_str());

		ret = conManager.getColumnValue("cancelled_tsn",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setCancelledTsn(val.c_str());

		ret = conManager.getColumnValue("receipt_no",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setReceiptNo(val.c_str());

		//iq_audi_301216
		ret = conManager.getColumnValue("ecr_no",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setEcrNo(val.c_str());

		ret = conManager.getColumnValue("cancelled_receipt_no",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setCanceledReceiptNo(val.c_str());

		ret = conManager.getColumnValue("batch_no",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setBatchDbId(atoi(val.c_str()));

		ret = conManager.getColumnValue("batch_txn_no",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setBatchTxNo(atoi(val.c_str()));

		ret = conManager.getColumnValue("tx_amount",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setTxAmount(atol(val.c_str()));

		ret = conManager.getColumnValue("cash_amount",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setCashAmount(atol(val.c_str()));

		ret = conManager.getColumnValue("tip_amount",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setTipAmount(atol(val.c_str()));

		ret = conManager.getColumnValue("litres",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setLitres(atol(val.c_str()));

		ret = conManager.getColumnValue("onl_status",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else {
			COnlineStatus onlineStatus;
			onlineStatus.setEmvFallbackToMag(val[0] == '1'?true:false);
			onlineStatus.setFloorLimit(val[1] == '1'?true:false);
			onlineStatus.setManualEntry(val[2] == '1'?true:false);
			onlineStatus.setNotHotcard(val[3] == '1'?true:false);
			onlineStatus.setSrcCode(val[4] == '1'?true:false);
			onlineStatus.setVelocity(val[5] == '1'?true:false);

			batchRec.setOnlStatus(onlineStatus);
		}

		ret = conManager.getColumnValue("fraud_ind",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else {
			CFraudIndicator fraudIndicator;
			fraudIndicator.setHotcard(val[0] == '1'?true:false);
			fraudIndicator.setSkimmedCard(val[1] == '1'?true:false);

			batchRec.setFraudInd(fraudIndicator);
		}

		ret = conManager.getColumnValue("emv_tags",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else {
			unsigned char bin[val.length()/2];
			fromHex(val,bin);
			std::string my_std_string(reinterpret_cast<const char *>(bin), val.length()/2);
			batchRec.setEmvTags(my_std_string);
		}

		ret = conManager.getColumnValue("odo_reading",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setOdoReading(val.c_str());


		ret = conManager.getColumnValue("product_info",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else {
			// todo
		}

		ret = conManager.getColumnValue("fleet_card_usage",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setFleetCardUsage(val.c_str());

		ret = conManager.getColumnValue("demo_tx",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setDemoTx(val.compare("1") == 0?true:false);


		ret = conManager.getColumnValue("pay_request",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setPayRequest(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("in_progress",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setInProgress(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("card_fees",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setCardFees(atol(val.c_str()));

		ret = conManager.getColumnValue("load_fees",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setLoadFees(atol(val.c_str()));

		ret = conManager.getColumnValue("approved",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setApproved(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("declined",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setDeclined(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("reason",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setReason(val);

		ret = conManager.getColumnValue("trans_currency",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batchRec.setTransCurrency(val);

		return DB_OK;
	}

	std::string CBatch::getCurrentDateTime() {
		time_t     now = time(0);
		struct tm  tstruct;
		char       buf[80];
		tstruct = *localtime(&now);
		strftime(buf, sizeof(buf), "%Y-%m-%d %X", &tstruct);

		return buf;
	}



		// ------------------------------------------------------------------
		/*!
		    Convert a hex string to a block of data
		*/
		void CBatch::fromHex(
		    const std::string &in,              //!< Input hex string
		    void *const data                    //!< Data store
		    )
		{
		    size_t          length      = in.length();
		    unsigned char   *byteData   = reinterpret_cast<unsigned char*>(data);

		    std::stringstream hexStringStream; hexStringStream >> std::hex;
		    for(size_t strIndex = 0, dataIndex = 0; strIndex < length; ++dataIndex)
		    {
		        // Read out and convert the string two characters at a time
		        const char tmpStr[3] = { in[strIndex++], in[strIndex++], 0 };

		        // Reset and fill the string stream
		        hexStringStream.clear();
		        hexStringStream.str(tmpStr);

		        // Do the conversion
		        int tmpValue = 0;
		        hexStringStream >> tmpValue;
		        byteData[dataIndex] = static_cast<unsigned char>(tmpValue);
		    }
		}


		int CBatch::getBatchDbId() {
			return batchDbId;
		}

		void CBatch::setBatchDbId(int batchDbId) {
			this->batchDbId = batchDbId;
		}

}
