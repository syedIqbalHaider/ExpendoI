#include <stdio.h>
#include <stdlib.h>
#include "libda/cconmanager.h"
#include "libda/climits.h"

using namespace com_verifone_conmanager;

namespace com_verifone_limits
{
	CConManager conManager;

	CLimits::CLimits() {
	}

	 std::string& CLimits::getCashbackAmtMax()  {
		return cashbackAmtMax;
	}

	void CLimits::setCashbackAmtMax( std::string cashbackAmtMax) {
		this->cashbackAmtMax = cashbackAmtMax;
	}

	 std::string& CLimits::getFloorLimitAmt()  {
		return floorLimitAmt;
	}

	void CLimits::setFloorLimitAmt( std::string floorLimitAmt) {
		this->floorLimitAmt = floorLimitAmt;
	}

	 std::string& CLimits::getTransactionAmtMax()  {
		return transactionAmtMax;
	}

	void CLimits::setTransactionAmtMax( std::string transactionAmtMax) {
		this->transactionAmtMax = transactionAmtMax;
	}

	 std::string& CLimits::getTransactionAmtMin()  {
		return transactionAmtMin;
	}

	void CLimits::setTransactionAmtMin( std::string transactionAmtMin) {
		this->transactionAmtMin = transactionAmtMin;
	}
}
