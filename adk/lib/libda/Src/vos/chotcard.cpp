#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <errno.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <boost/foreach.hpp>
#include <boost/tokenizer.hpp>
#include "libda/cconmanager.h"
#include "libda/chotcard.h"
#include "libda/cversions.h"
#include "libda/cversionsfile.h"

using namespace com_verifone_conmanager;
using namespace com_verifone_versions;
using namespace com_verifone_versionsfile;

namespace com_verifone_hotcard
{
	CConManager conManager;

	CHotcard::CHotcard() {
	}

	int CHotcard::isHotCard(std::string pan) {
		std::string sql = "SELECT * FROM hotcards WHERE pan = '"+pan+"';";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1)
			return DB_FAIL;
		else
			return DB_OK;
	}
	int CHotcard::deleteHotCard(std::string pan) {
		std::string sql = "DELETE FROM hotcards WHERE pan = '"+pan+"';";
		conManager.writeToDB(sql);
		return DB_OK;
	}
	int CHotcard::addHotCard(std::string pan) {
		std::string sql = "DELETE FROM hotcards WHERE pan = '"+pan+"';";
		conManager.writeToDB(sql);
		sql = "INSERT INTO hotcards (pan) VALUES ('"+pan+"');";
		conManager.writeToDB(sql);
		return DB_OK;
	}

	int CHotcard::getRecordCount() {
		std::string sql = "SELECT COUNT(*) AS cnt FROM hotcards;";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1)
			return DB_FAIL;

		std::string val;
		int ret = conManager.getColumnValue("cnt",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			return atoi(val.c_str());
	}

	int CHotcard::getVersion(std::string &version) {
		CVersions versions;

		std::map<int,CVersionsFile> files;
		int ret = versions.getFiles(VERSIONS_PARM_APP_NAME,files);
		if (ret != DB_OK)
			return DB_FAIL;
		else {
			std::map<int,CVersionsFile>::iterator it2;
			for (it2=files.begin(); it2!=files.end(); ++it2) {
				CVersionsFile file = it2->second;

				if (file.getName().compare(VERSIONS_PARM_HOTC_FILE_NAME) == 0) {
					version = file.getVersion();
					return DB_OK;
				}
			}
		}

		return DB_FAIL;
	}

	int CHotcard::importHotcards(std::string path, HIM him, std::string hcFileName) {
		switch (him) {
			case ALIENO:
				return processAlienoHotcardFile(path,hcFileName);
			case ISO:
				return processIsoHotcardFile(path,hcFileName);
			default:
				return DB_FAIL;
		}
		return DB_OK;
	}

	int CHotcard::processIsoHotcardFile(std::string path, std::string hcFileName) {
		bool nextByteEscaped = false;
		std::string line;
		int ret;

		if (hcFileName.compare("hot_crd.dat") == 0) {
			// we close the database connection in the end to speed up the import to database
			std::string deleteSql = "DELETE FROM hotcards;";
			conManager.writeToDB(deleteSql);
			deleteSql = "DELETE FROM sqlite_sequence WHERE name='hotcards'";
			conManager.writeToDB(deleteSql,false);
		}

		std::ifstream ifs(std::string(path).append("/").append(hcFileName).c_str());
		if (!ifs.is_open())
			return DB_FAIL;

		std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());

		if (str.length() < 4)
			return DB_OK;

		std::string version = str.substr(0,4);

		str = str.substr(4,str.length()-4); // version
		int i=0;
		int len = str.length();
		bool add;
		if (hcFileName.compare("hot_crd.dat") == 0 || hcFileName.compare("HC_add.dat") == 0)
			add = true;
		else
			add = false;

		std::string sql = "INSERT INTO hotcards (pan) SELECT ";
		int cnt=0;
		do {
			if (add) {
				std::string asciiPan;
				bcdToAscii((const unsigned char *)str.substr(i,10).c_str(),10,asciiPan);
				asciiPan = asciiPan.substr(0,asciiPan.find("F"));

				if (cnt == 0)
					sql = sql + "'"+asciiPan + "' AS pan\r\n";
				else
					sql = sql + " UNION SELECT '"+ asciiPan+"'\r\n";

				cnt ++;

				if (cnt > 499) {
					ret = conManager.writeToDB(sql, false);
					/*if (ret == DB_FAIL) {
						// duplicate
						std::string deleteSql = "DELETE FROM hotcards;";
						conManager.writeToDB(deleteSql);
						deleteSql = "DELETE FROM sqlite_sequence WHERE name='hotcards'";
						conManager.writeToDB(deleteSql, false);
						// retry insert
						conManager.writeToDB(sql, false);
					}*/
					sql = "INSERT INTO hotcards (pan) SELECT ";
					cnt = 0;
				}

			} else {
				ret = processPan(str.substr(i,10),add);
				if (ret != DB_OK) {
					conManager.close(); //close the connection
					return ret;
				}
			}
			i+=12; // includes 2 bytes at end
		} while( i < len);

		if (add && cnt>0) {
			ret = conManager.writeToDB(sql, false);
			/*if (ret == DB_FAIL) {
				// duplicate
				std::string deleteSql = "DELETE FROM hotcards;";
				conManager.writeToDB(deleteSql);
				deleteSql = "DELETE FROM sqlite_sequence WHERE name='hotcards'";
				conManager.writeToDB(deleteSql, false);
				// retry insert
				conManager.writeToDB(sql, false);
			}*/
		}

		conManager.close(); //close the connection

		CVersions versions;
		CVersionsFile file;
		file.setName(VERSIONS_PARM_HOTC_FILE_NAME);
		file.setPath(std::string(path).append("/").append(hcFileName));
		file.setVersion(version);
		versions.addFile(VERSIONS_PARM_APP_NAME,file);

		return DB_OK;

	}

	int CHotcard::processAlienoHotcardFile(std::string path, std::string hcFileName) {
		bool nextByteEscaped = false;
		std::string line;
		int ret;

		// we close the database connection in the end to speed up the import to database
		std::string sql = "DELETE FROM hotcards;";
		conManager.writeToDB(sql);
		sql = "DELETE FROM sqlite_sequence WHERE name='hotcards'";
		conManager.writeToDB(sql);

		std::ifstream ifs(std::string(HOTCARD_PATH).append("/").append(hcFileName).c_str());
		if (!ifs.is_open())
			return DB_FAIL;

		std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());

		str = str.substr(24,str.length()-24);
		int i=0;
		int len = str.length();
		do {
			ret = processPan(str.substr(i,10), true);
			if (ret != DB_OK) {
				conManager.close(); //close the connection
				return ret;
			}
			i+=10;
		} while( i < len);
		conManager.close(); //close the connection
		return DB_OK;

	}
	int CHotcard::processPan(std::string bcdPan, bool add) {

		std::string asciiPan;
		//printf("bcd pan = %s\n",bcdPan.c_str());
		bcdToAscii((const unsigned char *)bcdPan.c_str(),10,asciiPan);

		asciiPan = asciiPan.substr(0,asciiPan.find("F"));
		std::string sql;
		if (add) {
			sql = "DELETE FROM hotcards WHERE pan = '"+asciiPan+"';";
			conManager.writeToDB(sql, false);
			sql = "INSERT INTO hotcards (pan) VALUES ('"+asciiPan+"');";
			conManager.writeToDB(sql, false);
		} else {
			sql = "DELETE FROM hotcards WHERE pan = '"+asciiPan+"';";
			conManager.writeToDB(sql, false);
		}

		conManager.writeToDB(sql, false);
		return DB_OK;
	}

	bool CHotcard::bcdToAscii(const unsigned char* bcdPan, const int length, std::string& asciiPan)
	{
		char tmpPan[3];

		for(int i=0;i<length;i++) {
			sprintf(tmpPan,"%02X", bcdPan[i]);
			asciiPan += tmpPan;
		}
		return true;
	}
}
