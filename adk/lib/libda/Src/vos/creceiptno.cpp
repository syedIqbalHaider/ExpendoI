#include <stdio.h>
#include <stdlib.h>
#include "libda/creceiptno.h"
#include "libda/cconmanager.h"


using namespace com_verifone_conmanager;

namespace com_verifone_receiptno
{

	CConManager conManager;

	CReceiptNo::CReceiptNo(){}

	int CReceiptNo::getNextReceiptNo() {
		std::string sql = "SELECT * FROM receipt_no WHERE receipt_no_id = 1";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1)
			return DB_FAIL;

		std::string val;
		int ret = conManager.getColumnValue("receipt_no",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;

		int receiptNo = atoi(val.c_str());

		//iq_audi_311216
		if(receiptNo>=999999)
			receiptNo=0;

		receiptNo += 1;
		sql = "UPDATE receipt_no SET receipt_no = "+SSTR(receiptNo)+" WHERE receipt_no_id = 1";
		conManager.writeToDB(sql);

		return receiptNo;
	}

	int CReceiptNo::getCurrentReceiptNo() {
		std::string sql = "SELECT * FROM receipt_no WHERE receipt_no_id = 1";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1)
			return DB_FAIL;

		std::string val;
		int ret = conManager.getColumnValue("receipt_no",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;

		int receiptNo = atoi(val.c_str());
		return receiptNo;
	}

	void CReceiptNo::resetReceiptNo(int tsn) {
		std::string sql = "UPDATE receipt_no SET receipt_no = "+SSTR(tsn)+" WHERE receipt_no_id = 1";
		conManager.writeToDB(sql);
	}


	CEcrNumber::CEcrNumber(){}

	int CEcrNumber::getEcrNo() {
		std::string sql = "SELECT * FROM ecr_no WHERE ecr_no_id = 1";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1)
			return DB_FAIL;

		std::string val;
		int ret = conManager.getColumnValue("ecr_no",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;

		int ecrNo = atoi(val.c_str());
		return ecrNo;
	}


	void CEcrNumber::setEcrNo(int ecrNo) {
		std::string sql = "UPDATE ecr_no SET ecr_no = "+SSTR(ecrNo)+" WHERE ecr_no_id = 1";
		conManager.writeToDB(sql);
	}

}
