#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <errno.h>
#include <boost/foreach.hpp>
#include <boost/tokenizer.hpp>
#include "libda/cconmanager.h"
#include "libda/ccardconfig.h"
#include "libda/himdef.h"
#include "libda/cversions.h"
#include "libda/cversionsfile.h"

using namespace com_verifone_conmanager;
using namespace com_verifone_versions;
using namespace com_verifone_versionsfile;

namespace com_verifone_cardconfig
{
	CConManager conManager;

	CCardConfig::CCardConfig(){}

	int CCardConfig::lookupCardConfig(std::string pan, CCardData &cardData, std::string currency) {
		std::vector<std::vector<std::string> > records;
		bool gotIt = false;
		for (int i=pan.length(); i>0 ; i--) {
			std::string sql = "SELECT * FROM bin where bin = '"+pan.substr(0,i)+"';";
			records.clear();
			conManager.selectFromDB(sql.c_str(),records,false);
			if (records.size() > 1) {
				gotIt = true;
				break;
			}

//			printf("pan.substr(0,i) [%s] \r\n",pan.substr(0,i).c_str());
		}

		if (!gotIt) {
//			printf("FAIL GOT IT\r\n");

			conManager.close();
			return DB_FAIL;
		}

//		printf("GOT IT\r\n");

		CCardData carData;
		std::string val;
		int ret = conManager.getColumnValue("bin",val,records,1);
		if (ret != DB_OK) {
			conManager.close();
			return DB_FAIL;
		} else
			cardData.setBin(val);

		ret = conManager.getColumnValue("card_length",val,records,1);
		if (ret != DB_OK) {
			conManager.close();
			return DB_FAIL;
		} else
			cardData.setCardLength(atoi(val.c_str()));

		ret = conManager.getColumnValue("card_issued_name",val,records,1);
		if (ret != DB_OK) {
			conManager.close();
			return DB_FAIL;
		} else
			cardData.setCardIssuedName(val);

		ret = conManager.getColumnValue("excluded",val,records,1);
		if (ret != DB_OK) {
			conManager.close();
			return DB_FAIL;
		} else
			cardData.setExcluded(val.compare("1") == 0?true:false);

		int cardProductId;
		ret = conManager.getColumnValue("card_product_id",val,records,1);
		if (ret != DB_OK) {
			conManager.close();
			return DB_FAIL;
		} else {
			cardProductId = atoi(val.c_str());
			cardData.setCardProductId(cardProductId);
		}

//		printf("BEFORE CARD PROFILE\r\n");

		ret=populateCardProfile(cardProductId,cardData);
		if (ret != DB_OK) {
			conManager.close();
			return DB_FAIL;
		}

//		printf("AFTER CARD PROFILE\r\n");

		std::map<std::string, CAccountProfile> accounts;
		ret=DB_FAIL;

		if(currency.compare("840")==0)
			ret = populateAccounts(cardProductId,accounts);
		else if(currency.compare("422")==0)
			ret = populateAccountsCurrency2(cardProductId,accounts);

		if (ret != DB_OK) {
			conManager.close();
			return DB_FAIL;
		}

//		printf("IQBAL LAST\r\n");

		cardData.setAccounts(accounts);

		conManager.close();

		return DB_OK;
	}

	int CCardConfig::populateAccountDescription(std::string code, std::string &description) {

		std::vector<std::vector<std::string> > records;
		std::string sql = "SELECT * FROM account_description WHERE code = '"+code+"';";
		conManager.selectFromDB(sql.c_str(),records,false);
		if (records.size() <= 1)
			return DB_FAIL;

		int ret = conManager.getColumnValue("name",description,records,1);
		if (ret != DB_OK)
			return DB_FAIL;

		return DB_OK;
	}

	int CCardConfig::populateEmvAccountProfile(int accountProfileHostId,CAccountProfile &accountProfile) {
		std::vector<std::vector<std::string> > records;
		std::string sql = "SELECT * FROM account_profile WHERE account_profile_host_id = "+SSTR(accountProfileHostId)+";";
		conManager.selectFromDB(sql.c_str(),records,false);
		if (records.size() <= 1)
			return DB_FAIL;

		std::string val;
		int ret = conManager.getColumnValue("code",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else {
			accountProfile.setCode(val);
			std::string description;
			populateAccountDescription(val,description);
			accountProfile.setDescription(description);
		}

		ret = conManager.getColumnValue("draft_capture_mode",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			accountProfile.setDraftCaptureMode(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("allow_voice_app",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			accountProfile.setAllowVoiceApproval(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("allow_manual_entry",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			accountProfile.setAllowManualEntry(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("require_pin_entry",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			accountProfile.setRequirePinEntry(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("require_signature",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			accountProfile.setRequireSignature(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("check_card_value",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			accountProfile.setCheckCardValue(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("skim_card_value",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			accountProfile.setSkimCardValue(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("allow_budget",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			accountProfile.setAllowBudget(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("budget_amount_min",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			accountProfile.setBudgetAmountMin(val);

		ret = conManager.getColumnValue("budget_amount_min",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			accountProfile.setBudgetAmountMin(val);

		ret = conManager.getColumnValue("emv_fallback_limit_index",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else {
			CLimits limits;
			ret = poplateLimits(atoi(val.c_str()),limits);
			if (ret != DB_OK)
				return DB_FAIL;

			accountProfile.setEmvFallBackLimits(limits);
		}

		ret = conManager.getColumnValue("contactless_limit_index",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else {
			CLimits limits;
			ret = poplateLimits(atoi(val.c_str()),limits);
			if (ret != DB_OK)
				return DB_FAIL;

			accountProfile.setContactLessLimits(limits);
		}

		ret = conManager.getColumnValue("local_limit_index",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else {
			CLimits limits;
			ret = poplateLimits(atoi(val.c_str()),limits);
			if (ret != DB_OK)
				return DB_FAIL;

			accountProfile.setLocalLimits(limits);
		}

		ret = conManager.getColumnValue("offline_limit_index",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else {
			CLimits limits;
			ret = poplateLimits(atoi(val.c_str()),limits);
			if (ret != DB_OK)
				return DB_FAIL;

			accountProfile.setOfflineLimits(limits);
		}

		ret = conManager.getColumnValue("account_profile_index",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else {
			int accountProfileIndex = atoi(val.c_str());

			std::set<std::string> allowedtransactions;
			ret = populateAllowedTransactions(accountProfileIndex,allowedtransactions);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				accountProfile.setAllowedTransactions(allowedtransactions);
		}

		return DB_OK;
	}

	int CCardConfig::populateEmvAccountProfileCurrency2(int accountProfileHostId,CAccountProfile &accountProfile) {
		std::vector<std::vector<std::string> > records;
		std::string sql = "SELECT * FROM account_profile_curr2 WHERE account_profile_host_id = "+SSTR(accountProfileHostId)+";";
		conManager.selectFromDB(sql.c_str(),records,false);
		if (records.size() <= 1)
			return DB_FAIL;

		std::string val;
		int ret = conManager.getColumnValue("code",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else {
			accountProfile.setCode(val);
			std::string description;
			populateAccountDescription(val,description);
			accountProfile.setDescription(description);
		}

		ret = conManager.getColumnValue("draft_capture_mode",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			accountProfile.setDraftCaptureMode(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("allow_voice_app",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			accountProfile.setAllowVoiceApproval(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("allow_manual_entry",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			accountProfile.setAllowManualEntry(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("require_pin_entry",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			accountProfile.setRequirePinEntry(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("require_signature",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			accountProfile.setRequireSignature(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("check_card_value",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			accountProfile.setCheckCardValue(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("skim_card_value",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			accountProfile.setSkimCardValue(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("allow_budget",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			accountProfile.setAllowBudget(val.compare("1") == 0?true:false);

		ret = conManager.getColumnValue("budget_amount_min",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			accountProfile.setBudgetAmountMin(val);

		ret = conManager.getColumnValue("budget_amount_min",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			accountProfile.setBudgetAmountMin(val);

		ret = conManager.getColumnValue("emv_fallback_limit_index",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else {
			CLimits limits;
			ret = poplateLimits(atoi(val.c_str()),limits);
			if (ret != DB_OK)
				return DB_FAIL;

			accountProfile.setEmvFallBackLimits(limits);
		}

		ret = conManager.getColumnValue("contactless_limit_index",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else {
			CLimits limits;
			ret = poplateLimits(atoi(val.c_str()),limits);
			if (ret != DB_OK)
				return DB_FAIL;

			accountProfile.setContactLessLimits(limits);
		}

		ret = conManager.getColumnValue("local_limit_index",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else {
			CLimits limits;
			ret = poplateLimits(atoi(val.c_str()),limits);
			if (ret != DB_OK)
				return DB_FAIL;

			accountProfile.setLocalLimits(limits);
		}

		ret = conManager.getColumnValue("offline_limit_index",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else {
			CLimits limits;
			ret = poplateLimits(atoi(val.c_str()),limits);
			if (ret != DB_OK)
				return DB_FAIL;

			accountProfile.setOfflineLimits(limits);
		}

		ret = conManager.getColumnValue("account_profile_index",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else {
			int accountProfileIndex = atoi(val.c_str());

			std::set<std::string> allowedtransactions;
			ret = populateAllowedTransactions(accountProfileIndex,allowedtransactions);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				accountProfile.setAllowedTransactions(allowedtransactions);
		}


		return DB_OK;
	}


	int CCardConfig::populatePublicKey(std::string rid, std::map<std::string, CPublicKey> &publicKeys) {

//		printf("iqbal -RID [%s]\n",rid.c_str());

		std::string lowerRid = rid;
		lowerRid[0] = std::tolower(rid[0]);

		std::vector<std::vector<std::string> > records;
		std::string sql = "SELECT * FROM public_key where rid = '"+rid+"' or rid = '"+lowerRid+"';";
		records.clear();

		conManager.selectFromDB(sql.c_str(),records,false);

		if (records.size() <= 1){
//			printf("No public key record found \r\n");
			return DB_FAIL;
		}

		for (int i=1; i<records.size(); i++) {
			CPublicKey publicKey;
			std::string val;
			int ret = conManager.getColumnValue("rid",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				publicKey.setRid(val);

			ret = conManager.getColumnValue("public_key_index",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				publicKey.setPublicKeyIndex(val);

			ret = conManager.getColumnValue("hash_algorithm_indicator",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				publicKey.setHashAlgorithIndicator(val);

			ret = conManager.getColumnValue("public_key_modulus",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				publicKey.setModulus(val);

			ret = conManager.getColumnValue("public_key_exponent",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				publicKey.setExponent(val);

			ret = conManager.getColumnValue("public_key_check_sum",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				publicKey.setCheckSum(val);

			ret = conManager.getColumnValue("key_activation",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				publicKey.setActivation(val);

			ret = conManager.getColumnValue("key_expiry",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				publicKey.setExpiry(val);

			ret = conManager.getColumnValue("key_algorithm_indicator",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				publicKey.setKeyAlgorithmIndicator(val);

			publicKeys[publicKey.getPublicKeyIndex()] = publicKey;

		}

		return DB_OK;
	}

	int CCardConfig::lookupAidOverride(std::string bin, std::string aid, CEmvData &emvData, std::string currency) {

//		printf("AID OVERRIDE - Aid = %s\n",aid.c_str());

		CCardData cardData;
		if (lookupCardConfig(bin,cardData, currency) != DB_OK)
			return DB_FAIL;

//		printf("AID OVERRIDE - Aid = %s\n",aid.c_str());
//		printf("AID OVERRIDE - Card Product Id = %d\n",cardData.getCardProductId());

		std::vector<std::vector<std::string> > records;
		std::string upperAid = aid;
		for (int i = 0; i < aid.size(); i++)
			upperAid[i] = std::toupper(aid[i]);
		std::string sql = "SELECT * FROM aid_override where (aid = '"+aid+"' or aid = '"+upperAid+"') and card_product_id = '"+SSTR(cardData.getCardProductId())+"';";
		records.clear();
		conManager.selectFromDB(sql.c_str(),records,false);
		if (records.size() <= 1) {
			conManager.close();
//			printf("iqbal - No Aid Override found\n",aid.c_str());
			return DB_FAIL;
		}

		int accountProfileId;
		std::string val;
		int ret = conManager.getColumnValue("account_profile_id",val,records,1);
		if (ret != DB_OK) {
			conManager.close();
			return DB_FAIL;
		} else
			accountProfileId = atoi(val.c_str());

		std::string totalsGroup;
		ret = conManager.getColumnValue("totals_group",val,records,1);
		if (ret != DB_OK) {
			conManager.close();
			return DB_FAIL;
		} else
			totalsGroup = val;

		// lookup the new account profile
		emvData.setTotalsGroupName(totalsGroup);
		std::string superTotalsGroupName;
		ret = populateSuperTotalsGroup(val,superTotalsGroupName);
		if (ret == DB_OK)
			emvData.setSuperTotalsGroupName(superTotalsGroupName);

		CAccountProfile accountProfile;

		if(currency.compare("840")==0)
			populateEmvAccountProfile(accountProfileId,accountProfile);
		else if(currency.compare("422")==0)
			populateEmvAccountProfileCurrency2(accountProfileId,accountProfile);

		emvData.setAccountProfile(accountProfile);
		return DB_OK;
	}

	int CCardConfig::lookupEmvConfig(std::string aid, CEmvData &emvData, std::string currency) {
		std::vector<std::vector<std::string> > records;
		std::string upperAid = aid;
		for (int i = 0; i < aid.size(); i++)
			upperAid[i] = std::toupper(aid[i]);
		std::string sql = "SELECT * FROM aid where aid = '"+aid+"' or aid = '"+upperAid+"';";
		records.clear();
		conManager.selectFromDB(sql.c_str(),records,false);
		if (records.size() <= 1) {
//			printf("iqbal - No Aid found\n",aid.c_str());
			conManager.close();
			return DB_FAIL;
		}

		std::string val;
		int ret = conManager.getColumnValue("application_version",val,records,1);
		if (ret != DB_OK) {
//			printf("iqbal - app version found\n",aid.c_str());
			conManager.close();
			return DB_FAIL;
		} else
			emvData.setApplicationVersion(val);

		ret = conManager.getColumnValue("aid",val,records,1);
		if (ret != DB_OK) {
//			printf("iqbal - No Aid in table\n",aid.c_str());
			conManager.close();
			return DB_FAIL;
		} else
			emvData.setAid(val);

		ret = conManager.getColumnValue("default_ddol",val,records,1);
		if (ret != DB_OK) {
			conManager.close();
			return DB_FAIL;
		} else
			emvData.setDefaultDdol(val);

		ret = conManager.getColumnValue("default_tdol",val,records,1);
		if (ret != DB_OK) {
			conManager.close();
			return DB_FAIL;
		} else
			emvData.setDefaultTdol(val);

		ret = conManager.getColumnValue("target_percentage_max",val,records,1);
		if (ret != DB_OK) {
			conManager.close();
			return DB_FAIL;
		} else
			emvData.setTargetPercentageMax(val);

		ret = conManager.getColumnValue("target_percentage",val,records,1);
		if (ret != DB_OK) {
			conManager.close();
			return DB_FAIL;
		} else
			emvData.setTargetPercentage(val);

		ret = conManager.getColumnValue("threshold_amount",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			emvData.setThresholdAmt(val);

		ret = conManager.getColumnValue("emv_scheme_code",val,records,1);
		if (ret != DB_OK) {
			conManager.close();
			return DB_FAIL;
		} else
			emvData.setEmvSchemeCode(val);

		ret = conManager.getColumnValue("totals_group_name",val,records,1);
		if (ret != DB_OK) {
			conManager.close();
			return DB_FAIL;
		} else {
			emvData.setTotalsGroupName(val);
			std::string superTotalsGroupName;
			ret = populateSuperTotalsGroup(val,superTotalsGroupName);
			if (ret == DB_OK)
				emvData.setSuperTotalsGroupName(superTotalsGroupName);
		}

		ret = conManager.getColumnValue("tac_default",val,records,1);
		if (ret != DB_OK) {
			conManager.close();
			return DB_FAIL;
		} else
			emvData.setTacDefault(val);

		ret = conManager.getColumnValue("tac_decline",val,records,1);
		if (ret != DB_OK) {
			conManager.close();
			return DB_FAIL;
		} else
			emvData.setTacDecline(val);

		ret = conManager.getColumnValue("tac_online",val,records,1);
		if (ret != DB_OK) {
			conManager.close();
			return DB_FAIL;
		} else
			emvData.setTacOnline(val);

		ret = conManager.getColumnValue("card_data_input_mode",val,records,1);
		if (ret != DB_OK) {
			conManager.close();
			return DB_FAIL;
		} else
			emvData.setCardDataInputMode(val);

		ret = conManager.getColumnValue("latest_application_version",val,records,1);
		if (ret != DB_OK) {
			conManager.close();
			return DB_FAIL;
		} else
			emvData.setLatestApplicationVersion(val);

		ret = conManager.getColumnValue("partial_matching_allowed",val,records,1);
		if (ret != DB_OK) {
			conManager.close();
			return DB_FAIL;
		} else{
			if((val.length() > 0) && (val.at(0) == '1'))
				emvData.setPartialMatchingAllowed(true);
			else
				emvData.setPartialMatchingAllowed(false);
		}

//		printf("iqbal - before account host id\n",aid.c_str());
		int accountProfileHostId;
		ret = conManager.getColumnValue("account_profile_host_id",val,records,1);
		if (ret != DB_OK) {
//			printf("iqbal - before account host id not found\n",aid.c_str());
			conManager.close();
			return DB_FAIL;
		} else {
			accountProfileHostId = atoi(val.c_str());
			CAccountProfile accountProfile;

			if(currency.compare("840")==0)
				ret = populateEmvAccountProfile(accountProfileHostId,accountProfile);
			else if(currency.compare("422")==0)
				ret = populateEmvAccountProfileCurrency2(accountProfileHostId,accountProfile);


			emvData.setAccountProfile(accountProfile);
		}

		std::map<std::string, CPublicKey> publicKeys;
		ret = populatePublicKey(emvData.getAid().substr(0,10),publicKeys);
		if (ret != DB_OK) {
//			printf("iqbal -public key not found\n",aid.c_str());
			conManager.close();
			return DB_FAIL;
		}

		emvData.setPublicKeys(publicKeys);

		conManager.close();


		return DB_OK;
	}

	int CCardConfig::importCardConfig(std::string path, HIM him) {
		switch (him) {
			case ALIENO:
				return processAlienoHimCardFiles(path);
			case ISO:
				return processIsoHimCardFiles(path);
			default:
				return DB_FAIL;
		}
		return DB_OK;
	}


	int CCardConfig::importEmvConfig(std::string path, HIM him) {
		switch (him) {
			case ALIENO:
				return processAlienoHimEmvFiles(path);
			case ISO:
				return processIsoHimEmvFiles(path);
			default:
				return DB_FAIL;
		}
		return DB_OK;
	}

	int CCardConfig::populateCardProfile(int cardProductId,CCardData &cardData) {

		std::vector<std::vector<std::string> > records;
		std::string sql = "SELECT * FROM card_products WHERE card_product_id = "+SSTR(cardProductId)+";";
		conManager.selectFromDB(sql.c_str(),records,false);
		if (records.size() <= 1)
			return DB_FAIL;

		int cardProfileIndex;
		std::string val;
		int ret = conManager.getColumnValue("card_profile_index",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			cardProfileIndex = atoi(val.c_str());

		records.clear();
		sql = "SELECT * FROM card_profile WHERE card_profile_index = "+SSTR(cardProfileIndex)+";";
		conManager.selectFromDB(sql.c_str(),records,false);
		if (records.size() <= 1)
			return DB_FAIL;

		CCardProfile cardProfile;
		ret = conManager.getColumnValue("active",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			cardProfile.setActive((val.compare("1") == 0)?true:false);

		ret = conManager.getColumnValue("foreign_card",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			cardProfile.setForeignCard((val.compare("1") == 0)?true:false);

		ret = conManager.getColumnValue("check_service_code",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			cardProfile.setCheckServiceCode((val.compare("1") == 0)?true:false);

//		iq_audi_050117
		ret = conManager.getColumnValue("check_expiry_date",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			cardProfile.setCheckExpiryDate((val.compare("1") == 0)?true:false);

		ret = conManager.getColumnValue("check_luhn",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			cardProfile.setCheckLuhn((val.compare("1") == 0)?true:false);

		ret = conManager.getColumnValue("pin_length_min",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			cardProfile.setPinLenMin(atoi(val.c_str()));

		ret = conManager.getColumnValue("pin_length_max",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			cardProfile.setPinLenMax(atoi(val.c_str()));

		ret = conManager.getColumnValue("voice_approval_text",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			cardProfile.setVoiceApprovalText(val);

		ret = conManager.getColumnValue("card_profile_index",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			cardProfile.setIndex(atoi(val.c_str()));

		cardData.setCardProfile(cardProfile);

		return DB_OK;
	}

	std::set<std::string> CCardConfig::getAids() {
		std::set<std::string> aids;
		std::vector<std::vector<std::string> > records;
		std::string sql = "SELECT * FROM aid;";
		conManager.selectFromDB(sql.c_str(),records,true);
		if (records.size() <= 1)
			return aids;

		for (int i=1; i<records.size(); i++) {
			std::string val;
			int ret = conManager.getColumnValue("aid",val,records,i);
			if (ret != DB_OK)
				return aids;
			else
				aids.insert(val);
		}

		return aids;
	}

	std::map<int,CPublicKey> CCardConfig::getCapks(void) {

		std::map<int,CPublicKey> publicKeys;
		std::vector<std::vector<std::string> > records;
		std::string sql = "SELECT * FROM public_key;";
		int cnt=1;


		conManager.selectFromDB(sql.c_str(),records,true);
		if (records.size() <= 1)
			return publicKeys;

		for (int i=1; i<records.size(); i++) {
			CPublicKey publicKey;
			std::string val;

			int ret = conManager.getColumnValue("rid",val,records,i);
			if (ret != DB_OK)
				return publicKeys;
			else
				publicKey.setRid(val);

			ret = conManager.getColumnValue("public_key_index",val,records,i);
			if (ret != DB_OK)
				return publicKeys;
			else
				publicKey.setPublicKeyIndex(val);

			ret = conManager.getColumnValue("hash_algorithm_indicator",val,records,i);
			if (ret != DB_OK)
				return publicKeys;
			else
				publicKey.setKeyAlgorithmIndicator(val);

			ret = conManager.getColumnValue("public_key_modulus",val,records,i);
			if (ret != DB_OK)
				return publicKeys;
			else
				publicKey.setModulus(val);

			ret = conManager.getColumnValue("public_key_exponent",val,records,i);
			if (ret != DB_OK)
				return publicKeys;
			else
				publicKey.setExponent(val);

			ret = conManager.getColumnValue("public_key_check_sum",val,records,i);
			if (ret != DB_OK)
				return publicKeys;
			else
				publicKey.setCheckSum(val);

			ret = conManager.getColumnValue("key_activation",val,records,i);
			if (ret != DB_OK)
				return publicKeys;
			else
				publicKey.setActivation(val);

			ret = conManager.getColumnValue("key_expiry",val,records,i);
			if (ret != DB_OK)
				return publicKeys;
			else
				publicKey.setExpiry(val);

			ret = conManager.getColumnValue("key_algorithm_indicator",val,records,i);
			if (ret != DB_OK)
				return publicKeys;
			else
				publicKey.setKeyAlgorithmIndicator(val);

			publicKeys[cnt++] = publicKey;
		}

		return publicKeys;
	}

	int CCardConfig::populateAllowedTransactions(int accountProfileId,std::set<std::string> &allowedtransactions) {
		std::vector<std::vector<std::string> > records;
		std::string sql = "SELECT * FROM allowed_transactions WHERE account_profile_index = '"+SSTR(accountProfileId)+"';";
		conManager.selectFromDB(sql.c_str(),records,false);
		if (records.size() <= 1)
			return DB_FAIL;

		for (int i=1; i<records.size(); i++) {
			CAccountProfile accountProfile;
			std::string val;
			int ret = conManager.getColumnValue("code",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				allowedtransactions.insert(val);
		}

		return DB_OK;
	}

	int CCardConfig::populateAccounts(int cardProductId,std::map<std::string, CAccountProfile> &accounts) {
		std::vector<std::vector<std::string> > records;
		std::string sql = "SELECT * FROM attached_account aa INNER JOIN account_profile ap ON aa.account_profile_index = ap.account_profile_index WHERE aa.card_product_id = '"+SSTR(cardProductId)+"';";

		conManager.selectFromDB(sql.c_str(),records,false);
		if (records.size() <= 1)
			return DB_FAIL;

		for (int i=1; i<records.size(); i++) {
			CAccountProfile accountProfile;
			std::string val;

			int ret = conManager.getColumnValue("code",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else {
				accountProfile.setCode(val);
				std::string description;
				populateAccountDescription(val,description);
				accountProfile.setDescription(description);
			}

			ret = conManager.getColumnValue("draft_capture_mode",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				accountProfile.setDraftCaptureMode(val.compare("1") == 0?true:false);

			ret = conManager.getColumnValue("allow_voice_app",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				accountProfile.setAllowVoiceApproval(val.compare("1") == 0?true:false);

			ret = conManager.getColumnValue("allow_manual_entry",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				accountProfile.setAllowManualEntry(val.compare("1") == 0?true:false);

			ret = conManager.getColumnValue("require_pin_entry",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				accountProfile.setRequirePinEntry(val.compare("1") == 0?true:false);

			ret = conManager.getColumnValue("require_signature",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				accountProfile.setRequireSignature(val.compare("1") == 0?true:false);

			ret = conManager.getColumnValue("check_card_value",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				accountProfile.setCheckCardValue(val.compare("1") == 0?true:false);

			ret = conManager.getColumnValue("skim_card_value",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				accountProfile.setSkimCardValue(val.compare("1") == 0?true:false);

			ret = conManager.getColumnValue("allow_budget",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				accountProfile.setAllowBudget(val.compare("1") == 0?true:false);

			ret = conManager.getColumnValue("budget_amount_min",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				accountProfile.setBudgetAmountMin(val);

			ret = conManager.getColumnValue("budget_amount_min",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				accountProfile.setBudgetAmountMin(val);

			ret = conManager.getColumnValue("emv_fallback_limit_index",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else {
				CLimits limits;
				ret = poplateLimits(atoi(val.c_str()),limits);
				if (ret != DB_OK)
					return DB_FAIL;

				accountProfile.setEmvFallBackLimits(limits);
			}


			ret = conManager.getColumnValue("contactless_limit_index",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else {
				CLimits limits;
				ret = poplateLimits(atoi(val.c_str()),limits);
				if (ret != DB_OK)
					return DB_FAIL;
				accountProfile.setContactLessLimits(limits);
			}

			ret = conManager.getColumnValue("local_limit_index",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else {
				CLimits limits;
				ret = poplateLimits(atoi(val.c_str()),limits);
				if (ret != DB_OK)
					return DB_FAIL;

				accountProfile.setLocalLimits(limits);
			}

			ret = conManager.getColumnValue("offline_limit_index",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else {
				CLimits limits;
				ret = poplateLimits(atoi(val.c_str()),limits);
				if (ret != DB_OK)
					return DB_FAIL;

				accountProfile.setOfflineLimits(limits);
			}

			ret = conManager.getColumnValue("totals_group_name",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else {
				accountProfile.setTotalsGroupName(val);
				std::string superTotalsGroupName;
				ret = populateSuperTotalsGroup(val,superTotalsGroupName);
				if (ret == DB_OK) {
					accountProfile.setSuperTotalsGroupName(superTotalsGroupName);
				}
			}

			int accountProfileIndex;
			ret = conManager.getColumnValue("account_profile_index",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else  {
				accountProfileIndex = atoi(val.c_str());
				std::set<std::string> allowedtransactions;
				ret = populateAllowedTransactions(accountProfileIndex,allowedtransactions);
				if (ret != DB_OK)
					return DB_FAIL;

				accountProfile.setAllowedTransactions(allowedtransactions);
			}

			accounts[accountProfile.getCode()] = accountProfile;

		}

		return DB_OK;
	}

	int CCardConfig::populateAccountsCurrency2(int cardProductId,std::map<std::string, CAccountProfile> &accounts) {
		std::vector<std::vector<std::string> > records;
		std::string sql = "SELECT * FROM attached_account aa INNER JOIN account_profile_curr2 ap ON aa.account_profile_index = ap.account_profile_index WHERE aa.card_product_id = '"+SSTR(cardProductId)+"';";

		conManager.selectFromDB(sql.c_str(),records,false);
		if (records.size() <= 1)
			return DB_FAIL;

		for (int i=1; i<records.size(); i++) {
			CAccountProfile accountProfile;
			std::string val;

			int ret = conManager.getColumnValue("code",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else {
				accountProfile.setCode(val);
				std::string description;
				populateAccountDescription(val,description);
				accountProfile.setDescription(description);
			}

			ret = conManager.getColumnValue("draft_capture_mode",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				accountProfile.setDraftCaptureMode(val.compare("1") == 0?true:false);

			ret = conManager.getColumnValue("allow_voice_app",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				accountProfile.setAllowVoiceApproval(val.compare("1") == 0?true:false);

			ret = conManager.getColumnValue("allow_manual_entry",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				accountProfile.setAllowManualEntry(val.compare("1") == 0?true:false);

			ret = conManager.getColumnValue("require_pin_entry",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				accountProfile.setRequirePinEntry(val.compare("1") == 0?true:false);

			ret = conManager.getColumnValue("require_signature",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				accountProfile.setRequireSignature(val.compare("1") == 0?true:false);

			ret = conManager.getColumnValue("check_card_value",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				accountProfile.setCheckCardValue(val.compare("1") == 0?true:false);

			ret = conManager.getColumnValue("skim_card_value",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				accountProfile.setSkimCardValue(val.compare("1") == 0?true:false);

			ret = conManager.getColumnValue("allow_budget",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				accountProfile.setAllowBudget(val.compare("1") == 0?true:false);

			ret = conManager.getColumnValue("budget_amount_min",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				accountProfile.setBudgetAmountMin(val);

			ret = conManager.getColumnValue("budget_amount_min",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				accountProfile.setBudgetAmountMin(val);

			ret = conManager.getColumnValue("emv_fallback_limit_index",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else {
				CLimits limits;
				ret = poplateLimits(atoi(val.c_str()),limits);
				if (ret != DB_OK)
					return DB_FAIL;

				accountProfile.setEmvFallBackLimits(limits);
			}


			ret = conManager.getColumnValue("contactless_limit_index",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else {
				CLimits limits;
				ret = poplateLimits(atoi(val.c_str()),limits);
				if (ret != DB_OK)
					return DB_FAIL;
				accountProfile.setContactLessLimits(limits);
			}

			ret = conManager.getColumnValue("local_limit_index",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else {
				CLimits limits;
				ret = poplateLimits(atoi(val.c_str()),limits);
				if (ret != DB_OK)
					return DB_FAIL;

				accountProfile.setLocalLimits(limits);
			}

			ret = conManager.getColumnValue("offline_limit_index",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else {
				CLimits limits;
				ret = poplateLimits(atoi(val.c_str()),limits);
				if (ret != DB_OK)
					return DB_FAIL;

				accountProfile.setOfflineLimits(limits);
			}

			ret = conManager.getColumnValue("totals_group_name",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else {
				accountProfile.setTotalsGroupName(val);
				std::string superTotalsGroupName;
				ret = populateSuperTotalsGroup(val,superTotalsGroupName);
				if (ret == DB_OK) {
					accountProfile.setSuperTotalsGroupName(superTotalsGroupName);
				}
			}

			int accountProfileIndex;
			ret = conManager.getColumnValue("account_profile_index",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else  {
				accountProfileIndex = atoi(val.c_str());
				std::set<std::string> allowedtransactions;
				ret = populateAllowedTransactions(accountProfileIndex,allowedtransactions);
				if (ret != DB_OK)
					return DB_FAIL;

				accountProfile.setAllowedTransactions(allowedtransactions);
			}

			accounts[accountProfile.getCode()] = accountProfile;

		}

		return DB_OK;
	}

	int CCardConfig::populateSuperTotalsGroup(std::string totalsGroupName,std::string &supertotalsGroupName) {

		std::vector<std::vector<std::string> > records;
		std::string sql = "SELECT * FROM super_totals WHERE totals_group_name = '"+totalsGroupName+"';";
		conManager.selectFromDB(sql.c_str(),records,false);
		if (records.size() <= 1)
			return DB_FAIL;

		std::string val;
		int ret = conManager.getColumnValue("super_totals_group_name",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			supertotalsGroupName = val;

		return DB_OK;
	}

	int CCardConfig::poplateLimits(int limitsId, CLimits &limits) {

		std::vector<std::vector<std::string> > records;
		std::string sql = "SELECT * FROM limits WHERE limits_index = "+SSTR(limitsId)+";";
		conManager.selectFromDB(sql.c_str(),records,false);
		if (records.size() <= 1)
			return DB_OK; //contacless casn be empty

		std::string val;
		int ret = conManager.getColumnValue("cashback_amt_max",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			limits.setCashbackAmtMax(val);

		ret = conManager.getColumnValue("floor_limit_amt",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			limits.setFloorLimitAmt(val);

		ret = conManager.getColumnValue("transaction_amt_max",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			limits.setTransactionAmtMax(val);

		ret = conManager.getColumnValue("transaction_amt_min",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			limits.setTransactionAmtMin(val);

		return DB_OK;
	}

	int CCardConfig::processIsoHimEmvFiles(std::string path) {
		// we close the database connection in the end to speed up the import to database

		// Check if the first temp file exists, as an indication that new parameters have to be imported
		std::string tmppath = path;
		std::ifstream ifs(tmppath.append("/").append(ISO_AID_FILE_NAME).c_str());

		if (!ifs.good()){
//			printf("No ISO EMV parameters to import\n");
			return DB_NO_ACTION;
		}else
			ifs.close();

		std::string sql = "DELETE FROM aid;";
		conManager.writeToDB(sql,false);
		sql = "DELETE FROM sqlite_sequence WHERE name='aid'";
		conManager.writeToDB(sql,false);
		int ret = processIsoAidFile(EMV_PARM_PATH);
		if (ret != DB_OK) {
			conManager.close();
			return ret;
		}

		sql = "DELETE FROM public_key;";
		conManager.writeToDB(sql,false);
		sql = "DELETE FROM sqlite_sequence WHERE name='public_key'";
		conManager.writeToDB(sql,false);
		ret = processIsoPublicKeyFile(EMV_PARM_PATH);
		if (ret != DB_OK) {
			conManager.close();
			return ret;
		}

		sql = "DELETE FROM aid_override;";
		conManager.writeToDB(sql,false);
		sql = "DELETE FROM sqlite_sequence WHERE name='aid_override'";
		conManager.writeToDB(sql,false);
		ret = processIsoAidOverrideFile(EMV_PARM_PATH);
		if (ret != DB_OK) {
			conManager.close();
			return ret;
		}

		conManager.close(); //close the connection

		return DB_OK;

	}


	int CCardConfig::processAlienoHimEmvFiles(std::string path) {
		// we close the database connection in the end to speed up the import to database
		std::string sql = "DELETE FROM aid;";
		conManager.writeToDB(sql);
		sql = "DELETE FROM sqlite_sequence WHERE name='aid'";
		conManager.writeToDB(sql);
		int ret = processAlienoAidFile(EMV_PARM_PATH);
		if (ret != DB_OK) {
			conManager.close();
			return ret;
		}

		sql = "DELETE FROM public_key;";
		conManager.writeToDB(sql);
		sql = "DELETE FROM sqlite_sequence WHERE name='public_key'";
		conManager.writeToDB(sql);
		ret = processAlienoPublicKeyFile(EMV_PARM_PATH);
		if (ret != DB_OK) {
			conManager.close();
			return ret;
		}

		conManager.close(); //close the connection

		return DB_OK;

	}

	int CCardConfig::processAlienoHimCardFiles(std::string path) {

		// we close the database connection in the end to speed up the import to database
		std::string sql = "DELETE FROM bin;";
		conManager.writeToDB(sql);
		sql = "DELETE FROM sqlite_sequence WHERE name='bin'";
		conManager.writeToDB(sql);
		int ret = processAlienoBinFile(CARD_PARM_PATH);
		if (ret != DB_OK) {
			conManager.close();
			return ret;
		}

		sql = "DELETE FROM card_profile;";
		conManager.writeToDB(sql);
		sql = "DELETE FROM sqlite_sequence WHERE name='card_profile'";
		conManager.writeToDB(sql);
		ret = processAlienoCardProfileFile(CARD_PARM_PATH);
		if (ret != DB_OK) {
			conManager.close();
			return ret;
		}

		processAlienoForeignCard(CARD_PARM_PATH);
		if (ret != DB_OK) {
			conManager.close();
			return ret;
		}

		sql = "DELETE FROM attached_account;";
		conManager.writeToDB(sql);
		sql = "DELETE FROM sqlite_sequence WHERE name='attached_account'";
		conManager.writeToDB(sql);
		ret = processAlienoAttachedAccountsFile(CARD_PARM_PATH);
		if (ret != DB_OK) {
			conManager.close();
			return ret;
		}

		sql = "DELETE FROM account_profile;";
		conManager.writeToDB(sql);
		sql = "DELETE FROM sqlite_sequence WHERE name='account_profile'";
		conManager.writeToDB(sql);
		ret = processAlienoAccountProfileFile(CARD_PARM_PATH);
		if (ret != DB_OK) {
			conManager.close();
			return ret;
		}

		sql = "DELETE FROM allowed_transactions;";
		conManager.writeToDB(sql);
		sql = "DELETE FROM sqlite_sequence WHERE name='allowed_transactions'";
		conManager.writeToDB(sql);
		ret = processAlienoAllowedTransactionsFile(CARD_PARM_PATH);
		if (ret != DB_OK) {
			conManager.close();
			return ret;
		}

		sql = "DELETE FROM account_description;";
		conManager.writeToDB(sql);
		sql = "DELETE FROM sqlite_sequence WHERE name='account_description'";
		conManager.writeToDB(sql);
		ret = processAlienoAccountDescriptionFile(CARD_PARM_PATH);
		if (ret != DB_OK) {
			conManager.close();
			return ret;
		}

		sql = "DELETE FROM super_totals;";
		conManager.writeToDB(sql);
		sql = "DELETE FROM sqlite_sequence WHERE name='super_totals'";
		conManager.writeToDB(sql);
		ret = processAlienoSuperTotalsFile(CARD_PARM_PATH);
		if (ret != DB_OK) {
			conManager.close();
			return ret;
		}

		sql = "DELETE FROM limits;";
		conManager.writeToDB(sql);
		sql = "DELETE FROM sqlite_sequence WHERE name='limits'";
		conManager.writeToDB(sql);
		ret = processAlienoLimitsFile(CARD_PARM_PATH);
		if (ret != DB_OK) {
			conManager.close();
			return ret;
		}

		conManager.close();

		return DB_OK;
	}

	int CCardConfig::processIsoHimCardFiles(std::string path) {

		// we close the database connection in the end to speed up the import to database

		// Check if the first temp file exists, as an indication that new parameters have to be imported
		std::string tmppath = path;
		std::ifstream ifs(tmppath.append("/").append(ISO_BIN_FILE_NAME).c_str());

		if (!ifs.good()){
//			printf("No ISO card parameters to import\n");
			return DB_NO_ACTION;
		}else
			ifs.close();

//		printf("Before BIN\n");
		std::string sql = "DELETE FROM bin;";
		conManager.writeToDB(sql,false);
		sql = "DELETE FROM sqlite_sequence WHERE name='bin'";
		conManager.writeToDB(sql,false);
		int ret = processIsoBinFile(path);
		if (ret != DB_OK) {
			conManager.close();
			return ret;
		}
//		printf("After BIN\n");

		sql = "DELETE FROM card_profile;";
		conManager.writeToDB(sql,false);
		sql = "DELETE FROM sqlite_sequence WHERE name='card_profile'";
		conManager.writeToDB(sql,false);
		ret = processIsoCardProfileFile(path);
		if (ret != DB_OK) {
			conManager.close();
			return ret;
		}
//		printf("After card_profile\n");


		sql = "DELETE FROM card_products;";
		conManager.writeToDB(sql,false);
		sql = "DELETE FROM sqlite_sequence WHERE name='card_products'";
		conManager.writeToDB(sql,false);
		processIsoCardProductsFile(path);
		if (ret != DB_OK) {
			conManager.close();
			return ret;
		}
//		printf("After processCardProducts\n");

		sql = "DELETE FROM attached_account;";
		conManager.writeToDB(sql,false);
		sql = "DELETE FROM sqlite_sequence WHERE name='attached_account'";
		conManager.writeToDB(sql,false);
		ret = processIsoAttachedAccountsFile(path);
		if (ret != DB_OK) {
			conManager.close();
			return ret;
		}

//		printf("After attached_account\n");

		sql = "DELETE FROM account_profile;";
		conManager.writeToDB(sql,false);
		sql = "DELETE FROM sqlite_sequence WHERE name='account_profile'";
		conManager.writeToDB(sql,false);
		ret = processIsoAccountProfileFile(path);
		if (ret != DB_OK) {
			conManager.close();
			return ret;
		}

//		printf("After account_profile\n");
		sql = "DELETE FROM allowed_transactions;";
		conManager.writeToDB(sql,false);
		sql = "DELETE FROM sqlite_sequence WHERE name='allowed_transactions'";
		conManager.writeToDB(sql,false);
		ret = processIsoAllowedTransactionsFile(path);
		if (ret != DB_OK) {
			conManager.close();
			return ret;
		}

//		printf("After allowed_transactions\n");

		sql = "DELETE FROM account_description;";
		conManager.writeToDB(sql,false);
		sql = "DELETE FROM sqlite_sequence WHERE name='account_description'";
		conManager.writeToDB(sql,false);
		ret = processIsoAccountDescriptionFile(path);
		if (ret != DB_OK) {
			conManager.close();
			return ret;
		}

//		printf("After account_description\n");

		sql = "DELETE FROM super_totals;";
		conManager.writeToDB(sql,false);
		sql = "DELETE FROM sqlite_sequence WHERE name='super_totals'";
		conManager.writeToDB(sql,false);
		ret = processIsoSuperTotalsFile(path);
		if (ret != DB_OK) {
			conManager.close();
			return ret;
		}

//		printf("After super_totals\n");
		sql = "DELETE FROM limits;";
		conManager.writeToDB(sql,false);
		sql = "DELETE FROM sqlite_sequence WHERE name='limits'";
		conManager.writeToDB(sql,false);
		ret = processIsoLimitsFile(path);
		if (ret != DB_OK) {
			conManager.close();
			return ret;
		}

//		printf("After limits\n");

		conManager.close();

		return DB_OK;
	}

	int CCardConfig::processAlienoCardProfileFile(std::string path) {
		bool nextByteEscaped = false;
		std::string line;
		int ret;

		std::ifstream ifs(path.append("/").append(ALIENO_CARD_PROFILE_FILE_NAME).c_str());
		if (!ifs.is_open())
			return DB_FAIL;

		std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
		str = str.substr(14,str.length()-14); // version
		BOOST_FOREACH (char& c , str) {
			if (c == '\\') {
				nextByteEscaped = true;
				continue;
			}
			if (!nextByteEscaped && c == '.') {
				std::size_t found = line.find("\n");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				found = line.find("\r");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				ret = processAlienoCardProfileLine(line); //strip version
				if (ret != DB_OK)
					return ret;

				line = "";
				continue;
			}

			line += c;
			nextByteEscaped = false;
		}

		return DB_OK;
	}

	int CCardConfig::processIsoCardProfileFile(std::string path) {
		bool nextByteEscaped = false;
		std::string line;
		int ret;

		std::ifstream ifs(path.append("/").append(ISO_CARD_PROFILE_FILE_NAME).c_str());
		if (!ifs.is_open())
			return DB_FAIL;

		std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
		if (str.length() < 4)
			return DB_OK;
		str = str.substr(4,str.length()-4); // version

		std::string sql = "INSERT INTO card_profile (active,check_service_code,check_expiry_date,check_luhn,pin_length_min,pin_length_max,voice_approval_text,card_profile_index) SELECT ";
		int cnt=0;
		bool first = true;

		BOOST_FOREACH (char& c , str) {
			if (c == '\\') {
				nextByteEscaped = true;
				continue;
			}
			if (!nextByteEscaped && c == '.') {
				std::size_t found = line.find("\n");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				found = line.find("\r");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");

				processIsoCardProfileLine(line, sql,first);

				first = false;

				cnt ++;

				if (cnt > 499) {
					ret = conManager.writeToDB(sql, false);
					sql = "INSERT INTO card_profile (active,check_service_code,check_expiry_date,check_luhn,pin_length_min,pin_length_max,voice_approval_text,card_profile_index) SELECT ";
					cnt = 0;
					first = true;
				}

				line = "";
				continue;
			}

			line += c;
			nextByteEscaped = false;
		}

		if (cnt>0)
			ret = conManager.writeToDB(sql, false);

		return DB_OK;
	}

	int CCardConfig::processAlienoForeignCard(std::string path) {
		bool nextByteEscaped = false;
		std::string line;
		int ret;

		std::ifstream ifs(path.append("/").append(ALIENO_FOREIGN_CARD_FILE_NAME).c_str());
		if (!ifs.is_open())
			return DB_FAIL;

		std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
		str = str.substr(14,str.length()-14); // version
		BOOST_FOREACH (char& c , str) {
			if (c == '\\') {
				nextByteEscaped = true;
				continue;
			}
			if (!nextByteEscaped && c == '.') {
				std::size_t found = line.find("\n");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				found = line.find("\r");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				ret = processAlienoForeignCardLine(line); //strip version
				if (ret != DB_OK)
					return ret;

				line = "";
				continue;
			}

			line += c;
			nextByteEscaped = false;
		}

		return DB_OK;
	}

	int CCardConfig::processIsoCardProductsFile(std::string path) {
		bool nextByteEscaped = false;
				std::string line;
				int ret;

				std::ifstream ifs(path.append("/").append(ISO_FOREIGN_CARD_FILE_NAME).c_str());
				if (!ifs.is_open())
					return DB_FAIL;

				std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
				if (str.length() < 4)
					return DB_OK;
				str = str.substr(4,str.length()-4); // version
				std::string sql = "INSERT INTO card_products (card_product_id,card_profile_index,is_foreign) SELECT ";
				int cnt=0;
				bool first = true;

				BOOST_FOREACH (char& c , str) {
					if (c == '\\') {
						nextByteEscaped = true;
						continue;
					}
					if (!nextByteEscaped && c == '.') {
						std::size_t found = line.find("\n");
						if (found!=std::string::npos)
							line = line.replace(found,1,"");
						found = line.find("\r");
						if (found!=std::string::npos)
							line = line.replace(found,1,"");

						processIsoCardProductsLine(line, sql,first);

						first = false;

						cnt ++;

						if (cnt > 499) {
							ret = conManager.writeToDB(sql, false);
							sql = "INSERT INTO card_products (card_product_id,card_profile_index,is_foreign) SELECT ";
							cnt = 0;
							first = true;
						}

						line = "";
						continue;
					}

					line += c;
					nextByteEscaped = false;
				}

				if (cnt>0)
					ret = conManager.writeToDB(sql, false);

				return DB_OK;
	}

	int CCardConfig::processAlienoAccountDescriptionFile(std::string path) {
		bool nextByteEscaped = false;
		std::string line;
		int ret;

		std::ifstream ifs(path.append("/").append(ALIENO_ACC_DESCR_FILE_NAME).c_str());
		if (!ifs.is_open())
			return DB_FAIL;

		std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
		str = str.substr(14,str.length()-14); // version
		BOOST_FOREACH (char& c , str) {
			if (c == '\\') {
				nextByteEscaped = true;
				continue;
			}
			if (!nextByteEscaped && c == '.') {
				std::size_t found = line.find("\n");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				found = line.find("\r");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				ret = processAlienoAccountDescriptionLine(line);
				if (ret != DB_OK)
					return ret;

				line = "";
				continue;
			}

			line += c;
			nextByteEscaped = false;
		}

		return DB_OK;
	}

	int CCardConfig::processIsoAccountDescriptionFile(std::string path) {
		bool nextByteEscaped = false;
		std::string line;
		int ret;

		std::ifstream ifs(path.append("/").append(ISO_ACC_DESCR_FILE_NAME).c_str());
		if (!ifs.is_open())
			return DB_FAIL;

		std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
		if (str.length() < 4)
			return DB_OK;
		str = str.substr(4,str.length()-4); // version

		std::string sql = "INSERT INTO account_description (code,name) SELECT ";
		int cnt=0;
		bool first = true;

		BOOST_FOREACH (char& c , str) {
			if (c == '\\') {
				nextByteEscaped = true;
				continue;
			}
			if (!nextByteEscaped && c == '.') {
				std::size_t found = line.find("\n");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				found = line.find("\r");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");

				ret = processIsoAccountDescriptionLine(line,sql,first);
				first = false;

				cnt ++;

				if (cnt > 499) {
					ret = conManager.writeToDB(sql, false);
					sql = "INSERT INTO account_description (code,name) SELECT ";
					cnt = 0;
					first = true;
				}

				line = "";
				continue;
			}

			line += c;
			nextByteEscaped = false;
		}

		if (cnt>0)
			ret = conManager.writeToDB(sql, false);

		return DB_OK;
	}

	int CCardConfig::processAlienoAccountDescriptionLine(std::string line) {
		std::string code, description;
		boost::char_separator<char> sep(",", "", boost::keep_empty_tokens);
		boost::tokenizer< boost::char_separator<char> > tokens(line, sep);
		CCommsSettings settings;
		int cnt=0;

		BOOST_FOREACH (const std::string& t, tokens) {
			switch (cnt++) {
				case 0:
					code = t;
					break;
				case 1:
					description = t;
					break;
				default:
					break;
			}
		}
		return conManager.insertAccountDescription(code,description);
	}

	int CCardConfig::processIsoAccountDescriptionLine(std::string line,std::string &sql, bool first) {
		std::string code, description;
		boost::char_separator<char> sep(",", "", boost::keep_empty_tokens);
		boost::tokenizer< boost::char_separator<char> > tokens(line, sep);
		CCommsSettings settings;
		int cnt=0;

		BOOST_FOREACH (const std::string& t, tokens) {
			switch (cnt++) {
				case 0:
					code = t;
					break;
				case 1:
					description = t;
					break;
				default:
					break;
			}
		}

		if (first)
			sql = sql + "'"+code + "' AS code,'"+description+ "' AS name\r\n";
		else
			sql = sql + " UNION SELECT '"+ code+"','"+description+"'\r\n";

		return DB_OK;
	}

	int CCardConfig::processAlienoAllowedTransactionsFile(std::string path) {
		bool nextByteEscaped = false;
		std::string line;
		int ret;

		std::ifstream ifs(path.append("/").append(ALIENO_ALLOWED_TX_FILE_NAME).c_str());
		if (!ifs.is_open())
			return DB_FAIL;

		std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
		str = str.substr(14,str.length()-14); // version
		BOOST_FOREACH (char& c , str) {
			if (c == '\\') {
				nextByteEscaped = true;
				continue;
			}
			if (!nextByteEscaped && c == '.') {
				std::size_t found = line.find("\n");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				found = line.find("\r");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				ret = processAlienoAllowedTransactionsLine(line);
				if (ret != DB_OK)
					return ret;

				line = "";
				continue;
			}

			line += c;
			nextByteEscaped = false;
		}

		return DB_OK;
	}

	int CCardConfig::processIsoAllowedTransactionsFile(std::string path) {
		bool nextByteEscaped = false;
		std::string line;
		int ret;

		std::ifstream ifs(path.append("/").append(ISO_ALLOWED_TX_FILE_NAME).c_str());
		if (!ifs.is_open())
			return DB_FAIL;

		std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
		if (str.length() < 4)
			return DB_OK;
		str = str.substr(4,str.length()-4); // version

		std::string sql = "INSERT INTO allowed_transactions (account_profile_index,code) SELECT ";
		int cnt=0;
		bool first = true;


		BOOST_FOREACH (char& c , str) {
			if (c == '\\') {
				nextByteEscaped = true;
				continue;
			}
			if (!nextByteEscaped && c == '.') {
				std::size_t found = line.find("\n");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				found = line.find("\r");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");

				ret = processIsoAllowedTransactionsLine(line,sql,first);

				first = false;

				cnt ++;

				if (cnt > 499) {
					ret = conManager.writeToDB(sql, false);
					sql = "INSERT INTO allowed_transactions (account_profile_index,code) SELECT ";
					cnt = 0;
					first = true;
				}

				line = "";
				continue;
			}

			line += c;
			nextByteEscaped = false;
		}

		if (cnt>0)
			ret = conManager.writeToDB(sql, false);

		return DB_OK;
	}

	int CCardConfig::processAlienoSuperTotalsFile(std::string path) {
		bool nextByteEscaped = false;
		std::string line;
		int ret;

		std::ifstream ifs(path.append("/").append(ALIENO_SUPER_TOTALS_FILE_NAME).c_str());
		if (!ifs.is_open())
			return DB_FAIL;

		std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
		str = str.substr(14,str.length()-14); // version
		BOOST_FOREACH (char& c , str) {
			if (c == '\\') {
				nextByteEscaped = true;
				continue;
			}
			if (!nextByteEscaped && c == '.') {
				std::size_t found = line.find("\n");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				found = line.find("\r");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				ret = processAlienoSuperTotalsLine(line);
				if (ret != DB_OK)
					return ret;

				line = "";
				continue;
			}

			line += c;
			nextByteEscaped = false;
		}

		return DB_OK;
	}

	int CCardConfig::processIsoSuperTotalsFile(std::string path) {
		bool nextByteEscaped = false;
		std::string line;
		int ret;

		std::ifstream ifs(path.append("/").append(ISO_SUPER_TOTALS_FILE_NAME).c_str());
		if (!ifs.is_open())
			return DB_OK; // not manadatory

		std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
		if (str.length() < 4)
			return DB_OK;
		str = str.substr(4,str.length()-4); // version

		BOOST_FOREACH (char& c , str) {
			if (c == '\\') {
				nextByteEscaped = true;
				continue;
			}
			if (!nextByteEscaped && c == '.') {
				std::size_t found = line.find("\n");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				found = line.find("\r");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				ret = processIsoSuperTotalsLine(line);
				if (ret != DB_OK)
					return ret;

				line = "";
				continue;
			}

			line += c;
			nextByteEscaped = false;
		}

		return DB_OK;
	}

	int CCardConfig::processAlienoSuperTotalsLine(std::string line) {
		std::string totalsGroupName,superTotalsGroupName;
		boost::char_separator<char> sep(",", "", boost::keep_empty_tokens);
		boost::tokenizer< boost::char_separator<char> > tokens(line, sep);
		CCommsSettings settings;
		int cnt=0;

		BOOST_FOREACH (const std::string& t, tokens) {
			switch (cnt++) {
				case 0:
					totalsGroupName = t;
					break;
				case 1:
					superTotalsGroupName = t;
					break;
				default:
					break;
			}
		}

		return conManager.insertSuperTotals(totalsGroupName,superTotalsGroupName);
	}

	int CCardConfig::processIsoSuperTotalsLine(std::string line) {
		std::string totalsGroupName,superTotalsGroupName;
		boost::char_separator<char> sep(",", "", boost::keep_empty_tokens);
		boost::tokenizer< boost::char_separator<char> > tokens(line, sep);
		CCommsSettings settings;
		int cnt=0;

		BOOST_FOREACH (const std::string& t, tokens) {
			switch (cnt++) {
				case 0:
					totalsGroupName = t;
					break;
				case 1:
					superTotalsGroupName = t;
					break;
				default:
					break;
			}
		}

		return conManager.insertSuperTotals(totalsGroupName,superTotalsGroupName);
	}

	int CCardConfig::processAlienoLimitsFile(std::string path) {
		bool nextByteEscaped = false;
		std::string line;
		int ret;

		std::ifstream ifs(path.append("/").append(ALIENO_LIMITS_FILE_NAME).c_str());
		if (!ifs.is_open())
			return DB_FAIL;

		std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
		str = str.substr(14,str.length()-14); // version
		BOOST_FOREACH (char& c , str) {
			if (c == '\\') {
				nextByteEscaped = true;
				continue;
			}
			if (!nextByteEscaped && c == '.') {
				std::size_t found = line.find("\n");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				found = line.find("\r");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				ret = processAlienoLimitsLine(line);
				if (ret != DB_OK)
					return ret;

				line = "";
				continue;
			}

			line += c;
			nextByteEscaped = false;
		}

		return DB_OK;
	}

	int CCardConfig::processIsoLimitsFile(std::string path) {
		bool nextByteEscaped = false;
		std::string line;
		int ret;

		std::ifstream ifs(path.append("/").append(ISO_LIMITS_FILE_NAME).c_str());
		if (!ifs.is_open())
			return DB_FAIL;

		std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
		if (str.length() < 4)
			return DB_OK;
		str = str.substr(4,str.length()-4); // version

		std::string sql = "INSERT INTO limits (cashback_amt_max,floor_limit_amt,transaction_amt_max,transaction_amt_min,limits_index) SELECT ";
		int cnt=0;
		bool first = true;


		BOOST_FOREACH (char& c , str) {
			if (c == '\\') {
				nextByteEscaped = true;
				continue;
			}
			if (!nextByteEscaped && c == '.') {
				std::size_t found = line.find("\n");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				found = line.find("\r");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				ret = processIsoLimitsLine(line,sql,first);

				first = false;

				cnt ++;

				if (cnt > 499) {
					ret = conManager.writeToDB(sql, false);
					sql = "INSERT INTO limits (cashback_amt_max,floor_limit_amt,transaction_amt_max,transaction_amt_min,limits_index) SELECT ";
					cnt = 0;
					first = true;
				}

				line = "";
				continue;
			}

			line += c;
			nextByteEscaped = false;
		}

		if (cnt>0)
			ret = conManager.writeToDB(sql, false);

		return DB_OK;
	}

	int CCardConfig::processAlienoLimitsLine(std::string line) {
		std::string cashbackAmtMax,floorLimtAmt,txAmtMax,limtsIndex;
		boost::char_separator<char> sep(",", "", boost::keep_empty_tokens);
		boost::tokenizer< boost::char_separator<char> > tokens(line, sep);
		CCommsSettings settings;
		int cnt=0;

		BOOST_FOREACH (const std::string& t, tokens) {
			switch (cnt++) {
				case 0:
					limtsIndex = t;
					break;
				case 1:
					cashbackAmtMax = t;
					break;
				case 2:
					floorLimtAmt = t;
					break;
				case 3:
					txAmtMax = t;
					break;
				default:
					break;
			}
		}

		return conManager.insertLimits(cashbackAmtMax,floorLimtAmt,txAmtMax,SSTR("0"),limtsIndex);
	}

	int CCardConfig::processIsoLimitsLine(std::string line,std::string &sql, bool first) {
		std::string cashbackAmtMax,floorLimtAmt,txAmtMax,limtsIndex;
		boost::char_separator<char> sep(",", "", boost::keep_empty_tokens);
		boost::tokenizer< boost::char_separator<char> > tokens(line, sep);
		CCommsSettings settings;
		int cnt=0;

		BOOST_FOREACH (const std::string& t, tokens) {
			switch (cnt++) {
				case 0:
					limtsIndex = t;
					break;
				case 1:
					cashbackAmtMax = t;
					break;
				case 2:
					floorLimtAmt = t;
					break;
				case 3:
					txAmtMax = t;
					break;
				default:
					break;
			}
		}

		if (first)
			sql = sql + "'"+cashbackAmtMax + "' AS cashback_amt_max,'"+floorLimtAmt+ "' AS floor_limit_amt,'"+txAmtMax+"' AS transaction_amt_max,'"+limtsIndex+"' AS limits_index\r\n";
		else
			sql = sql + " UNION SELECT '"+ cashbackAmtMax+"','"+floorLimtAmt+"','"+txAmtMax+"','"+limtsIndex+"'\r\n";

		return DB_OK;
	}


	int CCardConfig::processAlienoAllowedTransactionsLine(std::string line) {
		std::string accountProfileIndex,code;
		boost::char_separator<char> sep(",", "", boost::keep_empty_tokens);
		boost::tokenizer< boost::char_separator<char> > tokens(line, sep);
		CCommsSettings settings;
		int cnt=0;

		BOOST_FOREACH (const std::string& t, tokens) {
			switch (cnt++) {
				case 0:
					accountProfileIndex = t;
					break;
				case 1:
					code = t;
					break;
				default:
					break;
			}
		}

		return conManager.insertAlowedTransactions(accountProfileIndex,code);
	}

	int CCardConfig::processIsoAllowedTransactionsLine(std::string line,std::string &sql, bool first) {
		std::string accountProfileIndex,code;
		boost::char_separator<char> sep(",", "", boost::keep_empty_tokens);
		boost::tokenizer< boost::char_separator<char> > tokens(line, sep);
		CCommsSettings settings;
		int cnt=0;

		BOOST_FOREACH (const std::string& t, tokens) {
			switch (cnt++) {
				case 0:
					accountProfileIndex = t;
					break;
				case 1:
					code = t;
					break;
				default:
					break;
			}
		}

		if (first)
			sql = sql + "'"+accountProfileIndex + "' AS account_profile_index,'"+code+ "' AS code\r\n";
		else
			sql = sql + " UNION SELECT '"+ accountProfileIndex+"','"+code+"'\r\n";

		return DB_OK;
	}

	int CCardConfig::processAlienoAccountProfileFile(std::string path) {
		bool nextByteEscaped = false;
		std::string line;
		int ret;

		std::ifstream ifs(path.append("/").append(ALIENO_ACC_PROFILE_FILE_NAME).c_str());
		if (!ifs.is_open())
			return DB_FAIL;

		std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
		str = str.substr(14,str.length()-14); // version
		BOOST_FOREACH (char& c , str) {
			if (c == '\\') {
				nextByteEscaped = true;
				continue;
			}
			if (!nextByteEscaped && c == '.') {
				std::size_t found = line.find("\n");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				found = line.find("\r");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				ret = processAlienoAccountProfileLine(line);
				if (ret != DB_OK)
					return ret;

				line = "";
				continue;
			}

			line += c;
			nextByteEscaped = false;
		}

		return DB_OK;
	}

	int CCardConfig::processIsoAccountProfileFile(std::string path) {
		bool nextByteEscaped = false;
		std::string line;
		int ret;

		std::ifstream ifs(path.append("/").append(ISO_ACC_PROFILE_FILE_NAME).c_str());
		if (!ifs.is_open())
			return DB_FAIL;

		std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
		if (str.length() < 4)
			return DB_OK;
		str = str.substr(4,str.length()-4); // version

		std::string sql = "INSERT INTO account_profile (code,draft_capture_mode,allow_voice_app,allow_manual_entry,require_pin_entry,require_signature,check_card_value,skim_card_value,allow_budget,budget_amount_min,emv_fallback_limit_index,local_limit_index,offline_limit_index,contactless_limit_index,account_profile_host_id,account_profile_index) SELECT ";
		int cnt=0;
		bool first = true;


		BOOST_FOREACH (char& c , str) {
			if (c == '\\') {
				nextByteEscaped = true;
				continue;
			}
			if (!nextByteEscaped && c == '.') {
				std::size_t found = line.find("\n");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				found = line.find("\r");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				ret = processIsoAccountProfileLine(line, sql, first);

				first = false;
				cnt ++;

				if (cnt > 499) {
					ret = conManager.writeToDB(sql, false);
					sql = "INSERT INTO account_profile (code,draft_capture_mode,allow_voice_app,allow_manual_entry,require_pin_entry,require_signature,check_card_value,skim_card_value,allow_budget,budget_amount_min,emv_fallback_limit_index,local_limit_index,offline_limit_index,contactless_limit_index,account_profile_host_id,account_profile_index) SELECT ";
					cnt = 0;
					first = true;
				}

				line = "";
				continue;
			}

			line += c;
			nextByteEscaped = false;
		}

		if (cnt>0)
			ret = conManager.writeToDB(sql, false);

		return DB_OK;
	}

	int CCardConfig::processAlienoAccountProfileLine(std::string line) {
		std::string accProfileIndex, code,allowVoiceAproval,draftCaptureCode,requireCardPinEntry,emvFallBackIndex,localLimitIndex,OfflineLimitIndex,requireSignature,allowManualEntry,allowBudget,budgetAmtMin,accountProfileHostId,contactlessIndex;
		boost::char_separator<char> sep(",", "", boost::keep_empty_tokens);
		boost::tokenizer< boost::char_separator<char> > tokens(line, sep);
		CCommsSettings settings;
		int cnt=0;

		BOOST_FOREACH (const std::string& t, tokens) {
			switch (cnt++) {
				case 0:
					accProfileIndex = t;
					break;
				case 1:
					code = t;
					break;
				case 2:
					allowVoiceAproval = t;
					break;
				case 3:
					draftCaptureCode = t;
					break;
				case 4:
					requireCardPinEntry = t;
					break;
				case 8:
					emvFallBackIndex = t;
					break;
				case 9:
					localLimitIndex = t;
					break;
				case 10:
					OfflineLimitIndex = t;
					break;
				case 11:
					requireSignature = t;
					break;
				case 12:
					allowManualEntry = t;
					break;
				case 13:
					allowBudget = t;
					break;
				case 14:
					budgetAmtMin = t;
					break;
				case 15:
					accountProfileHostId = t;
					break;
				case 16:
					contactlessIndex = t;
					break;
				default:
					break;
			}
		}
		return conManager.insertAccountProfile(accProfileIndex, "1","1",code,allowVoiceAproval,draftCaptureCode,requireCardPinEntry,emvFallBackIndex,localLimitIndex,OfflineLimitIndex,requireSignature,allowManualEntry,allowBudget,budgetAmtMin,accountProfileHostId,contactlessIndex);
	}

	int CCardConfig::processIsoAccountProfileLine(std::string line,std::string &sql, bool first) {
		std::string accProfileIndex, code,allowVoiceAproval,draftCaptureCode,requireCardPinEntry,emvFallBackIndex,localLimitIndex,OfflineLimitIndex,requireSignature,allowManualEntry,allowBudget,budgetAmtMin,accountProfileHostId,contactlessIndex;
		boost::char_separator<char> sep(",", "", boost::keep_empty_tokens);
		boost::tokenizer< boost::char_separator<char> > tokens(line, sep);
		CCommsSettings settings;
		int cnt=0;

		BOOST_FOREACH (const std::string& t, tokens) {
			switch (cnt++) {
				case 0:
					accProfileIndex = t;
					break;
				case 1:
					code = t;
					break;
				case 2:
					allowVoiceAproval = t;
					break;
				case 3:
					draftCaptureCode = t;
					break;
				case 4:
					requireCardPinEntry = t;
					break;
				case 8:
					emvFallBackIndex = t;
					break;
				case 9:
					localLimitIndex = t;
					break;
				case 10:
					OfflineLimitIndex = t;
					break;
				case 11:
					requireSignature = t;
					break;
				case 12:
					allowManualEntry = t;
					break;
				case 13:
					allowBudget = t;
					break;
				case 14:
					budgetAmtMin = t;
					break;
				case 15:
					accountProfileHostId = t;
					break;
				case 16:
					contactlessIndex = t;
					break;
				default:
					break;
			}
		}

		if (accountProfileHostId.empty())
			accountProfileHostId = accProfileIndex;

		if (first)
			sql = sql + "'"+code + "' AS code,'"+draftCaptureCode+ "' AS draft_capture_mode,'"+allowVoiceAproval+ "' AS allow_voice_app,'"+allowManualEntry+ "' AS allow_manual_entry,'"
			+requireCardPinEntry+ "' AS require_pin_entry,'"+requireSignature+ "' AS require_signature,'1' AS check_card_value,'1' AS skim_card_value,'"+allowBudget+ "' AS allow_budget,'"
			+budgetAmtMin+ "' AS budget_amount_min,'"+emvFallBackIndex+ "' AS emv_fallback_limit_index,'"+localLimitIndex+ "' AS local_limit_index,'"+OfflineLimitIndex+ "' AS offline_limit_index,'"
			+contactlessIndex+ "' AS contactless_limit_index,'"+accountProfileHostId+ "' AS account_profile_host_id,'"+accProfileIndex+ "' AS account_profile_index\r\n";
		else
			sql = sql + " UNION SELECT '"+ code+"','"+draftCaptureCode+"','"+allowVoiceAproval+"','"+allowManualEntry+"','"+requireCardPinEntry+
			"','"+requireSignature+"','1','1','"+allowBudget+"','"+budgetAmtMin+"','"+emvFallBackIndex+
			"','"+localLimitIndex+"','"+OfflineLimitIndex+"','"+contactlessIndex+"','"+accountProfileHostId+"','"+accProfileIndex+"'\r\n";

		return DB_OK;
	}

	int CCardConfig::processAlienoAttachedAccountsFile(std::string path) {
		bool nextByteEscaped = false;
		std::string line;
		int ret;

		std::ifstream ifs(path.append("/").append(ALIENO_ATTACHED_ACC_FILE_NAME).c_str());
		if (!ifs.is_open())
			return DB_FAIL;

		std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
		str = str.substr(14,str.length()-14); // version
		BOOST_FOREACH (char& c , str) {
			if (c == '\\') {
				nextByteEscaped = true;
				continue;
			}
			if (!nextByteEscaped && c == '.') {
				std::size_t found = line.find("\n");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				found = line.find("\r");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				ret = processAlienoAttachedAccountsLine(line);
				if (ret != DB_OK)
					return ret;

				line = "";
				continue;
			}

			line += c;
			nextByteEscaped = false;
		}

		return DB_OK;
	}

	int CCardConfig::processIsoAttachedAccountsFile(std::string path) {
		bool nextByteEscaped = false;
		std::string line;
		int ret;

		std::ifstream ifs(path.append("/").append(ISO_ATTACHED_ACC_FILE_NAME).c_str());
		if (!ifs.is_open())
			return DB_FAIL;

		std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
		if (str.length() < 4)
			return DB_OK;
		str = str.substr(4,str.length()-4); // version
		std::string sql = "INSERT INTO attached_account (totals_group_name,card_product_id,account_profile_index) SELECT ";
		int cnt=0;
		bool first = true;

		BOOST_FOREACH (char& c , str) {
			if (c == '\\') {
				nextByteEscaped = true;
				continue;
			}
			if (!nextByteEscaped && c == '.') {
				std::size_t found = line.find("\n");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				found = line.find("\r");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");

				ret = processIsoAttachedAccountsLine(line,sql,first);

				first = false;

				cnt ++;

				if (cnt > 499) {
					ret = conManager.writeToDB(sql, false);
					sql = "INSERT INTO attached_account (totals_group_name,card_product_id,account_profile_index) SELECT ";
					cnt = 0;
					first = true;
				}


				line = "";
				continue;
			}

			line += c;
			nextByteEscaped = false;
		}

		if (cnt>0)
			ret = conManager.writeToDB(sql, false);

		return DB_OK;
	}

	int CCardConfig::processAlienoAttachedAccountsLine(std::string line) {
		std::string cardProfileIndex, accountProfileIndex,totalsGroupName;
		boost::char_separator<char> sep(",", "", boost::keep_empty_tokens);
		boost::tokenizer< boost::char_separator<char> > tokens(line, sep);
		CCommsSettings settings;
		int cnt=0;

		BOOST_FOREACH (const std::string& t, tokens) {
			switch (cnt++) {
				case 0:
					cardProfileIndex = t;
					break;
				case 1:
					accountProfileIndex = t;
					break;
				case 2:
					totalsGroupName = t;
					break;
				default:
					break;
			}
		}
		return conManager.insertAttachedAccounts(cardProfileIndex,accountProfileIndex,totalsGroupName);
	}

	int CCardConfig::processIsoAttachedAccountsLine(std::string line,std::string &sql, bool first) {
		std::string cardProductId, accountProfileIndex,totalsGroupName;
		boost::char_separator<char> sep(",", "", boost::keep_empty_tokens);
		boost::tokenizer< boost::char_separator<char> > tokens(line, sep);
		CCommsSettings settings;
		int cnt=0;

		BOOST_FOREACH (const std::string& t, tokens) {
			switch (cnt++) {
				case 0:
					cardProductId = t;
					break;
				case 1:
					accountProfileIndex = t;
					break;
				case 2:
					totalsGroupName = t;
					break;
				default:
					break;
			}
		}

		if (first)
			sql = sql + "'"+totalsGroupName + "' AS totals_group_name,'"+cardProductId+ "' AS card_product_id,'"+accountProfileIndex+ "' AS account_profile_index\r\n";
		else
			sql = sql + " UNION SELECT '"+ totalsGroupName+"','"+cardProductId+"','"+accountProfileIndex+"'\r\n";

		return DB_OK;
	}

	int CCardConfig::processAlienoAidFile(std::string path) {
		bool nextByteEscaped = false;
		std::string line;
		int ret;

		std::ifstream ifs(path.append("/").append(ALIENO_AID_FILE_NAME).c_str());
		if (!ifs.is_open()) {
//			printf("Open failed with: %s\n",strerror(errno));
			return DB_FAIL;
		}


		std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
		str = str.substr(14,str.length()-14); // version
		BOOST_FOREACH (char& c , str) {
			if (c == '\\') {
				nextByteEscaped = true;
				continue;
			}
			if (!nextByteEscaped && c == '.') {
				std::size_t found = line.find("\n");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				found = line.find("\r");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				ret = processAlienoAidLine(line);
				if (ret != DB_OK)
					return ret;

				line = "";
				continue;
			}

			line += c;
			nextByteEscaped = false;
		}


		return DB_OK;

	}

	int CCardConfig::processIsoAidFile(std::string path) {
		bool nextByteEscaped = false;
		std::string line;
		int ret;

		std::ifstream ifs(path.append("/").append(ISO_AID_FILE_NAME).c_str());
		if (!ifs.is_open()) {
//			printf("Open failed with: %s\n",strerror(errno));
			return DB_FAIL;
		}

		std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
		if (str.length() < 4)
			return DB_OK;
		str = str.substr(4,str.length()-4); // version

		std::string sql = "INSERT INTO aid (aid,application_version,default_ddol,default_tdol,target_percentage_max,target_percentage,threshold_amount,emv_scheme_code,totals_group_name,tac_default,tac_decline,tac_online,card_data_input_mode,account_profile_host_id,latest_application_version,partial_matching_allowed) SELECT ";
		int cnt=0;
		bool first = true;

		BOOST_FOREACH (char& c , str) {
			if (c == '\\') {
				nextByteEscaped = true;
				continue;
			}
			if (!nextByteEscaped && c == '.') {
				std::size_t found = line.find("\n");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				found = line.find("\r");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				ret = processIsoAidLine(line,sql,first);
				first = false;

				cnt ++;

				if (cnt > 499) {
					ret = conManager.writeToDB(sql, false);
					sql = "INSERT INTO aid (aid,application_version,default_ddol,default_tdol,target_percentage_max,target_percentage,threshold_amount,emv_scheme_code,totals_group_name,tac_default,tac_decline,tac_online,card_data_input_mode,account_profile_host_id,latest_application_version,partial_matching_allowed) SELECT ";
					cnt = 0;
					first = true;
				}

				line = "";
				continue;
			}

			line += c;
			nextByteEscaped = false;
		}

		if (cnt>0)
			ret = conManager.writeToDB(sql, false);

		return DB_OK;

	}

	int CCardConfig::processAlienoPublicKeyFile(std::string path) {
		bool nextByteEscaped = false;
		std::string line;
		int ret;

		std::ifstream ifs(path.append("/").append(ISO_PUBLIC_KEY_FILE_NAME).c_str());
		if (!ifs.is_open()) {
//			printf("Open failed with: %s\n",strerror(errno));
			return DB_FAIL;
		}


		std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
		str = str.substr(14,str.length()-14); // version
		BOOST_FOREACH (char& c , str) {
			if (c == '\\') {
				nextByteEscaped = true;
				continue;
			}
			if (!nextByteEscaped && c == '.') {
				std::size_t found = line.find("\n");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				found = line.find("\r");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				ret = processAlienoPublicKeyLine(line);
				if (ret != DB_OK)
					return ret;

				line = "";
				continue;
			}

			line += c;
			nextByteEscaped = false;
		}


		return DB_OK;

	}

	int CCardConfig::processIsoAidOverrideFile(std::string path) {
		bool nextByteEscaped = false;
		std::string line;
		int ret;

		std::ifstream ifs(path.append("/").append(ISO_AID_OVERRIDE_FILE_NAME).c_str());
		if (!ifs.is_open()) {
//			printf("Open failed with: %s\n",strerror(errno));
			return DB_FAIL;
		}


		std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
		if (str.length() < 4)
			return DB_OK;
		str = str.substr(4,str.length()-4); // version

		BOOST_FOREACH (char& c , str) {
			if (c == '\\') {
				nextByteEscaped = true;
				continue;
			}
			if (!nextByteEscaped && c == '.') {
				std::size_t found = line.find("\n");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				found = line.find("\r");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				ret = processIsoAidOverrideLine(line);
				if (ret != DB_OK)
					return ret;

				line = "";
				continue;
			}

			line += c;
			nextByteEscaped = false;
		}


		return DB_OK;

	}

	int CCardConfig::processIsoPublicKeyFile(std::string path) {
		bool nextByteEscaped = false;
		std::string line;
		int ret;

		std::ifstream ifs(path.append("/").append(ISO_PUBLIC_KEY_FILE_NAME).c_str());
		if (!ifs.is_open()) {
//			printf("Open failed with: %s\n",strerror(errno));
			return DB_FAIL;
		}


		std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
		if (str.length() < 4)
			return DB_OK;
		str = str.substr(4,str.length()-4); // version

		BOOST_FOREACH (char& c , str) {
			if (c == '\\') {
				nextByteEscaped = true;
				continue;
			}
			if (!nextByteEscaped && c == '.') {
				std::size_t found = line.find("\n");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				found = line.find("\r");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				ret = processIsoPublicKeyLine(line);
				if (ret != DB_OK)
					return ret;

				line = "";
				continue;
			}

			line += c;
			nextByteEscaped = false;
		}


		return DB_OK;

	}

	int CCardConfig::processIsoBinFile(std::string path) {
		bool nextByteEscaped = false;
		std::string line;
		int ret;

		std::ifstream ifs(path.append("/").append(ISO_BIN_FILE_NAME).c_str());
		if (!ifs.is_open()) {
//			printf("Open failed with: %s\n",strerror(errno));
			return DB_FAIL;
		}


		std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
		if (str.length() < 4)
			return DB_OK;

		std::string version = str.substr(0,4);

		str = str.substr(4,str.length()-4); // version

		std::string sql = "INSERT INTO bin (bin,card_length,card_product_id,card_issued_name,excluded) SELECT ";
		int cnt=0;
		bool first = true;

		BOOST_FOREACH (char& c , str) {
			if (c == '\\') {
				nextByteEscaped = true;
				continue;
			}
			if (!nextByteEscaped && c == '.') {
				std::size_t found = line.find("\n");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				found = line.find("\r");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");

				processIsoBinLine(line, sql, first);
				first = false;

				cnt ++;

				if (cnt > 499) {
					ret = conManager.writeToDB(sql, false);
					sql = "INSERT INTO bin (bin,card_length,card_product_id,card_issued_name,excluded) SELECT ";
					cnt = 0;
					first = true;
				}

				line = "";
				continue;
			}

			line += c;
			nextByteEscaped = false;
		}

		if (cnt>0)
			ret = conManager.writeToDB(sql, false);


		CVersions versions;
		CVersionsFile file;
		file.setName(VERSIONS_PARM_CARD_FILE_NAME);
		file.setPath(path);
		file.setVersion(version);
		versions.addFile(VERSIONS_PARM_APP_NAME,file);

		return DB_OK;
	}

	int CCardConfig::getVersion(std::string &version) {
		CVersions versions;

		std::map<int,CVersionsFile> files;
		int ret = versions.getFiles(VERSIONS_PARM_APP_NAME,files);
		if (ret != DB_OK)
			return DB_FAIL;
		else {
			std::map<int,CVersionsFile>::iterator it2;
			for (it2=files.begin(); it2!=files.end(); ++it2) {
				CVersionsFile file = it2->second;

				if (file.getName().compare(VERSIONS_PARM_CARD_FILE_NAME) == 0) {
					version = file.getVersion();
					return DB_OK;
				}
			}
		}

		return DB_FAIL;
	}

	int CCardConfig::processAlienoBinFile(std::string path) {
		bool nextByteEscaped = false;
		std::string line;
		int ret;

		std::ifstream ifs(path.append("/").append(ALIENO_BIN_FILE_NAME).c_str());
		if (!ifs.is_open()) {
//			printf("Open failed with: %s\n",strerror(errno));
			return DB_FAIL;
		}


		std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
		str = str.substr(14,str.length()-14); // version

		BOOST_FOREACH (char& c , str) {
			if (c == '\\') {
				nextByteEscaped = true;
				continue;
			}
			if (!nextByteEscaped && c == '.') {
				std::size_t found = line.find("\n");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				found = line.find("\r");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");


				ret = processAlienoBinLine(line);
				if (ret != DB_OK)
					return ret;

				line = "";
				continue;
			}

			line += c;
			nextByteEscaped = false;
		}


		return DB_OK;
	}

	int CCardConfig::processAlienoAidLine(std::string line) {
		std::string aid,emvCode,accountProfileHostId,totalsGroupName,appVersion,ddol,tdol,percentageMax,percentage,amount,tacDefault,tacDecline,tacOnline,cardDataInputMode="DF";
		boost::char_separator<char> sep(",", "", boost::keep_empty_tokens);
		boost::tokenizer< boost::char_separator<char> > tokens(line, sep);
		CCommsSettings settings;
		int cnt=0;

		BOOST_FOREACH (const std::string& t, tokens) {
			switch (cnt++) {
				case 0:
					aid = t;
					break;
				case 1:
					emvCode = t;
					break;
				case 2:
					accountProfileHostId = t;
					break;
				case 3:
					totalsGroupName = t;
					break;
				case 4:
					appVersion = t;
					break;
				case 5:
					ddol = t;
					break;
				case 6:
					tdol = t;
					break;
				case 7:
					percentageMax = t;
					break;
				case 8:
					percentage = t;
					break;
				case 9:
					amount = t;
					break;
				case 10:
					tacDefault = t;
					break;
				case 11:
					tacDecline = t;
					break;
				case 12:
					tacOnline = t;
					break;
				case 13:
					cardDataInputMode = t;
					break;
				default:
					break;
			}
		}

		return conManager.insertAid(aid,emvCode,accountProfileHostId,totalsGroupName,appVersion,ddol,tdol,percentageMax,percentage,amount,tacDefault,tacDecline,tacOnline,cardDataInputMode);
	}

	int CCardConfig::processIsoAidLine(std::string line,std::string &sql, bool first) {
		std::string aid,emvCode,accountProfileHostId,totalsGroupName,appVersion,ddol,tdol,percentageMax,percentage,amount,tacDefault,tacDecline,tacOnline,latestAppVersion,partialMatchAll,cardDataInputMode="DF";
		boost::char_separator<char> sep(",", "", boost::keep_empty_tokens);
		boost::tokenizer< boost::char_separator<char> > tokens(line, sep);
		CCommsSettings settings;
		int cnt=0;

		BOOST_FOREACH (const std::string& t, tokens) {
			switch (cnt++) {
				case 0:
					aid = t;
					break;
				case 1:
					emvCode = t;
					break;
				case 2:
					accountProfileHostId = t;
					break;
				case 3:
					totalsGroupName = t;
					break;
				case 4:
					appVersion = t;
					break;
				case 5:
					ddol = t;
					break;
				case 6:
					tdol = t;
					break;
				case 7:
					percentageMax = t;
					break;
				case 8:
					percentage = t;
					break;
				case 9:
					amount = t;
					break;
				case 10:
					tacDefault = t;
					break;
				case 11:
					tacDecline = t;
					break;
				case 12:
					tacOnline = t;
					break;
				case 13:
					latestAppVersion = t;
					break;
				case 14:
					partialMatchAll = t;
					break;
				case 15:
					cardDataInputMode = t;
					break;
				default:
					break;
			}
		}

		if (first)
			sql = sql + "'"+aid+ "' AS aid,'"+appVersion+ "' AS application_version,'"+ddol+"' AS default_ddol,'"+tdol+"' AS default_tdol,'"
			+percentageMax+ "' AS target_percentage_max,'"+percentage+ "' AS target_percentage,'"+amount+ "' AS threshold_amount,'"
			+emvCode+ "' AS emv_scheme_code,'"+totalsGroupName+ "' AS totals_group_name,'"+tacDefault+ "' AS tac_default,'"
			+tacDecline+ "' AS tac_decline,'"+tacOnline+ "' AS tac_online,'"+cardDataInputMode+ "' AS card_data_input_mode,'"
			+accountProfileHostId+"' AS account_profile_host_id,'"+latestAppVersion+"' AS latest_application_version,'"+partialMatchAll+"' AS partial_matching_allowed\r\n";
		else
			sql = sql + " UNION SELECT '"+ aid+"','"+appVersion+"','"+ddol+"','"+tdol+"','"+percentageMax+"','"+percentage+"','"+amount+"','"+emvCode
			+"','"+totalsGroupName+"','"+tacDefault+"','"+tacDecline+"','"+tacOnline+"','"+cardDataInputMode+"','"+accountProfileHostId+"','"+latestAppVersion+"','"+partialMatchAll+"'\r\n";

		return DB_OK;
	}

	int CCardConfig::processIsoAidOverrideLine(std::string line) {
		std::string aid, cardProductId,accountProfileId,totalsGroup;
		boost::char_separator<char> sep(",", "", boost::keep_empty_tokens);
		boost::tokenizer< boost::char_separator<char> > tokens(line, sep);
		CCommsSettings settings;
		int cnt=0;

		BOOST_FOREACH (const std::string& t, tokens) {
			switch (cnt++) {
				case 0:
					cardProductId = t;
					break;
				case 1:
					aid = t;
					break;
				case 2:
					accountProfileId = t;
					break;
				case 3:
					totalsGroup = t;
					break;
				default:
					break;
			}
		}

		return conManager.insertAidOverride(aid, cardProductId,accountProfileId,totalsGroup);
	}

	int CCardConfig::processAlienoPublicKeyLine(std::string line) {
		std::string rid, publicKeyIndex,hashAlgorithmIndicator,publicKeyAlgorithmIndicator,modulus,exponent,checkSum,activation,expiry;
		boost::char_separator<char> sep(",", "", boost::keep_empty_tokens);
		boost::tokenizer< boost::char_separator<char> > tokens(line, sep);
		CCommsSettings settings;
		int cnt=0;

		BOOST_FOREACH (const std::string& t, tokens) {
			switch (cnt++) {
				case 0:
					rid = t;
					break;
				case 1:
					publicKeyIndex = t;
					break;
				case 2:
					hashAlgorithmIndicator = t;
					break;
				case 3:
					publicKeyAlgorithmIndicator = t;
					break;
				case 4:
					modulus = t;
					break;
				case 5:
					exponent = t;
					break;
				case 6:
					checkSum = t;
					break;
				case 7:
					activation = t;
					break;
				case 8:
					expiry = t;
					break;
				default:
					break;
			}
		}

		return conManager.insertPublicKey(rid, publicKeyIndex,hashAlgorithmIndicator,publicKeyAlgorithmIndicator,modulus,exponent,checkSum,activation,expiry);
	}

	int CCardConfig::processIsoPublicKeyLine(std::string line) {
		std::string rid, publicKeyIndex,hashAlgorithmIndicator,publicKeyAlgorithmIndicator,modulus,exponent,checkSum,activation,expiry;
		boost::char_separator<char> sep(",", "", boost::keep_empty_tokens);
		boost::tokenizer< boost::char_separator<char> > tokens(line, sep);
		CCommsSettings settings;
		int cnt=0;

		BOOST_FOREACH (const std::string& t, tokens) {
			switch (cnt++) {
				case 0:
					rid = t;
					rid  = rtrim(rid);
					break;
				case 1:
					publicKeyIndex = t;
					publicKeyIndex = rtrim(publicKeyIndex);
					break;
				case 2:
					hashAlgorithmIndicator = t;
					hashAlgorithmIndicator = rtrim(hashAlgorithmIndicator);
					break;
				case 3:
					publicKeyAlgorithmIndicator = t;
					publicKeyAlgorithmIndicator = rtrim(publicKeyAlgorithmIndicator);
					break;
				case 4:
					modulus = t;
					modulus = rtrim(modulus);
					break;
				case 5:
					exponent = t;
					exponent = rtrim(exponent);
					break;
				case 6:
					checkSum = t;
					checkSum = rtrim(checkSum);
					break;
				case 7:
					activation = t;
					activation = rtrim(activation);
					break;
				case 8:
					expiry = t;
					expiry = rtrim(expiry);
					break;
				default:
					break;
			}
		}

		return conManager.insertPublicKey(rid, publicKeyIndex,hashAlgorithmIndicator,publicKeyAlgorithmIndicator,modulus,exponent,checkSum,activation,expiry);
	}

	int CCardConfig::processAlienoForeignCardLine(std::string line) {

		std::string index, foreignCard;
		boost::char_separator<char> sep(",", "", boost::keep_empty_tokens);
		boost::tokenizer< boost::char_separator<char> > tokens(line, sep);
		CCommsSettings settings;
		int cnt=0;

		BOOST_FOREACH (const std::string& t, tokens) {
			switch (cnt++) {
				case 0:
					index = t;
					break;
				case 1:
					index = t;
					break;
				case 2:
					foreignCard = t;
					break;
				default:
					break;
			}
		}
		std::string sql = "UPDATE card_profile SET foreign_card = '"+foreignCard+"' WHERE card_profile_index = '"+SSTR(index)+"';";
		return conManager.writeToDB(sql,false);
	}

	int CCardConfig::processIsoCardProductsLine(std::string line,std::string &sql, bool first) {

		std::string cardProfileIndex, cardProductId,foreignCard;
		boost::char_separator<char> sep(",", "", boost::keep_empty_tokens);
		boost::tokenizer< boost::char_separator<char> > tokens(line, sep);
		CCommsSettings settings;
		int cnt=0;

		BOOST_FOREACH (const std::string& t, tokens) {
			switch (cnt++) {
				case 0:
					cardProductId = t;
					break;
				case 1:
					cardProfileIndex = t;
					break;
				case 2:
					foreignCard = t;
					break;
				default:
					break;
			}
		}
		if (first)
			sql = sql + "'"+cardProductId + "' AS card_product_id,'"+cardProfileIndex+ "' AS card_profile_index,'"+foreignCard+ "' AS is_foreign\r\n";
		else
			sql = sql + " UNION SELECT '"+ cardProductId+"','"+cardProfileIndex+"','"+foreignCard+"'\r\n";

		std::string sql2 = "UPDATE card_profile SET foreign_card = '"+foreignCard+"' WHERE card_profile_index = '"+SSTR(cardProfileIndex)+"';";
		return conManager.writeToDB(sql2,false);
	}


	int CCardConfig::processIsoCardProfileLine(std::string line,std::string &sql, bool first) {
		std::string index, active,checkServiceCode,checkExpiryDate,pinLenMin,pinLenMax,checkLuhn,approvalText;
		boost::char_separator<char> sep(",", "", boost::keep_empty_tokens);
		boost::tokenizer< boost::char_separator<char> > tokens(line, sep);
		CCommsSettings settings;
		int cnt=0;

		BOOST_FOREACH (const std::string& t, tokens) {
			switch (cnt++) {
				case 0:
					index = t;
					break;
				case 3:
					active = t;
					break;
				case 7:
					checkServiceCode = t;
					break;
				case 11:
					checkExpiryDate = t;
					break;
				case 13:
					pinLenMin = t;
					break;
				case 14:
					pinLenMax = t;
					break;
				case 15:
					checkLuhn = t;
					break;
				case 16:
					approvalText = t;
					break;
				default:
					break;
			}
		}

		if (first)
			sql = sql + "'"+active + "' AS active,'"+checkServiceCode+ "' AS check_service_code,'"+checkExpiryDate+ "' AS check_expiry_date,'"+checkLuhn+ "' AS check_luhn,'"+pinLenMin+ "' AS pin_length_min,'"+pinLenMax+ "' AS pin_length_max,'"+approvalText+ "' AS voice_approval_text,'"+index+ "' AS card_profile_index\r\n";
		else
			sql = sql + " UNION SELECT '"+ active+"','"+checkServiceCode+"','"+checkExpiryDate+"','"+checkLuhn+"','"+pinLenMin+"','"+pinLenMax+"','"+approvalText+"','"+index+"'\r\n";

		return DB_OK;
	}

	int CCardConfig::processAlienoCardProfileLine(std::string line) {
		std::string index, active,checkServiceCode,checkExpiryDate,pinLenMin,pinLenMax,checkLuhn,approvalText;
		boost::char_separator<char> sep(",", "", boost::keep_empty_tokens);
		boost::tokenizer< boost::char_separator<char> > tokens(line, sep);
		CCommsSettings settings;
		int cnt=0;

		BOOST_FOREACH (const std::string& t, tokens) {
			switch (cnt++) {
				case 0:
					index = t;
					break;
				case 3:
					active = t;
					break;
				case 7:
					checkServiceCode = t;
					break;
				case 11:
					checkExpiryDate = t;
					break;
				case 13:
					pinLenMin = t;
					break;
				case 14:
					pinLenMax = t;
					break;
				case 15:
					checkLuhn = t;
					break;
				case 16:
					approvalText = t;
					break;
				default:
					break;
			}
		}

		return conManager.insertCardProfile(index,active,checkServiceCode,checkExpiryDate,pinLenMin,pinLenMax,checkLuhn,approvalText);
	}

	int CCardConfig::processIsoBinLine(std::string line,std::string &sql, bool first) {
		std::string bin,cardLen,cardProductId,issuedName,excluded;
		boost::char_separator<char> sep(",", "", boost::keep_empty_tokens);
		boost::tokenizer< boost::char_separator<char> > tokens(line, sep);
		int cnt=0;

		BOOST_FOREACH (const std::string& t, tokens) {
			switch (cnt++) {
				case 0:
					// bin
					bin = t;
					break;
				case 1:
					// card len
					cardLen = t;
					break;
				case 2:
					// card profile index
					cardProductId = t;
					break;
				case 3:
					// issued name
					issuedName = t;
					break;
				case 4:
					// excluded
					excluded = t;
					break;
			}
		}


		if (first)
			sql = sql + "'"+bin + "' AS bin,'"+cardLen+ "' AS card_length,'"+cardProductId+ "' AS card_product_id,'"+issuedName+ "' AS card_issued_name,'"+excluded+ "' AS excluded\r\n";
		else
			sql = sql + " UNION SELECT '"+ bin+"','"+cardLen+"','"+cardProductId+"','"+issuedName+"','"+excluded+"'\r\n";

		return DB_OK;
	}

	int CCardConfig::processAlienoBinLine(std::string line) {
		std::string bin,cardLen,cardProfileIndex,issuedName,excluded;
		boost::char_separator<char> sep(",", "", boost::keep_empty_tokens);
		boost::tokenizer< boost::char_separator<char> > tokens(line, sep);
		int cnt=0;

		BOOST_FOREACH (const std::string& t, tokens) {
			switch (cnt++) {
				case 0:
					// bin
					bin = t;
					break;
				case 1:
					// card len
					cardLen = t;
					break;
				case 2:
					// card profile index
					cardProfileIndex = t;
					break;
				case 3:
					// issued name
					issuedName = t;
					break;
				case 4:
					// excluded
					excluded = t;
					break;
			}
		}

		return(conManager.insertBin(bin,cardLen,cardProfileIndex,issuedName));
	}

	// trim from end
	std::string &CCardConfig::rtrim(std::string &s) {
	        s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
	        return s;
	}
}
