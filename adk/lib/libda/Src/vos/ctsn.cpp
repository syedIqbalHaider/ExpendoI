#include <stdio.h>
#include <stdlib.h>
#include "libda/ctsn.h"
#include "libda/cconmanager.h"


using namespace com_verifone_conmanager;

namespace com_verifone_tsn
{

	CConManager conManager;

	CTsn::CTsn(){}

	int CTsn::getNextTsn() {
		std::string sql = "SELECT * FROM tsn WHERE tsn_id = 1";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1)
			return DB_FAIL;

		std::string val;
		int ret = conManager.getColumnValue("tsn",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;

		int tsn = atoi(val.c_str());

		if(tsn>=999999)
			tsn=0;

		tsn += 1;
		sql = "UPDATE tsn SET tsn = "+SSTR(tsn)+" WHERE tsn_id = 1";
		conManager.writeToDB(sql);

		return tsn;
	}

	int CTsn::getCurrentTsn() {
		std::string sql = "SELECT * FROM tsn WHERE tsn_id = 1";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1)
			return DB_FAIL;

		std::string val;
		int ret = conManager.getColumnValue("tsn",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;

		int tsn = atoi(val.c_str());
		return tsn;
	}

	void CTsn::resetTsn(int tsn) {
		std::string sql = "UPDATE tsn SET tsn = "+SSTR(tsn)+" WHERE tsn_id = 1";
		conManager.writeToDB(sql);
	}
}
