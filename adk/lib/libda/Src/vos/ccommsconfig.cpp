#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <boost/foreach.hpp>
#include <boost/tokenizer.hpp>
#include "libda/ccommsconfig.h"
#include "libda/cconmanager.h"
#include "libda/himdef.h"
#include "libda/cversions.h"
#include "libda/cversionsfile.h"

#define ABSA // ABSA uses different ISO comms format

using namespace com_verifone_commssettings;
using namespace com_verifone_conmanager;
using namespace com_verifone_versions;
using namespace com_verifone_versionsfile;

namespace com_verifone_commsconfig
{
	CConManager conManager;

	CCommsConfig::CCommsConfig(){}

	int CCommsConfig::getPriAuthCommsSettings(CCommsSettings &commsSettings) {

		std::string sql = "SELECT * FROM auth_comms_set;";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1)
			return DB_FAIL;


		std::string val;
		int ret = conManager.getColumnValue("pri_comms_parameters_id",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;

		int parmId = atoi(val.c_str());
		ret = conManager.getColumnValue("pri_comms_type",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;

		commsSettings.setCommsType((CCommsSettings::COMMS_TYPE)atoi(val.c_str()));

		sql = "SELECT * FROM auth_comms_parameters WHERE comms_parameters_id = "+SSTR(parmId)+";";
		records.clear();
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1)
			return DB_FAIL;

		ret = poplateCommsParameters(commsSettings,records);
		return ret;
	}

	int CCommsConfig::getPriSettleCommsSettings(CCommsSettings &commsSettings) {

		std::string sql = "SELECT * FROM settle_comms_set;";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1)
			return DB_FAIL;

		std::string val;
		int ret = conManager.getColumnValue("pri_comms_parameters_id",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;

		int parmId = atoi(val.c_str());

		ret = conManager.getColumnValue("pri_comms_type",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;

		commsSettings.setCommsType((CCommsSettings::COMMS_TYPE)atoi(val.c_str()));

		sql = "SELECT * FROM settle_comms_parameters WHERE comms_parameters_id = "+SSTR(parmId)+";";
		records.clear();
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1)
			return DB_FAIL;

		ret = poplateCommsParameters(commsSettings,records);
		return ret;
	}

	int CCommsConfig::getPriParamCommsSettings(CCommsSettings &commsSettings){

		std::string sql = "SELECT * FROM param_comms_set;";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1)
			return DB_FAIL;

		std::string val;
		int ret = conManager.getColumnValue("pri_comms_parameters_id",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;

		int parmId = atoi(val.c_str());

		ret = conManager.getColumnValue("pri_comms_type",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;

		commsSettings.setCommsType((CCommsSettings::COMMS_TYPE)atoi(val.c_str()));

		sql = "SELECT * FROM param_comms_parameters WHERE comms_parameters_id = "+SSTR(parmId)+";";
		records.clear();
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1)
			return DB_FAIL;

		ret = poplateCommsParameters(commsSettings,records);
		return ret;
	}

	int CCommsConfig::getPriSoftwareDwlndCommsSettings(CCommsSettings &commsSettings) {

		std::string sql = "SELECT * FROM software_comms_set;";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1)
			return DB_FAIL;

		std::string val;
		int ret = conManager.getColumnValue("pri_comms_parameters_id",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;

		int parmId = atoi(val.c_str());

		ret = conManager.getColumnValue("pri_comms_type",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;

		commsSettings.setCommsType((CCommsSettings::COMMS_TYPE)atoi(val.c_str()));

		sql = "SELECT * FROM software_comms_parameters WHERE comms_parameters_id = "+SSTR(parmId)+";";
		records.clear();
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1)
			return DB_FAIL;

		ret = poplateCommsParameters(commsSettings,records);
		return ret;
	}

	int CCommsConfig::setPriParamCommsSettings(CCommsSettings &commsSettings) {

		std::string sql = "SELECT * FROM param_comms_set;";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size() > 1) {

			std::string val;
			int ret = conManager.getColumnValue("pri_comms_parameters_id",val,records,1);
			if (ret != DB_OK)
				return DB_FAIL;

			int priId = atoi(val.c_str());

			sql = "DELETE FROM param_comms_parameters WHERE comms_parameters_id = "+SSTR(priId)+";";
			ret = conManager.writeToDB(sql);
			if (ret != DB_OK)
				return ret;


			int id = conManager.insertCommsParameters("param_comms_parameters",commsSettings);
			if (id < 0)
				return DB_FAIL;

			sql = "UPDATE param_comms_set SET pri_comms_parameters_id = "+SSTR(id)+";";
			ret = conManager.writeToDB(sql);
			if (ret != DB_OK)
				return ret;

			sql = "UPDATE param_comms_set SET pri_comms_type = "+SSTR(commsSettings.getCommsType())+";";
			ret = conManager.writeToDB(sql);
			if (ret != DB_OK)
				return ret;
		} else {
			// create
			int id = conManager.insertCommsParameters("param_comms_parameters",commsSettings);
			if (id < 0)
				return DB_FAIL;

			sql = "INSERT INTO param_comms_set VALUES ("+SSTR(commsSettings.getCommsType())+","+SSTR(id)+","+SSTR(commsSettings.getCommsType())+","+SSTR(SSTR(id)+");");
			int ret = conManager.writeToDB(sql);
			if (ret != DB_OK)
				return ret;
		}

		return DB_OK;
	}

	int CCommsConfig::setPriSettleCommsSettings(CCommsSettings &commsSettings) {

		std::string sql = "SELECT * FROM settle_comms_set;";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size() > 1) {

			std::string val;
			int ret = conManager.getColumnValue("pri_comms_parameters_id",val,records,1);
			if (ret != DB_OK)
				return DB_FAIL;

			int priId = atoi(val.c_str());

			sql = "DELETE FROM settle_comms_parameters WHERE comms_parameters_id = "+SSTR(priId)+";";
			ret = conManager.writeToDB(sql);
			if (ret != DB_OK)
				return ret;


			int id = conManager.insertCommsParameters("settle_comms_parameters",commsSettings);
			if (id < 0)
				return DB_FAIL;

			sql = "UPDATE settle_comms_set SET pri_comms_parameters_id = "+SSTR(id)+";";
			ret = conManager.writeToDB(sql);
			if (ret != DB_OK)
				return ret;

			sql = "UPDATE settle_comms_set SET pri_comms_type = "+SSTR(commsSettings.getCommsType())+";";
			ret = conManager.writeToDB(sql);
			if (ret != DB_OK)
				return ret;
		} else {
			// create
			int id = conManager.insertCommsParameters("settle_comms_parameters",commsSettings);
			if (id < 0)
				return DB_FAIL;

			sql = "INSERT INTO settle_comms_set VALUES ("+SSTR(commsSettings.getCommsType())+","+SSTR(id)+","+SSTR(commsSettings.getCommsType())+","+SSTR(SSTR(id)+");");
			int ret = conManager.writeToDB(sql);
			if (ret != DB_OK)
				return ret;
		}

		return DB_OK;
	}

	int CCommsConfig::setPriAuthCommsSettings(CCommsSettings &commsSettings) {

		std::string sql = "SELECT * FROM auth_comms_set;";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size() > 1) {

			std::string val;
			int ret = conManager.getColumnValue("pri_comms_parameters_id",val,records,1);
			if (ret != DB_OK)
				return DB_FAIL;

			int priId = atoi(val.c_str());

			sql = "DELETE FROM auth_comms_parameters WHERE comms_parameters_id = "+SSTR(priId)+";";
			ret = conManager.writeToDB(sql);
			if (ret != DB_OK)
				return ret;


			int id = conManager.insertCommsParameters("auth_comms_parameters",commsSettings);
			if (id < 0)
				return DB_FAIL;

			sql = "UPDATE auth_comms_set SET pri_comms_parameters_id = "+SSTR(id)+";";
			ret = conManager.writeToDB(sql);
			if (ret != DB_OK)
				return ret;

			sql = "UPDATE auth_comms_set SET pri_comms_type = "+SSTR(commsSettings.getCommsType())+";";
			ret = conManager.writeToDB(sql);
			if (ret != DB_OK)
				return ret;
		} else {
			// create
			int id = conManager.insertCommsParameters("auth_comms_parameters",commsSettings);
			if (id < 0)
				return DB_FAIL;

			sql = "INSERT INTO auth_comms_set VALUES ("+SSTR(commsSettings.getCommsType())+","+SSTR(id)+","+SSTR(commsSettings.getCommsType())+","+SSTR(SSTR(id)+");");
			int ret = conManager.writeToDB(sql);
			if (ret != DB_OK)
				return ret;
		}

		return DB_OK;
	}

	int CCommsConfig::setPriSoftwareCommsSettings(CCommsSettings &commsSettings) {

		std::string sql = "SELECT * FROM software_comms_set;";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size() > 1) {

			std::string val;
			int ret = conManager.getColumnValue("pri_comms_parameters_id",val,records,1);
			if (ret != DB_OK)
				return DB_FAIL;

			int priId = atoi(val.c_str());

			sql = "DELETE FROM software_comms_parameters WHERE comms_parameters_id = "+SSTR(priId)+";";
			ret = conManager.writeToDB(sql);
			if (ret != DB_OK)
				return ret;


			int id = conManager.insertCommsParameters("software_comms_parameters",commsSettings);
			if (id < 0)
				return DB_FAIL;

			sql = "UPDATE software_comms_set SET pri_comms_parameters_id = "+SSTR(id)+";";
			ret = conManager.writeToDB(sql);
			if (ret != DB_OK)
				return ret;

			sql = "UPDATE software_comms_set SET pri_comms_type = "+SSTR(commsSettings.getCommsType())+";";
			ret = conManager.writeToDB(sql);
			if (ret != DB_OK)
				return ret;
		} else {
			// create
			int id = conManager.insertCommsParameters("software_comms_parameters",commsSettings);
			if (id < 0)
				return DB_FAIL;

			sql = "INSERT INTO software_comms_set VALUES ("+SSTR(commsSettings.getCommsType())+","+SSTR(id)+","+SSTR(commsSettings.getCommsType())+","+SSTR(SSTR(id)+");");
			int ret = conManager.writeToDB(sql);
			if (ret != DB_OK)
				return ret;
		}

		return DB_OK;
	}

	int CCommsConfig::setSecAuthCommsSettings(CCommsSettings &commsSettings) {

		std::string sql = "SELECT * FROM auth_comms_set;";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size() > 1) {

			std::string val;
			int ret = conManager.getColumnValue("sec_comms_parameters_id",val,records,1);
			if (ret != DB_OK)
				return DB_FAIL;

			int priId = atoi(val.c_str());

			sql = "DELETE FROM auth_comms_parameters WHERE comms_parameters_id = "+SSTR(priId)+";";
			ret = conManager.writeToDB(sql);
			if (ret != DB_OK)
				return ret;


			int id = conManager.insertCommsParameters("auth_comms_parameters",commsSettings);
			if (id < 0)
				return DB_FAIL;

			sql = "UPDATE auth_comms_set SET sec_comms_parameters_id = "+SSTR(id)+";";
			ret = conManager.writeToDB(sql);
			if (ret != DB_OK)
				return ret;

			sql = "UPDATE auth_comms_set SET sec_comms_type = "+SSTR(commsSettings.getCommsType())+";";
			ret = conManager.writeToDB(sql);
			if (ret != DB_OK)
				return ret;
		} else {
			// create
			int id = conManager.insertCommsParameters("auth_comms_parameters",commsSettings);
			if (id < 0)
				return DB_FAIL;

			sql = "INSERT INTO auth_comms_set VALUES ("+SSTR(commsSettings.getCommsType())+","+SSTR(id)+","+SSTR(commsSettings.getCommsType())+","+SSTR(SSTR(id)+");");
			int ret = conManager.writeToDB(sql);
			if (ret != DB_OK)
				return ret;
		}

		return DB_OK;
	}

	int CCommsConfig::setSecSoftwareCommsSettings(CCommsSettings &commsSettings) {

		std::string sql = "SELECT * FROM software_comms_set;";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size() > 1) {

			std::string val;
			int ret = conManager.getColumnValue("sec_comms_parameters_id",val,records,1);
			if (ret != DB_OK)
				return DB_FAIL;

			int priId = atoi(val.c_str());

			sql = "DELETE FROM software_comms_parameters WHERE comms_parameters_id = "+SSTR(priId)+";";
			ret = conManager.writeToDB(sql);
			if (ret != DB_OK)
				return ret;


			int id = conManager.insertCommsParameters("software_comms_parameters",commsSettings);
			if (id < 0)
				return DB_FAIL;

			sql = "UPDATE software_comms_set SET sec_comms_parameters_id = "+SSTR(id)+";";
			ret = conManager.writeToDB(sql);
			if (ret != DB_OK)
				return ret;

			sql = "UPDATE software_comms_set SET sec_comms_type = "+SSTR(commsSettings.getCommsType())+";";
			ret = conManager.writeToDB(sql);
			if (ret != DB_OK)
				return ret;
		} else {
			// create
			int id = conManager.insertCommsParameters("software_comms_parameters",commsSettings);
			if (id < 0)
				return DB_FAIL;

			sql = "INSERT INTO software_comms_set VALUES ("+SSTR(commsSettings.getCommsType())+","+SSTR(id)+","+SSTR(commsSettings.getCommsType())+","+SSTR(SSTR(id)+");");
			int ret = conManager.writeToDB(sql);
			if (ret != DB_OK)
				return ret;
		}

		return DB_OK;
	}

	int CCommsConfig::setSecParamCommsSettings(CCommsSettings &commsSettings) {

		std::string sql = "SELECT * FROM param_comms_set;";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size() > 1) {

			std::string val;
			int ret = conManager.getColumnValue("sec_comms_parameters_id",val,records,1);
			if (ret != DB_OK)
				return DB_FAIL;

			int priId = atoi(val.c_str());

			sql = "DELETE FROM param_comms_parameters WHERE comms_parameters_id = "+SSTR(priId)+";";
			ret = conManager.writeToDB(sql);
			if (ret != DB_OK)
				return ret;


			int id = conManager.insertCommsParameters("param_comms_parameters",commsSettings);
			if (id < 0)
				return DB_FAIL;

			sql = "UPDATE param_comms_set SET sec_comms_parameters_id = "+SSTR(id)+";";
			ret = conManager.writeToDB(sql);
			if (ret != DB_OK)
				return ret;

			sql = "UPDATE param_comms_set SET sec_comms_type = "+SSTR(commsSettings.getCommsType())+";";
			ret = conManager.writeToDB(sql);
			if (ret != DB_OK)
				return ret;
		} else {
			// create
			int id = conManager.insertCommsParameters("param_comms_parameters",commsSettings);
			if (id < 0)
				return DB_FAIL;

			sql = "INSERT INTO param_comms_set VALUES ("+SSTR(commsSettings.getCommsType())+","+SSTR(id)+","+SSTR(commsSettings.getCommsType())+","+SSTR(SSTR(id)+");");
			int ret = conManager.writeToDB(sql);
			if (ret != DB_OK)
				return ret;
		}

		return DB_OK;
	}

	int CCommsConfig::setSecSettleCommsSettings(CCommsSettings &commsSettings) {

		std::string sql = "SELECT * FROM settle_comms_set;";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size() > 1) {

			std::string val;
			int ret = conManager.getColumnValue("sec_comms_parameters_id",val,records,1);
			if (ret != DB_OK)
				return DB_FAIL;

			int priId = atoi(val.c_str());

			sql = "DELETE FROM settle_comms_parameters WHERE comms_parameters_id = "+SSTR(priId)+";";
			ret = conManager.writeToDB(sql);
			if (ret != DB_OK)
				return ret;


			int id = conManager.insertCommsParameters("settle_comms_parameters",commsSettings);
			if (id < 0)
				return DB_FAIL;

			sql = "UPDATE settle_comms_set SET sec_comms_parameters_id = "+SSTR(id)+";";
			ret = conManager.writeToDB(sql);
			if (ret != DB_OK)
				return ret;

			sql = "UPDATE settle_comms_set SET sec_comms_type = "+SSTR(commsSettings.getCommsType())+";";
			ret = conManager.writeToDB(sql);
			if (ret != DB_OK)
				return ret;
		} else {
			// create
			int id = conManager.insertCommsParameters("settle_comms_parameters",commsSettings);
			if (id < 0)
				return DB_FAIL;

			sql = "INSERT INTO settle_comms_set VALUES ("+SSTR(commsSettings.getCommsType())+","+SSTR(id)+","+SSTR(commsSettings.getCommsType())+","+SSTR(SSTR(id)+");");
			int ret = conManager.writeToDB(sql);
			if (ret != DB_OK)
				return ret;
		}

		return DB_OK;
	}

	int CCommsConfig::getSecAuthCommsSettings(CCommsSettings &commsSettings) {

		std::string sql = "SELECT * FROM auth_comms_set;";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1)
			return DB_FAIL;

		std::string val;
		int ret = conManager.getColumnValue("sec_comms_parameters_id",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;

		int parmId = atoi(val.c_str());

		ret = conManager.getColumnValue("sec_comms_type",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;

		commsSettings.setCommsType((CCommsSettings::COMMS_TYPE)atoi(val.c_str()));

		sql = "SELECT * FROM auth_comms_parameters WHERE comms_parameters_id = "+SSTR(parmId)+";";
		records.clear();
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1)
			return DB_FAIL;

		ret = poplateCommsParameters(commsSettings,records);
		return ret;
	}

	int CCommsConfig::getSecSettleCommsSettings(CCommsSettings &commsSettings) {
		std::string sql = "SELECT * FROM settle_comms_set;";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1)
			return DB_FAIL;

		std::string val;
		int ret = conManager.getColumnValue("sec_comms_parameters_id",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;

		int parmId = atoi(val.c_str());

		ret = conManager.getColumnValue("sec_comms_type",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;

		commsSettings.setCommsType((CCommsSettings::COMMS_TYPE)atoi(val.c_str()));
		sql = "SELECT * FROM settle_comms_parameters WHERE comms_parameters_id = "+SSTR(parmId)+";";
		records.clear();
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1)
			return DB_FAIL;

		ret = poplateCommsParameters(commsSettings,records);
		return ret;
	}

	int CCommsConfig::getSecParamCommsSettings(CCommsSettings &commsSettings) {
		std::string sql = "SELECT * FROM param_comms_set;";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1)
			return DB_FAIL;

		std::string val;
		int ret = conManager.getColumnValue("sec_comms_parameters_id",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;

		int parmId = atoi(val.c_str());

		ret = conManager.getColumnValue("sec_comms_type",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;

		commsSettings.setCommsType((CCommsSettings::COMMS_TYPE)atoi(val.c_str()));

		sql = "SELECT * FROM param_comms_parameters WHERE comms_parameters_id = "+SSTR(parmId)+";";
		records.clear();
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1)
			return DB_FAIL;

		ret = poplateCommsParameters(commsSettings,records);
		return ret;
	}

	int CCommsConfig::getSecSoftwareDwlndCommsSettings(CCommsSettings &commsSettings) {
		std::string sql = "SELECT * FROM software_comms_set;";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1)
			return DB_FAIL;

		std::string val;
		int ret = conManager.getColumnValue("sec_comms_parameters_id",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;

		int parmId = atoi(val.c_str());

		ret = conManager.getColumnValue("sec_comms_type",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;

		commsSettings.setCommsType((CCommsSettings::COMMS_TYPE)atoi(val.c_str()));

		records.clear();
		sql = "SELECT * FROM software_comms_parameters WHERE comms_parameters_id = "+SSTR(parmId)+";";
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1)
			return DB_FAIL;

		ret = poplateCommsParameters(commsSettings,records);
		return ret;
	}

	int CCommsConfig::importCommsConfig(std::string path, HIM him) {
		switch (him) {
			case ALIENO:
				return processAlienoHimFiles(path);
			case ISO:
				return processIsoHimFiles(path);
			default:
				return DB_FAIL;
		}
	}

	int CCommsConfig::getVersion(std::string &version) {
		CVersions versions;

		std::map<int,CVersionsFile> files;
		int ret = versions.getFiles(VERSIONS_PARM_APP_NAME,files);
		if (ret != DB_OK)
			return DB_FAIL;
		else {
			std::map<int,CVersionsFile>::iterator it2;
			for (it2=files.begin(); it2!=files.end(); ++it2) {
				CVersionsFile file = it2->second;

				if (file.getName().compare(VERSIONS_PARM_COMMS_FILE_NAME) == 0) {
					version = file.getVersion();
					return DB_OK;
				}
			}
		}

		return DB_FAIL;
	}

	int CCommsConfig::processIsoHimFiles(std::string path) {
		bool nextByteEscaped = false;
		std::string line;
		int ret;

		std::ifstream ifs(path.append("/").append(ISO_COMMS_FILE_NAME).c_str());
		if (!ifs.is_open())
			return DB_FAIL;

		std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
		if (str.length() < 4)
			return DB_FAIL;

		std::string version = str.substr(0,4);

		str = str.substr(4,str.length()-4); // version

		BOOST_FOREACH (char& c , str) {
			if (c == '\\') {
				nextByteEscaped = true;
				continue;
			}
			if (!nextByteEscaped && c == '.') {
				std::size_t found = line.find("\n");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				found = line.find("\r");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");

				ret = processIsoHimLine(line);
				if (ret != DB_OK)
					return ret;

				line = "";
				continue;
			}

			line += c;
			nextByteEscaped = false;
		}

		CVersions versions;
		CVersionsFile file;
		file.setName(VERSIONS_PARM_COMMS_FILE_NAME);
		file.setPath(path);
		file.setVersion(version);
		versions.addFile(VERSIONS_PARM_APP_NAME,file);

		return DB_OK;
	}
#include <iostream>
//using namespace std;
using std::cout;

	int CCommsConfig::processIsoHimLine(std::string line) {
		std::string commsSet, commsSequence;
		boost::char_separator<char> sep(",", "", boost::keep_empty_tokens);
		boost::tokenizer< boost::char_separator<char> > tokens(line, sep);
		CCommsSettings settings;
		int cnt=0;

		BOOST_FOREACH (const std::string& t, tokens) {
			switch (cnt++) {
				case 0: // comms set
					commsSet = t;
					break;
				case 1: // comms type
					if (t.compare("00") == 0) {
					   // ethernet
						settings.setCommsType(CCommsSettings::ETHERNET);
					} else if (t.compare("01") == 0) {
					   // direct dial
						settings.setCommsType(CCommsSettings::DIRECTDIAL);
					} else if (t.compare("02") == 0) {
					   // quick connect
						settings.setCommsType(CCommsSettings::QUICKCONNECT);
					} else if (t.compare("03") == 0) {
					   // speedlink
						settings.setCommsType(CCommsSettings::SPEEDLINK);
					} else if (t.compare("04") == 0) {
					   // radiopad
						settings.setCommsType(CCommsSettings::RADIOPAD);
					} else if (t.compare("05") == 0) {
					   // gprs
						settings.setCommsType(CCommsSettings::GPRS);
					} else if (t.compare("06") == 0) {
					   // fnb direct
						settings.setCommsType(CCommsSettings::FNBDIRECT);
					}
					break;
				case 2: //sequence
					commsSequence = t;
					break;
				case 3: // values
					switch (settings.getCommsType()) {
						case CCommsSettings::ETHERNET:
							settings.setIp(t);
							break;
						case CCommsSettings::DIRECTDIAL:
							settings.setPhoneNumber(t);
							break;
						case CCommsSettings::QUICKCONNECT:
							settings.setPhoneNumber(t);
							break;
						case CCommsSettings::SPEEDLINK:
							settings.setPhoneNumber(t);
							break;
						case CCommsSettings::RADIOPAD:
							settings.setDte(t);
							break;
						case CCommsSettings::GPRS:
							settings.setIp(t);
							break;
						case CCommsSettings::FNBDIRECT:
							settings.setPhoneNumber(t);
							break;

					}
					break;
				case 4: //values
					switch (settings.getCommsType()) {
						case CCommsSettings::ETHERNET:
							settings.setPort(t);
#ifdef ABSA // See at top of file for description
							{
cout << "DBG 0\n";
								boost::char_separator<char> sep(";", "", boost::keep_empty_tokens);
								boost::tokenizer< boost::char_separator<char> > innerTokens(t, sep);
								int cnt2=0;

								BOOST_FOREACH (const std::string& u, innerTokens) {
									switch (cnt2++) {
										case 0: // ip
cout << "DBG 1 [" << u << "] \n";
											settings.setIp(u.substr(u.find("=")+1));
											break;
										case 1: // port
cout << "DBG 2 [" << u << "] \n";
											settings.setPort(u.substr(u.find("=")+1));
											break;
										default:
											break;
									}
								}
							}
#endif
							break;
						case CCommsSettings::DIRECTDIAL:
							break;
						case CCommsSettings::QUICKCONNECT:
							settings.setDte(t);
							break;
						case CCommsSettings::SPEEDLINK:
							settings.setDte(t);
							break;
						case CCommsSettings::RADIOPAD:
							break;
						case CCommsSettings::GPRS:
							settings.setPort(t);
							break;
						case CCommsSettings::FNBDIRECT:
							settings.setDte(t);
							break;
					}
					break;
			}
		}

		int ret;
		if (commsSet.compare("00") == 0) {
		// auth
			if (commsSequence.compare("1") == 0) {
				ret = setPriAuthCommsSettings(settings);
				ret = setSecAuthCommsSettings(settings);
			} else
				ret = setSecAuthCommsSettings(settings);

		} else if (commsSet.compare("01") == 0) {
		// settlement
			if (commsSequence.compare("1") == 0) {
			   ret = setPriSettleCommsSettings(settings);
			   ret = setSecSettleCommsSettings(settings);
			} else
			   ret = setSecSettleCommsSettings(settings);

			// for ABSA
			if (commsSequence.compare("1") == 0) {
			   ret = setPriParamCommsSettings(settings);
			   ret = setSecParamCommsSettings(settings);
			} else
			   ret = setSecParamCommsSettings(settings);

		} else if (commsSet.compare("03") == 0) {
		// software
			if (commsSequence.compare("1") == 0) {
			   ret = setPriSoftwareCommsSettings(settings);
			   ret = setSecSoftwareCommsSettings(settings);
			} else
			   ret = setSecSoftwareCommsSettings(settings);
		} else if (commsSet.compare("50") == 0) {
		// parameters
		   if (commsSequence.compare("1") == 0) {
			   ret = setPriParamCommsSettings(settings);
			   ret = setSecParamCommsSettings(settings);
		   } else
			   ret = setSecParamCommsSettings(settings);
		} else
			return DB_OK;

		return ret;
	}

	int CCommsConfig::processAlienoHimFiles(std::string path) {
		bool nextByteEscaped = false;
		std::string line;
		int ret;

		std::ifstream ifs(path.append("/").append(ALIENO_COMMS_FILE_NAME).c_str());
		if (!ifs.is_open())
			return DB_FAIL;

		std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
		str = str.substr(15,str.length()-15); // version
		BOOST_FOREACH (char& c , str) {
			if (c == '\\') {
				nextByteEscaped = true;
				continue;
			}
			if (!nextByteEscaped && c == '.') {
				std::size_t found = line.find("\n");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				found = line.find("\r");
				if (found!=std::string::npos)
					line = line.replace(found,1,"");
				ret = processAlienoHimLine(line); //strip version
				if (ret != DB_OK)
					return ret;

				line = "";
				continue;
			}

			line += c;
			nextByteEscaped = false;
		}

		return DB_OK;
	}
	int CCommsConfig::processAlienoHimLine(std::string line) {
		std::string commsSet, commsSequence;
		boost::char_separator<char> sep(",", "", boost::keep_empty_tokens);
		boost::tokenizer< boost::char_separator<char> > tokens(line, sep);
		CCommsSettings settings;
		int cnt=0;

		BOOST_FOREACH (const std::string& t, tokens) {
			switch (cnt++) {
				case 0: // comms set
					commsSet = t;
					break;
				case 1: // comms type
					if (t.compare("00") == 0) {
					   // ethernet
						settings.setCommsType(CCommsSettings::ETHERNET);
					} else if (t.compare("01") == 0) {
					   // direct dial
						settings.setCommsType(CCommsSettings::DIRECTDIAL);
					} else if (t.compare("02") == 0) {
					   // quick connect
						settings.setCommsType(CCommsSettings::QUICKCONNECT);
					} else if (t.compare("03") == 0) {
					   // speedlink
						settings.setCommsType(CCommsSettings::SPEEDLINK);
					} else if (t.compare("04") == 0) {
					   // radiopad
						settings.setCommsType(CCommsSettings::RADIOPAD);
					} else if (t.compare("05") == 0) {
					   // gprs
						settings.setCommsType(CCommsSettings::GPRS);
					} else if (t.compare("06") == 0) {
					   // fnb direct
						settings.setCommsType(CCommsSettings::FNBDIRECT);
					}
					break;
				case 2: //sequence
					commsSequence = t;
					break;
				case 3: // values
					switch (settings.getCommsType()) {
						case CCommsSettings::ETHERNET:
							settings.setIp(t);
							break;
						case CCommsSettings::DIRECTDIAL:
							settings.setPhoneNumber(t);
							break;
						case CCommsSettings::QUICKCONNECT:
							settings.setPhoneNumber(t);
							break;
						case CCommsSettings::SPEEDLINK:
							settings.setPhoneNumber(t);
							break;
						case CCommsSettings::RADIOPAD:
							settings.setDte(t);
							break;
						case CCommsSettings::GPRS:
							settings.setIp(t);
							break;
						case CCommsSettings::FNBDIRECT:
							settings.setPhoneNumber(t);
							break;

					}
					break;
				case 4: //values
					switch (settings.getCommsType()) {
						case CCommsSettings::ETHERNET:
							settings.setPort(t);
							break;
						case CCommsSettings::DIRECTDIAL:
							break;
						case CCommsSettings::QUICKCONNECT:
							settings.setDte(t);
							break;
						case CCommsSettings::SPEEDLINK:
							settings.setDte(t);
							break;
						case CCommsSettings::RADIOPAD:
							break;
						case CCommsSettings::GPRS:
							settings.setPort(t);
							break;
						case CCommsSettings::FNBDIRECT:
							settings.setDte(t);
							break;
					}
					break;
			}
		}

		int ret;
		if (commsSet.compare("00") == 0) {
		// auth
			if (commsSequence.compare("1") == 0)
				ret = setPriAuthCommsSettings(settings);
			else
				ret = setSecAuthCommsSettings(settings);
		} else if (commsSet.compare("01") == 0) {
		// settlement
			if (commsSequence.compare("1") == 0)
			   ret = setPriSettleCommsSettings(settings);
			else
			   ret = setSecSettleCommsSettings(settings);
		} else if (commsSet.compare("03") == 0) {
		// software
			if (commsSequence.compare("1") == 0)
			   ret = setPriSoftwareCommsSettings(settings);
		   else
			   ret = setSecSoftwareCommsSettings(settings);
		} else if (commsSet.compare("50") == 0) {
		// parameters
		   if (commsSequence.compare("1") == 0)
			   ret = setPriParamCommsSettings(settings);
		   else
			   ret = setSecParamCommsSettings(settings);
		} else
			return DB_FAIL;

		return ret;
	}

	int CCommsConfig::poplateCommsParameters(CCommsSettings &commsSettings, std::vector<std::vector<std::string> > &records) {
		std::string val;

		int ret = conManager.getColumnValue("ip",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			commsSettings.setIp(val);


		ret = conManager.getColumnValue("port",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			commsSettings.setPort(val);

		ret = conManager.getColumnValue("ip2",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			commsSettings.setIp2(val);

		ret = conManager.getColumnValue("port2",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			commsSettings.setPort2(val);

		ret = conManager.getColumnValue("phone_number",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			commsSettings.setPhoneNumber(val);

		ret = conManager.getColumnValue("DTE",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			commsSettings.setDte(val);


		ret = conManager.getColumnValue("NUI",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			commsSettings.setNui(val);

		return DB_OK;
	}
}
