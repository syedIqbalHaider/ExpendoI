#include <stdio.h>
#include <stdlib.h>
#include "libda/cuser.h"

namespace com_verifone_user
{
	CUser::CUser():bankingAllowed(true),pinRequired(true),refundsAllowed(true),userType(CASHIER){}

	bool CUser::isBankingAllowed()  {
		return bankingAllowed;
	}

	void CUser::setBankingAllowed(bool bankingAllowed) {
		this->bankingAllowed = bankingAllowed;
	}

	 std::string& CUser::getDateAdded()  {
		return dateAdded;
	}

	void CUser::setDateAdded( std::string dateAdded) {
		this->dateAdded = dateAdded;
	}

	 std::string& CUser::getId()  {
		return id;
	}

	void CUser::setId( std::string id) {
		this->id = id;
	}

	 std::string& CUser::getName()  {
		return name;
	}

	void CUser::setName( std::string name) {
		this->name = name;
	}

	 std::string& CUser::getPin()  {
		return pin;
	}

	void CUser::setPin( std::string pin) {
		this->pin = pin;
	}

	bool CUser::isPinRequired()  {
		return pinRequired;
	}

	void CUser::setPinRequired(bool pinRequired) {
		this->pinRequired = pinRequired;
	}

	bool CUser::isRefundsAllowed()  {
		return refundsAllowed;
	}

	void CUser::setRefundsAllowed(bool refundsAllowed) {
		this->refundsAllowed = refundsAllowed;
	}

	CUser::USER_TYPE CUser::getUserType()  {
		return userType;
	}

	void CUser::setUserType(USER_TYPE userType) {
		this->userType = userType;
	}

}
