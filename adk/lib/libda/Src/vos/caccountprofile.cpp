#include <stdio.h>
#include <stdlib.h>
#include "libda/cconmanager.h"
#include "libda/climits.h"
#include "libda/caccountprofile.h"

using namespace com_verifone_limits;
using namespace com_verifone_conmanager;


namespace com_verifone_accountprofile
{
	CConManager conManager;

	CAccountProfile::CAccountProfile():allowVoiceApproval(true),allowManualEntry(true),checkCardValue(true),requirePinEntry(true),
			skimCardValue(true),allowBudget(true),draftCaptureMode(true),requireSignature(true) {
	}

	bool CAccountProfile::isAllowBudget()  {
		return allowBudget;
	}

	void CAccountProfile::setAllowBudget(bool allowBudget) {
		this->allowBudget = allowBudget;
	}

	 std::set<std::string>& CAccountProfile::getAllowedTransactions()  {
		return allowedTransactions;
	}

	void CAccountProfile::setAllowedTransactions(
			 std::set<std::string>& allowedTransactions) {
		this->allowedTransactions = allowedTransactions;
	}

	bool CAccountProfile::isAllowManualEntry()  {
		return allowManualEntry;
	}

	void CAccountProfile::setAllowManualEntry(bool allowManualEntry) {
		this->allowManualEntry = allowManualEntry;
	}

	bool CAccountProfile::isAllowVoiceApproval()  {
		return allowVoiceApproval;
	}

	void CAccountProfile::setAllowVoiceApproval(bool allowVoiceApproval) {
		this->allowVoiceApproval = allowVoiceApproval;
	}

	 std::string& CAccountProfile::getBudgetAmountMin()  {
		return budgetAmountMin;
	}

	void CAccountProfile::setBudgetAmountMin( std::string budgetAmountMin) {
		this->budgetAmountMin = budgetAmountMin;
	}

	bool CAccountProfile::isCheckCardValue()  {
		return checkCardValue;
	}

	void CAccountProfile::setCheckCardValue(bool checkCardValue) {
		this->checkCardValue = checkCardValue;
	}

	 std::string& CAccountProfile::getCode()  {
		return code;
	}

	void CAccountProfile::setCode( std::string& code) {
		this->code = code;
	}

	 CLimits& CAccountProfile::getContactLessLimits()  {
		return contactLessLimits;
	}

	void CAccountProfile::setContactLessLimits( CLimits contactLessLimits) {
		this->contactLessLimits = contactLessLimits;
	}

	 std::string& CAccountProfile::getDescription()  {
		return description;
	}

	void CAccountProfile::setDescription( std::string description) {
		this->description = description;
	}

	bool CAccountProfile::isDraftCaptureMode()  {
		return draftCaptureMode;
	}

	void CAccountProfile::setDraftCaptureMode(bool draftCaptureMode) {
		this->draftCaptureMode = draftCaptureMode;
	}

	 CLimits& CAccountProfile::getEmvFallBackLimits()  {
		return emvFallBackLimits;
	}

	void CAccountProfile::setEmvFallBackLimits( CLimits emvFallBackLimits) {
		this->emvFallBackLimits = emvFallBackLimits;
	}

	 CLimits& CAccountProfile::getLocalLimits()  {
		return localLimits;
	}

	void CAccountProfile::setLocalLimits( CLimits localLimits) {
		this->localLimits = localLimits;
	}

	 CLimits& CAccountProfile::getOfflineLimits()  {
		return offlineLimits;
	}

	void CAccountProfile::setOfflineLimits( CLimits offlineLimits) {
		this->offlineLimits = offlineLimits;
	}

	bool CAccountProfile::isRequirePinEntry()  {
		return requirePinEntry;
	}

	void CAccountProfile::setRequirePinEntry(bool requirePinEntry) {
		this->requirePinEntry = requirePinEntry;
	}

	bool CAccountProfile::isRequireSignature()  {
		return requireSignature;
	}

	void CAccountProfile::setRequireSignature(bool requireSignature) {
		this->requireSignature = requireSignature;
	}

	bool CAccountProfile::isSkimCardValue()  {
		return skimCardValue;
	}

	void CAccountProfile::setSkimCardValue(bool skimCardValue) {
		this->skimCardValue = skimCardValue;
	}

	std::string& CAccountProfile::getSuperTotalsGroupName()  {
		return superTotalsGroupName;
	}

	void CAccountProfile::setSuperTotalsGroupName(
			 std::string superTotalsGroupName) {
		this->superTotalsGroupName = superTotalsGroupName;
	}

	std::string& CAccountProfile::getTotalsGroupName()  {
		return totalsGroupName;
	}

	void CAccountProfile::setTotalsGroupName( std::string totalsGroupName) {
		this->totalsGroupName = totalsGroupName;
	}

}
