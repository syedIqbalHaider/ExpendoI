#include <stdio.h>
#include <stdlib.h>
#include "libda/cvelocitycheck.h"
#include "libda/cconmanager.h"


using namespace com_verifone_conmanager;

namespace com_verifone_velocitycheck
{

	CConManager conManager;

	CVelocityCheck::CVelocityCheck(){}

	int CVelocityCheck::getAllPans(std::map<std::string,CPanInfo> &pans) {
		std::string sql = "SELECT * FROM velocity;";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1)
			return DB_OK; // no users

		std::string val;
		for (int i=1;i<records.size();i++) {
			CPanInfo panInfo;

			int ret = conManager.getColumnValue("pan",val,records,1);
			if (ret != DB_OK)
				return DB_FAIL;

			panInfo.setPan(val);

			ret = conManager.getColumnValue("counter",val,records,1);
			if (ret != DB_OK)
				return DB_FAIL;

			panInfo.setCounter(atoi(val.c_str()));

			ret = conManager.getColumnValue("timestamp",val,records,1);
			if (ret != DB_OK)
				return DB_FAIL;

			panInfo.setTimeStamp(atol(val.c_str()));

			ret = conManager.getColumnValue("last_used_timestamp",val,records,1);
			if (ret != DB_OK)
				return DB_FAIL;

			if (!val.empty())
				panInfo.setLastUsedTimestamp(atol(val.c_str()));

			pans[panInfo.getPan()] = panInfo;
		}

		return DB_OK;
	}

	int CVelocityCheck::checkPan(CPanInfo &panInfo) {
		std::string sql = "SELECT * FROM velocity WHERE pan = '"+SSTR(panInfo.getPan())+"' LIMIT 1;";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1)
			return DB_FAIL;

		std::string val;
		int ret = conManager.getColumnValue("counter",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;

		panInfo.setCounter(atoi(val.c_str()));

		ret = conManager.getColumnValue("timestamp",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;

		panInfo.setTimeStamp(atol(val.c_str()));

		ret = conManager.getColumnValue("last_used_timestamp",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;

		if (!val.empty())
			panInfo.setLastUsedTimestamp(atol(val.c_str()));

		return DB_OK;
	}

	int CVelocityCheck::removePan(std::string pan) {
		std::string sql = "DELETE FROM velocity WHERE pan = '"+pan+"';";
		conManager.writeToDB(sql.c_str());
		return DB_OK;
	}

	int CVelocityCheck::insertPan(CPanInfo &panInfo) {
		std::string sql = "DELETE FROM velocity WHERE pan = '"+panInfo.getPan()+"';";
		conManager.writeToDB(sql.c_str());

		int ret = conManager.insertPan(panInfo);
		if (ret != DB_OK)
			return ret;

		return DB_OK;
	}

	int CVelocityCheck::updatePan(CPanInfo &panInfo) {
		std::string sql = "DELETE FROM velocity WHERE pan = '"+panInfo.getPan()+"';";
		conManager.writeToDB(sql.c_str());

		int ret = conManager.insertPan(panInfo);
		if (ret != DB_OK)
			return ret;

		return DB_OK;
	}

}
