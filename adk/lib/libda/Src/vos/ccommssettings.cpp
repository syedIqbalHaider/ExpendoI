#include <stdio.h>
#include <stdlib.h>
#include "libda/ccommssettings.h"


namespace com_verifone_commssettings
{
	CCommsSettings::CCommsSettings():useSSL(false),commsType(CCommsSettings::ETHERNET){}

	std::string& CCommsSettings::getIp()  {
		return ip;
	}

	void CCommsSettings::setIp( std::string ip) {
		this->ip = ip;
	}

	std::string& CCommsSettings::getPort()  {
		return port;
	}

	void CCommsSettings::setPort( std::string port) {
		this->port = port;
	}

	std::string& CCommsSettings::getIp2()  {
		return ip2;
	}

	void CCommsSettings::setIp2( std::string ip2) {
		this->ip2 = ip2;
	}

	std::string& CCommsSettings::getPort2()  {
		return port2;
	}

	void CCommsSettings::setPort2( std::string port2) {
		this->port2 = port2;
	}

	bool CCommsSettings::isUseSsl()  {
		return useSSL;
	}

	void CCommsSettings::setUseSsl(bool useSsl) {
		useSSL = useSsl;
	}

	std::string& CCommsSettings::getPhoneNumber() {
		return phoneNumber;
	}
	void CCommsSettings::setPhoneNumber( std::string phoneNumber) {
		this->phoneNumber = phoneNumber;
	}

	std::string& CCommsSettings::getDte() {
		return dte;
	}
	void CCommsSettings::setDte( std::string dte){
		this->dte = dte;
	}
	std::string& CCommsSettings::getNui() {
		return nui;
	}
	void CCommsSettings::setNui( std::string nui) {
		this->nui = nui;
	}

	CCommsSettings::COMMS_TYPE CCommsSettings::getCommsType()  {
		return commsType;
	}

	void CCommsSettings::setCommsType(COMMS_TYPE commsType) {
		this->commsType = commsType;
	}

}

