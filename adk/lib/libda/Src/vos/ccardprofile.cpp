#include <stdio.h>
#include <stdlib.h>
#include "libda/cconmanager.h"
#include "libda/ccardprofile.h"

using namespace com_verifone_conmanager;


namespace com_verifone_cardprofile
{
	CConManager conManager;

	CCardProfile::CCardProfile():checkExpiryDate(true),checkLuhn(true),pinLenMax(6),pinLenMin(4),foreignCard(false),active(true),
			checkServiceCode(true),index(0){}

	bool CCardProfile::isActive()  {
		return active;
	}

	void CCardProfile::setActive(bool active) {
		this->active = active;
	}

	bool CCardProfile::isCheckExpiryDate()  {
		return checkExpiryDate;
	}

	void CCardProfile::setCheckExpiryDate(bool checkExpiryDate) {
		this->checkExpiryDate = checkExpiryDate;
	}

	bool CCardProfile::isCheckLuhn()  {
		return checkLuhn;
	}

	void CCardProfile::setCheckLuhn(bool checkLuhn) {
		this->checkLuhn = checkLuhn;
	}

	bool CCardProfile::isCheckServiceCode()  {
		return checkServiceCode;
	}

	void CCardProfile::setCheckServiceCode(bool checkServiceCode) {
		this->checkServiceCode = checkServiceCode;
	}

	bool CCardProfile::isForeignCard()  {
		return foreignCard;
	}

	void CCardProfile::setForeignCard(bool foreignCard) {
		this->foreignCard = foreignCard;
	}

	int CCardProfile::getPinLenMax()  {
		return pinLenMax;
	}

	void CCardProfile::setPinLenMax(int pinLenMax) {
		this->pinLenMax = pinLenMax;
	}

	int CCardProfile::getPinLenMin()  {
		return pinLenMin;
	}

	void CCardProfile::setPinLenMin(int pinLenMin) {
		this->pinLenMin = pinLenMin;
	}

	 std::string& CCardProfile::getVoiceApprovalText()  {
		return voiceApprovalText;
	}

	void CCardProfile::setVoiceApprovalText( std::string voiceApprovalText) {
		this->voiceApprovalText = voiceApprovalText;
	}

	int CCardProfile::getIndex() {
		return index;
	}

	void CCardProfile::setIndex(int index) {
		this->index = index;
	}
}
