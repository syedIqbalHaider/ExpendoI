#include <stdio.h>
#include <stdlib.h>
#include "libda/cconmanager.h"
#include "libda/cemvdata.h"

using namespace com_verifone_conmanager;

namespace com_verifone_emvdata
{
	CConManager conManager;

	CEmvData::CEmvData(){}

	CAccountProfile& CEmvData::getAccountProfile()
	{
		return accountProfile;
	}

	void CEmvData::setAccountProfile( CAccountProfile& accountProfile)
	{
		this->accountProfile = accountProfile;
	}

	std::string& CEmvData::getAid()
	{
		return aid;
	}

	void CEmvData::setAid( std::string& aid)
	{
		this->aid = aid;
	}

	std::string& CEmvData::getApplicationVersion()
	{
		return applicationVersion;
	}

	void CEmvData::setApplicationVersion( std::string applicationVersion)
	{
		this->applicationVersion = applicationVersion;
	}

	std::string& CEmvData::getDefaultDdol()
	{
		return defaultDdol;
	}

	void CEmvData::setDefaultDdol( std::string defaultDdol)
	{
		this->defaultDdol = defaultDdol;
	}

	std::string& CEmvData::getDefaultTdol()
	{
		return defaultTdol;
	}

	void CEmvData::setDefaultTdol( std::string defaultTdol)
	{
		this->defaultTdol = defaultTdol;
	}

	std::string& CEmvData::getEmvSchemeCode()
	{
		return emvSchemeCode;
	}

	void CEmvData::setEmvSchemeCode( std::string emvSchemeCode)
	{
		this->emvSchemeCode = emvSchemeCode;
	}

	std::map<std::string, CPublicKey>& CEmvData::getPublicKeys() {
		return publicKeys;
	}

	void CEmvData::setPublicKeys(std::map<std::string, CPublicKey> publicKeys) {
		this->publicKeys = publicKeys;
	}

	std::string& CEmvData::getTacDecline()
	{
		return tacDecline;
	}

	void CEmvData::setTacDecline( std::string tacDecline)
	{
		this->tacDecline = tacDecline;
	}

	std::string& CEmvData::getTacDefault()
	{
		return tacDefault;
	}

	void CEmvData::setTacDefault( std::string tacDefault)
	{
		this->tacDefault = tacDefault;
	}

	 std::string& CEmvData::getTacOnline()
	{
		return tacOnline;
	}

	void CEmvData::setTacOnline( std::string tacOnline)
	{
		this->tacOnline = tacOnline;
	}

	std::string& CEmvData::getTargetPercentage()
	{
		return targetPercentage;
	}

	void CEmvData::setTargetPercentage( std::string targetPercentage)
	{
		this->targetPercentage = targetPercentage;
	}

	std::string& CEmvData::getTargetPercentageMax()
	{
		return targetPercentageMax;
	}

	void CEmvData::setTargetPercentageMax( std::string targetPercentageMax)
	{
		this->targetPercentageMax = targetPercentageMax;
	}

	std::string& CEmvData::getThresholdAmt()
	{
		return thresholdAmt;
	}

	void CEmvData::setThresholdAmt( std::string thresholdAmt)
	{
		this->thresholdAmt = thresholdAmt;
	}

	std::string& CEmvData::getTotalsGroupName()
	{
		return totalsGroupName;
	}

	void CEmvData::setTotalsGroupName( std::string totalsGroupName)
	{
		this->totalsGroupName = totalsGroupName;
	}


	std::string& CEmvData::getSuperTotalsGroupName()
	{
		return superTotalsGroupName;
	}

	void CEmvData::setSuperTotalsGroupName( std::string superTotalsGroupName)
	{
		this->superTotalsGroupName = superTotalsGroupName;
	}


	std::string& CEmvData::getCardDataInputMode()  {
		return cardDataInputMode;
	}

	void CEmvData::setCardDataInputMode( std::string cardDataInputMode) {
		this->cardDataInputMode = cardDataInputMode;
	}

	std::string CEmvData::getLatestApplicationVersion()
	{
		return latestApplicationVersion;
	}
	void CEmvData::setLatestApplicationVersion( std::string latestApplicationVersion)
	{
		this->latestApplicationVersion = latestApplicationVersion;
	}

	bool CEmvData::getPartialMatchingAllowed()
	{
		return partialMatchingAllowed;
	}

	void CEmvData::setPartialMatchingAllowed(bool partialMatchingAllowed)
	{
		this->partialMatchingAllowed = partialMatchingAllowed;
	}
}
