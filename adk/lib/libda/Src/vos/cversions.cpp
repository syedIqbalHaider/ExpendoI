#include <stdio.h>
#include <stdlib.h>
#include "libda/cconmanager.h"
#include "libda/cversions.h"

using namespace com_verifone_conmanager;


namespace com_verifone_versions
{
	CConManager conManager;

	CVersions::CVersions() {}

	int CVersions::getApps(std::set<std::string> &apps) {
		std::vector<std::vector<std::string> > records;
		std::string sql = "SELECT DISTINCT app_name FROM versions;";
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size() <= 1)
			return DB_FAIL;

		for (int i=1; i<records.size(); i++) {
			std::string appName;
			std::string val;
			int ret = conManager.getColumnValue("app_name",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				apps.insert(val);
		}

		return DB_OK;
	}

	int CVersions::getFiles(std::string appName, std::map<int,CVersionsFile> &files) {
		std::vector<std::vector<std::string> > records;
		std::string sql = "SELECT * FROM versions WHERE app_name = '"+appName+"';";
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size() <= 1)
			return DB_FAIL;

		for (int i=1; i<records.size(); i++) {
			CVersionsFile file;
			std::string appName;
			std::string val;
			int ret = conManager.getColumnValue("file_name",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				file.setName(val);

			ret = conManager.getColumnValue("path",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				file.setPath(val);

			ret = conManager.getColumnValue("version",val,records,i);
			if (ret != DB_OK)
				return DB_FAIL;
			else
				file.setVersion(val);

			files[i] = file;
		}

		return DB_OK;
	}

	int CVersions::deleteFile(std::string appName,std::string fileName) {
			std::string sql = "DELETE FROM versions WHERE app_name = '"+appName+"' AND file_name = '"+fileName+"';";
			conManager.writeToDB(sql);
			return DB_OK;
	}

	int CVersions::deleteApp(std::string appName) {
		std::string sql = "DELETE FROM versions WHERE app_name = '"+appName+"';";
		conManager.writeToDB(sql);
		return DB_OK;
	}

	int CVersions::addFile(std::string appName, CVersionsFile file) {
		deleteFile(appName,file.getName());
		return conManager.insertFileWithVersion(appName,file);
	}


}
