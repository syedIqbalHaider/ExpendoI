#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <errno.h>
#include <string>     // std::string, std::stoi
#include "libda/cbatchmanager.h"
#include "libda/cconmanager.h"
#include "libda/himdef.h"
#include <libda/cbatch.h>
#include <libda/cterminalconfig.h>


using namespace com_verifone_batch;
using namespace com_verifone_conmanager;
using namespace com_verifone_terminalconfig;

namespace com_verifone_batchmanager
{
	CConManager conManager;

	CBatchManager::CBatchManager() {}

	int CBatchManager::closeBatch() {
		CBatch batch;

		int ret = getPreviousBatch(batch);
		if (ret == DB_OK) {

			// delete previous batch

			std::string sql = "DELETE FROM batch WHERE batch_id = "+SSTR(batch.getBatchDbId())+";";
			ret = conManager.writeToDB(sql);
			if (ret == DB_FAIL)
				return ret;

			sql = "DELETE FROM batch_rec WHERE batch_no = "+SSTR(batch.getBatchDbId())+";";
			ret = conManager.writeToDB(sql);
			if (ret == DB_FAIL)
				return ret;
		}

		ret = getBatch(batch);
		if (ret != DB_OK)
			return ret;

		// update current batch to closed
		batch.setDateTimeClosed(getCurrentDateTime());
		batch.setState(CLOSED);

		std::string sql = "DELETE FROM batch WHERE batch_id = "+SSTR(batch.getBatchDbId())+";";
		ret = conManager.writeToDB(sql);
		if (ret == DB_FAIL)
			return ret;

		int prevBatchDbId = batch.getBatchDbId();

		ret = conManager.insertBatch(batch);
		if (ret != DB_OK)
			return ret;


		// open new batch
		getBatch(batch);

		updateBatchRec(prevBatchDbId, batch.getBatchDbId());

		return DB_OK;
	}

	int CBatchManager::getNoRecordsInBatch() {
		std::string sql = "SELECT COUNT(*) AS CNT FROM batch_rec WHERE batch_no= (SELECT batch_id FROM batch WHERE state = 0);";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1) {
			return 0;
		}

		std::string val;
		int ret = conManager.getColumnValue("CNT",val,records,1);
		if (ret != DB_OK)
			return 0;
		else
			return atoi(val.c_str());
	}

	int CBatchManager::getNoRecordsInPreviousBatch() {
		std::string sql = "SELECT COUNT(*) AS CNT FROM batch_rec WHERE batch_no= (SELECT batch_id FROM batch WHERE state = 1);";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1) {
			return 0;
		}

		std::string val;
		int ret = conManager.getColumnValue("CNT",val,records,1);
		if (ret != DB_OK)
			return 0;
		else
			return atoi(val.c_str());
	}

	int CBatchManager::getBatch(CBatch &batch) {
		std::string sql = "SELECT * FROM batch WHERE batch_id = (select MAX(batch_id) from batch);";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1) {
			// open first batch
			batch.setBatchNo(1);
			batch.setState(OPEN);
			batch.setDateTimeOpened(getCurrentDateTime());
			batch.setDateTimeClosed("");
			conManager.insertBatch(batch);
			return DB_OK;
		}

		std::string val;
		int ret = conManager.getColumnValue("state",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batch.setState((BATCH_STATE)atoi(val.c_str()));

		ret = conManager.getColumnValue("batch_no",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batch.setBatchNo(atoi(val.c_str()));

		ret = conManager.getColumnValue("batch_id",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batch.setBatchDbId(atoi(val.c_str()));

		// check if there is an open batch
		if (batch.getState() == CLOSED) {
			CTerminalConfig tconf;
			if (batch.getBatchNo()+1 > 999) {
				batch.setBatchNo(1);
			} else {
				batch.setBatchNo(batch.getBatchNo()+1); //increment the batch
			}
			batch.setState(OPEN);
			batch.setDateTimeOpened(getCurrentDateTime());
			batch.setDateTimeClosed("");
			conManager.insertBatch(batch);
			return DB_OK;
		}

		ret = conManager.getColumnValue("datetime_opened",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batch.setDateTimeOpened(val.c_str());

		ret = conManager.getColumnValue("datetime_closed",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batch.setDateTimeClosed(val.c_str());

		return DB_OK;
	}

	int CBatchManager::getPreviousBatch(CBatch &batch) {
		std::string sql = "SELECT * FROM batch WHERE state = 1;";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(sql.c_str(),records);
		if (records.size()<=1) {
			return DB_FAIL;
		}

		std::string val;
		int ret = conManager.getColumnValue("state",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batch.setState((BATCH_STATE)atoi(val.c_str()));

		ret = conManager.getColumnValue("batch_no",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batch.setBatchNo(atoi(val.c_str()));

		ret = conManager.getColumnValue("batch_id",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batch.setBatchDbId(atoi(val.c_str()));

		ret = conManager.getColumnValue("datetime_opened",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batch.setDateTimeOpened(val.c_str());

		ret = conManager.getColumnValue("datetime_closed",val,records,1);
		if (ret != DB_OK)
			return DB_FAIL;
		else
			batch.setDateTimeClosed(val.c_str());

		return DB_OK;
	}

	int CBatchManager::resetBatchNo(int batchNo) {
		CBatch batch;
		int ret = getBatch(batch);
		if (ret != DB_OK)
			return ret;

		std::string sql = "UPDATE batch SET batch_no = "+SSTR(batchNo)+" WHERE batch_id = "+SSTR(batch.getBatchDbId())+";";
		ret = conManager.writeToDB(sql);
		if (ret == DB_FAIL)
			return ret;


		return DB_OK;
	}

	int CBatchManager::updateBatchRec(int prevDbBatchId, int newDbBatchId) {
		CBatch batch;
		int ret = getBatch(batch);
		if (ret != DB_OK)
			return ret;

		std::string sql = "UPDATE batch_rec SET batch_no = "+SSTR(newDbBatchId)+" WHERE batch_no = "+SSTR(prevDbBatchId)+";";
		ret = conManager.writeToDB(sql);
		if (ret == DB_FAIL)
			return ret;


		return DB_OK;
	}

	std::string CBatchManager::getCurrentDateTime() {
		time_t     now = time(0);
		struct tm  tstruct;
		char       buf[80];
		tstruct = *localtime(&now);
		strftime(buf, sizeof(buf), "%Y-%m-%d %X", &tstruct);

		return buf;
	}


}
