#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <boost/foreach.hpp>
#include <boost/tokenizer.hpp>
#include "libda/cterminalconfig.h"
#include "libda/cconmanager.h"
#include "libda/cversions.h"
#include "libda/cversionsfile.h"
#include "libda/cbatchmanager.h"

using namespace com_verifone_conmanager;
using namespace com_verifone_versions;
using namespace com_verifone_versionsfile;
using namespace com_verifone_batchmanager;

namespace com_verifone_terminalconfig
{
	CConManager conManager;

	CTerminalConfig::CTerminalConfig(){}

	int CTerminalConfig::importTerminalConfig(std::string path,HIM him) {
		switch (him) {
			case ALIENO:
				return processAlienoHimTerminalFiles(path);
			case ISO:
				return processIsoHimTerminalFiles(path);
			default:
				return DB_FAIL;
		}
		return DB_OK;
	}

	int CTerminalConfig::processAlienoHimTerminalFiles(std::string path) {
		std::string sql = "DELETE FROM terminal_config;";
		conManager.writeToDB(sql);
		sql = "DELETE FROM sqlite_sequence WHERE name='terminal_config'";
		conManager.writeToDB(sql);
		int ret = processAlienoTerminalFile(TERM_PARM_PATH);
		if (ret != DB_OK) {
			conManager.close();
			return ret;
		}

		conManager.close();

		return DB_OK;
	}

	int CTerminalConfig::getVersion(std::string &version) {
		CVersions versions;

		std::map<int,CVersionsFile> files;
		int ret = versions.getFiles(VERSIONS_PARM_APP_NAME,files);
		if (ret != DB_OK)
			return DB_FAIL;
		else {
			std::map<int,CVersionsFile>::iterator it2;
			for (it2=files.begin(); it2!=files.end(); ++it2) {
				CVersionsFile file = it2->second;

				if (file.getName().compare(VERSIONS_PARM_TERM_FILE_NAME) == 0) {
					version = file.getVersion();
					return DB_OK;
				}
			}
		}

		return DB_FAIL;
	}

	int CTerminalConfig::processIsoHimTerminalFiles(std::string path) {
		/*std::string sql = "DELETE FROM terminal_config;";
		conManager.writeToDB(sql);
		sql = "DELETE FROM sqlite_sequence WHERE name='terminal_config'";
		conManager.writeToDB(sql);*/

		int ret = processIsoTerminalFile(TERM_PARM_PATH);
		if (ret != DB_OK) {
			conManager.close();
			return ret;
		}

		processMessageToMerchant(TERM_PARM_PATH);
		processApplicationBitMap(TERM_PARM_PATH);
		processCustomParameters(TERM_PARM_PATH);

		conManager.close();

		return DB_OK;
	}

	int CTerminalConfig::processAlienoTerminalFile(std::string path) {
		bool nextByteEscaped = false;
		std::string line;
		int ret;

		std::ifstream ifs(path.append("/").append(ALIENO_TERMINAL_FILE_NAME).c_str());
		if (!ifs.is_open()) {
			return DB_FAIL;
		}

		std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
		str = str.substr(14,str.length()-14); // version
		BOOST_FOREACH (char& c , str) {
			if (!nextByteEscaped) {
				if (c == '\\') {
					nextByteEscaped = true;
					continue;
				} else if (c == '.') {
					ret = processIsoTerminalLine(line);
					if (ret != DB_OK)
						return ret;

					line = "";
					continue;
				}
			}

			line += c;
			nextByteEscaped = false;
		}

		return DB_OK;
	}

	int CTerminalConfig::processCustomParameters(std::string path) {
		bool nextByteEscaped = false;
		std::string line;
		int ret;

		std::ifstream ifs(path.append("/").append(ISO_CUSTOM_PARAMETERS_FILE_NAME).c_str());
		if (!ifs.is_open())
			return DB_OK; //file not mandatory

		std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
		str = str.substr(4,str.length()-4); // version
		if (str.length() < 4) {
			return DB_OK;
		}
		setTerminalConfigValue("custom_parameters",str);
		return DB_OK;
	}

	int CTerminalConfig::processApplicationBitMap(std::string path) {
		bool nextByteEscaped = false;
		std::string line;
		int ret;

		std::ifstream ifs(path.append("/").append(ISO_APPLICATION_BITMAP_FILE_NAME).c_str());
		if (!ifs.is_open())
			return DB_OK; //file not mandatory

		std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
		str = str.substr(4,str.length()-4); // version
		if (str.length() < 4) {
			return DB_OK;
		}

		BOOST_FOREACH (char& c , str) {
			if (!nextByteEscaped) {
				if (c == '\\') {
					nextByteEscaped = true;
					continue;
				} else if (c == '.') {
					line = line.substr(2,line.length()-2); //R,
					setTerminalConfigValue("menu_bitmap",line);
					return DB_OK;
				}
			}

			line += c;
			nextByteEscaped = false;
		}

		return DB_OK;
	}

	int CTerminalConfig::processMessageToMerchant(std::string path) {
		bool nextByteEscaped = false;
		std::string line;
		int ret;

		std::ifstream ifs(path.append("/").append(ISO_MESSAGE_TO_MERCHANT_FILE_NAME).c_str());
		if (!ifs.is_open()) {
			return DB_OK;
		}

		std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
		if (str.length() < 4)
			return DB_OK;
		str = str.substr(4,str.length()-4); // version
		BOOST_FOREACH (char& c , str) {
			if (!nextByteEscaped) {
				if (c == '\\') {
					nextByteEscaped = true;
					continue;
				} else if (c == '.') {
					setTerminalConfigValue("message_to_merchant",line);
					return DB_OK;
				}
			}

			line += c;
			nextByteEscaped = false;
		}

		return DB_OK;
	}

	int CTerminalConfig::processIsoTerminalFile(std::string path) {
		bool nextByteEscaped = false;
		std::string line;
		int ret;

		std::ifstream ifs(path.append("/").append(ISO_TERMINAL_FILE_NAME).c_str());
		if (!ifs.is_open()) {
			return DB_FAIL;
		}

		std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
		if (str.length() < 4)
			return DB_OK;

		std::string version = str.substr(0,4);

		str = str.substr(4,str.length()-4); // version

		BOOST_FOREACH (char& c , str) {
			if (!nextByteEscaped) {
				if (c == '\\') {
					nextByteEscaped = true;
					continue;
				} else if (c == '.') {
					ret = processIsoTerminalLine(line);
					if (ret != DB_OK) {
						return ret;
					}

					line = "";
					continue;
				}
			}

			line += c;
			nextByteEscaped = false;
		}

		CVersions versions;
		CVersionsFile file;
		file.setName(VERSIONS_PARM_TERM_FILE_NAME);
		file.setPath(path);
		file.setVersion(version);
		versions.addFile(VERSIONS_PARM_APP_NAME,file);

		return DB_OK;
	}

	int CTerminalConfig::processIsoTerminalLine(std::string line) {
		std::string name,value;
		boost::char_separator<char> sep(",", "", boost::keep_empty_tokens);
		boost::tokenizer< boost::char_separator<char> > tokens(line, sep);
		CCommsSettings settings;
		int cnt=0;

		BOOST_FOREACH (const std::string& t, tokens) {
			switch (cnt++) {
				case 0:
					name = t;
					break;
				case 1:
					value = t;
					break;
				default:
					break;
			}
		}

		std::string convertedName;
		switch (atoi(name.c_str())) {
			case 0:
				convertedName = "term_seq_number";
				break;
			case 1:
				{
					CBatchManager batchManager;
					CBatch batch;
					batchManager.getBatch(batch);
					if (batch.getBatchNo() == '1') {
						batchManager.resetBatchNo(atoi(value.c_str())+1);
					}
				}
				break;
			case 2:
				convertedName = "nui";
				break;
			case 3:
				convertedName = "merchant_name";
				break;
			case 4:
				convertedName = "business_code";
				break;
			case 5:
				convertedName = "param_dnld_time";
				break;
			case 6:
				convertedName = "settlement_time";
				break;
			case 7:
				convertedName = "max_settle_con_attempts";
				break;
			case 8:
				convertedName = "wait_time";
				break;
			case 10:
				convertedName = "emv_capability_bitmap";
				break;
			case 11:
				convertedName = "terminal_no";
				break;
			case 12:
				convertedName = "currency_code";
				break;
			case 14:
				convertedName = "velocity_cnt";
				break;
			case 15:
				convertedName = "currency_symbol";
				break;
			case 16:
				convertedName = "batch_max";
				break;
			case 17:
				convertedName = "velocity_per";
				break;
			case 18:
				convertedName = "trailer_text";
				break;
			case 19:
				// TODO set terminal date time
				break;
			case 20:
				convertedName = "terminal_fuction_bitmap";
				break;
			case 25:
				convertedName = "max_parm_con_attempts";
				break;
			case 26:
				convertedName = "wait_time";
				break;
			case 40:
				convertedName = "menu_bitmap";
				break;
			case 56:
				convertedName = "oil_max";
				break;
			default:
				convertedName = "";
				break;
		}

		if (convertedName.length() > 0) {
			setTerminalConfigValue(convertedName,value);
		}

		return DB_OK;
	}

	int CTerminalConfig::processAlienoTerminalLine(std::string line) {
		std::string name,value;
		boost::char_separator<char> sep(",", "", boost::keep_empty_tokens);
		boost::tokenizer< boost::char_separator<char> > tokens(line, sep);
		CCommsSettings settings;
		int cnt=0;

		BOOST_FOREACH (const std::string& t, tokens) {
			switch (cnt++) {
				case 0:
					name = t;
					break;
				case 1:
					value = t;
					break;
				default:
					break;
			}
		}

		std::string convertedName;
		switch (atoi(name.c_str())) {
			case 0:
				convertedName = "term_seq_number";
				break;
			case 1:
				convertedName = "last_batch_no";
				break;
			case 3:
				convertedName = "merchant_name";
				break;
			case 4:
				convertedName = "business_code";
				break;
			case 5:
				convertedName = "param_dnld_time";
				break;
			case 6:
				convertedName = "settlement_time";
				break;
			case 7:
				convertedName = "max_settle_con_attempts";
				break;
			case 8:
				convertedName = "wait_time";
				break;
			case 10:
				convertedName = "emv_capability_bitmap";
				break;
			case 11:
				convertedName = "terminal_no";
				break;
			case 12:
				convertedName = "currency_code";
				break;
			case 14:
				convertedName = "velocity_cnt";
				break;
			case 15:
				convertedName = "currency_symbol";
				break;
			case 16:
				convertedName = "batch_max";
				break;
			case 17:
				convertedName = "velocity_per";
				break;
			case 18:
				convertedName = "trailer_text";
				break;
			case 19:
				// set terminal date time
				break;
			case 20:
				convertedName = "terminal_fuction_bitmap";
				break;
			case 25:
				convertedName = "max_parm_con_attempts";
				break;
			case 26:
				convertedName = "wait_time";
				break;
			case 40:
				convertedName = "menu_bitmap";
				break;
			case 56:
				convertedName = "oil_max";
				break;
			default:
				convertedName = "";
				break;
		}

		if (convertedName.length() > 0)
			return conManager.insertTerminalConfig(convertedName,value);
		else
			return DB_OK;
	}

	bool CTerminalConfig::isAllowChequeVerification()  {
		std::string value;
		int ret = getTerminalConfigValue("chq_verif_allowed",value);
		if (ret == DB_OK) {
			if (value.compare("1") == 0)
				return true;
			else
				return false;
		} else
			return false; //default value if not exist
	}

	int CTerminalConfig::getTerminalSeqNumber() {
		std::string value;
		int ret = getTerminalConfigValue("term_seq_number",value);
		if (ret == DB_OK) {
			return atoi(value.c_str());
		} else
			return -1;
	}

	void CTerminalConfig::setTerminalSeqNumber(int seqNumber) {
		setTerminalConfigValue("term_seq_number",SSTR(seqNumber));
	}

	void CTerminalConfig::setAllowChequeVerification(bool allowChequeVerification) {
		if (allowChequeVerification)
			setTerminalConfigValue("chq_verif_allowed","1");
		else
			setTerminalConfigValue("chq_verif_allowed","0");
	}

	std::string CTerminalConfig::getBusinessCode() {
		std::string value;
		int ret = getTerminalConfigValue("business_code",value);
		if (ret == DB_OK) {
			return value;
		} else
			return "";
	}

	std::string CTerminalConfig::getTerminalMenuBitMap(){
		std::string value;
		int ret = getTerminalConfigValue("menu_bitmap",value);
		if (ret == DB_OK) {
			return value;
		} else
			return "";
	}

	std::string CTerminalConfig::getReverselTsn() {
		std::string value;
		int ret = getTerminalConfigValue("reversal_tsn",value);
		if (ret == DB_OK) {
			return value;
		} else
			return "";
	}

	void CTerminalConfig::setReverselTsn(int tsn) {
		setTerminalConfigValue("reversal_tsn",SSTR(tsn));
	}

	void CTerminalConfig::setTerminalMenuBitMap(std::string terminalMenuBitMap){
		setTerminalConfigValue("menu_bitmap",terminalMenuBitMap);
	}

	std::string CTerminalConfig::getTerminalFunctionBitMap() {
		std::string value;
		int ret = getTerminalConfigValue("terminal_fuction_bitmap",value);
		if (ret == DB_OK) {
			return value;
		} else
			return "";
	}
	void CTerminalConfig::setTerminalFunctionBitMap(std::string terminalFunctionBitMap) {
		setTerminalConfigValue("terminal_fuction_bitmap",terminalFunctionBitMap);
	}

	std::string CTerminalConfig::getCustomParameters(){
		std::string value;
		int ret = getTerminalConfigValue("custom_parameters",value);
		if (ret == DB_OK) {
			return value;
		} else
			return "";
	}
	void CTerminalConfig::setCustomParameters(std::string customParameters) {
		setTerminalConfigValue("custom_parameters",customParameters);
	}

	std::string CTerminalConfig::getEmvCapabilityBitMap(){
		std::string value;
		int ret = getTerminalConfigValue("emv_capability_bitmap",value);
		if (ret == DB_OK) {
			return value;
		} else
			return "";

	}
	void CTerminalConfig::setEmvCapabilityBitMap(std::string emvCapabilityBitMap){
		setTerminalConfigValue("emv_capability_bitmap",emvCapabilityBitMap);
	}

	void CTerminalConfig::setBusinessCode(std::string businessCode) {
		setTerminalConfigValue("business_code",businessCode);
	}

	void CTerminalConfig::setNUI(std::string nui) {
			setTerminalConfigValue("nui",nui);
	}

	std::string CTerminalConfig::getNUI() {
		std::string value;
		int ret = getTerminalConfigValue("nui",value);
		if (ret == DB_OK) {
			return value;
		} else
			return "";
	}

	void CTerminalConfig::setMessageToMerchant(std::string message) {
		setTerminalConfigValue("message_to_merchant",message);
	}
	std::string CTerminalConfig::getMessageToMerchant() {
		std::string value;
		int ret = getTerminalConfigValue("message_to_merchant",value);
		if (ret == DB_OK) {
			return value;
		} else
			return "";
	}

	bool CTerminalConfig::isAllowManualBanking()  {
		std::string value;
		int ret = getTerminalConfigValue("manual_bank_allowed",value);
		if (ret == DB_OK) {
			if (value.compare("1") == 0)
				return true;
			else
				return false;
		} else
			return true; //default value if not exist
	}

	void CTerminalConfig::setAllowManualBanking(bool allowManualBanking) {
		if (allowManualBanking)
			setTerminalConfigValue("manual_bank_allowed","1");
		else
			setTerminalConfigValue("manual_bank_allowed","0");
	}

	bool CTerminalConfig::isAllowmanualPan()  {
		std::string value;
		int ret = getTerminalConfigValue("manual_pan_allowed",value);
		if (ret == DB_OK) {
			if (value.compare("1") == 0)
				return true;
			else
				return false;
		} else
			return true; //default value if not exist
	}

	void CTerminalConfig::setAllowmanualPan(bool allowmanualPan) {
		if (allowmanualPan)
			setTerminalConfigValue("manual_pan_allowed","1");
		else
			setTerminalConfigValue("manual_pan_allowed","0");
	}

	bool CTerminalConfig::isAttendantsEnabled()  {
		std::string value;
		int ret = getTerminalConfigValue("attendants_enabled",value);
		if (ret == DB_OK) {
			if (value.compare("1") == 0)
				return true;
			else
				return false;
		} else
			return false; //default value if not exist
	}

	void CTerminalConfig::setAttendantsEnabled(bool attendantsEnabled) {
		if (attendantsEnabled)
			setTerminalConfigValue("attendants_enabled","1");
		else
			setTerminalConfigValue("attendants_enabled","0");
	}

	int CTerminalConfig::getBatchMax()  {
		std::string value;
		int ret = getTerminalConfigValue("batch_max",value);
		if (ret == DB_OK) {
			return atoi(value.c_str());
		} else
			return 9999; //default value if not exist
	}

	void CTerminalConfig::setBatchMax(int batchMax) {
		setTerminalConfigValue("batch_max",SSTR(batchMax));
	}

	bool CTerminalConfig::isBlindDial()  {
		std::string value;
		int ret = getTerminalConfigValue("blind_dial_enabled",value);
		if (ret == DB_OK) {
			if (value.compare("1") == 0)
				return true;
			else
				return false;
		} else
			return false; //default value if not exist
	}

	void CTerminalConfig::setBlindDial(bool blindDial) {
		if (blindDial)
			setTerminalConfigValue("blind_dial_enabled","1");
		else
			setTerminalConfigValue("blind_dial_enabled","0");
	}

	int CTerminalConfig::getConnectTimeout()  {
		std::string value;
		int ret = getTerminalConfigValue("connection_timeout",value);
		if (ret == DB_OK) {
			return atoi(value.c_str());
		} else
			return 30; //default value if not exist
	}

	void CTerminalConfig::setConnectTimeout(int connectTimeout) {
		setTerminalConfigValue("connection_timeout",SSTR(connectTimeout));
	}

	std::string CTerminalConfig::getCurrencyCode()  {
		std::string value;
		int ret = getTerminalConfigValue("currency_code",value);
		if (ret == DB_OK) {
			return value;
		} else
			return "840"; //default value if not exist
	}

	void CTerminalConfig::setCurrencyCode( std::string currencyCode) {
		setTerminalConfigValue("currency_code",currencyCode);
	}

	std::string CTerminalConfig::getCurrencySymbol()  {
		std::string value;
		int ret = getTerminalConfigValue("currency_symbol",value);
		if (ret == DB_OK) {
			return value;
		} else
			return "R"; //default value if not exist
	}

	void CTerminalConfig::setCurrencySymbol( std::string currencySymbol) {
		setTerminalConfigValue("currency_symbol",currencySymbol);
	}

	std::string CTerminalConfig::getDailType()  {
		std::string value;
		int ret = getTerminalConfigValue("dial_type",value);
		if (ret == DB_OK) {
			return value;
		} else
			return "TONE"; //default value if not exist
	}

	void CTerminalConfig::setDailType( std::string dailType) {
		setTerminalConfigValue("dial_type",dailType);
	}

	std::string CTerminalConfig::getLogicalTermId()  {
		std::string value;
		int ret = getTerminalConfigValue("logical_terminal_id",value);
		if (ret == DB_OK) {
			return value;
		} else
			return ""; //default value if not exist
	}

	void CTerminalConfig::setLogicalTermId( std::string logicalTermId) {
		setTerminalConfigValue("logical_terminal_id",logicalTermId);
	}

	int CTerminalConfig::getMaxSettleConnectionAttempts()  {
		std::string value;
		int ret = getTerminalConfigValue("max_settle_con_attempts",value);
		if (ret == DB_OK) {
			return atoi(value.c_str());
		} else
			return 3; //default value if not exist
	}

	void CTerminalConfig::setMaxSettleConnectionAttempts(
			int maxSettleConnectionAttempts) {
		setTerminalConfigValue("max_settle_con_attempts",SSTR(maxSettleConnectionAttempts));
	}

	int CTerminalConfig::getMaxParmConnectionAttempts()  {
			std::string value;
		int ret = getTerminalConfigValue("max_parm_con_attempts",value);
		if (ret == DB_OK) {
			return atoi(value.c_str());
		} else
			return 3; //default value if not exist
	}

	void CTerminalConfig::setMaxParmConnectionAttempts(
			int maxParmConnectionAttempts) {
		setTerminalConfigValue("max_parm_con_attempts",SSTR(maxParmConnectionAttempts));
	}

	std::string CTerminalConfig::getMenuType()  {
		std::string value;
		int ret = getTerminalConfigValue("menu_type",value);
		if (ret == DB_OK) {
			return value;
		} else
			return ""; //default value if not exist
	}

	void CTerminalConfig::setMenuType( std::string menuType) {
		setTerminalConfigValue("menu_type",menuType);
	}

	std::string CTerminalConfig::getMerchantCat()  {
		std::string value;
		int ret = getTerminalConfigValue("merchant_cat",value);
		if (ret == DB_OK) {
			return value;
		} else
			return ""; //default value if not exist
	}

	void CTerminalConfig::setMerchantCat( std::string merchantCat) {
		setTerminalConfigValue("merchant_cat",merchantCat);
	}

	std::string CTerminalConfig::getMerchantName()  {
		std::string value;
		int ret = getTerminalConfigValue("merchant_name",value);
		if (ret == DB_OK) {
			return value;
		} else
			return ""; //default value if not exist
	}

	void CTerminalConfig::setMerchantName( std::string merchantName) {
		setTerminalConfigValue("merchant_name",merchantName);
	}

	std::string CTerminalConfig::getMerchantNo()  {
		std::string value;
		int ret = getTerminalConfigValue("merchant_no",value);
		if (ret == DB_OK) {
			return value;
		} else
			return ""; //default value if not exist
	}

	void CTerminalConfig::setMerchantNo( std::string merchantNo) {
		setTerminalConfigValue("merchant_no",merchantNo);
	}

	long CTerminalConfig::getOilMax()  {
		std::string value;
		int ret = getTerminalConfigValue("oil_max",value);
		if (ret == DB_OK) {
			return atol(value.c_str());
		} else
			return 0; //default value if not exist
	}

	void CTerminalConfig::setOilMax(long oilMax) {
		setTerminalConfigValue("oil_max",SSTR(oilMax));
	}

	std::string CTerminalConfig::getParamDnldTime()  {
		std::string value;
		int ret = getTerminalConfigValue("param_dnld_time",value);
		if (ret == DB_OK) {
			return value;
		} else
			return ""; //default value if not exist
	}

	void CTerminalConfig::setParamDnldTime( std::string paramDnldTime) {
		setTerminalConfigValue("param_dnld_time",paramDnldTime);
	}

	int CTerminalConfig::getResponseTimeout()  {
		std::string value;
		int ret = getTerminalConfigValue("response_timeout",value);
		if (ret == DB_OK) {
			return atoi(value.c_str());
		} else
			return 60; //default value if not exist
	}

	void CTerminalConfig::setResponseTimeout(int responseTimeout) {
		setTerminalConfigValue("response_timeout",SSTR(responseTimeout));
	}

	std::string CTerminalConfig::getSessionKey()  {
		std::string value;
		int ret = getTerminalConfigValue("session_key",value);
		if (ret == DB_OK) {
			return value;
		} else
			return ""; //default value if not exist
	}

	void CTerminalConfig::setSessionKey( std::string sessionKey) {
		setTerminalConfigValue("session_key",sessionKey);
	}

	std::string CTerminalConfig::getSettlementTime()  {
		std::string value;
		int ret = getTerminalConfigValue("settlement_time",value);
		if (ret == DB_OK) {
			return value;
		} else
			return ""; //default value if not exist
	}

	void CTerminalConfig::setSettlementTime( std::string settlementTime) {
		setTerminalConfigValue("settlement_time",settlementTime);
	}

	std::string CTerminalConfig::getSpdhTerminalId()  {
		std::string value;
		int ret = getTerminalConfigValue("spdh_terminal_id",value);
		if (ret == DB_OK) {
			return value;
		} else
			return ""; //default value if not exist
	}

	void CTerminalConfig::setSpdhTerminalId( std::string spdhTerminalId) {
		setTerminalConfigValue("spdh_terminal_id",spdhTerminalId);
	}

	std::string CTerminalConfig::getSwitchBoardCode()  {
		std::string value;
		int ret = getTerminalConfigValue("switchboard_code",value);
		if (ret == DB_OK) {
			return value;
		} else
			return ""; //default value if not exist
	}

	void CTerminalConfig::setSwitchBoardCode( std::string switchBoardCode) {
		setTerminalConfigValue("switchboard_code",switchBoardCode);
	}

	std::string CTerminalConfig::getTerminalName()  {
		std::string value;
		int ret = getTerminalConfigValue("terminal_name",value);
		if (ret == DB_OK) {
			return value;
		} else
			return ""; //default value if not exist
	}

	void CTerminalConfig::setTerminalName( std::string terminalName) {
		setTerminalConfigValue("terminal_name",terminalName);
	}

	std::string CTerminalConfig::getTerminalNo()  {
		std::string value;
		int ret = getTerminalConfigValue("terminal_no",value);
		if (ret == DB_OK) {
			return value;
		} else
			return ""; //default value if not exist
	}

	void CTerminalConfig::setTerminalNo( std::string terminalNo) {
		setTerminalConfigValue("terminal_no",terminalNo);
	}

	std::string CTerminalConfig::getTrailerText()  {
		std::string value;
		int ret = getTerminalConfigValue("trailer_text",value);
		if (ret == DB_OK) {
			return value;
		} else
			return ""; //default value if not exist
	}

	void CTerminalConfig::setTrailerText( std::string trailerText) {
		setTerminalConfigValue("trailer_text",trailerText);
	}

	bool CTerminalConfig::isUseSwitchBoard()  {
		std::string value;
		int ret = getTerminalConfigValue("use_switchboard",value);
		if (ret == DB_OK) {
			if (value.compare("1") == 0)
				return true;
			else
				return false;
		} else
			return false; //default value if not exist
	}

	void CTerminalConfig::setUseSwitchBoard(bool useSwitchBoard) {
		if (useSwitchBoard)
			setTerminalConfigValue("use_switchboard","1");
		else
			setTerminalConfigValue("use_switchboard","0");
	}

	int CTerminalConfig::getVelocityCnt()  {
		std::string value;
		int ret = getTerminalConfigValue("velocity_cnt",value);
		if (ret == DB_OK) {
			return atoi(value.c_str());
		} else
			return 3; //default value if not exist
	}

	void CTerminalConfig::setVelocityCnt(int velocityCnt) {
		setTerminalConfigValue("velocity_cnt",SSTR(velocityCnt));
	}

	int CTerminalConfig::getVelocityPeriod()  {
		std::string value;
		int ret = getTerminalConfigValue("velocity_per",value);
		if (ret == DB_OK) {
			return atoi(value.c_str());
		} else
			return 24; //default value if not exist
	}

	void CTerminalConfig::setVelocityPeriod(int velocityPeriod) {
		setTerminalConfigValue("velocity_per",SSTR(velocityPeriod));
	}

	bool CTerminalConfig::isSetupComplete() {
		std::string value;
		int ret = getTerminalConfigValue("setup_complete",value);
		if (ret == DB_OK) {
			if (value.compare("1") == 0)
				return true;
			else
				return false;
		} else
			return false; //default value if not exist
	}

	void CTerminalConfig::setSetupComplete(bool setupComplete){
		if (setupComplete)
			setTerminalConfigValue("setup_complete","1");
		else
			setTerminalConfigValue("setup_complete","0");
	}


	bool CTerminalConfig::isWaitersEnabled()  {
		std::string value;
		int ret = getTerminalConfigValue("waiters_enabled",value);
		if (ret == DB_OK) {
			if (value.compare("1") == 0)
				return true;
			else
				return false;
		} else
			return false; //default value if not exist
	}

	void CTerminalConfig::setWaitersEnabled(bool waitersEnabled) {
		if (waitersEnabled)
			setTerminalConfigValue("waiters_enabled","1");
		else
			setTerminalConfigValue("waiters_enabled","0");
	}

	int CTerminalConfig::getWaitTime()  {
		std::string value;
		int ret = getTerminalConfigValue("wait_time",value);
		if (ret == DB_OK) {
			return atoi(value.c_str());
		} else
			return 5; //default value if not exist
	}

	void CTerminalConfig::setWaitTime(int waitTime) {
		setTerminalConfigValue("wait_time",SSTR(waitTime));
	}

	int CTerminalConfig::getTerminalConfigValue(std::string name,std::string &value) {
		std::string discQry = "SELECT * FROM terminal_config WHERE name = '"+name+"';";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(discQry.c_str(),records);
		if (records.size()>1) {
			conManager.getColumnValue("value",value,records,1);
			return DB_OK;
		} else
			return DB_FAIL;
	}

	void CTerminalConfig::setTerminalConfigValue(std::string name,std::string value) {
		std::string discQry = "SELECT * FROM terminal_config WHERE name = '"+name+"';";
		std::vector<std::vector<std::string> > records;
		conManager.selectFromDB(discQry.c_str(),records);
		if (records.size()>1)
			conManager.updateTerminalConfig(name,value);
		else
			conManager.insertTerminalConfig(name,value);
	}


	std::string CTerminalConfig::getTpdu()  {
		std::string value;
		int ret = getTerminalConfigValue("tpdu",value);
		if (ret == DB_OK) {
			return value;
		} else
			return ""; //default value if not exist
	}

	void CTerminalConfig::setTpdu( std::string tpdu) {
		setTerminalConfigValue("tpdu",tpdu);
	}

	std::string CTerminalConfig::getF49Enable()  {
		std::string value;
		int ret = getTerminalConfigValue("f49_enable",value);
		if (ret == DB_OK) {
			return value;
		} else
			return "0"; //default value if not exist
	}

	void CTerminalConfig::setF49Enable( std::string isEnable) {
		setTerminalConfigValue("f49_enable",isEnable);
	}

	std::string CTerminalConfig::getSSLEnable()  {
		std::string value;
		int ret = getTerminalConfigValue("ssl_enable",value);
		if (ret == DB_OK) {
			return value;
		} else
			return "0"; //default value if not exist
	}

	void CTerminalConfig::setSSLEnable( std::string isEnable) {
		setTerminalConfigValue("ssl_enable",isEnable);
	}

	std::string CTerminalConfig::getIsHexHeader()  {
		std::string value;
		int ret = getTerminalConfigValue("ishex_header",value);
		if (ret == DB_OK) {
			return value;
		} else
			return "1"; //default value if not exist
	}

 void CTerminalConfig::setIsHexHeader( std::string isEnable) {
		setTerminalConfigValue("ishex_header",isEnable);
	}

	std::string CTerminalConfig::getTpk()  {
		std::string value;
		int ret = getTerminalConfigValue("tpk",value);
		if (ret == DB_OK) {
			return value;
		} else
			return "11111111111111111111111111111111"; //default value if not exist
	}

 void CTerminalConfig::setTpk( std::string tpk){
		setTerminalConfigValue("tpk",tpk);
	}

	std::string CTerminalConfig::getIsfallbackAllowed()  {
			std::string value;
			int ret = getTerminalConfigValue("isfallback_allowed",value);
			if (ret == DB_OK) {
				return value;
			} else
				return "0"; //default value if not exist
		}

	void CTerminalConfig::setIsfallbackAllowed( std::string isEnable) {
		setTerminalConfigValue("isfallback_allowed",isEnable);
	}

	std::string CTerminalConfig::getEmvKernelVersion()  {
		std::string value;
		int ret = getTerminalConfigValue("emv_kernel_version",value);
		if (ret == DB_OK) {
			return value;
		} else
			return ""; //default value if not exist
	}

	void CTerminalConfig::setEmvKernelVersion( std::string emv_kernel_ver) {
		setTerminalConfigValue("emv_kernel_version",emv_kernel_ver);
	}

	std::string CTerminalConfig::getSdkVersion()  {
		std::string value;
		int ret = getTerminalConfigValue("sdk_version",value);
		if (ret == DB_OK) {
			return value;
		} else
			return ""; //default value if not exist
	}

	std::string CTerminalConfig::getAdkVersion()  {
		std::string value;
		int ret = getTerminalConfigValue("adk_version",value);
		if (ret == DB_OK) {
			return value;
		} else
			return ""; //default value if not exist
	}

	std::string CTerminalConfig::getAppVersion()  {
		std::string value;
		int ret = getTerminalConfigValue("app_version",value);
		if (ret == DB_OK) {
			return value;
		} else
			return ""; //default value if not exist
	}
}
