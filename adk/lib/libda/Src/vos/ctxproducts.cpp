#include <stdio.h>
#include <stdlib.h>
#include "libda/ctxproducts.h"


namespace com_verifone_txproducts
{
	CTxProducts::CTxProducts():amount(0),units(0),unitPrice(0){}

	long CTxProducts::getAmount()  {
		return amount;
	}

	void CTxProducts::setAmount(long amount) {
		this->amount = amount;
	}

	 std::string& CTxProducts::getAttribute()  {
		return attribute;
	}

	void CTxProducts::setAttribute(std::string attribute) {
		this->attribute = attribute;
	}

	 std::string& CTxProducts::getDescription()  {
		return description;
	}

	void CTxProducts::setDescription( std::string description) {
		this->description = description;
	}

	long CTxProducts::getUnitPrice()  {
		return unitPrice;
	}

	void CTxProducts::setUnitPrice(long unitPrice) {
		this->unitPrice = unitPrice;
	}

	long CTxProducts::getUnits()  {
		return units;
	}

	void CTxProducts::setUnits(long units) {
		this->units = units;
	}

}
