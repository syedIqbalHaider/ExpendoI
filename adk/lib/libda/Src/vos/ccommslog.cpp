#include <string>
#include <sstream>
#include "libda/cconmanager.h"
#include "libda/ccommslog.h"
#include "libda/himdef.h"

using namespace com_verifone_conmanager;
//using namespace std;

namespace com_verifone_commslog{

int CCommsLog::Log(CommsType type, CommsStatus status, string timestamp, string ip, string port, unsigned int seconds, unsigned int tx_count, unsigned int rx_count)
{
	CConManager conManager(DB_COMMSLOG_PATH);
	string comms_type, comms_status;
	stringstream secs, tx, rx;
	
	switch(type){
		case CT_AUTH:	comms_type = "Authorisation"; break;
		case CT_PARAM:	comms_type = "Parameters"; break;
		case CT_SOFT:	comms_type = "Software"; break;
		default:		comms_type = "Unknown"; break;
	}
	
	switch(status){
		case CS_DISCONNECT:	comms_status = "Ok"; break;
		case CS_TIMEOUT:	comms_status = "Timeout"; break;
		case CS_NO_CONNECT:	comms_status = "No Connection"; break;
		default:			comms_status = "Unknown"; break;
	}
	
	secs << seconds;
	tx << tx_count;
	rx << rx_count;
	
	return conManager.commsLog(comms_type, comms_status, timestamp, ip, port, secs.str(), tx.str(), rx.str());
}

int CCommsLog::getLog() {
	CConManager conManager(DB_COMMSLOG_PATH);
	
	std::string discQry = "SELECT * FROM Log;";
	std::vector<std::vector<std::string> > records;
	
	conManager.selectFromDB(discQry.c_str(),records);
	
	if (records.size()>1) {
		printf("DBG got data from DB\n");
		return DB_OK;
	} else{
		printf("DBG failed to get data from DB\n");
		return DB_FAIL;
	}
}

}
