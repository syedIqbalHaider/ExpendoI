#include <stdio.h>
#include <stdlib.h>
#include "libda/cproductinfo.h"


namespace com_verifone_productinfo
{
	CProductInfo::CProductInfo(){}

	std::string& CProductInfo::getExpiryDateDf33()  {
		return expiryDate_DF33;
	}

	void CProductInfo::setExpiryDateDf33( std::string expiryDateDf33) {
		expiryDate_DF33 = expiryDateDf33;
	}

	std::string& CProductInfo::getItemattrDf21()  {
		return itemattr_DF21;
	}

	void CProductInfo::setItemattrDf21( std::string itemattrDf21) {
		itemattr_DF21 = itemattrDf21;
	}

	std::string& CProductInfo::getItemCodeDDf12()  {
		return itemCodeD_DF12;
	}

	void CProductInfo::setItemCodeDDf12( std::string itemCodeDDf12) {
		itemCodeD_DF12 = itemCodeDDf12;
	}

	std::string& CProductInfo::getItemMessageDf38()  {
		return itemMessage_DF38;
	}

	void CProductInfo::setItemMessageDf38( std::string itemMessageDf38) {
		itemMessage_DF38 = itemMessageDf38;
	}

	std::string& CProductInfo::getItemTypeDf13()  {
		return itemType_DF13;
	}

	void CProductInfo::setItemTypeDf13( std::string itemTypeDf13) {
		itemType_DF13 = itemTypeDf13;
	}

	std::string& CProductInfo::getMerchantNumDf14()  {
		return merchantNum_DF14;
	}

	void CProductInfo::setMerchantNumDf14( std::string merchantNumDf14) {
		merchantNum_DF14 = merchantNumDf14;
	}

	std::string& CProductInfo::getNetworkNameDf36()  {
		return networkName_DF36;
	}

	void CProductInfo::setNetworkNameDf36( std::string networkNameDf36) {
		networkName_DF36 = networkNameDf36;
	}

	std::string& CProductInfo::getPrepaidIdCodeDf11()  {
		return prepaidIdCode_DF11;
	}

	void CProductInfo::setPrepaidIdCodeDf11(std::string prepaidIdCodeDf11) {
		prepaidIdCode_DF11 = prepaidIdCodeDf11;
	}

	std::string& CProductInfo::getProductNameDf37()  {
		return productName_DF37;
	}

	void CProductInfo::setProductNameDf37( std::string productNameDf37) {
		productName_DF37 = productNameDf37;
	}

	std::string& CProductInfo::getProviderDf22()  {
		return provider_DF22;
	}

	void CProductInfo::setProviderDf22( std::string providerDf22) {
		provider_DF22 = providerDf22;
	}

	std::string& CProductInfo::getSerialNumberDf23()  {
		return serialNumber_DF23;
	}

	void CProductInfo::setSerialNumberDf23( std::string serialNumberDf23) {
		serialNumber_DF23 = serialNumberDf23;
	}

	std::string& CProductInfo::getTrackTwoDf15()  {
		return trackTwo_DF15;
	}

	void CProductInfo::setTrackTwoDf15( std::string trackTwoDf15) {
		trackTwo_DF15 = trackTwoDf15;
	}

	 std::string& CProductInfo::getVoucherPinDf31()  {
		return voucherPin_DF31;
	}

	void CProductInfo::setVoucherPinDf31( std::string voucherPinDf31) {
		voucherPin_DF31 = voucherPinDf31;
	}

	 std::string& CProductInfo::getVoucherSerialDf32()  {
		return voucherSerial_DF32;
	}

	void CProductInfo::setVoucherSerialDf32( std::string voucherSerialDf32) {
		voucherSerial_DF32 = voucherSerialDf32;
	}
}
