/**********************************************************
  Copyright:  Verifone GmbH 2013

  Entity:     HTML Printer
  Filename:   prt.h
  Date:       May 2013

**********************************************************/
#ifndef PRINTER_IF_H_20130513
#define PRINTER_IF_H_20130513

/** \file prt.h
 * \addtogroup vfiprt Printing functions
 * @{
 */

#include <stdarg.h>
#include "types.h"

#if (defined _VRXEVO || defined _WIN32)
#  if   defined VFI_GUIPRT_EXPORT
#    define DllSpec __declspec(dllexport)
#  elif defined VFI_GUIPRT_IMPORT && defined _VRXEVO
#    define DllSpec __declspec(dllimport) // dllimport not required for Windows
#  else
#    define DllSpec
#  endif
#elif defined __GNUC__ && defined VFI_GUIPRT_EXPORT
#  define DllSpec  __attribute__((visibility ("default")))
#else
#  define DllSpec
#endif

#ifndef DOXYGEN
namespace vfihtml {}
namespace vfiprt {
#endif

#if 0
} // just to satisfy automatic indentation of the editor
#endif

using namespace vfihtml; // import common interfaces

/// @brief List of general errors
enum PrtError
{
  PRT_OK                 =  0,    /**< no error */
  // error codes from RawPrinter
  PRT_BUSY               = -1,    /**< Printing in progress */
  PRT_PAPERJAM           = -2,    /**< Paper jam */
  PRT_HEADOPEN           = -3,    /**< Head open */
  PRT_PAPEREND           = -4,    /**< Paper end */
  PRT_OVERHEAT           = -5,    /**< Head too hot */
  PRT_OVERVOLTAGE        = -6,    /**< Head over voltage */
  PRT_UNDERVOLTAGE       = -7,    /**< Head under voltage */
  PRT_FAIL               = -8,    /**< function failed (generic error) */
  PRT_SCRIPT_ERROR       = -9,    /**< error during script processing, check prtScriptError() for more details */
  PRT_NO_PRINTER         = -10,   /**< no printer available or file cannot be created in case of printing to a file */

  // additional error codes
  PRT_UNSUPPORTED        = -20,    /**< function not supported on used hardware */
  PRT_INVALID_PARAM      = -21,    /**< invalid parameters passed, e.g. NULL pointer for mandatory parameter */
  PRT_NO_RESOURCE        = -22,    /**< resource could not be allocated */
  PRT_FILE_NOT_FOUND     = -23,    /**< file not found */
  PRT_PROTOCOL           = -24,    /**< protocol error when talking to the print service */

  // transient error codes
  PRT_FINAL_RESULT       =- 40,    /**< will never be returned, just used for determining if a result is a final result */
  PRT_TIMEOUT            = -41     /**< timeout in prtWait() */
};

inline bool prtFinalResult(int x) { return x>PRT_FINAL_RESULT; }


enum PrtMode {
   PRT_PREFER_GRAPHICS, PRT_PREFER_TEXT
};

/// @brief List of numeric properties
enum PrtPropertyInt {
   PRT_PROP_STATE=0,          /**< printer state (just using the subset of PrtError that reflects the state (read only)*/
   PRT_PROP_HEAD_TEMP,        /**< Head temperature value in degrees Celsius (read only) \note V/OS and Verix do not support reading the head temperature */
   PRT_PROP_HEAD_VOLTAGE,     /**< Head voltage value in mV (read only) */
   PRT_PROP_PIXEL_WIDTH,      /**< printer width in pixels  (read only) */
   PRT_PROP_CONTRAST,         /**< printer contrast 0...255 \note Setting the contrast will only show some effect if the underlying system supports it.
                               *   V/OS and VX820Duet do not support setting the contrast. On Verix old versions of the OS may fail setting the contrast.
                               *   In this case the OS needs to be updated. */
   PRT_PROP_DEFAULT_FONT_SIZE, /**< Default font size  */
   PRT_PROP_PRINT_MODE        /**< Preferred print mode: PRT_PREFER_GRAPHICS (default) or PRT_PREFER_TEXT, ignored if not supported by the printer */
};

/// @brief List of string properties
enum PrtPropertyString {
   PRT_PROP_RESOURCE_PATH, /**< Resource path, default is resource/print */
   PRT_PROP_FILE_PREFIX,   /**< prefix that is added in front of the URL in uiInvokeURL and to the template names,
                            *   e.g. using "en/" would access the files in the subdirectory "en". */
   PRT_PROP_DEFAULT_FONT,  /**< Default font name  */
   PRT_PROP_CSS,           /**< name of a CSS file */
   PRT_PROP_INIFILE,       /**< name of the INI file, setting this property has the effect of reading and evaluating that file,
                            *   i.e. the default font, font size and the CSS file name are updated */
   PRT_PROP_JS_ROOT,       /**< Setting this path activates the JavaScript filesystem module. I/O is restricted to happen inside this path.
                            *   Use "$APPDIR" to refer to this path from within JavaScript. Images and videos may also use "$APPDIR"
                            *   to refer to files in this path. */
   PRT_PROP_DEVICE         /**< If not empty, printer device to be used on server side, e.g. /dev/ttyAMA0, not supported on all platforms,
                            *   only devices that are accessibly by the printer server can be used */
};

/** set int property
 * \param[in] property property to be set
 * \param[in] value new value
 * \return error code (see ::PrtError)
 * \note Properties are specific to each thread
 */
DllSpec int prtSetPropertyInt(enum PrtPropertyInt property, int value);

/** get int property
 * \param[in] property property to be set
 * \param[out] value current value
 * \return error code (see ::PrtError)
 */
DllSpec int prtGetPropertyInt(enum PrtPropertyInt property, int *value);

/** set string property
 * \param[in] property property to be set
 * \param[in] value new value
 * \return error code (see ::PrtError)
 * \note Properties are specific to each thread
 */
DllSpec int prtSetPropertyString(enum PrtPropertyString property, const char* value);

/** Overloaded function: Just using std::string for value  */
inline int prtSetPropertyString(enum PrtPropertyString property, const std::string &value)
{ return prtSetPropertyString(property,value.c_str()); }

/** get string property
 * \param[in] property property to be set
 * \param[out] value current value
 * \param[in] len size ouf output buffer \a value in bytes
 * \return error code (see ::PrtError)
 */
DllSpec int prtGetPropertyString(enum PrtPropertyString property, char* value, int len);

/** get string property
 * \param[in] property property to be set
 * \param[out] value current value
 * \return error code (see ::PrtError)
 */
DllSpec int prtGetPropertyString(enum PrtPropertyString property, std::string &value);

/** perform printf-like formatting.
 * \param[in] format printf-like format string. It supports the commonly known format specifiers
 *                   's', 'i', 'd', 'u', 'o', 'x', 'X', 'p', 'c', 'f', 'e', 'g'. In addition the
 *                   following format specifiers are supported:
 *                     - 'S': format as string and substitute the special HTML characters '&', '<', '>',
 *                       '&quot;' and ''' by character references (e.g. '&amp;', '&lt;' ...).
 *                     - 'C': format as character and sustitute HTML characters
 * \return formatted string */
DllSpec std::string prtFormat(const char *format, ...);

/** perfom printf-like formatting. This is the same as prtFormat() just taking a va_list
 * instead of a variable number of arguments */
DllSpec std::string prtFormatV(const char *format, va_list va);


/** print an HTML file (synchronous)
 * \param[in] value name value pairs that are used as for variable substitutions.
 * \param[in] url location of the HTML file. The location is prefixed by the resource path and by the optional prefix
 *                  (see also PRT_PROP_RESOURCE_PATH, PRT_PROP_FILE_PREFIX)
 * \param[in] landscape activate landscape printing
 * \return error code (see ::PrtError)
 */
DllSpec enum PrtError prtURL(const stringmap &value, const char *url, bool landscape=false);

/** Overloaded function: Just using std::string for url  */
inline enum PrtError prtURL(const stringmap &value, const std::string &url, bool landscape=false)
{ return prtURL(value,url.c_str(),landscape); }


/** the same as prtURL() just using an empty value map */
inline enum PrtError prtURL(const char *url, bool landscape=false)
{ stringmap value; return prtURL(value,url,landscape); }


/** Overloaded function: Just using std::string for url  */
inline enum PrtError prtURL(const std::string &url, bool landscape=false)
{ return prtURL(url.c_str(),landscape); }

/** print an HTML document (synchronous)
 * \param[in] value name value pairs that are used as for variable substitutions.
 * \param[in] text string containing an HTML fragment (i.e. the part between &lt;body> and &lt;/body>).
 * \param[in] landscape activate landscape printing
 * \return error code (see ::PrtError)
 */
DllSpec enum PrtError prtHTML(const stringmap &value,const std::string &text, bool landscape=false);

/** the same as prtHTML() just using an empty value map */
inline enum PrtError prtHTML(const std::string &text, bool landscape=false)
{ stringmap value;  return prtHTML(value,text,landscape); }


/** callback function that is called when printing has finished
 * \param[in] data data pointer provided by the application
 * \note The callback will be run within a different thread context!
 */
typedef void (*prtAsyncCallback)(void *data);


/** asynchronously start printing an HTML file, the result has to be obtained using prtWait().
 * \param[in] value name value pairs that are used as for variable substitutions.
 * \param[in] url location of the HTML file. The location is prefixed by the resource path and by the optional prefix
 *                  (see also PRT_PROP_RESOURCE_PATH, PRT_PROP_FILE_PREFIX)
 * \param[in] landscape activate landscape printing
 * \param[in] cb optional callback function that is called when printing has finished
 * \param[in] cbdata data pointer that is passed on to the callback function
 * \return error code (see ::PrtError)
 * \note If prtWait() is not called the next call to prtURLAsync() or prtHTMLAsync() will return PRT_BUSY.
 */
DllSpec enum PrtError prtURLAsync(const stringmap &value, const char *url, bool landscape=false, prtAsyncCallback cb=0, void *cbdata=0);

/** Overloaded function: Just using std::string for url  */
inline enum PrtError prtURLAsync(const stringmap &value, const std::string &url, bool landscape=false, prtAsyncCallback cb=0, void *cbdata=0)
{ return prtURLAsync(value,url.c_str(),landscape, cb,cbdata); }


/** the same as prtURLAsync() just using an empty value map */
inline enum PrtError prtURLAsync(const char *url, bool landscape=false, prtAsyncCallback cb=0, void *cbdata=0)
{ stringmap value; return prtURLAsync(value,url,landscape,cb,cbdata); }

/** Overloaded function: Just using std::string for url  */
inline enum PrtError prtURLAsync(const std::string &url, bool landscape=false, prtAsyncCallback cb=0, void *cbdata=0)
{ return prtURLAsync(url.c_str(),landscape,cb,cbdata); }

/** asynchronously start printing an HTML document, the result has to be obtained using prtWait().
 * \param[in] value name value pairs that are used as for variable substitutions.
 * \param[in] text string containing an HTML fragment (i.e. the part between &lt;body> and &lt;/body>).
 * \param[in] landscape activate landscape printing
 * \param[in] cb optional callback function that is called when printing has finished
 * \param[in] cbdata data pointer that is passed on to the callback function
 * \return error code (see ::PrtError)
 * \note If prtWait() is not called the next call to prtURLAsync() or prtHTMLAsync() will return PRT_BUSY.
 */
DllSpec enum PrtError prtHTMLAsync(const stringmap &value, const std::string &text, bool landscape=false, prtAsyncCallback cb=0, void *cbdata=0);

/** the same as prtHTMLAsync() just using an empty value map */
inline enum PrtError prtHTMLAsync(const std::string &text, bool landscape=false, prtAsyncCallback cb=0, void *cbdata=0)
{ stringmap value; return prtHTMLAsync(value,text,landscape,cb,cbdata); }

/** wait for the printing to finish and return the error code
 * \param[in] timeout_msec timeout in milliseconds, a negative value means infinite timeout
 * \return error code (see ::PrtError) or PRT_TIMEOUT in case the printing did not finish within the specified timeout.
 * \note In case of timeout prtWait() has to be called again.
 */
DllSpec enum PrtError prtWait(int timeout_msec=-1);

/** render an HTML file to a black and white PNG image
 * \param[in] destfile name of the destination PNG file
 * \param[in] width width of the image in pixels
 * \param[in] value name value pairs that are used as for variable substitutions.
 * \param[in] url location of the HTML file. The location is prefixed by the resource path and by the optional prefix
 *                  (see also PRT_PROP_RESOURCE_PATH, PRT_PROP_FILE_PREFIX)
 * \param[in] landscape activate landscape printing (image is rotated by 90 degrees)
 * \return error code (see ::PrtError)
 */
DllSpec enum PrtError prtURL2PNG(const char *destfile, int width, const stringmap &value, const char *url, bool landscape=false);

/** Overloaded function: Just using std::string for destfile  */
inline enum PrtError prtURL2PNG(const std::string &destfile, int width, const stringmap &value, const char *url, bool landscape=false)
{ return prtURL2PNG(destfile.c_str(),width,value,url,landscape); }

/** Overloaded function: Just using std::string for url  */
inline enum PrtError prtURL2PNG(const char *destfile, int width, const stringmap &value, const std::string &url, bool landscape=false)
{ return prtURL2PNG(destfile,width,value,url.c_str(),landscape); }

/** Overloaded function: Just using std::string for destfile and url  */
inline enum PrtError prtURL2PNG(const std::string &destfile, int width, const stringmap &value, const std::string &url, bool landscape=false)
{ return prtURL2PNG(destfile.c_str(),width,value,url.c_str(),landscape); }



/** shortcut omitting value */
inline enum PrtError prtURL2PNG(const char *destfile, int width, const char *url, bool landscape=false)
{
   stringmap value;
   return prtURL2PNG(destfile,width,value,url,landscape);
}

/** Overloaded function: Just using std::string for destfile  */
inline enum PrtError prtURL2PNG(const std::string &destfile, int width, const char *url, bool landscape=false)
{ return prtURL2PNG(destfile.c_str(),width,url,landscape); }

/** Overloaded function: Just using std::string for url  */
inline enum PrtError prtURL2PNG(const char *destfile, int width, const std::string &url, bool landscape=false)
{ return prtURL2PNG(destfile,width,url.c_str(),landscape); }

/** Overloaded function: Just using std::string for destfile and url */
inline enum PrtError prtURL2PNG(const std::string &destfile, int width, const std::string &url, bool landscape=false)
{ return prtURL2PNG(destfile.c_str(),width,url.c_str(),landscape); }


/** render an HTML file to a PNG image
 * \param[in] destfile name of the destination PNG file
 * \param[in] width width of the image in pixels
 * \param[in] value name value pairs that are used as for variable substitutions.
 * \param[in] text string containing an HTML fragment (i.e. the part between &lt;body> and &lt;/body>).
 * \param[in] landscape activate landscape printing (image is rotated by 90 degrees)
 * \return error code (see ::PrtError)
 */
DllSpec enum PrtError prtHTML2PNG(const char *destfile, int width, const stringmap &value, const std::string &text, bool landscape=false);

/** the same as prtHTML2PNG() just using an empty value map */
inline enum PrtError prtHTML2PNG(const char *destfile, int width, const std::string &text, bool landscape=false)
{
   stringmap value;
   return prtHTML2PNG(destfile,width,value,text,landscape);
}

/** Overloaded function: Just using std::string for destfile  */
inline enum PrtError prtHTML2PNG(const std::string &destfile, int width, const std::string &text, bool landscape=false)
{ return prtHTML2PNG(destfile.c_str(),width,text,landscape); }


/** control sequences that will be used during conversion to text.
 *  \note These control sequences may contain 0-bytes!
  */
struct prtControlSeq {
   std::string fontNormal;             /**< switch to normal (default) font (activated with font-size:24, see HTMLPrinter users guide) */
   std::string fontDoubleHeight;       /**< switch to double height font  (activated with font-size:32, see HTMLPrinter users guide) */
   std::string fontDoubleWidth;        /**< switch to double width font  (activated with font-size:40, see HTMLPrinter users guide) */
   std::string fontDoubleWidthHeight;  /**< switch to double width and height font  (activated with font-size:48, see HTMLPrinter users guide) */

   std::string italicOff;              /**< deactivate italic printing */
   std::string italicOn;               /**< activate italic printing */
   std::string boldOff;                /**< deactivate bold printing */
   std::string boldOn;                 /**< activate bold printing */
   std::string underlineOff;           /**< deactivate underline printing */
   std::string underlineOn;            /**< activate underline printing */
};

/** convert HTML document to text string. Custom control sequences can be passed that are inserted when switching the font size or style
 * \param[out] result resulting text string
 * \param[in] width width of the image in printable characters
 * \param[in] value name value pairs that are used as for variable substitutions.
 * \param[in] url location of the HTML file. The location is prefixed by the resource path and by the optional prefix
 *                  (see also PRT_PROP_RESOURCE_PATH, PRT_PROP_FILE_PREFIX)
 * \param[in] ctrl control sequences
 * \return error code (see ::PrtError)
 */
DllSpec enum PrtError prtURL2Text(std::string &result, int width, const stringmap &value, const char *url, const prtControlSeq &ctrl);

/** Overloaded function: Just using std::string for url */
inline enum PrtError prtURL2Text(std::string &result, int width, const stringmap &value, const std::string &url, const prtControlSeq &ctrl)
{ return prtURL2Text(result,width,value,url.c_str(),ctrl); }

/** short cut with empty value map */
inline enum PrtError prtURL2Text(std::string &result, int width, const char *url, const prtControlSeq &ctrl)
{ stringmap value; return prtURL2Text(result,width,value,url,ctrl); }

/** Overloaded function: Just using std::string for url */
inline enum PrtError prtURL2Text(std::string &result, int width, const std::string &url, const prtControlSeq &ctrl)
{ return prtURL2Text(result,width,url.c_str(),ctrl); }

/** convert HTML document to text string. Custom control sequences can be passed that are inserted when switching the font size or style
 * \param[out] result resulting text string
 * \param[in] width width of the image in printable characters
 * \param[in] value name value pairs that are used as for variable substitutions.
 * \param[in] text string containing an HTML fragment (i.e. the part between &lt;body> and &lt;/body>).
 * \param[in] ctrl control sequences
 * \return error code (see ::PrtError)
 */
DllSpec enum PrtError prtHTML2Text(std::string &result, int width, const stringmap &value, const std::string &text, const prtControlSeq &ctrl);

/** short cut with empty value map */
inline enum PrtError prtHTML2Text(std::string &result, int width, const std::string &text, const prtControlSeq &ctrl)
{ stringmap value; return prtHTML2Text(result,width,value,text,ctrl); }

#ifndef _VRXEVO
/** obtain internal file descriptor for use in poll(). This may be used to check whether the result
 * has been received from the print service.
 * \note On Verix using select() or pipe_pending() on this file descriptor will not work, since
 *       the task that invokes prtGetFD() will not own the returned file descriptor. In order to
 *       check wheather result has been received from the print device, please use event notification
 *       that can be enabled with prtSetVerixNotification().
 * \return file descriptor or -1 in case no printing is active
 */
DllSpec int prtGetFD();
#endif

/** load and set a catalog file containing name-value text pairs to be inserted with HTML placeholder <?text name?>.
 *  The current catalog is unloaded with filename=="" or by loading another catalog file.
 *  \param[in] filename of the catalog, empty string to unload the current dialog
 *  \return PRT_OK if file was successfully loaded, else error code (see ::PrtError)
 */
DllSpec int prtSetCatalog(const std::string &filename);

/** lookup a text from current loaded catalog by \a key name. If text is not found in catalog or
 *  no catalog is loaded the function returns value in in parameter \a default.
 *  \param[in] name of key used to lookup the text in calalog
 *  \param[in] deflt text that is returned, if text is not found in calalog
 *  \return text from catalog for success, else value in parameter \a default
 */
DllSpec std::string prtGetText(const std::string &name, const std::string &deflt="");

/** preprocess HTML code and return the resulting string
 * \param[in] value values used for insertions
 * \param[in] text string containing an HTML fragment (i.e. the part between &lt;body> and &lt;/body>).
 * \param[out] html resulting HTML code
 * \param[in] full optional parameter: If false or missing just substitute the XML processing instructions,
                   if true in addition use inline images, insert CSS code and return full HTML document
 * \return error code (see ::PrtError)
 */
DllSpec int prtGetHtml(const std::map<std::string,std::string> &value, const std::string &text, std::string &html, bool full);
DllSpec int prtGetHtml(const std::map<std::string,std::string> &value, const std::string &text, std::string &html);

/** preprocess HTML code and return the resulting string
 * \param[in] value values used for insertions
 * \param[in] url location of the dialog file. The location is prefixed by the resource path and by the optional prefix
 *                  (see also UI_PROP_RESOURCE_PATH, UI_PROP_FILE_PREFIX) unless an absolute path was provided.
 * \param[out] html resulting HTML code
 * \param[in] full optional parameter: If false or missing just substitute the XML processing instructions,
                   if true in addition use inline images, insert CSS code and return full HTML document
 * \return error code (see ::PrtError)
 */
DllSpec int prtGetHtmlURL(const std::map<std::string,std::string> &value, const std::string &url, std::string &html, bool full);
DllSpec int prtGetHtmlURL(const std::map<std::string,std::string> &value, const std::string &url, std::string &html);


/** set remote printer address
 * \param[in] address printer address. This value overwrites the address set using the LPD environment variable.
 *                    This may be used to set an IP address and port (e.g. ':0' would use the internal default printer).
 *                    Using an empty string will revert to the default setting.
 * \return error code (see ::PrtError).
 */
DllSpec int prtSetRemotePrinter(const std::string &address);


/** read library version
 * \return version string
 */
DllSpec const char *prtLibVersion();

/** returns a zero-terminated string with version and build information of libvfiguiprt
 *  in ADK version string format: \<major>.\<minor>.\<patch>-\<build>, e.g. "1.2.3-4"
 * \return version string */
DllSpec const char *prt_GetVersion();

#ifdef _VRXEVO
/** enable/disable user event notification (Verix only).
 *  This function enables the HTMLPrinter to notify a Verix application with an event EVT_USER
 *  to continue processing, if a printer result has been received. If the application uses one
 *  of the asynchronous print functions before calling blocking function wait_event(), EVT_USER
 *  is thrown to signal that it is time call function prtWait() to receive the result.
 *  Notification is enabled with user_task_id>0 and disabled with user_task_id==0. Value of
 *  variable \a user_bits can be read with Verix function read_user_event(), after EVT_USER
 *  was received.
 * \param[out] user_task_id ID of task to be notified
 * \param[out] user_bits user bits to be read with read_user_event()
 */
DllSpec void prtSetVerixNotification(int user_task_id, int user_bits);
#endif

/** \return string containing information about errors that were reported during last script processing */
DllSpec std::string prtScriptError();


/** de-/activate thread local properties for the current thread. Activating thread
 * local properties initially copies the global properties to the current thread.
 * \param[in] local if true activate thread local properties, if false discard them
 */
DllSpec void prtSetLocalProperties(bool local);

/** \return true if thread local properties are used, else return false */
DllSpec bool prtGetLocalProperties();


/** Set log mask
 * \param[in] mask logging mask (see PRT_LOGMASK)
 */
DllSpec void prtSetLogMask(unsigned mask);

/** Get log mask
 * \return current log mask
 */
DllSpec unsigned prtGetLogMask(void);

#ifndef DOXYGEN
} //namespace vfiprint
#endif

#undef DllSpec

/** @}*/

#endif  // #ifndef PRINTER_IF_H_20130513
