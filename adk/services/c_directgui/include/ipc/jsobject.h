
#ifndef IPC_JSOBJECT_H_150611
#define IPC_JSOBJECT_H_150611

/** \file jsobject.h 
 * \addtogroup vfiipc inter process communication
 * \{ 
 */
#include <string>
#include <map>
#include <vector>

#if (defined _VRXEVO || defined _WIN32)
#  if   defined VFI_IPC_SHARED_EXPORT
#    define DllSpec __declspec(dllexport)
#  elif defined VFI_IPC_STATIC_EXPORT || defined _WIN32  // dllimport not required for Windows
#    define DllSpec
#  else
#    define DllSpec __declspec(dllimport)
#  endif
#elif defined __GNUC__ && defined VFI_IPC_SHARED_EXPORT
#  define DllSpec  __attribute__((visibility ("default")))
#else 
#  define DllSpec
#endif

namespace vfiipc {
#if 0 
} // for automatic indentation
#endif

/** class for reading / writing JSON data */
class DllSpec JSObject {

 public:
   /** type of the contained data element */
   enum JSType {
      JSTNull,    /**< null (empty object) */ 
      JSTBool,    /**< boolean value */ 
      JSTString,  /**< string value */ 
      JSTInt,     /**< integer value  */ 
      JSTFloat,   /**< float value */ 
      JSTObject,  /**< object value */ 
      JSTArray    /**< array value */ 
   };

 protected:
   JSType v_type;
   bool v_bool;
   std::string v_string;
   double v_floatnum;
   long v_intnum;
   std::map<std::string,JSObject> v_object;
   std::vector<JSObject> v_array;

   static JSObject NullObject;

   int parse(const char *s);


 public:

   /** constructor, creates an empty object */
   JSObject() { v_type=JSTNull; }

   /** read type \return type */
   JSType type() const { return v_type; }

   /** check type for null \return true if type is JSTNull */
   bool isNull() const { return v_type==JSTNull; }

   /** check type for boolean \return true if type is JSTBool */
   bool isBool() const { return v_type==JSTBool; }

   /** check type for string \return true if type is JSTString  */
   bool isString() const { return v_type==JSTString; }   

   /** check type for number \return true if type is JSTInt or JSTFloat  */
   bool isNumber() const { return v_type==JSTFloat || v_type==JSTInt; } 

   /** check type for object \return true if type is JSTObject */
   bool isObject() const { return v_type==JSTObject; } 

   /** check type for array \return true if type is JSTArray */
   bool isArray() const { return v_type==JSTArray; } 

   /** \return obtain value as string, conversion is applied if required */
   std::string getString() const;

   /** Obtain content of string without copying the data
    * \return pointer to string if value is of type string, else NULL 
    * \note The pointer stays valid until the JSObject is modified or released
    */
   const std::string *getStringP() const;

   /** \return obtain value as number, conversion is applied if required */
   double getNumber() const;

   /** \return obtain value as integer, conversion is applied if required */
   long getInt() const;

   /** \return obtain value as boolean, conversion is applied if required */
   bool getBool() const;

   /** \return obtain value as boolean, conversion is applied if required */
   operator bool() const               { return getBool();   }

   /** \return obtain value as string, conversion is applied if required */
   operator std::string() const        { return getString(); }

   /** \return obtain value as char, conversion is applied if required */
   operator char() const               { return (char)getInt();          } 

   /** \return obtain value as unsigned char, conversion is applied if required */
   operator unsigned char() const      { return (unsigned char)getInt(); } 

   /** \return obtain value as short, conversion is applied if required */
   operator short() const              { return (short)getInt();         } 

   /** \return obtain value as unsigned short, conversion is applied if required */
   operator unsigned short() const     { return (unsigned short)getInt();} 

   /** \return obtain value as integer, conversion is applied if required */
   operator int() const                { return (int)getInt();           } 

   /** \return obtain value as unsigned, conversion is applied if required */
   operator unsigned() const           { return (unsigned)getInt();      } 

   /** \return obtain value as long, conversion is applied if required */
   operator long() const               { return (long)getInt();          } 

   /** \return obtain value as unsigned long, conversion is applied if required */
   operator unsigned long() const      { return (unsigned long)getInt(); } 

   /** \return obtain value as long long, conversion is applied if required */
   operator long long() const          { return (long long)getInt();     } 

   /** \return obtain value as unsigned long long, conversion is applied if required */
   operator unsigned long long() const { return (unsigned long long)getInt(); } 

   /** \return obtain value as float, conversion is applied if required */
   operator float() const              { return (float)getNumber();         } 

   /** \return obtain value as double, conversion is applied if required */
   operator double() const             { return (double)getNumber();        } 

   /** reset value to null (empty object) */
   void clear() { v_type=JSTNull; v_string.clear(); v_object.clear(); v_array.clear(); }

   /** assingment operator \return reference to current object */
   JSObject &operator=(bool val)        { v_type=JSTBool;    v_bool=val; return *this; }

   /** assingment operator \return reference to current object */
   JSObject &operator=(char *val)       { v_type=JSTString;  v_string=val?val:""; return *this; }

   /** assingment operator \return reference to current object */
   JSObject &operator=(const char *val) { v_type=JSTString;  v_string=val?val:""; return *this; }

   /** assingment operator \return reference to current object */
   JSObject &operator=(std::string val) { v_type=JSTString;  v_string=val; return *this; }

   /** assingment operator \return reference to current object */
   JSObject &operator=(double val)      { v_type=JSTFloat;   v_floatnum=val; return *this; }

   /** assingment operator \return reference to current object */
   JSObject &operator=(float val)       { v_type=JSTFloat;   v_floatnum=val; return *this; }

   /** assingment operator \return reference to current object */
   template<typename T> JSObject &operator=(T val) { v_type=JSTInt;  v_intnum=val; return *this; }

   /** check if this object has a member \a elem 
    * \param[in] elem member to test for 
    * \return true if the object has the named member */
   bool exists(const char *elem) const;

   /** check if this object has a member \a elem 
    * \param[in] elem member to test for 
    * \return true if the object has the named member */
   bool exists(const std::string &elem) const;

   /** remove element from object */
   void erase(const char *elem);

   /** remove element from object */
   void erase(const std::string &elem);

   /** access the member \a elem of this object. If the member does not exist, it is created 
    * \param[in] elem member to access
    * \return reference to the accessed member.
    */ 
   JSObject &operator()(const char *elem);

   /** access the member \a elem of this object. 
    * \param[in] elem member to access
    * \return reference to the accessed member or reference to null object if the member does not exist
    */ 
   const JSObject &operator()(const char *elem) const;

   /** access the member \a elem of this object. If the member does not exist, it is created 
    * \param[in] elem member to access
    * \return reference to the accessed member.
    */ 
   JSObject &operator()(const std::string &elem);

   /** access the member \a elem of this object. 
    * \param[in] elem member to access
    * \return reference to the accessed member or reference to null object if the member does not exist
    */ 
   const JSObject &operator()(const std::string &elem) const;
   
   /** access the array entry with index \a idx, the array is resized if required.
    * \param[in] idx array index
    * \return reference to the element in the array or reference to null object if idx<0
    */
   JSObject &operator[](int idx);

   /** access the array entry with index \a idx
    * \param[in] idx array index
    * \return reference to the element in the array or reference to null object if idx out of range
    */
   const JSObject &operator[](int idx) const;

   /** determine size of an array
    * \return size of the array. For non-arrays 0 is returned */
   unsigned size() const { return v_type==JSTArray?v_array.size():0; } 

   /** resize array
    * \param[in] new_size new size
    * \note If the object is not an array, it is cleared and then converted to array
    */
   void resize(unsigned new_size); 

   /** iterator type for iterating over the direct members of an object,
    * the iterator type has two members:
    *   - first name of the member
    *   - second content of the member
    */
   typedef std::map<std::string,JSObject>::iterator iterator;
   typedef std::map<std::string,JSObject>::const_iterator const_iterator;
   
   /** \return iterator for beginning of members */
   iterator begin() { return v_object.begin(); }

   /** \return iterator for end of members */
   iterator end() { return v_object.end(); }

   /** \return iterator for beginning of members */
   const_iterator begin() const { return v_object.begin(); }

   /** \return iterator for end of members */
   const_iterator end() const { return v_object.end(); }

   /** import JSON encoded string 
    * \param[in] s string to be imported
    * \return true if import was successful, false if an error occurred */
   bool load(const std::string &s) { return parse(s.c_str())==(int)s.length(); }

   /** export data as JSON encoded string
    * \return exported data
    */
   std::string dump() const;

   /** swap content with other object 
    * \param[in,out] o other object
    */
   void swap(JSObject &o);

   
   /** returns a zero-terminated string with version and build information of libvfijson 
    * \return version string */
   static const char *getVersion();
};

} // namespace vfiipc

#undef DllSpec

/** \} */
#endif
