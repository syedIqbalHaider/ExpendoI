#ifndef NOTIFY_H_20150722
#define NOTIFY_H_20150722


/** \file notify.h */

#include <string>
#include <ipc/jsobject.h>


#if (defined _VRXEVO || defined _WIN32)
#  if   defined VFI_IPC_SHARED_EXPORT
#    define DllSpec __declspec(dllexport)
#  elif defined VFI_IPC_STATIC_EXPORT || defined _WIN32  // dllimport not required for Windows
#    define DllSpec
#  else
#    define DllSpec __declspec(dllimport)
#  endif
#elif defined __GNUC__ && defined VFI_IPC_SHARED_EXPORT
#  define DllSpec  __attribute__((visibility ("default")))
#else 
#  define DllSpec
#endif

#ifndef DEPRECATED
#  ifdef __GNUC__
#    define DEPRECATED __attribute__((deprecated))
#  elif defined(_MSC_VER)
#    define DEPRECATED __declspec(deprecated)
#  else
#    define DEPRECATED
#  endif
#endif

/** IPC namespace */
namespace vfiipc {
#if 0
}
#endif

/** Notify result code */
enum NotifyResult {
   NOTIFY_OK=0,          /**< No error. */
   NOTIFY_ERR_SERVER=-1,  /**< notification server is not running */
   NOTIFY_ERR_APP_ID=-2,  /**< the application ID has not been set yet, use ipcSetAppID to set it */
   NOTIFY_ERR_PERMISSION=-3, /**< No permission to create the notification pipe */
   NOTIFY_ERR_CONFIG_FILE=-4,   /**< The configuration file could not be opened for reading */
   NOTIFY_ERR_CONFIG_SYNTAX=-5, /**< The configuration file contained invalid input */
};


/** application ID that will be used as source address when sending notifications
 * \param[in] app_id application ID, the app ID should match the application ID used in the manifest.
 */
DllSpec void ipcSetAppID(const std::string &app_id);

/** read application ID that has been set using ipcSetAppID
 * \return application ID
 */
DllSpec std::string ipcGetAppID();

/** Send notification 
 * \param[in] to destination application ID or '1' to send notifications to exactly one receiver
 *                 or '*' to broadcast to all registered receivers
 * \param[in] notification_id notification ID
 * \param[in] param parameters to send along the notification
 * \param[in] flags optional flags for future extensions
 * \param[in] from optional source application ID. If empty the application ID set by trigSetAppID is used.
 *                 This parameter is only required if a process needs to impersonate as another application,
 *                 e.g. the multi app controller may use it to send notifications on behalf of CP apps
 * \return error code
 */
DllSpec enum NotifyResult ipcNotify(const std::string &to,
             const std::string &notification_id,
             const vfiipc::JSObject &param,
             unsigned flags=0,
             const std::string &from=""
             );
             
/** Notify callback. The callback gets invoked when a matching notification has been sent. 
 * \param[in] data opaque data pointer that was passed by the application when registering the callback.
 * \param[in] from source application ID
 * \param[in] to destination application ID that was used to send the notification. This may be used to distinguish
 *               the different cases send to application, send to single, send to all.
 * \param[in] notification_id notification ID
 * \param[in] param parameters to send along the notification
 * \param[in] flags optional flags for future extensions
 * \note All notification and error callbacks are run in separate threads. These are started on demand and terminate when the callback terminates.
 */
typedef void (*NotifyCallback)(
   void *data,
   const std::string &from,
   const std::string &to,
   const std::string &notification_id,
   const vfiipc::JSObject &param,
   unsigned flags);


/** Register notification callback. New registrations will overwrite old ones for the same notification ID and destination app ID.
 * \param[in] notification_id notification ID
 * \param[in] cb notification callback to be invoked. If NULL the registration will be discarded
 * \param[in] data opaque data pointer passed on to the callback
 * \param[in] priority priority of the callback. If several matching callbacks are found and the notification is not sent as a broadcast,
 *                     then only the callback with the highest priority will be called.
 * \param[in] to optional destination application ID. If empty then the application ID set by trigSetAppID is used.
 *               This parameter is only required if a process needs to impersonate as another application,
 *               e.g. the multi app controller may use it to receive notifications on behalf of CP apps
 * \return error code 
 */
DllSpec enum NotifyResult ipcRegisterNotificationCB(const std::string &notification_id, NotifyCallback cb, void *data, int priority=0, const std::string &to="");


/** unregister a notification callback. When the function returns, it is guaranteed, that the callback has terminated and will not be invoked any longer.
 * \param[in] notification_id notification ID, must match the value passed to ipcRegisterNotificationCB
 * \param[in] cb notification callback used during registration. If NULL all callbacks registered for the notification ID are discarded. 
 *                            Use with care to not unintentionally discard registrations of other threads or libraries.
 * \param[in] data opaque data pointer used during registration. If NULL only cb is checked for finding the matching callback function.
 * \param[in] to optional destination application ID. If empty then the application ID set by trigSetAppID is used, must match the value 
 *                        passed to ipcRegisterNotificationCB.
 * \return error code
 * \note ipcUnregisterNotificationCB cannot be used within a callback to remove the callback itself: A deadlock occurs since it 
 *       waits for the callback to terminate before removing it.
 */
DllSpec enum NotifyResult ipcUnregisterNotificationCB(const std::string &notification_id, NotifyCallback cb, void *data=0, const std::string &to="");

/** unregister a notification callback, shortcut for ipcRegisterNotificationCB(notification_id,0,0,0,to);
 * \param[in] notification_id notification ID
 * \param[in] to optional destination application ID. If null then the application ID set by trigSetAppID is used.
 *               This parameter is only required if a process needs to impersonate as another application,
 *               e.g. the multi app controller may use it to receive notifications on behalf of CP apps
 * \return error code
 * \note ipcUnregisterNotificationCB cannot be used within a callback to remove the callback itself: A deadlock occurs since it 
 *       waits for the callback to terminate before removing it.
 */
DEPRECATED inline enum NotifyResult ipcUnregisterNotificationCB(const std::string &notification_id, const std::string &to="")
{
   return ipcUnregisterNotificationCB(notification_id,0,0,to);
}

/** Register error callback. The error callback is invoked if the notification server 
 *  cannot deliver a unicast notification to any receiver. Broadcast notifications are not considered.
 * \param[in] cb notification callback to be invoked. 
 * \param[in] data opaque data pointer passed on to the callback
 * \return error code
 * \note In case a receiver process terminates there is some risk that the failure to deliver a notification will go unnoticed:
 * If the process terminates after receiving a notification but before sending an error, this error cannot be detected.
 */
DllSpec enum NotifyResult ipcRegisterUnicastError(NotifyCallback cb, void *data);

/** Unregister error callback. When the function returns, it is guaranteed, that the callback has terminated and will not be invoked any longer.
 * \param[in] cb notification callback used during registration. If NULL all callbacks registered for the notification ID are discarded. 
 *                            Use with care to not unintentionally discard registrations of other threads or libraries.
 * \param[in] data opaque data pointer used during registration. If NULL only cb is checked for finding the matching callback function.
 * \return error code
 * \note In case a receiver process terminates there is some risk that the failure to deliver a notification will go unnoticed:
 * If the process terminates after receiving a notification but before sending an error, this error cannot be detected.
 * \par
 * \note ipcUnregisterUnicastError cannot be used within a callback to remove the callback itself: A deadlock occurs since it 
 *       waits for the callback to terminate before removing it.
 */
DllSpec enum NotifyResult ipcUnregisterUnicastError(NotifyCallback cb, void *data);

/** unregister an error callback, shortcut for ipcRegisterUnicastError(0,0);
 * \return error code
 * \note ipcUnregisterUnicastError cannot be used within a callback to remove the callback itself: A deadlock occurs since it 
 *       waits for the callback to terminate before removing it.
 */
DEPRECATED inline enum NotifyResult ipcUnregisterUnicastError() {
   return ipcRegisterUnicastError(0,0);
}

/** Observer callback, will be called for all delivered notifications. If no receiver has been found the callback
 * is called with an empty \a to parameter.
 * \param[in] data opaque data pointer that was passed by the application when registering the callback.
 * \param [in] from source application ID
 * \param [in] to resolved destination application ID
 * \param[in] notification_id notification ID
 * \param[in] flags optional flags for future extensions
 * \note All observer callbacks are run in separate threads. These are started on demand and terminate when the callback terminates.
 */
typedef void (*NotifyObserver)(
   void *data,
   const std::string &from, 
   const std::string &to, 
   const std::string &notification_id,
   unsigned flags
);

/** register notification observer callback. The callback will be invoked for each notification delivered. 
 * \param[in] cb notification callback to be invoked. 
 * \param[in] data opaque data pointer passed on to the callback
 * \return error code
 */
DllSpec enum NotifyResult ipcRegisterObserver(NotifyObserver cb, void *data);

/** unregister notification observer callback. When the function returns, it is guaranteed, that the callback has terminated and will not be invoked any longer.
 * \param[in] cb notification callback used during registration. If NULL all registrations will be discarded
 * \param[in] data opaque data pointer passed used during registration. If NULL only cb is checked for finding the matching callback function.
 * \return error code
 * \note ipcUnregisterObserver cannot be used within a callback to remove the callback itself: A deadlock occurs since it 
 *       waits for the callback to terminate before removing it.
 */
DllSpec enum NotifyResult ipcUnregisterObserver(NotifyObserver cb, void *data);


/** unregister notification observer callback. When the function returns, it is guaranteed, that the callback has terminated and will not be invoked any longer.
 * \return error code
 * \note ipcUnregisterObserver cannot be used within a callback to remove the callback itself: A deadlock occurs since it 
 *       waits for the callback to terminate before removing it.
 */
DEPRECATED inline enum NotifyResult ipcUnregisterObserver() {
   return ipcRegisterObserver(0,0);
}

/** start the notification server. Only one notification server may exist in the system. Normally it 
 * is started from the multi application controller.
 * \return error code
 * \note This function only returns in case of error.
 */
DllSpec enum NotifyResult ipcNotificationServer();


/** start the notification server. Only one notification server may exist in the system. Normally it 
 * is started from the multi application controller. The config_file names a configuration file that
 * contains a whitelist of notifications that may be received from external IP addresses and that may 
 * be sent to external ones.
 * \param[in] config_file name of the configuration file. May be NULL in which case it is identical to ipcNotificationServer().
 * \return error code
 * \note This function only returns in case of error.
 * \par
 * \note If there is a configuration, then the notification server binds to IPADDR_ANY so that notifications
 * from external IP addresses may be accepted depending on the configuration. If config_file is NULL or
 * the configuration is empty, then the notification server binds to localhost so that only connects from
 * localhost are supported. 
 * \par
 * \note On Verix connecting from external IP addresses is not supported, \a config_file is ignored.
 */
DllSpec enum NotifyResult ipcNotificationServer(const char *config_file);

} //namespace

#undef DllSpec
#endif
