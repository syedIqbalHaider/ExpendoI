#ifndef IPC_H_20140521
#define IPC_H_20140521

/** \file ipc.h inter process communication */

#include <vector>
#include <string>

#if (defined _VRXEVO || defined _WIN32)
#  if   defined VFI_IPC_SHARED_EXPORT
#    define DllSpec __declspec(dllexport)
#  elif defined VFI_IPC_STATIC_EXPORT || defined _WIN32  // dllimport not required for Windows
#    define DllSpec
#  else
#    define DllSpec __declspec(dllimport)
#  endif
#elif defined __GNUC__ && defined VFI_IPC_SHARED_EXPORT
#  define DllSpec  __attribute__((visibility ("default")))
#else 
#  define DllSpec
#endif

namespace vfiipc {

/** event callback function that is called, if incoming data is pending on the IPC object, 
 *  for which this function was registered with IPC::set_callback().
 * \note A pointer of the IPC object is passed as parameter 
 * \param[in] data data pointer that was passed by application to IPC::set_callback()
 * \param[in] pointer of IPC object, for which data is pending
 */
class IPC;
typedef void (*ipcCallback)(void *data, IPC *ipc);

/** base class for inter process communication.
 *  Both stream and message based operation is supported. For messages the message format is as follows:\n
 *  &lt;4 bytes prefix> &lt;4 bytes message size> &lt;4 bytes message ID> &lt;message>\n
 *  All numbers are in big endian format. Size includes the size of the message ID.
 *  The prefix is used for checking synchronization of sender and receiver and is used to re-synchronize
 *  in case synchonization has been lost: The receiver discards data until a valid prefix has been found.
 */
class DllSpec IPC {

   // prevent usage of copy constructor and assignment operator
   IPC(const IPC &o);
   void operator=(const IPC &o);

 protected:
   /** class storing private implementation data of class IPC */
   class IpcPrivate;

   /** constructor */
   IPC(IpcPrivate *_d);
   
   /** pointer to private implementation data of class IPC */
   IpcPrivate *d;

 public:

   /** destructor */
   virtual ~IPC();

   /** returns true, if IPC has indicated an EOF (end of file) and connection was closed.
    *  Invoke close() function for re-use of this IPC object.
    */
   virtual bool eof();
   
   /** returns true, IPC has indicated an error (e.g. connection is distrubed).
    *  Invoke close() function for re-use of this IPC object.
    */
   virtual bool error();

   /** accept an incoming connection for \a timeout_msec milliseconds. A negative value means wait forever.
    * Please note that accept() only works for IPC objecst in server mode (e.g. see TCP::listen() or Pipe::listen()).
    * If an incoming connection has been accepted a pointer to an object to this connection is returned 
    * that has been allocated using new. The caller overtakes ownership for this object and if it is not needed 
    * any longer it must be released using delete.
    * \param[in] timeout_msec timeout to wait for a new connection in milliseconds. A negative timeout means wait forever.
    * \return pointer to a new IPC object on success, else NULL in case of timeout or error
    * \note On Verix eVo this function must be called by same thread, which has invoked TCP::listen() or Pipe::listen().
    */
   virtual IPC *accept(int timeout_msec=-1) { (void)timeout_msec; return 0; }

   /** close any open internal file descriptors */
   virtual void close();
   
   /** write data to the stream
    * \param[in] data data to be written
    * \param[in] size number of bytes to be written
    * \return true in case of success, else false 
    */
   virtual bool write(const void *data, int size);

   /** read data from the stream
    * \param[out] data buffer that will receive the read bytes
    * \param[in] maxsize number of bytes to be read. 
    * \param[in] timeout_msec timeout for reading in milliseconds. A negative timeout means wait forever.
    * \param[in] timeout_msec2 timeout for reading after at least one byte has been read in milliseconds. A negative timeout means wait forever.
    * \return number of bytes read. A short read indicates EOF, timeout or error */
   virtual int read(void *data, int maxsize, int timeout_msec, int timeout_msec2);

   /** read data from the stream
    * \param[out] data buffer that will receive the read bytes
    * \param[in] maxsize number of bytes to be read. 
    * \param[in] timeout_msec timeout for reading in milliseconds. A negative timeout means wait forever.
    * \return number of bytes read. A short read indicates EOF, timeout or error */
   virtual int read(void *data, int maxsize, int timeout_msec=-1) { return read(data,maxsize,timeout_msec,timeout_msec); }


   /** write a message to the stream. The message is prefixed by \a prefix and the size of 
    *  the message (including msg_id, big endian) and by 4 bytes message ID.
    * \param[in] prefix prefix of the message
    * \param[in] msg_id message ID
    * \param[in] msg message to be sent
    * \param[in] size size of the message
    * \return true in case of success, else false 
    */
   virtual bool write_msg(unsigned prefix, int msg_id, const void *msg, int size);

   /** write a message to the stream. The message is prefixed by \a prefix and the size of 
    *  the message (big endian).
    * \param[in] prefix prefix of the message
    * \param[in] msg_id message ID
    * \param[in] msg message to be sent
    * \return true in case of success, else false 
    */
   virtual bool write_msg(unsigned prefix, int msg_id, const std::vector<unsigned char> &msg)
      { return write_msg(prefix,msg_id,msg.size()?&msg[0]:0,msg.size()); }

   /** write a message to the stream. The message is prefixed by \a prefix and the size of 
    *  the message (big endian).
    * \param[in] prefix prefix of the message
    * \param[in] msg_id message ID
    * \param[in] msg message to be sent
    * \return true in case of success, else false 
    */
   virtual bool write_msg(unsigned prefix, int msg_id, const std::vector<char> &msg)
      { return write_msg(prefix,msg_id,msg.size()?&msg[0]:0,msg.size()); }

   /** read message from the stream. 
    * \param[in] prefix The data stream is checked to start with \a prefix. This is used 
    *                   to re-synchronize on the incoming data stream if required.
    * \param[out] msg_id message ID
    * \param[out] msg received message.
    * \param[in] size_limit size limit to prevent exhaustive memory allocations in case
    *                 corrupt data has been received.
    * \param[in] timeout_msec timeout for reading in milliseconds. A negative timeout means wait forever.
    * \return true in case of success, else false 
    */
   virtual bool read_msg(unsigned prefix, int &msg_id, std::vector<unsigned char> &msg, int size_limit, int timeout_msec=-1);

   /** read message from the stream. 
    * \param[in] prefix The data stream is checked to start with \a prefix. This is used 
    *                   to re-synchronize on the incoming data stream if required.
    * \param[out] msg_id message ID
    * \param[out] msg received message.
    * \param[in] size_limit size limit to prevent exhaustive memory allocations in case
    *                 corrupt data has been received.
    * \param[in] timeout_msec timeout for reading in milliseconds. A negative timeout means wait forever.
    * \return true in case of success, else false 
    */
   virtual bool read_msg(unsigned prefix, int &msg_id, std::vector<char> &msg, int size_limit, int timeout_msec=-1);

   /** check for availability of incoming data on the stream. In case of an error 
    *  or an EOF this function also returns true and sets the internal flags for
    *  functions IPC::eof() and IPC::error().
    * \param[in] timeout_msec timeout to wait for available data in milliseconds.
    *                         A negative timeout means wait forever.
    * \return true in case of data is available (or EOF/error), else false
    */
   virtual bool poll_in(int timeout_msec=-1);

   /** For IPC::TCP objects:
    *  This function always returns the IP address and session port of the connected client or server, string format is: "&lt;IP address>:&lt;port>".
    *  For IPC::Pipe objects:
    *  - On V/OS and Raptor this function always returns PID, UID and GID of the remote process (string format: "&lt;pid> &lt;uid> &lt;gid>").
    *    Since credential information is obtained from underlying OS, on V/OS and Raptor it is not necessary to enable 
    *    Pipe::PC_EnableCredentials, since no additional resources are required on that platforms to provide this information.
    *  - On Verix eVo, this function returns an empty string by default. Credential messages must enabled by configuration 
    *    Pipe::PC_EnableCredentials in constructor Pipe(int config) and this function will return the following values:
    *    PID corresponds the remote task ID, GID is its group ID and UID is set to -1 (string format: "&lt;task ID> -1 &lt;group ID>").
    *    An IPC client gets the &lt;task ID> and &lt;group ID> of the remote server task, which has invoked Pipe::accept().
    *    An IPC server gets the &lt;task ID> and &lt;group ID> of the remote client task, which has invoked Pipe::connect().
    * \note In order to retrieve the remote information on Verix eVo, both IPC client and IPC server must have enabled
    *       Pipe::PC_EnableCredentials. For compatibility reasons the usage of credential information requires additional resources 
    *       in the IPC server: A second internal pipe is created to receive and transmit the credentials messages. This pipe is created
    *       with invocation of Pipe::listen(const char *pipe), whereas the pipe name corresponds to string of parameter \a pipe plus suffix "_2".
    * \return remote information string or an empty string */
   virtual const char *remote_addr() const;

#ifdef _VRXEVO
   /** enable/disable user event notification (Verix only).
    *  This function enables IPC to notify a Verix application with an event EVT_USER,
    *  if some data has been received. If the application waits with blocking function
    *  wait_event(), EVT_USER is thrown to signal that some IPC data was received and
    *  that it is time call IPC functions read() or read_msg(). Notification is enabled
    *  with user_task_id>0 and disabled with user_task_id==0. Please note that value 
    *  of \a user_bits must be greater than 0, otherwise no event will be triggered. 
    *  Value of \a user_bits is passed to the application and can be read with Verix
    *  function read_user_event(), after EVT_USER was received.
    * \param[in] user_task_id ID of task to be notified
    * \param[in] user_bits user bits to be read with read_user_event(), must be >0
    */
   virtual void set_verix_notification(int user_task_id, int user_bits);
#else
   /** obtain the file descriptor used for reading/writing data \return file descriptor 
    * \note This function is recently not supported for Verix eVo, since using select() 
    *       or pipe_pending() on this file descriptor will not work. Application that 
    *       would invoke getFD() is not owner of this file descriptor, which is owned 
    *       by IPC exclusively. In order to check wheather any data has been received
    *       from the IPC, please use event notification that can be enabled with 
    *       IPC::set_verix_notification().
    */
   int getFD() const;
#endif

   /** register a callback function for this IPC object, which is invoked, if incoming 
    *  data is pending and that IPC functions read() or read_msg() can be called to 
    *  read the data. Set cb to NULL to unregister the callback for this IPC object.
    * \param[in] cb callback function, which is invoked, if has been received.
    *               Passing a NULL pointer will unregister the callback.
    * \param[in] data data pointer, which is passed to callback function \a cb
    * \return    true for success, else callback couldn't be registered/unregistered.
    * \note Please note that the callback handler internally uses function
    *       IPC::set_verix_notification() on Verix eVo. For this reason application 
    *       is not allowed to set a callback and and invoke IPC::set_verix_notification() 
    *       on the same IPC object, since detection of incoming data with this callback 
    *       would not work.
    */
   virtual bool set_callback(ipcCallback cb, void *data=0);
   
   /** callbacks configured with function IPC::set_callback() are invoked in context of a 
    *  seperate thread that is started with first invocation of this function. On some 
    *  platforms (especially Verix eVo) an application can reduce the stacksize for this 
    *  thread, if the dynamic memory is limited. As default the stacksize is given by the
    *  system, e.g. on Verix eVo the thread is created with a stacksize of 64 kb.  
    *  Function IPC::set_callback_stacksize() can be called at system startup (before 
    *  first call of IPC::set_callback()) to set a customized stacksize for the callback thread. 
    * \param[in] size stacksize of the thread (in bytes) invoking callback function registered 
    *                 with IPC::set_callback(). Please note that minimal required stacksize
    *                 depends on the application code that is placed to the callback function.
    *                 Minimal value is 8k and sizes below this limit will set this minmal value.
    *                 A negative size means the usage of the system default stacksize (default).
    * \note This function has no effect, if the callback thread was already started 
    *       by calling IPC::set_callback(). A running callback thread is not restarted with 
    *       another stacksize.
    */
   static void set_callback_stacksize(int size=-1);

   /** returns a zero-terminated string with version and build information of libvfiipc 
    * \return version string */
   static const char *getVersion();
   
   /** returns a zero-terminated string with version and build information of libvfiipc
    *  in ADK version string format: &lt;major>.&lt;minor>.&lt;patch>-&lt;build>, e.g. "1.2.3-4"
    * \return version string */
   static const char *ipc_GetVersion();
   
protected:
   class IpcBuffer;
   /** base function for reading messages */
   virtual bool read_msg(unsigned prefix, int &msg_id, IpcBuffer &msg, int size_limit, int timeout_msec=-1);
   
public:
   /** write a message to the stream. The message is prefixed by \a prefix and the size of 
    *  the message (big endian).
    * \param[in] prefix prefix of the message
    * \param[in] msg_id message ID
    * \param[in] msg message to be sent
    * \return true in case of success, else false 
    */
   virtual bool write_msg(unsigned prefix, int msg_id, const std::string &msg)
      { return write_msg(prefix,msg_id,msg.data(),msg.size()); }
   
   /** read message from the stream. 
    * \param[in] prefix The data stream is checked to start with \a prefix. This is used 
    *                   to re-synchronize on the incoming data stream if required.
    * \param[out] msg_id message ID
    * \param[out] msg received message.
    * \param[in] size_limit size limit to prevent exhaustive memory allocations in case
    *                 corrupt data has been received.
    * \param[in] timeout_msec timeout for reading in milliseconds. A negative timeout means wait forever.
    * \return true in case of success, else false 
    */
   virtual bool read_msg(unsigned prefix, int &msg_id, std::string &msg, int size_limit, int timeout_msec=-1);
};

/** class for inter process communication via TCP
 */
class DllSpec TCP: public IPC { 
   // prevent usage of copy constructor and assignment operator
   TCP(const TCP &o);
   void operator=(const TCP &o);
   /** class storing private implementation data of class TCP */
   class TcpPrivate;

 public:

   /** constructor */
   TCP();
   /** destructor */
   ~TCP();
  
   /** initiate a connection to a remote IPC server via a TCP socket 
    * \param[in] hostname name or IP address of the destination host
    * \param[in] port port of the destination host
    * \param[in] timeout_msec connect timeout in milliseconds. A negative timeout means using default OS timeout.
    * \return true in case that connection was established successfully, else false 
    */
   bool connect(const char *hostname, unsigned short port,int timeout_msec=-1);
   
   /** creates a TCP listen socket for server mode to accept incomming connections with TCP::accept().
    * \param[in] port port of the TCP listen socket
    * \param[in] listen_address IP address of adapter that should be used to accept the connection
    *                           Default listen_address=0 means acceptance of connections on all available adapters.
    *                           Use "127.0.0.1" for local loopback device to avoid that external connections are accepted.
    * \return true in case that the TCP listen socket was created successfully, else false 
    */
   bool listen(unsigned short port, const char *listen_address=0);
   
   /** accepts the first connection on the queue of pending connections on the listen TCP socket. 
    * Please note that accept() only works for IPC object in server mode (see TCP::listen()).
    * If an incoming connection has been accepted a pointer to an object to this connection is returned 
    * that has been allocated using new. The caller overtakes ownership for this object and if it is not needed 
    * any longer it must be released using delete.
    * \param[in] timeout_msec timeout to wait for a new connection in milliseconds. A negative timeout means wait forever.
    * \return pointer to a new IPC object on success, else NULL in case of timeout or error
    * \note On Verix eVo this function must be called by same thread, which has invoked TCP::listen().
    */
   IPC * accept(int timeout_msec=-1);
   
   /** closes the session and all TCP socket sockets for this object */
   void close();
   
   /** Couples an external socket file descriptor to this IPC object. 
    *  The socket should belong to an active connection.
    * \param[in] socket_fd socket file descriptor
    * \return true in case that socket was coupled successful else false 
    * \note If this IPC object already is using another socket, the old
    *       socket is closed and \a socket_fd is used as new endpoint.
    *       Please note that this IPC object overtakes the ownership of the
    *       socket and application MUST call IPC::close() to close the socket.
    *       On Verix eVo it is also important that the task invoking IPC::setFD() 
    *       is the owner of the socket, otherwise setFD() will fail.
    *       In addition, IPC::remote_addr() will not provide any remote information
    *       for this IPC object (empty string), after IPC::setFD() was invoked.
    */
   bool setFD(int socket_fd);
};

#ifndef _WIN32 // class Pipe is recently not available on Windows
/** class for inter process communication via named pipes
 */
class DllSpec Pipe: public IPC {
   // prevent usage of copy constructor and assignment operator
   Pipe(const Pipe &o);
   void operator=(const Pipe &o);
   /** class storing private implementation data of class Pipe */
   class PipePrivate;

 public:
   
   /** Pipe configuration options (bit mask) */
   enum PipeConfig {
     PC_None=0,              /**< no configuration */
     PC_EnableCredentials=1  /**< enable credentials mode to get information of the remote process on Verix eVo.
                              *   On V/OS and Raptor this option is ignored. For more details please refer
                              *   to function IPC::remote_addr(). */
   };
   
   /** default constructor, equals config constructor Pipe(::PC_None) */
   Pipe();
   /** config constructor */
   Pipe(int config);
   /** destructor */
   ~Pipe();

   /** initiate a connection to an IPC server via a named pipe
    * \param[in] pipe name of the pipe, which the server has opened before with Pipe::listen()
    * \return true in case that connection was established successfully, else false 
    */
   bool connect(const char *pipe);
   
   /** creates a named pipe for server mode to listen and accept new incomming connections with Pipe::accept().
    * \param[in] pipe name of the pipe used for the listening
    * \return true in case that the listen pipe was created successfully, else false 
    */
   bool listen(const char *pipe);
   
   /** accepts the first connection on the queue of pending connections on the listen pipe. 
    * Please note that accept() only works for IPC object in server mode (see Pipe::listen()).
    * If an incoming connection has been accepted a pointer to an object to this connection is returned 
    * that has been allocated using new. The caller overtakes ownership for this object and if it is not needed 
    * any longer it must be released using delete.
    * \param[in] timeout_msec timeout to wait for a new connection in milliseconds. A negative timeout means wait forever.
    * \return pointer to a new IPC object on success, else NULL in case of timeout or error
    * \note On Verix eVo this function must be called by same thread, which has invoked Pipe::listen().
    */
   IPC * accept(int timeout_msec=-1);
   
   /** closes the session and all pipe handles for this object */
   void close();
};
#endif

} // namespace vfiipc

#undef DllSpec

#endif
