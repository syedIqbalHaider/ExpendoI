
////////////////////////////////////////////////////////////////////////////////
///////////////////////////// global Main loop /////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

var uim_session_id;
var uim_url;
var uim_regtab;
var uim_sigcap_name="";
var uim_sigcap_output="";
var uim_mbutton=[0,0,0];
var uim_click_cnt=0;


function log(x)
{
   x=(x+"").replace(/</g,"&lt;")
   x=x.replace(/>/g,"&gt;")
   document.getElementById("log").innerHTML+=x+"<br>"
}

function forall(node,f) {
   f(node);
   for(var i=0;i<node.childNodes.length;i++) forall(node.childNodes[i],f);
}   

function find(node,f) {
   var match=f(node);
   if(match) return match;
   for(var i=0;i<node.childNodes.length;i++) {
      match=find(node.childNodes[i],f);
      if(match) return match;
   }
}
   
function findRegionById(region_id) 
{
   if(!uim_regtab) return -1;
   for(var i=0;i<uim_regtab.length;i++) {
      if(uim_regtab[i].region_id==region_id) return i;
   }
   return -1;
}

function findRegionByRequest(request_id)
{
   if(!uim_regtab) return -1;
   for(var i=0;i<uim_regtab.length;i++) {
      if(uim_regtab[i].request_id==request_id) return i;
   }
   return -1;
}

function get_length(len,parent_len)
{
   var r;
   if(len.substr(len.length-2,2)=="px") {
      r=parseInt(len.substr(0,str.length-2));
   }
   if(len.substr(len.length-1,1)=="%") {
      r=parent_len*parseInt(len.substr(0,len.length-1))/100;
   }
   else {
      r=parseInt(len)*parent_len;
   }
   //alert(len+" of "+parent_len+"->"+r);
   return r;
}

function widget_cb(node,action,request_id)
{
   action=action.trim();

   if(action.substr(0,5)=="call ") {
      var p=action.indexOf(".");
      if(p==-1) return;

      var q=action.indexOf("(",p);
      if(q==-1) return;

      var r=action.indexOf(")",q);
      if(r==-1) return;

      var name=     action.substr(5,p-5).trim();
      var funcname= action.substr(p+1,q-p-1).trim();
      var param=    action.substr(q+1,r-q-1).trim();

      if(name=="document") {
         alert("call-1 "+name+"."+funcname+"("+param+")");
      }
      else if(name=="cursor") {
         alert("call-2 "+name+"."+funcname+"("+param+")");
      }
      else if(name=="active") {
         alert("call-3 "+name+"."+funcname+"("+param+")");
      }
      else if(name=="focus") {
         alert("call-4 "+name+"."+funcname+"("+param+")");
      }
      else {
         var input=document.getElementsByName(name)[0];
         if(input) {
            if(input.tagName=="SELECT") {
               if(funcname=="up") {
                  if(input.selectedIndex>0) input.selectedIndex--;
               }
               else if(funcname=="down") {
                  if(input.selectedIndex+1<input.length) input.selectedIndex++;
               }
               else if(funcname=="action") {
                  var selected=input.options[input.selectedIndex];
                  if(selected) {
                     var act=selected.getAttribute("action");
                     if(act) widget_cb(selected,act,request_id);
                  }
               }
            }
            else alert("call-5 "+name+"."+funcname+"("+param+")"+input.tagName);
         }
      }
   }
   else if(action.substr(0,7)=="return ") {
      //alert("return"); 
      send_result(request_id,{result:parseInt(action.substr(7))});      
   } 
   else if(action.substr(0,5)=="load ") {
      //alert("load");
      send_result(request_id,{result:-103,url:action.substr(5)}); 
   }
   else {  
      //alert("action='"+action+"'"); 
      send_result(request_id,{result:-102}); 
   }
    
}

function kbd_start(url)
{

   var insertChar=function(myValue) { 
      var myField=document.activeElement

      // special handling for return and abort, since these are not inserted into input fields 
      // but must be processed for accesskey handling
      if(myValue==String.fromCharCode(13) || myValue==String.fromCharCode(27)) {
         forall(document,function(n){
               if(n.accessKey==myValue) {
                  if(n.action) n.action();
               }
            });            
      }
 
      if(myField.input) {
         if(myValue==String.fromCharCode(8)) myField.input("bs");
         else myField.input(myValue);
         return;
      }

      //IE support 
      if (document.selection) { 
         myField.focus(); 
         var sel = document.selection.createRange(); 
         sel.moveStart('character',-myField.value.length)
         var startPos=sel.text.length

         sel = document.selection.createRange(); 
         if(myValue==String.fromCharCode(8)) {
            if(sel.text.length==0) sel.moveStart('character',-1);
            sel.text=""
               startPos=(startPos?startPos-1:0);
         } else {
            sel.text = myValue;
            startPos+=myValue.length
         }
         setTimeout(function(){ myField.focus();myField.setSelectionRange(startPos,startPos)},0);
      } 
      //Mozilla/Firefox/Netscape 7+ support 
      else if (myField.selectionStart || myField.selectionStart == '0') { 
         var startPos = myField.selectionStart; 
         var endPos = myField.selectionEnd; 
         if(myValue==String.fromCharCode(8)) {
            if(startPos==endPos && startPos>0) startPos--;
            myField.value = myField.value.substring(0, startPos)+ myField.value.substring(endPos, myField.value.length); 
            myField.selectionStart=myField.selectionEnd=startPos
               } else {
            myField.value = myField.value.substring(0, startPos)+ myValue+ myField.value.substring(endPos, myField.value.length); 
            myField.selectionStart=myField.selectionEnd=startPos+myValue.length
         }
         setTimeout(function(){ myField.focus()},0);
      } 
      else { 
         myField.value += myValue; 
      } 
   } 


   var xmlhttp=new XMLHttpRequest();
   xmlhttp.onreadystatechange=function() {
      if(xmlhttp.readyState==4 && xmlhttp.status==200) {
         // re-start keyboard poll
         setTimeout(function(){kbd_start(url)},0);
         var obj=JSON.parse(xmlhttp.responseText)
         if(!obj.key || obj.key<=0) return;

         insertChar(String.fromCharCode(obj.key));
         restart_timeout();
      }
   }
   xmlhttp.open('GET',url,true);
   xmlhttp.send();
}



function sigcap_start(node,localrender)
{
   // determine position of canvas
   var x=node.clientLeft;
   var y=node.clientTop;
   for(var n=node;n;n=n.offsetParent) {
      x+=n.offsetLeft-n.scrollLeft;
      y+=n.offsetTop-n.scrollTop;
   }     

   var request={ action: "start", left: x, top: y, width: node.clientWidth, height:node.clientHeight };

   var xmlhttp=new XMLHttpRequest();
   xmlhttp.onreadystatechange=function() {
      //console.log("state="+xmlhttp.readyState+" status="+xmlhttp.status);
      if(xmlhttp.readyState==4){
         if(xmlhttp.status==200) {
            //console.log("reply: "+xmlhttp.responseText);
            var obj=JSON.parse(xmlhttp.responseText)
            if(obj.result==0) uim_sigcap_name=node.name;
         }
         else localrender();
      }
   }
   xmlhttp.open('GET',uim_url+'sig?id='+uim_session_id,true);
   xmlhttp.open('POST',uim_url+'sig?id='+uim_session_id,true);
   xmlhttp.send(JSON.stringify(request));
}
  
function sigcap_end(request_id, result)
{

   var request={ action: result.result>=0?"end":"cancel", output:uim_sigcap_output }
   var xmlhttp=new XMLHttpRequest();
   xmlhttp.onreadystatechange=function() {
      if(xmlhttp.readyState==4) {
         if(xmlhttp.status==200) {
            //console.log("sigcapreply: "+xmlhttp.responseText);
            var obj=JSON.parse(xmlhttp.responseText)
            if(obj.filename) {
               result.value=result.value || new Object;
               result.value[uim_sigcap_name]=obj.filename;
            }
            //console.log("sigcap_end: "+JSON.stringify(result));
         }
         send_result(request_id, result);
         uim_sigcap_name="";
      }
   }
   xmlhttp.open('GET',uim_url+'sig?id='+uim_session_id,true);
   xmlhttp.open('POST',uim_url+'sig?id='+uim_session_id,true);
   xmlhttp.send(JSON.stringify(request));
}

function restart_timeout() {
   //console.log("restart_timeout");
   if(!uim_regtab) return;
   for(var i=0;i<uim_regtab.length;i++) {
      var region=uim_regtab[i];

      if(region.timeout_handle!=null) {
         clearTimeout(region.timeout_handle);
         region.timeout_handle=setTimeout(region.timeout_func,region.timeout_val);
      }

      var regnode=document.getElementById("region-"+region.region_id);
      forall(regnode,function(n) {
            if(n.timeout_handle!=null) {
               clearTimeout(n.timeout_handle);
               n.timeout_handle=setTimeout(n.timeout_func,n.timeout_val);
            }
         });
   }
}

function addEvent(node,type,func) 
{
   if(node.addEventListener) {
      node.addEventListener(type,func);
   }
   else if(node.attachEvent) {
      node.attachEvent("on"+type,func);
   }
   else node["on"+type]=func;
}

function app_start(url)
{
   uim_url=url;

   addEvent(document,"mousedown",function(e) {
         e=e||event; 
         uim_mbutton[e.button]=1; 
         uim_click_cnt++; 
         restart_timeout();
      });

   addEvent(document,"mouseup",function(e) {
         e=e||event; 
         uim_mbutton[e.button]=0; 
         uim_click_cnt++; 
         restart_timeout();
      });

   addEvent(document,"keydown",function(e) {
         restart_timeout();
      });

   // set up default region layout

   uim_regtab=new Array();
   var mainwin=document.getElementById("guimain");
   uim_regtab[0]=new Object();
   uim_regtab[0].region_id=0;
   
   mainwin.innerHTML="<div id='region-0' style='"
      +"position:absolute;"
      +"left:0px;"
      +"top:0px;"
      +"width:"+mainwin.clientWidth+"px;"
      +"height:"+mainwin.clientHeight+"px;'></div>";

   long_poll(url);
}

function long_poll(url) 
{

   var xmlhttp=new XMLHttpRequest();
   xmlhttp.onreadystatechange=function() {
      //log("state "+xmlhttp.readyState+" status "+xmlhttp.status)
      if(xmlhttp.readyState==4 && xmlhttp.status==200) {
         
         if(xmlhttp.responseText=="") {
                     // re-start long poll
            setTimeout(function(){long_poll(uim_url)},0); 
            return;
         }

         //log(xmlhttp.responseText);

         var obj=JSON.parse(xmlhttp.responseText)
         var request_id=obj.request_id;
         
         if(obj.action=="context") {
            uim_session_id=obj.session;
            kbd_start(uim_url+'key?id='+uim_session_id);
         }
         else if(obj.action=="layout") {
            var i;
            var has_timeout=0;
   
            // clear old regions
            if(uim_regtab) {
               for(i=0;i<uim_regtab.length;i++) {
                  if(uim_regtab[i].reply_pending) send_result(request_id,{result:-8});
               }
            }
            uim_regtab=new Array();
            var mainwin=document.getElementById("guimain");

            mainwin.innerHTML="";

            // create new region elements
            var uireg=obj.region;

            for(i=0;i<uireg.length;i++) {
               uim_regtab[i]=new Object();
               uim_regtab[i].region_id=uireg[i].id;
               
               var x0=uireg[i].left;
               var y0=uireg[i].top;
               var x1=uireg[i].right;
               var y1=uireg[i].bottom;
                              
               if(x0<0) x0+=mainwin.clientWidth;
               if(y0<0) y0+=mainwin.clientHeight;
               if(x1<0) x1+=mainwin.clientWidth;
               if(y1<0) y1+=mainwin.clientHeight;
               
               mainwin.innerHTML+="<div id='region-"+uireg[i].id+"' style='"
                  +"position:absolute;"
                  +"left:"+x0+"px;"
                  +"top:"+y0+"px;"
                  +"width:"+(x1-x0+1)+"px;"
                  +"height:"+(y1-y0+1)+"px;'></div>";
            }
            send_result(request_id,{result:0});
            // re-start long poll
            setTimeout(function(){long_poll(uim_url)},0); 
            return
         }
         else if(obj.action=="cancel") {
            var reg_id=obj.reg_id;

            var reg=findRegionById(reg_id);
            if(reg>=0) {
               // in case a dialog is displayed cancel it.
               var region=uim_regtab[reg];           
               if(region.reply_pending) {
                  send_result(region.request_id,{result:-8});
               }
            }
            // re-start long poll
            setTimeout(function(){long_poll(uim_url)},0); 
            return
         }
         else if(obj.action=="form" || obj.action=="pincheck") {
            var reg_id=obj.reg_id;

            var reg=findRegionById(reg_id);
            if(reg<0) {
               send_result(request_id,{result:-9});
               // re-start long poll
               setTimeout(function(){long_poll(uim_url)},0); 
               return;
            }
            var region=uim_regtab[reg];
            
            if(region.reply_pending) {
               send_result(region.request_id,{result:-8});
            }

            // create dialog
            regnode=document.getElementById("region-"+reg_id);
            regnode.style.fontFamily=obj.default_font;
            regnode.style.fontSize=obj.default_font_size+"px";
            regnode.innerHTML=obj.text;

            // fix layout of elements that should fill 100% of a table cell
            forall(regnode,function(n) {
                  if(!n.parentNode || !n.offsetParent || !n.getAttribute) return;
                  if(n.nodeName=="INPUT") return;
                  if(n.parentNode.nodeName=="TD") {
                     var t=n;
                     var pad=1;
                     while(t && t.nodeName!="TABLE") t=t.parentNode;
                     if(t) pad=t.cellPadding || 1;

                     if(n.style && n.style.width=="100%") {
                        n.style.width="1px";
                        n.style.width=n.offsetParent.clientWidth-2*pad+"px";
                     }

                     if(n.style && n.style.height=="100%") {
                        n.style.height="1px";
                        n.style.height=n.offsetParent.clientHeight-2*pad+"px";
                     }
                  }
               })



            // fill in initial values
            if(obj.value) {
               forall(regnode,
                  function(n) {
                     if(!n.name) return;
                     if(n.nodeName=="INPUT" && n.type=="checkbox") {
                        if(obj.value[n.name]) n.checked=1;
                     } 
                     else if(n.nodeName=="INPUT" && n.type=="radio") {
                        if(obj.value[n.name]==n.value) n.checked=1;
                     }
                     else {
                        n.value=obj.value[n.name];
                     }
                  }
               );
            }

            // set up input fields
            forall(regnode,
               function(n) {
                  if(n.nodeName!="INPUT" || !n.getAttribute) return;
                  var type=n.getAttribute("type");

                  if(type=="number") {
                     setFixedPointInput(n,
                                        n.getAttribute("precision"),
                                        obj.decimal_separator,
                                        obj.thousands_separator);
                  }
                  else if(type=="mask") {
                     setMaskedInput(n,
                                    n.getAttribute("mask"),
                                    n.getAttribute("allowed_chars"),
                                    obj.keymap);
                  }
                  else if(type=="password") {
                     setPasswordInput(n,
                                      n.getAttribute("allowed_chars"),
                                      obj.password_char,
                                      obj.keymap);
                  }
                  else if(type=="pin") { // pin input
                     setPinInput(n, 
                                 uim_url+'pin?id='+uim_session_id,
                                 obj.password_char,
                                 function() {
                                    var action=n.getAttribute("action");
                                    if(!action) return;
                                    widget_cb(n,action,request_id);
                                 });

                  }
                  else if(type=="signature") {
                     // substitute <input> with <canvas> so that we can do some drawing
                     var c=document.createElement("canvas");
                     var output=n.getAttribute("output");
                     var capture=(!output || output=="raw");

                     c.width= get_length(n.style.width, n.offsetParent.clientWidth)-1;
                     c.height=get_length(n.style.height,n.offsetParent.clientHeight)-8;
                     c.style.backgroundColor=n.style.backgroundColor;
                     c.name=n.name;
                     c.value=new Array();

                     n.parentNode.replaceChild(c,n);

                     // depending on "output" and presence of "sigcap" service install callbacks for local rendering                     
                     var activate_local_rendering=function() {
                        var gc=c.getContext("2d");
                        gc.strokeStyle="#000000";
                        gc.lineWidth=1;
                        gc.lineCap="round";
                        gc.lineJoin="round";


                        // determine mouse position within the canvas
                        var getPos=function(evt,node) {
                           evt=evt||event;
                           var x=node.clientLeft; //0;
                           var y=node.clientTop; // 0;
                           for(;node;node=node.offsetParent) {
                              x+=node.offsetLeft-node.scrollLeft;
                              y+=node.offsetTop-node.scrollTop;
                           }     
                           return { x: evt.clientX-(x-document.documentElement.scrollLeft),
                                    y: evt.clientY-(y-document.documentElement.scrollTop)};
                        }

                        is_separator=function(n) {
                           return n.x==-1 && n.y==-1;
                        }

                        var curclick=-1;
                        add_stroke=function(x,y) {
                           if(c.value.length==0 || is_separator(c.value[c.value.length-1])) {
                              if(x==-1 && y==-1) return; // discard since there is already a separator
                              gc.moveTo(x,y);
                           } 
                           else if(x>=0 && y>=0) {
                              gc.lineTo(x,y);
                              gc.stroke();
                           }
                           c.value[c.value.length]={x:x,y:y};     

                           if(c.value.length>=3) {
                              // compress data: eliminate points if the area of the triangle p1-p2-p3 is below some threshold
                              // and angle p1-p2-p3 is <90 degrees
                              var p1=c.value[c.value.length-3];
                              var p2=c.value[c.value.length-2];
                              var p3=c.value[c.value.length-1];
                                          
                              if(!is_separator(p1) && !is_separator(p2) && !is_separator(p3)) {
                                 var t=-(p2.y-p1.y)*(p3.x-p1.x)+(p2.x-p1.x)*(p3.y-p1.y);
                                 var t2=(p2.x-p1.x)*(p3.x-p2.x)+(p2.y-p1.y)*(p3.y-p2.y);
                                 if(t2>0 && -2<t && t<2) {
                                    c.value.splice(c.value.length-2,1);
                                 }
                              }
                           }
                        }

                        stroke_cb=function(e,sep) {
                           e=e||event;
                           if(curclick!=uim_click_cnt) {
                              add_stroke(-1,-1);
                              curclick=uim_click_cnt;
                           }
                           if(uim_mbutton[0]) {
                              var pos=getPos(e,c);
                              add_stroke(pos.x,pos.y);
                              if(sep) add_stroke(-1,-1);
                              e.preventDefault();
                           }
                        }

                        addEvent(c,"mousemove",function(e) { stroke_cb(e,0); });
                        addEvent(c,"mouseup",  function(e) { stroke_cb(e,1); if(!capture) c.value=new Array(); });
                     }

                     if(!output || output=="raw") activate_local_rendering();
                     else {
                        uim_sigcap_output=output;
                        sigcap_start(c,activate_local_rendering);
                     }

                  }
                  else if(type=="timeout") {
                     has_timeout=0;
                     n.style.visibility="hidden";
                     var tout=1000*parseInt(n.value);
                     var l=n.value.length;
                     if(l>2 && n.value.substr(l-2,2)=="ms") tout/=1000;

                     n.timeout_val=tout;
                     n.timeout_func=function() {
                                    var action=n.getAttribute("action");
                                    if(!action) return;
                                    widget_cb(n,action,request_id);
                     }
                     n.timeout_handle=setTimeout(n.timeout_func,n.timeout_val);
                  }
                  else if(type=="text") { // text input
                     setTextInput(n,
                                  n.getAttribute("allowed_chars"),
                                  obj.keymap);
                  }
               }
            );

            // set initial focus
            var i=find(regnode,function(n) { if(n.nodeName=="INPUT") return n; });
            if(i) i.focus();

            // fill in callbacks for processing "action"
            var displayonly=1
            forall(regnode,
               function(n) {        

                  if(n.nodeName=="SELECT") {
                     // IE does not support setting addEvent() on <option>
                     // therefore, add events to the <select> node
                     var i;
                     for(i=0;i<n.options.length;i++) {
                        var action=n.options[i].getAttribute("action");
                        if(action) break;
                     }
                     if(i>=n.options.length) return;

                     addEvent(n,"click",function() {
                           var option=n.options[n.selectedIndex]
                           var action=option.getAttribute("action");
                           if(!action) return;
                           widget_cb(option,action,request_id);
                        });
                     return;
                  }

                  if(!n.getAttribute) return;
                  var action=n.getAttribute("action");
                  if(!action) return;
                  
                  if(n.nodeName=="INPUT") {
                     addEvent(n,"keyup",function(e) {
                           e = e || window.event;
                           if (e.keyCode == 13) widget_cb(n,action,request_id);
                        });                     
                     displayonly=0;
                  } 
                  else if(n.nodeName=="BUTTON") {
                     displayonly=0;
                     addEvent(n,"click",function() {
                           widget_cb(n,action,request_id);
                        });
                     n.action=function(){
                        widget_cb(n,action,request_id);
                     }
                  }
               }
            );

            if(displayonly) {
               send_result(request_id,{result:0});
               region.reply_pending=0;
               region.heartbeat=null;
            } else {

               // install global timeout 
               if(!has_timeout && obj.timeout!=null && obj.timeout>=0) {
                  region.timeout_val=obj.timeout;
                  region.timeout_func=function(){send_result(request_id,{result:-3})};
                  region.timeout_handle=setTimeout(region.timeout_func,region.timeout_val);
               }

               region.request_id=request_id;
               region.reply_pending=1;
               region.heartbeat=setInterval(function(){send_result(request_id,{result:-100},1)},5000);
            }
         }
         else if(obj.action=="redirect") {            
            send_result(request_id,{result:0});
            setTimeout(function(){location=obj.url;},0);
         }
         // re-start long poll
         setTimeout(function(){long_poll(uim_url)},0); 
      }
   }
   if(!uim_session_id) {
      var p=location.href.indexOf("?");
      var param=(p>=0?location.href.substr(p):"");
      xmlhttp.open('GET',uim_url+param,true);
   } else {
      xmlhttp.open('GET',uim_url+'?id='+uim_session_id,true);
   }
   xmlhttp.send();
}

function send_result(request_id,result,heartbeat)
{
   if(heartbeat!=1) {
      if(uim_sigcap_name!="") {
         sigcap_end(request_id,result); // sigcap_end will call send_result after stopping signature capture
         return;
      }

      var r=findRegionByRequest(request_id);
      if(r>=0) {
         var region=uim_regtab[r];
         var regnode=document.getElementById("region-"+region.region_id);

         result.value=result.value || new Object();
         forall(regnode,function(n){
               if(n.name) {                  
                  if(n.nodeName=="INPUT" && n.type=="checkbox") {
                     result.value[n.name]=(n.checked?1:0);
                  } 
                  else if(n.nodeName=="INPUT" && n.type=="radio") {
                     if(result.value[n.name]==null) result.value[n.name]="";
                     if(n.checked) result.value[n.name]=n.value;
                  } 
                  else if(n.nodeName!="CANVAS" || result.value[n.name]==null) {
                     result.value[n.name]=n.backing_value || n.value;
                  }
               }
               if(n.timeout_handle) { // stop timeout
                  clearTimeout(n.timeout_handle);
                  n.timeout_handle=null;
               }
            });

         // stop global timeout
         if(region.timeout_handle) clearTimeout(region.timeout_handle);

         regnode.innerHTML="";

         if(!region.reply_pending) return; // only the first reply will be considered
         region.reply_pending=0;
         if(region.heartbeat) clearInterval(region.heartbeat);
         region.heartbeat=null;
      }
      //console.log("send_result: "+JSON.stringify(result));
   }
   result.request_id=request_id;
 
   var xmlhttp=new XMLHttpRequest();
   xmlhttp.onreadystatechange=function(){/* nothing to be done */}
   xmlhttp.open('POST',uim_url+'?id='+uim_session_id,true);
   xmlhttp.send(JSON.stringify(result));
}

