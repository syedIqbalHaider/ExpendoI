
#include "gui/gui.h"
#include <string>
#include <string.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include "gui/jsobject.h"

#ifdef _WIN32
#include <windows.h>
#define sleep(x) Sleep(1000*(x))
#define usleep(x) Sleep((x)/1000)
#endif

#include <sys/types.h>
#include <sys/timeb.h>
#ifdef linux
#   include <unistd.h>
#   include <dlfcn.h>
#endif

#ifdef _WIN32
#   define EMV_PIN 0x0A

// windows emulation for clock_gettime()
#ifndef CLOCK_MONOTONIC
#  define CLOCK_MONOTONIC 0 // dummy
#endif
static int clock_gettime(int clk_id, struct timespec *res)
{
   struct _timeb tb;
   _ftime(&tb);
   res->tv_sec=long(tb.time);
   res->tv_nsec=long(tb.millitm*1000000);
   return 0;
}

#elif defined _VOS
#   include <svcsec.h>
#elif defined _VRXEVO
#   include <svc.h>
#   include <svc_sec.h>
// EMV_PIN not available in svc_sec.h on Verix
#   define EMV_PIN 0x0A
#   include <unistd.h>
#   include "vrx/vrx_printf.h"

static void verix_init()
{
   // try to enable fast free() on Verix, which implies a much better performance !!!
#ifdef FREE_TYPE_NO_VALIDATE  /* setFree() only available since SDK 3.8.1 providing define FREE_TYPE_NO_VALIDATE and setFree().
                               * Not enough to check _SYS_VERSION only, since this will throw compiler error for older SDKs (missing setFree()) */
   if(  (_SYS_VERSION>=0x301)       // SDK version
      &&(_syslib_version()>=0x301)) // OS version
   {
      setFree(FREE_TYPE_NO_VALIDATE);
   }
#endif

   // activate the capacitive touch keypad (e.g. on VX600, e315, e335 ...)
   int hasCapacitiveKeypad;
   vfigui::uiGetPropertyInt(vfigui::UI_DEVICE_HAS_CAP_TOUCH_KEYPAD,&hasCapacitiveKeypad);
   if(hasCapacitiveKeypad) {
      int handle=open(DEV_COM1E,0);
      // activate the keypad
      if(handle<0 || iap_control_function(handle,IAP_CONTROL_KEYPAD_WAKE)<0) {
         vfigui::uiDisplay("Not able to activate keypad, exit.<br>");
         exit(1);
      }
      close(handle);
   }
}

#else
#   define EMV_PIN 0x0A
#endif

//using namespace std;
using namespace vfigui;


class Condition {
   pthread_mutex_t mutex;
   pthread_cond_t cond;

   Condition(const Condition &o);
   void operator=(const Condition &o);

 public:
   
   Condition();
   ~Condition();
   void lock()   { pthread_mutex_lock(&mutex); }
   bool trylock() { return (pthread_mutex_trylock(&mutex)==0); }
   void unlock() { pthread_mutex_unlock(&mutex); }
   bool wait(int tout=-1);
   void signal() { pthread_cond_signal(&cond); }
};

Condition::Condition()
{
   pthread_mutex_init(&mutex,0);

   pthread_condattr_t attr;
   pthread_condattr_init(&attr);
#ifndef _WIN32 // recently no clock_gettime() with mode CLOCK_MONOTONIC on windows :-(
   pthread_condattr_setclock(&attr,CLOCK_MONOTONIC); // use monotonic clock for timeout handling
#endif
   pthread_cond_init(&cond,&attr);
   pthread_condattr_destroy(&attr);
}

Condition::~Condition()
{
   pthread_cond_destroy(&cond);
   pthread_mutex_destroy(&mutex);
}

bool Condition::wait(int tout)
{
   if(tout<0) {
      pthread_cond_wait(&cond,&mutex);
      return true;
   }

   struct timespec ts;
   clock_gettime(CLOCK_MONOTONIC,&ts);
   ts.tv_sec+=tout/1000;
   ts.tv_nsec+=tout%1000*1000000;
   if(ts.tv_nsec>1000000000) { ts.tv_sec++; ts.tv_nsec-=1000000000; }

   return pthread_cond_timedwait(&cond,&mutex,&ts)==0;
}


enum STATUSBAR_MODE { NONE, STATUSBAR,KEYBOARD,STATUSBAR_KEYBOARD};

struct Statusbar {
   STATUSBAR_MODE statusbar_mode;
   bool thread_running;
   Condition cond;
   
   Statusbar() { statusbar_mode=NONE; thread_running=true; }
   ~Statusbar() {}
};

bool cb_cancel(void *) { return false; }

#define STATUSBAR_DISABLED(s) (s->statusbar_mode!=STATUSBAR && s->statusbar_mode!=STATUSBAR_KEYBOARD)

void *statusbar(void *p)
{
   bool first=true;
   Statusbar *status=(Statusbar *)p;

   status->cond.lock();
   while(1) {

      if(!status->thread_running) break;

      if(STATUSBAR_DISABLED(status)) {
         status->cond.wait();
         continue; // continue/terminate this thread
      }

      struct timeb tp;
      ftime(&tp);
      if(!first) {
         status->cond.wait(1000-tp.millitm); // wait for beginning of next second
         if(STATUSBAR_DISABLED(status)) continue;  // sleep/terminate this thread
         tp.time++;
      }
      first=false;
      char buf[64];
      strftime(buf,sizeof(buf),"%d.%m.%Y - %H:%M:%S",localtime(&tp.time));

      //struct timeb start,stop;
      //ftime(&start);
      uiDisplay(1,uiPrint("<div style='background-color:black;color:white;height:100%%;text-align:center;overflow:hidden'>%S</div>",buf));
      //ftime(&stop);
      //fprintf(stderr,"display_time: %d ms\n",int((stop.time-start.time)*1000+(int)stop.millitm-(int)start.millitm));
   }
   status->cond.unlock();
   return 0;
}

void async_callback(void *data, int request_id)
{
   Condition *cond=(Condition *)data;
   // example callback for invoking ui*Wait inside callback
   fprintf(stderr,"callback invoked\n");
   if(uiConfirmWait(request_id,0)!=UI_ERR_WAIT_TIMEOUT) {
      cond->lock();
      cond->signal(); // signal main application that the dialog has finished
      cond->unlock();
   }
}


void uiMain(int argc, char *argv[])
{
#ifdef _VRXEVO
   verix_init();
#endif

   uiSetPropertyInt(UI_PROP_CIRCULAR_MENU,1);
   uiSetPropertyInt(UI_PROP_TOUCH_ACTION_BEEP,1);

   // read some device properties
   int hasTouch;
   uiGetPropertyInt(UI_DEVICE_SUPPORTS_TOUCH, &hasTouch);
   int hasColorDisplay;
   uiGetPropertyInt(UI_DEVICE_SUPPORTS_COLOR_DISPLAY, &hasColorDisplay);
   int disp_w=-1,disp_h=-1;
   uiGetPropertyInt(UI_DEVICE_WIDTH, &disp_w);
   uiGetPropertyInt(UI_DEVICE_HEIGHT, &disp_h);

   // display platform info for 1 second
   {
     char rp[1024]="";
     uiGetPropertyString(UI_PROP_RESOURCE_PATH,rp,sizeof(rp));
     char *p=rp+strlen(rp)-1;
     while(p>rp && *p!='/' && *p!='\\') p--;
     if(p>rp) p++; else p=(char *)"none";
     uiDisplay(uiPrint("Platform: %s<br>Display: %dx%d %s<br>Resource path:<br>%s<br>Version: %s<br>",p,disp_w,disp_h,hasColorDisplay?"color":"b/w",rp,uiLibVersion()));
     sleep(1);
   }

   // select image folder
   const char *images="images";             // colored images
   if(!hasColorDisplay) images="images_bw"; // b/w images

   Statusbar status;
   pthread_t statusthread;
   uiThreadCreate(&statusthread,0,statusbar,&status);

   struct UIMenuEntry menu[]={      {"Display",0,0},
                                    {"Confirm",1,0},
                                    {"Confirm/Cancel",2,0},
                                    {"Input Name",3,0},
                                    {"Input Amount",4,0},
                                    {"Input Password",5,0},
                                    {"Input Mask",6,0},
                                    {"Input Multi",7,0},
                                    {"Input Signature",23,0},
                                    {"Input Signature PNG",24,0},
                                    {"Input Signature ScrShot",25,0},
                                    {"YesNo",8,0},
                                    {"YesNoCancel",9,0},
                                    {"Form Input",10,0},
                                    {"HTML file",11,0},
                                    {"HTML sequence",12,0},
                                    {"Menu",13,0},
                                    {"PIN Input",14,0},
                                    {"PIN Check",15,0},
                                    {"Prefix=de-",16,0},
                                    {"Prefix=",17,0},
                                    {"Display Table",18,0},
                                    {"Font",19,0},
                                    {"Callback",20,0},
                                    {"Statusbar/Keyboard",21,0},
                                    {"Timeout demo",22,0},
                                    {"Grid",26,0},
                                    {"QR-Code",27,0},
                                    {"Multilanguage",28,0},
                                    {"Default dialog",29,0},
                                    {"Cancel dialog",30,0},
                                    {"Comparisons",31,0}
#ifdef _VOS
                                    ,{"Redirect to wde.html",100,0}
                                    ,{"Webgui: iframe",101,0}
                                    ,{"Play Mx9-Video loop",102,0}
                                    ,{"Play Mx9-Video controls",103,0}
#endif
#ifdef _VRXEVO
                                    ,{"Obtain console",200,0}
                                    ,{"Wait event",201,0}
#endif
                                    ,{"Performance test",300,0}
                                   };


   for(unsigned i=0;i<sizeof(menu)/sizeof(menu[0]);i++) {
      if(!hasTouch) { // disable features for devices not having a touch display
         if(menu[i].value==23) menu[i].options|=UI_MENU_DISABLED;
         if(menu[i].value==24) menu[i].options|=UI_MENU_DISABLED;
         if(menu[i].value==25) menu[i].options|=UI_MENU_DISABLED;
         if(menu[i].value==26) menu[i].options|=UI_MENU_DISABLED;
         if(menu[i].value==100) menu[i].options|=UI_MENU_DISABLED;
         if(menu[i].value==101) menu[i].options|=UI_MENU_DISABLED;
      }
      if(!hasColorDisplay) { // disable features for devices not having a color display
         if(menu[i].value==27) menu[i].options|=UI_MENU_DISABLED;
         if(menu[i].value==102) menu[i].options|=UI_MENU_DISABLED;
         if(menu[i].value==103) menu[i].options|=UI_MENU_DISABLED;
      }
      if(disp_h<64) { // disable features for devices having a small display
         if(menu[i].value==10) menu[i].options|=UI_MENU_DISABLED;
         if(menu[i].value==18) menu[i].options|=UI_MENU_DISABLED;
      }
   }

   if(argc!=1) {
      string s="Parameters:<br>";
      for(int i=1;i<argc;i++) s+=string(argv[i])+"<br>";
      
      uiDisplay(s);
      sleep(3);
   }

   int r,s=0;
   while(1) {
         s=uiMenu("mainmenu","Menu",menu,sizeof(menu)/sizeof(menu[0]),s>=0?s:0);
         if(s==UI_ERR_CONNECTION_LOST) break;

      switch(s) {
         /********************* Display *********************/
         case 0: 
            uiDisplay("bgimage","<h4>Display</h4>Hello world!");
            sleep(2);
            uiDisplay("Line1<br>Line2<br>Line3<br>Line4<br>Line5<br>Line6<br>Line7<br>Line8<br>Line9<br>Line10<br>"
                      "Line1<br>Line2<br>Line3<br>Line4<br>Line5<br>Line6<br>Line7<br>Line8<br>Line9<br>Line10");
            sleep(2);
            break;

         /********************* Confirm *********************/
         case 1:{
            int request_id;
            Condition cond;
            cond.lock();
            request_id=uiConfirmAsync(0,"confirm","<h4>Confirmation</h4>Please press OK",async_callback,&cond);
            if(request_id>=0) {
               while(!cond.wait(1000)) printf("waiting\n");
            }
            cond.unlock();
            break;
         }

         /********************* Confirm/Cancel *********************/
         case 2: {
            string result_text="<h4>Confirmation</h4>";
            string result_image;

            r=uiConfirm("confirm-cancel","<h4>Confirmation</h4>Please press OK or Cancel");

            if(r==UI_ERR_OK) {
               result_text+="You have pressed OK";
               result_image="<center><img src='../"+string(images)+"/confirmation.png' style='margin:auto;display:block'></center>";
            }
            else {
               result_text+="You have pressed Cancel";
               result_image=string("<center><img src='../"+string(images)+"/stop.png' style='margin:auto;display:block'></center>");
            }

            // for small screens, display text in image in 2 steps
            if(disp_h>=64) {
               uiDisplay(result_text+"<br>"+result_image);
               sleep(2);
            } else {
               uiDisplay(result_text);
               sleep(2);
               uiDisplay(result_image);
               sleep(2);
            }
            break;
         }

         /********************* Input Name *********************/
         case 3: {
            int request_id;
            vector<string> value(1);
            uiSetPropertyInt(UI_PROP_UPDATE_EVENTS,1);
            value[0]="Test";
            r=request_id=uiInputAsync("input",value,"<h4>Input</h4>Please enter your name <input type='text' maxlength='20'>Footer");
            if(request_id>=0) {
               while((r=uiInputWait(request_id,value,1000))==UI_ERR_WAIT_TIMEOUT || r==UI_ERR_UPDATE_EVENT) 
               {
                  if(r==UI_ERR_UPDATE_EVENT) {
                     printf("update: %s\n",value[0].c_str());
                  } else {
                     printf("waiting\n"); 
                  }
               }
            }
         
            if(r==UI_ERR_OK) {
               uiDisplay(string("<h4>Input</h4>Your name is:<br><span style='color:#ff0000'>")+value[0]);
               sleep(2);
            } 
            else {
               uiDisplay(uiPrint("Result: %d<br>",r));
               sleep(2);
            }
            uiSetPropertyInt(UI_PROP_UPDATE_EVENTS,0);
            break;
         }

         /********************* Input Amount *********************/
         case 4: {
            vector<string> value(1);
            value[0]="123";
            r=uiInput("input",value,"<h4>Input</h4>Please enter Amount <input type='number' precision='2'>Footer");
            
            uiDisplay(uiPrint("Result: %d<br>Input Amount:<br><span style='color:#ff0000'>%s</span>",r,value[0].c_str()));
            sleep(2);

            break;
         }

         /********************* Input Password *********************/
         case 5: {
            uiSetPropertyInt(UI_PROP_PASSWORD_CHAR,0x2022);
            uiSetPropertyInt(UI_PROP_PASSWORD_SHOW_CHAR,1000); // show last entered digit for 1000ms
            vector<string> value(1);
            r=uiInput("input",value,"<h4>Input</h4>Please enter Password ('0'-'9') <input size='6' type='password' maxlength='6' allowed_chars='0123456789'>Footer");
            if(r==UI_ERR_OK) {
               uiDisplay(string("<h4>Input</h4>Password was:<br><span style='color:#ff0000'>")+value[0]);
               sleep(2);
            }
            break;
         }

         /********************* Input Mask *********************/
         case 6: {
            vector<string> value(1);
            value[0]="ab";
            r=uiInput("input",value,"<h4>Input</h4>Please enter your string value <input type='mask' mask='*** ***'>Footer");
            if(r==UI_ERR_OK) {
               uiDisplay(string("<h4>Input</h4>Your value is:<br><span style='color:#ff0000'>")+value[0]);
               sleep(2);
            }
            break;
         }

         /********************* Input Multi *********************/
         case 7: {
            vector<string> value(2);
            value[0]="123";
            value[1]="test";
            r=uiInput("input",value,
                      "Please enter amount <input type='number' precision='2'><br>"
                      "Please enter name <input type='text' maxlength='20'>");
            if(r==UI_ERR_OK) {
               uiConfirm("confirm","<h4>Input</h4>Result:<br><span style='color:#ff0000'>"+value[0]+"<br>"+value[1]);
            }
            break;
         }

         /********************* YesNo *********************/
         case 8: 
            r=uiConfirm("yesno","<h4>YesNo</h4>Select Yes or No");
            if(r==1) uiDisplay("<h4>YesNo</h4>You have selected Yes");
            else     uiDisplay("<h4>YesNo</h4>You have selected No");
            sleep(2);
            break;


         /********************* YesNoCancel *********************/
         case 9:
            r=uiConfirm("yesno-cancel","<h4>YesNo</h4>Select Yes/No/Cancel");
            if(r==1)      uiDisplay("<h4>YesNo</h4>You have selected Yes");
            else if(r==0) uiDisplay("<h4>YesNo</h4>You have selected No");
            else          uiDisplay("<h4>YesNo</h4>You have selected Cancel");
            sleep(2);
            break;

         /********************* Form Input *********************/
         case 10: {
            string html;
            map<string,string> value;

            value["name"]="Testuser";
            value["gender"]="male";
            value["age"]="1";
            value["subst"]="&<>\"'`!@$%()=+-{}[]";

            if(!hasTouch){
               html="Name: <input accesskey='&#13;' action='return 0' type='text' name='name'><br>"
                    "<table>"
                    "<tr><td>Gender:</td><td><input accesskey='&#133;' type='radio' name='gender' value='male'> male (M0)</td></tr>"
                    "<tr><td></td><td><input accesskey='&#134;' type='radio' name='gender' value='female'> female (M1)</td></tr>"
                    "</table>"
                    "Substituted value: <?var subst?>"
                    "<table>"
                    "<tr><td>18+</td><td><input accesskey='&#136;' type='checkbox' name='age'>(M2)</td></tr>"
                    "<tr><td>married</td><td><input accesskey='&#137;' type='checkbox' name='married'>(M3)</td></tr>"
                    "</table>"
                    "<button action='return 0' accesskey='&#13;' style='visibility:hidden'></button>"  // add a hidden ok button
                    "<button action='return -1' accesskey='&#27;' style='visibility:hidden'></button>"  // add a hidden cancel button
                    "<button accesskey='&#129;' action='call document.up()' style='visibility:hidden'></button>" // add hidden buttons for scrolling the page
                    "<button accesskey='&#132;' action='call document.down()' style='visibility:hidden'></button>"
                    "<button accesskey='&#138;' action='call focus.previous()' style='visibility:hidden'></button>" // add hidden buttons for focus selection
                    "<button accesskey='&#139;' action='call focus.next()' style='visibility:hidden'></button>";
            }else{
               html="Name: <input accesskey='&#13;' action='return 0' type='text' name='name'><br>"
                    "<table>"
                    "<tr><td>Gender:</td><td><input type='radio' name='gender' value='male'> male</td></tr>"
                    "<tr><td></td><td><input type='radio' name='gender' value='female'> female</td></tr>"
                    "</table>"
                    "Substituted value: <?var subst?>"
                    "<table>"
                    "<tr><td>18+</td><td><input type='checkbox' name='age'></td></tr>"
                    "<tr><td>married</td><td><input type='checkbox' name='married'></td></tr>"
                    "</table>"
                    "<button accesskey='&#13;' action='return 0'>submit</button>" // add submit button
                    "<button accesskey='&#27;' action='return -1'>cancel</button>" // add cancel button
                    "<button accesskey='&#129;' action='call document.up()' style='visibility:hidden'></button>" // add hidden buttons for scrolling the page
                    "<button accesskey='&#132;' action='call document.down()' style='visibility:hidden'></button>";
            }

            r=uiInvoke(value,html);
               
            string v=uiPrint("Result: %d<br>",r);
            map<string,string>::iterator i;
            for(i=value.begin();i!=value.end();++i) {
               v+=uiPrint("%S=%S<br>",i->first.c_str(),i->second.c_str());
            }

            uiConfirm("confirm",v);
            break;
         }

         /********************* HTML file *********************/
         case 11: {
            map<string,string> value;

            value["input"]="some text";

            r=uiInvokeURL(value,"test.html");

            string v=uiPrint("Result: %d<br>",r);
            map<string,string>::iterator i;
            for(i=value.begin();i!=value.end();++i) {
               v+=uiPrint("%S=%S<br>",i->first.c_str(),i->second.c_str());
            }

            uiConfirm("confirm",v);
            break;
         }

         /********************* HTML sequence *********************/
         case 12: {
            map<string,string> value;

            value["value"]="Testuser";
            value["confirm_label"]="Your name is:";

            string html="<h4>Input form</h4>"
                        "Name: <input action='load confirm.html' accesskey='&#13;' type='text' name='value'><br>";

            if (!hasTouch) {
               html+="<button action='return -1' accesskey='&#27;' style='visibility:hidden'></button>";  // add a hidden cancel button
            }
            else html+="<button accesskey='&#13;' action='load confirm.html'>submit</button>" // add submit button
                       "<button accesskey='&#27;' action='return -1'>cancel</button>"; // add cancel button

            html+="<button accesskey='&#129;' action='call document.up()' style='visibility:hidden'></button>" // add hidden buttons for scrolling the page
                  "<button accesskey='&#132;' action='call document.down()' style='visibility:hidden'></button>"
                  "<button accesskey='&#130;' action='call cursor.left()' style='visibility:hidden'></button>" // add hidden buttons for moving the cursor
                  "<button accesskey='&#131;' action='call cursor.right()' style='visibility:hidden'></button>";

            r=uiInvoke(value,html);

            string v=uiPrint("Result: %d<br>",r);
            map<string,string>::iterator i;
            for(i=value.begin();i!=value.end();++i) {
               v+=uiPrint("%S=%S<br>",i->first.c_str(),i->second.c_str());
            }

            uiConfirm("confirm",v);
            break;
         }

         /********************* Menu *********************/
         case 13: {
            map<string,string> value;
            r=uiInvokeURL(value,"mainmenu.html");
            string v=uiPrint("Result: %d<br>",r);
            map<string,string>::iterator i;
            for(i=value.begin();i!=value.end();++i) {
               v+=uiPrint("%S=%S<br>",i->first.c_str(),i->second.c_str());
            }
            uiConfirm("confirm",v);
            break;
         }

         /********************* PIN Input *********************/
         case 14: {
            map<string,string> value;

            // modify this values to change default behaviour of PIN entry
            uiSetPropertyInt(UI_PROP_PASSWORD_CHAR,'*');
            uiSetPropertyInt(UI_PROP_PIN_AUTO_ENTER,0);  // 1 -> enable
            uiSetPropertyInt(UI_PROP_PIN_CLEAR_ALL,0);   // 1 -> enable
            uiSetPropertyInt(UI_PROP_PIN_BYPASS_KEY,8);  // 13 -> enter key, 8 -> clear key
            uiSetPropertyInt(UI_PROP_PIN_ALGORITHM,EMV_PIN);
            uiSetPropertyInt(UI_PROP_PIN_INTERCHAR_TIMEOUT,5000); // 5 seconds interchar timeout;

            r=uiInvoke(value,"Demo PIN input<br><br>PIN <input type='pin' name='input' minlength='4' maxlength='6' action='return 0' style='background-color:black;color:white'>");

            string v=uiPrint("Result: %d<br>",r);
            map<string,string>::iterator i;
            for(i=value.begin();i!=value.end();++i) {
               v+=uiPrint("%S=%S<br>",i->first.c_str(),i->second.c_str());
            }
            uiConfirm("confirm",v);
            break;
         }

         /********************* PIN Check *********************/
         case 15:
            uiSetPropertyInt(UI_PROP_PASSWORD_CHAR,'*');
            uiSetPropertyInt(UI_PROP_PASSWORD_SHOW_CHAR,0);
            r=uiInputPINCheck("pin-check","1234","<h4>PINCheck</h4>Guess my PIN <input type='password' maxlength='4' allowed_chars='0123456789'>Footer");
            if(r==UI_ERR_OK) uiDisplay("<h4>PINCheck</h4>The PIN was valid, you're a clairvoyant!");
            else if(r==UI_ERR_WRONG_PIN) uiDisplay("<h4>PINCheck</h4>Your entered PIN wasn't valid.");
            else uiDisplay("<h4>PINCheck</h4>Input aborted or general error");
            sleep(2);
            break;

         /********************* Prefix=de- *********************/
         case 16:
            uiSetPropertyString(UI_PROP_FILE_PREFIX,"de-");
            break;

         /********************* Prefix= *********************/
         case 17:
            uiSetPropertyString(UI_PROP_FILE_PREFIX,0);
            break;

         /********************* Display Table *********************/
         case 18: {
            JSObject l;
            l[0]("name")="item1";
            l[0]("value")=1;
            l[1]("name")="item2";
            l[1]("value")=2;
            l[2]("name")="item3";
            l[2]("value")=3;
            map<string,string> m;
            m["test"]=l.dump();

            string ok_button="<button action='return 0' accesskey='&#13;'>OK</button>";
            if(!hasTouch) ok_button="<button action='return 0' accesskey='&#13;' style='visibility:hidden'></button>";

            uiInvoke(m,"Table"
                     "<?foreach test"
                     "|(table border=1)(tr)(th)JSON(/th)(th)Name(/th)(th)Value(/th)(/tr)"
                     "|(tr)(td)[](/td)(td)[name](/td)(td)[value](/td)(/tr)"
                     "|(/table)"
                     "|Empty table(br)"
                     "?>"
                     +ok_button+
                     "<button accesskey='&#129;' action='call document.up()' style='visibility:hidden'></button>" // add hidden buttons for scrolling the page
                     "<button accesskey='&#132;' action='call document.down()' style='visibility:hidden'></button>"
                     "<button accesskey='&#130;' action='call document.left()' style='visibility:hidden'></button>" // add hidden buttons for scrolling the page
                     "<button accesskey='&#131;' action='call document.right()' style='visibility:hidden'></button>");
            m["test"]="";
            uiInvoke(m,"Table"
                     "<?foreach test"
                     "|(table border=1)(tr)(th)JSON(/th)(th)Name(/th)(th)Value(/th)(/tr)"
                     "|(tr)(td)[](/td)(td)[name](/td)(td)[value](/td)(/tr)"
                     "|(/table)"
                     "|(br)Empty table(br)"
                     "?>"
                     +ok_button+
                     "<button accesskey='&#129;' action='call document.up()' style='visibility:hidden'></button>" // add hidden buttons for scrolling the page
                     "<button accesskey='&#132;' action='call document.down()' style='visibility:hidden'></button>"
                     "<button accesskey='&#130;' action='call document.left()' style='visibility:hidden'></button>" // add hidden buttons for scrolling the page
                     "<button accesskey='&#131;' action='call document.right()' style='visibility:hidden'></button>");
            l.clear();
            l[0]="item1";
            l[1]="item2";
            l[2]="item3";
            m["test"]=l.dump();
            uiInvoke(m,"List:<br><?foreach test||[](br)|?>"+ok_button);
            break;
         }

         /********************* Font *********************/
         case 19:
         {
            while(1) {
               int i;
               char font[256]="";
               const struct UIMenuEntry fmenu[]={{"dejavu sans mono",0,0},
                                                 {"dejavu sans"     ,1,0},
                                                 {"dejavu serif"    ,2,0},
                                                 {"tiresias pcfont" ,3,0},
                                                 {"fiery turk exxx" ,4,0}};
               int menusize=(sizeof(fmenu)/sizeof(fmenu[0]));

               uiGetPropertyString(UI_PROP_DEFAULT_FONT,font,sizeof(font));

               for(i=0;i<menusize;i++) if(fmenu[i].text==font) break;

               i=uiMenu("menu","<h4>Select font</h4>",fmenu,menusize,i>=menusize?0:i);
               if(i>=0) {
                  // configure new font
                  uiSetPropertyString(UI_PROP_DEFAULT_FONT,fmenu[i].text.c_str());

                  int fontsize;
                  const struct UIMenuEntry smenu[]={{"8",8,0},
                                                    {"9",9,0},
                                                    {"10",10,0},
                                                    {"11",11,0},
                                                    {"12",12,0},
                                                    {"13",13,0},
                                                    {"14",14,0},
                                                    {"16",16,0},
                                                    {"18",18,0},
                                                    {"20",20,0},
                                                    {"24",24,0},
                                                    {"28",28,0}};
                  int menusize=(sizeof(smenu)/sizeof(smenu[0]));

                  uiGetPropertyInt(UI_PROP_DEFAULT_FONT_SIZE,&fontsize);
                  for(i=0;i<menusize;i++) if(smenu[i].value==fontsize) break;
                  i=uiMenu("menu","<h4>Select font size</h4>",smenu,menusize,i>=menusize?0:smenu[i].value);
                  if(i>=0) uiSetPropertyInt(UI_PROP_DEFAULT_FONT_SIZE,i);
                  else if(i==UI_ERR_BACK) continue;
               }
               break;
            }
            break;
         }

         /********************* Callback *********************/
         case 20:
            r=uiConfirm("confirm","Dummy text",cb_cancel);
            uiConfirm("confirm",uiPrint("Cancelled dialog result: %d<br>",r));
            break;

         /********************* Statusbar/Keyboard *********************/
         case 21: {
            int tout;
            int max_opts=4;
            if(!hasTouch) max_opts=2;
            uiGetPropertyInt(UI_PROP_TIMEOUT,&tout);
            uiSetPropertyInt(UI_PROP_TIMEOUT,-1);
            status.statusbar_mode=(STATUSBAR_MODE)((status.statusbar_mode+1)%max_opts);
            switch(status.statusbar_mode) {
               case NONE: 
                  uiLayout("layout"); 
                  break;
               case STATUSBAR:
                  uiLayout("layout-status");
                  break;
               case KEYBOARD: 
                  uiLayout("layout-kbd"); 
                  uiInvokeURLDetached(2,"kbd.html"); 
                  break;
               case STATUSBAR_KEYBOARD: 
                  uiLayout("layout-status-kbd"); 
                  uiInvokeURLDetached(2,"kbd.html"); 
                  break;
            }
            status.cond.lock();
            // wake the statusbar thread
            status.cond.signal();
            status.cond.unlock();

            uiSetPropertyInt(UI_PROP_TIMEOUT,tout);
            break;
         }

         /********************* Timeout demo *********************/
         case 22: {
            uiInvokeURL("timeout0.html");
            break;
         }


         /********************* Input Signature *********************/
         case 23:
         case 24:
         case 25: {
            map<string,string> value;
            const char *sightml="";

            switch(s) {
               default:
               case 23: sightml="signature.html";  break; // raw (default)
               case 24: sightml="signature2.html"; break; // PNG
               case 25: sightml="signature3.html"; break; // screenshot
            }

            if((r=uiInvokeURL(value,sightml))!=UI_ERR_OK) {
               uiDisplay(uiPrint("Result: %d<br>",r));
               sleep(3);
               break;
            }

            JSObject v;
            v.load(value["signature"]);
            if(v.isArray()) {
               for(unsigned i=0;i<v.size();i++) {
                  printf("%d %d\n",int(v[i]("x")),int(v[i]("y")));
               }
            }
            else {
               string sigfile=value["signature"];
               if(sigfile=="") {
                  uiDisplay("No signature was given");
                  sleep(3);
                  break;
               }
               FILE *fp=fopen(sigfile.c_str(),"rb");
               if(!fp) {
                  uiDisplay(uiPrint("Couldn't open file %s!<br>",sigfile.c_str()));
                  sleep(3);
                  break;
               }
               fclose(fp);
               uiDisplay(uiPrint("file=%s<br>",value["signature"].c_str()));
               sleep(3);
            }
            break;
         }

         /********************* Grid *********************/
         case 26: {
            map<string,string> value;
            uiInvokeURL(value,"grid.html");
            break;
         }

         /********************* QR-Code *********************/
         case 27:
            uiInvokeURL("qrcode.html");
            break;

         /********************* Multilanguage *********************/
         case 28: {
            string lookup_en, lookup_de;
            map<string,string> value;
            value["sample"]="multilang.html";
            value["hl"]="headline"; // variable name for HTML catalog lookup
            printf("Catalog '%s' => ",uiGetCatalog().c_str());
            uiSetCatalog("en.ctlg");
            printf("'%s'\n",uiGetCatalog().c_str());
            lookup_en=uiGetText("text3","not found");
            uiInvokeURL(value,"multilang.html");
            uiSetCatalog("de.ctlg");
            lookup_de=uiGetText("text3","not found");
            uiInvokeURL(value,"multilang.html");
            uiSetCatalog(""); // unload
            uiDisplay(uiPrint("Lookup results:\nlookup_en=%s\nlookup_de=%s",lookup_en.c_str(),lookup_de.c_str()));
            sleep(3);
            break;
         }

         /********************* Default dialog *********************/
         case 29:
            uiInvokeURL("default.html");
            break;

         /********************* Cancel dialog *********************/
         case 30: {
            int request_id;
            request_id=uiConfirmAsync("confirm","<h4>Cancel dialog</h4>Press OK or dialog is cancelled after 5s.");
            if(request_id>=0) {
               int r;
               if((r=uiConfirmWait(request_id,5000))==UI_ERR_WAIT_TIMEOUT) {
                  r=uiInvokeCancel(request_id);
                  uiDisplay(uiPrint("Dialog cancelled<br>Result: %d<br>",r));
               }
               else {
                  uiDisplay(uiPrint("Dialog confirmed<br>Result: %d<br>",r));
               }
               sleep(3);
            }
            break;
         }

         /********************* Comparisons *********************/
         case 31:
            uiInvokeURL("compare.html");
            break;
#ifdef _VOS

         /********************* Redirect to wde.html (WebGUI only) *********************/
         case 100: {
            int r=uiRedirectURL("../wde.html");
            fprintf(stderr,"uiRedirectURL()=%d\n",r);
            if(r==UI_ERR_OK) return; // we are done, close current session
            break; // failed to redirect
         }

         /********************* Webgui: iframe *********************/
         case 101:
            uiInvokeURL("iframe.html");
            break;

         /********************* Play Mx9-Video loop *********************/
         case 102: {
            uiInvokeURL("video.html");
            const vector<UIErrorEntry> &err=uiErrorList();
            string s;
            for(unsigned i=0;i<err.size();i++) {
               s+=uiPrint("%d %S<br>",err[i].type, err[i].name.c_str());
            }
            if(s.length()) {
               uiDisplay(s);
               sleep(10);
            }
            break;
         }

         /********************* Play Mx9-Video controls *********************/
         case 103:
            uiInvokeURL("video2.html");
            break;
#endif
#ifdef _VRXEVO
         /********************* Obtain console  *********************/
         case 200: {
            uiObtainConsole();
            clrscr();
            write_at("Obtained console!",17, 0, 0);
            sleep(2);
            clrscr();
            uiReleaseConsole();
            uiDisplay("Released console!");
            sleep(2);
            break;
         }

         /********************* Wait event *********************/
         case 201: {
            int request_id;
            // enable Verix event notification
            uiSetVerixNotification(get_task_id(),1);
            request_id=uiConfirmAsync(0,"confirm","<h4>Wait for result</h4>Please press OK");
            if(request_id>=0) {
               while(1) {
                  wait_evt(EVT_USER);
                  if(uiConfirmWait(request_id,0)!=UI_ERR_WAIT_TIMEOUT) break;
               }
               uiDisplay(uiPrint("user_bits=%d<br>",read_user_event()));
            }
            // disable Verix event notification
            uiSetVerixNotification(0,0);
            sleep(3);
            break;
         }
#endif

         /********************* Performance test *********************/
         case 300: { // performance test
            int timeout,test=0;
            const int loop=20;
#define RUN_TEST(cmd) do { struct timeb tp_start, tp_stop; \
                           unsigned long current_ms,total_ms=0; \
                           int i=0; \
                           test++; \
                           for(i=0; i<loop; i++) { \
                             ftime(&tp_start); \
                             r=UI_ERR_FAIL; \
                             cmd; \
                             if(r!=UI_ERR_OK && r!=UI_ERR_TIMEOUT) break; \
                             ftime(&tp_stop); \
                             current_ms=(unsigned long)((long)(tp_stop.time-tp_start.time)*1000+((long)tp_stop.millitm-(long)tp_start.millitm)); \
                             total_ms+=current_ms; \
                           } \
                           if(i==loop) uiDisplay(uiPrint("Test %d<br>Loop: %d<br>Total time: %ld ms<br>Time per Dialog: %ld ms",test,i,total_ms,total_ms/(unsigned long)i)); \
                           sleep(3); \
                         } while(0)

            uiGetPropertyInt(UI_PROP_TIMEOUT, &timeout);
            // set timeout to 0 to force dialog to return directly
            uiSetPropertyInt(UI_PROP_TIMEOUT, 0);

            // TEST 1: Display a text (without input)
            RUN_TEST(r=uiDisplay(uiPrint("uiDisplay(): %d",i+1)));
            // TEST 2: same as TEST 1, but use asynchronous function
            int req_id;
            RUN_TEST(req_id=uiDisplayAsync(uiPrint("uiDisplayAsync(): %d",i+1));if(req_id>=0)r=uiDisplayWait(req_id));
            // TEST 3: same as TEST 1, but add an input control (hidden button)
            const string button="<button action='return 0' accesskey='&#13;' style='visibility:hidden'></button>";
            RUN_TEST(r=uiDisplay(uiPrint("uiDisplay()/Button: %d",i+1)+button));
            // TEST 4: same as TEST 1, but add 7 input control (hidden buttons)
            const string buttons="<button accesskey='&#13;' style='visibility:hidden' action='return 0'></button>"
                                 "<button accesskey='&#129;' action='call document.up()' style='visibility:hidden'></button>"
                                 "<button accesskey='&#132;' action='call document.down()' style='visibility:hidden'></button>"
                                 "<button accesskey='&#130;' action='call document.left()' style='visibility:hidden'></button>"
                                 "<button accesskey='&#131;' action='call document.right()' style='visibility:hidden'></button>"
                                 "<button accesskey='&#138;' action='call focus.previous()' style='visibility:hidden'></button>"
                                 "<button accesskey='&#139;' action='call focus.next()' style='visibility:hidden'></button>";
            RUN_TEST(r=uiDisplay(uiPrint("uiDisplay()/7Buttons: %d",i+1)+buttons));
            // TEST 5: same as TEST 4, but use a template to display the text with 7 hidden buttons
            RUN_TEST(r=uiDisplay("confirm",uiPrint("uiDisplay()/Template: %d",i+1)));

            // restore timeout
            uiSetPropertyInt(UI_PROP_TIMEOUT,timeout);
            break;
         }
      }
   }

   status.thread_running=false;
   status.cond.lock();
   // wake the statusbar thread and force to terminate
   status.cond.signal();
   status.cond.unlock();
   pthread_join(statusthread,0);
}
