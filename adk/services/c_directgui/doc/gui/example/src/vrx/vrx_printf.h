#ifndef VRX_PRINTF
#define VRX_PRINTF

#ifdef _VRXEVO

#include <eoslog.h>
#include <stdio.h>
#include <stdarg.h>
#include <string>

#ifndef LOGSYS_OS
# define LOGSYS_OS 1
#endif

#ifndef  USE_NAMESPACE
# define USE_NAMESPACE vfigui
#endif

namespace USE_NAMESPACE {
#if 0 
} // for automatic indentation
#endif

#define printf(a,...)   vrx_fprintf(stdout,a,##__VA_ARGS__)
#define fprintf vrx_fprintf
int vrx_fprintf(FILE *stream, const char *format, ...);

#define vprintf(a,b)   vrx_vfprintf(stdout,a,b)
#define vfprintf vrx_vfprintf
int vrx_vfprintf(FILE *stream, const char *format, va_list ap);

int vrx_fprintfm(const char *module, int line, FILE *stream, const char *format, ...);
int vrx_vfprintfm(const char *module, int line, FILE *stream, const char *format, va_list ap);

extern const char    *VRX_LOG_APPNAME;
extern short          VRX_LOG_TYPE;
extern unsigned long  VRX_LOG_FILTER;
extern const char    *VRX_LOG_PORT;

} // namespace vfiprt


#ifdef VRX_LOG_TRACELIB

#  define VRX_LOG_INIT2(a,p)   namespace USE_NAMESPACE {  \
   const char    *VRX_LOG_APPNAME=a; \
   short          VRX_LOG_TYPE=0;    \
   unsigned long  VRX_LOG_FILTER=0; \
   const char    *VRX_LOG_PORT=p; }
#  define VRX_LOG_INIT(a,b,c)
namespace USE_NAMESPACE {
   void vrx_configure_tracelib(const std::string &appName, const std::string &port, unsigned char speed, int mode);
   void vrx_get_tracelib_config(std::string &appName, std::string &port, unsigned char &speed, int &mode);
}
#elif defined LOGSYS_FLAG

#  define VRX_LOG_INIT(a,b,c)   namespace USE_NAMESPACE {  \
   const char    *VRX_LOG_APPNAME=a; \
   short          VRX_LOG_TYPE=b;    \
   unsigned long  VRX_LOG_FILTER=c; \
   const char    *VRX_LOG_PORT=0; }
#  define VRX_LOG_INIT2(a,p)
namespace USE_NAMESPACE {
   void vrx_configure_logsys(const std::string &appName, short logType, unsigned long filter);
   void vrx_get_logsys_config(std::string &appName, short &logType, unsigned long &filter);
}
#else 

#  define VRX_LOG_INIT2(a,p)
#  define VRX_LOG_INIT(a,b,c)

#endif

#endif

#endif
