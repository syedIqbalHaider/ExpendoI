
#include <string>
#include <errno.h>
#include <ctype.h>

#include <ceif.h>

#include "vrx_printf.h"

//using namespace std;
using namespace USE_NAMESPACE;

static void toupper(string &s)
{
   for(string::iterator i=s.begin();i!=s.end();++i) *i=toupper(*i);
}

static string getFileExt(const char *name)
{
   string str(name);
   string ext;
   if(str.length()>4) {
     ext=str.substr(str.length()-4);
     toupper(ext);
   }
   return ext;
}

int main(int argc, char *argv[])
{
  bool bNCPEnabled=false; // network control panel for initializing network interface
  string args,all_args;
  // init printf() to be redirected to logsys
  vrx_configure_logsys("LAUNCHER",LOGSYS_OS,0x00000001L);
  
  // filter out all args not specifying a program started by launcher
  for(int i=0;i<argc;i++) { 
    if(i>0) {
      if(all_args.length()>0) all_args.append(" "); all_args.append(argv[i]);
      string current(argv[i]);
      toupper(current);
      if(!bNCPEnabled && current=="START_NCP=1") {
        bNCPEnabled=true;
        continue;
      }
      string ext=getFileExt(argv[i]);
      if(ext!=".OUT" && ext!=".VSA") {
        if(args.length()>0) args.append(" ");
        args.append(argv[i]);
        continue;
      }
    }
  }
  printf("!!!! LAUNCHER called with args=%s !!!!",all_args.c_str());
  
  if(bNCPEnabled) {
    int res=ECE_VXNCP_PIPE;
    bool ncp_active=false;
    int console=open(DEV_CONSOLE, 0);
    if(console<0) {
      printf("!!!! Failed to open console for NCP (%d) !!!!",console);
      return -1;
    }
    // NCP event loop
    while (1) {
      if(!ncp_active) res=ceActivateNCP();
      printf("!!!! ceActivateNCP() result=%d !!!!",res);
      if(res==ECE_VXNCP_PIPE) {
        SVC_WAIT(500);
        continue;
      }
      ncp_active=true;  
      long lEvents = wait_evt(EVT_ACTIVATE);
      if(lEvents&EVT_ACTIVATE) break;
    }
    close(console);
  }
  
  // launch processes
  for(int i=0;i<argc;i++) { 
    if(i>0) {
      string ext=getFileExt(argv[i]);
      if(ext==".OUT" || ext==".VSA") {
        int task_id=run(argv[i],args.c_str());
        if(task_id>=0) printf("!!!! LAUNCHED %s (task=%d) with args=%s !!!!",argv[i],task_id,args.c_str());
        else           printf("!!!! FAILED to launch %s with args=%s (errno=%d) !!!!",argv[i],args.c_str(),errno);
      }
    }
  }
  return 0;
}
