
function decodeKeymap(keymap,allowed_chars)
{
   if(keymap==null || keymap=="") {
      keymap="0=0\n1=1\n2=2\n3=3\n4=4\n5=5\n6=6\n7=7\n8=8\n9=9\n";
   }

   var n=keymap.split("\n");
   var result=new Object;
   result.map=new Array;
   for(var i=0;i<n.length;i++) {
      if(n[i].length<2 || n[i].substr(1,1)!='=') continue;
      var key=n[i].substr(0,1);
      var chars=decodeURI(n[i].substr(2));
      if(allowed_chars.length) {
         for(var j=0;j<chars.length;j++) {
            if(allowed_chars.indexOf(chars[j])<0) {
               chars=chars.substr(0,j)+chars.substr(j+1);
               j--;
            }
         }
      }
      result.map[key]=chars;
   }

   result.timestamp=0;
   result.lastkey="";
   result.lastchar="";

   result.getkey=function(key) {
      var r=new Object;
      if(key.length==0) {
         r.key=key;
         r.del=false;
         return r;
      }
      if(key.length>1) { // special key like "bs", "left", etc.
         result.timestamp=0;
         result.lastkey="";
         result.lastchar="";
         r.key=key;
         r.del=false;
         return r;
      }

      var lk=result.lastkey;
      result.lastkey=key;
      var d=new Date;
      var ts=d.getTime();
      var charlist=result.map[key] || key;
      
      if(key==lk && charlist.length>1 && ts-result.timestamp<1000) {
         var c=charlist[(charlist.indexOf(result.lastchar)+1)%charlist.length];
         r.key=c;
         r.del=true;
      } else {
         r.key=charlist.substr(0,1);
         r.del=false;
      }
      result.lastchar=r.key;
      result.timestamp=ts;
      return r;
   }

   return result;
}

function setMaskedInput(node,inputmask,allowed_chars,keymap_string)
{
   // check input values
   var maxlen = 0;
   if(inputmask==null) inputmask = "**********";
   for(var i=0;i<inputmask.length;i++) if(inputmask[i]=='*') maxlen++;   
   if(allowed_chars==null) allowed_chars = "";
   
   var keymap=decodeKeymap(keymap_string,allowed_chars);
      
   var formatMasked= function(cursor_index)
   {
      var v="";
      var i,j=0;
      var cursor=0;
      var cupdate=true;

      for(i=0;i<inputmask.length;i++) {
         if(inputmask[i]=='*') {
            if(j<node.backing_value.length) {
               v+=node.backing_value[j++];
            } else {
               cupdate=false;
               v+="_";
            }
         } else {
            v+=inputmask[i];
         }
         if(   cupdate 
            && (cursor_index==-1 || j<=cursor_index))
         {
            cursor=v.length;
         }
      }

      node.value=v;
      node.selectionStart=node.selectionEnd=cursor;
   }
   
   var addEvent=function(type,func) 
   {
      if(node.addEventListener) {
         node.addEventListener(type,func)
      }
      else if(node.attachEvent) {
         node.attachEvent("on"+type,func)
      }
      else node["on"+type]=func
   }
   
   var getKey=function(event) {
      event=event||window.event
      var keycode=event.which
      if(event.type=="keydown") {
         if (keycode==8) return "bs"
         if (keycode==46) return "del"
         if (keycode==37) return "left"
         if (keycode==39) return "right"
         if (keycode==38) return "up"
         if (keycode==40) return "down"
         if (keycode==36) return "home"
         if (keycode==35) return "end"
         return "" // ignore all other keys on keydown
      }
      else {
         if(keycode<32) return ""
      }
      return String.fromCharCode(keycode)
   }
   
   /** transforms real cursor position (from input mask)  
    *  to index that is used for the backing value */
   var pos2idx=function(pos)
   {
      var i=0
      if(pos>inputmask.length) pos=inputmask.length;
      while(pos--) {
        if(inputmask[pos]=='*') i++;
      }
      if(i>node.backing_value.length) i=node.backing_value.length;
      return i;
   }
   
   var disableEvent = function(evt) {
      // stop event propagation
      if(evt && evt.preventDefault) evt.preventDefault()
      else evt.returnValue=false // IE

      return false
   }
   
   var disableSelection = function (target) {
      if (typeof target.onselectstart!="undefined") //For IE 
         target.onselectstart=function(){return false}
      else if (typeof target.style.MozUserSelect!="undefined") //For Firefox
         target.style.MozUserSelect="none";
      else //All other route (For Opera)
         target.onmousedown=function(){target.focus(); return false}
      target.style.cursor = "default"
   }
   
   disableSelection(node)
   node.backing_value=node.value
   formatMasked(-1);
   
   var input=function(c) {
      if(c=="") return true;
      var cursor_index = pos2idx(node.selectionStart)
      var k=keymap.getkey(c);
      c=k.key;

      if(c=="del") {
         if(cursor_index<node.backing_value.length) {
           // delete character after the cursor
           node.backing_value=node.backing_value.substring(0,cursor_index)+node.backing_value.substring(cursor_index+1);
         }
      }
      else if(c=="bs" || k.del) {
         if(cursor_index>0) {
           // delete character before the cursor
           node.backing_value=node.backing_value.substring(0,cursor_index-1)+node.backing_value.substring(cursor_index);
           cursor_index--;
         }
      }
      else if(c=="left") {
         if(cursor_index) cursor_index--;
      }
      else if(c=="right") {
         if(cursor_index<node.backing_value.length) cursor_index++;
      }
      else if(c=="home" || c=="up") {
         cursor_index=0;
      }
      else if(c=="end" ||c=="down") {
         cursor_index=node.backing_value.length;
      }

      if(c.length==1) {
         if(maxlen<0 || node.backing_value.length<maxlen) {
            // insert character at cursor position
            node.backing_value=node.backing_value.substring(0,cursor_index)+c+node.backing_value.substring(cursor_index);
            cursor_index++
         }
      }
      
      formatMasked(cursor_index);
      return !c.length;
   }
   
   var update=function(evt) {
      var c=getKey(evt)
      if(!input(c)) {
         // stop event propagation
         if(evt && evt.preventDefault) evt.preventDefault();
         else evt.returnValue=false; // IE        
         return false;
      }            
      return true;
   }            

   node.input=input;

   addEvent("focus",function(){formatMasked(-1);});
   addEvent("keypress",update);
   addEvent("keydown",update);
   addEvent("paste",disableEvent);
}

function setPasswordInput(node,  allowed_chars, password_char,keymap_string) {
   // check input values
   var maxlen=node.maxLength;
   node.type="text"
   if (allowed_chars == null) allowed_chars = "";
   if (password_char == null) password_char = "*"; else password_char=String.fromCharCode(password_char);

   var keymap=decodeKeymap(keymap_string,allowed_chars);

   var formatPassword = function(cursor_index) {
       var v = "";
       var i = 0
       for (i = 0; i < node.backing_value.length; i++) {
           v += password_char
       }
       node.value = v;
       if(cursor_index==-1) cursor_index=node.value.length;
       node.selectionStart=node.selectionEnd=cursor_index;
   }

   var addEvent = function(type, func) {
       if (node.addEventListener) {
           node.addEventListener(type, func)
       }
       else if (node.attachEvent) {
           node.attachEvent("on" + type, func)
       }
       else node["on" + type] = func
   }

   var getKey = function(event) {
       event = event || window.event
       var keycode = event.which
       if (event.type == "keydown") {
           if (keycode == 8) return "bs"
           if (keycode == 46) return "del"
           if (keycode == 37) return "left"
           if (keycode == 39) return "right"
           if (keycode==38) return "up"
           if (keycode==40) return "down"
           if (keycode == 36) return "home"
           if (keycode == 35) return "end"
           return "" // ignore all other keys on keydown
       }
       else {
           if (keycode < 32) return ""
       }
       return String.fromCharCode(keycode)
   }

   var disableEvent = function(evt) {
       // stop event propagation
       if (evt && evt.preventDefault) evt.preventDefault()
       else evt.returnValue = false // IE

       return false
   }

   var disableSelection = function(target) {
      if (typeof target.onselectstart != "undefined") //For IE 
         target.onselectstart = function() { return false }
      else if (typeof target.style.MozUserSelect != "undefined") //For Firefox
         target.style.MozUserSelect = "none";
      else //All other route (For Opera)
         target.onmousedown = function() { target.focus();return false }
      target.style.cursor = "default"
   }

   disableSelection(node)
   node.backing_value = node.value
   formatPassword();

   var input=function(c) {
      if(c=="") return true;
      var cursor_index = node.selectionStart
      var k=keymap.getkey(c);
      c=k.key;

      if (c == "del") {
         if (cursor_index < node.backing_value.length) {
            // delete character after the cursor
            node.backing_value = node.backing_value.substring(0, cursor_index) + node.backing_value.substring(cursor_index + 1);
         }
      }
      else if (c == "bs" || k.del) {
         if (cursor_index > 0) {
            // delete character before the cursor
            node.backing_value = node.backing_value.substring(0, cursor_index - 1) + node.backing_value.substring(cursor_index);
            cursor_index--;
         }
      }
      else if(c=="left") {
         if(cursor_index) cursor_index--;
      }
      else if(c=="right") {
         if(cursor_index<node.backing_value.length) cursor_index++;
      }
      else if(c=="home" || c=="up") {
         cursor_index=0;
      }
      else if(c=="end" ||c=="down") {
         cursor_index=node.backing_value.length;
      }

      if(c.length == 1) {
         if(maxlen<0 || node.backing_value.length < maxlen) {
            // insert character at cursor position
            node.backing_value = node.backing_value.substring(0, cursor_index) + c + node.backing_value.substring(cursor_index);
            cursor_index++
         }
      }

      formatPassword(cursor_index);
      return !c.length
   }

   var update=function(evt) {
      var c=getKey(evt)
      if(!input(c)) {
         // stop event propagation
         if(evt && evt.preventDefault) evt.preventDefault();
         else evt.returnValue=false; // IE        
         return false;
      }            
      return true;
   }            

   node.input=input;

   addEvent("focus", function(){formatPassword(-1);});
   addEvent("keypress", update);
   addEvent("keydown", update);
   addEvent("paste", disableEvent);
}

function setTextInput(node, allowed_chars,keymap_string) {
   // check input values
   var maxlen=node.maxLength;
   if (allowed_chars == null) allowed_chars = "";

   var keymap=decodeKeymap(keymap_string,allowed_chars);

   var formatText = function(cursor_index) {
      node.value =  node.backing_value
      if(cursor_index==-1) cursor_index=node.value.length;
      node.selectionStart=node.selectionEnd=cursor_index;
   }

   var addEvent = function(type, func) {
       if (node.addEventListener) {
           node.addEventListener(type, func)
       }
       else if (node.attachEvent) {
           node.attachEvent("on" + type, func)
       }
       else node["on" + type] = func
   }

   var getKey = function(event) {
       event = event || window.event
       var keycode = event.which
       if (event.type == "keydown") {
           if (keycode == 8) return "bs"
           if (keycode == 46) return "del"
           if (keycode == 37) return "left"
           if (keycode == 39) return "right"
           if (keycode==38) return "up"
           if (keycode==40) return "down"
           if (keycode == 36) return "home"
           if (keycode == 35) return "end"
           return "" // ignore all other keys on keydown
       }
       else {
           if (keycode < 32) return ""
       }
       return String.fromCharCode(keycode)
   }

   var disableEvent = function(evt) {
       // stop event propagation
       if (evt && evt.preventDefault) evt.preventDefault()
       else evt.returnValue = false // IE

       return false
   }

   node.backing_value = node.value
   formatText(-1)

   var input=function(c) {
      if(c=="") return true;
      var cursor_index = node.selectionStart;
      var k=keymap.getkey(c);
      c=k.key;

      if(k.del) node.selectionEnd=node.selectionStart; // discard selection

      if (c == "del") {
         if(node.selectionStart!=node.selectionEnd) {
            node.backing_value=node.backing_value.substring(0,node.selectionStart)+node.backing_value.substr(node.selectionEnd);
         } 
         else if (cursor_index < node.backing_value.length) {
            // delete character after the cursor
            node.backing_value = node.backing_value.substring(0, cursor_index) + node.backing_value.substring(cursor_index + 1);
         }
      }
      else if (c == "bs" || k.del) {
         if(node.selectionStart!=node.selectionEnd) {
            node.backing_value=node.backing_value.substring(0,node.selectionStart)+node.backing_value.substr(node.selectionEnd);
         } 
         if (cursor_index > 0) {
            // delete character before the cursor
            node.backing_value = node.backing_value.substring(0, cursor_index - 1) + node.backing_value.substring(cursor_index);
            cursor_index--
         }
      }
      else if(c=="left") {
         if (cursor_index) cursor_index--;
      }
      else if(c=="right") {
         if (cursor_index<node.backing_value.length) cursor_index++;
      }
      else if(c=="home" || c=="up") {
         cursor_index=0;
      }
      else if(c=="end" ||c=="down") {
         cursor_index=node.backing_value.length;
      }

      if(c.length == 1) {
         if(node.selectionStart!=node.selectionEnd) {
            node.backing_value=node.backing_value.substring(0,node.selectionStart)+node.backing_value.substr(node.selectionEnd);
         }          
         if(maxlen<0 || node.backing_value.length < maxlen) {
            // insert character at cursor position
            node.backing_value = node.backing_value.substring(0, cursor_index) + c + node.backing_value.substring(cursor_index);
            cursor_index++
         }
      }

      formatText(cursor_index);
      return !c.length
   }

   var update=function(evt) {
      var c=getKey(evt)
      if(!input(c)) {
         // stop event propagation
         if(evt && evt.preventDefault) evt.preventDefault();
         else evt.returnValue=false; // IE        
         return false;
      }
      return true;
   }            

   node.input=input;

   addEvent("focus", function(){formatText(node.selectionStart);});
   addEvent("keypress", update);
   addEvent("keydown", update);
   addEvent("paste", disableEvent);
}

function setFixedPointInput(node,precision,sep,tsep)
{
   // set default values
   precision=precision||2
   sep=sep||","
   tsep=tsep||"."
   var maxlen=node.maxLength
   if(maxlen<=0) maxlen=10

   var formatFixed=function()
   {
      var val=node.backing_value
      while(val.length>3 && val[0]=="0") val=val.substr(1)
      while(val.length<3) val="0"+val
      var r
      r=sep+val.substr(val.length-2,2)
      val=val.substr(0,val.length-2)
      
      while(val.length>3) {
         r=tsep+val.substr(val.length-3,3)+r
         val=val.substr(0,val.length-3)
      }   
      r=val+r
      node.value=r
      node.selectionStart=node.selectionEnd=node.value.length;
   }

   var addEvent=function(type,func) 
   {
      if(node.addEventListener) {
         node.addEventListener(type,func)
      }
      else if(node.attachEvent) {
         node.attachEvent("on"+type,func)
      }
      else node["on"+type]=func
   }


   var getKey=function(event) {
      event=event||window.event
      var keycode=event.which
      if(event.type=="keydown") {
         if(keycode==8) return "bs"
         if(keycode==46) return "del"
         if(keycode==37) return "left"
         if(keycode==39) return "right"
         return "" // ignore all other keys on keydown
      }
      else {
         if(keycode<32) return ""
      }
      return String.fromCharCode(keycode)
   }
   
   var disableEvent = function(evt) {
      // stop event propagation
      if(evt && evt.preventDefault) evt.preventDefault()
      else evt.returnValue=false // IE

      return false
   }
   
   var disableSelection = function (target) {
      if (typeof target.onselectstart!="undefined") //For IE 
         target.onselectstart=function(){return false}
      else if (typeof target.style.MozUserSelect!="undefined") //For Firefox
         target.style.MozUserSelect="none";
      else //All other route (For Opera)
         target.onmousedown=function(){target.focus();return false}
      target.style.cursor = "default"
   }
   
   disableSelection(node)
   node.backing_value=node.value
   node.type="text";
   node.style.textAlign="right";
   formatFixed()
   
   var input=function(c) {
      if(c=="") return true;
      while(node.backing_value[0]=="0") node.backing_value=node.backing_value.substr(1)
      if(c=="bs" || c=="del") {
         node.backing_value=node.backing_value.substr(0,node.backing_value.length-1);
      } 
      else if(c.length==1 && c>="0" && c<="9" && node.backing_value.length<maxlen ) {
         node.backing_value=node.backing_value+c
      }

      
      formatFixed();
      return !c.length
   }

   var update=function(evt) {
      var c=getKey(evt)
      if(!input(c)) {
         // stop event propagation
         if(evt && evt.preventDefault) evt.preventDefault();
         else evt.returnValue=false; // IE        
         return false;
      }            
      return true;
   }            

   node.input=input;
   
   addEvent("focus",formatFixed);
   addEvent("keypress",update);
   addEvent("keydown",update);
   addEvent("paste",disableEvent);
}


function setPinInput(node,url,password_char,callback)
{
   node.type="text";
   node.readOnly=true;
   node.backing_value=0;

   var poll_pin=function() {
      //console.log("poll_pin");                                           
      var xmlhttp=new XMLHttpRequest();
      xmlhttp.onreadystatechange=function() {
         if(xmlhttp.readyState==4 && xmlhttp.status==200) {
            var obj=JSON.parse(xmlhttp.responseText)
            //console.log(JSON.stringify(obj));                                                                   
            if(obj.pin==null) return;

            if(isNaN(obj.pin)) {
               if(obj.pin!="ok") node.backing_value=obj.pin;
               callback();
            }
            else {
               // re-start PIN polling
               setTimeout(poll_pin,200);
            
               node.backing_value=obj.pin;
               var v="";
               for(var i=0;i<obj.pin;i++) v+=String.fromCharCode(password_char);
               node.value=v;
            }
         }
      }
      xmlhttp.open('GET',url,true);
      xmlhttp.send();
   }
   poll_pin();
}
