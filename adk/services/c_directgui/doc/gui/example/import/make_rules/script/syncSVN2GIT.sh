#!/usr/bin/env bash

# In future this script should be callable by a chron-job and do all steps to synch subversion and Git repositories. (Jens W.)

# I have two remotes:
# 1. "Subversion", its imported with "git svn clone". see http://www.cocoanetics.com/2013/03/moving-from-svn-to-git/
# 2. "origin", which is stash

# known limitations:
# - errors, if tags are 'moved' (delete tag and create new tag with same name pointing to another commit)

#################################################################################


git checkout develop
git -c diff.mnemonicprefix=false -c core.quotepath=false svn rebase

bFoundNewTag=0

#create git-tags from svn-tags, which are interpreted as branches by git-svn
git for-each-ref refs/remotes/svn/tags | cut -d / -f 5- |
while read tagname
do
    echo "found $refs/remotes/svn/tags/$tagname"
    git tag -a "$tagname" -m"say farewell to SVN" "refs/remotes/svn/tags/$tagname"
    
    git branch -r -d "svn/tags/$tagname"
    
    git push origin ":refs/heads/tags/$tagname"
    git push origin tag "$tagname"
    
    bFoundNewTag=1
    lastTag="$tagname"    
done

#push changed develop to stash:
git push origin develop:develop

#set master to latest tag
if [ $bFoundNewTag -eq 1 ];
then
    git checkout master
    git merge $lastTag
    git push origin master:master
fi


