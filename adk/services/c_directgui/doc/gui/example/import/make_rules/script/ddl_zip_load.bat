@echo OFF

setlocal enabledelayedexpansion

set clearall=
set port=
set zip=
set add_para=

SET script_path=%~dp0

REM handle parameters
:Loop
if "%1"=="" goto continue
   set para=%~1
   if "!para:~0,2!"=="--" (
     if "!para:~0,11!"=="--clearall" (
       REM '=' is ignored by WinShell and stored to next parameter
       set clearall=%~2
       if "!clearall!"=="" goto usage
       shift
     ) else (
     if "!para:~0,11!"=="--ddl_port" (
       REM '=' is ignored by WinShell and stored to next parameter
       set port=%~2
       if "!port!"=="" goto usage
       shift
     ) else (
     if "!para:~0,11!"=="--add_para" (
       REM '=' is ignored by WinShell and stored to next parameter
       set add_para=%~2
       if "!add_para!"=="" goto usage
       shift
     )))
   ) else (
     if not "!zip!"=="" goto usage
     set zip=%~1
   )
shift
goto Loop
:continue

if "!zip!"=="" goto usage

REM specifiy ZIP output file extract filename from path and add prefix signed_
for %%F in (!zip!) do set zip_out=%%~nxF
set zip_out=!zip_out:~0,-4%!_signed.zip
del "!zip_out!" > NUL 2>&1

REM this is just for displaying on screen
for %%F in ("!zip!") do set zip_name=%%~nxF

title ZIP download for Verix eVo
cls
echo Install !zip_name! for Verix eVo
echo.
if "!port!"=="" (
echo Please start FST signing service and
set /p port=enter COM port number for download: 
if "!port!"=="" goto no_port_number
)

if "!clearall!"=="" (
set /p clearall=Clear all terminal files before download [y/n]? 
)
if "!clearall!"=="Y" set clearall=y

REM add FST and DDL to path
path %PATH%;"c:\Program Files (x86)\VeriFone\FST";"c:\Program Files\VeriFone\FST";%VRXSDK%\bin

set result=1

call:signfile "!zip!" "!zip_out!" result
if %result% equ 1 goto :signing_failed

echo Download files, please wait...
set clearflags=
if "!clearall!"=="y" set clearflags=-r*:*/ -rconfig.sys
set ddl_cmd=ddl -p!port! -c !clearflags! !add_para! *UNZIP=!zip_out! !zip_out!
echo !ddl_cmd!
!ddl_cmd!
if "!errorlevel!"=="1" goto :download_failed
goto end

:signfile
endlocal & set %~3=1
rd /S /Q exdir > NUL 2>&1
mkdir exdir
CScript !script_path!unzip.vbs "%~1" exdir
REM 7za.exe x "%~1" -oexdir
cd exdir
del /S *.crt > NUL 2>&1
set fst_cmd=FileSignature.exe sign.fst -nogui
echo Signing files, please wait...
echo !fst_cmd!
!fst_cmd!
REM check check signing result (OK if there are any *.crt files)
dir /b /s *.crt
if not "!errorlevel!"=="0" (
cd ..
rd /S /Q exdir > NUL 2>&1
goto:eof
)
del sign.fst > NUL 2>&1
cd ..
CScript !script_path!zip.vbs exdir "%~2"
REM 7za.exe a "%~2" exdir
if not "!errorlevel!"=="0" (
rd /S /Q exdir > NUL 2>&1
goto:eof
)
endlocal & set %~3=0
rd /S /Q exdir > NUL 2>&1
goto:eof

:no_port_number
echo Error: No port number specified!
goto end

:signing_failed
echo Error: Signing files failed!
goto end

:download_failed
echo Error: File download failed!
goto end

:usage
echo Usage: ddl_zip_load.bat [--clearall=y/n] [--ddl_port=port] [--add_para=parameter] file.zip
goto end

:end
pause
