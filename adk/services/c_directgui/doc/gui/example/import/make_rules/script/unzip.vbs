'Get command-line arguments.
Set objArgs = WScript.Arguments
'The location of the zip file.
ZipFile=objArgs(0)
'The folder the contents should be extracted to.
ExtractTo=objArgs(1)

'If the extraction location does not exist create it.
Set fso = CreateObject("Scripting.FileSystemObject")
If NOT fso.FolderExists(ExtractTo) Then
   fso.CreateFolder(ExtractTo)
End If

zip_file = fso.GetAbsolutePathName(ZipFile)
out_folder = fso.GetAbsolutePathName(ExtractTo)

'Extract the contants of the zip file.
set objShell = CreateObject("Shell.Application")

set FilesInZip=objShell.NameSpace(zip_file).items
objShell.NameSpace(out_folder).CopyHere(FilesInZip)

Set fso = Nothing
Set objShell = Nothing
