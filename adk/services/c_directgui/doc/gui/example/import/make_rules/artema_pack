artema_release_OBJDIR=./obj/$(NAME)/artema-release/
artema_debug_OBJDIR=./obj/$(NAME)/artema-debug/
artema_debug_VSECONFIG=artema-debug
artema_release_VSECONFIG=artema-release
artema_WORK_DIR ?= $(dir)
artema_APPNAME ?= appname
artema_DESTINATION ?= client
artema_GNU_TOOLS ?=$(artema_TOOLCHAIN_DIR)/CLITools
artema_PACKINGLISTBUILDER ?= ./import/sdk/artema/tools/plb.exe
artema_$(VARIANT)_HXPTARGET ?= ./export/artema$(artema_PATH_EXT)/$(VARIANT)
artema_GNU_TOOLS_BIN ?= $(artema_TOOLCHAIN_DIR)
artema_PACKID_ENDING ?= 0000
artema_PCK_NAME ?= <USERNO>
artema_APP_VERSION ?="<APP_VERSION_4DIGIT>"
artema_debug_HXPTARGET=./export/artema$(artema_PATH_EXT)/debug
artema_release_HXPTARGET=./export/artema$(artema_PATH_EXT)/release
artema_debug_APPNAME_LONG = $(artema_debug_OBJDIR)$(artema_APPNAME)
artema_release_APPNAME_LONG = $(artema_release_OBJDIR)$(artema_APPNAME)
artema_ENVNAME ?= $(artema_APPNAME)

ifeq (,$(findstring artema-debug, $(artema_$(VARIANT)_VSECONFIG)))
   artema_release_HXPNAME        = $(artema_release_APPNAME_LONG).hxp
   artema_release_HXP_MODE      ?= Genesis Auto-launch Desktop Application
   artema_release_TAG2           = "3"
   artema_release_HXPSOURCE      =$(artema_APPNAME).hxp
   # List of files to be included in the HXP file
   artema_release_APP_FILES_ges  = $(artema_APP_FILES) $(artema_release_APP_FILES)
else
   artema_debug_HXPNAME        = $(artema_debug_APPNAME_LONG)_d.hxp
   artema_debug_HXP_MODE      ?= Genesis Desktop Application
   artema_debug_TAG2           = "4"
   artema_debug_HXPSOURCE      = $(artema_APPNAME)_d.hxp
   # List of files to be included in the HXP file
   artema_debug_APP_FILES_ges  = $(artema_APP_FILES) $(artema_debug_APP_FILES)
endif

GAWK      = $(artema_GNU_TOOLS)/gawk
RM        = $(artema_GNU_TOOLS)/rm
CP        = $(artema_GNU_TOOLS)/cp
REPL_TOOL = import/sdk/artema/tools/repl_cmd_userno_version.awk
SIZETOOL  = $(artema_GNU_TOOLS)/../x86-win32/bin/arm-none-linux-gnueabi-size.exe
SDKINJECT = import/sdk/optimum/tools/SdkInject.exe

artema_debug_APP_FILES_ges ?= $(artema_debug_APPNAME_LONG)
artema_release_APP_FILES_ges ?= $(artema_release_APPNAME_LONG)
# application to start (allows to specify a run startup script)
artema_STARTUP_FILE ?= run

# destination on terminal
artema_DESTINATION ?= "$(artema_APPNAME)"

# Package identifier last number block
artema_PACKID_ENDING ?= "0000"

# binary executable
artema_debug_BINARY?=$(artema_debug_APPNAME_LONG)
artema_release_BINARY?=$(artema_release_APPNAME_LONG)
# HXP Options 
artema_HXP_OPTIONS = --nogui --newfile --silent --log "myerr1.log"
artema_HXP_OPTIONS += --mode "$(artema_$(VARIANT)_HXP_MODE)"
artema_HXP_OPTIONS += --name "$(artema_PCK_NAME)"
artema_HXP_OPTIONS += --title "$(artema_APPNAME)"
artema_HXP_OPTIONS += --version $(artema_APP_VERSION)	# <APP_VERSION_4DIGIT> or <APP_VERSION_WITH_DOT> will be substituted by repl_cmd_userno_version.awk
artema_HXP_OPTIONS += --company Hypercom
artema_HXP_OPTIONS += --destination "$(artema_DESTINATION)"
artema_HXP_OPTIONS += --main "$(artema_STARTUP_FILE)"
artema_HXP_OPTIONS += --no-auto-elfsign
artema_HXP_OPTIONS += --spec "$(artema_TCMSSPECFILE)"
artema_HXP_OPTIONS += --tcms "./import/sdk/artema/tools/tcms.cfg"
artema_HXP_OPTIONS += --toolchain_dir "$(artema_GNU_TOOLS_BIN)"
artema_HXP_OPTIONS += --no-hidden
artema_HXP_OPTIONS += --id "<USERNO>-$(artema_$(VARIANT)_TAG2)-100-$(artema_PACKID_ENDING) <APP_VERSION_WITH_DOT>"
#                 <USERNO> and <APP_VERSION_WITH_DOT> will be substituted by repl_cmd_userno_version.awk
#HXP_OPTIONS += --log $(artema_APPNAME)-plb.log

PACK_CALL = $(artema_WORK_DIR)/import/sdk/artema/tools/plb.exe $(artema_HXP_OPTIONS) $(artema_$(VARIANT)_HXPNAME) $(artema_$(VARIANT)_APP_FILES_ges) $(artema_SILENT)

########################################################
# to enforce immediate expansion first define as macro and then use $(eval $(call macro_template)) 
# note that $ needs to be duplicated for all variables but the ones that should be replaced immediately
define macro_artema_pack

$(TARGET)-$(VARIANT)-pack:
	@echo Creating HXP package...
	@echo BINARY -$(VARIANT):         $(artema_$(VARIANT)_BINARY)
	@echo APPNAME:                    $(artema_APPNAME)
	@echo VSECONFIG -$(VARIANT):      $(artema_$(VARIANT)_VSECONFIG)
	@echo APPNAME_LONG -$(VARIANT):   $(artema_$(VARIANT)_APPNAME_LONG)
	@echo HXPNAME -$(VARIANT):        $(artema_$(VARIANT)_HXPNAME)
	@echo HXPSOURCE -$(VARIANT):      $(artema_$(VARIANT)_HXPSOURCE)
	@echo TAG2 -$(VARIANT):           $(artema_$(VARIANT)_TAG2)
	@echo HXPTARGET -$(VARIANT):      $(artema_$(VARIANT)_HXPTARGET)
	@echo DESTINATION :               $(artema_DESTINATION)
	@echo ENVNAME :                   $(artema_ENVNAME)
	@echo GAWK:                       $(GAWK)
	@echo PACK_CALL:                  $(PACK_CALL)
	@echo TCMSSPECFILE:               $(artema_TCMSSPECFILE)
	@echo REPL_TOOL:                  $(REPL_TOOL)
	@echo Version:                    $(artema_APP_VERSION)

	$(GAWK) -f $(REPL_TOOL) $(PACK_CALL) $(artema_ENVNAME).env
	@echo HXP package successfully created.
	
endef

$(eval $(call macro_artema_pack))
	
	