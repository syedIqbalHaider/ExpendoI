#ifndef HEXUTIL_H
#define HEXUTIL_H

/*****************************************************************************
* Thales e-Transactions GmbH
******************************************************************************
* Internal ECR-Interface
******************************************************************************
*
* $Workfile:   hexutil.h  $                      $Date:   19 Mar 2007 10:28:54  $
* $Revision:   1.0  $                      $Modtime:   15 Mar 2007 16:00:46  $
* $Archive:   U:/ArcTRM/Terminals/archives/ARCHIVES/Headerdateien/hexutil.h-arc  $
*
*****************************************************************************/
/** @file hexutil.h
 *
 * Conversion functions Hexdump <-> Binary
 *
 * @author:      M. Meixner
 * @date Created:  09.09.2005
 */
/*****************************************************************************
*  (c) Thales e-Transactions GmbH, Bad Hersfeld
*****************************************************************************/
/*HOC************************************************************************
* REVISIONS HISTORY OF CHANGE:
*----------------------------------------------------------------------------
 $Log:   U:/ArcTRM/Terminals/archives/ARCHIVES/Headerdateien/hexutil.h-arc  $
 * 
 *    Rev 1.0   19 Mar 2007 10:28:54   Groennert
 * Initial revision.

****************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _VRXEVO
#   ifdef TLV_UTIL_EXPORT
#       define TLV_UTIL_INTERFACE __declspec(dllexport)
#   elif defined TLV_UTIL_IMPORT
#       define TLV_UTIL_INTERFACE __declspec(dllimport)
#   else
#       define TLV_UTIL_INTERFACE
#   endif
#else
#   define TLV_UTIL_INTERFACE __attribute__((visibility ("default")))
#endif

/** convert an ASCII-HEX buffer to binary
 * \param[out] dst destination buffer of size srclen/2
 * \param[in] src source buffer
 * \param[in] srclen length of source data, must be even! 
 * \return 1 if successful, 0 if the input contained invalid characters
 * \note src and dest may be identical
 * \author M. Meixner \date 9.9.2005
 * @deprecated Use prefixed version TLVUTIL_hextobin or TLVUTIL_hexstringtobin instead
 */
TLV_UTIL_INTERFACE int hextobin(unsigned char *dst, const unsigned char *src, int srclen);

/** convert an ASCII-HEX buffer to binary
 * \param[out] dst destination buffer of size srclen/2
 * \param[in] src source buffer
 * \param[in] srclen length of source data, must be even!
 * \return 1 if successful, 0 if the input contained invalid characters
 * \note src and dest may be identical
 * \author M. Meixner \date 9.9.2005
 */
TLV_UTIL_INTERFACE int TLVUTIL_hextobin(unsigned char *dst, const unsigned char *src, int srclen);

/** convert an ASCII-HEX buffer to binary
 * \param[out] dst destination buffer
 * \param[in] dstLength desitination buffer length
 * \param[in] src source buffer with terminated string containing HEX ASCII (blanks between bytes ignored)
 * \return length of binary data up to dstLength
 * \note src and dest may be identical
 */
TLV_UTIL_INTERFACE int TLVUTIL_hexstringtobin(unsigned char *dst, int dstLength, const unsigned char *src);

/** convert a binary buffer to ASCII-HEX representation (0-9,A-F)
 * \param[out] dst destination buffer of size 2*srclen+1
 * \param[in] src source buffer
 * \param[in] srclen length of source data 
 * \note src and dest may be identical
 * \author M. Meixner \date 9.9.2005
 * @deprecated Use prefixed version TLVUTIL_bintohexstring instead
 */
TLV_UTIL_INTERFACE void bintohex(unsigned char *dst, const unsigned char *src, int srclen);

/** convert a binary buffer to ASCII-HEX representation (0-9,A-F)
 * \param[out] dst destination buffer of size 2*srclen+1
 * \param[in] src source buffer
 * \param[in] srclen length of source data
 * \note src and dest may be identical
 * \author M. Meixner \date 9.9.2005
 */
TLV_UTIL_INTERFACE void TLVUTIL_bintohexstring(unsigned char *dst, const unsigned char *src, int srclen);

/** convert a binary buffer to ASCII-HEX representation (0-9,A-F)
 * Please note: A string termination is not appended.
 * \param[out] dst destination buffer of size 2*srclen
 * \param[in] src source buffer
 * \param[in] srclen length of source data
 * \note src and dest may be identical
 * \author M. Meixner \date 9.9.2005
 */
TLV_UTIL_INTERFACE void TLVUTIL_bintohex(unsigned char *dst, const unsigned char *src, int srclen);


#ifdef __cplusplus
}
#endif

#endif
