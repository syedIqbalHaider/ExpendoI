/****************************************************************************
*  Product:     InFusion
*  Company:     VeriFone
*  Author:      GSS R&D Germany
*  Content:     BER TLV functions
****************************************************************************/

#ifndef BTLV_H
#define BTLV_H

#include "mem_pool.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _VRXEVO
#   ifdef TLV_UTIL_EXPORT
#       define TLV_UTIL_INTERFACE __declspec(dllexport)
#   elif defined TLV_UTIL_IMPORT
#       define TLV_UTIL_INTERFACE __declspec(dllimport)
#   else
#       define TLV_UTIL_INTERFACE
#   endif
#else
#   define TLV_UTIL_INTERFACE __attribute__((visibility ("default")))
#endif

#define MAX_TAGSIZE 3 /**< maximum size of the (binary) tag name in bytes */

/**< constants for vBTLVBuildTag(): */
enum {
   CONTEXT_CLASS           =0x80,  /**< context class (used for vBTLVBuildTag()) */
   PRIVATE_CLASS           =0xC0,  /**< private class (used for vBTLVBuildTag()) */
   CONSTRUCTET_TAG         =0x20,  // typo, use CONSTRUCTED_TAG instead
   CONSTRUCTED_TAG         =0x20,  /**< constructed tag (used for vBTLVBuildTag()) */
   PRIMITIVE_TAG           =0x00   /**< primitive tag (used for vBTLVBuildTag()) */
};

/**< error codes */
enum {
   BTLV_ERR_INVALID=-1,  /**< invalid data was found, e.g. invalid tag, length, ... */
   BTLV_ERR_BUFFER=-2,   /**< buffer is too small */
   BTLV_ERR_MEMORY=-3,   /**< failed to allocate memory */
   BTLV_ERR_FILE=-4,     /**< failed to access/write file */
   BTLV_ERR_NOT_FOUND=-5, /**< the tag has not been found */
   BTLV_ERR_CONSTRUCTED=-6, /**< tried to extract data from a constructed node */
   BTLV_ERR_DONE_FILTER=-7, /**< the import filter has prematurely finished importing data */
   BTLV_ERR_ABORT_FILTER=-8 /**< the import filter has aborted the import of data */
};


/** result codes for filter callbacks */
enum BTLVFilter {
   BTLV_FLTR_KEEP=0,   /**< keep current node and continue */
   BTLV_FLTR_DONE=-1,  /**< keep current node and stop importing further nodes.
                      *   When scanning files for information this can be used to stop
                      *   reading the file when the desired information has been found. */
   BTLV_FLTR_REMOVE=-2,/**< remove current node and its children */
   BTLV_FLTR_ABORT=-3  /**< remove current node and stop importing further nodes */
};


/** context in which the filter callback was called */
enum BTLVContext {
   BTLV_IMPORT_START, /**< the node is just to be imported, i.e. it is created but still empty */
   BTLV_IMPORT_DONE  /**< the node has been completely imported */
};

/** result when matching paths to nodes */
enum BTLVMatch {
   BTLV_MISMATCH,   /**< the path to the node does not match */
   BTLV_MATCH,      /**< the path to the node is a perfect match */
   BTLV_CHILDMATCH, /**< the node is a child of a matching path */
   BTLV_PARENTMATCH /**< the node may be a parent of a matching path */
};


/** data node containing one tag or a root node
 * \date 4.12.2006 \author M. Meixner
 */
struct BTLVNode {
   struct BTLVNode *pxNext;       /**< used for chaining nodes */
   struct BTLVNode *pxFirst;      /**< first child (content) node */
   struct BTLVNode *pxParent;     /**< parent node */
   struct MemoryPool *pxMempool;  /**< memory pool used for allocation */
   char   tcName[2*MAX_TAGSIZE+1];/**< tag name (0-terminated C-string) */
   char   cConstructed;           /**< type: 0=primitive, 1=constructed */
   unsigned char *pucData;        /**< pointer to data (only primitive nodes) */
   unsigned uSize;                /**< size of data (primitive node) or
                                   *   total size of child nodes (constructed).
                                   *   This size does not include the tag name or the
                                   *   number of bytes for the size field */
};


/** Initialize the root node of the tree. The root node of the tree may
 *  reside on the stack or in the global data segment.
 *  If nodes have been created in the tree the memory allocated for them must be
 *  released using vBTLVClear() when done.
 * \param[in] pxNode pointer to the root node
 * \param[in] pxMempool memory pool from which memory is allocated for the child nodes
 *            and data or null pointer to allocate memory from the heap
 * \date 4.12.2006 \author M. Meixner
 * \note When using vBTLVInit on already initialized nodes, a memory leak may result.
 */
TLV_UTIL_INTERFACE void vBTLVInit(struct BTLVNode *pxNode, struct MemoryPool *pxMempool);


/** append a tag to the tree
 * \param[in,out] pxRoot root node of the tree
 * \param[in] pcPath path consisting of one or several tags separated by '/'. The tags
 *                  are given as C-string (i.e. hex-dump of the tag), e.g. '20/DF01'.
 *                  Intermediate nodes will be created as required. If a tag occurs several
 *                  times, it will append the node in the tree where the best (longest)
 *                  match was found. If this is not desired, pxBTLVFindTag() may be used
 *                  to navigate to the desired node and use a path relative to this node to
 *                  insert data.
 *                  (See also \ref pathname)
 * \param[in] pucData pointer to data to be stored into the tree. Note that only primitive
 *                  nodes may contain data. For constructed tags pucData is ignored and
 *                  should be set to 0. For primitive nodes pucData may be set to 0 to
 *                  just allocate the memory without initialization. The data may be inserted
 *                  later using the pointer from the return value.
 * \param[in] uSize size of the data to be stored into the tree. For constructed nodes uSize
 *                  is ignored and should be set to 0.
 * \return pointer to the newly allocated node.
 * \date 4.12.2006 \author M. Meixner
 */
TLV_UTIL_INTERFACE struct BTLVNode *pxBTLVAppendTag(struct BTLVNode *pxRoot,
                                               const char *pcPath,
                                               const unsigned char *pucData, unsigned uSize);


/** Write a tag in the tree. If the tag already exists it gets overwritten. If it does
 *  not yet exist, it is appended.
 * \param[in,out] pxRoot root node of the tree
 * \param[in] pcPath path consisting of one or several tags separated by '/'. (see pxBTLVAppendTag())
 *                  (See also \ref pathname)
 * \param[in] pucData pointer to data to be stored into the tree. Note that only primitive
 *                  nodes may contain data. For constructed tags pucData is ignored and
 *                  should be set to 0. For primitive nodes pucData may be set to 0 to
 *                  just allocate the memory without initialization. The data may be inserted
 *                  later using the pointer from the return value.
 * \param[in] uSize size of the data to be stored into the tree. For constructed nodes uSize
 *                  is ignored and should be set to 0.
 * \return pointer to the newly allocated node.
 * \date 4.12.2006 \author M. Meixner
 */
TLV_UTIL_INTERFACE struct BTLVNode *pxBTLVWriteTag(struct BTLVNode *pxRoot,
                                              const char *pcPath,
                                              const unsigned char *pucData,
                                              unsigned uSize);


/** write data to a node
 * \param[in] pxNode node to which to write data
 * \param[in] pucData pointer to data to be stored into the tree. Note that only primitive
 *                  nodes may contain data.
 * \param[in] uSize size of the data to be stored into the tree.
 * \return 1 in case of success, else 0
 * \date 15.5.2007 \author M. Meixner
 */
TLV_UTIL_INTERFACE int iBTLVWriteData(struct BTLVNode *pxNode, unsigned char *pucData, unsigned uSize);


/** Search for a tag in the tree
 * \param[in,out] pxRoot root node of the tree
 * \param[in] pcPath path consisting of one or several tags separated by '/'. The tags
 *                  are given as C-string (i.e. hex-dump of the tag), e.g. '20/DF01'.
 *                  If a tag occurs several times, the path will descend into the first (left) subtree.
 *                  The member pxNext may be used to access successive (right) siblings of
 *                  this node.
 *                  (See also \ref pathname)
 * \return pointer to the found node or 0.
 * \date 4.12.2006 \author M. Meixner
 */
TLV_UTIL_INTERFACE struct BTLVNode *pxBTLVFindTag(struct BTLVNode *pxRoot,const char *pcPath);


/** Search for the next tag of the same path in the tree. Within a tree nodes with the same
 * name may occur several times. pxBTLVFindTag only finds the first match. pxBTLVFindNextTag()
 * allows to search for other nodes with the same path name.
 * \param[in,out] pxRoot root node of the tree
 * \param[in] pcPath path consisting of one or several tags separated by '/'. The tags
 *                  are given as C-string (i.e. hex-dump of the tag), e.g. '20/DF01'.
 *                  If a tag occurs several times, the path will descend into the first (left) subtree.
 *                  The member pxNext may be used to access successive (right) siblings of
 *                  this node.
 *                  (See also \ref pathname)
 * \param[in] pxPrev previous node in the tree. If set to 0 pxBTLVFindNextTag is the same as
 *                   pxBTLVFindTag.
 * \return pointer to the found node or 0.
 * \date 4.12.2006 \author M. Meixner
 */
TLV_UTIL_INTERFACE struct BTLVNode *pxBTLVFindNextTag(struct BTLVNode *pxRoot,
                                                 const char *pcPath,
                                                 struct BTLVNode *pxPrev);


/** Remove the content of a node including child nodes, the node itself is not released.
 *  The node stays initialized, i.e. it is not required to call vBTLVInit() after vBTLVClear().
 * \param[in,out] pxNode node to be removed
 * \date 2.4.2007 \author M. Meixner
 */
TLV_UTIL_INTERFACE void vBTLVClear(struct BTLVNode *pxNode);


/** Remove a node from the tree and release the memory allocated for this node.
 * This function must not be called for root nodes that were allocated on the stack,
 * use vBTLVClear() in this case.
 * \param[in,out] pxNode node to be removed
 * \date 4.12.2006 \author M. Meixner
 */
TLV_UTIL_INTERFACE void vBTLVRemoveNode(struct BTLVNode *pxNode);


/** Move a node including subnodes from one tree to another. The node is appended as a child
*  to the supplied node. This function must not be called for root nodes that were allocated
*  on the stack.
* \param[in,out] pxRoot node to which the subtree is appended
* \param[in] pxNode node that is moved to another tree
* \date 18.2.2009 \author M. Meixner
*/
TLV_UTIL_INTERFACE void vBTLVMoveNode(struct BTLVNode *pxRoot, struct BTLVNode *pxNode);


/** calculate the size that will be required to export the tree into a flat,
 * BER-TLV encoded buffer. As a side effect the uSize entry in the nodes is updated.
 * \param[in,out] pxRoot node to be removed
 * \date 4.12.2006 \author M. Meixner
 */
TLV_UTIL_INTERFACE unsigned uBTLVContentSize(struct BTLVNode *pxRoot);


/** Write the data found in a tree to a buffer in BER-TLV format
 * \param[in] pxRoot root node
 * \param[out] pucBuffer destination buffer
 * \param[in] uMaxSize size of the destination buffer
 * \return number of written bytes. If an error happens a negative value is returned:
 *  - \b BTLV_ERR_BUFFER: the destination buffer is too small
 *  - \b BTLV_ERR_INVALID: invalid tag has been found
 * \date 4.12.2006 \author M. Meixner
 */
TLV_UTIL_INTERFACE int iBTLVExport(struct BTLVNode *pxRoot, unsigned char *pucBuffer, unsigned uMaxSize);


/** Write the data found in a tree to a file in BER-TLV format. The data is first
 *  written to a temporary file (suffix ".TMP") and then renamed to the original
 *  file name. By this an error or power failue while writing the file does not corrupt
 *  any data.
 * \param[in] pxRoot root node
 * \param[in] pcFilename file name
 * \return number of written bytes. If an error happens a negative value is returned:
 *  - \b BTLV_ERR_INVALID: invalid tag has been found
 *  - \b BTLV_ERR_MEMORY: not enough memory available
 *  - \b BTLV_ERR_FILE: error during file access
 * \date 30.1.2007 \author M. Meixner
 */
TLV_UTIL_INTERFACE int iBTLVExportFile(struct BTLVNode *pxRoot, const char *pcFilename);


/** append data to the end of the file. Incomplete nodes at the end of the file
 *  (e.g. resulting from a power failure) are removed first.
 *  As long as the data to be written has only one node on the topmost level,
 *  this function is atomic: Either this node is written completely or
 *  it is removed when the next node is appended or ignored when the file
 *  is read.
 * \param[in] pxRoot root node;
 * \param[in] pcFilename file name
 * \return number of written bytes. If an error happens a negative value is returned:
 *  - \b BTLV_ERR_INVALID: invalid tag has been found
 *  - \b BTLV_ERR_MEMORY: not enough memory available
 *  - \b BTLV_ERR_FILE: error during file access
 * \date 12.3.2007 \author M. Meixner
 */
TLV_UTIL_INTERFACE int iBTLVAppendFile(struct BTLVNode *pxRoot, const char *pcFilename);


/** read data from a buffer in BER-TLV format.
 * \param[out] pxRoot root node that will receive the input. If the node has child nodes
 *            these are released prior to reading the data.
 * \param[in] pucBuffer source buffer
 * \param[in] uSize size of the source buffer
 * \param[in] pxFilter import filter callback, see also: \ref importfilter.
 *            The pointer may be set to 0 to disable filtering.
 * \param[in] pvData pointer to data that is passed on to the import filter, not used when
 *            no callback was given.
 * \return error code:
 *  - \b 0: success
 *  - \b BTLV_ERR_MEMORY: out of memory
 *  - \b BTLV_ERR_INVALID: invalid tag/data has been found
 *  When reading fails, pxRoot->uSize contains the number of bytes imported without considering
 *  incomplete nodes.
 * \date 4.12.2006 \author M. Meixner
 */
TLV_UTIL_INTERFACE int iBTLVImport(struct BTLVNode *pxRoot, const unsigned char *pucBuffer, unsigned uSize,
                              enum BTLVFilter (*pxFilter)(void *, struct BTLVNode *, enum BTLVContext),void *pvData);


/** read data from a file in BER-TLV format.
 * \param[out] pxRoot root node that will receive the input. If the node has child nodes
 *            these are released prior to reading the data.
 * \param[in] pcFilename name of the file
 * \param[in] pxFilter import filter callback, see also: \ref importfilter.
 *            The pointer may be set to 0 to disable filtering.
 * \param[in] pvData pointer to data that is passed on to the import filter, not used when
 *            no callback was given.
 * \return number of bytes read. In case of an error a negative value is returned:
 *  - \b BTLV_ERR_MEMORY: out of memory
 *  - \b BTLV_ERR_INVALID: invalid tag/data has been found
 *  - \b BTLV_ERR_FILE: error during file access
 *  .
 *  When reading fails, pxRoot->uSize contains the number of bytes read without considering
 *  incomplete nodes.
 * \date 30.1.2007 \author M. Meixner
 */
TLV_UTIL_INTERFACE int iBTLVImportFile(struct BTLVNode *pxRoot, const char *pcFilename,
                enum BTLVFilter (*pxFilter)(void *, struct BTLVNode *, enum BTLVContext),void *pvData);


/** extract data from a primitive node
 * \param[in] pxRoot root node
 * \param[in] pcPath path consisting of one or several tags separated by '/'. The tags
 *                  are given as C-string (i.e. hex-dump of the tag), e.g. '20/DF01'.
 *                  If a tag occurs several times, the path will descend into the first (left) subtree.
 *                  (See also \ref pathname)
 * \param[out] pucBuffer destination buffer to which the result will be written
 * \param[in] uSize size of the buffer
 * \return number of written bytes. If an error happens a negative value is returned:
 *  - \b BTLV_ERR_NOT_FOUND: tag not found
 *  - \b BTLV_ERR_BUFFER: buffer too small
 *  - \b BTLV_ERR_CONSTRUCTED: tag is a constructed node
 *  .
 *  If the tag was not found pucBuffer remains unmodified.
 * \date 5.12.2006 \author M. Meixner
 */
TLV_UTIL_INTERFACE int iBTLVExtractTag(struct BTLVNode *pxRoot, const char *pcPath, unsigned char *pucBuffer, unsigned uSize);


/** match a node against a path.
 * \param[in] pxRoot node
 * \param[in] pcPath path consisting of one or several tags separated by '/'.
 *                  (See also \ref pathname)
 * \return one of:
 *  - \b BTLV_MATCH the node matches the path
 *  - \b BTLV_CHILDMATCH the node is a child of a matching path
 *  - \b BTLV_PARENTMATCH the node may be a parent of a matching path
 *  - \b BTLV_MISMATCH the node does not match the path
 * \date 6.2.2007 \author M. Meixner
 */
TLV_UTIL_INTERFACE enum BTLVMatch xBTLVMatchPath(struct BTLVNode *pxRoot, const char *pcPath);


/** extract the path from the root to the current node
 * \param[in] pxRoot current node
 * \param[out] pcBuffer buffer that is filled with the path
 * \param[in] iMaxsize size of pcBuffer in bytes
 * \return error code, 0 in case of success
 * \date 6.2.2007 \author M. Meixner
 */
TLV_UTIL_INTERFACE int iBTLVGetPath(struct BTLVNode *pxRoot, char *pcBuffer, int iMaxsize);




#define BTLVTagBufSize 9
typedef char BTLVTagBufType[BTLVTagBufSize];

#define ULTAG2ASC(ulTag) pcBTLVTagStr2(ulTag,_ULTAG2ASC_BUF)
#define ULTAG2ASCBUF BTLVTagBufType _ULTAG2ASC_BUF;

/**
 * convert tag from number to string with Buffer provided by caller
 * @param[in]	ulTag		tag number
 * @param[in]  	tcTagBuf	pointer to Buffer (content will be modified)
 * @return	tag string (pointer to tcTagBuf+offset)
 * @date 2014-04-17
 * @author J. Weckmann
 * @note this is for avoiding trouble if pcBTLVTagStr2 is called several times
 */
TLV_UTIL_INTERFACE  char *pcBTLVTagStr2(unsigned long ulTag, BTLVTagBufType tcTagBuf);


/** convert tag from number to string
 * \param[in] ulTag tag number
 * \return tag string
 * \date 04.02.2013 \author T. Buening
 * @note do not call pcBTLVTagStr twice at the same time, use pcBTLVTagStr2() in this case
 * @note This function is not thread safe! It is strongly recommended to use pcBTLVTagStr2 or macro ULTAG2ASC
 */
TLV_UTIL_INTERFACE char *pcBTLVTagStr(unsigned long ulTag);


/** convert tag from string to number
 * \param[in] pcTag tag string
 * \return tag number
 * \date 10.12.2014 \author T. Buening
 */
TLV_UTIL_INTERFACE unsigned long ulBTLVTagNum(const char *pcTag);


/** build tag name from given number
 * \param[in] ucbyte1 tag type definition, e.g. (::PRIVATE_CLASS | ::PRIMITIVE_TAG),
 *                    or (::CONTEXT_CLASS | ::CONSTRUCTET_TAG)
 * \param[in] tag_number reference number to build the tag
 * \param[out] pcTag buffer where to put the tag name (ASCII string)
 * \date 13.04.2010 \author A. Groennert
 * \code
 * //example:
 * {
 *   ULONG ulIndexNumber = 0x9999;
 *   char  cPath[30] = {0x00};
 *
 *   vBTLVBuildTag(CONTEXT_CLASS | CONSTRUCTET_TAG, ulIndexNumber, cPath);
 * }
 * \endcode
 */
TLV_UTIL_INTERFACE void vBTLVBuildTag(unsigned char ucbyte1, unsigned long tag_number, char* pcTag);


/** recover reference number from tag name
 * \param[in] ucTag buffer with tag name (ASCII string)
 * \param[out] ptag_number buffer where to put the reference number
 * \date 13.04.2010 \author A. Groennert
 */
TLV_UTIL_INTERFACE void vBTLVGetTagNumber(unsigned char* ucTag, unsigned long* ptag_number);


/** recover reference number from tag name
 * \param[in] Tag buffer with tag name (ASCII string)
 * \return tag number
 * \date 13.04.2010 \author A. Groennert
 */
TLV_UTIL_INTERFACE unsigned long ulBTLVGetTagNumber(const char *Tag);


/** get nth. child element from parent
 * \param[in] pxParent parent node
 * \param[in] n child element index (starting with zero)
 * \return pointer to the found node or 0.
 */
TLV_UTIL_INTERFACE struct BTLVNode *pxBTLVGetChild(const struct BTLVNode *pxParent,int n);


/** get offset of child element
 * \param[in] pxParent parent node
 * \param[in] pxChild child node
 * \return child offset within parent (-1 on error)
 */
TLV_UTIL_INTERFACE int iBTLVGetChildOffset(const struct BTLVNode *pxParent, const struct BTLVNode *pxChild);


/** get number of child elements from parent
 * \param[in] pxParent parent node
 * \return number of children
 */
TLV_UTIL_INTERFACE int iBTLVGetChildCount(const struct BTLVNode *pxParent);


/** get parent of a child
 * \param[in] pxChild child node
 * \return pointer to the found parent node or 0.
 */
TLV_UTIL_INTERFACE struct BTLVNode *pxBTLVGetParent(const struct BTLVNode *pxChild);

#ifdef __cplusplus
}
#endif

#endif
