/****************************************************************************
*  Product:     InFusion
*  Company:     VeriFone
*  Author:      GSS R&D Germany
*  Content:     Interface definitions and functions
****************************************************************************/


#ifndef EMVCTRL_COMMON_H   /* avoid double interface-includes */
  #define EMVCTRL_COMMON_H

#ifdef __cplusplus
extern "C" {
#endif



// ========================================================================================================
// === COMMON PART ===
// ========================================================================================================

/// @defgroup ADK_LIMITS Limitations and sizes
/// @ingroup ADK_GENERAL
//@{
#define EMV_ADK_MAX_LANG             20  ///< maximum number of languages to be configured
#define EMV_ADK_DEFAULT_AIDSUPP      20  ///< default maximum number of supported AIDs
#define EMV_ADK_MAX_AIDSUPP          EMV_ADK_DEFAULT_AIDSUPP  ///< @deprecated old define for default maximum number of supported AIDs, don't use anymore, use #EMV_ADK_DEFAULT_AIDSUPP instead
#define EMV_ADK_LIMIT_AIDSUPP        128 ///< maximum number of supported AIDs for extended AID support
#define EMV_ADK_MAX_LG_DDOL          80  ///< max. length of DDOL \if is_ct_doc (EMV_CT_APPLIDATA_STRUCT::Default_DDOL) \endif
#define EMV_ADK_MAX_LG_TDOL          80  ///< max. length of TDOL \if is_ct_doc (EMV_CT_APPLIDATA_STRUCT::Default_TDOL) \endif
#define EMV_ADK_MAX_PRIO_APP          5  ///< max. number of priority applications (see \if is_ct_doc EMV_CT_APPLIDATA_STRUCT::xAIDPrio \elseif is_ctls_doc EMV_CTLS_APPLIDATA_STRUCT::xAIDPrio \endif)
#define EMV_ADK_MAX_DOM_CHP           5  ///< max. number of domestic chip applications \if is_ct_doc (see EMV_CT_APPS_SELECT_STRUCT::xDomestic_Chip) \endif
#define EMV_ADK_MAX_FB_MSR           20  ///< max. number of assigned magstripe fallback applications per transaction \if is_ct_doc (see EMV_CT_APPS_SELECT_STRUCT::xFallback_MS) \endif . Candidates are every credit card, Maestro and maybe local applications.
#define EMV_ADK_MAX_CHP_TO_MSR        3  ///< max. number of assigned magstripe fallback applications per chip application \if is_ct_doc (see EMV_CT_APPLIDATA_STRUCT::tucFallbackMIDs) \endif

#define EMV_ADK_IAD_MAX_LEN          16  ///< max. length of issuer authentic. data (EMVCo tag 91) \if is_ct_doc @n because EMV_CT_HOST_STRUCT::AuthData includes "91xx" it has a maximum length of EMV_ADK_IAD_MAX_LEN + 2 \endif

#define EMV_ADK_SCRIPT_RESULT_LEN       5  ///< Max. length of issuer script result data \if is_ct_doc (output of EMV_CT_ContinueOnline(), see also EMV_CT_TRANSRES_STRUCT::scriptresults) \endif (not used for contactless)
#define EMV_ADK_SCRIPT_RESULT_MAX      10  ///< L2 kernel: Max. number of issuer script results \if is_ct_doc (output of EMV_CT_ContinueOnline(), see also EMV_CT_TRANSRES_STRUCT::scriptresults) @n Used for #EMV_ADK_SCRIPT_RESULT_MAX \endif (not used for contactless)

#define EMV_ADK_CHECKSUM_ASCII_SIZE    33  ///< EMV L2 kernel: Size of EMVCo checksum in ASCII representation (incl. zero termination) @n Used for \if is_ct_doc EMV_CT_APPLIDATA_STRUCT::ChksumASCIIEMVCO \elseif is_ctls_doc EMV_CTLS_APPLIDATA_STRUCT::Chksum_EntryPoint and EMV_CTLS_APPLIDATA_STRUCT::Chksum_Kernel \endif
#define EMV_ADK_VERSION_ASCII_SIZE    128  ///< Size of Framework version information in ASCII representation \if is_ct_doc (see EMV_CT_TERMDATA_STRUCT::FrameworkVersion) \endif
#define EMV_ADK_CHECKSUM_SIZE           4  ///< L2 kernel: Size of EMVCo checksum in bytes @n Used internally.
#define EMV_ADK_CHECKSUM_SIZE_NEW      16  ///< L2 kernel: Size of new EMVCo checksum in bytes @n Used internally.
#define EMV_ADK_BCD_AMOUNT_LEN          6
#define EMV_ADK_BIN_AMOUNT_LEN          4
#define EMV_ADK_BCD_COUNTRY_CURRENCY_CODE_LEN  2
#define EMV_ADK_BCD_DATE_OR_TIME_LEN           3

#define EMV_ADK_MAX_CVM           10                         ///< Number of CVM given in parameter \if is_ct_doc @c pucCvmList of EMV_CT_CALLBACK_FnT() (#TAG_BF06_CBK_LOCAL_CHECKS, #TAG_BF07_CBK_DCC). Also used in EMV_CT_TRANSRES_STRUCT::T_8E_CVM_List. Length in bytes: #EMV_ADK_MAX_CVM_LIST_LEN \endif (not used for contactless)
#define EMV_ADK_MAX_CVM_LIST_LEN  (8 + EMV_ADK_MAX_CVM * 2)  ///< Length in bytes of parameter \if is_ct_doc @c pucCvmList of EMV_CT_CALLBACK_FnT() (#TAG_BF05_CBK_DOM_APPS and #TAG_BF07_CBK_DCC). Also used in EMV_CT_TRANSRES_STRUCT::T_8E_CVM_List. Number of included CVMs: #EMV_ADK_MAX_CVM \endif (not used for contactless)

#define EMV_ADK_MAX_PAN_LANGUAGES       6  ///< Maximum number of languages \if is_ct_doc to give back in @c pucReducedLanguageList of EMV_CT_CALLBACK_FnT() (#TAG_BF05_CBK_DOM_APPS) \endif (Not used for contactless)

//@}  // end of TF_LIMITS


// ========================================================================================================
// === RETURN VALUES ===
// ========================================================================================================

/// @defgroup ADK_RET_CODE ADK return codes
/// @ingroup ADK_GENERAL
//@{
typedef unsigned char EMV_ADK_INFO;      ///< type of return code
#define    EMV_ADK_OK                                 0    ///< Function successful, no error
#define    EMV_ADK_APP_REQ_START                      0xA0 ///< CT Reentrance mode: Application requested return start reserved codes
#define    EMV_ADK_APP_REQ_CANDIDATE                  0xA1 ///< CT Reentrance mode: Application requested return application selection
#define    EMV_ADK_APP_REQ_READREC                    0xA2 ///< CT Reentrance mode: Application requested return read records
#define    EMV_ADK_APP_REQ_DATAAUTH                   0xA3 ///< CT Reentrance mode: Application requested return data authentication
#define    EMV_ADK_APP_REQ_ONL_PIN                    0xA4 ///< CT Reentrance mode: Application requested return for online PIN entry
#define    EMV_ADK_APP_REQ_OFL_PIN                    0xA5 ///< CT Reentrance mode: Application requested return for offline PIN entry
#define    EMV_ADK_APP_REQ_PLAIN_PIN                  0xA6 ///< CT Reentrance mode: Application requested return for plaintext PIN entry
#define    EMV_ADK_APP_REQ_CVM_END                    0xA7 ///< CT Reentrance mode: Application requested return cardholder verification
#define    EMV_ADK_APP_REQ_RISK_MAN                   0xA8 ///< CT Reentrance mode: Application requested return riskmanagement
#define    EMV_ADK_APP_REQ_CUST_CVM                   0xA9 ///< CT Reentrance mode: Application requested return for customCVM method
#define    EMV_ADK_APP_REQ_APPS_PREPROC               0xAA ///< CT Reentrance mode: Application requested return for candidate list manipulation
#define    EMV_ADK_APP_REQ_AFTER_GPO               	  0xAB ///< CT Reentrance mode: Application requested return for return after GPO
#define    EMV_ADK_APP_REQ_BUILD_LIST                 0xAC ///< CT Reentrance mode: Application requested return before terminal candidate list build
#define    EMV_ADK_APP_REQ_END                        0xAF ///< CT Reentrance mode: Application requested return end of reserved codes

#define    EMV_ADK_CTLS_OFFLINE_PIN                   0xCB ///< Transaction to be continued after PIN input for girocard (German debit card) Offline PIN CVM
#define    EMV_ADK_NOT_ACCEPTED                       0xCC ///< Transaction / Payment type not accepted
#define    EMV_ADK_CANCELLED                          0xCD ///< Contactless only \if is_ctls_doc @n Transaction was cancelled by EMV_CTLS_Break() \endif
#define    EMV_ADK_CARD_LOG_LOG_OK                    0xCE ///< Return code for successful read of cards transaction log (currently qPBOC only)
#define    EMV_ADK_CTLS_LOW_BATTERY                   0xCF ///< Return code for EMV_CTLS_SetupTransaction indicating mobile device's battery too low for contactless transaction. (ADKEMV-1207)
#define    EMV_ADK_NOT_ALLOWED_WRONG_CFG_INTF         0xD0 ///< CTLS: The function is not allowed when using this config interface
#define    EMV_ADK_VIRTTERMMAP_WRONG_INIT             0xD1 ///< Virtual terminal map is configured, but transaction is started with virtual terminal != 0
#define    EMV_ADK_TOO_MANY_TAPS                      0xD2 ///< CTLS: card holder exceeded the number of taps allowed for one transaction (Interac to show "Cannot Process transaction - too many taps")
#define    EMV_ADK_CAN_NOT_PROCESS                    0xD3 ///< CTLS: card can't be processed, e.g. PIN required in CTLS Interac but CT not available (will appear only if the CTLS kernel is configured in that way that Contact Chip is not supported)
#define    EMV_ADK_USE_ANOTHER_CTLS_CARD              0xD4 ///< CTLS: advice the customer to use another CTLS card (will appear only if the CTLS kernel is configured in that way that Contact Chip is not supported)
#define    EMV_ADK_USE_OTHER_TERMINAL                 0xD5 ///< CTLS: advice the merchant to use another terminal to process the transaction (Interac spec requirement)
#define    EMV_ADK_SCRIPT_PROCESSING_COMPLETE         0xD6 ///< CTLS: explicit script processing completed (CTLS, e.g. Discover DPAS)
#define    EMV_ADK_FALLBACK_CHIP_ONLY                 0xD7 ///< CTLS: Fallback to chip (CTLS), no msr allowed
#define    EMV_ADK_READER_CMD_NOT_ALLOWED             0xD8 ///< Contacless only \if is_ctls_doc @n VFI-Reader: Returned by EMV_CTLS_SetupTransaction() and EMV_CTLS_ContinueOffline() if VFI-Reader responds to "Activate Transaction" with "Command not allowed". This happens if VFI-Reader has not yet finished last transaction, e.g. is still waiting for ContinueOnline. \endif
#define    EMV_ADK_NOT_ALLOWED                        0xD9 ///< Parallel function call not allowed
#define    EMV_ADK_CTLS_NOT_AVAILABLE                 0xDA ///< CTLS not present on hardware per OS information
#define    EMV_ADK_USR_BCKSPC_KEY_PRESSED             0xDB ///< Application requested return  for E_USR_BACKSPACE_KEY_PRESSED, not used anymore
#define    EMV_ADK_CEILING_LIMIT                      0xDC ///< CTLS: amount is above the ceiling limit for CTLS (either terminal ceiling limit or highest scheme ceiling limit)
#define    EMV_ADK_CTLS_RETAP_SAME                    0xDD ///< CTLS: start over with re-tapping the same card (deactivation/activation/start txn)
#define    EMV_ADK_CTLS_DOMESTIC_APP                  0xDE ///< CTLS: Domestic Application Kernel selected
#define    EMV_ADK_NO_CARD                            0xDF ///< CTLS: No card processing so far
#define    EMV_ADK_NOAPP                              0xE0 ///< CT & CTLS: No common application card / terminal
#define    EMV_ADK_NO_EXEC                            0xE1 ///< CT & CTLS: not executable
#define    EMV_ADK_ARQC                               0xE2 ///< CT & CTLS: Transaction must be performed online
#define    EMV_ADK_TC                                 0xE3 ///< CT & CTLS: Transaction performed (offline / online)
#define    EMV_ADK_AAC                                0xE4 ///< CT & CTLS: Transaction declined
#define    EMV_ADK_AAR                                0xE5 ///< CT: Transaction cancellation (former "card referral"), not used anymore
#define    EMV_ADK_PARAM                              0xE6 ///< CT & CTLS: Parameter error (wrong parameter was passed to the function)
#define    EMV_ADK_CARDERR                            0xE7 ///< CT & CTLS: Proprietary card error (actually converted to #EMV_ADK_FALLBACK)
#define    EMV_ADK_BADAPP                             0xE8 ///< CT: Repeat EMV_Select function call-up because selected application on the proprietary card is erroneous. The currently selected application must be transferred (not NO_APPLI) -> this leads to new application selection but the candidate list is not reconstructed.
#define    EMV_ADK_CVM                                0xE9 ///< CT: User abort during PIN input (cancel button, timeout, card removed)
#define    EMV_ADK_ABORT                              0xEA ///< On transaction function it means that current transaction must be aborted. @n On configuration function it represents "failed".
#define    EMV_ADK_CARD_BLOCKED                       0xEB ///< CT & CTLS: Blocked card, regional dependent if fallback to magstripe should be done
#define    EMV_ADK_CARDERR_FORMAT                     0xEC ///< Card error e.g. erroneous TLV coding, incorrect data -> no transaction cancellation
#define    EMV_ADK_INTERNAL                           0xED ///< CT & CTLS: e.g. erroneous communication to PINPad, internal error, not (completely) initialized (function not allowed due to \if is_ct_doc EMV_CT_Init_Framework() with @c EMV_CT_INIT_OPT_BASE_INIT() \elseif is_ctls_doc EMV_CTLS_Init_Framework() with @c EMV_CTLS_INIT_OPT_BASE_INIT() \endif )
#define    EMV_ADK_ONLINE_PIN_RETRY                   0xEE ///< CT: Online PIN reentered (PIN check). @n \if is_ct_doc EMV_CT_ContinueOnline() \elseif is_ctls_doc EMV_CTLS_ContinueOnline() \endif will return this value in the following scenario: Online PIN was entered. Host returned "PIN wrong". @n Calling application must contact host again. And afterwards call \if is_ct_doc EMV_CT_ContinueOnline() \elseif is_ctls_doc EMV_CTLS_ContinueOnline() \endif once again.
#define    EMV_ADK_SAVE_ERROR                         0xEF ///< Required TLV data object is missing. (has not been entered by the application) -> Implementation error, thus no process control information (only used by the config functions)
#define    EMV_ADK_APP_BLOCKED                        0xF0 ///< Application blocked: AID evaluation at fallback, no global fallback because it is not allowed for this special blocked application
#define    EMV_ADK_READ_ERROR                         0xF1 ///< Error while reading EMV configuration
#define    EMV_ADK_ERR_HSM                            0xF2 ///< E.g. erroneous communication to PINPad, internal error, not used anymore
#define    EMV_ADK_TLV_BUILD_ERR                      0xF3 ///< CT & CTLS: Error in TLV data object = internal error
#define    EMV_ADK_FALLBACK                           0xF4 ///< CT & CTLS: Fallback to chip/msr (for CTLS) respectively magstripe (for CT) must be done
#define    EMV_ADK_ONL_PIN_REPEAT                     0xF5 ///< CT: wrong Online-PIN, repeat transaction (reduced) beginning with \if is_ct_doc EMV_CT_StartTransaction() \elseif is_ctls_doc EMV_CTLS_SetupTransaction() \endif
#define    EMV_ADK_MAND_ELEM_MISSING                  0xF6 ///< CT: mandatory (TLV) element missing (EMV-configuration), not used anymore
#define    EMV_ADK_INVALID_TERM_CAP                   0xF7 ///< CT & CTLS: invalid terminal capabilities (EMV-configuration), not used anymore
#define    EMV_ADK_REFERRAL                           0xF8 ///< CT: a referral of the application decides if the TRX is approved or not
#define    EMV_ADK_2_CTLS_CARDS                       0xF9 ///< CTLS: 2 CTLS cards in the field detected. If this is reported after a call to ContinueOffline 2 cards have been detected in the field. The transaction is terminated in this case and must be restarted with a new call of SetupTransaction. This is mandatory for the VFI-Reader ADK and optional (but works as well) for teh Velocity/VERTX based ADK.
#define    EMV_ADK_TXN_CTLS_L1_ERR                    0xFA ///< CTLS: level 1 problem, retap necessary
#define    EMV_ADK_TXN_CTLS_MOBILE                    0xFB ///< CTLS: mobile device, retap necessary
#define    EMV_ADK_TXN_CTLS_EMPTY_LIST                0xFC ///< CTLS: empty candidate list. The application shall display "please use other interface" and (depending on regional market requirements) additionally "or tap another card"
#define    EMV_ADK_TXN_CTLS_EMV_USE_OTHER_CARD        0xFD ///< CTLS: insert, swipe or try another card
#define    EMV_ADK_CTLS_DOMESTIC_ONLY_NOT_READABLE    0xFE ///< CTLS: domestic not readable (no AID and EMV not configured)
#define    EMV_ADK_CONTINUE                           0xFF ///< CT & CTLS: Transaction can be continued (e.g. CTLS Commit)
//@}


// ========================================================================================================
// === CONFIGURATION DEFINES ===
// ========================================================================================================

/// @defgroup VIRTUALTERMMAP_MODE Defines for type of virtual terminal map
/// @ingroup ADK_CONFIGURATION
///
/// Used in \if is_ct_doc EMV_CT_MapVirtualTerminal() \elseif is_ctls_doc EMV_CTLS_MapVirtualTerminal() \endif
//@{
typedef unsigned char EMV_ADK_VIRTUALTERMMAP_TYPE;  ///< typedef for "virtual terminal map type" (\if is_ct_doc EMV_CT_MapVirtualTerminal() \elseif is_ctls_doc EMV_CTLS_MapVirtualTerminal() \endif)
  #define EMV_ADK_VIRTUALTERMMAP_APPEND       0x01  ///< append one entry to virtual terminal map.
  #define EMV_ADK_VIRTUALTERMMAP_DELETE_ALL   0x20  ///< delete whole virtual terminal map.
//@}


/// @defgroup READ_APPLI_TYPE Defines for type of read application data mode
/// @ingroup DEF_CONF_APPLI
///
/// Used in \if is_ct_doc EMV_CT_GetAppliData() \elseif is_ctls_doc EMV_CTLS_GetAppliData() \endif
//@{
typedef unsigned char EMV_ADK_READAPPLI_TYPE; ///< typedef for "read appli type" (\if is_ct_doc EMV_CT_GetAppliData() \elseif is_ctls_doc EMV_CTLS_GetAppliData() \endif)
  #define EMV_ADK_READ_AID       0                  ///< read data of a given AID. Remark: If the same AID is configured with multiple param sets (Velocity CTLS only, see @ref EMV_ADK_FORCE_NEW_PARAMSET) only the first AID is presented here. To read all of the AIDs in this case please use first, next mechanism.
  #define EMV_ADK_READ_FIRST     1                  ///< read first AID
  #define EMV_ADK_READ_NEXT      2                  ///< read next AID
  #define EMV_ADK_READ_MAX_APPLI 3                  ///< future use
//@}


/// @defgroup APPLI_CONF_MODE Modes of application data and CAP key setting
/// @ingroup DEF_CONF_APPLI
//@{
typedef unsigned char EMV_ADK_HANDLE_RECORD_TYPE; ///< Type for handle application data setting (SetAppliData())
  #define EMV_ADK_SET_ONE_RECORD      1                  ///< Set record for 1 AID, If the AID exists, the parameters included are updated, not included parameters will be untouched. If the AID does not exist, not included parameters will be set with ADK default values. It is highly recommended to configure all parameters on initial setup. Remark: If the AID is existing and the AID is configured with multiple param sets (CTLS velocity only) all instances of this AID are updated --> if the AIDs need to be reconfigured independently it is necessary to delete this AID and confgure again from scratch, see @ref EMV_ADK_FORCE_NEW_PARAMSET
  #define EMV_ADK_SET_TWO_RECORDS     2                  ///< Set same record for 2 AIDS
  #define EMV_ADK_SET_THREE_RECORDS   3                  ///< Set same record for 3 AIDs
  #define EMV_ADK_SET_FOUR_RECORDS    4                  ///< Set same record for 4 AIDs
  #define EMV_ADK_SET_FIVE_RECORDS    5                  ///< Set same record for 5 AIDs
  #define EMV_ADK_CLEAR_ONE_RECORD    0x10               ///< Clear data of an AID record by AID. Remark: If the same AID is configured with multiple param sets (CTLS velocity only) all instances of this AID are deleted --> one record in this respect means: one AID with its multiple parameters sets, see @ref EMV_ADK_FORCE_NEW_PARAMSET
  #define EMV_ADK_CLEAR_ALL_RECORDS   0x20               ///< Clear all data
  #define EMV_ADK_FORCE_NEW_PARAMSET  0x30               ///< This is for the CTLS EntryPoint --> Force a new parameterset for an existing AID but having different kernel ID. This is for Velocity CTLS only since the entry point architecture allows to have different kernels (and therefore parameter sets) used for the same AID. So each AID can be configured mutiple times

  #define EMV_ADK_TEMP_UPDATE         0xF0               ///< Temporary dynamic update of AID data during a transaction
                                                         ///< @n This can be used for updating data during a transaction, e.g. if the transaction is interrupted with the parameter @c TxnSteps. The stored config in the XML files will NOT be touched
                                                         ///< @n After transaction initiation: It is activated when having finished the application selection == Final Select is done
                                                         ///< @n Before the final select any temporary updated data will be overwritten with the permanent AID parameters, which are copied for the current transaction after Final Select was performed.
//@}

#define EMV_ADK_ADD_TAG_SIZE        255        ///< Maximum field size for @c Additional_Tags
#define EMV_ADK_DEBUG_DATA_SIZE     32         ///< size for L2 kernel debug data + RFU + 2 byte framework status

/// @defgroup SPECIAL_TRXS Defines for special transaction configuration
/// @ingroup DEF_CONF_APPLI
/// @brief Special transactions, transaction types and flows
///
/// see \if is_ctls_doc EMV_CTLS_APPLIDATA_STRUCT::xuc_Special_TRX \elseif is_ct_doc EMV_CT_APPLIDATA_STRUCT::xuc_Special_TRX, EMV_DOM_CHIP_STRUCT::xuc_Special_TRX AND EMV_FALLBCK_MSR_STRUCT::xuc_Special_TRX \endif
/// @n For explanation of the transaction types see @ref TRANS_TYPES.
/// @n For an example see \if is_ctls_doc @ref subsec_emv_ctls_Config_AppliData_specialTrx \elseif is_ct_doc @ref subsec_emv_ct_Config_AppliData_specialTrx \endif

//@{
#define  EMV_ADK_EMV_ADK_MANUAL_REVERSAL_BYTE         0   ///< byte   for configuration of manual reversal
#define  EMV_ADK_MANUAL_REVERSAL_NIBBLE       >> 4 & 0x0F ///< nibble for configuration of manual reversal
#define  EMV_ADK_REFUND_BYTE                  0           ///< byte   for configuration of refund
#define  EMV_ADK_REFUND_NIBBLE                & 0x0F      ///< nibble for configuration of refund

#define  EMV_ADK_EMV_ADK_RESERVATION_BYTE             1   ///< byte   for configuration of reservation
#define  EMV_ADK_RESERVATION_NIBBLE           >> 4 & 0x0F ///< nibble for configuration of reservation
#define  EMV_ADK_TIP_BYTE                     1           ///< byte   for configuration of tip (gratuity)
#define  EMV_ADK_TIP_NIBBLE                   & 0x0F      ///< nibble for configuration of tip (gratuity)

#define  EMV_ADK_REFERRAL_BYTE                2           ///< byte   for configuration of referral @n not used for contactless
#define  EMV_ADK_REFERRAL_NIBBLE              >> 4 & 0x0F ///< nibble for configuration of referral @n not used for contactless
#define  EMV_ADK_VOICEAUT_BYTE                2           ///< byte   for configuration of voice authorization @n not used for contactless
#define  EMV_ADK_VOICEAUT_NIBBLE              & 0x0F      ///< nibble for configuration of voice authorization @n not used for contactless

#define  EMV_ADK_RFU_MODE_BYTE                3           ///< byte   RFU
#define  EMV_ADK_RFU_MODE_NIBBLE              >> 4 & 0x0F ///< nibble RFU
#define  EMV_ADK_FALLBACK_AFTER_CVM_BYTE      3           ///< byte   for configuration of "fallback to magstripe after start of cardholder verification or early PIN entry allowed" @n not used for contactless
#define  EMV_ADK_FALLBACK_AFTER_CVM_NIBBLE    & 0x0F      ///< nibble for configuration of "fallback to magstripe after start of cardholder verification or early PIN entry allowed" @n not used for contactless

#define  EMV_ADK_IGNORE_CARD_ERROR_BYTE       4           ///< byte   for configuration of "ignore card error after issuer authorization"
#define  EMV_ADK_IGNORE_CARD_ERROR_NIBBLE     >> 4 & 0x0F ///< nibble for configuration of "ignore card error after issuer authorization"

/* ------------------------------------------- */

#define  EMV_ADK_MANUAL_REVERSAL_NO           0 ///< future use
#define  EMV_ADK_MANUAL_REVERSAL_A            1 ///< future use
#define  EMV_ADK_MANUAL_REVERSAL_B            2 ///< future use
#define  EMV_ADK_REFUND_NO                    0 ///< refund forbidden
#define  EMV_ADK_REFUND_YES                   1 ///< refund allowed

#define  EMV_ADK_RESERVATION_NO               0 ///< reservation forbidden
#define  EMV_ADK_RESERVATION_A                1 ///< reservation variant a (without partial reversal) allowed, no amount increment (Reservierung-Erhoehung) @n according \if is_ctls_doc EMV_CTLS_START_STRUCT::TransType \elseif is_ct_doc EMV_CT_SELECT_STRUCT::TransType \endif : #EMV_ADK_TRAN_TYPE_INIT_RESERVATION_A @n for exception case see #EMV_CT_SELOP_RESERV_ALLOW_B_ON_A
#define  EMV_ADK_RESERVATION_A_PLUS           2 ///< reservation variant a (without partial reversal) allowed, amount increment (Reservierung-Erhoehung) allowed @n according \if is_ctls_doc EMV_CTLS_START_STRUCT::TransType \elseif is_ct_doc EMV_CT_SELECT_STRUCT::TransType \endif : #EMV_ADK_TRAN_TYPE_INIT_RESERVATION_A and #EMV_ADK_TRAN_TYPE_INCREMENT_A @n for exception case see #EMV_CT_SELOP_RESERV_ALLOW_B_ON_A
#define  EMV_ADK_RESERVATION_B                3 ///< reservation variant b (with partial reversal) allowed, no amount increment (Reservierung-Erhoehung) @n according \if is_ctls_doc EMV_CTLS_START_STRUCT::TransType \elseif is_ct_doc EMV_CT_SELECT_STRUCT::TransType \endif : #EMV_ADK_TRAN_TYPE_INIT_RESERVATION_B @n for exception case see #EMV_CT_SELOP_RESERV_ALLOW_B_ON_A
#define  EMV_ADK_RESERVATION_B_PLUS           4 ///< reservation variant b (with partial reversal) allowed, amount increment (Reservierung-Erhoehung) allowed @n according \if is_ctls_doc EMV_CTLS_START_STRUCT::TransType \elseif is_ct_doc EMV_CT_SELECT_STRUCT::TransType \endif : #EMV_ADK_TRAN_TYPE_INIT_RESERVATION_B and #EMV_ADK_TRAN_TYPE_INCREMENT_B @n for exception case see #EMV_CT_SELOP_RESERV_ALLOW_B_ON_A
#define  EMV_ADK_RESERVATION_B_OFFLINE        5 ///< reservation variant b (with partial reversal) allowed, no amount increment (Reservierung-Erhoehung) @n offline allowed (will be without partial reversal) @n according \if is_ctls_doc EMV_CTLS_START_STRUCT::TransType \elseif is_ct_doc EMV_CT_SELECT_STRUCT::TransType \endif: #EMV_ADK_TRAN_TYPE_INIT_RESERVATION_B @n for exception case see #EMV_CT_SELOP_RESERV_ALLOW_B_ON_A

#define  EMV_ADK_TIP_NO                       0 ///< tip (gratuity) forbidden
#define  EMV_ADK_TIP_YES                      1 ///< tip (gratuity) allowed (@deprecated old definition, do not use any more!)
#define  EMV_ADK_TIP_MODE_TIPPABLE            1 ///< tip (gratuity) allowed as tippable transaction
#define  EMV_ADK_TIP_MODE_MIXED               2 ///< tip (gratuity) tip mode depends on CVM
#define  EMV_ADK_TIP_MODE_INPUT               3 ///< tip (gratuity) allowed as payment with tip input

#define  EMV_ADK_REFERRAL_NO                  0 ///< voice referral forbidden \if is_ct_doc @n Will only take effect in case #REFERRAL_AFTER_TRX is set EMV_CT_APPLIDATA_STRUCT::App_FlowCap \endif @n not used for contactless
#define  EMV_ADK_REFERRAL_YES                 1 ///< voice referral allowed @n not used for contactless

#define  EMV_ADK_VOICE_NO                     0 ///< voice authorization forbidden @n not used for contactless
#define  EMV_ADK_VOICE_YES                    1 ///< voice authorization (part of TAC-IAC-default handling) allowed \if is_ct_doc @n @a Preconditions: @n - terminal is attended @n - #REFERRAL_AFTER_TRX is set (EMV_CT_APPLIDATA_STRUCT::App_FlowCap) \endif @n not used for contactless

#define  EMV_ADK_FALLBACK_AFTER_CVM_NO        0 ///< No fallback to magstripe after start cardholder verification or early PIN entry. \if is_ct_doc @n Take care: EMV_CT_APPLIDATA_STRUCT::uc_FallBack_Handling must be set to #FB_GERMAN_POS_SPEC. \endif @n not used for contactless
#define  EMV_ADK_FALLBACK_AFTER_CVM_YES       1 ///< Fallback to magstripe after start cardholder verification or early PIN entry. \if is_ct_doc @n Take care: EMV_CT_APPLIDATA_STRUCT::uc_FallBack_Handling must be set to #FB_GERMAN_POS_SPEC. \endif @n not used for contactless

#define  EMV_ADK_IGNORE_CARD_ERROR_NO         0 ///< Transaction decline on card error after issuer authorization.
#define  EMV_ADK_IGNORE_CARD_ERROR_YES        1 ///< Ignore card errors after issuer authorization. Anyhow approve the transaction.

#define  EMV_ADK_TRX_CONFIG_DEFAULT           "\x21\x20\x11\x00\x00\x00\x00\x00" ///< Default setting, for interpretation see \if is_ctls_doc @ref subsec_emv_ctls_Config_AppliData_specialTrx \elseif is_ct_doc @ref subsec_emv_ct_Config_AppliData_specialTrx \endif
//@}


// ========================================================================================================
// === TEXTS AND LANGUAGES ===
// ========================================================================================================

// === EMV_ADK_TXT_TYPE ===
typedef struct
{
  unsigned char   byLanguage;
  unsigned char   byTxt1;
  unsigned char   byTxt2;
} EMV_ADK_TXT_TYPE;

/// @defgroup APPLI_TEXTS Application text IDs
/// @ingroup DEF_CONF_TEXT
/// @brief Also see ::EMV_ADK_TXT_TYPE
//@{
#define EMV_ADK_TXT_NO_TXT              0x00  ///< no text (internal use)
#define EMV_ADK_TXT_REFUND_CONF_AMOUNT  0x01  ///< Refund @n EUR XXXXXX,XX @n Please confirm @n Needed in case #EMV_ADK_TRAN_TYPE_REFUND @c AND #REFUND_CONFIRM_AMOUNT.
#define EMV_ADK_TXT_AMOUNT              0x02  ///< AMOUNT @n EUR XXXXXX,XX @n PLEASE CONFIRM
#define EMV_ADK_TXT_3AMO_TIP            0xA1  ///< second part for 3 amounts for payment with tip input
#define EMV_ADK_TXT_3AMO_CASHBACK       0xA2  ///< second part for 3 amounts for payment with cash back
#define EMV_ADK_TXT_APPROVED            0x03  ///< APPROVED
#define EMV_ADK_TXT_AUTH_APPROVED       0xA3  ///< like #EMV_ADK_TXT_APPROVED, but for reservation
#define EMV_ADK_TXT_DECLINED            0x04  ///< DECLINED
#define EMV_ADK_TXT_AUTH_DECLINED       0xA4  ///< like #EMV_ADK_TXT_DECLINED, but for reservation
#define EMV_ADK_TXT_NOT_ACCEPTED        0x05  ///< NOT ACCEPTED
#define EMV_ADK_TXT_CARD_ERROR          0x06  ///< CARD ERROR
#define EMV_ADK_TXT_PROCESSING_ERROR    0x07  ///< PROCESSING ERROR
#define EMV_ADK_TXT_CARD_READ_OK        0x08  ///< card read ok
#define EMV_ADK_TXT_AUTHORIZING         0x09  ///< authorizing
#define EMV_ADK_TXT_REMOVE_CARD         0x10  ///< REMOVE CARD
#define EMV_ADK_TXT_USE_CHIP_READER     0x11  ///<
#define EMV_ADK_TXT_USE_MAG_STRIPE      0x12  ///<
#define EMV_ADK_TXT_VOICEAUT            0x13  ///< Voice authorization in case of communication problem
#define EMV_ADK_TXT_SEE_PHONE           0x14  ///< CTLS only: "See phone for instructions"
#define EMV_ADK_TXT_RETAP_SAME          0x15  ///< CTLS only: "Retap (same) card", probable reason: torn transaction
#define EMV_ADK_TXT_RETAP_SAME_L1       0x16  ///< CTLS only: "Retap (same) card", reason: L1 error \if is_ctls_doc @n Only sent in case CLTRXOP_L1_ERROR_CALLBACK() is activated @n Dependency to INPUT_CTLS_TRM_FLOWOPT_UI_SCHEME_DEFAULT() \endif
#define EMV_ADK_TXT_2_CARDS_IN_FIELD    0x17  ///< CTLS only: 2 card detected in the field \if is_ctls_doc @n Only sent in case CLTRXOP_L1_ERROR_CALLBACK() is activated \endif
#define EMV_ADK_TXT_CARD_READ_COMPLETE  0x18  ///< CTLS only: waiting for Card Removal in the ADK \if is_ctls_doc , App should display Card Read Complete, Remove Card @n Only sent in case CTLS_WAIT_CARD_REMOVAL_END() in application flow capabilties is activated \endif
//@}

/// @defgroup TF_LANGUAGES Known languages
/// @ingroup DEF_CONF_TEXT
/// @brief see ::EMV_ADK_TXT_TYPE
//@{
#define EMV_ADK_LANG_NO_LANG            0x00 ///< internal use only
#define EMV_ADK_LANG_ENGLISH            0x01 ///< English
#define EMV_ADK_LANG_GERMAN             0x02 ///< German
#define EMV_ADK_LANG_FRENCH             0x03 ///< French
#define EMV_ADK_LANG_SPANISH            0x04 ///< Spanish
#define EMV_ADK_LANG_ITALIAN            0x05 ///< Italian
#define EMV_ADK_LANG_CZECH              0x06 ///< Czech
#define EMV_ADK_LANG_SLOVAK             0x07 ///< Slovak
#define EMV_ADK_LANG_SWEDISH            0x08 ///< Swedish
#define EMV_ADK_LANG_POLISH             0x09 ///< Polish
#define EMV_ADK_LANG_GREEK              0x0A ///< Greek
#define EMV_ADK_LANG_TURKISH            0x0B ///< Turkish
#define EMV_ADK_LANG_DANSK              0x0C ///< Dansk
#define EMV_ADK_LANG_DUTCH              0x0D ///< Dutch
#define EMV_ADK_LANG_NORWEGIAN          0x0E ///< Norwegian
#define EMV_ADK_LANG_PORTUGUESE         0x0F ///< Portuguese
#define EMV_ADK_LANG_AUSTRIAN           0x10 ///< Austrian
#define EMV_ADK_LANG_ESTONIAN           0x11 ///< Estonian
#define EMV_ADK_LANG_FINNISH            0x12 ///< Finnish
#define EMV_ADK_LANG_LATVIA             0x13 ///< Latvia
#define EMV_ADK_LANG_LITHUANIA          0x14 ///< Lithuania
#define EMV_ADK_LANG_RUSSIAN            0x15 ///< Russian
#define EMV_ADK_LANG_BULGARIAN          0x16 ///< Bulgarian
#define EMV_ADK_LANG_CROATIAN           0x17 ///< Croatian
#define EMV_ADK_LANG_HUNGARIAN          0x18 ///< Hungarian
#define EMV_ADK_LANG_MOLDAVIAN          0x19 ///< Moldavian
#define EMV_ADK_LANG_ROMANIAN           0x1A ///< Romanian
#define EMV_ADK_LANG_SERBIAN            0x1B ///< Serbian
#define EMV_ADK_LANG_SLOVENIAN          0x1C ///< Slovenian
#define EMV_ADK_LANG_UNKNOWN            0xFF ///< internal use only
//@}


// ========================================================================================================
// === COMMON TRANSACTION, TERMINAL AND ICC DEFINES ===
// ========================================================================================================

/// @defgroup TERM_TYPES Terminaltypes (Tag 9F35)
/// @ingroup DEF_CONF_TERM
/// @brief see also [EMV B4], page 113
//@{
  #define EMV_ADK_TT_BANK_ATTENDED_ONL_ONLY             0x11 ///< Operational Control Provided By Financial Institution @n attended terminal, online only
  #define EMV_ADK_TT_BANK_ATTENDED_OFFL_ONL             0x12 ///< Operational Control Provided By Financial Institution @n attended terminal, offline with online capability
  #define EMV_ADK_TT_BANK_ATTENDED_OFFL_ONLY            0x13 ///< Operational Control Provided By Financial Institution @n attended terminal, offline only
  #define EMV_ADK_TT_BANK_UNATTENDED_ONL_ONLY           0x14 ///< Operational Control Provided By Financial Institution @n unattended terminal, online only @n ATM
  #define EMV_ADK_TT_BANK_UNATTENDED_OFFL_ONL           0x15 ///< Operational Control Provided By Financial Institution @n unattended terminal, offline with online capability @n ATM
  #define EMV_ADK_TT_BANK_UNATTENDED_OFFL_ONLY          0x16 ///< Operational Control Provided By Financial Institution @n unattended terminal, offline only @n ATM
  #define EMV_ADK_TT_ATTENDED_ONL_ONLY                  0x21 ///< Operational Control Provided By Merchant @n attended terminal, online only
  #define EMV_ADK_TT_ATTENDED_OFFL_ONL                  0x22 ///< Operational Control Provided By Merchant @n attended terminal, offline with online capability
  #define EMV_ADK_TT_ATTENDED_OFFL_ONLY                 0x23 ///< Operational Control Provided By Merchant @n attended terminal, offline only
  #define EMV_ADK_TT_UNATTENDED_ONL_ONLY                0x24 ///< Operational Control Provided By Merchant @n unattended terminal, online only
  #define EMV_ADK_TT_UNATTENDED_OFFL_ONL                0x25 ///< Operational Control Provided By Merchant @n unattended terminal, offline with online capability
  #define EMV_ADK_TT_UNATTENDED_OFFL_ONLY               0x26 ///< Operational Control Provided By Merchant @n unattended terminal, offline only
  #define EMV_ADK_TT_CRDHC_UNATTENDED_ONL_ONLY          0x34 ///< Operational Control Provided By Cardholder @n unattended terminal, online only
  #define EMV_ADK_TT_CRDHC_UNATTENDED_OFFL_ONL          0x35 ///< Operational Control Provided By Cardholder @n unattended terminal, offline with online capability
  #define EMV_ADK_TT_CRDHC_UNATTENDED_OFFL_ONLY         0x36 ///< Operational Control Provided By Cardholder @n unattended terminal, offline only
//@}


/// @defgroup TRANS_TYPES Transaction type (Tag 9C)
/// @ingroup DEF_FLOW_INPUT
/// @brief According to ISO 8583 - Annex A: Processing Code, Position 1 + 2
///
/// Used as input for @c TransType in EMV_CT_SELECT_TYPE / EMV_CTLS_START_TYPE
/// And output \if is_ct_doc EMV_CT_TRANSRES_STRUCT::T_9C_TransType \elseif is_ctls_doc EMV_CTLS_TRANSRES_STRUCT::T_9C_TransType \endif
///
/// As the transaction type is defined as BCD, we will use the range A0-FF for
/// special transactions. E.g. a reservation is 0xF0 (limitations of application
/// selection are considered) and will be resetted to 0x00 as soon as internally needed.
/// @n Allowed transaction types are configured with @ref SPECIAL_TRXS
/// @n
//@{
  #define EMV_ADK_TRAN_TYPE_GOODS_SERVICE         0x00  ///< Goods and services
  #define EMV_ADK_TRAN_TYPE_CASH                  0x01  ///< Cash advance
  #define EMV_ADK_TRAN_TYPE_CASHBACK              0x09  ///< Cash back (payment with cash hand out)
  #define EMV_ADK_TRAN_TYPE_MANUAL_CASH           0x12  ///< manual cash (PP3 testcase)
  #define EMV_ADK_TRAN_TYPE_MASTERCARD_CASH       0x17  ///< Cash for MasterCard (only output)
  #define EMV_ADK_TRAN_TYPE_REFUND                0x20  ///< Refund
//----------------------------------------
  #define EMV_ADK_TRAN_TYPE_INTERNAL_LIMIT        0x99
//----------------------------------------
  #define EMV_ADK_TRAN_TYPE_INIT_RESERVATION_A    0xF0  ///< Reservation variant a (without partial reversal)
  #define EMV_ADK_TRAN_TYPE_INCREMENT_A           0xF1  ///< Reservation increment variant a
  #define EMV_ADK_TRAN_TYPE_ERHOEHUNG_A           EMV_ADK_TRAN_TYPE_INCREMENT_A  ///< @deprecated use @ref EMV_ADK_TRAN_TYPE_INCREMENT_A
  #define EMV_ADK_TRAN_TYPE_BOOKING_A             0xF2  ///< Booking (reservation)
  #define EMV_ADK_TRAN_TYPE_BUCHUNG_A             EMV_ADK_TRAN_TYPE_BOOKING_A  ///< @deprecated use @ref EMV_ADK_TRAN_TYPE_BOOKING_A
  #define EMV_ADK_TRAN_TYPE_INIT_RESERVATION_B    0xF3  ///< Reservation variant b (with partial reversal)
  #define EMV_ADK_TRAN_TYPE_INCREMENT_B           0xF4  ///< Reservation increment variant a
  #define EMV_ADK_TRAN_TYPE_ERHOEHUNG_B           EMV_ADK_TRAN_TYPE_INCREMENT_B  ///< @deprecated use @ref EMV_ADK_TRAN_TYPE_INCREMENT_B
  #define EMV_ADK_TRAN_TYPE_BOOKING_B             0xF5  ///< Booking (reservation)
  #define EMV_ADK_TRAN_TYPE_BUCHUNG_B             EMV_ADK_TRAN_TYPE_BOOKING_B  ///< @deprecated use @ref EMV_ADK_TRAN_TYPE_BOOKING_B
  #define EMV_ADK_TRAN_TYPE_TIP                   0xE0  ///< Tip (gratuity)
  #define EMV_ADK_TRAN_TYPE_MANUAL_REVERSAL       0xD0  ///< Manual reversal
  #define EMV_ADK_TRAN_TYPE_APPROVAL_PHONE        0xD1  ///< Approval by phone
  #define EMV_ADK_TRAN_TYPE_READ_CARD_LOG         0xD2  ///< Try to read the card log (CUP CTLS feature)
  #define EMV_ADK_TRAN_TYPE_REPEAT_ONLINE_PIN     0xC0  ///< Transaction repetition after wrong online PIN entry
  #define EMV_ADK_TRAN_TYPE_REPEAT_AMOUNT_CHANGE  0xB0  ///< Contact only: Transaction repetition after amount change \if is_ct_doc (requirement detected in #TAG_BF06_CBK_LOCAL_CHECKS). @n Examples: domestic TIP (Austria) or cashback (Sweden). \endif
  #define EMV_ADK_TRAN_TYPE_REPEAT_TRX_INTERCEPT  0xA0  ///< Contact only: Transaction repetition after CR request \if is_ct_doc (requirement detected in #TAG_BF06_CBK_LOCAL_CHECKS, #DOM_OPTION_TRX_INTERCEPT). @n Examples: CR was asked after reading the PAN (Austria EPA). \endif @n not used for contactless
  #define EMV_ADK_TRAN_TYPE_REPEAT_TRX_DCC        0xA1  ///< Contact only: Transaction repetition after DCC handling. \if is_ct_doc Shall be used after break at #TAG_BF07_CBK_DCC. \endif
//@}

/// @defgroup CRYP_EMV_ADK_INF_ADD Additional information in cryptogram information (9F27)
/// @ingroup DEF_FLOW_OUTPUT
/// @brief 9F27 (cryptogram information) can carry more information than just AAC, TC, and ARQC.
/// The ICC has the possibility to give back these additional information.
/// Calling application may use these defines to analyse \if is_ct_doc EMV_CT_TRANSRES_STRUCT::T_9F27_CryptInfo \elseif is_ctls_doc EMV_CTLS_TRANSRES_STRUCT::T_9F27_CryptInfo \endif .
//@{
  #define EMV_ADK_CARD_REQUESTS_ADVICE          0x08 ///< Card requests advice
  #define EMV_ADK_CARD_ADDITIONAL_INFO          0x07 ///< Mask for "service not allowed", "PIN try limit exceeded", "Issuer authentication failed"
//@}


/// @defgroup STATUS_INFO Status information
/// @ingroup DEF_FLOW_OUTPUT
/// @brief defines for \if is_ct_doc EMV_CT_TRANSRES_STRUCT::StatusInfo \elseif is_ctls_doc EMV_CTLS_TRANSRES_STRUCT::StatusInfo \endif
//@{
#define EMV_ADK_SI_ONLINE_PIN_REQUIRED    0x00000001u  ///< this means Online PIN CVM was performed during the transaction (CT: already done in a callback by the app, CTLS: to be performed by the app once the card is out of the field) --> Online PIN processing (host encryption) is required if the transaction is not declined or if there is no fallback.
#define EMV_ADK_SI_SIGNATURE_REQUIRED     0x00000002u  ///< this means signature CVM was performed and the signature line must be printed on the receipt if the transaction is not declined or if there is no fallback.
#define EMV_ADK_SI_FORCED_ACCEPTANCE      0x00000004u  ///< Forced acceptance transaction
#define EMV_ADK_SI_USER_DEFINED_CVM       0x00000008u  ///< user defined CVM, this means that a custom CVM was performed and according to custom/domestic rules additional steps may apply
#define EMV_ADK_SI_ON_DEVICE_CVM          0x00000010u  ///< An On-Device CVM was performed (Amex: "Mobile CVM", Visa: "Consumer device CVM")
#define EMV_ADK_SI_CUSTOMER_CARDWITHDRAWL 0x00000080u  ///< Result is EMV_ADK_ABORT because customer has pulled out the card
#define EMV_ADK_SI_GICC_TIPPABLE          0x00000100u  ///< A tip transaction may follow this payment
#define EMV_ADK_SI_PTC_EXCEEDED           0x00000200u  ///< PIN try counter exceeded
#define EMV_ADK_SI_PIN_FAILURE            0x00000400u  ///< Last entered offline PIN was wrong
#define EMV_ADK_SI_CONTACTLESS_CHIP       0x00000800u  ///< A contactless chip transaction
#define EMV_ADK_SI_CONTACTLESS_MSR        0x00001000u  ///< A contactless magstripe txn
#define EMV_ADK_SI_TORN_CREATED           0x00002000u  ///< A torn transaction was created (PP3) @n not used for contact
#define EMV_ADK_SI_TORN_TRY_RECOVER       0x00004000u  ///< A torn transaction was tried to recover @n not used for contact
#define EMV_ADK_SI_CONTACTLESS_WAITING    0x00008000u  ///< The contactless transaction is still in the card waiting phase
#define EMV_ADK_SI_CONTACT_ONGOING        0x00008000u  ///< The contact transaction is still ongoing
#define EMV_ADK_SI_CTLS_LONG_TAP          0x00010000u  ///< Contactless "long tap" \if is_ctls_doc , only appears if transaction goes online, card has to remain in field until EMV_CTLS_ContinueOnline() returned \endif
#define EMV_ADK_SI_ARQC_RESTART_ANY       0x00020000u  ///< Contactless kernel asks for second activation after online request in any case even in case of online problem (e.g. girocard OUT.ORD=ANY)
#define EMV_ADK_SI_ARQC_RESTART_UTGO      0x00040000u  ///< Contactless kernel set OUT.ORD to EMV&OTGO and conditions for offline approval are valid. Retap required if unable to go online or script/auth data available.
//@}


/// @defgroup SMART_STATUS Smart status
/// @ingroup ADK_ICC_IF
/// @brief Return values for functions of group @ref FUNC_ICC
//@{
#define EMV_ADK_SMART_STATUS_OK         0  ///< okay (== comms ok == card in == card activated ... )
#define EMV_ADK_SMART_STATUS_REMOVED    1  ///< card removed = error caused by cardholder
#define EMV_ADK_SMART_STATUS_OVERFLOW   2  ///< error caused by terminal
#define EMV_ADK_SMART_STATUS_EXCHG_ERR  3  ///< error caused by ICC
#define EMV_ADK_SMART_TWO_CARDS         4  ///< Contactless: Two cards in the field
#define EMV_ADK_SMART_ERR_INIT          5  ///< error caused for initialization
#define EMV_ADK_SMART_INV_PARAM         6  ///< error caused passing invalid parameters
//@}

/// @defgroup FETCH_TAGS_OPTIONS Options for fetching TLV data
/// @ingroup DEF_FLOW_OUTPUT
/// @brief @c options for \if is_ct_doc EMV_CT_fetchTxnTags() \elseif is_ctls_doc EMV_CTLS_fetchTxnTags() \endif
///
//@{
#define EMV_ADK_FETCHTAGS_NO_EMPTY  (1 << 0)  ///< In case no data is found for a given TLV tag: don't include this tag in the output.
//@}

/// @defgroup UPDATE_TAGS_OPTIONS Options for updating TLV data
/// @ingroup DEF_FLOW_INPUT
/// @brief @c options for EMV_CT_updateTxnTags()
///
//@{
#define EMV_ADK_UPDATETAGS_ERROR_ON_NOT_ALLOWED_TAG  (1 << 0)  ///< return error #EMV_ADK_PARAM if one or several tags are not allowed to be updated. If bit is not set, not allowed tags will be silently ignored.
//@}

// ========================================================================================================
// === END OF COMMON PART ===
// ========================================================================================================

#ifdef __cplusplus
}     // extern "C"
#endif

#endif  // #ifndef EMVCTRL_COMMON_H
