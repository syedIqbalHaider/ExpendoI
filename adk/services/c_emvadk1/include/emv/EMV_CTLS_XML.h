/****************************************************************************
*  Product:     InFusion
*  Company:     VeriFone
*  Author:      GSS R&D Germany
*  Content:     Definitions and functions for serial interface
****************************************************************************/

#ifndef EMV_CTLS_XML_H   /* avoid double interface-includes */
  #define EMV_CTLS_XML_H


// ========================================================================================================
// === XML tags ===
// ========================================================================================================
/// @defgroup ADK_XML_TAGS Tags for configuration storage
/// @ingroup ADK_CONFIGURATION
/// @brief Used for storing the configuration in XML files.
///@{

#define XML_TAG_TERMDATA                              "TerminalData"                       ///< constructed xml tag for terminal data
#define XML_TAG_TERMDATA_TERM_TYP                     "TermTyp"                            ///<  @n TLV tag #TAG_9F35_TRM_TYPE, @n TermTyp in EMV_CTLS_TERMDATA_STRUCT::TermTyp, @n XML Tag: #XML_TAG_TERMDATA_TERM_TYP
#define XML_TAG_TERMDATA_COUNTRY_CODE_TERM            "CountryCodeTerm"                    ///<  @n TLV tag #TAG_9F1A_TRM_COUNTRY_CODE, @n TermCountryCode in EMV_CTLS_TERMDATA_STRUCT::TermCountryCode, @n XML Tag: #XML_TAG_TERMDATA_COUNTRY_CODE_TERM
#define XML_TAG_TERMDATA_CURRENCY_TRANS               "CurrencyTrans"                      ///<  @n TLV tag #TAG_5F2A_TRANS_CURRENCY, @n CurrencyTrans in EMV_CTLS_TERMDATA_STRUCT::CurrencyTrans, @n XML Tag: #XML_TAG_TERMDATA_CURRENCY_TRANS
#define XML_TAG_TERMDATA_EXP_TRANS                    "ExpTrans"                           ///<  @n TLV tag #TAG_5F36_TRANS_CURRENCY_EXP, @n ExpTrans in EMV_CTLS_TERMDATA_STRUCT::ExpTrans, @n XML Tag: #XML_TAG_TERMDATA_EXP_TRANS
#define XML_TAG_TERMDATA_SUPP_LANG                    "SuppLang"                           ///<  @n TLV tag #TAG_SUPP_LANG, @n SuppLang in EMV_CTLS_TERMDATA_STRUCT::SuppLang, @n XML Tag: #XML_TAG_TERMDATA_SUPP_LANG
#define XML_TAG_TERMDATA_IFD_SERIAL_NUMBER            "IFD_SerialNumber"                   ///<  @n TLV tag #TAG_9F1E_IFD_SERIAL_NB, @n IFDSerialNumber in EMV_CTLS_TERMDATA_STRUCT::IFDSerialNumber, @n XML Tag: #XML_TAG_TERMDATA_IFD_SERIAL_NUMBER
#define XML_TAG_TERMDATA_FLOW_OPTIONS                 "FlowOptions"                        ///<  @n TLV tag #TAG_DF8F0B_TERM_FLOW_OPTIONS, @n FlowOptions in EMV_CTLS_TERMDATA_STRUCT::FlowOptions, @n XML Tag: #XML_TAG_TERMDATA_FLOW_OPTIONS
#define XML_TAG_TERMDATA_MAXCTLS_TRANSLIMIT           "MaxCTLSTranslimit"                  ///<  @n TLV tag #TAG_DF8F0E_TERM_MAXCTLS_TRANSLIMIT, @n MaxCTLSTranslimit in EMV_CTLS_TERMDATA_STRUCT::MaxCTLSTranslimit, @n XML Tag: #XML_TAG_TERMDATA_MAXCTLS_TRANSLIMIT
#define XML_TAG_TERMDATA_BEEP_VOLUME                  "BeepVolume"                         ///<  @n TLV tag #TAG_DF62_BUZZER_VOLUME, @n EMV_CTLS_TERMDATA_STRUCT::BeepVolume
#define XML_TAG_TERMDATA_BEEP_FREQUENCY_SUCCESS       "BeepFrequencySuccess"               ///<  @n TLV tag #TAG_DFD006_BEEP_FREQ_SUCCESS, @n EMV_CTLS_TERMDATA_STRUCT::BeepFrequencySuccess
#define XML_TAG_TERMDATA_BEEP_FREQUENCY_ALERT         "BeepFrequencyAlert"                 ///<  @n TLV tag #TAG_DFD007_BEEP_FREQ_ALERT, @n EMV_CTLS_TERMDATA_STRUCT::BeepFrequencyAlert
#define XML_TAG_TERMDATA_KERNEL_VERSION               "KernelVersion"                      ///<  @n TLV tag #TAG_KERNEL_VERSION, @n KernelVersion in EMV_CTLS_TERMDATA_STRUCT::KernelVersion, @n XML Tag: #XML_TAG_TERMDATA_KERNEL_VERSION
#define XML_TAG_TERMDATA_FRAMEWORK_VERSION            "FrameworkVersion"                   ///<  @n TLV tag #TAG_DF11_LIB_VERSION, @n FrameworkVersion in EMV_CTLS_TERMDATA_STRUCT::FrameworkVersion, @n XML Tag: #XML_TAG_TERMDATA_FRAMEWORK_VERSION
#define XML_TAG_TERMDATA_L1DRIVER_VERSION             "L1DriverVersion"         		   ///<  @n TLV tag #TAG_DF6F_L1DRIVER_VERSION, @n L1DriverVersion in EMV_CTLS_TERMDATA_STRUCT::L1DriverVersion, @n XML Tag: #XML_TAG_TERMDATA_L1DRIVER_VERSION


#define XML_TAG_TERMDATA_HOTLIST                      "Hotlist"
#define XML_TAG_TERMDATA_HOTLIST_RECORD               "Hot_Item"
#define XML_TAG_TERMDATA_HOTLIST_PAN                  "PAN"
#define XML_TAG_TERMDATA_HOTLIST_SEQ_NUMBER           "PAN_Sequence_Number"


#define XML_TAG_CAP_KEYS                              "CapKeys"                            ///< constructed xml tag for CAP key data
#define XML_TAG_CAP_KEYS_CAPKEY                       "CapKey"                             ///< constructed xml tag for A SINGLE CAP key
#define XML_TAG_CAP_KEYS_INDEX                        "Index"                              ///<  @n TLV tag #TAG_KEY_INDEX, @n Index in EMV_CTLS_CAPKEY_STRUCT::Index, @n XML Tag: #XML_TAG_CAP_KEYS_INDEX
#define XML_TAG_CAP_KEYS_RID                          "RID"                                ///<  @n TLV tag #TAG_KEY_RID, @n RID in EMV_CTLS_CAPKEY_STRUCT::RID, @n XML Tag: #XML_TAG_CAP_KEYS_RID
#define XML_TAG_CAP_KEYS_KEY                          "Key"                                ///<  @n TLV tag #TAG_KEY_KEY, @n Key in EMV_CTLS_CAPKEY_STRUCT::Key, @n XML Tag: #XML_TAG_CAP_KEYS_KEY
#define XML_TAG_CAP_KEYS_KEYLEN                       "KeyLen"                             ///< @c Struct, @c XML Reference: @n KeyLen in EMV_CTLS_CAPKEY_STRUCT::KeyLen, @n XML Tag: #XML_TAG_CAP_KEYS_KEYLEN
#define XML_TAG_CAP_KEYS_EXPONENT                     "Exponent"                           ///<  @n TLV tag #TAG_KEY_EXPONENT, @n Exponent in EMV_CTLS_CAPKEY_STRUCT::Exponent, @n XML Tag: #XML_TAG_CAP_KEYS_EXPONENT
#define XML_TAG_CAP_KEYS_HASH                         "Hash"                               ///<  @n TLV tag #TAG_KEY_HASH, @n Hash in EMV_CTLS_CAPKEY_STRUCT::Hash, @n XML Tag: #XML_TAG_CAP_KEYS_HASH
#define XML_TAG_CAP_KEYS_REVOC_LIST                   "RevocationList"                     ///<  @n TLV tag #TAG_KEY_CRL, @n RevocEntries in EMV_CTLS_CAPKEY_STRUCT::RevocEntries, @n XML Tag: #XML_TAG_CAP_KEYS_REVOC_LIST

#define XML_TAG_APPLIDATA                             "ApplicationData"                    ///< constructed xml tag for application data
#define XML_TAG_APPLIDATA_APP                         "Application"                        ///< Constructed tag for one AID
#define XML_TAG_APPLIDATA_AID                         "AID"                                ///<  @n TLV tag #TAG_DF04_AID, @n AID in EMV_CTLS_APPLI_STRUCT::AID
#define XML_TAG_APPLIDATA_CL_MODES                    "CL_Modes"                           ///<  @n TLV tag #TAG_C2_TRM_CL_MODES, @n CL_Modes in EMV_CTLS_APPLIDATA_STRUCT::CL_Modes, @n XML Tag: #XML_TAG_APPLIDATA_CL_MODES
#define XML_TAG_APPLIDATA_APP_NAME                    "AppName"                            ///<  @n TLV tag #TAG_50_APP_LABEL, @n AppName in EMV_CTLS_APPLIDATA_STRUCT::AppName, @n XML Tag: #XML_TAG_APPLIDATA_APP_NAME
#define XML_TAG_APPLIDATA_ASI                         "ASI"                                ///<  @n TLV tag #TAG_DF20_ASI, @n ASI in EMV_CTLS_APPLIDATA_STRUCT::ASI, @n XML Tag: #XML_TAG_APPLIDATA_ASI
#define XML_TAG_APPLIDATA_COUNTRY_CODE_TERM           "CountryCodeTerm"                    ///<  @n TLV tag #TAG_5F28_ISS_COUNTRY_CODE, @n App_CountryCodeTerm in EMV_CTLS_APPLIDATA_STRUCT::App_CountryCodeTerm, @n XML Tag: #XML_TAG_APPLIDATA_COUNTRY_CODE_TERM
#define XML_TAG_APPLIDATA_BR_KEY                      "BrKey"                              ///<  @n TLV tag #TAG_9F15_MERCH_CATEG_CODE, @n BrKey in EMV_CTLS_APPLIDATA_STRUCT::BrKey, @n XML Tag: #XML_TAG_APPLIDATA_BR_KEY
#define XML_TAG_APPLIDATA_TERM_IDENT                  "TermIdent"                          ///<  @n TLV tag #TAG_9F1C_TRM_ID, @n TermIdent in EMV_CTLS_APPLIDATA_STRUCT::TermIdent, @n XML Tag: #XML_TAG_APPLIDATA_TERM_IDENT
#define XML_TAG_APPLIDATA_CL_CVM_SOFT_LIMIT           "CL_CVM_Soft_Limit"                  ///<  @n TLV tag #TAG_C0_TRM_CL_CVM_LIMIT, @n CL_CVM_Soft_Limit in EMV_CTLS_APPLIDATA_STRUCT::CL_CVM_Soft_Limit, @n XML Tag: #XML_TAG_APPLIDATA_CL_CVM_SOFT_LIMIT
#define XML_TAG_APPLIDATA_CL_CEILING_LIMIT            "CL_Ceiling_Limit"                   ///<  @n TLV tag #TAG_C1_TRM_CL_CEIL_LIMIT, @n CL_Ceiling_Limit in EMV_CTLS_APPLIDATA_STRUCT::CL_Ceiling_Limit, @n XML Tag: #XML_TAG_APPLIDATA_CL_CEILING_LIMIT
#define XML_TAG_APPLIDATA_FLOOR_LIMIT                 "FloorLimit"                         ///<  @n TLV tag #TAG_9F1B_TRM_FLOOR_LIMIT, @n FloorLimit in EMV_CTLS_APPLIDATA_STRUCT::FloorLimit, @n XML Tag: #XML_TAG_APPLIDATA_FLOOR_LIMIT
#define XML_TAG_CHPAPPLIDATA_VER_NUM                  "Chip_VerNumber"                     ///<  @n TLV tag #TAG_9F09_TRM_APP_VERSION_NB, @n CHPVerNum in EMV_CTLS_APPLIDATA_STRUCT::CHPVerNum, @n XML Tag: #XML_TAG_CHPAPPLIDATA_VER_NUM
#define XML_TAG_MSRAPPLIDATA_VER_NUM                  "MSR_VerNumber"                      ///<  @n TLV tag #TAG_9F6D_TRM_APP_VERSION_NB, @n MSRVerNum in EMV_CTLS_APPLIDATA_STRUCT::MSRVerNum, @n XML Tag: #XML_TAG_MSRAPPLIDATA_VER_NUM
#define XML_TAG_APPLIDATA_ADD_VER_NUM                 "AdditionalVersioNumbers"            ///<  @n TLV tag #TAG_DF5F_ADD_APP_VERSION, @n Additional_Versions_No in EMV_CTLS_APPLIDATA_STRUCT::Additional_Versions_No, @n XML Tag: #XML_TAG_APPLIDATA_ADD_VER_NUM
#define XML_TAG_APPLIDATA_MERCH_IDENT                 "MerchIdent"                         ///<  @n TLV tag #TAG_9F16_MERCHANT_ID, @n MerchIdent in EMV_CTLS_APPLIDATA_STRUCT::MerchIdent, @n XML Tag: #XML_TAG_APPLIDATA_MERCH_IDENT
#define XML_TAG_APPLIDATA_AID_PRIO                    "AID_Prio"                           ///<  @n TLV tag #TAG_DF1D_PRIO_APPLI, @n xAIDPrio in EMV_CTLS_APPLIDATA_STRUCT::xAIDPrio, @n XML Tag: #XML_TAG_APPLIDATA_AID_PRIO
#define XML_TAG_APPLIDATA_SPECIAL_TRX                 "SpecialTRX"                         ///<  @n TLV tag #TAG_DF1C_SPECIAL_TRX, @n xuc_Special_TRX in EMV_CTLS_APPLIDATA_STRUCT::xuc_Special_TRX, @n XML Tag: #XML_TAG_APPLIDATA_SPECIAL_TRX
#define XML_TAG_APPLIDATA_APP_FLOW_CAP                "AppFlowCap"                         ///<  @n TLV tag #TAG_DF2B_APP_FLOW_CAP, @n App_FlowCap in EMV_CTLS_APPLIDATA_STRUCT::App_FlowCap, @n XML Tag: #XML_TAG_APPLIDATA_APP_FLOW_CAP
#define XML_TAG_APPLIDATA_RETAP_FIELD_OFF             "RetapFieldOff"                      ///<  @n TLV tag #TAG_DF30_RETAP_FIELD_OFF, @n EMV_CTLS_APPLIDATA_STRUCT::RetapFieldOff, @n XML Tag: #XML_TAG_APPLIDATA_RETAP_FIELD_OFF
#define XML_TAG_APPLIDATA_ADDITIONAL_TAGS_TRM         "AdditionalTagsTRM"                  ///<  @n TLV tag #TAG_DF29_ADD_TAGS, @n Additional_Tags_TRM in EMV_CTLS_APPLIDATA_STRUCT::Additional_Tags_TRM, @n XML Tag: #XML_TAG_APPLIDATA_ADDITIONAL_TAGS_TRM
#define XML_TAG_APPLIDATA_ADDITIONAL_TAGS_CRD         "AdditionalTagsCRD"                  ///<  @n TLV tag #TAG_DF2C_ADD_TAGS_CRD, @n Additional_Tags_CRD in EMV_CTLS_APPLIDATA_STRUCT::Additional_Tags_CRD, @n XML Tag: #XML_TAG_APPLIDATA_ADDITIONAL_TAGS_CRD
#define XML_TAG_APPLIDATA_APP_TERM_CAP                "AppTermCap"                         ///<  @n TLV tag #TAG_9F33_TRM_CAPABILITIES, @n TermCap in EMV_CTLS_APPLIDATA_STRUCT::TermCap, @n XML Tag: #XML_TAG_APPLIDATA_APP_TERM_CAP
#define XML_TAG_APPLIDATA_APP_TERM_ADD_CAP            "AppTermAddCap"                      ///<  @n TLV tag #TAG_9F40_ADD_TRM_CAP, @n TermAddCap in EMV_CTLS_APPLIDATA_STRUCT::TermAddCap, @n XML Tag: #XML_TAG_APPLIDATA_APP_TERM_ADD_CAP
#define XML_TAG_APPLIDATA_APP_TERM_TYP                "AppTerminalType"                    ///<  @n TLV tag #TAG_9F35_TRM_TYPE, @n App_TermTyp in EMV_CTLS_APPLIDATA_STRUCT::App_TermTyp, @n XML Tag: #XML_TAG_APPLIDATA_APP_TERM_TYP
#define XML_TAG_APPLIDATA_TAC_DENIAL                  "TAC_Denial"                         ///<  @n TLV tag #TAG_DF21_TAC_DENIAL, @n TACDenial in EMV_CTLS_APPLIDATA_STRUCT::TACDenial, @n XML Tag: #XML_TAG_APPLIDATA_TAC_DENIAL
#define XML_TAG_APPLIDATA_TAC_ONLINE                  "TAC_Online"                         ///<  @n TLV tag #TAG_DF22_TAC_ONLINE, @n TACOnline in EMV_CTLS_APPLIDATA_STRUCT::TACOnline, @n XML Tag: #XML_TAG_APPLIDATA_TAC_ONLINE
#define XML_TAG_APPLIDATA_TAC_DEFAULT                 "TAC_Default"                        ///<  @n TLV tag #TAG_DF23_TAC_DEFAULT, @n TACDefault in EMV_CTLS_APPLIDATA_STRUCT::TACDefault, @n XML Tag: #XML_TAG_APPLIDATA_TAC_DEFAULT
#define XML_TAG_APPLIDATA_DEFAULT_TDOL                "DefaultTDOL"                        ///<  @n TLV tag #TAG_DF27_DEFAULT_TDOL, @n Default_TDOL in EMV_CTLS_APPLIDATA_STRUCT::Default_TDOL, @n XML Tag: #XML_TAG_APPLIDATA_DEFAULT_TDOL
#define XML_TAG_APPLIDATA_PP3_PMSG_TABLE              "PhoneMessageTable"                  ///<  @n TLV tag #TAG_FB_PP3_PMSG_TABLE, @n PhoneMessageTable in EMV_CTLS_APPLIDATA_STRUCT::PhoneMessageTable, @n XML Tag: #XML_TAG_APPLIDATA_PP3_PMSG_TABLE
#define XML_TAG_APPLIDATA_MSR_CVM_ABOVE               "MagstripeCVM_aboveLimit"            ///<  @n TLV tag #TAG_DF2F_MSR_CVM_ABOVE, @n MagstripeCVM_aboveLimit in EMV_CTLS_APPLIDATA_STRUCT::MagstripeCVM_aboveLimit, @n XML Tag: #XML_TAG_APPLIDATA_MSR_CVM_ABOVE
#define XML_TAG_APPLIDATA_MSR_CVM_BELOW               "MagstripeCVM_belowLimit"            ///<  @n TLV tag #TAG_DF10_MSR_CVM_BELOW, @n MagstripeCVM_belowLimit in EMV_CTLS_APPLIDATA_STRUCT::MagstripeCVM_belowLimit, @n XML Tag: #XML_TAG_APPLIDATA_MSR_CVM_BELOW
#define XML_TAG_APPLIDATA_CHIP_CVM_BELOW              "ChipCVM_belowLimit"                 ///<  @n TLV tag #TAG_DF44_CHIP_CVM_BELOW, @n ChipCVM_belowLimit in EMV_CTLS_APPLIDATA_STRUCT::ChipCVM_belowLimit, @n XML Tag: #XML_TAG_APPLIDATA_CHIP_CVM_BELOW
#define XML_TAG_APPLIDATA_CEILING_LIMIT_MOBILE        "CL_Ceiling_LimitMobile"             ///<  @n TLV tag #TAG_DF49_CEIL_LIMIT_MOBILE, @n CL_Ceiling_LimitMobile in EMV_CTLS_APPLIDATA_STRUCT::CL_Ceiling_LimitMobile, @n XML Tag: #XML_TAG_APPLIDATA_CEILING_LIMIT_MOBILE
#define XML_TAG_APPLIDATA_TORN_TXN_LIFETIME           "Torn_TXN_Liftime"                   ///<  @n TLV tag #TAG_DF45_CHIP_TXN_LIFETIME, @n TRN_TXN_Lifetime in EMV_CTLS_APPLIDATA_STRUCT::TRN_TXN_Lifetime, @n XML Tag: #XML_TAG_APPLIDATA_TORN_TXN_LIFETIME
#define XML_TAG_APPLIDATA_TORN_TXN_NO                 "Torn_TXN_Number"                    ///<  @n TLV tag #TAG_DF46_CHIP_TXN_NO, @n TRN_TXN_Number in EMV_CTLS_APPLIDATA_STRUCT::TRN_TXN_Number, @n XML Tag: #XML_TAG_APPLIDATA_TORN_TXN_NO
#define XML_TAG_APPLIDATA_TTC                         "Txn_Category_Code"                  ///<  @n TLV tag #TAG_9F53_TRANS_CATEGORY_CODE, @n TXN_CategoryCode in EMV_CTLS_APPLIDATA_STRUCT::TXN_CategoryCode, @n XML Tag: #XML_TAG_APPLIDATA_TTC
#define XML_TAG_APPLIDATA_MERCH_NAME_LOCATION         "MerchantName_Location"              ///<  @n TLV tag #TAG_9F4E_TAC_MERCHANTLOC, @n MerchantName_Location in EMV_CTLS_APPLIDATA_STRUCT::MerchantName_Location, @n XML Tag: #XML_TAG_APPLIDATA_MERCH_NAME_LOCATION
#define XML_TAG_APPLIDATA_VISA_TTQ                    "VisaTTQ"                            ///<  @n TLV tag #TAG_9F66_TTQ, @n VisaTTQ in EMV_CTLS_APPLIDATA_STRUCT::VisaTTQ, @n XML Tag: #XML_TAG_APPLIDATA_VISA_TTQ
#define XML_TAG_APPLIDATA_VISA_DRL_PARAMS             "VisaDRLParams"                      ///<  @n TLV tag #TAG_FA_VISA_DRL_RISK, @n EMV_CTLS_APPLIDATA_STRUCT::VisaDRLParams, @n XML Tag: #XML_TAG_APPLIDATA_VISA_DRL_PARAMS
#define XML_TAG_APPLIDATA_VISA_DRL_INDEX              "Index"                              ///<  @n Index in EMV_CTLS_VISA_DRL_STRUCT::Index, @n XML Tag: #XML_TAG_APPLIDATA_VISA_DRL_INDEX
#define XML_TAG_APPLIDATA_VISA_DRL_FLOORLIMIT         "Floorlimit"                         ///<  @n Floorlimit in EMV_CTLS_VISA_DRL_STRUCT::Floorlimit, @n XML Tag: #XML_TAG_APPLIDATA_VISA_DRL_FLOORLIMIT
#define XML_TAG_APPLIDATA_VISA_DRL_PRG_ID_LEN         "AppPrgIdLen"                        ///<  @n ucAppPrgIdLen in EMV_CTLS_VISA_DRL_STRUCT::ucAppPrgIdLen, @n XML Tag: #XML_TAG_APPLIDATA_VISA_DRL_PRG_ID_LEN
#define XML_TAG_APPLIDATA_VISA_DRL_APP_PRG_ID         "Application_PRG_ID"                 ///<  @n Application_PRG_ID in EMV_CTLS_VISA_DRL_STRUCT::Application_PRG_ID, @n XML Tag: #XML_TAG_APPLIDATA_VISA_DRL_APP_PRG_ID
#define XML_TAG_APPLIDATA_VISA_DRL_TXN_LIMIT          "TXNlimit"                           ///<  @n TXNlimit in EMV_CTLS_VISA_DRL_STRUCT::TXNlimit, @n XML Tag: #XML_TAG_APPLIDATA_VISA_DRL_TXN_LIMIT
#define XML_TAG_APPLIDATA_VISA_DRL_CVM_LIMIT          "CVMlimit"                           ///<  @n CVMlimit in EMV_CTLS_VISA_DRL_STRUCT::CVMlimit, @n XML Tag: #XML_TAG_APPLIDATA_VISA_DRL_CVM_LIMIT
#define XML_TAG_APPLIDATA_VISA_DRL_SWITCH             "FeatureSwitch"                      ///<  @n OnOffSwitch in EMV_CTLS_VISA_DRL_STRUCT::OnOffSwitch, @n XML Tag: #XML_TAG_APPLIDATA_VISA_DRL_SWITCH
#define XML_TAG_APPLIDATA_AMEX_DRL_PARAMS             "AmexDRLParams"                      ///<  @n TLV tag #TAG_FD_AMEX_DRL_RISK, @n AMEX_TerminalCaps in EMV_CTLS_APPLIDATA_STRUCT::AmexDRLParams, @n XML Tag: #XML_TAG_APPLIDATA_AMEX_DRL_PARAMS
#define XML_TAG_APPLIDATA_AMEX_DRL_INDEX              "Index"                              ///<  @n Index in EMV_CTLS_AMEX_DRL_STRUCT::Index, @n XML Tag: #XML_TAG_APPLIDATA_VISA_DRL_INDEX
#define XML_TAG_APPLIDATA_AMEX_DRL_FLOORLIMIT         "Floorlimit"                         ///<  @n Floorlimit in EMV_CTLS_AMEX_DRL_STRUCT::Floorlimit, @n XML Tag: #XML_TAG_APPLIDATA_VISA_DRL_FLOORLIMIT
#define XML_TAG_APPLIDATA_AMEX_DRL_TXN_LIMIT          "TXNlimit"                           ///<  @n TXNlimit in EMV_CTLS_AMEX_DRL_STRUCT::TXNlimit, @n XML Tag: #XML_TAG_APPLIDATA_VISA_DRL_TXN_LIMIT
#define XML_TAG_APPLIDATA_AMEX_DRL_CVM_LIMIT          "CVMlimit"                           ///<  @n CVMlimit in EMV_CTLS_AMEX_DRL_STRUCT::CVMlimit, @n XML Tag: #XML_TAG_APPLIDATA_VISA_DRL_CVM_LIMIT
#define XML_TAG_APPLIDATA_AMEX_DRL_SWITCH             "FeatureSwitch"                      ///<  @n OnOffSwitch in EMV_CTLS_AMEX_DRL_STRUCT::OnOffSwitch, @n XML Tag: #XML_TAG_APPLIDATA_VISA_DRL_SWITCH
#define XML_TAG_APPLIDATA_MTI_MERCHANT_TYPE_IND       "MTI_Merchant_Type_Ind"              ///<  @n TLV tag #TAG_9F58_MERCH_TYPE_INDICATOR, @n MTI_Merchant_Type_Ind in EMV_CTLS_APPLIDATA_STRUCT::MTI_Merchant_Type_Ind, @n XML Tag: #XML_TAG_APPLIDATA_MTI_MERCHANT_TYPE_IND
#define XML_TAG_APPLIDATA_TTI_TERM_TRANS_INFO         "TTI_Term_Trans_Info"                ///<  @n TLV tag #TAG_9F59_TERM_TRANS_INFO, @n TTI_Term_Trans_Info in EMV_CTLS_APPLIDATA_STRUCT::TTI_Term_Trans_Info, @n XML Tag: #XML_TAG_APPLIDATA_TTI_TERM_TRANS_INFO
#define XML_TAG_APPLIDATA_TTI_TERM_TRANS_TYPE         "TTT_Term_Trans_Type"                ///<  @n TLV tag #TAG_9F5A_TERM_TRANS_TYPE, @n TTT_Term_Trans_Type in EMV_CTLS_APPLIDATA_STRUCT::TTT_Term_Trans_Type, @n XML Tag: #XML_TAG_APPLIDATA_TTI_TERM_TRANS_TYPE
#define XML_TAG_APPLIDATA_TOS_TERM_OPTION_STATUS      "TOS_Term_Option_Status"             ///<  @n TLV tag #TAG_9F5E_TERM_OPTION_STATUS, @n TOS_Term_Option_Status in EMV_CTLS_APPLIDATA_STRUCT::TOS_Term_Option_Status, @n XML Tag: #XML_TAG_APPLIDATA_TOS_TERM_OPTION_STATUS
#define XML_TAG_APPLIDATA_TERM_RECEIPT_REQUIRED_LIMIT "TERM_RCPT_REQLimit"                 ///<  @n TLV tag #TAG_9F5D_TERM_CTLS_RECEIPT_REQUIRED_LIMIT, @n Term_CTLS_Receipt_REQLimit in EMV_CTLS_APPLIDATA_STRUCT::Term_CTLS_Receipt_REQLimit, @n XML Tag: #XML_TAG_APPLIDATA_TERM_RECEIPT_REQUIRED_LIMIT
#define XML_TAG_APPLIDATA_THRESHOLD_BCD               "Threshold_BCD"
#define XML_TAG_APPLIDATA_MAX_TARGET_PERCENTAGE       "MaxTargetPercentage"
#define XML_TAG_APPLIDATA_TARGET_PERCENTAGE           "TargetPercentage"
#define XML_TAG_APPLIDATA_TRY_AGAIN_LIMIT             "TryAgainLimit"
#define XML_TAG_AMEX_TERMINAL_CAPS                    "AMEX_Term_Caps"                     ///<  @n TLV tag #TAG_9F6D_AMEX_CAPABILITIES, @n AMEX_TerminalCaps in EMV_CTLS_APPLIDATA_STRUCT::AMEX_TerminalCaps, @n XML Tag: #XML_TAG_AMEX_TERMINAL_CAPS
#define XML_TAG_AMEX_ENH_TERMINAL_CAPS                "AMEX_Enhanced_Reader_Capabilties"   ///<  @n TLV tag # @n TLV tag #TAG_9F6E_AMEX_ENHANCED_CAPABILITIES, @n AMEX_TerminalCaps in EMV_CTLS_APPLIDATA_STRUCT::AMEX_TerminalCaps, @n XML Tag: #XML_TAG_AMEX_TERMINAL_CAPS, @n AMEX_Enhanced_TerminalCaps in EMV_CTLS_APPLIDATA_STRUCT::AMEX_TerminalCaps, @n XML Tag: #XML_TAG_AMEX_ENH_TERMINAL_CAPS
#define XML_TAG_APPLIDATA_CHKSUM_PARAMS               "ChksumParams"                       ///<  @n TLV tag #TAG_DF13_TERM_PARAM, @n Chksum_Params in EMV_CTLS_APPLIDATA_STRUCT::Chksum_Params, @n XML Tag: #XML_TAG_APPLIDATA_CHKSUM_PARAMS
#define XML_TAG_APPLIDATA_CHKSUM_ASCII_ENTRYPNT       "Chksum_EntryPoint"                  ///<  @n TLV tag #TAG_DF3B_PARAMETER_1, @n Chksum_EntryPoint in EMV_CTLS_APPLIDATA_STRUCT::Chksum_EntryPoint, @n XML Tag: #XML_TAG_APPLIDATA_CHKSUM_ASCII_ENTRYPNT
#define XML_TAG_APPLIDATA_CHKSUM_ASCII_KERNEL         "Chksum_Kernel"                      ///<  @n TLV tag #TAG_DF12_CHECKSUM, @n Chksum_Kernel in EMV_CTLS_APPLIDATA_STRUCT::Chksum_Kernel, @n XML Tag: #XML_TAG_APPLIDATA_CHKSUM_ASCII_KERNEL
#define XML_TAG_APPLIDATA_MASTER_AID                  "MasterAID"                          ///<  @n TLV tag #TAG_DF04_AID, @n MasterAID in EMV_CTLS_APPLIDATA_STRUCT::MasterAID, @n XML Tag: #XML_TAG_APPLIDATA_MASTER_AID

// new config interface

#define XML_TAG_AD                                    "ApplicationData"
#define XML_TAG_AD_APP                                "Application"
#define XML_TAG_AD_AID                                "AID"
#define XML_TAG_AD_KERNEL_ID                          "KernelID"

// global

#define XML_TAG_AD_DFAB02_ASI                         "ASI_DFAB02"                            ///<  @n TLV tag: #TAG_DFAB02_ASI                @n Struct: #EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::ASI_DFAB02;
#define XML_TAG_AD_DFAB03_APP_FLOW_CAP                "AppFlowCap_DFAB03"                     ///<  @n TLV tag: #TAG_DFAB03_APP_FLOW_CAP       @n Struct: #EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::AppFlowCap_DFAB03[5];
#define XML_TAG_AD_DFAB04_PRIO_APPS                   "PriorityApplications_DFAB04"           ///<  @n TLV tag: #TAG_DFAB04_PRIO_APPS          @n Struct: #EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::PriorityApplications_DFAB04[EMV_ADK_MAX_PRIO_APP];
#define XML_TAG_AD_DFAB05_SPECIAL_TRX_CONFIG          "SpecialTRXConfig_DFAB05"               ///<  @n TLV tag: #TAG_DFAB05_SPECIAL_TRX_CFG    @n Struct: #EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::SpecialTRXConfig_DFAB05[8];
#define XML_TAG_AD_DFAB06_CHKSUM_ENTRY_POINT          "ChksumEntryPoint_DFAB06"               ///<  @n TLV tag: #TAG_DFAB06_CHKSUM_EP          @n Struct: #EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::ChksumEntryPoint_DFAB06[EMV_ADK_CHECKSUM_ASCII_SIZE];
#define XML_TAG_AD_DFAB07_CHKSUM_KERNEL               "ChksumKernel_DFAB07"                   ///<  @n TLV tag: #TAG_DFAB07_CHKSUM_KERNEL      @n Struct: #EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::ChksumKernel_DFAB07[EMV_ADK_CHECKSUM_ASCII_SIZE];
#define XML_TAG_AD_DFAB08_RETAP_FIELD_OFF             "RetapFieldOff_DFAB08"                  ///<  @n TLV tag: #TAG_DFAB08_RETAP_FIELD_OFF    @n Struct: #EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::RetapFieldOff_DFAB08;
#define XML_TAG_AD_DFAB20_ADD_TAGS_TRM                "AdditionalTagsTRM_DFAB20"              ///<  @n TLV tag: #TAG_DFAB20_ADD_TAGS_TRM       @n Struct: #EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::AdditionalTagsTRM_DFAB20;
#define XML_TAG_AD_DFAB21_ADD_TAGS_CRD                "AdditionalTagsCRD_DFAB21"              ///<  @n TLV tag: #TAG_DFAB21_ADD_TAGS_CRD       @n Struct: #EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::AdditionalTagsCRD_DFAB21;
#define XML_TAG_AD_DFAB22_DEFAULT_APP_NAME            "DefaultApplicationName_DFAB22"         ///<  @n TLV tag: #TAG_DFAB22_DEF_APP_NAME       @n Struct: #EMV_CTLS_APPLIDATA_SCHEME_SPECIFIC_STRUCT::DefaultApplicationName_DFAB22[16+1];

// MasterCard

#define XML_TAG_AD_MK                                 "MasterCard"
#define XML_TAG_AD_GIROCARD                           "girocard"
#define XML_TAG_AD_MK_9F1C_TERM_IDENT                 "TermIdent_9F1C"                        ///<  @n TLV tag: #TAG_9F1C_TRM_ID                                    @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::TermIdent_9F1C[8];
#define XML_TAG_AD_MK_9F1A_TERM_COUNTRY_CODE          "TerminalCountryCode_9F1A"              ///<  @n TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE                          @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::TerminalCountryCode_9F1A[2];
#define XML_TAG_AD_MK_9F35_TERM_TYPE                  "TerminalType_9F35"                     ///<  @n TLV tag: #TAG_9F35_TRM_TYPE                                  @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::TerminalType_9F35;
#define XML_TAG_AD_MK_9F40_ADD_TERM_CAPS              "AdditionalTerminalCapabilities_9F40"   ///<  @n TLV tag: #TAG_9F40_ADD_TRM_CAP                               @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::AdditionalTerminalCapabilities_9F40[5];
#define XML_TAG_AD_MK_DF811E_MSR_CVM_ABOVE_LIMIT      "MagstripeCVM_aboveLimit_DF811E"        ///<  @n TLV tag: #TAG_DF811E_MSR_CVM_ABOVE_LIMIT                     @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::MagstripeCVM_aboveLimit_DF811E;
#define XML_TAG_AD_MK_DF812C_MSR_CVM_BELOW_LIMIT      "MagstripeCVM_belowLimit_DF812C"        ///<  @n TLV tag: #TAG_DF812C_MSR_CVM_BELOW_LIMIT                     @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::MagstripeCVM_belowLimit_DF812C;
#define XML_TAG_AD_MK_DF8118_CHP_CVM_ABOVE_LIMIT      "ChipCVM_aboveLimit_DF8118"             ///<  @n TLV tag: #TAG_DF8118_CVM_CAPABILITY__CVM_REQUIRED            @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::ChipCVM_aboveLimit_DF8118;
#define XML_TAG_AD_MK_DF8119_CHP_CVM_BELOW_LIMIT      "ChipCVM_belowLimit_DF8119"             ///<  @n TLV tag: #TAG_DF8119_CVM_CAPABILITY__NO_CVM_REQUIRED         @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::ChipCVM_belowLimit_DF8119;
#define XML_TAG_AD_MK_DF811F_SECURITY_CAP             "SecurityCapability_DF811F"             ///<  @n TLV tag: #TAG_DF811F_SECURITY_CAPABILITY                     @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::SecurityCapability_DF811F;
#define XML_TAG_AD_MK_DF8117_CARD_DATA_INPUT_CAP      "CardDataInputCapability_DF8117"        ///<  @n TLV tag: #TAG_DF8117_CARD_DATA_INPUT_CAPABILITY              @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::CardDataInputCapability_DF8117;
#define XML_TAG_AD_MK_DF8123_FLOOR_LIMIT              "FloorLimit_DF8123"                     ///<  @n TLV tag: #TAG_DF8123_FLOOR_LIMIT                             @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::FloorLimit_DF8123[6];
#define XML_TAG_AD_MK_DF8124_TRX_LIMIT_NO_ON_DEVICE   "TransactionLimitNoOnDevice_DF8124"     ///<  @n TLV tag: #TAG_DF8124_READER_CTLS_TRX_LIMIT__NO_ON_DEV_CVM    @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::TransactionLimitNoOnDevice_DF8124[6];
#define XML_TAG_AD_MK_DF8125_TRX_LIMIT_ON_DEVICE      "TransactionLimitOnDevice_DF8125"       ///<  @n TLV tag: #TAG_DF8125_READER_CTLS_TRX_LIMIT__ON_DEVICE_CVM    @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::TransactionLimitOnDevice_DF8125[6];
#define XML_TAG_AD_MK_DF8126_CVM_REQUIRED_LIMIT       "CVMRequiredLimit_DF8126"               ///<  @n TLV tag: #TAG_DF8126_CVM_REQUIRED_LIMIT                      @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::CVMRequiredLimit_DF8126[6];
#define XML_TAG_AD_MK_9F09_CHP_VERSION_NUMBER         "ChipVersionNumber_9F09"                ///<  @n TLV tag: #TAG_9F09_TRM_APP_VERSION_NB                        @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::ChipVersionNumber_9F09[2*EMV_CTLS_MAX_APP_VERS];
#define XML_TAG_AD_MK_9F6D_MSR_VERSION_NUMBER         "MSRVersionNumber_9F6D"                 ///<  @n TLV tag: #TAG_9F6D_TRM_APP_MSR_VERSION_NB                    @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::MSRVersionNumber_9F6D[2*EMV_CTLS_MAX_APP_VERS];
#define XML_TAG_AD_MK_DF811B_KERNEL_CONFIG            "KernelConfiguration_DF811B"            ///<  @n TLV tag: #TAG_DF811B_KERNEL_CONFIGURATION                    @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::KernelConfiguration_DF811B;
#define XML_TAG_AD_MK_9F53_TRX_CATEGORY_CODE          "TransactionCategoryCode_9F53"          ///<  @n TLV tag: #TAG_9F53_TRANS_CATEGORY_CODE                       @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::TransactionCategoryCode_9F53;
#define XML_TAG_AD_MK_DF8120_TAC_DEFAULT              "TACDefault_DF8120"                     ///<  @n TLV tag: #TAG_DF8120_TAC_DEFAULT                             @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::TACDefault_DF8120[5];
#define XML_TAG_AD_MK_DF8121_TAC_DENIAL               "TACDenial_DF8121"                      ///<  @n TLV tag: #TAG_DF8121_TAC_DENIAL                              @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::TACDenial_DF8121[5];
#define XML_TAG_AD_MK_DF8122_TAC_ONLINE               "TACOnline_DF8122"                      ///<  @n TLV tag: #TAG_DF8122_TAC_ONLINE                              @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::TACOnline_DF8122[5];
#define XML_TAG_AD_MK_DF810C_KERNEL_ID                "KernelID_DF810C"                       ///<  @n TLV tag: #TAG_DF810C_KERNEL_ID                               @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::KernelID_DF810C;
#define XML_TAG_AD_MK_9F1D_TRM_RISK_MGMT_DATA         "TerminalRiskManagementData_9F1D"       ///<  @n TLV tag: #TAG_9F1D_TRM_RISK_MNGT_DATA                        @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::TerminalRiskManagementData_9F1D[8];
#define XML_TAG_AD_MK_9F15_MERCHANT_CATEGORY_CODE     "MerchantCategoryCode_9F15"             ///<  @n TLV tag: #TAG_9F15_MERCH_CATEG_CODE                          @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::MerchantCategoryCode_9F15[2];
#define XML_TAG_AD_MK_9F16_MERCHANT_ID                "MerchantIdentifier_9F16"               ///<  @n TLV tag: #TAG_9F16_MERCHANT_ID                               @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::MerchantIdentifier_9F16[15+1];
#define XML_TAG_AD_MK_9F4E_MERCHANT_NAME_LOCATION     "MerchantNameAndLocation_9F4E"          ///<  @n TLV tag: #TAG_9F4E_TAC_MERCHANTLOC                           @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::MerchantNameAndLocation_9F4E[40+1];
#define XML_TAG_AD_MK_9F01_ACQUIRER_ID                "AcquirerIdentifier_9F01"               ///<  @n TLV tag: #TAG_9F01_ACQ_ID                                    @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::AcquirerIdentifier_9F01[6];
#define XML_TAG_AD_MK_DF8130_HOLD_TIME_VALUE          "HoldTimeValue_DF8130"                  ///<  @n TLV tag: #TAG_DF8130_HOLD_TIME_VALUE                         @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::HoldTimeValue_DF8130;
#define XML_TAG_AD_MK_DF812D_MESSAGE_HOLD_TIME        "MessageHoldTime_DF812D"                ///<  @n TLV tag: #TAG_DF812D_MESSAGE_HOLD_TIME                       @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::MessageHoldTime_DF812D[3];
#define XML_TAG_AD_MK_DF811C_TORN_TRX_LIFETIME        "TornTransactionLifetime_DF811C"        ///<  @n TLV tag: #TAG_DF811C_TRN_TXN_LIFETIME                        @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::TornTransactionLifetime_DF811C[2];
#define XML_TAG_AD_MK_DF811D_TORN_TRX_NUMBER          "TornTransactionNumber_DF811D"          ///<  @n TLV tag: #TAG_DF811D_TRN_TXN_NUMBER                          @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::TornTransactionNumber_DF811D;
#define XML_TAG_AD_MK_DF8131_PHONE_MSG_TABLE          "PhoneMessageTable_DF8131"              ///<  @n TLV tag: #TAG_DF8131_PHONE_MSG_TABLE                         @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::PhoneMessageTable_DF8131;
#define XML_TAG_AD_MK_DF8112_TAGS_TO_READ                  "TagsToRead_DF8112"                ///<  @n TLV tag: #TAG_DF8112_TAGS_TO_READ                            @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::TagsToRead_DF8112;
#define XML_TAG_AD_MK_FF8102_TAGS_TO_WRITE_BEFORE_GEN_AC   "TagsToWriteBeforeGenAC_FF8102"    ///<  @n TLV tag: #TAG_FF8102_TAGS_TO_WRITE_BEFORE_GEN_AC             @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::TagsToWriteBeforeGenAC_FF8102;
#define XML_TAG_AD_MK_FF8103_TAGS_TO_WRITE_AFTER_GEN_AC    "TagsToWriteAfterGenAC_FF8103"     ///<  @n TLV tag: #TAG_FF8103_TAGS_TO_WRITE_AFTER_GEN_AC              @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::TagsToWriteAfterGenAC_FF8103;
#define XML_TAG_AD_MK_DF8110_PROCEED_TO_FIRST_WRITE_FLAG   "ProceedToFirstWriteFlag_DF8110"   ///<  @n TLV tag: #TAG_DF8110_PROCEED_TO_FIRST_WRITE_FLAG             @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::ProceedToFirstWriteFlag_DF8110;
#define XML_TAG_AD_MK_9F5C_DS_REQUESTED_OPERATOR_ID        "DSRequestedOperatorID_9F5C"       ///<  @n TLV tag: #TAG_9F5C_DS_REQUESTED_OPERATOR_ID                  @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::DSRequestedOperatorID_9F5C;
#define XML_TAG_AD_MK_DF8127_DE_TIMEOUT_VALUE              "DETimeoutValue_DF8127"            ///<  @n TLV tag: #TAG_DF8127_DE_TIMEOUT_VALUE                        @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::DETimeoutValue_DF8127;
#define XML_TAG_AD_MK_DFAB31_APP_FLOW_CAP             "AppFlowCap_DFAB31"                     ///<  @n TLV tag: #TAG_DFAB31_APP_FLOW_CAP                            @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::AppFlowCap_DFAB31[5];
#define XML_TAG_AD_MK_DF8132_RR_MIN_GRACE_PERIOD      "RR_MinGracePeriod_DF8132"              ///<  @n TLV tag: #TAG_DF8132_RR_MIN_GRACE_PERIOD                     @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::RR_MinGracePeriod_DF8132
#define XML_TAG_AD_MK_DF8133_RR_MAX_GRACE_PREIOD      "RR_MaxGracePeriod_DF8133"              ///<  @n TLV tag: #TAG_DF8133_RR_MAX_GRACE_PERIOD                     @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::RR_MaxGracePeriod_DF8133
#define XML_TAG_AD_MK_DF8134_RR_EXP_TRANS_TIME_CAPDU  "RR_ExpTransTimeCAPDU_DF8134"           ///<  @n TLV tag: #TAG_DF8134_RR_TERM_EXPECTED_TRANS_TIME_CAPDU       @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::RR_ExpectedTransTime_CAPDU_DF8134
#define XML_TAG_AD_MK_DF8135_RR_EXP_TRANS_TIME_RAPDU  "RR_ExpTransTimeRAPDU_DF8135"           ///<  @n TLV tag: #TAG_DF8135_RR_TERM_EXPECTED_TRANS_TIME_RAPDU       @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::RR_ExpectedTransTime_RAPDU_DF8135
#define XML_TAG_AD_MK_DF8136_RR_ACCURACY_THRESHOLD    "RR_AccuracyThreshold_DF8136"           ///<  @n TLV tag: #TAG_DF8136_RR_ACCURACY_THRESHOLD                   @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::RR_AccuracyThreshold_DF8136
#define XML_TAG_AD_MK_DF8137_RR_TT_MISMATCH_THRESHOLD "RR_TransTimeMismatchThreshold_DF8137"  ///<  @n TLV tag: #TAG_DF8137_RR_TRANS_TIME_MISMATCH_THRESHOLD        @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::RR_TransTimeMismatchThreshold_DF8137
#define XML_TAG_AD_MK_9F7C_MERCHANT_CUSTOM_DATA       "MerchantCustomData_9F7C"               ///<  @n TLV tag: #TAG_9F7C_MERCHANT_CUSTOM_DATA                      @n Struct: #EMV_CTLS_APPLIDATA_MK_STRUCT::MerchantCustomData_9F7C

// Visa
#define XML_TAG_AD_VK                                 "Visa"
#define XML_TAG_AD_VK_9F1C_TERM_IDENT                 "TermIdent_9F1C"                        ///<  @n TLV tag: #TAG_9F1C_TRM_ID                           @n Struct: #EMV_CTLS_APPLIDATA_VK_STRUCT::TermIdent_9F1C[8];
#define XML_TAG_AD_VK_9F1A_TERM_COUNTRY_CODE          "TerminalCountryCode_9F1A"              ///<  @n TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE                 @n Struct: #EMV_CTLS_APPLIDATA_VK_STRUCT::TerminalCountryCode_9F1A[2];
#define XML_TAG_AD_VK_9F35_TERM_TYPE                  "TerminalType_9F35"                     ///<  @n TLV tag: #TAG_9F35_TRM_TYPE                         @n Struct: #EMV_CTLS_APPLIDATA_VK_STRUCT::TerminalType_9F35;
#define XML_TAG_AD_VK_9F66_TERM_TRX_QUALIFIER         "TerminalTransactionQualifier_9F66"     ///<  @n TLV tag: #TAG_9F66_TTQ                              @n Struct: #EMV_CTLS_APPLIDATA_VK_STRUCT::TerminalTransactionQualifier_9F66[4];
#define XML_TAG_AD_VK_9F33_TERM_CAPS                  "TerminalCapabilities_9F33"             ///<  @n TLV tag: #TAG_9F33_TRM_CAPABILITIES                 @n Struct: #EMV_CTLS_APPLIDATA_VK_STRUCT::TerminalCapabilities_9F33[3];
#define XML_TAG_AD_VK_9F40_ADD_TERM_CAPS              "AdditionalTerminalCapabilities_9F40"   ///<  @n TLV tag: #TAG_9F40_ADD_TRM_CAP                      @n Struct: #EMV_CTLS_APPLIDATA_VK_STRUCT::AdditionalTerminalCapabilities_9F40[5];
#define XML_TAG_AD_VK_9F09_VERSION_NUMBER             "VersionNumber_9F09"                    ///<  @n TLV tag: #TAG_9F09_TRM_APP_VERSION_NB               @n Struct: #EMV_CTLS_APPLIDATA_VK_STRUCT::VersionNumber_9F09[2*EMV_CTLS_MAX_APP_VERS];
#define XML_TAG_AD_VK_9F15_MERCHANT_CATEGORY_CODE     "MerchantCategoryCode_9F15"             ///<  @n TLV tag: #TAG_9F15_MERCH_CATEG_CODE                 @n Struct: #EMV_CTLS_APPLIDATA_VK_STRUCT::MerchantCategoryCode_9F15[2];
#define XML_TAG_AD_VK_9F16_MERCHANT_ID                "MerchantIdentifier_9F16"               ///<  @n TLV tag: #TAG_9F16_MERCHANT_ID                      @n Struct: #EMV_CTLS_APPLIDATA_VK_STRUCT::MerchantIdentifier_9F16[15+1];
#define XML_TAG_AD_VK_9F4E_MERCHANT_NAME_LOCATION     "MerchantNameAndLocation_9F4E"          ///<  @n TLV tag: #TAG_9F4E_TAC_MERCHANTLOC                  @n Struct: #EMV_CTLS_APPLIDATA_VK_STRUCT::MerchantNameAndLocation_9F4E[40+1];
#define XML_TAG_AD_VK_DFAB30_TEC_SUPPORT              "TecSupport_DFAB30"                     ///<  @n TLV tag: #TAG_DFAB30_TEC_SUPPORT                    @n Struct: #EMV_CTLS_APPLIDATA_VK_STRUCT::TecSupport_DFAB30;
#define XML_TAG_AD_VK_DFAB31_APP_FLOW_CAP             "AppFlowCap_DFAB31"                     ///<  @n TLV tag: #TAG_DFAB31_APP_FLOW_CAP                   @n Struct: #EMV_CTLS_APPLIDATA_VK_STRUCT::AppFlowCap_DFAB31[5];
#define XML_TAG_AD_VK_DFAB40_CTLS_FLOOR_LIMIT         "ContactlessFloorLimit_DFAB40"          ///<  @n TLV tag: #TAG_DFAB40_CTLS_FLOOR_LIMIT               @n Struct: #EMV_CTLS_APPLIDATA_VK_STRUCT::ContactlessFloorLimit_DFAB40[6];
#define XML_TAG_AD_VK_DFAB41_CTLS_TRX_LIMIT           "ContactlessTransactionLimit_DFAB41"    ///<  @n TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT                 @n Struct: #EMV_CTLS_APPLIDATA_VK_STRUCT::ContactlessTransactionLimit_DFAB41[6];
#define XML_TAG_AD_VK_DFAB42_CTLS_CVM_REQ_LIMIT       "ContactlessCVMRequiredLimit_DFAB42"    ///<  @n TLV tag: #TAG_DFAB42_CTLS_CVM_REQ_LIMIT             @n Struct: #EMV_CTLS_APPLIDATA_VK_STRUCT::ContactlessCVMRequiredLimit_DFAB42[6];
#define XML_TAG_AD_VK_FFAB01_VISA_DRL_PARAMS          "VisaDRLParams_FFAB01"                  ///<  @n TLV tag: #TAG_FFAB01_DRL_PARAMETER                  @n Struct: #EMV_CTLS_APPLIDATA_VK_STRUCT::VisaDRLParams_FFAB01;
#define XML_TAG_AD_VK_9F5A_APP_PROGRAM_ID             "AppProgramId_9F5A"                     ///<  @n TLV tag: #TAG_9F5A_APP_PROGRAM_ID                   @n Struct: #EMV_CTLS_VK_DRL_ENTRY_STRUCT::AppProgramId_9F5A[16]
#define XML_TAG_AD_VK_DFAB49_ON_OFF_SWITCH            "OnOffSwitch_DFAB49"                    ///<  @n TLV tag: #TAG_DFAB49_DRL_SWITCHES                   @n Struct: #EMV_CTLS_VK_DRL_ENTRY_STRUCT::OnOffSwitch_DFAB49

// American Express

#define XML_TAG_AD_AK                                 "AmericanExpress"
#define XML_TAG_AD_AK_9F1C_TERM_IDENT                 "TermIdent_9F1C"                            ///<  @n TLV tag: #TAG_9F1C_TRM_ID                       @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::TermIdent_9F1C[8];
#define XML_TAG_AD_AK_9F1A_TERM_COUNTRY_CODE          "TerminalCountryCode_9F1A"                  ///<  @n TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE             @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::TerminalCountryCode_9F1A[2];
#define XML_TAG_AD_AK_9F35_TERM_TYPE                  "TerminalType_9F35"                         ///<  @n TLV tag: #TAG_9F35_TRM_TYPE                     @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::TerminalType_9F35;
#define XML_TAG_AD_AK_9F6D_AMEX_CTLS_READER_CAPS      "AmexContactlessReaderCapabilities_9F6D"    ///<  @n TLV tag: #TAG_9F6D_AMEX_CAPABILITIES            @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::AmexContactlessReaderCapabilities_9F6D;
#define XML_TAG_AD_AK_9F6E_AMEX_TERM_TRX_CAPS         "AmexTerminalTransactionCapabilities_9F6E"  ///<  @n TLV tag: #TAG_9F6E_AMEX_ENHANCED_CAPABILITIES   @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::AmexTerminalTransactionCapabilities_9F6E[4];
#define XML_TAG_AD_AK_9F33_TERM_CAPS                  "TerminalCapabilities_9F33"                 ///<  @n TLV tag: #TAG_9F33_TRM_CAPABILITIES             @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::TerminalCapabilities_9F33[3];
#define XML_TAG_AD_AK_9F40_ADD_TERM_CAPS              "AdditionalTerminalCapabilities_9F40"       ///<  @n TLV tag: #TAG_9F40_ADD_TRM_CAP                  @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::AdditionalTerminalCapabilities_9F40[5];
#define XML_TAG_AD_AK_9F09_VERSION_NUMBER             "VersionNumber_9F09"                        ///<  @n TLV tag: #TAG_9F09_TRM_APP_VERSION_NB           @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::VersionNumber_9F09[2*EMV_CTLS_MAX_APP_VERS];
#define XML_TAG_AD_AK_9F15_MERCHANT_CATEGORY_CODE     "MerchantCategoryCode_9F15"                 ///<  @n TLV tag: #TAG_9F15_MERCH_CATEG_CODE             @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::MerchantCategoryCode_9F15[2];
#define XML_TAG_AD_AK_9F16_MERCHANT_ID                "MerchantIdentifier_9F16"                   ///<  @n TLV tag: #TAG_9F16_MERCHANT_ID                  @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::MerchantIdentifier_9F16[15+1];
#define XML_TAG_AD_AK_9F4E_MERCHANT_NAME_LOCATION     "MerchantNameAndLocation_9F4E"              ///<  @n TLV tag: #TAG_9F4E_TAC_MERCHANTLOC              @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::MerchantNameAndLocation_9F4E[40+1];
#define XML_TAG_AD_AK_9F01_ACQUIRER_ID                "AcquirerIdentifier_9F01"                   ///<  @n TLV tag: #TAG_9F01_ACQ_ID                       @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::AcquirerIdentifier_9F01[6];
#define XML_TAG_AD_AK_DFAB30_TEC_SUPPORT              "TecSupport_DFAB30"                         ///<  @n TLV tag: #TAG_DFAB30_TEC_SUPPORT                @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::TecSupport_DFAB30;
#define XML_TAG_AD_AK_DFAB31_APP_FLOW_CAP             "AppFlowCap_DFAB31"                         ///<  @n TLV tag: #TAG_DFAB31_APP_FLOW_CAP               @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::AppFlowCap_DFAB31[5];
#define XML_TAG_AD_AK_DFAB40_CTLS_FLOOR_LIMIT         "ContactlessFloorLimit_DFAB40"              ///<  @n TLV tag: #TAG_DFAB40_CTLS_FLOOR_LIMIT           @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::ContactlessFloorLimit_DFAB40[6];
#define XML_TAG_AD_AK_DFAB41_CTLS_TRX_LIMIT           "ContactlessTransactionLimit_DFAB41"        ///<  @n TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT             @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::ContactlessTransactionLimit_DFAB41[6];
#define XML_TAG_AD_AK_DFAB42_CTLS_CVM_REQ_LIMIT       "ContactlessCVMRequiredLimit_DFAB42"        ///<  @n TLV tag: #TAG_DFAB42_CTLS_CVM_REQ_LIMIT         @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::ContactlessCVMRequiredLimit_DFAB42[6];
#define XML_TAG_AD_AK_DFAB43_TAC_DEFAULT              "TACDefault_DFAB43"                         ///<  @n TLV tag: #TAG_DFAB43_TAC_DEFAULT                @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::TACDefault_DFAB43[5];
#define XML_TAG_AD_AK_DFAB44_TAC_DENIAL               "TACDenial_DFAB44"                          ///<  @n TLV tag: #TAG_DFAB44_TAC_DENIAL                 @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::TACDenial_DFAB44[5];
#define XML_TAG_AD_AK_DFAB45_TAC_ONLINE               "TACOnline_DFAB45"                          ///<  @n TLV tag: #TAG_DFAB45_TAC_ONLINE                 @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::TACOnline_DFAB45[5];
#define XML_TAG_AD_AK_FFAB01_AMEX_DRL_PARAMS          "AmexDRLParams_FFAB01"                      ///<  @n TLV tag: #TAG_FFAB01_DRL_PARAMETER              @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::AmexDRLParams_FFAB01;
#define XML_TAG_AD_AK_DFAB49_ON_OFF_SWITCH            "OnOffSwitch_DFAB49"                        ///<  @n TLV tag: #TAG_DFAB49_DRL_SWITCHES               @n Struct: #EMV_CTLS_AK_DRL_ENTRY_STRUCT::OnOffSwitch_DFAB49
#define XML_TAG_AD_AK_DFAB52_UN_RANGE                 "UnpredictableNumberRange_DFAB52"           ///<  @n TLV tag: #TAG_DFAB52_AMEX_UN_RANGE              @n Struct: #EMV_CTLS_APPLIDATA_AK_STRUCT::UnpredictableNumberRange_DFAB52

// JCB

#define XML_TAG_AD_JK                                 "JCB"
#define XML_TAG_AD_JK_9F1C_TERM_IDENT                 "TermIdent_9F1C"                                 ///<  @n TLV tag: #TAG_9F1C_TRM_ID                      @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::TermIdent_9F1C[8];
#define XML_TAG_AD_JK_9F1A_TERM_COUNTRY_CODE          "TerminalCountryCode_9F1A"                       ///<  @n TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE            @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::TerminalCountryCode_9F1A[2];
#define XML_TAG_AD_JK_9F35_TERM_TYPE                  "TerminalType_9F35"                              ///<  @n TLV tag: #TAG_9F35_TRM_TYPE                    @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::TerminalType_9F35;
#define XML_TAG_AD_JK_9F53_TERM_INTERCHANGE_PROFILE   "TerminalInterchangeProfile_9F53"                ///<  @n TLV tag: #TAG_9F53_TRM_INTERCHANGE_PROFILE     @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::TerminalInterchangeProfile_9F53[3];
#define XML_TAG_AD_JK_9F15_MERCHANT_CATEGORY_CODE     "MerchantCategoryCode_9F15"                      ///<  @n TLV tag: #TAG_9F15_MERCH_CATEG_CODE            @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::MerchantCategoryCode_9F15[2];
#define XML_TAG_AD_JK_9F4E_MERCHANT_NAME_LOCATION     "MerchantNameAndLocation_9F4E"                   ///<  @n TLV tag: #TAG_9F4E_TAC_MERCHANTLOC             @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::MerchantNameAndLocation_9F4E[40+1];
#define XML_TAG_AD_JK_9F01_ACQUIRER_ID                "AcquirerIdentifier_9F01"                        ///<  @n TLV tag: #TAG_9F01_ACQ_ID                      @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::AcquirerIdentifier_9F01[6];
#define XML_TAG_AD_JK_DFAB31_APP_FLOW_CAP             "AppFlowCap_DFAB31"                              ///<  @n TLV tag: #TAG_DFAB31_APP_FLOW_CAP              @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::AppFlowCap_DFAB31[5];
#define XML_TAG_AD_JK_DFAB40_CTLS_FLOOR_LIMIT         "ContactlessFloorLimit_DFAB40"                   ///<  @n TLV tag: #TAG_DFAB40_CTLS_FLOOR_LIMIT          @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::ContactlessFloorLimit_DFAB40[6];
#define XML_TAG_AD_JK_DFAB41_CTLS_TRX_LIMIT           "ContactlessTransactionLimit_DFAB41"             ///<  @n TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT            @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::ContactlessTransactionLimit_DFAB41[6];
#define XML_TAG_AD_JK_DFAB42_CTLS_CVM_REQ_LIMIT       "ContactlessCVMRequiredLimit_DFAB42"             ///<  @n TLV tag: #TAG_DFAB42_CTLS_CVM_REQ_LIMIT        @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::ContactlessCVMRequiredLimit_DFAB42[6];
#define XML_TAG_AD_JK_DFAB43_TAC_DEFAULT              "TACDefault_DFAB43"                              ///<  @n TLV tag: #TAG_DFAB43_TAC_DEFAULT               @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::TACDefault_DFAB43[5];
#define XML_TAG_AD_JK_DFAB44_TAC_DENIAL               "TACDenial_DFAB44"                               ///<  @n TLV tag: #TAG_DFAB44_TAC_DENIAL                @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::TACDenial_DFAB44[5];
#define XML_TAG_AD_JK_DFAB45_TAC_ONLINE               "TACOnline_DFAB45"                               ///<  @n TLV tag: #TAG_DFAB45_TAC_ONLINE                @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::TACOnline_DFAB45[5];
#define XML_TAG_AD_JK_DFAB46_RISK_MGMT_THRESHOLD      "RiskManagementThreshold_DFAB46"                 ///<  @n TLV tag: #TAG_DFAB46_RISK_MGMT_THRESHOLD       @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::RiskManagementThreshold_DFAB46[6];
#define XML_TAG_AD_JK_DFAB47_RISK_MGMT_TRGT_PERC      "RiskManagementTargetPercentage_DFAB47"          ///<  @n TLV tag: #TAG_DFAB47_RISK_MGMT_TRGT_PRCT       @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::RiskManagementTargetPercentage_DFAB47;
#define XML_TAG_AD_JK_DFAB48_RISK_MGMT_MAX_TRGT_PERC  "RiskManagementMaximumTargetPercentage_DFAB48"   ///<  @n TLV tag: #TAG_DFAB48_RISK_MGMT_MAX_TRGT_PRCT   @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::RiskManagementMaximumTargetPercentage_DFAB48;
#define XML_TAG_AD_JK_DFAB4B_COMBINATION_OPTIONS      "CombinationOptions_DFAB4B"                      ///<  @n TLV tag: #TAG_DFAB4B_COMBINATION_OPTIONS       @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::CombinationOptions_DFAB4B[2];
#define XML_TAG_AD_JK_DFAB4C_REMOVAL_TIMEOUT          "RemovalTimeout_DFAB4C"                          ///<  @n TLV tag: #TAG_DFAB4C_REMOVAL_TIMEOUT           @n Struct: #EMV_CTLS_APPLIDATA_JK_STRUCT::RemovalTimeout_DFAB4C[2];

// Discover

#define XML_TAG_AD_DK                                 "Discover"
#define XML_TAG_AD_DK_9F1C_TERM_IDENT                 "TermIdent_9F1C"                                 ///<  @n TLV tag: #TAG_9F1C_TRM_ID                      @n Struct: #EMV_CTLS_APPLIDATA_DK_STRUCT::TermIdent_9F1C[8];
#define XML_TAG_AD_DK_9F1A_TERM_COUNTRY_CODE          "TerminalCountryCode_9F1A"                       ///<  @n TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE            @n Struct: #EMV_CTLS_APPLIDATA_DK_STRUCT::TerminalCountryCode_9F1A[2];
#define XML_TAG_AD_DK_9F35_TERM_TYPE                  "TerminalType_9F35"                              ///<  @n TLV tag: #TAG_9F35_TRM_TYPE                    @n Struct: #EMV_CTLS_APPLIDATA_DK_STRUCT::TerminalType_9F35;
#define XML_TAG_AD_DK_9F66_TERM_TRX_QUALIFIER         "TerminalTransactionQualifier_9F66"              ///<  @n TLV tag: #TAG_9F66_TTQ                         @n Struct: #EMV_CTLS_APPLIDATA_DK_STRUCT::TerminalTransactionQualifier_9F66[4];
#define XML_TAG_AD_DK_9F33_TERM_CAPS                  "TerminalCapabilities_9F33"                      ///<  @n TLV tag: #TAG_9F33_TRM_CAPABILITIES            @n Struct: #EMV_CTLS_APPLIDATA_DK_STRUCT::TerminalCapabilities_9F33[3];
#define XML_TAG_AD_DK_9F40_ADD_TERM_CAPS              "AdditionalTerminalCapabilities_9F40"            ///<  @n TLV tag: #TAG_9F40_ADD_TRM_CAP                 @n Struct: #EMV_CTLS_APPLIDATA_DK_STRUCT::AdditionalTerminalCapabilities_9F40[5];
#define XML_TAG_AD_DK_9F09_VERSION_NUMBER             "VersionNumber_9F09"                             ///<  @n TLV tag: #TAG_9F09_TRM_APP_VERSION_NB          @n Struct: #EMV_CTLS_APPLIDATA_DK_STRUCT::VersionNumber_9F09[2*EMV_CTLS_MAX_APP_VERS];
#define XML_TAG_AD_DK_9F15_MERCHANT_CATEGORY_CODE     "MerchantCategoryCode_9F15"                      ///<  @n TLV tag: #TAG_9F15_MERCH_CATEG_CODE            @n Struct: #EMV_CTLS_APPLIDATA_DK_STRUCT::MerchantCategoryCode_9F15[2];
#define XML_TAG_AD_DK_DFAB30_TEC_SUPPORT              "TecSupport_DFAB30"                              ///<  @n TLV tag: #TAG_DFAB30_TEC_SUPPORT               @n Struct: #EMV_CTLS_APPLIDATA_DK_STRUCT::TecSupport_DFAB30;
#define XML_TAG_AD_DK_DFAB31_APP_FLOW_CAP             "AppFlowCap_DFAB31"                              ///<  @n TLV tag: #TAG_DFAB31_APP_FLOW_CAP              @n Struct: #EMV_CTLS_APPLIDATA_DK_STRUCT::AppFlowCap_DFAB31[5];
#define XML_TAG_AD_DK_DFAB40_CTLS_FLOOR_LIMIT         "ContactlessFloorLimit_DFAB40"                   ///<  @n TLV tag: #TAG_DFAB40_CTLS_FLOOR_LIMIT          @n Struct: #EMV_CTLS_APPLIDATA_DK_STRUCT::ContactlessFloorLimit_DFAB40[6];
#define XML_TAG_AD_DK_DFAB41_CTLS_TRX_LIMIT           "ContactlessTransactionLimit_DFAB41"             ///<  @n TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT            @n Struct: #EMV_CTLS_APPLIDATA_DK_STRUCT::ContactlessTransactionLimit_DFAB41[6];
#define XML_TAG_AD_DK_DFAB42_CTLS_CVM_REQ_LIMIT       "ContactlessCVMRequiredLimit_DFAB42"             ///<  @n TLV tag: #TAG_DFAB42_CTLS_CVM_REQ_LIMIT        @n Struct: #EMV_CTLS_APPLIDATA_DK_STRUCT::ContactlessCVMRequiredLimit_DFAB42[6];

// Interac

#define XML_TAG_AD_IK                                 "Interac"
#define XML_TAG_AD_IK_9F1C_TERM_IDENT                 "TermIdent_9F1C"                                 ///<  @n TLV tag: #TAG_9F1C_TRM_ID                              @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::TermIdent_9F1C[8];
#define XML_TAG_AD_IK_9F1A_TERM_COUNTRY_CODE          "TerminalCountryCode_9F1A"                       ///<  @n TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE                    @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::TerminalCountryCode_9F1A[2];
#define XML_TAG_AD_IK_9F35_TERM_TYPE                  "TerminalType_9F35"                              ///<  @n TLV tag: #TAG_9F35_TRM_TYPE                            @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::TerminalType_9F35;
#define XML_TAG_AD_IK_9F33_TERM_CAPS                  "TerminalCapabilities_9F33"                      ///<  @n TLV tag: #TAG_9F33_TRM_CAPABILITIES                    @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::TerminalCapabilities_9F33[3];
#define XML_TAG_AD_IK_9F40_ADD_TERM_CAPS              "AdditionalTerminalCapabilities_9F40"            ///<  @n TLV tag: #TAG_9F40_ADD_TRM_CAP                         @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::AdditionalTerminalCapabilities_9F40[5];
#define XML_TAG_AD_IK_9F5F_CTLS_FLOOR_LIMIT           "ContactlessFloorLimit_9F5F"                     ///<  @n TLV tag: #TAG_9F5F_READER_CTLS_FLOOR_LIMIT             @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::ContactlessFloorLimit_9F5F[6];
#define XML_TAG_AD_IK_9F58_MERCHANT_TYPE_INDICATOR    "MerchantTypeIndicator_9F58"                     ///<  @n TLV tag: #TAG_9F58_MERCH_TYPE_INDICATOR                @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::MerchantTypeIndicator_9F58;
#define XML_TAG_AD_IK_9F59_TERM_TRX_INFORMATION       "TerminalTransactionInformation_9F59"            ///<  @n TLV tag: #TAG_9F59_TERM_TRANS_INFO                     @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::TerminalTransactionInformation_9F59[3];
#define XML_TAG_AD_IK_9F5A_TERM_TRX_TYPE              "TerminalTransactionType_9F5A"                   ///<  @n TLV tag: #TAG_9F5A_TERM_TRANS_TYPE                     @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::TerminalTransactionType_9F5A;
#define XML_TAG_AD_IK_9F5E_TERM_OPTION_STATUS         "TerminalOptionStatus_9F5E"                      ///<  @n TLV tag: #TAG_9F5E_TERM_OPTION_STATUS                  @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::TerminalOptionStatus_9F5E[2];
#define XML_TAG_AD_IK_9F5D_RECEIPT_REQ_LIMIT          "ReceiptRequiredLimit_9F5D"                      ///<  @n TLV tag: #TAG_9F5D_TERM_CTLS_RECEIPT_REQUIRED_LIMIT    @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::ReceiptRequiredLimit_9F5D[6];
#define XML_TAG_AD_IK_DF6D_TRY_AGAIN_LIMIT            "TryAgainLimit_DF6D"                             ///<  @n TLV tag: #TAG_DF6D_TRY_AGAIN_LIMIT                     @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::TryAgainLimit_DF6D;
#define XML_TAG_AD_IK_9F09_VERSION_NUMBER             "VersionNumber_9F09"                             ///<  @n TLV tag: #TAG_9F09_TRM_APP_VERSION_NB                  @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::VersionNumber_9F09[2*EMV_CTLS_MAX_APP_VERS];
#define XML_TAG_AD_IK_9F15_MERCHANT_CATEGORY_CODE     "MerchantCategoryCode_9F15"                      ///<  @n TLV tag: #TAG_9F15_MERCH_CATEG_CODE                    @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::MerchantCategoryCode_9F15[2];
#define XML_TAG_AD_IK_9F16_MERCHANT_ID                "MerchantIdentifier_9F16"                        ///<  @n TLV tag: #TAG_9F16_MERCHANT_ID                         @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::MerchantIdentifier_9F16[15+1];
#define XML_TAG_AD_IK_9F4E_MERCHANT_NAME_LOCATION     "MerchantNameAndLocation_9F4E"                   ///<  @n TLV tag: #TAG_9F4E_TAC_MERCHANTLOC                     @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::MerchantNameAndLocation_9F4E[40+1];
#define XML_TAG_AD_IK_9F01_ACQUIRER_ID                "AcquirerIdentifier_9F01"                        ///<  @n TLV tag: #TAG_9F01_ACQ_ID                              @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::AcquirerIdentifier_9F01[6];
#define XML_TAG_AD_IK_DFAB31_APP_FLOW_CAP             "AppFlowCap_DFAB31"                              ///<  @n TLV tag: #TAG_DFAB31_APP_FLOW_CAP                      @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::AppFlowCap_DFAB31[5];
#define XML_TAG_AD_IK_DFAB41_CTLS_TRX_LIMIT           "ContactlessTransactionLimit_DFAB41"             ///<  @n TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT                    @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::ContactlessTransactionLimit_DFAB41[6];
#define XML_TAG_AD_IK_DFAB42_CTLS_CVM_REQ_LIMIT       "ContactlessCVMRequiredLimit_DFAB42"             ///<  @n TLV tag: #TAG_DFAB42_CTLS_CVM_REQ_LIMIT                @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::ContactlessCVMRequiredLimit_DFAB42[6];
#define XML_TAG_AD_IK_DFAB43_TAC_DEFAULT              "TACDefault_DFAB43"                              ///<  @n TLV tag: #TAG_DFAB43_TAC_DEFAULT                       @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::TACDefault_DFAB43[5];
#define XML_TAG_AD_IK_DFAB44_TAC_DENIAL               "TACDenial_DFAB44"                               ///<  @n TLV tag: #TAG_DFAB44_TAC_DENIAL                        @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::TACDenial_DFAB44[5];
#define XML_TAG_AD_IK_DFAB45_TAC_ONLINE               "TACOnline_DFAB45"                               ///<  @n TLV tag: #TAG_DFAB45_TAC_ONLINE                        @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::TACOnline_DFAB45[5];
#define XML_TAG_AD_IK_DFAB46_RISK_MGMT_THRESHOLD      "RiskManagementThreshold_DFAB46"                 ///<  @n TLV tag: #TAG_DFAB46_RISK_MGMT_THRESHOLD               @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::RiskManagementThreshold_DFAB46[6];
#define XML_TAG_AD_IK_DFAB47_RISK_MGMT_TRGT_PERC      "RiskManagementTargetPercentage_DFAB47"          ///<  @n TLV tag: #TAG_DFAB47_RISK_MGMT_TRGT_PRCT               @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::RiskManagementTargetPercentage_DFAB47;
#define XML_TAG_AD_IK_DFAB48_RISK_MGMT_MAX_TRGT_PERC  "RiskManagementMaximumTargetPercentage_DFAB48"   ///<  @n TLV tag: #TAG_DFAB48_RISK_MGMT_MAX_TRGT_PRCT           @n Struct: #EMV_CTLS_APPLIDATA_IK_STRUCT::RiskManagementMaximumTargetPercentage_DFAB48;

// EPAL

#define XML_TAG_AD_EK                                 "EPAL"
#define XML_TAG_AD_EK_9F1C_TERM_IDENT                 "TermIdent_9F1C"                               ///<  @n TLV tag: #TAG_9F1C_TRM_ID                  @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::TermIdent_9F1C[8];
#define XML_TAG_AD_EK_9F1A_TERM_COUNTRY_CODE          "TerminalCountryCode_9F1A"                     ///<  @n TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE        @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::TerminalCountryCode_9F1A[2];
#define XML_TAG_AD_EK_9F35_TERM_TYPE                  "TerminalType_9F35"                            ///<  @n TLV tag: #TAG_9F35_TRM_TYPE                @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::TerminalType_9F35;
#define XML_TAG_AD_EK_9F33_TERM_CAPS                  "TerminalCapabilities_9F33"                    ///<  @n TLV tag: #TAG_9F33_TRM_CAPABILITIES        @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::TerminalCapabilities_9F33[3];
#define XML_TAG_AD_EK_9F40_ADD_TERM_CAPS              "AdditionalTerminalCapabilities_9F40"          ///<  @n TLV tag: #TAG_9F40_ADD_TRM_CAP             @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::AdditionalTerminalCapabilities_9F40[5];
#define XML_TAG_AD_EK_9F09_VERSION_NUMBER             "VersionNumber_9F09"                           ///<  @n TLV tag: #TAG_9F09_TRM_APP_VERSION_NB      @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::VersionNumber_9F09[2*EMV_CTLS_MAX_APP_VERS];
#define XML_TAG_AD_EK_9F15_MERCHANT_CATEGORY_CODE     "MerchantCategoryCode_9F15"                    ///<  @n TLV tag: #TAG_9F15_MERCH_CATEG_CODE        @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::MerchantCategoryCode_9F15[2];
#define XML_TAG_AD_EK_9F16_MERCHANT_ID                "MerchantIdentifier_9F16"                      ///<  @n TLV tag: #TAG_9F16_MERCHANT_ID             @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::MerchantIdentifier_9F16[15+1];
#define XML_TAG_AD_EK_9F4E_MERCHANT_NAME_LOCATION     "MerchantNameAndLocation_9F4E"                 ///<  @n TLV tag: #TAG_9F4E_TAC_MERCHANTLOC         @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::MerchantNameAndLocation_9F4E[40+1];
#define XML_TAG_AD_EK_9F01_ACQUIRER_ID                "AcquirerIdentifier_9F01"                      ///<  @n TLV tag: #TAG_9F01_ACQ_ID                  @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::AcquirerIdentifier_9F01[6];
#define XML_TAG_AD_EK_9F66_TERM_TRX_QUALIFIER         "TerminalTransactionQualifier_9F66"            ///<  @n TLV tag: #TAG_9F66_TTQ                  @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::AcquirerIdentifier_9F01[6];
#define XML_TAG_AD_EK_DFAB31_APP_FLOW_CAP             "AppFlowCap_DFAB31"                            ///<  @n TLV tag: #TAG_DFAB31_APP_FLOW_CAP          @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::AppFlowCap_DFAB31[5];
#define XML_TAG_AD_EK_DFAB40_CTLS_FLOOR_LIMIT         "ContactlessFloorLimit_DFAB40"                 ///<  @n TLV tag: #TAG_DFAB40_CTLS_FLOOR_LIMIT      @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::ContactlessFloorLimit_DFAB40[6];
#define XML_TAG_AD_EK_DFAB41_CTLS_TRX_LIMIT           "ContactlessTransactionLimit_DFAB41"           ///<  @n TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT        @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::ContactlessTransactionLimit_DFAB41[6];
#define XML_TAG_AD_EK_DFAB43_TAC_DEFAULT              "TACDefault_DFAB43"                            ///<  @n TLV tag: #TAG_DFAB43_TAC_DEFAULT           @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::TACDefault_DFAB43[5];
#define XML_TAG_AD_EK_DFAB44_TAC_DENIAL               "TACDenial_DFAB44"                             ///<  @n TLV tag: #TAG_DFAB44_TAC_DENIAL            @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::TACDenial_DFAB44[5];
#define XML_TAG_AD_EK_DFAB45_TAC_ONLINE               "TACOnline_DFAB45"                             ///<  @n TLV tag: #TAG_DFAB45_TAC_ONLINE            @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::TACOnline_DFAB45[5];
#define XML_TAG_AD_EK_DFAB4A_CTLS_TRX_LIMIT_CASH      "CtlsTransactionLimitCash_DFAB4A"              ///<  @n TLV tag: #TAG_DFAB4A_CTLS_TRX_LIMIT_CASH   @n Struct: #EMV_CTLS_APPLIDATA_EK_STRUCT::CtlsTransactionLimitCash_DFAB4A[6];

// Visa Asia/Pacific

#define XML_TAG_AD_PK                                 "VisaAsiaPacific"
#define XML_TAG_AD_PK_9F1C_TERM_IDENT                 "TermIdent_9F1C"                                ///<  @n TLV tag: #TAG_9F1C_TRM_ID                 @n Struct: #EMV_CTLS_APPLIDATA_PK_STRUCT::TermIdent_9F1C[8];
#define XML_TAG_AD_PK_9F1A_TERM_COUNTRY_CODE          "TerminalCountryCode_9F1A"                      ///<  @n TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE       @n Struct: #EMV_CTLS_APPLIDATA_PK_STRUCT::TerminalCountryCode_9F1A[2];
#define XML_TAG_AD_PK_9F35_TERM_TYPE                  "TerminalType_9F35"                             ///<  @n TLV tag: #TAG_9F35_TRM_TYPE               @n Struct: #EMV_CTLS_APPLIDATA_PK_STRUCT::TerminalType_9F35;
#define XML_TAG_AD_PK_CVM_REQUIREMENTS                "CvmRequirements_DF04"                          ///<  @n TLV tag: #TAG_DF04_PK_CVM_REQUIREMENTS    @n Struct: #EMV_CTLS_APPLIDATA_PK_STRUCT::CvmRequirements_DF04;
#define XML_TAG_AD_PK_DFAB31_APP_FLOW_CAP             "AppFlowCap_DFAB31"                             ///<  @n TLV tag: #TAG_DFAB31_APP_FLOW_CAP         @n Struct: #EMV_CTLS_APPLIDATA_PK_STRUCT::AppFlowCap_DFAB31[5];
#define XML_TAG_AD_PK_DF02_CTLS_FLOOR_LIMIT           "ContactlessFloorLimit_DF02"                    ///<  @n TLV tag: #TAG_DF02_PK_FLOOR_LIMIT         @n Struct: #EMV_CTLS_APPLIDATA_PK_STRUCT::ContactlessFloorLimit_DF02[6];
#define XML_TAG_AD_PK_DFAB41_CTLS_TRX_LIMIT           "ContactlessTransactionLimit_DFAB41"            ///<  @n TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT       @n Struct: #EMV_CTLS_APPLIDATA_PK_STRUCT::ContactlessTransactionLimit_DFAB41[6];
#define XML_TAG_AD_PK_DF01_CTLS_CVM_REQ_LIMIT         "ContactlessCVMRequiredLimit_DF01"              ///<  @n TLV tag: #TAG_DF01_PK_CVM_REQ_LIMIT       @n Struct: #EMV_CTLS_APPLIDATA_PK_STRUCT::ContactlessCVMRequiredLimit_DF01[6];

// CUP

#define XML_TAG_AD_CK                                 "ChinaUnionPay"
#define XML_TAG_AD_CK_9F1C_TERM_IDENT                 "TermIdent_9F1C"                                ///<  @n TLV tag: #TAG_9F1C_TRM_ID                 @n Struct: #EMV_CTLS_APPLIDATA_CK_STRUCT::TermIdent_9F1C[8];
#define XML_TAG_AD_CK_9F1A_TERM_COUNTRY_CODE          "TerminalCountryCode_9F1A"                      ///<  @n TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE       @n Struct: #EMV_CTLS_APPLIDATA_CK_STRUCT::TerminalCountryCode_9F1A[2];
#define XML_TAG_AD_CK_9F35_TERM_TYPE                  "TerminalType_9F35"                             ///<  @n TLV tag: #TAG_9F35_TRM_TYPE               @n Struct: #EMV_CTLS_APPLIDATA_CK_STRUCT::TerminalType_9F35;
#define XML_TAG_AD_CK_9F66_TERM_TRX_QUALIFIER         "TerminalTransactionQualifier_9F66"             ///<  @n TLV tag: #TAG_9F66_TTQ                    @n Struct: #EMV_CTLS_APPLIDATA_CK_STRUCT::TerminalTransactionQualifier_9F66[4];
#define XML_TAG_AD_CK_9F33_TERM_CAPS                  "TerminalCapabilities_9F33"                     ///<  @n TLV tag: #TAG_9F33_TRM_CAPABILITIES       @n Struct: #EMV_CTLS_APPLIDATA_CK_STRUCT::TerminalCapabilities_9F33[3];
#define XML_TAG_AD_CK_9F40_ADD_TERM_CAPS              "AdditionalTerminalCapabilities_9F40"           ///<  @n TLV tag: #TAG_9F40_ADD_TRM_CAP            @n Struct: #EMV_CTLS_APPLIDATA_CK_STRUCT::AdditionalTerminalCapabilities_9F40[5];
#define XML_TAG_AD_CK_9F09_VERSION_NUMBER             "VersionNumber_9F09"                            ///<  @n TLV tag: #TAG_9F09_TRM_APP_VERSION_NB     @n Struct: #EMV_CTLS_APPLIDATA_CK_STRUCT::VersionNumber_9F09[2*EMV_CTLS_MAX_APP_VERS];
#define XML_TAG_AD_CK_9F15_MERCHANT_CATEGORY_CODE     "MerchantCategoryCode_9F15"                     ///<  @n TLV tag: #TAG_9F15_MERCH_CATEG_CODE       @n Struct: #EMV_CTLS_APPLIDATA_CK_STRUCT::MerchantCategoryCode_9F15[2];
#define XML_TAG_AD_CK_9F16_MERCHANT_ID                "MerchantIdentifier_9F16"                       ///<  @n TLV tag: #TAG_9F16_MERCHANT_ID            @n Struct: #EMV_CTLS_APPLIDATA_CK_STRUCT::MerchantIdentifier_9F16[15+1];
#define XML_TAG_AD_CK_9F4E_MERCHANT_NAME_LOCATION     "MerchantNameAndLocation_9F4E"                  ///<  @n TLV tag: #TAG_9F4E_TAC_MERCHANTLOC        @n Struct: #EMV_CTLS_APPLIDATA_CK_STRUCT::MerchantNameAndLocation_9F4E[40+1];
#define XML_TAG_AD_CK_DFAB30_TEC_SUPPORT              "TecSupport_DFAB30"                             ///<  @n TLV tag: #TAG_DFAB30_TEC_SUPPORT          @n Struct: #EMV_CTLS_APPLIDATA_CK_STRUCT::TecSupport_DFAB30;
#define XML_TAG_AD_CK_DFAB31_APP_FLOW_CAP             "AppFlowCap_DFAB31"                             ///<  @n TLV tag: #TAG_DFAB31_APP_FLOW_CAP         @n Struct: #EMV_CTLS_APPLIDATA_CK_STRUCT::AppFlowCap_DFAB31[5];
#define XML_TAG_AD_CK_DFAB40_CTLS_FLOOR_LIMIT         "ContactlessFloorLimit_DFAB40"                  ///<  @n TLV tag: #TAG_DFAB40_CTLS_FLOOR_LIMIT     @n Struct: #EMV_CTLS_APPLIDATA_CK_STRUCT::ContactlessFloorLimit_DFAB40[6];
#define XML_TAG_AD_CK_DFAB41_CTLS_TRX_LIMIT           "ContactlessTransactionLimit_DFAB41"            ///<  @n TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT       @n Struct: #EMV_CTLS_APPLIDATA_CK_STRUCT::ContactlessTransactionLimit_DFAB41[6];
#define XML_TAG_AD_CK_DFAB42_CTLS_CVM_REQ_LIMIT       "ContactlessCVMRequiredLimit_DFAB42"            ///<  @n TLV tag: #TAG_DFAB42_CTLS_CVM_REQ_LIMIT   @n Struct: #EMV_CTLS_APPLIDATA_CK_STRUCT::ContactlessCVMRequiredLimit_DFAB42[6];

// Gemalto

#define XML_TAG_AD_GK                                 "Gemalto"
#define XML_TAG_AD_GK_9F1C_TERM_IDENT                 "TermIdent_9F1C"                                ///<  @n TLV tag: #TAG_9F1C_TRM_ID                    @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::TermIdent_9F1C[8];
#define XML_TAG_AD_GK_9F1A_TERM_COUNTRY_CODE          "TerminalCountryCode_9F1A"                      ///<  @n TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE          @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::TerminalCountryCode_9F1A[2];
#define XML_TAG_AD_GK_9F35_TERM_TYPE                  "TerminalType_9F35"                             ///<  @n TLV tag: #TAG_9F35_TRM_TYPE                  @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::TerminalType_9F35;
#define XML_TAG_AD_GK_9F33_TERM_CAPS                  "TerminalCapabilities_9F33"                     ///<  @n TLV tag: #TAG_9F33_TRM_CAPABILITIES          @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::TerminalCapabilities_9F33[3];
#define XML_TAG_AD_GK_9F40_ADD_TERM_CAPS              "AdditionalTerminalCapabilities_9F40"           ///<  @n TLV tag: #TAG_9F40_ADD_TRM_CAP               @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::AdditionalTerminalCapabilities_9F40[5];
#define XML_TAG_AD_GK_9F09_VERSION_NUMBER             "VersionNumber_9F09"                            ///<  @n TLV tag: #TAG_9F09_TRM_APP_VERSION_NB        @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::VersionNumber_9F09[2*EMV_CTLS_MAX_APP_VERS];
#define XML_TAG_AD_GK_9F15_MERCHANT_CATEGORY_CODE     "MerchantCategoryCode_9F15"                     ///<  @n TLV tag: #TAG_9F15_MERCH_CATEG_CODE          @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::MerchantCategoryCode_9F15[2];
#define XML_TAG_AD_GK_9F16_MERCHANT_ID                "MerchantIdentifier_9F16"                       ///<  @n TLV tag: #TAG_9F16_MERCHANT_ID               @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::MerchantIdentifier_9F16[15+1];
#define XML_TAG_AD_GK_9F4E_MERCHANT_NAME_LOCATION     "MerchantNameAndLocation_9F4E"                  ///<  @n TLV tag: #TAG_9F4E_TAC_MERCHANTLOC           @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::MerchantNameAndLocation_9F4E[40+1];
#define XML_TAG_AD_GK_9F01_ACQUIRER_ID                "AcquirerIdentifier_9F01"                       ///<  @n TLV tag: #TAG_9F01_ACQ_ID                    @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::AcquirerIdentifier_9F01[6];
#define XML_TAG_AD_GK_DFAB31_APP_FLOW_CAP             "AppFlowCap_DFAB31"                             ///<  @n TLV tag: #TAG_DFAB31_APP_FLOW_CAP            @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::AppFlowCap_DFAB31[5];
#define XML_TAG_AD_GK_DFAB40_CTLS_FLOOR_LIMIT         "ContactlessFloorLimit_DFAB40"                  ///<  @n TLV tag: #TAG_DFAB40_CTLS_FLOOR_LIMIT        @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::ContactlessFloorLimit_DFAB40[6];
#define XML_TAG_AD_GK_DFAB41_CTLS_TRX_LIMIT           "ContactlessTransactionLimit_DFAB41"            ///<  @n TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT          @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::ContactlessTransactionLimit_DFAB41[6];
#define XML_TAG_AD_GK_DFAB42_CTLS_CVM_REQ_LIMIT       "ContactlessCVMRequiredLimit_DFAB42"            ///<  @n TLV tag: #TAG_DFAB42_CTLS_CVM_REQ_LIMIT      @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::ContactlessCVMRequiredLimit_DFAB42[6];
#define XML_TAG_AD_GK_DFAB43_TAC_DEFAULT              "TACDefault_DFAB43"                             ///<  @n TLV tag: #TAG_DFAB43_TAC_DEFAULT             @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::TACDefault_DFAB43[5];
#define XML_TAG_AD_GK_DFAB44_TAC_DENIAL               "TACDenial_DFAB44"                              ///<  @n TLV tag: #TAG_DFAB44_TAC_DENIAL              @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::TACDenial_DFAB44[5];
#define XML_TAG_AD_GK_DFAB45_TAC_ONLINE               "TACOnline_DFAB45"                              ///<  @n TLV tag: #TAG_DFAB45_TAC_ONLINE              @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::TACOnline_DFAB45[5];
#define XML_TAG_AD_GK_DFAB4F_CTLS_APP_KERN_CAP        "CtlsAppKernelCap_DFAB4F"                       ///<  @n TLV tag: #TAG_DFAB4F_PURE_CTLS_APP_KERN_CAP  @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::CtlsAppKernelCap_DFAB4F
#define XML_TAG_AD_GK_DFAB50_MTOL                     "MTOL_DFAB50"                                   ///<  @n TLV tag: #TAG_DFAB50_PURE_MTOL               @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::MTOL_DFAB50
#define XML_TAG_AD_GK_DFAB51_DEFAULT_DDOL             "DefaultDDOL_DFAB51"                            ///<  @n TLV tag: #TAG_DFAB51_CTLS_DEFAULT_DDOL       @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::DefaultDDOL_DFAB51
#define XML_TAG_AD_GK_9F76_TERMINAL_TRX_DATA          "TerminalTransactionData_9F76"                  ///<  @n TLV tag: #TAG_9F76_PURE_TERM_TRX_DATA        @n Struct: #EMV_CTLS_APPLIDATA_GK_STRUCT::TerminalTransactionData_9F76

// RuPay

#define XML_TAG_AD_RK                                 "RuPay"
#define XML_TAG_AD_RK_9F1C_TERM_IDENT                 "TermIdent_9F1C"                                ///<  @n TLV tag: #TAG_9F1C_TRM_ID                      @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::TermIdent_9F1C[8];
#define XML_TAG_AD_RK_9F1A_TERM_COUNTRY_CODE          "TerminalCountryCode_9F1A"                      ///<  @n TLV tag: #TAG_9F1A_TRM_COUNTRY_CODE            @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::TerminalCountryCode_9F1A[2];
#define XML_TAG_AD_RK_9F35_TERM_TYPE                  "TerminalType_9F35"                             ///<  @n TLV tag: #TAG_9F35_TRM_TYPE                    @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::TerminalType_9F35;
#define XML_TAG_AD_RK_9F33_TERM_CAPS                  "TerminalCapabilities_9F33"                     ///<  @n TLV tag: #TAG_9F33_TRM_CAPABILITIES            @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::TerminalCapabilities_9F33[3];
#define XML_TAG_AD_RK_9F40_ADD_TERM_CAPS              "AdditionalTerminalCapabilities_9F40"           ///<  @n TLV tag: #TAG_9F40_ADD_TRM_CAP                 @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::AdditionalTerminalCapabilities_9F40[5];
#define XML_TAG_AD_RK_9F09_VERSION_NUMBER             "VersionNumber_9F09"                            ///<  @n TLV tag: #TAG_9F09_TRM_APP_VERSION_NB          @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::VersionNumber_9F09[2*EMV_CTLS_MAX_APP_VERS];
#define XML_TAG_AD_RK_9F15_MERCHANT_CATEGORY_CODE     "MerchantCategoryCode_9F15"                     ///<  @n TLV tag: #TAG_9F15_MERCH_CATEG_CODE            @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::MerchantCategoryCode_9F15[2];
#define XML_TAG_AD_RK_DFAB31_APP_FLOW_CAP             "AppFlowCap_DFAB31"                             ///<  @n TLV tag: #TAG_DFAB31_APP_FLOW_CAP              @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::AppFlowCap_DFAB31[5];
#define XML_TAG_AD_RK_DFAB40_CTLS_FLOOR_LIMIT         "ContactlessFloorLimit_DFAB40"                  ///<  @n TLV tag: #TAG_DFAB40_CTLS_FLOOR_LIMIT          @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::ContactlessFloorLimit_DFAB40[6];
#define XML_TAG_AD_RK_DFAB41_CTLS_TRX_LIMIT           "ContactlessTransactionLimit_DFAB41"            ///<  @n TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT            @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::ContactlessTransactionLimit_DFAB41[6];
#define XML_TAG_AD_RK_DFAB42_CTLS_CVM_REQ_LIMIT       "ContactlessCVMRequiredLimit_DFAB42"            ///<  @n TLV tag: #TAG_DFAB42_CTLS_CVM_REQ_LIMIT        @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::ContactlessCVMRequiredLimit_DFAB42[6];
#define XML_TAG_AD_RK_DFAB43_TAC_DEFAULT              "TACDefault_DFAB43"                             ///<  @n TLV tag: #TAG_DFAB43_TAC_DEFAULT               @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::TACDefault_DFAB43[5];
#define XML_TAG_AD_RK_DFAB44_TAC_DENIAL               "TACDenial_DFAB44"                              ///<  @n TLV tag: #TAG_DFAB44_TAC_DENIAL                @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::TACDenial_DFAB44[5];
#define XML_TAG_AD_RK_DFAB45_TAC_ONLINE               "TACOnline_DFAB45"                              ///<  @n TLV tag: #TAG_DFAB45_TAC_ONLINE                @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::TACOnline_DFAB45[5];
#define XML_TAG_AD_RK_DFAB46_RISK_MGMT_THRESHOLD      "RiskManagementThreshold_DFAB46"                ///<  @n TLV tag: #TAG_DFAB46_RISK_MGMT_THRESHOLD       @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::RiskManagementThreshold_DFAB46[6];
#define XML_TAG_AD_RK_DFAB47_RISK_MGMT_TRGT_PERC      "RiskManagementTargetPercentage_DFAB47"         ///<  @n TLV tag: #TAG_DFAB47_RISK_MGMT_TRGT_PRCT       @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::RiskManagementTargetPercentage_DFAB47;
#define XML_TAG_AD_RK_DFAB48_RISK_MGMT_MAX_TRGT_PERC  "RiskManagementMaximumTargetPercentage_DFAB48"  ///<  @n TLV tag: #TAG_DFAB48_RISK_MGMT_MAX_TRGT_PRCT   @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::RiskManagementMaximumTargetPercentage_DFAB48;
#define XML_TAG_AD_RK_DFAB4D_CALLBACK_TIMEOUT         "CallbackTimeout_DFAB4D"                        ///<  @n TLV tag: #TAG_DFAB4D_RUPAY_CALLBACK_TIMEOUT    @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::CallbackTimeout_DFAB4D[2];
#define XML_TAG_AD_RK_DFAB4E_TORN_TRX_INTERVAL        "TornTransactionInterval_DFAB4E"                ///<  @n TLV tag: #TAG_DFAB4E_RUPAY_TORN_TRX_INTERVAL   @n Struct: #EMV_CTLS_APPLIDATA_RK_STRUCT::TornTransactionInterval_DFAB4E[2];

// Visa Asia/Pacific

#define XML_TAG_AD_DOM                                "domestic"
#define XML_TAG_AD_DOM_DFAB31_APP_FLOW_CAP            "AppFlowCap_DFAB31"                             ///<  @n TLV tag: #TAG_DFAB31_APP_FLOW_CAP         @n Struct: #EMV_CTLS_APPLIDATA_DOM_STRUCT::AppFlowCap_DFAB31[5];
#define XML_TAG_AD_DOM_DFAB41_CTLS_TRX_LIMIT          "ContactlessTransactionLimit_DFAB41"            ///<  @n TLV tag: #TAG_DFAB41_CTLS_TRX_LIMIT       @n Struct: #EMV_CTLS_APPLIDATA_DOM_STRUCT::ContactlessTransactionLimit_DFAB41[6];

#define XML_TAG_VTM                                   "VirtualTerminalMap"
#define XML_TAG_VTM_ENTRY                             "VTMEntry"
#define XML_TAG_VTM_TERMINAL                          "Terminal"
#define XML_TAG_VTM_TRANSTYPE                         "TransType_9C"
#define XML_TAG_VTM_CURRENCYCODE                      "CurrencyCode_5F2A"

///@}

#endif
