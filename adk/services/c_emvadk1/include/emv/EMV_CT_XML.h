/****************************************************************************
*  Product:     InFusion
*  Company:     VeriFone
*  Author:      GSS R&D Germany
*  Content:     Definitions and functions for serial interface
****************************************************************************/

#ifndef EMV_CT_XML_H   /* avoid double interface-includes */
  #define EMV_CT_XML_H


// ========================================================================================================
// === XML tags ===
// ========================================================================================================
/// @defgroup ADK_XML_TAGS Tags for configuration storage
/// @ingroup ADK_CONFIGURATION
/// @brief Used for storing the configuration in XML files.
///@{

#define XML_TAG_TERMDATA                        "TerminalData"            ///< constructed xml tag for terminal data
#define XML_TAG_TERMDATA_TERM_TYP               "TermTyp"                 ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F35_TRM_TYPE, @n TermTyp in EMV_CT_TERMDATA_STRUCT::TermTyp, @n XML Tag: #XML_TAG_TERMDATA_TERM_TYP
#define XML_TAG_TERMDATA_COUNTRY_CODE_TERM      "CountryCodeTerm"         ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F1A_TRM_COUNTRY_CODE, @n TermCountryCode in EMV_CT_TERMDATA_STRUCT::TermCountryCode, @n XML Tag: #XML_TAG_TERMDATA_COUNTRY_CODE_TERM
#define XML_TAG_TERMDATA_TERM_CAP               "TermCap"                 ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F33_TRM_CAPABILITIES, @n TermCap in EMV_CT_TERMDATA_STRUCT::TermCap, @n XML Tag: #XML_TAG_TERMDATA_TERM_CAP
#define XML_TAG_TERMDATA_TERM_ADD_CAP           "TermAddCap"              ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F40_ADD_TRM_CAP, @n TermAddCap in EMV_CT_TERMDATA_STRUCT::TermAddCap, @n XML Tag: #XML_TAG_TERMDATA_TERM_ADD_CAP
#define XML_TAG_TERMDATA_TERM_IDENT             "TermIdent"               ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F1C_TRM_ID, @n TermIdent in EMV_CT_TERMDATA_STRUCT::TermIdent, @c XML Tag: #XML_TAG_TERMDATA_TERM_IDENT
#define XML_TAG_TERMDATA_CURRENCY_TRANS         "CurrencyTrans"           ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_5F2A_TRANS_CURRENCY, @n CurrencyTrans in EMV_CT_TERMDATA_STRUCT::CurrencyTrans, @n XML Tag: #XML_TAG_TERMDATA_CURRENCY_TRANS
#define XML_TAG_TERMDATA_EXP_TRANS              "ExpTrans"                ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_5F36_TRANS_CURRENCY_EXP, @n ExpTrans in EMV_CT_TERMDATA_STRUCT::ExpTrans, @n XML Tag: #XML_TAG_TERMDATA_EXP_TRANS
#define XML_TAG_TERMDATA_SUPP_LANG              "SuppLang"                ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_SUPP_LANG, @n SuppLang in EMV_CT_TERMDATA_STRUCT::SuppLang, @n XML Tag: #XML_TAG_TERMDATA_SUPP_LANG
#define XML_TAG_TERMDATA_IFD_SERIAL_NUMBER      "IFD_SerialNumber"        ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F1E_IFD_SERIAL_NB, @n IFDSerialNumber in EMV_CT_TERMDATA_STRUCT::IFDSerialNumber, @n XML Tag: #XML_TAG_TERMDATA_IFD_SERIAL_NUMBER
#define XML_TAG_TERMDATA_KERNEL_VERSION         "KernelVersion"           ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_KERNEL_VERSION, @n KernelVersion in EMV_CT_TERMDATA_STRUCT::KernelVersion, @n XML Tag: #XML_TAG_TERMDATA_KERNEL_VERSION
#define XML_TAG_TERMDATA_FRAMEWORK_VERSION      "FrameworkVersion"        ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF11_LIB_VERSION, @n FrameworkVersion in EMV_CT_TERMDATA_STRUCT::FrameworkVersion, @n XML Tag: #XML_TAG_TERMDATA_FRAMEWORK_VERSION
#define XML_TAG_TERMDATA_L1DRIVER_VERSION       "L1DriverVersion"         ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_L1DRIVER_VERSION, @n L1DriverVersion in EMV_CT_TERMDATA_STRUCT::L1DriverVersion, @n XML Tag: #XML_TAG_TERMDATA_L1DRIVER_VERSION

#define XML_TAG_CAP_KEYS                        "CapKeys"                 ///< constructed xml tag for CAP key data
#define XML_TAG_CAP_KEYS_CAPKEY                 "CapKey"                  ///< constructed xml tag for A SINGLE CAP key
#define XML_TAG_CAP_KEYS_INDEX                  "Index"                   ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_KEY_INDEX, @n Index in EMV_CT_CAPKEY_STRUCT::Index, @n XML Tag: #XML_TAG_CAP_KEYS_INDEX
#define XML_TAG_CAP_KEYS_RID                    "RID"                     ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_KEY_RID, @n RID in EMV_CT_CAPKEY_STRUCT::RID, @n XML Tag: #XML_TAG_CAP_KEYS_RID
#define XML_TAG_CAP_KEYS_KEY                    "Key"                     ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_KEY_KEY, @n Key in EMV_CT_CAPKEY_STRUCT::Key, @n XML Tag: #XML_TAG_CAP_KEYS_KEY
#define XML_TAG_CAP_KEYS_KEYLEN                 "KeyLen"                  ///< @c Struct, @c XML Reference: @n KeyLen in EMV_CT_CAPKEY_STRUCT::KeyLen, @n XML Tag: #XML_TAG_CAP_KEYS_KEYLEN
#define XML_TAG_CAP_KEYS_EXPONENT               "Exponent"                ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_KEY_EXPONENT, @n Exponent in EMV_CT_CAPKEY_STRUCT::Exponent, @n XML Tag: #XML_TAG_CAP_KEYS_EXPONENT
#define XML_TAG_CAP_KEYS_HASH                   "Hash"                    ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_KEY_HASH, @n Hash in EMV_CT_CAPKEY_STRUCT::Hash, @n XML Tag: #XML_TAG_CAP_KEYS_HASH
#define XML_TAG_CAP_KEYS_REVOC_LIST             "RevocationList"          ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_KEY_CRL, @n RevocEntries in EMV_CT_CAPKEY_STRUCT::RevocEntries, @n XML Tag: #XML_TAG_CAP_KEYS_REVOC_LIST

#define XML_TAG_APPLIDATA                       "ApplicationData"                        ///< constructed xml tag for application data
#define XML_TAG_APPLIDATA_APP                   "Application"                            ///< Constructed tag for one AID
#define XML_TAG_APPLIDATA_AID                   "AID"                                    ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF04_AID, @n AID in EMV_CT_APPLI_STRUCT::AID
#define XML_TAG_APPLIDATA_VER_NUM               "VerNum"                                 ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F09_TRM_APP_VERSION_NB, @n VerNum in EMV_CT_APPLIDATA_STRUCT::VerNum, @n XML Tag: #XML_TAG_APPLIDATA_VER_NUM
#define XML_TAG_APPLIDATA_APP_NAME              "AppName"                                ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_50_APP_LABEL, @n AppName in EMV_CT_APPLIDATA_STRUCT::AppName, @n XML Tag: #XML_TAG_APPLIDATA_APP_NAME
#define XML_TAG_APPLIDATA_ASI                   "ASI"                                    ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF20_ASI, @n ASI in EMV_CT_APPLIDATA_STRUCT::ASI, @n XML Tag: #XML_TAG_APPLIDATA_ASI
#define XML_TAG_APPLIDATA_BR_KEY                "BrKey"                                  ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F15_MERCH_CATEG_CODE, @n BrKey in EMV_CT_APPLIDATA_STRUCT::BrKey, @n XML Tag: #XML_TAG_APPLIDATA_BR_KEY
#define XML_TAG_APPLIDATA_TERM_IDENT            "TermIdent"                              ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F1C_TRM_ID, @n TermIdent in EMV_CT_APPLIDATA_STRUCT::TermIdent, @n XML Tag: #XML_TAG_APPLIDATA_TERM_IDENT
#define XML_TAG_APPLIDATA_FLOOR_LIMIT           "FloorLimit"                             ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F1B_TRM_FLOOR_LIMIT, @n FloorLimit in EMV_CT_APPLIDATA_STRUCT::FloorLimit, @n XML Tag: #XML_TAG_APPLIDATA_FLOOR_LIMIT
#define XML_TAG_APPLIDATA_SECURE_LIMIT          "SecurityLimit"                          ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF49_APL_SEC_LIMIT, @n Security_Limit in EMV_CT_APPLIDATA_STRUCT::Security_Limit, @n XML Tag: #XML_TAG_APPLIDATA_SECURE_LIMIT
#define XML_TAG_APPLIDATA_NON_SECURE_CAPS       "BelowLimitTerminalCapabilities"         ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF4A_APL_SEC_CAPS, @n Capabilities_belowLimit in EMV_CT_APPLIDATA_STRUCT::Capabilities_belowLimit, @n XML Tag: #XML_TAG_APPLIDATA_NON_SECURE_CAPS
#define XML_TAG_APPLIDATA_THRESHOLD             "Threshold"                              ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF24_THRESHHOLD, @n Threshhold in EMV_CT_APPLIDATA_STRUCT::Threshhold, @n XML Tag: #XML_TAG_APPLIDATA_THRESHOLD
#define XML_TAG_APPLIDATA_TARGET_PERCENTAGE     "TargetPercentage"                       ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF26_PERCENT_ONL, @n TargetPercentage in EMV_CT_APPLIDATA_STRUCT::TargetPercentage, @n XML Tag: #XML_TAG_APPLIDATA_TARGET_PERCENTAGE
#define XML_TAG_APPLIDATA_MAX_TARGET_PERCENTAGE "MaxTargetPercentage"                    ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF25_MAXPERCENT_ONL, @n MaxTargetPercentage in EMV_CT_APPLIDATA_STRUCT::MaxTargetPercentage, @n XML Tag: #XML_TAG_APPLIDATA_MAX_TARGET_PERCENTAGE
#define XML_TAG_APPLIDATA_TAC_DENIAL            "TAC_Denial"                             ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF21_TAC_DENIAL, @n TACDenial in EMV_CT_APPLIDATA_STRUCT::TACDenial, @n XML Tag: #XML_TAG_APPLIDATA_TAC_DENIAL
#define XML_TAG_APPLIDATA_TAC_ONLINE            "TAC_Online"                             ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF22_TAC_ONLINE, @n TACOnline in EMV_CT_APPLIDATA_STRUCT::TACOnline, @n XML Tag: #XML_TAG_APPLIDATA_TAC_ONLINE
#define XML_TAG_APPLIDATA_TAC_DEFAULT           "TAC_Default"                            ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF23_TAC_DEFAULT, @n TACDefault in EMV_CT_APPLIDATA_STRUCT::TACDefault, @n XML Tag: #XML_TAG_APPLIDATA_TAC_DEFAULT
#define XML_TAG_APPLIDATA_EMV_APPLICATION       "EMV_Application"                        ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF2D_EMV_APPLI, @n EMV_Application in EMV_CT_APPLIDATA_STRUCT::EMV_Application, @n XML Tag: #XML_TAG_APPLIDATA_EMV_APPLICATION
#define XML_TAG_APPLIDATA_DEFAULT_TDOL          "DefaultTDOL"                            ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF27_DEFAULT_TDOL, @n Default_TDOL in EMV_CT_APPLIDATA_STRUCT::Default_TDOL, @n XML Tag: #XML_TAG_APPLIDATA_DEFAULT_TDOL
#define XML_TAG_APPLIDATA_DEFAULT_DDOL          "DefaultDDOL"                            ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF28_DEFAULT_DDOL, @n Default_DDOL in EMV_CT_APPLIDATA_STRUCT::Default_DDOL, @n XML Tag: #XML_TAG_APPLIDATA_DEFAULT_DDOL
#define XML_TAG_APPLIDATA_MERCH_IDENT           "MerchIdent"                             ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F16_MERCHANT_ID, @n MerchIdent in EMV_CT_APPLIDATA_STRUCT::MerchIdent, @n XML Tag: #XML_TAG_APPLIDATA_MERCH_IDENT
#define XML_TAG_APPLIDATA_CDA_PROCESSING        "CDA_Processing"                         ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF3E_CDA_PROC, @n CDAProcessing in EMV_CT_APPLIDATA_STRUCT::CDAProcessing, @n XML Tag: #XML_TAG_APPLIDATA_CDA_PROCESSING
#define XML_TAG_APPLIDATA_AC_BEFORE_AFTER       "AC_BeforeAfter"                         ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF15_OFFL_ONLY_PROCESS, @n ACBeforeAfter in EMV_CT_APPLIDATA_STRUCT::ACBeforeAfter, @n XML Tag: #XML_TAG_APPLIDATA_AC_BEFORE_AFTER
#define XML_TAG_APPLIDATA_AIP_CVM_NOT_SUPPORTED "AIP_CVM_NotSupported"                   ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF2E_CVM_NOT_SUPP, @n AIP_CVM_not_supported in EMV_CT_APPLIDATA_STRUCT::AIP_CVM_not_supported, @n XML Tag: #XML_TAG_APPLIDATA_AIP_CVM_NOT_SUPPORTED
#define XML_TAG_APPLIDATA_POS_ENTRY_MODE        "POS_EntryMode"                          ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F39_POS_ENTRY_MODE, @n POS_EntryMode in EMV_CT_APPLIDATA_STRUCT::POS_EntryMode, @n XML Tag: #XML_TAG_APPLIDATA_POS_ENTRY_MODE
#define XML_TAG_APPLIDATA_ADD_VER_NUM           "AdditionalVersionNumbers"               ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF5F_ADD_APP_VERSION, @n Additional_Versions_No in EMV_CT_APPLIDATA_STRUCT::Additional_Versions_No, @n XML Tag: #XML_TAG_APPLIDATA_ADD_VER_NUM
#define XML_TAG_APPLIDATA_APP_FLOW_CAP          "AppFlowCap"                             ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF2B_APP_FLOW_CAP, @n App_FlowCap in EMV_CT_APPLIDATA_STRUCT::App_FlowCap, @n XML Tag: #XML_TAG_APPLIDATA_APP_FLOW_CAP
#define XML_TAG_APPLIDATA_ADDITIONAL_TAGS_TRM   "AdditionalTagsTRM"                      ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF29_ADD_TAGS, @n Additional_Tags_TRM in EMV_CT_APPLIDATA_STRUCT::Additional_Tags_TRM, @n XML Tag: #XML_TAG_APPLIDATA_ADDITIONAL_TAGS_TRM
#define XML_TAG_APPLIDATA_ADDITIONAL_TAGS_CRD   "AdditionalTagsCRD"                      ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF2C_ADD_TAGS_CRD, @n Additional_Tags_CRD in EMV_CT_APPLIDATA_STRUCT::Additional_Tags_CRD, @n XML Tag: #XML_TAG_APPLIDATA_ADDITIONAL_TAGS_CRD
#define XML_TAG_APPLIDATA_TAGLIST               "MandatoryTaglistCRD"                    ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF2A_DUTY_TAGS, @n Mandatory_Tags_CRD in EMV_CT_APPLIDATA_STRUCT::Mandatory_Tags_CRD, @n XML Tag: #XML_TAG_APPLIDATA_TAGLIST
#define XML_TAG_APPLIDATA_APP_TERM_CAP          "AppTermCap"                             ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F33_TRM_CAPABILITIES, @n App_TermCap in EMV_CT_APPLIDATA_STRUCT::App_TermCap, @n XML Tag: #XML_TAG_APPLIDATA_APP_TERM_CAP
#define XML_TAG_APPLIDATA_COUNTRY_CODE_TERM     "CountryCodeTerm"                        ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F1A_TRM_COUNTRY_CODE, @n App_CountryCodeTerm in EMV_CT_APPLIDATA_STRUCT::App_CountryCodeTerm, @n XML Tag: #XML_TAG_APPLIDATA_COUNTRY_CODE_TERM
#define XML_TAG_APPLIDATA_APP_TERM_ADD_CAP      "AppTermAddCap"                          ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F40_ADD_TRM_CAP, @n App_TermAddCap in EMV_CT_APPLIDATA_STRUCT::App_TermAddCap, @n XML Tag: #XML_TAG_APPLIDATA_APP_TERM_ADD_CAP
#define XML_TAG_APPLIDATA_APP_TERM_TYP          "AppTerminalType"                        ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F35_TRM_TYPE, @n App_TermTyp in EMV_CT_APPLIDATA_STRUCT::App_TermTyp, @n XML Tag: #XML_TAG_APPLIDATA_APP_TERM_TYP
#define XML_TAG_APPLIDATA_AID_PRIO              "AID_Prio"                               ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF1D_PRIO_APPLI, @n xAIDPrio in EMV_CT_APPLIDATA_STRUCT::xAIDPrio, @n XML Tag: #XML_TAG_APPLIDATA_AID_PRIO
#define XML_TAG_APPLIDATA_FALLBACK_MIDS         "FallbackMIDs"                           ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF1D_PRIO_APPLI, @n tucFallbackMIDs in EMV_CT_APPLIDATA_STRUCT::tucFallbackMIDs, @n XML Tag: #XML_TAG_APPLIDATA_FALLBACK_MIDS
#define XML_TAG_APPLIDATA_SPECIAL_TRX           "SpecialTRX"                             ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF1C_SPECIAL_TRX, @n xuc_Special_TRX in EMV_CT_APPLIDATA_STRUCT::xuc_Special_TRX, @n XML Tag: #XML_TAG_APPLIDATA_SPECIAL_TRX
#define XML_TAG_APPLIDATA_FALLBACK_HANDLING     "FallbackHandling"                       ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF18_FALLABCK, @n uc_FallBack_Handling in EMV_CT_APPLIDATA_STRUCT::uc_FallBack_Handling, @n XML Tag: #XML_TAG_APPLIDATA_FALLBACK_HANDLING
#define XML_TAG_APPLIDATA_CUSTOMER_CVM          "CustomerCVM"                            ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF2F_CVM_CUSTOM, @n Customer_CVM in EMV_CT_APPLIDATA_STRUCT::Customer_CVM, @n XML Tag: #XML_TAG_APPLIDATA_CUSTOMER_CVM
#define XML_TAG_APPLIDATA_CHKSUM_PARAMS         "ChksumParams"                           ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF13_TERM_PARAM, @n Chksum_Params in EMV_CT_APPLIDATA_STRUCT::Chksum_Params, @n XML Tag: #XML_TAG_APPLIDATA_CHKSUM_PARAMS
#define XML_TAG_APPLIDATA_CHKSUM_ASCII_EMVCO    "ChksumASCII_EMVCO"                      ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF12_CHECKSUM, @n ChksumASCIIEMVCO in EMV_CT_APPLIDATA_STRUCT::ChksumASCIIEMVCO, @n XML Tag: #XML_TAG_APPLIDATA_CHKSUM_ASCII_EMVCO
#define XML_TAG_APPLIDATA_MASTER_AID            "MasterAID"                              ///< @c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF04_AID, @n MasterAID in EMV_CT_APPLIDATA_STRUCT::MasterAID, @n XML Tag: #XML_TAG_APPLIDATA_MASTER_AID

#define XML_TAG_VTM                             "VirtualTerminalMap"
#define XML_TAG_VTM_ENTRY                       "VTMEntry"
#define XML_TAG_VTM_TERMINAL                    "Terminal"
#define XML_TAG_VTM_TRANSTYPE                   "9C_TransType"
#define XML_TAG_VTM_CURRENCYCODE                "CurrencyCode_5F2A"

///@}

#endif
