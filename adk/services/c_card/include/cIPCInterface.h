#if !defined(IPCINTERFACE_INCLUDED)
#define IPCINTERFACE_INCLUDED

#ifndef __cplusplus
#error "This file is for C++ only!"
#endif
/*****************************************************************************
 *
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/
/**
 * @file        cIPCInterface.h
 *
 * @author      Jacek Olbrys
 *
 * @brief       IPC Interface Class Header
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C++ Coding
 *              Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/
#include "cCmd.h"
#include "cCommandMan.h"

/***************************************************************************
 * Using
 **************************************************************************/
using namespace com_verifone_ipcpacket;
using namespace com_verifone_cmd;

/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
namespace com_verifone_interface
{

    /**
     * @addtogroup Card
     * @{ 
     */

    /***************************************************************************
     *    external C++ - API's
     **************************************************************************/
    class cIPCInterface
    {
    private:
        CCommandMan	  *pCmdManager;

    public:
        /**
         * @brief Constructor
         *
         * @param[in] szAppName pointer to application name
         * @param[in] pCmdMan pointer to command manager
         */
        cIPCInterface(const char *szAppName, CCommandMan *pCmdMan);

        /**
         * @brief This function implements initialization.
         *
         * @return
         *          - RESULT_SUCCESS on success
         *          - RESULT_FAILURE on failure
         *
         */
        virtual int impInitialise(int argc, char *argv[]);

        /**
         * @brief This function receive packet and execute proper handel command class. This function should never end.
         *
         */
        virtual short impWaitRequest();

        /**
         * @brief This function execute proper handel command class.
         *
         * @param[in] requestBuffer pointer to buffer with data from IPC
         * @param[in] requestLength length of the data in requestBuffer
         *
         * @return
         *         - RESULT_SUCCESS on success
         *         - RESULT_ERROR on error
         */
        virtual short mHandleRequest(uint8_t* requestBuffer, int requestLength);

        /**
         * @brief This functions returns name of this class.
         *
         * @return pointer to the static buffer with name of this class.
         */
        virtual char* mIdentify();

    
        /**
         * @brief This function send to other application error message.
         *
         * @param[in]  senderName name of receiver
         * @param[in]  number of command which cause error
         * @param[in]  respCode response code
         */
        virtual void sendErrorMessage( const char *senderName, uint8_t commandCode, uint8_t respCode );
    };

/**
 * @}
 */

/***************************************************************************
 * Module namspace: end
 **************************************************************************/
}
#endif

