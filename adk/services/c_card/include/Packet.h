#ifndef CPACKET_H
#define CPACKET_H

#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 *
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/
/**
 * @file        Packet.h
 *
 * @author      Jacek Olbrys
 *
 * @brief       Definition of Packet Format
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C++ Coding
 *              Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/

#include <stdint.h>

/***************************************************************************
 * Using
 **************************************************************************/

/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
namespace com_verifone_ipcpacket
{

/**
 * @addtogroup Pack
 * @{ 
 */

/*************************************************************************** 
 * Preprocessor constant definitions
 **************************************************************************/
//const uint16_t	MAX_DATA_LEN =	1024;

/*************************************************************************** 
 * Macro definitions
 **************************************************************************/

/*************************************************************************** 
 * Data type definitions
 **************************************************************************/

/*************************************************************************** 
 * Exported variable declarations
 **************************************************************************/

/*************************************************************************** 
 * Exported function declarations
 **************************************************************************/

/*************************************************************************** 
 * Exported class declarations
 **************************************************************************/

/**
 * @brief        The class describes the base class for the incoming packet.
 *               It contains corresponding functions
 *
 */

class CPacket
{
   public:
   enum error_t
   {
      EERROR = -1,
      ESUCCESS = 0,
      ENOTFOUND,
      ENOSPACE,
      EAPPNAME_IS_NULL,
      EBERTLV_IS_EMPTY
   };
   
   static const char *fromAppName;
   static const int MAX_PACKET_SIZE = 1024;
   static const int MAX_APP_NAME = 30;
   uint8_t getCmdCode();
   const char* getAppName();
   void destroyListTags( void );
   
  // virtual error_t SetData(uint8_t byCmd, char *szSender, uint8_t *data, uint32_t len) = 0;
  // virtual error_t SetData(char *szSender, uint8_t *data, uint32_t len) = 0;
 
   virtual ~CPacket()
   {
      ClearData();
   }

   protected:

   uint8_t byCmdCode;
   char abyAppName[MAX_APP_NAME+1];

   /* -------------------------------------------------------------------------- */
   /** @brief CPacket - simple constructor
    */
   /* ---------------------------------------------------------------------------- */
   CPacket(): pstHeadListTags(NULL)
   {
      ;
   }

   /* -------------------------------------------------------------------------- */
   /** @brief setCmdCode - function set byCmdCode attribute
    * 
    * @param[in] byCmdCodeI - number of command
    * 
    * @return ESUCCESS
    */
   /* ---------------------------------------------------------------------------- */
   c_card_error_t setCmdCode(const uint8_t &byCmdCodeI);

   /* -------------------------------------------------------------------------- */
   /** @brief setAppName - function copying name of the application to the getAppName
    *                      attribute
    * 
    * @param abyAppNameI - pointer to the buffer contains name of the application
    * 
    * @return 
              EERROR   - when the function failed
              ESUCCESS - when the function fills attributes
    */
   /* ---------------------------------------------------------------------------- */
   c_card_error_t setAppName(const char * abyAppNameI);

   /* -------------------------------------------------------------------------- */
   /** @brief makeListTags - function pars buffer to structures ber-tlv
    * 
    * @param abyBuf     - pointer to the buffer
    * @param byBufLen   - length of buffer with name of the application
    * 
    *         EERROR   - when the function failed
    *         ESUCCESS - when the function made the tag's list
    * @return 
    */
   /* ---------------------------------------------------------------------------- */
   c_card_error_t makeListTags(uint8_t *abyBuf, uint16_t byBufLen);

   /* -------------------------------------------------------------------------- */
   /** @brief addTagTLV - function adds tag to the list structures ber-tlv
    * 
    * @param pbyTag     - pointer to the buffer contains tag
    * @param byTagLen   - length of tag
    * @param pbyValue   - pointer to the buffer contains value for tag
    * @param ulnLen     - length of buffer with data
    * 
    * @return 
    *         EERROR   - when the function failed
    *         ESUCCESS - when the function added a tag to the tag's list
    */
   /* ---------------------------------------------------------------------------- */
   c_card_error_t addTagTLV( const u_char *pbyTag, u_char byTagLen, u_char *pbyValue, u_long ulnLen );

   /* -------------------------------------------------------------------------- */
   /** @brief ClearData - virtual function clearing attributes
    */
   /* ---------------------------------------------------------------------------- */
   virtual void ClearData();

   /* -------------------------------------------------------------------------- */
   /** @brief SetDataPacket
    * 
    * @param szSender 
    * @param byCmdCode 
    * @param pbyData 
    * @param byLen 
    * 
    * @return 
    */
   /* ---------------------------------------------------------------------------- */
   c_card_error_t SetDataPacket( char *szSender, uint8_t byCmdCode, uint8_t *pbyData, uint32_t byLen);
   
   private:
   /* -------------------------------------------------------------------------- */
   /** @brief CPacket -  constructor copying
    * 
    * @param CPacket& 
    */
   /* ---------------------------------------------------------------------------- */
   CPacket(CPacket& )
   {
   }

};

/**
 * @}
 */

/***************************************************************************
 * Module namspace: end
 **************************************************************************/
}

#endif /* CPACKET_H */

