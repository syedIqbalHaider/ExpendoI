#ifndef C_MEMORY_CARD_H
#define C_MEMORY_CARD_H

#ifndef __cplusplus 
#error "This file is for C++ only!"
#endif
/*****************************************************************************
 *
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/
/**
 * @file        cMemoryCard.h
 *
 * @author      Jacek Olbrys
 *
 * @brief       Support for SLE4442 Memory cards
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C++ Coding
 *              Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/
#include <stdint.h>

#include "cMemoryCard.h"

/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
namespace com_verifone_memory_card
{

    /**
     * @addtogroup Card
     * @{ 
     */

    static const int32_t MEM_OK = 0;
    static const int32_t MEM_FAILURE = -1;
    static const int32_t MEM_NO_DATA = -2;
    static const int32_t MEM_INVALID_PARAMETERS = -3;
    static const int32_t MEM_MEMORY_UNCHANGED = -4;
    static const int32_t MEM_MEMORY_CHANGED = -5;
    static const int32_t MEM_PWD_INCORRECT_1TRY_LEFT = -6;
    static const int32_t MEM_PWD_INCORRECT_2TRIES_LEFT = -7;

    class CMemoryCard
    {
        public:
            CMemoryCard();

            bool is_card_memory_card(uint8_t *ATR); // Detects card inserted type
            void card_removed() { weHaveSLE442 = false; clear(); }
            bool data_read() { return imageSize > 0; }
            int32_t read_card(); // Reads memory card, stores everything in internal buffer
            int32_t get_bytes(uint8_t *buffer, size_t bufferSize, uint8_t address, uint8_t bytesCount); // gets bytes from internal buffer
            int32_t write_bytes(uint8_t *buffer, uint8_t address, uint8_t bytesCount); // updates internal buffer
            int32_t update_card(uint8_t *pin); // writes internal buffer back to the card

        private:
            void clear();
            int32_t read_card_data(uint8_t *buffer); // No need to pass size, it's internal function

            static const int MEM_CARD_BUFFER_SIZE = 262;  // = max 256 byte image + 6 for command overhead
            uint8_t memoryCardImage[MEM_CARD_BUFFER_SIZE];
            uint8_t memoryCardOriginalImage[MEM_CARD_BUFFER_SIZE];
            int32_t imageSize;
            bool weHaveSLE442;
    };

/**
 * @}
 */

/***************************************************************************
 * Module namspace: end
 **************************************************************************/

} //namespace ends here
#endif

