#ifndef C_EMV_TRANSACTION_H 
#define C_EMV_TRANSACTION_H
 
#ifndef __cplusplus 
#  error "This file is for C++ only!"
#endif
/*****************************************************************************
 *
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/
/**
 * @file        cTransaction.h
 *
 * @author      Jacek Olbrys
 *
 * @brief       Transaction Header
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C++ Coding
 *              Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/
#include "cEMV.h"
#include "cIPCInterface.h"
#include "cMemoryCard.h"

/*************************************************************************** 
 * Preprocessor constant definitions
 **************************************************************************/
#define CARD_APP_CARD_MAGNETIC  0x01
#define CARD_APP_CARD_ICC       0x02

#define CARD_APP_MVT_DEFAULT    0

#define CARD_APP_DEF_TRACK1_LEN 79
#define CARD_APP_DEF_TRACK2_LEN 40
#define CARD_APP_DEF_TRACK3_LEN 107
#define CARD_APP_DEF_ATR_LEN    32*2+1 // for zero-termination
#define CARD_APP_DEF_TRACK_STAT_LEN 3

//GUIAPP message identifiers
#define CARD_APP_GUI_INSERT     9
#define CARD_APP_GUI_PLEASE_WAIT    4
#define CARD_APP_GUI_PROCESSING     3
#define CARD_APP_GUI_REMOVE         10
#define CARD_APP_GUI_ENTER_KEY      87

#define CARD_APP_GUI_NO_PROMPT  -1
#define CARD_APP_GUI_ENTER_PIN  1
#define CARD_APP_GUI_INCORRECT_PIN  2
#define CARD_APP_GUI_LAST_PIN       3

#define CARD_APP_GUI_S_PROMPTS      "prompts"
#define CARD_APP_GUI_S_EMV          "emv"
#define CARD_APP_GUI_S_PIN          "pin"


#define CARD_APP_MON_FOREVER        1
#define CARD_APP_MON_ONCE           0
#define CARD_APP_MON_NOBREAK        2

// #define NUM_TAGS_TO_CACHE            15

/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
namespace com_verifone_emv
{

/**
 * @addtogroup Cmd
 * @{ 
 */

class CEMVtrns
{
public:
    CEMVtrns(void);

    bool init_the_kernel();

    int send_notify(int notify_event);

    int update_config(int record);

    int monitor_ICC(int flags);

    int wait_for_card(int card_type, unsigned int timeout, bool detect_card_type = false, int icc_flags = 0);

    int EMVtransaction(int opers, CEMV *cEMV);

    inline void setTLVinput(unsigned char *InTLVlist_p, int InTLV_list_len_p) 
        {   InTLVlist = InTLVlist_p; 
            InTLV_list_len = InTLV_list_len_p;
        }
    inline void setTLVoutput(unsigned char *OutTLVlist_p, int OutTLV_list_len_p, unsigned char *OutTLVdata_p, int OutTLV_data_len_p, int TLV_data_len_p) 
        {
            OutTLVlist = OutTLVlist_p;
            OutTLV_list_len = OutTLV_list_len_p;
            OutTLVdata = OutTLVdata_p;
            OutTLV_data_len = OutTLV_data_len_p;
            TLV_data_len = TLV_data_len_p;
        }

    inline void InitCardReader(void) {return;}

    int CacheTags();
    int GetTag(unsigned short tag, unsigned char * buffer, unsigned short * size);
    void ClearTagsCache()
        {
            if (TagCache) delete [] TagCache;
            TagCache = 0;
            TagCacheSize = 0;
            // memset(CachedTags, 0, sizeof(CachedTags));
        }
    uint8_t ATR[CARD_APP_DEF_ATR_LEN];
    uint8_t track1[CARD_APP_DEF_TRACK1_LEN];
    uint8_t track2[CARD_APP_DEF_TRACK2_LEN];
    uint8_t track3[CARD_APP_DEF_TRACK3_LEN];
    uint8_t track_status[CARD_APP_DEF_TRACK_STAT_LEN];

    void ClearEMVCollxn();

    unsigned char *InTLVlist;
    int InTLV_list_len;
    unsigned char *OutTLVlist;
    int OutTLV_list_len;
    unsigned char *OutTLVdata;
    int OutTLV_data_len;
    unsigned char *script71;
    int script71_len;
    unsigned char *script72;
    int script72_len;
    int TLV_data_len;
    int icc_card_type;

    int monitor_flags;

    uint8_t cmd_status;

    com_verifone_memory_card::CMemoryCard sle442;

    int waitMagSemaphore() { return 0; }
    int releaseMagSemaphore() { return 0; }
    bool isMagSemaphoreBusy() { return false; }

    void getMagData(IPCPacket & response);

    int icc_card_status;

private:
    int processMagReader(int mag_handle);
    int processICCReader(int flags = 0);
    unsigned char * TagCache;
    unsigned int TagCacheSize;
    bool DetectCardType();
    bool IsI2CCard();
};

/**
 * @}
 */

/***************************************************************************
 * Module namspace: end
 **************************************************************************/
}
#endif

