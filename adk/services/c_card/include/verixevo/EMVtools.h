/*****************************************************************************
 * 
 * Copyright (C) 2007 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/
/**
 * @file       EMVTools.h
 *
 * @author     Jacek Olbrys
 * 
 * @brief      EMV Tool Prototypes
 *
 * @remarks    This file should be compliant with Verifone EMEA R&D C++ Coding   
 *             Standard 1.0.x  
 */
#include <string>

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

extern "C"
{
#include "emvsizes.h"
#include "est.h"
#include "mvt.h"

//#include <VXEMVAP_confio.h>
}


//here we define additional tags for Mobile Servus in SA
#define TAG_DFAF01_PACKET_TYPE 				0xDFAF01
#define TAG_DFAF03_ACTIVE_INTERFACES		0xDFAF03
#define TAG_DFAF0E_CAPK_MODULUS				0xDFAF0E
#define TAG_DFAF0F_CAPK_EXPONENT			0xDFAF0F
#define TAG_DFAF10_CAPK_HASH				0xDFAF10
#define TAG_DFAF11_PARTIAL_SELECTION		0xDFAF11
#define TAG_DFAF12_SECOND_AVN				0xDFAF12
#define TAG_DFAF13_REC_APP_NAME				0xDFAF13
#define TAG_DFAF14_RND_SEL_THRESHOLD		0xDFAF14
#define TAG_DFAF15_RND_SEL_PERCENTAGE		0xDFAF15
#define TAG_DFAF16_RND_MAX_RND_PERCENTAGE	0xDFAF16
#define TAG_DFAF17_TAC_DEFAULT				0xDFAF17
#define TAG_DFAF18_TAC_DENIAL				0xDFAF18
#define TAG_DFAF19_TAC_ONLINE				0xDFAF19
#define TAG_DFAF1A_DEFAULT_TDOL				0xDFAF1A
#define TAG_DFAF1B_DEFAULT_DDOL				0xDFAF1B
#define TAG_DFAF1C_FALLBACK					0xDFAF1C
#define TAG_DFAF1D_SELECTION				0xDFAF1D
#define TAG_DFAF1E_MCC						0xDFAF1E

#define TAG_DFAF1F_CAPK_EXPIRY				0xDFAF1F
#define TAG_DFAF32_CAPK_EXPIRY				0xDFAF32
#define TAG_DFCC20_CAPK_FILE				0xDFCC20

#define EMV_CONFIG_FILE						"EMV.ini"
#define EMV_CONFIG_GLOBAL					"global"
#define EMV_CONFIG_MVT						"MVT.DAT"
#define EMV_CONFIG_EST						"EST.DAT"

// This defines CAPK index treated as removed
#define CAPK_FILLER 0x00

/*
#ifdef VFI_GUI_GUIAPP

typedef int c_encPIN;

class cPINdisplay
{
	public:
		
		cPINdisplay();
		virtual ~cPINdisplay()
			{ }
		inline void SetEntryXY(long x_val, long y_val) {x=x_val; y=y_val;}
		inline long GetEntryX() { return x; }
		inline long GetEntryY() { return y; }
		inline void SetMainMsg(std::string msg) {main_msg = msg;}
		inline void SetExtraMsg(std::string msg) {extra_msg = msg;}
		inline void SetDisplayAmount(int disp_line) {amount_line = disp_line;}
		virtual int Display(c_encPIN encPIN) = 0;
		virtual bool ComputeEntryXY() = 0;
		
		std::string BuildAmtMsg();
		std::string BuildExtraMsg();

	protected:
		std::string main_msg;
		std::string extra_msg;
		int amount_line;
		long x;
		long y;
};

class cPINdisplayDefault: public cPINdisplay
{
	public:
		cPINdisplayDefault(): cPINdisplay()
			{}
		virtual ~cPINdisplayDefault()
			{ clrscr(); }
		virtual int Display(c_encPIN encPIN);
		virtual bool ComputeEntryXY();
};

class cPINdisplayGuiApp: public cPINdisplay
{
	public:
		cPINdisplayGuiApp();
		virtual ~cPINdisplayGuiApp();
		virtual int Display(c_encPIN encPIN);
		virtual bool ComputeEntryXY();
	private:
		bool guiConnected;
		bool consoleClaimed;
		bool pinExecuted;
};
#endif
*/
/*class cAppSelDisplay
{
	public:
		cAppSelDisplay();
		virtual ~cAppSelDisplay();
		virtual int Display() = 0;
	protected:
	private:
}; */
	
class cUpdateCfg
{
	public:
		cUpdateCfg() {CAPK_index = 0xFF; CAPK_modulus.clear(); CAPK_exponent.clear(); CAPK_file.clear(); ESTrecord = -1; currentRID.clear();}
		int updateConfig(int tagNum, unsigned char * value, int Len);
		int commitChanges(bool updateEST = true);
		int locateConfigRecords(const char *AID, bool createNew = true);
		int updateCurrentAID(char *AID, char *ver1, char *ver2, short partial_flag, char *name);
		int updateCAPK(short index, char *filename, int position, char *expiry);
		std::string getConfig(const char *AID, short CAPKIndex);
		int deleteConfig(const char * AID);
		int buildCAPK(void);
		std::string getCAPK(short index);
		int deleteCAPK(const char * AID, short index);
		inline void setCAPKmodulus(char *modulus, int mod_size) {CAPK_modulus.assign(modulus, mod_size);}
		inline void setCAPKexponent(char *exponent, int exp_size) {CAPK_exponent.assign(exponent, exp_size);}
		inline void setCAPKhash(char *hash, int hash_size) {CAPK_hash.assign(hash, hash_size); }
		inline void setCAPKexpiry(char *expiry, int expiry_size) {CAPK_expiry.assign(expiry, expiry_size); }

	private:
		EST_REC EST_current;
		MVT_REC MVT_current;
		int EST_offset;
		int MVT_offset;
		int AID_index;
		short CAPK_index;
		std::string CAPK_modulus;
		std::string CAPK_exponent;
		std::string CAPK_hash;
		std::string CAPK_file;
		std::string CAPK_expiry;
		
		void setDefaultMVT(MVT_REC * ptrMVT);
		void setDefaultEST(EST_REC * ptrEST);
		
		void setKernelChecksum(MVT_REC * ptrMVT);
		int ESTrecord;
		std::string currentRID;
};

void printConfig(char *configFilename);
int genConfig(char *configFilename);
int importConfig(char *configFilename);
void check_RAM(void);
int parseInTLV(unsigned char *buffer, int buflen, bool permanent);
int parseOutTLV(unsigned char *bufferIn, int buflenIn, unsigned char *bufferOut, int buflenOut);
int inGetPTermID(char *ptid);
int get_display_resolution(int &x, int &y);
unsigned short getUsrPin(unsigned char *pstPin);
void usEMVDisplayErrorPrompt(unsigned short errorID);
unsigned short usEMVPerformSignature(void);
unsigned short usEMVPerformOnlinePIN(void);
EMVResult usEMVIssAcqCVM(unsigned short issacq, unsigned short *code);
EMVBoolean bIsCardBlackListed(byte * pan, unsigned short panLen, byte * panSeqNo, unsigned short panSeqLen);
void InitMinimumTLVset(void);
void SetDefaultFunctionPointers(void);
int GetAIDPair(void);
unsigned short usCandListModify_Default(srAIDList * candidateList);
unsigned short GetPINStatusDetail();
bool setPINParams();
void displaySecurePINPrompt();
void restoreGuiAppConsole();
bool isCardRemoved();
bool isCardInserted();


bool isTagBlacklisted(int tagNum);

int isSeparateGPO();

int getScreenMaxX();
int getScreenMaxY();
int get_console_handle();
int DisplayPromptSelectSection(int prompt_number, const char * const section, int at_x, int at_y, bool clear_screen = false);
int DisplayPromptSelectSection(int prompt_number, const char * const section, bool clear_screen = true);
int DisplayText(const char * const text, int text_len, int at_x, int at_y, bool clear_screen = false);
int DisplayText(const char * const text, int text_len, bool clear_screen = true);

