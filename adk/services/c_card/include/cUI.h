
/*****************************************************************************
 *
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/
/**
 * @file        cUI.h
 *
 * @author      Jacek Olbrys
 *
 * @brief       UI Interface warpper
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C++ Coding
 *              Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/

extern "C"
{
#include "vxemvapdef.h"
}
#include "EMVCWrappers.h"
#include "EMVCStructs.h"

/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
namespace com_verifone_emv
{

/**
 * @addtogroup Card
 * @{ 
 */

/***************************************************************************
 *    external C++ - API's
 **************************************************************************/
class CUI
{
public:
   CUI(void);

    int (*inMenuFunc) (char **, int);
    void (*vdPromptManager) (unsigned short);
    unsigned short (*usAmtEntryFunc) (unsigned long *);
    unsigned short (*usCandListModify)(srAIDList *psrCandidateList);

private:

protected:
}; /* CHost */

/**
 * @}
 */

/***************************************************************************
 * Module namspace: end
 **************************************************************************/
}

