#ifndef COMMANDMAN_H
#define COMMANDMAN_H

#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 * 
 * Copyright (C) 2007 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/
/**
 * @file       CommandMan.h
 *
 * @author     Jacek Olbrys
 * 
 * @brief      Definition of command manager class
 *
 * @remarks    This file should be compliant with Verifone EMEA R&D C Coding  
 *             Standard 1.0.0 
 */

/***************************************************************************
 * Includes
 **************************************************************************/
#include <string.h>

#include "cCmd.h"

/***************************************************************************
 * Using
 **************************************************************************/

/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
namespace com_verifone_cmd
{

/**
 * @addtogroup Cmd
 * @{ 
 */

/*************************************************************************** 
 * Preprocessor constant definitions
 **************************************************************************/
#define CMD_MAN_MAX_CMD   40

/*************************************************************************** 
 * Macro definitions
 **************************************************************************/

/*************************************************************************** 
 * Data type definitions
 **************************************************************************/
typedef struct RegItem
{
      /* registered class */
   uint8_t cmdID;

      /* registered command object */
   CCmd* cmdObj;

} RegItem_t;

/*************************************************************************** 
 * Exported variable declarations
 **************************************************************************/

/*************************************************************************** 
 * Exported function declarations
 **************************************************************************/

/*************************************************************************** 
 * Exported class declarations
 **************************************************************************/

/**
 * @brief        The class is a base for other communication classes.
 *
 */
class CCommandMan
{
   public:
      /**
       * @brief          Constructor
       *
       */
      CCommandMan(): m_regIdx(0), m_underProcCmd(NULL) 
            {memset(m_regData, 0, sizeof(m_regData));}


      /**
       * @brief            This function registers the command object with Class and Instruction.
       *                   If the Class and Instruction fields in the incoming packet matche the
       *                   registration data the corresponding command object is called.
       *
       * @param[in]    cmdID - command ID
       * @param[in]    cmd   - pointer to command object
       *
       * @return       0 when OK, otherwise -1
       *
       */
      int32_t regCmd(uint8_t cmdID, CCmd *cmd);

      /**
       * @brief            This function sprocess the incoming packet. It does the following operations:
       *                       a) do a basic checks
       *                       b) send response "busy" if device is processing another packet
       *                       b) call the right registered command object
       *
       * @param[in]    pack - incoming packet
       *
       * @return       0 when OK, otherwise -1
       *
       */
      int32_t procExter(IPCPacket *pack);

    
   protected:
   private:

      /**
       * @brief            This function gets the command object which was registered with 
       *                      Class and instruction.
       *
       * @param[in]    cmdID - command number
       *
       * @return       Registered command object or NULL
       *
       */
      CCmd* getCmdObj(uint8_t cmdID);

      /* registered packet fields and command objects */
      RegItem_t m_regData[CMD_MAN_MAX_CMD];
      /* index to the first empty entry */
      uint32_t m_regIdx;
      /* pointer to command object which is currently processed */
      CCmd* m_underProcCmd;

};

/**
 * @}
 */

/***************************************************************************
 * Module namspace: end
 **************************************************************************/
}

#endif /* COMMANDMAN_H */

