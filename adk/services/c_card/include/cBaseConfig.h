#ifndef C_BASE_CONFIG_H 
#define C_BASE_CONFIG_H
 
#ifndef __cplusplus 
#  error "This file is for C++ only!"
#endif 
/*****************************************************************************
 *
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/
/**
 * @file        CBaseConfig.h
 *
 * @author      Jacek Olbrys
 *
 * @brief       Configuration file parser
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C++ Coding
 *              Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/
#include <stdio.h>
#include "CARDApp.h"

/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
using namespace com_verifone_crdapi;
namespace com_verifone_emv
{

/***************************************************************************
 *    Definitions
 **************************************************************************/
#define DRIVE_RAM       "I:"
#define DRIVE_FLASH     "F:"
#define CONFIG_FILE_NAME "C_CARD_CONFIG"
#define CONFIG_SECTION "C_CARD_SECTION"
#define CONFIG_SECTION_DEF "c_card"

/***************************************************************************
 *    external C++ - API's
 **************************************************************************/
class CBaseConfig
{
public:
   CBaseConfig() :  m_pFile(0) {}
   virtual ~CBaseConfig() { close(); }
   
   /** 
    * @brief Error codes
    */
   /*
   typedef enum
   {
      ESUCCESS = 0,  //< Success code 
      EERROR         //< Generic error code 
   } error_t;
*/
   
   /** 
    * @brief Open configuration file
    * 
    * @param[in] pFilename Configuration filename
    * 
    * @return SUCCESS on success
    */
   c_card_error_t open(const char *pFilename, const char *pMode="r");
   /** 
    * @brief Check if configuration file is open 
    * 
    * @return true if file is open
    *
    * @see #open
    */
   bool isOpen(void) {    return !!m_pFile; }
   /** 
    * @brief Close configuration file
    */
   void close(void);

   /** 
    * @brief Write text to a file
    * 
    * @param[in] format Format (like scanf)
    * @param[in] ... Parameter for format
    * 
    * @return Negative value on error
    */
   int writeFormat(const char *format, ...);

   /** Trim from unused chars return new pointer
    */
   char* trim(char *string);
   /** 
    * @brief Utility function to check if file exists
    * 
    * @param[in] pFilename Filename 
    * 
    * @return true if file exists
    */
   static bool fileExists(const char *pFilename);

   /** 
    * @brief read the config file
    * 
    * @param[in] filename
    * 
    * @return Negative value on error
    */
   c_card_error_t iReadConfig(char const * pFilename);

   /** 
    * @brief Utility function to print the data to the log
    */
   void vPrintConfig();


   char section_req[40];
   char configFilename[FILENAME_MAX+3];

protected:
  /** 
      * @Abstract method that  must be implemented by the derived class
      */
    virtual void vPrintDerivedConfig()=0;

  /** 
      * @Abstract method that  must be implemented by the derived class
      */
    virtual int iReadDerivedConfig(const char *key, const char *val)=0;
  

private:
   FILE *m_pFile; /**< Configuration file handle */
   

}; /* CBaseConfig */

/**
 * @}
 */

/***************************************************************************
 * Module namspace: end
 **************************************************************************/
}
#endif /* C_BASE_CONFIG_H */

