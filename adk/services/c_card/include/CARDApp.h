/*****************************************************************************
 *
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/
/**
 * @file        CARDApp.h
 *
 * @author      Jacek Olbrys
 *
 * @brief       CardApp service header
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C++ Coding
 *              Standard 1.0.x
 */


#ifndef __CardApp_H
#define __CardApp_H

#include <stddef.h>

#ifdef __cplusplus
namespace com_verifone_crdapi {

//Namespace
#endif

enum
{
	ACK_RESPONSE_TIME = 1000
};

/***************************************************************************
 *    Definitions
 **************************************************************************/
enum cardapp_commands
{
	// API Command types
	RESET_EXTERNAL_READER								=	54,			//this command is compatible with SCAPP
	CMD_CANCEL_WAITING									=	90,
	CMD_START_WAITING										=	91,
	CMD_MONITOR_ICC											=	92,
	CMD_UNSOLICITED_EVENT								=	93,
	CMD_SEND_APDU												=	94,
	CMD_SET_GET_TLV											=	95,
	CMD_SET_PIN_PARAMS									=	96,
	CMD_VERIFY_PIN											=	97,
	CMD_SET_MESSAGE											=	98,
	CMD_EXTERNAL_APP_SEL								=	99,
	CMD_LEDS_CONTROL										=	100,
	CMD_L1_BEGIN_TRANSACTION						=	111,
	CMD_L1PLETE_TRN											=	112,
	CMD_L2_SELECT_READ									=	121,
	CMD_L2_TRM_VERIFY										=	122,
	CMD_L2_FIRST_AC											=	123,
	CMD_L2PLETE													=	124,
	CMD_L3_SELECT												=	131,
	CMD_L3_GPO													=	132,
	CMD_L3_READ_DATA										=	133,
	CMD_L3_CARDHOLDER_VERIFY						=	134,
	CMD_L3_TRM													=	135,
	CMD_L3_DATA_AUTH										=	136,
	CMD_L3_FIRST_AC											=	137,
	CMD_L3_EXT_AUTH											=	138,
	CMD_L3_SCRIPT_PROCESSING						=	139,
	CMD_L3_SECOND_AC										=	130,
	CMD_RESET_EMV_CONFIGURATION					=	140,
	CMD_SUPER_EMV												=	200,
	CMD_READ_I2C_CARD										=	170,
	CMD_WRITE_I2C_CARD									=	171,
	CMD_READ_MEMORY_CARD								=	172,
	CMD_UPDATE_MEMORY_CARD							=	173,
	CMD_WRITE_MEMORY_CARD								=	174
};

// return codes and error codes
enum return_codes
{
	CRD_OKAY														=	0,
	CRD_FAILED													=	1,
	CRD_TIMEOUT													=	2,
	CRD_TRAN_DECLINED										=	4,
	CRD_TRAN_APPROVED										=	5,
	CRD_TRAN_ONLINE											=	6,
	CRD_PIN_CANCEL											=	20,
	CRD_PIN_BYPASS											=	21,
	CRD_CANCEL													=	22,
	CRD_SIZE_ERROR											=	23,
	CRD_MAC_VER_FAILED									=	24,
	CRD_MSG_SIZE_TOO_LONG								=	25,
	CRD_SEQ_ERROR												=	26,
	CRD_ICC_FAIL												=	27,
	CRD_INIT_ERROR											=	28,
	CRD_CODE_RETRY_LIMIT								=	31,
	CRD_CODE_INVALID_REQ								=	35,
	CRD_CODE_API_REQ										=	36,
	CRD_CODE_INVALID_CMD								=	37,
	CRD_CODE_INVALID_LOCAL_KEY_IDX			=	40,
	CRD_CODE_UPDATE_KEY_FAILED					=	41,
	CRD_CODE_TRANSPORT_KEYS_CHANGED			=	42,
	CRD_CODE_KEY_UPDATE_LIMIT						=	43,
	CRD_CODE_INVALID_KEY								=	45,
	CRD_CODE_PIN_BLOCKED								=	90,
	CRD_CODE_PIN_GET_CHALLENGE_ERROR		=	91,
	CRD_CODE_PIN_ENCIPHER_ERROR					=	92,
	CRD_CODE_PIN_VERIFY_FAILED					=	93,
	CRD_CODE_PIN_MISSING_PIN_BLOCK_DATA	=	94,
	CRD_CODE_INVALID_KEY_LENGTH					=	95,
	CRD_CODE_MISSING_TMK_OR_DUKPT_KEY		=	96,
	CRD_CODE_MISSING_TPK_OR_TAK					=	97,
	CRD_CODE_MISSING_STAN_OR_PAN				=	98,
	CRD_CODE_CARD_INSERTED							=	99,
	CRD_CODE_CARD_REMOVED								=	100,

	CRD_CODE_MEM_CARD_UNCHANGED					=	101,
	CRD_CODE_MEM_CARD_CHANGED						=	102,
	CRD_CODE_CARD_SWIPED								=	103,
	CRD_CODE_CARD_ICC_DISABLED					=	104,
	CRD_CODE_CARD_ICC_ENABLED						=	105,

	CRD_CODE_CMD_BUSY										=	249,
	CRD_CODE_INVALID_APP_ID							=	250,
	CRD_CODE_POS_INVALID_FIELD_LEN			=	251,
	CRD_CODE_POS_MSG_LEN								=	252,
	CRD_CODE_UNKNOWN_CMD								=	253,
	CRD_CODE_INVAL											=	254,
};

enum c_card_error_t
    {
      ESUCCESS = 0,
      EHANDCOMMERROR,
      EPOSTIMEOTMISS,
      EHOSTIDMISS,
      EKEYTYPEMISS,
      EKEYDATA,
      EMESSFORMACMISS,
      EMESSFORENCMISS,
      EMESSFORDECMISS,
      EGENMACMISS,
      EMACFAILURE,
      ELOCKMISS,
      EACTIONCODEMISS,
      ECANCEL,
      EPOSTIMEOUT,
      ESEQERR,
      ERESPONSESEND,
      ENOTCOMPLETE,
      EERROR
    };

enum c_card_led_opers_t
    {
        LED_DISABLE = 0,
        LED_ENABLE = 1,
        LED_BLINK = 2
    };

#define SEND_EXTRA_STATUS_NOTIFY 1


#define CARD_APP_FLAG_USE_ICC	0x20
#define CARD_APP_FLAG_USE_MAG	0x10
#define CARD_APP_FLAG_BLOCKING	0x80
#define CARD_APP_FLAG_UNSOLICITED	0x40
#define CARD_APP_FLAG_GET_PUB_KEY	0x04
#define CARD_APP_FLAG_ASYNC			0x08
#define CARD_APP_FLAG_CARD_TYPE		0x8000 // top bit


#define RESET_FLAGS_CLEAR_EMV_FILES       0x01
#define RESET_FLAGS_CLEAR_EMV_COLLXN      0x02
#define RESET_FLAGS_CLEAR_UPDATE_DONE     0x80000000

#define APDU_FLAGS_CLOSE_ICC_READER       0x01
#define APDU_FLAGS_COLD_RESET_ICC_READER  0x02
#define APDU_FLAGS_WARM_RESET_ICC_READER  0x03

#define CARD_APP_STATUS_M_CONTINUE	0x0000
#define CARD_APP_STATUS_M_STOP		0x0001
#define CARD_APP_STATUS_M_OK		0x0000
#define CARD_APP_STATUS_M_WARNING	0x0020
#define CARD_APP_STATUS_M_ERROR		0x0010
#define CARD_APP_STATUS_M_PIN		0x0002
#define CARD_APP_STATUS_M_SIGNATURE	0x0004

#define CARD_APP_STATUS_D_OK			0x0000
#define CARD_APP_STATUS_D_EMPTY			0x0001
#define CARD_APP_STATUS_D_CHIP_ERROR	0x0002
#define CARD_APP_STATUS_D_REMOVED		0x0004
#define CARD_APP_STATUS_D_APPL_BLOCKED		0x0008
#define CARD_APP_STATUS_D_PARAMETER		0x0010
#define CARD_APP_STATUS_D_CANCELLED		0x0020
#define CARD_APP_STATUS_D_GENERAL		0x4000
#define CARD_APP_STATUS_D_BYPASS		0x0040
#define CARD_APP_STATUS_D_TIMEOUT		0x0080
#define CARD_APP_STATUS_D_CARD_BLOCKED		0x0100
#define CARD_APP_STATUS_D_CONF_EMPTY		0x0200


#define CARD_APP_STATUS_D_FALLBACK		0x8000
#define CARD_APP_STATUS_D_NO_FALLBACK	0x0000

#if 0
#define CARD_APP_RET_OK						0x00
#define CARD_APP_RET_NO_UI				0x01
#define CARD_APP_RET_REMOVED			0x02
#define CARD_APP_RET_ORDER				0x03
#define CARD_APP_RET_DECLINED			0x04
#define CARD_APP_RET_APPROVED			0x05
#define CARD_APP_RET_ONLINE				0x06
#define CARD_APP_RET_ICC_FAIL			0x07
#define CARD_APP_RET_TIMEOUT			0x08
#define CARD_APP_RET_ICC_OK				0x09
#define CARD_APP_RET_MAG_OK				0x0A
#define CARD_APP_RET_MSG_RECEIVED	0x0B
#define CARD_APP_RET_CANCELED			0x0C
#define CARD_APP_RET_FAIL					0xFF
#endif

#define CARD_APP_OPER_TLV		0x0000
#define CARD_APP_OPER_SELECT	0x0001
#define CARD_APP_OPER_GPO		0x0002
#define CARD_APP_OPER_READ		0x0004
#define CARD_APP_OPER_VERIFY	0x0008
#define CARD_APP_OPER_RESTRICT	0x0010
#define CARD_APP_OPER_TRM		0x0020
#define CARD_APP_OPER_DATA_AUTH	0x0040
#define CARD_APP_OPER_FIRST_AC	0x0080
#define CARD_APP_OPER_EXT_AUTH	0x0100
#define CARD_APP_OPER_SCRIPT	0x0200
#define CARD_APP_OPER_SECOND_AC	0x0400
#define CARD_APP_OPER_PIN_VERIFY	0x0800
#define CARD_APP_OPER_CONFIG	0x8000

#define CARD_APP_DEF_MAX_SCRIPTS 5
#define CARD_APP_DEF_ONLINE_PIN		0x80


#define PIN_ENTRY_STATUS_OK                     0
#define PIN_ENTRY_STATUS_BYPASSED_POS           1
#define PIN_ENTRY_STATUS_BYPASSED_CARDHOLDER    2
#define PIN_ENTRY_STATUS_CARD_REMOVED           3
#define PIN_ENTRY_STATUS_CANCELLED_POS          4
#define PIN_ENTRY_STATUS_CANCELLED_CARDHOLDER   5
#define PIN_ENTRY_STATUS_TIMEOUT                6

enum pin_type
{
    CARD_APP_PIN_OFFLINE = 1,
    CARD_APP_PIN_ONLINE_VSS,
    CARD_APP_SECURE_PIN_OFFLINE,
    CARD_APP_PIN_OFFLINE_EXT,
    CARD_APP_EXTERNAL_PIN_OFFLINE // GVR
};

enum pin_status
{
	CARD_APP_PIN_OK,
	CARD_APP_PIN_INCORRECT,
	CARD_APP_PIN_LAST,
	CARD_APP_PIN_BYPASS,

};


enum unsolicited_events
{
	CARD_APP_EVT_PIN_BYPASSED = 1,
	CARD_APP_EVT_PIN_CANCELLED,
	CARD_APP_EVT_PIN_REQUIRED,
	CARD_APP_EVT_PIN_TIMEOUT,
	CARD_APP_EVT_PIN_LAST,
	CARD_APP_EVT_PIN_BLOCKED,
	CARD_APP_EVT_APP_SELECT,
	CARD_APP_EVT_APP_SELECT_DONE,
	CARD_APP_EVT_PIN_ENTRY,
	CARD_APP_EVT_PIN_DONE,
	CARD_APP_EVT_RST_FAILED,
	CARD_APP_EVT_PIN_WRONG,
	CARD_APP_EVT_PIN_OK,
	CARD_APP_EVT_PIN_DIGIT


};


enum card_types
{
	CARD_APP_CARDTYPE_REQUIRES_POWERUP = -2,
	CARD_APP_CARDTYPE_NOT_SET = -1,
	CARD_APP_CARDTYPE_UNKNOWN = 0,
	CARD_APP_CARDTYPE_EMV_ASYNC = 2,
	CARD_APP_CARDTYPE_EMV_SYNC = 4,
	CARD_APP_CARDTYPE_I2C = 8,
	CARD_APP_CARDTYPE_TRADEPRO = 16

};

enum pin_styles
{
	CARD_APP_PIN_NO_AMOUNT = 0x80
};

#define ISO2_SPAIN "es"


/***************************************************************************
 *    Types
 **************************************************************************/


/***************************************************************************
 *    external C++ - API's
 **************************************************************************/
#ifdef __cplusplus
class cardapp_client
{
public:
	cardapp_client(const char *app_name,unsigned long timeout = 5000) :connected(false)
	{
		if(Connect(app_name, timeout)==0)
			connected = true;
	}
	bool IsConnected() { return connected; }

	int WaitForCard(int flags, int timeout);
	int EMV(int operation, const char *to_add, const char *to_get, char *get_here, int buf_len);


private:
	int Connect( const char *eapp_name, unsigned long timeout );

	bool connected;
	char app_name_crd[64];
	static const char *app_name;
	//long printer_owner_id;
	size_t AsciiToBin(const char * asciiBuf, unsigned char * const binBuf, size_t binSize);
	size_t BinToAscii(const unsigned char * binBuf, size_t binLen, char * const asciiBuf, size_t asciiSize);

private:	//NonCopyable
	cardapp_client(const cardapp_client &c);
	cardapp_client& operator=(const cardapp_client &c);

};
#endif

/***************************************************************************
 *    external C - API's
 **************************************************************************/

#ifdef __cplusplus
extern "C" {
//#error C++
#endif

	int CardApp_Connect( const char *app_name, unsigned long timeout );
	int iCardAppWaitForCard(int flags, int timeout);
	int iCardAppAPDU(char * out, char * in);
	int iCardAppTLV(int operation, char *TLV, int *TLVlen);
	int iCardAppVerify(char *PINkey, int keylen);
	//int iCardAppSetUI(LANGUAGE_SET *language);
	int iCardApEMV(int operation, const char *to_add, const char *to_get, char *get_here, int buf_len);
	void CardApp_Disconnect(void);

#ifdef __cplusplus
}	//extern "C"
#endif

#ifdef __cplusplus
}
//Namespace
#endif

#endif

