#ifndef CMD_H
#define CMD_H

#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 * 
 * Copyright (C) 2007 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/
/**
 * @file       cCmd.h
 *
 * @author     Jacek Olbrys
 * 
 * @brief      Implements the base command class
 *
 * @remarks    This file should be compliant with Verifone EMEA R&D C++ Coding   
 *             Standard 1.0.x  
 */

/***************************************************************************
 * Includes
 **************************************************************************/
#include "stdint.h"
#include "tlv_parser.h"
#include "libipcpacket/ipcpacket.hpp"
#include "CARDApp.h"

/***************************************************************************
 * Using
 **************************************************************************/
using namespace com_verifone_ipcpacket;
using namespace com_verifone_crdapi;

/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
namespace com_verifone_cmd
{

/**
 * @addtogroup Cmd
 * @{ 
 */

/*************************************************************************** 
 * Preprocessor constant definitions
 **************************************************************************/

#if defined (__ARMCC_VERSION)
  #define ALIGN_PTR __packed
#else // __GNUC__
  #define ALIGN_PTR
#endif

/*************************************************************************** 
 * Macro definitions
 **************************************************************************/

/*************************************************************************** 
 * Data type definitions
 **************************************************************************/

/*************************************************************************** 
 * Exported variable declarations
 **************************************************************************/

/*************************************************************************** 
 * Exported function declarations
 **************************************************************************/

/*************************************************************************** 
 * Exported class declarations
 **************************************************************************/

/**
 * @brief        Base class for all other command class
 *
 */
class CCmd
{
public:
      
   /** 
    * @brief Error enumeration.
    *   
    * @note This errors are return from handler function.            
    */
   
      
   /**
    * @brief      This function implements command handling.
    * 
    * @note       This is virtual function, which should be redefined in each command class.
    *             Allways returns succcess to caller function and sends RESP_CODE_UNKNWON_CMD to sender.
    *  
    * @param[in]  pack       Incomming package
    * 
    * @return     
    *             - ESUCCESS   Always return success to caller.
    */
   virtual c_card_error_t handleCommand(IPCPacket *pack);

   /**
    * @brief      This function check if Cancel Process Command comes and send generic response to sender.
    * 
    * @note       This is virtual function, which may be redefined in each command class. 
    *  
    * @param[in]  pack       Incomming package
    * 
    * @return     
    *             - ESUCCESS      Continue command handling
    *             - ECANCEL       Cancel process command comes
    *             - EPOSTIMEOUT   Timeout
    *             - ESEQERR       Sequence error
    *             - ERESPONSESEND Error while sending response   
    */
   virtual c_card_error_t isCanceled(IPCPacket &response);
   
   /**
    * @brief      Virtual destructor.
    * 
    */
   virtual ~CCmd()
   {
   }

protected:
private:
};

/**
 * @}
 */

/***************************************************************************
 * Module namspace: end
 **************************************************************************/
}

#endif /* CMD_H */

