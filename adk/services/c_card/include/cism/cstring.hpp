#ifndef C_STRING_HPP_
#define C_STRING_HPP_
/*****************************************************************************
 *
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/
/**
 * @file        cstring.hpp
 *
 * @author      LucjanBryndza
 *
 * @brief       cstring class
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C++ Coding
 *              Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/
#include <stddef.h>
#include <cstring>
#include <cstddef>
#include <cassert>
#include <cstring>

/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
namespace com_verifone_utils
{

/**
 * @addtogroup Card
 * @{ 
 */

/* Not compatibile with ISO-C++ new doesnt throw
TODO: Compatibility with true stl string */

/***************************************************************************
 *    Templates
 **************************************************************************/
template <typename T> T min(T a, T b)
{
    return a < b ? a : b ;
}

/***************************************************************************
 *    external C++ - API's
 **************************************************************************/
class c_string
{
public:

    //Default constructor
    c_string()
    {
        m_capacity = ccapacity(1);
        str = new char[m_capacity];
        assert(str);
        str[0] = '\0';
    }
    //Constructor from string
    c_string(const char *s)
    {
        size_t length = std::strlen(s);
        m_capacity = ccapacity(length);
        str = new char[m_capacity];
        assert(str);
        std::strcpy( str, s );
    }
    //Constructor from selected size
    c_string(const char *s, size_t n)
    {
        m_capacity = ccapacity(n);
        str = new char[m_capacity];
        assert(str);
        std::memcpy( str, s, n );
        str[n] = '\0';
    }
    //Copy constructor
    c_string(const c_string & cs)
    {
        m_capacity = cs.m_capacity;
        str = new char[m_capacity];
        assert(str);
        std::strcpy( str, cs.str );
    }
    //Destructor
    ~c_string()
    {
        delete [] str;
    }
    //Assign operator
    c_string& operator=(const c_string &s)
    {
        if ( this != &s) //Prevent assign itself
        {
            if(m_capacity != s.m_capacity)
            {
                m_capacity = s.m_capacity;
                delete [] str;
                str = new char[m_capacity];
                assert(str);
            }
            std::strcpy( str, s.str );
        }
        return *this;
    }
    //Assign operator
    c_string& operator=(const char * s)
    {
        clear();
        if(s) {
            size_t length = std::strlen(s);
            m_capacity = ccapacity(length);
            str = new char[m_capacity];
            assert(str);
            std::strcpy( str, s );
        }
        return *this;
    }
    //Fast operators
    char* c_str() { return str; }
    const char* c_str() const { return str; }
    std::size_t capacity() const { return m_capacity; }
    std::size_t size() const { return std::strlen(str); }
    bool        empty() const { return (std::strlen(str)==0); }
    void clear() { str[0] = '\0'; }
    const char& operator[] ( std::size_t pos ) const
    {
        assert(std::strlen(str)>=pos);
        return str[pos];
    }
    char& operator[] ( std::size_t pos )
    {
        assert(std::strlen(str)>=pos);
        return str[pos];
    }

    void reserve(std::size_t n)
    {
        if( n > (std::strlen(str)+1) )
        {
            char *new_spc = new char[ccapacity(n)];
            assert(new_spc);
            std::strcpy( new_spc, str );
            delete [] str;
            str = new_spc;
        }
    }
    c_string& operator+=(const c_string &s)
    {
        if(size()+s.size() >= capacity())
        {
            m_capacity = ccapacity(size() + s.size());
            char *tmp = new char[m_capacity];
            assert(tmp);
            //std::strcpy(tmp,str);
            delete [] str;
            str = tmp;
        }
        std::strcat( str, s.str );
        return *this;
    }
    void pad(int padded_len, const char *padchar = " ")
    {
        m_capacity = ccapacity(size() + padded_len);
        char *tmp = new char[m_capacity];
        assert(tmp);
        for (int i = 0; i < padded_len; i++) { 
            std::strcat(tmp, padchar); 
        } 
        std::strcat(tmp, str);
        delete [] str;
        str = tmp;
    }   
private:
    std::size_t ccapacity(std::size_t orig_size)
    {
        return (orig_size/64 + 1) *64 + 1;
    }

private:
    std::size_t m_capacity;
    char *str;
};

static inline bool operator== ( const c_string& lhs, const c_string& rhs )
{
    return !std::strncmp( lhs.c_str(), rhs.c_str(), min(lhs.capacity(),rhs.capacity()) );
}

static inline bool operator== ( const char* lhs, const c_string& rhs )
{
    return !std::strncmp( lhs, rhs.c_str(), min(std::strlen(lhs)+1,rhs.capacity()) );
}

static inline bool operator== ( const c_string& lhs, const char* rhs )
{
    return !std::strncmp( rhs, lhs.c_str(), min(std::strlen(rhs)+1,lhs.capacity()) );
}

static inline bool operator!= ( const c_string& lhs, const c_string& rhs )
{
    return !(lhs==rhs);
}

static inline bool operator!= ( const char* lhs, const c_string& rhs )
{
    return !(lhs==rhs);
}

static inline bool operator!= ( const c_string& lhs, const char* rhs )
{
    return !(lhs==rhs);
}

/**
 * @}
 */

/***************************************************************************
 * Module namspace: end
 **************************************************************************/
}
#endif

