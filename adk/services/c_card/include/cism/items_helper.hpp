#ifndef ITEMS_HELPER_HPP_
#define ITEMS_HELPER_HPP_
/*****************************************************************************
 *
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/
/**
 * @file        items_helper.h
 *
 * @author      LucjanBryndza
 *
 * @brief       items helper
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C++ Coding
 *              Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/
#include <vector>
#include "cism/cstring.hpp"

/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
namespace com_verifone_utils
{

/**
 * @addtogroup Card
 * @{ 
 */


/***************************************************************************
 *    external C++ - API's
 **************************************************************************/
/* FIXME: GUI command should use STL containers, or constant string
 * after rewriting cGui can use direct STL vector instead helper class
 */
class cstring_list
{
public:
    //Default constructor
    cstring_list(const std::vector<c_string> &item)
    {
        len= item.size();
        items = new char*[len];
        int item_count = 0;
        for( std::vector<c_string>::const_iterator i= item.begin();
            i!= item.end();
            i++, item_count++
           )
        {

            items[item_count] = new char[i->size()+1];
            std::strcpy(items[item_count],i->c_str());
        }

    }
    //Default destructor
    ~cstring_list()
    {
        for(std::size_t i=0; i<len; i++)
        {
            delete [] items[i];
        }
        delete [] items;
    }
    //Get
    char ** c_str_array() { return items; }
    std::size_t size() const { return len; }
private:
    char **items;
    std::size_t len;
};

/**
 * @}
 */

/***************************************************************************
 * Module namspace: end
 **************************************************************************/
}
#endif /* ITEMS_HELPER_HPP_ */

