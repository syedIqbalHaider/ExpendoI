#ifndef CmdEMVmain_H
#define CmdEMVMain_H

#ifndef __cplusplus
#error "This file is for C++ only!"
#endif
/*****************************************************************************
 *
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/
/**
 * @file        cEMVmain.h
 *
 * @author      Jacek Olbrys
 *
 * @brief       EMV Main
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C++ Coding
 *              Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/
#include "cCmd.h"
#include "cmd/cmd_SetMessages.h"
#include "cmd/cmd_setPINparams.h"
#include "cmd/cmd_StartWait.h"
#include "cmd/cmd_SuperEMV.h"
#include "cmd/cmd_VerifyPIN.h"
#include "cmd/cmd_APDU.h"
#include "cmd/cmd_MonitorICC.h"
#include "cmd/cmd_InitVSS.h"
#include "cmd/cmd_ExecuteMacro.h"
#include "cmd/cmd_Unsolicited.h"
#ifdef VFI_PLATFORM_VERIXEVO
#include "cmd/cmd_ReadI2C.h"
#include "cmd/cmd_WriteI2C.h"
#include "cmd/cmd_ReadMemory.h"
#include "cmd/cmd_UpdateMemory.h"
#include "cmd/cmd_WriteMemory.h"
#endif
#ifdef VFI_PLATFORM_VOS
#include "cmd/cmd_LEDs.h"
#endif
#include "cmd/cmd_Reset.h"

/***************************************************************************
 * Using
 **************************************************************************/
using namespace com_verifone_ipcpacket;

/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
namespace com_verifone_cmd
{

/**
 * @addtogroup Cmd
 * @{ 
 */

/*************************************************************************** 
 * Preprocessor constant definitions
 **************************************************************************/

/*************************************************************************** 
 * Macro definitions
 **************************************************************************/

/*************************************************************************** 
 * Data type definitions
 **************************************************************************/

/*************************************************************************** 
 * Exported variable declarations
 **************************************************************************/

/*************************************************************************** 
 * Exported function declarations
 **************************************************************************/

/*************************************************************************** 
 * Exported class declarations
 **************************************************************************/


class CCmdEMVmain : public CCmd
{
    public:

       /**
        *   @brief      Process incomming packet.
        * 
        *   @param[in]  pack - pointer to incomming packet 
        * 
        *   @return     ESUCCESS - incomming packet handled successfull, otherwise returns proper error
        * 
        */
        c_card_error_t handleCommand(IPCPacket *pack);

    protected:

    private:
        /**
         * @brief       Converts sting all to lower case.
         *
         * @param[in]   pszString - Pointer to the sting to be processed
         *
         */
        void strtolower( char *pszString );
};

/**
 * @}
 */

/***************************************************************************
 * Module namspace: end
 **************************************************************************/
}

#endif

