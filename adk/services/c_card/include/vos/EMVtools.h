/*****************************************************************************
 *
 * Copyright (C) 2007 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/
/**
 * @file       EMVTools.h
 *
 * @author     Jacek Olbrys
 *
 * @brief      EMV Tool Prototypes
 *
 * @remarks    This file should be compliant with Verifone EMEA R&D C++ Coding
 *             Standard 1.0.x
 */
#include <string>
#include <map>

#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <unistd.h>

#include <libpml/pml.h>
#include "libpml/pml_abstracted_api.h"
#include "libpml/pml_port.h"


#include "EMV_CT_Interface.h"
#ifdef VFI_PLATFORM_VOS
#include "sys/socket.h"
#include "sys/types.h"
#include "netinet/in.h"
#include "arpa/inet.h"
#include <fcntl.h>

extern "C"
{
#include <svcmgr/svc_led.h>
}
#endif


//definition for comatibility with vos
#ifdef VFI_PLATFORM_VERIXEVO

#define LED_SELECT_MSR1	0
#define LED_SELECT_MSR2	0
#define LED_SELECT_MSR3	0
#define LED_STATE_OFF	0
#define LED_STATE_ON	0

#endif
//here we define additional tags for Mobile Servus in SA
#define TAG_DFAF01_PACKET_TYPE 				0xDFAF01
#define TAG_DFAF03_ACTIVE_INTERFACES		0xDFAF03
#define TAG_DFAF0E_CAPK_MODULUS				0xDFAF0E
#define TAG_DFAF0F_CAPK_EXPONENT			0xDFAF0F
#define TAG_DFAF10_CAPK_HASH				0xDFAF10
#define TAG_DFAF11_PARTIAL_SELECTION		0xDFAF11
#define TAG_DFAF12_SECOND_AVN				0xDFAF12
#define TAG_DFAF13_REC_APP_NAME				0xDFAF13
#define TAG_DFAF14_RND_SEL_THRESHOLD		0xDFAF14
#define TAG_DFAF15_RND_SEL_PERCENTAGE		0xDFAF15
#define TAG_DFAF16_RND_MAX_RND_PERCENTAGE	0xDFAF16
#define TAG_DFAF17_TAC_DEFAULT				0xDFAF17
#define TAG_DFAF18_TAC_DENIAL				0xDFAF18
#define TAG_DFAF19_TAC_ONLINE				0xDFAF19
#define TAG_DFAF1A_DEFAULT_TDOL				0xDFAF1A
#define TAG_DFAF1B_DEFAULT_DDOL				0xDFAF1B
#define TAG_DFAF1C_FALLBACK					0xDFAF1C
#define TAG_DFAF1D_SELECTION				0xDFAF1D
#define TAG_DFAF1E_MCC						0xDFAF1E

#define TAG_DFCC20_CAPK_FILE				0xDFCC20
#define TAG_DFCC21_EXTRA_PARAMS				0xDFCC21

#define EMV_CONFIG_FILE						"EMV.ini"
#define EMV_CONFIG_GLOBAL					"global"
#define EMV_CONFIG_MVT						"MVT.DAT"
#define EMV_CONFIG_EST						"EST.DAT"

#define EMV_CT_EXTRADATA_COUNTER 1

extern unsigned char (*EMV_local_WriteTagData)(unsigned char *data, unsigned char length, unsigned short parent, unsigned long tag);


using namespace com_verifone_pml;

typedef struct EMV_CT_EXTRADATA
{
	unsigned char   Info_Included_Data[8];     ///< Which data is included in the message
	unsigned char   T_9F17_PIN_try_counter;

} EMV_CT_EXTRADATA_TYPE;

//iqbal_audi_030217 this strut was the part of previous ADK so for updated adk we add here
typedef  struct EMV_CT_CARDDATA_STRUCT // EMV_CT_CARDDATA_TYPE
{
  unsigned char T_5A_PAN[10];                    ///< Application Primary Account Number (PAN) @n TLV Tag #TAG_5A_APP_PAN
  unsigned char T_5F24_AppExpDate[3];            ///< Application Expiration Date @n TLV tag #TAG_5F24_APP_EXP_DATE
  unsigned char T_5F34_PANSequenceNo[1];         ///< PAN sequence number @n TLV tag #TAG_5F34_PAN_SEQUENCE_NB
  EMV_CT_TRACK2_TYPE T_57_DataTrack2;                  ///< Track 2 Equivalent Data @n TLV tag #TAG_57_TRACK2_EQUIVALENT
  unsigned char T_82_AIP[2];                     ///< Application Interchange Profile @n TLV tag #TAG_82_AIP
  unsigned char T_84_DFName[1 + 16];             ///< Dedicated File (DF) Name @n TLV tag #TAG_84_DF_NAME
  unsigned char T_5F2A_CurrencyTrans[2];         ///< Transaction Currency Code (ISO 4217) @n TLV tag #TAG_5F2A_TRANS_CURRENCY
  unsigned char T_9F1A_TermCountryCode[2];              ///< Terminal Country Code @n TLV tag #TAG_9F1A_TRM_COUNTRY_CODE
  unsigned char T_9F33_TermCap[3];               ///< Terminal Capabilities (as configured by @c TermCap in ::EMV_CT_TERMDATA_TYPE, but some bits may be deactivated due to configuration of EMV_CT_APPLIDATA_STRUCT::App_TermCap) @n TLV tag #TAG_9F33_TRM_CAPABILITIES
  unsigned char T_9F35_TermTyp;                  ///< Terminal Type, see @ref TERM_TYPES ///< see @ref TERM_TYPES @n TLV tag #TAG_9F35_TRM_TYPE
  unsigned char T_9F1E_IFDSerialNumber[8];       ///< Interface Device (IFD) serial number @n TLV tag #T_9F1E_IFDSerialNumber
  unsigned char T_9F09_VerNum[2];                ///< Application Version Number @n TLV tag #TAG_9F09_TRM_APP_VERSION_NB
  unsigned char Additional_Tags[EMV_ADK_ADD_TAG_SIZE];   ///< Additional special tag data, which was requested with EMV_CT_PAYMENT_TYPE::Additional_Result_Tags @n TLV tag #TAG_DF29_ADD_TAGS
  unsigned char T_9C_TransType;                  ///< Transaction Type, see @ref TRANS_TYPES @n TLV tag #TAG_9C_TRANS_TYPE
  unsigned char T_5F25_AppEffDate[3];            ///< Application Effective Date @n TLV tag #TAG_5F25_APP_EFF_DATE
  unsigned char T_5F28_IssCountryCode[2];        ///< Issuer Country Code @n TLV tag #TAG_5F28_ISS_COUNTRY_CODE
  unsigned char T_9F42_AppCurrencyCode[2];       ///< Application Currency Code @n TLV tag #TAG_9F42_APP_CURRENCY_CODE
  unsigned char T_9F40_AddTermCap[5];            ///< Additional Terminal Capabilities (as configured by @c TermAddCap in ::EMV_CT_TERMDATA_TYPE, but some bits may be deactivated due to configuration of EMV_CT_APPLIDATA_STRUCT::App_TermAddCap) @n TLV tag #TAG_9F40_ADD_TRM_CAP
  EMV_CT_CRDNAME_TYPE T_5F20_Cardholder;            ///< Cardholder Name @n TVL tag #TAG_5F20_CARDHOLDER_NAME
  unsigned char T_DF62_ErrorData[15];            ///< Additional error data (German requirement),  optional: filled if transaction is not sucessful, see @ref DEF_DF62
  unsigned char T_5F2D_Lang_Pref[8+1];           ///< Language Preference (zero terminated) @n TLV tag #TAG_5F2D_LANGUAGE
  unsigned char T_9F08_ICC_Appli_Vers_No[2];     ///< ICC Application Version Number @n TLV tag #TAG_9F08_ICC_APP_VERSION_NB
  unsigned char T_5F36_Trx_Currency_Exp;         ///< Transaction Currency Exponent @n TLV tag #TAG_5F36_TRANS_CURRENCY_EXP
  unsigned char T_5F30_ServiceCode[2];           ///< Service code as defined in ISO/IEC 7813 for track 1 and track 2, format: n3 @n TLV tag #TAG_5F30_SERVICE_CODE
  unsigned short StatusInfo;                     ///< Common status info (see @ref STATUS_INFO) @n TLV tag #TAG_DF42_STATUS

  unsigned char T_DF61_Info_Received_Data[8];  ///< Information which of these data was provided by the ICC, see @ref DEF_DF61_TRANSRES
}  EMV_CT_CARDDATA_TYPE;


typedef struct EMV_CANDIDATE_LIST
{
	unsigned char   AID[9];
	unsigned char   label[20];
	unsigned char	priority;

} EMV_CANDIDATE_LIST_TYPE;



#define ADK_LOG_FILE "/home/usr1/logs/emv_log"
#define ADK_LOG_IP	"10.10.10.10"

long long getTimeStampMs(void);

int eventsFlatten(int events_handler, eventset_t &evts);
int open_ICC(void);
void close_ICC(void);

void SignalInit(void );


class logger
{
	public:
		int setupLog(int type);
		void Log( const char * format, ... );
		void Log_loadavg(const char *msg);
		void LogHex(unsigned char *hex_data, int data_len);
		void LogAps();
		inline void setFilename(const char *filename) {log_file.assign(filename);}
		inline void setIPaddress(const char *IP_addr) {address.assign(IP_addr);}
		std::string address;
		std::string log_file;
		int port;
		int socket_h;
		int log_type;
		long long time_start;
		long long time_end;
};



#if 0
class cPINdisplay
{
	public:

		cPINdisplay();
		virtual ~cPINdisplay()
			{ }
		inline void SetEntryXY(long x_val, long y_val) {x=x_val; y=y_val;}
		inline long GetEntryX() { return x; }
		inline long GetEntryY() { return y; }
		inline void SetMainMsg(std::string msg) {main_msg = msg;}
		inline void SetExtraMsg(std::string msg) {extra_msg = msg;}
		inline void SetDisplayAmount(int disp_line) {amount_line = disp_line;}
		virtual int Display() = 0;
		virtual bool ComputeEntryXY() = 0;

		std::string BuildAmtMsg();
		std::string BuildExtraMsg();

	protected:
		std::string main_msg;
		std::string extra_msg;
		int amount_line;
		long x;
		long y;
};

class cPINdisplayDefault: public cPINdisplay
{
	public:
		cPINdisplayDefault(): cPINdisplay()
			{}
		virtual ~cPINdisplayDefault()
			{ }
		virtual int Display();
		virtual bool ComputeEntryXY();
};

class cPINdisplayGuiApp: public cPINdisplay
{
	public:
		cPINdisplayGuiApp();
		virtual ~cPINdisplayGuiApp();
		virtual int Display();
		virtual bool ComputeEntryXY();
	private:
		bool guiConnected;
		bool consoleClaimed;
		bool pinExecuted;
};
#endif




class cEMVcollection
{
	public:
		cEMVcollection();
		int GetTLVData(int tagNum, unsigned char *value, int *Len);
		int SetTLVData(int tagNum, unsigned char *value, int Len);

		inline void AssignScript71(unsigned char *data, int size)
		{
			hostData.ScriptCritData = data; hostData.LenScriptCrit = size; hostData.Info_Included_Data[0] |= INPUT_ONL_SCRIPTCRIT;
		};
		inline void AssignScript72(unsigned char *data, int size)
		{
			hostData.ScriptUnCritData = data; hostData.LenScriptUnCrit = size; hostData.Info_Included_Data[0] |= INPUT_ONL_SCRIPTUNCRIT;
		}

		EMV_CT_TRANSRES_TYPE	transData;
		EMV_CT_CARDDATA_TYPE	cardData;
		EMV_CT_EXTRADATA_TYPE	extraData;
		EMV_CT_APPLIDATA_TYPE	appData;
		EMV_CT_PAYMENT_TYPE		payData;
		EMV_CT_HOST_TYPE		hostData;
		EMV_CT_SELECTRES_TYPE	selectData;
		EMV_CT_SELECT_TYPE		inData;
		EMV_CT_TERMDATA_TYPE	termData;
		EMV_CT_CAPKEY_TYPE	capkData;

		unsigned char t_91_authData[18];
		unsigned char t_Info_Included_Data[8];

		// clear object
		void Clear();
		void ClearEverything();
		// cache
		void ClearPool()
			{ tags_collection.clear(); }

		
		inline long getTrxCntr() { return trx_cnt++;}

		
		std::map<std::string, std::string> HTML_labels;

		
		std::map<std::string, std::string> AID_priority;

	private:
		int GetTag(int tag, unsigned char * buffer, int * size);
		int PutTag(int tag, const unsigned char * buffer, int size);

		int tagToBuffer(unsigned short tag, const unsigned char * value, size_t Len, unsigned char * tagVal, size_t tagValSize);
		unsigned int AppliDataAddLen;

		typedef std::map<int, std::string> TAGS_COLLXN;
		TAGS_COLLXN tags_collection;
		
		
		int trx_cnt;
};

class cUpdateCfg
{
	public:
		cUpdateCfg() ;
		int updateConfig(int tagNum, unsigned char * value, int Len);
		int commitChanges(void);
		int locateConfigRecords(const char *AID);
		int updateCurrentAID(const char *AID, char *ver1, char *ver2);
		int updateCAPK(short index, char *filename, int position);
		std::string getConfig(const char *AID, short capkIndex);
		int deleteConfig(const char *AID);
		int buildCAPK(void);
		int deleteCAPK(const char *AID, short capkIndex);
		inline void setCAPKmodulus(char *modulus, int mod_size) {CAPK_modulus.assign(modulus, mod_size);}
		inline void setCAPKexponent(char *exponent, int exp_size) {CAPK_exponent.assign(exponent, exp_size);}

	private:
		com_verifone_TLVLite::ConstData serializeTag(const unsigned long tag);
		int serializeAIDData(std::string & conf, const EMV_CT_APPLIDATA_TYPE * pAIDData);
		int serializeCAPKData(std::string & conf, const EMV_CT_CAPREAD_TYPE * pCAPKData);
		int deleteAID(const char * AID);

#ifdef EMV_MODULE
		EST_REC EST_current;
		MVT_REC MVT_current;
		int EST_offset;
		int MVT_offset;
#endif

		int AID_index;
		short CAPK_index;
		std::string CAPK_modulus;
		std::string CAPK_exponent;
		std::string CAPK_file;

};

class cLEDsHandler
{
    public:
        cLEDsHandler(): allowed(true)
            {}
        int show(int leds, bool force = false)
            { return changeState(leds, LED_ENABLE, force); }
        int hide(int leds, bool force = false)
            { return changeState(leds, LED_DISABLE, force); }
        int changeState(int leds, unsigned char operation, bool force = false);
        int blink(int leds, unsigned int duration, unsigned int timeout, bool force = false);
        void set_lock(bool oper)
            { allowed = oper; }
    private:
        bool allowed;
};

std::string ADK_err(EMV_ADK_INFO err);


int get_display_resolution(int &x, int &y);
unsigned short getUsrPin(unsigned char *pstPin);
void usEMVDisplayErrorPrompt(unsigned short errorID);
unsigned short usEMVPerformSignature(void);
unsigned short usEMVPerformOnlinePIN(void);
void InitMinimumTLVset(void);
void SetDefaultFunctionPointers(void);
int GetAIDPair(void);
unsigned short GetPINStatusDetail();
bool setPINParams();



void printConfig(char *configFilename);
void genConfig(char *configFilename);
int importConfig(char *configFilename);
int parseInTLV(unsigned char *buffer, int buflen, bool permanent);
int parseOutTLV(unsigned char *bufferIn, int buflenIn, unsigned char *bufferOut, int buflenOut);
int inGetPTermID(char *ptid);
int get_display_resolution(int &x, int &y);

void displaySecurePINPrompt();
void restoreGuiAppConsole();
bool isCardRemoved(void);
bool isCardInserted(void);


bool isTagBlacklisted(int tagNum);

int isSeparateGPO();

int getScreenMaxX();
int getScreenMaxY();
int get_console_handle();
int DisplayPromptSelectSection(int prompt_number, const char * const section, int at_x, int at_y, bool clear_screen = false);
int DisplayPromptSelectSection(int prompt_number, const char * const section, bool clear_screen = true);
int DisplayText(const char * const text, int text_len, int at_x, int at_y, bool clear_screen = false);
int DisplayText(const char * const text, int text_len, bool clear_screen = true);

int RemoveAIDduplicates(void);
int CandListModify(EMV_CT_CANDIDATE_TYPE T_BF04_Candidates[5]);
int SendMonitorMsg(uint8_t respCode);
bool isPINinCVM(unsigned char *cvm, int cvm_size);
bool openEMVlib();

void ICCoutEvent(void);
int SendMonitorMsg(uint8_t respCode);


