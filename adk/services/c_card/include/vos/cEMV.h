#ifndef C_EMV_H 
#define C_EMV_H
 
#ifndef __cplusplus 
#  error "This file is for C++ only!"
#endif 

/*****************************************************************************
 *
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/
/**
 * @file        cEMV.h
 *
 * @author      Jacek Olbrys
 *
 * @brief       EMV wrapper class
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C++ Coding
 *              Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <string>


#include <liblog/logsys.h>

#include "EMV_Common_Interface.h"
//#include "EMV_CT_Interface.h"
//#include "E2E_EMV_CT_Serialize.h"


/***************************************************************************
 *    Definitions
 **************************************************************************/
#define CARD_APP_MAX_TLV        512
#define CARD_APP_MAX_APDU       270

#define CARD_APP_STATUS_M_CONTINUE  0x0000
#define CARD_APP_STATUS_M_STOP      0x0001
#define CARD_APP_STATUS_M_OK        0x0000
#define CARD_APP_STATUS_M_WARNING   0x0020
#define CARD_APP_STATUS_M_ERROR     0x0010
#define CARD_APP_STATUS_M_PIN       0x0002
#define CARD_APP_STATUS_M_SIGNATURE 0x0004

#define CARD_APP_STATUS_D_OK            0x0000
#define CARD_APP_STATUS_D_EMPTY         0x0001
#define CARD_APP_STATUS_D_CHIP_ERROR    0x0002
#define CARD_APP_STATUS_D_REMOVED       0x0004
#define CARD_APP_STATUS_D_APPL_BLOCKED      0x0008
#define CARD_APP_STATUS_D_PARAMETER     0x0010
#define CARD_APP_STATUS_D_CANCELLED     0x0020
#define CARD_APP_STATUS_D_GENERAL       0x4000
#define CARD_APP_STATUS_D_BYPASS        0x0040
#define CARD_APP_STATUS_D_TIMEOUT       0x0080
#define CARD_APP_STATUS_D_CARD_BLOCKED      0x0100
#define CARD_APP_STATUS_D_CONF_EMPTY        0x0200
#define CARD_APP_STATUS_D_NOT_ACCEPT		0x0400

#define CARD_APP_STATUS_D_FALLBACK      0x8000
#define CARD_APP_STATUS_D_NO_FALLBACK   0x0000

#define CARD_APP_RET_OK             0x00
#define CARD_APP_RET_NO_UI          0x01
#define CARD_APP_RET_REMOVED        0x02
#define CARD_APP_RET_ORDER          0x03
#define CARD_APP_RET_DECLINED       0x04
#define CARD_APP_RET_APPROVED       0x05
#define CARD_APP_RET_ONLINE         0x06
#define CARD_APP_RET_ICC_FAIL       0x07
#define CARD_APP_RET_TIMEOUT        0x08
#define CARD_APP_RET_ICC_OK         0x09
#define CARD_APP_RET_MAG_OK         0x0A
#define CARD_APP_RET_MSG_RECEIVED   0x0B
#define CARD_APP_RET_CANCELED       0x0C
#define CARD_APP_RET_CARD_BLOCKED       0x0D
#define CARD_APP_RET_APP_BLOCKED        0x0E
#define CARD_APP_RET_PARAM      0x0F
#define CARD_APP_RET_NO_APPS        0x10
#define CARD_APP_RET_SELECT_AGAIN	0x11
#define CARD_APP_RET_FAIL           0xFF

#define CARD_APP_EVT_NONE			0x00
#define CARD_APP_EVT_CARD_IN		0x01
#define CARD_APP_EVT_CARD_OUT		0x02
#define CARD_APP_EVT_PIPE			0x04
#define CARD_APP_EVT_MSR			0x08
#define CARD_APP_EVT_MSR_READY		0x10

#define CARD_APP_OPER_TLV       0x0000
#define CARD_APP_OPER_SELECT    0x0001
#define CARD_APP_OPER_GPO       0x0002
#define CARD_APP_OPER_READ      0x0004
#define CARD_APP_OPER_VERIFY    0x0008
#define CARD_APP_OPER_RESTRICT  0x0010
#define CARD_APP_OPER_TRM       0x0020
#define CARD_APP_OPER_DATA_AUTH 0x0040
#define CARD_APP_OPER_FIRST_AC  0x0080
#define CARD_APP_OPER_EXT_AUTH  0x0100
#define CARD_APP_OPER_SCRIPT    0x0200
#define CARD_APP_OPER_SECOND_AC 0x0400
#define CARD_APP_OPER_PIN_VERIFY 0x0800
#define CARD_APP_OPER_CONFIG	0x8000

#define CARD_APP_DEF_MAX_SCRIPTS 5
#define CARD_APP_DEF_ONLINE_PIN     0x80

#define PIN_ENTRY_STATUS_OK                     0
#define PIN_ENTRY_STATUS_BYPASSED_POS           1
#define PIN_ENTRY_STATUS_BYPASSED_CARDHOLDER    2
#define PIN_ENTRY_STATUS_CARD_REMOVED           3
#define PIN_ENTRY_STATUS_CANCELLED_POS          4
#define PIN_ENTRY_STATUS_CANCELLED_CARDHOLDER   5
#define PIN_ENTRY_STATUS_TIMEOUT                6
#define PIN_ENTRY_STATUS_BAD_PIN                7
#define PIN_ENTRY_STATUS_LAST_TRY               8
#define PIN_ENTRY_STATUS_BLOCKED                9
#define PIN_ENTRY_STATUS_APDU_ERROR             10
#define PIN_ENTRY_STATUS_ERROR                  20

#define PIN_ENTRY_STATUS_NOT_SET                256

//compatiblity settings
#define FORCED_ONLINE           114
#define FORCED_DECLINE          115
#define HOST_AUTHORISED                 1
#define HOST_DECLINED                   2
#define FAILED_TO_CONNECT               3
//class cPINdisplay;
#define GEN_TC		0x40
#define GEN_AAC		0x00
#define GEN_ARQC	0x80

#define ICC_UNPREDICTABLE_SIZE                  8
#define CARDAPP_LIST_SEPARATOR	";"

//definitions for compatibility with Verix EMV Module
#define     EMV_FAILURE                     (unsigned short)0x9999 
#define     EMV_SUCCESS                     (unsigned short)0x9000

//definitions of  specific tags
//TBC
#define TAG_9F7B_ABSA 0x9F7B



/*************************************************************************** 
 * Using 
 **************************************************************************/ 

/*************************************************************************** 
 * Module namespace: begin 
 **************************************************************************/ 
namespace com_verifone_emv
{

/**
 * @addtogroup Card
 * @{ 
 */

/***************************************************************************
 *    external C++ - API's
 **************************************************************************/
class CEMV
{
public:
   CEMV(void);

    int SelectApplication(void);

    int GetProcessingOptions(void);

    int ReadCardData(void);


    int OfflineSteps(EMV_ADK_INFO break_ret);

    int CardholderVerification(void);

    int DataAuthentication(void);

    int ProcessRestrictions(void);

    int TerminalRiskManagement(void);

    int FirstAC(void);

    int ExternalAuthenticate(void);

    int ProcessScript(void);

    int SecondAC(void);

    int AddScript(char *script);

    int UpdateTLV(unsigned char *list, int list_len);

    int ReadTLV(unsigned char *list, int list_len, unsigned char *out_list, int out_list_len);

    int PostOnline(void);

    int OfflinePINVerification();

    inline uint16_t GetStatusMain(void) { return status_main; }

    inline uint16_t GetStatusDetail(void) { return status_detailed; }

    inline void SetStatusMain(uint16_t status) {status_main |= status; dlog_msg("SetStatusMain: %X", status); }

    inline void SetStatusDetail(uint16_t status) {status_detailed |= status; dlog_msg("SetStatusDetail: %X", status);}

    inline uint8_t GetPINEntryStatus(void) { return pin_entry_status; }

    inline void SetPINEntryStatus(uint8_t status) {pin_entry_status = status; dlog_msg("PIN Entry Status: %X", status);}

    inline void ClearStatus(void) {status_main = 0; status_detailed = 0; operation_reg = 0; term_decision = 0; pinBypassStatus = 0; }

    inline int GetScriptResult71(unsigned char *scr_result) { memcpy(scr_result, script_result71, script_size71); return script_size71;}

    inline int GetScriptResult72(unsigned char *scr_result) { memcpy(scr_result, script_result72, script_size72); return script_size72;}

    inline void SetPINparams(unsigned char type, unsigned char entry_type) {PIN_type = type; PIN_entry_type = entry_type;}

    inline unsigned char GetPINentrytype() { return PIN_entry_type; }

    inline int GetPINStatus(void) {return PIN_status;}

    inline void SetPINStatus(int status) {PIN_status = status;}

    inline std::string GetPINhtml(void) {return PIN_html;}

    inline void SetPINhtml(std::string html) {PIN_html = html;}

    int setResponse(int EMVModuleRet, int operation);

    int checkStatus(int operation);

    inline void SetTermDecision(int trm_dec) {term_decision = trm_dec;}

    inline void SetPINBypassStatus(unsigned short status) { pinBypassStatus = status; }
    inline unsigned short GetPINBypassStatus() { return pinBypassStatus; }

    unsigned long ICC_reader;

    void setCardBlackListed(int _blacklisted = 0) {is_card_blacklisted = (_blacklisted == 1);}
    bool isCardBlackListed()
    {
        return is_card_blacklisted;
    }
    // Vx820 - enable ICC monitoring
    bool startICCMonitoring();
    void setRestoreConsole() { guiHasConsole = true; }
    void clearRestoreConsole() { guiHasConsole = false; }
    bool getRestoreConsole() { return guiHasConsole; }
    // bool restoreConsole();
    bool runBeeperThread();
    bool stopBeeperThread();

    void SetReasonOnline(unsigned char ro) { reasonOnline = ro; }
    unsigned char GetReasonOnline() { return reasonOnline; }

    int SetParam(long Tag, uint8_t *Val, int Len );

    int GetParam(long Tag, uint8_t *Val, int *Len);

   
    //inline cPINdisplay * getPINDisplayObj() { return pPinDisplay; }

    void configureCallbacks(bool guiAppActive);

    // GVR
    void setExternalPIN(const char * ext_pin, size_t ext_pin_size) { external_pin.assign(ext_pin, ext_pin_size); }
    bool isExternalPIN() { return external_pin.size() > 0; }
    void setExternalPINMode(int pmode) { external_pin_mode = pmode; }

private:
    uint16_t status_main;
    uint16_t status_detailed;
    uint8_t pin_entry_status;
    bool found_matching_apps;
    int operation_reg;
    int term_decision;
    unsigned char script_result71[5*CARD_APP_DEF_MAX_SCRIPTS];
    unsigned char script_result72[5*CARD_APP_DEF_MAX_SCRIPTS];
    int script_size71;
    int script_size72;
    bool is_card_blacklisted;
    unsigned char PIN_type;
    unsigned char PIN_entry_type;
    bool guiHasConsole;
    int beeperThreadID;
    unsigned char reasonOnline;
    unsigned short pinBypassStatus;
    int PIN_status;
    int PIN_try;
    //cPINdisplay * pPinDisplay;
    std::string PIN_html;
    std::string external_pin;
    int external_pin_mode;

    int GetChallenge(unsigned char * icc_unpredictable); // buffer has to be 8 characters long!
protected:
};

/** 
 * @} 
 */ 
 
} 
/***************************************************************************  
 * Module namespace: end   
**************************************************************************/ 
 
#endif /* C_EMV_H */ 

