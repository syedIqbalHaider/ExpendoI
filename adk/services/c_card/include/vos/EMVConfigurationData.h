/****************************************************************************
*  Product:     InFusion Reference Application
*  Company:     VeriFone
*  Author:      R&D Latvia
****************************************************************************/

#ifndef _XDEMOEMVCONFIGURATIONDATA_H_
#define _XDEMOEMVCONFIGURATIONDATA_H_

#include "EMV_CT_Interface.h"


/****************************************************************************
*  CONTACT CONFIGURATION
****************************************************************************/

  EMV_CT_TERMDATA_TYPE	xTermDataTermEmpty =
 {
 0x25,																// terminal type
 {0x00,0x00},														// country code (France)
 {0xE0,0xD0,0xC8},													// terminal capabilities
 {0xF0,0x00,0xB0,0xA0,0x01},										// additional terminal capabilities
 {0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38},							//Terminal Identification, validity bit #EMV_CT_INPUT_TRM_ID @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F1C_TRM_ID, @n TermIdent in EMV_CT_TERMDATA_STRUCT::TermIdent, @c XML Tag: #XML_TAG_TERMDATA_TERM_IDENT
 {0x09,0x78},														// currency (EUR)
 0x02,																// currency exponent
 {EMV_ADK_LANG_ENGLISH,0x00,0x00,0x00,0x00,0x00,0x00,0x00},	// languages
 {0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38}, 						// IFD Serial Number
 {0x00},															// Kernel Version
 {0x00},															// Framework Version
 {0xFF, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},					// Info_Included_Data
 };

 EMV_CT_TERMDATA_TYPE  GeneralTermDataDefault =
{
0x22,                                                      // terminal type
{0x02,0x50},                                               // country code (France)
{0xE0,0xF8,0xC8},                                          // terminal capabilities
{0xF0,0x00,0xB0,0xA0,0x01},                                // additional terminal capabilities
{0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38},							//Terminal Identification, validity bit #EMV_CT_INPUT_TRM_ID @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F1C_TRM_ID, @n TermIdent in EMV_CT_TERMDATA_STRUCT::TermIdent, @c XML Tag: #XML_TAG_TERMDATA_TERM_IDENT
{0x09,0x78},                                               // currency (EUR)
0x02,                                                      // currency exponent
{EMV_ADK_LANG_ENGLISH,0x00,0x00,0x00,0x00,0x00,0x00,0x00},     // languages
{0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38},                 // IFD Serial Number
{0x00},                                                    // Kernel Version
{0x00},                                                    // Framework Version
{0xFF, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},          // Info_Included_Data
};


/* example offline with online capability */
 EMV_CT_TERMDATA_TYPE  xTermDataTermCombinedAttended =
{
0x22,                                                              // terminal type
{0x02,0x50},                                                       // country code (France)
{0xE0,0xF8,0xC8},                                                  // terminal capabilities
{0xF0,0x00,0xB0,0xA0,0x01},                                        // additional terminal capabilities
{0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38},							//Terminal Identification, validity bit #EMV_CT_INPUT_TRM_ID @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F1C_TRM_ID, @n TermIdent in EMV_CT_TERMDATA_STRUCT::TermIdent, @c XML Tag: #XML_TAG_TERMDATA_TERM_IDENT
{0x09,0x78},                                                       // currency (EUR)
0x02,                                                              // currency exponent
{EMV_ADK_LANG_ENGLISH,EMV_ADK_LANG_GERMAN,0x00,0x00,0x00,0x00,0x00,0x00},  // languages
{0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38},                         // IFD Serial Number
{0x00},                                                            // Kernel Version
{0x00},                                                            // Framework Version
{0xFF, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},                  // Info_Included_Data
};

 EMV_CT_TERMDATA_TYPE  xTermDataTermCombinedUnAttended =
{
0x25,                                                              // terminal type
{0x02,0x50},                                                       // country code (France)
{0xE0,0xD0,0xC8},                                                  // terminal capabilities
{0xF0,0x00,0xB0,0xA0,0x01},                                        // additional terminal capabilities
{0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38},							//Terminal Identification, validity bit #EMV_CT_INPUT_TRM_ID @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_9F1C_TRM_ID, @n TermIdent in EMV_CT_TERMDATA_STRUCT::TermIdent, @c XML Tag: #XML_TAG_TERMDATA_TERM_IDENT
{0x09,0x78},                                                       // currency (EUR)
0x02,                                                              // currency exponent
{EMV_ADK_LANG_ENGLISH,EMV_ADK_LANG_GERMAN,0x00,0x00,0x00,0x00,0x00,0x00},  // languages
{0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38},                         // IFD Serial Number
{0x00},                                                            // Kernel Version
{0x00},                                                            // Framework Version
{0xFF, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},                  // Info_Included_Data
};


 EMV_CT_APPLI_TYPE  xAID_MC =
{
7,
{0xA0,0x00,0x00,0x00,0x04,0x10,0x10}
};

 EMV_CT_APPLIDATA_TYPE  xAppliData_MC =
{
{0x00,0x02},                                                                        // VerNum[2];
{0x4D,0x61,0x73,0x74,0x65,0x72,0x63,0x61,0x72,0x64,0x00},                           // AppName[16+1];
0x01,                                                                               // ASI;
{0x59,0x99},                                                                        // BrKey[2];
{0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38},                                          // TermIdent[8];
{0x00,0x00,0x13,0x88},                                                              // FloorLimit[4];
{0x00,0x00,0x01,0xF4},                                                              // Threshhold[4];
0x00,                                                                               // TargetPercentage;
0x00,                                                                               // MaxTargetPercentage;
{0x04,0x00,0x00,0x00,0x00},                                                         // TACDenial[5];
{0xF8,0x50,0xAC,0xF8,0x00},                                                         // TACOnline[5];
{0xFC,0x50,0xAC,0xA0,0x00},                                                         // TACDefault[5];
0x01,                                                                               // EMV_Application;
{0x0F,{0x9F,0x02,0x06,0x5F,0x2A,0x02,0x9A,0x03,0x9C,0x01,0x95,0x05,0x9F,0x37,0x04}}, // Default_TDOL[1+MAX_LG_TDOL];
{0x03, {0x9F,0x37,0x04}},                                                           // Default_DDOL[1+MAX_LG_DDOL];
{0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20},       // MerchIdent[15];
CDA_EMV_MODE_3,                                                                     // CDA mode
EMV_CT_PROCESS_TACIAC_DEFAULT_BEFORE,                                                      // offline only behaviour
0x00,                                                                               // AIP_CVM_not_supported;
0x05,                                                                               // POS entry mode
{0x00},																			///< up to 10 additional version numbers, optional if needed for compliancy (Verix up to 10, Velocity up to 2), validity bit #EMV_CT_INPUT_APL_ADD_VERSIONS  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF5F_ADD_APP_VERSION, @n Additional_Versions_No in EMV_CT_APPLIDATA_STRUCT::Additional_Versions_No, @n XML Tag: #XML_TAG_APPLIDATA_ADD_VER_NUM
{0x00,0x00,0x00,0x00},														 ///< General: Binary coded @n Below this limit simplified termcaps may take place, e.g. CVM processing is skipped/handled as per parameter Availability bit: #EMV_CT_INPUT_APL_SEC_LIMIT  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF49_APL_SEC_LIMIT, @n Security_Limit in EMV_CT_APPLIDATA_STRUCT::Security_Limit, @n XML Tag: #XML_TAG_APPLIDATA_SECURE_LIMIT
{0x01,0x1F,0x15,0x00,0x00},                                                         // App_FlowCap[5]

{0x9F,0x53,0x01,0x52},                                                              // Additional_Tags[ADD_TAG_SIZE];
{ 3, {0x9F,0x7B,0xFF}},
{ 1, {0x8E,0,0,0,0,0,0,0,0,0}},                                                     // Taglist;

{0xE0,0xF8,0xC8},                                                                   // terminal capabilities
{0x00,0x00,0x00},															///< Below security limit terminal capabilities (e.g. cardholder verification methods) for CT chip transactions below the CVM / Security limit. Availability bit: #EMV_CT_INPUT_APL_SEC_CAPS  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF4A_APL_SEC_CAPS, @n Capabilities_belowLimit in EMV_CT_APPLIDATA_STRUCT::Capabilities_belowLimit, @n XML Tag: #XML_TAG_APPLIDATA_NON_SECURE_CAPS
{0x02,0x50},                                                                        // country code (France)
{0xF0,0x00,0xB0,0xA0,0x01},                                                         // additional terminal capabilities
0x22,                                                                               // terminal type

{                                                                                   // xAIDPrio[MAX_PRIO_APP];
  {
    0,
    {0x00}
  },
  {
    0,
    {0x00}
  },
  {
    0,
    {0x00}
  },
  {
    0,
    {0x00}
  },
  {
    0,
    {0x00}
  }
},
{0x00,0x00,0x00},                                                                   // tucFallbackMIDs[MAX_CHP_TO_MSR]
{0x21,0x20,0x11,0x00,0x00,0x00,0x00,0x00},                                          // xuc_Special_TRX[8];
0x00,                                                                               // uc_FallBack_Handling;
0x00,									///< to enable a customer CVM for CVM for this AID, to fill according EMV specs if needed (range 1000000-101111 is allowed), validity bit #EMV_CT_INPUT_APL_CUSTOMER_CVM  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF2F_CVM_CUSTOM, @n Customer_CVM in EMV_CT_APPLIDATA_STRUCT::Customer_CVM, @n XML Tag: #XML_TAG_APPLIDATA_CUSTOMER_CVM
{0x01,0x00,0x00,0x00,0x00},                                                         // checksum params
{0x00},                                                                             // checksum value (empty)
{ 0, {0x00}},                                                                       // Master AID (empty)

{0xFF, 0xFF, 0xFF, 0xFF, 0x07, 0x00, 0x00, 0x00}                                    // Info_Included_Data
};

 EMV_CT_APPLI_TYPE  xAID_VIS =
{
7,
{0xA0,0x00,0x00,0x00,0x03,0x10,0x10}
};

 EMV_CT_APPLIDATA_TYPE  xAppliData_VIS =
{
{0x00,0x8C},                                                                        // VerNum[2];
{0x56,0x69,0x73,0x61,0x00},                                                         // AppName[16+1];
0x01,                                                                               // ASI;
{0x59,0x99},                                                                        // BrKey[2];
{0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38},                                          // TermIdent[8];
{0x00,0x00,0x13,0x88},                                                              // FloorLimit[4];
{0x00,0x00,0x01,0xF4},                                                              // Threshhold[4];
0x00,                                                                               // TargetPercentage;
0x00,                                                                               // MaxTargetPercentage;
{0x00,0x10,0x00,0x00,0x00},                                                         // TACDenial[5];
{0xD8,0x40,0x04,0xF8,0x00},                                                         // TACOnline[5];
{0xD8,0x40,0x00,0xA8,0x00},                                                         // TACDefault[5];
0x01,                                                                               // EMV_Application;
{0x00, {0x00}},                                                                             // Default_TDOL[1+MAX_LG_TDOL];
{0x03,{0x9F,0x37,0x04}},                                                            // Default_DDOL[1+MAX_LG_DDOL];
{0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20},       // MerchIdent[15];
CDA_EMV_MODE_3,                                                                     // CDA mode
EMV_CT_PROCESS_TACIAC_DEFAULT_BEFORE,                                                      // offline only behaviour
0x00,                                                                               // AIP_CVM_not_supported;
0x05,                                                                               // POS entry mode
{0x00},																			///< up to 10 additional version numbers, optional if needed for compliancy (Verix up to 10, Velocity up to 2), validity bit #EMV_CT_INPUT_APL_ADD_VERSIONS  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF5F_ADD_APP_VERSION, @n Additional_Versions_No in EMV_CT_APPLIDATA_STRUCT::Additional_Versions_No, @n XML Tag: #XML_TAG_APPLIDATA_ADD_VER_NUM
{0x00,0x00,0x00,0x00},														 ///< General: Binary coded @n Below this limit simplified termcaps may take place, e.g. CVM processing is skipped/handled as per parameter Availability bit: #EMV_CT_INPUT_APL_SEC_LIMIT  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF49_APL_SEC_LIMIT, @n Security_Limit in EMV_CT_APPLIDATA_STRUCT::Security_Limit, @n XML Tag: #XML_TAG_APPLIDATA_SECURE_LIMIT
{0x01,0x1F,0x15,0x00,0x00},                                                         // App_FlowCap[5];

{0x00},                                                                             // Additional_Tags[ADD_TAG_SIZE];
{ 3, {0x9F,0x7B,0xFF}},
{ 2, {0x57,(unsigned short)0x9F08,0,0,0,0,0,0,0,0}},                                // Taglist;

{0xE0,0xF8,0xC8},                                                                   // terminal capabilities
{0x00,0x00,0x00},															///< Below security limit terminal capabilities (e.g. cardholder verification methods) for CT chip transactions below the CVM / Security limit. Availability bit: #EMV_CT_INPUT_APL_SEC_CAPS  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF4A_APL_SEC_CAPS, @n Capabilities_belowLimit in EMV_CT_APPLIDATA_STRUCT::Capabilities_belowLimit, @n XML Tag: #XML_TAG_APPLIDATA_NON_SECURE_CAPS
{0x02,0x50},                                                                        // country code (France)
{0xF0,0x00,0xB0,0xA0,0x01},                                                         // additional terminal capabilities
0x22,                                                                               // terminal type

{                                                                                   // xAIDPrio[MAX_PRIO_APP];
  {
    0,
    {0x00}
  },
  {
    0,
    {0x00}
  },
  {
    0,
    {0x00}
  },
  {
    0,
    {0x00}
  },
  {
    0,
    {0x00}
  }
},
{0x00,0x00,0x00},                                                                   // tucFallbackMIDs[MAX_CHP_TO_MSR]
{0x21,0x20,0x11,0x00,0x00,0x00,0x00,0x00},                                          // xuc_Special_TRX[8];
0x00,                                                                               // uc_FallBack_Handling;
0x00,									///< to enable a customer CVM for CVM for this AID, to fill according EMV specs if needed (range 1000000-101111 is allowed), validity bit #EMV_CT_INPUT_APL_CUSTOMER_CVM  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF2F_CVM_CUSTOM, @n Customer_CVM in EMV_CT_APPLIDATA_STRUCT::Customer_CVM, @n XML Tag: #XML_TAG_APPLIDATA_CUSTOMER_CVM
{0x01,0x00,0x00,0x00,0x00},                                                         // checksum params
{0x00},                                                                             // checksum value (empty)
{ 0, {0x00}},                                                                       // Master AID (empty)

{0xFF, 0xFF, 0xFF, 0xFF, 0x07, 0x00, 0x00, 0x00}                                    // Info_Included_Data
};

 EMV_CT_APPLI_TYPE  xAID_MAESTRO =
{
7,
{0xA0,0x00,0x00,0x00,0x04,0x30,0x60}
};

 EMV_CT_APPLIDATA_TYPE  xAppliData_MAESTRO =
{
{0x00,0x02},                                                                        // VerNum[2];
{0x4D,0x61,0x65,0x73,0x74,0x72,0x6F,0x00},                                          // AppName[16+1];
0x01,                                                                               // ASI;
{0x59,0x99},                                                                        // BrKey[2];
{0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38},                                          // TermIdent[8];
//  {0x00,0x00,0x13,0x88},                                                              // FloorLimit = 50.00
{0x00,0x00,0x01,0xF4},                                                              // FloorLimit = 5.00 (as used in TIP tests)
{0x00,0x00,0x01,0xF4},                                                              // Threshhold[4];
0x00,                                                                               // TargetPercentage;
0x00,                                                                               // MaxTargetPercentage;
{0x04,0x00,0x00,0x00,0x00},                                                         // TACDenial[5];
{0xF8,0x50,0xAC,0xF8,0x00},                                                         // TACOnline[5];
{0xFC,0x50,0xAC,0xA0,0x00},                                                         // TACDefault[5];
0x01,                                                                               // EMV_Application;
{0x0F,{0x9F,0x02,0x06,0x5F,0x2A,0x02,0x9A,0x03,0x9C,0x01,0x95,0x05,0x9F,0x37,0x04}}, // Default_TDOL[1+MAX_LG_TDOL];
{0x03,{0x9F,0x37,0x04}},                                                            // Default_DDOL[1+MAX_LG_DDOL];
{0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20},       // MerchIdent[15];
CDA_EMV_MODE_3,                                                                     // CDA mode
EMV_CT_PROCESS_TACIAC_DEFAULT_BEFORE,                                                      // offline only behaviour
0x00,                                                                               // AIP_CVM_not_supported;
0x05,                                                                               // POS entry mode
{0x00},																			///< up to 10 additional version numbers, optional if needed for compliancy (Verix up to 10, Velocity up to 2), validity bit #EMV_CT_INPUT_APL_ADD_VERSIONS  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF5F_ADD_APP_VERSION, @n Additional_Versions_No in EMV_CT_APPLIDATA_STRUCT::Additional_Versions_No, @n XML Tag: #XML_TAG_APPLIDATA_ADD_VER_NUM
{0x00,0x00,0x00,0x00},														 ///< General: Binary coded @n Below this limit simplified termcaps may take place, e.g. CVM processing is skipped/handled as per parameter Availability bit: #EMV_CT_INPUT_APL_SEC_LIMIT  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF49_APL_SEC_LIMIT, @n Security_Limit in EMV_CT_APPLIDATA_STRUCT::Security_Limit, @n XML Tag: #XML_TAG_APPLIDATA_SECURE_LIMIT
{0x01,0x1F,0x15,0x00,0x00},                                                         // App_FlowCap[5];

{0x9F,0x53,0x01,0x52},                                                              // Additional_Tags[ADD_TAG_SIZE];
{ 3, {0x9F,0x7B,0xFF}},
{ 0, {0,0,0,0,0,0,0,0,0,0}},                                                        // Taglist;

{0xE0,0xD0,0xC8},                                                                   // terminal capabilities
{0x00,0x00,0x00},															///< Below security limit terminal capabilities (e.g. cardholder verification methods) for CT chip transactions below the CVM / Security limit. Availability bit: #EMV_CT_INPUT_APL_SEC_CAPS  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF4A_APL_SEC_CAPS, @n Capabilities_belowLimit in EMV_CT_APPLIDATA_STRUCT::Capabilities_belowLimit, @n XML Tag: #XML_TAG_APPLIDATA_NON_SECURE_CAPS
{0x02,0x50},                                                                        // country code (France)
{0xF0,0x00,0xB0,0xA0,0x01},                                                         // additional terminal capabilities
0x22,                                                                               // terminal type

{                                                                                   // xAIDPrio[MAX_PRIO_APP];
  {
    0,
    {0x00}
  },
  {
    0,
    {0x00}
  },
  {
    0,
    {0x00}
  },
  {
    0,
    {0x00}
  },
  {
    0,
    {0x00}
  }
},
{0x00,0x00,0x00},                                                                   // tucFallbackMIDs[MAX_CHP_TO_MSR]
{0x21,0x20,0x11,0x00,0x00,0x00,0x00,0x00},                                          // xuc_Special_TRX[8];
0x00,                                                                               // uc_FallBack_Handling;
0x00,									///< to enable a customer CVM for CVM for this AID, to fill according EMV specs if needed (range 1000000-101111 is allowed), validity bit #EMV_CT_INPUT_APL_CUSTOMER_CVM  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF2F_CVM_CUSTOM, @n Customer_CVM in EMV_CT_APPLIDATA_STRUCT::Customer_CVM, @n XML Tag: #XML_TAG_APPLIDATA_CUSTOMER_CVM
{0x01,0x00,0x00,0x00,0x00},                                                         // checksum params
{0x00},                                                                             // checksum value (empty)
{ 0, {0x00}},                                                                       // Master AID (empty)

{0xFF, 0xFF, 0xFF, 0xFF, 0x07, 0x00, 0x00, 0x00}                                    // Info_Included_Data
};

 EMV_CT_APPLI_TYPE  xAID_VIS_ELEC =
{
7,
{0xA0,0x00,0x00,0x00,0x03,0x20,0x10}
};

 EMV_CT_APPLIDATA_TYPE  xAppliData_VIS_ELEC =
{
{0x00,0x8C},                                                                        // VerNum[2];
{0x56,0x69,0x73,0x61,0x20,0x45,0x6C,0x65,0x63,0x74,0x72,0x6F,0x6E,0x00},            // AppName[16+1];
0x01,                                                                               // ASI;
{0x59,0x99},                                                                        // BrKey[2];
{0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38},                                          // TermIdent[8];
{0x00,0x00,0x13,0x88},                                                              // FloorLimit[4];
{0x00,0x00,0x01,0xF4},                                                              // Threshhold[4];
0x00,                                                                               // TargetPercentage;
0x00,                                                                               // MaxTargetPercentage;
{0x00,0x10,0x00,0x00,0x00},                                                         // TACDenial[5];
{0xD8,0x40,0x04,0xF8,0x00},                                                         // TACOnline[5];
{0xD8,0x40,0x00,0xA8,0x00},                                                         // TACDefault[5];
0x01,                                                                               // EMV_Application;
{0x00, {0x00}},                                                                     // Default_TDOL[1+MAX_LG_TDOL];
{0x03,{0x9F,0x37,0x04}},                                                            // Default_DDOL[1+MAX_LG_DDOL];
{0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20},       // MerchIdent[15];
CDA_EMV_MODE_3,                                                                     // CDA mode
EMV_CT_PROCESS_TACIAC_DEFAULT_BEFORE,                                                      // offline only behaviour
0x00,                                                                               // AIP_CVM_not_supported;
0x05,                                                                               // POS entry mode
{0x00},																			///< up to 10 additional version numbers, optional if needed for compliancy (Verix up to 10, Velocity up to 2), validity bit #EMV_CT_INPUT_APL_ADD_VERSIONS  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF5F_ADD_APP_VERSION, @n Additional_Versions_No in EMV_CT_APPLIDATA_STRUCT::Additional_Versions_No, @n XML Tag: #XML_TAG_APPLIDATA_ADD_VER_NUM
{0x00,0x00,0x00,0x00},														 ///< General: Binary coded @n Below this limit simplified termcaps may take place, e.g. CVM processing is skipped/handled as per parameter Availability bit: #EMV_CT_INPUT_APL_SEC_LIMIT  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF49_APL_SEC_LIMIT, @n Security_Limit in EMV_CT_APPLIDATA_STRUCT::Security_Limit, @n XML Tag: #XML_TAG_APPLIDATA_SECURE_LIMIT
{0x01,0x1F,0x15,0x00,0x00},                                                         // App_FlowCap[5];

{0x00},                                                                             // Additional_Tags[ADD_TAG_SIZE];
{ 3, {0x9F,0x7B,0xFF}},
{ 2, {0x57,(unsigned short)0x9F08,0,0,0,0,0,0,0,0}},                                // Taglist;

{0xE0,0xD0,0xC8},                                                                   // terminal capabilities
{0x00,0x00,0x00},															///< Below security limit terminal capabilities (e.g. cardholder verification methods) for CT chip transactions below the CVM / Security limit. Availability bit: #EMV_CT_INPUT_APL_SEC_CAPS  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF4A_APL_SEC_CAPS, @n Capabilities_belowLimit in EMV_CT_APPLIDATA_STRUCT::Capabilities_belowLimit, @n XML Tag: #XML_TAG_APPLIDATA_NON_SECURE_CAPS
{0x02,0x50},                                                                        // country code (France)
{0xF0,0x00,0xB0,0xA0,0x01},                                                         // additional terminal capabilities
0x22,                                                                               // terminal type

{                                                                                   // xAIDPrio[MAX_PRIO_APP];
  {
    0,
    {0x00}
  },
  {
    0,
    {0x00}
  },
  {
    0,
    {0x00}
  },
  {
    0,
    {0x00}
  },
  {
    0,
    {0x00}
  }
},
{0x00,0x00,0x00},                                                                   // tucFallbackMIDs[MAX_CHP_TO_MSR]
{0x21,0x20,0x11,0x00,0x00,0x00,0x00,0x00},                                          // xuc_Special_TRX[8];
0x00,                                                                               // uc_FallBack_Handling;
0x00,									///< to enable a customer CVM for CVM for this AID, to fill according EMV specs if needed (range 1000000-101111 is allowed), validity bit #EMV_CT_INPUT_APL_CUSTOMER_CVM  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF2F_CVM_CUSTOM, @n Customer_CVM in EMV_CT_APPLIDATA_STRUCT::Customer_CVM, @n XML Tag: #XML_TAG_APPLIDATA_CUSTOMER_CVM
{0x01,0x00,0x00,0x00,0x00},                                                         // checksum params
{0x00},                                                                             // checksum value (empty)
{ 0, {0x00}},                                                                       // Master AID (empty)

{0xFF, 0xFF, 0xFF, 0xFF, 0x07, 0x00, 0x00, 0x00}                                    // Info_Included_Data
};

 EMV_CT_APPLI_TYPE  xAID_VIS_VPAY =
{
7,
{0xA0,0x00,0x00,0x00,0x03,0x20,0x20}
};

 EMV_CT_APPLIDATA_TYPE  xAppliData_VIS_VPAY =
{
{0x00,0x8C},                                                                        // VerNum[2];
{0x56,0x20,0x50,0x41,0x59,0x00},                                                    // AppName[16+1];
0x01,                                                                               // ASI;
{0x59,0x99},                                                                        // BrKey[2];
{0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38},                                          // TermIdent[8];
{0x00,0x00,0x13,0x88},                                                              // FloorLimit[4];
{0x00,0x00,0x01,0xF4},                                                              // Threshhold[4];
0x00,                                                                               // TargetPercentage;
0x00,                                                                               // MaxTargetPercentage;
{0x00,0x10,0x00,0x00,0x00},                                                         // TACDenial[5];
{0xD8,0x40,0x04,0xF8,0x00},                                                         // TACOnline[5];
{0xD8,0x40,0x00,0xA8,0x00},                                                         // TACDefault[5];
0x01,                                                                               // EMV_Application;
{0x00, {0x00}},                                                                     // Default_TDOL[1+MAX_LG_TDOL];
{0x03,{0x9F,0x37,0x04}},                                                            // Default_DDOL[1+MAX_LG_DDOL];
{0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20},       // MerchIdent[15];
CDA_EMV_MODE_3,                                                                     // CDA mode
EMV_CT_PROCESS_TACIAC_DEFAULT_BEFORE,                                                      // offline only behaviour
0x00,                                                                               // AIP_CVM_not_supported;
0x05,                                                                               // POS entry mode
{0x00},																			///< up to 10 additional version numbers, optional if needed for compliancy (Verix up to 10, Velocity up to 2), validity bit #EMV_CT_INPUT_APL_ADD_VERSIONS  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF5F_ADD_APP_VERSION, @n Additional_Versions_No in EMV_CT_APPLIDATA_STRUCT::Additional_Versions_No, @n XML Tag: #XML_TAG_APPLIDATA_ADD_VER_NUM
{0x00,0x00,0x00,0x00},														 ///< General: Binary coded @n Below this limit simplified termcaps may take place, e.g. CVM processing is skipped/handled as per parameter Availability bit: #EMV_CT_INPUT_APL_SEC_LIMIT  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF49_APL_SEC_LIMIT, @n Security_Limit in EMV_CT_APPLIDATA_STRUCT::Security_Limit, @n XML Tag: #XML_TAG_APPLIDATA_SECURE_LIMIT
{0x01,0x1F,0x15,0x00,0x00},                                                         // App_FlowCap[5];

{0x00},                                                                             // Additional_Tags[ADD_TAG_SIZE];
{ 3, {0x9F,0x7B,0xFF}},
{ 2, {0x57,(unsigned short)0x9F08,0,0,0,0,0,0,0,0}},                                // Taglist;

{0xE0,0xD0,0xC8},                                                                   // terminal capabilities
{0x00,0x00,0x00},															///< Below security limit terminal capabilities (e.g. cardholder verification methods) for CT chip transactions below the CVM / Security limit. Availability bit: #EMV_CT_INPUT_APL_SEC_CAPS  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF4A_APL_SEC_CAPS, @n Capabilities_belowLimit in EMV_CT_APPLIDATA_STRUCT::Capabilities_belowLimit, @n XML Tag: #XML_TAG_APPLIDATA_NON_SECURE_CAPS
{0x02,0x50},                                                                        // country code (France)
{0xF0,0x00,0xB0,0xA0,0x01},                                                         // additional terminal capabilities
0x22,                                                                               // terminal type

{                                                                                   // xAIDPrio[MAX_PRIO_APP];
  {
    0,
    {0x00}
  },
  {
    0,
    {0x00}
  },
  {
    0,
    {0x00}
  },
  {
    0,
    {0x00}
  },
  {
    0,
    {0x00}
  }
},
{0x00,0x00,0x00},                                                                   // tucFallbackMIDs[MAX_CHP_TO_MSR]
{0x21,0x20,0x11,0x00,0x00,0x00,0x00,0x00},                                          // xuc_Special_TRX[8];
0x00,                                                                               // uc_FallBack_Handling;
0x00,									///< to enable a customer CVM for CVM for this AID, to fill according EMV specs if needed (range 1000000-101111 is allowed), validity bit #EMV_CT_INPUT_APL_CUSTOMER_CVM  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF2F_CVM_CUSTOM, @n Customer_CVM in EMV_CT_APPLIDATA_STRUCT::Customer_CVM, @n XML Tag: #XML_TAG_APPLIDATA_CUSTOMER_CVM
{0x01,0x00,0x00,0x00,0x00},                                                         // checksum params
{0x00},                                                                             // checksum value (empty)
{ 0, {0x00}},                                                                       // Master AID (empty)

{0xFF, 0xFF, 0xFF, 0xFF, 0x07, 0x00, 0x00, 0x00}                                    // Info_Included_Data
};

 EMV_CT_APPLI_TYPE  xAID_JCB =
{
7,
{0xA0,0x00,0x00,0x00,0x65,0x10,0x10}
};

 EMV_CT_APPLIDATA_TYPE  xAppliData_JCB =
{
{0x00,0x00},                                                                        // VerNum[2];
{"JCB"},                                                                            // AppName[16+1];
0x01,                                                                               // ASI;
{0x59,0x99},                                                                        // BrKey[2];
{0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38},                                          // TermIdent[8];
{0x00,0x00,0x13,0x88},                                                              // FloorLimit[4];
{0x00,0x00,0x00,0x00},                                                              // Threshhold[4];
0x00,                                                                               // TargetPercentage;
0x00,                                                                               // MaxTargetPercentage;
{0x00,0x10,0x00,0x00,0x00},                                                         // TACDenial[5];
{0xD8,0x40,0x04,0xF8,0x00},                                                         // TACOnline[5];
{0xDC,0x40,0x00,0xA8,0x00},                                                         // TACDefault[5];
0x01,                                                                               // EMV_Application;
{0x00, {0x00}},                                                                     // Default_TDOL[1+MAX_LG_TDOL];
{0x03,{0x9F,0x37,0x04}},                                                            // Default_DDOL[1+MAX_LG_DDOL];
{0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20},       // MerchIdent[15];
CDA_EMV_MODE_3,                                                                     // CDA mode
EMV_CT_PROCESS_TACIAC_DEFAULT_BEFORE,                                                      // offline only behaviour
0x00,                                                                               // AIP_CVM_not_supported;
0x05,                                                                               // POS entry mode
{0x00},																			///< up to 10 additional version numbers, optional if needed for compliancy (Verix up to 10, Velocity up to 2), validity bit #EMV_CT_INPUT_APL_ADD_VERSIONS  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF5F_ADD_APP_VERSION, @n Additional_Versions_No in EMV_CT_APPLIDATA_STRUCT::Additional_Versions_No, @n XML Tag: #XML_TAG_APPLIDATA_ADD_VER_NUM
{0x00,0x00,0x00,0x00},														 ///< General: Binary coded @n Below this limit simplified termcaps may take place, e.g. CVM processing is skipped/handled as per parameter Availability bit: #EMV_CT_INPUT_APL_SEC_LIMIT  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF49_APL_SEC_LIMIT, @n Security_Limit in EMV_CT_APPLIDATA_STRUCT::Security_Limit, @n XML Tag: #XML_TAG_APPLIDATA_SECURE_LIMIT
{0x01,0x1F,0x15,0x00,0x00},                                                         // App_FlowCap[5];

{0x00},                                                                             // Additional_Tags[ADD_TAG_SIZE];
{ 3, {0x9F,0x7B,0xFF}},
{ 0, {0,0,0,0,0,0,0,0,0,0}},                                                        // Taglist;

{0xE0,0xF8,0xC8},                                                                   // terminal capabilities
{0x00,0x00,0x00},															///< Below security limit terminal capabilities (e.g. cardholder verification methods) for CT chip transactions below the CVM / Security limit. Availability bit: #EMV_CT_INPUT_APL_SEC_CAPS  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF4A_APL_SEC_CAPS, @n Capabilities_belowLimit in EMV_CT_APPLIDATA_STRUCT::Capabilities_belowLimit, @n XML Tag: #XML_TAG_APPLIDATA_NON_SECURE_CAPS
{0x02,0x50},                                                                        // country code (France)
{0xF0,0x00,0xB0,0xA0,0x01},                                                         // additional terminal capabilities
0x22,                                                                               // terminal type

{                                                                                   // xAIDPrio[MAX_PRIO_APP];
  {
    0,
    {0x00}
  },
  {
    0,
    {0x00}
  },
  {
    0,
    {0x00}
  },
  {
    0,
    {0x00}
  },
  {
    0,
    {0x00}
  }
},
{0x00,0x00,0x00},                                                                   // tucFallbackMIDs[MAX_CHP_TO_MSR]
{0x21,0x20,0x11,0x00,0x00,0x00,0x00,0x00},                                          // xuc_Special_TRX[8];
0x00,                                                                               // uc_FallBack_Handling;
0x00,									///< to enable a customer CVM for CVM for this AID, to fill according EMV specs if needed (range 1000000-101111 is allowed), validity bit #EMV_CT_INPUT_APL_CUSTOMER_CVM  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF2F_CVM_CUSTOM, @n Customer_CVM in EMV_CT_APPLIDATA_STRUCT::Customer_CVM, @n XML Tag: #XML_TAG_APPLIDATA_CUSTOMER_CVM
{0x01,0x00,0x00,0x00,0x00},                                                         // checksum params
{0x00},                                                                             // checksum value (empty)
{ 0, {0x00}},                                                                       // Master AID (empty)

{0xFF, 0xFF, 0xFF, 0xFF, 0x07, 0x00, 0x00, 0x00}                                    // Info_Included_Data
};

 EMV_CT_APPLI_TYPE  xAID_AMEX =
{
6,
{0xA0,0x00,0x00,0x00,0x25,0x01}
};

 EMV_CT_APPLIDATA_TYPE  xAppliData_AMEX =
{
{0x00,0x01},                                                                        // VerNum[2];
{"AMEX"},                                                                           // AppName[16+1];
0x01,                                                                               // ASI;
{0x00,0x00},                                                                        // BrKey[2];
{0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38},                                          // TermIdent[8];
{0x00,0x00,0x13,0x88},                                                              // FloorLimit[4];
{0x00,0x00,0x00,0x00},                                                              // Threshhold[4];
0x00,                                                                               // TargetPercentage;
0x00,                                                                               // MaxTargetPercentage;
{0x00,0x00,0x00,0x00,0x00},                                                         // TACDenial[5];
{0x00,0x00,0x00,0x00,0x00},                                                         // TACOnline[5];
{0x00,0x00,0x00,0x00,0x00},                                                         // TACDefault[5];
0x01,                                                                               // EMV_Application;
{0x00, {0x00}},                                                                     // Default_TDOL[1+MAX_LG_TDOL];
{0x03,{0x9F,0x37,0x04}},                                                            // Default_DDOL[1+MAX_LG_DDOL];
{0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20},       // MerchIdent[15];
CDA_EMV_MODE_3,                                                                     // CDA mode
EMV_CT_PROCESS_TACIAC_DEFAULT_BEFORE,                                                      // offline only behaviour
0x00,                                                                               // AIP_CVM_not_supported;
0x05,                                                                               // POS entry mode
{0x00},																			///< up to 10 additional version numbers, optional if needed for compliancy (Verix up to 10, Velocity up to 2), validity bit #EMV_CT_INPUT_APL_ADD_VERSIONS  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF5F_ADD_APP_VERSION, @n Additional_Versions_No in EMV_CT_APPLIDATA_STRUCT::Additional_Versions_No, @n XML Tag: #XML_TAG_APPLIDATA_ADD_VER_NUM
{0x00,0x00,0x00,0x00},														 ///< General: Binary coded @n Below this limit simplified termcaps may take place, e.g. CVM processing is skipped/handled as per parameter Availability bit: #EMV_CT_INPUT_APL_SEC_LIMIT  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF49_APL_SEC_LIMIT, @n Security_Limit in EMV_CT_APPLIDATA_STRUCT::Security_Limit, @n XML Tag: #XML_TAG_APPLIDATA_SECURE_LIMIT
{0x01,0x1F,0x15,0x00,0x00},                                                         // App_FlowCap[5];

{0x00},                                                                             // Additional_Tags[ADD_TAG_SIZE];
{ 3, {0x9F,0x7B,0xFF}},
{ 0, {0,0,0,0,0,0,0,0,0,0}},                                                        // Taglist;

{0xE0,0xF8,0xC8},                                                                   // terminal capabilities
{0x00,0x00,0x00},															///< Below security limit terminal capabilities (e.g. cardholder verification methods) for CT chip transactions below the CVM / Security limit. Availability bit: #EMV_CT_INPUT_APL_SEC_CAPS  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF4A_APL_SEC_CAPS, @n Capabilities_belowLimit in EMV_CT_APPLIDATA_STRUCT::Capabilities_belowLimit, @n XML Tag: #XML_TAG_APPLIDATA_NON_SECURE_CAPS
{0x02,0x50},                                                                        // country code (France)
{0xF0,0x00,0xB0,0xA0,0x01},                                                         // additional terminal capabilities
0x22,                                                                               // terminal type

{                                                                                   // xAIDPrio[MAX_PRIO_APP];
  {
    0,
    {0x00}
  },
  {
    0,
    {0x00}
  },
  {
    0,
    {0x00}
  },
  {
    0,
    {0x00}
  },
  {
    0,
    {0x00}
  }
},
{0x00,0x00,0x00},                                                                   // tucFallbackMIDs[MAX_CHP_TO_MSR]
{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},                                          // xuc_Special_TRX[8];
0x00,                                                                               // uc_FallBack_Handling;
0x00,									///< to enable a customer CVM for CVM for this AID, to fill according EMV specs if needed (range 1000000-101111 is allowed), validity bit #EMV_CT_INPUT_APL_CUSTOMER_CVM  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF2F_CVM_CUSTOM, @n Customer_CVM in EMV_CT_APPLIDATA_STRUCT::Customer_CVM, @n XML Tag: #XML_TAG_APPLIDATA_CUSTOMER_CVM
{0x01,0x00,0x00,0x00,0x00},                                                         // checksum params
{0x00},                                                                             // checksum value (empty)
{ 0, {0x00}},                                                                       // Master AID (empty)

{0xFF, 0xFF, 0xFF, 0xFF, 0x07, 0x00, 0x00, 0x00}                                    // Info_Included_Data
};

// EMV_CT_APPLI_TYPE  xAID_POSTCARD =
//{
//7,
//{0xA0,0x00,0x00,0x01,0x11}
//};

 //iqbal_audi_030217 comment below

//// EMV_CT_APPLIDATA_TYPE  xAppliData_POSTCARD =
////{
////{0x00,0x8C},                                                                        // VerNum[2];
////{"Postcard"},                                                                       // AppName[16+1];
////0x01,                                                                               // ASI;
////{0x59,0x99},                                                                        // BrKey[2];
////{0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38},                                          // TermIdent[8];
////{0x00,0x00,0x13,0x88},                                                              // FloorLimit[4];
////{0x00,0x00,0x00,0x00},                                                              // Threshhold[4];
////0x00,                                                                               // TargetPercentage;
////0x00,                                                                               // MaxTargetPercentage;
////{0x00,0x10,0x00,0x00,0x00},                                                         // TACDenial[5];
////{0xD8,0x40,0x04,0xF8,0x00},                                                         // TACOnline[5];
////{0xDC,0x40,0x00,0xA8,0x00},                                                         // TACDefault[5];
////0x01,                                                                               // EMV_Application;
////{0x00, {0x00}},                                                                     // Default_TDOL[1+MAX_LG_TDOL];
////{0x03,{0x9F,0x37,0x04}},                                                            // Default_DDOL[1+MAX_LG_DDOL];
////{0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20},       // MerchIdent[15];
////CDA_EMV_MODE_3,                                                                     // CDA mode
////EMV_CT_PROCESS_TACIAC_DEFAULT_BEFORE,                                                      // offline only behaviour
////0x00,                                                                               // AIP_CVM_not_supported;
////0x05,                                                                               // POS entry mode
////{0x01,0x1F,0x15,0x00,0x00},                                                         // App_FlowCap[5];
////
////{0x00},                                                                             // Additional_Tags[ADD_TAG_SIZE];
////{ 3, {0x9F,0x7B,0xFF}},
////{ 0, {0,0,0,0,0,0,0,0,0,0}},                                                        // Taglist;
////
////{0xE0,0xF8,0xC8},                                                                   // terminal capabilities
////{0x02,0x50},                                                                        // country code (France)
////{0xF0,0x00,0xB0,0xA0,0x01},                                                         // additional terminal capabilities
////0x22,                                                                               // terminal type
////
////{                                                                                   // xAIDPrio[MAX_PRIO_APP];
////  {
////    0,
////    {0x00}
////  },
////  {
////    0,
////    {0x00}
////  },
////  {
////    0,
////    {0x00}
////  },
////  {
////    0,
////    {0x00}
////  },
////  {
////    0,
////    {0x00}
////  }
////},
////{0x00,0x00,0x00},                                                                   // tucFallbackMIDs[MAX_CHP_TO_MSR]
////{0x21,0x20,0x11,0x00,0x00,0x00,0x00,0x00},                                          // xuc_Special_TRX[8];
////0x00,                                                                               // uc_FallBack_Handling;
////
////{0x01,0x00,0x00,0x00,0x00},                                                         // checksum params
////{0x00},                                                                             // checksum value (empty)
////{ 0, {0x00}},                                                                       // Master AID (empty)
////
////{0xFF, 0xFF, 0xFF, 0xFF, 0x07, 0x00, 0x00, 0x00}                                    // Info_Included_Data
////};
////
//// EMV_CT_APPLI_TYPE  xAID_DANK =
////{
////7,
////{0xA0,0x00,0x00,0x01,0x21}
////};
////
//// EMV_CT_APPLIDATA_TYPE  xAppliData_DANK =
////{
////{0x00,0x00},                                                                        // VerNum[2];
////{"Dankort"},                                                                        // AppName[16+1];
////0x01,                                                                               // ASI;
////{0x59,0x99},                                                                        // BrKey[2];
////{0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38},                                          // TermIdent[8];
////{0x00,0x00,0x13,0x88},                                                              // FloorLimit[4];
////{0x00,0x00,0x00,0x00},                                                              // Threshhold[4];
////0x00,                                                                               // TargetPercentage;
////0x00,                                                                               // MaxTargetPercentage;
////{0x00,0x10,0x00,0x00,0x00},                                                         // TACDenial[5];
////{0xD8,0x40,0x04,0xF8,0x00},                                                         // TACOnline[5];
////{0xDC,0x40,0x00,0xA8,0x00},                                                         // TACDefault[5];
////0x01,                                                                               // EMV_Application;
////{0x00, {0x00}},                                                                     // Default_TDOL[1+MAX_LG_TDOL];
////{0x03,{0x9F,0x37,0x04}},                                                            // Default_DDOL[1+MAX_LG_DDOL];
////{0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20},       // MerchIdent[15];
////CDA_EMV_MODE_3,                                                                     // CDA mode
////EMV_CT_PROCESS_TACIAC_DEFAULT_BEFORE,                                                      // offline only behaviour
////0x00,                                                                               // AIP_CVM_not_supported;
////0x05,                                                                               // POS entry mode
////{0x01,0x1F,0x15,0x00,0x00},                                                         // App_FlowCap[5];
////
////{0x00},                                                                             // Additional_Tags[ADD_TAG_SIZE];
////{ 3, {0x9F,0x7B,0xFF}},
////{ 0, {0,0,0,0,0,0,0,0,0,0}},                                                        // Taglist;
////
////{0xE0,0xF8,0xC8},                                                                   // terminal capabilities
////{0x02,0x50},                                                                        // country code (France)
////{0xF0,0x00,0xB0,0xA0,0x01},                                                         // additional terminal capabilities
////0x22,                                                                               // terminal type
////
////{                                                                                   // xAIDPrio[MAX_PRIO_APP];
////  {
////    0,
////    {0x00}
////  },
////  {
////    0,
////    {0x00}
////  },
////  {
////    0,
////    {0x00}
////  },
////  {
////    0,
////    {0x00}
////  },
////  {
////    0,
////    {0x00}
////  }
////},
////{0x00,0x00,0x00},                                                                   // tucFallbackMIDs[MAX_CHP_TO_MSR]
////{0x21,0x20,0x11,0x00,0x00,0x00,0x00,0x00},                                          // xuc_Special_TRX[8];
////0x00,                                                                               // uc_FallBack_Handling;
////
////{0x01,0x00,0x00,0x00,0x00},                                                         // checksum params
////{0x00},                                                                             // checksum value (empty)
////{ 0, {0x00}},                                                                       // Master AID (empty)
////
////{0xFF, 0xFF, 0xFF, 0xFF, 0x07, 0x00, 0x00, 0x00}                                    // Info_Included_Data
////};
////
////
//// EMV_CT_APPLI_TYPE  xAID_girocard =
////{
////10,
////{0xA0, 0x00, 0x00, 0x03, 0x59, 0x10, 0x10, 0x02, 0x80, 0x01}
////};
////
//// EMV_CT_APPLIDATA_TYPE  xAppliData_girocard =
////{
////{0x00,0x02},                                                                        // VerNum[2];
////{0x67,0x69,0x72,0x6F,0x63,0x61,0x72,0x64,0x00},                                     // AppName[16+1];
////0x01,                                                                               // ASI;
////{0x59,0x99},                                                                        // BrKey[2];
////{0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38},                                          // TermIdent[8];
////{0x00,0x00,0x13,0x88},                                                              // FloorLimit[4];
////{0x00,0x00,0x01,0xF4},                                                              // Threshhold[4];
////0x00,                                                                               // TargetPercentage;
////0x00,                                                                               // MaxTargetPercentage;
////{0x04,0x00,0x00,0x00,0x00},                                                         // TACDenial[5];
////{0xF8,0x50,0xAC,0xF8,0x00},                                                         // TACOnline[5];
////{0xFC,0x50,0xAC,0xA0,0x00},                                                         // TACDefault[5];
////0x01,                                                                               // EMV_Application;
////{0x0F,{0x9F,0x02,0x06,0x5F,0x2A,0x02,0x9A,0x03,0x9C,0x01,0x95,0x05,0x9F,0x37,0x04}},  // Default_TDOL[1+MAX_LG_TDOL];
////{0x03,{0x9F,0x37,0x04}},                                                            // Default_DDOL[1+MAX_LG_DDOL];
////{0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20},       // MerchIdent[15];
////CDA_EMV_MODE_3,                                                                     // CDA mode
////EMV_CT_PROCESS_TACIAC_DEFAULT_BEFORE,                                                      // offline only behaviour
////0x00,                                                                               // AIP_CVM_not_supported;
////0x05,                                                                               // POS entry mode
////{0x01,0x1F,0x15,0x00,0x00},                                                         // App_FlowCap[5];
////
////{0x9F,0x53,0x01,0x52},                                                              // Additional_Tags[ADD_TAG_SIZE];
////{ 3, {0x9F,0x7B,0xFF}},
////{ 1, {0x8E,0,0,0,0,0,0,0,0,0}},                                                     // Taglist;
////
//////  {0xE0,0xD0,0xC8},                                                                   // terminal capabilities
////{0xE0,0xD8,0xC8},                                                                   // terminal capabilities
////{0x02,0x50},                                                                        // country code (France)
////{0xF0,0x00,0xF0,0xA0,0x01},                                                         // additional terminal capabilities
////0x22,                                                                               // terminal type
////
////{                                                                                   // xAIDPrio[MAX_PRIO_APP];
////  {
////    0,
////    {0x00}
////  },
////  {
////    0,
////    {0x00}
////  },
////  {
////    0,
////    {0x00}
////  },
////  {
////    0,
////    {0x00}
////  },
////  {
////    0,
////    {0x00}
////  }
////},
////{0x00,0x00,0x00},                                                                   // tucFallbackMIDs[MAX_CHP_TO_MSR]
////{0x21,0x20,0x11,0x00,0x00,0x00,0x00,0x00},                                          // xuc_Special_TRX[8];
////0x00,                                                                               // uc_FallBack_Handling;
////
////{EMV_CT_CHECKSUM_INCLUDE_VERSION|EMV_CT_CHECKSUM_SUPPORT_ONL_DATA_CAPTURE|EMV_CT_CHECKSUM_SUPPORT_PSE,EMV_CT_CHECKSUM_PIN_BYPASS,0,0,0},  // checksum params
////  // Approval EMV L2 6.16l, Vx520, V/OS Version 0625, 2012-12-11 (2-02400-1-1C-RFI-1212-4.3.a, 2-02400-1-1OS-RFI-1212-4.3.a):
////  // Checksum must be 31 34 18 ED
////  // TermCaps: 0xE0,0xB8,0xC8;  AddTermCaps: 0xF0,0x00,0xF0,0xA0,0x01;
////  // Checksum params: 0x61,0x04,0x00,0x00,0x00 (= CS_INCLUDE_VERSION|CS_SUPPORT_ONL_DATA_CAPTURE|CS_SUPPORT_PSE,CS_PIN_BYPASS,0,0,0)
////
////{0x00},                                                                             // checksum value (empty)
////{ 0, {0x00}},                                                                       // Master AID (empty)
////
////{0xFF, 0xFF, 0xFF, 0xFF, 0x07, 0x00, 0x00, 0x00}                                    // Info_Included_Data
////};
////
////
//// EMV_CT_APPLI_TYPE  xAID_EAPS =
////{
////5,
////{0xA0, 0x00, 0x00, 0x03, 0x59}
////};
////
//// EMV_CT_APPLIDATA_TYPE  xAppliData_EAPS =
////{
////{0x00,0x02},                                                                        // VerNum[2];
////{0x45,0x41,0x50,0x53,0x00},                                                         // AppName[16+1];
////0x01,                                                                               // ASI;
////{0x59,0x99},                                                                        // BrKey[2];
////{0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38},                                          // TermIdent[8];
////{0x00,0x00,0x13,0x88},                                                              // FloorLimit[4];
////{0x00,0x00,0x01,0xF4},                                                              // Threshhold[4];
////0x00,                                                                               // TargetPercentage;
////0x00,                                                                               // MaxTargetPercentage;
////{0x04,0x00,0x00,0x00,0x00},                                                         // TACDenial[5];
////{0xF8,0x50,0xAC,0xF8,0x00},                                                         // TACOnline[5];
////{0xFC,0x50,0xAC,0xA0,0x00},                                                         // TACDefault[5];
////0x01,                                                                               // EMV_Application;
////{0x0F,{0x9F,0x02,0x06,0x5F,0x2A,0x02,0x9A,0x03,0x9C,0x01,0x95,0x05,0x9F,0x37,0x04}},  // Default_TDOL[1+MAX_LG_TDOL];
////{0x03,{0x9F,0x37,0x04}},                                                            // Default_DDOL[1+MAX_LG_DDOL];
////{0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20},       // MerchIdent[15];
////CDA_EMV_MODE_3,                                                                     // CDA mode
////EMV_CT_PROCESS_TACIAC_DEFAULT_BEFORE,                                                      // offline only behaviour
////0x00,                                                                               // AIP_CVM_not_supported;
////0x05,                                                                               // POS entry mode
////{0x01,0x1F,0x15,0x00,0x00},                                                         // App_FlowCap[5];
////
////{0x9F,0x53,0x01,0x52},                                                              // Additional_Tags[ADD_TAG_SIZE];
////{ 3, {0x9F,0x7B,0xFF}},
////{ 1, {0x8E,0,0,0,0,0,0,0,0,0}},                                                     // Taglist;
////
////{0xE0,0xF0,0xC8},                                                                   // terminal capabilities
////{0x02,0x50},                                                                        // country code (France)
////{0xF0,0x00,0xB0,0xA0,0x01},                                                         // additional terminal capabilities
////0x22,                                                                               // terminal type
////
////{                                                                                   // xAIDPrio[MAX_PRIO_APP];
////  {
////    0,
////    {0x00}
////  },
////  {
////    0,
////    {0x00}
////  },
////  {
////    0,
////    {0x00}
////  },
////  {
////    0,
////    {0x00}
////  },
////  {
////    0,
////    {0x00}
////  }
////},
////{0x00,0x00,0x00},                                                                   // tucFallbackMIDs[MAX_CHP_TO_MSR]
////{0x21,0x20,0x11,0x00,0x00,0x00,0x00,0x00},                                          // xuc_Special_TRX[8];
////0x00,                                                                               // uc_FallBack_Handling;
////
////{0x01,0x00,0x00,0x00,0x00},                                                         // checksum params
////{0x00},                                                                             // checksum value (empty)
////{ 0, {0x00}},                                                                       // Master AID (empty)
////
////{0xFF, 0xFF, 0xFF, 0xFF, 0x07, 0x00, 0x00, 0x00}                                    // Info_Included_Data
////};
////

 EMV_CT_APPLI_TYPE  xAID_Empty =
{
7,
{0xA0,0x00,0x00,0x00,0x00,0x00,0x00}
};

 EMV_CT_APPLIDATA_TYPE  xAppliData_Empty =
{
{0x00,0x80},                                                                        // VerNum[2];
{0x00,0x00,0x00,0x00,0x00},                                                         // AppName[16+1];
0x01,                                                                               // ASI;
{0x59,0x99},                                                                        // BrKey[2];
{0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38},                                          // TermIdent[8];
{0x00,0x00,0x13,0x88},                                                              // FloorLimit[4];
{0x00,0x00,0x01,0xF4},                                                              // Threshhold[4];
0x00,                                                                               // TargetPercentage;
0x00,                                                                               // MaxTargetPercentage;
{0x00,0x10,0x00,0x00,0x00},                                                         // TACDenial[5];
{0xD8,0x40,0x04,0xF8,0x00},                                                         // TACOnline[5];
{0xD8,0x40,0x00,0xA8,0x00},                                                         // TACDefault[5];
0x01,                                                                               // EMV_Application;
{0x00, {0x00}},                                                                             // Default_TDOL[1+MAX_LG_TDOL];
{0x03,{0x9F,0x37,0x04}},                                                            // Default_DDOL[1+MAX_LG_DDOL];
{0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20},       // MerchIdent[15];
CDA_EMV_MODE_3,                                                                     // CDA mode
EMV_CT_PROCESS_TACIAC_DEFAULT_BEFORE,                                                      // offline only behaviour
0x00,                                                                               // AIP_CVM_not_supported;
0x05,                                                                               // POS entry mode
{0x00},																			///< up to 10 additional version numbers, optional if needed for compliancy (Verix up to 10, Velocity up to 2), validity bit #EMV_CT_INPUT_APL_ADD_VERSIONS  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF5F_ADD_APP_VERSION, @n Additional_Versions_No in EMV_CT_APPLIDATA_STRUCT::Additional_Versions_No, @n XML Tag: #XML_TAG_APPLIDATA_ADD_VER_NUM
{0x00,0x00,0x00,0x00},														 ///< General: Binary coded @n Below this limit simplified termcaps may take place, e.g. CVM processing is skipped/handled as per parameter Availability bit: #EMV_CT_INPUT_APL_SEC_LIMIT  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF49_APL_SEC_LIMIT, @n Security_Limit in EMV_CT_APPLIDATA_STRUCT::Security_Limit, @n XML Tag: #XML_TAG_APPLIDATA_SECURE_LIMIT
{0x01,0x1F,0x15,0x00,0x00},                                                         // App_FlowCap[5];

{0x00},                                                                             // Additional_Tags[ADD_TAG_SIZE];
{ 3, {0x9F,0x7B,0xFF}},
{ 2, {0x57,(unsigned short)0x9F08,0,0,0,0,0,0,0,0}},                                // Taglist;

{0xE0,0xF8,0xC8},                                                                   // terminal capabilities
{0x00,0x00,0x00},															///< Below security limit terminal capabilities (e.g. cardholder verification methods) for CT chip transactions below the CVM / Security limit. Availability bit: #EMV_CT_INPUT_APL_SEC_CAPS  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF4A_APL_SEC_CAPS, @n Capabilities_belowLimit in EMV_CT_APPLIDATA_STRUCT::Capabilities_belowLimit, @n XML Tag: #XML_TAG_APPLIDATA_NON_SECURE_CAPS
{0x02,0x50},                                                                        // country code (France)
{0xF0,0x00,0xB0,0xA0,0x01},                                                         // additional terminal capabilities
0x22,                                                                               // terminal type

{                                                                                   // xAIDPrio[MAX_PRIO_APP];
  {
    0,
    {0x00}
  },
  {
    0,
    {0x00}
  },
  {
    0,
    {0x00}
  },
  {
    0,
    {0x00}
  },
  {
    0,
    {0x00}
  }
},
{0x00,0x00,0x00},                                                                   // tucFallbackMIDs[MAX_CHP_TO_MSR]
{0x21,0x20,0x11,0x00,0x00,0x00,0x00,0x00},                                          // xuc_Special_TRX[8];
0x00,                                                                               // uc_FallBack_Handling;
0x00,									///< to enable a customer CVM for CVM for this AID, to fill according EMV specs if needed (range 1000000-101111 is allowed), validity bit #EMV_CT_INPUT_APL_CUSTOMER_CVM  @n@c Tag, @c Struct, @c XML Reference: @n TLV tag #TAG_DF2F_CVM_CUSTOM, @n Customer_CVM in EMV_CT_APPLIDATA_STRUCT::Customer_CVM, @n XML Tag: #XML_TAG_APPLIDATA_CUSTOMER_CVM
{0x01,0x00,0x00,0x00,0x00},                                                         // checksum params
{0x00},                                                                             // checksum value (empty)
{ 0, {0x00}},                                                                       // Master AID (empty)

{0xFF, 0xFF, 0xFF, 0xFF, 0x07, 0x00, 0x00, 0x00}                                    // Info_Included_Data
};

static  EMV_CT_APPLI_TYPE*  ptxAIDEmpty[] =
{
&xAID_Empty,
NULL
  // Interac
  // Diners
};

static  EMV_CT_APPLIDATA_TYPE*  ptxAppliDataEmpty[] =
{
&xAppliData_Empty,
NULL
  // Interac
  // Diners
};



static  EMV_CT_APPLI_TYPE*  ptxAID[] =
{
&xAID_MC,
&xAID_VIS,
&xAID_MAESTRO,
&xAID_VIS_ELEC,
&xAID_VIS_VPAY,
&xAID_JCB,
&xAID_AMEX,
//&xAID_POSTCARD,   //iqbal_audi_030217
//&xAID_DANK,
//&xAID_girocard,
//&xAID_EAPS,
NULL
  // Interac
  // Diners
};

static  EMV_CT_APPLIDATA_TYPE*  ptxAppliData[] =
{
&xAppliData_MC,
&xAppliData_VIS,
&xAppliData_MAESTRO,
&xAppliData_VIS_ELEC,
&xAppliData_VIS_VPAY,
&xAppliData_JCB,
&xAppliData_AMEX,
//&xAppliData_POSTCARD,
//&xAppliData_DANK,
//&xAppliData_girocard,
//&xAppliData_EAPS,
NULL
  // Interac
  // Diners
};


EMV_CT_CAPKEY_TYPE  xKeyData_MC1T1 =
{
(unsigned char *)"\xA0\x00\x00\x00\x04",                              // RID
0xFB,                          				// Index
(unsigned char *)"\xA9\x54\x8D\xFB\x39\x8B\x48\x12\x3F\xAF\x41\xE6\xCF\xA4\xAE\x1E\x23\x52\xB5\x18\xAB\x4B\xCE\xFE\xCD\xB0\xB3\xED\xEC\x09\x02\x87\xD8\x8B\x12\x25\x9F\x36\x1C\x1C\xC0\x88\xE5\xF0\x66\x49\x44\x17\xE8\xEE\x8B\xBF\x89\x91\xE2\xB3\x2F\xF1\x6F\x99\x46\x97\x84\x2B\x3D\x6C\xB3\x7A\x2B\xB5\x74\x2A\x44\x0B\x63\x56\xC6\x2A\xA3\x3D\xB3\xC4\x55\xE5\x9E\xDD\xF7\x86\x47\x01\xD0\x3A\x5B\x83\xEE\x9E\x9B\xD8\x3A\xB9\x33\x02\xAC\x2D\xFE\x63\xE6\x61\x20\xB0\x51\xCF\x08\x1F\x56\x32\x6A\x71\x30\x3D\x95\x2B\xB3\x36\xFF\x12\x61\x0D", 	//CAPK
0x80,			//key len
0x02,			//Exponent
NULL, 		//Hash
NULL,			//No of entries in revocation list
NULL			//Revocation list
};

EMV_CT_CAPKEY_TYPE  xKeyData_MC1T2 =
{
(unsigned char *)"\xA0\x00\x00\x00\x04",                              // RID
0xFA,                          				// Index
(unsigned char *)"\xA9\x0F\xCD\x55\xAA\x2D\x5D\x99\x63\xE3\x5E\xD0\xF4\x40\x17\x76\x99\x83\x2F\x49\xC6\xBA\xB1\x5C\xDA\xE5\x79\x4B\xE9\x3F\x93\x4D\x44\x62\xD5\xD1\x27\x62\xE4\x8C\x38\xBA\x83\xD8\x44\x5D\xEA\xA7\x41\x95\xA3\x01\xA1\x02\xB2\xF1\x14\xEA\xDA\x0D\x18\x0E\xE5\xE7\xA5\xC7\x3E\x0C\x4E\x11\xF6\x7A\x43\xDD\xAB\x5D\x55\x68\x3B\x14\x74\xCC\x06\x27\xF4\x4B\x8D\x30\x88\xA4\x92\xFF\xAA\xDA\xD4\xF4\x24\x22\xD0\xE7\x01\x35\x36\xC3\xC4\x9A\xD3\xD0\xFA\xE9\x64\x59\xB0\xF6\xB1\xB6\x05\x65\x38\xA3\xD6\xD4\x46\x40\xF9\x44\x67\xB1\x08\x86\x7D\xEC\x40\xFA\xAE\xCD\x74\x0C\x00\xE2\xB7\xA8\x85\x2D", 	//CAPK
0x90,			//key len
0x03,			//Exponent
(unsigned char *)"\x5B\xED\x40\x68\xD9\x6E\xA1\x6D\x2D\x77\xE0\x3D\x60\x36\xFC\x7A\x16\x0E\xA9\x9C", 		//Hash
NULL,			//No of entries in revocation list
NULL			//Revocation list
};

EMV_CT_CAPKEY_TYPE  xKeyData_MC1T3 =
{
(unsigned char *)"\xA0\x00\x00\x00\x04",                              // RID
0xF1,                          				// Index
(unsigned char *)"\xA0\xDC\xF4\xBD\xE1\x9C\x35\x46\xB4\xB6\xF0\x41\x4D\x17\x4D\xDE\x29\x4A\xAB\xBB\x82\x8C\x5A\x83\x4D\x73\xAA\xE2\x7C\x99\xB0\xB0\x53\xA9\x02\x78\x00\x72\x39\xB6\x45\x9F\xF0\xBB\xCD\x7B\x4B\x9C\x6C\x50\xAC\x02\xCE\x91\x36\x8D\xA1\xBD\x21\xAA\xEA\xDB\xC6\x53\x47\x33\x7D\x89\xB6\x8F\x5C\x99\xA0\x9D\x05\xBE\x02\xDD\x1F\x8C\x5B\xA2\x0E\x2F\x13\xFB\x2A\x27\xC4\x1D\x3F\x85\xCA\xD5\xCF\x66\x68\xE7\x58\x51\xEC\x66\xED\xBF\x98\x85\x1F\xD4\xE4\x2C\x44\xC1\xD5\x9F\x59\x84\x70\x3B\x27\xD5\xB9\xF2\x1B\x8F\xA0\xD9\x32\x79\xFB\xBF\x69\xE0\x90\x64\x29\x09\xC9\xEA\x27\xF8\x98\x95\x95\x41\xAA\x67\x57\xF5\xF6\x24\x10\x4F\x6E\x1D\x3A\x95\x32\xF2\xA6\xE5\x15\x15\xAE\xAD\x1B\x43\xB3\xD7\x83\x50\x88\xA2\xFA\xFA\x7B\xE7", 	//CAPK
0xB0,			//key len
0x03,			//Exponent
(unsigned char *)"\xD8\xE6\x8D\xA1\x67\xAB\x5A\x85\xD8\xC3\xD5\x5E\xCB\x9B\x05\x17\xA1\xA5\xB4\xBB", 		//Hash
NULL,			//No of entries in revocation list
NULL			//Revocation list
};

EMV_CT_CAPKEY_TYPE  xKeyData_MC1T4 =
{
(unsigned char *)"\xA0\x00\x00\x00\x04",                              // RID
0xEF,                          				// Index
(unsigned char *)"\xA1\x91\xCB\x87\x47\x3F\x29\x34\x9B\x5D\x60\xA8\x8B\x3E\xAE\xE0\x97\x3A\xA6\xF1\xA0\x82\xF3\x58\xD8\x49\xFD\xDF\xF9\xC0\x91\xF8\x99\xED\xA9\x79\x2C\xAF\x09\xEF\x28\xF5\xD2\x24\x04\xB8\x8A\x22\x93\xEE\xBB\xC1\x94\x9C\x43\xBE\xA4\xD6\x0C\xFD\x87\x9A\x15\x39\x54\x4E\x09\xE0\xF0\x9F\x60\xF0\x65\xB2\xBF\x2A\x13\xEC\xC7\x05\xF3\xD4\x68\xB9\xD3\x3A\xE7\x7A\xD9\xD3\xF1\x9C\xA4\x0F\x23\xDC\xF5\xEB\x7C\x04\xDC\x8F\x69\xEB\xA5\x65\xB1\xEB\xCB\x46\x86\xCD\x27\x47\x85\x53\x0F\xF6\xF6\xE9\xEE\x43\xAA\x43\xFD\xB0\x2C\xE0\x0D\xAE\xC1\x5C\x7B\x8F\xD6\xA9\xB3\x94\xBA\xBA\x41\x9D\x3F\x6D\xC8\x5E\x16\x56\x9B\xE8\xE7\x69\x89\x68\x8E\xFE\xA2\xDF\x22\xFF\x7D\x35\xC0\x43\x33\x8D\xEA\xA9\x82\xA0\x2B\x86\x6D\xE5\x32\x85\x19\xEB\xBC\xD6\xF0\x3C\xDD\x68\x66\x73\x84\x7F\x84\xDB\x65\x1A\xB8\x6C\x28\xCF\x14\x62\x56\x2C\x57\x7B\x85\x35\x64\xA2\x90\xC8\x55\x6D\x81\x85\x31\x26\x8D\x25\xCC\x98\xA4\xCC\x6A\x0B\xDF\xFF\xDA\x2D\xCC\xA3\xA9\x4C\x99\x85\x59\xE3\x07\xFD\xDF\x91\x50\x06\xD9\xA9\x87\xB0\x7D\xDA\xEB\x3B", 	//CAPK
0xF8,			//key len
0x03,			//Exponent
(unsigned char *)"\x21\x76\x6E\xBB\x0E\xE1\x22\xAF\xB6\x5D\x78\x45\xB7\x3D\xB4\x6B\xAB\x65\x42\x7A", 		//Hash
NULL,			//No of entries in revocation list
NULL			//Revocation list
};

EMV_CT_CAPKEY_TYPE  xKeyData_MC1T5 =
{
(unsigned char *)"\xA0\x00\x00\x00\x04",                              // RID
0xFE,                          				// Index
(unsigned char *)"\xA6\x53\xEA\xC1\xC0\xF7\x86\xC8\x72\x4F\x73\x7F\x17\x29\x97\xD6\x3D\x1C\x32\x51\xC4\x44\x02\x04\x9B\x86\x5B\xAE\x87\x7D\x0F\x39\x8C\xBF\xBE\x8A\x60\x35\xE2\x4A\xFA\x08\x6B\xEF\xDE\x93\x51\xE5\x4B\x95\x70\x8E\xE6\x72\xF0\x96\x8B\xCD\x50\xDC\xE4\x0F\x78\x33\x22\xB2\xAB\xA0\x4E\xF1\x37\xEF\x18\xAB\xF0\x3C\x7D\xBC\x58\x13\xAE\xAE\xF3\xAA\x77\x97\xBA\x15\xDF\x7D\x5B\xA1\xCB\xAF\x7F\xD5\x20\xB5\xA4\x82\xD8\xD3\xFE\xE1\x05\x07\x78\x71\x11\x3E\x23\xA4\x9A\xF3\x92\x65\x54\xA7\x0F\xE1\x0E\xD7\x28\xCF\x79\x3B\x62\xA1", 	//CAPK
0x80,			//key len
0x03,			//Exponent
(unsigned char *)"\x9A\x29\x5B\x05\xFB\x39\x0E\xF7\x92\x3F\x57\x61\x8A\x9F\xDA\x29\x41\xFC\x34\xE0", 		//Hash
NULL,			//No of entries in revocation list
NULL			//Revocation list
};

EMV_CT_CAPKEY_TYPE  xKeyData_MC1T6 =
{
(unsigned char *)"\xA0\x00\x00\x00\x04",                              // RID
0xF8,                          				// Index
(unsigned char *)"\xA1\xF5\xE1\xC9\xBD\x86\x50\xBD\x43\xAB\x6E\xE5\x6B\x89\x1E\xF7\x45\x9C\x0A\x24\xFA\x84\xF9\x12\x7D\x1A\x6C\x79\xD4\x93\x0F\x6D\xB1\x85\x2E\x25\x10\xF1\x8B\x61\xCD\x35\x4D\xB8\x3A\x35\x6B\xD1\x90\xB8\x8A\xB8\xDF\x04\x28\x4D\x02\xA4\x20\x4A\x7B\x6C\xB7\xC5\x55\x19\x77\xA9\xB3\x63\x79\xCA\x3D\xE1\xA0\x8E\x69\xF3\x01\xC9\x5C\xC1\xC2\x05\x06\x95\x92\x75\xF4\x17\x23\xDD\x5D\x29\x25\x29\x05\x79\xE5\xA9\x5B\x0D\xF6\x32\x3F\xC8\xE9\x27\x3D\x6F\x84\x91\x98\xC4\x99\x62\x09\x16\x6D\x9B\xFC\x97\x3C\x36\x1C\xC8\x26\xE1", 	//CAPK
0x80,			//key len
0x03,			//Exponent
(unsigned char *)"\xF0\x6E\xCC\x6D\x2A\xAE\xBF\x25\x9B\x7E\x75\x5A\x38\xD9\xA9\xB2\x4E\x2F\xF3\xDD", 		//Hash
NULL,			//No of entries in revocation list
NULL			//Revocation list
};

EMV_CT_CAPKEY_TYPE  xKeyData_MC1T7 =
{
(unsigned char *)"\xA0\x00\x00\x00\x04",                              // RID
0xF3,                          				// Index
(unsigned char *)"\x98\xF0\xC7\x70\xF2\x38\x64\xC2\xE7\x66\xDF\x02\xD1\xE8\x33\xDF\xF4\xFF\xE9\x2D\x69\x6E\x16\x42\xF0\xA8\x8C\x56\x94\xC6\x47\x9D\x16\xDB\x15\x37\xBF\xE2\x9E\x4F\xDC\x6E\x6E\x8A\xFD\x1B\x0E\xB7\xEA\x01\x24\x72\x3C\x33\x31\x79\xBF\x19\xE9\x3F\x10\x65\x8B\x2F\x77\x6E\x82\x9E\x87\xDA\xED\xA9\xC9\x4A\x8B\x33\x82\x19\x9A\x35\x0C\x07\x79\x77\xC9\x7A\xFF\x08\xFD\x11\x31\x0A\xC9\x50\xA7\x2C\x3C\xA5\x00\x2E\xF5\x13\xFC\xCC\x28\x6E\x64\x6E\x3C\x53\x87\x53\x5D\x50\x95\x14\xB3\xB3\x26\xE1\x23\x4F\x9C\xB4\x8C\x36\xDD\xD4\x4B\x41\x6D\x23\x65\x40\x34\xA6\x6F\x40\x3B\xA5\x11\xC5\xEF\xA3", 	//CAPK
0x90,			//key len
0x03,			//Exponent
(unsigned char *)"\xA6\x9A\xC7\x60\x3D\xAF\x56\x6E\x97\x2D\xED\xC2\xCB\x43\x3E\x07\xE8\xB0\x1A\x9A", 		//Hash
NULL,			//No of entries in revocation list
NULL			//Revocation list
};

/*
 * Default CAPK Keys
 * T represent Test Key
 * P represents Production Key
 */
static  EMV_CT_CAPKEY_TYPE*  ptxKeysData[] =
{
#if 1
//&xKeyData_MC1T1,
&xKeyData_MC1T2,
&xKeyData_MC1T3,
&xKeyData_MC1T4,
&xKeyData_MC1T5,
&xKeyData_MC1T6,
&xKeyData_MC1T7,
#else

#endif
NULL
};



#endif /* _XDEMOEMVCONFIGURATIONDATA_H_ */
