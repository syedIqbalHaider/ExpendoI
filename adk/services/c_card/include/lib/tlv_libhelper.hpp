#ifndef TLV_LIBHELPER_HPP_
#define TLV_LIBHELPER_HPP_

/*****************************************************************************
 *
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/
/**
 * @file        tlv_libhelper.hpp
 *
 * @author      Lucjan Bryndza
 *
 * @brief       TLV Helper classes & templates
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C++ Coding
 *              Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/
#include <tlv-lite/ConstData.h>
#include <tlv-lite/SafeBuffer.hpp>
#include <tlv-lite/TagLenVal.hpp>
#include <tlv-lite/ValueConverter.hpp>
#include "cism/cstring.hpp"

/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
namespace com_verifone_crdapi_hlp
{

/**
 * @addtogroup Card
 * @{ 
 */

/* ----------------------------------------------------------------- */
struct parser
{
    virtual void operator()(com_verifone_TLVLite::TagLenVal const & tlv) = 0;
};

/* ----------------------------------------------------------------- */
namespace parsers
{
    class resp_parser : public parser
    {
    public:
        resp_parser(): is_err(false) {}
        //Parse the tags
        virtual void operator()(com_verifone_TLVLite::TagLenVal const & tlv);
        int get_respcode() const {return rcode;}
        bool is_error() { return is_err; }
    protected:
        void set_error(bool err)
        {
            is_err |= err;
        }
    private:
        int rcode;
        bool is_err;
    };

    template <typename T>
    class param1_parser : public resp_parser
    {
    public:
        param1_parser(const com_verifone_TLVLite::ConstData_t &tag_,resp_parser *chain_=NULL)
        : tag(tag_),chain(chain_) {}
        virtual void operator()(com_verifone_TLVLite::TagLenVal const & tlv)
        {
            using namespace com_verifone_TLVLite;
            if(tlv.getTag()==tag)
            {
                ValueConverter<T> value(tlv.getData());
                param1 = value.getValue();
                set_error(value.isError());
            }
            else
            {
                if(chain) (*chain)(tlv);
                else resp_parser::operator()(tlv);
            }
        }
        T get_param1() { return param1; }
    private:
        T param1;
        com_verifone_TLVLite::ConstData_t tag;
        resp_parser *chain;
    };

    class str_parser : public resp_parser
    {
        public:
            str_parser(const com_verifone_TLVLite::ConstData_t &tag_)
            : tag(tag_) {}
            virtual void operator()(com_verifone_TLVLite::TagLenVal const & tlv);
            const com_verifone_utils::c_string& get_str() const { return str; }
        private:
            com_verifone_utils::c_string str;
            com_verifone_TLVLite::ConstData_t tag;
    };

    class data_parser : public resp_parser
    {
        public:
            data_parser(const com_verifone_TLVLite::ConstData_t &tag_)
            : tag(tag_) { m_pdata = NULL; m_size = 0; }
            virtual void operator()(com_verifone_TLVLite::TagLenVal const & tlv);
            const unsigned char * get_data() const { return m_pdata; }
            const std::size_t get_size() const { return m_size; }
        private:
            unsigned char * m_pdata;
            std::size_t m_size;
            com_verifone_TLVLite::ConstData_t tag;
    };
}

/* ----------------------------------------------------------------- */
class tlv_libhelper
{
    private:    //Config
        static const unsigned TLV_BUF_SIZE = 2048;
        static const unsigned APP_NAME_CRD_SIZE = 64;
        static const unsigned KEEP_ALIVE_TIMEOUT = 10000;
    
    public:
        static const unsigned READ_TIMEOUT = 6000;
        tlv_libhelper(unsigned char cmd_, const char *app_name_, const char *app_name_crd_);
        template<typename T> void add_tag(const com_verifone_TLVLite::ConstData_t& tag, T val)
        {
             buffer.append(
                com_verifone_TLVLite::TagLenVal(
                        tag, com_verifone_TLVLite::ConstData_t(&val,sizeof(val))
                )
            );
        }
        void add_tag(const com_verifone_TLVLite::ConstData_t& tag, const char *txt);
        void add_tag(const com_verifone_TLVLite::ConstData_t& tag, char *txt)
        {
            add_tag(tag,const_cast<const char*>(txt));
        }
        void add_tag(const com_verifone_TLVLite::ConstData_t& tag, const void *data, std::size_t size);
        int send();
        int receive( parser &p, unsigned long timeout=0,
                short (*fcancel)(void *)=NULL,void *param =NULL);
        int check_allive();
        int send_rcv_simple();
        int send_rcv_data(const com_verifone_TLVLite::ConstData_t& data_tag, unsigned char *data, std::size_t * pLen, std::size_t size);

    private:
        unsigned char tlv_buf[TLV_BUF_SIZE];
        com_verifone_TLVLite::SafeBuffer buffer;
        const char *app_name;
        char app_name_crd[APP_NAME_CRD_SIZE];
        unsigned char cmd;

    private:    //Noncopyable
        tlv_libhelper(tlv_libhelper &);
        tlv_libhelper& operator=(tlv_libhelper&);
};

/**
 * @}
 */

/***************************************************************************
 * Module namspace: end
 **************************************************************************/
}
#endif /* TLV_LIBHELPER_HPP_ */

