#ifndef C_CARDAPP_CONFIG_H
#define C_CARDAPP_CONFIG_H

#ifndef __cplusplus
#  error "This file is for C++ only!"
#endif

/*****************************************************************************
 *
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/
/**
 * @file        cCARDAppConfig.h
 *
 * @author      Jacek Olbrys
 *
 * @brief       CardApp service configuration
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C++ Coding
 *              Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/

#include "libipcpacket/ipcpacket.hpp"
#include <stdint.h>
#include <svc.h>
#include <string>
#include <vector>
#include <map>
#include "EMV_Common_Interface.h"
#include "EMV_CT_SerInterface.h"
#include "EMV_CT_Interface.h"

extern "C" {
#include <tlv_parser.h>
}

#include "cBaseConfig.h"
#include <libpml/pml_port.h>

/***************************************************************************
 *    Definitions
 **************************************************************************/
#define AID_SIZE                32+1 // Constant from EMV Kernel header, emvsizes.h
#define AID_REC_SIZE            2*(AID_SIZE+1)
#define DAT_HEADER_SIZE         16
#define AID_FILENAME            "AID.DAT"
#define MAX_AID_REC             20

#define MAX_AVN_LEN ((unsigned char)2)

#define PIN_TYPE_PLAIN_TEXT 0x01
#define PIN_TYPE_ENCIPHERED 0x04

#define ENTRY_TYPE_NORMAL   0x00
#define ENTRY_TYPE_ATOMIC   0x01

#define IPC_VERSION	1

#define LOG_TO_FILE 1
#define LOG_TO_UDP 2
#define LOG_TO_NULL 3

#define MAX_PIN_HTML 12

/***************************************************************************
 * Using
 **************************************************************************/
using namespace com_verifone_ipcpacket;

/***************************************************************************
 * Module namespace: begin
 **************************************************************************/
namespace com_verifone_emv
{

/**
 * @addtogroup CARDApp
 * @{
 */

/**
 * Preprocessor constant definitions
 **/

#ifndef CARD_APP_NAME
#define CARD_APP_NAME "CARDAPP"
#endif

#ifndef GUI_APP_NAME_
#define GUI_APP_NAME_ "GUIAPP"
#endif

/***************************************************************************
 *    Types
 **************************************************************************/
typedef uint8_t appid_t;

typedef struct AID
{
	char szAID1[AID_SIZE+1];
	char szAID2[AID_SIZE+1];
}AID_REC;

const int MAX_PIN_BYPASS_PASSWORD_LEN = 10;
const int MIN_PIN_BYPASS_PASSWORD_LEN = 5;

/***************************************************************************
 *    external C++ - API's
 **************************************************************************/
class CCardAppConfig: public CBaseConfig
{
public:
   CCardAppConfig();
   ~CCardAppConfig();


   typedef enum
   {
      GUI_APP = 1,
      GUI_LOCAL,
      GUI_FLEXI,
   } gui_t;

   /**
    * @brief Command processing break cause
    */
   typedef enum cmd_break_e
   {
      CMD_BREAK_NO_BREAK_E = 0,  /**< No break */
      CMD_BREAK_CANCEL_E,        /**< Cancel command */
      CMD_BREAK_TIMEOUT_E,       /**< POS timeout */
      CMD_BREAK_PIN_BYPASS_E,    /**< PIN bypass only */
      CMD_BREAK_SEQ_ERROR_E,     /**< Another command (not Cancel) */
      CMD_BREAK_GUI_RESP_E,      /**< GuiApp response */
   }  cmd_break_t;

   typedef enum
   {
      PIN_BYPASS_DISABLED = 0,
      PIN_BYPASS_ENABLED = 1,
      PIN_BYPASS_SECURED = 2
   }  pin_bypass_t;

   typedef enum
   {
      CURR_SYMBOL_AUTO = 0,
      CURR_SYMBOL_LEFT = 1,
      CURR_SYMBOL_RIGHT = 2
   }  currency_symbol_loc;

   typedef enum
   {
      PIN_ENTRY_TYPE__INVALID_E              = -1,       /**< Value not set */
      PIN_ENTRY_TYPE_MANDATORY_E             = 0,        /**< PIN Mandatory  */
      PIN_ENTRY_TYPE_OPTIONAL_E             = 1, /**< PIN Optional */
      PIN_ENTRY_TYPE_OPTIONAL_0LN_PIN_ENC_E = 2, /**< PIN Optional 0 length PIN Encryption */
      PIN_ENTRY_TYPE_OPTIONAL_0LN_PIN_E     = 3 /**< 0 Length PIN */
   } pin_entry_type_t;


   // Update config - HAS to be here as tokens are defined in this file...
   int UpdateToken(int tagNum, unsigned char * value, int len);


   void RestoreDefaults();

   inline long GetPINtype(void) {return PINtype;}

   inline void SetPINtype(int pin_type) { PINtype = pin_type;}

   inline long GetPINtimeout(void) {return PINtimeout;}

   inline void SetPINtimeout(long timeout) { PINtimeout = timeout;}

   inline short GetCAPKndx(void) {return CAPK_index;}

   inline void SetCAPKndx(short ndx) {CAPK_index = ndx;}

   inline unsigned long GetAIDOper(void) {return AID_oper;}

   inline void SetAIDOper(unsigned long oper) {AID_oper = oper;}

   inline long GetPINintercharTimeout(void) {return PINintercharTimeout;}

   inline void SetPINintercharTimeout(long timeout) { PINintercharTimeout = timeout;}

   inline long GetPINfirstcharTimeout(void) {return PINfirstcharTimeout;}

   inline void SetPINfirstcharTimeout(long timeout) { PINfirstcharTimeout = timeout;}

   inline long GetPINbeeperTimeout(void) {return PINbeeperTimeout;}

   inline void SetPINbeeperTimeout(long timeout) { PINbeeperTimeout = timeout;}

   inline long GetPINsmallScreenTimeout(void) {return PINsmallScreenTimeout;}

   inline void SetPINsmallScreenTimeout(long timeout) { PINsmallScreenTimeout = timeout;}

   inline long GetApplicationSelectionTimeout(void) {return AppSelTimeout;}

   inline void SetApplicationSelectionTimeout(long timeout) { AppSelTimeout = timeout;}

   inline bool GetNotifyStatus(void) {return send_notify;}

   inline void SetNotifyStatus(bool notify_status) { send_notify = notify_status;}

   inline bool GetWaitForRemovalStatus(void) {return wait_for_removal;}

   inline void SetWaitForRemovalStatus(bool wait_status) { wait_for_removal = wait_status;}

   inline bool GetUseDisplay(void) {return use_display;}

   inline void SetUseDisplay(bool display_status) { use_display = display_status;}

   inline bool GetUseLEDs(void) {return use_leds;}

   inline void SetUseLEDs(bool leds_status) { use_leds = leds_status;}

   inline int GetLastVSSslot(void) {return VSS_slot_last;}

   inline void SetLastVSSslot(int slot) { VSS_slot_last = slot;}

   inline int GetTerminalType(void) {return Term_type;}

   inline void SetTerminalType(int term) { Term_type = term;}

   inline int GetAutoFlag(void) {return auto_flag;}

   inline void SetAutoFlag(int term) { auto_flag = term;}

   inline int GetICChandle(void) {return icc_mon_handle;}

   inline void SetICChandle(int term) { icc_mon_handle = term;}

   inline int GetMSRhandle(void) {return msr_mon_handle;}

   inline void SetMSRhandle(int term) { msr_mon_handle = term;}

   inline int GetInterhandle(void) {return inter_handle;}

   inline void SetInterhandle(int term) { inter_handle = term;}

   inline int GetOfflinePINEntryStyle() { return offlinePINEntryStyle; }

   inline void SetOfflinePINEntryStyle(int style) { offlinePINEntryStyle = style; }

   inline bool GetPINDisplayCurrencySymbol() { return PINDisplayCurrencySymbol; }

   inline void SetPINDisplayCurrencySymbol(bool displaySymb) { PINDisplayCurrencySymbol = displaySymb; }

   void SetPINDisplayCurrencySymbolLoc(unsigned char currSymbol);

   bool PINDisplayCurrencySymbolLeft(bool symbols_used);

   //inline void SetCurrencySymbolLoc(currency_symbol_loc currLoc) { currSymbolLoc = currLoc; }

   inline char GetPINRadixSeparator() { return PINRadixSeparator; }
   inline void SetPINRadixSeparator(char sep) { PINRadixSeparator = sep; }

   inline unsigned char GetMaxPINLen(void) {return max_pin_len;}
   inline void SetMaxPINLen(unsigned char pin_len) {max_pin_len = pin_len;}

   inline unsigned char GetMinPINLen(void) {return min_pin_len;}
   inline void SetMinPINLen(unsigned char pin_len) {min_pin_len = pin_len;}

   inline pin_entry_type_t GetOnlinePINEntryType(void) {return online_pin_entry_type;}

   void SetOnlinePINEntryType(unsigned char entry_type);

   inline bool GetOnlinePINCancelAllowed(void) {return online_pin_cancel_allowed;}
   inline void SetOnlinePINCancelAllowed(bool cancel_allowed) { online_pin_cancel_allowed = cancel_allowed;}

   inline unsigned short GetExtAuthFlag(void) {return ext_auth_flag;}
   inline void SetExtAuthFlag(unsigned short flag) { ext_auth_flag = flag;}

   inline bool GetCDAMode(void) {return cda_mode;}
   inline void SetCDAMode(unsigned char cdamode) { cda_mode = cdamode;}

   inline bool GetSuperEMVWaitForCard() { return super_emv_waits_for_card; }
   inline void SetSuperEMVWaitForCard(bool newVal) { super_emv_waits_for_card = newVal; }

   inline void GetSecondAVN(unsigned char *avn) {memcpy(avn, secondAVN, MAX_AVN_LEN);}
   inline void SetSecondAVN(unsigned char *avn) {memcpy(secondAVN, avn, MAX_AVN_LEN);}

   inline std::string GetCurrentAID(void) {return AID;}
   inline void SetCurrentAID(std::string msg) {AID = msg;}

   inline std::string GetEnterPINMsg(void) {return msg_enter_pin;}
   inline void SetEnterPINMsg(std::string msg) {msg_enter_pin = msg;}

   inline std::string GetIncorrectPINMsg(void) {return msg_incorrect_pin;}
   inline void SetIncorrectPINMsg(std::string msg) {msg_incorrect_pin = msg;}

   inline std::string GetLastPINMsg(void) {return msg_last_pin_try;}
   inline void SetLastPINMsg(std::string msg) {msg_last_pin_try = msg;}


   inline std::string GetCountryReq(void) {return country_req;}
   inline void SetCountryReq(const char *msg) {country_req.assign(msg, strlen(msg));}


   inline std::string GetCountryParams(void) {return country_params;}
   inline void SetCountryParams(const char *msg) {country_params.assign(msg, strlen(msg));}

   void SetContextParams(std::string msg);

   inline std::string GetIPLog(void) {return IP_UDP_Log;}
   inline void SetIPLog(std::string msg) {IP_UDP_Log = msg;}


   inline int GetPinX() { return pin_x; }
   inline void SetPinX(int x) { pin_x = x; }

   inline int GetPinY() { return pin_y; }
   inline void SetPinY(int y) { pin_y = y; }

   bool IsPINBypassEnabled();
   bool IsSecuredPINBypassEnabled();
   inline char GetPINBypassKey() { return pin_bypass_key; }
   bool CheckPINBypassPassword( const char * const password );

   bool IsExternalApplicationSelectionEnabled() { return external_application_selection; }
   void SetExternalApplicationSelection(bool _extAppSel) { external_application_selection = _extAppSel; }


   inline bool GetIsUX100(void) {return isUX100;}
   inline void SetIsUX100(bool ux100) {isUX100 = ux100;}

   inline bool GetShowIncorrectPIN(void) {return showIncorrectPIN;}
   inline void SetShowIncorrectPIN(bool show) {showIncorrectPIN = show;}

   inline bool GetParseCardData(void) {return parseCardData;}
   inline void SetParseCardData(bool parse) {parseCardData = parse;}

   inline int GetInternalLogging() {return internal_logging;}

   inline std::string GetDefaultPINHTML() { return default_PIN_HTML; }
   inline bool HasPINHTML(size_t index) { if (index <= MAX_PIN_HTML) return PIN_HTML[index].size() > 0; else return false; }
   inline std::string GetPINHTML(size_t index) { if (index <= MAX_PIN_HTML) return PIN_HTML[index]; else return std::string(""); }

   inline bool GetUxSwipeOnInsertion() {return uxSwipeOnInsertion;}

   inline int GetMainTaskId() { return myPID; }
   inline void SetMainTaskId(int taskID) {myPID = taskID; }

   inline char GetPINblankChar() { return PINblankChar; }
   inline void SetPINblankChar(char PINchar) {PINblankChar = PINchar; }

   inline char GetPINoption() { return PINoption; }
   inline void SetPINoption(char PINopt) {PINoption = PINopt; }

   inline int GetExpPINdigits() { return expPINdigits; }
   inline void SetExpPINdigits(int PINdigits) {expPINdigits = PINdigits; }

   inline bool GetICCNegotiateSpeed() { return iccNegotiateSpeed; }
   inline void SetICCNegotiateSpeed(bool iccNegotiate) { iccNegotiateSpeed = iccNegotiate; }

   inline bool GetUsePINwarnings() { return pin_warnings; }
   inline void SetUsePINwarnings(bool use_warnings) { pin_warnings = use_warnings; }

   inline bool GetWarmResetAfterICCOff() { return warm_reset_after_icc_off; }
   inline void SetWarmResetAfterICCOff(bool warm) { warm_reset_after_icc_off = warm; }

   inline bool GetProcessBlockedApp() { return process_blocked_app; }
   inline void SetProcessBlockedApp(bool process_bl_ap) { process_blocked_app = process_bl_ap; }

   inline bool GetIgnoreICCevt() { return ignore_icc_events; }
   inline void SetIgnoreICCevt(bool flag_icc_evt) { ignore_icc_events = flag_icc_evt; }

   inline int GetPollTimer_min() { return poll_timer_min; }
   inline void SetPollTimer_min(int timer_val) { poll_timer_min = timer_val; }

   
   inline int GetPollTimer_max() { return poll_timer_max; }
   inline void SetPollTimer_max(int timer_val) { poll_timer_max = timer_val; }

   inline bool GetLogADK() { return log_adk_callback; }
   inline void SetLogADK(bool log) { log_adk_callback = log; }
   inline bool GetUseMSRCounters() { return use_msr_counters; }
   inline long GetUxTimerRemoval() { return timer_removal; }

   inline std::string GetHTML_PIN_OK() { return html_pin_ok; }
   inline void SetHTML_PIN_OK(const std::string & h) { html_pin_ok = h; }
   inline std::string GetHTML_PIN_Retry() { return html_pin_retry; }
   inline void SetHTML_PIN_Retry(const std::string & h) { html_pin_retry = h; }
   inline std::string GetHTML_PIN_Last_Try() { return html_pin_last_try; }
   inline void SetHTML_PIN_Last_Try(const std::string & h) { html_pin_last_try = h; }
   inline std::map<std::string, std::string> GetHTML_PIN_OK_Params() { return values_pin_ok; }
   inline void SetHTML_PIN_OK_Params(const std::string & p) { process_html_parameters(p.c_str(), values_pin_ok); }
   inline std::map<std::string, std::string> GetHTML_PIN_Retry_Params() { return values_pin_retry; }
   inline void SetHTML_PIN_Retry_Params(const std::string & p) { process_html_parameters(p.c_str(), values_pin_retry); }
   inline std::map<std::string, std::string> GetHTML_PIN_Last_Try_Params() { return values_pin_last_try; }
   inline void SetHTML_PIN_Last_Try_Params(const std::string & p) { process_html_parameters(p.c_str(), values_pin_last_try); }


   /**
    * @brief Get application name
    *
    * @return Pointer to null-terminated string
    */
   inline static const char *getName(void) { return CARDAPP_NAME; }

   /**
    * @brief Get application ID
    *
    * @return Application ID
    */
   inline appid_t getID(void) {return m_ID;}

   /**
    * @brief Application interface
    *
    * @return Application interface ID
    */
   inline gui_t GuiInterface(void) {return gui_interface;}

   inline void SetGuiInterface(gui_t gui_int) {gui_interface = gui_int;}

   inline char *DefaultTalker(void) {return default_talker;}


   cmd_break_t checkCmdBreak();
   cmd_break_t checkPipeStatus();
   cmd_break_t getAppSelection(char * aid, size_t &aidBufSize, char * label, size_t &labelBufSize);

   char extraPINmessage[ADD_SCREEN_TEXT+1];
   int pinScreenTitle;

   char cSenderName[21];

   char cNotifyName[21];

   char cEventNotifyName[21];

   AID_REC srAID[MAX_AID_REC];


   std::string cust_language;

   //void clearConfig(); // This clears settable configuration data

protected:
    virtual void vPrintDerivedConfig();
    virtual int iReadDerivedConfig(const char *key, const char *val);

private:
   appid_t m_ID; /**< Application ID */
   bool m_Locked; /**< Application lock flag */
   bool m_CmdLocked;       /**< Command processing flag */
   cmd_break_t m_CmdBreak; /**< Break cause */
   int  m_PosTimeoutTimer; /**< POS timer descriptor */
   gui_t	gui_interface;
   char default_talker[10 + 1];
   long PINtimeout;		//PIN entry timeout in ms
   long PINfirstcharTimeout; // PIN entry first character timeout in ms
   long PINintercharTimeout; // PIN inter-character timeout in ms
   long PINbeeperTimeout; // PIN beeps timeout
   long PINsmallScreenTimeout; // PIN timeout for alternate texts on small screen devices (Vx700 / Vx520 / ...)
   long AppSelTimeout; // Application selection timeout in ms
   bool send_notify;
   bool wait_for_removal;
   bool use_display;
   bool use_leds;
   bool bypass_allowed;
   int PINtype;
   int VSS_slot_last;
   int Term_type;
   int offlinePINEntryStyle;
   bool PINDisplayCurrencySymbol;
   currency_symbol_loc currSymbolLoc;
   char PINRadixSeparator;
   pin_bypass_t pin_bypass_flag;
   char pin_bypass_key;
   char pin_bypass_password[MAX_PIN_BYPASS_PASSWORD_LEN+1];
   unsigned char max_pin_len;
   unsigned char min_pin_len;
   pin_entry_type_t online_pin_entry_type;
   bool online_pin_cancel_allowed;
   unsigned short ext_auth_flag;
   int pin_x, pin_y;
   unsigned char cda_mode;
   unsigned char secondAVN[2];
   bool external_application_selection;
   bool super_emv_waits_for_card;
   int auto_flag;
   std::string msg_enter_pin;
   std::string msg_incorrect_pin;
   std::string msg_last_pin_try;
   std::string AID;
   std::string country_req;
   std::string IP_UDP_Log;
   short CAPK_index;
   unsigned long AID_oper;
   int icc_mon_handle;
   int msr_mon_handle;
   int inter_handle;
   bool isUX100;
   bool showIncorrectPIN;
   std::string country_params;
   bool parseCardData;
   int internal_logging;
   std::vector<std::string> PIN_HTML;
   std::string default_PIN_HTML;
   int myPID;
   bool uxSwipeOnInsertion;
   char PINblankChar;
   char PINoption;
   int expPINdigits;
   bool iccNegotiateSpeed;
   bool pin_warnings;
   bool warm_reset_after_icc_off;
   bool process_blocked_app;
   bool ignore_icc_events;
   int poll_timer_min;
   int poll_timer_max;
   bool log_adk_callback;
   bool use_msr_counters;
   long timer_removal;
   std::string html_pin_ok;
   std::string html_pin_retry;
   std::string html_pin_last_try;
   std::map<std::string, std::string> values_pin_ok;
   std::map<std::string, std::string> values_pin_retry;
   std::map<std::string, std::string> values_pin_last_try;
   
   void process_html_parameters(const char * val, std::map<std::string, std::string> & values);
};

/**
 * @}
 */


}
/***************************************************************************
 * Module namespace: end
**************************************************************************/

#endif

