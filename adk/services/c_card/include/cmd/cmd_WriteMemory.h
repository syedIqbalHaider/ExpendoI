#ifndef WRITEMEMORYCARD_H
#define WRITEMEMORYCARD_H

#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 * 
 * Copyright (C) 2007 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file       cmd_WriteMemory.h
 *
 * @author     Kamil Pawlowski
 * 
 *
 * @remarks    This file should be compliant with Verifone EMEA R&D C++ Coding   
 *             Standard 1.0.x   
 */

/***************************************************************************
 * Includes
 **************************************************************************/

#include "stdint.h"
#include "libipcpacket/ipcpacket.hpp"
#include "tlv_parser.h"
#include "cCmd.h"


/***************************************************************************
 * Using
 **************************************************************************/

/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
namespace com_verifone_cmd
{

/**
 * @addtogroup Cmd
 * @{ 
 */

/*************************************************************************** 
 * Preprocessor constant definitions
 **************************************************************************/

/*************************************************************************** 
 * Macro definitions
 **************************************************************************/

/*************************************************************************** 
 * Data type definitions
 **************************************************************************/

/*************************************************************************** 
 * Exported variable declarations
 **************************************************************************/

/*************************************************************************** 
 * Exported function declarations
 **************************************************************************/

/*************************************************************************** 
 * Exported class declarations
 **************************************************************************/

/**
 * @brief        Write Memory Card class
 *
 */
class CWriteMemory : public CCmd
{
public:
   /** 
     * @brief 
     *       This function implements handling of the reset card reader command
     *  
     * @param[in]   *pack  Pointer to incomming package
     * 
     * @return      
     *              - ESSUCCESS       MAC Generated successful
     *              - ECANCEL         Command canceled
     *              - EPOSTIMEOTMISS  POS Timeout parameter miss
     *              - EHOSTIDMISS     Host Id parameter miss
     *              - EMESSFORMACMISS Message for MAC parameter miss
     *              - EHANDCOMMERROR  Error while MAC calculation
     *              - ERESPONSESEND   Response send error
     *              
     */ 
   virtual c_card_error_t handleCommand(com_verifone_ipcpacket::IPCPacket *pack);


   /**
    * @brief   This function check if Cancel Process Command comes and send response. 
    *  
    * @param[in]   *pack       Pointer to incomming package
    * 
    * @return      
    *             - ESUCCESS      Continue command handling
    *             - ECANCEL       Cancel process command comes
    *             - EPOSTIMEOUT   Timeout
    *             - ESEQERR       Sequence error
    *             - ERESPONSESEND Error while sending response
    */
   virtual c_card_error_t isCanceled(com_verifone_ipcpacket::IPCPacket &response);

protected:
private:
};


/**
 * @}
 */

/***************************************************************************
 * Module namspace: end
 **************************************************************************/
}

#endif /* WRITEMEMORYCARD_H */


