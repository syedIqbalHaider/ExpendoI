#ifndef TLV_PARSER_H
#define TLV_PARSER_H

/*****************************************************************************
 *
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/
/**
 * @file        tlv_parser.h
 *
 * @author      Jacek Olbrys
 *
 * @brief       TLV Parser
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C++ Coding
 *              Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/
#define __STDINT_NO_EXPORTS
#include <stdint.h>
#include "CARDApp.h"

const char * respCodeString(uint8_t respCode);

/***************************************************************************
 * Using
 **************************************************************************/

/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
//namespace com_verifone_parser
//{

/**
 * @addtogroup parser
 * @{
 */

/***************************************************************************
 * Preprocessor constant definitions
 **************************************************************************/

#define CODE_UNLOCK  (0)
#define CODE_LOCK    (1)

#define CODE_ACTION  (0x80)

#define RESP_CODE_SIZE   ( 1)

#define RESP_CODE_SUCCESS                    (  0)
#define RESP_CODE_FAILED                     (  1)
#define RESP_CODE_TIMEOUT                    (  2)
#define RESP_CODE_TRAN_DECLINED              (  4)
#define RESP_CODE_TRAN_APPROVED              (  5)
#define RESP_CODE_TRAN_ONLINE                (  6)
#define RESP_CODE_CARD_BLOCKED				 (  7)	
#define RESP_CODE_PIN_CANCEL                 ( 20)
#define RESP_CODE_PIN_BYPASS                 ( 21)
#define RESP_CODE_CMD_SEQ_ERROR              ( 26)
#define RESP_CODE_RETRY_LIMIT                ( 31)
#define RESP_CODE_INVALID_REQ                ( 35)
#define RESP_CODE_API_REQ                    ( 36)
#define RESP_CODE_INVALID_CMD                ( 37)
#define RESP_CODE_INVALID_LOCAL_KEY_IDX      ( 40)
#define RESP_CODE_UPDATE_KEY_FAILED          ( 41)
#define RESP_CODE_TRANSPORT_KEYS_CHANGED     ( 42)
#define RESP_CODE_KEY_UPDATE_LIMIT           ( 43)
#define RESP_CODE_INVALID_KEY                ( 45)
#define RESP_CODE_PIN_BLOCKED                ( 90)
#define RESP_CODE_PIN_GET_CHALLENGE_ERROR    ( 91)
#define RESP_CODE_PIN_ENCIPHER_ERROR         ( 92)
#define RESP_CODE_PIN_VERIFY_FAILED          ( 93)
#define RESP_CODE_PIN_MISSING_PIN_BLOCK_DATA ( 94)
#define RESP_CODE_INVALID_KEY_LENGTH         ( 95)
#define RESP_CODE_MISSING_TMK_OR_DUKPT_KEY   ( 96)
#define RESP_CODE_MISSING_TPK_OR_TAK         ( 97)
#define RESP_CODE_MISSING_STAN_OR_PAN        ( 98)
#define RESP_CODE_CARD_INSERTED              ( 99)
#define RESP_CODE_CARD_REMOVED               (100)

#define RESP_CODE_MEM_CARD_UNCHANGED         (101)
#define RESP_CODE_MEM_CARD_CHANGED           (102)

#define RESP_CODE_CARD_SWIPED				 (103)
#define RESP_CODE_CHIP_ERROR				 (104)


#define RESP_CODE_CMD_BUSY                   (249)
#define RESP_CODE_INVALID_APP_ID             (250)
#define RESP_CODE_POS_INVALID_FIELD_LEN      (251)
#define RESP_CODE_POS_MSG_LEN                (252)
#define RESP_CODE_UNKNOWN_CMD                (253)
#define RESP_CODE_INVAL                      (254)

/** new resp code */
#define RESP_CODE_CMD_CANCEL                 ( 22)
/** new resp code */
#define RESP_CODE_SIZE_ERROR                 ( 23)
/** new resp code */
#define RESP_CODE_MAC_VER_FAILED             ( 24)
/** new resp code */
#define RESP_CODE_MSG_SIZE_TO_LONG           ( 25)
/** new resp code */
#define RESP_CODE_ICC_FAIL                   ( 27)


// Flags for Wait for Card
#define WAIT_FOR_CARD_WARM_RESET            1
#define WAIT_FOR_CARD_SKIP_KERNEL_REINIT    2 // V/OS only!


struct respCodeInfo
{
   respCodeInfo(const char *_respCodeString, uint8_t _respCode)
               : m_respCode(_respCode), m_respCodeString(_respCodeString)
   {

   }
   uint8_t     m_respCode;
   const char *m_respCodeString;
};


#define ADD_SCREEN_TEXT          (16)
#define DISPLAY_TEXT             (2*ADD_SCREEN_TEXT) // should be able to accomodate 2 full lines of text
#define CODE_SIZE                ( 1)
#define ACTION_CODE_SIZE         ( 1)
#define COMMAND_SIZE             ( 1)
#define DATA_ALGORITHM_SIZE      ( 1)
#define PAN_SIZE                 (10)
#define STAN_SIZE                ( 6)
#define TRANS_AMOUNT_SIZE        ( 7)
#define TRANS_CURR_CODE_SIZE     ( 2)
#define ONLINE_PIN_CIPHER_BLOCK_SIZE    ( 8)
#define KSN_SIZE                 (10)
#define POS_TIMEOUT_SIZE         ( 1)
#define EXT_POS_TIMEOUT_SIZE     ( 4)
#define HOST_ID_SIZE             ( 1)
#define KEY_TYPE_SIZE            ( 1)
#define LOCK_SIZE                ( 1)
#define PIN_ENTRY_TYPE_SIZE      ( 1)
#define PIN_BLOCK_FORMAT_SIZE    ( 1)
#define KEY_MANAGEMENT_SIZE      ( 1)
#define PIN_ALGORITHM_SIZE       ( 1)
#define PIN_TRY_FLAG_SIZE        ( 1)
#define TRANS_CURR_EXPONENT_SIZE ( 1)
#define MAX_PIN_LENGTH_SIZE      ( 1)
#define MIN_PIN_LENGTH_SIZE      ( 1)
#define PIN_ENTRY_TIMEOUT_SIZE   ( 2)
#define PIN_CANCEL_SIZE          ( 1)
#define FILENAME_SIZE   (32)

#define MACRO_PAN_SIZE           ( 6)

#define POS_TIMEOUT_TS         (2)
#define ACTION_CODE_TS         (2)
#define HOST_KEY_FILE_TS       (3)
#define CODE_TS                (2)
#define HOST_ID_TS             (2)
#define KEY_TYPE_TS            (2)
#define KEY_DATA_TS            (2)
#define LOCK_TS                (3)
#define PAN_TS                 (2)
#define STAN_TS                (2)
#define TRANS_AMOUNT_TS        (2)
#define TRANS_CURR_CODE_TS     (2)
#define TRANS_CURR_EXP_TS      (2)
#define PIN_ENTRY_TYPE_TS      (2)
#define PIN_BLOCK_FORMAT_TS    (3)
#define PIN_TRY_FLAG_TS        (2)
#define ADD_SCREEN_TEXT_TS     (3)
#define ADD_SCREEN_TEXT_2_TS   (3)
#define MIN_PIN_LENGTH_TS      (3)
#define MAX_PIN_LENGTH_TS      (3)
#define PIN_ENTRY_TIMEOUT_TS   (3)
#define PIN_FIRSTCHAR_TIMEOUT_TS (3)
#define PIN_INTERCHAR_TIMEOUT_TS (3)
#define PIN_CANCEL_TS          (3)
#define MESS_FOR_MAC_TS        (2)
#define MAC_TS                 (2)
#define MESS_FOR_ENC_TS        (2)
#define PIN_ENTRY_TYPE_TS      (2)
#define KEY_MANAGEMENT_TS      (3)
#define PIN_ALGORITHM_TS       (3)
#define DATA_ALGORITHM_TS      (3)
#define KSN_TS                 (3)
#define TRANS_CAT_EXP_TS       (2)
#define ONLINE_PIN_CIPHER_TS   (2)
#define PLAIN_TXT_PIN_BLOCK_TS (3)
#define SCRIPT_NAME_TS         (3)
#define SCRIPT_NAME_TS         (3)
#define PIN_CANCEL_TS          (3)
#define ONLINE_PIN_CIPHER_TS   (2)
#define MESS_FOR_DEC_TS        (3)
#define FLAGS_TS               (3)
#define MSG_ENTER_PIN_TS       (3)
#define MSG_AMOUNT_TS          (3)
#define MSG_FONT_FILE_TS       (3)
#define MONITOR_PERIOD_TS      (3)
#define PIN_OFFLINE_TYPE_TS    (3)
#define PIN_OFFLINE_ENTRY_TYPE_TS   (3)
#define PIN_MAX_LEN_TS         (3)
#define PIN_MIN_LEN_TS         (3)
#define PIN_ONLINE_ENTRY_TYPE_TS    (3)
#define PIN_ONLINE_CANCEL_TS   (3)
#define EXTERNAL_APP_SELECTION_TS   (3)
#define PIN_ONLINE_EXTRA_MSG_TS     (3)
#define PARSE_EXTRA_TAGS_TS         (3)
#define SWIPE_STATUS_TS        (3)
#define HTML_EXTRA_TS			(3)
#define CAPK_INDEX_TS           (2)
#define APP_AID_PRIORITY_TS 1



#define IN_TLV_TS               (3)
#define OUT_TLV_LIST_TS         (3)
#define CHANGE_STATUS_TS        (3)
#define PROC_STATUS_TS          (3)
#define OUT_TLV_TS              (3)
#define OPERATIONS_TS           (3)
#define SELECT_TS               (3)
#define PROC_FLAG_TS            (3)
#define CONF_RECORD_TS          (3)
#define AUTH_RESULT_TS          (3)
#define ATR_TS                  (3)
#define PIN_TIMEOUT_TS          (3)
#define PIN_HTML_TS				(3)
#define EVENT_TS                (3)
#define CARD_BLACKLISTED_TS     (3)
#define VSS_NAME_TS             (3)
#define VSS_SLOT_TS             (3)
#define VSS_MACRO_TS            (3)
#define VSS_IN_TS               (3)
#define VSS_OUT_TS              (3)
#define VSS_RESULT_TS           (3)
#define MAG_TRACK_TS            (3)
#define CARDTYPE_TS             (3)
#define PIN_RESULT_TS           (3)
#define LED_DURATION_TS         (3)
#define LED_REPEATS_TS          (3)
#define RADIX_SEPARATOR_TS      (3)
#define PIN_STYLE_TS            (3)
#define PIN_CURR_SYMBOL_TS      (3)
#define PIN_CURR_SYMBOL_LOC_TS  (3)
#define PIN_BEEPER_TIMEOUT_TS   (3)
#define IN_APDU_TS              (3)
#define OUT_APDU_TS             (3)
#define SCRIPT_RESULT_TS        (3)
#define EXTRA_DATA_TS           (3)
#define KERNEL_CHECKSUM_TS      (3)
#define CUST_LANG_TS            (3)
#define HTML_PIN_TS             (3)
#define HTML_PIN_PARAMS_TS      (3)

#define APP_LABEL_TS 1
#define APP_PREF_NAME_TS 2
#define APP_AID_TS 2


#define MEM_CARD_ADDRESS_TS     (3)
#define MEM_CARD_BYTE_COUNT_TS  (3)
#define MEM_CARD_DATA_TS        (3)
#define MEM_CARD_PIN_TS     (3)

#define SHOW_INCORRECT_PIN_TS   (3)

#define CANCEL_COM                (90)
#define START_WAITING_COM         (91)
#define MONITOR_ICC_COM           (92)
#define UNSOLICITED_EVENT_COM     (93)
#define SEND_APDU_COM             (94)
#define SET_GET_TLV_COM           (95)
#define SET_PIN_PARAMS_COM        (96)
#define VERIFY_PIN_COM            (97)
#define SET_MESSAGE_COM           (98)
#define L1_BEGIN_TRANSACTION_COM  (111)
#define L1_COMPLETE_TRN_COM       (112)
#define L2_SELECT_READ_COM        (121)
#define L2_TRM_VERIFY_COM         (122)
#define L2_FIRST_AC_COM           (123)
#define L2_COMPLETE_COM           (124)
#define L3_SELECT_COM             (131)
#define L3_GPO_COM                (132)
#define L3_READ_DATA_COM          (133)
#define L3_CARDHOLDER_VERIFY_COM  (134)
#define L3_TRM_COM                (135)
#define L3_DATA_AUTH_COM          (136)
#define L3_FIRST_AC_COM           (137)
#define L3_EXT_AUTH_COM           (138)
#define L3_SCRIPT_PROCESSING_COM  (139)
#define L3_SECOND_AC_COM          (130)
#define UPDATE_EMV_CONFIG_COM     (150)
#define INSTALL_VSS_COM           (160)
#define EXECUTE_VSS_MACRO_COM     (161)
#define SUPER_EMV_COM             (200)
#define RESET_EXTERNAL_READER_COM (54)      //this command is compatible with SCAPP
#define MEM_READ_I2C_CARD         (170)
#define MEM_WRITE_I2C_CARD        (171)
#define MEM_READ_MEMORY_CARD      (172)
#define MEM_UPDATE_MEMORY_CARD    (173)
#define MEM_WRITE_MEMORY_CARD     (174)
#define EXTERNAL_APP_SEL_EVT      (99)
#define LEDS_CONTROL_COM          (100)
#define RESET_EMV_CONFIG          (140)


#define CMD_RPC_CRYPTO_ON         (55)
#define CMD_RPC_CRYPTO_OFF        (56)


#define REASON_ONLINE_UNKNOWN               (99)
#define REASON_ONLINE_FORCED_BY_ACCEPTOR    ( 6) // see Apacs for details
#define REASON_ONLINE_FORCED_BY_ISSUER      ( 9)
#define REASON_ONLINE_OVER_FLOOR            (10)
#define REASON_ONLINE_RANDOM_SELECTION      ( 3)
#define REASON_ONLINE_FORCED_BY_IC          ( 5)
#define REASON_ONLINE_FORCED_BY_PED         ( 8)

/***************************************************************************
 * Macro definitions
 **************************************************************************/

/***************************************************************************
 * Data type definitions
 **************************************************************************/

/***************************************************************************
 * Exported variable declarations
 **************************************************************************/

extern uint8_t arbyTagActionCode         [ACTION_CODE_TS];
extern uint8_t arbyTagAddScreenText      [ADD_SCREEN_TEXT_TS];
extern uint8_t arbyTagAddScreenText2     [ADD_SCREEN_TEXT_2_TS];
extern uint8_t arbyTagCode               [CODE_TS];
extern uint8_t arbyTagDataAlgorithm      [DATA_ALGORITHM_TS];
extern uint8_t arbyTagHostId             [HOST_ID_TS];
extern uint8_t arbyTagKeyData            [KEY_DATA_TS];
extern uint8_t arbyTagKeyManagement      [KEY_MANAGEMENT_TS];
extern uint8_t arbyTagKeyType            [KEY_TYPE_TS];
extern uint8_t arbyTagKSN                [KSN_TS];
extern uint8_t arbyTagLock               [LOCK_TS];
extern uint8_t arbyTagMAC                [MAC_TS];
extern uint8_t arbyTagMaxPINLength       [MAX_PIN_LENGTH_TS];
extern uint8_t arbyTagMessForDec         [MESS_FOR_DEC_TS];
extern uint8_t arbyTagMessForEnc         [MESS_FOR_ENC_TS];
extern uint8_t arbyTagMessageForMAC      [MESS_FOR_MAC_TS];
extern uint8_t arbyTagMinPINLength       [MIN_PIN_LENGTH_TS];
extern uint8_t arbyTagOnlinePINCipher    [ONLINE_PIN_CIPHER_TS];
extern uint8_t arbyTagPINhtml			 [PIN_HTML_TS];
extern uint8_t arbyTagPan                [PAN_TS];
extern uint8_t arbyTagPinAlgorithm       [PIN_ALGORITHM_TS];
extern uint8_t arbyTagPinBlockFormat     [PIN_BLOCK_FORMAT_TS];
extern uint8_t arbyTagPinCancel          [PIN_CANCEL_TS];
extern uint8_t arbyTagPinEntryTimeout    [PIN_ENTRY_TIMEOUT_TS];
extern uint8_t arbyTagPinEntryType       [PIN_ENTRY_TYPE_TS];
extern uint8_t arbyTagPinTryFlag         [PIN_TRY_FLAG_TS];
extern uint8_t arbyTagPlainTxtPinBlock   [PLAIN_TXT_PIN_BLOCK_TS];
extern uint8_t arbyTagPosTimeout         [POS_TIMEOUT_TS];
extern uint8_t arbyTagScriptName         [SCRIPT_NAME_TS];
extern uint8_t arbyTagStan               [STAN_TS];
extern uint8_t arbyTagTransAmount        [TRANS_AMOUNT_TS];
extern uint8_t arbyTagTransCatExp        [TRANS_CAT_EXP_TS];
extern uint8_t arbyTagTransCurrCode      [TRANS_CURR_CODE_TS];
extern uint8_t arbyTagTransCurrExp       [TRANS_CURR_EXP_TS];
extern uint8_t arbyTagFlags              [FLAGS_TS];
extern uint8_t arbyTagMessagePIN        [MSG_ENTER_PIN_TS];
extern uint8_t arbyTagMessageAmount     [MSG_AMOUNT_TS];
extern uint8_t arbyTagFontFile                  [MSG_FONT_FILE_TS];
extern uint8_t arbyTagMonitorPeriod   [MONITOR_PERIOD_TS];
extern uint8_t arbyTagInAPDU            [IN_APDU_TS];
extern uint8_t arbyTagOutAPDU           [OUT_APDU_TS];
extern uint8_t arbyTagInAPDU_tmpl            [IN_APDU_TS];
extern uint8_t arbyTagOutAPDU_tmpl           [OUT_APDU_TS];




extern uint8_t arbyTagInTLV         [IN_TLV_TS];
extern uint8_t arbyTagOutTLVlist        [OUT_TLV_LIST_TS];
extern uint8_t arbyTagChangeStatus      [CHANGE_STATUS_TS];
extern uint8_t arbyTagProcStatus        [PROC_STATUS_TS];
extern uint8_t arbyTagOutTLV            [OUT_TLV_TS];
extern uint8_t arbyTagOperations        [OPERATIONS_TS];
extern uint8_t arbyTagSelect            [SELECT_TS];
extern uint8_t arbyTagProcFlag          [PROC_FLAG_TS];
extern uint8_t arbyTagConfRecord        [CONF_RECORD_TS];
extern uint8_t arbyTagAuthResult        [AUTH_RESULT_TS];
extern uint8_t arbyTagATR               [ATR_TS];
extern uint8_t arbyTagPINtimeout        [PIN_TIMEOUT_TS];
extern uint8_t arbyTagEvent             [EVENT_TS];
extern uint8_t arbyTagCardBlacklisted   [CARD_BLACKLISTED_TS];
extern uint8_t arbyTagPINfirstcharTimeout [PIN_FIRSTCHAR_TIMEOUT_TS];
extern uint8_t arbyTagPINintercharTimeout [PIN_INTERCHAR_TIMEOUT_TS];
extern uint8_t arbyTagVSSname           [VSS_NAME_TS];
extern uint8_t arbyTagVSSslot           [VSS_SLOT_TS];
extern uint8_t arbyTagVSSmacro          [VSS_MACRO_TS];
extern uint8_t arbyTagVSSIn             [VSS_IN_TS];
extern uint8_t arbyTagVSSOut            [VSS_OUT_TS];
extern uint8_t arbyTagVSSResult         [VSS_RESULT_TS];
extern uint8_t arbyTagTrack1            [MAG_TRACK_TS];
extern uint8_t arbyTagTrack2            [MAG_TRACK_TS];
extern uint8_t arbyTagTrack3            [MAG_TRACK_TS];
extern uint8_t arbyTagCardType          [CARDTYPE_TS];
extern uint8_t arbyTagPINResult         [PIN_RESULT_TS];
extern uint8_t arbyTagLEDDuration       [LED_DURATION_TS];
extern uint8_t arbyTagLEDRepeats        [LED_REPEATS_TS];

extern uint8_t arbyTagRadixSeparator    [RADIX_SEPARATOR_TS];
extern uint8_t arbyTagPINStyle          [PIN_STYLE_TS];
extern uint8_t arbyTagPINCurrSymbol     [PIN_CURR_SYMBOL_TS];
extern uint8_t arbyTagPINCurrSymbolLoc  [PIN_CURR_SYMBOL_TS];
extern uint8_t arbyTagPINBeeperTimeout  [PIN_BEEPER_TIMEOUT_TS];
extern uint8_t arbyTagPINOfflinetype    [PIN_OFFLINE_TYPE_TS];
extern uint8_t arbyTagPinOfflineEntryType       [PIN_OFFLINE_ENTRY_TYPE_TS];
extern uint8_t arbyTagPinMaxLen     [PIN_MAX_LEN_TS];
extern uint8_t arbyTagPinMinLen     [PIN_MIN_LEN_TS];
extern uint8_t arbyTagPinOnlineEntryType        [PIN_ONLINE_ENTRY_TYPE_TS];
extern uint8_t arbyTagPinOnlineCancel       [PIN_ONLINE_CANCEL_TS];
extern uint8_t arbyTagExternalAppSelection  [EXTERNAL_APP_SELECTION_TS];
extern uint8_t arbyTagPinOnlineExtraMsg     [PIN_ONLINE_EXTRA_MSG_TS];
extern uint8_t arbyTagParseExtraTags       [PARSE_EXTRA_TAGS_TS];
extern uint8_t arbyTagCAPKindex             [CAPK_INDEX_TS];

extern uint8_t arbyTagHTMLextra			[HTML_EXTRA_TS];


extern uint8_t arbyTagKernelChecksum    [KERNEL_CHECKSUM_TS];
extern uint8_t arbyTagTACdefault        [EXTRA_DATA_TS];
extern uint8_t arbyTagTACdenial         [EXTRA_DATA_TS];
extern uint8_t arbyTagTAConline         [EXTRA_DATA_TS];
extern uint8_t arbyTagROSThreshold      [EXTRA_DATA_TS];
extern uint8_t arbyTagROSTargetPercent  [EXTRA_DATA_TS];
extern uint8_t arbyTagROSMaxPercent     [EXTRA_DATA_TS];
extern uint8_t arbyTagContextData       [EXTRA_DATA_TS];


extern uint8_t arbyTagScriptRes71       [SCRIPT_RESULT_TS];
extern uint8_t arbyTagScriptRes72       [SCRIPT_RESULT_TS];
extern uint8_t arbyTagSwipeStatus       [SWIPE_STATUS_TS];

extern uint8_t arbyTagMemoryCardAddress [MEM_CARD_ADDRESS_TS];
extern uint8_t arbyTagMemoryCardByteCount   [MEM_CARD_BYTE_COUNT_TS];
extern uint8_t arbyTagMemoryCardData    [MEM_CARD_DATA_TS];
extern uint8_t arbyTagMemoryCardPIN [MEM_CARD_PIN_TS];

extern uint8_t arbyTagShowIncorrectPIN  [SHOW_INCORRECT_PIN_TS];

extern uint8_t arbyAppLabel[APP_LABEL_TS];
extern uint8_t arbyAppPrefName[APP_PREF_NAME_TS];
extern uint8_t arbyAppAID[APP_AID_TS];
extern uint8_t arbyAppPriority[APP_AID_PRIORITY_TS];


extern uint8_t arbyTagCustLang[CUST_LANG_TS];

extern uint8_t arbyTagHTML_PIN_OK[HTML_PIN_TS];
extern uint8_t arbyTagHTML_PIN_Incorrect[HTML_PIN_TS];
extern uint8_t arbyTagHTML_PIN_LastTry[HTML_PIN_TS];
extern uint8_t arbyTagHTML_PIN_OK_Params[HTML_PIN_PARAMS_TS];
extern uint8_t arbyTagHTML_PIN_Incorrect_Params[HTML_PIN_PARAMS_TS];
extern uint8_t arbyTagHTML_PIN_LastTry_Params[HTML_PIN_PARAMS_TS];


struct SCA_TXN;

extern SCA_TXN g_stTxn;

/***************************************************************************
 * Exported function declarations
 **************************************************************************/



/***************************************************************************
 * Exported class declarations
 **************************************************************************/


/**
 * @}
 */

/***************************************************************************
 * Module namspace: end
 **************************************************************************/
#endif // TLV_PARSER_H


