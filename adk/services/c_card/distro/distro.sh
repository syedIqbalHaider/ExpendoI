#!/bin/bash
set -e

rm -rf /tmp/distro
mkdir /tmp/distro

cp ram/* /tmp/distro

cp -ra CONTROL /tmp/distro
cp -ra flash /tmp/distro

cp ../Platform_vos_gcc/debug/Out/cardapp /tmp/distro/flash
cp ../Platform_vos_gcc/debug/Out/crdtest /tmp/distro/lib


mkdir /tmp/distro/lib
#cp -afr /usr/lib/arm-unknown-linux-gnueabi/arm-unknown-linux-gnueabi/lib/libstdc++.so* /tmp/distro/lib
cp -afr /usr/lib/arm-unknown-linux-gnueabi/usr/lib/libstdc++.so* /tmp/distro/lib
cp -afr /usr/lib/arm-unknown-linux-gnueabi/usr/lib/libboost_system-mt.so* /tmp/distro/lib
cp -afr /usr/lib/arm-unknown-linux-gnueabi/usr/lib/libboost_regex-mt.so* /tmp/distro/lib
cp -afr /usr/lib/arm-unknown-linux-gnueabi/usr/lib/libboost_filesystem-mt.so* /tmp/distro/lib
cp -afr /usr/lib/arm-unknown-linux-gnueabi/usr/lib/libpng*.so* /tmp/distro/lib

cp -afr lib/* /tmp/distro/lib

pushd /tmp/distro

mxpackage --devcert cardapp.sh ./*

popd

mv /tmp/distro/*.tar .

