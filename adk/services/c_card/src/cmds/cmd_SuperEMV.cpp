#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 *
 * Copyright (C) 2007 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file       cmd_Level3.cpp
 *
 *
 * @remarks    This file should be compliant with Verifone EMEA R&D C++ Coding
 *             Standard 1.0.x
 */
#include <tlv-lite/ConstValue.hpp>

#include "cCARDAppConfig.h"
#include "cEMV.h"
#include "cTransaction.h"
#include "tlv_parser.h"

#include <liblog/logsys.h>

#include "cmd/cmd_SuperEMV.h"

#include <libpml/pml_abstracted_api.h>
#include <libpml/pml_port.h>

#include <tlv-lite/ConstData.h>
#include <tlv-lite/ValueConverter.hpp>
#include <tlv-lite/ConstValue.hpp>
#include <libipcpacket/ipcpacket.hpp>

#include "EMVtools.h"


#ifdef VFI_PLATFORM_VOS
void EMVlog(char const * msg);
#endif
#ifdef VFI_PLATFORM_VERIXEVO
void EMVlog(char const * /*unused*/)
{
}
#endif


/***************************************************************************
 * Using
 **************************************************************************/
using namespace com_verifone_emv;
using namespace com_verifone_ipcpacket;


extern CCardAppConfig   g_CardAppConfig;
extern CEMV			g_CEMV;
extern CEMVtrns		g_CEMVtrns;


#ifdef VFI_PLATFORM_VOS
extern cEMVcollection  g_EMVcollection;
extern logger			g_logger;
#endif


/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
namespace com_verifone_cmd
{


void VerbouseStatus(uint16_t st_main, uint16_t st_detail, char *text)
{
	int text_len = 0;

	if(st_main & CARD_APP_STATUS_M_CONTINUE)
	{
		strcpy(text+text_len, "CONTINUE ");
		text_len+= strlen(text);
	}
	if(st_main & CARD_APP_STATUS_M_STOP)
	{
		strcpy(text+text_len, "STOP ");
		text_len+= strlen(text);

	}
	if(st_main & CARD_APP_STATUS_M_WARNING)
	{
		strcpy(text+text_len, "WARNING ");
		text_len+= strlen(text);

	}
	if(st_main & CARD_APP_STATUS_M_ERROR)
	{
		strcpy(text+text_len, "ERROR ");
		text_len+= strlen(text);

	}
	if(st_main & CARD_APP_STATUS_M_PIN)
	{
		strcpy(text+text_len, "ONLINE PIN ");
		text_len+= strlen(text);

	}
	if(st_main & CARD_APP_STATUS_M_SIGNATURE)
	{
		strcpy(text+text_len, "SIGNATURE ");
		text_len+= strlen(text);

	}
	if(st_detail & CARD_APP_STATUS_D_CARD_BLOCKED)
	{
		strcpy(text+text_len, "CARD BLOCKED ");
		text_len+= strlen(text);

	}
	if(st_detail & CARD_APP_STATUS_D_APPL_BLOCKED)
	{
		strcpy(text+text_len, "APPLICATION BLOCKED ");
		text_len+= strlen(text);

	}
	if(st_detail & CARD_APP_STATUS_D_CANCELLED)
	{
		strcpy(text+text_len, "CANCELLED ");
		text_len+= strlen(text);

	}
	if(st_detail & CARD_APP_STATUS_D_CHIP_ERROR)
	{
		strcpy(text+text_len, "CHIP ERROR ");
		text_len+= strlen(text);

	}
	if(st_detail & CARD_APP_STATUS_D_EMPTY)
	{
		strcpy(text+text_len, "CANDIDATE LIST EMPTY ");
		text_len+= strlen(text);

	}
	if(st_detail & CARD_APP_STATUS_D_FALLBACK)
	{
		strcpy(text+text_len, "FALLBACK ALLOWED ");
		text_len+= strlen(text);

	}
	if(st_detail & CARD_APP_STATUS_D_GENERAL)
	{
		strcpy(text+text_len, "GENERAL ");
		text_len+= strlen(text);

	}
	if(st_detail & CARD_APP_STATUS_D_NO_FALLBACK)
	{
		strcpy(text+text_len, "FALLBACK NOT ALLOWED ");
		text_len+= strlen(text);

	}
	if(st_detail & CARD_APP_STATUS_D_OK)
	{
		strcpy(text+text_len, "OK ");
		text_len+= strlen(text);

	}
	if(st_detail & CARD_APP_STATUS_D_PARAMETER)
	{
		strcpy(text+text_len, "INCORRECT PARAMETERS ");
		text_len+= strlen(text);

	}
	if(st_detail & CARD_APP_STATUS_D_REMOVED)
	{
		strcpy(text+text_len, "CARD REMOVED ");
		text_len+= strlen(text);

	}


}


std::string VerbouseOpers(int opers)
{
	std::string status;
	if(opers & CARD_APP_OPER_CONFIG)
		status.append("CONFIG ");
	if(opers & CARD_APP_OPER_DATA_AUTH)
		status.append("DATA AUTH ");
	if(opers & CARD_APP_OPER_EXT_AUTH)
		status.append("EXT AUTH ");
	if(opers & CARD_APP_OPER_FIRST_AC)
		status.append("FIRST AC ");
	if(opers & CARD_APP_OPER_GPO)
		status.append("GPO ");
	if(opers & CARD_APP_OPER_PIN_VERIFY)
		status.append("PIN VERIFY ");
	if(opers & CARD_APP_OPER_READ)
		status.append("OPER READ ");
	if(opers & CARD_APP_OPER_RESTRICT)
		status.append("RESTRICTIONS ");
	if(opers & CARD_APP_OPER_SCRIPT)
		status.append("SCRIPT PROC. ");
	if(opers & CARD_APP_OPER_SECOND_AC)
		status.append("SECON AC ");
	if(opers & CARD_APP_OPER_SELECT)
		status.append("SELECTION ");
	if(opers == CARD_APP_OPER_TLV)
		status.append("TLV ONLY ");
	if(opers & CARD_APP_OPER_TRM)
		status.append("TRM ");
	if(opers & CARD_APP_OPER_VERIFY)
		status.append("VERIFY ");

	EMVlog("REQUESTED OPERATIONS");
	EMVlog((char *)status.c_str());

	return status;
}

   CSuperEMVCmd::CSuperEMVCmd(void)
   {
        opers_reg = 0xFFFF;
   }

   CSuperEMVCmd::CSuperEMVCmd(int opers)
   {
        opers_reg = opers;
   }


   c_card_error_t CSuperEMVCmd::isCanceled(IPCPacket &response)
   {
      CCardAppConfig::cmd_break_e ret = g_CardAppConfig.checkCmdBreak(); //check break status
      uint8_t errorCode;   //error code send to sender application
      c_card_error_t returnCode;  //function return code

      switch( ret )
      {
      case CCardAppConfig::CMD_BREAK_NO_BREAK_E: //no break
         return ESUCCESS;
      case CCardAppConfig::CMD_BREAK_CANCEL_E:   //command canceled
         errorCode  = RESP_CODE_CMD_CANCEL;
         returnCode = ECANCEL;
         break;
      case CCardAppConfig::CMD_BREAK_TIMEOUT_E:  //timeout
         errorCode  = RESP_CODE_TIMEOUT;
         returnCode = EPOSTIMEOUT;
         break;
      case CCardAppConfig::CMD_BREAK_SEQ_ERROR_E:
         errorCode  = RESP_CODE_CMD_SEQ_ERROR;
         returnCode = ESEQERR;
         break;
      default:
         return ESUCCESS;
      }


      //prepare response message
      response.addTag(com_verifone_TLVLite::ConstData(arbyTagCode, CODE_TS), com_verifone_TLVLite::MakeBERValue(errorCode));

      if( IPCPACKET_SUCCESS != response.send() )
      {
         return ERESPONSESEND;
      }

      return returnCode;
   }

  #define PARSE_BUFFER_TOT 512
  #define PARSE_BUFFER_WORK	100

  c_card_error_t CSuperEMVCmd::handleCommand(IPCPacket *pack)
  {
      using namespace com_verifone_TLVLite;
      IPCPacket  response; //response message initialization
      uint8_t   errorCode  = RESP_CODE_SUCCESS;   //error code which will be send to sender application
      c_card_error_t   returnCode = ESUCCESS;   //function return code
      int opers;
      int blacklisted;
      int iRet;
      uint8_t   OutTLVdata_buf[CARD_APP_MAX_TLV];
      ConstData value(ConstData::getInvalid());
      bool is_inserted = isCardInserted();

	  #ifdef VFI_PLATFORM_VOS
	  long long time_start_l = getTimeStampMs();
	  long long time_end_l = time_start_l;
	  #endif

      //setting IN/OUT buffers
      dlog_msg("*****SUPPER COMMAND*****");

      response.setCmdCode(pack->getCmdCode());
      response.setAppName(pack->getAppName());

	  
	  g_CEMVtrns.cmd_status = RESP_CODE_INVAL;


      g_CEMVtrns.InTLV_list_len = 0;
      g_CEMVtrns.OutTLV_list_len = 0;
      g_CEMVtrns.OutTLVdata = OutTLVdata_buf;
      g_CEMVtrns.OutTLV_data_len = CARD_APP_MAX_TLV;
      g_CEMVtrns.monitor_flags &= 0xFD;


      if( IPCPACKET_SUCCESS != pack->getTag(ConstData(arbyTagInTLV, IN_TLV_TS), value) )
      {
          dlog_msg("In TLV list missing");
      }
      else
      {
          dlog_msg("In TLV list len: %d", value.getSize());
          g_CEMVtrns.InTLVlist = const_cast<unsigned char *>(value.getByteBuffer()); // fixme!!!!
          g_CEMVtrns.InTLV_list_len = value.getSize();
      }
      if( IPCPACKET_SUCCESS != pack->getTag(ConstData(arbyTagOutTLVlist, OUT_TLV_LIST_TS), value) )
      {
          dlog_msg("Out TLV list missing");
      }
      else
      {
          dlog_msg("Out TLV list len: %d", value.getSize());
          g_CEMVtrns.OutTLVlist = const_cast<unsigned char *>(value.getByteBuffer()); // fixme!!!
          g_CEMVtrns.OutTLV_list_len = value.getSize();
      }

      if( IPCPACKET_SUCCESS != pack->getTag(ConstData(arbyTagOperations, OPERATIONS_TS), value) )
      {
          dlog_msg("Operations register missing using the one from init: %X", opers_reg);
          opers = opers_reg;
      }
      else
      {
          ValueConverter<int> valueConv(value);
          opers = valueConv.getValue(0);
          dlog_msg("OPERS: %X", opers);
      }
      dlog_msg("OPERS: %s", VerbouseOpers(opers).c_str());

      if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagAuthResult, AUTH_RESULT_TS), value) )
      {
          ValueConverter<int> valueConv(value);
          int auth_res = valueConv.getValue(0);
          g_CEMV.SetTermDecision(auth_res);
          dlog_msg("Setting Authorisation Result to: %d START(114=FORCED_ONLINE,115=FORCED_DECLINE,0=NO_DECISION), CONTINUE(1=AUTH OK; 2=DECLINE; 3=FAILED_TO_CONNECT; 152=REFFERAL", auth_res);
      }
      if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagPINtimeout, PIN_TIMEOUT_TS), value ) )
      {
          ValueConverter<long> valueConv(value);
          long PINtimeout = valueConv.getValue(60000);
          g_CardAppConfig.SetPINtimeout(PINtimeout);
          dlog_msg("Setting PIN timeout to: %dms" , PINtimeout);
      }
      if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagPINhtml, PIN_HTML_TS), value ) )
      {
          std::string tmp_data;
          tmp_data.assign((const char *)value.getByteBuffer(), value.getSize());
          g_CEMV.SetPINhtml(tmp_data);
          dlog_msg("Setting specific PIN entry screen: '%s'" , g_CEMV.GetPINhtml().c_str());
      }
      else
      {
          dlog_msg("Using default PIN entry screen");
          g_CEMV.SetPINhtml("");
      }
      if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagPINfirstcharTimeout, PIN_FIRSTCHAR_TIMEOUT_TS), value ) )
      {
          ValueConverter<long> valueConv(value);
          long PINtimeout = valueConv.getValue(0);
          g_CardAppConfig.SetPINfirstcharTimeout(PINtimeout);
          dlog_msg("Setting PIN firstchar timeout to: %dms" , PINtimeout);
      }
      if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagPINintercharTimeout, PIN_INTERCHAR_TIMEOUT_TS), value ) )
      {
          ValueConverter<long> valueConv(value);
          long PINtimeout = valueConv.getValue(0);
          g_CardAppConfig.SetPINintercharTimeout(PINtimeout);
          dlog_msg("Setting PIN interchar timeout to: %dms" , PINtimeout);
      }
      if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagPINBeeperTimeout, PIN_BEEPER_TIMEOUT_TS), value ) )
      {
          ValueConverter<long> valueConv(value);
          long PINbeeperTimeout = valueConv.getValue(0);
          g_CardAppConfig.SetPINbeeperTimeout(PINbeeperTimeout);
          dlog_msg("Setting PIN beeper timeout to: %dms" , PINbeeperTimeout);
      }

      if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagCardBlacklisted, CARD_BLACKLISTED_TS), value ) )
      {
          ValueConverter<int> valueConv(value);
          int blacklisted = valueConv.getValue(0);
          g_CEMV.setCardBlackListed(blacklisted);
          dlog_msg("Setting Card Blacklisted flag to: %d", blacklisted);
      }
      if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagRadixSeparator, RADIX_SEPARATOR_TS), value ) )
      {
          ValueConverter<char> valueConv(value);
          char radix = valueConv.getValue(0);
          g_CardAppConfig.SetPINRadixSeparator(radix);
          dlog_msg("Setting PIN radix separator to '%c' (%02Xh)", radix, radix);
      }
      if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagPINStyle, PIN_STYLE_TS), value ) )
      {
          ValueConverter<uint8_t> valueConv(value);
          uint8_t PINentry_style = valueConv.getValue(0);
          g_CardAppConfig.SetOfflinePINEntryStyle(PINentry_style);
          dlog_msg("Setting PIN entry style to %d", PINentry_style);
      }
      else
      {
          uint8_t PINentry_style = 0;
          g_CardAppConfig.SetOfflinePINEntryStyle(PINentry_style);
      }
      if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagPINCurrSymbol, PIN_CURR_SYMBOL_TS), value) )
      {
          ValueConverter<uint8_t> valueConv(value);
          uint8_t PINSymbDisp = valueConv.getValue(0);
          g_CardAppConfig.SetPINDisplayCurrencySymbol(PINSymbDisp == 1);
          dlog_msg("Setting PIN entry symbol displaying to %d", PINSymbDisp);
      }
      if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagPINCurrSymbolLoc, PIN_CURR_SYMBOL_LOC_TS), value) )
      {
          ValueConverter<uint8_t> valueConv(value);
          uint8_t PINSymbLoc = valueConv.getValue(0);
          g_CardAppConfig.SetPINDisplayCurrencySymbolLoc(PINSymbLoc);
          dlog_msg("Setting PIN entry symbol location to %d", PINSymbLoc);
      }

      if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagPinMaxLen, PIN_MAX_LEN_TS), value ) )
      {
          ValueConverter<uint8_t> valueConv(value);
          uint8_t PINMaxLen = valueConv.getValue(12);
          g_CardAppConfig.SetMaxPINLen(PINMaxLen);
          dlog_msg("Setting PIN max len to to: %d" , PINMaxLen);
      }
      if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagPinMinLen, PIN_MIN_LEN_TS), value ) )
      {
          ValueConverter<uint8_t> valueConv(value);
          uint8_t PINMinLen = valueConv.getValue(3);
          g_CardAppConfig.SetMinPINLen(PINMinLen);
          dlog_msg("Setting PIN min len to: %d" , PINMinLen);
      }
      if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagPinOnlineEntryType, PIN_ONLINE_ENTRY_TYPE_TS), value) )
      {
          ValueConverter<uint8_t> valueConv(value);
          uint8_t OnlinePINEntryType = valueConv.getValue(0);
          g_CardAppConfig.SetOnlinePINEntryType(OnlinePINEntryType);
          dlog_msg("Setting online PIN entry type to: %d" , OnlinePINEntryType);
      }
      if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagPinOnlineCancel, PIN_ONLINE_CANCEL_TS), value) )
      {
          ValueConverter<uint8_t> valueConv(value);
          uint8_t OnlinePINCancelAllowed = valueConv.getValue(1);
          g_CardAppConfig.SetOnlinePINCancelAllowed(OnlinePINCancelAllowed == 0);
          dlog_msg("Setting online PIN cancellation to: %d" , OnlinePINCancelAllowed == 0);
      }
      if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagExternalAppSelection, EXTERNAL_APP_SELECTION_TS), value) )
      {
          ValueConverter<uint8_t> valueConv(value);
          uint8_t ExternalAppSelection = valueConv.getValue(1);
          g_CardAppConfig.SetExternalApplicationSelection(ExternalAppSelection == 0);
          dlog_msg("Setting External Application Selection to: %d" , ExternalAppSelection == 0);
      }
      if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagPinOnlineExtraMsg, PIN_ONLINE_EXTRA_MSG_TS), value) )
      {
          int copyLen = sizeof(g_CardAppConfig.extraPINmessage)-1;
          if (value.getSize() < copyLen) copyLen = value.getSize();
          memcpy(g_CardAppConfig.extraPINmessage, value.getByteBuffer(), copyLen);
          g_CardAppConfig.extraPINmessage[copyLen] = 0;
          dlog_msg("Setting extra PIN message to: '%s'" , g_CardAppConfig.extraPINmessage);
      }
      #ifdef VFI_PLATFORM_VOS
      if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagHTMLextra, HTML_EXTRA_TS), value) )
      {
        dlog_msg("HTML data:");
        char curr_sep;
        int str_len = value.getSize();
        int i=0;
        int j=0;
        int j_max;
        char label[PARSE_BUFFER_WORK];
        char to_print[PARSE_BUFFER_WORK];
        char get_buffer[PARSE_BUFFER_TOT];
        char *to_copy;

        memcpy(get_buffer, value.getByteBuffer(), str_len);
        to_copy = label;
        while((i < str_len) && (j < PARSE_BUFFER_WORK))
        {
            switch(get_buffer[i])
            {
            case '=':
              to_copy[j]=0;
              to_copy = to_print;
              j = 0;
              break;
            case ';':
              to_copy[j]=0;
              to_copy = label;
              j = 0;
              g_EMVcollection.HTML_labels[label]=to_print;
              break;
            default:
              to_copy[j++] = get_buffer[i];
              break;
            }
          i++;
        }

        for(std::map<std::string, std::string>::iterator it = g_EMVcollection.HTML_labels.begin(); it!=g_EMVcollection.HTML_labels.end(); ++it)
        {
          dlog_msg("[%s]=%s", (it->first).c_str(), (it->second).c_str());
        }
      }
      else
      {
          g_EMVcollection.HTML_labels.clear();
      }
      #endif
      if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagHTML_PIN_OK, HTML_PIN_TS), value) )
      {
          std::string tmp_data;
          tmp_data.assign((const char *)value.getByteBuffer(), value.getSize());
          g_CardAppConfig.SetHTML_PIN_OK(tmp_data);
          dlog_msg("Setting HTML displayed when PIN is OK to: '%s'" , g_CardAppConfig.GetHTML_PIN_OK().c_str());
      }
      if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagHTML_PIN_OK_Params, HTML_PIN_PARAMS_TS), value) )
      {
          std::string tmp_data;
          tmp_data.assign((const char *)value.getByteBuffer(), value.getSize());
          g_CardAppConfig.SetHTML_PIN_OK_Params(tmp_data);
          dlog_msg("Setting params for HTML PIN OK to: '%s'" , tmp_data.c_str());
      }

      if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagHTML_PIN_Incorrect, HTML_PIN_TS), value) )
      {
          std::string tmp_data;
          tmp_data.assign((const char *)value.getByteBuffer(), value.getSize());
          g_CardAppConfig.SetHTML_PIN_Retry(tmp_data);
          dlog_msg("Setting HTML displayed when PIN is incorrect to: '%s'" , g_CardAppConfig.GetHTML_PIN_Retry().c_str());
      }
      if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagHTML_PIN_Incorrect_Params, HTML_PIN_PARAMS_TS), value) )
      {
          std::string tmp_data;
          tmp_data.assign((const char *)value.getByteBuffer(), value.getSize());
          g_CardAppConfig.SetHTML_PIN_Retry_Params(tmp_data);
          dlog_msg("Setting params for HTML PIN Retry to: '%s'" , tmp_data.c_str());
      }

      if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagHTML_PIN_LastTry, HTML_PIN_TS), value) )
      {
          std::string tmp_data;
          tmp_data.assign((const char *)value.getByteBuffer(), value.getSize());
          g_CardAppConfig.SetHTML_PIN_Last_Try(tmp_data);
          dlog_msg("Setting HTML displayed on PIN last try to: '%s'" , g_CardAppConfig.GetHTML_PIN_Last_Try().c_str());
      }
      if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagHTML_PIN_LastTry_Params, HTML_PIN_PARAMS_TS), value) )
      {
          std::string tmp_data;
          tmp_data.assign((const char *)value.getByteBuffer(), value.getSize());
          g_CardAppConfig.SetHTML_PIN_Last_Try_Params(tmp_data);
          dlog_msg("Setting params for HTML PIN Last Try to: '%s'" , tmp_data.c_str());
      }

      if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagParseExtraTags, PARSE_EXTRA_TAGS_TS), value) )
      {
          ValueConverter<uint8_t> valueConv(value);
          uint8_t parseAllTags = valueConv.getValue(1);
          g_CardAppConfig.SetParseCardData(parseAllTags == 0);
          dlog_msg("Setting Parse Card Data to: %d" , parseAllTags == 0);
      }


      if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagContextData, EXTRA_DATA_TS), value) )
      {
          std::string tmp_data;
          tmp_data.assign((const char *)value.getByteBuffer(), value.getSize());
          g_CardAppConfig.SetContextParams(tmp_data);
          dlog_msg("Setting context specific parameters: '%s'" , g_CardAppConfig.GetCountryParams().c_str());
      }

      if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagCustLang, CUST_LANG_TS), value) )
      {
          if (value.getSize())
          {
              g_CardAppConfig.cust_language.assign(static_cast<const char *>(value.getBuffer()), value.getSize());
              dlog_msg("Setting language prefix: '%s'" , g_CardAppConfig.cust_language.c_str());
          }
      }

      if ( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagFlags, FLAGS_TS), value) )
      {
          ValueConverter<uint32_t> valueFlags(value);
          uint32_t flags = valueFlags.getValue(0);
          dlog_msg("Flags: %d (%d)", flags, flags & 0x01);
          g_CardAppConfig.SetAIDOper(flags & 0x01); // if b0 set, CAPK will be deleted, otherwise it will be read
      }
      else
      {
          g_CardAppConfig.SetAIDOper(0); // CAPK Read operation by default
      }

      //uint8_t arbyTagAID[]	= {0x4F};
      //uint8_t arbyTagCAPKindex[] = {0x8F};
      if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyAppAID, APP_AID_TS), value) )
      {
          char cAID[AID_SIZE*2+1];
          std::string sAID;
          com_verifone_pml::svcHex2Dsp(static_cast<const char *>(value.getBuffer()), value.getSize(), cAID, sizeof(cAID));
          cAID[value.getSize()*2] = 0;
          sAID.assign(cAID);
          dlog_msg("Current AID: %s", sAID.c_str());
          g_CardAppConfig.SetCurrentAID(sAID);
          if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagCAPKindex, CAPK_INDEX_TS), value) )
          {
              ValueConverter<char> valueConv(value);
              uint8_t CAPK_ind = valueConv.getValue(0);
              dlog_msg("Current CAPK Index %Xh", CAPK_ind);
              g_CardAppConfig.SetCAPKndx(CAPK_ind);
          }
          else
          {
              g_CardAppConfig.SetCAPKndx(0);
          }
      }
      else
      {
          g_CardAppConfig.SetCurrentAID("");
      }

      if(errorCode == RESP_CODE_SUCCESS)
      {
          iRet = g_CEMVtrns.EMVtransaction(opers, &g_CEMV);
          dlog_msg("EMVTransaction RET: %X", iRet);

          switch(iRet)
          {
            case CARD_APP_RET_ONLINE:
              errorCode = RESP_CODE_TRAN_ONLINE;
              break;
            case CARD_APP_RET_DECLINED:
              errorCode = RESP_CODE_TRAN_DECLINED; 
              g_CEMV.SetStatusMain(CARD_APP_STATUS_M_STOP);
              break;
            case CARD_APP_RET_APPROVED:
              errorCode = RESP_CODE_TRAN_APPROVED;
              g_CEMV.SetStatusMain(CARD_APP_STATUS_M_STOP);
              break;
            case CARD_APP_RET_OK:
              errorCode = RESP_CODE_SUCCESS;
              break;
            case CARD_APP_RET_REMOVED:
              errorCode = RESP_CODE_CARD_REMOVED; 
              g_CEMV.SetStatusMain(CARD_APP_STATUS_M_STOP);
              g_CEMV.SetStatusDetail(CARD_APP_STATUS_D_REMOVED);
              break;
            case CARD_APP_RET_ICC_FAIL:
              errorCode = RESP_CODE_ICC_FAIL;
              g_CEMV.SetStatusMain(CARD_APP_STATUS_M_STOP);
              g_CEMV.SetStatusDetail(CARD_APP_STATUS_D_CHIP_ERROR);
              break;
            case CARD_APP_RET_ORDER:
              errorCode = RESP_CODE_CMD_SEQ_ERROR;
              break;
            case CARD_APP_RET_CANCELED:
              errorCode = RESP_CODE_PIN_CANCEL;
              break;
			case CARD_APP_RET_CARD_BLOCKED:
              errorCode = RESP_CODE_CARD_BLOCKED;
              break;
            default:
              dlog_alert("Unknown result");
              //errorCode = RESP_CODE_FAILED; break;
              break;
          };

          #ifdef LOGSYS_DEBUG
          char text[100];
          text[0] = 0;
          VerbouseStatus(g_CEMV.GetStatusMain(), g_CEMV.GetStatusDetail(), text);
          dlog_msg("***TRANSACTION STATUS: %X:%X = %s", g_CEMV.GetStatusMain(), g_CEMV.GetStatusDetail(), text);

          #ifdef VFI_PLATFORM_VOS
          g_logger.Log("***TRANSACTION STATUS: %X:%X = %s", g_CEMV.GetStatusMain(), g_CEMV.GetStatusDetail(), text);
          #endif
          #endif

		  if(g_CEMVtrns.cmd_status != RESP_CODE_INVAL)
		  {
				dlog_msg("Changing response code to: %d", g_CEMVtrns.cmd_status);
				errorCode = g_CEMVtrns.cmd_status;
		  }
          response.addTag(ConstData(arbyTagCode, CODE_TS), MakeBERValue(errorCode), true);
          if(g_CEMVtrns.TLV_data_len > 0)
          {
              dlog_msg("Adding %d bytes of read TLV objects", g_CEMVtrns.TLV_data_len);
              response.addTag(ConstData(arbyTagOutTLV, OUT_TLV_TS), ConstData(g_CEMVtrns.OutTLVdata, g_CEMVtrns.TLV_data_len));
          }

          uint32_t tr_status;
          tr_status = (static_cast<uint32_t>(g_CEMV.GetStatusMain()) << 16);
          tr_status |= static_cast<uint32_t>(g_CEMV.GetStatusDetail());
          response.addTag(ConstData(arbyTagProcStatus, PROC_STATUS_TS), MakeBERValue(tr_status));
          /*
          uint16_t status_tmp = g_CEMV.GetStatusMain();
          unsigned char tr_status[sizeof(uint16_t)*2];
          uint16_t status_tmp = g_CEMV.GetStatusMain();
          memcpy(tr_status, &status_tmp, sizeof(uint16_t));
          status_tmp = g_CEMV.GetStatusDetail();
          memcpy(tr_status+sizeof(uint16_t), &status_tmp, sizeof(uint16_t));
          response.addTag(ConstData(arbyTagProcStatus, PROC_STATUS_TS), (const char * const*)tr_status, sizeof(uint16_t)*2);
          */

         

          dlog_msg("Supper command success");
      }

	 #ifdef VFI_PLATFORM_VOS
	 time_end_l = getTimeStampMs();
	 g_logger.Log("SUPER COMMAND execution end: %08.08ldms Sending response...", time_end_l - time_start_l);
	 dlog_msg("SUPER COMMAND execution end: %08.08ldms", time_end_l - time_start_l);
	 #endif

	  

      if( IPCPACKET_SUCCESS != response.send() )
      {
/*          while(1)
          {
              com_verifone_pml::error_tone();
              SVC_WAIT(300);
          }
*/        g_logger.Log("ERROR - unable to send IPC message !!!");
          return ERESPONSESEND;
      }

	  #ifdef VFI_PLATFORM_VOS
	  g_logger.Log("SUPER COMMAND Response sent");
	  #endif


      #ifdef VFI_PLATFORM_VOS
      if(is_inserted && isCardRemoved())
      {
          dlog_msg("Signaling extra card removal event (cmd_SuperEMV)...");
          //ICCoutEvent();
          iRet = (c_card_error_t)SendMonitorMsg(RESP_CODE_CARD_REMOVED);
      }
      #endif

      if(g_CEMV.GetStatusMain() & CARD_APP_STATUS_M_STOP)
      {
          dlog_msg("Waiting for card removal...");
          if(g_CardAppConfig.GetWaitForRemovalStatus())
          {
              g_CEMVtrns.monitor_flags |= CARD_APP_MON_NOBREAK;
          }
      }

      return ESUCCESS;
  }

}



