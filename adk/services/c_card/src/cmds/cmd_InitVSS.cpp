#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 *
 * Copyright (C) 2007 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file       cmd_InitVSS.cpp
 *
 *
 * @remarks    This file should be compliant with Verifone EMEA R&D C++ Coding
 *             Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/
#include <svc.h>
#ifdef VFI_PLATFORM_VERIXEVO
#include <svc_sec.h>
#endif
#ifdef VFI_PLATFORM_VOS
#include <svcsec.h>
#include <fcntl.h>
#endif

#include "cCARDAppConfig.h"
#include "cEMV.h"
#include "cTransaction.h"
#include "tlv_parser.h"
#include "liblog/logsys.h"

#include "cmd/cmd_InitVSS.h"
#include "libpml/pml_abstracted_api.h"
#include "libpml/pml_port.h"

#include <tlv-lite/ConstData.h>
#include <tlv-lite/ValueConverter.hpp>
#include <tlv-lite/ConstValue.hpp>
#include <libipcpacket/ipcpacket.hpp>



/***************************************************************************
 * Using
 **************************************************************************/
using namespace com_verifone_ipcpacket;
using namespace com_verifone_emv;

extern CCardAppConfig   g_CardAppConfig;
extern CEMV			g_CEMV;
extern CEMVtrns		g_CEMVtrns;


/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
namespace com_verifone_cmd
{


   c_card_error_t CInitVSSCmd::isCanceled(IPCPacket &response)
   {
      CCardAppConfig::cmd_break_e ret = g_CardAppConfig.checkCmdBreak(); //check break status
      uint8_t errorCode;   //error code send to sender application
      c_card_error_t returnCode;  //function return code

      switch( ret )
      {
      case CCardAppConfig::CMD_BREAK_NO_BREAK_E: //no break
         return ESUCCESS;
      case CCardAppConfig::CMD_BREAK_CANCEL_E:   //command canceled
         errorCode  = RESP_CODE_CMD_CANCEL;
         returnCode = ECANCEL;
         break;
      case CCardAppConfig::CMD_BREAK_TIMEOUT_E:  //timeout
         errorCode  = RESP_CODE_TIMEOUT;
         returnCode = EPOSTIMEOUT;
         break;
      case CCardAppConfig::CMD_BREAK_SEQ_ERROR_E:
         errorCode  = RESP_CODE_CMD_SEQ_ERROR;
         returnCode = ESEQERR;
         break;
      default:
         return ESUCCESS;
      }


      //prepare response message
      response.addTag(com_verifone_TLVLite::ConstData(arbyTagCode, CODE_TS), com_verifone_TLVLite::MakeBERValue(errorCode));

      if( IPCPACKET_SUCCESS != response.send() )
      {
         return ERESPONSESEND;
      }

      return returnCode;
   }

  c_card_error_t CInitVSSCmd::handleCommand(IPCPacket *pack)
  {
      using namespace com_verifone_TLVLite;
      IPCPacket  response; //response message initialization
      uint8_t   errorCode  = RESP_CODE_SUCCESS;   //error code which will be send to sender application
      c_card_error_t   returnCode = ESUCCESS;   //function return code
      int iRet = 0;
      int crypto_task = -1;
      int crypto_handle = -1;
      int VSS_In_len = 0;
      uint8_t *script_name = NULL;
      uint8_t * VSS_In = NULL;
      int VSS_macro = 0;
      uint8_t inBuffer[8+1];
      int slot_no = -1;
      ConstData value(ConstData::getInvalid());

      c_card_error_t ret;

      dlog_msg("*****INIT_VSS_COM*****");

      response.setCmdCode(pack->getCmdCode());
      response.setAppName(pack->getAppName());

      //checking if dev/crypto is opened
      if( IPCPACKET_SUCCESS != pack->getTag(ConstData(arbyTagVSSname, VSS_NAME_TS), value) )
      {
          dlog_msg("VSS script name missing - closing /dev/crypto and exiting");
          crypto_handle = get_owner ("/dev/crypto", &crypto_task);
          if(crypto_task == get_task_id())
          {
              close(crypto_handle);
              crypto_task = -2;
          }
      }
      else
      {
          script_name = const_cast<unsigned char *>(value.getByteBuffer());
      }
      if( IPCPACKET_SUCCESS != pack->getTag(ConstData(arbyTagVSSslot, VSS_SLOT_TS), value))
      {
          slot_no = g_CardAppConfig.GetLastVSSslot();
          dlog_msg("Using last VSS slot number - %d", slot_no);
      }
      else
      {
          ValueConverter<int> valueConv(value);
          slot_no = valueConv.getValue(0);
      }
      if( IPCPACKET_SUCCESS != pack->getTag(ConstData(arbyTagVSSmacro, VSS_MACRO_TS), value))
      {
          dlog_msg("VSS script macro number missing - INIT ONLY");
      }
      else
      {
          ValueConverter<int> valueConv(value);
          VSS_macro = valueConv.getValue(0);
          if( IPCPACKET_SUCCESS != pack->getTag(ConstData(arbyTagVSSIn, VSS_IN_TS), value))
          {
              dlog_msg("No Input data for macro execution");
          }
          else
          {
              VSS_In = const_cast<unsigned char *>(value.getByteBuffer());
              VSS_In_len = value.getSize();
          }

      }

      if(crypto_task != -2)
      {
          crypto_handle = get_owner ("/dev/crypto", &crypto_task);
          if((crypto_handle < 0) || (crypto_task <= 0))
          {
              crypto_handle = open("/dev/crypto", 0);
              crypto_handle = get_owner ("/dev/crypto", &crypto_task);
          }
          dlog_msg("Crypto opened by %d, handle: %d", crypto_task, crypto_handle);
      }

      if(crypto_task != get_task_id())
      {
          dlog_msg("Crypto device is opened by another task!!!");
          errorCode = RESP_CODE_INVALID_APP_ID;
      }
      else
      {
          iRet = EERROR;
          if((slot_no >= 0) && (iRet = iPS_GetScriptStatus(slot_no, inBuffer)) != ESUCCESS)
          {
              dlog_msg("Installing script: %s", script_name);
              //JACEK - difference between verix and vos!
              #ifdef VFI_PLATFORM_VERIXEVO
              if((iRet = iPS_InstallScript(reinterpret_cast<char *>(script_name)))!= ESUCCESS)
              #endif
              #ifdef VFI_PLATFORM_VOS
              if((iRet = iPS_InstallScript())!= ESUCCESS)
              #endif
              {
                  dlog_msg("Can not install script: %s - ret: %d", script_name, iRet);
                  errorCode = RESP_CODE_FAILED;
              }
              else
              {
                  iRet = iPS_GetScriptStatus(slot_no, inBuffer);
              }
          }
          if( iRet == ESUCCESS)
          {
              dlog_msg("Script %s installed in slot %d", inBuffer, slot_no);
              response.addTag(ConstData(arbyTagVSSslot, VSS_SLOT_TS), MakeBERValue(slot_no));
              g_CardAppConfig.SetLastVSSslot(slot_no);
              errorCode = RESP_CODE_SUCCESS;
              if(VSS_macro != 0)
              {
                  dlog_msg("Executing macro: %d", VSS_macro);
                  uint8_t *VSS_Out = new uint8_t[256];
                  uint16_t VSS_Out_len = 0;
                  iRet = iPS_ExecuteScript(slot_no, VSS_macro, VSS_In_len, VSS_In, 256, &VSS_Out_len, VSS_Out);
                  dlog_msg("Script: %d, macro: 0x%X, in data size: %d  out data size: %d RET: %d", slot_no, VSS_macro, VSS_In_len, VSS_Out_len, iRet);
                  if(VSS_Out_len > 0)
                  {
                      response.addTag(ConstData(arbyTagVSSOut, VSS_OUT_TS), ConstData(VSS_Out, VSS_Out_len));
                      response.addTag(ConstData(arbyTagVSSResult, VSS_RESULT_TS), MakeBERValue(iRet));
                  }
                  delete [] VSS_Out;
                  SVC_WAIT(0);
              }
              else
              {
                  response.addTag(ConstData(arbyTagVSSResult, VSS_RESULT_TS), MakeBERValue(iRet));
              }
          }
          else
          {
              //we must find slot number
              int i;
              for(i = 0; i < 8; i++)
              {
                  if((iRet = iPS_GetScriptStatus(i, inBuffer)) == ESUCCESS)
                  {
                      dlog_msg("Script %s found in slot %d", inBuffer, i);
                      g_CardAppConfig.SetLastVSSslot(i);
                      response.addTag(ConstData(arbyTagVSSslot, VSS_SLOT_TS),  MakeBERValue(i));
                      response.addTag(ConstData(arbyTagVSSResult, VSS_RESULT_TS), MakeBERValue(iRet));
                      i = 8;
                  }
              }
          }
      }
      response.addTag(ConstData(arbyTagCode, CODE_TS), MakeBERValue(errorCode));

      if( IPCPACKET_SUCCESS != response.send() )
      {
          return ERESPONSESEND;
      }

      return ESUCCESS;
  }

}


