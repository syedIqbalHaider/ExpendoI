#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 *
 * Copyright (C) 2007 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/


/***************************************************************************
 * Includes
 **************************************************************************/
#include "cmd/cmd_UpdateMemory.h"
#include "cCARDAppConfig.h"
#include "cEMV.h"
#include "cTransaction.h"
#include "tlv_parser.h"

#include <cardslot.h>

#include <tlv-lite/ConstData.h>
#include <tlv-lite/ValueConverter.hpp>
#include <tlv-lite/ConstValue.hpp>


/***************************************************************************
 * Using
 **************************************************************************/
using namespace com_verifone_ipcpacket;
using namespace com_verifone_emv;

extern CCardAppConfig   g_CardAppConfig;
extern CEMV             g_CEMV;
extern CEMVtrns         g_CEMVtrns;


/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
namespace com_verifone_cmd
{

/**
 * @addtogroup Cmd
 * @{
 */

   c_card_error_t CUpdateMemory::isCanceled(IPCPacket &response)
   {
      CCardAppConfig::cmd_break_e ret = g_CardAppConfig.checkCmdBreak(); //check break status
      uint8_t errorCode;   //error code send to sender application
      c_card_error_t returnCode;  //function return code

      switch( ret )
      {
      case CCardAppConfig::CMD_BREAK_NO_BREAK_E: //no break
         return ESUCCESS;
      case CCardAppConfig::CMD_BREAK_CANCEL_E:   //command canceled
         errorCode  = RESP_CODE_CMD_CANCEL;
         returnCode = ECANCEL;
         break;
      case CCardAppConfig::CMD_BREAK_TIMEOUT_E:  //timeout
         errorCode  = RESP_CODE_TIMEOUT;
         returnCode = EPOSTIMEOUT;
         break;
      case CCardAppConfig::CMD_BREAK_SEQ_ERROR_E:
         errorCode  = RESP_CODE_CMD_SEQ_ERROR;
         returnCode = ESEQERR;
         break;
      default:
         return ESUCCESS;
      }


      //prepare response message
      response.addTag(com_verifone_TLVLite::ConstData(arbyTagCode, CODE_TS), com_verifone_TLVLite::MakeBERValue(errorCode));

      if( IPCPACKET_SUCCESS != response.send() )
      {
         return ERESPONSESEND;
      }

      return returnCode;
   }


    c_card_error_t CUpdateMemory::handleCommand(IPCPacket *pack)
    {
        using namespace com_verifone_TLVLite;
        uint8_t errorCode  = RESP_CODE_SUCCESS;   //error code which will be send to sender application
        c_card_error_t returnCode = ESUCCESS;   //function return code
        IPCPacket response; //response message initialization

        response.setCmdCode(pack->getCmdCode());
        response.setAppName(pack->getAppName());

        uint8_t *pCardPIN = 0;
        ConstData value(ConstData::getInvalid());

        dlog_msg("UpdateMemory: handle");

        if( IPCPACKET_SUCCESS != pack->getTag(ConstData(arbyTagMemoryCardPIN, MEM_CARD_PIN_TS), value))
        {
            dlog_error("PIN is missing");
            errorCode = RESP_CODE_FAILED;
        }
        else
        {
            if (value.getSize() == 3) pCardPIN = const_cast<uint8_t *>(static_cast<const uint8_t *>(value.getBuffer()));
            else
            {
                dlog_error("Invalid PIN length %d", value.getSize());
                errorCode = RESP_CODE_FAILED;
            }
        }

        if(  isCanceled(response) != ESUCCESS ) //check break status, if this command was cancelled
        {
            return ECANCEL;
        }
        if (Get_Card_State(g_CEMV.ICC_reader) != CARD_PRESENT)
        {
            dlog_alert("No card!");
            errorCode = RESP_CODE_CARD_REMOVED;
        }
        if (g_CEMVtrns.icc_card_type != CARD_APP_CARDTYPE_EMV_SYNC)
        {
            dlog_alert("No Memory card is inserted, aborting!");
            errorCode = RESP_CODE_MEM_CARD_UNCHANGED;
        }


        if (errorCode == RESP_CODE_SUCCESS)
        {
            // Now, handle the command. 
            int32_t res = g_CEMVtrns.sle442.update_card(pCardPIN);
            switch (res)
            {
                case com_verifone_memory_card::MEM_OK: 
                    // Success
                    break;
                case com_verifone_memory_card::MEM_MEMORY_UNCHANGED:
                    errorCode = RESP_CODE_MEM_CARD_UNCHANGED; 
                    break;
                case com_verifone_memory_card::MEM_MEMORY_CHANGED:
                    errorCode = RESP_CODE_MEM_CARD_CHANGED; 
                    break;
                default:
                    dlog_alert("Error code %d", res);
                    errorCode = RESP_CODE_MEM_CARD_UNCHANGED; 
                    break;
            }
        }

        response.addTag(ConstData(arbyTagCode, CODE_TS), MakeBERValue(errorCode));

        // Send answer
        dlog_msg("Sending response");
        if( IPCPACKET_SUCCESS != response.send() )
        {
            dlog_msg("Sending response error");
            returnCode = ERESPONSESEND;
        }
        return returnCode;
    }

/***************************************************************************
 * Exportable function definitions
 **************************************************************************/

/***************************************************************************
 * Exportable class members' definitions
 **************************************************************************/


/**
 * @}
 */

/***************************************************************************
 * Module namspace: end
 **************************************************************************/
}

