#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 *
 * Copyright (C) 2007 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/


/***************************************************************************
 * Includes
 **************************************************************************/
#include "cmd/cmd_WriteI2C.h"
#include "cCARDAppConfig.h"
#include "cEMV.h"
#include "cTransaction.h"
#include "tlv_parser.h"

#include <EE2K4K.h>
#include <cardslot.h>

#include <tlv-lite/ConstData.h>
#include <tlv-lite/ValueConverter.hpp>
#include <tlv-lite/ConstValue.hpp>


/***************************************************************************
 * Using
 **************************************************************************/
using namespace com_verifone_ipcpacket;
using namespace com_verifone_emv;

extern CCardAppConfig   g_CardAppConfig;
extern CEMV             g_CEMV;
extern CEMVtrns         g_CEMVtrns;


/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
namespace com_verifone_cmd
{

/**
 * @addtogroup Cmd
 * @{
 */

   c_card_error_t CWriteI2C::isCanceled(IPCPacket &response)
   {
      CCardAppConfig::cmd_break_e ret = g_CardAppConfig.checkCmdBreak(); //check break status
      uint8_t errorCode;   //error code send to sender application
      c_card_error_t returnCode;  //function return code

      switch( ret )
      {
      case CCardAppConfig::CMD_BREAK_NO_BREAK_E: //no break
         return ESUCCESS;
      case CCardAppConfig::CMD_BREAK_CANCEL_E:   //command canceled
         errorCode  = RESP_CODE_CMD_CANCEL;
         returnCode = ECANCEL;
         break;
      case CCardAppConfig::CMD_BREAK_TIMEOUT_E:  //timeout
         errorCode  = RESP_CODE_TIMEOUT;
         returnCode = EPOSTIMEOUT;
         break;
      case CCardAppConfig::CMD_BREAK_SEQ_ERROR_E:
         errorCode  = RESP_CODE_CMD_SEQ_ERROR;
         returnCode = ESEQERR;
         break;
      default:
         return ESUCCESS;
      }


      //prepare response message
      response.addTag(com_verifone_TLVLite::ConstData(arbyTagCode, CODE_TS), com_verifone_TLVLite::MakeBERValue(errorCode));

      if( IPCPACKET_SUCCESS != response.send() )
      {
         return ERESPONSESEND;
      }

      return returnCode;
   }


    c_card_error_t CWriteI2C::handleCommand(IPCPacket *pack)
    {
        using namespace com_verifone_TLVLite;
        uint8_t   errorCode  = RESP_CODE_SUCCESS;   //error code which will be send to sender application
        c_card_error_t   returnCode = ESUCCESS;   //function return code
        IPCPacket  response; //response message initialization

        uint16_t memCardAddress = 0;
        uint8_t *pData = 0;
        int length = 0;
        ConstData value(ConstData::getInvalid());

        dlog_msg("WriteI2C: handle");

        response.setCmdCode(pack->getCmdCode());
        response.setAppName(pack->getAppName());


        if( IPCPACKET_SUCCESS != pack->getTag(ConstData(arbyTagMemoryCardAddress, MEM_CARD_ADDRESS_TS), value))
        {
            dlog_error("Address missing");
            errorCode = RESP_CODE_FAILED;
        }
        else
        {
            ValueConverter<uint16_t> valueConv(value);
            memCardAddress = valueConv.getValue(0);
        }
        value = ConstData::getInvalid();
        if( IPCPACKET_SUCCESS != pack->getTag(ConstData(arbyTagMemoryCardData, MEM_CARD_DATA_TS), value))
        {
            dlog_error("Data count missing");
            errorCode = RESP_CODE_FAILED;
        }
        else
        {
            if (value.isValid())
            {
                pData = const_cast<unsigned char *>(value.getByteBuffer()); // fixme!!!
                length = value.getSize();
            }
        }

        if(  isCanceled(response) != ESUCCESS ) //check break status, if this command was cancelled
        {
            return ECANCEL;
        }
        if (Get_Card_State(g_CEMV.ICC_reader) != CARD_PRESENT)
        {
            dlog_alert("No card!");
            errorCode = RESP_CODE_CARD_REMOVED;
        }
        if (g_CEMVtrns.icc_card_type != CARD_APP_CARDTYPE_I2C)
        {
            dlog_alert("No I2C card is inserted, aborting!");
            errorCode = RESP_CODE_MEM_CARD_UNCHANGED;
        }

        if (errorCode == RESP_CODE_SUCCESS)
        {
            // Now, handle the command. 
            uint8_t respBuf[300] = { '\0' };
            uint8_t cmdBuf[300] = { '\0' };
            unsigned long cmdLen = 0;
            uint16_t respLen = 0;
            int apduStat = 0;
            uint8_t addrLSB = (memCardAddress & 0xFF);
            uint8_t addrMSB = ((memCardAddress & 0xFF00) >> 8);

            dlog_msg("Writing %04X bytes to address: MSB %02X, LSB %02X", length, addrMSB, addrLSB);
            EE2K4K_WRITE_BYTES(cmdBuf, addrMSB, addrLSB, pData, length, cmdLen);
            apduStat = Transmit_APDU(g_CEMV.ICC_reader, cmdBuf, cmdLen, respBuf, &respLen); // This CRASHES!
            dlog_msg("Write result %d, response len %d", apduStat, respLen);
            if (apduStat == CARDSLOT_SUCCESS)
            {
                // Success
                response.addTag(ConstData(arbyTagMemoryCardData, MEM_CARD_DATA_TS), ConstData(respBuf, respLen));
            }
            else
            {
                // Failure
                errorCode = RESP_CODE_MEM_CARD_CHANGED;
            }
        }

        response.addTag(ConstData(arbyTagCode, CODE_TS), MakeBERValue(errorCode));


        // Send answer
        dlog_msg("Sending response");
        if( ESUCCESS != response.send() )
        {
            dlog_msg("Sending response error");
            returnCode = ERESPONSESEND;
        }
        return returnCode;
    }

/***************************************************************************
 * Exportable function definitions
 **************************************************************************/

/***************************************************************************
 * Exportable class members' definitions
 **************************************************************************/


/**
 * @}
 */

/***************************************************************************
 * Module namspace: end
 **************************************************************************/
}

