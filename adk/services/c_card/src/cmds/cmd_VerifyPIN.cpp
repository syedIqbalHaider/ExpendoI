#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 *
 * Copyright (C) 2007 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file       cmd_GenerateMac.cpp
 *
 *
 *
 * @remarks    This file should be compliant with Verifone EMEA R&D C++ Coding
 *             Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/

#include <liblog/logsys.h>
#include <tlv-lite/ConstData.h>
#include <tlv-lite/ValueConverter.hpp>
#include <tlv-lite/ConstValue.hpp>
#include <libipcpacket/ipcpacket.hpp>


#include "cCARDAppConfig.h"
#include "cEMV.h"
#include "cTransaction.h"
#include "tlv_parser.h"

#include <libpml/pml_abstracted_api.h>
#include <libpml/pml_port.h>


#include "cmd/cmd_VerifyPIN.h"

#include "PIN.h"


#if 0
   #define RESP_PIN_SUCCESS          0                 
/*    0 ->  0 - PIN accepted by card          */

   #define RESP_PIN_INVALID_PARAM    -203      
/* -203 ->  2 - Invalid parameter to function */

   #define RESP_PIN_CANCELLED_POS    -207  
/* -207 ->  8 - PIN cancelled ??              */

   #define RESP_PIN_SLOT_ERROR       -307       
/* -307 -> 10 - ADPU message send failed      */

   #define RESP_PIN_BAD_ICC_RESPONSE -231
/* -231 -> 21 - Challenge response failed     */

   #define RESP_PIN_CARD_REMOVED     -275     
/* -275 -> 23 - Card not present              */

   #define RESP_PIN_RETRY            -212        
/* -212 -> 28 - retry PIN entry               */

   #define RESP_PIN_FAILED           -213      
/* -213 -> 29 - last try PIN failed           */

   #define RESP_PIN_EXCEEDCOUNTER    -215   
/* -215 -> 31 - PIN try counter is zero       */

   #define RESP_PIN_KEY_MISSING      -287      
/* -287 -> 70 - Cannot get Key                */

   #define RESP_PIN_INTERNAL_FAILURE -204        
/* -204 -> 99 - RSA algorithm failed          */

   #define RESP_PIN_BYPASSED_POS      -317
/* -317 -> xx - PIN Bypass was executed       */

   #define RESP_PIN_BYPASSED_CARDHOLDER     -318
   #define RESP_PIN_CANCELLED_CARDHOLDER    -319
   #define RESP_PIN_TIMEOUT                 -305
#endif

/***************************************************************************
 * Using
 **************************************************************************/
using namespace com_verifone_ipcpacket;
using namespace com_verifone_emv;

extern CCardAppConfig   g_CardAppConfig;
extern CEMV			g_CEMV;
extern CEMVtrns		g_CEMVtrns;

/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
	namespace com_verifone_cmd
	{

		c_card_error_t CVerifyPINCmd::isCanceled(IPCPacket &response)
		{
			CCardAppConfig::cmd_break_e ret = g_CardAppConfig.checkCmdBreak(); //check break status
			uint8_t errorCode;   //error code send to sender application
			c_card_error_t returnCode;  //function return code

			switch( ret )
			{
			case CCardAppConfig::CMD_BREAK_NO_BREAK_E: //no break
				return ESUCCESS;
			case CCardAppConfig::CMD_BREAK_CANCEL_E: //command canceled
				errorCode = RESP_CODE_CMD_CANCEL;
				returnCode = ECANCEL;
				break;
			case CCardAppConfig::CMD_BREAK_TIMEOUT_E:  //timeout
				errorCode = RESP_CODE_TIMEOUT;
				returnCode = EPOSTIMEOUT;
				break;
			case CCardAppConfig::CMD_BREAK_SEQ_ERROR_E:
				errorCode = RESP_CODE_CMD_SEQ_ERROR;
				returnCode = ESEQERR;
				break;
			default:
				return ESUCCESS;
			}
			//prepare response message
			response.addTag(com_verifone_TLVLite::ConstData(arbyTagCode, CODE_TS), com_verifone_TLVLite::MakeBERValue(errorCode));

			if( IPCPACKET_SUCCESS!= response.send() )
			{
				return ERESPONSESEND;
			}

			return returnCode;
		}
	

		c_card_error_t CVerifyPINCmd::handleCommand(IPCPacket *pack)
		{
			using namespace com_verifone_TLVLite;
			IPCPacket  response; //response message initialization
			uint8_t   errorCode  = RESP_CODE_SUCCESS;   //error code which will be send to sender application
			c_card_error_t   returnCode = ESUCCESS;   //function return code
			ConstData value(ConstData::getInvalid());
			uint8_t PIN_type_l = PIN_TYPE_PLAIN_TEXT;
			uint8_t PIN_entrytype_l = ENTRY_TYPE_NORMAL;
			int iRet;
			uint8_t   OutTLVdata_buf[CARD_APP_MAX_TLV];

			//setting IN/OUT buffers
			
			dlog_msg("*****VERIFY PIN COMMAND*****");

			response.setCmdCode(pack->getCmdCode());
			response.setAppName(pack->getAppName());
			
			g_CEMVtrns.InTLV_list_len = 0;
			g_CEMVtrns.OutTLV_list_len = 0;
			g_CEMVtrns.OutTLVdata = OutTLVdata_buf;
			g_CEMVtrns.OutTLV_data_len = CARD_APP_MAX_TLV;
			g_CEMVtrns.monitor_flags &= 0xFD;
			
			if( IPCPACKET_SUCCESS != pack->getTag(ConstData(arbyTagInTLV, IN_TLV_TS), value) )
			{
				dlog_msg("In TLV list missing");
			}
			else
			{
				dlog_msg("In TLV list len: %d", value.getSize());
				g_CEMVtrns.InTLVlist = const_cast<unsigned char *>(value.getByteBuffer()); // fixme!!!!
				g_CEMVtrns.InTLV_list_len = value.getSize();
			}
			if( IPCPACKET_SUCCESS != pack->getTag(ConstData(arbyTagOutTLVlist, OUT_TLV_LIST_TS), value) )
			{
				dlog_msg("Out TLV list missing");
				
			}
			else
			{
				dlog_msg("Out TLV list len: %d", value.getSize());
				g_CEMVtrns.OutTLVlist = const_cast<unsigned char *>(value.getByteBuffer()); // fixme!!!
				g_CEMVtrns.OutTLV_list_len = value.getSize();
			}

			if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagPINOfflinetype, PIN_OFFLINE_TYPE_TS), value ) )
			{
				ValueConverter<uint8_t> valueConv(value);
				PIN_type_l = valueConv.getValue(0);
				dlog_msg("Setting PIN  type to %d", PIN_type_l);
			}
			if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagPlainTxtPinBlock, PLAIN_TXT_PIN_BLOCK_TS), value ) )
			{
				dlog_alert("External PIN verification only, no PIN entry!");
				g_CEMV.setExternalPIN(static_cast<const char *>(value.getBuffer()), value.getSize());
				PIN_entrytype_l = ENTRY_TYPE_ATOMIC;
				if (IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagPinAlgorithm, PIN_ALGORITHM_TS), value ) )
				{
					ValueConverter<int> valueConv(value);
					g_CEMV.setExternalPINMode(valueConv.getValue(-1));
				}
				else
				{
					g_CEMV.setExternalPINMode(-1);
				}
			}
			else
			{
				if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagPINtimeout, PIN_TIMEOUT_TS), value ) )
				{
					ValueConverter<long> valueConv(value);
					long PINtimeout = valueConv.getValue(0);
					g_CardAppConfig.SetPINtimeout(PINtimeout);
					dlog_msg("Setting PIN timeout to: %dms" , PINtimeout);
				}
				if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagPINfirstcharTimeout, PIN_FIRSTCHAR_TIMEOUT_TS), value ) )
				{
					ValueConverter<long> valueConv(value);
					long PINtimeout = valueConv.getValue(0);
					g_CardAppConfig.SetPINfirstcharTimeout(PINtimeout);
					dlog_msg("Setting PIN firstchar timeout to: %dms" , PINtimeout);
				}
				if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagPINintercharTimeout, PIN_INTERCHAR_TIMEOUT_TS), value ) )
				{
					ValueConverter<long> valueConv(value);
					long PINtimeout = valueConv.getValue(0);
					g_CardAppConfig.SetPINintercharTimeout(PINtimeout);
					dlog_msg("Setting PIN interchar timeout to: %dms" , PINtimeout);
				}
				if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagRadixSeparator, RADIX_SEPARATOR_TS), value ) )
				{
					ValueConverter<char> valueConv(value);
					char radix = valueConv.getValue(0);
					g_CardAppConfig.SetPINRadixSeparator(radix);
					dlog_msg("Setting PIN radix separator to '%c' (%02Xh)", radix, radix);
				}
				if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagPINStyle, PIN_STYLE_TS), value ) )
				{
					ValueConverter<uint8_t> valueConv(value);
					uint8_t PINentry_style = valueConv.getValue(0);
					g_CardAppConfig.SetOfflinePINEntryStyle(PINentry_style);
					dlog_msg("Setting PIN entry style to %d", PINentry_style);
				}
				else
	  			{
			  	  uint8_t PINentry_style = 0;
				  g_CardAppConfig.SetOfflinePINEntryStyle(PINentry_style);
	  			}
				if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagPINCurrSymbolLoc, PIN_CURR_SYMBOL_LOC_TS), value) )
				{
					ValueConverter<uint8_t> valueConv(value);
					uint8_t PINSymbLoc = valueConv.getValue(0);
					g_CardAppConfig.SetPINDisplayCurrencySymbolLoc(PINSymbLoc);
					dlog_msg("Setting PIN entry symbol location to %d", PINSymbLoc);
				}
				if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagPINBeeperTimeout, PIN_BEEPER_TIMEOUT_TS), value ) )
				{
					ValueConverter<long> valueConv(value);
					long PINbeeperTimeout = valueConv.getValue(0);
					g_CardAppConfig.SetPINbeeperTimeout(PINbeeperTimeout);
					dlog_msg("Setting PIN beeper timeout to: %dms" , PINbeeperTimeout);
				}
				if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagPinOfflineEntryType, PIN_OFFLINE_ENTRY_TYPE_TS), value ) )
				{
					ValueConverter<uint8_t> valueConv(value);
					PIN_entrytype_l = valueConv.getValue(0);
					dlog_msg("Setting PIN entry type to %d", PIN_entrytype_l);
				}
				if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagShowIncorrectPIN, SHOW_INCORRECT_PIN_TS), value) )
				{
					ValueConverter<uint8_t> valueConv(value);
					uint8_t ShowIncorrectPIN = valueConv.getValue(1);
					g_CardAppConfig.SetShowIncorrectPIN(ShowIncorrectPIN == 1);
					dlog_msg("Setting show incorrect PIN to: %d", ShowIncorrectPIN == 1);
				}
			}

			g_CEMV.SetPINparams(PIN_type_l, PIN_entrytype_l);

			if(errorCode == RESP_CODE_SUCCESS)
			{
				iRet = g_CEMVtrns.EMVtransaction(CARD_APP_OPER_PIN_VERIFY, &g_CEMV);
				dlog_msg("EMVTransaction RET: %X", iRet);

			#if 0
				switch(iRet)
				{
					case RESP_PIN_SUCCESS:
						errorCode = RESP_CODE_SUCCESS;
						break;
					case RESP_PIN_INVALID_PARAM:
						errorCode = RESP_CODE_INVALID_CMD;
						break;
					case RESP_PIN_CANCELLED_POS:
					case RESP_PIN_CANCELLED_CARDHOLDER:
						errorCode = RESP_CODE_PIN_CANCEL;
						break;
					case RESP_PIN_SLOT_ERROR:   
						errorCode = RESP_CODE_PIN_ENCIPHER_ERROR;
						break;
					case RESP_PIN_BAD_ICC_RESPONSE:
						errorCode = RESP_CODE_PIN_MISSING_PIN_BLOCK_DATA;
						break;
					case RESP_PIN_CARD_REMOVED:
						errorCode = RESP_CODE_CARD_REMOVED;
						break;
					case RESP_PIN_RETRY:
						errorCode = RESP_CODE_PIN_VERIFY_FAILED;
						break;
					case RESP_PIN_FAILED:
						errorCode = RESP_CODE_PIN_BLOCKED;
						break;
					case RESP_PIN_EXCEEDCOUNTER:
						errorCode = RESP_CODE_RETRY_LIMIT;
						break;
					case RESP_PIN_KEY_MISSING:
						errorCode = RESP_CODE_INVALID_KEY;
						break;
					case RESP_PIN_INTERNAL_FAILURE:
						errorCode = RESP_CODE_FAILED;
						break;
					case RESP_PIN_BYPASSED_POS:
					case RESP_PIN_BYPASSED_CARDHOLDER:
						errorCode = RESP_CODE_PIN_BYPASS;
						break;
					case RESP_PIN_TIMEOUT:
						errorCode = RESP_CODE_TIMEOUT;
						break;
					default:
						dlog_alert("Unknown error code %d", iRet);
						errorCode = RESP_CODE_FAILED;
						break;
				}
			#endif
				switch(iRet)
				{
					case PIN_VERIFY_OK:
						errorCode = RESP_CODE_SUCCESS;
						break;
					case PIN_VERIFY_CANCEL:
						errorCode = RESP_CODE_PIN_CANCEL;
						break;
					case PIN_VERIFY_ERROR:
						errorCode = RESP_CODE_PIN_VERIFY_FAILED;
						break;
					case PIN_VERIFY_BYPASS:
					case PIN_VERIFY_POS_BYPASS:
						errorCode = RESP_CODE_PIN_BYPASS;
						break;
					case PIN_VERIFY_TIMEOUT:
						errorCode = RESP_CODE_TIMEOUT;
						break;
					case PIN_VERIFY_BLOCKED:
						errorCode = RESP_CODE_PIN_BLOCKED;
						break;
					case PIN_VERIFY_CARD_REMOVED:
						errorCode = RESP_CODE_CARD_REMOVED;
						break;
					case PIN_VERIFY_POS_CANCEL:
						errorCode = RESP_CODE_CMD_CANCEL;
						break;
					case PIN_VERIFY_GET_CHALLENGE:
						errorCode = RESP_CODE_PIN_MISSING_PIN_BLOCK_DATA;
						break;
					//case PIN_VERIFY_LAST_TRY:
					//	errorCode = RESP_CODE_PIN_VERIFY_FAILED;
					default:
						dlog_alert("Unknown error code %d", iRet);
						errorCode = RESP_CODE_FAILED;
						break;
				}

				dlog_msg("Adding response %d", errorCode);
				response.addTag(ConstData(arbyTagCode, CODE_TS), MakeBERValue(errorCode));
				errorCode = g_CEMV.GetPINEntryStatus();
				dlog_msg("Adding PIN status %d", errorCode);
				response.addTag(ConstData(arbyTagPINResult, PIN_RESULT_TS), MakeBERValue(errorCode));
				if(g_CEMVtrns.TLV_data_len > 0)
				{
					dlog_msg("Adding %d bytes of read TLV objects", g_CEMVtrns.TLV_data_len);
					response.addTag(ConstData(arbyTagOutTLV, OUT_TLV_TS), ConstData(g_CEMVtrns.OutTLVdata, g_CEMVtrns.TLV_data_len));
				}
				
				
				dlog_msg("offline PIN verify DONE: ret: %d", errorCode);
			}
			
			if( IPCPACKET_SUCCESS != response.send() )
			{
				/*
				while(1)
				{
					com_verifone_pml::error_tone();
					SVC_WAIT(300);
				}
				*/
				return ERESPONSESEND;
			}
			if(g_CEMV.GetStatusMain() & CARD_APP_STATUS_M_STOP)
			{
				dlog_msg("Waiting for card removal...");
				if(g_CardAppConfig.GetWaitForRemovalStatus())
				{
					g_CEMVtrns.monitor_flags |= CARD_APP_MON_NOBREAK;
				}
			}

			return ESUCCESS;
		}

	}



