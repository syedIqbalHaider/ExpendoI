#ifndef __cplusplus
#error "This file is for C++ only!"
#endif
/*****************************************************************************
 *
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/
/**
 * @file        cmd_Reset.cpp
 *
 * @author      Jacek Olbrys
 *
 * @brief       APDU Command
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C++ Coding
 *              Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/
#include <tlv-lite/ConstData.h>
#include <tlv-lite/ValueConverter.hpp>
#include <tlv-lite/ConstValue.hpp>
#include <libipcpacket/ipcpacket.hpp>


#include "cCARDAppConfig.h"
#include "cEMV.h"
#include "EMVtools.h"
#include "cTransaction.h"
#include "tlv_parser.h"

#include <liblog/logsys.h>

#include <libpml/pml_abstracted_api.h>
#include <libpml/pml_port.h>


#include "cmd/cmd_Reset.h"

#ifdef VFI_PLATFORM_VOS
#define EMV_XML_APPLICATIONS "flash/EMV_Applications.xml"
#define EMV_XML_TERMINAL "flash/EMV_Terminal.xml"
#define EMV_XML_KEY "flash/EMV_Keys.xml"

#endif
#ifdef VFI_PLATFORM_VERIXEVO
#define EMV_XML_APPLICATIONS "EST.DAT"
#define EMV_XML_TERMINAL "MVT.DAT"
#endif


/***************************************************************************
 * Using
 **************************************************************************/
using namespace com_verifone_emv;
using namespace com_verifone_ipcpacket;


extern CCardAppConfig   g_CardAppConfig;
extern CEMV         g_CEMV;
extern CEMVtrns     g_CEMVtrns;

/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
namespace com_verifone_cmd
{

/**
 * @addtogroup Card
 * @{ 
 */

   /***************************************************************************
    * Exportable class members' definitions
    **************************************************************************/

   c_card_error_t CResetCmd::isCanceled(IPCPacket &response)
   {
      CCardAppConfig::cmd_break_e ret = g_CardAppConfig.checkCmdBreak(); //check break status
      uint8_t errorCode;   //error code send to sender application
      c_card_error_t returnCode;  //function return code

      switch( ret )
      {
      case CCardAppConfig::CMD_BREAK_NO_BREAK_E: //no break
         return ESUCCESS;
      case CCardAppConfig::CMD_BREAK_CANCEL_E:   //command canceled
         errorCode  = RESP_CODE_CMD_CANCEL;
         returnCode = ECANCEL;
         break;
      case CCardAppConfig::CMD_BREAK_TIMEOUT_E:  //timeout
         errorCode  = RESP_CODE_TIMEOUT;
         returnCode = EPOSTIMEOUT;
         break;
      case CCardAppConfig::CMD_BREAK_SEQ_ERROR_E:
         errorCode  = RESP_CODE_CMD_SEQ_ERROR;
         returnCode = ESEQERR;
         break;
      default:
         return ESUCCESS;
      }

      //prepare response message
      response.addTag(com_verifone_TLVLite::ConstData(arbyTagCode, CODE_TS), com_verifone_TLVLite::MakeBERValue(errorCode));
      if( IPCPACKET_SUCCESS!= response.send() )
      {
         return ERESPONSESEND;
      }
      return returnCode;
   }


  c_card_error_t CResetCmd::handleCommand(IPCPacket *pack)
  {
        using namespace com_verifone_TLVLite;
        IPCPacket  response; //response message initialization
        uint8_t   errorCode  = RESP_CODE_SUCCESS;   //error code which will be send to sender application
        unsigned short        InAPDU_len = 0;
        unsigned short       OutAPDU_len = 0;
        int       iRet;
        c_card_error_t        return_code = ESUCCESS;
        static const unsigned int DEFAULT_FLAGS = RESET_FLAGS_CLEAR_EMV_COLLXN; // Delete files
        
        unsigned int flags = DEFAULT_FLAGS;
        ConstData value(ConstData::getInvalid());

		
        dlog_msg("*****Reset*****");
        
        g_CEMVtrns.monitor_flags &= 0xFD;


        response.setCmdCode(pack->getCmdCode());
        
        response.setAppName(pack->getAppName());



        if( IPCPACKET_SUCCESS != pack->getTag(ConstData(arbyTagFlags, FLAGS_TS), value))
        {
            dlog_msg("Flags missing - assuming reset to factory settings");
            flags = DEFAULT_FLAGS;
        }
        else
        {
            ValueConverter<unsigned int>conv(value);
            flags = conv.getValue(DEFAULT_FLAGS);
        }
        dlog_msg("Processing reset, flags %Xh", flags);
        if (flags & RESET_FLAGS_CLEAR_EMV_FILES)
        {
            com_verifone_pml::delete_file(EMV_XML_APPLICATIONS);
            com_verifone_pml::delete_file(EMV_XML_TERMINAL);
            #ifdef VFI_PLATFORM_VOS
            com_verifone_pml::delete_file(EMV_XML_KEY);
            #endif
            #ifdef VFI_PLATFORM_VERIXEVO
            // Create empty MVT and EST files - we only need headers
            int EST_h = open(EMV_XML_APPLICATIONS, O_CREAT | O_WRONLY);
            int MVT_h = open(EMV_XML_TERMINAL, O_CREAT | O_WRONLY);
            const int HEADER_SIZE = 16;
            char buf[HEADER_SIZE];
            memset(buf, 0, sizeof(buf));
            if (write(EST_h, buf, HEADER_SIZE) != HEADER_SIZE)
            {
                dlog_alert("EST.DAT create error");
            }
            if (write(MVT_h, buf, HEADER_SIZE) != HEADER_SIZE)
            {
                dlog_alert("MVT.DAT create error");
            }
            close(EST_h);
            close(MVT_h);
            #endif
        }
        if (flags & RESET_FLAGS_CLEAR_EMV_COLLXN)
        {
            // Clear TLV pool
            g_CEMVtrns.ClearEMVCollxn();
        }
        #ifdef VFI_PLATFORM_VOS
        if (flags & RESET_FLAGS_CLEAR_UPDATE_DONE)
        {
            // Just force config reload
            bool kernelOpened = g_CEMVtrns.Is_Kernel_Opened();
            if (kernelOpened)
            {
                g_CEMVtrns.Close_Kernel();
            }
            if ((iRet = g_CEMVtrns.Open_Kernel()) != EMV_ADK_OK)
            {
                dlog_error("Cannot open the kernel!");
                g_CEMVtrns.Close_Kernel();
                errorCode = CARD_APP_RET_ICC_FAIL;
            }
            else
            {
                if (flags & RESET_FLAGS_CLEAR_EMV_FILES)
                {
//                    g_CEMVtrns.DefaultConfig(true);  //iq_audi_070217
                }
                if (!kernelOpened)
                {
                    g_CEMVtrns.Close_Kernel();
                }
            }
        }
        #endif

        response.addTag(ConstData(arbyTagCode, CODE_TS), MakeBERValue(errorCode));

        if( IPCPACKET_SUCCESS!= response.send() )
        {
/*            while(1)
            {
                com_verifone_pml::error_tone();
                SVC_WAIT(300);
            }
*/            
            return_code =  ERESPONSESEND;
        }

       
        return return_code;
    }

/**
 * @}
 */

/***************************************************************************
 * Module namspace: end
 **************************************************************************/
}


