#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 *
 * Copyright (C) 2007 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file       cmd_APDU.cpp
 *
 *
 * @remarks    This file should be compliant with Verifone EMEA R&D C++ Coding
 *             Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/

#include <liblog/logsys.h>
#include <tlv-lite/ConstData.h>
#include <tlv-lite/ValueConverter.hpp>
#include <tlv-lite/ConstValue.hpp>
#include <libipcpacket/ipcpacket.hpp>


#include "cCARDAppConfig.h"
#include "cEMV.h"
#include "cTransaction.h"
#include "tlv_parser.h"
#include "EMVtools.h"


#include "cmd/cmd_StartWait.h"

#include <libpml/pml_abstracted_api.h>
#include <libpml/pml_port.h>




/***************************************************************************
 * Using
 **************************************************************************/
using namespace com_verifone_ipcpacket;
using namespace com_verifone_emv;


extern CCardAppConfig   g_CardAppConfig;
extern CEMV			g_CEMV;
extern CEMVtrns		g_CEMVtrns;
#ifdef VFI_PLATFORM_VOS
extern logger			g_logger;
#endif


/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
namespace com_verifone_cmd
{


  c_card_error_t CStartWaitCmd::isCanceled(IPCPacket &response)
  {
      CCardAppConfig::cmd_break_e ret = g_CardAppConfig.checkCmdBreak(); //check break status
      uint8_t errorCode;   //error code send to sender application
      c_card_error_t returnCode;  //function return code

      dlog_msg("received cancel or unexpected message ret: %d", ret);

      switch( ret )
      {
      case CCardAppConfig::CMD_BREAK_NO_BREAK_E: //no break
         return ESUCCESS;
      case CCardAppConfig::CMD_BREAK_CANCEL_E:   //command canceled
         errorCode  = RESP_CODE_CMD_CANCEL;
         returnCode = ECANCEL;
         break;
      case CCardAppConfig::CMD_BREAK_TIMEOUT_E:  //timeout
         errorCode  = RESP_CODE_TIMEOUT;
         returnCode = EPOSTIMEOUT;
         break;
      case CCardAppConfig::CMD_BREAK_SEQ_ERROR_E:
         errorCode  = RESP_CODE_CMD_SEQ_ERROR;
         returnCode = ESEQERR;
         break;
      default:
         return ESUCCESS;
      }


      //prepare response message
      //response.addTag(com_verifone_TLVLite::ConstData(arbyTagCode, CODE_TS), com_verifone_TLVLite::MakeBERValue(errorCode));

      //if( IPCPACKET_SUCCESS != response.send() )
      //{
      //   return ERESPONSESEND;
      //}

      return returnCode;
  }

  c_card_error_t CStartWaitCmd::handleCommand(IPCPacket *pack)
  {
      using namespace com_verifone_TLVLite;
      IPCPacket  response; //response message initialization
      uint8_t   errorCode  = RESP_CODE_FAILED;   //error code which will be send to sender application
      c_card_error_t   returnCode = ESUCCESS;   //function return code
      int flags;
      int iRet;
      int card_type = 0;
      uint32_t posTimeout = 0;
      uint32_t iterationTimeout = 0;
      long end_time, curr_time;
      ConstData value(ConstData::getInvalid());
	  int ICC_Wait_timeout = -1;

      c_card_error_t ret;


      g_CEMVtrns.monitor_flags &= 0xFD;
      //g_CardAppConfig.SetPINtimeout(0);
      //dlog_msg("PIN entry timeout set to 0ms (no timeout)");

      dlog_msg("*****START_WAITING_COM*****");

      response.setCmdCode(pack->getCmdCode());
      response.setAppName(pack->getAppName());

      if(IPCPACKET_SUCCESS!= pack->getTag(ConstData(arbyTagPosTimeout, POS_TIMEOUT_TS), value) )
      {
          dlog_msg("missing POS Timeout");
          // posTimeout = 0;
      }
      else
      {
          if (value.getSize() == 1)
          {
              ValueConverter<unsigned char> valueConv(value);
              posTimeout = valueConv.getValue(0);
              posTimeout *= TICKS_PER_SEC;
          }
          else if (value.getSize() == 4)
          {
              ValueConverter<unsigned int> valueConv(value);
              posTimeout = valueConv.getValue(0);
          }
          else
          {
              dlog_msg("invalid POS timeout!");
          }
      }

      if(posTimeout > 0)
      {
          dlog_msg("Timeout set to %d ms", posTimeout);
		  ICC_Wait_timeout = posTimeout;
      }
      else
      {
          dlog_msg("Waiting forever...");
		  ICC_Wait_timeout = -1;
      }
      if( IPCPACKET_SUCCESS != pack->getTag(ConstData(arbyTagProcFlag, PROC_FLAG_TS), value) )
      {
          dlog_msg("Processing flags missing - using default: 0x00");
          flags = 0;
      }
      else
      {
          ValueConverter<int > valueConv(value);
          flags = valueConv.getValue(0);
          dlog_msg("Proc flags: %X", flags);
      }
      if(!(flags & CARD_APP_FLAG_USE_ICC)) card_type |= CARD_APP_CARD_ICC;
      if(!(flags & CARD_APP_FLAG_USE_MAG)) card_type |= CARD_APP_CARD_MAGNETIC;
      if((flags & CARD_APP_FLAG_UNSOLICITED))
      {
          //we should answer here to the command and start waiting
          dlog_msg("Unsolicited packet required, answering command immediately...");
          errorCode = RESP_CODE_SUCCESS;
          g_CardAppConfig.SetNotifyStatus(true);
      }

      iRet = CARD_APP_EVT_NONE;
          dlog_msg("waiting for %d ms...", ICC_Wait_timeout);
		  #ifdef VFI_PLATFORM_VOS
	  	  g_logger.Log("waiting for %d ms...", ICC_Wait_timeout);
	  	  #endif
          iRet = g_CEMVtrns.wait_for_card(card_type, ICC_Wait_timeout, (flags & CARD_APP_FLAG_CARD_TYPE));
          dlog_msg("wait_for_card RET: %X", iRet);
		  if(iRet == CARD_APP_EVT_NONE)
		  {
			  errorCode = RESP_CODE_TIMEOUT;

		  }

		  
          if(iRet & CARD_APP_EVT_CARD_IN)
          {
              errorCode = RESP_CODE_SUCCESS;
          }
          
          if(iRet & CARD_APP_EVT_MSR)
          {
          	  if(g_CardAppConfig.GetIsUX100())
          	  {
          	      errorCode = RESP_CODE_CARD_REMOVED;
          	  }
			  else
			  {
				  errorCode = RESP_CODE_CARD_SWIPED;
			  }
              //g_CEMVtrns.InitCardReader();
              dlog_msg("Cleaning Reader status - wait_for_card: %X", CARD_APP_RET_MAG_OK);
          }
          if(iRet & CARD_APP_EVT_CARD_OUT)
          {
              errorCode = RESP_CODE_CARD_REMOVED;
              //g_CEMVtrns.InitCardReader();
              dlog_msg("Cleaning Reader status - wait_for_card: %X", CARD_APP_RET_REMOVED);
          }

		  if(iRet & CARD_APP_EVT_PIPE)
		  {
		  	errorCode = RESP_CODE_CMD_SEQ_ERROR;
          	if( (ret = isCanceled(response)) != ESUCCESS ) //check break status, if this command was canceled
          	{
            	dlog_msg("Command cancelled");
              	errorCode = RESP_CODE_CMD_CANCEL;
          	}
		  }


     
      if(1)
      {
      	  
          g_CEMVtrns.getMagData(response);
          if (g_CEMVtrns.icc_card_type != CARD_APP_CARDTYPE_NOT_SET && g_CEMVtrns.icc_card_type != CARD_APP_CARDTYPE_REQUIRES_POWERUP)
          {
              response.addTag(ConstData(arbyTagCardType, CARDTYPE_TS), MakeBERValue(g_CEMVtrns.icc_card_type));
              dlog_msg("Detected card type %d", g_CEMVtrns.icc_card_type);
          }
          int ATR_len = strlen((const char *)g_CEMVtrns.ATR)/2;
          if(ATR_len > 0)
          {
              unsigned char ATR_bin[CARD_APP_DEF_ATR_LEN/2];
              com_verifone_pml::svcDsp2Hex((const char *)g_CEMVtrns.ATR, ATR_len*2, reinterpret_cast<char *>(ATR_bin), ATR_len);
              response.addTag(ConstData(arbyTagATR, ATR_TS), ConstData(ATR_bin, ATR_len));
              dlog_msg("Adding ATR to response %d bytes", ATR_len);
          }
          memset(g_CEMVtrns.ATR, 0, sizeof(g_CEMVtrns.ATR));
      }

      response.addTag(ConstData(arbyTagCode, CODE_TS), MakeBERValue(errorCode));

      
	  #ifdef VFI_PLATFORM_VOS
	  g_logger.Log("Sending Card Wait notification: %X", errorCode);
	  #endif

      if( IPCPACKET_SUCCESS != response.send() )
      {
          return ERESPONSESEND;
      }

	  #ifdef VFI_PLATFORM_VOS
	  g_logger.Log("Send OK...");
	  #endif

      return ESUCCESS;
  }

}


