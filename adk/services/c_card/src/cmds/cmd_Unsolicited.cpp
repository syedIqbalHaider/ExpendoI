#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 *
 * Copyright (C) 2007 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file       cmd_APDU.cpp
 *
 *
 * @remarks    This file should be compliant with Verifone EMEA R&D C++ Coding
 *             Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/

#include <tlv-lite/ConstData.h>
#include <tlv-lite/ValueConverter.hpp>
#include <tlv-lite/ConstValue.hpp>
#include <libipcpacket/ipcpacket.hpp>
#include <libpml/pml_abstracted_api.h>

#include "cCARDAppConfig.h"
#include "cEMV.h"
#include "cTransaction.h"
#include "tlv_parser.h"


#include "liblog/logsys.h"

#include "EMVtools.h"

#include "cmd/cmd_Unsolicited.h"


/***************************************************************************
 * Using
 **************************************************************************/
using namespace com_verifone_ipcpacket;
using namespace com_verifone_emv;



extern CCardAppConfig   g_CardAppConfig;
extern CEMV			g_CEMV;
extern CEMVtrns		g_CEMVtrns;
#ifdef VFI_PLATFORM_VOS
extern logger			g_logger;
#endif


/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
namespace com_verifone_cmd
{


   c_card_error_t CUnsolicitedCmd::isCanceled(IPCPacket &response)
   {
      CCardAppConfig::cmd_break_e ret = g_CardAppConfig.checkCmdBreak(); //check break status
      uint8_t errorCode;   //error code send to sender application
      c_card_error_t returnCode;  //function return code

      switch( ret )
      {
      case CCardAppConfig::CMD_BREAK_NO_BREAK_E: //no break
         return ESUCCESS;
      case CCardAppConfig::CMD_BREAK_CANCEL_E:   //command canceled
         errorCode  = RESP_CODE_CMD_CANCEL;
         returnCode = ECANCEL;
         break;
      case CCardAppConfig::CMD_BREAK_TIMEOUT_E:  //timeout
         errorCode  = RESP_CODE_TIMEOUT;
         returnCode = EPOSTIMEOUT;
         break;
      case CCardAppConfig::CMD_BREAK_SEQ_ERROR_E:
         errorCode  = RESP_CODE_CMD_SEQ_ERROR;
         returnCode = ESEQERR;
         break;
      default:
         return ESUCCESS;
      }


      //prepare response message
      response.addTag(com_verifone_TLVLite::ConstData(arbyTagCode, CODE_TS), com_verifone_TLVLite::MakeBERValue(errorCode));

      if( IPCPACKET_SUCCESS!= response.send() )
      {
         return ERESPONSESEND;
      }

      return returnCode;
   }

  c_card_error_t CUnsolicitedCmd::handleCommand(IPCPacket *pack)
  {
      using namespace com_verifone_TLVLite;
      IPCPacket  response; //response message initialization
      uint8_t   errorCode  = RESP_CODE_SUCCESS;   //error code which will be send to sender application
      c_card_error_t   returnCode = ESUCCESS;   //function return code
      int iRet = 0;
      c_card_error_t ret;
	  int ipc_ret;

	  bool send_response = false;

      dlog_msg("*****UNSOLICITED_COM*****");

      response.setCmdCode(pack->getCmdCode());
      ipc_ret = response.setAppName(pack->getAppName());


      g_CEMVtrns.monitor_flags |= CARD_APP_MON_FOREVER;

      iRet = g_CEMVtrns.monitor_ICC(g_CEMVtrns.monitor_flags);
      dlog_msg("monitor_ICC RET: %X", iRet);

      g_CEMVtrns.monitor_flags &= 0xFD;

	  

	  if(iRet & CARD_APP_EVT_CARD_IN)
	  {
		  if (g_CEMVtrns.icc_card_status)
		  {
			  errorCode = g_CEMVtrns.icc_card_status;
			  g_CEMVtrns.icc_card_status = 0;
			  if (g_CEMVtrns.icc_card_type != CARD_APP_CARDTYPE_NOT_SET && g_CEMVtrns.icc_card_type != CARD_APP_CARDTYPE_REQUIRES_POWERUP)
			  {
				  response.addTag(ConstData(arbyTagCardType, CARDTYPE_TS), MakeBERValue(g_CEMVtrns.icc_card_type));
				  dlog_msg("Detected card type %d", g_CEMVtrns.icc_card_type);
			  }
			  int ATR_len = strlen((const char *)g_CEMVtrns.ATR)/2;
			  if(ATR_len > 0)
			  {
				  unsigned char ATR_bin[CARD_APP_DEF_ATR_LEN/2];
				  com_verifone_pml::svcDsp2Hex((const char *)g_CEMVtrns.ATR, ATR_len*2, reinterpret_cast<char *>(ATR_bin), ATR_len);
				  response.addTag(ConstData(arbyTagATR, ATR_TS), ConstData(ATR_bin, ATR_len));
				  dlog_msg("Adding ATR to response %d bytes", ATR_len);
			  }
			  memset(g_CEMVtrns.ATR, 0, sizeof(g_CEMVtrns.ATR));
		   }
		   else
		   {
			  errorCode = RESP_CODE_CARD_INSERTED;
		   }
		   send_response = true;

	  }

	  if(iRet & CARD_APP_EVT_CARD_OUT)
	  {
		  errorCode = RESP_CODE_CARD_REMOVED;
		  //g_CEMVtrns.InitCardReader();
		  dlog_msg("Cleaning Reader status - monitor_ICC: %X", CARD_APP_RET_REMOVED);
		  send_response = true;

	  }

	  if(iRet & CARD_APP_EVT_MSR)
	  {
		  if((g_CardAppConfig.GetIsUX100() && !g_CardAppConfig.GetIgnoreICCevt()) || (iRet & CARD_APP_EVT_CARD_OUT))
		  {
			  errorCode = RESP_CODE_CARD_REMOVED;
		  }
		  else
		  {
			  errorCode = RESP_CODE_CARD_SWIPED;
		  }
		  send_response = true;

	  }

	  if(iRet & CARD_APP_EVT_PIPE)
	  {
		  returnCode = ESEQERR;
	  }

	  if(iRet & CARD_APP_EVT_MSR_READY)
	  {
		  returnCode = ESEQERR;
	  }

	  if(send_response)
	  {

	      int track_len = g_CEMVtrns.getMagData(response);

		  if((g_CardAppConfig.GetIgnoreICCevt()) && (track_len <= 6) && (iRet != CARD_APP_EVT_CARD_OUT))
		  {
			  dlog_msg("ignoring failed MSR read");
			  return ESEQERR;
		  }

	      response.addTag(ConstData(arbyTagCode, CODE_TS), MakeBERValue(errorCode));

	      std::string to_app(g_CardAppConfig.cNotifyName);
	      if(response.setAppName(to_app) == IPCPACKET_SUCCESS)
	      {

		  	#ifdef VFI_PLATFORM_VOS
		  	g_logger.Log("Sending Card Monitor notification: %X", errorCode);
		  	#endif
		  
	      	if( IPCPACKET_SUCCESS!= response.send() )
	      	{
	         	 returnCode = ERESPONSESEND;
	      	}
	      }
		  else
		  {
			dlog_msg("unknown IPC receiver, ignoring message");
			returnCode = ESEQERR;
		  }
	  }

      return returnCode;
  }

}


