#ifndef __cplusplus
#error "This file is for C++ only!"
#endif
/*****************************************************************************
 *
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/
/**
 * @file        cmd_ExecuteMacro.cpp
 *
 * @author      Jacek Olbrys
 *
 * @brief       ?
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C++ Coding
 *              Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/
#include <svc.h>
#ifdef VFI_PLATFORM_VOS
#include <svcsec.h>
#endif
#ifdef VFI_PLATFORM_VERIXEVO
#include <svc_sec.h>
#endif

#include "cCARDAppConfig.h"
#include "cEMV.h"
#include "cTransaction.h"
#include "tlv_parser.h"

#include "liblog/logsys.h"
#include "EMVtools.h"

#include "cmd/cmd_ExecuteMacro.h"

#include <tlv-lite/ConstData.h>
#include <tlv-lite/ValueConverter.hpp>
#include <tlv-lite/ConstValue.hpp>
#include <libipcpacket/ipcpacket.hpp>



/***************************************************************************
 * Using
 **************************************************************************/
using namespace com_verifone_ipcpacket;
using namespace com_verifone_emv;

extern CCardAppConfig   g_CardAppConfig;
extern CEMV			g_CEMV;
extern CEMVtrns		g_CEMVtrns;

/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
namespace com_verifone_cmd
{

   
   /**
    * @addtogroup Card
    * @{ 
    */
   
     /***************************************************************************
      * Exportable class members' definitions
      **************************************************************************/

   c_card_error_t CExecuteMacroCmd::isCanceled(IPCPacket &response)
   {
      CCardAppConfig::cmd_break_e ret = g_CardAppConfig.checkCmdBreak(); //check break status
      uint8_t errorCode;   //error code send to sender application
      c_card_error_t returnCode;  //function return code

      switch( ret )
      {
      case CCardAppConfig::CMD_BREAK_NO_BREAK_E: //no break
         return ESUCCESS;
      case CCardAppConfig::CMD_BREAK_CANCEL_E:   //command canceled
         errorCode  = RESP_CODE_CMD_CANCEL;
         returnCode = ECANCEL;
         break;
      case CCardAppConfig::CMD_BREAK_TIMEOUT_E:  //timeout
         errorCode  = RESP_CODE_TIMEOUT;
         returnCode = EPOSTIMEOUT;
         break;
      case CCardAppConfig::CMD_BREAK_SEQ_ERROR_E:
         errorCode  = RESP_CODE_CMD_SEQ_ERROR;
         returnCode = ESEQERR;
         break;
      default:
         return ESUCCESS;
      }


      //prepare response message
      response.addTag(com_verifone_TLVLite::ConstData(arbyTagCode, CODE_TS), com_verifone_TLVLite::MakeBERValue(errorCode));

      if( IPCPACKET_SUCCESS != response.send() )
      {
         return ERESPONSESEND;
      }

      return returnCode;
   }

  c_card_error_t CExecuteMacroCmd::handleCommand(IPCPacket *pack)
  {
        using namespace com_verifone_TLVLite;
        IPCPacket  response; //response message initialization
        uint8_t   errorCode  = RESP_CODE_SUCCESS;   //error code which will be send to sender application
        c_card_error_t   returnCode = ESUCCESS;   //function return code
        ConstData value(ConstData::getInvalid());
        int iRet = 0;
        int VSS_In_len = 0;
        uint8_t *VSS_In = NULL;
        int VSS_slot = 0;
        int VSS_macro = 0;
        int crypto_handle = -1;
        int crypto_task;
        c_card_error_t ret;

        dlog_msg("*****EXECUTE_MACRO_COM*****");

        response.setCmdCode(pack->getCmdCode());
        response.setAppName(pack->getAppName());

        if( IPCPACKET_SUCCESS != pack->getTag(ConstData(arbyTagVSSslot, VSS_SLOT_TS), value))
        {
            dlog_msg("VSS script number missing");
            errorCode = RESP_CODE_FAILED;
        }
        else
        {
            ValueConverter<int> valueConv(value);
            VSS_slot = valueConv.getValue(0);
        }
        if( IPCPACKET_SUCCESS != pack->getTag(ConstData(arbyTagVSSmacro, VSS_MACRO_TS), value))
        {
            dlog_msg("VSS script macro number missing");
            errorCode = RESP_CODE_FAILED;
        }
        else
        {
            ValueConverter<int> valueConv(value);
            VSS_macro = valueConv.getValue(0);
            if( IPCPACKET_SUCCESS != pack->getTag(ConstData(arbyTagVSSIn, VSS_IN_TS), value))
            {
                dlog_msg("No Input data for macro execution");
            }
            else
            {
                VSS_In = const_cast<unsigned char *>(value.getByteBuffer());
                VSS_In_len = value.getSize();
            }
        }

        if(errorCode == RESP_CODE_SUCCESS)
        {
            uint8_t inBuffer[8+1];
            if(VSS_slot & CARD_APP_DEF_ONLINE_PIN)
            {
                // Disable notifications - not needed here
                bool notifyStatus = g_CardAppConfig.GetNotifyStatus();
                g_CardAppConfig.SetNotifyStatus(false);
                long pinType = g_CardAppConfig.GetPINtype();
                g_CardAppConfig.SetPINtype(CARD_APP_PIN_ONLINE_VSS);

#if 0 //GUIAPP is not supported - JACEK
                if( GuiApp_Connect( (char *)CARD_APP_NAME, 10000 ) == GUI_OKAY )
                {
                    
                    dlog_msg("Requesting online PIN entry... encryption with VSS slot: %d macro 0x%X", *VSS_slot, *VSS_macro);
                    unsigned char PINdigits[20];
                    iRet = static_cast<int>(getUsrPin(PINdigits));
                    if (iRet <= 4 || iRet >= 20)
                    {
                        // Error!
                        dlog_alert("PIN entry error %d", iRet);
                    }
                    else
                    {
                        iRet = 0;
                    }
                    response.addTag(arbyTagVSSResult, VSS_RESULT_TS, (uchar *)&iRet, sizeof(iRet));
                    *VSS_slot &= 0x7F;
                    GuiApp_ClearDisplay(0);
                    GuiApp_Disconnect();
                }
#endif
                g_CardAppConfig.SetNotifyStatus(notifyStatus);
                g_CardAppConfig.SetPINtype(pinType);
            }
            if(((VSS_slot & 0x7F) >= 0) && ((VSS_slot & 0x7F) <= 7))
            {
                errorCode = RESP_CODE_SUCCESS;
                if(VSS_macro >= 0x10)
                {
                    dlog_msg("Executing macro: 0x%X", VSS_macro);
                    uint8_t *VSS_Out = new uint8_t[256];
                    uint16_t VSS_Out_len = 0;
                    iRet = iPS_ExecuteScript(VSS_slot, VSS_macro, VSS_In_len, VSS_In, 256, &VSS_Out_len, VSS_Out);
                    dlog_msg("Script: %d, macro: 0x%X, in data size: %d  out data size: %d RET: 0x%X=%d", VSS_slot, VSS_macro, VSS_In_len, VSS_Out_len, iRet, iRet);
                    if(VSS_Out_len > 0)
                    {
                        dlog_hex(VSS_Out, VSS_Out_len, "Macro output:");
                        response.addTag(ConstData(arbyTagVSSOut, VSS_OUT_TS), ConstData(VSS_Out, VSS_Out_len));
                    }
                    response.addTag(ConstData(arbyTagVSSResult, VSS_RESULT_TS), MakeBERValue(iRet));
                    delete [] VSS_Out;
                }
                else
                {
                    errorCode = RESP_CODE_SUCCESS;
                    dlog_msg("Invalid macro number: 0x%X", VSS_macro);
                }
            }
            else
            {
                errorCode = RESP_CODE_SUCCESS;
                dlog_msg("Invalid script number: 0x%X", VSS_slot);
            }
        }
        response.addTag(ConstData(arbyTagCode, CODE_TS), MakeBERValue(errorCode));

        if( IPCPACKET_SUCCESS != response.send() )
        {
            return ERESPONSESEND;
        }

        return ESUCCESS;
    }

/**
 * @}
 */

/***************************************************************************
 * Module namspace: end
 **************************************************************************/
}


