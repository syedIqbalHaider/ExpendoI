#ifndef __cplusplus
#error "This file is for C++ only!"
#endif
/*****************************************************************************
 *
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/
/**
 * @file        cmd_APDU.cpp
 *
 * @author      Jacek Olbrys
 *
 * @brief       APDU Command
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C++ Coding
 *              Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/

#include <tlv-lite/ConstData.h>
#include <tlv-lite/ValueConverter.hpp>
#include <tlv-lite/ConstValue.hpp>
#include <libipcpacket/ipcpacket.hpp>

#include <liblog/logsys.h>

#ifdef VFI_PLATFORM_VERIXEVO
#include <vxemvapdef.h>
#include "EMVCWrappers.h"
#include <extlibvoy.h>
#include <cardslot.h>
#endif
#include "cCARDAppConfig.h"
#include "cEMV.h"
#include "EMVtools.h"
#include "cTransaction.h"
#include "tlv_parser.h"

#include <libpml/pml_abstracted_api.h>
#include <libpml/pml_port.h>


#include "cmd/cmd_APDU.h"

void EMVlog(char const * msg);


/***************************************************************************
 * Using
 **************************************************************************/
using namespace com_verifone_emv;
using namespace com_verifone_ipcpacket;


extern CCardAppConfig   g_CardAppConfig;
extern CEMV         g_CEMV;
extern CEMVtrns     g_CEMVtrns;

#ifdef VFI_PLATFORM_VOS
extern logger			g_logger;
#endif


/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
namespace com_verifone_cmd
{

/**
 * @addtogroup Card
 * @{ 
 */

   /***************************************************************************
    * Exportable class members' definitions
    **************************************************************************/

   c_card_error_t CAPDUCmd::isCanceled(IPCPacket &response)
   {
      CCardAppConfig::cmd_break_e ret = g_CardAppConfig.checkCmdBreak(); //check break status
      uint8_t errorCode;   //error code send to sender application
      c_card_error_t returnCode;  //function return code

      switch( ret )
      {
      case CCardAppConfig::CMD_BREAK_NO_BREAK_E: //no break
         return ESUCCESS;
      case CCardAppConfig::CMD_BREAK_CANCEL_E:   //command canceled
         errorCode  = RESP_CODE_CMD_CANCEL;
         returnCode = ECANCEL;
         break;
      case CCardAppConfig::CMD_BREAK_TIMEOUT_E:  //timeout
         errorCode  = RESP_CODE_TIMEOUT;
         returnCode = EPOSTIMEOUT;
         break;
      case CCardAppConfig::CMD_BREAK_SEQ_ERROR_E:
         errorCode  = RESP_CODE_CMD_SEQ_ERROR;
         returnCode = ESEQERR;
         break;
      default:
         return ESUCCESS;
      }

      //prepare response message
      response.addTag(com_verifone_TLVLite::ConstData(arbyTagCode, CODE_TS), com_verifone_TLVLite::MakeBERValue(errorCode));
      if( IPCPACKET_SUCCESS!= response.send() )
      {
         return ERESPONSESEND;
      }
      return returnCode;
   }


  c_card_error_t CAPDUCmd::handleCommand(IPCPacket *pack)
  {
        using namespace com_verifone_TLVLite;
        IPCPacket             response; //response message initialization
        uint8_t               errorCode  = RESP_CODE_SUCCESS;   //error code which will be send to sender application
        ConstData             value(ConstData::getInvalid());
        uint8_t *             InAPDU_buf = 0;
        uint8_t               OutAPDU_buf[CARD_APP_MAX_APDU];
        unsigned short        InAPDU_len = 0;
        unsigned short        OutAPDU_len = 0;
        c_card_error_t        iRet = ESUCCESS;
        const unsigned int    DEFAULT_FLAGS = 0;
        unsigned int          flags = DEFAULT_FLAGS;
        bool                  apduPresent = true;
        bool                  useTmpl = false;
        
        g_CEMVtrns.monitor_flags &= 0xFD;

        dlog_msg("*****APDU*****");
        EMVlog("Processing APDU command");


        #ifdef VFI_PLATFORM_VOS
        long long time_start_l = getTimeStampMs();
        long long time_end_l = time_start_l;
        #endif

        response.setCmdCode(pack->getCmdCode());
        response.setAppName(pack->getAppName());


        if( IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagFlags, FLAGS_TS), value))
        {
            ValueConverter<unsigned int>conv(value);
            flags = conv.getValue(DEFAULT_FLAGS);
            dlog_msg("After processing, flags %u", flags);
        }

        if( IPCPACKET_SUCCESS!= pack->getTag(ConstData(arbyTagInAPDU, IN_APDU_TS), value))
        {
            dlog_msg("In APDU command missing, checking the other tag...");
            if( IPCPACKET_SUCCESS!= pack->getTag(ConstData(arbyTagInAPDU_tmpl, IN_APDU_TS), value))
            {
                dlog_msg("In APDU command missing");
                apduPresent = false;
                errorCode = RESP_CODE_FAILED;
            }
            else
            {
                InAPDU_buf = const_cast<unsigned char *>(value.getByteBuffer()); // fixme!!! 
                InAPDU_len = value.getSize();
                dlog_msg("In APDU command len: %d", InAPDU_len);
                useTmpl = true;
            }
        }
        else
        {
            InAPDU_buf = const_cast<unsigned char *>(value.getByteBuffer()); // fixme!!! 
            InAPDU_len = value.getSize();
            dlog_msg("In APDU command len: %d", InAPDU_len);
        }
        if(errorCode == RESP_CODE_SUCCESS)
        {
            char APDU_ret = 0;
            if (!isCardInserted())
            {
                dlog_error("Card is removed!");
                errorCode = RESP_CODE_CARD_REMOVED;
            }
            else
            {
                int num_tries = 2;
                do
                {
                    // Card is inserted at that point, check is above
                    // if (g_CEMVtrns.icc_card_type == CARD_APP_CARDTYPE_NOT_SET) g_CEMVtrns.wait_for_card(CARD_APP_CARD_ICC, 100);
                    APDU_ret = 0;
				#ifdef VFI_PLATFORM_VERIXEVO    //iq_audi_030217
                    if((APDU_ret = EMV_CT_SmartISO(0, InAPDU_len, InAPDU_buf, &OutAPDU_len, OutAPDU_buf)) != EMV_ADK_OK)
				#else
                    if((APDU_ret = EMV_CT_SmartISO(0, InAPDU_len, InAPDU_buf, &OutAPDU_len, OutAPDU_buf,1024)) != EMV_ADK_OK)
				#endif
                    {
                        dlog_error("Reader initialization error %d", APDU_ret);
                        dlog_msg("%s", ADK_err(APDU_ret).c_str());
                        if (--num_tries > 0 && 
                              (APDU_ret == EMV_ADK_SMART_STATUS_REMOVED ||
                              APDU_ret == EMV_ADK_SMART_STATUS_EXCHG_ERR)
                           )
                        {
                            int icc = g_CEMVtrns.wait_for_card(CARD_APP_CARD_ICC, 100, g_CardAppConfig.GetWarmResetAfterICCOff());
                            if (icc & CARD_APP_EVT_CARD_IN)
                            {
                                g_CEMVtrns.icc_card_status = CRD_CODE_CARD_ICC_ENABLED;
                                continue;
                            }
                        }
                        if(isCardRemoved())
                        {
                            errorCode = RESP_CODE_CARD_REMOVED;
                            g_CEMVtrns.icc_card_status = 0;
                        }
                        else
                        {
                            errorCode = RESP_CODE_FAILED;
                        }
                        break;
                    }
                    else
                    {
                      
                      break;
                      
                    }
                  #ifdef VFI_PLATFORM_VERIXEVO
                    unsigned long scr_slot = CUSTOMER_CARD;
                    if((APDU_ret = Transmit_APDU(scr_slot, InAPDU_buf, InAPDU_len, OutAPDU_buf, &OutAPDU_len)) != CARDSLOT_SUCCESS)
                    {
                        dlog_msg("Sending APDU command to Customer Slot: RET: %d Response APDU len: %d", APDU_ret, OutAPDU_len);
                        OutAPDU_len = 0;
                        if ((APDU_ret == E_SLOT_INITIALIZE) )
                        {
                            dlog_alert("Slot must be initialized!");
                            APDU_ret = Init_CardSlot( scr_slot );
                            if (--num_tries > 0 && APDU_ret == CARDSLOT_SUCCESS)
                            {
                                g_CEMVtrns.wait_for_card(CARD_APP_CARD_ICC, 100, false, g_CardAppConfig.GetWarmResetAfterICCOff());
                                g_CEMVtrns.icc_card_status = CRD_CODE_CARD_ICC_ENABLED;
                                continue;
                            }
                            dlog_error("Cardslot initialization error %d", APDU_ret);
                        }
                        else if ((APDU_ret == E_ICC_RESET))
                        {
                            g_CEMVtrns.wait_for_card(CARD_APP_CARD_ICC, 100);
                            continue;
                        }
                        else if((APDU_ret == E_TRANSMIT))
                        {
                            g_CEMVtrns.wait_for_card(CARD_APP_CARD_ICC, 100);
                            continue;
                        }
                        // Just in case... 
                        else if (APDU_ret == CARD_ABSENT)
                        {
                          dlog_error("Card removed!");
                          errorCode = RESP_CODE_CARD_REMOVED;
                          break;
                        }
                        errorCode = RESP_CODE_FAILED;
                        break;
                    }
                    else
                    {
                        break;
                    }
                  #endif
                } while(true);
                dlog_msg("Sending APDU command to Customer Slot: RET: %d Response APDU len: %d", APDU_ret, OutAPDU_len);
            }
            
            if(OutAPDU_len > 0)
            {
                dlog_msg("Adding APDU response to the message - %d bytes SW1SW2: %02X%02X", OutAPDU_len, OutAPDU_buf[OutAPDU_len-2], OutAPDU_buf[OutAPDU_len-1]);
                if(useTmpl)
                {
                    response.addTag(ConstData(arbyTagOutAPDU_tmpl, OUT_APDU_TS), ConstData(OutAPDU_buf, OutAPDU_len));
                }
                else
                {
                    response.addTag(ConstData(arbyTagOutAPDU, OUT_APDU_TS), ConstData(OutAPDU_buf, OutAPDU_len));
                }
            }
        }
        if (flags && (errorCode != RESP_CODE_CARD_REMOVED))
        {
            if (isCardRemoved())
            {
                dlog_error("Card is removed!");
                if (!apduPresent) errorCode = RESP_CODE_CARD_REMOVED;
            }
            else if (g_CEMVtrns.icc_card_type != CARD_APP_CARDTYPE_EMV_ASYNC &&
                     g_CEMVtrns.icc_card_type != CARD_APP_CARDTYPE_EMV_SYNC &&
                     g_CEMVtrns.icc_card_type != CARD_APP_CARDTYPE_REQUIRES_POWERUP)
            {
                dlog_error("No valid card detected!");
                if (!apduPresent) errorCode = RESP_CODE_INVALID_REQ;
            }
            else
            {
                if (flags == APDU_FLAGS_CLOSE_ICC_READER)
                {
                    dlog_msg("Closing ICC reader");
                    #ifdef VFI_PLATFORM_VERIXEVO
                    Terminate_CardSlot(g_CEMV.ICC_reader, SWITCH_OFF_CARD);
                    Close_CardSlot(g_CEMV.ICC_reader);
                    #endif
                    #ifdef VFI_PLATFORM_VOS
                    EMV_CT_SmartPowerOff(0);
                    //g_CEMVtrns.Close_Kernel();
                    g_CEMVtrns.icc_card_type = CARD_APP_CARDTYPE_NOT_SET;
                    #endif
                    if (!apduPresent) errorCode = RESP_CODE_SUCCESS;
                    g_CEMVtrns.icc_card_status = CRD_CODE_CARD_ICC_DISABLED;
                    g_CEMVtrns.icc_card_type = CARD_APP_CARDTYPE_REQUIRES_POWERUP;
                    g_CEMV.SetPINStatus(PIN_ENTRY_STATUS_NOT_SET);
                }
                else if (flags == APDU_FLAGS_WARM_RESET_ICC_READER || flags == APDU_FLAGS_COLD_RESET_ICC_READER)
                {
                    dlog_msg("%s ICC reset", flags == APDU_FLAGS_WARM_RESET_ICC_READER ? "warm" : "cold");
                    // GVR requirement: When cold reset is required and card is still powered, it should be powered off first
                    if (g_CEMVtrns.icc_card_type != CARD_APP_CARDTYPE_REQUIRES_POWERUP && flags == APDU_FLAGS_COLD_RESET_ICC_READER)
                    {
                        #ifdef VFI_PLATFORM_VERIXEVO
                        Terminate_CardSlot(g_CEMV.ICC_reader, SWITCH_OFF_CARD);
                        Close_CardSlot(g_CEMV.ICC_reader);
                        #endif
                        #ifdef VFI_PLATFORM_VOS
                        EMV_CT_SmartPowerOff(0);
                        #endif
                    }
                    int waitFlags = WAIT_FOR_CARD_SKIP_KERNEL_REINIT;
                    if (flags == APDU_FLAGS_WARM_RESET_ICC_READER) waitFlags |= WAIT_FOR_CARD_WARM_RESET;
                    int ret = g_CEMVtrns.wait_for_card(CARD_APP_CARD_ICC, 100, true, waitFlags); // get atr
                    dlog_msg("After reset wait_for_card returned flags 0x%x", ret);
                    if (ret & CARD_APP_EVT_CARD_IN)
                    {
                        g_CEMVtrns.icc_card_status = CRD_CODE_CARD_ICC_ENABLED;
                        g_CEMV.SetPINStatus(PIN_ENTRY_STATUS_NOT_SET);
                        if (!apduPresent) errorCode = RESP_CODE_SUCCESS;
                    }
                    else if (ret & CARD_APP_EVT_CARD_OUT)
                    {
                        if (!apduPresent) errorCode = RESP_CODE_CARD_REMOVED;
                    }
                    else
                    {
                        if (!apduPresent) errorCode = RESP_CODE_FAILED;
                    }
                }
            }
        }
        
        response.addTag(ConstData(arbyTagCode, CODE_TS), MakeBERValue(errorCode));

        if( IPCPACKET_SUCCESS!= response.send() )
        {
            iRet =  ERESPONSESEND;
        }
        char amtDsp[CARD_APP_MAX_APDU*2];

        #ifdef VFI_PLATFORM_VOS
        time_end_l = getTimeStampMs();
        g_logger.Log("APDU COMMAND execution end: %08.08ldms", time_end_l - time_start_l);
        dlog_msg("APDU COMMAND execution end: %08.08ldms", time_end_l - time_start_l);
        g_CEMVtrns.wait_flags = CARD_APP_CARD_ICC | CARD_APP_CARD_MAGNETIC;
        #endif
        EMVlog("***APDU IN:");
        com_verifone_pml::svcHex2Dsp((const char *)InAPDU_buf, InAPDU_len, amtDsp, sizeof(amtDsp));
        amtDsp[InAPDU_len*2] = 0;
        EMVlog(amtDsp);
        EMVlog("***APDU OUT:");
        com_verifone_pml::svcHex2Dsp((const char *)OutAPDU_buf, OutAPDU_len, amtDsp, sizeof(amtDsp));
        amtDsp[OutAPDU_len*2] = 0;
        EMVlog(amtDsp);
        return iRet;
    }

/**
 * @}
 */

/***************************************************************************
 * Module namspace: end
 **************************************************************************/
}

