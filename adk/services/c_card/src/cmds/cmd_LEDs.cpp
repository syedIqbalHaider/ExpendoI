#ifndef __cplusplus
#error "This file is for C++ only!"
#endif
/*****************************************************************************
 *
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/
/**
 * @file        cmd_LEDs.cpp
 *
 * @author      Jacek Olbrys
 *
 * @brief       LEDs driving Command
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C++ Coding
 *              Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/

#include <errno.h>

#include <tlv-lite/ConstData.h>
#include <tlv-lite/ValueConverter.hpp>
#include <tlv-lite/ConstValue.hpp>
#include <libipcpacket/ipcpacket.hpp>

#include <liblog/logsys.h>

#include "cCARDAppConfig.h"
#include "cEMV.h"
#include "EMVtools.h"
#include "cTransaction.h"
#include "tlv_parser.h"

#include <libpml/pml_abstracted_api.h>
#include <libpml/pml_port.h>


#include "cmd/cmd_LEDs.h"

#ifdef VFI_PLATFORM_VOS
extern cLEDsHandler g_leds;
#endif

void EMVlog(char const * msg);


/***************************************************************************
 * Using
 **************************************************************************/
using namespace com_verifone_emv;
using namespace com_verifone_ipcpacket;


extern CCardAppConfig   g_CardAppConfig;
extern CEMV         g_CEMV;
extern CEMVtrns     g_CEMVtrns;

/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
namespace com_verifone_cmd
{

/**
 * @addtogroup Card
 * @{
 */

   /***************************************************************************
    * Exportable class members' definitions
    **************************************************************************/

   c_card_error_t CLEDsCmd::isCanceled(IPCPacket &response)
   {
      CCardAppConfig::cmd_break_e ret = g_CardAppConfig.checkCmdBreak(); //check break status
      uint8_t errorCode;   //error code send to sender application
      c_card_error_t returnCode;  //function return code

      switch( ret )
      {
      case CCardAppConfig::CMD_BREAK_NO_BREAK_E: //no break
         return ESUCCESS;
      case CCardAppConfig::CMD_BREAK_CANCEL_E:   //command canceled
         errorCode  = RESP_CODE_CMD_CANCEL;
         returnCode = ECANCEL;
         break;
      case CCardAppConfig::CMD_BREAK_TIMEOUT_E:  //timeout
         errorCode  = RESP_CODE_TIMEOUT;
         returnCode = EPOSTIMEOUT;
         break;
      case CCardAppConfig::CMD_BREAK_SEQ_ERROR_E:
         errorCode  = RESP_CODE_CMD_SEQ_ERROR;
         returnCode = ESEQERR;
         break;
      default:
         return ESUCCESS;
      }

      //prepare response message
      response.addTag(com_verifone_TLVLite::ConstData(arbyTagCode, CODE_TS), com_verifone_TLVLite::MakeBERValue(errorCode));
      if( IPCPACKET_SUCCESS!= response.send() )
      {
         return ERESPONSESEND;
      }
      return returnCode;
   }


  c_card_error_t CLEDsCmd::handleCommand(IPCPacket *pack)
  {
        using namespace com_verifone_TLVLite;
        IPCPacket  response; //response message initialization
        uint8_t   errorCode  = RESP_CODE_SUCCESS;   //error code which will be send to sender application
        ConstData value(ConstData::getInvalid());
        unsigned int flags = 0;
        unsigned char operation = 0;
        int timeout = 0; // DEFAULT value, infinite timeout
        int duration;
        c_card_error_t       iRet = ESUCCESS;


        dlog_msg("*****LEDs*****");
        EMVlog("Processing LEDs command");


        response.setCmdCode(pack->getCmdCode());
        response.setAppName(pack->getAppName());


        if (IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagOperations, OPERATIONS_TS), value))
        {
            ValueConverter<unsigned char>conv(value);
            operation = conv.getValue(0);
            dlog_msg("LED operation %Xh", operation);
        }
        else
        {
            dlog_alert("Missing operation!");
            errorCode = RESP_CODE_FAILED;
        }

        if (IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagFlags, FLAGS_TS), value))
        {
            ValueConverter<unsigned int>conv(value);
            flags = conv.getValue(0);
            dlog_msg("LED flags %u", flags);
        }
        else
        {
            dlog_alert("Missing flags!");
            errorCode = RESP_CODE_FAILED;
        }
        if (operation == LED_BLINK)
        {
            if (IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagLEDRepeats, LED_REPEATS_TS), value))
            {
                if (value.getSize() == 4)
                {
                    ValueConverter<int> valueConv(value);
                    timeout = valueConv.getValue(0);
                }
                else
                {
                    dlog_alert("Invalid LED timeout, assuming %d!", timeout);
                }
            }
            else
            {
                dlog_alert("Missing LED timeout, assuming %d", timeout);
            }
            if (IPCPACKET_SUCCESS == pack->getTag(ConstData(arbyTagLEDDuration, LED_DURATION_TS), value))
            {
                if (value.getSize() == 4)
                {
                    ValueConverter<int> valueConv(value);
                    duration = valueConv.getValue(0);
                }
                else
                {
                    dlog_alert("Invalid LED duration!");
                    errorCode = RESP_CODE_FAILED;
                }
            }
            else
            {
                dlog_error("Missing LED duration!");
                errorCode = RESP_CODE_FAILED;
            }
        }


        if (errorCode == RESP_CODE_SUCCESS)
        {
            #ifdef VFI_PLATFORM_VOS
            int res = 0;
            if (operation == LED_DISABLE || operation == LED_ENABLE)
            {
                // Turn LEDs or on off
                res = g_leds.changeState(flags, operation, true);
            }
            else if (operation == LED_BLINK)
            {
                res = g_leds.blink(flags, duration, timeout, true);
            }
            else
            {
                dlog_error("Invalid operation %d", operation);
                errorCode = RESP_CODE_INVALID_CMD;
            }
            if (res != 0)
            {
                dlog_error("Call failed, error %d, errno %d", res, errno);
                if (errno == EINVAL) errorCode = RESP_CODE_INVALID_REQ;
                else if (errno == EACCES) errorCode = RESP_CODE_INVAL;
                else errorCode = RESP_CODE_API_REQ;
            }
            #endif
            #ifdef VFI_PLATFORM_VERIXEVO
            dlog_error("Command is unsupported!");
            errorCode = RESP_CODE_INVALID_CMD;
            #endif
        }


        response.addTag(ConstData(arbyTagCode, CODE_TS), MakeBERValue(errorCode));

        if( IPCPACKET_SUCCESS!= response.send() )
        {
            iRet =  ERESPONSESEND;
        }

        
        
        return iRet;
    }

/**
 * @}
 */

/***************************************************************************
 * Module namspace: end
 **************************************************************************/
}

