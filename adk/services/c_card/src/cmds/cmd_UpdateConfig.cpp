#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 *
 * Copyright (C) 2007 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @file       cmd_APDU.cpp
 *
 *
 * @remarks    This file should be compliant with Verifone EMEA R&D C++ Coding
 *             Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/

#include <tlv-lite/ConstData.h>
#include <tlv-lite/ValueConverter.hpp>
#include <tlv-lite/ConstValue.hpp>
#include <libipcpacket/ipcpacket.hpp>


#include "cCARDAppConfig.h"
#include "cEMV.h"
#include "cTransaction.h"
#include "tlv_parser.h"

#include "liblog/logsys.h"

#include "cmd/cmd_UpdateConfig.h"



/***************************************************************************
 * Using
 **************************************************************************/
using namespace com_verifone_ipcpacket;
using namespace com_verifone_emv;

extern CCardAppConfig   g_CardAppConfig;
extern CEMV			g_CEMV;
extern CEMVtrns		g_CEMVtrns;


/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
namespace com_verifone_cmd
{


   c_card_error_t CUpdateConfigCmd::isCanceled(IPCPacket &response)
   {
      CCardAppConfig::cmd_break_e ret = g_CardAppConfig.checkCmdBreak(); //check break status
      uint8_t errorCode;   //error code send to sender application
      c_card_error_t returnCode;  //function return code

      switch( ret )
      {
      case CCardAppConfig::CMD_BREAK_NO_BREAK_E: //no break
         return ESUCCESS;
      case CCardAppConfig::CMD_BREAK_CANCEL_E:   //command canceled
         errorCode  = RESP_CODE_CMD_CANCEL;
         returnCode = ECANCEL;
         break;
      case CCardAppConfig::CMD_BREAK_TIMEOUT_E:  //timeout
         errorCode  = RESP_CODE_TIMEOUT;
         returnCode = EPOSTIMEOUT;
         break;
      case CCardAppConfig::CMD_BREAK_SEQ_ERROR_E:
         errorCode  = RESP_CODE_CMD_SEQ_ERROR;
         returnCode = ESEQERR;
         break;
      default:
         return ESUCCESS;
      }


      //prepare response message
      response.addTag(com_verifone_TLVLite::ConstData(arbyTagCode, CODE_TS), com_verifone_TLVLite::MakeBERValue(errorCode));

      if( IPCPACKET_SUCCESS != response.send() )
      {
         return ERESPONSESEND;
      }

      return returnCode;
   }

  c_card_error_t CUpdateConfigCmd::handleCommand(IPCPacket *pack)
  {
		
		IPCPacket  response; //response message initialization
		uint8_t   errorCode  = RESP_CODE_SUCCESS;   //error code which will be send to sender application
	   	c_card_error_t   returnCode = ESUCCESS;   //function return code
		int iRet = 0;
		uint8_t   OutTLVdata_buf[CARD_APP_MAX_TLV];
		c_card_error_t ret;
		uint16_t length;
		
		int       *record;

		g_CEMVtrns.monitor_flags &= 0xFD;

		dlog_msg("*****UPDATE_CONFIG_COM*****");

		response.setCmdCode(pack->getCmdCode());
		response.setAppName(pack->getAppName());

		dlog_error("COMMAND NOT IMPLEMENTED!");
		
		response.addTag(com_verifone_TLVLite::ConstData(arbyTagCode, CODE_TS), com_verifone_TLVLite::MakeBERValue(errorCode));
		

		return ESUCCESS;
	}

}



