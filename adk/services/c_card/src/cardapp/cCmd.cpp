#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 * 
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/
/**
 * @file       cCmd.cpp
 *
 * @author     Jacek Olbrys
 * 
 * @brief      Implementation of the base command class.
 *
 *
 * @remarks    This file should be compliant with Verifone EMEA R&D C++ Coding
 *             Standard 1.0.x  
 */

/***************************************************************************
 * Includes
 **************************************************************************/

#include <tlv-lite/ConstData.h>
#include <tlv-lite/ConstValue.hpp>

#include <liblog/logsys.h>

#include "cCmd.h"

#include "cCARDAppConfig.h"
#include "cEMV.h"
#include "cTransaction.h"
#include "tlv_parser.h"
#include "CARDApp.h"


/***************************************************************************
 * Using
 **************************************************************************/
using namespace com_verifone_ipcpacket;
using namespace com_verifone_emv;
extern CCardAppConfig   g_CardAppConfig;

/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
namespace com_verifone_cmd
{

/**
 * @addtogroup Cmd
 * @{ 
 */

/***************************************************************************
 * Exportable function definitions
 **************************************************************************/

/***************************************************************************
 * Exportable class members' definitions
 **************************************************************************/

c_card_error_t CCmd::handleCommand(IPCPacket *pack)
{
    uint8_t  errorCode  = RESP_CODE_UNKNOWN_CMD; //error code which will be send
    IPCPacket response;   //initialization of respsonse message

    response.setCmdCode(pack->getCmdCode());
    response.setAppName(pack->getAppName());
    response.addTag(com_verifone_TLVLite::ConstData(arbyTagCode, CODE_TS), com_verifone_TLVLite::MakeBERValue(errorCode));

    if( IPCPACKET_SUCCESS!= response.send() ) //sending response message
    {
        return ERESPONSESEND;
    }

    return ESUCCESS;
}

c_card_error_t CCmd::isCanceled(IPCPacket &response)
{
    CCardAppConfig::cmd_break_e ret = g_CardAppConfig.checkCmdBreak(); //get break status
    uint8_t errorCode;   //error code which will be send
    c_card_error_t returnCode;  //function return code

    switch( ret )  //check break status
    {
        case CCardAppConfig::CMD_BREAK_NO_BREAK_E:  //no break
            return ESUCCESS;
        case CCardAppConfig::CMD_BREAK_CANCEL_E:    //cancel arrived
            errorCode  = RESP_CODE_CMD_CANCEL;
            returnCode = ECANCEL; 
            break;
        case CCardAppConfig::CMD_BREAK_TIMEOUT_E:   //timeout occurs
            errorCode  = RESP_CODE_TIMEOUT;
            returnCode = EPOSTIMEOUT;
            break;
        case CCardAppConfig::CMD_BREAK_SEQ_ERROR_E:
            errorCode  = RESP_CODE_CMD_SEQ_ERROR;
            returnCode = ESEQERR;
            break;
        default:
            return ESUCCESS;
    }

    //prepare response message
    response.addTag(com_verifone_TLVLite::ConstData(arbyTagCode, CODE_TS), com_verifone_TLVLite::MakeBERValue(errorCode));

    if( IPCPACKET_SUCCESS != response.send() ) //sending response message
    {
        return ERESPONSESEND;
    }

    return returnCode;
}

/**
 * @}
 */

/***************************************************************************
 * Module namspace: end
 **************************************************************************/
}

