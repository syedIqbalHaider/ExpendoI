#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 * 
 * Copyright (C) 2007 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/
    /**
     * @file        cEMVmain.cpp
     *
     * @author      Jacek Olbrys
     *
     * @brief       EMV Main file
     *
     * @remarks     This file should be compliant with Verifone EMEA R&D C++ Coding
     *              Standard 1.0.x
     */

/***************************************************************************
 * Includes
 **************************************************************************/
#include <ctype.h>

#include <liblog/logsys.h>

#include "cEMVmain.h"
#include "tlv_parser.h"

/***************************************************************************
 * Using
 **************************************************************************/

/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
namespace com_verifone_cmd
{

/**
 * @addtogroup Cmd
 * @{
 */

/***************************************************************************
 * Exportable function definitions
 **************************************************************************/
void CCmdEMVmain::strtolower( char *pszString )
{
    int i;

    if( pszString == NULL )
        return;

    for( i=0; i < strlen(pszString); ++i )
    {
        pszString[i] = tolower( pszString[i] );
    }
}

/***************************************************************************
 * Exportable class members' definitions
 **************************************************************************/

c_card_error_t CCmdEMVmain::handleCommand(IPCPacket *pack)
{
    CCmd *pobCmd = NULL;
    c_card_error_t errorCode;

    switch(pack->getCmdCode())
    {
        case INSTALL_VSS_COM:
            pobCmd = new CInitVSSCmd;
            break;

        case EXECUTE_VSS_MACRO_COM:
            pobCmd = new CExecuteMacroCmd;
            break;

        case START_WAITING_COM:
            pobCmd = new CStartWaitCmd;
            break;

        case MONITOR_ICC_COM:
            pobCmd = new CMonitorICCCmd; 
            break;

        case UPDATE_EMV_CONFIG_COM:
            pobCmd = new CMonitorICCCmd; 
            break;

        case UNSOLICITED_EVENT_COM:
            pobCmd = new CUnsolicitedCmd;
            break;

        case SEND_APDU_COM:
            pobCmd = new CAPDUCmd; 
            break;

        case SET_GET_TLV_COM:
            pobCmd = new CSuperEMVCmd(CARD_APP_OPER_TLV);
            break;

        case SET_PIN_PARAMS_COM:
            pobCmd = new CsetPINparamsCmd;
            break;

        case VERIFY_PIN_COM:
            pobCmd = new CVerifyPINCmd;
            break;

        case SET_MESSAGE_COM:
            pobCmd = new CSetMessagesCmd;
            break;

        case L1_BEGIN_TRANSACTION_COM:
            pobCmd = new CSuperEMVCmd(CARD_APP_OPER_SELECT | CARD_APP_OPER_GPO | CARD_APP_OPER_READ | CARD_APP_OPER_DATA_AUTH |
            CARD_APP_OPER_TRM | CARD_APP_OPER_VERIFY | CARD_APP_OPER_FIRST_AC);
            break;

        case L1_COMPLETE_TRN_COM:
            pobCmd = new CSuperEMVCmd(CARD_APP_OPER_EXT_AUTH | CARD_APP_OPER_SCRIPT | CARD_APP_OPER_SECOND_AC);
            break;

        case L2_SELECT_READ_COM:
            pobCmd = new CSuperEMVCmd(CARD_APP_OPER_SELECT | CARD_APP_OPER_GPO | CARD_APP_OPER_READ);
            break;

        case L2_TRM_VERIFY_COM:
            pobCmd = new CSuperEMVCmd(CARD_APP_OPER_DATA_AUTH | CARD_APP_OPER_TRM | CARD_APP_OPER_VERIFY);
            break;

        case L2_FIRST_AC_COM:
            pobCmd = new CSuperEMVCmd(CARD_APP_OPER_FIRST_AC);
            break;

        case L2_COMPLETE_COM:
            pobCmd = new CSuperEMVCmd(CARD_APP_OPER_EXT_AUTH | CARD_APP_OPER_SCRIPT | CARD_APP_OPER_SECOND_AC);;
            break;

        case L3_SELECT_COM:
            pobCmd = new CSuperEMVCmd(CARD_APP_OPER_SELECT);
            break;

        case L3_GPO_COM:
            pobCmd = new CSuperEMVCmd(CARD_APP_OPER_GPO);
            break;

        case L3_READ_DATA_COM:
            pobCmd = new CSuperEMVCmd(CARD_APP_OPER_READ);
            break;

        case L3_CARDHOLDER_VERIFY_COM:
            pobCmd = new CSuperEMVCmd(CARD_APP_OPER_VERIFY);
            break;

        case L3_TRM_COM:
            pobCmd = new CSuperEMVCmd(CARD_APP_OPER_TRM);
            break;

        case L3_DATA_AUTH_COM:
            pobCmd = new CSuperEMVCmd(CARD_APP_OPER_DATA_AUTH);
            break;

        case L3_FIRST_AC_COM:
            pobCmd = new CSuperEMVCmd(CARD_APP_OPER_FIRST_AC);
            break;

        case L3_EXT_AUTH_COM:
            pobCmd = new CSuperEMVCmd(CARD_APP_OPER_EXT_AUTH);
            break;

        case L3_SCRIPT_PROCESSING_COM:
            pobCmd = new CSuperEMVCmd(CARD_APP_OPER_SCRIPT);
            break;

        case L3_SECOND_AC_COM:
            pobCmd = new CSuperEMVCmd(CARD_APP_OPER_SECOND_AC);
            break;

        case SUPER_EMV_COM:
            pobCmd = new CSuperEMVCmd;
            break;

#ifdef VFI_PLATFORM_VERIXEVO
       case MEM_READ_I2C_CARD:
            pobCmd = new CReadI2C;
            break;

      case MEM_WRITE_I2C_CARD:
            pobCmd = new CWriteI2C;
            break;

       case MEM_READ_MEMORY_CARD:
            pobCmd = new CReadMemory;
            break;

      case MEM_WRITE_MEMORY_CARD:
            pobCmd = new CWriteMemory;
            break;

      case MEM_UPDATE_MEMORY_CARD:
            pobCmd = new CUpdateMemory;
            break;
#endif
#ifdef VFI_PLATFORM_VOS
      case LEDS_CONTROL_COM:
            pobCmd = new CLEDsCmd;
            break;
#endif
        case RESET_EMV_CONFIG:
            pobCmd = new CResetCmd;
            break;


        default:
            dlog_alert("Command not supported %X", pack->getCmdCode());
            return EHANDCOMMERROR;
    }

    if(pobCmd == NULL)
    {
        dlog_msg("CARDAPP error> MEMORY ALLOCATION FAILED!");
        return ECANCEL;
    }

    errorCode = pobCmd->handleCommand(pack);

    if ( errorCode != ESUCCESS )
    {
       dlog_alert("command number %X returned: %X", pack->getCmdCode(), errorCode);
    }

    delete pobCmd;
    return errorCode;
}

/**
 * @}
 */

/***************************************************************************
 * Module namspace: end
 **************************************************************************/
}

