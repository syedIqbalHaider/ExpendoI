/***************************************************************************
 *
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 *
 **************************************************************************/
/**
 * @file        tlv_parser.cpp
 *
 * @author      Jacek Olbrys
 *
 * @brief       TLV parser
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C++ Coding
 *              Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/
#include <svc.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <liblog/logsys.h>

#include "tlv_parser.h"

/***************************************************************************
 * Global variables
 **************************************************************************/
uint8_t arbyTagActionCode         [ACTION_CODE_TS]         = { 0xDF, 0x2A };
uint8_t arbyTagAddScreenText      [ADD_SCREEN_TEXT_TS]     = { 0xDF, 0x81, 0x02 };
uint8_t arbyTagAddScreenText2     [ADD_SCREEN_TEXT_2_TS]   = { 0xDF, 0x81, 0x12 };
uint8_t arbyTagCode               [CODE_TS]                = { 0xDF, 0x30 };
uint8_t arbyTagDataAlgorithm      [DATA_ALGORITHM_TS]      = { 0xDF, 0x81, 0x0B };
uint8_t arbyTagHostId             [HOST_ID_TS]             = { 0xDF, 0x23 };
uint8_t arbyTagKeyData            [KEY_DATA_TS]            = { 0xDF, 0x2E };
uint8_t arbyTagKeyManagement      [KEY_MANAGEMENT_TS]      = { 0xDF, 0x81, 0x09 };
uint8_t arbyTagKeyType            [KEY_TYPE_TS]            = { 0xDF, 0x46 };
uint8_t arbyTagKSN                [KSN_TS]                 = { 0xDF, 0x81, 0x03 };
uint8_t arbyTagLock               [LOCK_TS]                = { 0xDF, 0x81, 0x10 };
uint8_t arbyTagMAC                [MAC_TS]                 = { 0xDF, 0x7B };
uint8_t arbyTagMaxPINLength       [MAX_PIN_LENGTH_TS]      = { 0xDF, 0x81, 0x05 };
uint8_t arbyTagMessForDec         [MESS_FOR_DEC_TS]        = { 0xDF, 0x81, 0x11 };
uint8_t arbyTagMessForEnc         [MESS_FOR_ENC_TS]        = { 0xDF, 0x0F };
uint8_t arbyTagMessageForMAC      [MESS_FOR_MAC_TS]        = { 0xDF, 0x0F };
uint8_t arbyTagMinPINLength       [MIN_PIN_LENGTH_TS]      = { 0xDF, 0x81, 0x04 };
uint8_t arbyTagOnlinePINCipher    [ONLINE_PIN_CIPHER_TS]   = { 0xDF, 0x6C };
uint8_t arbyTagPan                [PAN_TS]                 = { 0xDF, 0x36 };
uint8_t arbyTagPinAlgorithm       [PIN_ALGORITHM_TS]       = { 0xDF, 0x81, 0x0A };
uint8_t arbyTagPinBlockFormat     [PIN_BLOCK_FORMAT_TS]    = { 0xDF, 0x81, 0x08 };
uint8_t arbyTagPinCancel          [PIN_CANCEL_TS]          = { 0xDF, 0x81, 0x07 };
uint8_t arbyTagPinEntryTimeout    [PIN_ENTRY_TIMEOUT_TS]   = { 0xDF, 0x81, 0x06 };
uint8_t arbyTagPinEntryType       [PIN_ENTRY_TYPE_TS]      = { 0xDF, 0x7D };
uint8_t arbyTagPinTryFlag         [PIN_TRY_FLAG_TS]        = { 0xDF, 0x05 };
uint8_t arbyTagPlainTxtPinBlock   [PLAIN_TXT_PIN_BLOCK_TS] = { 0xDF, 0x81, 0x07 };
uint8_t arbyTagPosTimeout         [POS_TIMEOUT_TS]         = { 0xDF, 0x1F };
uint8_t arbyTagScriptName         [SCRIPT_NAME_TS]         = { 0xDF, 0x81, 0x0C };
uint8_t arbyTagStan               [STAN_TS]                = { 0xDF, 0x7C };
uint8_t arbyTagTransAmount        [TRANS_AMOUNT_TS]        = { 0xDF, 0x17 };
uint8_t arbyTagTransCatExp        [TRANS_CAT_EXP_TS]       = { 0xDF, 0x1C };
uint8_t arbyTagTransCurrCode      [TRANS_CURR_CODE_TS]     = { 0xDF, 0x24 };
uint8_t arbyTagTransCurrExp       [TRANS_CURR_EXP_TS]      = { 0xDF, 0x1C };
uint8_t arbyTagFlags              [FLAGS_TS]               = { 0xDF, 0x81, 0x0D };
uint8_t arbyTagMessagePIN           [MSG_ENTER_PIN_TS]          = {0xDF, 0x81, 0x1A};
uint8_t arbyTagMessageAmount        [MSG_AMOUNT_TS]             = {0xDF, 0x81, 0x1B};
uint8_t arbyTagFontFile             [MSG_FONT_FILE_TS]          = {0xDF, 0x81, 0x1c};
uint8_t arbyTagMonitorPeriod        [MONITOR_PERIOD_TS]         = {0xDF, 0x81, 0x1d};

uint8_t arbyTagInTLV                [IN_TLV_TS]                 = {0xFF, 0xCC, 0x10};
uint8_t arbyTagOutTLVlist           [OUT_TLV_LIST_TS]           = {0xDF, 0xCC, 0x11};
uint8_t arbyTagChangeStatus         [CHANGE_STATUS_TS]          = {0xDF, 0xCC, 0x55};
uint8_t arbyTagProcStatus           [PROC_STATUS_TS]            = {0xDF, 0xCC, 0x01};
uint8_t arbyTagOutTLV               [OUT_TLV_TS]                = {0xFF, 0xCC, 0x11};
uint8_t arbyTagOperations           [OPERATIONS_TS]             = {0xDF, 0xCC, 0x56};
uint8_t arbyTagSelect               [SELECT_TS]                 = {0xDF, 0xCC, 0x57};
uint8_t arbyTagProcFlag             [PROC_FLAG_TS]              = {0xDF, 0xCC, 0x58};
uint8_t arbyTagConfRecord           [CONF_RECORD_TS]            = {0xDF, 0xCC, 0x59};
uint8_t arbyTagAuthResult           [AUTH_RESULT_TS]            = {0xDF, 0xCC, 0x60};
uint8_t arbyTagATR                  [ATR_TS]                    = {0xDF, 0xCC, 0x61};
uint8_t arbyTagPINtimeout           [PIN_TIMEOUT_TS]            = {0xDF, 0xCC, 0x62};
uint8_t arbyTagEvent                [EVENT_TS]                  = {0xDF, 0xCC, 0x63};
uint8_t arbyTagCardBlacklisted      [CARD_BLACKLISTED_TS]       = {0xDF, 0xCC, 0x64};
uint8_t arbyTagPINfirstcharTimeout  [PIN_FIRSTCHAR_TIMEOUT_TS]  = {0xDF, 0xCC, 0x65};
uint8_t arbyTagPINintercharTimeout  [PIN_INTERCHAR_TIMEOUT_TS]  = {0xDF, 0xCC, 0x66};
uint8_t arbyTagVSSname              [VSS_NAME_TS]               = {0xDF, 0xCC, 0x70};
uint8_t arbyTagVSSslot              [VSS_SLOT_TS]               = {0xDF, 0xCC, 0x71};
uint8_t arbyTagVSSmacro             [VSS_MACRO_TS]              = {0xDF, 0xCC, 0x72};
uint8_t arbyTagVSSIn                [VSS_IN_TS]                 = {0xDF, 0xCC, 0x73};
uint8_t arbyTagVSSOut               [VSS_OUT_TS]                = {0xDF, 0xCC, 0x74};
uint8_t arbyTagTrack1               [MAG_TRACK_TS]              = {0xDF, 0xCC, 0x41};
uint8_t arbyTagTrack2               [MAG_TRACK_TS]              = {0xDF, 0xCC, 0x42};
uint8_t arbyTagTrack3               [MAG_TRACK_TS]              = {0xDF, 0xCC, 0x43};
uint8_t arbyTagCardType             [CARDTYPE_TS]               = {0xDF, 0xCC, 0x44};
uint8_t arbyTagPINResult            [PIN_RESULT_TS]             = {0xDF, 0xCC, 0x45};
uint8_t arbyTagPINhtml              [PIN_HTML_TS]               = {0xDF, 0xCC, 0x48};
uint8_t arbyTagLEDDuration          [LED_DURATION_TS]           = {0xDF, 0xCC, 0x46};
uint8_t arbyTagLEDRepeats           [LED_REPEATS_TS]            = {0xDF, 0xCC, 0x47};
uint8_t arbyTagVSSResult            [VSS_RESULT_TS]             = {0xDF, 0xCC, 0x75};
uint8_t arbyTagInAPDU               [IN_APDU_TS]                = {0xDF, 0xCC, 0x20};
uint8_t arbyTagOutAPDU              [OUT_APDU_TS]               = {0xDF, 0xCC, 0x21};
uint8_t arbyTagInAPDU_tmpl          [IN_APDU_TS]                = {0xFF, 0xCC, 0x20};
uint8_t arbyTagOutAPDU_tmpl         [OUT_APDU_TS]               = {0xFF, 0xCC, 0x21};

uint8_t arbyTagCustLang             [CUST_LANG_TS]              = {0xFF, 0xCC, 0x76};

uint8_t arbyTagHTML_PIN_OK          [HTML_PIN_TS]               = {0xDF, 0xCC, 0x4A};
uint8_t arbyTagHTML_PIN_Incorrect   [HTML_PIN_TS]               = {0xDF, 0xCC, 0x4C};
uint8_t arbyTagHTML_PIN_LastTry     [HTML_PIN_TS]               = {0xDF, 0xCC, 0x4E};
uint8_t arbyTagHTML_PIN_OK_Params   [HTML_PIN_PARAMS_TS]        = {0xDF, 0xCC, 0x4B};
uint8_t arbyTagHTML_PIN_Incorrect_Params  [HTML_PIN_PARAMS_TS]  = {0xDF, 0xCC, 0x4D};
uint8_t arbyTagHTML_PIN_LastTry_Params    [HTML_PIN_PARAMS_TS]  = {0xDF, 0xCC, 0x4F};

uint8_t arbyTagRadixSeparator       [RADIX_SEPARATOR_TS]        = {0xDF, 0xA2, 0x17};
uint8_t arbyTagPINStyle             [PIN_STYLE_TS]              = {0xDF, 0xA2, 0x18};
uint8_t arbyTagPINCurrSymbol        [PIN_CURR_SYMBOL_TS]        = {0xDF, 0xA2, 0x19};
uint8_t arbyTagPINCurrSymbolLoc     [PIN_CURR_SYMBOL_LOC_TS]    = {0xDF, 0xA2, 0x1A};
uint8_t arbyTagPINBeeperTimeout     [PIN_BEEPER_TIMEOUT_TS]     = {0xDF, 0xA2, 0x1B};
uint8_t arbyTagPINOfflinetype       [PIN_OFFLINE_TYPE_TS]       = {0xDF, 0xA2, 0x1C};
uint8_t arbyTagPinOfflineEntryType  [PIN_OFFLINE_ENTRY_TYPE_TS] = {0xDF, 0xA2, 0x1D};
uint8_t arbyTagPinMaxLen            [PIN_MAX_LEN_TS]            = {0xDF, 0xA2, 0x1E};
uint8_t arbyTagPinMinLen            [PIN_MIN_LEN_TS]            = {0xDF, 0xA2, 0x1F};
uint8_t arbyTagPinOnlineEntryType   [PIN_ONLINE_ENTRY_TYPE_TS]  = {0xDF, 0xA2, 0x20};
uint8_t arbyTagPinOnlineCancel      [PIN_ONLINE_CANCEL_TS]      = {0xDF, 0xA2, 0x21};
uint8_t arbyTagPinOnlineExtraMsg    [PIN_ONLINE_EXTRA_MSG_TS]   = {0xDF, 0xA2, 0x22};
uint8_t arbyTagParseExtraTags       [PARSE_EXTRA_TAGS_TS]       = {0xDF, 0xA2, 0x23};
uint8_t arbyTagExternalAppSelection [EXTERNAL_APP_SELECTION_TS] = {0xDF, 0xA2, 0x04};

uint8_t arbyTagHTMLextra			[HTML_EXTRA_TS]				= {0xDF, 0xA2, 0x24};

uint8_t arbyTagKernelChecksum       [KERNEL_CHECKSUM_TS]        = {0xDF, 0xDF, 0x05};
uint8_t arbyTagTACdefault           [EXTRA_DATA_TS]             = {0xDF, 0xDF, 0x06};
uint8_t arbyTagTACdenial            [EXTRA_DATA_TS]             = {0xDF, 0xDF, 0x07};
uint8_t arbyTagTAConline            [EXTRA_DATA_TS]             = {0xDF, 0xDF, 0x08};
uint8_t arbyTagROSThreshold         [EXTRA_DATA_TS]             = {0xDF, 0xDF, 0x0A};
uint8_t arbyTagROSTargetPercent     [EXTRA_DATA_TS]             = {0xDF, 0xDF, 0x0B};
uint8_t arbyTagROSMaxPercent        [EXTRA_DATA_TS]             = {0xDF, 0xDF, 0x0C};
uint8_t arbyTagContextData        	[EXTRA_DATA_TS]             = {0xDF, 0xCC, 0x77};


uint8_t arbyTagScriptRes71          [SCRIPT_RESULT_TS]          = {0xDF, 0xDF, 0x29};
uint8_t arbyTagScriptRes72          [SCRIPT_RESULT_TS]          = {0xDF, 0xDF, 0x2A};

uint8_t arbyTagSwipeStatus          [SWIPE_STATUS_TS]           = {0xDF, 0xDF, 0x6E};

uint8_t arbyTagMemoryCardAddress    [MEM_CARD_ADDRESS_TS]       = {0xDF, 0xCC, 0x30};
uint8_t arbyTagMemoryCardByteCount  [MEM_CARD_BYTE_COUNT_TS]    = {0xDF, 0xCC, 0x31};
uint8_t arbyTagMemoryCardData       [MEM_CARD_DATA_TS]          = {0xDF, 0xCC, 0x32};
uint8_t arbyTagMemoryCardPIN        [MEM_CARD_PIN_TS]           = {0xDF, 0xCC, 0x33};

uint8_t arbyTagShowIncorrectPIN     [SHOW_INCORRECT_PIN_TS]     = {0xDF, 0xCC, 0x34};

/* EMV tags, but needed locally */
uint8_t arbyAppLabel[APP_LABEL_TS] = { 0x50 };
uint8_t arbyAppPrefName[APP_PREF_NAME_TS] = { 0x9F, 0x12 };
uint8_t arbyAppAID[APP_AID_TS] = { 0x9F, 0x06 };
uint8_t arbyTagCAPKindex[CAPK_INDEX_TS] = {0x9F, 0x22};
uint8_t arbyAppPriority[APP_AID_PRIORITY_TS] = { 0x87 };

respCodeInfo responseCodes[] =
                           {
                              respCodeInfo("RESP_CODE_SUCCESS", RESP_CODE_SUCCESS),
                              respCodeInfo("RESP_CODE_FAILED", RESP_CODE_FAILED),
                              respCodeInfo("RESP_CODE_TIMEOUT", RESP_CODE_TIMEOUT),
                              respCodeInfo("RESP_CODE_PIN_CANCEL", RESP_CODE_PIN_CANCEL),
                              respCodeInfo("RESP_CODE_CMD_SEQ_ERROR", RESP_CODE_CMD_SEQ_ERROR),
                              respCodeInfo("RESP_CODE_RETRY_LIMIT", RESP_CODE_RETRY_LIMIT),
                              respCodeInfo("RESP_CODE_INVALID_REQ", RESP_CODE_INVALID_REQ),
                              respCodeInfo("RESP_CODE_API_REQ", RESP_CODE_API_REQ),
                              respCodeInfo("RESP_CODE_INVALID_CMD", RESP_CODE_INVALID_CMD),
                              respCodeInfo("RESP_CODE_INVALID_LOCAL_KEY_IDX", RESP_CODE_INVALID_LOCAL_KEY_IDX),
                              respCodeInfo("RESP_CODE_UPDATE_KEY_FAILED", RESP_CODE_UPDATE_KEY_FAILED),
                              respCodeInfo("RESP_CODE_TRANSPORT_KEYS_CHANGED", RESP_CODE_TRANSPORT_KEYS_CHANGED),
                              respCodeInfo("RESP_CODE_KEY_UPDATE_LIMIT", RESP_CODE_KEY_UPDATE_LIMIT),
                              respCodeInfo("RESP_CODE_INVALID_KEY", RESP_CODE_INVALID_KEY),
                              respCodeInfo("RESP_CODE_PIN_BLOCKED", RESP_CODE_PIN_BLOCKED),
                              respCodeInfo("RESP_CODE_PIN_GET_CHALLENGE_ERROR", RESP_CODE_PIN_GET_CHALLENGE_ERROR),
                              respCodeInfo("RESP_CODE_PIN_ENCIPHER_ERROR", RESP_CODE_PIN_ENCIPHER_ERROR),
                              respCodeInfo("RESP_CODE_PIN_VERIFY_FAILED", RESP_CODE_PIN_VERIFY_FAILED),
                              respCodeInfo("RESP_CODE_PIN_MISSING_PIN_BLOCK_DATA", RESP_CODE_PIN_MISSING_PIN_BLOCK_DATA),
                              respCodeInfo("RESP_CODE_INVALID_KEY_LENGTH", RESP_CODE_INVALID_KEY_LENGTH),
                              respCodeInfo("RESP_CODE_MISSING_TMK_OR_DUKPT_KEY", RESP_CODE_MISSING_TMK_OR_DUKPT_KEY),
                              respCodeInfo("RESP_CODE_MISSING_TPK_OR_TAK", RESP_CODE_MISSING_TPK_OR_TAK),
                              respCodeInfo("RESP_CODE_MISSING_STAN_OR_PAN", RESP_CODE_MISSING_STAN_OR_PAN),
                              respCodeInfo("RESP_CODE_CMD_BUSY", RESP_CODE_CMD_BUSY),
                              respCodeInfo("RESP_CODE_INVALID_APP_ID", RESP_CODE_INVALID_APP_ID),
                              respCodeInfo("RESP_CODE_POS_INVALID_FIELD_LEN", RESP_CODE_POS_INVALID_FIELD_LEN),
                              respCodeInfo("RESP_CODE_POS_MSG_LEN", RESP_CODE_POS_MSG_LEN),
                              respCodeInfo("RESP_CODE_UNKNOWN_CMD", RESP_CODE_UNKNOWN_CMD),
                              respCodeInfo("RESP_CODE_INVAL", RESP_CODE_INVAL),
                              respCodeInfo("RESP_CODE_CMD_CANCEL",RESP_CODE_CMD_CANCEL),
                              respCodeInfo("RESP_CODE_CMD_SEQ_ERROR",RESP_CODE_CMD_SEQ_ERROR),
                              respCodeInfo("RESP_CODE_SIZE_ERROR",RESP_CODE_SIZE_ERROR),
                              respCodeInfo("RESP_CODE_MAC_VER_FAILED",RESP_CODE_MAC_VER_FAILED),
                              respCodeInfo("RESP_CODE_MSG_SIZE_TO_LONG",RESP_CODE_MSG_SIZE_TO_LONG),
                              respCodeInfo(0, 0)
                           };

/***************************************************************************
 * Exportable function definitions
 **************************************************************************/
const char * respCodeString(uint8_t respCode)
{
   const char* returnString = 0;
   static char buff[30];

   for(int i = 0 ; ;++i)
   {
      if( responseCodes[i].m_respCodeString == 0 )
      {
         break;
      }

      if( responseCodes[i].m_respCode == respCode )
      {
         returnString = responseCodes[i].m_respCodeString;
         break;
      }
   }

   if(returnString == 0)
   {
      sprintf(buff, "RESP CODE UNKWNON %d", respCode);
      return buff;
   }
   else
   {
      return returnString;
   }
}



