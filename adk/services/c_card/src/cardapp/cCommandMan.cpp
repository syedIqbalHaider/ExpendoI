#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 * 
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/
/**
 * @file       CommmandMan.cpp
 *
 * @author     Jacek Olbrys
 * 
 * @brief      Implementation of the base command manager.
 *
 *
 * @remarks    This file should be compliant with Verifone EMEA R&D C++ Coding
 *             Standard 1.0.x  
 */

/***************************************************************************
 * Includes
 **************************************************************************/
#include <svc.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <liblog/logsys.h>

#include "cCommandMan.h"

/***************************************************************************
 * Using
 **************************************************************************/
using namespace com_verifone_ipcpacket;

/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
namespace com_verifone_cmd
{

/**
 * @addtogroup Cmd
 * @{ 
 */

/***************************************************************************
 * Exportable function definitions
 **************************************************************************/

/***************************************************************************
 * Exportable class members' definitions
 **************************************************************************/

int32_t CCommandMan::regCmd(uint8_t cmdID, CCmd *cmd)
{
    int32_t retVal = -1;

    dlog_msg("Registering command %X, index: %d", cmd, m_regIdx);

    if( m_regIdx < CMD_MAN_MAX_CMD )
    {
        m_regData[m_regIdx].cmdID = cmdID;
        m_regData[m_regIdx].cmdObj = cmd;
        m_regIdx ++;
        retVal = 0;
    }
    else
    {
        dlog_msg("RegCmd error: maximum of %d commands already reached", CMD_MAN_MAX_CMD );
    }
    return retVal;
}

int32_t CCommandMan::procExter(IPCPacket *pack)
{
    int32_t processed = 0;
    c_card_error_t ret = ESUCCESS;

    /* main processing */
    if( processed == 0 )
    {
        if( m_underProcCmd == NULL )
        {
            // we are not processing any other command now 
            CCmd* incomingCmd = getCmdObj( pack->getCmdCode() );
            if( incomingCmd )
            {
                // coresponding command object is registered 
                dlog_msg("Calling message handler...p:%X", incomingCmd);
                ret = incomingCmd->handleCommand( pack );

                if( ret == ENOTCOMPLETE ) {
                    m_underProcCmd = incomingCmd;
                }
            }
            else
            {
                dlog_msg("Cannot create object for command number %X", pack->getCmdCode() );
            }
        }
        else
        {  
            dlog_msg("command received while still processing previous cmd" );
            dlog_msg("underProcCmd=%d", m_underProcCmd );
        }
    }
    return ret;
}

CCmd* CCommandMan::getCmdObj(uint8_t cmdID)
{
    CCmd* cmd = NULL;

    //dlog_msg("Command ID: %d", cmdID);
    for( uint32_t i = 0; i < m_regIdx; i++)
    {
        if( m_regData[i].cmdID == cmdID )
        {
            cmd = m_regData[i].cmdObj;
            break;
        }
    }
    return cmd;
}

/**
 * @}
 */

/***************************************************************************
 * Module namspace: end
 **************************************************************************/
}

