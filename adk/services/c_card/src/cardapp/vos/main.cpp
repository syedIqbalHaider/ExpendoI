#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 *
 * Copyright (C) 2007 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/
/**
 * @file        main.cpp
 *
 * @author      Jacek Olbrys
 *
 * @brief       Card Service - The main module
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C++ Coding
 *              Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/

/*************************************
 Pulling in all required include files, in extern "C" as we don't
 know whether or not the files are CPP defintions or C functions */

//#include <algorithm>

#include <svc.h>
#include <stdio.h>
#include <unistd.h>


#include <liblog/logsys.h>
#include <fmtlog/fmtlog.hpp>

#include "cBaseConfig.h"
#include "cCARDAppConfig.h"
#include "tlv_parser.h"

#include <libpml/pml.h>
#include "libpml/pml_abstracted_api.h"
#include "libpml/pml_port.h"


#include "cEMV.h"
#include "cEMVmain.h"

#include "cIPCInterface.h"
#include <libipc/ipc.h>

#include "EMVtools.h"

#include <sys/socket.h>
#include <sys/types.h>
#include <sys/syscall.h>

#include <libpingui.h>


#ifdef VFI_GUI_DIRECTGUI
#  ifdef DIRECTGUI2
#    include <html/gui.h>
#  else
#    include <gui/gui.h>
#  endif
#  ifdef DIRECTGUI_PROXY
#    include <libguiclient/guiclient.h>
#  endif
#endif

#include <algorithm>
#include <pthread.h>
#include "cTransaction.h"
#include <msrDevice.h>

/***************************************************************************
 * Using
 **************************************************************************/
using namespace com_verifone_ipcpacket;
using namespace com_verifone_cmd;
using namespace com_verifone_emv;
using namespace com_verifone_interface;
using namespace com_verifone_ipc;
/***************************************************************************
 * Global Variables
 **************************************************************************/
CCardAppConfig  g_CardAppConfig;
CEMV            g_CEMV;
CEMVtrns        g_CEMVtrns;
cIPCInterface * pInterface = NULL;
cEMVcollection  g_EMVcollection;
logger          g_logger;
cLEDsHandler    g_leds;

extern int monitoring_thread;
const char apLogicalName[] = "CARDAPP";

#define ICC_READER_START "START"
#define ICC_READER_STOP "STOP"
#define MSR_DATA_READY "MSR_DATA_READY"

void EMVlog(char const * msg);
int main_loop(int argc, char *argv[]);



/***************************************************************************
 * Master Application - main()
 **************************************************************************/



void guiSetup(){
#ifdef VFI_GUI_DIRECTGUI
#ifdef DIRECTGUI_PROXY
    dlog_msg("Using GUIProxy");
    vfiguiproxy::GUIClient::instance().init(); 
    vfiguiproxy::GUIClient::instance().setLanguagesDir("cardapp/langs");
#else
    dlog_msg("Using DirectGUI");
    vfigui::uiReadConfig();

    std::string resourceDirectory(apLogicalName);
    std::transform(resourceDirectory.begin(), resourceDirectory.end(), resourceDirectory.begin(), ::tolower);

    // setting up resource directory
    std::string currRsrcPath, defaultRsrcPath;
    vfigui::uiGetPropertyString(vfigui::UI_PROP_RESOURCE_PATH, currRsrcPath);
    vfigui::uiGetPropertyString(vfigui::UI_PROP_RESOURCE_DEFAULT_PATH, defaultRsrcPath);

    dlog_msg("DirectGUI setup: %s, %s", currRsrcPath.c_str(), defaultRsrcPath.c_str());
    vfigui::uiSetPropertyString(vfigui::UI_PROP_RESOURCE_PATH, currRsrcPath + "/" + resourceDirectory);
    vfigui::uiSetPropertyString(vfigui::UI_PROP_RESOURCE_DEFAULT_PATH, defaultRsrcPath + "/" + resourceDirectory);
#endif
#endif
}

#if defined VFI_GUI_DIRECTGUI && !defined VFI_GUI_PROXY && !defined DIRECTGUI2
#  define MAIN_RETURN_VALUE(value) /*nothing*/
void uiMain(int argc, char *argv[])
#else
#  define MAIN_RETURN_VALUE(value) value
int main (int argc, char *argv[])
#endif
{
	
	pid_t task_id;

	int icc_handle;

	int icc_phandle[2];
	int inter_phandle[2];
	CCommandMan 	  *pCommandMan	 = NULL;
	CCmdEMVmain 	  *pEMVmainCmd = NULL;

	dlog_scope lscope(apLogicalName);
	dlog_msg("*****%s ver. %s has STARTED - pid: %d*****",	CARDAPP_NAME, CARDAPP_VERSION, getpid());
	SVC_WAIT(0);

	g_CardAppConfig.SetICChandle(-1);
	
		dlog_msg("Starting MAIN LOOP");

		SignalInit();


		std::string name(CARDAPP_NAME);
		std::transform(name.begin(), name.end(), name.begin(), ::toupper);
		int initRet = com_verifone_pml::pml_init(name.c_str());

		dlog_msg("PML init ret: %d", initRet);
		// add versioning
		com_verifone_pml::appver::add_app_version(CARDAPP_VERSION);
		// Register libpingui version
		com_verifone_pin_gui::init();
		// ADK version
		com_verifone_pml::appver::register_library( "EMV_ADK", EMV_CT_FRAMEWORK_VERSION );
		
		//Checking terminal type
		unsigned long EXT_result = 0;

		g_CardAppConfig.SetTerminalType(1);		//must be set later - JACEK

		pCommandMan = new CCommandMan();
		pEMVmainCmd = new CCmdEMVmain();

		pCommandMan->regCmd(INSTALL_VSS_COM, pEMVmainCmd);
		pCommandMan->regCmd(EXECUTE_VSS_MACRO_COM, pEMVmainCmd);
		pCommandMan->regCmd(SUPER_EMV_COM, pEMVmainCmd);
		pCommandMan->regCmd(START_WAITING_COM, pEMVmainCmd);
		pCommandMan->regCmd(MONITOR_ICC_COM, pEMVmainCmd);
		pCommandMan->regCmd(UNSOLICITED_EVENT_COM, pEMVmainCmd);
		pCommandMan->regCmd(SEND_APDU_COM, pEMVmainCmd);
		pCommandMan->regCmd(SET_GET_TLV_COM, pEMVmainCmd);
		pCommandMan->regCmd(SET_PIN_PARAMS_COM, pEMVmainCmd);
		pCommandMan->regCmd(VERIFY_PIN_COM, pEMVmainCmd);
		pCommandMan->regCmd(SET_MESSAGE_COM, pEMVmainCmd);
		pCommandMan->regCmd(L1_BEGIN_TRANSACTION_COM, pEMVmainCmd);
		pCommandMan->regCmd(L1_COMPLETE_TRN_COM, pEMVmainCmd);
		pCommandMan->regCmd(L2_SELECT_READ_COM, pEMVmainCmd);
		pCommandMan->regCmd(L2_TRM_VERIFY_COM, pEMVmainCmd);
		pCommandMan->regCmd(L2_FIRST_AC_COM, pEMVmainCmd);
		pCommandMan->regCmd(L2_COMPLETE_COM, pEMVmainCmd);
		pCommandMan->regCmd(L3_SELECT_COM, pEMVmainCmd);
		pCommandMan->regCmd(L3_GPO_COM, pEMVmainCmd);
		pCommandMan->regCmd(L3_READ_DATA_COM, pEMVmainCmd);
		pCommandMan->regCmd(L3_CARDHOLDER_VERIFY_COM, pEMVmainCmd);
		pCommandMan->regCmd(L3_TRM_COM, pEMVmainCmd);
		pCommandMan->regCmd(L3_DATA_AUTH_COM, pEMVmainCmd);
		pCommandMan->regCmd(L3_FIRST_AC_COM, pEMVmainCmd);
		pCommandMan->regCmd(L3_EXT_AUTH_COM, pEMVmainCmd);
		pCommandMan->regCmd(L3_SCRIPT_PROCESSING_COM, pEMVmainCmd);
		pCommandMan->regCmd(L3_SECOND_AC_COM, pEMVmainCmd);
		pCommandMan->regCmd(RESET_EMV_CONFIG, pEMVmainCmd);
		pCommandMan->regCmd(LEDS_CONTROL_COM, pEMVmainCmd);

		pInterface = new cIPCInterface(apLogicalName, pCommandMan);

		if(pInterface == NULL)
		{
			dlog_error( "new() for interface returned NULL!" );
			return MAIN_RETURN_VALUE(-1);
		}
		
		if(pInterface->impInitialise(argc,argv) != IPC_SUCCESS)
		{
			dlog_error( "unable to initialise interface" );
			return MAIN_RETURN_VALUE(-1);
		}
		dlog_msg("SUCCESS: IPC interface initialised");

		guiSetup();
		
		#ifdef VFI_PLATFORM_VOS

		// Set working directory
		chdir(com_verifone_pml::getRAMRoot().c_str());

		// Make flash directory
		if (mkdir(com_verifone_pml::getFlashRoot().c_str(), S_IRWXU | S_IRWXG | S_IRWXO) != 0)
		{
			if (errno != EEXIST) dlog_error("Cannot create %s directory, errno %d", com_verifone_pml::getFlashRoot().c_str(), errno);
		}

		g_logger.Log("*****%s has STARTED - pid: %d tid: %d*****",  apLogicalName, getpid(), syscall(SYS_gettid));


		
		#endif

		#ifdef VFI_PLATFORM_VERIXEVO
	
		dlog_msg("*****%s ver. %s has STARTED - pid: %d*****",  CARDAPP_NAME, CARDAPP_VERSION, getpid());
		dlog_msg("*****VSS engine version: %s*****", pcPS_GetVSSVersion());

	
		#endif

		if(g_CardAppConfig.iReadConfig(g_CardAppConfig.getName()) != ESUCCESS) // Initialise configuration
		{
			dlog_error("ERROR: CardAppConfig->init() has failed!");
			// TODO: Should we report an error here?
		}

		g_logger.setFilename(ADK_LOG_FILE);
		g_logger.setupLog(g_CardAppConfig.GetInternalLogging());
		
		g_logger.Log("using EMV ADK ver: %s", EMV_CT_FRAMEWORK_VERSION);

		g_logger.LogAps();

		g_leds.set_lock(g_CardAppConfig.GetUseLEDs());

		//g_CEMV.startICCMonitoring();
		
		g_CEMVtrns.monitor_flags |= CARD_APP_MON_FOREVER;		//start monitoring automatically (to avoid componrnt starting problems)

		g_CEMVtrns.init_the_kernel();

		pInterface->impWaitRequest();

	return MAIN_RETURN_VALUE(0);
}

#if defined VFI_GUI_DIRECTGUI && !defined VFI_GUI_PROXY && !defined DIRECTGUI2
int main (int argc, char *argv[])
{
	uiMain(argc, argv);

	return 0;
}
#endif

