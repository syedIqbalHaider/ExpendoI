#ifndef __cplusplus
#  error "This file is for C++ only!"
#endif

/***************************************************************************
 *
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 *
 **************************************************************************/
/**
 * @file        cTransaction.cpp
 *
 * @author      Jacek Olbrys
 *
 * @brief       Transaction Handler
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C++ Coding
 *              Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/
#include <algorithm>
#include <svc.h>
#include <svcsec.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <fcntl.h>
#include <msrDevice.h>
#include <sys/ioctl.h>
#include <sys/syscall.h>


//#include <SLE44x2.h>
//#include <EE2K4K.h>

#include <libda/cterminalconfig.h>

#include <libpml/pml.h>
#include "libpml/pml_abstracted_api.h"
#include "libpml/pml_port.h"

#include "E2E_EMV_CT_Serialize.h"

#include <libvoy.h>


#include <liblog/logsys.h>
#include <libprompts/user_prompt_lite.hpp>

#include "cEMV.h"
#include "cTransaction.h"
#include "cCARDAppConfig.h"
#include "EMVtools.h"
#include "EMVConfigurationData.h"
#include <fmtlog/fmtlog.hpp>

#include <tlv-lite/ConstData.h>
#include <tlv-lite/ValueConverter.hpp>
#include <tlv-lite/ConstValue.hpp>

#include <libpml/ux.h>
#include <libpml/pml.h>
#include <libpml/pml_os.h>

extern logger g_logger;
extern cEMVcollection g_EMVcollection;
extern cLEDsHandler g_leds;

#define  EMV_CT_RFU_1                  0x01  ///< reserved for probable reader number
#define  EMV_CT_RFU_2                  0x02  ///< reserved for probable reader number
#define  EMV_CT_TRY_PPS                0x04  ///< Try to increase the baudrate by using PPS
#define  EMV_CT_SKIP_ATR               0x08  ///< Skip ATR reading, not included in response
#define  EMV_CT_TRY_PPS_EPA            0x10  ///< PPS handling for Austrian Maestro Cards
#define  EMV_CT_WARMRESET              0x20  ///< Warm Reset to ICC (Cold reset must have been applied before)

#ifndef msrDecodeDirection
namespace {
    int msrDecodeDirection() { return 0; }
}
#endif

using namespace com_verifone_terminalconfig;
using std::string;


int updated=0;

void CUST_EPA_TA1_11_PPS(unsigned char* ATRBuffer, unsigned char* atrLen)
{
  int ret = 0;

  if (ATRBuffer[2] == 0x11)
  {
    // find historical bytes position
    unsigned char histPos = atrLen[1] - (ATRBuffer[1] & 0x0F) - 1;
    dlog_msg("EPA: historical bytes, pos:%d, ATR len:%d", histPos, atrLen[1]);

    if (memcmp(ATRBuffer+histPos, "EPA", 3) == 0)
    {
      unsigned char tucAtrBuf[50];
      unsigned char ucTCK = 0;
      int i;
      unsigned char CapBuffer[4] ;
      CapBuffer[0] = 0; // convert to Big Endian
      CapBuffer[1] = 0;
      CapBuffer[2] = 0;

      dlog_msg("EPA: Try ATR adaption");

      CapBuffer[3] = NO_STANDARD;
      ret += IFD_Set_Capabilities(Tag_Standard, CapBuffer);
      dlog_msg("EPA: Set NO_STANDARD, ret:%d", ret);

      CapBuffer[3] = MANUAL_PTS;
      ret += IFD_Set_Capabilities(Tag_Pts_Management, CapBuffer);
      dlog_msg("EPA: Set MANUAL_PTS, ret:%d", ret);

      // prepare buffer for Tag_Override_Card_ATR
      memset(&tucAtrBuf, 0x00, sizeof(tucAtrBuf));

      // write len include TCK
      tucAtrBuf[0] = histPos+1;

      // copy interface bytes
      memcpy(&tucAtrBuf[1], &ATRBuffer[0], histPos);

      // change T0 (without historical bytes)
      tucAtrBuf[2] = tucAtrBuf[2] & 0xF0;

      // set TA1 to (fake) value
      tucAtrBuf[3] = 0x13;

      // calculate TCK (start with T0)
      for(i=2; i<=histPos;i++) ucTCK^=tucAtrBuf[i];
      tucAtrBuf[histPos+1] = ucTCK;
      dlog_msg("EPA: New ATR with TCK:0x%02X", ucTCK);

      // override card ATR
      ret += IFD_Set_Capabilities(Tag_Override_Card_ATR, (unsigned char *)&tucAtrBuf);
      dlog_msg("EPA: Set Override_Card_ATR, ret:%d", ret);

      if (ret == 0) // override successful
      {
        ATRBuffer[2] = 0x13; // set to trigger PPS
        dlog_msg("EPA: Set 0x13 to TA1");
      }
    }
  }
} /**
* adaptTA1
* According to card capabilities and hardware TDA capabilities adapt
* the selected PPS values. Clock of the TDA can be set to: 4, 5, 10 and 20 MHz
* @param[in] TA1 from the ATR
* @param[out] none
* @return 11 for no valid detection ->
*         Value for correct PPS1
*/
unsigned char CUST_adaptTA1(unsigned char ta1)
{
  // Check TA1 according to F and D transmission parameters.
  // If Clock is:
  // 4MHz -> Use 4MHz
  // 5, 6, 7.5, 8MHz -> Use 5MHz
  // 10, 12, 15, 16MHz -> Use 10MHz
  // 20MHz -> Use 20MHz
  unsigned char aloneFi = ta1 & 0xF0;

  switch(ta1)
  {
    // First the list of our standard 5Mhz cards
    case 0x12: case 0x13: case 0x14: case 0x18: case 0x15: case 0x19: case 0x16:
    case 0x92: case 0x93: case 0x94: case 0x98: case 0x95: case 0x99: case 0x96:
    // Our 0x97 test cards are returned with 0x96 in ATR, so they seems to be not supported?
    case 0x97:
      return ta1;
      break;
    default: break;
  }
  // TODO: Don't know if all the fall-back scenarios are valid for the specific card?
  // Compare only Fi
  switch (aloneFi)
  {
    // 4MHz
    case 0x00:
      return ta1;
      break;
    // 5 MHz
    case 0x10: case 0x90:
      return ta1;
      break;
    // 6 + 8 MHz
    case 0x20: case 0x30:
      return (ta1 & 0x1f);
      break;
    // 7.5MHz
    case 0xa0:
      return (ta1 & 0x9f);
      break;
    // 10MHz
    case 0xb0:
      return ta1;
      break;
    // 12, 15 and 16MHz
    case 0x40: case 0x50: case 0xc0:
      return (ta1 & 0xbf);
      break;
    // 20MHz
    case 0x60: case 0xd0:
      return ta1;
      break;
    // 5MHz with Fd=372 and Dd=1
    default:
      return 0x11;
      break;
  }
}


RESPONSECODE CUST_EMV_RunPPS(unsigned char ucOptions)
{
  unsigned char ATRBuffer[64] ;
  unsigned char atrLen[2];
  unsigned char protocolType[4];
  //DWORD pType;

  // To select protocol types we need:
  // Tag_Asynch_protocol_types
  // And the ATR to check.

  if ( (IFD_Get_Capabilities(Tag_Current_Protocol_Type, protocolType) == IFD_Success)
      && (IFD_Get_Capabilities(Tag_ATR_Length, atrLen) == IFD_Success) )
  {
    if (IFD_Get_Capabilities (Tag_ATR_String, ATRBuffer) == IFD_Success)
    {
      //Check TS if 0x3b = direct convention or 0x3f = inverse convention
      if (ATRBuffer[0] == 0x3b || ATRBuffer[0] == 0x3f)
      {
        // Check T0 if TA1 is present
        if ( ATRBuffer[1] & 0x10 )
        {
          if ((ucOptions & EMV_CT_TRY_PPS_EPA) == EMV_CT_TRY_PPS_EPA)
          {
            CUST_EPA_TA1_11_PPS(ATRBuffer, atrLen);
          }

          // EMV defines to skip PPS in case of standard 0x11 card. (Also issue of VOEB test 3TA.006.90.06.)
          // Remark this is not an error in case of ISO7816-3.
          if (ATRBuffer[2] != 0x11)
          {
            dlog_msg("PPS TA1 %02X %02X", ATRBuffer[2], CUST_adaptTA1(ATRBuffer[2]));
            // Set the PTS1 with the TA1 capabilities, this should be enough?
            return IFD_Set_Protocol_Parameters(protocolType[3], NEGOTIATE_PTS1, CUST_adaptTA1(ATRBuffer[2]), 0,0);
          }
        }
      }

      // no critical error, just the PPS was not applicable because the card does not support it
      return IFD_Success;
    }
  }

  return IFD_Failure;
}


unsigned char CUST_EMVADK_SmartDetect(unsigned char ucOptions)
{
  unsigned char CardBuffer[64] ;
  RESPONSECODE RespCode;
  unsigned char erg = EMV_ADK_SMART_STATUS_REMOVED;

  CardBuffer[0] = 0 ; // convert to Big Endian
  CardBuffer[1] = 0 ;
  CardBuffer[2] = 0 ;
  CardBuffer[3] = CUSTOMER_CARD ;

  RespCode = IFD_Set_Capabilities( Tag_Open_SCard_Reader, CardBuffer ) ;
  if(RespCode != IFD_Success)
  {
    dlog_msg("Smart Detect: open scard reader %d\n",RespCode);
  }
  else
  {
    RespCode = IFD_Set_Capabilities( Tag_Open_ICC, CardBuffer ) ;
    if(RespCode != IFD_Success)
    {
      dlog_msg("Smart Detect: open icc %d\n",RespCode);
    }
    else
    {
      RespCode = IFD_Set_Capabilities ( Tag_Select_ICC, CardBuffer ) ;
      if(RespCode != IFD_Success)
      {
        dlog_msg("Smart Detect: select icc %d\n",RespCode);
      }
      else
      {
        RespCode = IFD_Is_ICC_Present();
        if(RespCode == IFD_Success)
          erg = EMV_ADK_SMART_STATUS_OK;
      }
    }
  }
  return(erg);
}


unsigned char CUST_EMVADK_SmartReset(unsigned char ucOptions, unsigned char* pucATR, unsigned long* pnATRLength)
{
  RESPONSECODE RespCode;
  // used for setting up
  unsigned char ADK_Response = EMV_ADK_SMART_STATUS_EXCHG_ERR;
  unsigned char CardBuffer[64] ;
  unsigned char ATRLenBuffer[2] ;

  CardBuffer[0] = 0 ; // convert to Big Endian
  CardBuffer[1] = 0 ;
  CardBuffer[2] = 0 ;
  CardBuffer[3] = CUSTOMER_CARD ;

  // -----------------------------------------------------------------
  if((ucOptions & EMV_CT_WARMRESET) == EMV_CT_WARMRESET)
  {
    //fprintf(stdout, "Warm Reset\n");
    RespCode = IFD_Power_ICC(IFD_RESET);
  }
  else
  {
    //fprintf(stdout, "Cold Reset\n");
    IFD_Set_Capabilities (Tag_Open_SCard_Reader, CardBuffer);
    IFD_Set_Capabilities (Tag_Open_ICC, CardBuffer);
    IFD_Set_Capabilities (Tag_Select_ICC, CardBuffer);
    RespCode = IFD_Power_ICC(IFD_POWER_UP);
  }

  // -----------------------------------------------------------------
  if(RespCode == IFD_Success)
  {
    // Option not to fetch the ATR (config option) to speed up things
    if((ucOptions & EMV_CT_SKIP_ATR) != EMV_CT_SKIP_ATR)
    {
      CardBuffer[0] = 0 ; // convert to Big Endian
      CardBuffer[1] = 0 ;
      CardBuffer[2] = 0 ;
      CardBuffer[3] = CUSTOMER_CARD ;

      // ask for the ATR 0x0303
      RespCode = IFD_Get_Capabilities (Tag_ATR_String, CardBuffer) ;
      // ask for ATR length tag 0x0305
      RespCode = IFD_Get_Capabilities(Tag_ATR_Length, ATRLenBuffer);
      if(RespCode == IFD_Success)
      {
        *pnATRLength = (0x00FF & ATRLenBuffer[0] )*256 + (0x00FF  & ATRLenBuffer[1]) ;

        memcpy ( pucATR, CardBuffer, *pnATRLength) ;

        RespCode = IFD_Get_Capabilities(Tag_ICC_Type, ATRLenBuffer);

        ADK_Response = EMV_ADK_SMART_STATUS_OK;
      }
    }
    else
    {
      ADK_Response = EMV_ADK_SMART_STATUS_OK;
      *pnATRLength = 0;
    }

    if(ADK_Response == EMV_ADK_SMART_STATUS_OK)
    {
      if (((ucOptions & EMV_CT_TRY_PPS)     == EMV_CT_TRY_PPS) ||
          ((ucOptions & EMV_CT_TRY_PPS_EPA) == EMV_CT_TRY_PPS_EPA))
      {
        // try to increase the baudrate
        dlog_msg("******* Try PPS\n");
        if (CUST_EMV_RunPPS(ucOptions) != IFD_Success)
        {
        	dlog_msg("****** PPS Fail\n");

        	// if there is a more critical error, reboot the ICC without doing PPS
        	if(IFD_Power_ICC(IFD_POWER_UP) != IFD_Success) // do not fetch ATR again, reuse existing one
        	{
        	  ADK_Response = EMV_ADK_SMART_STATUS_EXCHG_ERR;
        	}
        	// else: still successful, no change
        }
        else
        {
          dlog_msg("****** PPS OK (success or TA1=11 kept)\n");
        }
      }
    }
  }

  return(ADK_Response);
}

unsigned char CUST_EMVADK_SmartPowerOff(unsigned char ucOptions)
{
  unsigned char erg = EMV_ADK_SMART_STATUS_OK;
  RESPONSECODE RespCode;
  
    unsigned char CardBuffer[64] ;
    CardBuffer[0] = 0 ; // convert to Big Endian
    CardBuffer[1] = 0 ;
    CardBuffer[2] = 0 ;
    CardBuffer[3] = CUSTOMER_CARD ;
      
  // Check card in, because libvoy will otherwise produce segfault in "IFD_Power_ICC(IFD_POWER_DOWN)"
  erg = CUST_EMVADK_SmartDetect(ucOptions);
   
  if( erg == EMV_ADK_SMART_STATUS_OK)
  {  	
    erg = EMV_ADK_SMART_STATUS_EXCHG_ERR;
        
    RespCode = IFD_Set_Capabilities ( Tag_Select_ICC, CardBuffer );
    
    if(RespCode != IFD_Success)
    {
      dlog_msg("Smart Reselect for Power Off failed: %d\n",RespCode);
      IFD_Set_Capabilities( Tag_Close_ICC, CardBuffer ) ;
      RespCode = IFD_Set_Capabilities( Tag_Open_ICC, CardBuffer ) ;
      if(RespCode != IFD_Success)
        dlog_msg("Smart ReOpent for Power Off failed: %d\n",RespCode);      	
      RespCode = IFD_Set_Capabilities ( Tag_Select_ICC, CardBuffer );        
      if(RespCode != IFD_Success)
        dlog_msg("Smart Reselect 2 for Power Off failed: %d\n",RespCode);
    }
    RespCode = IFD_Power_ICC(IFD_POWER_DOWN);
    if (RespCode == 0x00)
    {
      erg = EMV_ADK_SMART_STATUS_OK;
    }
    else
    {
      dlog_msg("Smart Off failed: %d\n",RespCode);    	    	
    }
    IFD_Set_Capabilities ( Tag_Close_ICC, CardBuffer );
  }

  return(erg);
}


#if 0
unsigned char CUST_EMVADK_SmartReset(unsigned char ucOptions, unsigned char* pucATR, unsigned long* pnATRLength)
{
  RESPONSECODE RespCode;
  int res, offset;
  // used for setting up
  unsigned char ADK_Response = EMV_ADK_SMART_STATUS_EXCHG_ERR;
  unsigned char CardBuffer[64] ;
  unsigned char ATRLenBuffer[2] ;
  BYTE abBuffer[270];
  int i;

  dlog_msg("Extended SmartReset");
  CardBuffer[0] = 0 ; // convert to Big Endian
  CardBuffer[1] = 0 ;
  CardBuffer[2] = 0 ;
  CardBuffer[3] = CUSTOMER_CARD ;

  // -----------------------------------------------------------------
  IFD_Set_Capabilities (Tag_Open_ICC, CardBuffer);
  IFD_Set_Capabilities (Tag_Select_ICC, CardBuffer);


  dlog_msg("ICC Powering\n");
  RespCode = IFD_Power_ICC(IFD_POWER_UP) ;

  // -----------------------------------------------------------------
  if(RespCode == IFD_Success)
  {
    // Option not to fetch the ATR (config option) to speed up things
    if((ucOptions & EMV_CT_SKIP_ATR) != EMV_CT_SKIP_ATR)
    {
      CardBuffer[0] = 0 ; // convert to Big Endian
      CardBuffer[1] = 0 ;
      CardBuffer[2] = 0 ;
      CardBuffer[3] = CUSTOMER_CARD ;

      // ask for the ATR 0x0303
      RespCode = IFD_Get_Capabilities (Tag_ATR_String, CardBuffer) ;
      // ask for ATR length tag 0x0305
      RespCode = IFD_Get_Capabilities(Tag_ATR_Length, ATRLenBuffer);

      if(RespCode == IFD_Success)
      {
        *pnATRLength = (0x00FF & ATRLenBuffer[0] )*256 + (0x00FF  & ATRLenBuffer[1]) ;

        memcpy ( pucATR, CardBuffer, *pnATRLength) ;

        RespCode = IFD_Get_Capabilities(Tag_ICC_Type, ATRLenBuffer);

        ADK_Response = EMV_ADK_SMART_STATUS_OK;
      }
    }
    else
    {
       dlog_msg("ICC Powering-ERROR\n");
       ADK_Response = EMV_ADK_SMART_STATUS_OK;
      *pnATRLength = 0;
    }

    if(ADK_Response == EMV_ADK_SMART_STATUS_OK)
    {
      dlog_msg("ICC Powering-OK\n");

    /* ------------------------------------------------------- */

    if (((ucOptions & EMV_CT_TRY_PPS)     == EMV_CT_TRY_PPS) ||
        ((ucOptions & EMV_CT_TRY_PPS_EPA) == EMV_CT_TRY_PPS_EPA))
    {
        // try to increase the baudrate
        dlog_msg("******* Try PPS\n");
        if (CUST_EMV_RunPPS(ucOptions) != IFD_Success)
        {
                dlog_msg("****** PPS Fail\n");

                // if there is a more critical error, reboot the ICC without doing PPS
                if(IFD_Power_ICC(IFD_POWER_UP) != IFD_Success) // do not fetch ATR again, reuse existing one
                {
                  ADK_Response = EMV_ADK_SMART_STATUS_EXCHG_ERR;
                }
                // else: still successful, no change
        }
        else
        {
          dlog_msg("****** PPS OK (success or TA1=11 kept)\n");
        }
    }
    }
  }
  return(ADK_Response);
}

#endif



void EMVlog(char const * msg);


/***************************************************************************
 * Using
 **************************************************************************/
using namespace com_verifone_TLVLite;
using namespace com_verifone_guiapi;
using namespace com_verifone_emv;
using namespace com_verifone_pml;


/***************************************************************************
 * external global variables
 **************************************************************************/
extern CCardAppConfig   g_CardAppConfig;
extern CEMV         g_CEMV;
extern CEMVtrns     g_CEMVtrns;
extern cEMVcollection g_EMVcollection;

#define ICC_RESPONSE_TOKEN "*** Rcv from ICC:"
#define APDU_BUFFER_SIZE 512

#define MAG_MODE O_RDWR

int parseCallback(unsigned char *buffer, int buflen)
{
	int numObj = 0;
	unsigned char Tag[3];
	int tagNum;
	int Len;
	unsigned char *current;
	unsigned char *script;
	int i;
	int proc_len=0;
	unsigned short Ret = 0;
	cUpdateCfg UpdateCfg;


	current = buffer;

	// dlog_msg("CBK TLV Input data len: %d", buflen);
	while(buflen > proc_len)
	{
		//dlog_msg("Processing %d/%d", proc_len, buflen);
		Tag[0] = current[0];
		script = current;
		current++;
		proc_len++;

		if((Tag[0] & 0x1F) == 0x1F) //more than 1 byte tag
		{
			Tag[1] = current[0];
			current++;
			proc_len++;
		}
		else
		{
			Tag[1] = 0x00;
		}

		//if((Tag[1] & 0x7F) != Tag[1]) //more than 2 byte tag
		if(Tag[1] & 0x80) //more than 2 byte tag
		{
			Tag[2] = current[0];
			current++;
			proc_len++;
		}
		else
		{
			Tag[2] = 0x00;
		}

		if(Tag[2] > 0)
		{
			tagNum = Tag[0]*0x10000 + Tag[1]*0x100 + Tag[2];
		}
		else if(Tag[1] > 0)
		{
			tagNum = Tag[0]*0x100 + Tag[1];
		}
		else
		{
			tagNum = Tag[0];
		}

		//dlog_msg("CBK Processing TAG: %0X", tagNum);
		Len = (int)current[0];
		current++;
		proc_len++;
		if(Len & 0x80)
		{
			//well... we have more than one byte coding len
			i = Len & 0x7F;
			Len = 0;
			while(i)
			{
				Len += ((int)current[0]) << (8*(i-1));
				i--;
				current++;
				proc_len++;
			}
		}

		//CHECKING CORECTNESS OF DATA PARSING
		if(buflen - (current - buffer) < Len)
		{
			dlog_msg("Input data error, dropping further parsing...(remaining len: %d current object len: %d)", buflen - (current - buffer), Len);
			return -1;
		}
		
		//dlog_msg("CBK TLV: %02X%02X%02X Len:0x%X (%d)", Tag[0], Tag[1], Tag[2], Len, Len);
		if(Tag[0] & 0x20)   //constructed
		{
			//dlog_msg("CBK TLV constructed: %02X%02X%02X Len:%X", Tag[0], Tag[1], Tag[2], Len);
			/*
			int tagNum;
			if(Tag[2] > 0)
			{
				tagNum = Tag[0]*0x10000 + Tag[1]*0x100 + Tag[2];
			}
			else
			{
				tagNum = Tag[0]*0x100 + Tag[1];
			}
			*/
			if (tagNum == TAG_BF04_CBK_REDUCE_CAND)
			{
				dlog_alert(">>KP<< Candidate list processing!");
			}
			else if (tagNum == TAG_BF08_CBK_PIN)
			{
				dlog_alert(">>KP<< PIN Entry!");
			}

			parseCallback(current, Len);
		}
		else                //primitive
		{
			if (g_CardAppConfig.GetLogADK())
			{
				char APDU_str[APDU_BUFFER_SIZE*2+1];
				APDU_str[0] = 0;
				if(Len > APDU_BUFFER_SIZE*2)
				{
					Len = APDU_BUFFER_SIZE*2;
					dlog_msg("Cutting message to %d bytes", Len);
				}
				memcpy(APDU_str, current, Len);
				APDU_str[Len] = 0;
				EMVlog((char *)APDU_str);
				#ifdef VFI_PLATFORM_VOS
				dlog_msg("CBK '%s'", APDU_str);
				#endif
			}
			numObj++;
			//checking ICC response
			if(g_CardAppConfig.GetParseCardData())
			{
				if(memcmp(current, ICC_RESPONSE_TOKEN, strlen(ICC_RESPONSE_TOKEN)) == 0)
				{
					dlog_alert("Parsing extra data");
					int len = strlen((const char *)current);
					char APDU_str[APDU_BUFFER_SIZE*2];
					unsigned char APDU_hex[APDU_BUFFER_SIZE];
					int i = strlen(ICC_RESPONSE_TOKEN);
					int j = 0;
					//while(i < len && j < APDU_BUFFER_SIZE)
					while((i < Len) && (j < Len))
					{
						if((current[i] != ' ') && (current [i] != '*'))
						{
							APDU_str[j] = current[i];
							j++;
						}
						i++;
					}
					APDU_str[j] = 0;
					EMVlog(APDU_str);
					int hex_len;
					hex_len = com_verifone_pml::svcDsp2Hex(APDU_str, j, reinterpret_cast<char *>(APDU_hex), APDU_BUFFER_SIZE) - 2;
					if((hex_len > 0) && ((APDU_hex[0] == CARD_APP_EMV_TEMPLATE_70) || (APDU_hex[0] == CARD_APP_EMV_TEMPLATE_6F)))
					{
						parseInTLV(APDU_hex,hex_len, false);
					}
				}
			}
		}
		current+=Len;
		proc_len+=Len;
	}
	return numObj;
}


void EMV_Callback(unsigned char *pucSend, short sSendSize, unsigned char *pucReceive, short *psReceiveSize, void* externalData)
{
	*psReceiveSize = 0;
	#ifdef DEBUG
	// dlog_msg("EMV_Callback %d bytes of data", sSendSize);
	parseCallback(pucSend, sSendSize);
	#else
	if (g_CardAppConfig.GetParseCardData() || g_CardAppConfig.GetInternalLogging() != LOG_TO_NULL) parseCallback(pucSend, sSendSize);
	#endif
}



EMV_CT_CALLBACK_FnT pEMV_Callback = (EMV_CT_CALLBACK_FnT)EMV_Callback;


namespace counters
{
	static const char * VIPA_COUNTERS_DESC[] = {
		"MSR read attempts",
		"T1 success",
		"T2 success",
		"T3 success"
	};
	static const int CNT_BEGIN = 120;
	static const int CNT_MSR_READ_ATTEMPTS = CNT_BEGIN;
	static const int CNT_MSR_T1_READS = CNT_MSR_READ_ATTEMPTS+1;
	static const int CNT_MSR_T2_READS = CNT_MSR_T1_READS+1;
	static const int CNT_MSR_T3_READS = CNT_MSR_T2_READS+1;
	static const int CNT_SIZE = (sizeof VIPA_COUNTERS_DESC) / (sizeof VIPA_COUNTERS_DESC[0]);


	void initialize()
	{
		if (g_CardAppConfig.GetUseMSRCounters())
		{
			dlog_msg("Initializing MSR counters");
			for (int i = CNT_BEGIN; i < CNT_BEGIN + CNT_SIZE; ++i)
			{
				com_verifone_pml::pml_os::OS_COUNTER os_cnt;
				memset(&os_cnt, 0, sizeof(os_cnt));
				if (!com_verifone_pml::pml_os::is_custom_counter_defined(i, VIPA_COUNTERS_DESC[i-CNT_BEGIN]))
				{
					strncpy(os_cnt.name, VIPA_COUNTERS_DESC[i-CNT_BEGIN], sizeof(os_cnt.name)-1);
					int res = com_verifone_pml::pml_os::set_counter(i, &os_cnt);
					dlog_msg("Set MSR counter %d: res %d, errno %d", i, res, errno);
				}
				else
				{
					if (!com_verifone_pml::pml_os::get_counter(i, &os_cnt))
					{
						dlog_msg("MSR counter %d, name %s, value %d", i, os_cnt.name, os_cnt.value);
					}
					else
					{
							dlog_alert("MSR counter %d error: errno %d", i, errno);
					}
				}
			}
		}
	}
}

/***************************************************************************
 * Exportable function definitions
 **************************************************************************/

/***************************************************************************
 * Exportable class members' definitions
 **************************************************************************/
#ifdef VFI_PLATFORM_VOS
static void fProcessMSR(void)
{
	g_CEMVtrns.waitMagSemaphore();

	event_item ev_usr;
	ev_usr.event_id = events::user0;
	ev_usr.evt.user.flags = 0x0;

	std::string name(CARDAPP_NAME);
	std::transform(name.begin(), name.end(), name.begin(), ::toupper);

	dlog_msg("MSR callback: pid: %d tid: %d", getpid(), syscall(SYS_gettid));

	g_CEMVtrns.processMagReader();

	g_CEMVtrns.releaseMagSemaphore();
	
	dlog_msg("Mag stripe read succesfully, raising event (to: %s)", name.c_str());
	event_raise(name.c_str(), ev_usr);
}
#endif

CEMVtrns::CEMVtrns(void)
{
	ux uxHandler;
	InTLVlist = NULL;
	InTLV_list_len = 0;
	OutTLVlist = NULL;
	OutTLV_list_len = 0;
	OutTLVdata = NULL;
	OutTLV_data_len = 0;
	monitor_flags = 0;
	wait_flags = CARD_APP_CARD_ICC | CARD_APP_CARD_MAGNETIC;
	script71 = NULL;
	script71_len = 0;
	script72 = NULL;
	script72_len = 0;
	mem_kernel = NULL;
	mem_size = 0;
	ICC_locked = false;
	mag_handle_int = -1;
	#ifdef VFI_PLATFORM_VOS
	sem_init(&magMutex, 0, 1);
	#endif
	dlog_msg("MSR reader monitoring callback registered: %X", mag_handle_int);
	cmd_status = RESP_CODE_INVAL;
	card_with_mag_stripe = false;
	process_mag_stripe = true;
	getMsrTracks = false;
	//card_with_mag_stripe = false;
	//process_mag_stripe = false;
	icc_card_status = 0;
	ignore_icc_events = false;
	monitor_thread = -1;
	is_inserted = isCardInserted();
	if(uxHandler.is_ux())
		poll_timer = 75;
	else
		poll_timer = 200;
	swipe_direction = 0;
}

EMV_ADK_INFO CEMVtrns::Open_Kernel(void)
{
	if (Is_Kernel_Opened()) return EMV_ADK_OK;
	void *externalData = 0;
	EMV_ADK_INFO Ret;


//#ifdef VFI_PLATFORM_VERIXEVO	 //iq_audi_030217
//
//	mem_size = EMV_CT_GetEMVMem();
//
//
//	char msg_buf[100];
//
//	g_logger.Log_loadavg("System load on EMV kernel opening");
//
//
//	dlog_msg("Kernel requires %d bytes of memory", mem_size);
//
//	if(mem_kernel == NULL)
//	{
//		mem_kernel = new unsigned char[mem_size];
//		//JACEK - lets "reserve" the memory - for testing only
//		memset(mem_kernel,0xFF, mem_size);
//	}
//
//	if(mem_kernel != NULL)
//	{
//		//memset(mem_kernel, 0, mem_size);
//		Ret = EMV_CT_Init_Framework(mem_size, mem_kernel, EMV_Callback, externalData, /*EMV_CT_INIT_OPT_BASE_INIT |*/ EMV_CT_INIT_OPT_TRACE | EMV_CT_INIT_OPT_TRACE_CLT);
//		dlog_msg("Open kernel ret :%X, mem requested: %d bytes", Ret, mem_size);
//		sprintf(msg_buf, "\r\n***START TRANSACTION***\r\n\r\n EMV_CT_Init_Framework ret :%X, mem requested: %d bytes", Ret, mem_size);
//		EMVlog(msg_buf);
//		g_CEMV.ClearStatus();
//		if (EMV_CT_SmartDetect(0) == EMV_ADK_SMART_STATUS_OK)
//		{
//			icc_card_type = CARD_APP_CARDTYPE_EMV_ASYNC;
//		}
//		else
//		{
//			icc_card_type = CARD_APP_CARDTYPE_NOT_SET;
//		}
//	}
//	else
//	{
//		Ret = -1;
//		dlog_msg("CRITICAL ERROR! - can not allocate memory for EMV kernel (%d bytes)", mem_size);
//		EMVlog("CRITICAL ERROR! - can not allocate memory for EMV kernel");
//	}
//
//#else
//	mem_size = EMV_CT_GetEMVMem();
	int numberOfAIDs=30;  //iqbal number of possible expected AIDs may be but not sure
	char msg_buf[100];

	g_logger.Log_loadavg("System load on EMV kernel opening");


//	dlog_msg("Kernel requires %d bytes of memory", mem_size);
//
//	if(mem_kernel == NULL)
//	{
//		mem_kernel = new unsigned char[mem_size];
//		//JACEK - lets "reserve" the memory - for testing only
//		memset(mem_kernel,0xFF, mem_size);
//	}

	//memset(mem_kernel, 0, mem_size);
	//iq_audi_030217
	Ret = EMV_CT_Init_Framework(numberOfAIDs, (EMV_CT_CALLBACK_FnT)EMV_Callback, externalData, /*EMV_CT_INIT_OPT_BASE_INIT |*/ EMV_CT_INIT_OPT_TRACE | EMV_CT_INIT_OPT_TRACE_CLT);
	dlog_msg("Open kernel ret :%X, mem requested: %d bytes", Ret, mem_size);
//	printf(msg_buf, "\r\n***START TRANSACTION***\r\n\r\n EMV_CT_Init_Framework ret :%X, mem requested: %d bytes", Ret, mem_size);
	EMVlog(msg_buf);
	g_CEMV.ClearStatus();

	if(Ret==EMV_ADK_OK)
	{
		mem_size=1; //iq_audi_030217 b/c indicates kernel is open, a/c to current flow
//		printf(msg_buf, "\r\n iqbal: ****Init EMV CT Framework OK");

		if (EMV_CT_SmartDetect(0) == EMV_ADK_SMART_STATUS_OK)
		{
			icc_card_type = CARD_APP_CARDTYPE_EMV_ASYNC;
		}
		else
		{
			icc_card_type = CARD_APP_CARDTYPE_NOT_SET;
		}
	}
	else
	{
		mem_size=0;
//		printf(msg_buf, "\r\n iqbal: ****Init EMV CT Framework error [0x%02x]",Ret);
	}
//#endif

	return Ret;
}

bool CEMVtrns::Is_Kernel_Opened(void)
{
	return (mem_size > 0);
}
void CEMVtrns::Close_Kernel(void)
{

	g_logger.Log_loadavg("System load on EMV kernel closure");
	if(Is_Kernel_Opened())
	{
		if (isCardInserted()) CUST_EMVADK_SmartPowerOff(0);
		dlog_msg("Closing kernel data structures...(mem size = %d bytes)", mem_size);
		EMV_CT_Exit_Framework();
		EMVlog("\r\n***END TRANSACTION***\r\n\r\nEMV_CT_Exit_Framework - closing kernel data structures");
		mem_size = 0;
		delete [] mem_kernel;
		mem_kernel = NULL;
		icc_card_type = CARD_APP_CARDTYPE_NOT_SET;
		g_leds.changeState(LED_SELECT_MSR1|LED_SELECT_MSR2|LED_SELECT_MSR3, LED_STATE_OFF);
	}
	// g_EMVcollection.ClearEverything();
}


//iq_audi_030217
#define EMV_XML_APPLICATIONS "EMV_Applications.xml"
#define EMV_XML_TERMINAL "EMV_Terminal.xml"
#define EMV_XML_KEY "EMV_Keys.xml"

//this function creates full EMV config
int CEMVtrns::DefaultConfig(bool add_empty)
{
	EMV_ADK_INFO ret = 0;
	EMV_CT_APPLI_TYPE *pxAID;
	EMV_CT_APPLIDATA_TYPE *pxAppliData;
	EMV_CT_CAPKEY_TYPE *pxKeyData;
	char szPTID[10];
	int i=0;

	if(add_empty)
	{
		EMVlog("Adding empty configuration files");
	}

	if((access(EMV_XML_TERMINAL, F_OK) == -1) || add_empty)
	{
		EMVlog("Building default XML configuration files");
		// set TID
		inGetPTermID(reinterpret_cast<char *>(GeneralTermDataDefault.IFDSerialNumber));
		dlog_msg("Terminal ID: '%s'", GeneralTermDataDefault.IFDSerialNumber);
		//com_verifone_pml::svcInfoPtid(szPTID, sizeof(szPTID));
		//strncpy(reinterpret_cast<char *>(GeneralTermDataDefault.IFDSerialNumber), szPTID, sizeof(GeneralTermDataDefault.IFDSerialNumber));
		if(add_empty)
		{
			g_EMVcollection.termData = xTermDataTermEmpty;
		}
		else
		{
			g_EMVcollection.termData = GeneralTermDataDefault;
		}
		EMVlog("EMV_CT_SetTermData");
		ret = EMV_CT_SetTermData(&g_EMVcollection.termData);
		EMVlog((char *)ADK_err(ret).c_str());
		dlog_msg("Adding default terminal data ret: %X", ret);
		dlog_msg("%s", ADK_err(ret).c_str());
	}

	if((access(EMV_XML_APPLICATIONS, F_OK) == -1) || add_empty)
	{
		if(add_empty)
		{
			pxAID = ptxAIDEmpty[i];
			pxAppliData = ptxAppliDataEmpty[i];
		}
		else
		{
			pxAID = ptxAID[i];
			pxAppliData = ptxAppliData[i];
		}
		while(pxAID != NULL && pxAppliData != NULL)
		{

			EMVlog("EMV_CT_SetAppliData");
			ret = EMV_CT_SetAppliData(EMV_ADK_SET_ONE_RECORD, pxAID, pxAppliData);
			EMVlog((char *)ADK_err(ret).c_str());
			dlog_msg("Adding default applications data ret: %X", ret);
			dlog_msg("%s", ADK_err(ret).c_str());
			if(add_empty)
			{
				pxAID = ptxAIDEmpty[i];
				pxAppliData = ptxAppliDataEmpty[i];
			}
			else
			{
				pxAID = ptxAID[i];
				pxAppliData = ptxAppliData[i];
			}
			i++;
		}


		EMVlog("Default XML configuration files created");
	}
	else
	{
		EMVlog("Using existing APPLI XML configuration files");
	}

	if((access(EMV_XML_KEY, F_OK) == -1))
	{

		pxKeyData = ptxKeysData[i];

		while(pxKeyData != NULL)
		{
//			EMVlog("EMV_CT_SetKeyData");

//			printf("iq: EMV KEY [%X]/r/n", pxKeyData->Key);
//			printf("iq: EMV KEYS Len [%X] /r/n", pxKeyData->KeyLen);
//			printf("iq: EMV KEYS Exp [%X] /r/n", pxKeyData->Exponent);
//			printf("iq: EMV KEYS Hash [%X] /r/n", pxKeyData->Hash);
//			printf("iq: EMV KEYS RID [%02X%02X%02X%02X%02X] /r/n", pxKeyData->RID[0], pxKeyData->RID[1], pxKeyData->RID[2], pxKeyData->RID[3], pxKeyData->RID[4]);
//			printf("iq: EMV KEYS RevEntries [%02X%02X%02X] /r/n", pxKeyData->RevocEntries[0], pxKeyData->RevocEntries[1], pxKeyData->RevocEntries[2]);
//			printf("iq: EMV KEYS Index [%X] /r/n", pxKeyData->Index);
//			printf("iq: EMV KEYS No. Rev Entries [%X] /r/n", pxKeyData->noOfRevocEntries);

			ret =EMV_CT_StoreCAPKey(EMV_ADK_SET_ONE_RECORD,pxKeyData);
			EMVlog((char *)ADK_err(ret).c_str());
			dlog_msg("Adding default Keys data ret: %X", ret);
//			dlog_msg("%s", ADK_err(ret).c_str());
//			printf("iq: EMV KEYS LOADING [%X]/r/n", ret);

			i++;
			pxKeyData = ptxKeysData[i];
		}


		EMVlog("Default XML configuration files created");
	}
	else
	{
		EMVlog("Using existing CAPK XML configuration files");
	}

	return 0;
}

#ifdef VFI_PLATFORM_VOS
static int openMagStripeReader()
{
	int handle = msrOpen(O_RDONLY, (void*)fProcessMSR);
	// The below works on Ux only, with SDK 3019
	#ifdef msrSetHybridMode
	if (handle >= 0)
	{
		int mode = MSR_IGNORE_EVENTS | MSR_WAIT4SWITCH;
		msrSetHybridMode(&mode);
	}
	#endif
	return handle;
}
#endif

//Needed for Online PIN entry, which could happen before performing transactions and requires the TLV Pool initialised
bool CEMVtrns::init_the_kernel()
{
	int res;

	g_EMVcollection.ClearEverything();
	g_EMVcollection.Clear();

	Open_Kernel();
	DefaultConfig(false);  // iq_audi_070217
	Close_Kernel();

	ux uxHandler;
	g_CardAppConfig.SetIsUX100(uxHandler.is_ux());
	InitCardReader();
	
	if (mag_handle_int < 0)
	{
		#ifdef VFI_PLATFORM_VOS
		mag_handle_int = openMagStripeReader();
		#endif
		#ifdef VFI_PLATFORM_VERIXEVO
		mag_handle_int = open(DEV_CARD, 0);
		#endif
	}
	
	return true;
}

void CEMVtrns::InitCardReader()
{
	card_with_mag_stripe = false;
	if (g_CardAppConfig.GetIsUX100())
	{
		if (g_CardAppConfig.GetUxSwipeOnInsertion()) process_mag_stripe = true;
		else process_mag_stripe = false;
		getMsrTracks = false;
	}
	else
	{
		process_mag_stripe = true;
		getMsrTracks = true;
	}
	g_logger.Log("setting parameters: UX100: %d, swipe on insertion: %d, process mag stripe: %d", g_CardAppConfig.GetIsUX100(), g_CardAppConfig.GetUxSwipeOnInsertion(), process_mag_stripe);
	
	is_inserted = isCardInserted();
}

int CEMVtrns::send_notify(int notify_event)
{
	return send_notify(notify_event, NULL, 0, true);
}
	
int CEMVtrns::send_notify(int notify_event, unsigned char *event_data, int event_data_size, bool extended)
{
	using namespace com_verifone_TLVLite;
	IPCPacket  response;

	if(g_CardAppConfig.GetNotifyStatus())
	{
		dlog_msg("sending notification message: event %d to %s (event extended: %d to %s, event data: %d)", notify_event, g_CardAppConfig.cNotifyName, extended, g_CardAppConfig.cEventNotifyName, event_data_size);
		response.addTag(ConstData(arbyTagEvent, EVENT_TS), MakeBERValue(notify_event));
		response.setCmdCode(UNSOLICITED_EVENT_COM);
		if (notify_event == CARD_APP_EVT_PIN_ENTRY)
		{
			// Add PIN try counter
			unsigned char pinTryCtr;
			unsigned short pinTryCtrLen = 0;
			g_EMVcollection.GetTLVData(TAG_9F17_PIN_TRIES_LEFT, &pinTryCtr, (int *)&pinTryCtrLen);
			if (pinTryCtrLen == 1)
			{
				const int PIN_TRY_CTR_TS = 2;
				const uint8_t arbyPinTryCtr[PIN_TRY_CTR_TS] = { 0x9F, 0x17 };
				response.addTag(ConstData(arbyPinTryCtr, PIN_TRY_CTR_TS), MakeBERValue(pinTryCtr));
			}
		}
		response.setAppName(g_CardAppConfig.cNotifyName);
		response.send();
		
		SVC_WAIT(0);
	}
	if(extended && strlen(g_CardAppConfig.cEventNotifyName))
	{
		dlog_msg("Sending extended event to %s", g_CardAppConfig.cEventNotifyName);
		if(event_data && event_data_size)
		{
			response.addTag(ConstData(arbyTagContextData, EXTRA_DATA_TS), MakeBERValue((int)event_data));
		}
		response.setAppName(g_CardAppConfig.cEventNotifyName);
		response.send();
	}
	return(ESUCCESS);
}

int CEMVtrns::update_config(int record)
{
	int iRet = 0;
	int numTLV = 0;
	EMV_CT_APPLI_TYPE pxAID;
	EMV_ADK_INFO ADK_ret;
	std::string ADK_str;

	//we should read ADK data here

	ADK_ret = EMV_CT_GetAppliData(EMV_ADK_READ_AID, &pxAID, &(g_EMVcollection.appData));
	ADK_str = ADK_err(ADK_ret);
	dlog_msg("EMV_CT_GetAppliData: %s", ADK_str.c_str());
	ADK_ret = EMV_CT_GetTermData(&(g_EMVcollection.termData));
	ADK_str = ADK_err(ADK_ret);
	dlog_msg("EMV_CT_GetTermData: %s", ADK_str.c_str());

	numTLV = parseInTLV(InTLVlist, InTLV_list_len, false);
	dlog_msg("Added/Updated %d TLV objects in collection", numTLV);

	TLV_data_len = parseOutTLV(OutTLVlist, OutTLV_list_len, OutTLVdata, OutTLV_data_len);
	dlog_msg("Sent %d bytes of requested data", TLV_data_len);
	return 0;
}

#ifdef VFI_PLATFORM_VERIXEVO
int CEMVtrns::processMagReader()
{
	
	int read_data = 0;
	
    if(mag_handle_int < 0)
    {
        int task_id=0;
        mag_handle_int = get_owner(DEV_CARD, &task_id);
        dlog_msg("Crypto opened by %d, handle: %d", task_id, mag_handle_int);
        if (task_id != get_task_id() || mag_handle_int < 0)
        {
            return read_data;
        }
    }

    char track_buffer[2+2+2+CARD_APP_DEF_TRACK1_LEN+CARD_APP_DEF_TRACK2_LEN+CARD_APP_DEF_TRACK3_LEN];
    int offset = 0;
    read_data=read(mag_handle_int, track_buffer, 2+2+2+CARD_APP_DEF_TRACK1_LEN+CARD_APP_DEF_TRACK2_LEN+CARD_APP_DEF_TRACK3_LEN);
    dlog_msg("Mag card: %d bytes read", read_data);
    memset(track_status, 0, sizeof(track_status));
    if(read_data)
    {
        track_status[0] = track_buffer[1];
        if(track_buffer[1] == 0)
        {
            memcpy(track1, track_buffer+2, track_buffer[0]-2);
            dlog_msg("Track1 present:%s", track1);
        }
        offset+=track_buffer[0];
        track_status[1] = track_buffer[offset+1];
        if(track_buffer[offset+1] == 0)
        {
            memcpy(track2, track_buffer+offset+2, track_buffer[offset+0]-2);
            dlog_msg("Track2 present:%s", track2);
        }
        offset+=track_buffer[offset+0];
        track_status[2] = track_buffer[offset+1];
        if(track_buffer[offset+1] == 0)
        {
            memcpy(track3, track_buffer+offset+2, track_buffer[offset+0]-2);
            dlog_msg("Track3 present:%s", track3);
        }
    }
    return read_data;
}
#endif

#ifdef VFI_PLATFORM_VOS

int CEMVtrns::processMagReader()
{
	int read_data = 0;

	if(1)
	{

		char track_buffer[2+2+2+CARD_APP_DEF_TRACK1_LEN+CARD_APP_DEF_TRACK2_LEN+CARD_APP_DEF_TRACK3_LEN];
		int offset;

		if (mag_handle_int == -1)
		{
			mag_handle_int = openMagStripeReader();
		}
		msrDisableHTdes();        // This will disable VCL and should end the swipe velocity check.

		ClearMagstripeData();

		if(1)
		{
			offset = 0;

			read_data = msrRead(track_buffer, 2+2+2+CARD_APP_DEF_TRACK1_LEN+CARD_APP_DEF_TRACK2_LEN+CARD_APP_DEF_TRACK3_LEN);
			swipe_direction = msrDecodeDirection();
			g_logger.Log("Mag card: %d bytes read", read_data);
			dlog_hex(track_buffer, read_data, "msrRead");
			//if(process_mag_stripe)
			if(wait_flags & CARD_APP_CARD_MAGNETIC)
			{
				dlog_msg("Request track data, swipe direction %d...", swipe_direction);
				if(read_data > 6)
				{
					track_status[0] = track_buffer[1];
					if((track_status[0] == 0) && (sizeof(track1) > track_buffer[0]-2))
					{
						 memcpy(track1, track_buffer+2, track_buffer[0]-2);
						 track1[track_buffer[offset+0]-2] = 0;
						 dlog_msg("Track1 present:%s", track1);
						 g_logger.Log("Track1 present:%s", track1);
					}
					offset+=track_buffer[0];
					track_status[1] = track_buffer[offset+1];
					if((track_status[1] == 0) && (sizeof(track2) > track_buffer[offset+0]-2))
					{
						 memcpy(track2, track_buffer+offset+2, track_buffer[offset+0]-2);
						 track2[track_buffer[offset+0]-2] = 0;
						 dlog_msg("Track2 present:%s", track2);
						 g_logger.Log("Track2 present:%s", track2);
					}
					offset+=track_buffer[offset+0];
					track_status[2] = track_buffer[offset+1];
					if((track_status[2] == 0) && (sizeof(track3) > track_buffer[offset+0]-2))
					{
						 memcpy(track3, track_buffer+offset+2, track_buffer[offset+0]-2);
						 track3[track_buffer[offset+0]-2] = 0;
						 dlog_msg("Track3 present:%s", track3);
						 g_logger.Log("Track3 present:%s", track3);
					}
				}
				else if (read_data == 6)
				{
					dlog_msg("Swipe read error, cancelling");
					track_status[0] = track_buffer[1];
					track_status[1] = track_buffer[3];
					track_status[2] = track_buffer[5];
				}
				else
				{
					ClearMagstripeData();
				}
			}
			else
			{
				dlog_msg("Ignoring unwanted mag stripe data..");
				g_logger.Log("Magstripe processing blocked! - ignoring data");
				read_data = 0;
				ClearMagstripeData();
			}
		

		}
		//mag_handle_int = -1;
		//msrClose();

	}
	else
	{
		dlog_msg("ERROR! opening mag reader for reading");
	}
	return read_data;
}
#endif


int CEMVtrns::processICCReader(int flags /* = 0 */)
{
	const unsigned char GEM_CARD_ID = 0x53;
	int iRet = CARD_APP_RET_ICC_FAIL;
	short len = 0;
	short offset = 0;
	unsigned long ATR_len = 0;
	uint8_t ATR_buffer[CARD_APP_DEF_ATR_LEN];
	memset(ATR_buffer, 0, CARD_APP_DEF_ATR_LEN);
	memset(ATR, 0, CARD_APP_DEF_ATR_LEN);

	
	if(!(wait_flags & CARD_APP_CARD_ICC))
	{
		g_logger.Log("ICC processing blocked!");
		iRet = CARD_APP_RET_CANCELED;
	}
	else if (!(flags & WAIT_FOR_CARD_SKIP_KERNEL_REINIT))
	{

		Close_Kernel();	//closing kernel data structures before new transaction


		if ((iRet = Open_Kernel()) != EMV_ADK_OK)
		{

			Close_Kernel();
			iRet = CARD_APP_RET_ICC_FAIL;

		}
	}
	else
	{
		if(Is_Kernel_Opened())
		{
			iRet = CARD_APP_RET_OK;
		}
		else if ((iRet = Open_Kernel()) != EMV_ADK_OK)
		{

			Close_Kernel();
			iRet = CARD_APP_RET_ICC_FAIL;

		}
	}

	if (iRet == CARD_APP_RET_OK)
	{

		//DefaultConfig();
		unsigned char ATR_hex[CARD_APP_DEF_ATR_LEN];

		int ret = -1;
		int adk_flags = 0;

		if (flags & WAIT_FOR_CARD_WARM_RESET)
		{
			dlog_msg("Warm reset");
			adk_flags |= EMV_CT_WARMRESET;
		}
		else if (g_CardAppConfig.GetICCNegotiateSpeed()) 
		{
			dlog_msg("ICC reset with speed negotiations...");
			adk_flags |= EMV_CT_TRY_PPS;
			
		}
		if(adk_flags == 0)
		{
			dlog_msg("Standard ICC reset");
			ret = EMV_CT_SmartReset(0, ATR_hex, &ATR_len);
		}
		else
		{
			ret = CUST_EMVADK_SmartReset(adk_flags, ATR_hex, &ATR_len);
		}
	
		dlog_msg("EMV_CT_SmartReset ret: %d ATR len: %d, flags were %Xh", ret, ATR_len, adk_flags);
		if(ret == EMV_ADK_SMART_STATUS_OK && ATR_len)
		{
			com_verifone_pml::svcHex2Dsp((const char *)ATR_hex, ATR_len, (char *)ATR, CARD_APP_DEF_ATR_LEN);
			dlog_msg("Kernel initialised: %d ATR: %d bytes: %s", ret, ATR_len, ATR);
			dlog_msg("Kernel init done...");
			g_logger.Log("ATR: %s", ATR);
			icc_card_type = CARD_APP_CARDTYPE_EMV_ASYNC;
			iRet = CARD_APP_RET_ICC_OK;
		}
		else
		{
			dlog_msg("ERROR - cannot reset ICC card");
			iRet = CARD_APP_RET_ICC_FAIL;
			icc_card_type = CARD_APP_CARDTYPE_NOT_SET;
		}

	}
	if (iRet == CARD_APP_RET_ICC_OK)
	{
		dlog_msg("Clearing statuses for new transaction");
		g_CEMV.ClearStatus();
	}

	return iRet;
}



void logCardEvents(int events)
{
	char evts[20];

	std::string events_verbouse;

	sprintf(evts, "[%.02X]=", events);
	events_verbouse = evts;

	if(events & CARD_APP_EVT_CARD_IN)
	{
		events_verbouse+="CARD_IN ";
	}
	if(events & CARD_APP_EVT_CARD_OUT)
	{
		events_verbouse+="CARD_OUT ";
	}
	if(events & CARD_APP_EVT_PIPE)
	{
		events_verbouse+="PIPE ";
	}
	if(events & CARD_APP_EVT_MSR)
	{
		events_verbouse+="MSR ";
	}
	if(events & CARD_APP_EVT_MSR_READY)
	{
		events_verbouse+="WAIT ";
	}
	dlog_msg("Events: %s", events_verbouse.c_str());
	g_logger.Log("Events: %s", events_verbouse.c_str());
}

#define CARD_APP_TIMER_OUT_DELAY 200
#define CARD_APP_TIMER_CHECK 200

int CEMVtrns::monitor_ICC(int flags)
{
	return monitor_ICC(flags, INFINITE_TIMEOUT);
}


int CEMVtrns::monitor_ICC(int flags, const long wait_timeout)
{
	int evt_Ret = CARD_APP_EVT_NONE;
	int iRet = 0;
	int icc_handle = -1;
	event_item ev_icc, ev_ipc, ev_mag, ev_usr;
	event_item localTimer;
	event_item checkTimer;
	eventset_t pml_events;
	int events_mon = event_open();
	int wait_event_ret = 0;
	long pml_timeout = wait_timeout;
	bool continue_waiting = true;
	
	int timer_h = -1;
	int timer_poll = -1;

	/*
	if (icc_card_status)
	{
		close(events_mon);
		if(isCardInserted())
		{
			return CARD_APP_EVT_CARD_IN;
		}
		else
		{
			icc_card_status = 0;
			return CARD_APP_EVT_CARD_OUT;
		}
	}
*/
	
		
	unsigned char ucRet = 0;
	

	EMVlog("opening devices and registering handlers in pml");

	if(flags & CARD_APP_MON_NOBREAK)
	{
		dlog_msg("Waiting for card status change forever!");
	}
	else
	{
		iRet = event_ctl(events_mon, ctl::ADD, com_verifone_ipc::get_events_handler());
		dlog_msg("event_ctl(...ipc) ret: %d", iRet);
	}

	if(wait_flags & CARD_APP_CARD_MAGNETIC)
	{
		#ifdef VFI_PLATFORM_VERIXEVO
		ev_usr.event_id = events::mag;
		ev_usr.evt.com.fd = -1;
    	ev_usr.evt.com.flags = 0;
		#endif
		
		#ifdef VFI_PLATFORM_VOS
		ev_usr.event_id = events::user0;
		ev_usr.evt.com.flags = EPOLLIN|EPOLLPRI;
		#endif
		iRet = event_ctl(events_mon, ctl::ADD, ev_usr);
		dlog_msg("event_ctl(...usr) ret: %d", iRet);
	}
	else
	{
		g_logger.Log("MSR reader disabled");
	}

	if(wait_flags & CARD_APP_CARD_ICC)
	{

		ev_icc.event_id = events::user1;
		ev_icc.evt.com.fd = -1;
    	ev_icc.evt.com.flags = EPOLLIN|EPOLLPRI;
		
		iRet = event_ctl(events_mon, ctl::ADD, ev_icc);
		dlog_msg("event_ctl(...icc) ret: %d", iRet);
	}
	else
	{
		g_logger.Log("ICC reader disabled");
	}
	
	//lets switch green led on
	g_leds.changeState(LED_SELECT_MSR1, LED_STATE_ON);

	g_CardAppConfig.SetMainTaskId(get_task_id());

	if(g_CardAppConfig.GetIsUX100() && process_mag_stripe && g_CardAppConfig.GetUxTimerRemoval() && (wait_flags & CARD_APP_CARD_MAGNETIC))
	{
		g_logger.Log("Adding card removal timer...%dms", g_CardAppConfig.GetUxTimerRemoval());
		dlog_msg("Adding card removal timer...%dms", g_CardAppConfig.GetUxTimerRemoval());
		if ((timer_h = set_timer(localTimer, g_CardAppConfig.GetUxTimerRemoval())) >= 0)
		{
			event_ctl(events_mon, ctl::ADD, localTimer);
		}
	}
	else
	{
		#ifdef VFI_PLATFORM_VERIXEVO
		poll_timer = 600;
		#endif

		dlog_msg("Adding card monitor timer...%dms", poll_timer);
		if ((timer_poll = set_timer(checkTimer, poll_timer, true)) >= 0)
		{
			event_ctl(events_mon, ctl::ADD, checkTimer);
		}
	}
	
	EMVlog("START WAITING FOR EVENTS...");

	g_CardAppConfig.SetLogADK(false);
	do
	{
		pml_events.clear();
		//g_logger.Log("START WAITING FOR EVENTS process mag stripe: %d, card with mag stripe: %d timeout: %d", process_mag_stripe, card_with_mag_stripe, pml_timeout);
		//dlog_msg("START waiting, process mag stripe: %d, card with mag stripe: %d timeout: %d", process_mag_stripe, card_with_mag_stripe, pml_timeout);
		wait_event_ret = event_wait(events_mon, pml_events, pml_timeout);
		eventsFlatten(events_mon, pml_events);
		if((pml_events.size() > 0) || (wait_event_ret > 0))
		{
			
			for (eventset_t::iterator it = pml_events.begin() ; it != pml_events.end(); ++it)
			{
				//dlog_msg("event received %d", it->event_id);
				enum {
					ICC = 1,
					SWIPE = 2,
					TIMER = 4,
					PIPE = 8
				} EVENTS;
				unsigned long event_flags = 0;
				switch (it->event_id)
				{
					case events::icc: event_flags |= ICC; break;
					case events::user0: event_flags |= SWIPE; break;
					case events::mag: event_flags |= SWIPE; break;
					case events::timer: event_flags |= TIMER; break;
					case events::ipc: event_flags |= PIPE; break;
				}
				while (event_flags)
				{
					if (event_flags & TIMER)
					{
						if((timer_h > 0) && !check_timer(localTimer))
						{
							dlog_msg("TIMEOUT waiting for mag stripes!!!");
							evt_Ret |= CARD_APP_EVT_CARD_OUT;
							process_mag_stripe = false;		//ignore mag event after timeout
							clr_timer(localTimer);
							timer_h = -1;
						}
						if((timer_poll > 0) && !check_timer(checkTimer))
						{
							//dlog_msg("Checking card status!!!");
							bool inserted = isCardInserted();
							if(is_inserted != inserted)
							{
								is_inserted = inserted;
								if(is_inserted)
								{
									dlog_msg("CARD INSERTED");
									g_logger.Log("CARD INSERTED");
									evt_Ret |= CARD_APP_EVT_CARD_IN;
									icc_card_status = RESP_CODE_CARD_INSERTED;
									g_leds.changeState(LED_SELECT_MSR3, LED_STATE_ON);
									icc_card_type = CARD_APP_CARDTYPE_EMV_ASYNC;
								}
								else
								{
									dlog_msg("CARD REMOVED");
									g_logger.Log("CARD REMOVED");
									// Clear TLV pool
									evt_Ret = CARD_APP_EVT_NONE;
									Close_Kernel();
									
									g_EMVcollection.ClearEverything();
									g_CardAppConfig.RestoreDefaults();
									g_CEMV.SetPINStatus(PIN_ENTRY_STATUS_NOT_SET);
									g_leds.changeState(LED_SELECT_MSR1|LED_SELECT_MSR2|LED_SELECT_MSR3, LED_STATE_OFF);
									icc_card_type = CARD_APP_CARDTYPE_NOT_SET;
									icc_card_status = RESP_CODE_CARD_INSERTED;
									if(g_CardAppConfig.GetIsUX100())
									{
										if(g_CardAppConfig.GetIgnoreICCevt() || (!(wait_flags & CARD_APP_CARD_MAGNETIC)))
										{
											g_logger.Log("Answering immediately to ICC event...");
											dlog_alert("Answering immediately to ICC event...");
											evt_Ret |= CARD_APP_EVT_CARD_OUT;
											continue_waiting = false;
										}
										else if((wait_flags & CARD_APP_CARD_MAGNETIC) && card_with_mag_stripe)
										{
											dlog_alert("Forcing wait for swipe data...");
											pml_timeout = wait_timeout;
											evt_Ret |= CARD_APP_EVT_MSR_READY;
										}
										else
										{
											if(1)
											{
												g_logger.Log("Adding card removal delay timer...%dms", CARD_APP_TIMER_OUT_DELAY);
												dlog_msg("Adding card removal delay timer...%dms", CARD_APP_TIMER_OUT_DELAY);
												if ((timer_h = set_timer(localTimer, CARD_APP_TIMER_OUT_DELAY)) >= 0)
												{
													event_ctl(events_mon, ctl::ADD, localTimer);
												}
												evt_Ret |= CARD_APP_EVT_MSR_READY;
												card_with_mag_stripe = true;
											}
											else
											{
												
												dlog_alert("Answering immediately to ICC event...");
												evt_Ret |= CARD_APP_EVT_CARD_OUT;
												continue_waiting = false;
											}
										}
										if(card_with_mag_stripe) process_mag_stripe = true;

										card_with_mag_stripe = false;
									}
									else
									{
										evt_Ret |= CARD_APP_EVT_CARD_OUT;
									}
								}
								
								#ifdef VFI_PLATFORM_VERIXEVO
								continue_waiting = false;
								#endif
							}
							else
							{
								//dlog_msg("No change...");
								evt_Ret = CARD_APP_EVT_NONE;
							}
							
						}
						event_flags &= ~TIMER;
						continue;
					}
					if (event_flags & SWIPE)
					{
						dlog_msg("Card swiped (user event received, reading data)...");
						//waitMagSemaphore();
						if (g_CardAppConfig.GetIsUX100())
						{
							if ((process_mag_stripe || g_CardAppConfig.GetIgnoreICCevt() || (isCardRemoved() && is_inserted)) && (track_status[0] == 0 || track_status[1] == 0 || track_status[2] == 0))
							{
								dlog_msg("Got mag swipe on Ux");
								evt_Ret |= CARD_APP_EVT_MSR;
								card_with_mag_stripe = false;
								getMsrTracks = true;
								continue_waiting = false;
								if ( !(is_inserted = isCardInserted()) )
								{
									g_EMVcollection.ClearEverything();
									g_CardAppConfig.RestoreDefaults();
									g_CEMV.SetPINStatus(PIN_ENTRY_STATUS_NOT_SET);
									g_leds.changeState(LED_SELECT_MSR1|LED_SELECT_MSR2|LED_SELECT_MSR3, LED_STATE_OFF);
									icc_card_type = CARD_APP_CARDTYPE_NOT_SET;
								}
								//releaseMagSemaphore();
								break;
							}
							else
							{
								dlog_alert("Ignoring mag swipe on Ux");
								g_logger.Log("Ignoring mag swipe on Ux");
								ClearMagstripeData();
								card_with_mag_stripe = true;
								evt_Ret &= (~CARD_APP_EVT_MSR);
								evt_Ret |= CARD_APP_EVT_MSR_READY;
							}
						}
						else
						{
							evt_Ret |= CARD_APP_EVT_MSR;
							card_with_mag_stripe = true;
							#ifdef VFI_PLATFORM_VERIXEVO
							processMagReader();
							continue_waiting = false;
							#endif
							getMsrTracks = true;
						}
						//releaseMagSemaphore();
						event_flags &= ~SWIPE;
						continue;
					}
					if (event_flags & PIPE)
					{
						dlog_msg("IPC event received");
						evt_Ret |= CARD_APP_EVT_PIPE;
						continue_waiting = false;
						event_flags &= ~PIPE;
						continue;
					}
				}
			}
		}
		else
		{
			dlog_msg("No more events in the queue, exiting for processing");
			continue_waiting = false;
		}
		if(continue_waiting && (evt_Ret == CARD_APP_EVT_NONE))
		{
			pml_timeout = wait_timeout;
		}
		else
		{
			pml_timeout = 5;

		}
		
	}
	while(continue_waiting);

	g_CardAppConfig.SetLogADK(true);
	if(timer_h >= 0)
	{
		dlog_msg("monitor_ICC: Closing card removal timer");
		clr_timer(localTimer);
	}

	if(timer_poll >= 0)
	{
		dlog_msg("monitor_ICC: Closing check timer");
		clr_timer(checkTimer);
	}

	logCardEvents(evt_Ret);
	
	event_close(events_mon);
	dlog_msg("Waiting for swipe: %d, mag card: %d", process_mag_stripe, card_with_mag_stripe);

	return evt_Ret;
}

int CEMVtrns::wait_for_card(int card_type, unsigned int timeout, bool detect_card_type /* = false */, int icc_flags /* = 0 */)
{
	int iRet = CARD_APP_EVT_NONE;
	int reset_ret = CARD_APP_RET_ICC_FAIL;
	int m_PosTimeoutTimer = -1;
	const unsigned char GEM_CARD_ID = 0x53;
	long pml_timeout = INFINITE_TIMEOUT;

	wait_flags = card_type;
	g_logger.Log("wait_for_card flags set to: 0x%.02X", wait_flags);


	if(isCardRemoved())
	{
		do
		{
			g_logger.Log("wait_far_card calling monitor_ICC");
			iRet = monitor_ICC(0, timeout);
		}
		while(iRet == CARD_APP_EVT_MSR_READY);	//only wait any other status must be reported (== is here intentially!!!)
	}
	else
	{
		icc_flags |= WAIT_FOR_CARD_SKIP_KERNEL_REINIT;
		dlog_msg("Processing following transaction without kernel re-init!");
		g_logger.Log("Processing following transaction without kernel re-init!");
	}

	
	if(isCardInserted())
	{
		dlog_msg("ICC card in reader trying Reset (flags: 0x%X)", icc_flags);
		g_logger.Log("ICC card in reader trying Reset (flags: 0x%X)", icc_flags);

		reset_ret = processICCReader(icc_flags);

		iRet = CARD_APP_EVT_CARD_IN;
				
		dlog_msg("Answering immediately...");
		if (isCardRemoved()) iRet = CARD_APP_EVT_CARD_OUT;
			
	}


	
	return iRet;
}

std::string hexToAscii(unsigned char *in,int inLen) {
	char tmpCharBuf[3];
	std::string tmpString;
	for(int i=0;i<inLen;i++) {
		sprintf(tmpCharBuf,"%02X", in[i]);
		tmpString += tmpCharBuf;
	}

	return tmpString;
}



int CEMVtrns::EMVtransaction(int opers, CEMV *cEMV)
{

	int iRet = CARD_APP_RET_OK;
	int numTLV = 0;
	int post_mask;
	int offline_mask;
	bool permanent = false;

	post_mask = CARD_APP_OPER_EXT_AUTH | CARD_APP_OPER_SECOND_AC | CARD_APP_OPER_SCRIPT;

//	printConfig(EMV_CONFIG_FILE);

	g_logger.Log("\r\n***** EMVtransaction oper: 0x%X *****\r\n", opers);
//	printf("start EMV Processing \r\n");

	if(opers & CARD_APP_OPER_CONFIG)
	{
		dlog_msg("Updating configuration files");
		permanent = true;
		//let's open kernel as all subsequent functions keeps it open if was opened before
		g_CEMVtrns.Open_Kernel();
	}
	if((opers & post_mask) == post_mask)
	{
		g_logger.Log("Cleaning host data structure");
//		printf("Cleaning host data structure \r\n");
		//online processing required, we must clean host response structure
		g_EMVcollection.hostData.LenAuth = 0;
		g_EMVcollection.hostData.AuthData = NULL;
		strcpy((char *)g_EMVcollection.hostData.AuthResp, "00");
		g_EMVcollection.hostData.LenScriptCrit = 0;
		g_EMVcollection.hostData.ScriptCritData = NULL;
		g_EMVcollection.hostData.LenScriptUnCrit = 0;
		g_EMVcollection.hostData.ScriptUnCritData = NULL;
		g_EMVcollection.hostData.Result_referral = 1;
		g_EMVcollection.hostData.OnlineResult = 1;
		g_EMVcollection.hostData.Info_Included_Data[0] = INPUT_ONL_ONLINE_RESP;
	}
	if(!(opers & CARD_APP_OPER_SELECT))
	{
		dlog_msg("EMVtransaction:Operations reg: %X", opers);
//		printf("EMVtransaction:Operations reg: %X \r\n", opers);
		numTLV = parseInTLV(InTLVlist, InTLV_list_len, permanent);
		dlog_msg("Added/Updated %d TLV objects in collection", numTLV);
	}
	else
	{
		dlog_msg("icc card type '%d'", icc_card_type);
		if (icc_card_type == CARD_APP_CARDTYPE_NOT_SET)
		{
			if(g_CardAppConfig.GetSuperEMVWaitForCard())
			{
				// we have to perform initialisation
				dlog_msg("Waiting for card insertion (forever...)");
				wait_for_card(wait_flags, 0, true);
			}
			else if(isCardInserted())
			{
				dlog_msg("Card inserted requires powerup... trying...");
				if(wait_for_card(wait_flags, 100, true, false) == CARD_APP_EVT_CARD_IN)
				{
					dlog_msg("ICC Powerup OK");
				}
			}
			else
			{
				dlog_alert("EMV card is NOT inserted, terminating the transaction!");
				return CARD_APP_RET_ICC_FAIL;
			}
		}
		else if (icc_card_type == CARD_APP_CARDTYPE_REQUIRES_POWERUP)
		{
			wait_for_card(wait_flags, 100, g_CardAppConfig.GetWarmResetAfterICCOff());
			icc_card_status = CRD_CODE_CARD_ICC_ENABLED;
		}
		
	}

#ifdef VFI_PLATFORM_VOS
		//	kernel version iq_audi_080517
		if(updated==0)
		{
			CTerminalConfig termcfg;
			EMV_CT_TERMDATA_TYPE TermData;
			EMV_CT_GetTermData(&TermData);
			string emvkernelVer(TermData.KernelVersion);
			termcfg.setEmvKernelVersion(emvkernelVer);
//			printf("iq: kversion [%s] \r\n",TermData.KernelVersion);
			updated=1;
		}
#endif

	if(opers & (~CARD_APP_OPER_CONFIG))
	{
//		printf("if(opers & (~CARD_APP_OPER_CONFIG)) \r\n");
		//first we need to get GUI ready
		if (g_CardAppConfig.GetUseDisplay())
		{
			if(g_CardAppConfig.GuiInterface() == CCardAppConfig::GUI_APP)
			{
				//
			}
			else
			{
				DisplayPromptSelectSection(CARD_APP_GUI_PLEASE_WAIT, CARD_APP_GUI_S_PROMPTS);
			}
		}

		//now we process all requested operations
		if(opers & CARD_APP_OPER_SELECT)
		{
			dlog_msg("EMV Selection");
			iRet = cEMV->SelectApplication();
			dlog_msg("EMVtransaction:Operations reg: %X", opers);
			if (iRet == CARD_APP_RET_OK)
			{
				numTLV = parseInTLV(InTLVlist, InTLV_list_len, false);
				dlog_msg("Added/Updated %d TLV objects in collection", numTLV);
			}
		}
		if((opers & CARD_APP_OPER_GPO) && (iRet == CARD_APP_RET_OK))
		{
			dlog_msg("EMV GPO");
			iRet = cEMV->GetProcessingOptions();
		}
		if((opers & CARD_APP_OPER_READ) && (iRet == CARD_APP_RET_OK))
		{
			dlog_msg("EMV Read");
			iRet = cEMV->ReadCardData();
		}

		offline_mask = CARD_APP_OPER_DATA_AUTH | CARD_APP_OPER_RESTRICT | CARD_APP_OPER_TRM | CARD_APP_OPER_VERIFY | CARD_APP_OPER_FIRST_AC;

		if(((opers & offline_mask) == offline_mask) && (iRet == CARD_APP_RET_OK))
		{
			dlog_msg("EMV Offline Processing");
			iRet = cEMV->OfflineSteps(EMV_ADK_OK);
		}
		else
		{
			if((opers & CARD_APP_OPER_DATA_AUTH) && (iRet == CARD_APP_RET_OK))
			{
				dlog_msg("EMV Data Auth");
				//iRet = cEMV->DataAuthentication();
				iRet = cEMV->OfflineSteps(EMV_ADK_APP_REQ_DATAAUTH);
			}
			if((opers & CARD_APP_OPER_RESTRICT) && (iRet == CARD_APP_RET_OK))
			{
				dlog_msg("EMV Restrictions");
				iRet = cEMV->ProcessRestrictions();
			}

			if((opers & CARD_APP_OPER_VERIFY) && (iRet == CARD_APP_RET_OK))
					{
				dlog_msg("EMV Cardholder Verification");
				//iRet = cEMV->CardholderVerification();
				iRet = cEMV->OfflineSteps(EMV_ADK_APP_REQ_CVM_END);
			}
			if((opers & CARD_APP_OPER_TRM) && (iRet == CARD_APP_RET_OK))
			{
				dlog_msg("EMV TRM");
				//iRet = cEMV->TerminalRiskManagement();
				iRet = cEMV->OfflineSteps(EMV_ADK_APP_REQ_RISK_MAN);
			}

			if((opers & CARD_APP_OPER_FIRST_AC) && (iRet == CARD_APP_RET_OK))
			{
				dlog_msg("EMV First AC");
				iRet = cEMV->FirstAC();
			}
		}

		if(((opers & post_mask) == post_mask) && (iRet == CARD_APP_RET_OK))
		{
			dlog_msg("EMV UseHostData - opers:%X params:%X", opers, post_mask);
			iRet = cEMV->PostOnline();
		}
		else
		{
			if((opers & CARD_APP_OPER_EXT_AUTH) && (iRet == CARD_APP_RET_OK))
			{
				dlog_msg("EMV External Auth");
				iRet = cEMV->ExternalAuthenticate();
			}
			if((opers & CARD_APP_OPER_SCRIPT) && (iRet == CARD_APP_RET_OK))
			{
				dlog_msg("EMV Script 71");
				iRet = cEMV->ProcessScript();
			}
			if((opers & CARD_APP_OPER_SECOND_AC) && (iRet == CARD_APP_RET_OK))
			{
				dlog_msg("EMV Second AC");
				iRet = cEMV->SecondAC();
			}
			if((opers & CARD_APP_OPER_SCRIPT) && (iRet == CARD_APP_RET_OK))
			{
				dlog_msg("EMV Script 72");
				iRet = cEMV->ProcessScript();
			}

		}
		if(opers & CARD_APP_OPER_PIN_VERIFY)
		{
			dlog_msg("Offline PIN verification for external kernels");
			if (g_CEMV.isExternalPIN()) g_CardAppConfig.SetPINtype(CARD_APP_EXTERNAL_PIN_OFFLINE);
			else g_CardAppConfig.SetPINtype(CARD_APP_PIN_OFFLINE_EXT);
			unsigned char pinTryCtr = 3;
			int pinTryCtrLen = 0;
			if (g_EMVcollection.GetTLVData(TAG_9F17_PIN_TRIES_LEFT, &pinTryCtr, &pinTryCtrLen) <= 0)
			{
				pinTryCtr = 3;
			}
			switch (pinTryCtr)
			{
			default:
				if (g_CEMV.GetPINStatus() == PIN_ENTRY_STATUS_NOT_SET || !g_CardAppConfig.GetShowIncorrectPIN())
				{
					g_CEMV.SetPINStatus(PIN_ENTRY_STATUS_OK);
					g_CEMV.SetPINEntryStatus(PIN_ENTRY_STATUS_OK);
				}
				else
				{
					g_CEMV.SetPINStatus(PIN_ENTRY_STATUS_BAD_PIN);
					g_CEMV.SetPINEntryStatus(PIN_ENTRY_STATUS_BAD_PIN);
				}
				break;
			case 1:
				g_CEMV.SetPINStatus(PIN_ENTRY_STATUS_LAST_TRY);
				g_CEMV.SetPINEntryStatus(PIN_ENTRY_STATUS_LAST_TRY);
				break;
			case 0:
				g_CEMV.SetPINStatus(PIN_ENTRY_STATUS_BLOCKED);
				g_CEMV.SetPINEntryStatus(PIN_ENTRY_STATUS_BLOCKED);
				break;
			}
			iRet = cEMV->OfflinePINVerification();
		}
		//additional status checking - required for cardslot closure
		cEMV->checkStatus(CARD_APP_OPER_TLV);

		// Finally, perform card presence check
		if (cEMV->GetStatusDetail() & CARD_APP_STATUS_D_REMOVED)
		{
			dlog_alert("Card removal detected, clearing the collxn!");
			//iRet = CARD_APP_RET_REMOVED;
			Close_Kernel();
			g_EMVcollection.ClearEverything();
			g_CardAppConfig.RestoreDefaults();
			TLV_data_len = 0;
		}
		else
		{
			TLV_data_len = parseOutTLV(OutTLVlist, OutTLV_list_len, OutTLVdata, OutTLV_data_len);
		}
	}
	else
	{
//		printf("if(opers & (~CARD_APP_OPER_CONFIG)) else  \r\n");
		dlog_msg("Adding/Reading TLV objects only");
		TLV_data_len = parseOutTLV(OutTLVlist, OutTLV_list_len, OutTLVdata, OutTLV_data_len);
	}

	if(permanent && numTLV == 0)
	{
		dlog_msg("Preparing configuration data for AID: %s", g_CardAppConfig.GetCurrentAID().c_str());
		cUpdateCfg UpdateCfg;
		if (g_CardAppConfig.GetAIDOper() == 0)
		{
			std::string cfg_data = UpdateCfg.getConfig((char *)g_CardAppConfig.GetCurrentAID().c_str(), g_CardAppConfig.GetCAPKndx());
			TLV_data_len = cfg_data.size();
			memcpy(OutTLVdata, cfg_data.data(), TLV_data_len);
			//cfg_data+=UpdateCfg.getCAPK(g_CardAppConfig.GetCAPKndx());
			//TLV_data_len = cfg_data.size()/2;
			//com_verifone_pml::svcDsp2Hex(cfg_data.c_str(), TLV_data_len*2, reinterpret_cast<char *>(OutTLVdata), TLV_data_len);
		}
		else
		{
			if (g_CardAppConfig.GetCAPKndx() != 0) UpdateCfg.deleteCAPK(g_CardAppConfig.GetCurrentAID().c_str(), g_CardAppConfig.GetCAPKndx());
			else UpdateCfg.deleteConfig(g_CardAppConfig.GetCurrentAID().c_str());
		}

	}
	dlog_msg("Sent %d bytes of requested data", TLV_data_len);

	//finnaly we check if transaction is completed
	if(!(opers & CARD_APP_OPER_CONFIG))
	{
		if (cEMV->GetStatusMain() & CARD_APP_STATUS_M_STOP)
		{
			dlog_alert("Transaction completed");
			if (cEMV->GetStatusDetail() & CARD_APP_STATUS_D_REMOVED)
			{
				dlog_alert("Card removal detected, clearing the collxn!");
				//iRet = CARD_APP_RET_REMOVED;
				Close_Kernel();
				g_EMVcollection.ClearEverything();
				g_CardAppConfig.RestoreDefaults();
			}
			else
			{
				#ifdef VFI_PLATFORM_VOS
                //Close_Kernel();
				EMV_CT_SmartPowerOff(0);
				icc_card_type = CARD_APP_CARDTYPE_NOT_SET;
                #endif
				
				g_logger.Log("Transaction completed, card powered down, still in reader...");
			}
		}
	}

	return iRet;
}


/*
 * DetectCardType() only checks for Memory / I2C / TradePro cards!
 * EMV detection is performed in main thread!
 */
bool CEMVtrns::DetectCardType()
{
	unsigned char retVal;

	// Check SLE442 card type
	do
	{
		// It is assumed here that each method, which attempts to detect a card, cleans afterwards
		// which means, calls Terminate_CardSlot() in case detection fails
		dlog_msg("Trying with SLE44x2...");
		if (sle442.is_card_memory_card(ATR))
		{
			icc_card_type = CARD_APP_CARDTYPE_EMV_SYNC;
			break;
		}
		// Try I2C
		dlog_msg("Trying with I2C...");
		// Try reading it to confirm???
		if (!IsI2CCard()) // LOCAL function!
		{
			dlog_error("No I2C card!");
		}
		else
		{
			// That's it - we have I2C card!
			icc_card_type = CARD_APP_CARDTYPE_I2C;
			ATR[0] = 0;
			break;
		}
		// Further cards if necessary
	} while (false);

	if (icc_card_type != CARD_APP_CARDTYPE_NOT_SET) return true;

	dlog_msg("unknown card type in card slot");
	// card in slot, no ATR returned
	//initialisation attempts  might've let cardslot in a funny state

	return false;
}


/*
 * IsI2CCard() tries to detect I2C card
 * As there seem to be no direct way to detect such card, we're trying to read some bytes...
 */
bool CEMVtrns::IsI2CCard()
{

	bool result = false;
	//JACEK - memory cards are not supported yet
/*
	// As there is no way to get ATR or detect I2C cards in any way, we have to simply try reading that card and hope that's enough...
	short shBytesToRead = 4;
	short shAddressMSB = 0x00;
	short shAddressLSB = 0x00;
	unsigned char ucCmdData[100] = {0};
	unsigned char ucRespData[100] = {0};
	unsigned long ulCmdLen = 0;
	unsigned char ucRetVal = 0;
	unsigned short usRespLen = 0;
	bool result = false;

	// set card type to iso 7816 sync card of type SLE44x2
	ucRetVal = Set_Card_Type( g_CEMV.ICC_reader, EE2K4K, ISO_7816 );
	dlog_msg("Set Card Type returned %d", ucRetVal);
	if (ucRetVal != CARDSLOT_SUCCESS)
	{
		dlog_alert("Cannot initialise I2C!");
	}
	else
	{
		ucRetVal = Reset_CardSlot( g_CEMV.ICC_reader, RESET_COLD );
		dlog_msg("Sync reset returned %d", ucRetVal );
		if (ucRetVal != CARDSLOT_SUCCESS)
		{
			dlog_error("Reset_CardSlot failed!");
		}
		else
		{
			EE2K4K_READ_BYTES(ucCmdData, shAddressMSB, shAddressLSB, shBytesToRead, ulCmdLen);
			ucRetVal = Transmit_APDU(g_CEMV.ICC_reader, ucCmdData, ulCmdLen, ucRespData, &usRespLen);
			if(ucRetVal != CARDSLOT_SUCCESS || usRespLen == 0)
			{
				dlog_alert("Cannot read I2C card!");
			}
			else
			{
				dlog_hex(ucRespData, usRespLen, "READ FROM CARD");
				dlog_msg("I2C read success, assuming I2C card inserted");
				result = true;
			}
		}
	}
	if (result == false)
	{
		// Terminate cardslot - may be in bad state
		ucRetVal = Terminate_CardSlot(g_CEMV.ICC_reader, SWITCH_OFF_CARD);
		dlog_msg("Terminate CardSlot returned %d", ucRetVal);
		Close_CardSlot(g_CEMV.ICC_reader);
	}
	*/
	return result;
}

void CEMVtrns::ClearEMVCollxn()
{
	// todo: implement properly...
	g_EMVcollection.ClearEverything();
}

#ifdef VFI_PLATFORM_VOS


int CEMVtrns::waitMagSemaphore()
{
	dlog_msg("Start waiting on Mag Semaphore...");
    return sem_wait(&magMutex);
}

int CEMVtrns::releaseMagSemaphore()
{
	dlog_msg("Releasing Mag Semaphore...");
    return sem_post(&magMutex);
}

bool CEMVtrns::isMagSemaphoreBusy()
{
    int val = 1;
    if (!sem_getvalue(&magMutex, &val))
    {
        return val == 0;
    }
    return false;
}

#endif

#ifdef VFI_PLATFORM_VERIXEVO

int CEMVtrns::waitMagSemaphore()
{
    return 0;
}

int CEMVtrns::releaseMagSemaphore()
{
    return 0;
}

bool CEMVtrns::isMagSemaphoreBusy()
{
    
    return false;
}

#endif

void CEMVtrns::ClearMagstripeData()
{
    memset(track1, 0, sizeof(track1));
    memset(track2, 0, sizeof(track2));
    memset(track3, 0, sizeof(track3));
    memset(track_status, 0, sizeof(track_status));
}


int CEMVtrns::getMagData(IPCPacket & response)
{
	int numBytes = 0;
	int tr_len=0;
    if (getMsrTracks)
    {
        waitMagSemaphore();
        if((tr_len=strlen((const char *)track1)))
        {
            dlog_msg("Adding track1...%d bytes", strlen((const char *)track1));
            g_logger.Log("Adding track1...%d bytes", strlen((const char *)track1));
            response.addTag(ConstData(arbyTagTrack1, MAG_TRACK_TS), ConstData(track1, strlen((const char *)track1)));
            numBytes+=tr_len;
            swipe_cntrs.rep_track1++;
        }
        if((tr_len=strlen((const char *)track2)))
        {
            dlog_msg("Adding track2...%d bytes", strlen((const char *)track2));
            g_logger.Log("Adding track2...%d bytes", strlen((const char *)track2));
            response.addTag(ConstData(arbyTagTrack2, MAG_TRACK_TS), ConstData(track2, strlen((const char *)track2)));
            numBytes+=tr_len;
            swipe_cntrs.rep_track2++;
        }
        if((tr_len=strlen((const char *)track3)))
        {
            dlog_msg("Adding track3...%d bytes", strlen((const char *)track3));
            g_logger.Log("Adding track3...%d bytes", strlen((const char *)track3));
            response.addTag(ConstData(arbyTagTrack3, MAG_TRACK_TS), ConstData(track3, strlen((const char *)track3)));
            numBytes+=tr_len;
            swipe_cntrs.rep_track3++;
        }

        if((tr_len=strlen((const char *)track_status)))
        {
            response.addTag(ConstData(arbyTagSwipeStatus, SWIPE_STATUS_TS), ConstData(track_status, sizeof(track_status)));
            numBytes+=tr_len;
        }

        ClearMagstripeData();
        process_mag_stripe = false;
        releaseMagSemaphore();
        getMsrTracks = false;
    }
	return numBytes;
}
