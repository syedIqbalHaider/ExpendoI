#ifndef __cplusplus
#  error "This file is for C++ only!"
#endif

/***************************************************************************
 *
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 *
 **************************************************************************/
/**
 * @file        EMVTools.cpp
 *
 * @author      Jacek Olbrys
 *
 * @brief       EMV Tools
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C++ Coding
 *              Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/
#include "EMV_Common_Interface.h"
#include "EMV_CT_Interface.h"
#include "E2E_EMV_CT_Serialize.h"

#include "cIPCInterface.h"
#include "libipcpacket/ipcpacket.hpp"

#include <svc.h>
#include <svcsec.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <vector>
#include <dlfcn.h>
#include <errno.h>


#include <libpml/pml.h>
#include "libpml/pml_abstracted_api.h"
#include "libpml/pml_port.h"
#include <liblog/logsys.h>
#include <libprompts/user_prompt_lite.hpp>
//#include "GuiApp.h"


#include "app_selection.h"
#include "cEMV.h"


#include "cTransaction.h"
#include "cCARDAppConfig.h"
#include "EMVtools.h"
#include <libminini/minIni.h>

#include <tlv-lite/SafeBuffer.hpp>
#include <tlv-lite/TagLenVal.hpp>

#include <tlv-lite/ConstData.h>
#include <tlv-lite/ValueConverter.hpp>
#include <tlv-lite/ConstValue.hpp>
#include <libipcpacket/ipcpacket.hpp>


#include "PIN.h"

#include <algorithm>

#ifdef VFI_GUI_GUIAPP
#include <libprompts/user_prompt_lite.hpp>
//#include "GuiApp.h"

using namespace com_verifone_guiapi;
#endif




void EMVlog(char const * msg);


//extern "C"
//{
//#include "vxemvap.h"
//}

/***************************************************************************
 * Definitions
 **************************************************************************/
#define CURR_CODE_LEN 8
#define CURRENCY_CODE_NULL {0, {'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0'}}
#define CARD_APP_DEF_MAX_TLV_LEN    256
#define M_MAX_PIN_LENGTH_MAX 12
#define M_MIN_PIN_LENGTH_MIN 4
#define WINDOW_MAX 0xFFFFFFFF

/***************************************************************************
 * Using
 **************************************************************************/
using namespace com_verifone_guiapi;
using namespace com_verifone_emv;
using namespace com_verifone_pml;

extern CCardAppConfig   g_CardAppConfig;
extern CEMV         g_CEMV;
extern CEMVtrns     g_CEMVtrns;
extern cEMVcollection	g_EMVcollection;
extern logger g_logger;

static void * EMV_lib_handle = NULL;

unsigned char (*EMV_local_WriteTagData)(unsigned char *data, unsigned char length, unsigned short parent, unsigned long tag) = 0;



//extern EMV_CONFIG EMVGlobConfig;

typedef struct currency_code_s
{
      uint16_t code; /**< Numeric currency code  */
      char name[CURR_CODE_LEN+1]; /**< Currency code name */
} currency_code_t;

const currency_code_t M_CURRENCY_CODES[] =
    {
        {9999, "   "}, {784, "AED"}, {971, "AFN"}, {8, "ALL"}, {51, "AMD"}, {532, "ANG"},
        {973, "AOA"}, {32, "ARS"}, {36, "AUD"}, {533, "AWG"}, {944, "AZN"},
        {977, "BAM"}, {52, "BBD"}, {50, "BDT"}, {975, "BGN"}, {48, "BHD"},
        {108, "BIF"}, {60, "BMD"}, {96, "BND"}, {68, "BOB"}, {984, "BOV"},
        {986, "BRL"}, {44, "BSD"}, {64, "BTN"}, {72, "BWP"}, {974, "BYR"},
        {84, "BZD"}, {124, "CAD"}, {976, "CDF"}, {947, "CHE"}, {756, "CHF"},
        {948, "CHW"}, {990, "CLF"}, {152, "CLP"}, {156, "CNY"}, {170, "COP"},
        {970, "COU"}, {188, "CRC"}, {192, "CUP"}, {132, "CVE"}, {203, "CZK"},
        {262, "DJF"}, {208, "DKK"}, {214, "DOP"}, {12, "DZD"}, {233, "EEK"},
        {818, "EGP"}, {232, "ERN"}, {230, "ETB"}, {978, "EUR"}, {242, "FJD"},
        {238, "FKP"}, {826, "GBP"}, {981, "GEL"}, {936, "GHS"}, {292, "GIP"},
        {270, "GMD"}, {324, "GNF"}, {320, "GTQ"}, {328, "GYD"}, {344, "HKD"},
        {340, "HNL"}, {191, "HRK"}, {332, "HTG"}, {348, "HUF"}, {360, "IDR"},
        {376, "ILS"}, {356, "INR"}, {368, "IQD"}, {364, "IRR"}, {352, "ISK"},
        {388, "JMD"}, {400, "JOD"}, {392, "JPY"}, {404, "KES"}, {417, "KGS"},
        {116, "KHR"}, {174, "KMF"}, {408, "KPW"}, {410, "KRW"}, {414, "KWD"},
        {136, "KYD"}, {398, "KZT"}, {418, "LAK"}, {422, "LBP"}, {144, "LKR"},
        {430, "LRD"}, {426, "LSL"}, {440, "LTL"}, {428, "LVL"}, {434, "LYD"},
        {504, "MAD"}, {498, "MDL"}, {969, "MGA"}, {807, "MKD"}, {104, "MMK"},
        {496, "MNT"}, {446, "MOP"}, {478, "MRO"}, {480, "MUR"}, {462, "MVR"},
        {454, "MWK"}, {484, "MXN"}, {979, "MXV"}, {458, "MYR"}, {943, "MZN"},
        {516, "NAD"}, {566, "NGN"}, {558, "NIO"}, {578, "NOK"}, {524, "NPR"},
        {554, "NZD"}, {512, "OMR"}, {590, "PAB"}, {604, "PEN"}, {598, "PGK"},
        {608, "PHP"}, {586, "PKR"}, {985, "PLN"}, {600, "PYG"}, {634, "QAR"},
        {946, "RON"}, {941, "RSD"}, {643, "RUB"}, {646, "RWF"}, {682, "SAR"},
        {90, "SBD"}, {690, "SCR"}, {938, "SDG"}, {752, "SEK"}, {702, "SGD"},
        {654, "SHP"}, {703, "SKK"}, {694, "SLL"}, {706, "SOS"}, {968, "SRD"},
        {678, "STD"}, {760, "SYP"}, {748, "SZL"}, {764, "THB"}, {972, "TJS"},
        {795, "TMM"}, {788, "TND"}, {776, "TOP"}, {949, "TRY"}, {780, "TTD"},
        {901, "TWD"}, {834, "TZS"}, {980, "UAH"}, {800, "UGX"}, {840, "USD"},
        CURRENCY_CODE_NULL
    };

// Based on this table: http://www.xe.com/symbols.php
// Some symbols were not listed and are left as codes
const currency_code_t M_CURRENCY_SYMBOLS[] =
     {
        {9999, "   "},                     {784, "AED"},                       {971, "\xD8\x8B"},
        {8, "\x4C\x65\x6B"},               {51, "AMD"},                        {532, "\xC6\x92"},
        {973, "AOA"},                      {32, "$"},                          {36, "$"},
        {533, "\xC6\x92"},                 {944, "\xD0\xBC\xD0\xB0\xD0\xBD"},
        {977, "\x4B\x4D"},                 {52, "$"},                          {50, "BDT"},
        {975, "\xD0\xBB\xD0\xB2"},         {48, "BHD"},
        {108, "BIF"},                      {60, "$"},                          {96, "$"},
        {68, "$b"},                        {984, "BOV"},
        {986, "R$"},                       {44, "$"},                          {64, "BTN"},
        {72, "P"},                         {974, "p."},
        {84, "BZ$"},                       {124, "$"},                         {976, "CDF"},
        {947, "CHE"},                      {756, "CHF"},
        {948, "CHW"},                      {990, "CLF"},                       {152, "$"},
        {156, "\xC2\xA5"},                 {170, "$"},
        {970, "COU"},                      {188, "\xE2\x82\xA1"},              {192, "\xE2\x82\xB1"},
        {132, "CVE"},                      {203, "\x4B\xC4\x8D"},
        {262, "DJF"},                      {208, "kr"},                        {214, "RD$"},
        {12, "DZD"},                       {233, "kr"},
        {818, "\xC2\xA3"},                 {232, "ERN"},                       {230, "ETB"},
        //{978, "\xE2\x82\xAC"},             {242, "$"},
        // Kamil_P1: Due to font incompatibility, changed the EUR character to the one matching font.
        {978, "\xC2\x80"},                 {242, "$"},
        {238, "\xC2\xA3"},                 {826, "\xC2\xA3"},                  {981, "GEL"},
        {936, "GHS"},                      {292, "\xC2\xA3"},
        {270, "GMD"},                      {324, "GNF"},                       {320, "Q"},
        {328, "$"},                        {344, "$"},
        {340, "L"},                        {191, "kn"},                        {332, "HTG"},
        {348, "Ft"},                       {360, "Rp"},
        {376, "\xE2\x82\xAA"},             {356, "INR"},                       {368, "IQD"},
        {364, "\xEF\xB7\xBC"},             {352, "ISK"},
        {388, "J$"},                       {400, "JOD"},                       {392, "\xC2\xA5"},
        {404, "KES"},                      {417, "\xD0\xBB\xD0\xB2"},
        {116, "\xE1\x9F\x9B"},             {174, "KMF"},                       {408, "\xE2\x82\xA9"},
        {410, "\xE2\x82\xA9"},             {414, "KWD"},
        {136, "$"},                        {398, "\xD0\xBB\xD0\xB2"},          {418, "\xE2\x82\xAD"},
        {422, "\xC2\xA3"},                 {144, "\xE2\x82\xA8"},
        {430, "$"},                        {426, "LSL"},                       {440, "Lt"},
        {428, "Ls"},                       {434, "LYD"},
        {504, "MAD"},                      {498, "MDL"},                       {969, "MGA"},
        {807, "\xD0\xB4\xD0\xB5\xD0\xBD"}, {104, "MMK"},
        {496, "\xE2\x82\xAE"},             {446, "MOP"},                       {478, "MRO"},
        {480, "\xE2\x82\xA8"},             {462, "MVR"},
        {454, "MWK"},                      {484, "$"},                         {979, "MXV"},
        {458, "RM"},                       {943, "MT"},
        {516, "$"},                        {566, "\xE2\x82\xA6"},              {558, "C$"},
        {578, "kr"},                       {524, "\xE2\x82\xa8"},
        {554, "$"},                        {512, "\xEF\xB7\xBC"},              {590, "B/."},
        {604, "S/."},                      {598, "PGK"},
        {608, "\xE2\x82\xB1"},             {586, "\xE2\x82\xA8"},              {985, "z\xC5\x82"},
        {600, "Gs"},                       {634, "\xEF\xB7\xBC"},
        {946, "lei"},                      {941, "\xD0\x94\xD0\xB8\xD0\xBD."},
        {643, "\xD1\x80\xD1\x83\xD0\xB1"}, {646, "RWF"},                       {682, "\xEF\xB7\xBC"},
        {90, "$"},                         {690, "\xE2\x82\xA8"},              {938, "SDG"},
        {752, "kr"},                       {702, "$"},
        {654, "\xC2\xA3"},                 {703, "SKK"},                       {694, "SLL"},
        {706, "S"},                        {968, "$"},
        {678, "STD"},                      {760, "\xC2\xA3"},                  {748, "SZL"},
        {764, "\xE0\xB8\xBF"},             {972, "TJS"},
        {795, "TMM"},                      {788, "TND"},                       {776, "TOP"},
        {949, "TL"},                       {780, "TT$"},
        {901, "NT$"},                      {834, "TZS"},                       {980, "\xE2\x82\xB4"},
        {800, "UGX"},                      {840, "$"},
        CURRENCY_CODE_NULL
     };

struct ux100SecureVersionStruct
{
	char id_mode[50];
	char family[10];
	char version[12];

};
static struct sigaction sigSegvHandler_org;
static struct sigaction sigKillHandler_org;


//linux signals handling

void mysighandler_segv(int sig)
{
	g_logger.Log("Application crashing due to memory problems... SIGSEGV");
	exit(0);
}

void mysighandler_kill(int sig)
{
	g_logger.Log("Closing cardapp... Termination request");
	g_CEMVtrns.Close_Kernel();
	exit(0);
	
}


void SignalInit(void )
{

	struct sigaction sigHandler;
	memset(&sigHandler, 0, sizeof(struct sigaction));
	sigHandler.sa_handler = mysighandler_segv;
	sigemptyset(&sigHandler.sa_mask);
	sigHandler.sa_flags = SA_NODEFER;
	sigaction(SIGSEGV, &sigHandler, &sigSegvHandler_org);
	memset(&sigHandler, 0, sizeof(struct sigaction));
	sigHandler.sa_handler = mysighandler_kill;
	sigemptyset(&sigHandler.sa_mask);
	sigHandler.sa_flags = SA_NODEFER;
	sigaction(SIGTERM, &sigHandler, &sigKillHandler_org);
	sigaction(SIGINT, &sigHandler, &sigKillHandler_org);
	sigaction(SIGQUIT, &sigHandler, &sigKillHandler_org);

}


//end

int eventsFlatten(int events_handler, eventset_t &evts) 
{
    eventset_t revents;

    for (unsigned i = 0; i < evts.size(); i++) {
        if (evts[i].event_id == events::cascade) {
            eventset_t cascade_events;
            event_read(evts[i].evt.cascade.fd, cascade_events);
            eventsFlatten(events_handler, cascade_events);
            revents.insert(revents.end(), cascade_events.begin(), cascade_events.end());
        } else {
            revents.push_back(evts[i]);
        }
    }

    evts = revents;
    return evts.size();
};


void ICCoutEvent(void)
{

	event_item ev_icc;
	ev_icc.event_id = events::icc;
	ev_icc.evt.user.flags = 0x0;

	std::string name(CARDAPP_NAME);
	std::transform(name.begin(), name.end(), name.begin(), ::toupper);

	EMVlog("Raising ICC OUT event");

	dlog_msg("ICC card out raising event (to: %s)", name.c_str());
	event_raise(name.c_str(), ev_icc);

	
}

int SendMonitorMsg(uint8_t respCode)
{
	int iRet = ESUCCESS;

	#if SEND_EXTRA_STATUS_NOTIFY
	using namespace com_verifone_TLVLite;
	IPCPacket response; 
	dlog_msg("Sending Monitor Message from the command...(respCode: 0x%X to %s)", respCode, g_CardAppConfig.cNotifyName);
	response.setCmdCode(MONITOR_ICC_COM);
	std::string to_app(g_CardAppConfig.cNotifyName);
	response.setAppName(to_app);
	response.addTag(ConstData(arbyTagCode, CODE_TS), MakeBERValue(respCode));
	g_CEMVtrns.getMagData(response);
	if( IPCPACKET_SUCCESS!= response.send() )
	{
		iRet =	ERESPONSESEND;
	}
	#endif
	return iRet;
}

#ifdef VFI_PLATFORM_VOS

long long getTimeStampMs(void)
{
	struct timeval tv;
	gettimeofday(&tv, 0);

	long long retv = (long long)tv.tv_sec*1000;
	retv += tv.tv_usec/1000;

	return retv;
}

#define LOADAVG_PATH "/proc/loadavg"

void logger::Log_loadavg(const char *msg)
{
	char load_data[100];
	int pfd;
	if ((pfd = open(LOADAVG_PATH, O_RDONLY)) != -1)
	{
		int read_bytes = 0;
		if((read_bytes = read(pfd, load_data, sizeof(load_data)-1)))
		{
			load_data[read_bytes] = 0;
			Log("%s: %s", msg, load_data);
			dlog_msg("%s: %s", msg, load_data);
		}
		close(pfd);
	}
}

int logger::setupLog(int type)
{
	struct sockaddr_in sSockData;

	int iRet = 0;
	log_type = type;

	if((log_type == LOG_TO_UDP) && g_CardAppConfig.GetIPLog().size())
	{
		char LOG_HELLO[50];
		sprintf(LOG_HELLO, "Start UDP logging from CARDAPP");
		dlog_msg("Start UDP logging to %s:%d", g_CardAppConfig.GetIPLog().c_str(), port);

		port = 7777;
		//setIPaddress((char *)g_CardAppConfig.GetIPLog().c_str());
		address = g_CardAppConfig.GetIPLog();
		memset(sSockData.sin_zero, 0 , 8);
		//sSockData.sin_addr.s_addr = htonl(inet_addr(address.c_str()));
		sSockData.sin_addr.s_addr = inet_addr(address.c_str());
		sSockData.sin_port = htons(port);
		sSockData.sin_family = AF_INET;
		if((socket_h = socket(AF_INET, SOCK_DGRAM, 0)) >= 0)
		{
			sendto(socket_h, LOG_HELLO, strlen(LOG_HELLO), 0, (struct sockaddr *)&sSockData, sizeof(sSockData));
			if((iRet = connect(socket_h,(struct sockaddr *)&sSockData,sizeof(sSockData))) != 0)
			{
				log_type = LOG_TO_FILE;
				setFilename(ADK_LOG_FILE);
				Log("ERROR!!! Cannot connect socket (%s:%d) errno:%X!!!", address.c_str(), port, errno);
				shutdown(socket_h, 2);
				socket_h = -1;

			}
		}
		else
		{
			log_type = LOG_TO_FILE;
			setFilename(ADK_LOG_FILE);
			Log("ERROR!!! Cannot open socket (%s:%d) errno:%X!!!", address.c_str(), port, errno);
			shutdown(socket_h, 2);
			socket_h = -1;
		}
	}
	else if(log_type != LOG_TO_NULL)
	{
		log_type = LOG_TO_FILE;
	}
	if(log_type == LOG_TO_FILE)
	{
		setFilename(ADK_LOG_FILE);
	}
	time_start = getTimeStampMs();
}

void logger::Log( const char * format, ... )
{
  if(log_type != LOG_TO_NULL)
  {
  	char buffer[2048];
  	char *current;
  	current= buffer;
  	time_end = getTimeStampMs();

	if(time_end - time_start < 0)
		time_start = time_end;
  	sprintf(current, "[%08.08ld]", time_end - time_start);
  	current+=strlen(buffer);
  	va_list args;
  	va_start (args, format);
  	vsnprintf (current, 2048-10, format, args);
  	if(log_type == LOG_TO_UDP)
  	{
		::send(socket_h, buffer, strlen(buffer),0);
	 	::send(socket_h, "\r\n", 2, 0);
  	}
  	if(log_type == LOG_TO_FILE)
  	{
  		int pfd;
		if ((pfd = open(log_file.c_str(), O_RDWR | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP)) != -1)
		{
			lseek (pfd, 0, SEEK_END);
			write(pfd, buffer, strlen(buffer));
			write(pfd, "\r\n", 2);
			close(pfd);
		}
  	}
  	va_end (args);
  }
}

#endif

#define LOG_LINE_SIZE 32
char Hex_digits[]={'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

void logger::LogHex(unsigned char *hex_data, int data_len)
{
	char log_line[LOG_LINE_SIZE+1+LOG_LINE_SIZE/2+1];
	int i, j, k;
	int end_val;

	j=0;
	while(j <= data_len)
	{
		k=0;
		if(data_len - j < LOG_LINE_SIZE/2)
			end_val = data_len-j;
		else
			end_val = LOG_LINE_SIZE/2;
		
		for(i = 0; i < end_val; i++)
		{
			log_line[k++] = Hex_digits[((0xF0&hex_data[i+j]) >> 4)];
			log_line[k++] = Hex_digits[(0x0F&hex_data[i+j])];
		}
		log_line[k++] = ' ';
		for(i = 0; i < end_val; i++)
		{
			if((hex_data[i+j] >= 0x20) && (hex_data[i+j] <= 0x7D))
				log_line[k++] = hex_data[i+j];
			else
				log_line[k++] = '.';
		}
		log_line[k++] = 0;
		Log("%s", log_line);
		j+=LOG_LINE_SIZE/2;
	}
}

void logger::LogAps()
{
	appver::applist_t appList;
	appver::get_apps_list(appList);
	Log("********************");
	Log("There are %d registered apps", appList.size());
	for (appver::applist_t::const_iterator appit = appList.begin(); appit != appList.end(); ++appit)
	{
		std::pair<std::string, std::string > app(*appit);
		Log("App %s, ver %s", app.first.c_str(), app.second.c_str());
		appver::applist_t liblist;
		appver::get_lib_list(liblist, app.first.c_str());
		for (appver::applist_t::const_iterator libit = liblist.begin(); libit!= liblist.end(); ++libit)
		{
			std::pair<std::string, std::string > app(*libit);
			Log("Lib %s, ver %s", app.first.c_str(), app.second.c_str());
			
		}
		
	}
	Log("********************");
	Log("All known libs:");
	appver::applist_t liblist;
	appver::get_lib_list(liblist);
	for (appver::applist_t::const_iterator libit = liblist.begin(); libit!= liblist.end(); ++libit)
	{
		std::pair<std::string, std::string > app(*libit);
		Log("Lib %s, ver %s", app.first.c_str(), app.second.c_str());
	}
	
	Log("********************");
	
}


#ifdef VFI_PLATFORM_VERIXEVO

void logger::Log_loadavg(const char *msg)
{
	dlog_msg("System load data unavailable");
}


void logger::Log( const char * format, ... )
{
  if(log_type != LOG_TO_NULL)
  {
  	char buffer[2048];
  	char *current;
  	current= buffer;
  	time_end = read_ticks();

	if(time_end - time_start < 0)
		time_start = time_end;
  	sprintf(current, "[%08.08ld]", time_end - time_start);
  	current+=strlen(buffer);
  	va_list args;
  	va_start (args, format);
  	vsnprintf (current, 2048-10, format, args);
  	dlog_msg(buffer);
	va_end (args);
  }
}

int logger::setupLog(int type)
{

	int iRet = 0;
	
	time_start = read_ticks();

	return iRet;
}


#endif



#if 0

#define UX100_PIN_MIN 4
#define UX100_PIN_MAX 12
#define UX100_PIN_TIMEOUT 300*1000
#define UX100_PIN_INTERCHAR_TIMEOUT	10*1000

#define UX100_ENTER 0x10
#define UX100_CANCEL 0x0D
#define UX100_CLEAR 0x0E
#define UX100_HELP 0x0F
#define UX100_DIGIT 0x11

int GetPIN_Ux100(void)
{

	int nb_digits = -1;
	bool end = false;
	int key_pressed;
	int retVal;

	if(!g_CardAppConfig.GetIsUX100())
		return PIN_VERIFY_ERROR;

	if(__ux100_keybdInit() != 0)
        {
                dlog_error("Ux keyboard init error!");
		return PIN_VERIFY_ERROR;
        }

	__ux100_dispSetBacklightMode(1, 0);

	if((retVal = iPS_SelectPINAlgo(0x0A)))
	{
		dlog_error("Error setting PIN Algo ret: %X - 1st try", retVal);
		if(retVal == E_KM_BAD_SEQUENCE)
		{
			__ux100_pinAbort();
			retVal = iPS_SelectPINAlgo(0x0A);
			if(retVal != 0)
			{
				dlog_error("Error setting PIN Algo ret: %X - 2nd try", retVal);
				return PIN_VERIFY_ERROR;
			}
		}
		else
		{
			return PIN_VERIFY_ERROR;
		}
	}

	retVal = PIN_VERIFY_ERROR;

	if(__ux100_pinStartPinEntry(UX100_PIN_MIN, UX100_PIN_MAX, UX100_PIN_TIMEOUT) == 0)
	{
		__ux100_pinDelete(0);
		dlog_msg("UX100 - starting PIN entry");
		EMVlog("UX100 - starting PIN entry");
		retVal = PIN_VERIFY_OK;
		while(!end)
		{
			key_pressed = __ux100_keybdGetKey(UX100_PIN_INTERCHAR_TIMEOUT);
			switch(key_pressed)
			{
				case UX100_ENTER:
					__ux100_pinSendEncPinBlockToVault();
					dlog_msg("UX100 - PIN entry DONE!");
					EMVlog("UX100 - PIN entry DONE!");
					end = true;
					retVal = PIN_VERIFY_OK;
					break;
				case -1:
					dlog_msg("UX100 - PIN entry error: %d", errno);
					retVal = PIN_VERIFY_ERROR;
					end = true;
					break;
				default:
					nb_digits = __ux100_pinGetNbCurrDigitEntered();
					break;
			}
			dlog_msg("UX100 - key pressed: %X, PIN digits: %d", key_pressed, nb_digits);

			if(__ux100_pinEntryMode() != 1)
			{
				dlog_msg("UX100 - incorrect status of PIN entry, exiting");
				__ux100_pinAbort();
				end = true;
				retVal = PIN_VERIFY_ERROR;
			}
		}
	}
	return retVal;
}

int VerifyPIN_Ux100()
{
        return 0x9000;
}

void init_pin()
{
   static bool inited;
   if(inited) return;
   inited=true;

   void *handle;

   EMVlog("Detecting terminal type...");

   handle=dlopen("/usr/local/lib/svcmgr/libsvc_ux100.so",RTLD_LAZY);
   if(handle)
   {
      __ux100_pinStartPinEntry         =(int (*)(unsigned char,unsigned char, int))dlsym(handle,"ux100_pinStartPinEntry");
      __ux100_pinDelete                =(int (*)(int))                             dlsym(handle,"ux100_pinDelete");
      __ux100_pinAbort                 =(int (*)())                                dlsym(handle,"ux100_pinAbort");
      __ux100_pinSendEncPinBlockToVault=(int (*)())                                dlsym(handle,"ux100_pinSendEncPinBlockToVault");
      __ux100_pinGetNbCurrDigitEntered =(int (*)())                                dlsym(handle,"ux100_pinGetNbCurrDigitEntered");
      __ux100_pinEntryMode             =(int (*)())                                dlsym(handle,"ux100_pinEntryMode");

	  __ux100_keybdInit					=(int (*)())                                dlsym(handle,"ux100_keybdInit");
	  __ux100_keybdGetKey				=(int (*)(unsigned int))                                dlsym(handle,"ux100_keybdGetKey");
	  __ux100_dispSetBacklightMode		=(int (*)(int, unsigned int))                                dlsym(handle,"ux100_dispSetBacklightMode");

	  __ux100_infoGetSecureVersion		=(struct ux100SecureVersionStruct (*)())	dlsym(handle, "ux100_infoGetSecureVersion");
	  __ux100_infoGetPairingStatus		=(int (*)())								dlsym(handle, "ux100_infoGetPairingStatus");

      //__ux100_secGetRemovalSwitchChallenge=(struct uxByteBuffer (*)())     dlsym(handle,"ux100_secGetRemovalSwitchChallenge");
      //__ux100_secRemovalSwitchReset       =(int (*)( struct uxByteBuffer)) dlsym(handle,"ux100_secRemovalSwitchReset");
      //__ux100_infoGetRemSwStatus          =(int (*)())                     dlsym(handle,"ux100_infoGetRemSwStatus");
      //__ux100_secPerformParing            =(int (*)(int))                  dlsym(handle,"ux100_secPerformParing");
      //__ux100_infoGetOperationalMode      =(int (*)())                     dlsym(handle,"ux100_infoGetOperationalMode");

      if(!__ux100_pinStartPinEntry || !__ux100_pinDelete || !__ux100_pinAbort || !__ux100_pinSendEncPinBlockToVault
         || !__ux100_pinGetNbCurrDigitEntered || !__ux100_pinEntryMode || !__ux100_keybdInit || !__ux100_keybdGetKey || !__ux100_dispSetBacklightMode
      //   || !__ux100_secGetRemovalSwitchChallenge || !__ux100_secRemovalSwitchReset  || !__ux100_infoGetRemSwStatus
      //   || !__ux100_secPerformParing || !__ux100_infoGetOperationalMode
      )
      {
         __ux100_pinStartPinEntry         =0;
         __ux100_pinDelete                =0;
         __ux100_pinAbort                 =0;
         __ux100_pinSendEncPinBlockToVault=0;
         __ux100_pinGetNbCurrDigitEntered =0;
         __ux100_pinEntryMode             =0;
		 __ux100_keybdInit					=0;
		 __ux100_keybdGetKey				=0;
		 __ux100_dispSetBacklightMode		=0;
         //__ux100_secGetRemovalSwitchChallenge=0;
         //__ux100_secRemovalSwitchReset       =0;
         //__ux100_infoGetRemSwStatus          =0;
         //__ux100_secPerformParing            =0;
         //__ux100_infoGetOperationalMode      =0;
         dlclose(handle);
		 g_CardAppConfig.SetIsUX100(false);
      }
	  else
	  {
	  		ux100SecureVersionStruct secVersion;
	  		dlog_msg("DETECTED UX300/UX100 device");
			EMVlog("DETECTED UX300/UX100 device");
			if(__ux100_infoGetSecureVersion != NULL)
			{
				secVersion = __ux100_infoGetSecureVersion();
				char msg[100];
				sprintf(msg, "id:%s, family:%s, ver:%s ", secVersion.id_mode, secVersion.family, secVersion.version);
				EMVlog(msg);
				int pairing_status = __ux100_infoGetPairingStatus();
				sprintf(msg, "Pairing status: %d", pairing_status);
				EMVlog(msg);
			}
			g_CardAppConfig.SetIsUX100(true);
	  }
   	}
}


int getPromptLocal(int prompt_id, char *prompt)
{
	if(prompt != NULL)
	{
		prompt[0] = 0;
		switch(prompt_id)
		{
			case CARD_APP_GUI_ENTER_PIN:
				strcpy(prompt, g_CardAppConfig.GetEnterPINMsg().c_str());
				break;
			case CARD_APP_GUI_INCORRECT_PIN:
				strcpy(prompt, g_CardAppConfig.GetIncorrectPINMsg().c_str());
				break;
			case CARD_APP_GUI_LAST_PIN:
				strcpy(prompt, g_CardAppConfig.GetLastPINMsg().c_str());
				break;
		}
	}
	return strlen(prompt);

}

#endif

#define EMV_KERNEL_LIB	"/home/usr1/lib/libEMV_CT_Kernel.so"

bool openEMVlib()
{
	if (EMV_lib_handle) dlclose(EMV_lib_handle);

	EMV_lib_handle = dlopen(EMV_KERNEL_LIB, RTLD_LAZY);

	if(EMV_lib_handle)
    {
        EMV_local_WriteTagData =(unsigned char (*)(unsigned char *,unsigned char, unsigned short, unsigned long))       dlsym(EMV_lib_handle,"EMVCTK_WriteTagData");
		if(EMV_local_WriteTagData != NULL)
		{
			g_logger.Log("EMV kernel functions registered");
		}
    }
	else
	{
		g_logger.Log("Cannot open %s", EMV_KERNEL_LIB);
	}

	return(EMV_local_WriteTagData != NULL);
}

#define CVM_PIN_START	0x01
#define CVM_PIN_END		0x05	
#define CVM_AMOUNT_FIELDS	8

bool isPINinCVM(unsigned char *cvm, int cvm_size)
{
	unsigned char *current = cvm;

	current+=CVM_AMOUNT_FIELDS;
	while((current - cvm) < cvm_size)
	{
		if(((current[0] & 0x3F) >= CVM_PIN_START) && ((current[0] & 0x3F) <= CVM_PIN_END))
		{
			return true;
		}
		current+=2;
	}
	return false;
}



std::string ADK_err(EMV_ADK_INFO err)
{
	std::string v_err;

	switch(err)
	{
		case EMV_ADK_OK:
			v_err = "EMV_ADK_OK";
			break;
	    case EMV_ADK_APP_REQ_START:
			v_err = "EMV_ADK_APP_REQ_START";
			break;
		case EMV_ADK_APP_REQ_CANDIDATE:
			v_err = "EMV_ADK_APP_REQ_CANDIDATE - Application requested return application selection";
			break;
		case EMV_ADK_APP_REQ_READREC:
			v_err = "EMV_ADK_APP_REQ_READREC - Application requested return read records";
			break;
		case EMV_ADK_APP_REQ_DATAAUTH:
			v_err = "EMV_ADK_APP_REQ_DATAAUTH - Application requested return data authentication";
			break;
		case EMV_ADK_APP_REQ_ONL_PIN:
			v_err = "EMV_ADK_APP_REQ_ONL_PIN - Application requested return for online PIN entry";
			break;
		case EMV_ADK_APP_REQ_OFL_PIN:
			v_err = "EMV_ADK_APP_REQ_OFL_PIN - Application requested return for offline PIN entry";
			break;
		case EMV_ADK_APP_REQ_PLAIN_PIN:
			v_err = "EMV_ADK_APP_REQ_PLAIN_PIN - Application requested return for plaintext PIN entry";
			break;
		case EMV_ADK_APP_REQ_CVM_END:
			v_err = "EMV_ADK_APP_REQ_CVM_END - Application requested return cardholder verification";
			break;
		case EMV_ADK_APP_REQ_RISK_MAN:
			v_err = "EMV_ADK_APP_REQ_RISK_MAN - Application requested return riskmanagement";
			break;
		case EMV_ADK_APP_REQ_END:
			v_err = "EMV_ADK_APP_REQ_END - Application requested return end of reserved codes";
			break;
		case EMV_ADK_CTLS_RETAP_SAME:
			v_err = "EMV_ADK_CTLS_RETAP_SAME - start over with re-tapping the same card (deactivation/activation/start txn)";
			break;
		case EMV_ADK_CTLS_DOMESTIC_APP:
			v_err = "EMV_ADK_CTLS_DOMESTIC_APP - Domestic ApplicationKernel selected";
			break;
		case EMV_ADK_NO_CARD:
			v_err = "EMV_ADK_NO_CARD - No card processing so far";
			break;
		case EMV_ADK_NOAPP:
			v_err = "EMV_ADK_NOAPP - No common application card / terminal";
			break;
		case EMV_ADK_NO_EXEC:
			v_err = "EMV_ADK_NO_EXEC - not executable";
			break;
		case EMV_ADK_ARQC:
			v_err = "EMV_ADK_ARQC - Transaction must be performed online";
			break;
		case EMV_ADK_TC:
			v_err = "EMV_ADK_TC - Transaction performed (offline / online)";
			break;
		case EMV_ADK_AAC:
			v_err = "EMV_ADK_AAC - Transaction cancellation";
			break;
		case EMV_ADK_AAR:
			v_err = "EMV_ADK_AAR - Transaction cancellation (former card referral)";
			break;
		case EMV_ADK_PARAM:
			v_err = "EMV_ADK_PARAM - Parameter error";
			g_logger.Log("!!! CRITICAL ERROR - system needs reboot !!!");
			break;
		case EMV_ADK_CARDERR:
			v_err = "EMV_ADK_CARDERR - Proprietary card error (actually converted to #EMV_ADK_FALLBACK)";
			break;
		case EMV_ADK_BADAPP:
			v_err = "EMV_ADK_BADAPP - Repeat EMV_Select function call-up because selected application on the proprietary card is erroneous. The currently selected application must be transferred (not NO_APPLI) -> this leads to new application selection but the candidate list is not reconstructed.";
			break;
		case EMV_ADK_CVM:
			v_err = "EMV_ADK_CVM - User abort during PIN input (cancel button, timeout, card removed)";
			break;
		case EMV_ADK_ABORT:
			v_err = "EMV_ADK_ABORT - Current transaction must be aborted";
			break;
		case EMV_ADK_CARD_BLOCKED:
			v_err = "EMV_ADK_CARD_BLOCKED - Blocked card, regional dependent if fallback to magstripe should be done";
			break;
		case EMV_ADK_CARDERR_FORMAT:
			v_err = "EMV_ADK_CARDERR_FORMAT - Card error e.g. erroneous TLV coding, incorrect data -> no transaction cancellation";
			break;
		case EMV_ADK_INTERNAL:
			v_err = "EMV_ADK_INTERNAL - E.g. erroneous communication to PINPad, internal error";
			break;
		case EMV_ADK_ONLINE_PIN_RETRY:
			v_err = "EMV_ADK_ONLINE_PIN_RETRY - Online PIN reentered (PIN check). @n EMV_CT_ContinueOnline() will return this value in the following scenario: Online PIN was entered. Host returned <PIN wrong>. @n Calling application must contact host again. And afterwards call EMV_CT_ContinueOnline() once again.";
			break;
		case EMV_ADK_SAVE_ERROR:
			v_err = "EMV_ADK__SAVE_ERROR - Required TLV data object is missing. (has not been entered by the application) -> Implementation error, thus no process control information (only used by the config functions)";
			break;
		case EMV_ADK_APP_BLOCKED:
			v_err = "EMV_ADK_APP_BLOCKED - Application blocked: AID evaluation at fallback, no global fallback because it is not allowed for this special blocked application";
			break;
		case EMV_ADK_READ_ERROR:
			v_err = "EMV_ADK_READ_ERROR - Error while reading EMV configuration";
			break;
		case EMV_ADK_ERR_HSM:
			v_err = "EMV_ADK_ERR_HSM - E.g. erroneous communication to PINPad, inter-nal error";
			break;
		case EMV_ADK_TLV_BUILD_ERR:
			v_err = "EMV_ADK_TLV_BUILD_ERR - Error in TLV data object = internal error";
			break;
		case EMV_ADK_FALLBACK:
			v_err = "EMV_ADK_FALLBACK - Fallback to magstripe must be done";
			break;
		case EMV_ADK_ONL_PIN_REPEAT:
			v_err = "EMV_ADK_ONL_PIN_REPEAT - wrong Online-PIN, repeat transaction (reduced) beginning with EMV_CT_StartTransaction()";
			break;
		case EMV_ADK_MAND_ELEM_MISSING:
			v_err = "EMV_ADK_MAND_ELEM_MISSING - mandatory (TLV) element missing (EMV-configuration)";
			break;
		case EMV_ADK_INVALID_TERM_CAP:
			v_err = "EMV_ADK_INVALID_TERM_CAP - invalid terminal capabilities (EMV-configuration)";
			break;
		case EMV_ADK_REFERRAL:
			v_err = "EMV_ADK_REFERRAL - a referral of the application decides if the TRX is approved or not";
			break;
		case EMV_ADK_2_CTLS_CARDS:
			v_err = "EMV_ADK_2_CTLS_CARDS -  2 CL cards in the field detected";
			break;
		case EMV_ADK_TXN_CTLS_L1_ERR:
			v_err = "EMV_ADK_TXN_CTLS_L1_ERR - CL level 1 problem, retap necessary";
			break;
		case EMV_ADK_TXN_CTLS_MOBILE:
			v_err = "EMV_ADK_TXN_CTLS_MOBILE - CL mobile device, retap necessary";
			break;
		case EMV_ADK_TXN_CTLS_EMPTY_LIST:
			v_err = "EMV_ADK_TXN_CTLS_EMPTY_LIST - CL empty candidate list allows the fallback with <please use other interface or tap a different card> --> unsupported card";
			break;
		case EMV_ADK_TXN_CTLS_EMV_USE_OTHER_CARD:
			v_err = "EMV_ADK_TXN_CTLS_EMV_USE_OTHER_CARD - use another card";
			break;
		case EMV_ADK_CTLS_DOMESTIC_ONLY_NOT_READABLE:
			v_err = "EMV_ADK_CTLS_DOMESTIC_ONLY_NOT_READABLE - CL domestic not readable (no AID and EMV not configured)";
			break;
		case EMV_ADK_CONTINUE:
			v_err = "EMV_ADK_CONTINUE - Transaction can be continued (internal processing)";
			break;


	}

	return v_err;


}



void printConfig(char *configFilename)
{

	char section[40], key[40], value[256];
	int s, k;

	dlog_msg("Procesing config from file: %s", configFilename);

	for (s = 0; ini_getsection(s, section, sizeof section, configFilename) > 0; s++)
	{
		int i=0;
		while (section[i])
  		{
    		section[i] = toupper(section[i]);
    		i++;
  		}
		dlog_msg("[%s]", section);
		for (k = 0; ini_getkey(section, k, key, sizeof key, configFilename) > 0; k++)
		{
			if(ini_gets(section, key, "0", value, sizeof(value), configFilename) > 0)
			{
				dlog_msg("[%s]:%s=%s", section, key, value);
			}
		}
	}
}
cUpdateCfg::cUpdateCfg()
{

}



int cUpdateCfg::commitChanges(void)
{
	int iRet = 0;
	std::string AID;
	EMV_CT_APPLI_TYPE pxAID;
	EMV_ADK_INFO err;
	bool kernelOpened = g_CEMVtrns.Is_Kernel_Opened();
	long long t_start_l = 0;
	long long t_end_l = 0;


	if (!kernelOpened)
	{
		if ((iRet = g_CEMVtrns.Open_Kernel()) != EMV_ADK_OK)
		{
			dlog_error("Cannot open the kernel!");
			g_CEMVtrns.Close_Kernel();
			return CARD_APP_RET_ICC_FAIL;
		}
	}

	AID = g_CardAppConfig.GetCurrentAID();
	dlog_msg("Updating config for AID: %s", AID.c_str());
	if(AID.size() > 0)
	{
		pxAID.aidlen = com_verifone_pml::svcDsp2Hex(AID.data(), AID.size(), reinterpret_cast<char *>(pxAID.AID), sizeof(pxAID.AID));
		dlog_msg("CAPK index: %X, CAPK exp: %X", g_EMVcollection.capkData.Index, g_EMVcollection.capkData.Exponent);
		if (g_EMVcollection.capkData.Index != 0)
		{
			if (g_EMVcollection.capkData.RID == 0)
			{
				// Set RID manually
				g_EMVcollection.capkData.RID = new unsigned char[5];
				memcpy(g_EMVcollection.capkData.RID, pxAID.AID, 5);
			}
			EMVlog("EMV_CT_StoreCAPKey - update config");
			dlog_msg("EMV_CT_StoreCAPKey - update config");
			//t_start_l = getTimeStampMs();
			err = EMV_CT_StoreCAPKey(EMV_ADK_SET_ONE_RECORD, &g_EMVcollection.capkData);
			//t_end_l = getTimeStampMs();
			//g_logger.Log("EMV_CT_StoreCAPKey(%s) execution end: %08.08ldms", AID.c_str(), t_end_l - t_start_l);
			EMVlog((char *)ADK_err(err).c_str());
			dlog_msg((char *)ADK_err(err).c_str());
		}
		else if(AID.size() >= 10 && AID.size() <= 32)		//only full AID's
		{
			EMVlog("EMV_CT_SetAppliData - update config");
			dlog_msg("EMV_CT_SetAppliData - update config");
			//t_start_l = getTimeStampMs();
			err = EMV_CT_SetAppliData(EMV_ADK_SET_ONE_RECORD, &pxAID, &g_EMVcollection.appData);
			//t_end_l = getTimeStampMs();
			//g_logger.Log("EMV_CT_SetAppliData(%s) execution end: %08.08ldms", AID.c_str(), t_end_l - t_start_l);
			EMVlog((char *)ADK_err(err).c_str());
			dlog_msg((char *)ADK_err(err).c_str());
		}
	}
	else
	{
		EMVlog("EMV_CT_SetTermData - update config");
		dlog_msg("EMV_CT_SetTermData - update config");
		//t_start_l = getTimeStampMs();
		err = EMV_CT_SetTermData(&g_EMVcollection.termData);
		//t_end_l = getTimeStampMs();
		//g_logger.Log("EMV_CT_SetTermData() execution end: %08.08ldms", t_end_l - t_start_l);
		EMVlog((char *)ADK_err(err).c_str());
		dlog_msg((char *)ADK_err(err).c_str());
	}

	if (!kernelOpened) g_CEMVtrns.Close_Kernel();

	return iRet;

}

int cUpdateCfg::locateConfigRecords(const char *AID)
{

	EMV_CT_APPLI_TYPE pxAID;
	AID_index = 0;
	std::string AID_s = g_CardAppConfig.GetCurrentAID();
	EMV_ADK_INFO ADK_ret;
	int iRet = 0;

	bool kernelOpened = g_CEMVtrns.Is_Kernel_Opened();

	if (!kernelOpened)
	{
		if ((iRet = g_CEMVtrns.Open_Kernel()) != EMV_ADK_OK)
		{
			dlog_error("Cannot open the kernel!");
			g_CEMVtrns.Close_Kernel();
			return CARD_APP_RET_ICC_FAIL;
		}
	}

	EMVlog("updating EMV configuration context EMV_CT_GetTermData");
	ADK_ret = EMV_CT_GetTermData(&g_EMVcollection.termData);
	EMVlog((char *)ADK_err(ADK_ret).c_str());
	if(AID_s.size() > 0)
	{
		EMVlog((char *)AID_s.c_str());
		pxAID.aidlen = com_verifone_pml::svcDsp2Hex(AID_s.data(), AID_s.size(), reinterpret_cast<char *>(pxAID.AID), sizeof(pxAID.AID));
		EMVlog("updating EMV configuration context EMV_CT_GetAppliData");
		ADK_ret = EMV_CT_GetAppliData(EMV_ADK_READ_AID, &pxAID, &g_EMVcollection.appData);
		EMVlog((char *)ADK_err(ADK_ret).c_str());


		//review missing critical app data
		if((g_EMVcollection.appData.App_TermCap[0]+g_EMVcollection.appData.App_TermCap[1]+g_EMVcollection.appData.App_TermCap[2]) == 0)
		{
			memcpy(g_EMVcollection.appData.App_TermCap, g_EMVcollection.termData.TermCap, 3);
		}
		if((g_EMVcollection.appData.App_TermAddCap[0]+g_EMVcollection.appData.App_TermAddCap[1]+g_EMVcollection.appData.App_TermAddCap[2]+g_EMVcollection.appData.App_TermAddCap[3]+g_EMVcollection.appData.App_TermAddCap[4]) == 0)
		{
			memcpy(g_EMVcollection.appData.App_TermAddCap, g_EMVcollection.termData.TermAddCap, 3);
		}
		if(g_EMVcollection.appData.App_TermTyp == 0)
		{
			g_EMVcollection.appData.App_TermTyp = g_EMVcollection.termData.TermTyp;
		}
		if((g_EMVcollection.appData.App_CountryCodeTerm[0]+g_EMVcollection.appData.App_CountryCodeTerm[1]) == 0)
		{
			memcpy(g_EMVcollection.appData.App_CountryCodeTerm, g_EMVcollection.termData.TermCountryCode, 2);
		}


		g_EMVcollection.appData.Info_Included_Data[0] = 0xFF;
		g_EMVcollection.appData.Info_Included_Data[1] = 0xFF;
		g_EMVcollection.appData.Info_Included_Data[2] = 0xFF;
		g_EMVcollection.appData.Info_Included_Data[3] = 0xFF;
		g_EMVcollection.appData.Info_Included_Data[4] = 0x07;
		g_EMVcollection.appData.Info_Included_Data[5] = 0x00;
		g_EMVcollection.appData.Info_Included_Data[6] = 0x00;
		g_EMVcollection.appData.Info_Included_Data[7] = 0x00;
		g_EMVcollection.appData.App_FlowCap[0] = FORCE_RISK_MANAGEMENT | BLACKLIST /*| PIN_BYPASS | FORCE_ONLINE */;  //iq_audi_60317
		g_EMVcollection.appData.App_FlowCap[1] = /*CASH_SUPPORT | CASHBACK_SUPPORT |*/ EMV_CT_CHECK_INCONS_TRACK2_PAN;	//iq_audi_60317
	}

	if (!kernelOpened) g_CEMVtrns.Close_Kernel();

	return AID_index;
}

com_verifone_TLVLite::ConstData cUpdateCfg::serializeTag(const unsigned long tag)
{
	using namespace com_verifone_TLVLite;
	ConstData value(ConstData::getInvalid());
	if (tag > 0xFFFF && tag < 0x100000000)
	{
		// Special stuff, we have to serialize three bytes only
		char tbuf[3];
		SafeBuffer buf(tbuf, sizeof tbuf);
		buf.append(static_cast<unsigned char>( (tag & 0xFF0000) >> 16));
		buf.append(static_cast<unsigned char>( (tag & 0xFF00) >> 8));
		buf.append(static_cast<unsigned char>( (tag & 0xFF)));
		value.clone(ConstData(buf.getBuffer(), buf.getLength()));
	}
	else
	{
		if (tag <= 0xFF) value.clone(MakeBERValue(static_cast<unsigned char>(tag)));
		else if (tag <= 0xFFFF) value.clone(MakeBERValue(static_cast<unsigned short>(tag)));
		else value.clone(MakeBERValue(tag));
	}
	// dlog_hex(value.getBuffer(), value.getSize(), "SERIALIZED TAG");
	return value;
}

int cUpdateCfg::serializeAIDData(std::string & conf, const EMV_CT_APPLIDATA_TYPE * pAIDData)
{
	using namespace com_verifone_TLVLite;
	// Serialize pAid
	assert(pAIDData);
	char buffer[1024];
	SafeBuffer buf(buffer, sizeof buffer);

	if (pAIDData->Info_Included_Data[0] & EMV_CT_INPUT_APL_VERSION)
	{
		buf.append(TagLenVal(serializeTag(TAG_9F09_TRM_APP_VERSION_NB),
				ConstData(pAIDData->VerNum, sizeof pAIDData->VerNum)));
	}
	if (pAIDData->Info_Included_Data[0] & EMV_CT_INPUT_APL_ASI)
	{
		buf.append(TagLenVal(serializeTag(TAG_DFAF11_PARTIAL_SELECTION),
				MakeBERValue(pAIDData->ASI)));
	}
	if (pAIDData->Info_Included_Data[0] & EMV_CT_INPUT_APL_NAME)
	{
		buf.append(TagLenVal(serializeTag(TAG_DFAF13_REC_APP_NAME),
				ConstData(pAIDData->AppName, sizeof pAIDData->AppName)));
	}
	if (pAIDData->Info_Included_Data[0] & EMV_CT_INPUT_APL_MERCHANT_CATCODE)
	{
		buf.append(TagLenVal(serializeTag(TAG_9F15_MERCH_CATEG_CODE),
				ConstData(pAIDData->BrKey, sizeof pAIDData->BrKey)));
	}
	if (pAIDData->Info_Included_Data[0] & EMV_CT_INPUT_APL_TID)
	{
		buf.append(TagLenVal(serializeTag(TAG_9F1C_TRM_ID),
				ConstData(pAIDData->TermIdent, sizeof pAIDData->TermIdent)));
	}
	if (pAIDData->Info_Included_Data[0] & EMV_CT_INPUT_APL_FLOOR_LIMIT)
	{
		buf.append(TagLenVal(serializeTag(TAG_9F1B_TRM_FLOOR_LIMIT),
				ConstData(pAIDData->FloorLimit, sizeof pAIDData->FloorLimit)));
	}
	if (pAIDData->Info_Included_Data[0] & EMV_CT_INPUT_APL_THRESH)
	{
		buf.append(TagLenVal(serializeTag(TAG_DFAF14_RND_SEL_THRESHOLD),
				ConstData(pAIDData->Threshhold, sizeof pAIDData->Threshhold)));
	}
	if (pAIDData->Info_Included_Data[0] & EMV_CT_INPUT_APL_TARGET)
	{
		buf.append(TagLenVal(serializeTag(TAG_DFAF15_RND_SEL_PERCENTAGE),
				MakeBERValue(pAIDData->TargetPercentage)));
	}
	if (pAIDData->Info_Included_Data[1] & EMV_CT_INPUT_APL_MAXTARGET)
	{
		buf.append(TagLenVal(serializeTag(TAG_DFAF16_RND_MAX_RND_PERCENTAGE),
				MakeBERValue(pAIDData->MaxTargetPercentage)));
	}
	if (pAIDData->Info_Included_Data[1] & EMV_CT_INPUT_APL_TAC_DEFAULT)
	{
		buf.append(TagLenVal(serializeTag(TAG_DFAF17_TAC_DEFAULT),
				ConstData(pAIDData->TACDefault, sizeof pAIDData->TACDefault)));
	}
	if (pAIDData->Info_Included_Data[1] & EMV_CT_INPUT_APL_TAC_DENIAL)
	{
		buf.append(TagLenVal(serializeTag(TAG_DFAF18_TAC_DENIAL),
				ConstData(pAIDData->TACDenial, sizeof pAIDData->TACDenial)));
	}
	if (pAIDData->Info_Included_Data[1] & EMV_CT_INPUT_APL_TAC_ONLINE)
	{
		buf.append(TagLenVal(serializeTag(TAG_DFAF19_TAC_ONLINE),
				ConstData(pAIDData->TACOnline, sizeof pAIDData->TACOnline)));
	}
	if (pAIDData->Info_Included_Data[1] & EMV_CT_INPUT_APL_TDOL)
	{
		buf.append(TagLenVal(serializeTag(TAG_DFAF1A_DEFAULT_TDOL),
				ConstData(pAIDData->Default_TDOL.DOL, pAIDData->Default_TDOL.dollen)));
	}
	if (pAIDData->Info_Included_Data[1] & EMV_CT_INPUT_APL_DDOL)
	{
		buf.append(TagLenVal(serializeTag(TAG_DFAF1B_DEFAULT_DDOL),
				ConstData(pAIDData->Default_DDOL.DOL, pAIDData->Default_DDOL.dollen)));
	}
	if (pAIDData->Info_Included_Data[2] & EMV_CT_INPUT_APL_TERM_CAPS)
	{
		buf.append(TagLenVal(serializeTag(TAG_9F33_TRM_CAPABILITIES),
				ConstData(pAIDData->App_TermCap, sizeof pAIDData->App_TermCap)));
	}
	if (pAIDData->Info_Included_Data[2] & EMV_CT_INPUT_APL_COUNTRY_CODE)
	{
		buf.append(TagLenVal(serializeTag(TAG_9F1A_TRM_COUNTRY_CODE),
				ConstData(pAIDData->App_CountryCodeTerm, sizeof pAIDData->App_CountryCodeTerm)));
	}
	if (pAIDData->Info_Included_Data[2] & EMV_CT_INPUT_APL_ADD_TERM_CAPS)
	{
		buf.append(TagLenVal(serializeTag(TAG_9F40_ADD_TRM_CAP),
				ConstData(pAIDData->App_TermAddCap, sizeof pAIDData->App_TermAddCap)));
	}
	if (pAIDData->Info_Included_Data[3] & EMV_CT_INPUT_APL_TRM_TYPE)
	{
		buf.append(TagLenVal(serializeTag(TAG_9F35_TRM_TYPE),
				MakeBERValue(pAIDData->App_TermTyp)));
	}
	if (buf.getLength())
	{
		conf.append(buffer, buf.getLength());
		return buf.getLength();
	}
	return 0;
}

int cUpdateCfg::serializeCAPKData(std::string & conf, const EMV_CT_CAPREAD_TYPE * pCAPKData)
{
	using namespace com_verifone_TLVLite;
	// Not much to do here...
	char buffer[512];
	SafeBuffer buf(buffer, sizeof buffer);
	ConstData tagData(ConstData::getInvalid());

	assert(pCAPKData);
	buf.append(TagLenVal(serializeTag(TAG_8F_CERTIF_AUTH_PK_ID), MakeBERValue(pCAPKData->Index)));

	char ridASCII[20];
	memset(ridASCII, 0, sizeof(ridASCII));
	com_verifone_pml::svcHex2Dsp(reinterpret_cast<const char *>(pCAPKData->RID), sizeof(pCAPKData->RID),
			ridASCII, sizeof(ridASCII)-1);
	char ridID[4];
	memset(ridID, 0, sizeof ridID);
	com_verifone_pml::svcHex2Dsp(reinterpret_cast<const char *>(&pCAPKData->Index), sizeof(pCAPKData->Index),
			ridID, sizeof(ridID)-1);
	strcat(ridASCII, ".");
	strcat(ridASCII, ridID);
	buf.append(TagLenVal(serializeTag(TAG_DFCC20_CAPK_FILE), ConstData(ridASCII, strlen(ridASCII))));

	// CAPK Expiration is not available, ADK doesn't return it

	if (buf.getLength())
	{
		conf.append(buffer, buf.getLength());
		return buf.getLength();
	}
	return 0;
}

std::string cUpdateCfg::getConfig(const char *AID, short capkIndex)
{
	using namespace com_verifone_TLVLite;
	const size_t EMV_RID_SIZE = 10;
	const size_t EMV_MAX_AID_SIZE = 33; // 16 bytes max AID len + 0-termination
	EMV_ADK_INFO ADK_ret = EMV_ADK_OK;
	EMV_CT_APPLI_TYPE struct_AID;
	EMV_CT_APPLIDATA_TYPE AppliData;
	std::string conf;
	ConstData tagData(ConstData::getInvalid());
	char buffer[2048];

	bool kernelOpened = g_CEMVtrns.Is_Kernel_Opened();
	if (!kernelOpened)
	{
		if ((ADK_ret = g_CEMVtrns.Open_Kernel()) != EMV_ADK_OK)
		{
			dlog_error("Cannot open the kernel!");
			g_CEMVtrns.Close_Kernel();
			return conf;
		}
	}

	if (AID && strlen(AID) > 0)
	{
		if (capkIndex) // return CAPK data only
		{
			unsigned char max = 0;
			ADK_ret = EMV_CT_ReadCAPKeys(NULL, &max);
			if (ADK_ret == EMV_ADK_OK)
			{
				dlog_msg("There are %d CAPKs", max);
				EMV_CT_CAPREAD_TYPE * capkTable = new EMV_CT_CAPREAD_TYPE[max];
				assert(capkTable);
				ADK_ret = EMV_CT_ReadCAPKeys(capkTable, &max);
				if (ADK_ret == EMV_ADK_OK)
				{
					for (size_t i = 0; i < max; ++i)
					{
						char ridASCII[EMV_MAX_AID_SIZE];
						memset(ridASCII, 0, EMV_MAX_AID_SIZE);
						com_verifone_pml::svcHex2Dsp(reinterpret_cast<char *>(capkTable[i].RID), sizeof(capkTable[i].RID),
								ridASCII, EMV_MAX_AID_SIZE-1);
						if (strlen(AID) >= 10 && !memcmp(ridASCII, AID, 10) && capkIndex == capkTable[i].Index)
						{
							dlog_msg("Found CAPK index %d", capkIndex);
							serializeCAPKData(conf, &capkTable[i]);
						}
					}
				}
				delete [] capkTable;
			}
		}
		else // return AID / RID data
		{
			if (strlen(AID) == EMV_RID_SIZE)
			{
				// We need to browse through all the RIDs to find matches and serialize them all...
				dlog_msg("Building config for RID: %s", AID);
				ADK_ret = EMV_CT_GetAppliData(EMV_ADK_READ_FIRST, &struct_AID, &AppliData);
				while (ADK_ret == EMV_ADK_OK)
				{
					char aidASCII[EMV_MAX_AID_SIZE];
					memset(aidASCII, 0, EMV_MAX_AID_SIZE);
					com_verifone_pml::svcHex2Dsp(reinterpret_cast<char *>(struct_AID.AID), struct_AID.aidlen, aidASCII, EMV_MAX_AID_SIZE-1);
					if ((struct_AID.aidlen<<1) >= EMV_RID_SIZE && !memcmp(aidASCII, AID, EMV_RID_SIZE))
					{
						dlog_msg("Found match, AID: %s", aidASCII);
						SafeBuffer buf(buffer, sizeof buffer);
						buf.append(TagLenVal(serializeTag(TAG_9F06_AID),
								ConstData(struct_AID.AID, struct_AID.aidlen)));
						conf.append(buffer, buf.getLength());
						serializeAIDData(conf, &AppliData);
					}
					ADK_ret = EMV_CT_GetAppliData(EMV_ADK_READ_NEXT, &struct_AID, &AppliData);
				}
			}
			else
			{
				dlog_msg("Building config for AID: '%s', len %d", AID, strlen(AID));
				strcpy((char *)struct_AID.AID, AID);
				struct_AID.aidlen = strlen(AID);
				ADK_ret = EMV_CT_GetAppliData(EMV_ADK_READ_AID, &struct_AID, &AppliData);

				if(ADK_ret == EMV_ADK_OK)
				{
					serializeAIDData(conf, &AppliData);
				}
				else
				{
					dlog_alert("No configuration found!");
				}
			}
		} // else - AID only
	}
	else
	{
		// No AID! Get terminal configuration
		dlog_msg("No RID/AID, returning terminal configuration");
		EMV_CT_TERMDATA_TYPE TermData;
		ADK_ret = EMV_CT_GetTermData(&TermData);
		if (ADK_ret == EMV_ADK_OK)
		{
			SafeBuffer buf(buffer, sizeof buffer);
			buf.append(TagLenVal(serializeTag(TAG_9F35_TRM_TYPE), MakeBERValue(TermData.TermTyp)));
			buf.append(TagLenVal(serializeTag(TAG_9F1A_TRM_COUNTRY_CODE),
					ConstData(TermData.TermCountryCode, sizeof TermData.TermCountryCode)));
			buf.append(TagLenVal(serializeTag(TAG_9F33_TRM_CAPABILITIES),
					ConstData(TermData.TermCap, sizeof TermData.TermCap)));
			buf.append(TagLenVal(serializeTag(TAG_9F35_TRM_TYPE),
					ConstData(TermData.TermAddCap, sizeof TermData.TermAddCap)));
			buf.append(TagLenVal(serializeTag(TAG_5F2A_TRANS_CURRENCY),
					ConstData(TermData.CurrencyTrans, sizeof TermData.CurrencyTrans)));
			buf.append(TagLenVal(serializeTag(TAG_5F36_TRANS_CURRENCY_EXP), MakeBERValue(TermData.ExpTrans)));
			conf.assign(buffer, buf.getLength());
		}
	}
	if (!kernelOpened) g_CEMVtrns.Close_Kernel();
	return conf;
}

int cUpdateCfg::deleteAID(const char * AID)
{
	EMV_CT_APPLI_TYPE aidData;
	// EMV_CT_APPLIDATA_TYPE aidParams;
	memset(&aidData, 0, sizeof aidData);
	size_t aidLen = strlen(AID);
	com_verifone_pml::svcDsp2Hex(AID, aidLen, reinterpret_cast<char *>(aidData.AID), sizeof aidData.AID);
	aidData.aidlen = static_cast<unsigned char>(aidLen >> 1);
	EMV_ADK_INFO ADK_ret = EMV_CT_SetAppliData(EMV_ADK_CLEAR_ONE_RECORD, &aidData, NULL);
	dlog_msg("AID '%s' deletion result %d", AID, ADK_ret);
	if (ADK_ret == EMV_ADK_OK) return 0;
	return -1;
}

int cUpdateCfg::deleteConfig(const char *AID)
{
	int iRet = -1;
	if (AID && strlen(AID))
	{
		bool kernelOpened = g_CEMVtrns.Is_Kernel_Opened();
		if (!kernelOpened)
		{
			if (g_CEMVtrns.Open_Kernel() != EMV_ADK_OK)
			{
				dlog_error("Cannot open the kernel!");
				g_CEMVtrns.Close_Kernel();
				return iRet;
			}
		}
		const size_t EMV_RID_SIZE = 10;
		if (strlen(AID) == EMV_RID_SIZE)
		{
			dlog_msg("Deleting RID '%s'", AID);
			std::vector<std::string> aidsToDelete;
			const size_t EMV_MAX_AID_SIZE = 33; // 16 bytes max AID len + 0-termination
			EMV_CT_APPLI_TYPE struct_AID;
			EMV_CT_APPLIDATA_TYPE AppliData;
			EMV_ADK_INFO ADK_ret = EMV_CT_GetAppliData(EMV_ADK_READ_FIRST, &struct_AID, &AppliData);
			while (ADK_ret == EMV_ADK_OK)
			{
				char aidASCII[EMV_MAX_AID_SIZE];
				memset(aidASCII, 0, EMV_MAX_AID_SIZE);
				com_verifone_pml::svcHex2Dsp(reinterpret_cast<char *>(struct_AID.AID), struct_AID.aidlen, aidASCII, EMV_MAX_AID_SIZE-1);
				dlog_msg("Got AID '%s'", aidASCII);
				if ((struct_AID.aidlen<<1) >= EMV_RID_SIZE && !memcmp(aidASCII, AID, EMV_RID_SIZE))
				{
					aidsToDelete.push_back(aidASCII);
					//iRet = deleteAID(aidASCII);
					//if (iRet != 0) break; // Error!
				}
				ADK_ret = EMV_CT_GetAppliData(EMV_ADK_READ_NEXT, &struct_AID, &AppliData);
			}
			dlog_msg("We have %d AIDs to delete", aidsToDelete.size());
			while (!aidsToDelete.empty())
			{
				std::string aid = aidsToDelete.back();
				aidsToDelete.pop_back();
				iRet = deleteAID(aid.c_str());
				if (iRet != 0) break; // Error!
			}
		}
		else
		{
			iRet = deleteAID(AID);
		}
		if (!kernelOpened) g_CEMVtrns.Close_Kernel();
	}
	return iRet;
}

int cUpdateCfg::buildCAPK(void)
{
	int iRet = 0;

	return iRet;
}

int cUpdateCfg::deleteCAPK(const char *AID, short capkIndex)
{
	int iRet = -1;
	if (AID && strlen(AID))
	{
		dlog_msg("Deleting CAPK index %d for AID '%s'", capkIndex, AID);
		EMV_CT_CAPKEY_TYPE capkData;
		memset(&capkData, 0, sizeof capkData);
		unsigned char RID[5];
		com_verifone_pml::svcDsp2Hex(AID, (sizeof RID)<<1, reinterpret_cast<char *>(RID), sizeof RID);
		capkData.RID = RID;
		capkData.Index = static_cast<unsigned char>(capkIndex);
		EMV_ADK_INFO ADK_ret = EMV_CT_StoreCAPKey(EMV_ADK_CLEAR_ONE_RECORD, &capkData);
		dlog_msg("CAPK deletion result %d", ADK_ret);
		if (ADK_ret == EMV_ADK_OK) iRet = 0;
	}

	return iRet;
}

int cUpdateCfg::updateCAPK(short index, char * filename, int position)
{
	int iRet = -1;

	return iRet;

}


int cUpdateCfg::updateCurrentAID(const char *AID, char *ver1, char *ver2)
{
	int iRet = 1;

	return iRet;
}


int cUpdateCfg::updateConfig(int tagNum, unsigned char * value, int Len)
{

	int iRet = 0;

	dlog_msg("updateConfigTLV: tag: %X, len: %d", tagNum, Len);
	g_EMVcollection.SetTLVData(tagNum, value, Len);



	return iRet;

}

int cLEDsHandler::changeState(int leds, unsigned char operation, bool force /*=false*/)
{
    int res = -1;
    if (allowed || force)
    {
        if (operation == LED_ENABLE || operation == LED_DISABLE)
        {
            res = led_showCancel();
            dlog_msg("led_showCancel result %d", res);
            res = led_changeState(leds, operation);
            dlog_msg("led_changeState result %d", res);
        }
        else
        {
            dlog_error("Invalid LED operation %d", operation);
            errno = EINVAL;
        }
    }
    else
    {
        dlog_alert("LEDs are locked!");
        res = 0;
    }
    return res;
}

int cLEDsHandler::blink(int leds, unsigned int duration, unsigned int timeout, bool force /*=false*/)
{
    int res = -1;
    if (allowed || force)
    {
        char state[5];
        state[0] = (leds & LED_SELECT_MSR1) ? '\x01' : '\x00';
        state[1] = (leds & LED_SELECT_MSR2) ? '\x01' : '\x00';
        state[2] = (leds & LED_SELECT_MSR3) ? '\x01' : '\x00';
        //sprintf(state, "%1x%1x%1x",
        //    ( (leds & LED_SELECT_MSR1) ? 1 : 0 ),
        //    ( (leds & LED_SELECT_MSR2) ? 1 : 0 ),
        //    ( (leds & LED_SELECT_MSR3) ? 1 : 0 )
        //);
        dlog_msg("LED duration %d, timeout %d", duration, timeout);
        dlog_hex(state, 3, "LED states");
        res = led_showCancel();
        dlog_msg("led_showCancel result %d", res);
        res = led_showInit();
        dlog_msg("led_showInit res %d", res);
        if (res == 0)
        {
            res = led_showAddState(reinterpret_cast<unsigned char *>(state), duration);
            dlog_msg("led_showAddState res %d", res);
        }
        if (res == 0)
        {
            char leds_empty[] = "\x00\x00\x00";
            res = led_showAddState(reinterpret_cast<unsigned char *>(leds_empty), duration);
            dlog_msg("led_showAddState res %d", res);
        }
        if (res == 0) res = led_show(0);
        dlog_msg("led_show result %d", res);
    }
    return res;
}



cEMVcollection::cEMVcollection()
{
	ClearEverything();
	trx_cnt = 0;
}

void cEMVcollection::ClearEverything()
{
	memset(&transData, 0, sizeof(transData));
	memset(&cardData, 0, sizeof(cardData));
	memset(&appData, 0, sizeof(appData));
	memset(&payData, 0, sizeof(payData));
	memset(&hostData, 0, sizeof(hostData));
	memset(&selectData, 0, sizeof(selectData));
	memset(&inData, 0, sizeof(inData));
	memset(&termData, 0, sizeof(termData));
	memset(&capkData, 0, sizeof(capkData));
	memset(&extraData, 0, sizeof(extraData));
	memset(t_91_authData, 0, sizeof(t_91_authData));
	memset(t_Info_Included_Data, 0, sizeof(t_Info_Included_Data));
	AppliDataAddLen = 0;
	tags_collection.clear();
	
}

void cEMVcollection::Clear()
{
	// Kamil_P1: The below is commented out to keep processing consistent (clearing may change the result of config load...)
	//memset(&appData, 0, sizeof(appData));
	//memset(&termData, 0, sizeof(termData));
	if (capkData.RID) delete [] capkData.RID;
	if (capkData.Key) delete [] capkData.Key;
	if (capkData.Hash) delete [] capkData.Hash;
	memset(&capkData, 0, sizeof(capkData));
	memset(appData.Info_Included_Data, 0, sizeof(appData.Info_Included_Data)); // clear flags here
	memset(termData.Info_Included_Data, 0, sizeof(termData.Info_Included_Data)); // also clear flags
	AppliDataAddLen = 0;
}

int cEMVcollection::tagToBuffer(unsigned short tag, const unsigned char * value, size_t Len, unsigned char * tagVal, size_t tagValSize)
{
	using namespace com_verifone_TLVLite;
	SafeBuffer buf(tagVal, tagValSize);
	ConstData tagData(ConstData::getInvalid());
	tagData.clone(MakeBERValue(tag));
	buf.append(TagLenVal(tagData, ConstData(value, Len)));
	if (buf.isOverflow()) return 0;
	return buf.getLength();
}

int cEMVcollection::GetTLVData(int tagNum, unsigned char *value, int *Len)
{
	long amt = 0;
	*Len = 0;
	switch(tagNum)
	{
		case TAG_4F_APP_ID:
		case 0x4F00:
			if (selectData.T_DF61_Info_Received_Data[1] & DF61_SEL_AID)
			{
				memcpy(value, selectData.T_DF04_Aidselected.AID, selectData.T_DF04_Aidselected.aidlen);
				*Len = selectData.T_DF04_Aidselected.aidlen;
			}
			break;
		case TAG_50_APP_LABEL:
		case 0x5000:
			if (selectData.T_DF61_Info_Received_Data[0] & DF61_SEL_50_APPLICATION_NAME)
			{
				strcpy((char *)value, (const char *)selectData.T_50_ApplicationName);
				*Len = strlen((const char*)selectData.T_50_ApplicationName);
			}
			break;
		case TAG_57_TRACK2_EQUIVALENT:
		case 0x5700:
			if (transData.T_DF61_Info_Received_Data[1] & TRX_57_DATA_TRACK2)
			{
				memcpy(value, transData.T_57_DataTrack2.tr2data, transData.T_57_DataTrack2.tr2len);
				*Len = transData.T_57_DataTrack2.tr2len;
			}
			break;
		case TAG_5A_APP_PAN:
		case 0x5A00:
			if (transData.T_DF61_Info_Received_Data[0] & TRX_5A_PAN)
			{
				memcpy(value, transData.T_5A_PAN, sizeof(transData.T_5A_PAN));
				*Len = sizeof(transData.T_5A_PAN);
				while(value[(*Len)-1] == 0xFF) (*Len)--;
			}
			break;
		case TAG_5F20_CARDHOLDER_NAME:
			if (transData.T_DF61_Info_Received_Data[5] & TRX_5F20_CARDHOLDER)
			{
				memcpy(value, transData.T_5F20_Cardholder.crdName, transData.T_5F20_Cardholder.crdNameLen);
				*Len = transData.T_5F20_Cardholder.crdNameLen;
			}
			break;
		case TAG_5F24_APP_EXP_DATE:
			if (transData.T_DF61_Info_Received_Data[0] & TRX_5F24_APPEXPDATE)
			{
				memcpy(value, transData.T_5F24_AppExpDate, sizeof(transData.T_5F24_AppExpDate));
				*Len = sizeof(transData.T_5F24_AppExpDate);
			}
			break;
		case TAG_5F25_APP_EFF_DATE:
			if (transData.T_DF61_Info_Received_Data[3] & TRX_5F25_APPEFFDATE)
			{
				memcpy(value, transData.T_5F25_AppEffDate, sizeof(transData.T_5F25_AppEffDate));
				*Len = sizeof(transData.T_5F25_AppEffDate);
			}
			break;
		case TAG_5F28_ISS_COUNTRY_CODE:
			if (transData.T_DF61_Info_Received_Data[3] & TRX_5F28_ISSCOUNTRYCODE)
			{
				memcpy(value, transData.T_5F28_IssCountryCode, sizeof(transData.T_5F28_IssCountryCode));
				*Len = sizeof(transData.T_5F28_IssCountryCode);
			}
			break;
		case TAG_5F2A_TRANS_CURRENCY:
			if (transData.T_DF61_Info_Received_Data[2] & TRX_5F2A_TRX_CURRENCY)
			{
				memcpy(value, transData.T_5F2A_CurrencyTrans, sizeof(transData.T_5F2A_CurrencyTrans));
				*Len = sizeof(transData.T_5F2A_CurrencyTrans);
			}
			break;
		case TAG_5F2D_LANGUAGE:
			if (transData.T_DF61_Info_Received_Data[5] & TRX_5F2D_LANG_PREFERENCE)
			{
				memcpy(value, transData.T_5F2D_Lang_Pref, sizeof(transData.T_5F2D_Lang_Pref));
				*Len = sizeof(transData.T_5F2D_Lang_Pref);
			}
			break;
		case TAG_5F30_SERVICE_CODE:
			if (transData.T_DF61_Info_Received_Data[6] & TRX_5F30_SERVICE_CODE)
			{
				memcpy(value, transData.T_5F30_ServiceCode, sizeof(transData.T_5F30_ServiceCode));
				*Len = sizeof(transData.T_5F30_ServiceCode);
			}
			break;
		case TAG_5F34_PAN_SEQUENCE_NB:
			if (transData.T_DF61_Info_Received_Data[1] & TRX_5F34_PAN_SEQ_NUMBER)
			{
				memcpy(value, transData.T_5F34_PANSequenceNo, sizeof(transData.T_5F34_PANSequenceNo));
				*Len = sizeof(transData.T_5F34_PANSequenceNo);
			}
			break;
		case TAG_5F36_TRANS_CURRENCY_EXP:
			if (transData.T_DF61_Info_Received_Data[5] & TRX_5F36_TRX_CURRENCY_EXPO)
			{
				value[0] = transData.T_5F36_Trx_Currency_Exp;
				*Len = 1;
			}
			break;
		case TAG_5F57_ACCOUNT_TYPE:
			if (t_Info_Included_Data[0] & INPUT_OFL_ACCOUNT_TYPE)
			{
				value[0] = payData.uc_AccountType;
				*Len = 1;
			}
			break;
		case TAG_81_BIN_AMOUNT_AUTH:
		case 0x8100:
			if (t_Info_Included_Data[1] & INPUT_OFL_AMOUNT)
			{
				amt = atol((const char *)payData.Amount);
				memcpy(value, (void *)&amt, sizeof(long));
				*Len = sizeof(long);
			}
			break;
		case TAG_82_AIP:
		case 0x8200:
			if (transData.T_DF61_Info_Received_Data[2] & TRX_82_AIP)
			{
				memcpy(value, transData.T_82_AIP, sizeof(transData.T_82_AIP));
				*Len = sizeof(transData.T_82_AIP);
			}
			break;
		/*
		case TAG_87_APP_PRIORITY_ID:
		case 0x8700:
			*Len = 0;
			break;
		*/
		case TAG_89_AUTH_CODE:
		case 0x8900:
			if (hostData.Info_Included_Data[0] & INPUT_ONL_AUTHCODE)
			{
				memcpy(value, hostData.AuthorizationCode, sizeof(hostData.AuthorizationCode));
				*Len = sizeof(hostData.AuthorizationCode);
			}
			break;
		case TAG_8A_AUTH_RESP_CODE:
		case 0x8A00:
			if (hostData.Info_Included_Data[0] & INPUT_ONL_ONLINE_AC)
			{
				memcpy(value, hostData.AuthResp, sizeof(hostData.AuthResp));
				*Len = sizeof(hostData.AuthResp);
			}
			break;
		/*
		case TAG_8C_CDOL_1:
		case 0x8C00:
			*Len = 0;
			break;
		case TAG_8D_CDOL_2:
		case 0x8D00:
			*Len = 0;
			break;
		*/
		case TAG_8E_CVM_LIST:
		case 0x8E00:
			if (transData.T_DF61_Info_Received_Data[6] & EMV_CT_TRX_8E_CVM_List)
			{
				memcpy(value, transData.T_8E_CVM_List, EMV_ADK_MAX_CVM_LIST_LEN);
				*Len = EMV_ADK_MAX_CVM_LIST_LEN;
			}
			break;
		/*
		case TAG_8F_CERTIF_AUTH_PK_ID:
		case 0x8F00:
			*Len = 0;
			break;
		case TAG_90_ISS_PK_CERTIF:
		case 0x9000:
			*Len = 0;
			break;
		*/
		case TAG_91_ISS_AUTH_DATA:
		case 0x9100:
			if (hostData.Info_Included_Data[0] & INPUT_ONL_AUTHDATA)
			{
				memcpy(value, hostData.AuthData+2, hostData.LenAuth-2);
				*Len = hostData.LenAuth-2;
			}
			break;
		/*
		case TAG_92_ISS_PK_REMAINDER:
		case 0x9200:
			*Len = 0;
			break;
		case TAG_93_SGND_STAT_APP_DATA:
		case 0x9300:
			*Len = 0;
			break;
		case TAG_94_AFL:
		case 0x9400:
			*Len = 0;
			break;
		*/
		case TAG_95_TVR:
		case 0x9500:
			if (transData.T_DF61_Info_Received_Data[1] & TRX_95_TVR)
			{
				memcpy(value, transData.T_95_TVR, sizeof(transData.T_95_TVR));
				*Len = sizeof(transData.T_95_TVR);
			}
			break;
		/*
		case TAG_97_TDOL:
		case 0x9700:
			*Len = 0;
			break;
		case TAG_98_TC_HASH_VALUE:
		case 0x9800:
			*Len = 0;
			break;
		case TAG_99_TRANS_PIN_DATA:
		case 0x9900:
			*Len = 0;
			break;
		*/
		case TAG_9A_TRANS_DATE:
		case 0x9A00:
			if (transData.T_DF61_Info_Received_Data[1] & TRX_9A_DATE)
			{
				memcpy(value, transData.T_9A_Date, sizeof(transData.T_9A_Date));
				*Len = sizeof(transData.T_9A_Date);
			}
			break;
		case TAG_9B_TSI:
		case 0x9B00:
			if (transData.T_DF61_Info_Received_Data[3] & TRX_9B_TSI)
			{
				memcpy(value, transData.T_9B_TSI, sizeof(transData.T_9B_TSI));
				*Len = sizeof(transData.T_9B_TSI);
			}
			break;
		case TAG_9C_TRANS_TYPE:
		case 0x9C00:
			if (transData.T_DF61_Info_Received_Data[1] & TRX_9C_TRANSTYPE)
			{
				value[0] = transData.T_9C_TransType;
				*Len = 1;
			}
			break;
		/*
		case TAG_9D_DDF_NAME:
		case 0x9D00:
			*Len = 0;
			break;
		case TAG_9F01_ACQ_ID:
			*Len = 0;
			break;
		*/
		case TAG_9F02_NUM_AMOUNT_AUTH:

			if (t_Info_Included_Data[1] & INPUT_OFL_AMOUNT)
			{
				memcpy(value, payData.Amount, sizeof(payData.Amount));
				*Len = sizeof(payData.Amount);
			}
			break;
		case TAG_9F04_BIN_AMOUNT_OTHER:
			if (t_Info_Included_Data[0] & INPUT_OFL_CB_AMOUNT)
			{
				amt = atol((const char *)payData.Cashback_Amount);
				memcpy(value, (void *)&amt, sizeof(long));
				*Len = sizeof(long);
			}
			break;
		/*
		case TAG_9F05_APP_DISCR_DATA:
			*Len = 0;
			break;
		*/
		case TAG_9F06_AID:
			if (transData.T_DF61_Info_Received_Data[3] & TRX_9F06_AID)
			{
				*Len = transData.T_9F06_AID.aidlen;
				memcpy(value, transData.T_9F06_AID.AID, transData.T_9F06_AID.aidlen);
			}
			break;
		case TAG_84_DF_NAME:
		case 0x8400:
			if (transData.T_DF61_Info_Received_Data[2] & TRX_84_DFNAME)
			{
				*Len = transData.T_84_DFName.aidlen;
				memcpy(value, transData.T_84_DFName.AID, *Len);
			}
			break;
		/*
		case TAG_9F07_APP_USAGE_CONTROL:
			*Len = 0;
			break;
		*/
		case TAG_9F08_ICC_APP_VERSION_NB:
			if (transData.T_DF61_Info_Received_Data[5] & TRX_9F08_ICC_APPLI_VERS_NO)
			{
				memcpy(value, transData.T_9F08_ICC_Appli_Vers_No, sizeof(transData.T_9F08_ICC_Appli_Vers_No));
				*Len = sizeof(transData.T_9F08_ICC_Appli_Vers_No);
			}
			break;
		case TAG_9F09_TRM_APP_VERSION_NB:
			if (transData.T_DF61_Info_Received_Data[3] & TRX_9F09_VERNUM)
			{
				memcpy(value, transData.T_9F09_VerNum, sizeof(transData.T_9F09_VerNum));
				*Len = sizeof(transData.T_9F09_VerNum);
			}
			break;
		/*
		case TAG_9F0B_CARDHOLDER_NAME_XT:
			*Len = 0;
			break;
		*/
		case TAG_9F0D_IAC_DEFAULT:
			if (transData.T_DF61_Info_Received_Data[4] & TRX_9F0D_IAC_DEFAULT)
			{
				memcpy(value, transData.T_9F0D_IACDefault, sizeof(transData.T_9F0D_IACDefault));
				*Len = sizeof(transData.T_9F0D_IACDefault);
			}
			break;
		case TAG_9F0E_IAC_DENIAL:
			if (transData.T_DF61_Info_Received_Data[4] & TRX_9F0E_IAC_DENIAL)
			{
				memcpy(value, transData.T_9F0E_IACDenial, sizeof(transData.T_9F0E_IACDenial));
				*Len = sizeof(transData.T_9F0E_IACDenial);
			}
			break;
		case TAG_9F0F_IAC_ONLINE:
			if (transData.T_DF61_Info_Received_Data[4] & TRX_9F0F_IAC_ONLINE)
			{
				memcpy(value, transData.T_9F0F_IACOnline, sizeof(transData.T_9F0F_IACOnline));
				*Len = sizeof(transData.T_9F0F_IACOnline);
			}
			break;
                /*
		case TAG_9F10_ISS_APP_DATA:
			if (transData.T_DF61_Info_Received_Data[1] & TRX_9F10_DATAISSUER)
			{
				memcpy(value, transData.T_9F10_DataIssuer.issData, transData.T_9F10_DataIssuer.issDataLen);
				*Len = transData.T_9F10_DataIssuer.issDataLen;
			}
			break; */
		case TAG_9F11_ISS_CODE_TABLE_ID:
			if (selectData.T_DF61_Info_Received_Data[0] & DF61_SEL_9F11_CODE_TABLE)
			{
				value[0] = selectData.T_9F11_CodeTable;
				*Len = 1;
			}
			break;
		case TAG_9F12_APP_PREFERRED_NAME:
			if (transData.T_DF61_Info_Received_Data[3] & TRX_APPNAME)
			{
				strcpy((char *)value, (const char *)transData.AppName);
				*Len = strlen((const char *)transData.AppName);
			}
			break;
		/*
		case TAG_9F13_LAST_ONLINE_ATC:
			*Len = 0;
			break;
		case TAG_9F14_LO_OFFLINE_LIMIT:
			*Len = 0;
			break;
		*/
		case TAG_9F15_MERCH_CATEG_CODE:
			if (appData.Info_Included_Data[0] & EMV_CT_INPUT_APL_MERCHANT_CATCODE)
			{
				memcpy(value, appData.BrKey, sizeof(appData.BrKey));
				*Len = sizeof(appData.BrKey);
			}
			break;
		case TAG_9F16_MERCHANT_ID:
			if (transData.T_DF61_Info_Received_Data[5] & TRX_9F16_MERCHANT_ID)
			{
				memcpy(value, transData.T_9F16_MerchIdent, sizeof(transData.T_9F16_MerchIdent));
				*Len = sizeof(transData.T_9F16_MerchIdent);
			}
			break;
		case TAG_9F17_PIN_TRIES_LEFT:
			if (extraData.Info_Included_Data[0] & EMV_CT_EXTRADATA_COUNTER)
			{
				value[0] = extraData.T_9F17_PIN_try_counter;
				*Len = 1;
			}
			break;
		/*
		case TAG_9F18_ISS_SCRIPT_ID:
			*Len = 0;
			break;
		*/
		case TAG_9F1A_TRM_COUNTRY_CODE:
			if (transData.T_DF61_Info_Received_Data[2] & TRX_9F1A_TERM_COUNTRY_CODE)
			{
				memcpy(value, transData.T_9F1A_TermCountryCode, sizeof(transData.T_9F1A_TermCountryCode));
				*Len = sizeof(transData.T_9F1A_TermCountryCode);
			}
			break;
		case TAG_9F1B_TRM_FLOOR_LIMIT:
			if (appData.Info_Included_Data[0] & EMV_CT_INPUT_APL_FLOOR_LIMIT)
			{
				memcpy(value, appData.FloorLimit, sizeof(appData.FloorLimit));
				*Len = sizeof(appData.FloorLimit);
			}
			break;
		case TAG_9F1C_TRM_ID:
			if (appData.Info_Included_Data[0] & EMV_CT_INPUT_APL_TID)
			{
				memcpy(value, appData.TermIdent, sizeof(appData.TermIdent));
				*Len = sizeof(appData.TermIdent);
			}
			break;
		/*
		case TAG_9F1D_TRM_RISK_MNGT_DATA:
			*Len = 0;
			break;
		*/
		case TAG_9F1E_IFD_SERIAL_NB:
			if (cardData.T_DF61_Info_Received_Data[2] & TRX_9F1E_IFDSERIALNUMBER)
			{
				memcpy(value, cardData.T_9F1E_IFDSerialNumber, sizeof(cardData.T_9F1E_IFDSerialNumber));
				*Len = sizeof(cardData.T_9F1E_IFDSerialNumber);
			}
			break;
		/*
		case TAG_9F1F_TRACK_1_DISCR_DATA:
			*Len = 0;
			break;
		case TAG_9F20_TRACK_2_DISCR_DATA:
			*Len = 0;
			break;
		*/
		case TAG_9F21_TRANS_TIME:
			if (transData.T_DF61_Info_Received_Data[1] & TRX_9F21_TIME)
			{
				memcpy(value, transData.T_9F21_Time, sizeof(transData.T_9F21_Time));
				*Len = sizeof(transData.T_9F21_Time);
			}
			break;
		/*
		case TAG_9F22_CERTIF_AUTH_PK_ID:
			*Len = 0;
			break;
		case TAG_9F23_HI_OFFLINE_LIMIT:
			*Len = 0;
			break;
		*/
		case TAG_9F26_APP_CRYPTOGRAM:
			if (transData.T_DF61_Info_Received_Data[0] & TRX_9F26_CRYPTOGRAMM)
			{
				memcpy(value, transData.T_9F26_Cryptogramm, sizeof(transData.T_9F26_Cryptogramm));
				*Len = sizeof(transData.T_9F26_Cryptogramm);
			}
			break;
		case TAG_9F27_CRYPT_INFO_DATA:
			if (transData.T_DF61_Info_Received_Data[0] & TRX_9F27_CRYPTINFO)
			{
				value[0] = transData.T_9F27_CryptInfo;
				*Len = 1;
			}
			break;
		/*
		case TAG_9F2D_ICC_PIN_PK_CERTIF:
			*Len = 0;
			break;
		//case TAG_9F2E_ICC_PIN_PK_EXP:
		//	*Len = 0;
		//	break;
		case TAG_9F2F_ICC_PIN_PK_REMAIN:
			*Len = 0;
			break;
		case TAG_9F32_ISS_PK_EXP:
			*Len = 0;
			break;
		*/
		case TAG_9F33_TRM_CAPABILITIES:
			//more locations for update!
			if (transData.T_DF61_Info_Received_Data[2] & TRX_9F33_TERMCAP)
			{
				memcpy(value, transData.T_9F33_TermCap, sizeof(transData.T_9F33_TermCap));
				*Len = sizeof(transData.T_9F33_TermCap);
			}
			break;
		case TAG_9F34_CVM_RESULTS:
			if (transData.T_DF61_Info_Received_Data[2] & TRX_9F34_CVM_RES)
			{
				memcpy(value, transData.T_9F34_CVM_Res, sizeof(transData.T_9F34_CVM_Res));
				*Len = sizeof(transData.T_9F34_CVM_Res);
			}
			break;
		case TAG_9F35_TRM_TYPE:
			if (transData.T_DF61_Info_Received_Data[2] & TRX_9F35_TERMTYP)
			{
				value[0] = transData.T_9F35_TermTyp;
				*Len = 1;
			}
			break;
		case TAG_9F36_ATC:
			if (transData.T_DF61_Info_Received_Data[0] & TRX_9F36_ATC)
			{
				memcpy(value, transData.T_9F36_ATC, sizeof(transData.T_9F36_ATC));
				*Len = sizeof(transData.T_9F36_ATC);
			}
			break;
		case TAG_9F37_UNPREDICTABLE_NB:
			if (transData.T_DF61_Info_Received_Data[1] & TRX_9F37_RANDOM_NUMBER)
			{
				memcpy(value, transData.T_9F37_RandomNumber, sizeof(transData.T_9F37_RandomNumber));
				*Len = sizeof(transData.T_9F37_RandomNumber);
			}
			break;
		/*
		case TAG_9F38_PDOL:
			*Len = 0;
			break;
		*/
		case TAG_9F39_POS_ENTRY_MODE:
			if (transData.T_DF61_Info_Received_Data[0] & TRX_9F39_POS_ENTRY_MODE)
			{
				value[0] = transData.T_9F39_POSEntryMode;
				*Len = 1;
			}
			break;
		case TAG_9F3A_AMNT_REF_CURRENCY:
			if (t_Info_Included_Data[1] & INPUT_OFL_AMOUNT)
			{
				memcpy(value, payData.Amount, sizeof(payData.Amount));
				*Len = sizeof(payData.Amount);
			}
			break;
		/*
		case TAG_9F3B_APP_REF_CURRENCY:
			*Len = 0;
			break;
		*/
		case TAG_9F3C_TRANS_REF_CURRENCY:
			if (t_Info_Included_Data[1] & INPUT_OFL_AMOUNT_CURRENCY)
			{
				memcpy(value, payData.CurrencyTrans, sizeof(payData.CurrencyTrans));
				*Len = sizeof(payData.CurrencyTrans);
			}
			break;
		case TAG_9F3D_TRANS_CURRENCY_EXP:
			if (t_Info_Included_Data[1] & INPUT_OFL_CUREXPONENT)
			{
				value[0] = payData.ExpTrans;
				*Len = 1;
			}
			break;
		case TAG_9F40_ADD_TRM_CAP:
			if (transData.T_DF61_Info_Received_Data[5] & TRX_9F40_TERMCAP)
			{
				memcpy(value, transData.T_9F40_AddTermCap, sizeof(transData.T_9F40_AddTermCap));
				*Len = sizeof(transData.T_9F40_AddTermCap);
			}
			break;
		case TAG_9F41_TRANS_SEQUENCE_NB:
			//multiple locations
			if (transData.T_DF61_Info_Received_Data[0] & TRX_9F41_TRANSCOUNT)
			{
				memcpy(value, transData.T_9F41_TransCount, sizeof(transData.T_9F41_TransCount));
				*Len = sizeof(transData.T_9F41_TransCount);
			}
			break;
//		case TAG_9F42_APP_CURRENCY_CODE:	//iq_audi_030217
//			if (cardData.T_DF61_Info_Received_Data[5] & TRX_9F42_APPCURRENCYCODE)
//			{
//				memcpy(value, cardData.T_9F42_AppCurrencyCode, sizeof(cardData.T_9F42_AppCurrencyCode));
//				*Len = sizeof(cardData.T_9F42_AppCurrencyCode);
//			}
//			break;
		case TAG_9F43_APP_REF_CURRCY_EXP:
			if (t_Info_Included_Data[1] & INPUT_OFL_CUREXPONENT)
			{
				value[0] = payData.ExpTrans;
				*Len = 1;
			}
			break;
		case TAG_9F44_APP_CURRENCY_EXP:
			if (t_Info_Included_Data[1] & INPUT_OFL_AMOUNT_CURRENCY)
			{
				memcpy(value, payData.CurrencyTrans, sizeof(payData.CurrencyTrans));
				*Len = sizeof(payData.CurrencyTrans);
			}
			break;
		case TAG_9F45_DATA_AUTHENT_CODE:
			if (transData.T_DF61_Info_Received_Data[4] & TRX_9F45_DATAAUTHCODE)
			{
				memcpy(value, transData.T_9F45_DataAuthCode, sizeof(transData.T_9F45_DataAuthCode));
				*Len = sizeof(transData.T_9F45_DataAuthCode);
			}
			break;
		/*
		case TAG_9F46_ICC_PK_CERTIF:
			*Len = 0;
			break;
		//case TAG_9F47_ICC_PK_EXP:
		//	*Len = 0;
		//	break;
		case TAG_9F48_ICC_PK_REMAINDER:
			*Len = 0;
			break;
		case TAG_9F49_DDOL:
			*Len = 0;
			break;
		case TAG_9F4A_SDA_TAG_LIST:
			*Len = 0;
			break;
		case TAG_9F4B_SDA_DATA:
			*Len = 0;
			break;
		*/
		case TAG_9F4C_ICC_DYNAMIC_NB:
			if (transData.T_DF61_Info_Received_Data[4] & TRX_9F4C_ICCDYNNUMBER)
			{
				memcpy(value, transData.T_9F4C_ICCDynNumber.iccRND, transData.T_9F4C_ICCDynNumber.iccRNDLen);
				*Len = transData.T_9F4C_ICCDynNumber.iccRNDLen;
			}
			break;
		/*
		case TAG_9F4E_TAC_MERCHANTLOC:
			*Len = 0;
			break;
		case TAG_9F51_APP_CURRENCY_CODE:
			*Len = 0;
			break;
		case TAG_9F52_DEFAULT_ACTION:
			*Len = 0;
			break;
		case TAG_9F53_CONSECUTIVE_LIM_TRANS_INT:
			*Len = 0;
			break;
		case TAG_9F54_CUMULATIVE_TRANS_LIMET:
			*Len = 0;
			break;
		case TAG_9F56_ISS_AUTH_INCATOR:
			*Len = 0;
			break;
		case TAG_9F5D_VISA_AOSA:
			*Len = 0;
			break;
		case TAG_9F66_VISA_TTQ:
			*Len = 0;
			break;
		case TAG_9F6C_VISA_CTQ:
			*Len = 0;
			break;
		*/
		case TAG_9F53_TRANS_CATEGORY_CODE:
			if (transData.T_DF61_Info_Received_Data[6] & TRX_9F53_MC_CATCODE)
			{
				memcpy(value, transData.T_9F53_MC_CatCode, sizeof(transData.T_9F53_MC_CatCode));
				*Len = sizeof(transData.T_9F53_MC_CatCode);
			}
			break;

		case TAG_DF24_THRESHHOLD:
			if (appData.Info_Included_Data[0] & EMV_CT_INPUT_APL_THRESH)
			{
				memcpy(value, appData.Threshhold, sizeof(appData.Threshhold));
				*Len = sizeof(appData.Threshhold);
			}
			break;

		default:
			// Try fetching tag from the ADK
			// dlog_msg("Trying to fetch tag %Xh", tagNum);
			*Len = 0;
				
			if (GetTag(tagNum, value, Len) < 0)
			{
				// Not found, set *Len to 0 to avoid problems
				*Len = 0;
			}
			
				/*
				if(*Len == 0)
				{
					bool canFetch = true;
					bool kernelOpened = g_CEMVtrns.Is_Kernel_Opened();
					if (!kernelOpened)
					{
						g_logger.Log("OPENING EMV KERNEL...");
						if (g_CEMVtrns.Open_Kernel() != EMV_ADK_OK)
						{
							dlog_error("Cannot open the kernel!");
							g_CEMVtrns.Close_Kernel();
							canFetch = false;
						}
					}
					if (canFetch)
					{
						
						using namespace com_verifone_TLVLite;
						unsigned long tag = tagNum;
						unsigned char localBuf[384];
						unsigned short resultSize = 0;
						if (EMV_ADK_OK == EMV_CT_fetchTxnTags(0, &tag, 1, localBuf, sizeof(localBuf), &resultSize))
						{
							// We got something
							dlog_msg("FetchTxnTags: got %d bytes of data", resultSize);
							dlog_hex(localBuf, resultSize, "DATA");
							ConstData buf(localBuf, resultSize);
							TLVListWrapper tlvAnswer(buf);
							for (TLVIterator itAns = tlvAnswer.begin(); itAns != tlvAnswer.end(); ++itAns)
							{
								TagLenVal tlv = *itAns;
								*Len = tlv.getData().getSize();
								if (*Len > 0)
								{
									memcpy(value, tlv.getData().getBuffer(), *Len);
								}
							}
						}
						else
						{
							*Len = 0;
							value[0]=0;
						}
					}
					if (!kernelOpened) 
					{
						g_logger.Log("Closing EMV kernel...");
						g_CEMVtrns.Close_Kernel();
					}
				}
				*/
				
			break;
	}

	return *Len;
}

int cEMVcollection::SetTLVData(int tagNum, unsigned char *value, int Len)
{

	switch(tagNum)
	{
		case TAG_4F_APP_ID:
		case 0x4F00:
			if (sizeof(selectData.T_DF04_Aidselected) >= Len)
			{
				memcpy(selectData.T_DF04_Aidselected.AID, value, Len);
				selectData.T_DF04_Aidselected.aidlen = Len;
				selectData.T_DF61_Info_Received_Data[0] |= DF61_SEL_AID;
			}
			break;
		case TAG_50_APP_LABEL:
		case 0x5000:
			if (sizeof(selectData.T_50_ApplicationName) > Len)
			{
				memset(selectData.T_50_ApplicationName, 0, sizeof(selectData.T_50_ApplicationName));
				memcpy(selectData.T_50_ApplicationName, value, Len < sizeof(selectData.T_50_ApplicationName) ? Len:sizeof(selectData.T_50_ApplicationName)-1);
				//Len = strlen((const char *)selectData.T_50_ApplicationName);
				selectData.T_DF61_Info_Received_Data[0] |= DF61_SEL_50_APPLICATION_NAME;
				memset(appData.AppName, 0, sizeof(appData.AppName));
				memcpy(appData.AppName, value, Len < sizeof(appData.AppName) ? Len:sizeof(appData.AppName)-1);
				//strcpy((char *)appData.AppName, (const char *)value);
				appData.Info_Included_Data[0] |= EMV_CT_INPUT_APL_NAME;
			}
			else
			{
				Len = 0;
			}
			break;
		case TAG_57_TRACK2_EQUIVALENT:
		case 0x5700:
			memcpy(transData.T_57_DataTrack2.tr2data, value, Len);
			transData.T_57_DataTrack2.tr2len = Len;
			transData.T_DF61_Info_Received_Data[1] |= TRX_57_DATA_TRACK2;
			break;
		case TAG_5A_APP_PAN:
		case 0x5A00:
			memcpy(transData.T_5A_PAN, value, Len);
			transData.T_DF61_Info_Received_Data[0] |= TRX_5A_PAN;
			break;
		case TAG_5F20_CARDHOLDER_NAME:
			memcpy(transData.T_5F20_Cardholder.crdName, value, Len);
			transData.T_5F20_Cardholder.crdNameLen = Len;
			transData.T_DF61_Info_Received_Data[5] |= TRX_5F20_CARDHOLDER;
			break;
		case TAG_5F24_APP_EXP_DATE:
			memcpy(transData.T_5F24_AppExpDate, value, Len);
			transData.T_DF61_Info_Received_Data[0] |= TRX_5F24_APPEXPDATE;
			break;
		case TAG_5F25_APP_EFF_DATE:
			memcpy(transData.T_5F25_AppEffDate, value, Len);
			transData.T_DF61_Info_Received_Data[0] |= TRX_5F24_APPEXPDATE;
			break;
		case TAG_5F28_ISS_COUNTRY_CODE:
			memcpy(transData.T_5F28_IssCountryCode, value, Len);
			transData.T_DF61_Info_Received_Data[3] |= TRX_5F28_ISSCOUNTRYCODE;
			break;
		case TAG_5F2A_TRANS_CURRENCY:

			if(sizeof(transData.T_5F2A_CurrencyTrans) >= Len)
			{
				memcpy(transData.T_5F2A_CurrencyTrans, value, Len);
				transData.T_DF61_Info_Received_Data[2] |= TRX_5F2A_TRX_CURRENCY;
				memcpy(termData.CurrencyTrans, value, Len);
				termData.Info_Included_Data[0] |= EMV_CT_INPUT_TRM_CURRENCY;
				memcpy(payData.CurrencyTrans, value, Len);
				t_Info_Included_Data[1] |= INPUT_OFL_AMOUNT_CURRENCY;
				if (!(t_Info_Included_Data[1] & INPUT_OFL_CUREXPONENT))
				{
					payData.ExpTrans = 2;
					t_Info_Included_Data[1] |= INPUT_OFL_CUREXPONENT;
				}
				memcpy(cardData.T_5F2A_CurrencyTrans, value, Len);
				cardData.T_DF61_Info_Received_Data[2] |= TRX_5F2A_TRX_CURRENCY;
			}
			else
			{
				Len = 0;
			}
			break;
		case TAG_5F2D_LANGUAGE:
			if(sizeof(transData.T_5F2D_Lang_Pref) >= Len)
			{
				memcpy(transData.T_5F2D_Lang_Pref, value, Len);
				transData.T_DF61_Info_Received_Data[5] |= TRX_5F2D_LANG_PREFERENCE;
			}
			else
				Len = 0;
			break;
		case TAG_5F30_SERVICE_CODE:
			if(sizeof(transData.T_5F30_ServiceCode) >= Len)
			{
				memcpy(transData.T_5F30_ServiceCode, value, Len);
				transData.T_DF61_Info_Received_Data[6] |= TRX_5F30_SERVICE_CODE;
			}
			else
				Len = 0;
			break;
		case TAG_5F34_PAN_SEQUENCE_NB:
			if(sizeof(transData.T_5F34_PANSequenceNo) >= Len)
			{
				memcpy(transData.T_5F34_PANSequenceNo, value, Len);
				transData.T_DF61_Info_Received_Data[1] |= TRX_5F34_PAN_SEQ_NUMBER;
				// t_Info_Included_Data[1] |= INPUT_OFL_CUREXPONENT;
			}
			else
				Len = 0;
			break;
		case TAG_5F36_TRANS_CURRENCY_EXP:
			if(sizeof(transData.T_5F36_Trx_Currency_Exp) >= Len)
			{
				transData.T_5F36_Trx_Currency_Exp = value[0];
				transData.T_DF61_Info_Received_Data[5] |= TRX_5F36_TRX_CURRENCY_EXPO;
				payData.ExpTrans = value[0];
				t_Info_Included_Data[1] |= INPUT_OFL_CUREXPONENT;
			}
			else
				Len = 0;
			break;
		case TAG_5F57_ACCOUNT_TYPE:
			if(Len == 1)
			{
				value[0] = payData.uc_AccountType;
				t_Info_Included_Data[0] |= INPUT_OFL_ACCOUNT_TYPE;
			}
			else
				Len = 0;
			break;
		/*
		case TAG_81_BIN_AMOUNT_AUTH:
		case 0x8100:
			if(sizeof(long) == Len)
			{
				char tmp[sizeof(payData.Amount) * 2 + 1];
				long amt = 0;
				sprintf(tmp, "012.012%d", (long)value);
				//SVC_DSP_2_HEX(tmp, (char *)payData.Amount, sizeof(payData.Amount));
				com_verifone_pml::svcDsp2Hex((const char *)tmp, sizeof(payData.Amount), (char *)payData.Amount, sizeof(payData.Amount)*2+1);
				amt = atol((const char *)payData.Amount);
				memcpy(value, (void *)&amt, sizeof(long));
			}
			else
			{
				Len = 0;
			}
			break;
		*/
		case TAG_82_AIP:
		case 0x8200:
			if(sizeof(transData.T_82_AIP) >= Len)
			{
				memcpy(transData.T_82_AIP, value, Len);
				transData.T_DF61_Info_Received_Data[2] |= TRX_82_AIP;
			}
			else
				Len = 0;
			break;
		/*
		case TAG_87_APP_PRIORITY_ID:
		case 0x8700:
			Len = 0;
			break;
		*/
		case TAG_89_AUTH_CODE:
		case 0x8900:
			if (sizeof(hostData.AuthorizationCode) >= Len)
			{
				memcpy(hostData.AuthorizationCode, value, Len);
				hostData.Info_Included_Data[0] |= INPUT_ONL_AUTHCODE;
			}
			break;
		case TAG_8A_AUTH_RESP_CODE:
		case 0x8A00:
			if(sizeof(hostData.AuthResp) >= Len)
			{
				memcpy(hostData.AuthResp, value, Len);
				hostData.Info_Included_Data[0] |= INPUT_ONL_ONLINE_AC;
				dlog_msg("Added AuthResp (%02X:%02X)", hostData.AuthResp[0], hostData.AuthResp[1]);
			}
			else
				Len = 0;
			break;
		/*
		case TAG_8C_CDOL_1:
		case 0x8C00:
			Len = 0;
			break;
		case TAG_8D_CDOL_2:
		case 0x8D00:
			Len = 0;
			break;
		*/
		case TAG_8E_CVM_LIST:
		case 0x8E00:
			if(EMV_ADK_MAX_CVM_LIST_LEN >= Len)
			{
				memcpy(transData.T_8E_CVM_List, value, Len);
				transData.T_DF61_Info_Received_Data[6] |= EMV_CT_TRX_8E_CVM_List;
			}
			else
				Len = 0;
			break;
		/*
		case TAG_8F_CERTIF_AUTH_PK_ID:
		case 0x8F00:
			Len = 0;
			break;
		case TAG_90_ISS_PK_CERTIF:
		case 0x9000:
			Len = 0;
			break;
		*/
		case TAG_91_ISS_AUTH_DATA:
		case 0x9100:
			if(Len <= sizeof(t_91_authData)-2) // we need to include 91xx, where xx is len, in this data...
			{
				hostData.AuthData = t_91_authData;
				hostData.AuthData[0] = 0x91;
				hostData.AuthData[1] = static_cast<unsigned char>(Len);
				memcpy(hostData.AuthData+2, value, Len);
				hostData.LenAuth = Len+2;
				hostData.Info_Included_Data[0] |= INPUT_ONL_AUTHDATA;
				g_logger.Log("Iss Auth Added (len: %d) %02X%02X%02X", hostData.LenAuth, hostData.AuthData[0], hostData.AuthData[1], hostData.AuthData[2]);
			}
			else
			{
				Len = 0;
			}
			break;
		/*
		case TAG_92_ISS_PK_REMAINDER:
		case 0x9200:
			Len = 0;
			break;
		case TAG_93_SGND_STAT_APP_DATA:
		case 0x9300:
			Len = 0;
			break;
		case TAG_94_AFL:
		case 0x9400:
			Len = 0;
			break;
		*/
		case TAG_95_TVR:
		case 0x9500:
			if(Len == sizeof(transData.T_95_TVR))
			{
				memcpy(transData.T_95_TVR, value, Len);
				transData.T_DF61_Info_Received_Data[1] |= TRX_95_TVR;
			}
			else
			{
				Len = 0;
			}
			break;
		/*
		case TAG_97_TDOL:
		case 0x9700:
			Len = 0;
			break;
		case TAG_98_TC_HASH_VALUE:
		case 0x9800:
			Len = 0;
			break;
		case TAG_99_TRANS_PIN_DATA:
		case 0x9900:
			Len = 0;
			break;
		*/
		case TAG_9A_TRANS_DATE:
		case 0x9A00:
			if(sizeof(transData.T_9A_Date) >= Len)
			{
				memcpy(transData.T_9A_Date, value, Len);
				transData.T_DF61_Info_Received_Data[1] |= TRX_9A_DATE;
				memcpy(payData.Date, value, Len);
				t_Info_Included_Data[1] |= INPUT_OFL_DATE;
			}
			else
			{
				Len = sizeof(transData.T_9A_Date);
			}
			break;
		case TAG_9B_TSI:
		case 0x9B00:
			if(sizeof(transData.T_9B_TSI) >= Len)
			{
				memcpy(transData.T_9B_TSI, value, Len);
				transData.T_DF61_Info_Received_Data[3] |= TRX_9B_TSI;
			}
			else
				Len = 0;
			break;
		case TAG_9C_TRANS_TYPE:
		case 0x9C00:
			if(Len == 1)
			{
				transData.T_9C_TransType = value[0];
				transData.T_DF61_Info_Received_Data[1] |= TRX_9C_TRANSTYPE;
				inData.TransType = value[0];
			}
			else
			{
				Len = 0;
			}
			break;
		/*
		case TAG_9D_DDF_NAME:
		case 0x9D00:
			Len = 0;
			break;
		case TAG_9F01_ACQ_ID:
			Len = 0;
			break;
		*/
		case TAG_9F02_NUM_AMOUNT_AUTH:
			if(Len == 6)
			{
				memcpy(payData.Amount, value, Len);
				t_Info_Included_Data[1] |= INPUT_OFL_AMOUNT;
				dlog_hex(value, Len, ">>> set amount <<<");
			}
			else
			{
				Len = 0;
			}
			break;
		case TAG_9F03_NUM_AMOUNT_OTHER:
			if (Len == 6)
			{
				memcpy(payData.Cashback_Amount, value, Len);
				t_Info_Included_Data[0] |= INPUT_OFL_CB_AMOUNT;
			}
			else
			{
				Len = 0;
			}
			break;
		/*
		case TAG_9F04_BIN_AMOUNT_OTHER:
			if(Len == sizeof(long))
			{
				char tmp[sizeof(payData.Cashback_Amount) * 2 + 1];
				sprintf(tmp, "012.012%d", (long)value);
				//SVC_DSP_2_HEX(tmp, (char *)payData.Cashback_Amount, sizeof(payData.Cashback_Amount));
				com_verifone_pml::svcDsp2Hex((const char *)tmp, sizeof(payData.Cashback_Amount), (char *)payData.Cashback_Amount, sizeof(payData.Cashback_Amount)*2+1);
			}
			else
			{
				Len = 0;
			}
			break;
		*/
		/*
		case TAG_9F05_APP_DISCR_DATA:
			Len = 0;
			break;
		*/
		case TAG_9F06_AID:
			if (sizeof(transData.T_9F06_AID.AID) >= Len)
			{
				memcpy(transData.T_9F06_AID.AID, value, Len);
				transData.T_9F06_AID.aidlen = Len;
				transData.T_DF61_Info_Received_Data[3] |= TRX_9F06_AID;
			}
			else
			{
				Len = 0;
			}
			break;
		/*
		case TAG_9F07_APP_USAGE_CONTROL:
			Len = 0;
			break;
		*/
		case TAG_9F08_ICC_APP_VERSION_NB:
			if(sizeof(transData.T_9F08_ICC_Appli_Vers_No) == Len)
			{
				memcpy(transData.T_9F08_ICC_Appli_Vers_No, value, Len);
				transData.T_DF61_Info_Received_Data[5] |= TRX_9F08_ICC_APPLI_VERS_NO;
			}
			else
				Len = 0;
			break;
		case TAG_9F09_TRM_APP_VERSION_NB:
			if(sizeof(transData.T_9F09_VerNum) == Len)
			{
				memcpy(transData.T_9F09_VerNum, value, Len);
				transData.T_DF61_Info_Received_Data[3] |= TRX_9F09_VERNUM;
				memcpy(appData.VerNum, value, Len);
				appData.Info_Included_Data[0] |= EMV_CT_INPUT_APL_VERSION;
			}
			else
				Len = 0;
			break;
		/*
		case TAG_9F0B_CARDHOLDER_NAME_XT:
			Len = 0;
			break;
		*/
		case TAG_9F0D_IAC_DEFAULT:
			if(sizeof(transData.T_9F0D_IACDefault) == Len)
			{
				memcpy(transData.T_9F0D_IACDefault, value, Len);
				transData.T_DF61_Info_Received_Data[4] |= TRX_9F0D_IAC_DEFAULT;
			}
			else
				Len = 0;
			break;
		case TAG_9F0E_IAC_DENIAL:
			if(sizeof(transData.T_9F0E_IACDenial) == Len)
			{
				memcpy(transData.T_9F0E_IACDenial, value, Len);
				transData.T_DF61_Info_Received_Data[4] |= TRX_9F0E_IAC_DENIAL;
			}
			else
				Len = 0;
			break;
		case TAG_9F0F_IAC_ONLINE:
			if(sizeof(transData.T_9F0F_IACOnline) == Len)
			{
				memcpy(transData.T_9F0F_IACOnline, value, Len);
				transData.T_DF61_Info_Received_Data[4] |= TRX_9F0F_IAC_ONLINE;
			}
			else
				Len = 0;
			break;
                /*
		case TAG_9F10_ISS_APP_DATA:
			if(sizeof(transData.T_9F10_DataIssuer.issData) >= Len)
			{
				memcpy(transData.T_9F10_DataIssuer.issData, value, Len);
				transData.T_9F10_DataIssuer.issDataLen = Len;
				transData.T_DF61_Info_Received_Data[1] |= TRX_9F10_DATAISSUER;
			}
			else
			{
				Len = 0;
			}
			break; */
		case TAG_9F11_ISS_CODE_TABLE_ID:
			if(Len == 1)
			{
				selectData.T_9F11_CodeTable = value[0];
				selectData.T_DF61_Info_Received_Data[0] |= DF61_SEL_9F11_CODE_TABLE;
			}
			else
				Len = 0;
			break;
		case TAG_9F12_APP_PREFERRED_NAME:
			if(sizeof(transData.AppName) > Len)
			{
				memset(transData.AppName, 0, sizeof(transData.AppName));
				memcpy(transData.AppName, value, Len < sizeof(transData.AppName) ? Len:sizeof(transData.AppName)-1);
				transData.T_DF61_Info_Received_Data[3] |= TRX_APPNAME;
				memset(appData.AppName, 0, sizeof(appData.AppName));
				memcpy(appData.AppName, value, Len < sizeof(transData.AppName) ? Len:sizeof(appData.AppName)-1);
				appData.Info_Included_Data[0] |= EMV_CT_INPUT_APL_NAME;
			}
			else
				Len = 0;
			break;
		/*
		case TAG_9F13_LAST_ONLINE_ATC:
			Len = 0;
			break;
		case TAG_9F14_LO_OFFLINE_LIMIT:
			Len = 0;
			break;
		*/
		case TAG_9F15_MERCH_CATEG_CODE:
			if(sizeof(appData.BrKey) >= Len)
			{
				memcpy(appData.BrKey, value, Len);
				appData.Info_Included_Data[0] |= EMV_CT_INPUT_APL_MERCHANT_CATCODE;
			}
			else
				Len = 0;
			break;
		case TAG_9F16_MERCHANT_ID:
			if(sizeof(transData.T_9F16_MerchIdent) >= Len)
			{
				memcpy(transData.T_9F16_MerchIdent, value, Len);
				transData.T_DF61_Info_Received_Data[5] |= TRX_9F16_MERCHANT_ID;
				memcpy(appData.MerchIdent, value, Len);
				appData.Info_Included_Data[1] |= EMV_CT_INPUT_APL_MERCHANT_IDENT;
			}
			else
			{
				Len = 0;
			}
			break;
		case TAG_9F17_PIN_TRIES_LEFT:
			extraData.T_9F17_PIN_try_counter = value[0];
			extraData.Info_Included_Data[0] |= EMV_CT_EXTRADATA_COUNTER;
			Len = 1;
			break;
		/*
		case TAG_9F18_ISS_SCRIPT_ID:
			Len = 0;
			break;
		*/
		case TAG_9F1A_TRM_COUNTRY_CODE:
			if(sizeof(transData.T_9F1A_TermCountryCode) >= Len)
			{
				memcpy(transData.T_9F1A_TermCountryCode, value, Len);
				transData.T_DF61_Info_Received_Data[2] |= TRX_9F1A_TERM_COUNTRY_CODE;
				memcpy(termData.TermCountryCode, value, Len);
				termData.Info_Included_Data[0] |= EMV_CT_INPUT_TRM_COUNTRY_CODE;
				memcpy(appData.App_CountryCodeTerm, value, Len);
				appData.Info_Included_Data[2] |= EMV_CT_INPUT_APL_COUNTRY_CODE;
			}
			else
			{
				Len = 0;
			}
			break;
		case TAG_9F1B_TRM_FLOOR_LIMIT:
			if(sizeof(appData.FloorLimit) >= Len)
			{
				memcpy(appData.FloorLimit, value, Len);
				appData.Info_Included_Data[0] |= EMV_CT_INPUT_APL_FLOOR_LIMIT;
				// dlog_msg("Floor set!");
			}
			else
				Len = 0;
			break;
		case TAG_9F1C_TRM_ID:
			if(sizeof(appData.TermIdent) >= Len)
			{
				memcpy(appData.TermIdent, value, Len);
				appData.Info_Included_Data[0] |= EMV_CT_INPUT_APL_TID;
			}
			else
				Len = 0;
			break;
		/*
		case TAG_9F1D_TRM_RISK_MNGT_DATA:
			Len = 0;
			break;
		*/
		case TAG_9F1E_IFD_SERIAL_NB:
			if(sizeof(cardData.T_9F1E_IFDSerialNumber) >= Len)
			{
				memcpy(cardData.T_9F1E_IFDSerialNumber, value, Len);
				cardData.T_DF61_Info_Received_Data[2] |= TRX_9F1E_IFDSERIALNUMBER;
				memcpy(termData.IFDSerialNumber, value, Len);
				termData.Info_Included_Data[1] |= EMV_CT_INPUT_TRM_IFD_SERIAL;
			}
			else
			{
				Len = 0;
			}
			break;
		/*
		case TAG_9F1F_TRACK_1_DISCR_DATA:
			Len = 0;
			break;
		case TAG_9F20_TRACK_2_DISCR_DATA:
			Len = 0;
			break;
		*/
		case TAG_9F21_TRANS_TIME:
			if(sizeof(transData.T_9F21_Time) == Len)
			{
				memcpy(transData.T_9F21_Time, value, Len);
				transData.T_DF61_Info_Received_Data[1] |= TRX_9F21_TIME;
				memcpy(payData.Time, value, Len);
				t_Info_Included_Data[1] |= INPUT_OFL_TIME;
			}
			else
			{
				Len = 0;
			}
			break;
		/*
		case TAG_9F22_CERTIF_AUTH_PK_ID:
			Len = 0;
			break;
		case TAG_9F23_HI_OFFLINE_LIMIT:
			Len = 0;
			break;
		*/
		case TAG_9F26_APP_CRYPTOGRAM:
			if(sizeof(transData.T_9F26_Cryptogramm) >= Len)
			{
				memcpy(transData.T_9F26_Cryptogramm, value, Len);
				transData.T_DF61_Info_Received_Data[0] |= TRX_9F26_CRYPTOGRAMM;
			}
			else
				Len = 0;
			break;
		case TAG_9F27_CRYPT_INFO_DATA:
			if(Len == 1)
			{
				transData.T_9F27_CryptInfo= value[0];
				transData.T_DF61_Info_Received_Data[0] |= TRX_9F27_CRYPTINFO;
			}
			else
				Len = 0;
			break;
		/*
		case TAG_9F2D_ICC_PIN_PK_CERTIF:
			Len = 0;
			break;
		//case TAG_9F2E_ICC_PIN_PK_EXP:
		//	Len = 0;
		//	break;
		case TAG_9F2F_ICC_PIN_PK_REMAIN:
			Len = 0;
			break;
		case TAG_9F32_ISS_PK_EXP:
			Len = 0;
			break;
		*/
		case TAG_9F33_TRM_CAPABILITIES:
			if(sizeof(transData.T_9F33_TermCap) >= Len)
			{
				//more locations for update!
				EMVlog("Terminal capabilities updated");
				memcpy(transData.T_9F33_TermCap, value, Len);
				transData.T_DF61_Info_Received_Data[2] |= TRX_9F33_TERMCAP;
				memcpy(termData.TermCap, value, Len);
				termData.Info_Included_Data[0] |= EMV_CT_INPUT_TRM_CAPABILITIES;
				memcpy(cardData.T_9F33_TermCap, value, Len);
				cardData.T_DF61_Info_Received_Data[2] |= TRX_9F33_TERMCAP;
				memcpy(appData.App_TermCap, value, Len);
				appData.Info_Included_Data[2] |= EMV_CT_INPUT_APL_TERM_CAPS;
			}
			else
			{
				Len = 0;
			}
			break;
		case TAG_9F34_CVM_RESULTS:
			if(sizeof(transData.T_9F34_CVM_Res) >= Len)
			{
				memcpy(transData.T_9F34_CVM_Res, value, Len);
				transData.T_DF61_Info_Received_Data[2] |= TRX_9F34_CVM_RES;
			}
			else
				Len = 0;
			break;
		case TAG_9F35_TRM_TYPE:
			if(Len == 1)
			{
				EMVlog("Terminal Type updated");
				transData.T_9F35_TermTyp = value[0];
				termData.TermTyp = value[0];
				termData.Info_Included_Data[0] |= EMV_CT_INPUT_TRM_TYPE;
				appData.App_TermTyp = value[0];
				appData.Info_Included_Data[3] |= EMV_CT_INPUT_APL_TRM_TYPE;
				cardData.T_9F35_TermTyp = value[0];
				cardData.T_DF61_Info_Received_Data[2] |= TRX_9F35_TERMTYP;
			}
			else
			{
				Len = 0;
			}
			break;
		case TAG_9F36_ATC:
			if(sizeof(transData.T_9F36_ATC) >= Len)
			{
				memcpy(transData.T_9F36_ATC, value, Len);
				transData.T_DF61_Info_Received_Data[0] |= TRX_9F36_ATC;
			}
			else
				Len = 0;
			break;
		case TAG_9F37_UNPREDICTABLE_NB:
			if(sizeof(transData.T_9F37_RandomNumber) >= Len)
			{
				memcpy(transData.T_9F37_RandomNumber, value, Len);
				transData.T_DF61_Info_Received_Data[0] |= TRX_9F37_RANDOM_NUMBER;
			}
			else
				Len = 0;
			break;
		/*
		case TAG_9F38_PDOL:
			Len = 0;
			break;
		*/
		case TAG_9F39_POS_ENTRY_MODE:
			if(Len == 1)
			{
				transData.T_9F39_POSEntryMode = value[0];
				transData.T_DF61_Info_Received_Data[0] |= TRX_9F39_POS_ENTRY_MODE;
			}
			else
				Len = 0;
			break;
		case TAG_9F3A_AMNT_REF_CURRENCY:
			if(sizeof(payData.Amount) == Len)
			{
				memcpy(payData.Amount, value, Len);
				t_Info_Included_Data[1] |= INPUT_OFL_AMOUNT;
			}
			else
				Len = 0;
			break;
		/*
		case TAG_9F3B_APP_REF_CURRENCY:
			Len = 0;
			break;
		*/
		case TAG_9F3C_TRANS_REF_CURRENCY:
			if(sizeof(payData.CurrencyTrans) == Len)
			{
				memcpy(payData.CurrencyTrans, value, Len);
				t_Info_Included_Data[1] |= INPUT_OFL_AMOUNT_CURRENCY;
			}
			else
				Len = 0;
			break;
		case TAG_9F3D_TRANS_CURRENCY_EXP:
			if(Len == 1)
			{
				payData.ExpTrans = value[0];
				t_Info_Included_Data[1] |= INPUT_OFL_CUREXPONENT;
				termData.ExpTrans = value[0];
				termData.Info_Included_Data[0] |= EMV_CT_INPUT_TRM_EXP_CURRENCY;
			}
			else
				Len = 0;
			break;
		case TAG_9F40_ADD_TRM_CAP:
			if(sizeof(transData.T_9F40_AddTermCap) == Len)
			{
				memcpy(transData.T_9F40_AddTermCap, value, Len);
				transData.T_DF61_Info_Received_Data[5] |= TRX_9F40_TERMCAP;
				memcpy(termData.TermAddCap, value, Len);
				termData.Info_Included_Data[0] |= EMV_CT_INPUT_TRM_ADD_CAPS;
				memcpy(cardData.T_9F40_AddTermCap, value, Len);
				cardData.T_DF61_Info_Received_Data[5] |= TRX_9F40_TERMCAP;
				memcpy(appData.App_TermAddCap, value, Len);
				appData.Info_Included_Data[2] |= EMV_CT_INPUT_APL_ADD_TERM_CAPS;
			}
			else
			{
				Len = 0;
			}
			break;
		case TAG_9F41_TRANS_SEQUENCE_NB:
			//multiple locations
			if(sizeof(transData.T_9F41_TransCount) <= Len)
			{
				memcpy(transData.T_9F41_TransCount, value, Len);
				transData.T_DF61_Info_Received_Data[0] |= TRX_9F41_TRANSCOUNT;
				memcpy(payData.TransCount, value, Len);
				t_Info_Included_Data[0] |= INPUT_OFL_TXN_COUNTER;
			}
			else
			{
				Len = 0;
			}
			break;
//		case TAG_9F42_APP_CURRENCY_CODE:	//iq_audi_030217
//			if(sizeof(cardData.T_9F42_AppCurrencyCode) == Len)
//			{
//				memcpy(cardData.T_9F42_AppCurrencyCode, value, Len);
//				cardData.T_DF61_Info_Received_Data[5] |= TRX_9F42_APPCURRENCYCODE;
//			}
//			else
//				Len = 0;
//			break;
		case TAG_9F43_APP_REF_CURRCY_EXP:
			if(Len == 1)
			{
				payData.ExpTrans = value[0];
				t_Info_Included_Data[1] |= INPUT_OFL_CUREXPONENT;
			}
			else
				Len = 0;
			break;
		/*
		case TAG_9F44_APP_CURRENCY_EXP:
			Len = 0;
			break;
		*/
		case TAG_9F45_DATA_AUTHENT_CODE:
			if(sizeof(transData.T_9F45_DataAuthCode) == Len)
			{
				memcpy(transData.T_9F45_DataAuthCode, value, Len);
				transData.T_DF61_Info_Received_Data[4] |= TRX_9F45_DATAAUTHCODE;
			}
			else
				Len = 0;
			break;
		/*
		case TAG_9F46_ICC_PK_CERTIF:
			Len = 0;
			break;
		//case TAG_9F47_ICC_PK_EXP:
		//	Len = 0;
		//	break;
		case TAG_9F48_ICC_PK_REMAINDER:
			Len = 0;
			break;
		case TAG_9F49_DDOL:
			Len = 0;
			break;
		case TAG_9F4A_SDA_TAG_LIST:
			Len = 0;
			break;
		case TAG_9F4B_SDA_DATA:
			Len = 0;
			break;
		*/
		case TAG_9F4C_ICC_DYNAMIC_NB:
			if(sizeof(transData.T_9F4C_ICCDynNumber.iccRND) >= Len)
			{
				memcpy(transData.T_9F4C_ICCDynNumber.iccRND, value, Len);
				transData.T_9F4C_ICCDynNumber.iccRNDLen = Len;
				transData.T_DF61_Info_Received_Data[4] |= TRX_9F4C_ICCDYNNUMBER;
			}
			else
			{
				Len = 0;
			}
			break;
		case TAG_9F53_TRANS_CATEGORY_CODE:
			if(sizeof(transData.T_9F53_MC_CatCode) >= Len)
			{
				memcpy(transData.T_9F53_MC_CatCode, value, Len);
				transData.T_DF61_Info_Received_Data[6] |= TRX_9F53_MC_CATCODE;
				unsigned char tagVal[20];
				int copySize = tagToBuffer(TAG_9F53_TRANS_CATEGORY_CODE, value, Len, tagVal, sizeof(tagVal));
				if (copySize > 0)
				{
					if (copySize <= sizeof(appData.Additional_Tags_TRM)-AppliDataAddLen)
					{
						memcpy(appData.Additional_Tags_TRM+AppliDataAddLen, tagVal, copySize);
						AppliDataAddLen += copySize;
					}
				}
			}
			else
			{
				Len = 0;
			}
			break;
		/*
		case TAG_9F4E_TAC_MERCHANTLOC:
			Len = 0;
			break;
		case TAG_9F51_APP_CURRENCY_CODE:
			Len = 0;
			break;
		case TAG_9F52_DEFAULT_ACTION:
			Len = 0;
			break;
		case TAG_9F53_CONSECUTIVE_LIM_TRANS_INT:
			Len = 0;
			break;
		case TAG_9F54_CUMULATIVE_TRANS_LIMET:
			Len = 0;
			break;
		case TAG_9F56_ISS_AUTH_INCATOR:
			Len = 0;
			break;
		case TAG_9F5D_VISA_AOSA:
			Len = 0;
			break;
		case TAG_9F66_VISA_TTQ:
			Len = 0;
			break;
		case TAG_9F6C_VISA_CTQ:
			Len = 0;
			break;
		*/
		case TAG_DF21_TAC_ABLEHNUNG:
			if(sizeof(appData.TACDenial) == Len)
			{
				memcpy(appData.TACDenial, value, Len);
				appData.Info_Included_Data[1] |= EMV_CT_INPUT_APL_TAC_DENIAL;
				memcpy(transData.TACDenial, value, Len);
				transData.T_DF61_Info_Received_Data[4] |= TRX_TAC_DENIAL;
			}
			else
			{
				Len = 0;
			}
			break;
		case TAG_DF22_TAC_ONLINE:
			if(sizeof(appData.TACOnline) == Len)
			{
				memcpy(appData.TACOnline, value, Len);
				appData.Info_Included_Data[1] |= EMV_CT_INPUT_APL_TAC_ONLINE;
				memcpy(transData.TACOnline, value, Len);
				transData.T_DF61_Info_Received_Data[4] |= TRX_TAC_ONLINE;
			}
			else
			{
				Len = 0;
			}
			break;
		case TAG_DF23_TAC_DEFAULT:
			if(sizeof(appData.TACDefault) == Len)
			{
				memcpy(appData.TACDefault, value, Len);
				appData.Info_Included_Data[1] |= EMV_CT_INPUT_APL_TAC_DEFAULT;
				memcpy(transData.TACDefault, value, Len);
				transData.T_DF61_Info_Received_Data[4] |= TRX_TAC_DEFAULT;
			}
			else
			{
				Len = 0;
			}
			break;
		case TAG_DF27_DEFAULT_TDOL:
			if(EMV_ADK_MAX_LG_TDOL >= Len)
			{
				memcpy(appData.Default_TDOL.DOL, value, Len);
				appData.Info_Included_Data[1] |= EMV_CT_INPUT_APL_TDOL;
				appData.Default_TDOL.dollen = Len;
			}
			else
			{
				Len = 0;
			}
			break;
		case TAG_DF28_DEFAULT_DDOL:
			if(EMV_ADK_MAX_LG_DDOL >= Len)
			{
				memcpy(appData.Default_DDOL.DOL, value, Len);
				appData.Info_Included_Data[1] |= EMV_CT_INPUT_APL_DDOL;
				appData.Default_DDOL.dollen = Len;
			}
			else
			{
				Len = 0;
			}
			break;
		case TAG_DF40_FORCE_ONLINE:
			if(Len == 1)
			{
				payData.Force_Online = value[0];
				t_Info_Included_Data[1] |= INPUT_OFL_FORCE_ONLINE;
			}
			else
				Len = 0;
			break;
		case TAG_DF24_THRESHHOLD:
			if (Len == sizeof(appData.Threshhold))
			{
				memcpy(appData.Threshhold, value, Len);
				appData.Info_Included_Data[0] |= EMV_CT_INPUT_APL_THRESH;
			}
			else
			{
				Len = 0;
			}
			break;
		case TAG_DF25_MAXPERCENT_ONL:
			if (Len > 0)
			{
				appData.MaxTargetPercentage = *value;
				appData.Info_Included_Data[1] |= EMV_CT_INPUT_APL_MAXTARGET;
			}
			break;
		case TAG_DF26_PERCENT_ONL:
			if (Len > 0)
			{
				appData.TargetPercentage = *value;
				appData.Info_Included_Data[0] |= EMV_CT_INPUT_APL_TARGET;
			}
			break;
		case TAG_KEY_INDEX:
			if (Len == 1) capkData.Index= value[0];
			else Len = 0;
			break;
		case TAG_KEY_KEY:
			if (capkData.Key) delete [] capkData.Key;
			capkData.Key = new unsigned char[Len];
			memcpy(capkData.Key, value, Len);
			capkData.KeyLen = Len;
			break;
		case TAG_KEY_EXPONENT:
			if (Len == 1)
			{
				capkData.Exponent = value[0];
			}
			else if(Len == 3)
			{
				capkData.Exponent = 1;
			}
			else Len = 0;
			break;
		/*
		case TAG_KEY_EXPIRYDATE:
			Len = 0;
			break;
		*/
		case TAG_KEY_HASH:
			if (capkData.Hash) delete [] capkData.Hash;
			capkData.Hash = new unsigned char[Len];
			memcpy(capkData.Hash, value, Len);
			break;
		case TAG_KEY_RID:
			if (capkData.RID) delete [] capkData.RID;
			capkData.RID = new unsigned char[Len];
			memcpy(capkData.RID, value, Len);
			break;
		case TAG_DF20_ASI:
			if (Len == 1)
			{
				appData.ASI = *value;
				appData.Info_Included_Data[0] |= EMV_CT_INPUT_APL_ASI;
			}
			break;
		
/*		case TAG_DFCC21_EXTRA_PARAMS:
			g_logger.Log("Setting context specific parameter set: %s", value);
			g_CardAppConfig.SetCountryParams((const char *)value);
			break; */
		default:
			if (g_CardAppConfig.UpdateToken(tagNum, value, Len) < 0)
			{
				dlog_msg("Adding to local collection: %X", tagNum);
				if (PutTag(tagNum, value, Len) < 0)
				{
					Len = 0;
				}
			}
			break;

	}

	dlog_msg("Update TAG: %X, ret: %d", tagNum, Len);

	return Len;
}

/*
 * Gets tag, stored only by CardApp
 */
int cEMVcollection::GetTag(int tag, unsigned char * buffer, int * size)
{
	TAGS_COLLXN::const_iterator it = tags_collection.find(tag);
	if (it != tags_collection.end())
	{
		*size = it->second.size();
		memcpy(buffer, it->second.data(), *size);
		return 0;
	}
	return -1;
}

/*
 * Stores a tag, which cannot be added to EMV ADK
 */
int cEMVcollection::PutTag(int tag, const unsigned char * buffer, int size)
{
	if (size > 0)
	{
		std::string tmp(reinterpret_cast<const char *>(buffer), size);
		TAGS_COLLXN::iterator it = tags_collection.find(tag);
		if (it != tags_collection.end())
		{
			// It's there
			it->second.assign(tmp);
		}
		// not found, add now
		tags_collection.insert(make_pair(tag, tmp));
		return 0;
	}
	return -1;
}


/*
int cEMVcollection::SetTLVData(int tagNum, unsigned char *value, int Len)
{
	int iRet = 0;
	switch(tagNum)
	{
		case TAG_4F_APP_ID:
		case 0x4F00:
			break;
		case TAG_50_APP_LABEL:
		case 0x5000:
			break;
		case TAG_57_TRACK2_EQUIVALENT:
		case 0x5700:
			break;
		case TAG_5A_APP_PAN:
		case 0x5A00:
			break;
		case TAG_5F20_CARDHOLDER_NAME:
			break;
		case TAG_5F24_APP_EXP_DATE:
			break;
		case TAG_5F25_APP_EFF_DATE:
			break;
		case TAG_5F28_ISS_COUNTRY_CODE:
			break;
		case TAG_5F2A_TRANS_CURRENCY:
			break;
		case TAG_5F2D_LANGUAGE:
			break;
		case TAG_5F30_SERVICE_CODE:
			break;
		case TAG_5F34_PAN_SEQUENCE_NB:
			break;
		case TAG_5F36_TRANS_CURRENCY_EXP:
			break;
		case TAG_5F57_ACCOUNT_TYPE:
			break;
		case TAG_81_BIN_AMOUNT_AUTH:
		case 0x8100:
			break;
		case TAG_82_AIP:
		case 0x8200:
			break;
		case TAG_87_APP_PRIORITY_ID:
		case 0x8700:
			break;
		case TAG_8A_AUTH_RESP_CODE:
		case 0x8A00:
			break;
		case TAG_8C_CDOL_1:
		case 0x8C00:
			break;
		case TAG_8D_CDOL_2:
		case 0x8D00:
			break;
		case TAG_8E_CVM_LIST:
		case 0x8E00:
			break;
		case TAG_8F_CERTIF_AUTH_PK_ID:
		case 0x8F00:
			break;
		case TAG_90_ISS_PK_CERTIF:
		case 0x9000:
			break;
		case TAG_91_ISS_AUTH_DATA:
		case 0x9100:
			break;
		case TAG_92_ISS_PK_REMAINDER:
		case 0x9200:
			break;
		case TAG_93_SGND_STAT_APP_DATA:
		case 0x9300:
			break;
		case TAG_94_AFL:
		case 0x9400:
			break;
		case TAG_95_TVR:
		case 0x9500:
			break;
		case TAG_97_TDOL:
		case 0x9700:
			break;
		case TAG_98_TC_HASH_VALUE:
		case 0x9800:
			break;
		case TAG_99_TRANS_PIN_DATA:
		case 0x9900:
			break;
		case TAG_9A_TRANS_DATE:
		case 0x9A00:
			break;
		case TAG_9B_TSI:
		case 0x9B00:
			break;
		case TAG_9C_TRANS_TYPE:
		case 0x9C00:
			break;
		case TAG_9D_DDF_NAME:
		case 0x9D00:
			break;
		case TAG_9F01_ACQ_ID:
			break;
		case TAG_9F02_NUM_AMOUNT_AUTH:
			break;
		case TAG_9F04_BIN_AMOUNT_OTHER:
			break;
		case TAG_9F05_APP_DISCR_DATA:
			break;
		case TAG_9F06_AID:
			break;
		case TAG_9F07_APP_USAGE_CONTROL:
			break;
		case TAG_9F08_ICC_APP_VERSION_NB:
			break;
		case TAG_9F09_TRM_APP_VERSION_NB:
			break;
		case TAG_9F0B_CARDHOLDER_NAME_XT:
			break;
		case TAG_9F0D_IAC_DEFAULT:
			break;
		case TAG_9F0E_IAC_DENIAL:
			break;
		case TAG_9F0F_IAC_ONLINE:
			break;
		case TAG_9F10_ISS_APP_DATA:
			break;
		case TAG_9F11_ISS_CODE_TABLE_ID:
			break;
		case TAG_9F12_APP_PREFERRED_NAME:
			break;
		case TAG_9F13_LAST_ONLINE_ATC:
			break;
		case TAG_9F14_LO_OFFLINE_LIMIT:
			break;
		case TAG_9F15_MERCH_CATEG_CODE:
			break;
		case TAG_9F16_MERCHANT_ID:
			break;
		case TAG_9F17_PIN_TRIES_LEFT:
			break;
		case TAG_9F18_ISS_SCRIPT_ID:
			break;
		case TAG_9F1A_TRM_COUNTRY_CODE:
			break;
		case TAG_9F1B_TRM_FLOOR_LIMIT:
			break;
		case TAG_9F1C_TRM_ID:
			break;
		case TAG_9F1D_TRM_RISK_MNGT_DATA:
			break;
		case TAG_9F1E_IFD_SERIAL_NB:
			break;
		case TAG_9F1F_TRACK_1_DISCR_DATA:
			break;
		case TAG_9F20_TRACK_2_DISCR_DATA:
			break;
		case TAG_9F21_TRANS_TIME:
			break;
		case TAG_9F22_CERTIF_AUTH_PK_ID:
			break;
		case TAG_9F23_HI_OFFLINE_LIMIT:
			break;
		case TAG_9F26_APP_CRYPTOGRAM:
			break;
		case TAG_9F27_CRYPT_INFO_DATA:
			break;
		case TAG_9F2D_ICC_PIN_PK_CERTIF:
			break;
		case TAG_9F2E_ICC_PIN_PK_EXP:
			break;
		case TAG_9F2F_ICC_PIN_PK_REMAIN:
			break;
		case TAG_9F32_ISS_PK_EXP:
			break;
		case TAG_9F33_TRM_CAPABILITIES:
			break;
		case TAG_9F34_CVM_RESULTS:
			break;
		case TAG_9F35_TRM_TYPE:
			break;
		case TAG_9F36_ATC:
			break;
		case TAG_9F37_UNPREDICTABLE_NB:
			break;
		case TAG_9F38_PDOL:
			break;
		case TAG_9F39_POS_ENTRY_MODE:
			break;
		case TAG_9F3A_AMNT_REF_CURRENCY:
			break;
		case TAG_9F3B_APP_REF_CURRENCY:
			break;
		case TAG_9F3C_TRANS_REF_CURRENCY:
			break;
		case TAG_9F3D_TRANS_CURRENCY_EXP:
			break;
		case TAG_9F40_ADD_TRM_CAP:
			break;
		case TAG_9F41_TRANS_SEQUENCE_NB:
			break;
		case TAG_9F42_APP_CURRENCY_CODE:
			break;
		case TAG_9F43_APP_REF_CURRCY_EXP:
			break;
		case TAG_9F44_APP_CURRENCY_EXP:
			break;
		case TAG_9F45_DATA_AUTHENT_CODE:
			break;
		case TAG_9F46_ICC_PK_CERTIF:
			break;
		case TAG_9F47_ICC_PK_EXP:
			break;
		case TAG_9F48_ICC_PK_REMAINDER:
			break;
		case TAG_9F49_DDOL:
			break;
		case TAG_9F4A_SDA_TAG_LIST:
			break;
		case TAG_9F4B_SDA_DATA:
			break;
		case TAG_9F4C_ICC_DYNAMIC_NB:
			break;
		case TAG_9F4E_TAC_MERCHANTLOC:
			break;
		case TAG_9F51_APP_CURRENCY_CODE:
			break;
		case TAG_9F52_DEFAULT_ACTION:
			break;
		case TAG_9F53_CONSECUTIVE_LIM_TRANS_INT:
			break;
		case TAG_9F54_CUMULATIVE_TRANS_LIMET:
			break;
		case TAG_9F56_ISS_AUTH_INCATOR:
			break;
		case TAG_9F5D_VISA_AOSA:
			break;
		case TAG_9F66_VISA_TTQ:
			break;
		case TAG_9F6C_VISA_CTQ:
			break;
		default:
			break;

	}

	return iRet;
}
*/


/*empty template with all TLV tags...
int cEMVcollection::
{
	int iRet = 0;
	switch(tagNum)
	{
		case TAG_4F_APP_ID:
		case 0x4F00:
			break;
		case TAG_50_APP_LABEL:
		case 0x5000:
			break;
		case TAG_57_TRACK2_EQUIVALENT:
		case 0x5700:
			break;
		case TAG_5A_APP_PAN:
		case 0x5A00:
			break;
		case TAG_5F20_CARDHOLDER_NAME:
			break;
		case TAG_5F24_APP_EXP_DATE:
			break;
		case TAG_5F25_APP_EFF_DATE:
			break;
		case TAG_5F28_ISS_COUNTRY_CODE:
			break;
		case TAG_5F2A_TRANS_CURRENCY:
			break;
		case TAG_5F2D_LANGUAGE:
			break;
		case TAG_5F30_SERVICE_CODE:
			break;
		case TAG_5F34_PAN_SEQUENCE_NB:
			break;
		case TAG_5F36_TRANS_CURRENCY_EXP:
			break;
		case TAG_5F57_ACCOUNT_TYPE:
			break;
		case TAG_81_BIN_AMOUNT_AUTH:
		case 0x8100:
			break;
		case TAG_82_AIP:
		case 0x8200:
			break;
		case TAG_87_APP_PRIORITY_ID:
		case 0x8700:
			break;
		case TAG_8A_AUTH_RESP_CODE:
		case 0x8A00:
			break;
		case TAG_8C_CDOL_1:
		case 0x8C00:
			break;
		case TAG_8D_CDOL_2:
		case 0x8D00:
			break;
		case TAG_8E_CVM_LIST:
		case 0x8E00:
			break;
		case TAG_8F_CERTIF_AUTH_PK_ID:
		case 0x8F00:
			break;
		case TAG_90_ISS_PK_CERTIF:
		case 0x9000:
			break;
		case TAG_91_ISS_AUTH_DATA:
		case 0x9100:
			break;
		case TAG_92_ISS_PK_REMAINDER:
		case 0x9200:
			break;
		case TAG_93_SGND_STAT_APP_DATA:
		case 0x9300:
			break;
		case TAG_94_AFL:
		case 0x9400:
			break;
		case TAG_95_TVR:
		case 0x9500:
			break;
		case TAG_97_TDOL:
		case 0x9700:
			break;
		case TAG_98_TC_HASH_VALUE:
		case 0x9800:
			break;
		case TAG_99_TRANS_PIN_DATA:
		case 0x9900:
			break;
		case TAG_9A_TRANS_DATE:
		case 0x9A00:
			break;
		case TAG_9B_TSI:
		case 0x9B00:
			break;
		case TAG_9C_TRANS_TYPE:
		case 0x9C00:
			break;
		case TAG_9D_DDF_NAME:
		case 0x9D00:
			break;
		case TAG_9F01_ACQ_ID:
			break;
		case TAG_9F02_NUM_AMOUNT_AUTH:
			break;
		case TAG_9F04_BIN_AMOUNT_OTHER:
			break;
		case TAG_9F05_APP_DISCR_DATA:
			break;
		case TAG_9F06_AID:
			break;
		case TAG_9F07_APP_USAGE_CONTROL:
			break;
		case TAG_9F08_ICC_APP_VERSION_NB:
			break;
		case TAG_9F09_TRM_APP_VERSION_NB:
			break;
		case TAG_9F0B_CARDHOLDER_NAME_XT:
			break;
		case TAG_9F0D_IAC_DEFAULT:
			break;
		case TAG_9F0E_IAC_DENIAL:
			break;
		case TAG_9F0F_IAC_ONLINE:
			break;
		case TAG_9F10_ISS_APP_DATA:
			break;
		case TAG_9F11_ISS_CODE_TABLE_ID:
			break;
		case TAG_9F12_APP_PREFERRED_NAME:
			break;
		case TAG_9F13_LAST_ONLINE_ATC:
			break;
		case TAG_9F14_LO_OFFLINE_LIMIT:
			break;
		case TAG_9F15_MERCH_CATEG_CODE:
			break;
		case TAG_9F16_MERCHANT_ID:
			break;
		case TAG_9F17_PIN_TRIES_LEFT:
			break;
		case TAG_9F18_ISS_SCRIPT_ID:
			break;
		case TAG_9F1A_TRM_COUNTRY_CODE:
			break;
		case TAG_9F1B_TRM_FLOOR_LIMIT:
			break;
		case TAG_9F1C_TRM_ID:
			break;
		case TAG_9F1D_TRM_RISK_MNGT_DATA:
			break;
		case TAG_9F1E_IFD_SERIAL_NB:
			break;
		case TAG_9F1F_TRACK_1_DISCR_DATA:
			break;
		case TAG_9F20_TRACK_2_DISCR_DATA:
			break;
		case TAG_9F21_TRANS_TIME:
			break;
		case TAG_9F22_CERTIF_AUTH_PK_ID:
			break;
		case TAG_9F23_HI_OFFLINE_LIMIT:
			break;
		case TAG_9F26_APP_CRYPTOGRAM:
			break;
		case TAG_9F27_CRYPT_INFO_DATA:
			break;
		case TAG_9F2D_ICC_PIN_PK_CERTIF:
			break;
		case TAG_9F2E_ICC_PIN_PK_EXP:
			break;
		case TAG_9F2F_ICC_PIN_PK_REMAIN:
			break;
		case TAG_9F32_ISS_PK_EXP:
			break;
		case TAG_9F33_TRM_CAPABILITIES:
			break;
		case TAG_9F34_CVM_RESULTS:
			break;
		case TAG_9F35_TRM_TYPE:
			break;
		case TAG_9F36_ATC:
			break;
		case TAG_9F37_UNPREDICTABLE_NB:
			break;
		case TAG_9F38_PDOL:
			break;
		case TAG_9F39_POS_ENTRY_MODE:
			break;
		case TAG_9F3A_AMNT_REF_CURRENCY:
			break;
		case TAG_9F3B_APP_REF_CURRENCY:
			break;
		case TAG_9F3C_TRANS_REF_CURRENCY:
			break;
		case TAG_9F3D_TRANS_CURRENCY_EXP:
			break;
		case TAG_9F40_ADD_TRM_CAP:
			break;
		case TAG_9F41_TRANS_SEQUENCE_NB:
			break;
		case TAG_9F42_APP_CURRENCY_CODE:
			break;
		case TAG_9F43_APP_REF_CURRCY_EXP:
			break;
		case TAG_9F44_APP_CURRENCY_EXP:
			break;
		case TAG_9F45_DATA_AUTHENT_CODE:
			break;
		case TAG_9F46_ICC_PK_CERTIF:
			break;
		case TAG_9F47_ICC_PK_EXP:
			break;
		case TAG_9F48_ICC_PK_REMAINDER:
			break;
		case TAG_9F49_DDOL:
			break;
		case TAG_9F4A_SDA_TAG_LIST:
			break;
		case TAG_9F4B_SDA_DATA:
			break;
		case TAG_9F4C_ICC_DYNAMIC_NB:
			break;
		case TAG_9F4E_TAC_MERCHANTLOC:
			break;
		case TAG_9F51_APP_CURRENCY_CODE:
			break;
		case TAG_9F52_DEFAULT_ACTION:
			break;
		case TAG_9F53_CONSECUTIVE_LIM_TRANS_INT:
			break;
		case TAG_9F54_CUMULATIVE_TRANS_LIMET:
			break;
		case TAG_9F56_ISS_AUTH_INCATOR:
			break;
		case TAG_9F5D_VISA_AOSA:
			break;
		case TAG_9F66_VISA_TTQ:
			break;
		case TAG_9F6C_VISA_CTQ:
			break;
		default:
			break;

	}

	return iRet;
}
*/

/*
int SetTLVData(int tagNum, unsigned char *value, int Len)
{
	int iRet = 0;

	return iRet;
}
*/

//adding primitive TLV objects to data collection

#define NUMERIC_AMT_SIZE 6
#define BINARY_AMT_SIZE 6
#define TVR_SIZE 5
#define TRAN_TYPE_SIZE 2

int checkPrimitiveTLV(int tagNum, unsigned char * value, int Len)
{

	//dlog_msg("checkPrimitiveTLV: tag: %X, len: %d", tagNum, Len);

    unsigned short Ret = 0;


	if((tagNum == TAG_95_TVR) || (tagNum == 0x9500))
	{
		g_EMVcollection.SetTLVData(tagNum, value, Len);
	}
	else if((tagNum == TAG_9B_TSI) || (tagNum == 0x9B00))
	{
		g_EMVcollection.SetTLVData(tagNum, value, Len);
	}
	else if(tagNum == TAG_9F02_NUM_AMOUNT_AUTH)
	{
		//we must add binary representation of numeric value
		char amtDsp[NUMERIC_AMT_SIZE*2 + 1];
		long amtBin;
		//SVC_HEX_2_DSP((const char *)value, amtDsp, NUMERIC_AMT_SIZE);
		com_verifone_pml::svcHex2Dsp((const char *)value, NUMERIC_AMT_SIZE, amtDsp, NUMERIC_AMT_SIZE*2 + 1);
		amtDsp[NUMERIC_AMT_SIZE*2] = 0;
		amtBin = atol(amtDsp);
		if((Ret = g_EMVcollection.SetTLVData(TAG_81_BIN_AMOUNT_AUTH, (unsigned char *)&amtBin, BINARY_AMT_SIZE)))
		{
			dlog_msg("TAG %X added with value: %X", TAG_81_BIN_AMOUNT_AUTH, amtBin);
			Ret = g_EMVcollection.SetTLVData(tagNum, value, Len);
		}
	}
	else if(tagNum == TAG_9F03_NUM_AMOUNT_OTHER)
	{
		//we must add binary representation of numeric value
		char amtDsp[NUMERIC_AMT_SIZE*2 + 1];
		long amtBin;
		//SVC_HEX_2_DSP((const char *)value, amtDsp, NUMERIC_AMT_SIZE);
		com_verifone_pml::svcHex2Dsp((const char *)value, NUMERIC_AMT_SIZE, amtDsp, NUMERIC_AMT_SIZE*2 + 1);
		amtDsp[NUMERIC_AMT_SIZE*2] = 0;
		amtBin = atol(amtDsp);
		if((Ret = g_EMVcollection.SetTLVData(TAG_9F04_BIN_AMOUNT_OTHER, (unsigned char *)&amtBin, BINARY_AMT_SIZE)))
		{
			dlog_msg("TAG %X added with value: %X", TAG_9F04_BIN_AMOUNT_OTHER, amtBin);
			Ret = g_EMVcollection.SetTLVData(tagNum, value, Len);
		}
	}
	else if(tagNum == TAG_9F1B_TRM_FLOOR_LIMIT)
	{
		// We must set floor limit using SetROSParams() as well
		char floor[sizeof(long)*2 + 1];
		long floorBin;
		//assert(Len == sizeof(floorBin));
		//SVC_HEX_2_DSP((const char *)value, floor, Len);
		com_verifone_pml::svcHex2Dsp((const char *)value, Len, floor, sizeof(long)*2 + 1);
		floor[Len*2] = 0;
		//thresholdBin = atol(floor);
		sscanf(floor, "%x", &floorBin);
		dlog_msg("Updated floor limit with value: %d", floorBin);
		Ret = g_EMVcollection.SetTLVData(tagNum, value, Len);
	}
	else if((tagNum == 0xDFDF06) || (tagNum == TAG_DFAF17_TAC_DEFAULT)) 	//TAC default
	{
		Ret = g_EMVcollection.SetTLVData(TAG_DF23_TAC_DEFAULT, value, Len);
	}
	else if((tagNum == 0xDFDF07) || (tagNum == TAG_DFAF18_TAC_DENIAL)) 	//TAC denial
	{
		Ret = g_EMVcollection.SetTLVData(TAG_DF21_TAC_ABLEHNUNG, value, Len);
	}
	else if((tagNum == 0xDFDF08) || (tagNum == TAG_DFAF19_TAC_ONLINE)) 	//TAC online
	{
		Ret = g_EMVcollection.SetTLVData(TAG_DF22_TAC_ONLINE, value, Len);
	}
	else if(tagNum == TAG_DFAF1A_DEFAULT_TDOL) 	//default TDOL
	{
		Ret = g_EMVcollection.SetTLVData(TAG_DF27_DEFAULT_TDOL, value, Len);
	}
	else if(tagNum == TAG_DFAF1B_DEFAULT_DDOL) 	//default DDOL
	{
		Ret = g_EMVcollection.SetTLVData(TAG_DF28_DEFAULT_DDOL, value, Len);
	}
 	else if(tagNum == 0xDFDF0D) 	//force on line
 	{
		char num[sizeof(int)*2 + 1];
		int trmdec = 0;
		//assert(Len == sizeof(num));
		com_verifone_pml::svcHex2Dsp((const char *)value, Len, num, sizeof(num));
		num[Len*2] = 0;
		if (sscanf(num, "%d", &trmdec) == 1)
		{
			if (trmdec == 1) Ret = g_EMVcollection.SetTLVData(TAG_DF40_FORCE_ONLINE, value, Len);
			else if (trmdec == 2) g_CEMV.SetTermDecision(FORCED_DECLINE);
		}
		dlog_msg("Transaction forced online: (%d)!", trmdec);
	}
	else if((tagNum == 0xDFDF0A) || (tagNum == TAG_DFAF14_RND_SEL_THRESHOLD))	//ROS - Threshold
	{
		char threshold[sizeof(long)*2 + 1];
		long thresholdBin;
		//SVC_HEX_2_DSP((const char *)value, threshold, Len);
		com_verifone_pml::svcHex2Dsp((const char *)value, Len, threshold, sizeof(long)*2 + 1);
		threshold[Len*2] = 0;
		sscanf(threshold, "%x", &thresholdBin);
		dlog_msg("Set ROS Threshold to %ld (size %d)", thresholdBin, Len);
		//Ret = g_EMVcollection.SetTLVData(TAG_DF24_THRESHHOLD, (unsigned char *)&thresholdBin, sizeof(thresholdBin));
		Ret = g_EMVcollection.SetTLVData(TAG_DF24_THRESHHOLD, value, Len);
	}
	else if((tagNum == 0xDFDF0B) || (tagNum == TAG_DFAF15_RND_SEL_PERCENTAGE))	//ROS - Target RS Percent
	{
		char rsTarget[sizeof(long)*2 + 1];
		unsigned char rsTargetBin;
		//assert(Len == sizeof(rsTargetBin));
		//SVC_HEX_2_DSP((const char *)value, rsTarget, Len);
		com_verifone_pml::svcHex2Dsp((const char *)value, Len, rsTarget, sizeof(long)*2 + 1);
		rsTarget[Len*2] = 0;
		//rsTargetBin = atol(rsTarget);
		sscanf(rsTarget, "%x", &rsTargetBin);
		dlog_msg("Set RS Target Percent to %d (size %d)", rsTargetBin, Len);
		Ret = g_EMVcollection.SetTLVData(TAG_DF26_PERCENT_ONL, value, Len);
	}
	else if((tagNum == 0xDFDF0C) || (tagNum == TAG_DFAF16_RND_MAX_RND_PERCENTAGE)) 	//ROS - Max RS Percent
	{
		char rsMaxPercent[sizeof(long)*2 + 1];
		unsigned char rsMaxPercentBin;
		//assert(Len == sizeof(rsMaxPercent));
		//SVC_HEX_2_DSP((const char *)value, rsMaxPercent, Len);
		com_verifone_pml::svcHex2Dsp((const char *)value, Len, rsMaxPercent, sizeof(long)*2 + 1);
		rsMaxPercent[Len*2] = 0;
		//rsMaxPercentBin = atol(rsMaxPercent);
		sscanf(rsMaxPercent, "%x", &rsMaxPercentBin);
		Ret = g_EMVcollection.SetTLVData(TAG_DF25_MAXPERCENT_ONL, value, Len);
	}
	else if(tagNum == TAG_DFAF12_SECOND_AVN) 	//second AVN
	{
		if(Len <= MAX_AVN_LEN)
		{
			g_CardAppConfig.SetSecondAVN(value);
			dlog_msg("Second AVN set: %X%X", value[0], value[1]);
		}
		g_EMVcollection.SetTLVData(TAG_DFAF12_SECOND_AVN, value, Len);
	}
	else if(tagNum == TAG_DFAF1D_SELECTION) 	//automatic selection
	{
		g_CardAppConfig.SetAutoFlag(value[0]);
		dlog_msg("Automatic Selection set: %X", value[0]);
	}
	else if (tagNum == TAG_9F22_CERTIF_AUTH_PK_ID || tagNum == TAG_KEY_INDEX)
	{
		g_EMVcollection.SetTLVData(TAG_KEY_INDEX, value, Len);
	}
	else if (tagNum == 0xDFAF0E || tagNum == TAG_KEY_KEY)
	{
		g_EMVcollection.SetTLVData(TAG_KEY_KEY, value, Len);
	}
	else if (tagNum == 0xDFAF0F || tagNum == TAG_KEY_EXPONENT)
	{
		g_EMVcollection.SetTLVData(TAG_KEY_EXPONENT, value, Len);
	}
//	else if (tagNum == 0xDFAF1F || tagNum == TAG_KEY_EXPIRYDATE)		//iq_audi_020317 no part of latest ADK TAG_KEY_EXPIRYDATE
//	{
//		// unhexlify this
//		g_EMVcollection.SetTLVData(TAG_KEY_EXPIRYDATE, value, Len);
//	}
	else if (tagNum == 0xDFAF10 || tagNum == TAG_KEY_HASH)
	{
		g_EMVcollection.SetTLVData(TAG_KEY_HASH, value, Len);
	}
	else if (tagNum == TAG_KEY_RID) // our tag for RID doesn't exist...
	{
		g_EMVcollection.SetTLVData(TAG_KEY_RID, value, Len);
	}
	else if(tagNum == 0xDFAF11) // Partial selection
	{
		g_EMVcollection.SetTLVData(TAG_DF20_ASI, value, Len);
	}
	else
	{
		Ret = g_EMVcollection.SetTLVData(tagNum, value, Len);
	}

	return Ret;

}


int getTLV(int tagNum, unsigned char *tempTLV, unsigned short *TLVlen)
{
    dlog_msg("Processing TAG: %X", tagNum);
	g_logger.Log("getTLV: Processing TAG: 0x%X", tagNum);
    int tmpLen = 0;

    if(tagNum == TAG_95_TVR)
    {
        tmpLen = g_EMVcollection.GetTLVData(TAG_95_TVR, tempTLV, &tmpLen);
        tmpLen = TVR_SIZE;
        dlog_msg("processing TVR: %02X%02X%02X%02X%02X", tempTLV[0],tempTLV[1],tempTLV[2],tempTLV[3],tempTLV[4]);
    }
        else if(tagNum == TAG_9B_TSI)
        {
            tmpLen = g_EMVcollection.GetTLVData(TAG_9B_TSI, tempTLV, &tmpLen);
            dlog_msg("processing TSI: %02X%02X", tempTLV[0],tempTLV[1]);
        }
        else if((tagNum == 0xDFDF06) || (tagNum == TAG_DFAF17_TAC_DEFAULT))     //TAC default
        {
            tmpLen = g_EMVcollection.GetTLVData(TAG_DF23_TAC_DEFAULT, tempTLV, &tmpLen);
            dlog_msg("adding TAC default");
        }
        else if((tagNum == 0xDFDF07) || (tagNum == TAG_DFAF18_TAC_DENIAL))     //TAC denial
        {
            tmpLen = g_EMVcollection.GetTLVData(TAG_DF21_TAC_ABLEHNUNG, tempTLV, &tmpLen);
            dlog_msg("adding TAC denial");
        }
        else if((tagNum == 0xDFDF08) || (tagNum == TAG_DFAF19_TAC_ONLINE))     //TAC online
        {
            tmpLen = g_EMVcollection.GetTLVData(TAG_DF22_TAC_ONLINE, tempTLV, &tmpLen);
            dlog_msg("adding TAC online");
        }
        else if(tagNum == 0xDFDF09)     //Kernel - Last SW1SW2 from the card
        {
            tmpLen = g_EMVcollection.GetTLVData(TAG_DF6F_CBK_PIN_ICC_RESP, tempTLV, &tmpLen);
            dlog_msg("adding last SW1SW2 from ICC");
        }
        else if((tagNum == 0xDFDF0A) || (tagNum == TAG_DFAF14_RND_SEL_THRESHOLD))     //ROS - Threshold
        {
            tmpLen = g_EMVcollection.GetTLVData(TAG_DF24_THRESHHOLD, tempTLV, &tmpLen);
           dlog_msg("Adding ROS Threshold");
        }
        else if((tagNum == 0xDFDF0B) || (tagNum == TAG_DFAF15_RND_SEL_PERCENTAGE))     //ROS - Target RS Percent
        {
            tmpLen = g_EMVcollection.GetTLVData(TAG_DF26_PERCENT_ONL, tempTLV, &tmpLen);
            dlog_msg("Adding RS Percent");
        }
        else if((tagNum == 0xDFDF0C) || (tagNum == TAG_DFAF16_RND_MAX_RND_PERCENTAGE))    //ROS - Max RS Percent
        {
            tmpLen = g_EMVcollection.GetTLVData(TAG_DF25_MAXPERCENT_ONL, tempTLV, &tmpLen);
            dlog_msg("Adding RS Max Percent");
        }
        else if(tagNum == 0xDFDF29)     //Script result 71
        {
            tmpLen = g_CEMV.GetScriptResult71(tempTLV);
            dlog_msg("Adding Script Result 71");
        }
        else if(tagNum == 0xDFDF2A)     //Script result 72
        {
            tmpLen = g_CEMV.GetScriptResult72(tempTLV);
            dlog_msg("Adding Script Result 72");
        }
        else if(tagNum == 0xDFDF25)     //Reason Online
        {
            unsigned char ro = g_CEMV.GetReasonOnline();
            tmpLen = sizeof(ro);
            memcpy(tempTLV, &ro, tmpLen);
            dlog_msg("Adding Reason Online (%d)", ro);
        }
        else if(tagNum == 0xDFDF04) // EMV Kernel Version
        {
            tmpLen = g_EMVcollection.GetTLVData(TAG_DF11_LIB_VERSION, tempTLV, &tmpLen);
            dlog_msg("Adding EMV Kernel version '%s'", tempTLV);
        }
        else if(tagNum == 0xDFDF05) // EMV Kernel Checksums (read from MVT.DAT!!!!)
        {
            // Kamil_P1: Disabled, EMV ADK doesn't set DF12 yet
            #if 0
            if (g_CEMVtrns.Is_Kernel_Opened())
            {
                tmpLen = g_EMVcollection.GetTLVData(TAG_DF12_CHECKSUM, tempTLV, &tmpLen);
            }
            else
            #else
            bool canFetch = true;
            bool kernelOpened = g_CEMVtrns.Is_Kernel_Opened();
            if (!kernelOpened)
            {
                if (g_CEMVtrns.Open_Kernel() != EMV_ADK_OK)
                {
                    dlog_error("Cannot open the kernel!");
                    g_CEMVtrns.Close_Kernel();
                    canFetch = false;
                }
            }
            if (canFetch)
            #endif
            {
                std::string AID = g_CardAppConfig.GetCurrentAID();
                EMV_ADK_INFO ADK_ret;
                if (AID.size())
                {
                    EMV_CT_APPLI_TYPE pxAID;
                    pxAID.aidlen = com_verifone_pml::svcDsp2Hex(AID.data(), AID.size(), reinterpret_cast<char *>(pxAID.AID), sizeof(pxAID.AID));
                    ADK_ret = EMV_CT_GetAppliData(EMV_ADK_READ_AID, &pxAID, &g_EMVcollection.appData);
                    if (ADK_ret == EMV_ADK_OK)
                    {
                        tmpLen = EMV_ADK_CHECKSUM_ASCII_SIZE;
                        memcpy(tempTLV, g_EMVcollection.appData.ChksumASCIIEMVCO, tmpLen);
                    }
                }
                else
                {
                    // Iterate through all AIDs
                    using namespace com_verifone_TLVLite;
                    SafeBuffer buf(tempTLV, CARD_APP_DEF_MAX_TLV_LEN);
                    EMV_CT_APPLI_TYPE pxAID;
                    ADK_ret = EMV_CT_GetAppliData(EMV_ADK_READ_FIRST, &pxAID, &g_EMVcollection.appData);
                    while (ADK_ret == EMV_ADK_OK)
                    {
                        #ifdef LOGSYS_FLAG
                        char cAID[AID_SIZE*2+1];
                        int cAIDLen = com_verifone_pml::svcHex2Dsp(reinterpret_cast<char *>(&pxAID.AID[0]), pxAID.aidlen, cAID, sizeof(cAID));

                        dlog_msg("AID %s, cksum %s", cAID, g_EMVcollection.appData.ChksumASCIIEMVCO);
                        #endif
                        buf.append(TagLenVal(ConstData(arbyAppAID, APP_AID_TS),
                                ConstData(pxAID.AID, pxAID.aidlen)));
                        buf.append(TagLenVal(ConstData(arbyTagKernelChecksum, KERNEL_CHECKSUM_TS),
                                ConstData(g_EMVcollection.appData.ChksumASCIIEMVCO, EMV_ADK_CHECKSUM_ASCII_SIZE)));
                        ADK_ret = EMV_CT_GetAppliData(EMV_ADK_READ_NEXT, &pxAID, &g_EMVcollection.appData);
                    }
                    if (!buf.isOverflow()) tmpLen = buf.getLength();
                }
            }
            #if 1
            if (!kernelOpened) g_CEMVtrns.Close_Kernel();
            #endif
            if (tmpLen) dlog_msg("Adding Kernel checksum '%s'", tempTLV);
            else dlog_alert("Cannot get Kernel checksum!");
        }
        else
        {
            tmpLen = 0;
            if (!isTagBlacklisted(tagNum))
            {
                g_EMVcollection.GetTLVData(tagNum, tempTLV, &tmpLen);
            }
       }
        *TLVlen = (unsigned short)tmpLen;
        return tmpLen;
}

int parseInTLV(unsigned char *buffer, int buflen, bool permanent)
{
    int numObj = 0;
    unsigned char Tag[3];
    int tagNum;
    int Len;
    unsigned char *current;
    unsigned char *script;
    int i;
    int proc_len=0;
    unsigned short Ret = 0;
    cUpdateCfg UpdateCfg;
	static std::string AID;
	static std::string priority_indicator;

    if(permanent)
    {
        g_EMVcollection.Clear(); // make sure data is cleared
        UpdateCfg.locateConfigRecords(g_CardAppConfig.GetCurrentAID().c_str());
    }

    current = buffer;
    dlog_msg("TLV Input data len: %d", buflen);
    while(buflen > proc_len)
    {
        //dlog_msg("Processing %d/%d", proc_len, buflen);
        Tag[0] = current[0];
        script = current;
        current++;
        proc_len++;

        // Skip fillers (bytes 0x00 and 0xFF)
        if(Tag[0] == 0x00 || Tag[0] == 0xFF)
        {
            continue;
        }
        if((Tag[0] & 0x1F) == 0x1F) //more than 1 byte tag
        {
            Tag[1] = current[0];
            current++;
            proc_len++;
        }
        else
        {
            Tag[1] = 0x00;
        }

        //if((Tag[1] & 0x7F) != Tag[1]) //more than 2 byte tag
        if(Tag[1] & 0x80) //more than 2 byte tag
        {
            Tag[2] = current[0];
            current++;
            proc_len++;
        }
        else
        {
            Tag[2] = 0x00;
        }

        if(Tag[2] > 0)
        {
            tagNum = Tag[0]*0x10000 + Tag[1]*0x100 + Tag[2];
        }
        else
        {
            tagNum = Tag[0]*0x100 + Tag[1];
        }
        //dlog_msg("Processing TAG: %0X", tagNum);
        Len = (int)current[0];
        current++;
        proc_len++;
        if(Len & 0x80)
        {
            //well... we have more than one byte coding len
            i = Len & 0x7F;
            Len = 0;
            while(i)
            {
                // Len+=(int)current[0]*pow((double)0x10, i-1);
                Len += ((int)current[0]) << (8*(i-1));
                i--;
                current++;
                proc_len++;
            }
        }

		//CHECKING CORECTNESS OF DATA PARSING
		if(buflen - (current - buffer) < Len)
		{
			dlog_msg("Input data error, dropping further parsing...(remaining len: %d current object len: %d)", buflen - (current - buffer), Len);
			return -1;
		}
		
        if(Tag[0] & 0x20)   //constructed
        {
            if(tagNum == TAG_71_ISS_SCRIPT_TPLT_1 || tagNum == TAG_72_ISS_SCRIPT_TPLT_2
                || tagNum == 0x7100 || tagNum == 0x7200)
            {
            	if(Len > 0)
            	{
	                //scripts
	                dlog_msg("Script type: %X", Tag[0]);

	                if (tagNum == TAG_71_ISS_SCRIPT_TPLT_1 || tagNum == 0x7100)
	                {
	                    g_CEMVtrns.script71 = script;
	                    g_CEMVtrns.script71_len = Len+(current - script);
	                    g_EMVcollection.AssignScript71(script,  Len+(current - script));
	                }
	                else if (tagNum == TAG_72_ISS_SCRIPT_TPLT_2 || tagNum == 0x7200)
	                {
	                    g_CEMVtrns.script72 = script;
	                    g_CEMVtrns.script72_len = Len+(current - script);
	                    g_EMVcollection.AssignScript72(script, Len+(current - script));
	                }
            	}
				else
				{
					dlog_msg("Ignorring 0 length scripts...");
				}
            }
            else
            {
                //any other
                g_logger.Log("TLV constructed: %X Len: %d", tagNum, Len);
                parseInTLV(current, Len, false);
            }
        }
        else                //primitive
        {
            numObj++;
            #if 0
            if(permanent)
            {
                Ret = UpdateCfg.updateConfig(tagNum, current, Len);
            }
            else
            {
                Ret = checkPrimitiveTLV(tagNum, current, Len);
            }
            //dlog_msg("TLV primitive: %X Len: %d added: %d", tagNum, Len, Ret);
            #endif
            Ret = checkPrimitiveTLV(tagNum, current, Len);
			if ((tagNum == TAG_84_DF_NAME) || (tagNum == 0x8400) || (tagNum == TAG_4F_APP_ID) || (tagNum == 0x4F00))
			{
				AID.assign((char *)current, Len);
				dlog_msg("Adding AID to list: %.02X%.02X%.02X%.02X%.02X%.02X%.02X", current[0], current[1], current[2], current[3], current[4], current[5], current[6]);
			}
			if ((tagNum == TAG_87_APP_PRIORITY_ID) || (tagNum == 0x8700))
			{
				priority_indicator.assign((char *)current, Len);
				g_EMVcollection.AID_priority[AID] = priority_indicator;
				dlog_msg("Adding priority indicator: %X", current[0]);
			}
            dlog_msg("TLV primitive: %X Len: %d added: %d", tagNum, Len, Ret);
        }
        current+=Len;
        proc_len+=Len;
    }
    if(permanent && numObj > 0)
    {
        UpdateCfg.commitChanges();
    }
    else
    {
        /*
        std::string AID;
        EMV_CT_APPLI_TYPE pxAID;
        AID = g_CardAppConfig.GetCurrentAID();
	if(AID.size() > 0)
	{
		pxAID.aidlen = com_verifone_pml::svcDsp2Hex(AID.data(), AID.size(), reinterpret_cast<char *>(pxAID.AID), sizeof(pxAID.AID));
                EMVlog("EMV_CT_SetAppliData");
                dlog_msg("EMV_CT_SetAppliData");
                EMV_ADK_INFO err = EMV_CT_SetAppliData(EMV_ADK_TEMP_UPDATE, &pxAID, &g_EMVcollection.appData);
                EMVlog((char *)ADK_err(err).c_str());
                dlog_msg((char *)ADK_err(err).c_str());
        }
        else
        {
                dlog_error("Cannot determine AID!");
        }
        */
        EMVlog("EMV_CT_SetAppliData");
        dlog_msg("EMV_CT_SetAppliData");
        EMV_ADK_INFO err = EMV_CT_SetAppliData(EMV_ADK_TEMP_UPDATE, NULL, &g_EMVcollection.appData);
        EMVlog((char *)ADK_err(err).c_str());
        dlog_msg((char *)ADK_err(err).c_str());
    }
    return numObj;
}

bool isTagBlacklisted(int tagNum)
{
    unsigned short B_LIST[] = { TAG_99_TRANS_PIN_DATA };
    for (int i = 0; i < sizeof(B_LIST)/sizeof(B_LIST[0]); i++)
    {
        if (B_LIST[i] == tagNum)
        {
            dlog_alert("Tag %02X is blacklisted, we won't return it!", tagNum);
            return true;
        }
    }
    return false;
}

int parseOutTLV(unsigned char *bufferIn, int buflenIn, unsigned char *bufferOut, int buflenOut)
{

    int numObj = 0;
    unsigned char *currentIn;
    unsigned char *currentOut;
    unsigned char Tag[3];
    int tagNum;
    unsigned short TLVlen = 0;
    unsigned char tempTLV[CARD_APP_DEF_MAX_TLV_LEN];
    int Outlen = 0;

	g_logger.Log("Processing outgoing TLV object list: (outbuffer len: %d bytes:", buflenOut);
	g_logger.LogHex(bufferIn, buflenIn);

    if((buflenIn == 0) || (buflenOut == 0) || (bufferIn == NULL) || (bufferOut == NULL))
        return 0;

    currentIn = bufferIn;
    currentOut = bufferOut;
    while(bufferIn+buflenIn > currentIn)
    {
        Tag[0] = currentIn[0];
        currentIn++;
        #if 0
        if(Tag[0] & 0x20)   //constructed
        {
            return -1;      //constructed objects are not allowed
        }
        #endif
        if((Tag[0] & 0x1F) == 0x1F) //more than 1 byte tag
        {
            Tag[1] = currentIn[0];
            currentIn++;
        }
        else
        {
            Tag[1] = 0x00;
        }
        if(Tag[1] & 0x80) //more than 1 byte tag
        {
            Tag[2] = currentIn[0];
            currentIn++;
        }
        else
        {
            Tag[2] = 0x00;
        }
        if(Tag[2] > 0)
        {
            tagNum = Tag[0]*0x10000 + Tag[1]*0x100 + Tag[2];
        }
        else
        {
            tagNum = Tag[0]*0x100 + Tag[1];
        }
        //dlog_msg("Processing TAG: %X", tagNum);
        getTLV(tagNum, tempTLV, &TLVlen);
        if(buflenOut - Outlen >= TLVlen + 5)    //max size of tag and its len field
        {
            //there is still place in out buffer
            currentOut[0] = Tag[0];
            currentOut++;
            Outlen++;
            if(Tag[1] > 0)
            {
                currentOut[0] = Tag[1];
                currentOut++;
                Outlen++;
            }
            if(Tag[2] > 0)
            {
                currentOut[0] = Tag[2];
                currentOut++;
                Outlen++;
            }
            if(TLVlen > 0x7F)
            {
                currentOut[0] = 0x81;
                currentOut++;
                Outlen++;
            }
            currentOut[0] = static_cast<unsigned char>(TLVlen);
            currentOut++;
            Outlen++;
            memcpy(currentOut, tempTLV, TLVlen);
            //dlog_msg("adding %d bytes, current len: %d - total %d TLV objects", TLVlen, Outlen, numObj);
            currentOut+=TLVlen;
            Outlen+=TLVlen;
            numObj++;
        }

    }
	g_logger.Log("TLV out list ready: %d of %d bytes used", Outlen, buflenOut);
    return Outlen;

}

#define EMV_TERM_ID_SIZE 8
int inGetPTermID(char *ptid)
{
    char    tempPTID [EMV_TERM_ID_SIZE + 2] = {0};

    com_verifone_pml::svcInfoPtid(tempPTID, EMV_TERM_ID_SIZE + 2);
    //SVC_INFO_PTID (tempPTID);
    strncpy (ptid, tempPTID, EMV_TERM_ID_SIZE);
    ptid[EMV_TERM_ID_SIZE+1] = '\0';
    return ESUCCESS;
}

int get_display_resolution(int &x, int &y)
{
    int left, right,top,bottom;
    int textRows, textPixelsY;
    int charSizeRow=0, charSizeCol=0;

	#if 0
    set_display_coordinate_mode(CHARACTER_MODE);
    wherewin(&left, &top, &right, &bottom);
    get_character_size(&charSizeRow, &charSizeCol);

    textRows = bottom + 1 - top;
    textPixelsY = charSizeRow * textRows;

    set_display_coordinate_mode(PIXEL_MODE);
    window(0, 0, WINDOW_MAX, WINDOW_MAX);
    int ret = wherewin(&left, &top, &right, &bottom);
    if(ret<0) return ret;
    int graphPixelsY = bottom + 1 - top;
    int diffY = graphPixelsY - textPixelsY;
    if (diffY > charSizeRow)
    {
        y = textPixelsY;
    }
    else
    {
        y = graphPixelsY;
    }
    x = right + 1 - left;
    set_display_coordinate_mode(CHARACTER_MODE);
	#endif
    return 0;
}

bool VerifyPINBypassPassword(int &m_Console, char *amtDsp, long &x_pos, long &y_pos, bool smallScreen, char * addText)
{
    return true;
}

unsigned short getUsrSecurePin(unsigned char *pstPin)
{
    dlog_alert("Secure PIN only!");
    // g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_ENTRY);
    return 0;
}

void restoreGuiAppConsole()
{
    //GuiApp_RestoreConsole();
}

#define E_USR_PIN_CANCELLED 0
#define E_USR_PIN_BYPASSED 1
#define E_USR_ABORT 2


unsigned short getUsrPin(unsigned char *pstPin)
{
	//cPINdisplayGuiApp PINdisplay;
    uint8_t minPINLength = M_MIN_PIN_LENGTH_MIN;
    uint8_t maxPINLength = M_MAX_PIN_LENGTH_MAX;
    long x_pos = 0, y_pos = 0, x_font = 0, y_font = 0;
    int x_screen = 0, y_screen = 0;
    char xy_screen[2];
    bool codesUsed = false;
    bool smallScreen = false; // Some devices (Vx700, Vx520, ...) have small screen -> Only two lines of text plus PIN box fit
    static const int MAX_ALTERNATE_SCREENS = 2;
    char alternateScreens[MAX_ALTERNATE_SCREENS][ADD_SCREEN_TEXT+1];
    int alternateScreenIndex = 0;

    pstPin[0] = 0;
    dlog_msg("PIN entry requested");
    g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_ENTRY);

    unsigned short dLen =0;
    unsigned char amt_num[6];
    unsigned char currExp = 2;
    const currency_code_t *pCurrCode = &M_CURRENCY_CODES[0];
    uint16_t trn_curr = 0;
	std::string amtDsp;
	std::string extDsp;

	//PINdisplay.Display();
    //here we start actual pin entry
    int key_index=0;
    bool pin_bypassed = false;
    static const long TIMEOUT_EVENT = EVT_TIMER;
    int m_PosTimeoutTimer = -1;

    if(g_CardAppConfig.GetPINtype() == CARD_APP_PIN_ONLINE_VSS)
    {
        unsigned char dummy = 0;
        int retVal;
        PINRESULT pinOUTData;
        int pinStatus;
        PINPARAMETER psKeypadSetup;
        long interestingEvents = EVT_PIPE;
        long detectedEvents;

        psKeypadSetup.ucMin = g_CardAppConfig.GetMinPINLen();
        psKeypadSetup.ucMax = g_CardAppConfig.GetMaxPINLen();
        psKeypadSetup.ucEchoChar = '*';
        psKeypadSetup.ucDefChar = g_CardAppConfig.GetPINblankChar();
        psKeypadSetup.ucOption = g_CardAppConfig.GetPINoption(); // Bit 3 is set by default
        if (g_CardAppConfig.GetOnlinePINCancelAllowed())
            psKeypadSetup.ucOption |= 0x02; // bit 1
        if (g_CardAppConfig.GetOnlinePINEntryType() != CCardAppConfig::PIN_ENTRY_TYPE_MANDATORY_E)
            psKeypadSetup.ucOption |= 0x10; // bit 4
        dlog_msg("ONLINE PIN entry, flags %02X", psKeypadSetup.ucOption);
        iPS_SetPINParameter(&psKeypadSetup);
        iPS_SelectPINAlgo(0x0B); // Always
        retVal=iPS_RequestPINEntry(0, &dummy);
        // Set up PIN entry timeout (if applicable)

        do
        {
            // Get PIN Response
            retVal=iPS_GetPINResponse(&pinStatus, &pinOUTData);
            // Check PIN timeout
            // Check cancellation
            //dlog_msg("Detected events %d", detectedEvents);
            std::string source_task;
            if (com_verifone_ipc::check_pending(source_task) == com_verifone_ipc::IPC_SUCCESS)
            {
                CCardAppConfig::cmd_break_t break_status=g_CardAppConfig.checkCmdBreak();
                if(break_status == CCardAppConfig::CMD_BREAK_CANCEL_E)
                {
                    iPS_CancelPIN();
                    pinStatus = 0xFF;
                    break;
                }
            }

            SVC_WAIT(300);
        } while (pinStatus == 0x02);

		switch(pinStatus)
        {
             case 0x00:
                dlog_msg("INFO:EnterEncryptPIN: PIN_OK_");
                key_index = pinOUTData.nbPinDigits;
                break;
             case 0x06: // if allowed
                dlog_msg("INFO:EnterEncryptPIN: PIN_0PIN_");
                key_index = E_USR_PIN_BYPASSED;
                break; // ok, process
             case 0x05: //user cancel
                dlog_msg("INFO:EnterEncryptPIN: PIN_ABORT_CANCEL_ %d ", pinOUTData.encPinBlock[0]);
                key_index = static_cast<unsigned short>(E_USR_PIN_CANCELLED);
                break;
             case 0x0A:
                dlog_msg("INFO:EnterEncryptPIN: PIN_ABORT_CLEAR_");
                key_index = static_cast<unsigned short>(E_USR_ABORT);
                break;
             case 0x01: // unknown error ??
                dlog_msg("INFO:EnterEncryptPIN: PIN_IDLE_");
                key_index = static_cast<unsigned short>(E_USR_ABORT);
                break;
             case 0xFE: // timeout
                dlog_msg("INFO:EnterEncryptPIN: PIN_TIMEOUT_");
                key_index = static_cast<unsigned short>(E_USR_ABORT);
                break;
             case 0xFF: // aborted externally
                dlog_msg("INFO:EnterEncryptPIN: PIN_ABORTED_BY_POS_");
                key_index = static_cast<unsigned short>(E_USR_PIN_CANCELLED);
                break;
             default:
                dlog_msg("INFO:EnterEncryptPIN: PIN response error");
                key_index = static_cast<unsigned short>(E_USR_ABORT);
                break;
        }
        dlog_msg("PIN Processing %d digits", pinOUTData.nbPinDigits);

    }
    else if(g_CardAppConfig.GetPINtype() == CARD_APP_SECURE_PIN_OFFLINE)
    {
        dlog_msg("Secure PIN - returning control to EMV Kernel");
        return ESUCCESS;
    }

    return key_index;
}

/* For PIN Library only */

void usEMVDisplayErrorPrompt(unsigned short errorID)
{
    unsigned long prompt_index = 0;
    char section_name[10];
    unsigned char x_pos = 0, y_pos = 0;
    int display = 0;
#if 0
    switch(errorID)
    {
        case E_PIN_REQD:
            break;
        case E_INVALID_PIN:
            dlog_msg("Incorrect PIN");
            g_CardAppConfig.pinScreenTitle = CARD_APP_GUI_INCORRECT_PIN;
            //strcpy(section_name, CARD_APP_GUI_S_PIN);
            //prompt_index = CARD_APP_GUI_INCORRECT_PIN;
            //display = 0;
            break;
        case E_LAST_PIN_TRY:
            dlog_msg("Last PIN try");
            //strcpy(section_name, CARD_APP_GUI_S_PIN);
            //prompt_index = CARD_APP_GUI_LAST_PIN;
            //display = 0;
            g_CardAppConfig.pinScreenTitle = CARD_APP_GUI_LAST_PIN;
            g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_LAST);
            break;
        case E_PIN_TRY_LT_EXCEED:
            g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_BLOCKED);
            break;
        case E_INVALID_CAPK:
            break;
        case E_CAPK_FILE_NOT_FOUND:
            break;
        case E_COMBINED_DDA_AC_FAILED:
            break;
        case E_BLACKLISTED_CARD:
            break;
        case E_NO_ATR:
            break;

        case E_INVALID_KEY_LENGTH:
            break;
        case E_CVM_NOT_PERFORMED:
            break;
        case E_PIN_BLOCKED:
            g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_BLOCKED);
            break;
        case UNRECOGNIZED_CVM:
            break;
        case ONLINE_PIN_PERFORMED:
            break;

        case E_INVALID_ATR:
            break;
        case E_APP_BLOCKED:
            break;
        case EMV_PIN_SESSION_IN_PROGRESS:
            //dlog_msg("PIN session in progress");
            if (peek_event() & EVT_PIPE)
            {
                dlog_msg("Message received - checking if not cancel...");
                read_evt(EVT_PIPE);
                CCardAppConfig::cmd_break_t break_status=g_CardAppConfig.checkCmdBreak();
                if(break_status == CCardAppConfig::CMD_BREAK_CANCEL_E)
                {
                    dlog_alert("PIN entry cancelled, exiting...");
                    g_CEMV.SetPINBypassStatus(EXT_PIN_CNCL);        //JACEK - to be setup
                    //key_index = static_cast<unsigned short>(E_USR_ABORT);
                    //entry_end = 1;
                    //g_CEMV.SetStatusDetail(CARD_APP_STATUS_D_CANCELLED);
                    //g_CEMV.SetPINEntryStatus(PIN_ENTRY_STATUS_CANCELLED_POS);
                }
                else if(break_status == CCardAppConfig::CMD_BREAK_PIN_BYPASS_E)
                {
                    dlog_alert("Bypassing PIN entry ...");
                    g_CEMV.SetPINBypassStatus(EXT_PIN_BYPASS);      //JACEK - to be setup
                    //key_index = static_cast<unsigned short>(E_USR_PIN_BYPASSED);
                    //entry_end = 1;
                    //g_CEMV.SetStatusDetail(CARD_APP_STATUS_D_BYPASS);
                    //g_CEMV.SetPINEntryStatus(PIN_ENTRY_STATUS_BYPASSED_POS);
                }
            }
            SVC_WAIT(100);
            break;
        case E_USR_PIN_CANCELLED:
            dlog_msg("PIN cancelled by Cardholder!");
            break;
        case E_USR_PIN_BYPASSED:
            dlog_msg("PIN bypassed by Cardholder!");
            break;
        case EMV_PIN_SESSION_COMPLETE:
            dlog_msg("PIN session completed!");
/*          if (g_CEMVtrns.beeperThreadID)
            {
                dlog_msg("Killing beeper thread");
                post_user_event(g_CEMVtrns.beeperThreadID, 1); // post any user event, thread will end this way
                g_CEMVtrns.beeperThreadID = 0;
            } */
            g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_DONE);
            break;
    }

    dlog_msg(">>>MODULE PROMPT: %X", errorID);
    if(display)
    {
        if(prompt_index > 0)
        {
            if(g_CardAppConfig.GuiInterface() == CCardAppConfig::GUI_APP)
            {
                dlog_msg("Displaying prompt %X using GUIAPP", errorID);

                if(g_CardAppConfig.GetUseDisplay())
                {
                    //JACEK - to be setup
                    /*
                    GuiApp_DisplayPromptSelectSection(prompt_index, section_name, x_pos, y_pos);
                    error_tone();
                    SVC_WAIT(600);
                    error_tone();
                    SVC_WAIT(600);
                    error_tone();
                    SVC_WAIT(600);
                    */
                }
            }
            else
            {
                dlog_error( "LOCAL GUI SUPPORT NOT YET IMPLEMENTED!" );
            }
        }
    }
    else
    {
        if (prompt_index > 0)
        {
            userPromptLite prompt;
            prompt.getPrompt(section_name, prompt_index, g_CardAppConfig.extraPINmessage, ADD_SCREEN_TEXT+1 );
            dlog_msg("Additional PIN prompt is %s", g_CardAppConfig.extraPINmessage);
        }
    }
	#endif
}

unsigned short usEMVPerformSignature(void)
{
    dlog_msg("Signature Requested");
    g_CEMV.SetStatusMain(CARD_APP_STATUS_M_SIGNATURE);
    return ESUCCESS;
}

unsigned short usEMVPerformOnlinePIN(void)
{
    dlog_msg("Online PIN requested");
    g_CEMV.SetStatusMain(CARD_APP_STATUS_M_PIN);

    return ESUCCESS;
}


bool bIsCardBlackListed(unsigned char * pan, unsigned short panLen, unsigned char * panSeqNo, unsigned short panSeqLen)
{
    dlog_msg("Checking blacklist result %d", g_CEMV.isCardBlackListed());
    return g_CEMV.isCardBlackListed();
    //return EMV_FALSE;

}

unsigned short usGetExtAuthDecision(unsigned short usStatusWord)
{
    dlog_alert("External Authenticate has failed, error %04Xh", usStatusWord);
    if (usStatusWord == 0x6985)
    {
        return g_CardAppConfig.GetExtAuthFlag();
    }
    return 0;
}

unsigned short usGetPOSDecision (void)
{
    if (g_CEMV.GetPINBypassStatus() != 0)
    {
        dlog_alert("PIN Bypass status changed to %02Xh!", g_CEMV.GetPINBypassStatus());
        return g_CEMV.GetPINBypassStatus();
    }
    return 0;
}


void InitMinimumTLVset(void)
{
    unsigned char Data[] = {0, 0, 0, 0, 0, 0};

	#if 0
    dlog_msg("setting minimum TLV set neceseary for proper operation: %X, %X, %X, %X, %X, %X",
        TAG_81_BIN_AMOUNT_AUTH, TAG_9F02_NUM_AMOUNT_AUTH, TAG_9F03_NUM_AMOUNT_OTHER,
        TAG_9F04_BIN_AMOUNT_OTHER, TAG_5F57_ACCOUNT_TYPE, TAG_9C_TRANS_TYPE);

    g_EMVcollection.SetTLVData(TAG_81_BIN_AMOUNT_AUTH, Data, BINARY_AMT_SIZE);
    g_EMVcollection.SetTLVData(TAG_9F02_NUM_AMOUNT_AUTH, Data, NUMERIC_AMT_SIZE);
    g_EMVcollection.SetTLVData(TAG_9F03_NUM_AMOUNT_OTHER, Data, NUMERIC_AMT_SIZE);
    g_EMVcollection.SetTLVData(TAG_9F04_BIN_AMOUNT_OTHER, Data, BINARY_AMT_SIZE);
    g_EMVcollection.SetTLVData(TAG_5F57_ACCOUNT_TYPE, Data, TRAN_TYPE_SIZE);
    g_EMVcollection.SetTLVData(TAG_9C_TRANS_TYPE, Data, TRAN_TYPE_SIZE);
	#endif
}

int GetAIDPair(void)
{
    int hAIDHandle  = 0;
    int iHeaderSize     = DAT_HEADER_SIZE;
    int iAIDIndex = 0;
    std::string loc = com_verifone_pml::getFlashRoot();
    loc.append(1, '/');
    loc.append(AID_FILENAME);

    hAIDHandle = open(loc.c_str(),O_RDONLY);

    if(hAIDHandle > 0)
    {
        long lnSeekResult = lseek (hAIDHandle, iHeaderSize, SEEK_SET);

        if (lnSeekResult != iHeaderSize)
        {
            close(hAIDHandle);
            return CARD_APP_RET_FAIL;
        }
        memset(g_CardAppConfig.srAID[iAIDIndex].szAID1, 0x00, sizeof(g_CardAppConfig.srAID[iAIDIndex].szAID1));
        memset(g_CardAppConfig.srAID[iAIDIndex].szAID2, 0x00, sizeof(g_CardAppConfig.srAID[iAIDIndex].szAID2));

		char szTempBuff[AID_REC_SIZE] = {0};

        while((read(hAIDHandle, szTempBuff, AID_REC_SIZE) == AID_REC_SIZE) && (iAIDIndex < MAX_AID_REC))
        {
            strncpy((char*)g_CardAppConfig.srAID[iAIDIndex].szAID1, szTempBuff, AID_SIZE);
            strncpy((char*)g_CardAppConfig.srAID[iAIDIndex].szAID2, szTempBuff+AID_SIZE+1, AID_SIZE);

            iAIDIndex++;

            memset(g_CardAppConfig.srAID[iAIDIndex].szAID1, 0x00, sizeof(g_CardAppConfig.srAID[iAIDIndex].szAID1));
            memset(g_CardAppConfig.srAID[iAIDIndex].szAID2, 0x00, sizeof(g_CardAppConfig.srAID[iAIDIndex].szAID2));
            memset(szTempBuff, 0, sizeof(szTempBuff));
        }
        close(hAIDHandle);
    }
    else
    {
        memset(g_CardAppConfig.srAID[0].szAID1, 0x00, sizeof(g_CardAppConfig.srAID[0].szAID1));
        memset(g_CardAppConfig.srAID[0].szAID2, 0x00, sizeof(g_CardAppConfig.srAID[0].szAID2));
    }
    dlog_msg(">>>GetAIDPair<<< %d/%d AID pairs found", iAIDIndex, MAX_AID_REC);
    return CARD_APP_RET_OK;

}

//checks for duplicated candidates, return number of removed entries

int RemoveAIDduplicates(void)
{
    int AIDCount = 0;
	int AIDout = 0;
	int i, j;
	EMV_CT_CANDIDATE_TYPE	 T_BF04_Candidates[5];			//temporary copy of candidate list
	bool addAID = true;


	memcpy(T_BF04_Candidates, g_EMVcollection.selectData.T_BF04_Candidates, sizeof(T_BF04_Candidates));
	memset(g_EMVcollection.selectData.T_BF04_Candidates, 0, sizeof(T_BF04_Candidates));

    while (T_BF04_Candidates[AIDCount].candidate.aidlen != 0) ++AIDCount;

	AIDout = 0;
	
	for(i = 0; i < AIDCount; i++)
	{
		addAID = true;
		for(j = 0; j < AIDout; j++)
		{
			if(memcmp(T_BF04_Candidates[i].candidate.AID, g_EMVcollection.selectData.T_BF04_Candidates[j].candidate.AID, T_BF04_Candidates[i].candidate.aidlen) == 0)
			{
				if(memcmp(T_BF04_Candidates[i].name, g_EMVcollection.selectData.T_BF04_Candidates[j].name, strlen((const char *)T_BF04_Candidates[i].name)) == 0)
				{
					dlog_msg("Duplicate candidate list entry found for: %s", T_BF04_Candidates[i].name);
					addAID = false;
				}
			}
		}
		if(addAID)
		{
			dlog_msg("Adding candidate for: %s", T_BF04_Candidates[i].name);
			g_EMVcollection.selectData.T_BF04_Candidates[AIDout] = T_BF04_Candidates[i];
			AIDout++;
			g_logger.Log("Adding candidate for: %s (total %d candidates for selection)", T_BF04_Candidates[i].name, AIDout);
		}
			
	}

	
	return AIDout;
}

//CandListModify - as required for UK "Chip&PIN" project
/************************************************************
* CandListModify:
*
*************************************************************/

int CandListModify(EMV_CT_CANDIDATE_TYPE T_BF04_Candidates[5])
{
    int AIDCount = 0;
    int excludedAIDCount = 0;
    int iMatchFound   = 0;
    int iAIDIndex = 0, iCandListIndex = 0, iIndexToModify = 0;
    char szAID[AID_SIZE+1] = {0};

    while (T_BF04_Candidates[AIDCount].candidate.aidlen != 0) ++AIDCount;

    dlog_msg("CandListModify, we have %d AIDs", AIDCount);

    // Take one record of srAID structure, compare the two AIDS
    // across the candidate list. If the matchcount is 2,
    // i.e; if both the AIDS of one record occur in candidate list then
    // mark the first AID of the pair in srAID as invalid in candidate list

    iAIDIndex = 0;
    while((g_CardAppConfig.srAID[iAIDIndex].szAID1[0] != 0) && (g_CardAppConfig.srAID[iAIDIndex].szAID2[0] != 0))
    {
        iMatchFound = 0;
        dlog_msg("Checking AID pair: %s:%s", g_CardAppConfig.srAID[iAIDIndex].szAID1, g_CardAppConfig.srAID[iAIDIndex].szAID2);
        for (iCandListIndex = 0; iCandListIndex < AIDCount; iCandListIndex++)
        {
            if (T_BF04_Candidates[iCandListIndex].name[0] == '\0') continue; // Skip excluded AIDs
            // Convert from Hex to Ascii
            memset(szAID,0x00,sizeof(szAID));
            com_verifone_pml::svcHex2Dsp(reinterpret_cast<char *>(T_BF04_Candidates[iCandListIndex].candidate.AID), T_BF04_Candidates[iCandListIndex].candidate.aidlen, szAID, sizeof(szAID));
            dlog_msg("Processing AID: %s index: %d", szAID, iCandListIndex);

            //typically AID from candidate list len is >= AID from the list so we cut it to the size of the list
            if (0 == strncmp(const_cast<char*>(szAID), const_cast<char*>(g_CardAppConfig.srAID[iAIDIndex].szAID1), strlen(const_cast<char*>(g_CardAppConfig.srAID[iAIDIndex].szAID1))))
            {
                iIndexToModify = iCandListIndex;
                iMatchFound++;
            }

            if (0 == strncmp(const_cast<char*>(szAID), const_cast<char*>(g_CardAppConfig.srAID[iAIDIndex].szAID2), strlen(const_cast<char*>(g_CardAppConfig.srAID[iAIDIndex].szAID2))))
            {
                iMatchFound++;
            }
        }

        if (2 == iMatchFound)
        {
            dlog_msg("Removing AID index %d", iIndexToModify);
            T_BF04_Candidates[iIndexToModify].name[0] = 0;
            ++excludedAIDCount;
        }
        iAIDIndex++;
    }
    //if (g_CardAppConfig.IsExternalApplicationSelectionEnabled() && AIDCount - 1 > excludedAIDCount)
		
    if (g_CardAppConfig.IsExternalApplicationSelectionEnabled())
	{
        dlog_alert("Performing external application selection!");
        IPCPacket response;

        response.setCmdCode(EXTERNAL_APP_SEL_EVT);
        for (int i=0; i < AIDCount; i++)
        {
            if (T_BF04_Candidates[i].name[0] != '\0')
            {
                using namespace com_verifone_TLVLite;
                response.addTag(ConstData(arbyAppAID, APP_AID_TS), ConstData(T_BF04_Candidates[i].candidate.AID, T_BF04_Candidates[i].candidate.aidlen));
                response.addTag(ConstData(arbyAppLabel, APP_LABEL_TS), ConstData(T_BF04_Candidates[i].name, strlen(reinterpret_cast<char *>(T_BF04_Candidates[i].name))));
				std::string AID_to_select;
				std::string AID_priority;
				AID_to_select.assign((char *)T_BF04_Candidates[i].candidate.AID, (size_t)T_BF04_Candidates[i].candidate.aidlen);
				AID_priority = g_EMVcollection.AID_priority[AID_to_select];
				response.addTag(ConstData(arbyAppPriority, APP_AID_PRIORITY_TS), ConstData(AID_priority.c_str(), 1));
            }
        }
        response.setAppName(g_CardAppConfig.cNotifyName);
        response.send();
        // wait for response here and process it
        const int MAX_AID_LEN = 16;
        char aid[MAX_AID_LEN];
        size_t aidSize = sizeof(aid);
        char label[MAX_LABEL_LEN];
        size_t labelSize = sizeof(label);
        int result = 0;
        int events_mon = event_open();
        int icc_handle = g_CardAppConfig.GetICChandle();
	if (icc_handle >= 0)
        {
            event_item ev_icc;
            ev_icc.evt.com.fd = icc_handle;
            ev_icc.event_id = events::icc;
            ev_icc.evt.com.flags = EPOLLIN | EPOLLPRI;
            int iRet = event_ctl(events_mon, ctl::ADD, ev_icc);
            dlog_msg("event_ctl(...icc) ret: %d", iRet);
            event_ctl(events_mon, ctl::ADD, ev_icc);
        }
        event_ctl(events_mon, ctl::ADD, com_verifone_ipc::get_events_handler(g_CardAppConfig.cNotifyName));

        eventset_t pml_events;
        do
        {
            dlog_msg("APP SEL: Waiting for events");

            if(event_wait(events_mon, pml_events) > 0)
            {
                dlog_msg("We have %d events", pml_events.size());
                for (eventset_t::iterator it = pml_events.begin() ; it != pml_events.end(); ++it)
                {
                    if (it->event_id == events::icc)
                    {
                        if(isCardRemoved())
                        {
                             dlog_alert("Card removed!");
                             result = APP_SEL_CARD_REMOVED;
                             break;
                        }
                    }
                    else if (it->event_id == events::ipc)
                    {
                        dlog_msg("Message received - checking if not cancel...");
                        aidSize = sizeof(aid); labelSize = sizeof(label);
                        CCardAppConfig::cmd_break_t break_status = g_CardAppConfig.getAppSelection(aid, aidSize, label, labelSize);
                        if (break_status == CCardAppConfig::CMD_BREAK_CANCEL_E)
                        {
                            result = APP_SEL_POS_CANCELLED;
                            break;
                        }
                        else if (break_status == CCardAppConfig::CMD_BREAK_NO_BREAK_E)
                        {
                            result = 1;
                        }

                    }
                }
                pml_events.clear();
            }
        } while(result == 0);
        event_close(events_mon);
        if (result < 0)
        {
            dlog_alert("Cancel received or card removed, cancelling transaction!");
            return result;
        }
        if (aidSize && labelSize && !isCardRemoved())
        {
            for (int i=0; i<AIDCount; i++)
            {
                if (T_BF04_Candidates[i].name[0] != '\0')
                {
                    if (T_BF04_Candidates[i].candidate.aidlen == aidSize && !memcmp(T_BF04_Candidates[i].candidate.AID, aid, aidSize) &&
                        strlen(reinterpret_cast<char *>(T_BF04_Candidates[i].name)) == labelSize && !memcmp(T_BF04_Candidates[i].name, label, labelSize))
                    {
                        dlog_msg("AID index %d selected", i);
                        return i;
                    }
                }
            }
            dlog_error("Invalid answer, nothing blocked!");
            return APP_SEL_INVALID_PARAMETERS;
        }
        else
        {
            dlog_error("Error occurred or no data!");
            return APP_SEL_INVALID_PARAMETERS;
        }
    }

    return APP_SEL_ERROR;
}

#if 0
void SetDefaultFunctionPointers(void)
{
//---------------------------------------------------------------------
// getLastTxnAmt()    GET_LAST_TXN_AMT
//---------------------------------------------------------------------
//    The application should call
//    inVxEMVApSetFunctionality(GET_LAST_TXN_AMT, getLastTxnAmt)
//    API during transaction initialization.
//    This API returns the last transaction amount for a card with
//    the same PAN as the card inserted for the current
//    transaction, and returns an amount equal to 0 if the record
//    does not exist for the same PAN.
    inVxEMVAPSetFunctionality(GET_LAST_TXN_AMT, NULL);

//---------------------------------------------------------------------
// getTerminalParam()    GET_TERMINAL_PARAMETER
//---------------------------------------------------------------------
//    The application should call
//    inVxEMVApSetFunctionality(GET_TERMINAL_PARAMETER, NULL)
//    API during transaction initialization.
//    This API provides all the required data for TAC (Terminal
//    Action Codes), and also contains data for the default DDOL
//    and TDOL.
    inVxEMVAPSetFunctionality(GET_TERMINAL_PARAMETER, NULL);

//---------------------------------------------------------------------
// getCapkExp()    GET_CAPK_DATA
//---------------------------------------------------------------------
//    The application should call
//    inVxEMVApSetFunctionality(GET_CAPK_DATA,NULL)
//    API to use default implementation in the EMV Module.
//    This API obtains the CAPK Modulus and Exponent required
//    for SDA, DDA, CDA and enciphered PIN off-line. This is
//    performed by opening the relevant CAPK configuration file
//    and extracting the key in binary form.
//    usEMV_Set_Functionality(GET_CAPK_DATA, (void *)&getAppCapkExp);
    inVxEMVAPSetFunctionality(GET_CAPK_DATA, NULL);

//---------------------------------------------------------------------
// getUsrPin()    GET_USER_PIN
//---------------------------------------------------------------------
//    The application should call
//    inVxEMVApSetFunctionality(GET_USER_PIN,getUsrPIN)
//    API during transaction initialization.
//    This API is called as a part of CVM. It obtains the PIN to
//    complete CVM processing.
    inVxEMVAPSetFunctionality(GET_USER_PIN, (void *)&getUsrSecurePin);

//---------------------------------------------------------------------
// usEMVDisplayErrorPrompt()    DISPLAY_ERROR_PROMPT
//---------------------------------------------------------------------
//    The application should call
//    inVxEMVApSetFunctionality(DISPLAY_ERROR_PROMPT,
//                              usEMVDisplayErrorPrompt)
//    API during transaction initialization.
//    This API should be implemented by the application.
    inVxEMVAPSetFunctionality(DISPLAY_ERROR_PROMPT, (void *)&usEMVDisplayErrorPrompt);

//---------------------------------------------------------------------
// usEMVPerformSignature()    PERFORM_SIGNATURE
//---------------------------------------------------------------------
//    The application should call
//    inVxEMVApSetFunctionality(PERFORM_SIGN, usEMVPerformSignature)
//    API during transaction initialization.
//    This API should be implemented by the application.
    inVxEMVAPSetFunctionality(PERFORM_SIGNATURE, (void *)&usEMVPerformSignature);

//---------------------------------------------------------------------
// usEMVPerformOnlinePIN()    PERFORM_ONLINE_PIN
//---------------------------------------------------------------------
//    The application should call
//    inVxEMVApSetFunctionality(PERFORM_ONLINE_PIN,
//                              usEMVPerformOnlinePIN)
//    API during transaction initialization.
//    This API should be implemented by the application.
//    Note: Online PIN validation is not a part of the EMV Module.
    inVxEMVAPSetFunctionality(PERFORM_ONLINE_PIN, (void *)&usEMVPerformOnlinePIN);

//---------------------------------------------------------------------
// usEMVIssAcqCVM()    PERFORM_ISS_ACQ_CVM
//---------------------------------------------------------------------
//    The application should call
//    inVxEMVApSetFunctionality(PERFORM_ISS_ACQ_CV M, usEMVIssAcqCVM)
//    API during transaction initialization.
//    This API should be implemented by the application.
    inVxEMVAPSetFunctionality(PERFORM_ISS_ACQ_CVM, (void*)&usEMVIssAcqCVM);


//---------------------------------------------------------------------
// bIsMultipleOccurencesAllowed()    IS_PARTIAL_SELECT_ALLOWED
//---------------------------------------------------------------------
//    The application should call
//    inVxEMVApSetFunctionality(IS_PARTIAL_SELECT_ALLOWED, NULL)
//    API to use default implementation in the EMV Module.
//    This API is used by the library during the selection to allow
//    partial selection of AIDs. This API should be implemented by
//    the application.
    inVxEMVAPSetFunctionality(IS_PARTIAL_SELECT_ALLOWED, NULL);

    inVxEMVAPSetFunctionality(IS_CARD_BLACKLISTED, (void *)&bIsCardBlackListed);
//    usEMV_Set_Functionality(IS_CARD_BLACKLISTED, (void *)&bIsCardBlackListed);

    inVxEMVAPSetFunctionality(Get_EXT_AUTH_DECISION, (void *)&usGetExtAuthDecision);

    inVxEMVAPSetFunctionality(GET_POS_CVM_DECISION, (void *)&usGetPOSDecision);

    inVxEMVAPSetFunctionality(GET_TERMINAL_AVN, (void *)&usGetTermAVN);
}

int isSeparateGPO()
{
    int res = -1;
    MVT_REC mvt_rec;
    if (!loadMVTRec(&mvt_rec))
    {
        return res;
    }
    dlog_msg("RFU1: %d", mvt_rec.shRFU1);
    if (mvt_rec.shRFU1 & 0x80) res = 1;
    else res = 0;
    dlog_msg("SeparateGPO %d", res);
    return res;
}


#endif

void SetDefaultFunctionPointers(void)
{
	dlog_msg("Setting callbacks...");
}

int isSeparateGPO()
{
	return 0;
}


bool isCardRemoved()
{
    return (EMV_CT_SmartDetect(0) == EMV_ADK_SMART_STATUS_REMOVED);
	
}

bool isCardInserted()
{

	
	unsigned char crd_status = EMV_CT_SmartDetect(0);

	if(crd_status != EMV_ADK_SMART_STATUS_OK && crd_status != EMV_ADK_SMART_STATUS_REMOVED)
	{
		g_CardAppConfig.SetLogADK(true);
		dlog_msg("***CRITICAL ERROR!!! - incorrect card status value! : %d", crd_status);
		g_logger.Log("***CRITICAL ERROR!!! - incorrect card status value! : %d", crd_status);
	}
    return (crd_status == EMV_ADK_SMART_STATUS_OK);

}


static void getScreenTextDimensions(int &maxCharsInLine, int &maxLines)
{
    static int x = 0, y = 0;
	#if 0
    if (x == 0 || y == 0)
    {
        int x_font, y_font;
        int x_screen, y_screen;
        switch(getgrid())
        {
            case 0:
                x_font = 8;
                y_font = 16;
                break;
            case 2:
                x_font = 6;
                y_font = 8;
                break;
            default:
                y_font = 8;
                x_font = 8;
                break;
        }
        get_display_resolution(x_screen, y_screen);
        dlog_msg("Screen res: %d x %d, font x %d, y %d", x_screen, y_screen, x_font, y_font);
        x = x_screen / x_font;
        y = y_screen / y_font;
    }
	#endif
    maxCharsInLine = x;
    maxLines = y;
}

int getScreenMaxX()
{
    int x, y;
    getScreenTextDimensions(x, y);
    return x;
}

int getScreenMaxY()
{
    int x, y;
    getScreenTextDimensions(x, y);
    return y;
}

/*
bool setPINParams()
{
    srPINParams psParams;
    const int maxX = getScreenMaxX();

    if (g_CardAppConfig.GetPinX() == 0 || g_CardAppConfig.GetPinY() == 0)
    {

        g_CardAppConfig.SetPinX((maxX >> 1) - (M_MAX_PIN_LENGTH_MAX >> 1));
        g_CardAppConfig.SetPinY(4);
        dlog_msg("PIN entry box coords: x = %d, y = %d", g_CardAppConfig.GetPinX(), g_CardAppConfig.GetPinY());
    }

    memset(&psParams, 0, sizeof(psParams));

    psParams.ucMin = 4;
    psParams.ucMax = 12;
    psParams.ucEchoChar = '*';
    psParams.ucDefChar = ' ';
    psParams.ucDspLine = 4; //g_CardAppConfig.GetPinY();
    psParams.ucDspCol = 8; //JACEK - fix that!//(maxX- psParams.ucMax)/2;   //g_CardAppConfig.GetPinX();
    //psParams.ucOption = 0x12;
    psParams.ucOption = 0x02;
    if (g_CardAppConfig.GetOfflinePINEntryStyle() == 0x00)
    {
        psParams.ucOption |= 0x08; // set bit 3 - Make Clear behave like Backspace
    }
    #if 1
    psParams.ulFirstKeyTimeOut = g_CardAppConfig.GetPINfirstcharTimeout() * 1000;
    psParams.ulInterCharTimeOut = g_CardAppConfig.GetPINintercharTimeout() * 1000;
    psParams.ulWaitTime = g_CardAppConfig.GetPINtimeout() * 1000;
    dlog_msg("First char timeout %d, inter char timeout %d, PIN timeout %d", psParams.ulFirstKeyTimeOut, psParams.ulInterCharTimeOut,psParams.ulWaitTime);
    if (g_CardAppConfig.GetPINfirstcharTimeout() && (g_CardAppConfig.GetPINfirstcharTimeout() > g_CardAppConfig.GetPINtimeout()))
    {
        dlog_alert("Kernel bug workaround: First char timeout (%d) is greater than PIN timeout (%d)!", g_CardAppConfig.GetPINfirstcharTimeout(), g_CardAppConfig.GetPINtimeout());
        psParams.ulFirstKeyTimeOut = g_CardAppConfig.GetPINtimeout() * 1000;
    }

    psParams.abortOnPINEntryTimeOut = 1; //always cancel
    #else
    psParams.ulFirstKeyTimeOut = 20000;
    psParams.ulInterCharTimeOut = 10000;
    psParams.ulWaitTime = 30000;
    psParams.abortOnPINEntryTimeOut = 1;
    #endif
    #if 1
    if (g_CardAppConfig.IsPINBypassEnabled())
    {
        unsigned char bpKey = g_CardAppConfig.GetPINBypassKey();
        dlog_msg("Cardholder PIN bypass enabled, key %02Xh", bpKey);
        if (bpKey != ENTER_KEY_PINBYPASS && bpKey != CLEAR_KEY_PINBYPASS)
        {
            dlog_alert("Invalid PIN bypass key, changing to default (%02X)", ENTER_KEY_PINBYPASS);
            bpKey = ENTER_KEY_PINBYPASS;
        }
        psParams.ucPINBypassKey = bpKey;
    }
    else
    {
        psParams.ucPINBypassKey = static_cast<unsigned char>(NO_PINBYPASS);
    }
    #else
    dlog_msg("Setting PIN bypass key to %02X", ENTER_KEY_PINBYPASS);
    psParams.ucPINBypassKey = ENTER_KEY_PINBYPASS;  // Bksp + 1
    #endif
    psParams.ucSubPINBypass = EMV_TRUE;

    //Use this API to set the PIN parameters.
    int result = usEMVSetPinParams(&psParams);
    dlog_msg("usEMVSetPINParams result %04Xh", result);
    return (result == EMV_SUCCESS);
}
*/

void displaySecurePINPrompt()
{
    // display PIN prompt using GuiApp or manually

    int pintype = g_CardAppConfig.GetPINtype();
    if (pintype != CARD_APP_SECURE_PIN_OFFLINE)
        g_CardAppConfig.SetPINtype(CARD_APP_SECURE_PIN_OFFLINE);
    unsigned char pin[15];
    getUsrPin(pin);
    g_CardAppConfig.SetPINtype(pintype);
    // do we have to run thread?
    if (g_CardAppConfig.GetPINbeeperTimeout() > 0)
    {
        dlog_alert("PIN Beeper is configured, starting beeper thread!");
        g_CEMV.runBeeperThread();
    }

    return;
}

#define OVERLAY_SENSE_SCAN          0x02
#define FW_CHECK_IN_PROGRESS        0x02
#define MODEL_VX600		"VX600"

int get_console_handle()
{
	#if 0
    int m_console = get_console(1);
    int iRet = 0;
	char model_number[12+1];

	//SVC_INFO_MODELNO (model_number);
	com_verifone_pml::svcInfoModelNum(model_number,12+1);
	model_number[12]=0;
	dlog_msg("MODEL NUMBER: %s", model_number);

    if (m_console < 0)
    {
        dlog_alert("get_console() failed, errno %d", errno);
        int consoleOwner = 0;
        m_console = get_owner(DEV_CONSOLE, &consoleOwner);
        dlog_msg("Console owner task id %d, our id %d, handle %d", consoleOwner, get_task_id(), m_console);
        if (consoleOwner == 0)
        {
            m_console = open(DEV_CONSOLE, 0);
            dlog_msg("Console open result %d", m_console);

        }
	    else if(consoleOwner != get_task_id())
	    {
			//console is not available.. checking for 3 times more...
			int i = 0;
			m_console = -1;
			while(i++ < 4 && m_console < 0)
			{
				SVC_WAIT(100);
				m_console = open(DEV_CONSOLE, 0);
			}
	   	}
		if(m_console >= 0)
		{
			iRet = cs_set_sleep_state(0);
            dlog_msg("Vx600: cs_set_sleep_state(0) RET: %d", iRet);
		}
    }
    if(m_console >= 0)
    {
            if(memcmp(model_number, MODEL_VX600, strlen(MODEL_VX600)) == 0)
            {
	            iRet = cs_overlay_scan(OVERLAY_SENSE_SCAN);
	            dlog_msg("vx600: cs_overlay_scan(0x02) RET: %X", iRet);
				int i = 0;
				while(i++ < 4 && iRet != 0)
				{
					SVC_WAIT(100);
					iRet = cs_overlay_scan(OVERLAY_SENSE_SCAN);
				}
				//this code has problems on Vx600 Gen2.5
				/*
				if(iRet == 0)
				{
	            	do
	            	{
	                	iRet = cs_spi_cmd_status();
	                	SVC_WAIT(100); // Be battery-friendly
	            	} while (iRet == 0);
				}
				else
				{
					return -1;
				}
				*/

	            do {
	                iRet = is_keypad_secure();
	                SVC_WAIT(100); // Be battery-friendly
	            } while (iRet & FW_CHECK_IN_PROGRESS);
	            dlog_msg("Vx600: is_keypad_secure() RET: %X", iRet);

	            iRet = cs_set_sleep_state(0);
	            dlog_msg("Vx600: cs_set_sleep_state(0) RET: %d", iRet);


            }

    }

    return m_console;
	#endif
	return 0;
}

void close_console(void)
{
	#if 0
	int consoleOwner = 0;
	int m_console = get_owner(DEV_CONSOLE, &consoleOwner);

	if (consoleOwner == get_task_id())
	{
		dlog_msg("We have console, closing");
		close(m_console);
	}
	#endif
}

/* internal function, formats text (\c -> centers, \n\r -> newlines) */
static int FormatText(char *text, int &length, const int max_length)
{
    dlog_msg("Input: '%s'", text);

    char * pCenter = strstr(text, "\\c");
    if (pCenter)
    {
        const int maxX = getScreenMaxX();
        char output[DISPLAY_TEXT+1];
        memset(output, 0, sizeof(output));
        int outputIndex = 0;
        char * pText = text;
        while (pCenter)
        {
            int diff = 0;
            if (*pCenter == '\\')
            {
                diff = 2;
                pCenter += diff;
            }
            else
            {
                diff = 1;
                ++pCenter;
            }
            if (outputIndex >= sizeof(output))
            {
                dlog_error("Overflow!");
                return 0;
            }
            int beforeCenterSize = pCenter - pText - diff;
            if (beforeCenterSize > 0)
            {
                memcpy(output+outputIndex, pText, beforeCenterSize);
                outputIndex += beforeCenterSize;
            }
            if (outputIndex && *(output+outputIndex-1) != static_cast<char>(0x0a))
            {
                // Add newline to center next data
                *(output+outputIndex) = static_cast<char>(0x0a);
                ++outputIndex;
            }
            pText = pCenter;
            pCenter = strstr(pText, "\\c");
            int len;
            if (pCenter)
            {
                len = pCenter - pText;
            }
            else
            {
                pCenter = strchr(pText, '\n');
                if (pCenter) len = pCenter - pText + 1;
                else len = strlen(pText);
            }

            if (diff == 2)
            {
                int numSpaces = (maxX - len) >> 1;
                if (numSpaces + outputIndex + len >= sizeof(output))
                {
                    dlog_error("Overflow!");
                    return 0;
                }
                while (numSpaces--)
                {
                    *(output+outputIndex) = ' ';
                    ++outputIndex;
                }
            }
            memcpy(output+outputIndex, pText, len);
            outputIndex += len;
            pText = pCenter;
        }
        dlog_msg("Output: '%s'", output);
        strncpy(text, output, max_length);
        length = strlen(output);
    }
    return length;
}

int DisplayText(const char * const text, int text_len, bool clear_screen /* = true */)
{
    return DisplayText(text, text_len, -1, -1, clear_screen);
}

int DisplayText(const char * const text, int text_len, int at_x, int at_y, bool clear_screen /* = false */)
{
    int res = -1;
    int m_console = 1;  //Jecek - change structure here
    #if 0
    if (m_console >= 0)
    {
        char textLocal[DISPLAY_TEXT+1];
        textLocal[DISPLAY_TEXT] = 0;
        strncpy(textLocal, text, DISPLAY_TEXT);
        int localLen = strlen(textLocal);
        if (clear_screen) clrscr();
        FormatText(textLocal, localLen, DISPLAY_TEXT);
        if (at_x == -1 || at_y == -1)
            write(m_console, textLocal, localLen);
        else
            write_at(textLocal, localLen, at_x, at_y);
        res = 1;
    }
	#endif
    return res;
}

int DisplayPromptSelectSection(int prompt_number, const char * const section, bool clear_screen /* = true */)
{
    return DisplayPromptSelectSection(prompt_number, section, -1, -1, clear_screen);
}

int DisplayPromptSelectSection(int prompt_number, const char * const section, int at_x, int at_y, bool clear_screen /* = false */)
{
    int res = -1;
	#if 0
    int m_console = get_console_handle();
    if (m_console >= 0)
    {
        userPromptLite prompt;
        char text[DISPLAY_TEXT+1];
        text[DISPLAY_TEXT] = 0;
        dlog_msg("Display manual prompt: section %s, prompt %d", section, prompt_number);
        if (prompt.getPrompt(section, prompt_number, text, sizeof(text)-1 ) <= 0)
        {
            res = 1;
            switch(prompt_number)
            {
                case CARD_APP_GUI_INSERT: strncpy(text, "\tInsert card", DISPLAY_TEXT); break;
                case CARD_APP_GUI_PLEASE_WAIT: strncpy(text, "\tPlease wait...", DISPLAY_TEXT); break;
                case CARD_APP_GUI_PROCESSING: strncpy(text, "\tProcessing", DISPLAY_TEXT); break;
                case CARD_APP_GUI_REMOVE: strncpy(text, "\tRemove card", DISPLAY_TEXT); break;
                case CARD_APP_GUI_ENTER_KEY: strncpy(text, "\tPress enter", DISPLAY_TEXT); break;
                default: strncpy(text, "\tINVALID SCREEN!", DISPLAY_TEXT); res = 0; break;
            }
        }
        dlog_msg("Retrieved prompt: '%s'", text);
        int text_len = strlen(text);
        if (clear_screen) clrscr();
        FormatText(text, text_len, DISPLAY_TEXT);
        if (at_x == -1 || at_y == -1)
            write(m_console, text, text_len);
        else
            write_at(text, text_len, at_x, at_y);
    }
    else
    {
        dlog_error("Cannot obtain the console!");
    }
	#endif
    return res;
}




