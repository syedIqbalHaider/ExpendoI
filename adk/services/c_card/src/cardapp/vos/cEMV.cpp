#ifndef __cplusplus
#  error "This file is for C++ only!"
#endif

/*****************************************************************************
 *
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/
/**
 * @file       cEMV.cpp
 *
 * @author     Jacek Olbrys
 *
 * @brief      Implementation of the EMV services.
 *
 *
 * @remarks    This file should be compliant with Verifone EMEA R&D C++ Coding
 *             Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/
#include <svc.h>
#include <svcsec.h>
#include <stdlib.h>
#include <string.h>

#include "lib/tlv_libhelper.hpp"

#include <liblog/logsys.h>
#include <libpml/pml.h>
#include <libpml/pml_abstracted_api.h>
#include <libpml/pml_port.h>

#ifdef VFI_GUI_GUIAPP
#include "GuiApp.h"
#endif

#include "cCARDAppConfig.h"

#include "cEMV.h"
#include "cTransaction.h"
#include "EMVtools.h"
#include "PIN.h"
#include "Pin_gui.h"
#include "app_selection.h"
#include "CARDApp.h"

#include "E2E_EMV_CT_Serialize.h"

#include <algorithm>


#define TAG_IC    0x00F2  // Identifies data originating from the ICC
#define TAG_TE    0x00F5  // Identifies data originating from the terminal



#ifndef EMV_CT_TRXOP_MERCHINFO_CALLBACK
	#define EMV_CT_TRXOP_MERCHINFO_CALLBACK	TRXOP_MERCHINFO_CALLBACK
//	#define EMV_CT_TRXOP_ADD_TRACE_CALLBACK	TRXOP_ADD_TRACE_CALLBACK
	#define EMV_CT_TRXOP_HOTLST_LOG_CALLBACK	TRXOP_HOTLST_LOG_CALLBACK
  	#define EMV_CT_TRXOP_LOCAL_CHCK_CALLBACK	TRXOP_LOCAL_CHCK_CALLBACK
	#define EMV_CT_TRXOP_CARDHINFO_CALLBACK	TRXOP_CARDHINFO_CALLBACK
#endif


// Define this if CARDAPP has to perform DOL length checks (EMV Kernel 5.0.6 doesn't do that)
//#define DOL_CHECKS

/***************************************************************************
 * Using
 **************************************************************************/
using namespace com_verifone_emv;
using namespace com_verifone_pml;


/***************************************************************************
 * External global variables
 **************************************************************************/
extern  CCardAppConfig   g_CardAppConfig;
extern CEMVtrns     g_CEMVtrns;
extern CEMV         g_CEMV;
extern cEMVcollection g_EMVcollection;


/***************************************************************************
 * Exportable function definitions
 **************************************************************************/
/*
int verify_pin( unsigned char    ucPinType,
                unsigned char    ucEntryType,
                unsigned short * pusStatus );
*/
//the above function is neceseary for compatibility with libverifypin library


extern logger g_logger;

void EMVlog(char const * msg)
{
	g_logger.Log("%s", msg);
}

int iPC_GenerateRandom( unsigned char * rnd)
{
    return GenerateRandom(rnd);
}

int inMenuFunc_mini(char **labels, int numLabels)
{
    dlog_msg("MenuFunc called!");
    return 0;
}

void vdPromptManager_mini(unsigned short inCondition)
{
    dlog_msg(">>>PROMPT MANAGER: %X", inCondition);
}

unsigned short usAmtEntryFunc_mini(unsigned long *lnAmountValue)
{
    dlog_msg("AmtEntryFunct called");
    *lnAmountValue = 0;
    return ESUCCESS;
}
int procEvents(eventset_t pml_events)
{
	int result = -1;
	while (!pml_events.empty())
	{
		event_item event = pml_events.back();
		pml_events.pop_back();
		if (event.event_id == events::cascade)
		{
			eventset_t cascadeEvents;
			if (event_read(event.evt.cascade.fd, cascadeEvents) > 0)
			{
				result = procEvents(cascadeEvents);
			}
		}
		else if (event.event_id == events::icc)
		{
			dlog_alert("Card removed!");
			g_CEMV.SetStatusDetail(CARD_APP_STATUS_D_REMOVED);
			g_CEMV.SetStatusMain(CARD_APP_STATUS_M_STOP);
			result = 1; // card removed
			break;
		}
		else if (event.event_id == events::ipc)
		{
			CCardAppConfig::cmd_break_t break_status = g_CardAppConfig.checkPipeStatus();
			if(break_status == CCardAppConfig::CMD_BREAK_CANCEL_E)
			{
				dlog_alert("App selection cancelled, exiting...");
				g_CEMV.SetStatusDetail(CARD_APP_STATUS_D_CANCELLED);
				g_CEMV.SetStatusMain(CARD_APP_STATUS_M_STOP);
				result = 1; // cancelled
				break;
			}
			else if (break_status == CCardAppConfig::CMD_BREAK_NO_BREAK_E)
			{
				dlog_msg("GUIAPP answer");
				result = 0;
				break;
			}
		}
	}
	return result;
}

int EntryCancelled(void *unused)
{
	//dlog_msg("Selection Cancelled!");
	//PML implementation here
	unsigned long ulEventValue = 0;
	int result = 1;
	int events_mon = event_open();
	eventset_t pml_events;
	int num_events = 0;
	event_item item_event;

	if(events_mon > 0)
	{
		event_item event;
		event.event_id = events::icc;
		event_ctl(events_mon, ctl::ADD, event);
		event_ctl(events_mon, ctl::ADD, com_verifone_ipc::get_events_handler());

		bool waiting = true;
		do
		{
			dlog_msg("App Selection - Wait for events");
			event_wait(events_mon, pml_events);
			if (pml_events.size())
			{
				result = procEvents(pml_events);
				if (result >= 0) waiting = false;
			}
		} while(waiting);
		event_close(events_mon);
	}
	return static_cast<short int>(result);
}

#ifdef VFI_GUI_GUIAPP

int inMenuFunc_GUIAPP(char **labels, int numLabels)
{
	int iRet = 0;

	g_CEMVtrns.send_notify(CARD_APP_EVT_APP_SELECT);

	for(int i= 0; i< numLabels; i++)
	{
		dlog_msg("Label %d - %s", i+1, labels[i]);
	}

	// TODO: Make application selection timeout configurable via some private tag!
	dlog_msg("Select application with timeout %d", g_CardAppConfig.GetApplicationSelectionTimeout()*1000);
	if( com_verifone_guiapi::GuiApp_Connect( (char *)CARD_APP_NAME, 10000 ) == com_verifone_guiapi::GUI_OKAY )
	{
		iRet = com_verifone_guiapi::GuiApp_SelectOption("", "", labels, numLabels, g_CardAppConfig.GetApplicationSelectionTimeout()*1000, EntryCancelled, NULL);
		if (iRet >= 0 && iRet < numLabels)
		{
			iRet++;
		}
		else
		{
			dlog_alert("Select application failed, error %d", iRet);
			if (iRet == com_verifone_guiapi::GUI_CANCELLED)
			{
				g_CEMV.SetStatusDetail(CARD_APP_STATUS_D_CANCELLED);
				g_CEMV.SetStatusMain(CARD_APP_STATUS_M_STOP);
			}
		}

		com_verifone_guiapi::GuiApp_ClearDisplay(0);
		if(g_CardAppConfig.GetUseDisplay())
		{
			com_verifone_guiapi::GuiApp_DisplayPromptSelectSection(CARD_APP_GUI_PLEASE_WAIT, CARD_APP_GUI_S_PROMPTS, 0, 0);
		}
		com_verifone_guiapi::GuiApp_Disconnect();
    }
	g_CEMVtrns.send_notify(CARD_APP_EVT_APP_SELECT_DONE);
	dlog_msg("Selected option: %d", iRet);

	return iRet;
}
#endif
int inMenuFunc_Default(char **labels, int numLabels)
{
	int iRet = 0;
	int i;
	event_item ev_ipc, ev_timer, ev_icc, ev_kbd;
	ev_ipc.event_id = events::ipc;
	ev_ipc.evt.com.fd = -1;
	ev_ipc.evt.com.flags = 0;
	int events_mon = event_open();
	ev_icc.event_id = events::icc;
	ev_kbd.event_id = events::keyboard;
	ev_timer.event_id = events::timer;

	eventset_t pml_events;

    dlog_msg("MenuFunc called for %d items!", numLabels);

    //Check for Kernel bug - after application selection, Kernel always calls this function, even if it's not necessary ...
    if (numLabels == 2)
    {
        if (!strcmp(labels[0], " ")) return 2;
        else if (!strcmp(labels[1], " ")) return 1;
    }

    g_CEMVtrns.send_notify(CARD_APP_EVT_APP_SELECT);

    for(i= 0; i< numLabels; i++)
        dlog_msg("Label %d - %s", i+1, labels[i]);


    if(1)
    {
        // dlog_error("%s NOT PRESENT", GUI_APP_NAME_);

		dlog_msg("Adding events::ipc ret :%X", iRet);
        const int maxLines = getScreenMaxY() > 9 ? 9 : getScreenMaxY(); // we can use numbers 1-9 only
        int start = 0;
        char buffer[DISPLAY_TEXT+1];
        buffer[DISPLAY_TEXT] = 0;
        for( i = 0; i < numLabels && i < maxLines; i++)
        {
            snprintf(buffer, DISPLAY_TEXT, "%d: %s\n", i+1, labels[i]);
            DisplayText(buffer, strlen(buffer), i == 0);
        }

		iRet = event_ctl(events_mon, ctl::ADD, ev_ipc);
		dlog_msg("Adding events::ipc ret :%X", iRet);

		iRet = event_ctl(events_mon, ctl::ADD, ev_icc);
		dlog_msg("Adding events::ipc ret :%X", iRet);

		iRet = event_ctl(events_mon, ctl::ADD, ev_kbd);
		dlog_msg("Adding events::ipc ret :%X", iRet);


        if (g_CardAppConfig.GetApplicationSelectionTimeout())
        {
			set_timer(ev_timer, (g_CardAppConfig.GetApplicationSelectionTimeout()*1000));
			event_ctl(events_mon, ctl::ADD, ev_timer);
        }
        char key_read = 0;
        bool entry_end = false;
        int m_Console = get_console_handle();
        while(!entry_end)
        {

			event_wait(events_mon, pml_events);
			if (pml_events.size())
			{
				for (eventset_t::iterator it = pml_events.begin() ; it != pml_events.end(); ++it)
				{
			   		if (it->event_id == events::timer)
			   		{
						dlog_alert("Application selection timed out!");
		                entry_end = true;
			   		}
					if (it->event_id == events::icc)
			   		{
			   			if(isCardRemoved())
			   			{
							dlog_alert("Card REMOVED! - CANCELING");
                			entry_end = 1;
		                	g_CEMV.SetStatusDetail(CARD_APP_STATUS_D_CANCELLED);
                			g_CEMV.SetStatusMain(CARD_APP_STATUS_M_STOP);
			   			}
			   		}
					if (it->event_id == events::keyboard)
			   		{
						if(read(m_Console, &key_read, 1) > 0)
	                	{
		                    key_read &= 0x7F;
		                    dlog_msg("KEY>>>> %X", key_read);
		                    if (key_read >= '1' && key_read <= '0'+(numLabels < maxLines ? numLabels : maxLines))
		                    {
		                        iRet = key_read+start-'0';
		                        entry_end = true;
		                    }
		                    else if (start > 0 && key_read == '*')
		                    {
		                        start -= maxLines;
		                        for( i = start; i < numLabels && i < start + maxLines; i++)
		                        {
		                            snprintf(buffer, DISPLAY_TEXT, "%d: %s\n", i-start+1, labels[i]);
		                            DisplayText(buffer, strlen(buffer), i == start);
		                        }
		                    }
		                    else if (start + maxLines < numLabels && key_read == '#')
		                    {
		                        start += maxLines;
		                        for( i = start; i < numLabels && i < start + maxLines; i++)
		                        {
		                            snprintf(buffer, DISPLAY_TEXT, "%d: %s\n", i-start+1, labels[i]);
		                            DisplayText(buffer, strlen(buffer), i == start);
		                        }
		                    }
		                    else
		                    {
		                        com_verifone_pml::error_tone();
		                    }
		                }
		            }

					if (it->event_id == events::ipc)
			   		{
						dlog_msg("Message received - checking if not cancel...");
                		CCardAppConfig::cmd_break_t break_status = g_CardAppConfig.checkCmdBreak();
		                if(break_status == CCardAppConfig::CMD_BREAK_CANCEL_E)
        		        {
                		    dlog_alert("Application selection cancelled, exiting...");
		                    entry_end = true;
        		            g_CEMV.SetStatusDetail(CARD_APP_STATUS_D_CANCELLED);
                		    g_CEMV.SetStatusMain(CARD_APP_STATUS_M_STOP);
                		}
			   		}

				}
			}

        }
        clrscr();
        clr_timer(ev_timer);
        if(g_CardAppConfig.GetUseDisplay())
        {
            DisplayPromptSelectSection(CARD_APP_GUI_PLEASE_WAIT, CARD_APP_GUI_S_PROMPTS);
        }

    }

    g_CEMVtrns.send_notify(CARD_APP_EVT_APP_SELECT_DONE);
    dlog_msg("Selected option: %d", iRet);
    event_close(events_mon);

    return iRet;
}


unsigned short usAmtEntryFunc_Default(unsigned long *lnAmountValue)
{
    dlog_msg("AmtEntryFunct called");
    *lnAmountValue = 0;
    return ESUCCESS;
}

CEMV::CEMV(void)
{
    status_main = CARD_APP_STATUS_M_CONTINUE | CARD_APP_STATUS_M_OK;
    status_detailed = CARD_APP_STATUS_D_OK;
    ICC_reader = 0;
    operation_reg = 0;
    term_decision = FAILED_TO_CONNECT;
    is_card_blacklisted = false;
    PIN_type = PIN_TYPE_PLAIN_TEXT;
    PIN_entry_type = ENTRY_TYPE_NORMAL;
    beeperThreadID = 0;
    PIN_status = PIN_ENTRY_STATUS_NOT_SET;
    //pPinDisplay = 0;
    external_pin_mode = -1;
}

bool CEMV::startICCMonitoring()
{
    unsigned long Value;
    int ret;

    /********* INIT: AT THE BEGINNING OF THE APPLICATION ********/
    /* Open ICC1 and reserve the memory : Tag 0x0188 */


	g_CEMVtrns.monitor_flags |= CARD_APP_MON_FOREVER;		//start monitoring automatically (to avoid componrnt starting problems)

	/*
    Value = CUSTOMER_CARD;
	
		
    ret = IFD_Set_Capabilities( Tag_Open_ICC, (BYTE*)(&Value));
    if(ret != IFD_Success)
    {
        dlog_error("** IFD_Set_Capabilities err %d **", ret);
		
        return false;
    }
*/

	//open_ICC();

    return true;
}

int CheckPINresult(unsigned char SW1, unsigned char SW2)
{
	int result = PIN_ENTRY_STATUS_ERROR; // Default to unrecoverable error
	switch(SW1)
	{
		case 0x90:
			if (SW2 == 0x00)
			{
				if (g_CardAppConfig.GetPINtype() != CARD_APP_EXTERNAL_PIN_OFFLINE)
				{
					g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_OK);
				}
				dlog_msg("PIN OK");
				EMVlog("PIN OK");
				result = PIN_ENTRY_STATUS_OK;
			}
			break;
		case 0x63:
			if ( (SW2 & 0xC0) == 0xC0)
			{
				unsigned char counter = SW2 & ~0xC0;
				g_EMVcollection.SetTLVData(TAG_9F17_PIN_TRIES_LEFT, &counter, sizeof(counter));
				if (g_CardAppConfig.GetPINtype() != CARD_APP_EXTERNAL_PIN_OFFLINE)
				{
					g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_WRONG);
				}
				dlog_msg("PIN Incorrect");
				EMVlog("PIN Incorrect");
				if(SW2 > 0xC1)
				{
					if (g_CardAppConfig.GetShowIncorrectPIN()) result = PIN_ENTRY_STATUS_BAD_PIN;
					else result = PIN_ENTRY_STATUS_OK;
				}
				else if(SW2 == 0xC1)
				{
					result = PIN_ENTRY_STATUS_LAST_TRY;
				}
				else if(SW2 == 0xC0)
				{
					result = PIN_ENTRY_STATUS_BLOCKED;
				}
			}
			break;
		case 0x69: // either 6983 or 6984
			if (SW2 == 0x83 || SW2 == 0x84) result = PIN_ENTRY_STATUS_BLOCKED;
			break;
		default:
			return PIN_ENTRY_STATUS_ERROR;
	}
	return result;
}

EMV_ADK_INFO InterpretEMVret(EMV_ADK_INFO EMV_ret, EMV_CT_TRANSAC_TYPE *pxTransactionInput)
{
	EMV_ADK_INFO Ret = EMV_ret;
	EMV_ADK_INFO loc_Ret;
	unsigned char pucPINResultData[32];

	int pin_ret = 0;

	pucPINResultData[0] = 0;
	pucPINResultData[1] = 0;

	//checking card presence
	if(EMV_CT_SmartDetect(0) != EMV_ADK_SMART_STATUS_OK)
	{
		//card removed or not redable
		EMV_ret = EMV_ADK_NO_CARD;
	}

	switch(EMV_ret)
	{
		case EMV_ADK_OK:
			dlog_msg("ADK_OK");
			EMVlog("EMV_CT_* ret: ADK_OK");
    		break;
		case EMV_ADK_APP_REQ_READREC:
			dlog_msg("0xA2 - Application requested return read records");
			EMVlog("EMV_CT_* ret: 0xA2 - Application requested return read records");
			g_CEMV.setResponse(EMV_ADK_OK, EMV_ADK_APP_REQ_READREC);
			Ret = EMV_ADK_OK;
			break;
		case EMV_ADK_APP_REQ_DATAAUTH:
			dlog_msg("0xA3 - Application requested return data authentication");
			EMVlog("EMV_CT_* ret: 0xA3 - Application requested return data authentication");
			g_CEMV.setResponse(EMV_ADK_OK, CARD_APP_OPER_DATA_AUTH);
			break;
		case EMV_ADK_APP_REQ_CVM_END:
			dlog_msg("0xA7 - Application requested return cardholder verification");
			EMVlog("EMV_CT_* ret: 0xA7 - Application requested return cardholder verification");
			/*
			// We have to notify users
			if(g_CardAppConfig.GetNotifyStatus())
			{
				// Check used CVM for offline PIN
				unsigned char cvmUsed[3];
				int len = 0;
				g_EMVcollection.GetTLVData(TAG_9F34_CVM_RESULTS, cvmUsed, &len);
				if (len == 3)
				{
					dlog_msg("CVM code used %02X, condition %02X, result %02X", cvmUsed[0], cvmUsed[1], cvmUsed[2]);
					unsigned char cvm = cvmUsed[0] & 0x3F; // filter out top bits
					if (((cvm == 0x05 || // Offline PIN + sig
						cvm == 0x01 || // Offline plaintext PIN
						cvm == 0x04)) && // Offline enciphered PIN
						cvmUsed[2] == 0x01) // Result is Failed
					{
						unsigned char pinTryCtr;
						int pinTryCtrLen = 0;
						g_EMVcollection.GetTLVData(TAG_9F17_PIN_TRIES_LEFT, &pinTryCtr, &pinTryCtrLen);
						if (pinTryCtrLen == 0)
						{
							g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_BLOCKED);
						}
					}
				}
				g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_DONE);
			}
			*/
			g_CEMV.setResponse(EMV_ADK_OK, CARD_APP_OPER_VERIFY);
			break;
		case EMV_ADK_APP_REQ_RISK_MAN:
			dlog_msg("0xA8 - Application requested return riskmanagement");
			EMVlog("EMV_CT_* ret: 0xA8 - Application requested return riskmanagement");
			g_CEMV.setResponse(EMV_ADK_OK, CARD_APP_OPER_RESTRICT|CARD_APP_OPER_TRM);
			break;
		case EMV_ADK_APP_REQ_ONL_PIN:
			dlog_msg("0xA4 - Application requested return for online PIN entry");
			EMVlog("EMV_CT_* ret: 0xA4 - Application requested return for online PIN entry");
			g_CEMV.SetStatusMain(CARD_APP_STATUS_M_PIN);
			break;
		case EMV_ADK_APP_REQ_OFL_PIN:
			dlog_msg("0xA5 - Application requested return for offline PIN entry");
			EMVlog("EMV_CT_* ret: 0xA5 - Application requested return for offline PIN entry");
			pin_ret = g_CEMV.OfflinePINVerification();
			g_logger.Log("OfflinePINVerification ret: %d", pin_ret);
			if(pin_ret == PIN_VERIFY_OK)
			{
				/*
				loc_Ret = EMV_CT_Send_PIN_Offline(pucPINResultData);
				dlog_msg("Sending PIN to the card ret %X SW1:%X SW2:%X", loc_Ret, pucPINResultData[0], pucPINResultData[1]);
				g_CEMV.SetPINStatus(CheckPINresult(pucPINResultData[0], pucPINResultData[1]));
				g_logger.Log("EMV_CT_Send_PIN_Offline ret: %X  SW1SW2: 02.02%X%02.02X", loc_Ret, pucPINResultData[0], pucPINResultData[1]);
				*/
				EMVlog("PIN OK!");
			}
			else if(pin_ret == PIN_VERIFY_BYPASS)
			{
				pxTransactionInput->TxnSteps[2] = MS_PIN_BYPASS;
				pxTransactionInput->Info_Included_Data[0] |= INPUT_OFL_TXN_STEPS;
				EMVlog("PIN entry bypass");
			}
			else if(pin_ret == PIN_VERIFY_CANCEL || pin_ret == PIN_VERIFY_POS_CANCEL)
			{
				g_CEMVtrns.cmd_status = RESP_CODE_PIN_CANCEL;
				Ret = EMV_ADK_CVM;
				EMVlog("PIN entry cancelled setting resp code to RESP_CODE_PIN_CANCEL");
			}
			else if(pin_ret == PIN_VERIFY_CARD_REMOVED)
			{
				g_CEMVtrns.cmd_status = RESP_CODE_CARD_REMOVED;
				Ret = EMV_ADK_CVM;
				EMVlog("PIN entry card removed setting resp code to RESP_CODE_CARD_REMOVED");
			}
			else
			{
				Ret = EMV_ADK_CVM;
				EMVlog("Cardholder Aborted (on PIN entry)");
			}
			break;
		case EMV_ADK_APP_REQ_PLAIN_PIN:
			dlog_msg("0xA6 - Application requested return for plaintext PIN entry");
			EMVlog("EMV_CT_* ret: 0xA6 - Application requested return for plaintext PIN entry");
			pin_ret = g_CEMV.OfflinePINVerification();
			g_logger.Log("OfflinePINVerification ret: %d", pin_ret);
			if(pin_ret == PIN_VERIFY_OK)
			{
				/*
				loc_Ret = EMV_CT_Send_PIN_Offline(pucPINResultData);
				dlog_msg("Sending PIN to the card ret %X SW1:%X SW2:%X", loc_Ret, pucPINResultData[0], pucPINResultData[1]);
				g_CEMV.SetPINStatus(CheckPINresult(pucPINResultData[0], pucPINResultData[1]));
				char msg[100];
				sprintf(msg, "EMV_CT_Send_PIN_Offline ret: %X SW1SW2: %X%X", loc_Ret, pucPINResultData[0], pucPINResultData[1]);
				*/
				EMVlog("PIN OK!");
				
			}
			else if(pin_ret == PIN_VERIFY_BYPASS)
			{
				pxTransactionInput->TxnSteps[2] = MS_PIN_BYPASS;
				pxTransactionInput->Info_Included_Data[0] |= INPUT_OFL_TXN_STEPS;
				EMVlog("PIN entry bypass");
			}
			else if(pin_ret == PIN_VERIFY_CANCEL || pin_ret == PIN_VERIFY_POS_CANCEL)
			{
				g_CEMVtrns.cmd_status = RESP_CODE_PIN_CANCEL;
				Ret = EMV_ADK_CVM;
				EMVlog("PIN entry cancelled setting resp code to RESP_CODE_PIN_CANCEL");
			}
			else if(pin_ret == PIN_VERIFY_CARD_REMOVED)
			{
				g_CEMVtrns.cmd_status = RESP_CODE_CARD_REMOVED;
				Ret = EMV_ADK_CVM;
				EMVlog("PIN entry card removed setting resp code to RESP_CODE_CARD_REMOVED");
			}
			else
			{
				Ret = EMV_ADK_CVM;
				EMVlog("Cardholder Aborted (on PIN entry)");
			}
			break;
		case EMV_ADK_APP_REQ_END:
			dlog_msg("0xAF - Application requested return end of reserved codes");
			EMVlog("EMV_CT_* ret: 0xAF - Application requested return end of reserved codes");
			break;
		default:
			dlog_msg("%X - Other response", EMV_ret);
			g_logger.Log("EMV_CT_* ret: %X %s", EMV_ret, ADK_err(EMV_ret).c_str());
			break;


	}

	return Ret;
}

int CEMV::checkStatus(int operation)
{
    int mask;
    int result;

    //checking if mandatory steps were done
    switch(operation)
    {
        case CARD_APP_OPER_SELECT:
            dlog_msg("Clearing statuses for new transaction...");
            ClearStatus();
            g_EMVcollection.ClearEverything();
        case CARD_APP_OPER_TLV:
            mask = 0;
            break;
        case CARD_APP_OPER_GPO:
            mask = CARD_APP_OPER_SELECT;
            break;
        case CARD_APP_OPER_READ:
            mask = CARD_APP_OPER_SELECT | CARD_APP_OPER_GPO;
            break;
        case CARD_APP_OPER_DATA_AUTH:
        case CARD_APP_OPER_VERIFY:
        case CARD_APP_OPER_RESTRICT:
        case CARD_APP_OPER_TRM:
            mask = CARD_APP_OPER_SELECT | CARD_APP_OPER_GPO | CARD_APP_OPER_READ;
            break;
        case CARD_APP_OPER_FIRST_AC:
            mask = CARD_APP_OPER_SELECT | CARD_APP_OPER_GPO | CARD_APP_OPER_READ |
                    CARD_APP_OPER_DATA_AUTH | CARD_APP_OPER_VERIFY | CARD_APP_OPER_TRM | CARD_APP_OPER_RESTRICT;
            break;
        case CARD_APP_OPER_EXT_AUTH:
        case CARD_APP_OPER_SECOND_AC:
        case CARD_APP_OPER_SCRIPT:
            mask = CARD_APP_OPER_SELECT | CARD_APP_OPER_GPO | CARD_APP_OPER_READ |
                    CARD_APP_OPER_DATA_AUTH | CARD_APP_OPER_VERIFY | CARD_APP_OPER_TRM | CARD_APP_OPER_RESTRICT| CARD_APP_OPER_FIRST_AC;
            break;
        default:
            mask = 0xFFFF;
            break;
    }

    //checking if card is in
    if (isCardRemoved())
    {
        // Clear TLV pool
        SetStatusMain(CARD_APP_STATUS_M_ERROR | CARD_APP_STATUS_M_STOP);
        SetStatusDetail(CARD_APP_STATUS_D_REMOVED | CARD_APP_STATUS_D_NO_FALLBACK);
        return CARD_APP_RET_REMOVED;
    }
    //checking if card is PROPER
    else if (g_CEMVtrns.icc_card_type != CARD_APP_CARDTYPE_EMV_SYNC && g_CEMVtrns.icc_card_type != CARD_APP_CARDTYPE_EMV_ASYNC)
    {
        // Bad card is inserted
        dlog_error("Improper card inserted (%d)", g_CEMVtrns.icc_card_type);
        SetStatusMain(CARD_APP_STATUS_M_ERROR | CARD_APP_STATUS_M_STOP);
        SetStatusDetail(CARD_APP_STATUS_D_CHIP_ERROR | CARD_APP_STATUS_D_NO_FALLBACK);
        return CARD_APP_RET_ICC_FAIL;
    }

    //checking if transaction can be continued
    if(GetStatusMain() & CARD_APP_STATUS_M_STOP)
    {
        dlog_msg("checkStatus:Transaction can not be continued:CARD_APP_STATUS_M_STOP");

        if (g_CardAppConfig.GetUseDisplay())
        {
            if(1)
            {
                DisplayPromptSelectSection(CARD_APP_GUI_REMOVE, CARD_APP_GUI_S_PROMPTS);
            }
        }

        mask = 0xFFFF;
        // g_CEMVtrns.icc_card_type = CARD_APP_CARDTYPE_NOT_SET;
        if(GetStatusDetail() & CARD_APP_STATUS_D_REMOVED)
        {
            dlog_msg("Closing CardSlot");
            g_CEMVtrns.Close_Kernel();
        }
    }
    // checking if aborted
    else
    {
        std::string source_task;
        if(com_verifone_ipc::check_pending(source_task) == com_verifone_ipc::IPC_SUCCESS) //check if message waits
        {
            CCardAppConfig::cmd_break_t break_status = g_CardAppConfig.checkCmdBreak();
            if(break_status == CCardAppConfig::CMD_BREAK_CANCEL_E)
            {
                dlog_alert("Transaction cancelled by POS, exiting...");
                SetStatusDetail(CARD_APP_STATUS_D_CANCELLED);
                return CARD_APP_RET_CANCELED;
            }
        }
    }

    dlog_msg("Operations register: %X, mask %X", operation_reg, mask);
    result = ((operation_reg & mask) == mask);

    dlog_msg("Checking operation:%X, result:%d", operation, result);
    if (result == 0) return CARD_APP_RET_ORDER;
    return CARD_APP_RET_OK;
}



int CEMV::setResponse(int EMVModuleRet, int operation)
{
    int result = CARD_APP_RET_OK;
    if (EMVModuleRet != CARD_APP_RET_OK)
    {
        if (isCardRemoved())
        {
            dlog_msg("Card removed... closing...");
            EMVModuleRet = EMV_ADK_NO_CARD;
        }
    }
    switch(EMVModuleRet)
    {
        case EMV_ADK_OK:
        case EMV_ADK_ARQC:
        case EMV_ADK_APP_REQ_DATAAUTH:
        case EMV_ADK_APP_REQ_RISK_MAN:
        case EMV_ADK_APP_REQ_CVM_END:
            SetStatusMain(CARD_APP_STATUS_M_OK | CARD_APP_STATUS_M_CONTINUE);
            SetStatusDetail(CARD_APP_STATUS_D_OK | CARD_APP_STATUS_D_NO_FALLBACK);
            result = CARD_APP_RET_OK;
            break;
        case EMV_ADK_AAC:
        case EMV_ADK_TC:
            SetStatusMain(CARD_APP_STATUS_M_OK | CARD_APP_STATUS_M_STOP);
            SetStatusDetail(CARD_APP_STATUS_D_OK | CARD_APP_STATUS_D_NO_FALLBACK);
            result = CARD_APP_RET_OK;
            break;
        case EMV_ADK_NO_CARD:
            SendMonitorMsg(RESP_CODE_CARD_REMOVED);
            //ICCoutEvent();
        case EMV_ADK_CVM:
            SetStatusMain(CARD_APP_STATUS_M_OK | CARD_APP_STATUS_M_STOP);
            if (isCardRemoved())
            {
                SetStatusDetail(CARD_APP_STATUS_D_REMOVED | CARD_APP_STATUS_D_NO_FALLBACK);
                result = CARD_APP_RET_REMOVED;
            }
            else
            {
                SetStatusDetail(CARD_APP_STATUS_D_CANCELLED | CARD_APP_STATUS_D_NO_FALLBACK);
                result = CARD_APP_RET_CANCELED;
            }
            break;
        case EMV_ADK_APP_BLOCKED:
            SetStatusMain(CARD_APP_STATUS_M_OK | CARD_APP_STATUS_M_STOP);
            SetStatusDetail(CARD_APP_STATUS_D_APPL_BLOCKED| CARD_APP_STATUS_D_NO_FALLBACK);
            result = CARD_APP_RET_APP_BLOCKED;
            break;
        case EMV_ADK_CARD_BLOCKED:
            SetStatusMain(CARD_APP_STATUS_M_OK | CARD_APP_STATUS_M_STOP);
            SetStatusDetail(CARD_APP_STATUS_D_CARD_BLOCKED| CARD_APP_STATUS_D_NO_FALLBACK);
            result = CARD_APP_RET_CARD_BLOCKED;
            break;
        case -100: //invalid parameter:
            SetStatusMain(CARD_APP_STATUS_M_ERROR| CARD_APP_STATUS_M_STOP);
            SetStatusDetail(CARD_APP_STATUS_D_PARAMETER | CARD_APP_STATUS_D_FALLBACK);
            result = CARD_APP_RET_PARAM;
            break;
        case EMV_ADK_CARDERR:
            SetStatusMain(CARD_APP_STATUS_M_ERROR | CARD_APP_STATUS_M_STOP);
            SetStatusDetail(CARD_APP_STATUS_D_CHIP_ERROR | CARD_APP_STATUS_D_FALLBACK);
            result = CARD_APP_RET_ICC_FAIL;
            break;
        case EMV_ADK_ABORT:
            SetStatusMain(CARD_APP_STATUS_M_OK | CARD_APP_STATUS_M_STOP);
            SetStatusDetail(CARD_APP_STATUS_D_CHIP_ERROR | CARD_APP_STATUS_D_NO_FALLBACK);
            result = CARD_APP_RET_CANCELED;
            break;
        case EMV_ADK_FALLBACK:
            SetStatusMain(CARD_APP_STATUS_M_OK | CARD_APP_STATUS_M_STOP);
            SetStatusDetail(CARD_APP_STATUS_D_EMPTY | CARD_APP_STATUS_D_FALLBACK);
            result = CARD_APP_RET_NO_APPS;
            break;
        case EMV_ADK_BADAPP:
            SetStatusMain(CARD_APP_STATUS_M_ERROR | CARD_APP_STATUS_M_STOP);
            SetStatusDetail(CARD_APP_STATUS_D_CHIP_ERROR | CARD_APP_STATUS_D_FALLBACK);
            result = CARD_APP_RET_ICC_FAIL;
            break;
        case EMV_ADK_NOAPP:
            SetStatusMain(CARD_APP_STATUS_M_ERROR | CARD_APP_STATUS_M_STOP);
            SetStatusDetail(CARD_APP_STATUS_D_EMPTY | CARD_APP_STATUS_D_FALLBACK);
            result = CARD_APP_RET_NO_APPS;
            break;
        default:
            SetStatusMain(CARD_APP_STATUS_M_ERROR | CARD_APP_STATUS_M_CONTINUE);
            SetStatusDetail(CARD_APP_STATUS_D_CHIP_ERROR | CARD_APP_STATUS_D_NO_FALLBACK);
            result = CARD_APP_RET_SELECT_AGAIN;
            break;
    }
    operation_reg |= operation;
    dlog_msg("setResponse: Ret: %X, operation: %X", EMVModuleRet, operation);
    dlog_msg("setResponse:Operation register: %X (adding: %X)", operation_reg, operation);
    dlog_msg("setResponse:Status: %0000000X:%0000000X", GetStatusMain(), GetStatusDetail());
    return result;

}

int getData_loc(int tagNum,unsigned char * value,int * Len)
{

	/*
	unsigned short tlvDataLength;
	unsigned long reqTags[1];

	char tmp[100];

	reqTags[0] = static_cast<unsigned long>(tagNum);
	if(EMV_CT_fetchTxnTags(EMV_ADK_FETCHTAGS_NO_EMPTY, reqTags, 1, value, 100, &tlvDataLength) == EMV_ADK_OK)
	{
		EMVlog("EMV_CT_fetchTxnTags OK");
		sprintf(tmp, "tag %X len %d", reqTags[0], tlvDataLength);
		EMVlog(tmp);
		*Len = (int)tlvDataLength;
		return (int)tlvDataLength;
	}
	else
	{
		EMVlog("EMV_CT_fetchTxnTags NOK");
		*Len = 0;
		return 0;
	}
	*/

	return g_EMVcollection.GetTLVData(tagNum, value, Len);
}

int setData_loc(int tagNum,unsigned char * value,int Len)
{
	return g_EMVcollection.SetTLVData(tagNum, value, Len);
}


bool isCardpresent_loc()
{
    return (EMV_CT_SmartDetect(0) == EMV_ADK_SMART_STATUS_OK);
}

break_ret_t checkBreak_AppSel_loc()
{
	CCardAppConfig::cmd_break_t break_status = g_CardAppConfig.checkPipeStatus();
	break_ret_t result;
	switch (break_status)
	{
		case CCardAppConfig::CMD_BREAK_CANCEL_E: result = Cancelled; break;
		case CCardAppConfig::CMD_BREAK_GUI_RESP_E: result = GuiAppData; break;
		default: // Ignore other stuff
		case CCardAppConfig::CMD_BREAK_NO_BREAK_E: result = NoData; break;
	}
	return result;
}

break_ret_t checkBreak_PIN_loc()
{
	CCardAppConfig::cmd_break_t break_status = g_CardAppConfig.checkCmdBreak();
	break_ret_t result;
	switch (break_status)
	{
		case CCardAppConfig::CMD_BREAK_CANCEL_E: result = Cancelled; break;
		case CCardAppConfig::CMD_BREAK_PIN_BYPASS_E: result = Bypassed; break;
		default:
		case CCardAppConfig::CMD_BREAK_NO_BREAK_E: result = NoData; break;
	}
	return result;
}


int EntryCallBack_ccard(int num_digits, int status, char* filename, char* filename_warning)
{
	std::string empty;
	int iRet = 0;

	dlog_msg("Number of valid PIN digits: %d, entry status: %d, PIN status: %d", num_digits, status, g_CEMV.GetPINStatus());
	if(status != 0xFF)
	{
		std::string htmlFile = g_CardAppConfig.GetPINHTML(static_cast<size_t>(num_digits));
		if (htmlFile.size() == 0)
		{
			htmlFile = g_CardAppConfig.GetDefaultPINHTML();
		}
		// HTML has to be defined here... but just in case let's make sure
		if (htmlFile.size() == 0)
		{
			htmlFile.assign("EnterPINstart.html");
		}
		strcpy(filename, htmlFile.c_str());
		dlog_msg("Displaying HTML: %s", filename);

		filename_warning[0] = 0;
		if (g_CardAppConfig.GuiInterface() == CCardAppConfig::GUI_FLEXI)
		{
			switch(g_CEMV.GetPINStatus())
			{
				case PIN_ENTRY_STATUS_BAD_PIN:
					strcpy(filename_warning, "IncorrectPINextra.html");
					break;
				case PIN_ENTRY_STATUS_LAST_TRY:
					strcpy(filename_warning, "LastPINextra.html");
					break;
			}
		}
	}
	else
	{
		g_logger.Log("PIN digit entered (total: %d digits)", num_digits);
		g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_DIGIT, (unsigned char *)num_digits, sizeof(int), true);
	}

	

	return iRet;
}


int CEMV::GetChallenge(unsigned char * icc_unpredictable)
{
	unsigned char GetChallengeCmd[] = "\x00\x84\x00\x00\x00";
	unsigned short GetChallengeCmdSize = sizeof(GetChallengeCmd)-1;
	unsigned short ICC_response_length = 0;
	unsigned char ICC_response[CARD_APP_MAX_APDU];
	unsigned char reader_num = 0;
	int result = EMV_CT_SmartISO(reader_num, GetChallengeCmdSize, GetChallengeCmd, &ICC_response_length, ICC_response,1024);
	if (result != EMV_ADK_OK)
	{
		dlog_msg("Get Challenge error %d", result);
		dlog_msg("%s", ADK_err(result).c_str());
		return 0xFFFF;
	}
	if (ICC_response_length == 10)
	{
		result = ICC_response[8] << 8;
		result |= ICC_response[9];
		dlog_msg("Get Challenge succeeded, SW1SW2 %04Xh", result);
		dlog_hex(ICC_response, ICC_response_length, "ICC response");
		if (result == 0x9000)
		{
			memcpy(icc_unpredictable, ICC_response, ICC_UNPREDICTABLE_SIZE);
		}
	}
	else if (ICC_response_length == 2)
	{
		result = ICC_response[0] << 8;
		result |= ICC_response[1];
		dlog_error("Get Challenge failed, SW1SW2 %04Xh", result);
	}
	else
	{
		dlog_error("Invalid Get Challenge response: length %d", ICC_response_length);
		result = 0xFFFF;
	}
	return result;
}

#define MAX_AID_SIZE 10

int CEMV::OfflinePINVerification()
{
	int Ret;
	bool PIN_bypass_unlocked=true;

	bool full_verify = true;

	cPINdisplay * pPINdisplay;

	#ifdef VFI_GUI_GUIAPP
	pPINdisplay = new cPINdisplayGuiApp;
	#endif

	#ifdef VFI_GUI_DIRECTGUI
	if (g_CardAppConfig.GuiInterface() == CCardAppConfig::GUI_FLEXI)
	{

		//support for flexi PIn in direct gui
		pPINdisplay = new cPINdisplayFlexi(g_CardAppConfig.GetPINbeeperTimeout());
		dlog_msg("Using cPINdisplayFlexi for PIN entry ptr:%X", pPINdisplay);
	}
	else
	{
		pPINdisplay = new cPINdisplayDirectGUI(g_CardAppConfig.GetPINbeeperTimeout());
		dlog_msg("Using DirectGUI for PIN entry ptr:%X", pPINdisplay);
		if(CEMV::PIN_html.size())
		{
			dlog_msg("Setting PIN entry html file: %s", CEMV::PIN_html.c_str());
			pPINdisplay->SetPINhtml(CEMV::PIN_html);
		}
	}
	#endif

	if(pPINdisplay == NULL)
		return PIN_VERIFY_ERROR;

	c_encPIN encPIN;
	PINPARAMETER psKeypadSetup;

	PIN_try++;

	if(PIN_entry_type == ENTRY_TYPE_ATOMIC)
	{
		full_verify = false;
	}

	do
	{
		if (g_CardAppConfig.GetPINtype() != CARD_APP_EXTERNAL_PIN_OFFLINE)
		{
			if(g_CEMV.GetPINStatus() == PIN_ENTRY_STATUS_LAST_TRY)
			{
				g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_LAST);
			}
			else if(g_CEMV.GetPINStatus() == PIN_ENTRY_STATUS_BLOCKED)
			{
				g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_BLOCKED);
				dlog_error("PIN is blocked!");
				delete pPINdisplay;
				return PIN_VERIFY_BLOCKED;
			}
			else
			{
				g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_ENTRY);
			}

			psKeypadSetup.ucMin = g_CardAppConfig.GetMinPINLen();
			psKeypadSetup.ucMax = g_CardAppConfig.GetMaxPINLen();
			psKeypadSetup.ucEchoChar = '*';
			psKeypadSetup.ucDefChar = g_CardAppConfig.GetPINblankChar();
			psKeypadSetup.ucOption = g_CardAppConfig.GetPINoption(); // Bit 3 is set by default
			/*if (g_CardAppConfig.GetOnlinePINCancelAllowed())

			{

				psKeypadSetup.ucOption |= 0x02; // bit 1

			}

			if (g_CardAppConfig.GetOnlinePINEntryType() != CCardAppConfig::PIN_ENTRY_TYPE_MANDATORY_E)

			{

				psKeypadSetup.ucOption |= 0x10; // bit 4

			}*/

			//checking country specific requirements for pin bypass
			std::string cntr_req = g_CardAppConfig.GetCountryReq();
			if(cntr_req.size())
			{
				dlog_msg("Additional PIN bypass requirements for: %s", cntr_req.c_str());
				g_logger.Log("Additional PIN bypass requirements for: %s", cntr_req.c_str());
				if(cntr_req.find(ISO2_SPAIN, 0, strlen(ISO2_SPAIN)) != std::string::npos)
				{
					PIN_bypass_unlocked=false;
					std::string cntr_params = g_CardAppConfig.GetCountryParams();
					//spanish market requires checking AID's for pin bypass
					int index_start = 0;
					int index_end = 0;
					std::vector<std::string> params;
					std::string AID_str;
					int Len = 0xFF;
					unsigned char AID_proc[MAX_AID_SIZE];
					unsigned char AID_list[MAX_AID_SIZE];

					g_EMVcollection.GetTLVData(TAG_9F06_AID, AID_proc, &Len);
					g_logger.Log("Processing pin bypass rules for Spain... AID: %02.02X%02.02X%02.02X%02.02X%02.02X%02.02X%02.02X", AID_proc[0],AID_proc[1],AID_proc[2],AID_proc[3],AID_proc[4],AID_proc[5],AID_proc[6]);
					while((index_end = cntr_params.find(CARDAPP_LIST_SEPARATOR, index_start, strlen(CARDAPP_LIST_SEPARATOR))) != std::string::npos)
					{
						AID_str = cntr_params.substr(index_start, index_end-index_start);
						svcDsp2Hex(AID_str.c_str(), AID_str.size(), (char *)AID_list, MAX_AID_SIZE);
						
						g_logger.Log("Checking AID: %s", AID_str.c_str());
						if(memcmp(AID_list, AID_proc, Len) == 0)
						{
							g_logger.Log("PIN entry bypass allowed for %s", AID_str.c_str());
							PIN_bypass_unlocked = true;
						}
						index_start = index_end+1;
					}


				}
			}
			else
			{
				g_logger.Log("NO country specific requirements for PIN entry");

			}

			if (g_CardAppConfig.IsPINBypassEnabled() && PIN_bypass_unlocked)
			{
				unsigned char bypassKey = g_CardAppConfig.GetPINBypassKey();
				if (bypassKey == 0x08) // backspace
				{
					psKeypadSetup.ucOption |= 0x10; // bit 4
				}
				else
				{
					psKeypadSetup.ucOption |= 0x02; // bit 1
				}
				pPINdisplay->setPINparams(psKeypadSetup, bypassKey);
				dlog_msg("PIN bypass enabled bypass key: 0x%X", bypassKey);
			}
			else
			{
				pPINdisplay->setPINparams(psKeypadSetup);
				psKeypadSetup.ucOption &= 0xFD;	//clearing bit 1
				dlog_msg("PIN bypass disabled");
			}

			dlog_msg("Offline PIN entry, flags %02X", psKeypadSetup.ucOption);
			pPINdisplay->setRadixSeparator(g_CardAppConfig.GetPINRadixSeparator());
			pPINdisplay->setCurrSymbolLeft(g_CardAppConfig.PINDisplayCurrencySymbolLeft(false));
			pPINdisplay->setDispCurrSymbol(g_CardAppConfig.GetPINDisplayCurrencySymbol());
			dlog_msg("Offline PIN entry timeouts: first %d, next %d, global %d", g_CardAppConfig.GetPINfirstcharTimeout(),
										g_CardAppConfig.GetPINintercharTimeout(), g_CardAppConfig.GetPINtimeout());
			pPINdisplay->setPINtimeouts(g_CardAppConfig.GetPINfirstcharTimeout()*1000,
										g_CardAppConfig.GetPINintercharTimeout()*1000,
										g_CardAppConfig.GetPINtimeout()*1000);


			pPINdisplay->setTLVget(getData_loc);
			pPINdisplay->setTLVset(setData_loc);
			pPINdisplay->setCheckBreak(checkBreak_PIN_loc);
			pPINdisplay->setICCpresent(isCardpresent_loc);
			pPINdisplay->setEntryCallBack(EntryCallBack_ccard);

			pPINdisplay->SetPINprogress(g_CEMV.GetPINStatus());

			if ((g_CardAppConfig.GetOfflinePINEntryStyle() & CARD_APP_PIN_NO_AMOUNT) == CARD_APP_PIN_NO_AMOUNT)
			{
				pPINdisplay->SetAmtMsg(" ");
			}
			if (g_CardAppConfig.cust_language.size()) pPINdisplay->SetLanguage(g_CardAppConfig.cust_language);
			//if(PINdisplay.Display(encPIN) == 0)
			{
				pPINdisplay->ComputeEntryXY();
				if(g_CardAppConfig.GetUsePINwarnings())
				{
					if (g_CardAppConfig.GuiInterface() == CCardAppConfig::GUI_FLEXI)
					{
						switch(g_CEMV.GetPINStatus())
						{
							case PIN_ENTRY_STATUS_BAD_PIN:
								pPINdisplay->SetWarningMsg(g_CardAppConfig.GetIncorrectPINMsg());
								break;
							case PIN_ENTRY_STATUS_LAST_TRY:
								pPINdisplay->SetWarningMsg(g_CardAppConfig.GetLastPINMsg());
								break;
						
						}
					}
				}
				else
				{
					pPINdisplay->SetWarningMsg("");
				}
				pPINdisplay->AddHTMLvars(g_EMVcollection.HTML_labels);
				Ret = pPINdisplay->GetPIN(encPIN);
				dlog_msg("GetPIN result %d", Ret);
				g_logger.Log("GetPIN result %d", Ret);
				pPINdisplay->Clear();
				if(Ret == PIN_VERIFY_BYPASS || Ret == PIN_VERIFY_POS_BYPASS)
				{
					g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_BYPASSED);
					dlog_msg("PIN entry bypassed");
					EMVlog("PIN entry bypassed");
				}
				else if(Ret == PIN_VERIFY_CANCEL || Ret == PIN_VERIFY_POS_CANCEL)
				{
					g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_CANCELLED);
					dlog_msg("PIN entry cancelled");
					EMVlog("PIN entry cancelled");
				}
				else if(Ret == PIN_VERIFY_TIMEOUT)
				{
					g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_TIMEOUT);
					dlog_msg("PIN entry timeout");
					EMVlog("PIN entry timeout");
				}
				else if(Ret == PIN_VERIFY_ABORT)
				{
					g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_CANCELLED);
					dlog_msg("PIN entry abort, error occurred!");
					EMVlog("PIN entry abort, error occurred!");
				}
				else if(Ret == PIN_VERIFY_CARD_REMOVED)
				{
					g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_CANCELLED);
					dlog_msg("PIN entry abort, card removed!");
					EMVlog("PIN entry abort, card removed!");
				}
				else
				{
					dlog_msg("PIN entry done");
					EMVlog("PIN entry done");
				}
			}
		}
		else
		{
			encPIN.addExternalPIN(external_pin.c_str(), external_pin.size());
			encPIN.addExternalPINMode(external_pin_mode);
			external_pin.clear();
			Ret = PIN_VERIFY_OK;
		}

		
		//checking pin for external kernels
		if (g_CardAppConfig.GetPINtype() == CARD_APP_PIN_OFFLINE_EXT || g_CardAppConfig.GetPINtype() == CARD_APP_EXTERNAL_PIN_OFFLINE)
		{
			if(Ret == PIN_VERIFY_OK)
			{
				if(PIN_type == PIN_TYPE_ENCIPHERED)
				{
					// we need the following tags present. if not present then return error
					//
					// (9f47 OR 9f2e) AND df0e
					//
					// where:
					// 9f47 = ICC Public Key Exponent
					// 9f2e = ICC PIN Encipherment Public Key Exponent
					// df0e = Public key modulus

					static unsigned char modulus[MAX_MODULUS_LENGTH];
					static int modulus_length = 0;
					static unsigned char exponent[4];
					static int exponent_length = 0;

					if( g_EMVcollection.GetTLVData(TAG_9F47_ICC_PK_EXP, exponent, &exponent_length) > 0 )
					{
						dlog_msg("using tag 9f47");
					}
					else if( g_EMVcollection.GetTLVData(TAG_9F2E_ICC_PIN_PK_EXP, exponent, &exponent_length) > 0)
					{
						dlog_msg("using tag 9f2e");
					}
					else
					{
						// couldn't find either, return error
						dlog_error("error tag 9f47 or 9f2e not present");
					}

					if( g_EMVcollection.GetTLVData(0xdf0e, modulus, &modulus_length) <= 0 )
					{
						dlog_error("error tag df0e not present");
					}
					dlog_msg("modulus len=%d, exponent len=%d", modulus_length, exponent_length);

					encPIN.addExponent(exponent, exponent_length);
					encPIN.addModulus(modulus, modulus_length);

					// Kamil_P1: We need to issue Get Challenge
					unsigned char icc_unpredictable_num[ICC_UNPREDICTABLE_SIZE];
					Ret = GetChallenge(icc_unpredictable_num);
					if (Ret != 0x9000)
					{
						int PIN_result = PIN_ENTRY_STATUS_ERROR;
						SetPINEntryStatus(PIN_result);
						SetPINStatus(PIN_result);
						unsigned char sw1sw2_buf[2];
						sw1sw2_buf[0] = static_cast<unsigned char>((Ret & 0xFF00) >> 8);
						sw1sw2_buf[1] = static_cast<unsigned char>(Ret & 0x00FF);
						setData_loc(TAG_DF6F_CBK_PIN_ICC_RESP, sw1sw2_buf, 2);
						Ret = PIN_VERIFY_GET_CHALLENGE;
						break; // We can terminate PIN entry here
					}
					encPIN.addICCrandom(icc_unpredictable_num, ICC_UNPREDICTABLE_SIZE);

				}
				Ret = encPIN.VerifyPIN();
				unsigned char sw1sw2_buf[2];
				sw1sw2_buf[0] = static_cast<unsigned char>((Ret & 0xFF00) >> 8);
				sw1sw2_buf[1] = static_cast<unsigned char>(Ret & 0x00FF);
				setData_loc(TAG_DF6F_CBK_PIN_ICC_RESP, sw1sw2_buf, 2);
				// setData_loc(0xDFDF09, sw1sw2_buf, 2); // eVo compatibility

				if( Ret == 0x9000)
				{
					Ret = PIN_VERIFY_OK;
					g_CEMV.SetPINEntryStatus(PIN_ENTRY_STATUS_OK);
					g_CEMV.SetPINStatus(PIN_ENTRY_STATUS_OK);
				}
				else
				{
					//g_CEMV.SetPINEntryStatus(CheckPINresult((unsigned char)(Ret & 0xFF), (unsigned 	char)(Ret/0x100)));
					//Ret = PIN_VERIFY_ERROR;
					//EMVlog("Incorrect PIN");
					int PIN_result = CheckPINresult((unsigned char)(Ret/0x100), (unsigned char)(Ret&0xFF));
					g_CEMV.SetPINEntryStatus(PIN_result);
					g_CEMV.SetPINStatus(PIN_result);
					if (PIN_result == PIN_ENTRY_STATUS_BLOCKED) Ret = PIN_VERIFY_BLOCKED;
					else if (PIN_result == PIN_ENTRY_STATUS_ERROR) Ret = PIN_VERIFY_ABORT;
					// else if (PIN_result == PIN_ENTRY_STATUS_LAST_TRY) Ret = PIN_VERIFY_LAST_TRY;
					else Ret = PIN_VERIFY_ERROR;
				}
				g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_DONE);
			}
			else
			{
				// Error occurred, PIN not entered properly, so reset flag
				SetPINStatus(PIN_ENTRY_STATUS_NOT_SET);
			}
		}
		else if (g_CardAppConfig.GetPINtype() == CARD_APP_SECURE_PIN_OFFLINE || g_CardAppConfig.GetPINtype() == CARD_APP_PIN_OFFLINE)
		{
			if(Ret == PIN_VERIFY_OK)
			{
				unsigned char pucPINResultData[32] = { 0 };
				int loc_Ret = EMV_CT_Send_PIN_Offline(pucPINResultData);
				dlog_msg("Sending PIN to the card ret %X SW1:%X SW2:%X", loc_Ret, pucPINResultData[0], pucPINResultData[1]);
				g_CEMV.SetPINStatus(CheckPINresult(pucPINResultData[0], pucPINResultData[1]));
				dlog_msg("EMV_CT_Send_PIN_Offline ret: %X  SW1SW2: 02.02%X%02.02X", loc_Ret, pucPINResultData[0], pucPINResultData[1]);
				g_logger.Log("EMV_CT_Send_PIN_Offline ret: %X  SW1SW2: 02.02%X%02.02X", loc_Ret, pucPINResultData[0], pucPINResultData[1]);
				char msg[100];
				sprintf(msg, "EMV_CT_Send_PIN_Offline ret: %X SW1SW2: %X%X", loc_Ret, pucPINResultData[0], pucPINResultData[1]);
				EMVlog(msg);
				Ret = PIN_VERIFY_OK;
				full_verify = false;
				
				g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_DONE);
			}
			else
			{
				// Error occurred, PIN not entered properly, so reset flag
				SetPINStatus(PIN_ENTRY_STATUS_NOT_SET);
			}
		}
		else
		{
			full_verify = false;
		}
	//}while((full_verify) && ((Ret!=PIN_VERIFY_OK) && (Ret!=PIN_VERIFY_BLOCKED) && (Ret!=PIN_VERIFY_CARD_REMOVED)));
	} while((full_verify) && (Ret==PIN_VERIFY_ERROR));

	delete pPINdisplay;

	
	return Ret;


}

int CEMV::SetParam(long Tag, uint8_t *Val, int Len )
{
	int iRet = 0;

	return iRet;
}

int CEMV::GetParam(long Tag, uint8_t *Val, int *Len)
{
	int iRet = 0;

	return iRet;
}

int SetDefaultPayData(EMV_CT_PAYMENT_TYPE *payData)
{
	time_t cur_time = time(NULL);
	struct tm *t_m;
	char cTransCount[8+1];

	memset(payData->Amount, 0, 6);
	memset(payData->CurrencyTrans, 0, 2);
	payData->ExpTrans = 2;

	sprintf((char *)cTransCount, "%.08d", g_EMVcollection.getTrxCntr());
	com_verifone_pml::svcDsp2Hex(cTransCount, 8, (char *)payData->TransCount, 4);
	memcpy(g_EMVcollection.transData.T_9F41_TransCount, payData->TransCount, 4);

	g_logger.Log("Transaction Counter: %.02X%.02X%.02X%.02X", payData->TransCount[0], payData->TransCount[1], payData->TransCount[2], payData->TransCount[3]);

	char msg[100];

	if(cur_time > 0)
	{
		if((t_m = localtime(&cur_time)) != NULL)
		{


			payData->Date[0] = t_m->tm_year-100;		//year
			payData->Date[1] = t_m->tm_mon+1;			//month
			payData->Date[2] = t_m->tm_mday;			//day
			payData->Time[0] = t_m->tm_hour;				//hour
			payData->Time[1] = t_m->tm_min;				//minute
			payData->Time[2] = t_m->tm_sec;				//second


			sprintf(msg, "%02.02d%02.02d%02.02d%02.02d%02.02d%02.02d", 	payData->Date[0], payData->Date[1], payData->Date[2], payData->Time[0], payData->Time[1], payData->Time[2]);

			com_verifone_pml::svcDsp2Hex((const char *)msg, 6, reinterpret_cast<char *>(payData->Date), 3);
			com_verifone_pml::svcDsp2Hex((const char *)msg+6, 6, reinterpret_cast<char *>(payData->Time), 3);
		}

	}
	else
	{
		payData->Date[0] = 0x00;
		payData->Date[1] = 0x00;
		payData->Date[2] = 0x00;
		payData->Time[0] = 0x00;
		payData->Time[1] = 0x00;
		payData->Time[2] = 0x00;
	}

	g_logger.Log("Transaction date and time (yymmddhhmmss): %s", msg);


	inGetPTermID((char*)g_EMVcollection.termData.IFDSerialNumber);
	g_EMVcollection.termData.Info_Included_Data[1] |= EMV_CT_INPUT_TRM_IFD_SERIAL;
	memcpy(g_EMVcollection.transData.T_9F1E_IFDSerialNumber, g_EMVcollection.termData.IFDSerialNumber, 8);
	memcpy(g_EMVcollection.cardData.T_9F1E_IFDSerialNumber, g_EMVcollection.termData.IFDSerialNumber, 8);
	g_EMVcollection.cardData.T_DF61_Info_Received_Data[2] |= TRX_9F1E_IFDSERIALNUMBER;
	g_logger.Log("Terminal ID: '%s'", g_EMVcollection.termData.IFDSerialNumber);

	return 0;
}

int CEMV::SelectApplication(void)
{
	EMV_CT_SELECT_TYPE  xSelectInput;
	EMV_CT_SELECTRES_TYPE xSelectRes;
	EMV_ADK_INFO ADK_ret;
	EMV_CT_APPS_SELECT_TYPE SEL_Data;
	EMV_CT_APPLI_TYPE excludeAID[3];

	int autoFlag = g_CardAppConfig.GetAutoFlag();
	int iRet = checkStatus(CARD_APP_OPER_SELECT);

	if (iRet != CARD_APP_RET_OK)
		return iRet;

	memset(&xSelectInput, 0, sizeof(xSelectInput));
	memset(&xSelectRes, 0, sizeof(xSelectRes));
	memset(&SEL_Data, 0, sizeof(SEL_Data));
	memset(&excludeAID, 0, sizeof(EMV_CT_APPLI_TYPE)*3);
	reasonOnline = REASON_ONLINE_UNKNOWN;

	//setting initial parameters
	SEL_Data.No_DirectorySelect = 0;
	SEL_Data.ucCardholderConfirmation = CARD_CONF_YES;
	SEL_Data.ExcludeEmvAIDs[0] = excludeAID[0];
	SEL_Data.ExcludeEmvAIDs[1] = excludeAID[1];
	SEL_Data.ExcludeEmvAIDs[2] = excludeAID[2];
	SEL_Data.countDomesticChip = 0;
	//SEL_Data.xDomestic_Chip = NULL;
	SEL_Data.countFallbackMS= 0;
	//SEL_Data.xFallback_MS = NULL;

	xSelectInput.InitTXN_Buildlist = BUILD_NEW;
	xSelectInput.TransType = 0x00;		//sale transaction as default

	SetDefaultPayData(&g_EMVcollection.payData);

	xSelectInput.TXN_Data = g_EMVcollection.payData;
	xSelectInput.SEL_Data = SEL_Data;

	xSelectInput.TxnOptions[0] = 0;
	xSelectInput.TxnOptions[1] = 0;
	xSelectInput.TxnOptions[2] = 0;//EMV_CT_TRXOP_ADD_TRACE_CALLBACK;
	xSelectInput.TxnOptions[3] = 0;
	xSelectInput.TxnOptions[4] = 0;

	xSelectInput.Info_Included_Data[0] = INPUT_SEL_BUILDLIST|INPUT_SEL_AMOUNT|INPUT_SEL_AMOUNT_CURRENCY|INPUT_SEL_DATE|INPUT_SEL_TIME|INPUT_SEL_TTYPE;
	xSelectInput.Info_Included_Data[1] = INPUT_SEL_TXN_OPTIONS;
	// xSelectInput.Info_Included_Data[1] = 0;
	xSelectInput.Info_Included_Data[2] = INPUT_SEL_TXN_STEPS | INPUT_SEL_TXN_COUNTER;
	xSelectInput.Info_Included_Data[3] = 0;
	xSelectInput.Info_Included_Data[4] = 0;
	xSelectInput.Info_Included_Data[5] = 0;
	xSelectInput.Info_Included_Data[6] = 0;
	xSelectInput.Info_Included_Data[7] = 0;

	
	xSelectInput.TxnSteps[0] = RETURN_CANDIDATE_LIST;
	xSelectInput.TxnSteps[1] = 0;
	//xSelectInput.TxnSteps[2] = MS_RETURN_CALLBACKS;
	xSelectInput.TxnSteps[2] = 0;

	g_EMVcollection.AID_priority.clear();

	do
	{
		dlog_msg("Starting application selection...candidate list construction");
		EMVlog("Starting application selection...candidate list construction");
		ADK_ret = EMV_CT_StartTransaction(&xSelectInput, &g_EMVcollection.selectData);
		if(isCardRemoved())
		{
			ADK_ret = EMV_ADK_NO_CARD;
		}
		dlog_msg("EMV_CT_StartTransaction RET (after candidate list construction: %X", ADK_ret);
		dlog_msg("%s", ADK_err(ADK_ret).c_str());
		g_logger.Log((char *)ADK_err(ADK_ret).c_str());

		for(std::map<std::string, std::string>::iterator it = g_EMVcollection.AID_priority.begin(); it!=g_EMVcollection.AID_priority.end(); ++it)
    	{
        	g_logger.Log("candidates AID(%d): %.02X%.02X%.02X%.02X%.02X%.02X%.02X%.02X%.02X priority: %.02X", it->first.size(), it->first[0],it->first[1],it->first[2],it->first[3],it->first[4],it->first[5],it->first[6],it->first[7],it->first[8], it->second[0]);
        	dlog_msg("candidates AID(%d): %.02X%.02X%.02X%.02X%.02X%.02X%.02X%.02X%.02X priority: %.02X", it->first.size(), it->first[0],it->first[1],it->first[2],it->first[3],it->first[4],it->first[5],it->first[6],it->first[7],it->first[8], it->second[0]);
    	}

		if(ADK_ret == EMV_ADK_APP_REQ_CANDIDATE)
		{
			//now we must construct menu for selection
			int AIDCount = 0;
			int AIDcnt = 0;
			
			
			while (g_EMVcollection.selectData.T_BF04_Candidates[AIDCount].candidate.aidlen != 0) ++AIDCount;
			dlog_msg("%d AIDs found on candidate list", AIDCount);
			if(AIDCount > 1)
			{
				AIDcnt = RemoveAIDduplicates();
				dlog_msg("%d candidates left on the list after duplicates removal", AIDcnt);
			}
			else
			{
				AIDcnt = 2; //dummy value forcing selection menu
			}
			int candidate = REUSE_EXISTING_LIST_SEL_0;
			int sel = CandListModify(g_EMVcollection.selectData.T_BF04_Candidates);

			//and finally we call selection
			if(sel >= 0)
			{
				dlog_msg("Modify: Selected candidate %d", sel);
				candidate += sel;
			}
			else if (sel == APP_SEL_CARD_REMOVED)
			{
				dlog_msg("Card removed!");
				g_CEMV.SetStatusDetail(CARD_APP_STATUS_D_REMOVED);
				g_CEMV.SetStatusMain(CARD_APP_STATUS_M_STOP);
				break;
			}
			else if (sel == APP_SEL_POS_CANCELLED)
			{
				dlog_msg("Selection cancelled");
				g_CEMV.SetStatusDetail(CARD_APP_STATUS_D_CANCELLED);
				g_CEMV.SetStatusMain(CARD_APP_STATUS_M_STOP);
				break;
			}
			else if(AIDcnt > 1)
			{
				char labels[5][17];		//magic numbers according to EMV lib definitions
				int numLabels;
				int i;

				memset(labels, 0, sizeof(labels));
				for(numLabels = 0; numLabels < 5; numLabels++)
				{
					//dlog_msg("Adding label: %s", g_EMVcollection.selectData.T_BF04_Candidates[numLabels].name);
					if (strlen((const char *)g_EMVcollection.selectData.T_BF04_Candidates[numLabels].name))
					{
						strncpy(labels[numLabels], (const char *)g_EMVcollection.selectData.T_BF04_Candidates[numLabels].name, strlen((const char *)g_EMVcollection.selectData.T_BF04_Candidates[numLabels].name));
					}
					else
					{
						break;
					}
				}
				char * labels_gui[5];
				for(i = 0; i < numLabels; ++i)
				{
					labels_gui[i] = labels[i];
				}
				g_CEMVtrns.send_notify(CARD_APP_EVT_APP_SELECT);
				#ifdef VFI_GUI_GUIAPP
				cAppSelDisplayGuiApp appSel;
				#endif
				#ifdef VFI_GUI_DIRECTGUI
				cAppSelDisplayDirectGUI appSel("", "mainmenu.html");
				#endif
				appSel.setIsICCpresent(isCardpresent_loc);
				appSel.setCheckBreak(checkBreak_AppSel_loc);
				int result = appSel.displayApplicationSelection(const_cast<const char **>(labels_gui), numLabels, g_CardAppConfig.GetApplicationSelectionTimeout()*1000);
				g_CEMVtrns.send_notify(CARD_APP_EVT_APP_SELECT_DONE);
				if (result < 0)
				{
					dlog_error("Application selection error");
					switch (result)
					{
						case APP_SEL_CARD_REMOVED:
							dlog_msg("Card removed!");
							g_CEMV.SetStatusDetail(CARD_APP_STATUS_D_REMOVED);
							g_CEMV.SetStatusMain(CARD_APP_STATUS_M_STOP);
							break;
						case APP_SEL_CANCELLED:
						case APP_SEL_POS_CANCELLED:
							dlog_msg("Selection cancelled");
							g_CEMV.SetStatusDetail(CARD_APP_STATUS_D_CANCELLED);
							g_CEMV.SetStatusMain(CARD_APP_STATUS_M_STOP);
							break;
						default:
							// default failure
							g_CEMV.SetStatusDetail(CARD_APP_STATUS_D_GENERAL);
							g_CEMV.SetStatusMain(CARD_APP_STATUS_M_STOP);
							break;
					}
					// TODO: How do we inform EMV ADK?
					ADK_ret = EMV_ADK_CVM; // User abort (includes POS cancel)
				}
				else
				{
					candidate += result;
				}
			}

			if(ADK_ret != EMV_ADK_CVM)
			{
				xSelectInput.TxnSteps[0] = 0;
				xSelectInput.TxnSteps[1] = 0;
				xSelectInput.TxnSteps[2] = 0;

				xSelectInput.Info_Included_Data[0] = INPUT_SEL_BUILDLIST|INPUT_SEL_AMOUNT|INPUT_SEL_AMOUNT_CURRENCY|INPUT_SEL_DATE|INPUT_SEL_TIME|INPUT_SEL_TTYPE;
				xSelectInput.Info_Included_Data[1] = INPUT_SEL_TXN_OPTIONS;
				xSelectInput.Info_Included_Data[2] = 0;

				xSelectInput.InitTXN_Buildlist = candidate;

				xSelectInput.TXN_Data = g_EMVcollection.payData;
				xSelectInput.SEL_Data = SEL_Data;

				dlog_msg("Starting final selection...");
				EMVlog("Starting final selection...");
				ADK_ret = EMV_CT_StartTransaction(&xSelectInput, &g_EMVcollection.selectData);
			}

			dlog_msg("EMV_CT_StartTransaction RET (after final selection: %X", ADK_ret);
			dlog_msg("%s", ADK_err(ADK_ret).c_str());
			EMVlog((char *)ADK_err(ADK_ret).c_str());
		}

		//updating TLV collection
		if(ADK_ret == EMV_ADK_OK)
		{
			EMVlog("Updating current context of EMV data");
			EMV_CT_GetAppliData(EMV_ADK_READ_AID, &g_EMVcollection.selectData.T_DF04_Aidselected, &g_EMVcollection.appData);
			EMV_CT_GetTermData(&g_EMVcollection.termData);
		}
		else if (ADK_ret == EMV_ADK_BADAPP)
		{
			xSelectInput.InitTXN_Buildlist = REUSE_LIST_REMOVE_AID;
		}
	} while (ADK_ret == EMV_ADK_BADAPP);

	
	if(ADK_ret == EMV_ADK_FALLBACK)
		ADK_ret = EMV_ADK_NOAPP;

	iRet = setResponse((int)ADK_ret, CARD_APP_OPER_SELECT);


	dlog_msg("SelectApplication - RET: %X, Status: %0000000X:%0000000X", iRet, GetStatusMain(), GetStatusDetail());

	return iRet;
}

int CEMV::GetProcessingOptions(void)
{
    int iRet = checkStatus(CARD_APP_OPER_GPO);


	//Jacek - this is commnted out as GPO is not supported separately
	#if 0
	EMV_CT_SELECT_TYPE	xSelectInput;
	EMV_CT_SELECTRES_TYPE xSelectRes;
	EMV_ADK_INFO ADK_ret;
	EMV_CT_APPS_SELECT_TYPE SEL_Data;

	int autoFlag = g_CardAppConfig.GetAutoFlag();

	if (iRet != CARD_APP_RET_OK)
		return iRet;

	memset(&xSelectInput, 0, sizeof(xSelectInput));
	memset(&xSelectRes, 0, sizeof(xSelectRes));
	memset(&SEL_Data, 0, sizeof(SEL_Data));
	reasonOnline = REASON_ONLINE_UNKNOWN;
	if (g_CardAppConfig.GetUseDisplay())
	{
		if(1)
		{
			DisplayPromptSelectSection(CARD_APP_GUI_PLEASE_WAIT, CARD_APP_GUI_S_PROMPTS);
		}
	}

	//setting initial parameters
	xSelectInput.InitTXN_Buildlist = 1;
	xSelectInput.TransType = 0; 	//sale transaction as default
	xSelectInput.TXN_Data = g_EMVcollection.payData;
	xSelectInput.SEL_Data = SEL_Data;
	memset(xSelectInput.TxnOptions, 0, sizeof(xSelectInput.TxnOptions));
	memset(xSelectInput.TxnSteps, 0, sizeof(xSelectInput.TxnSteps));

    if (iRet != CARD_APP_RET_OK)
        return iRet;

    if(isSeparateGPO())
    {
		ADK_ret = EMV_CT_StartTransaction(&xSelectInput, &xSelectRes);
        iRet = setResponse((int)ADK_ret, CARD_APP_OPER_GPO);
    }
    else
    {
        dlog_msg("GPO performed together with selection...");
        iRet = setResponse(0, CARD_APP_OPER_GPO);
    }
    dlog_msg("GetProcessingOptions - RET: %X, Status: %0000000X:%0000000X", iRet, GetStatusMain(), GetStatusDetail());

    return iRet;
	#endif

	iRet = setResponse(0, CARD_APP_OPER_GPO);

	return CARD_APP_RET_OK;
}

#define BUF_MAX 100

int CEMV::ReadCardData(void)
{
    int iIndex = 0;
    unsigned short usAux = 0;
    int iRet = checkStatus(CARD_APP_OPER_READ);

	EMV_CT_TRANSAC_TYPE	xTransactionInput;
	EMV_ADK_INFO ADK_ret;
	EMV_CT_APPS_SELECT_TYPE SEL_Data;

    if (iRet != CARD_APP_RET_OK)
        return iRet;

	xTransactionInput.TXN_Data = g_EMVcollection.payData;
	xTransactionInput.TxnSteps[0] = RETURN_AFTER_READ_RECORD;
	xTransactionInput.TxnSteps[1] = 0;
	//xTransactionInput.TxnSteps[2] = MS_RETURN_CALLBACKS;
	xTransactionInput.TxnSteps[2] = 0;

	xTransactionInput.TxnOptions[0] = 0;
	xTransactionInput.TxnOptions[1] = 0;
	xTransactionInput.TxnOptions[2] = /*EMV_CT_TRXOP_ADD_TRACE_CALLBACK|*/EMV_CT_TRXOP_CARDHINFO_CALLBACK|EMV_CT_TRXOP_MERCHINFO_CALLBACK;
	xTransactionInput.TxnOptions[3] = 0;
	xTransactionInput.TxnOptions[4] = 0;

        memcpy(xTransactionInput.Info_Included_Data, g_EMVcollection.t_Info_Included_Data, sizeof(xTransactionInput.Info_Included_Data));
	xTransactionInput.Info_Included_Data[0] |= INPUT_OFL_TXN_STEPS;

	do
	{
    	ADK_ret = EMV_CT_ContinueOffline( &xTransactionInput, &g_EMVcollection.transData);
		dlog_msg("ReadCardData:EMV_CT_ContinueOffline:%s", ADK_err(ADK_ret).c_str());
		g_logger.Log("ReadCardData:EMV_CT_ContinueOffline:%s", ADK_err(ADK_ret).c_str());
		ADK_ret = InterpretEMVret(ADK_ret, &xTransactionInput);
	}
	while((ADK_ret != EMV_ADK_APP_REQ_READREC) && (ADK_ret >= EMV_ADK_APP_REQ_START) && (ADK_ret <= EMV_ADK_APP_REQ_END));

	#if 0
    unsigned char *ptrack2 = new unsigned char[BUF_MAX];
    unsigned char *pPAN = new unsigned char[BUF_MAX];
    //Check if PAN from TLV = PAN present track 2 - if not BAD_TRACK2

    if((ptrack2 != NULL) && (pPAN != NULL))
    {
        dlog_msg("Checking track2 PAN consistency...");
		memcpy(ptrack2, g_EMVcollection.transData.T_57_DataSP2.tr2data, g_EMVcollection.transData.T_57_DataSP2.tr2len);
		usAux = (unsigned short)g_EMVcollection.transData.T_57_DataSP2.tr2len;
        if (usAux)
        {
             dlog_hex(ptrack2, usAux, "Track2 data:");
             iIndex = 6;                //PAN can not be less than 12 digits
             while(iIndex < usAux)      //we look for end of PAN data in track2 data
             {
                if((ptrack2[iIndex] & 0xD0) == 0xD0)
                {
                    ptrack2[iIndex] = 0x00;
                    break;
                }
                if((ptrack2[iIndex] & 0x0D) == 0x0D)
                {
                    ptrack2[iIndex] &= 0xF0;
                    iIndex++;
                    break;
                }
                iIndex++;
             }
             //Get the PAN from the collection
             usAux = 0;
			 memcpy(pPAN, g_EMVcollection.transData.T_5A_PAN, sizeof(g_EMVcollection.transData.T_5A_PAN));
			 usAux = (unsigned short)sizeof(g_EMVcollection.transData.T_5A_PAN);
             if(usAux)
             {
                dlog_hex(pPAN, usAux, "PAN data:");
                while(pPAN[usAux-1] == 0xFF)
                    usAux--;

                if((pPAN[usAux-1] & 0x0F) == 0x0F)
                    pPAN[usAux-1] &= 0xF0;

                if(iIndex < usAux)
                    iIndex = usAux;
            }
            else
            {
                dlog_msg("Appl PAN (5A) data not present!");
            }
            if(memcmp(pPAN, ptrack2, iIndex) !=0)
            {
                dlog_msg("PAN does not match!!! - ABORTING...");
                iRet = EMV_ADK_CVM; // CHIP_ERROR allows fallback, so cannot be used here. We assume the app is blocked.
            }
        }
        else
        {
            dlog_msg("Track2 (57) data not present!");
        }
    }
    else
    {
        dlog_msg("Memory problem!!!");
    }
    delete [] ptrack2;
    delete [] pPAN;
	#endif
    iRet = setResponse((int)ADK_ret, CARD_APP_OPER_READ);
    dlog_msg("ReadCardData - RET: %X, Status: %0000000X:%0000000X", iRet, GetStatusMain(), GetStatusDetail());
    return iRet;
}

//this function process all offline steps of the transaction. ADK kernel has fixed order of terminal operations, there is no way to change it.
int CEMV::OfflineSteps(EMV_ADK_INFO break_ret)
{
	int iRet = checkStatus(CARD_APP_OPER_VERIFY);
	int iIndex = 0;
	unsigned short usAux = 0;
	bool PIN_CVM_used = false;

	EMV_CT_TRANSAC_TYPE xTransactionInput;
	EMV_ADK_INFO ADK_ret;

	g_CEMV.SetPINStatus(PIN_ENTRY_STATUS_OK);
	g_CardAppConfig.SetPINtype(CARD_APP_PIN_OFFLINE);
	g_EMVcollection.payData.Additional_Result_Tags.anztag = 5;
	g_EMVcollection.payData.Additional_Result_Tags.tags[0] = TAG_9F7B_ABSA;
	g_EMVcollection.payData.Additional_Result_Tags.tags[1] = TAG_9F17_PIN_TRIES_LEFT;
	g_EMVcollection.payData.Additional_Result_Tags.tags[2] = TAG_4F_APP_ID;
	g_EMVcollection.payData.Additional_Result_Tags.tags[3] = TAG_9F06_AID;
	g_EMVcollection.payData.Additional_Result_Tags.tags[4] = TAG_9F10_ISS_APP_DATA;

	if (iRet != CARD_APP_RET_OK)
		return iRet;

	dlog_msg("OFFLINE PROCESSING STARTED");

	PIN_try = 0;

	if(term_decision == FORCED_ONLINE)
	{
		g_EMVcollection.payData.Force_Online = 1;
		g_EMVcollection.t_Info_Included_Data[1] |= INPUT_OFL_FORCE_ONLINE;
	}

	g_EMVcollection.transData.T_9F27_CryptInfo= 0xFF;
	xTransactionInput.TXN_Data = g_EMVcollection.payData;
	xTransactionInput.TxnOptions[0] = 0;
	xTransactionInput.TxnOptions[1] = 0;
	xTransactionInput.TxnOptions[2] = /*EMV_CT_TRXOP_ADD_TRACE_CALLBACK|*/EMV_CT_TRXOP_CARDHINFO_CALLBACK|EMV_CT_TRXOP_MERCHINFO_CALLBACK;
	xTransactionInput.TxnOptions[3] = 0;
	xTransactionInput.TxnOptions[4] = 0;

	xTransactionInput.TxnSteps[0] = RETURN_FOR_CVM_PROCESS | RETURN_FOR_CVM_FINISH | RETURN_AFTER_RISK_MANGEMENT | RETURN_AFTER_DATA_AUTH;

	//xTransactionInput.TxnSteps[0] = RETURN_FOR_CVM_PROCESS | RETURN_FOR_CVM_FINISH;
	xTransactionInput.TxnSteps[1] = 0;
	//xTransactionInput.TxnSteps[2] = MS_RETURN_CALLBACKS;
	xTransactionInput.TxnSteps[2] = 0;
        memcpy(xTransactionInput.Info_Included_Data, g_EMVcollection.t_Info_Included_Data, sizeof(xTransactionInput.Info_Included_Data));
	xTransactionInput.Info_Included_Data[0] |= INPUT_OFL_TXN_STEPS | INPUT_OFL_ADD_TAGS;
        //memset(xTransactionInput.Info_Included_Data, 0, sizeof(xTransactionInput.Info_Included_Data));
	//xTransactionInput.Info_Included_Data[0] |= INPUT_OFL_TXN_STEPS | INPUT_OFL_CB_AMOUNT ;
	//xTransactionInput.Info_Included_Data[1] |= INPUT_OFL_AMOUNT | INPUT_OFL_DATE | INPUT_OFL_TIME;
        dlog_msg("OFFLINE PROCESSING FLAGS: %02X, %02X", xTransactionInput.Info_Included_Data[0], xTransactionInput.Info_Included_Data[1]);


	if(term_decision == FORCED_DECLINE)
		xTransactionInput.TxnSteps[2] = MS_DECLINE_AAC;

	
	do
	{
		if(isCardInserted())
		{
			ADK_ret = EMV_CT_ContinueOffline( &xTransactionInput, &g_EMVcollection.transData);
			dlog_msg("Offline steps:EMV_CT_ContinueOffline:%s", ADK_err(ADK_ret).c_str());
			ADK_ret = InterpretEMVret(ADK_ret, &xTransactionInput);
			if((ADK_ret >= EMV_ADK_APP_REQ_OFL_PIN) && (ADK_ret <= EMV_ADK_APP_REQ_PLAIN_PIN))
			{
				PIN_CVM_used = true;
			}
		}
		else
		{
			ADK_ret = EMV_ADK_NO_CARD;
		}
	}
	while((ADK_ret != break_ret) && (ADK_ret >= EMV_ADK_APP_REQ_START) && (ADK_ret <= EMV_ADK_APP_REQ_END));

	//checking if card not removed
	if(ADK_ret == EMV_ADK_NO_CARD)
	{
		EMVlog("CARD REMOVED!!!");
	}


	unsigned char PIN_try_cntr = 0xFF;
	g_EMVcollection.SetTLVData(TAG_9F17_PIN_TRIES_LEFT, &PIN_try_cntr, 1);

	parseInTLV(g_EMVcollection.transData.Add_TXN_Tags, EMV_ADK_ADD_TAG_SIZE, false);

	char addTags[EMV_ADK_ADD_TAG_SIZE*2+1];
	com_verifone_pml::svcHex2Dsp((const char *)g_EMVcollection.transData.Add_TXN_Tags, EMV_ADK_ADD_TAG_SIZE, addTags, EMV_ADK_ADD_TAG_SIZE*2+1);
	addTags[EMV_ADK_ADD_TAG_SIZE*2] = 0;
	EMVlog("EXTRA DATA REQUESTED:");
	EMVlog(addTags);

	int Len;

	g_EMVcollection.GetTLVData(TAG_9F17_PIN_TRIES_LEFT, &PIN_try_cntr, &Len);

	if((PIN_try_cntr == 0) && (Len > 0))
	{
		g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_BLOCKED);
	}


	
	dlog_msg("OFFLINE PROCESSING FINISHED ret:%X %s", ADK_ret, ADK_err(ADK_ret).c_str());

	if((ADK_ret > EMV_ADK_APP_REQ_START) && (ADK_ret < EMV_ADK_APP_REQ_END))
	{

		dlog_msg("Exit on %X", ADK_ret);
		iRet = setResponse((int)ADK_ret, CARD_APP_OPER_VERIFY|CARD_APP_OPER_RESTRICT|CARD_APP_OPER_TRM|CARD_APP_OPER_DATA_AUTH|CARD_APP_OPER_FIRST_AC);
	}
	else
	{
		iRet = setResponse((int)ADK_ret, CARD_APP_OPER_VERIFY|CARD_APP_OPER_RESTRICT|CARD_APP_OPER_TRM|CARD_APP_OPER_DATA_AUTH|CARD_APP_OPER_FIRST_AC);

	    unsigned char CID = CID = g_EMVcollection.transData.T_9F27_CryptInfo;
		dlog_msg("FirstAC - CID %X", CID);

		switch(ADK_ret)
		{
			case EMV_ADK_TC:
		        SetStatusMain(CARD_APP_STATUS_M_OK | CARD_APP_STATUS_M_STOP);
		        SetStatusDetail(CARD_APP_STATUS_D_OK | CARD_APP_STATUS_D_NO_FALLBACK);
		        iRet =  CARD_APP_RET_APPROVED;
				break;
		    case EMV_ADK_ARQC:
		        SetStatusMain(CARD_APP_STATUS_M_OK | CARD_APP_STATUS_M_CONTINUE);
		        SetStatusDetail(CARD_APP_STATUS_D_OK | CARD_APP_STATUS_D_NO_FALLBACK);
		        iRet = CARD_APP_RET_ONLINE;
				break;
		    case EMV_ADK_AAC:
		        SetStatusMain(CARD_APP_STATUS_M_OK | CARD_APP_STATUS_M_STOP);
		        SetStatusDetail(CARD_APP_STATUS_D_OK | CARD_APP_STATUS_D_NO_FALLBACK);
		        iRet =  CARD_APP_RET_DECLINED;
				break;
			case EMV_ADK_NO_CARD:
		        SetStatusMain(CARD_APP_STATUS_M_ERROR | CARD_APP_STATUS_M_STOP);
		        SetStatusDetail(CARD_APP_STATUS_D_REMOVED | CARD_APP_STATUS_D_NO_FALLBACK);
		        iRet =  CARD_APP_RET_REMOVED;
				break;
			default:
				SetStatusMain(CARD_APP_STATUS_M_ERROR| CARD_APP_STATUS_M_STOP);
		        SetStatusDetail(CARD_APP_STATUS_D_GENERAL| CARD_APP_STATUS_D_FALLBACK);
				iRet = CARD_APP_RET_FAIL;
				break;
		}
	}
	return iRet;

}

int CEMV::CardholderVerification(void)
{
    int iRet = checkStatus(CARD_APP_OPER_VERIFY);

	int iIndex = 0;
    unsigned short usAux = 0;

	EMV_CT_TRANSAC_TYPE	xTransactionInput;
	EMV_ADK_INFO ADK_ret;

    if (iRet != CARD_APP_RET_OK)
        return iRet;

    xTransactionInput.TXN_Data = g_EMVcollection.payData;
	xTransactionInput.TxnSteps[0] = RETURN_FOR_CVM_PROCESS | RETURN_FOR_CVM_FINISH ;
	xTransactionInput.TxnSteps[1] = 0;
	xTransactionInput.TxnSteps[2] = 0;


	xTransactionInput.TxnOptions[0] = 0;
	xTransactionInput.TxnOptions[1] = 0;
	xTransactionInput.TxnOptions[2] = 0;//EMV_CT_TRXOP_ADD_TRACE_CALLBACK;
	xTransactionInput.TxnOptions[3] = 0;
	xTransactionInput.TxnOptions[4] = 0;

        memcpy(xTransactionInput.Info_Included_Data, g_EMVcollection.t_Info_Included_Data, sizeof(xTransactionInput.Info_Included_Data));
	xTransactionInput.Info_Included_Data[0] |= INPUT_OFL_TXN_STEPS;

	do
	{
    	ADK_ret = EMV_CT_ContinueOffline( &xTransactionInput, &g_EMVcollection.transData);
		dlog_msg("CardholdeVerification:EMV_CT_ContinueOffline:%s", ADK_err(ADK_ret).c_str());
		ADK_ret = InterpretEMVret(ADK_ret, &xTransactionInput);

	}
	while((ADK_ret != EMV_ADK_APP_REQ_CVM_END) && (ADK_ret >= EMV_ADK_APP_REQ_START) && (ADK_ret <= EMV_ADK_APP_REQ_END));

	iRet = setResponse((int)ADK_ret, CARD_APP_OPER_VERIFY);

    if (g_CardAppConfig.GetUseDisplay())
    {
        if(1)
        {
            DisplayPromptSelectSection(CARD_APP_GUI_PLEASE_WAIT, CARD_APP_GUI_S_PROMPTS);
        }
    }
    stopBeeperThread();
    return iRet;
}

int CEMV::ProcessRestrictions(void)
{
    int iRet = checkStatus(CARD_APP_OPER_RESTRICT);

    if (iRet != CARD_APP_RET_OK)
        return iRet;

	//Jacek - commented out, as not supported atomically

	#if 0
	int iIndex = 0;
    unsigned short usAux = 0;

	EMV_CT_TRANSAC_TYPE	xTransactionInput;
	EMV_ADK_INFO ADK_ret;

    if (iRet != CARD_APP_RET_OK)
        return iRet;

    xTransactionInput.TXN_Data = g_EMVcollection.payData;
    ADK_ret = EMV_CT_ContinueOffline( &xTransactionInput, &g_EMVcollection.transData);

    iRet = setResponse((int)ADK_ret, CARD_APP_OPER_RESTRICT);
    dlog_msg("ProcessRestrictions - RET: %X, Status: %0000000X:%0000000X", iRet, GetStatusMain(), GetStatusDetail());
    // Reason Online
    if (reasonOnline != REASON_ONLINE_UNKNOWN)
    {
        char tvrByte  = g_EMVcollection.transData.T_95_TVR[1]; // get byte 2
        if (tvrByte & 0x20) // check bit 6 (Card not yet effective)
        {
            reasonOnline = REASON_ONLINE_FORCED_BY_ACCEPTOR;
        }
        else if (tvrByte & 0x40) // check bit 7 (Card Expired)
        {
            reasonOnline = REASON_ONLINE_FORCED_BY_ISSUER;
        }
    }
	#endif

	iRet = setResponse(0, CARD_APP_OPER_RESTRICT);
    return CARD_APP_RET_OK;
}

int CEMV::TerminalRiskManagement(void)
{
    int iRet = checkStatus(CARD_APP_OPER_TRM);

    if (iRet != CARD_APP_RET_OK)
        return iRet;


	int iIndex = 0;
    unsigned short usAux = 0;

	EMV_CT_TRANSAC_TYPE	xTransactionInput;
	EMV_ADK_INFO ADK_ret;

    if (iRet != CARD_APP_RET_OK)
        return iRet;

    xTransactionInput.TXN_Data = g_EMVcollection.payData;
	xTransactionInput.TxnOptions[0] = RETURN_AFTER_RISK_MANGEMENT;
	xTransactionInput.TxnOptions[1] = 0;
	xTransactionInput.TxnOptions[2] = MS_RETURN_CALLBACKS;

        memcpy(xTransactionInput.Info_Included_Data, g_EMVcollection.t_Info_Included_Data, sizeof(xTransactionInput.Info_Included_Data));
	xTransactionInput.Info_Included_Data[0] |= INPUT_OFL_TXN_STEPS;

	do
	{
    	ADK_ret = EMV_CT_ContinueOffline( &xTransactionInput, &g_EMVcollection.transData);
		dlog_msg("TerminalRiskManagement:EMV_CT_ContinueOffline:%s", ADK_err(ADK_ret).c_str());
    	ADK_ret = InterpretEMVret(ADK_ret, &xTransactionInput);
	}
	while((ADK_ret != EMV_ADK_APP_REQ_RISK_MAN) && (ADK_ret >= EMV_ADK_APP_REQ_START) && (ADK_ret <= EMV_ADK_APP_REQ_END));

    iRet = setResponse((int)ADK_ret, CARD_APP_OPER_TRM);


    dlog_msg("TerminalRiskManagement - RET: %X, Status: %0000000X:%0000000X", iRet, GetStatusMain(), GetStatusDetail());
    // Reason Online
    if (reasonOnline != REASON_ONLINE_UNKNOWN)
    {

        char tvrByte  = g_EMVcollection.transData.T_95_TVR[3]; // get byte 4
        if (tvrByte & 0x80) // check bit 8 (transaction exceeds floor limit)
        {
            reasonOnline = REASON_ONLINE_OVER_FLOOR;
        }
        else if (tvrByte & 0x10) // check bit 5 (transaction randomly selected for online)
        {
            reasonOnline = REASON_ONLINE_RANDOM_SELECTION;
        }
        else if ((tvrByte & 0x20) || (tvrByte & 0x40)) // check bits 6/7 (upper/lower consecutive offline limit exceeded)
        {
            reasonOnline = REASON_ONLINE_FORCED_BY_ACCEPTOR;
        }
        else
        {
            tvrByte = g_EMVcollection.transData.T_95_TVR[1]; // get byte 2
            if (tvrByte & 0x08) // check bit 4 (new card)
            {
                reasonOnline = REASON_ONLINE_FORCED_BY_ISSUER;
            }
        }
    }

    return CARD_APP_RET_OK;
}

int CEMV::DataAuthentication(void)
{
    int iRet = checkStatus(CARD_APP_OPER_DATA_AUTH);

	int iIndex = 0;
    unsigned short usAux = 0;

	EMV_CT_TRANSAC_TYPE	xTransactionInput;
	EMV_ADK_INFO ADK_ret;

    if (iRet != CARD_APP_RET_OK)
        return iRet;

    xTransactionInput.TXN_Data = g_EMVcollection.payData;
	xTransactionInput.TxnOptions[0] = RETURN_AFTER_DATA_AUTH;
	xTransactionInput.TxnOptions[1] = 0;
	xTransactionInput.TxnOptions[2] = MS_RETURN_CALLBACKS;

        memcpy(xTransactionInput.Info_Included_Data, g_EMVcollection.t_Info_Included_Data, sizeof(xTransactionInput.Info_Included_Data));
	xTransactionInput.Info_Included_Data[0] |= INPUT_OFL_TXN_STEPS;

	do
	{
    	ADK_ret = EMV_CT_ContinueOffline( &xTransactionInput, &g_EMVcollection.transData);
		dlog_msg("DataAuthentication:EMV_CT_ContinueOffline:%s", ADK_err(ADK_ret).c_str());
    	ADK_ret = InterpretEMVret(ADK_ret, &xTransactionInput);
	}
	while((ADK_ret != EMV_ADK_APP_REQ_DATAAUTH) && (ADK_ret >= EMV_ADK_APP_REQ_START) && (ADK_ret <= EMV_ADK_APP_REQ_END));


	iRet = setResponse((int)ADK_ret, CARD_APP_OPER_DATA_AUTH);
    dlog_msg("DataAuthentication - RET: %X, Status: %0000000X:%0000000X", iRet, GetStatusMain(), GetStatusDetail());


    return iRet;
}

int CEMV::FirstAC(void)
{
    int iRet = checkStatus(CARD_APP_OPER_FIRST_AC);
    unsigned char CID;
    unsigned short tagLen;

/*
    if(iRet == CARD_APP_RET_ORDER)
    {
        // We know order is improper - still, we shall check whether we're performing Refund and we're done with GPO and previous steps
        if (!checkStatus(CARD_APP_OPER_TRM)) // Check for CARD_APP_OPER_RESTRICT / CARD_APP_OPER_VERIFY / CARD_APP_OPER_DATA_AUTH would do as well
        {
            // We're not past GPO stage - error!
            return CARD_APP_RET_ORDER;
        }
        // Now let's check TranType tag
        Ushort dLen = 0;
        char pData;
        if(usEMVGetTLVFromColxn(TAG_9C00_TRAN_TYPE, reinterpret_cast<unsigned char *>(&pData), &dLen)!=EMV_SUCCESS)
        {
            // Unlikely - no TranType set??
            return CARD_APP_RET_ORDER;
        }
        // Check if TranType is Refund
        dlog_msg("TranType is %02Xh", pData);
        if (dLen != 1 || pData != 32)
        {
            // TranType is not a Refund - error!
            return CARD_APP_RET_ORDER;
        }
        dlog_alert("Allowing 1stGenAC as proper short path for Refunds only - but forcing Decline!");
        term_decision = FORCED_DECLINE;
    }
    else if (iRet != CARD_APP_RET_OK)
        return iRet;

    if (term_decision != FORCED_DECLINE && term_decision != FORCED_ONLINE && term_decision != 0)
    {
        term_decision = 0;
    }
*/
	EMV_CT_TRANSAC_TYPE xTransactionInput;
	EMV_ADK_INFO ADK_ret;

	if (iRet != CARD_APP_RET_OK)
		return iRet;
    dlog_msg("Terminal decision set %d", term_decision);
	xTransactionInput.TXN_Data = g_EMVcollection.payData;
	xTransactionInput.TxnOptions[0] = 0;
	xTransactionInput.TxnOptions[1] = 0;
	xTransactionInput.TxnOptions[2] = 0;

        memcpy(xTransactionInput.Info_Included_Data, g_EMVcollection.t_Info_Included_Data, sizeof(xTransactionInput.Info_Included_Data));
	xTransactionInput.Info_Included_Data[0] |= INPUT_OFL_TXN_STEPS;

	if(term_decision == FORCED_DECLINE)
		xTransactionInput.TxnSteps[2] = MS_DECLINE_AAC;

	do
	{
    	ADK_ret = EMV_CT_ContinueOffline( &xTransactionInput, &g_EMVcollection.transData);
		dlog_msg("FirstAC:EMV_CT_ContinueOffline:%s", ADK_err(ADK_ret).c_str());
		ADK_ret = InterpretEMVret(ADK_ret, &xTransactionInput);
	}
	while((ADK_ret >= EMV_ADK_APP_REQ_START) && (ADK_ret <= EMV_ADK_APP_REQ_END));

    iRet = setResponse((int)ADK_ret, CARD_APP_OPER_FIRST_AC);
    printf("FirstAC - RET: %X, Status: %0000000X:%0000000X \r\n", iRet, GetStatusMain(), GetStatusDetail());
    dlog_msg("FirstAC - RET: %X, Status: %0000000X:%0000000X", iRet, GetStatusMain(), GetStatusDetail());
    // Reason Online

	CID = g_EMVcollection.transData.T_9F27_CryptInfo;
    dlog_msg("FirstAC - CID %X", CID);
    if((CID &  GEN_TC) == GEN_TC)
    {
        SetStatusMain(CARD_APP_STATUS_M_OK | CARD_APP_STATUS_M_STOP);
        SetStatusDetail(CARD_APP_STATUS_D_OK | CARD_APP_STATUS_D_NO_FALLBACK);
		strcpy((char *)g_EMVcollection.hostData.AuthResp, "Y1");
        return CARD_APP_RET_APPROVED;
    }
    else if((CID & GEN_ARQC) == GEN_ARQC)
    {
        SetStatusMain(CARD_APP_STATUS_M_OK | CARD_APP_STATUS_M_CONTINUE);
        SetStatusDetail(CARD_APP_STATUS_D_OK | CARD_APP_STATUS_D_NO_FALLBACK);
        return CARD_APP_RET_ONLINE;
    }
    else if((CID & GEN_AAC) == GEN_AAC)
    {
        SetStatusMain(CARD_APP_STATUS_M_OK | CARD_APP_STATUS_M_STOP);
        SetStatusDetail(CARD_APP_STATUS_D_OK | CARD_APP_STATUS_D_NO_FALLBACK);
		strcpy((char *)g_EMVcollection.hostData.AuthResp, "Z1");
        return CARD_APP_RET_DECLINED;
    }
    else
    {
        return CARD_APP_RET_FAIL;
    }

    return CARD_APP_RET_OK;
}

int CEMV::ExternalAuthenticate(void)
{
    int iRet = checkStatus(CARD_APP_OPER_EXT_AUTH);

    if (iRet != CARD_APP_RET_OK)
        return iRet;
    /*
    iRet = setResponse(inVXEMVAPAuthIssuer(), CARD_APP_OPER_EXT_AUTH);
    dlog_msg("ExternalAuthenticate - RET: %X, Status: %0000000X:%0000000X", iRet, GetStatusMain(), GetStatusDetail());
    */
    return iRet;
}

int CEMV::ProcessScript(void)
{
    unsigned char scriptID[4];
    int iRet = checkStatus(CARD_APP_OPER_SCRIPT);

    if (iRet != CARD_APP_RET_OK)
        return iRet;
    /*
    iRet = setResponse(inVXEMVAPProcessScript(scriptID, script_result), CARD_APP_OPER_SCRIPT);
    dlog_msg("ProcessScript - RET: %X, Status: %0000000X:%0000000X", iRet, GetStatusMain(), GetStatusDetail());
    */
    return iRet;
}

int CEMV::SecondAC(void)
{
    unsigned char CID;
    unsigned short tagLen;
    int iRet = checkStatus(CARD_APP_OPER_SECOND_AC);

    if (iRet != CARD_APP_RET_OK)
        return iRet;

	/*
    iRet = setResponse(inVXEMVAPSecondGenerateAC(term_decision), CARD_APP_OPER_SECOND_AC);
    dlog_msg("SecondAC - RET: %X, Status: %0000000X:%0000000X", iRet, GetStatusMain(), GetStatusDetail());
    usEMVGetTLVFromColxn( TAG_9F27_CRYPT_INFO_DATA, &CID, &tagLen );
    dlog_msg("SecondAC - CID %X", CID);
    SetStatusMain(CARD_APP_STATUS_M_OK | CARD_APP_STATUS_M_STOP);
    SetStatusDetail(CARD_APP_STATUS_D_OK | CARD_APP_STATUS_D_NO_FALLBACK);
    if((CID & GEN_AAC) == GEN_AAC)
    {
        return CARD_APP_RET_DECLINED;
    }
    else if((CID & GEN_TC) == GEN_TC)
    {
        return CARD_APP_RET_APPROVED;
    }
    else
    {
        return CARD_APP_RET_FAIL;
    }
    */
    return CARD_APP_RET_OK;
}

int CEMV::PostOnline(void)
{
    int num_scripts = 0;
    unsigned char CID;
    unsigned short tagLen;

	EMV_ADK_INFO ADK_ret;

    int iRet = checkStatus(CARD_APP_OPER_SECOND_AC);

	//Jacek - must be checked
	/*
    if (iRet != CARD_APP_RET_OK)
        return iRet;
	*/
    script_size71 = 0;
    script_size72 = 0;
    if (term_decision != HOST_AUTHORISED && term_decision != HOST_DECLINED && term_decision != FAILED_TO_CONNECT)
    {
        term_decision = FAILED_TO_CONNECT;
    }
    dlog_msg("Terminal decision set %d", term_decision);

	switch(term_decision)
	{
		case HOST_AUTHORISED:
		case HOST_DECLINED:
			g_EMVcollection.hostData.OnlineResult = TRUE;
			break;
		default:
			g_EMVcollection.hostData.OnlineResult = FALSE;
			break;
	}
	g_EMVcollection.hostData.Info_Included_Data[0] |= INPUT_ONL_ONLINE_RESP;

	//cleaning script results
	memset(&g_EMVcollection.transData.scriptresults, 0, sizeof(EMV_CT_SRCRIPTRES_TYPE));
	g_EMVcollection.hostData.TxnSteps[0] = 0;
	g_EMVcollection.hostData.TxnSteps[1] = 0;
	g_EMVcollection.hostData.TxnSteps[2] = MS_RETURN_CALLBACKS;


	g_EMVcollection.hostData.TxnOptions[0] = 0;
	g_EMVcollection.hostData.TxnOptions[1] = 0;
	g_EMVcollection.hostData.TxnOptions[2] = 0;//EMV_CT_TRXOP_ADD_TRACE_CALLBACK;
	g_EMVcollection.hostData.TxnOptions[3] = 0;
	g_EMVcollection.hostData.TxnOptions[4] = 0;

	//g_EMVcollection.hostData.Info_Included_Data[0] = 0;

	ADK_ret = EMV_CT_ContinueOnline(&g_EMVcollection.hostData, &g_EMVcollection.transData);
	num_scripts = int(g_EMVcollection.transData.scriptresults.countScriptCrit+ g_EMVcollection.transData.scriptresults.countScriptUnCrit);
	dlog_msg("PostOnline:EMV_CT_ContinueOnline:%s", ADK_err(ADK_ret).c_str());
	g_logger.Log("PostOnline:EMV_CT_ContinueOnline:%s Info_Included_data[0]=0x%02X", ADK_err(ADK_ret).c_str(), g_EMVcollection.hostData.Info_Included_Data[0]);
	switch(ADK_ret)
	{
		case EMV_ADK_TC:
			SetStatusMain(CARD_APP_STATUS_M_OK | CARD_APP_STATUS_M_STOP);
			SetStatusDetail(CARD_APP_STATUS_D_OK | CARD_APP_STATUS_D_NO_FALLBACK);
			iRet =	CARD_APP_RET_APPROVED;
			break;
		case EMV_ADK_AAC:
			SetStatusMain(CARD_APP_STATUS_M_OK | CARD_APP_STATUS_M_STOP);
			SetStatusDetail(CARD_APP_STATUS_D_OK | CARD_APP_STATUS_D_NO_FALLBACK);
			iRet =	CARD_APP_RET_DECLINED;
			break;
		default:
			SetStatusMain(CARD_APP_STATUS_M_ERROR| CARD_APP_STATUS_M_STOP);
			SetStatusDetail(CARD_APP_STATUS_D_GENERAL| CARD_APP_STATUS_D_FALLBACK);
			iRet = CARD_APP_RET_FAIL;
			break;
	}

    iRet = setResponse((int)ADK_ret, CARD_APP_OPER_EXT_AUTH | CARD_APP_OPER_SCRIPT | CARD_APP_OPER_SECOND_AC);
    dlog_msg("PostOnline - RET: %X, Status: %0000000X:%0000000X", iRet, GetStatusMain(), GetStatusDetail());

    //the code below is dedicated for getting script results separately for both script types 71 and 72
    dlog_msg("Issuer scripts processed: %d", num_scripts);
    dlog_msg("Script results returned: %d", (g_EMVcollection.transData.T_DF61_Info_Received_Data[3] & TRX_SCRIPTRESULTS));
    if(num_scripts > 0 && (g_EMVcollection.transData.T_DF61_Info_Received_Data[3] & TRX_SCRIPTRESULTS))
    {
        using namespace com_verifone_TLVLite;
        SafeBuffer crit(script_result71, sizeof(script_result71));
        dlog_msg("Count script crit %d", g_EMVcollection.transData.scriptresults.countScriptCrit);
        for (int i = 0; i < g_EMVcollection.transData.scriptresults.countScriptCrit; ++i)
        {
            crit.append(ConstData(g_EMVcollection.transData.scriptresults.ScriptCritResult[i], EMV_ADK_SCRIPT_RESULT_LEN));
        }
        script_size71 = crit.getLength();
        if (script_size71) dlog_hex(script_result71, script_size71, "SCRIPT71");
        SafeBuffer uncrit(script_result72, sizeof(script_result72));
        dlog_msg("Count script uncrit %d", g_EMVcollection.transData.scriptresults.countScriptUnCrit);
        for (int i = 0; i < g_EMVcollection.transData.scriptresults.countScriptUnCrit; ++i)
        {
            uncrit.append(ConstData(g_EMVcollection.transData.scriptresults.ScriptUnCritResult[i], EMV_ADK_SCRIPT_RESULT_LEN));
        }
        script_size72 = uncrit.getLength();
        if (script_size72) dlog_hex(script_result72, script_size72, "SCRIPT72");

        dlog_msg("script 71 size = %d bytes, script 72 size = %d bytes", g_CEMVtrns.script71_len, g_CEMVtrns.script72_len);

        g_CEMVtrns.script71 = NULL;
        g_CEMVtrns.script71_len = 0;
        g_CEMVtrns.script72 = NULL;
        g_CEMVtrns.script72_len = 0;
    }
    CID = g_EMVcollection.transData.T_9F27_CryptInfo;
    dlog_msg("SecondAC - CID %X", CID);
    SetStatusMain(CARD_APP_STATUS_M_OK | CARD_APP_STATUS_M_STOP);
    SetStatusDetail(CARD_APP_STATUS_D_OK | CARD_APP_STATUS_D_NO_FALLBACK);
    if((CID & GEN_AAC) == GEN_AAC)
    {
        return CARD_APP_RET_DECLINED;
    }
    else if((CID & GEN_TC) == GEN_TC)
    {
        return CARD_APP_RET_APPROVED;
    }
    else
    {
        return CARD_APP_RET_FAIL;
    }
    return CARD_APP_RET_OK;
}

int CEMV::AddScript(char *script)
{
    int iRet = CARD_APP_RET_OK;
    return iRet;
}

int CEMV::UpdateTLV(unsigned char *list, int list_len)
{
    int iRet = CARD_APP_RET_OK;
    return iRet;
}

int CEMV::ReadTLV(unsigned char *list, int list_len, unsigned char *out_list, int out_list_len)
{
    int iRet = CARD_APP_RET_OK;
    return iRet;
}

/* beeper thread */
typedef struct
{
    long timeout;
    int mainTaskID;
} BEEPER_TIMEOUT_PARAMS;

int beeper_timeout_routine(BEEPER_TIMEOUT_PARAMS *params)
{
    static const long EVENT_TIMEOUT = EVT_BIO;
    int result = 0;
    int timerID = 0;

	event_item ev_timer;
	ev_timer.event_id = events::timer;
	int events_mon = event_open();
	eventset_t pml_events;

	int iRet = event_ctl(events_mon, ctl::ADD, ev_timer);
	dlog_msg("Adding events::ipc ret :%X", iRet);

    if (params && params->timeout > 0)
    {
        com_verifone_pml::normal_tone();
		set_timer(ev_timer, params->timeout);
		event_ctl(events_mon, ctl::ADD, ev_timer);
        //dlog_msg("Waiting for %d milliseconds or interruption", params->timeout);
        do
        {
			event_wait(events_mon, pml_events);
			if (pml_events.size())
			{
				for (eventset_t::iterator it = pml_events.begin() ; it != pml_events.end(); ++it)
				{
			   		if (it->event_id == events::timer)
			   		{
						//dlog_alert("TGK Timed Out!");
                		com_verifone_pml::normal_tone();
                		timerID = set_timer(ev_timer, params->timeout);
			   		}

				}
			}
			//we should consider break on user event reception

        } while(true);
    }
    clr_timer(ev_timer);
    if (params) delete params;
    event_close(events_mon);
    return 0;
}

bool CEMV::runBeeperThread()
{
	#if 0
    if (beeperThreadID == 0)
    {
        BEEPER_TIMEOUT_PARAMS *params = new BEEPER_TIMEOUT_PARAMS;
        params->timeout = g_CardAppConfig.GetPINbeeperTimeout() * 1000; // config timeout is measured in seconds
        // params->mainTaskID = get_task_id();
        beeperThreadID = run_thread(reinterpret_cast<int>(beeper_timeout_routine), reinterpret_cast<int>(params), 1 * 1024);
    }
	#endif
    return (beeperThreadID != 0);
}

bool CEMV::stopBeeperThread()
{
	#if 0
    if (beeperThreadID)
    {
        dlog_msg("Killing beeper thread");
        post_user_event(beeperThreadID, 1); // post any user event, thread will end this way
        beeperThreadID = 0;
    }
	#endif
    return true;
}


