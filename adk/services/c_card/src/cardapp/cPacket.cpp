#ifndef __cplusplus
#  error "This file is for C++ only!"
#endif

/***************************************************************************
 *
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 *
 **************************************************************************/
/**
 * @file        cPacket.cpp
 *
 * @author      Jacek Olbrys
 *
 * @brief       Base Packet Class
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C++ Coding
 *              Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/
#include <string.h>

#include <liblog/logsys.h>

#include "Packet.h"

/***************************************************************************
 * Using
 **************************************************************************/
using namespace com_verifone_pack;


/***************************************************************************
 * Exportable function definitions
 **************************************************************************/

/***************************************************************************
 * Exportable class members' definitions
 **************************************************************************/
#ifdef USE_BER_TLV_LIB
uint8_t CPacket::getCmdCode()
{
    return byCmdCode;
}

CPacket::c_card_error_t CPacket::setCmdCode(const uint8_t &byCmdCodeI)
{
    byCmdCode = byCmdCodeI;
    return ESUCCESS;
}

CPacket::c_card_error_t CPacket::setAppName(const char * abyAppNameI)
{
    if (NULL == abyAppNameI)
    {
        memset(abyAppName, '\0', MAX_APP_NAME);
        return ESUCCESS;
    }

    uint8_t uiLenAppName = strlen(const_cast<char*>(abyAppNameI));
    if (uiLenAppName > MAX_APP_NAME)
    {
        return EERROR;
    }

    memcpy(abyAppName, abyAppNameI, uiLenAppName);
    abyAppName[uiLenAppName] = '\0';
    return ESUCCESS;
}

CPacket::c_card_error_t CPacket::makeListTags(uint8_t *abyBuf, uint16_t byBufLen)
{
    if( NULL == abyBuf )
    {
        dlog_msg("makeListTags: empty buffer ?");
        return EERROR;
    }
    pstHeadListTags = pstTLV_make_element();
    // +-1 => cmd shift  
    if( SUCCESS != usnTLV_parse_buffer( pstHeadListTags, abyBuf, byBufLen) )
    {
        dlog_msg("makeListTags: parse error");
        return EERROR;
    }
    return ESUCCESS;
}

void CPacket::ClearData()
{
    byCmdCode = 0;
    memset(abyAppName, 0, MAX_APP_NAME);
    destroyListTags();
}

void CPacket::destroyListTags(void)
{
    vTLV_destroy_element(pstHeadListTags);
    pstHeadListTags = NULL;
}

CPacket::c_card_error_t CPacket::SetDataPacket(char *szSender, uint8_t byCmdCode,
      uint8_t *pbyData, uint32_t byLen)
{
    if (EERROR == setAppName(szSender))
    {
        dlog_msg("SetDataPacket: setAppName error");
        return EERROR;
    }
    if (EERROR == setCmdCode(byCmdCode))
    {
        dlog_msg("SetDataPacket: setCmdCode error");
        return EERROR;
    }
    if (EERROR == makeListTags(pbyData, byLen) )
    {
        dlog_msg("SetDataPacket: makeListTags error");
        return EERROR;
    }
    return ESUCCESS;
}

CPacket::c_card_error_t CPacket::addTagTLV( const u_char *pbyTag, u_char byTagLen, u_char *pbyValue, u_long ulnLen )
{
    BER_TLV *pstBT;

    dlog_msg("CPacket::addTagTLV lenTag[%d]",byTagLen);
    if(NULL == pbyTag || 0 == byTagLen || NULL == pbyValue || 0 == ulnLen)
    {
        dlog_msg("addTagTLV: EINVAL");
        return EERROR;
    }

    if(pstHeadListTags == NULL)
    {
        pstHeadListTags = pstTLV_make_primitive_element(pbyTag, byTagLen, pbyValue, ulnLen );

        if (pstHeadListTags == NULL)
        {
            dlog_msg("Cannot create pTLVHeadElement");
            return EERROR;
        }

        dlog_msg("SUCCESS: CPacket::addTagTLV make");
        return ESUCCESS;
    }
    pstBT = pstTLV_append_element(pstHeadListTags);
    if (pstBT == NULL)
    {
        dlog_msg("Cannot append pTLVElement");
        return EERROR;
    }
    vTLV_set_up_primitive_element(pstBT, pbyTag, byTagLen, pbyValue, ulnLen);
    dlog_msg("SUCCESS: CPacket::addTagTLV");
    return ESUCCESS;
}

const char* CPacket::getAppName()
{
    return abyAppName;
}

#endif

#ifdef USE_TLV_LITE_LIB

uint8_t CPacket::getCmdCode()
{
    return byCmdCode;
}

CPacket::c_card_error_t CPacket::setCmdCode(const uint8_t &byCmdCodeI)
{
    byCmdCode = byCmdCodeI;
    return ESUCCESS;
}

CPacket::c_card_error_t CPacket::setAppName(const char * abyAppNameI)
{
    if (NULL == abyAppNameI)
    {
        memset(abyAppName, '\0', MAX_APP_NAME);
        return ESUCCESS;
    }

    uint8_t uiLenAppName = strlen(const_cast<char*>(abyAppNameI));
    if (uiLenAppName > MAX_APP_NAME)
    {
        return EERROR;
    }

    memcpy(abyAppName, abyAppNameI, uiLenAppName);
    abyAppName[uiLenAppName] = '\0';
    return ESUCCESS;
}

CPacket::c_card_error_t CPacket::makeListTags(uint8_t *abyBuf, uint16_t byBufLen)
{
    if( NULL == abyBuf )
    {
        dlog_msg("makeListTags: empty buffer ?");
        return EERROR;
    }
    pstHeadListTags = pstTLV_make_element();
    /* +-1 => cmd shift  */
    if( SUCCESS != usnTLV_parse_buffer( pstHeadListTags, abyBuf, byBufLen) )
    {
        dlog_msg("makeListTags: parse error");
        return EERROR;
    }
    return ESUCCESS;
}

void CPacket::ClearData()
{
    byCmdCode = 0;
    memset(abyAppName, 0, MAX_APP_NAME);
    destroyListTags();
}

void CPacket::destroyListTags(void)
{
    vTLV_destroy_element(pstHeadListTags);
    pstHeadListTags = NULL;
}

CPacket::c_card_error_t CPacket::SetDataPacket(char *szSender, uint8_t byCmdCode,
      uint8_t *pbyData, uint32_t byLen)
{
    if (EERROR == setAppName(szSender))
    {
        dlog_msg("SetDataPacket: setAppName error");
        return EERROR;
    }
    if (EERROR == setCmdCode(byCmdCode))
    {
        dlog_msg("SetDataPacket: setCmdCode error");
        return EERROR;
    }
    if (EERROR == makeListTags(pbyData, byLen) )
    {
        dlog_msg("SetDataPacket: makeListTags error");
        return EERROR;
    }
    return ESUCCESS;
}

CPacket::c_card_error_t CPacket::addTagTLV( const u_char *pbyTag, u_char byTagLen, u_char *pbyValue, u_long ulnLen )
{
    BER_TLV *pstBT;

    dlog_msg("CPacket::addTagTLV lenTag[%d]",byTagLen);
    if(NULL == pbyTag || 0 == byTagLen || NULL == pbyValue || 0 == ulnLen)
    {
        dlog_msg("addTagTLV: EINVAL");
        return EERROR;
    }

    if(pstHeadListTags == NULL)
    {
        pstHeadListTags = pstTLV_make_primitive_element(pbyTag, byTagLen, pbyValue, ulnLen );

        if (pstHeadListTags == NULL)
        {
            dlog_msg("Cannot create pTLVHeadElement");
            return EERROR;
        }

        dlog_msg("SUCCESS: CPacket::addTagTLV make");
        return ESUCCESS;
    }
    pstBT = pstTLV_append_element(pstHeadListTags);
    if (pstBT == NULL)
    {
        dlog_msg("Cannot append pTLVElement");
        return EERROR;
    }
    vTLV_set_up_primitive_element(pstBT, pbyTag, byTagLen, pbyValue, ulnLen);
    dlog_msg("SUCCESS: CPacket::addTagTLV");
    return ESUCCESS;
}

const char* CPacket::getAppName()
{
    return abyAppName;
}
#endif
