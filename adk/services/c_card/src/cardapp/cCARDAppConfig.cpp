#ifndef __cplusplus
#  error "This file is for C++ only!"
#endif
/***************************************************************************
**
 *
 * Copyright (C) 2006 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 *
 **************************************************************************/
/**
 * @file        cCARDAppConfig.cpp
 *
 * @author      Jacek Olbrys
 *
 * @brief       Card service configuration
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C++ Coding
 *              Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/
#include <algorithm>

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <liblog/logsys.h>
#include <libminini/minIni.h>
#include <tlv-lite/ConstData.h>
#include <tlv-lite/ValueConverter.hpp>
#include <tlv-lite/ConstValue.hpp>

#include "cBaseConfig.h"
#include "cCARDAppConfig.h"
#include "cTransaction.h"
#include "cIPCInterface.h"
#include "CARDApp.h"
#ifdef VFI_PLATFORM_VERIXEVO
	#include <EMVResult.hpp>
#endif

/***************************************************************************
 * Using
 **************************************************************************/
using namespace com_verifone_ipcpacket;
/***************************************************************************
 * Module namespace: begin
 **************************************************************************/
namespace com_verifone_emv
{

/**
 * @addtogroup Card
 * @{
 */

/**
 * Local namespace: begin
 **/
    namespace
    {
       /**
        * @addtogroup Card
        * @{
        */

       /*
        * Local preprocessor definitions and macros
        */
        const unsigned char CANCEL_CODE_ = 0x80;
        const unsigned char BYPASS_CODE_ = 0xFF;
        const char * const CFG_TOKEN_APPLICATION_ID_ = "APPLICATION_ID";
        const int TIMER_NOT_SET_ = -1;
        const int KEY_MAX_LEN_ = 50;
        const int VALUE_MAX_LEN_ = 10;
        const int MS_PER_SEC_ = 1000;

        const char * const CFG_TOKEN_PIN_TIMEOUT_ = "PIN_TIMEOUT";
        const char * const CFG_TOKEN_PIN_FIRSTCHAR_TIMEOUT_ = "PIN_FIRSTCHAR_TIMEOUT";
        const char * const CFG_TOKEN_PIN_INTERCHAR_TIMEOUT_ = "PIN_INTERCHAR_TIMEOUT";
        const char * const CFG_TOKEN_PIN_SMALL_SCREEN_TIMEOUT_ = "PIN_ALTERNATE_TIMEOUT";
        const char * const CFG_TOKEN_APP_SEL_TIMEOUT_ = "APPLICATION_SELECTION_TIMEOUT";
        const char * const CFG_TOKEN_NOTIFICATIONS_ = "NOTIFY_ABOUT_CARDHOLDER_EVENTS";
        const char * const CFG_TOKEN_WAIT_FOR_CARD_REMOVAL_ = "WAIT_FOR_CARD_REMOVAL";
        const char * const CFG_TOKEN_USE_DISPLAY_ = "DISPLAY_MESSAGES";
        const char * const CFG_TOKEN_USE_LEDS_ = "AUTO_DRIVE_LEDS";
        const char * const CFG_TOKEN_BYPASS_ALLOWED_ = "POS_BYPASS_ALLOWED";
        const char * const CFG_TOKEN_PIN_TYPE_ = "PIN_TYPE";
        const char * const CFG_TOKEN_VSS_SLOT_LAST_ = "LAST_VSS_SLOT";
        const char * const CFG_TOKEN_TERM_TYPE_ = "TERMINAL_TYPE";
        const char * const CFG_TOKEN_OFFLINE_PIN_ENTRY_STYLE_ = "OFFLINE_PIN_ENTRY_STYLE";
        const char * const CFG_TOKEN_OFFLINE_PIN_CURRENCY_SYMBOL_ = "PIN_USE_CURRENCY_SYMBOL";
        const char * const CFG_TOKEN_OFFLINE_PIN_CURRENCY_LOC_ = "PIN_CURRENCY_LOCATION";
        const char * const CFG_TOKEN_OFFLINE_PIN_RADIX_SEP_ = "PIN_SEPARATOR_CHAR";
        const char * const CFG_TOKEN_OFFLINE_PIN_BYPASS_FLAG_ = "OFFLINE_PIN_BYPASS";
        const char * const CFG_TOKEN_OFFLINE_PIN_BYPASS_KEY_ = "OFFLINE_PIN_BYPASS_KEY";
        const char * const CFG_TOKEN_OFFLINE_PIN_BYPASS_PASSWORD_ = "OFFLINE_PIN_BYPASS_PASSWORD";
        const char * const CFG_TOKEN_EXT_AUTH_FAILURE_FLAG_ = "EXTERNAL_AUTHENTICATE_FAILURE";
        const char * const CFG_TOKEN_CDA_MODE_ = "CDA_MODE";
        const char * const CFG_TOKEN_MSG_ENTER_PIN = "MSG_ENTER_PIN";
        const char * const CFG_TOKEN_MSG_INCORRECT_PIN = "MSG_INCORRECT_PIN";
        const char * const CFG_TOKEN_MSG_LAST_PIN_TRY = "LAST_PIN_TRY";
        const char * const CFG_TOKEN_SUPER_EMV_WAIT_FOR_CARD_ = "SUPER_EMV_COMMAND_WAITS_FOR_CARD";
        const char * const CFG_TOKEN_SHOW_INCORRECT_PIN_ = "SHOW_INCORRECT_PIN_AFTER_FIRST_TRY";
        const char * const CFG_TOKEN_COUNTRY_REQUIREMENTS = "COUNTRY_REQUIREMENTS";
        const char * const CFG_TOKEN_COUNTRY_PARAMS = "COUNTRY_PARAMS";
        const char * const CFG_TOKEN_EMV_LOG_UDP = "LOG_IP_ADDRESS";
        const char * const CFG_TOKEN_GUI_TYPE = "GUI_TYPE";
        const char * const CFG_TOKEN_PARSE_CARD_DATA = "PARSE_CARD_DATA";
        const char * const CFG_TOKEN_INTERNAL_LOGGING = "INTERNAL_LOGGING";
        const char * const CFG_TOKEN_DEFAULT_PIN_HTML = "DEFAULT_PIN_HTML";
        const char * const CFG_TOKEN_PIN_HTML = "PIN_HTML_DIGIT";
        const char * const CFG_TOKEN_UX_SWIPE_ON_INSERT_ = "UX_ALLOW_SWIPES_ON_INSERTION";
        const char * const CFG_TOKEN_PIN_BLANK_CHAR = "PIN_BLANK_CHAR";
        const char * const CFG_TOKEN_PIN_DEFAULT_KEYBOARD_OPTIONS = "PIN_KEYBOARD_OPTIONS";
        const char * const CFG_TOKEN_NUM_OF_EXPECTED_PIN_DIGITS = "NUM_OF_EXPECTED_PIN_DIGITS";
        const char * const CFG_TOKEN_NEGOTIATE_ICC_SPEED = "NEGOTIATE_ICC_COMMUNICATION_SPEED";
        const char * const CFG_TOKEN_INSERT_PIN_WARNINGS = "INSERT_PIN_WARNINGS";
        const char * const CFG_TOKEN_WARM_RESET_AFTER_ICC_OFF = "WARM_RESET_AFTER_ICC_OFF";
        const char * const CFG_TOKEN_REPORT_ALL_SWIPES = "REPORT_ALL_SWIPES";
        const char * const CFG_TOKEN_USE_MSR_COUNTERS = "USE_MSR_COUNTERS";
        const char * const CFG_TOKEN_TIME_REMOVAL = "UX_WAIT_FOR_MSR_TIMER";
        const char * const CFG_TOKEN_HTML_PIN_OK = "HTML_PIN_OK";
        const char * const CFG_TOKEN_HTML_PIN_RETRY = "HTML_PIN_RETRY";
        const char * const CFG_TOKEN_HTML_PIN_LAST_TRY = "HTML_PIN_LAST_TRY";
        const char * const CFG_TOKEN_HTML_PARAMS = "HTML_PIN_PARAMS";
        const char * const CFG_TOKEN_HTML_PARAMS_PIN_OK = "HTML_PIN_OK_PARAMS";
        const char * const CFG_TOKEN_HTML_PARAMS_PIN_RETRY = "HTML_PIN_RETRY_PARAMS";
        const char * const CFG_TOKEN_HTML_PARAMS_PIN_LAST_TRY = "HTML_PIN_LAST_TRY_PARAMS";

      /**
        * @}
        */
    } /* Local namespace: end */

/***************************************************************************
* Exportable function definitions
**************************************************************************/

/***************************************************************************
 * Exportable class member's definitions
 **************************************************************************/

CCardAppConfig::CCardAppConfig(): m_ID(0), m_Locked(false), m_CmdLocked(false),
    m_CmdBreak(CMD_BREAK_NO_BREAK_E), m_PosTimeoutTimer(TIMER_NOT_SET_)
{
    memset(default_talker, 0, sizeof(default_talker));
    extraPINmessage[0]=0;
    pinScreenTitle = CARD_APP_GUI_ENTER_PIN;
    PINtimeout = 0;
    PINfirstcharTimeout = 0;
    PINintercharTimeout = 0;
    PINbeeperTimeout = 0;
    PINsmallScreenTimeout = 3; // alternate timeout for small screen devices
    AppSelTimeout = 60; // 60 seconds for application selection
    send_notify = true;
    wait_for_removal = false;
    use_display = false;
    use_leds = true;
    bypass_allowed = true;
    // PINtype = CARD_APP_PIN_OFFLINE;
    PINtype = CARD_APP_SECURE_PIN_OFFLINE;
    VSS_slot_last = 0;
    Term_type = -1;
    offlinePINEntryStyle = 1; // 0 = UK style, 1 = European style
    PINDisplayCurrencySymbol = true;
    currSymbolLoc = CURR_SYMBOL_AUTO;
    PINRadixSeparator = '.'; // default
    pin_bypass_flag = PIN_BYPASS_DISABLED; // disabled by default
    pin_bypass_key = 0x0D; // Enter
    memset(pin_bypass_password, 0, sizeof(pin_bypass_password));
    max_pin_len = 6;  // Mandatory (used for online PIN only!)
    min_pin_len = 4;   // Mandatory (used for online PIN only!)
    online_pin_entry_type = PIN_ENTRY_TYPE_MANDATORY_E;
    online_pin_cancel_allowed = true;
    ext_auth_flag = EMV_SUCCESS;
    pin_x = pin_y = 0;
    cda_mode = 0;
    secondAVN[0] = 0;
    secondAVN[1] = 0;
    external_application_selection = false;
    super_emv_waits_for_card = false;
    auto_flag = 0;
    msg_enter_pin.assign("ENTER PIN");
    msg_incorrect_pin.assign("INCORRECT PIN");
    msg_last_pin_try.assign("LAST PIN TRY");
    gui_interface = GUI_LOCAL;
    isUX100 = false;
    showIncorrectPIN = true;
    CAPK_index = 0;
    AID_oper = 0;
    parseCardData = false;
    internal_logging = LOG_TO_NULL;
    default_PIN_HTML.assign("EnterPINstart.html");
    PIN_HTML.resize(MAX_PIN_HTML+1); // 13 elements for PIN lengths 0 through 12
    uxSwipeOnInsertion = false;
    myPID = get_task_id();
    PINblankChar = ' ';
    PINoption = 0x08;
    expPINdigits = 4;
    iccNegotiateSpeed = true;
    pin_warnings = false;
    warm_reset_after_icc_off = false;
    process_blocked_app = true;
    ignore_icc_events = false;
    poll_timer_min = 200;
    poll_timer_max = 1000;
    log_adk_callback = false;
    timer_removal = 1000; // default value
}

CCardAppConfig::~CCardAppConfig()
{
}

/*
 * This restores default locally used variables after transaction is completed.
 * Add any further data when necessary
 */


void CCardAppConfig::SetContextParams(std::string msg)
{
	country_params = msg;
	ini_puts(section_req, CFG_TOKEN_COUNTRY_PARAMS, msg.c_str(), configFilename);
}


void CCardAppConfig::RestoreDefaults()
{
    pinScreenTitle = CARD_APP_GUI_ENTER_PIN;
    extraPINmessage[0] = 0;
    ext_auth_flag = EMV_SUCCESS;
    external_application_selection = false;
    PINRadixSeparator = '.';
}

bool CCardAppConfig::IsPINBypassEnabled()
{
    if (pin_bypass_flag == PIN_BYPASS_ENABLED || (pin_bypass_flag == PIN_BYPASS_SECURED && pin_bypass_password != 0)) return true;
    return false;
}

bool CCardAppConfig::IsSecuredPINBypassEnabled()
{
    if (pin_bypass_flag == PIN_BYPASS_SECURED && strlen(pin_bypass_password) >= MIN_PIN_BYPASS_PASSWORD_LEN) return true;
    return false;
}

bool CCardAppConfig::CheckPINBypassPassword( const char * const password )
{
    bool res = false;
    if (password && password[0] != '\0' && pin_bypass_password)
    {
        if (!strcmp(password, pin_bypass_password)) res = true;
    }
    dlog_msg("Password verification: %d", res);
    return res;
}

void CCardAppConfig::SetPINDisplayCurrencySymbolLoc(unsigned char currSymbol)
{
    switch (currSymbol)
    {
        default:
        case 0: currSymbolLoc = CURR_SYMBOL_AUTO; break;
        case 1: currSymbolLoc = CURR_SYMBOL_LEFT; break;
        case 2: currSymbolLoc = CURR_SYMBOL_RIGHT; break;
    }
}

bool CCardAppConfig::PINDisplayCurrencySymbolLeft(bool symbols_used)
{
    if (currSymbolLoc != CURR_SYMBOL_AUTO) return currSymbolLoc == CURR_SYMBOL_LEFT;
    else if (symbols_used) return false;
    return true;
}

void CCardAppConfig::SetOnlinePINEntryType(unsigned char entry_type)
{
    switch (entry_type)
    {
        case 0: online_pin_entry_type = PIN_ENTRY_TYPE_MANDATORY_E; break;
        case 1: online_pin_entry_type = PIN_ENTRY_TYPE_OPTIONAL_E; break;
        case 2: online_pin_entry_type = PIN_ENTRY_TYPE_OPTIONAL_0LN_PIN_ENC_E; break;
        case 3: online_pin_entry_type = PIN_ENTRY_TYPE_OPTIONAL_0LN_PIN_E; break;
        default: online_pin_entry_type = PIN_ENTRY_TYPE__INVALID_E; break;
    }
}

void CCardAppConfig::vPrintDerivedConfig()
{
    dlog_msg("PIN Timeout              : %d", PINtimeout);
    dlog_msg("PIN First Char Timeout   : %d", PINfirstcharTimeout);
    dlog_msg("PIN InterChar Timeout    : %d", PINintercharTimeout);
    dlog_msg("PIN Beeper Timeout       : %d", PINbeeperTimeout);
    dlog_msg("PIN small screen Timeout : %d", PINsmallScreenTimeout);
    dlog_msg("PIN Type                 : %d", PINtype);
    dlog_msg("PIN Currency Symbols     : %d", PINDisplayCurrencySymbol);
    dlog_msg("PIN Radix Separator      : %d", PINRadixSeparator);
    dlog_msg("PIN POS Bypass Allowed   : %d", bypass_allowed);
    dlog_msg("PIN Cardholder Bypass Typ: %d", pin_bypass_flag);
    dlog_msg("PIN Bypass Key           : %02Xh", pin_bypass_key);
    dlog_msg("PIN Bypass Password      : %s", pin_bypass_password);
    dlog_msg("Offline PIN Entry Style  : %d", offlinePINEntryStyle);
    dlog_msg("App Selection Timeout    : %d", AppSelTimeout);
    dlog_msg("Send notifications       : %d", send_notify);
    dlog_msg("Wait for removal         : %d", wait_for_removal);
    dlog_msg("Use display (not GuiApp) : %d", use_display);
    dlog_msg("Use LEDs                 : %d", use_leds);
    dlog_msg("VSS Slot Last            : %d", VSS_slot_last);
    dlog_msg("SuperEMV waits for card  : %d", super_emv_waits_for_card);
    dlog_msg("Show Incorrect PIN after : %d", showIncorrectPIN);
    dlog_msg("Terminal type            : %d", Term_type);
    dlog_msg("Internal logging         : %d", internal_logging);
    dlog_msg("IP LOG address           : %s", IP_UDP_Log.c_str());
    dlog_msg("Parse card data          : %d", parseCardData);
    dlog_msg("Default HTML             : %s", default_PIN_HTML.c_str());
    for (size_t i = 0; i < MAX_PIN_HTML; ++i)
    {
        if (PIN_HTML[i].size()) dlog_msg("HTML for digit %d        : %s", i, PIN_HTML[i].c_str());
    }
    dlog_msg("Ux, swipe on insertion   : %d", uxSwipeOnInsertion);
    dlog_msg("Negotiate ICC speed      : %d", iccNegotiateSpeed);
    dlog_msg("Warm reset after ICC off : %d", warm_reset_after_icc_off);
    dlog_msg("Report all swipes        : %d", ignore_icc_events);
    dlog_msg("Use MSR counters         : %d", use_msr_counters);
    dlog_msg("Ux wait for MSR after rem: %d", timer_removal);
    dlog_msg("HTML for PIN OK          : %s", html_pin_ok.c_str());
    for (std::map<std::string, std::string>::const_iterator it = values_pin_ok.begin(); it != values_pin_ok.end(); ++it)
    {
        dlog_msg("HTML for PIN OK Param: %s -> %s", it->first.c_str(), it->second.c_str());
    }
    dlog_msg("HTML for PIN retry       : %s", html_pin_retry.c_str());
    for (std::map<std::string, std::string>::const_iterator it = values_pin_retry.begin(); it != values_pin_retry.end(); ++it)
    {
        dlog_msg("HTML for PIN Retry Param: %s -> %s", it->first.c_str(), it->second.c_str());
    }
    dlog_msg("HTML for PIN last try    : %s", html_pin_last_try.c_str());
    for (std::map<std::string, std::string>::const_iterator it = values_pin_last_try.begin(); it != values_pin_last_try.end(); ++it)
    {
        dlog_msg("HTML for PIN Last Try Param: %s -> %s", it->first.c_str(), it->second.c_str());
    }
}


int CCardAppConfig::iReadDerivedConfig(const char *key, const char *val)
{
    int errCode = ESUCCESS;
    dlog_msg("-> Key '%s', value '%s'", key, val);

    if(strcmp(key, CFG_TOKEN_APPLICATION_ID_) == 0)
    {
        int ival = atoi(val);
        if(isdigit(val[0]))
        {
            m_ID = static_cast<appid_t>(ival);
            errCode = ESUCCESS;
        }
        else
        {
            errCode = EERROR;
        }
    }

    if (strcmp(key, CFG_TOKEN_PIN_TIMEOUT_) == 0)
    {
        PINtimeout = atol(val);
    }
	else if (strcmp(key, CFG_TOKEN_GUI_TYPE) == 0)
    {
        gui_interface = (gui_t)atoi(val);
    }
    else if (strcmp(key, CFG_TOKEN_PIN_FIRSTCHAR_TIMEOUT_) == 0)
    {
        PINfirstcharTimeout = atol(val);
    }
    else if (strcmp(key, CFG_TOKEN_PIN_INTERCHAR_TIMEOUT_) == 0)
    {
        PINintercharTimeout = atol(val);
    }
    else if (strcmp(key, CFG_TOKEN_PIN_SMALL_SCREEN_TIMEOUT_) == 0)
    {
        PINsmallScreenTimeout = atol(val);
    }
    else if (strcmp(key, CFG_TOKEN_APP_SEL_TIMEOUT_) == 0)
    {
        AppSelTimeout = atol(val);
    }
    else if (strcmp(key, CFG_TOKEN_NOTIFICATIONS_) == 0)
    {
        if (atoi(val) == 1) send_notify = true;
        else send_notify = false;
    }
    else if (strcmp(key, CFG_TOKEN_WAIT_FOR_CARD_REMOVAL_) == 0)
    {
        if (atoi(val) == 1) wait_for_removal = true;
        else wait_for_removal = false;
    }
    else if (strcmp(key, CFG_TOKEN_USE_DISPLAY_) == 0)
    {
        if (atoi(val) == 1) use_display = true;
        else use_display = false;
    }
    else if (strcmp(key, CFG_TOKEN_USE_LEDS_) == 0)
    {
        if (atoi(val) == 1) use_leds = true;
        else use_leds = false;
    }
    else if (strcmp(key, CFG_TOKEN_BYPASS_ALLOWED_) == 0)
    {
        if (atoi(val) == 1) bypass_allowed = true;
        else bypass_allowed = false;
    }
    else if (strcmp(key, CFG_TOKEN_PIN_TYPE_) == 0)
    {
        switch (atoi(val))
        {
        default:
           dlog_alert("Invalid pin type %d", atoi(val));
           case 1: PINtype = CARD_APP_PIN_OFFLINE; break;
           case 2: PINtype = CARD_APP_PIN_ONLINE_VSS; break;
           case 3: PINtype = CARD_APP_SECURE_PIN_OFFLINE; break;
        }
    }
    else if (strcmp(key, CFG_TOKEN_VSS_SLOT_LAST_) == 0)
    {
        VSS_slot_last = atol(val);
    }
    else if (strcmp(key, CFG_TOKEN_TERM_TYPE_) == 0)
    {
        Term_type = atol(val);
    }
    else if (strcmp(key, CFG_TOKEN_OFFLINE_PIN_ENTRY_STYLE_) == 0)
    {
        int pes = atol(val);
        if (pes == 0 || pes == 1) offlinePINEntryStyle = pes;
        else dlog_alert("Invalid offline PIN entry style value %d", pes);
    }
    else if (strcmp(key, CFG_TOKEN_OFFLINE_PIN_CURRENCY_SYMBOL_) == 0)
    {
        if (atoi(val) == 1) PINDisplayCurrencySymbol = true;
        else PINDisplayCurrencySymbol = false;
    }
    else if (strcmp(key, CFG_TOKEN_OFFLINE_PIN_CURRENCY_LOC_) == 0)
    {
        switch (atoi(val))
        {
        default:
           dlog_alert("Invalid currency symbol location %d", atoi(val));
        case 0: currSymbolLoc = CURR_SYMBOL_AUTO; break;
        case 1: currSymbolLoc = CURR_SYMBOL_LEFT; break;
        case 2: currSymbolLoc = CURR_SYMBOL_RIGHT; break;
        }
    }
    else if (strcmp(key, CFG_TOKEN_OFFLINE_PIN_RADIX_SEP_) == 0)
    {
        if (isprint(val[0])) PINRadixSeparator = val[0];
        else dlog_alert("Invalid PIN radix separator %c (%02X)", val[0], val[0]);
    }
    else if (strcmp(key, CFG_TOKEN_OFFLINE_PIN_BYPASS_FLAG_) == 0)
    {
        switch (atoi(val))
        {
        default:
           dlog_alert("Invalid PIN bypass flag %d", atoi(val));
        case 0: pin_bypass_flag = PIN_BYPASS_DISABLED; break;
        case 1: pin_bypass_flag = PIN_BYPASS_ENABLED; break;
        case 2: pin_bypass_flag = PIN_BYPASS_SECURED; break;
        }
    }
    else if (strcmp(key, CFG_TOKEN_OFFLINE_PIN_BYPASS_KEY_) == 0)
    {
        dlog_msg("PIN bypass key val '%s'", val);
        int newKey = 0;
        bool success = sscanf(val, "%x", &newKey) > 0;
        //if (!success) success = sscanf(val, "%d", &newKey) > 0;
        if (success) pin_bypass_key = static_cast<char>(newKey);
//        if (isprint(val[0])) pin_bypass_key = val[0];
        else dlog_alert("Invalid PIN bypass key '%s'", val);
    }
    else if (strcmp(key, CFG_TOKEN_OFFLINE_PIN_BYPASS_PASSWORD_) == 0)
    {
        memset(pin_bypass_password, 0, sizeof(pin_bypass_password));
        strncpy(pin_bypass_password, val, sizeof(pin_bypass_password)-1);
        if (strlen(val) >= sizeof(pin_bypass_password))
            dlog_alert("PIN bypass password too long!");
    }

#ifdef VFI_PLATFORM_VERIXEVO
    else if (strcmp(key, CFG_TOKEN_EXT_AUTH_FAILURE_FLAG_) == 0)
    {

        switch(atol(val))
        {
            case 0: ext_auth_flag = EMV_FAILURE; break;
            case 1:
            default: ext_auth_flag = EMV_SUCCESS; break;
        }
    }
    else if (strcmp(key, CFG_TOKEN_CDA_MODE_) == 0)
    {
        switch(atoi(val))
        {
            case 1: cda_mode = CDA_MODE_1; break;
            case 2: cda_mode = CDA_MODE_2; break;
            default: //yes, default for Kernel 6.0.1 is Mode 3
            case 3: cda_mode = CDA_MODE_3; break;
            case 4: cda_mode = CDA_MODE_4; break;
        }
    }
#endif
    else if (strcmp(key, CFG_TOKEN_MSG_ENTER_PIN) == 0)
    {
       SetEnterPINMsg(val);
    }
    else if (strcmp(key, CFG_TOKEN_MSG_INCORRECT_PIN) == 0)
    {
        SetIncorrectPINMsg(val);
    }
    else if (strcmp(key, CFG_TOKEN_MSG_LAST_PIN_TRY) == 0)
    {
        SetLastPINMsg(val);
    }
    else if (strcmp(key, CFG_TOKEN_SUPER_EMV_WAIT_FOR_CARD_) == 0)
    {
        if (atoi(val)) super_emv_waits_for_card = true;
        else super_emv_waits_for_card = false;
    }
    else if (strcmp(key, CFG_TOKEN_SHOW_INCORRECT_PIN_) == 0)
    {
        if (atoi(val)) showIncorrectPIN = true;
        else showIncorrectPIN = false;
    }
    else if (strcmp(key, CFG_TOKEN_COUNTRY_REQUIREMENTS) == 0)
    {
        SetCountryReq(val);
    }
    else if (strcmp(key, CFG_TOKEN_COUNTRY_PARAMS) == 0)
    {
        SetCountryParams(val);
    }
    else if (strcmp(key, CFG_TOKEN_EMV_LOG_UDP) == 0)
    {
        SetIPLog(val);
    }
    else if (strcmp(key, CFG_TOKEN_PARSE_CARD_DATA) == 0)
    {
        if (atoi(val)) parseCardData = true;
        else parseCardData = false;
    }
    else if (strcmp(key, CFG_TOKEN_INTERNAL_LOGGING) == 0)
    {
        internal_logging = atoi(val);
    }
    else if (strcmp(key, CFG_TOKEN_DEFAULT_PIN_HTML) == 0)
    {
        default_PIN_HTML.assign(val);
    }
    else if (strncmp(key, CFG_TOKEN_PIN_HTML, strlen(CFG_TOKEN_PIN_HTML)) == 0)
    {
        char scanFor[20];
        char tmp[50];
        size_t index = 0;
        sprintf(scanFor, "%%%ds%%d", strlen(CFG_TOKEN_PIN_HTML));
        dlog_msg("Scanning %s for %s", key, scanFor);
        int res = sscanf(key, scanFor, tmp, &index);
        if (res == 2)
        {
            PIN_HTML[index].assign(val);
        }
        else
        {
            dlog_error("Invalid token!");
            errCode = EERROR;
        }
    }
    else if (strncmp(key, CFG_TOKEN_UX_SWIPE_ON_INSERT_, strlen(CFG_TOKEN_UX_SWIPE_ON_INSERT_)) == 0)
    {
        if (atoi(val)) uxSwipeOnInsertion = true;
        else uxSwipeOnInsertion = false;
    }
    else if (strncmp(key, CFG_TOKEN_PIN_BLANK_CHAR, strlen(CFG_TOKEN_PIN_BLANK_CHAR)) == 0)
    {
        PINblankChar = val[0];
    }
    else if (strncmp(key, CFG_TOKEN_NUM_OF_EXPECTED_PIN_DIGITS, strlen(CFG_TOKEN_NUM_OF_EXPECTED_PIN_DIGITS)) == 0)
    {
        expPINdigits = atoi(val);
    }
    else if (strncmp(key, CFG_TOKEN_PIN_DEFAULT_KEYBOARD_OPTIONS, strlen(CFG_TOKEN_PIN_DEFAULT_KEYBOARD_OPTIONS)) == 0)
    {
        PINoption = atoi(val);
    }
    else if (strncmp(key, CFG_TOKEN_NEGOTIATE_ICC_SPEED, strlen(CFG_TOKEN_NEGOTIATE_ICC_SPEED)) == 0)
    {
        if (atoi(val)) iccNegotiateSpeed = true;
        else iccNegotiateSpeed = false;
    }
    else if (strncmp(key, CFG_TOKEN_INSERT_PIN_WARNINGS, strlen(CFG_TOKEN_INSERT_PIN_WARNINGS)) == 0)
    {
        if (atoi(val)) pin_warnings = true;
        else pin_warnings = false;
    }
    else if (strncmp(key, CFG_TOKEN_WARM_RESET_AFTER_ICC_OFF, strlen(CFG_TOKEN_WARM_RESET_AFTER_ICC_OFF)) == 0)
    {
        if (atoi(val)) warm_reset_after_icc_off = true;
        else warm_reset_after_icc_off = false;
    }
    else if (strncmp(key, CFG_TOKEN_REPORT_ALL_SWIPES, strlen(CFG_TOKEN_REPORT_ALL_SWIPES)) == 0)
    {
        if (atoi(val)) ignore_icc_events = true;
        else ignore_icc_events = false;
    }
    else if (strncmp(key, CFG_TOKEN_USE_MSR_COUNTERS, strlen(CFG_TOKEN_USE_MSR_COUNTERS)) == 0)
    {
        if (atoi(val)) use_msr_counters = true;
        else use_msr_counters = false;
    }
    else if (strncmp(key, CFG_TOKEN_TIME_REMOVAL, strlen(CFG_TOKEN_TIME_REMOVAL)) == 0)
    {
        timer_removal = atol(val);
    }
    else if (strcmp(key, CFG_TOKEN_HTML_PIN_OK) == 0)
    {
        html_pin_ok.assign(val);
    }
    else if (strcmp(key, CFG_TOKEN_HTML_PIN_RETRY) == 0)
    {
        html_pin_retry.assign(val);
    }
    else if (strcmp(key, CFG_TOKEN_HTML_PIN_LAST_TRY) == 0)
    {
        html_pin_last_try.assign(val);
    }
    else if (strcmp(key, CFG_TOKEN_HTML_PARAMS) == 0)
    {
        process_html_parameters(val, values_pin_ok);
        values_pin_retry = values_pin_ok;
        values_pin_last_try = values_pin_ok;
    }
    else if (strcmp(key, CFG_TOKEN_HTML_PARAMS_PIN_OK) == 0)
    {
        process_html_parameters(val, values_pin_ok);
    }
    else if (strcmp(key, CFG_TOKEN_HTML_PARAMS_PIN_RETRY) == 0)
    {
        process_html_parameters(val, values_pin_retry);
    }
    else if (strcmp(key, CFG_TOKEN_HTML_PARAMS_PIN_LAST_TRY) == 0)
    {
        process_html_parameters(val, values_pin_last_try);
    }
    else
    {
        errCode = EERROR;
    }


    return errCode;
}

void CCardAppConfig::process_html_parameters(const char * val, std::map<std::string, std::string> & values)
{
   
}

CCardAppConfig::cmd_break_t CCardAppConfig::checkPipeStatus()
{
    // Check GuiApp answer pending
    std::string pendingFrom("GUIAPP");
    if (com_verifone_ipc::check_pending(pendingFrom) == com_verifone_ipc::IPC_SUCCESS)
    {
        return CMD_BREAK_GUI_RESP_E;
    }
    // Check pending from other tasks
    pendingFrom.clear();
    if (com_verifone_ipc::check_pending(pendingFrom) == com_verifone_ipc::IPC_SUCCESS)
    {
        return checkCmdBreak();
    }
    return CMD_BREAK_NO_BREAK_E;
}

CCardAppConfig::cmd_break_t CCardAppConfig::checkCmdBreak()
{
	using namespace com_verifone_TLVLite;
	std::string rec_buffer;
	std::string source_task;
	std::string received_from;

	if (strlen(cSenderName)) source_task.assign(cSenderName);
	if(com_verifone_ipc::receive( rec_buffer, source_task, received_from) != com_verifone_ipc::IPC_SUCCESS)
	{
		return CMD_BREAK_NO_BREAK_E; /* no message or error */
	}
	IPCPacket pcancel;
	int length;

	pcancel.setCmdCode((uint8_t)(rec_buffer[0]));
	pcancel.setAppName(received_from);
	// dlog_hex(rec_buffer.data(), rec_buffer.size(), "RECVD STUFF");

	dlog_msg("Received packet [%d] from %s, addressed to %s", rec_buffer[0], received_from.c_str(), source_task.c_str());
	if(pcancel.addBuffer((const char *)(rec_buffer.c_str()), (uint32_t)(rec_buffer.size())) != IPCPACKET_SUCCESS)
	{
		dlog_msg("invalid packet ??");
		m_CmdBreak = CMD_BREAK_SEQ_ERROR_E;
		return  m_CmdBreak; /* no response */
	}

	IPCPacket response;
	response.setCmdCode(pcancel.getCmdCode());
	response.setAppName(pcancel.getAppName());
	uint8_t errCode;
	ConstData value(ConstData::getInvalid());

	if(pcancel.getCmdCode() == CANCEL_COM
				&& IPCPACKET_SUCCESS == pcancel.getTag(ConstData(arbyTagActionCode, ACTION_CODE_TS), value)
				&& value.getSize() == ACTION_CODE_SIZE )
	{
		ValueConverter<int> valueConv(value);
		unsigned char cancelCode = valueConv.getValue(0);

		dlog_msg("Received code %02X", cancelCode);
		switch (cancelCode)
		{
			case CANCEL_CODE_:
				errCode = RESP_CODE_SUCCESS; /* cancel ok */
				m_CmdBreak = CMD_BREAK_CANCEL_E;
				break;
			case BYPASS_CODE_:
				if (bypass_allowed)
				{
					errCode = RESP_CODE_SUCCESS; /* cancel ok */
					m_CmdBreak = CMD_BREAK_PIN_BYPASS_E;
					break;
				}
				// fall back to next case otherwise
				default:
					errCode = RESP_CODE_CMD_SEQ_ERROR; /* busy/seq error */
					m_CmdBreak = CMD_BREAK_SEQ_ERROR_E;
					break;
		}
	}
	else
	{
		errCode = RESP_CODE_CMD_SEQ_ERROR; /* busy/seq error */
	}

	response.addTag(ConstData(arbyTagCode, CODE_TS), MakeBERValue(errCode)); //return error code to sender app
	response.send();

	return m_CmdBreak;
}

CCardAppConfig::cmd_break_t CCardAppConfig::getAppSelection(char * aid, size_t &aidBufSize, char * label, size_t &labelBufSize)
{
    using namespace com_verifone_TLVLite;
    std::string rec_buffer;
    std::string source_task;
    std::string received_from;

    // strcpy(cSenderName, m_pCmdLockOwner);
    if(com_verifone_ipc::receive( rec_buffer, source_task, received_from) != com_verifone_ipc::IPC_SUCCESS)
    {
        return CMD_BREAK_SEQ_ERROR_E; /* no message or error */
    }

    IPCPacket psel;
    psel.setCmdCode((uint8_t)(rec_buffer[0]));
    psel.setAppName(received_from);
    if(psel.addBuffer((const char *)(rec_buffer.data()), (uint32_t)(rec_buffer.size())) != IPCPACKET_SUCCESS)
    {
        dlog_msg("invalid packet ??");
        m_CmdBreak = CMD_BREAK_SEQ_ERROR_E;
        return  m_CmdBreak; /* no response */
    }

    ConstData cAID(ConstData::getInvalid());
    ConstData cLabel(ConstData::getInvalid());
    ConstData cCancelCode(ConstData::getInvalid());


    if(psel.getCmdCode() == EXTERNAL_APP_SEL_EVT
            && IPCPACKET_SUCCESS == psel.getTag(ConstData(arbyAppAID, APP_AID_TS), cAID)
            && IPCPACKET_SUCCESS == psel.getTag(ConstData(arbyAppLabel, APP_LABEL_TS), cLabel)
      )
    {
        dlog_msg("We have selection!");
        dlog_hex(cAID.getBuffer(), cAID.getSize(), "AID");
        dlog_hex(cLabel.getBuffer(), cLabel.getSize(), "LABEL");
        if (aid)
        {
            if (cAID.getSize() <= aidBufSize)
            {
                memcpy(aid, cAID.getBuffer(), cAID.getSize());
                aidBufSize = cAID.getSize();
            }
            else
            {
                aidBufSize = 0;
                dlog_error("AID buffer too small!");
            }
        }
        if (label)
        {
            if (cLabel.getSize() <= labelBufSize)
            {
                memcpy(label, cLabel.getBuffer(), cLabel.getSize());
                labelBufSize = cLabel.getSize();
            }
            else
            {
                labelBufSize = 0;
                dlog_error("Label buffer too small!");
            }
        }
        return CMD_BREAK_NO_BREAK_E;
    }
    else if(psel.getCmdCode() == CANCEL_COM
            && IPCPACKET_SUCCESS == psel.getTag(ConstData(arbyTagActionCode, ACTION_CODE_TS), cCancelCode)
            && cCancelCode.getSize() == ACTION_CODE_SIZE )
    {
          ValueConverter<unsigned char> valueConv(cCancelCode);
          unsigned char cancelCode = valueConv.getValue(0);
          dlog_msg("Code %02X", cancelCode);
          return CMD_BREAK_CANCEL_E;
    }

    return CMD_BREAK_SEQ_ERROR_E; /* no message or error */
}


int CCardAppConfig::UpdateToken(int tagNum, unsigned char * value, int len)
{
    int result = -1;
    const char * token = NULL;
    switch (tagNum)
    {
        case 0xDFCC21:
            token = CFG_TOKEN_COUNTRY_PARAMS; break;
        case 0xDFCC4A:
            token = CFG_TOKEN_HTML_PIN_OK; break;
        case 0xDFCC4B:
            token = CFG_TOKEN_HTML_PARAMS_PIN_OK; break;
        case 0xDFCC4C:
            token = CFG_TOKEN_HTML_PIN_RETRY; break;
        case 0xDFCC4D:
            token = CFG_TOKEN_HTML_PARAMS_PIN_RETRY; break;
        case 0xDFCC4E:
            token = CFG_TOKEN_HTML_PIN_LAST_TRY; break;
        case 0xDFCC4F:
            token = CFG_TOKEN_HTML_PARAMS_PIN_LAST_TRY; break;
        case 0xDFCC62: // PIN Timeout
            token = CFG_TOKEN_PIN_TIMEOUT_; break;
        case 0xDFCC65:
            token = CFG_TOKEN_PIN_FIRSTCHAR_TIMEOUT_; break;
        case 0xDFCC66:
            token = CFG_TOKEN_PIN_INTERCHAR_TIMEOUT_; break;
        case 0xDFA217:
            token = CFG_TOKEN_OFFLINE_PIN_RADIX_SEP_; break;
        case 0xDFA219:
            token = CFG_TOKEN_OFFLINE_PIN_CURRENCY_SYMBOL_; break;
        default:
            break;
    }
    if (token)
    {
        std::string temp(reinterpret_cast<char *>(value), static_cast<size_t>(len));
        dlog_msg("Storing configuration token [%s] with value [%s]", token, temp.c_str());
        result = ini_puts(section_req, token, temp.c_str(), configFilename);
        if (result >= 0) iReadDerivedConfig(token, temp.c_str());
    }
    return result;
}



/**
 * @}
 */

/***************************************************************************
 * Module namespace: end
 **************************************************************************/
}

