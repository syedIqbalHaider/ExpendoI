#ifndef __cplusplus
#  error "This file is for C++ only!"
#endif

/***************************************************************************
 *
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 *
 **************************************************************************/
/**
 * @file        cBaseConfig.cpp
 *
 * @author      Jacek Olbrys
 *
 * @brief       Configuration file parser
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C++ Coding
 *              Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/
#include <svc.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>
#include <cstring>

#ifdef VFI_PLATFORM_VOS
#include <libpml/pml_port.h>
#endif

#include <libpml/pml_abstracted_api.h>
#include <liblog/logsys.h>
#include <libminini/minIni.h>


#include "cBaseConfig.h"

/***************************************************************************
 * Using
 **************************************************************************/

/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
namespace com_verifone_emv
{

/**
 * @addtogroup Card
 * @{ 
 */

namespace
{
    const unsigned  LINE_MAX_LEN  = 768;
}

/***************************************************************************
 * Exportable function definitions
 **************************************************************************/

/***************************************************************************
 * Exportable class members' definitions
 **************************************************************************/

void CBaseConfig::close()
{
    if(m_pFile != 0)
   {
        fclose(m_pFile);
        m_pFile = 0;
    }
}
c_card_error_t CBaseConfig::open(const  char *pFilename, const char *pMode)
{
    m_pFile = fopen(pFilename, pMode);
    if(!m_pFile)
    {
        return EERROR;
    }
    return ESUCCESS;
}

int CBaseConfig::writeFormat(const char *format, ...)
{
    int retVal;
    va_list args;
    if(!format)
    {
        return -1;
    }
    va_start(args, format);
    retVal = vfprintf (m_pFile, format, args);
    va_end(args);
    return retVal;
}

bool CBaseConfig::fileExists(const char *pFilename)
{
    return(access(pFilename, F_OK) == 0);
}

c_card_error_t CBaseConfig::iReadConfig(char const * pFilename)
{
	int k, i;
	//char key[256], section_req[40];
	//char configFilename[FILENAME_MAX+3]; /* +drive letter(F:) +'\0' */
	char value[256];
	int cfg_size = 0;
	char key[256];

	if((cfg_size = com_verifone_pml::get_env(CONFIG_FILE_NAME, configFilename, FILENAME_MAX)) > 0)
	{
		configFilename[cfg_size] = 0;
	}
	else
	{
		static const char * DEFS[] = {
			"%s.ini", "F:%s.ini", "I:%s.ini",
			"%s.cfg", "F:%s.cfg", "I:%s.cfg",
		};
		for (int i = 0; i<sizeof(DEFS)/sizeof(DEFS[0]); ++i)
		{
			sprintf(configFilename, DEFS[i], pFilename);
			if(fileExists(configFilename)) break;
		}
	}
	if((cfg_size = com_verifone_pml::get_env(CONFIG_SECTION, section_req, FILENAME_MAX)) > 0)
	{
		section_req[cfg_size] = 0;
	}
	else
	{
		strcpy(section_req, CONFIG_SECTION_DEF);
	}
	i=0;
	while (section_req[i])
	{
		section_req[i] = tolower(section_req[i]);
		i++;
	}
	dlog_msg("Processing config from file: %s, section: %s", configFilename, section_req);

	const char * SECTIONS[] = {
		NULL, section_req
	};
	bool sectionExists = true;
	for (int i = 0; i < sizeof(SECTIONS) / sizeof(SECTIONS[0]); i++)
	{
		for (k = 0; ini_getkey(SECTIONS[i], k, key, sizeof key - 1, configFilename) > 0; k++)
		{
			if(ini_gets(SECTIONS[i], key, "0", value, sizeof value -1, configFilename) > 0)
			{
				sectionExists = SECTIONS[i] != NULL;
				if(iReadDerivedConfig(key, value) == EERROR)
				{
					dlog_alert( "Invalid Config: %s=%s", key, value);
				}
			}
		}
	}
	if (!sectionExists) section_req[0] = 0;

	#if 0
	for (s = 0; ini_getsection(s, section, sizeof section - 1, configFilename) > 0; s++) 
	{
		i=0;
		while (section[i])
		{
			section[i] = tolower(section[i]);
			i++;
		}
		dlog_msg("[%s]/[%s]", section, section_req);
		if(strncmp(section, section_req, strlen(section_req)) == 0)
		{
			for (k = 0; ini_getkey(section, k, key, sizeof key - 1, configFilename) > 0; k++)
			{
				if(ini_gets(section, key, "0", value, sizeof value -1, configFilename) > 0)
				{
					dlog_msg("[%s]:%s:%s", section, key, value);
					if(iReadDerivedConfig(key, value) == EERROR)
					{
						dlog_alert( "Invalid Config: %s=%s", key, value);
					}
				}
			}
		}
	}
	#endif
	vPrintConfig();
	return ESUCCESS;

}


//Trim from spaces and delimiters
char* CBaseConfig::trim(char *string)
{
    if(!string ) return string;
    char* ret = string;
    //leading spaces
    while(*ret)
    {
        if(*ret == ' ' || *ret == '\r' || *ret == '\n' || *ret == '\t')
            ret++;
        else break;
    }
    //Tailing spaces
    char *first=NULL;
    for(char *s = ret ; *s; s++)
    {
        if(*s == ' ' || *s == '\r' || *s == '\n'|| *s == '\t')
        {
            if(!first) first = s;
        }
        else
        {
            first = NULL;
        }
    }
    if( first ) *first = '\0';
    return ret;
}

/*
 * function to print out the configuration data.
 */
void CBaseConfig::vPrintConfig()
{
    dlog_msg( "Configuration data:");
    dlog_msg( "===================");
    vPrintDerivedConfig();
    dlog_msg( "===================");
}

/**
 * @}
 */

/***************************************************************************
 * Module namspace: end
 **************************************************************************/
}

