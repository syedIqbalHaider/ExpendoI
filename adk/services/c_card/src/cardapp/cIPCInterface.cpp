#ifndef __cplusplus
#  error "This file is for C++ only!"
#endif

/***************************************************************************
 *
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 *
 **************************************************************************/
/**
 * @file        cIPCInterface.cpp
 *
 * @author      Jacek Olbrys
 *
 * @brief       IPC Interface class
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C++ Coding
 *              Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/
#include <algorithm>

#include <tlv-lite/ConstData.h>
#include <tlv-lite/ConstValue.hpp>

#include <libpml/pml.h>
#include <libpml/pml_abstracted_api.h>
#include <libpml/pml_port.h>
#include <libpml/pml_os.h>


#include "cCARDAppConfig.h"
#include "cIPCInterface.h"


#include <libipc/ipc.h>
#include <liblog/logsys.h>

#define MONITORING_COMMAND 53

#include "cTransaction.h"
#include "EMVtools.h"

/***************************************************************************
 * Using
 **************************************************************************/
using namespace com_verifone_ipc;
using namespace com_verifone_ipcpacket;
using namespace com_verifone_emv;
using namespace com_verifone_pml;


/***************************************************************************
 * External global variables
 **************************************************************************/
extern CCardAppConfig   g_CardAppConfig;
extern CEMVtrns         g_CEMVtrns;
#ifdef VFI_PLATFORM_VOS
extern logger			g_logger;
#endif


/***************************************************************************
 * Global Variables
 **************************************************************************/

/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
namespace com_verifone_interface
{

    /**
     * @addtogroup Card
     * @{ 
     */

    /***************************************************************************
     * Exportable function definitions
     **************************************************************************/
    
    /***************************************************************************
     * Exportable class members' definitions
     **************************************************************************/

    cIPCInterface::cIPCInterface(const char *szAppName, CCommandMan *pCmdMan)
    {
        pCmdManager = pCmdMan;
    }

    /**
     * Pure Virtual Method that must be implemented by any derived class to ensure
     * that the onject can be identified.
     */
    char* cIPCInterface::mIdentify()
    {
        static char InterfaceName[] = "IPC Interface";
        dlog_msg("cIPCInterface::mIdentify");
        return InterfaceName;
    }

    /**
     * Pure Virtual Method that must be implemented by any derived class to ensure
     * that the version of the object can be obtained.
     */
    int cIPCInterface::impInitialise(int argc, char *argv[])
    {
        short shResult = IPC_ERROR_SYSTEM;
        std::string my_app(CARDAPP_NAME);
        std::transform(my_app.begin(), my_app.end(), my_app.begin(), ::toupper);

        dlog_msg("IPC - impInitialise %s", CARDAPP_NAME);

        //IPC lib init
        if(com_verifone_ipc::init(my_app) == IPC_SUCCESS )
        {
                      
            dlog_msg("IPC init - OK");
            shResult = IPC_SUCCESS;

        }
        else
        {
            dlog_msg("IPC init - ERROR");
        }

        
        return shResult;
    }

    short cIPCInterface::impWaitRequest()
    {
        std::string rec_buffer;
        std::string source_task;
        std::string received_from;
        int iContinue = 1;
        static char cMonitorSenderName[21];
        unsigned char mon_cmd[] = {MONITOR_ICC_COM, arbyTagProcFlag[0], arbyTagProcFlag[1], arbyTagProcFlag[2], 0x04, 0,0,0,0};
        int mon_cmd_len = 9;
        short procRet = 0;
        event_item ev_ipc;
        ev_ipc.event_id = events::ipc;
        ev_ipc.evt.com.fd = -1;
        ev_ipc.evt.com.flags = 0;
       
        com_verifone_pml::app_notify_initialization_complete();


		memset(g_CardAppConfig.cEventNotifyName, 0, sizeof(g_CardAppConfig.cEventNotifyName));
		memset(g_CardAppConfig.cNotifyName, 0, sizeof(g_CardAppConfig.cNotifyName));
        // go into loop waiting for pipe event from MAPP
        dlog_msg("Waiting for incomming message...");
        while(iContinue)
        {
            memset(g_CardAppConfig.cSenderName, 0, sizeof(g_CardAppConfig.cSenderName));
            if(g_CEMVtrns.monitor_flags)
            {
                source_task.clear();
                received_from.clear();
                if( com_verifone_ipc::receive( rec_buffer, source_task, received_from) == IPC_SUCCESS)
                {
                    strcpy(g_CardAppConfig.cSenderName, received_from.c_str());
                    dlog_msg( "Received Msg [%d] from %s (bytes %d)", rec_buffer[0], received_from.c_str() , rec_buffer.size());
                    dlog_hex((unsigned char*)rec_buffer.c_str(), rec_buffer.size(), "[RAWDATA]");

                    // got a request
                    if(rec_buffer[0] == MONITOR_ICC_COM)
                    {
                        strcpy(cMonitorSenderName, g_CardAppConfig.cSenderName);
                        strcpy(g_CardAppConfig.cNotifyName, g_CardAppConfig.cSenderName);
                    }

					if(rec_buffer[0] == UNSOLICITED_EVENT_COM)
                    {
                        strcpy(g_CardAppConfig.cEventNotifyName, g_CardAppConfig.cSenderName);
                    }

                    mHandleRequest( (unsigned char *)rec_buffer.c_str(), (uint32_t)rec_buffer.size() );
                }
                else
                {
                    strcpy(g_CardAppConfig.cSenderName, cMonitorSenderName);
                    dlog_msg("Starting automatic ICC reader monitoring task, response to %s", g_CardAppConfig.cNotifyName);
                    procRet = mHandleRequest( mon_cmd, (uint32_t)mon_cmd_len );

                    if(procRet == IPC_ERROR_RECEIVE)
                    {
                        dlog_msg("Stop ICC monitoring and Processing received command");
                        if( com_verifone_ipc::receive( rec_buffer, source_task, received_from) == IPC_SUCCESS)
                        {
                            strcpy(g_CardAppConfig.cSenderName, received_from.c_str());
                            dlog_msg( "Received Msg [%d] from %s (bytes %d)", rec_buffer[0], received_from.c_str() , rec_buffer.size());
                            dlog_hex((unsigned char*)rec_buffer.c_str(), rec_buffer.size(), "[RAWDATA]");

                            // got a request
                            if(rec_buffer[0] == MONITOR_ICC_COM)
                            {
                                strcpy(cMonitorSenderName, g_CardAppConfig.cSenderName);
                                strcpy(g_CardAppConfig.cNotifyName, g_CardAppConfig.cSenderName);
                    		}

							if(rec_buffer[0] == UNSOLICITED_EVENT_COM)
                    		{
                        		strcpy(g_CardAppConfig.cEventNotifyName, g_CardAppConfig.cSenderName);
                    		}
                            
                            mHandleRequest( (unsigned char *)rec_buffer.c_str(), (uint32_t)rec_buffer.size() );
                        }
                        else
                        {
                            dlog_msg("No message in queue");
                        }
                    }
                }
            }
            else
            {
                
                if( com_verifone_ipc::receive( rec_buffer, source_task, received_from) == IPC_SUCCESS)
                {
                    strcpy(g_CardAppConfig.cSenderName, received_from.c_str());
                    dlog_msg( "Received Msg [%d] from %s (bytes %d)", rec_buffer[0], received_from.c_str() , rec_buffer.size());
                    dlog_hex((unsigned char*)rec_buffer.c_str(), rec_buffer.size(), "[RAWDATA]");

                    // got a request
                    if(rec_buffer[0] == MONITOR_ICC_COM)
                    {
                        strcpy(cMonitorSenderName, g_CardAppConfig.cSenderName);
                        strcpy(g_CardAppConfig.cNotifyName, g_CardAppConfig.cSenderName);
                    }

					if(rec_buffer[0] == UNSOLICITED_EVENT_COM)
                    {
                    	strcpy(g_CardAppConfig.cEventNotifyName, g_CardAppConfig.cSenderName);
                    }

                    mHandleRequest( (unsigned char *)rec_buffer.c_str(), (uint32_t)rec_buffer.size() );
                }
            }
            
        }
        return IPC_SUCCESS;
    }

    void cIPCInterface::sendErrorMessage( const char *senderName, uint8_t commandCode, uint8_t respCode )
    {
        IPCPacket response;
        response.setAppName(senderName);
        response.setCmdCode(commandCode);
        response.addTag(com_verifone_TLVLite::ConstData(arbyTagCode, CODE_TS), com_verifone_TLVLite::MakeBERValue(respCode));
        response.send();
    }

    short cIPCInterface::mHandleRequest(unsigned char* requestBuffer, int requestLength)
    {
        std::string from_app(g_CardAppConfig.cSenderName);
        IPCPacket PackIn;
        PackIn.setAppName(from_app);
        PackIn.setCmdCode(requestBuffer[0]);
        int IPC_ret;

        #ifdef VFI_PLATFORM_VOS
        g_logger.Log("Received command: %d - sending for processing", requestBuffer[0]);
        #endif

        if((IPC_ret = PackIn.addTags((const char*)requestBuffer+1, requestLength-1 )) != IPCPACKET_SUCCESS)
        {
            dlog_msg("ERROR: cIPCInterface::mHandleRequest PackIn.addTags: %d", IPC_ret);
            return IPC_ERROR_SYSTEM;
        }
        PackIn.showList();

        switch( requestBuffer[0] )
        {
            case INSTALL_VSS_COM:
            case EXECUTE_VSS_MACRO_COM:
            case START_WAITING_COM:
            case UPDATE_EMV_CONFIG_COM:
            case MONITOR_ICC_COM:
            case UNSOLICITED_EVENT_COM:
            case SEND_APDU_COM:
            case SET_GET_TLV_COM:
            case SET_PIN_PARAMS_COM:
            case VERIFY_PIN_COM:
            case SET_MESSAGE_COM:
            case L1_BEGIN_TRANSACTION_COM:
            case L1_COMPLETE_TRN_COM:
            case L2_SELECT_READ_COM:
            case L2_TRM_VERIFY_COM:
            case L2_FIRST_AC_COM:
            case L2_COMPLETE_COM:
            case L3_SELECT_COM:
            case L3_GPO_COM:
            case L3_READ_DATA_COM:
            case L3_CARDHOLDER_VERIFY_COM:
            case L3_TRM_COM:
            case L3_DATA_AUTH_COM:
            case L3_FIRST_AC_COM:
            case L3_EXT_AUTH_COM:
            case L3_SCRIPT_PROCESSING_COM:
            case L3_SECOND_AC_COM:
            case SUPER_EMV_COM:
            case RESET_EXTERNAL_READER_COM:
            case MEM_READ_I2C_CARD:
            case MEM_WRITE_I2C_CARD:
            case MEM_READ_MEMORY_CARD:
            case MEM_UPDATE_MEMORY_CARD:
            case MEM_WRITE_MEMORY_CARD:
            case RESET_EMV_CONFIG:
            case LEDS_CONTROL_COM:
                //command registered
                #ifdef VFI_PLATFORM_VOS
        		g_logger.Log("Processing command number %d requestor: %s, %d bytes of data", requestBuffer[0], from_app.c_str(), requestLength);
		        #endif
                dlog_msg("Processing command number %d requestor: %s, %d bytes of data", requestBuffer[0], from_app.c_str(), requestLength);
                break;
            default:
                //unknown
                dlog_msg("Unknown command number %d", requestBuffer[0]);
                sendErrorMessage(g_CardAppConfig.cSenderName, requestBuffer[0], RESP_CODE_UNKNOWN_CMD );
                return IPC_ERROR_SYSTEM;
        }

        
        /*if((IPC_ret = PackIn.addBuffer((const char*)requestBuffer+1, requestLength-1 )) != IPCPACKET_SUCCESS)
        {
            dlog_msg("ERROR: cIPCInterface::mHandleRequest PackIn.SetData: %d", IPC_ret);
            return IPC_ERROR_SYSTEM;
        } */
        
        if (pCmdManager != NULL)
        {
        	#ifdef VFI_PLATFORM_VOS
        	g_logger.Log("Message OK, processing...");
        	#endif
            dlog_msg("Message OK, processing...");
            if(pCmdManager->procExter( &PackIn ) == ESEQERR)
                return IPC_ERROR_RECEIVE;
        }
        else
        {
        	#ifdef VFI_PLATFORM_VOS
        	g_logger.Log("IPC - mHandleRequest Failure Cmd Manager is NULL");
        	#endif
            dlog_msg("IPC - mHandleRequest Failure Cmd Manager is NULL");
        }
        return IPC_SUCCESS;
    }

/**
 * @}
 */

/***************************************************************************
 * Module namspace: end
 **************************************************************************/

} // namespace

