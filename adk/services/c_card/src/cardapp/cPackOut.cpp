#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 *
 * Copyright (C) 2007 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/
/**
 * @file        cPackOut.cpp
 *
 * @author      Jacek Olbrys
 *
 * @brief       Out going packet builder
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C++ Coding
 *              Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/

#define MAX_IPC_MSG_SIZE 4096

extern "C"
{
//#include "crc.h"
}
#include <liblog/logsys.h>
#include <libipc/ipc.h>

#include "cPackOut.h"
#include "cCARDAppConfig.h"
#include "cIPCInterface.h"

/***************************************************************************
 * Using
 **************************************************************************/
using namespace com_verifone_emv;
using namespace com_verifone_interface;
using namespace com_verifone_ipc;

extern CCardAppConfig   g_CardAppConfig;
/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
//extern com_verifone_host::CHostConfig g_HostConfig;
//extern com_verifone_scapp::CSCAppConfig g_SCAppConfig;

namespace com_verifone_pack
{

/**
 * @addtogroup Pack
 * @{
 */

/***************************************************************************
 * Exportable function definitions
 **************************************************************************/

/***************************************************************************
 * Exportable class members' definitions
 **************************************************************************/

CPacket::c_card_error_t CPackOut::send( void )
{
    //dlog_msg("CPackOut::send");
    return send(CCardAppConfig::getName(), getAppName());
}

CPackOut::CPackOut(CPackIn *pPackIn)
    : m_spaceUsed(0)
{
    if(pPackIn)
    {
        setCommandCode(pPackIn->getCmdCode());
        setReciever(pPackIn->getAppName());
    }
}

CPacket::c_card_error_t CPackOut::send(const char *toAppName)
{
    //dlog_msg("CPackOut::send [%s]", toAppName);
    return send(CCardAppConfig::getName(), toAppName);
}

CPacket::c_card_error_t CPackOut::send(const char *fromAppName, const char *toAppName )
{
    uint8_t abyBuf[MAX_PACKET_SIZE];
    u_long uiBufLen;

    dlog_msg("CPackOut:;send: %s => %s", fromAppName, toAppName);
    if(fromAppName == NULL || toAppName == NULL ||
         strlen(fromAppName) == 0 || strlen(toAppName) == 0)
    {
        return EAPPNAME_IS_NULL;
    }

    if( EERROR == setAppName( toAppName ) )
    {
        dlog_msg("send: setAppName failed");
        return EERROR;
    }

    if( NULL == pstHeadListTags )
    {
        dlog_msg("EBERTLV_IS_EMPTY");
        return EBERTLV_IS_EMPTY;
    }

    if ( 0 == byCmdCode )
    {
        dlog_msg("invalid command");
        return EERROR;
    }
    else
    {
        abyBuf[0] = byCmdCode;
        if (SUCCESS != usnTLV_flatten_element(abyBuf+1, &uiBufLen,
            MAX_PACKET_SIZE-1, pstHeadListTags, TRUE) )
        {
            dlog_msg("send: usnTLV_flatten_element failed (head)");
            return EERROR;
        }
        /* command  */
        uiBufLen += 1;
    }

    if (uiBufLen > MAX_PACKET_SIZE)
    {
        dlog_msg("send: MAX_PACKET_SIZE");
        return EERROR;
    }

    dlog_hex(abyBuf, uiBufLen, fromAppName);
    std::string to_send; 
    const std::string to_send_name(toAppName);

    to_send.assign((const char *)abyBuf, uiBufLen);
    //to_send_name.assign(toAppName);

    if(com_verifone_ipc::send(to_send, to_send_name)!= IPC_SUCCESS)
    {
        dlog_msg("send:ipc_send failed");
        return EERROR;
    }

    return ESUCCESS;
}

CPacket::c_card_error_t CPackOut::setReciever( const char *toAppName )
{
    return setAppName( toAppName );
}

CPacket::c_card_error_t CPackOut::setCommandCode( uint8_t byCmdCode )
{
    return setCmdCode( byCmdCode );
}

CPacket::c_card_error_t CPackOut::addTag( const u_char *pbyTag, u_char byTagLen, u_char *pbyValue, u_long ulnLen )
{
    if( (m_spaceUsed + byTagLen + ulnLen + lengthSize(ulnLen)) >= MAX_IPC_MSG_SIZE ) //check if there is enougth free space in buff
    {
        dlog_msg("CPackOut::addTag [%x%x] not enougth space",pbyTag[0],pbyTag[1]);
        return CPacket::ENOSPACE;
    }

    m_spaceUsed += byTagLen + ulnLen + lengthSize(ulnLen); //increment used space counter

    dlog_msg("CPackOut::addTag [%x%x]",pbyTag[0],pbyTag[1]);
    return addTagTLV( pbyTag, byTagLen, pbyValue, ulnLen );
}

uint32_t CPackOut::lengthSize(uint32_t length)
{
    dlog_msg("CPackOut::lengthSize data size %d", length);

    if( length < 127 )   //this length can be send by one octet in TLV
    {
        //dlog_msg("Size will be send as 1 byte");
        return 1;
    }

    if( (length / 256) > 0 )
    {
        //dlog_msg("Size will be send as 3 bytes");
        return 3;
    }
    else
    {
        //dlog_msg("Size will be send as 2 bytes");
        return 2;
    }
}

uint32_t CPackOut::spaceLeft()
{
    return MAX_IPC_MSG_SIZE - m_spaceUsed;
}

/**
 * @}
 */

/***************************************************************************
 * Module namspace: end
 **************************************************************************/
}

