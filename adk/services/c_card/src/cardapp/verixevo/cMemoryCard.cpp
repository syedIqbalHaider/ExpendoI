#ifndef __cplusplus
#  error "This file is for C++ only!"
#endif

/***************************************************************************
 *
 * Copyright (C) 2006 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 *
 **************************************************************************/
/**
 * @file        cMemoryCard.cpp
 *
 * @author      Jacek Olbrys
 *
 * @brief       Memory Card Handler
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C++ Coding
 *              Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/
#include <svc.h>
#include <string.h>

#include <cardslot.h>

#include <liblog/logsys.h>

#include "cMemoryCard.h"
#include "cEMV.h"
#include "cTransaction.h"

#include <SLE44x2.h>

/***************************************************************************
 * Using
 **************************************************************************/
using namespace com_verifone_emv;

extern CEMV             g_CEMV;
//extern CEMVtrns         g_CEMVtrns;

/***************************************************************************
 * Module namespace: begin
 **************************************************************************/

namespace com_verifone_memory_card
{

    
    /**
     * @addtogroup Card
     * @{ 
     */

    /***************************************************************************
     * Exportable function definitions
     **************************************************************************/

    /***************************************************************************
     * Exportable class members' definitions
     **************************************************************************/

    CMemoryCard::CMemoryCard(): weHaveSLE442(false)
    {
        clear();
    }

    void CMemoryCard::clear()
    {
        memset(memoryCardImage, 0, sizeof(memoryCardImage));
        memset(memoryCardOriginalImage, 0, sizeof(memoryCardOriginalImage));
        imageSize = 0;
    }

    /* Detects card inserted and verifies whether it's Memory card
     */
    bool CMemoryCard::is_card_memory_card(uint8_t *ATR)
    {
        bool result = false;
        unsigned char retVal;
        unsigned char aucAPDUTxBuf[CARD_APP_DEF_ATR_LEN / 2];
        unsigned char aucAPDURxBuf[CARD_APP_DEF_ATR_LEN / 2];
        unsigned short usAPDUTxLen = 0;
        unsigned short usAPDURxLen = 0;

        // set card type to iso 7816 sync card of type SLE44x2
        retVal = Set_Card_Type( g_CEMV.ICC_reader, SLE44X2, ISO_7816 );
        dlog_msg("Set Card Type returned %d", retVal);
        if (retVal != CARDSLOT_SUCCESS)
        {
            dlog_alert("Set_Card_Type() call has failed - no SLE4442 card!");
        }
        else
        {
            retVal = Reset_CardSlot( g_CEMV.ICC_reader, RESET_COLD );
            dlog_msg("Sync reset returned %d", retVal );

            if( retVal != CARDSLOT_SUCCESS )
            {
                dlog_alert("Reset_CardSlot has failed - no SLE442 card found!");
            }
            else
            {
                // get ATR
                SLE44X2_GET_ATR_BYTES( aucAPDUTxBuf, usAPDUTxLen );
                if( Transmit_APDU( g_CEMV.ICC_reader, aucAPDUTxBuf, usAPDUTxLen, aucAPDURxBuf, &usAPDURxLen ) != CARDSLOT_SUCCESS )
                {
                    dlog_error( "error reading ATR" );
                    // card in slot, no ATR returned
                }
                else
                {
                    // if we get here we have some ATR for SLE4442 card
                    unsigned char aucBadATR[CARD_APP_DEF_ATR_LEN / 2];
                    memset(aucBadATR, 0xFF, sizeof(aucBadATR));
                    // Kamil_P1: Check whether the card has REALLY been detected... (workaround)
                    if (!memcmp(aucAPDURxBuf, aucBadATR, usAPDURxLen))
                    {
                        // Bad card detected, ATR is invalid
                        dlog_msg ("Doesn't seem as SLE442 card - ATR length %d, data 0xFF only. Assuming bad card", usAPDURxLen);
                    }
                    else
                    {
                        // It's SLE442 card!
                        dlog_msg("SLE442 card detected!");
                        SVC_HEX_2_DSP((const char*)aucAPDURxBuf, reinterpret_cast<char *>(ATR), usAPDURxLen);
                        dlog_msg("ATR: %s", ATR);
                        //memcpy(ATR, aucAPDURxBuf, iATRLength);
                        //dlog_hex(ATR, iATRLength, "ATR");
                        //ATR[iATRLength] = 0;
                        result = true;
                        weHaveSLE442 = true;
                    }
                }
            }
        }
        if (result == false)
        {
            //initialisation attempts  might've let cardslot in a funny state - added to avoid any possible issues... 
            retVal = Terminate_CardSlot(g_CEMV.ICC_reader, SWITCH_OFF_CARD);
            dlog_msg("Terminate CardSlot returned %d", retVal);
        }
        return result;
    }

    /* Reads the card (INTERNAL!) 
     * Assumes that Memory card is inserted - no checks are performed here!
     */
    int32_t CMemoryCard::read_card_data(uint8_t *memoryCardImage)
    {
        uint8_t aucAPDUTxBuf[MEM_CARD_BUFFER_SIZE];    // = max 256 byte image + 6 for command overhead
        uint8_t aucAPDURxBuf[MEM_CARD_BUFFER_SIZE];    // = max 256 byte image + 6 for command overhead
        int32_t imageSize;
        uint16_t usAPDUTxLen;
        uint16_t usAPDURxLen = 0;
        char cRet;

        dlog_msg ( "MemoryCardRead" );
        clear();

        if (!weHaveSLE442)
        {
            dlog_error(" No SLE442 card detected!");
            return MEM_FAILURE;
        }

        // get ATR
        SLE44X2_GET_ATR_BYTES( aucAPDUTxBuf, usAPDUTxLen );
        if( Transmit_APDU( g_CEMV.ICC_reader, aucAPDUTxBuf, usAPDUTxLen, aucAPDURxBuf, &usAPDURxLen ) != CARDSLOT_SUCCESS )
        {
            dlog_error ( "error reading ATR" );
            return MEM_FAILURE;
        }
        memcpy( memoryCardImage, aucAPDURxBuf, 4 );
        imageSize = 4;

        // read the whole card into buffers
        SLE44X2_READ_MAIN_MEMORY( aucAPDUTxBuf, 0x04, 0xFC, usAPDUTxLen );
        if( Transmit_APDU( g_CEMV.ICC_reader, aucAPDUTxBuf, usAPDUTxLen, aucAPDURxBuf, &usAPDURxLen ) != CARDSLOT_SUCCESS )
        {
            dlog_error ( "error reading data" );
            return MEM_FAILURE;
        }
        memcpy( &memoryCardImage[4], aucAPDURxBuf, usAPDURxLen );
        imageSize += usAPDURxLen;

        return imageSize;
    }


    /* Reads card data
     */
    int32_t CMemoryCard::read_card()
    {
        int32_t res = read_card_data(memoryCardImage);
        if (res != MEM_FAILURE)
        {
            imageSize = res;
            memcpy( memoryCardOriginalImage, memoryCardImage, MEM_CARD_BUFFER_SIZE );
            res = MEM_OK;
        }
        return res;
    }

    /* Gets bytes from buffer
     */
    int32_t CMemoryCard::get_bytes(uint8_t *buffer, size_t bufferSize, uint8_t address, uint8_t bytesCount)
    {
        if (imageSize == 0)
        {
            dlog_error("No data read!");
            return MEM_FAILURE;
        }
        if (imageSize < address + bytesCount)
        {
            dlog_error("Invalid parameters!");
            return MEM_INVALID_PARAMETERS;
        }
        if (bufferSize >= bytesCount)
        {
            // Copy
            memcpy( buffer, memoryCardOriginalImage + address, bytesCount );
            bufferSize = bytesCount;
            return bytesCount;
        }
        dlog_error("Buffer too small!");
        return MEM_INVALID_PARAMETERS;
    }

    /* Writes bytes to internal buffer
     */
    int32_t CMemoryCard::write_bytes(uint8_t *buffer, uint8_t address, uint8_t bytesCount)
    {
        if (imageSize == 0)
        {
            dlog_error("No data read!");
            return MEM_NO_DATA;
        }
        if (imageSize < address + bytesCount)
        {
            dlog_error("Invalid parameters!");
            return MEM_INVALID_PARAMETERS;
        }

        memcpy( memoryCardImage, buffer, bytesCount );
        return MEM_OK;
    }

    /* Updates card
     */
    int32_t CMemoryCard::update_card(uint8_t *pin)
    {
        uint8_t aucAPDUTxBuf[MEM_CARD_BUFFER_SIZE];    // = max 256 byte image + 6 for command overhead
        uint8_t aucAPDURxBuf[MEM_CARD_BUFFER_SIZE];    // = max 256 byte image + 6 for command overhead
        uint8_t aucTempImage[MEM_CARD_BUFFER_SIZE];    // = max 256 byte image + 6 for command overhead
        uint8_t ucErrCount;
        uint16_t usTempLen;
        uint16_t usAPDUTxLen;
        uint16_t usAPDURxLen;
        int16_t uiFirstChangedByte = -1;
        int16_t uiLastChangedByte = -1;
        int32_t retVal;
        int ii;
        uint8_t ucUpdateVerified;

        dlog_msg ( "MemoryCardUpdate" );

        if (imageSize == 0)
        {
            dlog_error("No data read!");
            return MEM_NO_DATA;
        }

        // check if the current image is the same as data on card, if it is then no point writing to the card
        if( memcmp( memoryCardImage, memoryCardOriginalImage, imageSize ) == 0 )
        {
            dlog_error ( "Data Not Changed" );
            return MEM_MEMORY_UNCHANGED;
        }
        // read 4-byte security memory
        SLE4442_READ_SECURITY_MEMORY( aucAPDUTxBuf, usAPDUTxLen );
        if( Transmit_APDU( g_CEMV.ICC_reader, aucAPDUTxBuf, usAPDUTxLen, aucAPDURxBuf, &usAPDURxLen ) != CARDSLOT_SUCCESS )
        {
            dlog_error ( "error reading security memory" );
            return MEM_MEMORY_UNCHANGED;
        }
        // analyse security memory
        //dlog_msg ( "Security Memory %2.2X %2.2X %2.2X %2.2X", aucAPDURxBuf[0], aucAPDURxBuf[1], aucAPDURxBuf[2], aucAPDURxBuf[3] );
        ucErrCount = aucAPDURxBuf[0];

        /*	Byte 1 values:
        ==============
        0x07 = 00000111 or 3 tries (maximum).
        0x03 = 00000011 or 2 tries.
        0x01 = 00000001 or 1 try.
        0x00 = 00000000 or card blocked. */

        // write one bit of EC to 0
        if( ucErrCount == 0x07 )
        {
            ucErrCount = 0x03;
        }
        else if( ucErrCount == 0x03 )
        {
            ucErrCount = 0x01;
        }
        else
        {
            // invalid value, assume card locked
            dlog_error ( "invalid err count %d, assuming card locked", ucErrCount );
            return MEM_PWD_INCORRECT_1TRY_LEFT;
        }

        // write newly decremented errcount to the card
        SLE4442_UPDATE_SECURITY_MEMORY( aucAPDUTxBuf, 0, ucErrCount, usAPDUTxLen );
        if( Transmit_APDU( g_CEMV.ICC_reader, aucAPDUTxBuf, usAPDUTxLen, aucAPDURxBuf, &usAPDURxLen ) != CARDSLOT_SUCCESS )
        {
            dlog_error ( "error updating security memory" );
            return MEM_MEMORY_UNCHANGED;
        }
        // compare verification for byte 1
        SLE4442_COMPARE_VERIFICATION_DATA( aucAPDUTxBuf, 1, pin[0], usAPDUTxLen );
        if( Transmit_APDU( g_CEMV.ICC_reader, aucAPDUTxBuf, usAPDUTxLen, aucAPDURxBuf, &usAPDURxLen ) != CARDSLOT_SUCCESS )
        {
            dlog_error ( "error comparing byte 1" );
            return MEM_MEMORY_UNCHANGED;
        }
        // compare verification for byte 2
        SLE4442_COMPARE_VERIFICATION_DATA( aucAPDUTxBuf, 2, pin[1], usAPDUTxLen );
        if( Transmit_APDU( g_CEMV.ICC_reader, aucAPDUTxBuf, usAPDUTxLen, aucAPDURxBuf, &usAPDURxLen ) != CARDSLOT_SUCCESS )
        {
            dlog_error ( "error comparing byte 2" );
            return MEM_MEMORY_UNCHANGED;
        }
        // compare verification for byte 3
        SLE4442_COMPARE_VERIFICATION_DATA( aucAPDUTxBuf, 3, pin[2], usAPDUTxLen );
        if( Transmit_APDU( g_CEMV.ICC_reader, aucAPDUTxBuf, usAPDUTxLen, aucAPDURxBuf, &usAPDURxLen ) != CARDSLOT_SUCCESS )
        {
            dlog_error ( "error comparing byte 3" );
            return MEM_MEMORY_UNCHANGED;
        }

        // reset errcount to 3 retries
        SLE4442_UPDATE_SECURITY_MEMORY( aucAPDUTxBuf, 0, 0xFF, usAPDUTxLen );
        if( Transmit_APDU( g_CEMV.ICC_reader, aucAPDUTxBuf, usAPDUTxLen, aucAPDURxBuf, &usAPDURxLen ) != CARDSLOT_SUCCESS )
        {
            dlog_error ( "errcount reset failed" );
            return MEM_MEMORY_UNCHANGED;
        }

        // read errcount back
        // read 4-byte security memory
        SLE4442_READ_SECURITY_MEMORY( aucAPDUTxBuf, usAPDUTxLen );
        if( Transmit_APDU( g_CEMV.ICC_reader, aucAPDUTxBuf, usAPDUTxLen, aucAPDURxBuf, &usAPDURxLen ) != CARDSLOT_SUCCESS )
        {
            dlog_error ( "error reading security memory" );
            return MEM_MEMORY_UNCHANGED;
        }
        // analyse security memory
        //dlog_msg ( "Security Memory %2.2X %2.2X %2.2X %2.2X", aucAPDURxBuf[0], aucAPDURxBuf[1], aucAPDURxBuf[2], aucAPDURxBuf[3] );
        ucErrCount = aucAPDURxBuf[0];

        // if errcount = 3 retries then unlock was successful
        // if errcount != 3 retries then unlock failed, remaining tries is number of bits left
        if( ucErrCount != 0x07 )
        {
            // unlock failed
            dlog_msg ( "Reset Counter error, unlock failed" );
            if( ucErrCount == 0x03 )
            {
                dlog_alert ( "2 remaining attempts" );
                return MEM_PWD_INCORRECT_2TRIES_LEFT;
            }
            else if( ucErrCount == 0x01 )
            {
                dlog_alert ( "1 remaining attempt" );
                return MEM_PWD_INCORRECT_1TRY_LEFT;
            }
            else if( ucErrCount == 0x00 )
            {
                dlog_error ( "0 remaining attempts, card locked" );
                return MEM_PWD_INCORRECT_1TRY_LEFT;
            }
            dlog_msg ( "error response, ucErrCount=%d", ucErrCount );
            return MEM_MEMORY_CHANGED;
        }

        // card is now unlocked, write changes to the card
        // figure out range of bytes that have changed, then update this range only in one write
        for( ii=0; ii<imageSize; ii++ )
        {
            if( memoryCardOriginalImage[ii] != memoryCardImage[ii] )
            {
                if( uiFirstChangedByte < 0 )
                    uiFirstChangedByte = ii;
                uiLastChangedByte = ii;
            }
        }
        //dlog_msg ( "range %d to %d updated", uiFirstChangedByte, uiLastChangedByte );

        // if range is entire card image (256 bytes), then need to split it into 2 writes as the 
        // SLE44X2_UPDATE_MAIN_MEMORY macro can only update 0xFF bytes at a time
        if( uiLastChangedByte - uiFirstChangedByte == 0xFF )
        {
            // update in two requests, half at a time
            SLE44X2_UPDATE_MAIN_MEMORY( aucAPDUTxBuf, 0, 0x80, memoryCardImage, usAPDUTxLen );
            if( Transmit_APDU( g_CEMV.ICC_reader, aucAPDUTxBuf, usAPDUTxLen, aucAPDURxBuf, &usAPDURxLen ) != CARDSLOT_SUCCESS )
            {
                dlog_error ( "error updating main memory" );
                return MEM_MEMORY_CHANGED;
            }
            SLE44X2_UPDATE_MAIN_MEMORY( aucAPDUTxBuf, 0x80, 0x80, (memoryCardImage+0x80), usAPDUTxLen );
            if( Transmit_APDU( g_CEMV.ICC_reader, aucAPDUTxBuf, usAPDUTxLen, aucAPDURxBuf, &usAPDURxLen ) != CARDSLOT_SUCCESS )
            {
                dlog_error ( "error updating main memory" );
                return MEM_MEMORY_CHANGED;
            }
        }
        else
        {
            // update it with one write command
            SLE44X2_UPDATE_MAIN_MEMORY( aucAPDUTxBuf, uiFirstChangedByte, uiLastChangedByte-uiFirstChangedByte+1, (memoryCardImage+uiFirstChangedByte), usAPDUTxLen );
            if( Transmit_APDU( g_CEMV.ICC_reader, aucAPDUTxBuf, usAPDUTxLen, aucAPDURxBuf, &usAPDURxLen ) != CARDSLOT_SUCCESS )
            {
                dlog_error ( "error updating main memory" );
                return MEM_MEMORY_CHANGED;
            }
        }

        // read the data back to check that the update worked ok
        usTempLen = read_card_data( aucTempImage );
        if( usTempLen == 0 )
        {
            dlog_msg ( "error reading card" );
            return MEM_MEMORY_CHANGED;
        }
        // compare against image, byte for byte for debug purposes
        ucUpdateVerified = 1;
        //dlog_msg ( "image size %d", imageSize );
        for( ii=0; ii<imageSize; ii++ )
        {
            if( aucTempImage[ii] != memoryCardImage[ii] )
            {
                dlog_msg ( "error - byte %d not updated card=%2.2X, buffer=%2.2X", ii, aucTempImage[ii], memoryCardImage[ii] );
                ucUpdateVerified = 0;
            }
        }
        //   if( memcmp( aucTempImage, m_aucSLE4442Image, imageSize ) != 0 )
        if( ucUpdateVerified == 0 )
        {
            dlog_msg ( "error - main memory not updated correctly" );
            return MEM_MEMORY_CHANGED;
        }

        // got here, must have updated okay
        //dlog_msg ( "card updated okay" );
        return MEM_OK;
    }

/**
 * @}
 */

/***************************************************************************
 * Module namspace: end
 **************************************************************************/
}

