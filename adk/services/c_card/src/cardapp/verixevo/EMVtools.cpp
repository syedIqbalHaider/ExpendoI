#ifndef __cplusplus
#  error "This file is for C++ only!"
#endif

/***************************************************************************
 *
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 *
 **************************************************************************/
/**
 * @file        EMVTools.cpp
 *
 * @author      Jacek Olbrys
 *
 * @brief       EMV Tools
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C++ Coding
 *              Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/
#ifdef VFI_GUI_GUIAPP
#include "GuiApp.h"
#endif

#include <svc.h>
#include <svc_sec.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <vector>

#include <strncasecmp.h>
//#include <memory> // for auto_ptr


extern "C"
{
#include "emvsizes.h"
#include "est.h"
#include "mvt.h"

//#include <VXEMVAP_confio.h>
}
#include <liblog/logsys.h>
#include <libprompts/user_prompt_lite.hpp>

#include "app_selection.h"
#include "cEMV.h"
#include <emvTags.hpp>
#include <EMVResult.hpp>
#include "cTransaction.h"
#include "cCARDAppConfig.h"
#include "EMVtools.h"
#include <libminini/minIni.h>

#include <libpml/pml.h>

//#ifdef VFI_GUI_DIRECTGUI
#include "Pin_gui.h"
//#endif

//#include <libtags/tags.h> 

#include <tlv-lite/ConstData.h>
#include <tlv-lite/ValueConverter.hpp>
#include <tlv-lite/ConstValue.hpp>

extern "C"
{
#include "vxemvap.h"
}



/***************************************************************************
 * Definitions
 **************************************************************************/
#define CURR_CODE_LEN 8
#define CURRENCY_CODE_NULL {0, {'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0'}}
#define CARD_APP_DEF_MAX_TLV_LEN    256
#define M_MAX_PIN_LENGTH_MAX 12
#define M_MIN_PIN_LENGTH_MIN 4
#define WINDOW_MAX 0xFFFFFFFF

/***************************************************************************
 * Using
 **************************************************************************/
using namespace com_verifone_guiapi;
using namespace com_verifone_emv;
using namespace com_verifone_pml;


extern CCardAppConfig   g_CardAppConfig;
extern CEMV         g_CEMV;
extern CEMVtrns     g_CEMVtrns;

extern EMV_CONFIG EMVGlobConfig;

#define VX600 1

typedef struct currency_code_s
{
      uint16_t code; /**< Numeric currency code  */
      char name[CURR_CODE_LEN+1]; /**< Currency code name */
} currency_code_t;

const currency_code_t M_CURRENCY_CODES[] =
    {
        {9999, "   "}, {784, "AED"}, {971, "AFN"}, {8, "ALL"}, {51, "AMD"}, {532, "ANG"},
        {973, "AOA"}, {32, "ARS"}, {36, "AUD"}, {533, "AWG"}, {944, "AZN"},
        {977, "BAM"}, {52, "BBD"}, {50, "BDT"}, {975, "BGN"}, {48, "BHD"},
        {108, "BIF"}, {60, "BMD"}, {96, "BND"}, {68, "BOB"}, {984, "BOV"},
        {986, "BRL"}, {44, "BSD"}, {64, "BTN"}, {72, "BWP"}, {974, "BYR"},
        {84, "BZD"}, {124, "CAD"}, {976, "CDF"}, {947, "CHE"}, {756, "CHF"},
        {948, "CHW"}, {990, "CLF"}, {152, "CLP"}, {156, "CNY"}, {170, "COP"},
        {970, "COU"}, {188, "CRC"}, {192, "CUP"}, {132, "CVE"}, {203, "CZK"},
        {262, "DJF"}, {208, "DKK"}, {214, "DOP"}, {12, "DZD"}, {233, "EEK"},
        {818, "EGP"}, {232, "ERN"}, {230, "ETB"}, {978, "EUR"}, {242, "FJD"},
        {238, "FKP"}, {826, "GBP"}, {981, "GEL"}, {936, "GHS"}, {292, "GIP"},
        {270, "GMD"}, {324, "GNF"}, {320, "GTQ"}, {328, "GYD"}, {344, "HKD"},
        {340, "HNL"}, {191, "HRK"}, {332, "HTG"}, {348, "HUF"}, {360, "IDR"},
        {376, "ILS"}, {356, "INR"}, {368, "IQD"}, {364, "IRR"}, {352, "ISK"},
        {388, "JMD"}, {400, "JOD"}, {392, "JPY"}, {404, "KES"}, {417, "KGS"},
        {116, "KHR"}, {174, "KMF"}, {408, "KPW"}, {410, "KRW"}, {414, "KWD"},
        {136, "KYD"}, {398, "KZT"}, {418, "LAK"}, {422, "LBP"}, {144, "LKR"},
        {430, "LRD"}, {426, "LSL"}, {440, "LTL"}, {428, "LVL"}, {434, "LYD"},
        {504, "MAD"}, {498, "MDL"}, {969, "MGA"}, {807, "MKD"}, {104, "MMK"},
        {496, "MNT"}, {446, "MOP"}, {478, "MRO"}, {480, "MUR"}, {462, "MVR"},
        {454, "MWK"}, {484, "MXN"}, {979, "MXV"}, {458, "MYR"}, {943, "MZN"},
        {516, "NAD"}, {566, "NGN"}, {558, "NIO"}, {578, "NOK"}, {524, "NPR"},
        {554, "NZD"}, {512, "OMR"}, {590, "PAB"}, {604, "PEN"}, {598, "PGK"},
        {608, "PHP"}, {586, "PKR"}, {985, "PLN"}, {600, "PYG"}, {634, "QAR"},
        {946, "RON"}, {941, "RSD"}, {643, "RUB"}, {646, "RWF"}, {682, "SAR"},
        {90, "SBD"}, {690, "SCR"}, {938, "SDG"}, {752, "SEK"}, {702, "SGD"},
        {654, "SHP"}, {703, "SKK"}, {694, "SLL"}, {706, "SOS"}, {968, "SRD"},
        {678, "STD"}, {760, "SYP"}, {748, "SZL"}, {764, "THB"}, {972, "TJS"},
        {795, "TMM"}, {788, "TND"}, {776, "TOP"}, {949, "TRY"}, {780, "TTD"},
        {901, "TWD"}, {834, "TZS"}, {980, "UAH"}, {800, "UGX"}, {840, "USD"},
        CURRENCY_CODE_NULL
    };

// Based on this table: http://www.xe.com/symbols.php
// Some symbols were not listed and are left as codes
const currency_code_t M_CURRENCY_SYMBOLS[] =
     {
        {9999, "   "},                     {784, "AED"},                       {971, "\xD8\x8B"},
        {8, "\x4C\x65\x6B"},               {51, "AMD"},                        {532, "\xC6\x92"},
        {973, "AOA"},                      {32, "$"},                          {36, "$"},
        {533, "\xC6\x92"},                 {944, "\xD0\xBC\xD0\xB0\xD0\xBD"},
        {977, "\x4B\x4D"},                 {52, "$"},                          {50, "BDT"},
        {975, "\xD0\xBB\xD0\xB2"},         {48, "BHD"},
        {108, "BIF"},                      {60, "$"},                          {96, "$"},
        {68, "$b"},                        {984, "BOV"},
        {986, "R$"},                       {44, "$"},                          {64, "BTN"},
        {72, "P"},                         {974, "p."},
        {84, "BZ$"},                       {124, "$"},                         {976, "CDF"},
        {947, "CHE"},                      {756, "CHF"},
        {948, "CHW"},                      {990, "CLF"},                       {152, "$"},
        {156, "\xC2\xA5"},                 {170, "$"},
        {970, "COU"},                      {188, "\xE2\x82\xA1"},              {192, "\xE2\x82\xB1"},
        {132, "CVE"},                      {203, "\x4B\xC4\x8D"},
        {262, "DJF"},                      {208, "kr"},                        {214, "RD$"},
        {12, "DZD"},                       {233, "kr"},
        {818, "\xC2\xA3"},                 {232, "ERN"},                       {230, "ETB"},
        //{978, "\xE2\x82\xAC"},             {242, "$"},
        // Kamil_P1: Due to font incompatibility, changed the EUR character to the one matching font. 
        {978, "\xC2\x80"},                 {242, "$"},
        {238, "\xC2\xA3"},                 {826, "\xC2\xA3"},                  {981, "GEL"},
        {936, "GHS"},                      {292, "\xC2\xA3"},
        {270, "GMD"},                      {324, "GNF"},                       {320, "Q"},
        {328, "$"},                        {344, "$"},
        {340, "L"},                        {191, "kn"},                        {332, "HTG"},
        {348, "Ft"},                       {360, "Rp"},
        {376, "\xE2\x82\xAA"},             {356, "INR"},                       {368, "IQD"},
        {364, "\xEF\xB7\xBC"},             {352, "ISK"},
        {388, "J$"},                       {400, "JOD"},                       {392, "\xC2\xA5"},
        {404, "KES"},                      {417, "\xD0\xBB\xD0\xB2"},
        {116, "\xE1\x9F\x9B"},             {174, "KMF"},                       {408, "\xE2\x82\xA9"},
        {410, "\xE2\x82\xA9"},             {414, "KWD"},
        {136, "$"},                        {398, "\xD0\xBB\xD0\xB2"},          {418, "\xE2\x82\xAD"},
        {422, "\xC2\xA3"},                 {144, "\xE2\x82\xA8"},
        {430, "$"},                        {426, "LSL"},                       {440, "Lt"},
        {428, "Ls"},                       {434, "LYD"},
        {504, "MAD"},                      {498, "MDL"},                       {969, "MGA"},
        {807, "\xD0\xB4\xD0\xB5\xD0\xBD"}, {104, "MMK"},
        {496, "\xE2\x82\xAE"},             {446, "MOP"},                       {478, "MRO"},
        {480, "\xE2\x82\xA8"},             {462, "MVR"},
        {454, "MWK"},                      {484, "$"},                         {979, "MXV"},
        {458, "RM"},                       {943, "MT"},
        {516, "$"},                        {566, "\xE2\x82\xA6"},              {558, "C$"},
        {578, "kr"},                       {524, "\xE2\x82\xa8"},
        {554, "$"},                        {512, "\xEF\xB7\xBC"},              {590, "B/."},
        {604, "S/."},                      {598, "PGK"},
        {608, "\xE2\x82\xB1"},             {586, "\xE2\x82\xA8"},              {985, "z\xC5\x82"},
        {600, "Gs"},                       {634, "\xEF\xB7\xBC"},
        {946, "lei"},                      {941, "\xD0\x94\xD0\xB8\xD0\xBD."}, 
        {643, "\xD1\x80\xD1\x83\xD0\xB1"}, {646, "RWF"},                       {682, "\xEF\xB7\xBC"},
        {90, "$"},                         {690, "\xE2\x82\xA8"},              {938, "SDG"},
        {752, "kr"},                       {702, "$"},
        {654, "\xC2\xA3"},                 {703, "SKK"},                       {694, "SLL"},
        {706, "S"},                        {968, "$"},
        {678, "STD"},                      {760, "\xC2\xA3"},                  {748, "SZL"},
        {764, "\xE0\xB8\xBF"},             {972, "TJS"},
        {795, "TMM"},                      {788, "TND"},                       {776, "TOP"},
        {949, "TL"},                       {780, "TT$"},
        {901, "NT$"},                      {834, "TZS"},                       {980, "\xE2\x82\xB4"},
        {800, "UGX"},                      {840, "$"},
        CURRENCY_CODE_NULL
     };

int getPromptLocal(int prompt_id, char *prompt)
{
	if(prompt != NULL)
	{
		prompt[0] = 0;
		switch(prompt_id)
		{
			case CARD_APP_GUI_ENTER_PIN:
				strcpy(prompt, g_CardAppConfig.GetEnterPINMsg().c_str());
				break;
			case CARD_APP_GUI_INCORRECT_PIN:
				strcpy(prompt, g_CardAppConfig.GetIncorrectPINMsg().c_str());
				break;
			case CARD_APP_GUI_LAST_PIN:
				strcpy(prompt, g_CardAppConfig.GetLastPINMsg().c_str());
				break;
		}
	}
	return strlen(prompt);

}

void check_RAM(void)
{
    dlog_msg(">>>>STACK: %ld/%ld HEAP: %ld/%ld<<<<<", _stack_max(), _stacksize, _heap_max(), _heapsize);
}

#define GEN_VER_SIZE             16
bool loadMVTRec(MVT_REC * mvt_rec)
{
    bool res = false;
    int handle = open(MVT_FILENAME, O_RDONLY);
    if (handle <= 0)
    {
        dlog_error("Cannot open MVT.DAT!");
        return res;
    }
    lseek(handle, GEN_VER_SIZE, SEEK_SET);
    if (read(handle, (char *)mvt_rec, SIZE_MVT_REC) == SIZE_MVT_REC)
    {
        dlog_msg("Record read ok");
        res = true;
    }
    else
    {
        dlog_error("Cannot read first MVT record!");
    }
    close(handle);
    return res;
}

void printConfig(char *configFilename)
{
	
	char section[40], key[40], value[256];
	int i, s, k;

	dlog_msg("Procesing config from file: %s", configFilename);
	
	for (s = 0; ini_getsection(s, section, sizeof section, configFilename) > 0; s++) 
	{
		i=0;
		while (section[i])
  		{
    		section[i] = toupper(section[i]);
    		i++;
  		}
		dlog_msg("[%s]", section);
		for (k = 0; ini_getkey(section, k, key, sizeof key, configFilename) > 0; k++)
		{
			if(ini_gets(section, key, "0", value, sizeof(value), configFilename) > 0)
			{
				dlog_msg("[%s]:%s=%s", section, key, value);
			}
		}
	}
}

//this function adds AIDs to the first empty slot in EST file
//RETURN: 
int addEST_AID(char *AID, EST_REC *EST_tmp)
{
	int iRet = 0;
	
	if(strlen(EST_tmp->szSupportedAID1) <= 0)
	{
		iRet = 1;
		strcpy(EST_tmp->szSupportedAID1, AID);
		return iRet;
	}
	if(strlen(EST_tmp->szSupportedAID2) <= 0)
	{
		iRet = 2;
		strcpy(EST_tmp->szSupportedAID2, AID);
		return iRet;
	}
	if(strlen(EST_tmp->szSupportedAID3) <= 0)
	{
		iRet = 3;
		strcpy(EST_tmp->szSupportedAID3, AID);
		return iRet;
	}
	if(strlen(EST_tmp->szSupportedAID4) <= 0)
	{
		iRet = 4;
		strcpy(EST_tmp->szSupportedAID4, AID);
		return iRet;
	}
	if(strlen(EST_tmp->szSupportedAID5) <= 0)
	{
		iRet = 5;
		strcpy(EST_tmp->szSupportedAID5, AID);
		return iRet;
	}
	if(strlen(EST_tmp->szSupportedAID6) <= 0)
	{
		iRet = 6;
		strcpy(EST_tmp->szSupportedAID6, AID);
		return iRet;
	}
	if(strlen(EST_tmp->szSupportedAID7) <= 0)
	{
		iRet = 7;
		strcpy(EST_tmp->szSupportedAID7, AID);
		return iRet;
	}
	if(strlen(EST_tmp->szSupportedAID8) <= 0)
	{
		iRet = 8;
		strcpy(EST_tmp->szSupportedAID8, AID);
		return iRet;
	}
	if(strlen(EST_tmp->szSupportedAID9) <= 0)
	{
		iRet = 9;
		strcpy(EST_tmp->szSupportedAID9, AID);
		return iRet;
	}
	if(strlen(EST_tmp->szSupportedAID10) <= 0)
	{
		iRet = 10;
		strcpy(EST_tmp->szSupportedAID10, AID);
		return iRet;
	}

	return iRet;
}

int addEST_CAPK(char *CAPK, int index, EST_REC *EST_tmp)
{
	int iRet = 0;

	if(EST_tmp->inPublicKeyIndex1 <= 0)
	{
		iRet = 1;
		strcpy(EST_tmp->szCAPKFile1, CAPK);
		EST_tmp->inPublicKeyIndex1 = index;
		return iRet;
	}
	if(EST_tmp->inPublicKeyIndex2 <= 0)
	{
		iRet = 2;
		strcpy(EST_tmp->szCAPKFile2, CAPK);
		EST_tmp->inPublicKeyIndex2 = index;
		return iRet;
	}
	if(EST_tmp->inPublicKeyIndex3 <= 0)
	{
		iRet = 3;
		strcpy(EST_tmp->szCAPKFile3, CAPK);
		EST_tmp->inPublicKeyIndex3 = index;
		return iRet;
	}
	if(EST_tmp->inPublicKeyIndex4 <= 0)
	{
		iRet = 4;
		strcpy(EST_tmp->szCAPKFile4, CAPK);
		EST_tmp->inPublicKeyIndex4 = index;
		return iRet;
	}
	if(EST_tmp->inPublicKeyIndex5 <= 0)
	{
		iRet = 5;
		strcpy(EST_tmp->szCAPKFile5, CAPK);
		EST_tmp->inPublicKeyIndex5 = index;
		return iRet;
	}
	if(EST_tmp->inPublicKeyIndex6 <= 0)
	{
		iRet = 6;
		strcpy(EST_tmp->szCAPKFile6, CAPK);
		EST_tmp->inPublicKeyIndex6 = index;
		return iRet;
	}
	if(EST_tmp->inPublicKeyIndex7 <= 0)
	{
		iRet = 7;
		strcpy(EST_tmp->szCAPKFile7, CAPK);
		EST_tmp->inPublicKeyIndex7 = index;
		return iRet;
	}
	if(EST_tmp->inPublicKeyIndex8 <= 0)
	{
		iRet = 8;
		strcpy(EST_tmp->szCAPKFile8, CAPK);
		EST_tmp->inPublicKeyIndex8 = index;
		return iRet;
	}
	if(EST_tmp->inPublicKeyIndex9 <= 0)
	{
		iRet = 9;
		strcpy(EST_tmp->szCAPKFile9, CAPK);
		EST_tmp->inPublicKeyIndex9 = index;
		return iRet;
	}
	if(EST_tmp->inPublicKeyIndex10 <= 0)
	{
		iRet = 10;
		strcpy(EST_tmp->szCAPKFile10, CAPK);
		EST_tmp->inPublicKeyIndex10 = index;
		return iRet;
	}
	if(EST_tmp->inPublicKeyIndex11 <= 0)
	{
		iRet = 11;
		strcpy(EST_tmp->szCAPKFile11, CAPK);
		EST_tmp->inPublicKeyIndex11 = index;
		return iRet;
	}
	if(EST_tmp->inPublicKeyIndex12 <= 0)
	{
		iRet = 12;
		strcpy(EST_tmp->szCAPKFile12, CAPK);
		EST_tmp->inPublicKeyIndex12 = index;
		return iRet;
	}
	if(EST_tmp->inPublicKeyIndex13 <= 0)
	{
		iRet = 13;
		strcpy(EST_tmp->szCAPKFile13, CAPK);
		EST_tmp->inPublicKeyIndex13 = index;
		return iRet;
	}
	if(EST_tmp->inPublicKeyIndex14 <= 0)
	{
		iRet = 14;
		strcpy(EST_tmp->szCAPKFile14, CAPK);
		EST_tmp->inPublicKeyIndex14 = index;
		return iRet;
	}
	if(EST_tmp->inPublicKeyIndex15 <= 0)
	{
		iRet = 15;
		strcpy(EST_tmp->szCAPKFile15, CAPK);
		EST_tmp->inPublicKeyIndex15 = index;
		return iRet;
	}

	return iRet;

}


//checking if correct AID related param is requested
int checkAIDparams(char *key, char *value, EST_REC *EST_tmp, int index)
{
	int iRet = index;
	long lTag;
	long lVal;
	char value_bin[256];
	short *inPartialNameAllowedFlag = NULL;
	char *szTermAVN = NULL;
	char *szSecondTermAVN = NULL;
	char *szRecommenedAppNameAID = NULL;
		
	lTag = strtol(key,NULL,16);
	dlog_msg("Updating %s:%X with %s - index: %d", key, lTag, value, index);
	//selecting correct params set
	switch(index)
	{
		case 1:
			inPartialNameAllowedFlag = &EST_tmp->inPartialNameAllowedFlag1;
			szTermAVN = EST_tmp->szTermAVN1;
			szSecondTermAVN = EST_tmp->szSecondTermAVN1;
			szRecommenedAppNameAID = EST_tmp->szRecommenedAppNameAID1;
			break;
		case 2:
			inPartialNameAllowedFlag = &EST_tmp->inPartialNameAllowedFlag2;
			szTermAVN = EST_tmp->szTermAVN2;
			szSecondTermAVN = EST_tmp->szSecondTermAVN2;
			szRecommenedAppNameAID = EST_tmp->szRecommenedAppNameAID2;
			break;
		case 3:
			inPartialNameAllowedFlag = &EST_tmp->inPartialNameAllowedFlag3;
			szTermAVN = EST_tmp->szTermAVN3;
			szSecondTermAVN = EST_tmp->szSecondTermAVN3;
			szRecommenedAppNameAID = EST_tmp->szRecommenedAppNameAID3;
			break;
		case 4:
			inPartialNameAllowedFlag = &EST_tmp->inPartialNameAllowedFlag4;
			szTermAVN = EST_tmp->szTermAVN4;
			szSecondTermAVN = EST_tmp->szSecondTermAVN4;
			szRecommenedAppNameAID = EST_tmp->szRecommenedAppNameAID4;
			break;
		case 5:
			inPartialNameAllowedFlag = &EST_tmp->inPartialNameAllowedFlag5;
			szTermAVN = EST_tmp->szTermAVN5;
			szSecondTermAVN = EST_tmp->szSecondTermAVN5;
			szRecommenedAppNameAID = EST_tmp->szRecommenedAppNameAID5;
			break;
		case 6:
			inPartialNameAllowedFlag = &EST_tmp->inPartialNameAllowedFlag6;
			szTermAVN = EST_tmp->szTermAVN6;
			szSecondTermAVN = EST_tmp->szSecondTermAVN6;
			szRecommenedAppNameAID = EST_tmp->szRecommenedAppNameAID6;
			break;
		case 7:
			inPartialNameAllowedFlag = &EST_tmp->inPartialNameAllowedFlag7;
			szTermAVN = EST_tmp->szTermAVN7;
			szSecondTermAVN = EST_tmp->szSecondTermAVN7;
			szRecommenedAppNameAID = EST_tmp->szRecommenedAppNameAID7;
			break;
		case 8:
			inPartialNameAllowedFlag = &EST_tmp->inPartialNameAllowedFlag8;
			szTermAVN = EST_tmp->szTermAVN8;
			szSecondTermAVN = EST_tmp->szSecondTermAVN8;
			szRecommenedAppNameAID = EST_tmp->szRecommenedAppNameAID8;
			break;
		case 9:
			inPartialNameAllowedFlag = &EST_tmp->inPartialNameAllowedFlag9;
			szTermAVN = EST_tmp->szTermAVN9;
			szSecondTermAVN = EST_tmp->szSecondTermAVN9;
			szRecommenedAppNameAID = EST_tmp->szRecommenedAppNameAID9;
			break;
		case 10:
			inPartialNameAllowedFlag = &EST_tmp->inPartialNameAllowedFlag10;
			szTermAVN = EST_tmp->szTermAVN10;
			szSecondTermAVN = EST_tmp->szSecondTermAVN10;
			szRecommenedAppNameAID = EST_tmp->szRecommenedAppNameAID10;
			break;
		case 0:
			break;
		default:
			return -1;
			

	}
	switch(lTag)
	{
		case TAG_DFAF11_PARTIAL_SELECTION:
			if(index && inPartialNameAllowedFlag != NULL)
			{
				lVal = strtol(value,NULL,10);
				*inPartialNameAllowedFlag = (int)(lVal-1); // Return 0-1
			}
			break;
		case TAG_9F09_TERM_VER_NUM:
			if(index && szTermAVN != NULL)
				strcpy(szTermAVN, value);
			break;
		case TAG_DFAF12_SECOND_AVN:
			if(index && szSecondTermAVN != NULL)
				strcpy(szSecondTermAVN, value);
			break;
		case TAG_DFAF13_REC_APP_NAME:
			if(index && szRecommenedAppNameAID != NULL)
				strcpy(szRecommenedAppNameAID, value);
			break;
		default:
			iRet = -1;
			break;

	}
	return iRet;

}

int updateESTparameter(char *key, char *value, EST_REC *EST_tmp)
{
	int iRet = 1;
	long lTag;
	long lVal;
	char value_bin[256];
	
	lTag = strtol(key,NULL,16);
	dlog_msg("Updating %s:%X with %s", key, lTag, value);
	switch(lTag)
	{
		/*
		case TAG_DFAF14_RND_SEL_THRESHOLD:
			lVal = strtol(value,NULL,10);
			MVT_tmp->lnEMVRSThreshold = lVal;
			break;
		case TAG_DFAF15_RND_SEL_PERCENTAGE:
			lVal = strtol(value,NULL,10);
			MVT_tmp->inEMVTargetRSPercent = (int)lVal;
			break;
		case TAG_DFAF16_RND_MAX_RND_PERCENTAGE:
			lVal = strtol(value,NULL,10);
			MVT_tmp->inEMVMaxTargetRSPercent = (int)lVal;
			break;
		case TAG_DFAF17_TAC_DEFAULT:
			SVC_DSP_2_HEX ((const char *)value, reinterpret_cast<char *>(MVT_tmp->szEMVTACDefault), TVR_SIZE);
			break;
		case TAG_DFAF18_TAC_DENIAL:
			SVC_DSP_2_HEX ((const char *)value, reinterpret_cast<char *>(MVT_tmp->szEMVTACDenial), TVR_SIZE);
			break;
		case TAG_DFAF19_TAC_ONLINE:
			SVC_DSP_2_HEX ((const char *)value, reinterpret_cast<char *>(MVT_tmp->szEMVTACOnline), TVR_SIZE);
			break;
		case TAG_DFAF1A_DEFAULT_TDOL:
			SVC_DSP_2_HEX ((const char *)value, reinterpret_cast<char *>(MVT_tmp->szDefaultTDOL), strlen(value)/2);
			break;
		case TAG_DFAF1B_DEFAULT_DDOL:
			SVC_DSP_2_HEX ((const char *)value, reinterpret_cast<char *>(MVT_tmp->szDefaultDDOL), strlen(value)/2);
			break;
		case TAG_DFAF1E_MCC:
			SVC_DSP_2_HEX ((const char *)value, reinterpret_cast<char *>(MVT_tmp->szEMVMerchantCategoryCode), strlen(value)/2);
			break;
		case TAG_9F1A_TERM_COUNTY_CODE:
			SVC_DSP_2_HEX ((const char *)value, reinterpret_cast<char *>(MVT_tmp->szEMVTermCountryCode), strlen(value)/2);
			break;
		case TAG_5F2A_TRANS_CURCY_CODE:
			SVC_DSP_2_HEX ((const char *)value, reinterpret_cast<char *>(MVT_tmp->szEMVTermCurrencyCode), strlen(value)/2);
			break;
		case TAG_5F36_TRANS_CURR_EXP:
			lVal = strtol(value,NULL,10);
			MVT_tmp->inEMVTermCurExp = (int)lVal;
			break;
		case TAG_9F40_TERM_ADTNAL_CAP:
			SVC_DSP_2_HEX ((const char *)value, reinterpret_cast<char *>(MVT_tmp->szEMVTermAddCapabilities), strlen(value)/2);
			break;
		case TAG_9F33_TERM_CAP:
			SVC_DSP_2_HEX ((const char *)value, reinterpret_cast<char *>(MVT_tmp->szEMVTermCapabilities), strlen(value)/2);
			break;
		case TAG_9F35_TERM_TYPE:
			SVC_DSP_2_HEX ((const char *)value, reinterpret_cast<char *>(MVT_tmp->szEMVTermType), strlen(value)/2);
			break;
		default:
			dlog_msg("Object not recognised %X ignoring...", lTag);
			break;
			
	*/
		}
	return iRet;

}

int updateMVTparameter(char *key, char *value, MVT_REC *MVT_tmp)
{
	int iRet = 1;
	long lTag;
	long lVal;
	char value_bin[256];

	lTag = strtol(key,NULL,16);
	dlog_msg("Updating %s:%X with %s", key, lTag, value);
	switch(lTag)
	{
		case TAG_9F1B_TERM_FLOOR_LIMIT:
			lVal = strtol(value,NULL,10);
			MVT_tmp->lnEMVFloorLimit = lVal;
			break;
		case TAG_DFAF14_RND_SEL_THRESHOLD:
			lVal = strtol(value,NULL,10);
			MVT_tmp->lnEMVRSThreshold = lVal;
			break;
		case TAG_DFAF15_RND_SEL_PERCENTAGE:
			lVal = strtol(value,NULL,10);
			MVT_tmp->inEMVTargetRSPercent = (int)lVal;
			break;
		case TAG_DFAF16_RND_MAX_RND_PERCENTAGE:
			lVal = strtol(value,NULL,10);
			MVT_tmp->inEMVMaxTargetRSPercent = (int)lVal;
			break;
		case TAG_DFAF17_TAC_DEFAULT:
		case 0xDFDF06:
			SVC_DSP_2_HEX ((const char *)value, reinterpret_cast<char *>(MVT_tmp->szEMVTACDefault), TVR_SIZE);
			break;
		case TAG_DFAF18_TAC_DENIAL:
		case 0xDFDF07:
			SVC_DSP_2_HEX ((const char *)value, reinterpret_cast<char *>(MVT_tmp->szEMVTACDenial), TVR_SIZE);
			break;
		case TAG_DFAF19_TAC_ONLINE:
		case 0xDFDF08:
			SVC_DSP_2_HEX ((const char *)value, reinterpret_cast<char *>(MVT_tmp->szEMVTACOnline), TVR_SIZE);
			break;
		case TAG_DFAF1A_DEFAULT_TDOL:
			SVC_DSP_2_HEX ((const char *)value, reinterpret_cast<char *>(MVT_tmp->szDefaultTDOL), strlen(value)/2);
			break;
		case TAG_DFAF1B_DEFAULT_DDOL:
			SVC_DSP_2_HEX ((const char *)value, reinterpret_cast<char *>(MVT_tmp->szDefaultDDOL), strlen(value)/2);
			break;
		case TAG_DFAF1E_MCC:
			SVC_DSP_2_HEX ((const char *)value, reinterpret_cast<char *>(MVT_tmp->szEMVMerchantCategoryCode), strlen(value)/2);
			break;
		case TAG_9F1A_TERM_COUNTY_CODE:
			SVC_DSP_2_HEX ((const char *)value, reinterpret_cast<char *>(MVT_tmp->szEMVTermCountryCode), strlen(value)/2);
			break;
		case TAG_5F2A_TRANS_CURCY_CODE:
			SVC_DSP_2_HEX ((const char *)value, reinterpret_cast<char *>(MVT_tmp->szEMVTermCurrencyCode), strlen(value)/2);
			break;
		case TAG_5F36_TRANS_CURR_EXP:
 			lVal = strtol(value,NULL,10);
			MVT_tmp->inEMVTermCurExp = (int)lVal;
			break;
		case TAG_9F40_TERM_ADTNAL_CAP:
			SVC_DSP_2_HEX ((const char *)value, reinterpret_cast<char *>(MVT_tmp->szEMVTermAddCapabilities), strlen(value)/2);
			break;
		case TAG_9F33_TERM_CAP:
			SVC_DSP_2_HEX ((const char *)value, reinterpret_cast<char *>(MVT_tmp->szEMVTermCapabilities), strlen(value)/2);
			break;
		case TAG_9F35_TERM_TYPE:
			SVC_DSP_2_HEX ((const char *)value, reinterpret_cast<char *>(MVT_tmp->szEMVTermType), strlen(value)/2);
			break;
		case 0xDFDF0C: // ROS Max Target
			lVal = strtol(value,NULL,10);
			MVT_tmp->inEMVMaxTargetRSPercent = lVal;
			break;
		case 0xDFDF0B: // Target RS Percent
			lVal = strtol(value,NULL,10);
			MVT_tmp->inEMVTargetRSPercent = lVal;
			break;
		case 0xDFDF0A: // ROS Threshold
			lVal = strtol(value,NULL,10);
			MVT_tmp->lnEMVRSThreshold = lVal;
			break;

		default:
			dlog_msg("object not recognised %X ignoring...", lTag);
			break;
			

	}

	return iRet;
}


int genConfig(char *configFilename)
{
	const int RID_SIZE = (EMV_RID_SIZE-1)/2;
	char section[40], key[40], value[256];
	int i, s, k;
	char RID_current[MAX_AID_LEN*2+1];
	bool add_EST = false;
	bool add_MVT = false;
	char emv_config_global[20];

	std::vector<EST_REC> EST_file;
	std::vector<MVT_REC> MVT_file;
	EST_REC EST_tmp;
	MVT_REC MVT_tmp;
	int EST_index = -1;
	int MVT_index = -1;
	int EST_size = 0;

	i=0;
	while (EMV_CONFIG_GLOBAL[i])
	{
		emv_config_global[i] = toupper(EMV_CONFIG_GLOBAL[i]);
		i++;
	}
	emv_config_global[i] = 0;

	dlog_msg("genConfig config from file: %s", configFilename);
	
	for (s = 0; ini_getsection(s, section, sizeof section, configFilename) > 0; s++) 
	{
		i=0;
		while (section[i])
		{
			section[i] = toupper(section[i]);
			i++;
		}
		dlog_msg("[%s]", section);
		memcpy(RID_current, section, RID_SIZE*2);
		RID_current[RID_SIZE*2] = 0;
		//checking if section is already present in EST_file
		if(memcmp(emv_config_global, section, strlen(emv_config_global)) == 0)
		{
			//parameters for default MVT record
			add_EST = false;	//we should not create new EST for this set
			dlog_msg("Default configuration section found: %s", emv_config_global);
			if(MVT_file.size() > 0)
			{
				MVT_index = 0;
				dlog_msg("using default MVT record");
			}
			else
			{
				MVT_file.resize(MVT_file.size() + 1);
				MVT_index = 0;
				dlog_msg("default MVT record created");
			}
		}
		else 
		{
			if((EST_size = EST_file.size()) > 0)
			{
				add_EST = false;
				EST_index = 0;
				for (std::vector<EST_REC>::iterator it = EST_file.begin(); it != EST_file.end(); ++it)
				{
					if(memcmp(it->szRID, section, strlen(it->szRID)) == 0)
					{
						//record with the same RID/AID already present
						//if there are more params following it means new record must be created for specific AID
						//more params can be added to RID sections, but AID always creates new record
						dlog_msg("PROCESSING: %s...", section);
						EST_tmp = *it;
						if(strlen(section) > RID_SIZE)
						{
							//we got new AID
							for (k = 0; ini_getkey(section, k, key, sizeof key, configFilename) > 0; k++)
							{
								//more params follws
								//checking if following params are AID settings
								if(checkAIDparams(key, value, &EST_tmp, 0)== -1)
								{
									dlog_msg("Creating AID specific EST record - %s", section);
									add_EST = true;
								}
							}
							if(!add_EST)
							{
								int AID_ndx = addEST_AID(section, &EST_tmp);
								dlog_msg("Adding AID %s (pos: %d) to record for RID %s", section, AID_ndx, EST_tmp.szRID);
								for (k = 0; ini_getkey(section, k, key, sizeof key, configFilename) > 0; k++)
								{
									if(checkAIDparams(key, value, &EST_tmp, AID_ndx))
									{
										dlog_msg("AID %s added to RID: %s on position %d", section, EST_tmp.szRID, AID_ndx);
									}
								}

							}
						}
						else
						{
							//more parameters for existing RID...
							dlog_msg("Record for %s already exist, updating...", section);
							add_EST = false;
							if(it->inEMVTableRecord > 0)
							{
								MVT_index = it->inEMVTableRecord;

								if(it->inEMVTableRecord < MVT_file.size())
								{
									dlog_msg("corresponding MVT record: %d", MVT_index);
								}
								else
								{
									dlog_msg("ERROR! - invalid MVT reference! %d", MVT_index);
									return -1;
								}
							}
							else
							{
								//creating corresponding MVT record
								MVT_file.resize(MVT_file.size() + 1);
								MVT_index = MVT_file.size();
								it->inEMVTableRecord = MVT_index;
								dlog_msg("Corresponding MVT record for RID %s is %d", EST_tmp.szRID, MVT_index);
							}
						}
						break;
					}
					EST_index++;
				}
			}
			else
			{
				dlog_msg("no records in EST file - adding");
				add_EST = true;	
				//creating corresponding MVT record
				MVT_file.resize(MVT_file.size() + 1);
				MVT_index = MVT_file.size();
				dlog_msg("Corresponding MVT record for RID %s is %d", EST_tmp.szRID, MVT_index);
			}
		}

		if(add_EST)
		{
			//adding new RID
			dlog_msg("Adding new EST record for RID: %s", section);
			EST_file.resize(EST_size+1);
			EST_tmp = EST_file[EST_file.size()-1];
			EST_index = EST_file.size()-1;
			strcpy(EST_tmp.szRID, section);
			EST_tmp.inEMVTableRecord = MVT_index;
		}

		//current record is pointed by EST_current and MVT_current, we can add data
		//building MVT and EST records
		MVT_tmp = MVT_file[MVT_index];
		for (k = 0; ini_getkey(section, k, key, sizeof key, configFilename) > 0; k++)
		{
			if(ini_gets(section, key, "0", value, sizeof(value), configFilename) > 0)
			{
				dlog_msg("[%s]:%s=%s", section, key, value);
				updateMVTparameter(key, value, &MVT_tmp);
			}
		}
		if(MVT_index >= 0)
			MVT_file[MVT_index] = MVT_tmp;
		if(EST_index >= 0)
			EST_file[EST_index] = EST_tmp;
	}

	dlog_msg("EST prepared: %d records", EST_file.size());
	if (EST_file.size())
	{
		const char EST_TEMP[] = "EST.DAT.TMP";
		const int EST_HEADER_SIZE = 16;
		int EST_copy_h = open(EST_TEMP, O_CREAT | O_TRUNC | O_WRONLY);
		int EST_h = open(EMV_CONFIG_EST, O_RDWR);
		int written_bytes = 0;
		if (EST_h >= 0)
		{
			int read_bytes = read(EST_h, value, EST_HEADER_SIZE);
			written_bytes = write(EST_copy_h, value, EST_HEADER_SIZE);
			close(EST_h);
		}
		else
		{
			memset(value, 0, EST_HEADER_SIZE);
			written_bytes = write(EST_copy_h, value, EST_HEADER_SIZE);
		}

		for (std::vector<EST_REC>::iterator it = EST_file.begin(); it != EST_file.end(); ++it)
		{
			dlog_msg("RID: %s", it->szRID);
			EST_REC estrect = *it;
			written_bytes = write(EST_copy_h, reinterpret_cast<char *>(&(*it)), sizeof(EST_REC));
			if (written_bytes != sizeof(EST_REC)) dlog_alert("Write error: wrote %d, should write %d", written_bytes, sizeof(EST_REC));
		}
		close(EST_copy_h);
		_remove(EMV_CONFIG_EST);
		_rename(EST_TEMP, EMV_CONFIG_EST);
	}
	dlog_msg("MVT prepared: %d records", MVT_file.size());
	if (MVT_file.size())
	{
		const char MVT_TEMP[] = "MVT.DAT.TMP";
		const int MVT_HEADER_SIZE = 16;
		int MVT_h = open(EMV_CONFIG_MVT, O_RDWR);
		int MVT_copy_h = open(MVT_TEMP, O_CREAT | O_TRUNC | O_WRONLY);
		int written_bytes = 0;
		if (MVT_h >= 0)
		{
			int read_bytes = read(MVT_h, value, MVT_HEADER_SIZE);
			written_bytes = write(MVT_copy_h, value, MVT_HEADER_SIZE);
			close(MVT_h);
		}
		for (std::vector<MVT_REC>::iterator it = MVT_file.begin(); it != MVT_file.end(); ++it)
		{
			written_bytes = write(MVT_copy_h, reinterpret_cast<char *>(&(*it)), sizeof(MVT_REC));
			if (written_bytes != sizeof(MVT_REC)) dlog_alert("Write error: wrote %d, should write %d", written_bytes, sizeof(MVT_REC));
		}
		close(MVT_copy_h);
		_remove(EMV_CONFIG_MVT);
		_rename(MVT_TEMP, EMV_CONFIG_MVT);
	}

	return EST_file.size() && MVT_file.size();
}

int addMVT(char *section, MVT_REC *MVT_data)
{
	int iRet = 0;
	char Key[40];
	char Data[40];

	sprintf(Key, "%X", TAG_9F1B_TERM_FLOOR_LIMIT);
	sprintf(Data, "%ld", MVT_data->lnEMVFloorLimit);
	dlog_msg("Adding floor limit: %s=%s", Key, Data);
	iRet = ini_puts(section, Key, Data, EMV_CONFIG_FILE);
	

	sprintf(Key, "%X", TAG_DFAF14_RND_SEL_THRESHOLD);
	sprintf(Data, "%ld", MVT_data->lnEMVRSThreshold);
	dlog_msg("Adding RSThreshold: %s=%s", Key, Data);
	iRet = ini_puts(section, Key, Data, EMV_CONFIG_FILE);

	sprintf(Key, "%X", TAG_DFAF15_RND_SEL_PERCENTAGE);
	sprintf(Data, "%d", MVT_data->inEMVTargetRSPercent);
	dlog_msg("Adding Target Percent: %s=%s", Key, Data);
	iRet = ini_puts(section, Key, Data, EMV_CONFIG_FILE);

	sprintf(Key, "%X", TAG_DFAF16_RND_MAX_RND_PERCENTAGE);
	sprintf(Data, "%d", MVT_data->inEMVMaxTargetRSPercent);
	dlog_msg("Adding Max Target Percent: %s=%s", Key, Data);
	iRet = ini_puts(section, Key, Data, EMV_CONFIG_FILE);

	sprintf(Key, "%X", TAG_DFAF17_TAC_DEFAULT);
	dlog_msg("Adding TAC default: %s=%s", Key, MVT_data->szEMVTACDefault);
	iRet = ini_puts(section, Key, MVT_data->szEMVTACDefault, EMV_CONFIG_FILE);

	sprintf(Key, "%X", TAG_DFAF18_TAC_DENIAL);
	dlog_msg("Adding TAC denial: %s=%s", Key, MVT_data->szEMVTACDenial);
	iRet = ini_puts(section, Key, MVT_data->szEMVTACDenial, EMV_CONFIG_FILE);

	sprintf(Key, "%X", TAG_DFAF19_TAC_ONLINE);
	dlog_msg("Adding TAC online: %s=%s", Key, MVT_data->szEMVTACOnline);
	iRet = ini_puts(section, Key, MVT_data->szEMVTACOnline, EMV_CONFIG_FILE);

	sprintf(Key, "%X", TAG_DFAF1A_DEFAULT_TDOL);
	dlog_msg("Adding default TDOL: %s=%s", Key, MVT_data->szDefaultTDOL);
	iRet = ini_puts(section, Key, MVT_data->szDefaultTDOL, EMV_CONFIG_FILE);

	sprintf(Key, "%X", TAG_DFAF1B_DEFAULT_DDOL);
	dlog_msg("Adding default DDOL: %s=%s", Key, MVT_data->szDefaultDDOL);
	iRet = ini_puts(section, Key, MVT_data->szDefaultDDOL, EMV_CONFIG_FILE);

	sprintf(Key, "%X", TAG_9F1A_TERM_COUNTY_CODE);
	dlog_msg("Adding Term Country Code: %s=%s", Key, MVT_data->szEMVTermCountryCode);
	iRet = ini_puts(section, Key, MVT_data->szEMVTermCountryCode, EMV_CONFIG_FILE);

	sprintf(Key, "%X", TAG_5F2A_TRANS_CURCY_CODE);
	dlog_msg("Adding Trans Currency Code: %s=%s", Key, MVT_data->szEMVTermCurrencyCode);
	iRet = ini_puts(section, Key, MVT_data->szEMVTermCurrencyCode, EMV_CONFIG_FILE);

	sprintf(Key, "%X", TAG_5F36_TRANS_CURR_EXP);
	sprintf(Data, "%d", MVT_data->inEMVTermCurExp);
	dlog_msg("Adding Currency Exp: %s=%s", Key, Data);
	iRet = ini_puts(section, Key, Data, EMV_CONFIG_FILE);
	
	sprintf(Key, "%X", TAG_9F33_TERM_CAP);
	dlog_msg("Adding Terminal Capability: %s=%s", Key, MVT_data->szEMVTermCapabilities);
	iRet = ini_puts(section, Key, MVT_data->szEMVTermCapabilities, EMV_CONFIG_FILE);

	sprintf(Key, "%X", TAG_9F40_TERM_ADTNAL_CAP);
	dlog_msg("Adding Additional Terminal Capabilities: %s=%s", Key, MVT_data->szEMVTermAddCapabilities);
	iRet = ini_puts(section, Key, MVT_data->szEMVTermAddCapabilities, EMV_CONFIG_FILE);

	sprintf(Key, "%X", TAG_9F35_TERM_TYPE);
	dlog_msg("Adding Terminal Type: %s=%s", Key, MVT_data->szEMVTermType);
	iRet = ini_puts(section, Key, MVT_data->szEMVTermType, EMV_CONFIG_FILE);

	sprintf(Key, "%X", TAG_9F15_MER_CAT_CODE);
	dlog_msg("Adding Merchant Cat Code: %s=%s", Key, MVT_data->szEMVMerchantCategoryCode);
	iRet = ini_puts(section, Key, MVT_data->szEMVMerchantCategoryCode, EMV_CONFIG_FILE);

	sprintf(Key, "%X", TAG_DFAF1E_MCC);
	dlog_msg("Adding Terminal Cat Code: %s=%s", Key, MVT_data->szEMVTerminalCategoryCode);
	iRet = ini_puts(section, Key, MVT_data->szEMVTerminalCategoryCode, EMV_CONFIG_FILE);

	return iRet;
}

//this function imports data from EST.dat MVT.dat
int importConfig(char *configFilename)
{
	int MVT_h = open(EMV_CONFIG_MVT, O_RDONLY);
	int EST_h = open(EMV_CONFIG_EST, O_RDONLY);

	const int EST_HEADER_SIZE = 16;
	const int MVT_HEADER_SIZE = 16;

	EST_REC *EST_tmp;
	MVT_REC *MVT_tmp;

	int read_bytes = 0;
	int total_read = 0;

	char *EST_buf;
	char *MVT_buf;

	int iRet = 1;
	
	char Key[40];
	char Data[40];
	
	EST_buf = new char[sizeof(EST_REC)];
	MVT_buf = new char[sizeof(MVT_REC)];

	read_bytes = read(MVT_h, MVT_buf, MVT_HEADER_SIZE);
	read_bytes = read(MVT_h, MVT_buf, sizeof(MVT_REC));
	MVT_tmp = (MVT_REC*)MVT_buf;

	addMVT("global", MVT_tmp);
		
	//first processing EST file
	if(MVT_h && EST_buf)
	{
		dlog_msg("Processing EST file");
		read_bytes = read(EST_h, EST_buf, EST_HEADER_SIZE);
		read_bytes = read(EST_h, EST_buf, sizeof(EST_REC));
		while(read_bytes)
		{
			dlog_msg("Processing %d/%d bytes", read_bytes, sizeof(EST_REC));
			EST_tmp = (EST_REC*)EST_buf;
			dlog_msg("RID: %s, label: %s", EST_tmp->szRID, EST_tmp->szSchemeLabel);
			iRet = ini_puts(EST_tmp->szRID, "00", "00", EMV_CONFIG_FILE);
			//adding CAPK here
			
			if(strlen(EST_tmp->szSupportedAID1))
			{
				dlog_msg("Adding section for AID: %s", EST_tmp->szSupportedAID1);
				sprintf(Key, "%X", TAG_DFAF11_PARTIAL_SELECTION);
				sprintf(Data, "%d", EST_tmp->inPartialNameAllowedFlag1);
				iRet = ini_puts(EST_tmp->szSupportedAID1, Key, Data, EMV_CONFIG_FILE);
				sprintf(Key, "%X", TAG_9F09_TERM_VER_NUM);
				iRet = ini_puts(EST_tmp->szSupportedAID1, Key, EST_tmp->szTermAVN1, EMV_CONFIG_FILE);
				sprintf(Key, "%X", TAG_DFAF12_SECOND_AVN);
				iRet = ini_puts(EST_tmp->szSupportedAID1, Key, EST_tmp->szSecondTermAVN1, EMV_CONFIG_FILE);
				sprintf(Key, "%X", TAG_DFAF13_REC_APP_NAME);
				iRet = ini_puts(EST_tmp->szSupportedAID1, Key, EST_tmp->szRecommenedAppNameAID1, EMV_CONFIG_FILE);

			}
			if(strlen(EST_tmp->szSupportedAID2))
			{
				dlog_msg("Adding section for AID: %s", EST_tmp->szSupportedAID2);
				sprintf(Key, "%X", TAG_DFAF11_PARTIAL_SELECTION);
				sprintf(Data, "%d", EST_tmp->inPartialNameAllowedFlag2);
				iRet = ini_puts(EST_tmp->szSupportedAID2, Key, Data, EMV_CONFIG_FILE);
				sprintf(Key, "%X", TAG_9F09_TERM_VER_NUM);
				iRet = ini_puts(EST_tmp->szSupportedAID2, Key, EST_tmp->szTermAVN2, EMV_CONFIG_FILE);
				sprintf(Key, "%X", TAG_DFAF12_SECOND_AVN);
				iRet = ini_puts(EST_tmp->szSupportedAID2, Key, EST_tmp->szSecondTermAVN2, EMV_CONFIG_FILE);
				sprintf(Key, "%X", TAG_DFAF13_REC_APP_NAME);
				iRet = ini_puts(EST_tmp->szSupportedAID2, Key, EST_tmp->szRecommenedAppNameAID2, EMV_CONFIG_FILE);

			}
			if(strlen(EST_tmp->szSupportedAID3))
			{
				dlog_msg("Adding section for AID: %s", EST_tmp->szSupportedAID3);
				sprintf(Key, "%X", TAG_DFAF11_PARTIAL_SELECTION);
				sprintf(Data, "%d", EST_tmp->inPartialNameAllowedFlag3);
				iRet = ini_puts(EST_tmp->szSupportedAID3, Key, Data, EMV_CONFIG_FILE);
				sprintf(Key, "%X", TAG_9F09_TERM_VER_NUM);
				iRet = ini_puts(EST_tmp->szSupportedAID3, Key, EST_tmp->szTermAVN3, EMV_CONFIG_FILE);
				sprintf(Key, "%X", TAG_DFAF12_SECOND_AVN);
				iRet = ini_puts(EST_tmp->szSupportedAID3, Key, EST_tmp->szSecondTermAVN3, EMV_CONFIG_FILE);
				sprintf(Key, "%X", TAG_DFAF13_REC_APP_NAME);
				iRet = ini_puts(EST_tmp->szSupportedAID3, Key, EST_tmp->szRecommenedAppNameAID3, EMV_CONFIG_FILE);

			}
			if(strlen(EST_tmp->szSupportedAID4))
			{
				dlog_msg("Adding section for AID: %s", EST_tmp->szSupportedAID4);
				sprintf(Key, "%X", TAG_DFAF11_PARTIAL_SELECTION);
				sprintf(Data, "%d", EST_tmp->inPartialNameAllowedFlag4);
				iRet = ini_puts(EST_tmp->szSupportedAID4, Key, Data, EMV_CONFIG_FILE);
				sprintf(Key, "%X", TAG_9F09_TERM_VER_NUM);
				iRet = ini_puts(EST_tmp->szSupportedAID4, Key, EST_tmp->szTermAVN4, EMV_CONFIG_FILE);
				sprintf(Key, "%X", TAG_DFAF12_SECOND_AVN);
				iRet = ini_puts(EST_tmp->szSupportedAID4, Key, EST_tmp->szSecondTermAVN4, EMV_CONFIG_FILE);
				sprintf(Key, "%X", TAG_DFAF13_REC_APP_NAME);
				iRet = ini_puts(EST_tmp->szSupportedAID4, Key, EST_tmp->szRecommenedAppNameAID4, EMV_CONFIG_FILE);

			}
			if(strlen(EST_tmp->szSupportedAID5))
			{
				dlog_msg("Adding section for AID: %s", EST_tmp->szSupportedAID5);
				sprintf(Key, "%X", TAG_DFAF11_PARTIAL_SELECTION);
				sprintf(Data, "%d", EST_tmp->inPartialNameAllowedFlag5);
				iRet = ini_puts(EST_tmp->szSupportedAID5, Key, Data, EMV_CONFIG_FILE);
				sprintf(Key, "%X", TAG_9F09_TERM_VER_NUM);
				iRet = ini_puts(EST_tmp->szSupportedAID5, Key, EST_tmp->szTermAVN5, EMV_CONFIG_FILE);
				sprintf(Key, "%X", TAG_DFAF12_SECOND_AVN);
				iRet = ini_puts(EST_tmp->szSupportedAID5, Key, EST_tmp->szSecondTermAVN5, EMV_CONFIG_FILE);
				sprintf(Key, "%X", TAG_DFAF13_REC_APP_NAME);
				iRet = ini_puts(EST_tmp->szSupportedAID5, Key, EST_tmp->szRecommenedAppNameAID5, EMV_CONFIG_FILE);

			}
			if(strlen(EST_tmp->szSupportedAID6))
			{
				dlog_msg("Adding section for AID: %s", EST_tmp->szSupportedAID6);
				sprintf(Key, "%X", TAG_DFAF11_PARTIAL_SELECTION);
				sprintf(Data, "%d", EST_tmp->inPartialNameAllowedFlag6);
				iRet = ini_puts(EST_tmp->szSupportedAID6, Key, Data, EMV_CONFIG_FILE);
				sprintf(Key, "%X", TAG_9F09_TERM_VER_NUM);
				iRet = ini_puts(EST_tmp->szSupportedAID6, Key, EST_tmp->szTermAVN6, EMV_CONFIG_FILE);
				sprintf(Key, "%X", TAG_DFAF12_SECOND_AVN);
				iRet = ini_puts(EST_tmp->szSupportedAID6, Key, EST_tmp->szSecondTermAVN6, EMV_CONFIG_FILE);
				sprintf(Key, "%X", TAG_DFAF13_REC_APP_NAME);
				iRet = ini_puts(EST_tmp->szSupportedAID6, Key, EST_tmp->szRecommenedAppNameAID6, EMV_CONFIG_FILE);

			}
			if(strlen(EST_tmp->szSupportedAID7))
			{
				dlog_msg("Adding section for AID: %s", EST_tmp->szSupportedAID7);
				sprintf(Key, "%X", TAG_DFAF11_PARTIAL_SELECTION);
				sprintf(Data, "%d", EST_tmp->inPartialNameAllowedFlag7);
				iRet = ini_puts(EST_tmp->szSupportedAID7, Key, Data, EMV_CONFIG_FILE);
				sprintf(Key, "%X", TAG_9F09_TERM_VER_NUM);
				iRet = ini_puts(EST_tmp->szSupportedAID7, Key, EST_tmp->szTermAVN7, EMV_CONFIG_FILE);
				sprintf(Key, "%X", TAG_DFAF12_SECOND_AVN);
				iRet = ini_puts(EST_tmp->szSupportedAID7, Key, EST_tmp->szSecondTermAVN7, EMV_CONFIG_FILE);
				sprintf(Key, "%X", TAG_DFAF13_REC_APP_NAME);
				iRet = ini_puts(EST_tmp->szSupportedAID7, Key, EST_tmp->szRecommenedAppNameAID7, EMV_CONFIG_FILE);

			}
			if(strlen(EST_tmp->szSupportedAID8))
			{
				dlog_msg("Adding section for AID: %s", EST_tmp->szSupportedAID8);
				sprintf(Key, "%X", TAG_DFAF11_PARTIAL_SELECTION);
				sprintf(Data, "%d", EST_tmp->inPartialNameAllowedFlag8);
				iRet = ini_puts(EST_tmp->szSupportedAID8, Key, Data, EMV_CONFIG_FILE);
				sprintf(Key, "%X", TAG_9F09_TERM_VER_NUM);
				iRet = ini_puts(EST_tmp->szSupportedAID8, Key, EST_tmp->szTermAVN8, EMV_CONFIG_FILE);
				sprintf(Key, "%X", TAG_DFAF12_SECOND_AVN);
				iRet = ini_puts(EST_tmp->szSupportedAID8, Key, EST_tmp->szSecondTermAVN8, EMV_CONFIG_FILE);
				sprintf(Key, "%X", TAG_DFAF13_REC_APP_NAME);
				iRet = ini_puts(EST_tmp->szSupportedAID8, Key, EST_tmp->szRecommenedAppNameAID8, EMV_CONFIG_FILE);

			}
			if(strlen(EST_tmp->szSupportedAID9))
			{
				dlog_msg("Adding section for AID: %s", EST_tmp->szSupportedAID9);
				sprintf(Key, "%X", TAG_DFAF11_PARTIAL_SELECTION);
				sprintf(Data, "%d", EST_tmp->inPartialNameAllowedFlag9);
				iRet = ini_puts(EST_tmp->szSupportedAID9, Key, Data, EMV_CONFIG_FILE);
				sprintf(Key, "%X", TAG_9F09_TERM_VER_NUM);
				iRet = ini_puts(EST_tmp->szSupportedAID9, Key, EST_tmp->szTermAVN9, EMV_CONFIG_FILE);
				sprintf(Key, "%X", TAG_DFAF12_SECOND_AVN);
				iRet = ini_puts(EST_tmp->szSupportedAID9, Key, EST_tmp->szSecondTermAVN9, EMV_CONFIG_FILE);
				sprintf(Key, "%X", TAG_DFAF13_REC_APP_NAME);
				iRet = ini_puts(EST_tmp->szSupportedAID9, Key, EST_tmp->szRecommenedAppNameAID9, EMV_CONFIG_FILE);

			}
			if(strlen(EST_tmp->szSupportedAID10))
			{
				dlog_msg("Adding section for AID: %s", EST_tmp->szSupportedAID10);
				sprintf(Key, "%X", TAG_DFAF11_PARTIAL_SELECTION);
				sprintf(Data, "%d", EST_tmp->inPartialNameAllowedFlag10);
				iRet = ini_puts(EST_tmp->szSupportedAID10, Key, Data, EMV_CONFIG_FILE);
				sprintf(Key, "%X", TAG_9F09_TERM_VER_NUM);
				iRet = ini_puts(EST_tmp->szSupportedAID10, Key, EST_tmp->szTermAVN10, EMV_CONFIG_FILE);
				sprintf(Key, "%X", TAG_DFAF12_SECOND_AVN);
				iRet = ini_puts(EST_tmp->szSupportedAID10, Key, EST_tmp->szSecondTermAVN10, EMV_CONFIG_FILE);
				sprintf(Key, "%X", TAG_DFAF13_REC_APP_NAME);
				iRet = ini_puts(EST_tmp->szSupportedAID10, Key, EST_tmp->szRecommenedAppNameAID10, EMV_CONFIG_FILE);

			}
			
			read_bytes = read(EST_h, EST_buf, sizeof(EST_REC));
		}
		
		
	}
	if (EST_buf) delete [] EST_buf;
	if (MVT_buf) delete [] MVT_buf;
	close(MVT_h);
	close(EST_h);

	return iRet;

}

int cUpdateCfg::commitChanges(bool updateEST /* = true */)
{
	int iRet = 0;

	if (updateEST)
	{
		int EST_h = open(EMV_CONFIG_EST, O_WRONLY);
		if(EST_h >= 0)
		{
			dlog_msg("Writing into EST file...");
			lseek(EST_h, EST_offset, SEEK_SET);
			iRet = write(EST_h, (const char*)&EST_current, sizeof(EST_REC));
			dlog_msg("%d bytes written into EST file...", iRet);
			close(EST_h);
		}
	}

	int MVT_h = open(EMV_CONFIG_MVT, O_WRONLY);
	if(MVT_h >= 0)
	{
		if (!updateEST) setKernelChecksum(&MVT_current);
		dlog_msg("Writing into MVT file...");
		lseek(MVT_h, MVT_offset, SEEK_SET);
		iRet = write(MVT_h, (const char*)&MVT_current, sizeof(MVT_REC));
		dlog_msg("%d bytes written into MVT file...", iRet);
		close(MVT_h);
	}

	return iRet;

}


void cUpdateCfg::setKernelChecksum(MVT_REC * ptrMVT)
{
    // Kernel 6.2.0
    static const char * CKSUM_ALL_CVM = "OW1H4U3tnF00xA01Y"; // e0f8c8, tt 22
    static const char * CKSUM_NO_ONLINE_PIN = "OW1H4U2tnF00xA01Y"; //e0b8c8 , tt 22
    static const char * CKSUM_NO_ONLINE_PIN_NO_NO_CVM = "OW1H4U2lnF00xA01Y"; //e0b0c8, tt 22
    static const char * CKSUM_OFFLINE_ONLY = "OW1G4U2tnF00xA01Z"; //e0b8c8, tt 23
    static const char * CKSUM_UNATT_OFFLINE_ONLY = "OW1G4M2On600x501a"; //6098C8, tt 25
    static const char * CKSUM_UNATT_ONLINE = "OW1G4M3On600x501a"; //60D8C8, tt 25
    static const char * CKSUM_NO_NO_CVM = "OW1H4U3lnF00xA01Y"; // e0f0c8, tt 22
    static const char * CKSUM_SIG_ONLY = "OW1H4U0WnF00xA01Y"; //e020c8, tt 22
    static const char * CKSUM_NO_CVM_ONLY = "OW1H4U08nF00xA01Y"; // e008c8, tt 22
    static const char * CKSUM_NO_OFFLINE_PIN = "OW1H4U1dnF00xA01Y"; // e068c8, tt 22
    static const char * CKSUM_NO_NO_CVM_ONLINE_ONLY = "OW1H4U3lnF00xA01X"; //e0f0c8, tt 21
    static const char * CKSUM_INVALID = "INVALID_CHECKSUM"; // All other cases
    
    static const char * VX700 = "VX700";

    const char * const termCaps = ptrMVT->szEMVTermCapabilities;
    int termType = atoi(ptrMVT->szEMVTermType);
    const char * cksum = 0;
    dlog_msg("Terminal type %d, Capabilities '%s'", termType, termCaps);
    char model_number[12+1];
    SVC_INFO_MODELNO (model_number);
    model_number[12]=0;

    if (memcmp(model_number, VX700, strlen(VX700)) == 0)
    {
        dlog_msg("Unattended device!");
        if (termType == 25) // Unattended - Offline with online capability
        {
            if (!strcasecmp(termCaps, "6098c8"))
            {
                cksum = CKSUM_UNATT_OFFLINE_ONLY;
            }
            else if (!strcasecmp(termCaps, "60D8C8"))
            {
                cksum = CKSUM_UNATT_ONLINE;
            }
        }
    }
    else // Attended terminals
    {
        if (termType == 23) // Offline only (rarity!)
        {
            if (!strcasecmp(termCaps, "e0b8c8"))
            {
                cksum = CKSUM_OFFLINE_ONLY;
            }
        }
        else if (termType == 22) // Offline with online capability
        {
            if (!strcasecmp(termCaps, "e0f8c8"))
            {
                cksum = CKSUM_ALL_CVM;
            }
            else if (!strcasecmp(termCaps, "e0b8c8"))
            {
                cksum = CKSUM_NO_ONLINE_PIN;
            }
            else if (!strcasecmp(termCaps, "e0b0c8"))
            {
                cksum = CKSUM_NO_ONLINE_PIN_NO_NO_CVM;
            }
            else if (!strcasecmp(termCaps, "e0f0c8"))
            {
                cksum = CKSUM_NO_NO_CVM;
            }
            else if (!strcasecmp(termCaps, "e020c8"))
            {
                cksum = CKSUM_SIG_ONLY;
            }
            else if (!strcasecmp(termCaps, "e008c8"))
            {
                cksum = CKSUM_NO_CVM_ONLY;
            }
            else if (!strcasecmp(termCaps, "e068c8"))
            {
                cksum = CKSUM_NO_OFFLINE_PIN;
            }

        }
        else if (termType == 21) // Online only
        {
            if (!strcasecmp(termCaps, "e0f0c8"))
            {
                cksum = CKSUM_NO_NO_CVM_ONLINE_ONLY;
            }
        }
    }
    if (!cksum) cksum = CKSUM_INVALID;
    strncpy(ptrMVT->szRFU1, cksum, EMV_STRING_SIZE);
    dlog_msg("Kernel checksum set: '%s'", ptrMVT->szRFU1);
}

void cUpdateCfg::setDefaultMVT(MVT_REC *ptrMVT)
{
	memset(ptrMVT, 0, sizeof(MVT_REC));

	ptrMVT->inSchemeReference = -1;
	ptrMVT->inIssuerReference = -1;
	ptrMVT->inTRMDataPresent = 1;
	ptrMVT->inModifyCandListFlag = 1;
	ptrMVT->inNextRecord = -1;
	ptrMVT->inEMVTermCurExp = 2;
	ptrMVT->inBlackListedCardSupportFlag = 1;
//	ptrMVT->inMerchantForcedOnlineFlag = 1;
//	memcpy(ptrMVT->szEMVTermCapabilities, "E020E0", 6);
/*
	memcpy(ptrMVT->szEMVTermCapabilities, "E0B8C0", 6);
	memcpy(ptrMVT->szEMVTermAddCapabilities, "F000F0F001", 10); 
	memcpy(ptrMVT->szEMVTermType, "22", 2);
	memcpy(ptrMVT->szEMVTerminalCategoryCode, "R", 1);
	memcpy(ptrMVT->szEMVMerchantCategoryCode, "2701", 4);
	memcpy(ptrMVT->szEMVTermCurrencyCode, "0826", 4);
*/
}

void cUpdateCfg::setDefaultEST(EST_REC *ptrEST)
{
	memset(ptrEST, 0, sizeof(EST_REC));
	short *ptr = &(ptrEST->inPublicKeyIndex1);
	for (int i = 0; i < 15; i++)
	{
		*ptr = 256;
		ptr += (sizeof(short) + EMV_PK_FILE_NAME+1 + EMV_PK_EXPDATE_SIZE+1) >> 1;
	}

	ptr = &(ptrEST->inPartialNameAllowedFlag1);
	for (int i = 0; i < 10; i++)
	{
		*ptr = 2; // 2 means allow
		ptr += (sizeof(short) + EMV_AVN_SIZE+1 + EMV_AVN_SIZE+1 + MAX_APPNAME_LEN+1 + EMV_MAX_AID_SIZE+1) >> 1;
	}
	ptrEST->inEMVTableRecord = 1;
	strcpy(ptrEST->szTermAVN1, "0000");
	strcpy(ptrEST->szSecondTermAVN1, "0000");

}

//this function can be called several times for the same RID - next calls returns following records related to the same RID

int cUpdateCfg::locateConfigRecords(const char *AID, bool createNew /* = true */)
{
	int EST_h = open(EMV_CONFIG_EST, O_RDWR);
	int MVT_h = open(EMV_CONFIG_MVT, O_RDWR);

	const int EST_HEADER_SIZE = 16;
	const int MVT_HEADER_SIZE = 16;

	EST_REC *EST_tmp;
	MVT_REC *MVT_tmp;

	int read_bytes = 0;
	int total_read = 0;

	char *EST_buf = new char[sizeof(EST_REC)];
	char *MVT_buf = new char[sizeof(MVT_REC)];

	int iRet = 1;

	char Key[40];
	char Data[40];

	int MVT_pointer = -1;

	int curr_EST = 0;

	EST_offset = 0;
	AID_index = 0;


	if (AID == NULL || strlen(AID) == 0)
	{
		int result = 0;
		// special case
		MVT_offset = lseek(MVT_h, 0, SEEK_END);
		if (MVT_offset == MVT_HEADER_SIZE)
		{
			if (createNew)
			{
				dlog_msg("Setting up default MVT record");
				setDefaultMVT(&MVT_current);
				// if (MVT_offset == MVT_HEADER_SIZE) MVT_current.shRFU1 = 0x0080; // Enable separate GPO
				iRet = write(MVT_h, reinterpret_cast<char *>(&MVT_current), sizeof(MVT_REC));
			}
		}
		else
		{
			// We could be reading - check if MVT default record exists
			bool defaultRecordReferenced = false;
			dlog_msg("Checking if default record is referenced");
			lseek(EST_h, 0, SEEK_SET);
			read_bytes = read(EST_h, EST_buf, EST_HEADER_SIZE);
			int est_rec_no = 0;
			while (!defaultRecordReferenced && read_bytes > 0)
			{
				read_bytes = read(EST_h, EST_buf, sizeof(EST_REC));
				memcpy(&EST_current, EST_buf, sizeof(EST_REC));
				dlog_msg("EST to MVT record %d reference %d", est_rec_no++, EST_current.inEMVTableRecord);
				if (EST_current.inEMVTableRecord == 0)
				{
					dlog_alert("Yes it is!");
					defaultRecordReferenced = true;
				}
			}
			if (!defaultRecordReferenced)
			{
				// Just read first MVT record
				
				lseek(MVT_h, 0, SEEK_SET);
				dlog_msg("Reading default MVT record");
				read_bytes = read(MVT_h, MVT_buf, MVT_HEADER_SIZE);
				MVT_offset = read_bytes;
				read_bytes = read(MVT_h, MVT_buf, sizeof(MVT_REC));
				if (read_bytes == sizeof(MVT_REC))
				{
					memcpy(&MVT_current, MVT_buf, sizeof(MVT_REC));
					result = 1;
				}
				else
				{
					dlog_error("Cannot read default MVT record! Read %d bytes", read_bytes);
					result = -1;
				}
			}
		}
		close(EST_h);
		close(MVT_h);
		if (EST_buf) delete [] EST_buf;
		if (MVT_buf) delete [] MVT_buf;
		return result;
	}
	//first processing EST file
	//checking if this is next call for the same RID
	if(memcmp(currentRID.c_str(), AID, strlen(AID)) == 0)
	{
		dlog_msg(">>>Locating next record for the same RID: %s (last matching: %d)", AID, ESTrecord);
	}
	else
	{
		dlog_msg(">>>Locating first record for RID: %s", AID);
		currentRID.assign(AID, strlen(AID));
		ESTrecord = -1;
	}
	if(EST_h && EST_buf)
	{
		dlog_msg("Processing EST file");
		read_bytes = read(EST_h, EST_buf, EST_HEADER_SIZE);
		EST_offset = read_bytes;
		read_bytes = read(EST_h, EST_buf, sizeof(EST_REC));
		curr_EST = 0;
		while(read_bytes > 0)
		{
			dlog_msg("Processing %d/%d bytes", read_bytes, sizeof(EST_REC));
			memcpy(&EST_current, EST_buf, sizeof(EST_REC));
			dlog_msg("RID: %s, label: %s", EST_current.szRID, EST_current.szSchemeLabel);
			if(ESTrecord < curr_EST)
			{
				dlog_msg("Checking pos.1 %s", EST_current.szSupportedAID1);
				if(memcmp(EST_current.szSupportedAID1, AID, strlen(AID)) == 0)
				{
					
					dlog_msg("EST record located for AID %s-%d", AID, (EST_offset-EST_HEADER_SIZE)/sizeof(EST_REC));
					MVT_pointer = EST_current.inEMVTableRecord;
					AID_index = 1;
					break;
				}
				dlog_msg("Checking pos.2 %s", EST_current.szSupportedAID2);
				if(memcmp(EST_current.szSupportedAID2, AID, strlen(AID)) == 0)
				{
					dlog_msg("EST record located for AID %s-%d", AID, (EST_offset-EST_HEADER_SIZE)/sizeof(EST_REC));
					MVT_pointer = EST_current.inEMVTableRecord;
					AID_index = 2;
					break;
					
				}
				dlog_msg("Checking pos.3 %s", EST_current.szSupportedAID3);
				if(memcmp(EST_current.szSupportedAID3, AID, strlen(AID)) == 0)
				{
					dlog_msg("EST record located for AID %s-%d", AID, (EST_offset-EST_HEADER_SIZE)/sizeof(EST_REC));
					MVT_pointer = EST_current.inEMVTableRecord;
					AID_index = 3;
					break;
				}
				dlog_msg("Checking pos.4 %s", EST_current.szSupportedAID4);
				if(memcmp(EST_current.szSupportedAID4, AID, strlen(AID)) == 0)
				{
					dlog_msg("EST record located for AID %s-%d", AID, (EST_offset-EST_HEADER_SIZE)/sizeof(EST_REC));
					MVT_pointer = EST_current.inEMVTableRecord;
					AID_index = 4;
					break;
				}
				dlog_msg("Checking pos.5 %s", EST_current.szSupportedAID5);
				if(memcmp(EST_current.szSupportedAID5, AID, strlen(AID)) == 0)
				{
					dlog_msg("EST record located for AID %s-%d", AID, (EST_offset-EST_HEADER_SIZE)/sizeof(EST_REC));
					MVT_pointer = EST_current.inEMVTableRecord;
					AID_index = 5;
					break;
				}
				dlog_msg("Checking pos.6 %s", EST_current.szSupportedAID6);
				if(memcmp(EST_current.szSupportedAID6, AID, strlen(AID)) == 0)
				{
					dlog_msg("EST record located for AID %s-%d", AID, (EST_offset-EST_HEADER_SIZE)/sizeof(EST_REC));
					MVT_pointer = EST_current.inEMVTableRecord;
					AID_index = 6;
					break;
				}
				dlog_msg("Checking pos.7 %s", EST_current.szSupportedAID7);
				if(memcmp(EST_current.szSupportedAID7, AID, strlen(AID)) == 0)
				{
					dlog_msg("EST record located for AID %s-%d", AID, (EST_offset-EST_HEADER_SIZE)/sizeof(EST_REC));
					MVT_pointer = EST_current.inEMVTableRecord;
					AID_index = 7;
					break;
				}
				dlog_msg("Checking pos.8 %s", EST_current.szSupportedAID8);
				if(memcmp(EST_current.szSupportedAID8, AID, strlen(AID)) == 0)
				{
					dlog_msg("EST record located for AID %s-%d", AID, (EST_offset-EST_HEADER_SIZE)/sizeof(EST_REC));
					MVT_pointer = EST_current.inEMVTableRecord;
					AID_index = 8;
					break;
				}
				dlog_msg("Checking pos.9 %s", EST_current.szSupportedAID9);
				if(memcmp(EST_current.szSupportedAID9, AID, strlen(AID)) == 0)
				{
					dlog_msg("EST record located for AID %s-%d", AID, (EST_offset-EST_HEADER_SIZE)/sizeof(EST_REC));
					MVT_pointer = EST_current.inEMVTableRecord;
					AID_index = 9;
					break;
				}
				dlog_msg("Checking pos.10 %s", EST_current.szSupportedAID10);
				if(memcmp(EST_current.szSupportedAID10, AID, strlen(AID)) == 0)
				{
					dlog_msg("EST record located for AID %s-%d", AID, (EST_offset-EST_HEADER_SIZE)/sizeof(EST_REC));
					MVT_pointer = EST_current.inEMVTableRecord;
					AID_index = 10;
					break;
				}
			}
			EST_offset += read_bytes;
			curr_EST++;
			read_bytes = read(EST_h, EST_buf, sizeof(EST_REC));
			dlog_msg("%d bytes read from EST.dat file", read_bytes);
			
		}
		if(AID_index)
		{
			ESTrecord = curr_EST;
		}
		if(MVT_pointer < 0)
		{
			if (createNew)
			{
				//EST record not found
				EST_offset = lseek(EST_h, 0, SEEK_END);
				//setDefaultEST(reinterpret_cast<EST_REC *>(EST_buf));
				//iRet = write(EST_h, EST_buf, sizeof(EST_REC));
				setDefaultEST(&EST_current);
				iRet = write(EST_h, reinterpret_cast<char *>(&EST_current), sizeof(EST_REC));
				dlog_msg("EST record for AID '%s' not found... adding new (%d bytes)", AID, iRet);
				if (strlen(AID) > EMV_RID_SIZE) strcpy(EST_current.szSupportedAID1, AID);
				strncpy(EST_current.szRID, AID, EMV_RID_SIZE-1);
				AID_index = 1;
				MVT_offset = lseek(MVT_h, 0, SEEK_END);
				//setDefaultMVT(reinterpret_cast<MVT_REC *>(MVT_buf));
				//iRet = write(MVT_h, MVT_buf, sizeof(MVT_REC));
				//memcpy(&MVT_current, MVT_buf, sizeof(MVT_REC));
				setDefaultMVT(&MVT_current);
				// if (MVT_offset == MVT_HEADER_SIZE) MVT_current.shRFU1 = 0x0080; // Enable separate GPO
				iRet = write(MVT_h, reinterpret_cast<char *>(&MVT_current), sizeof(MVT_REC));
				EST_current.inEMVTableRecord = (MVT_offset+iRet-MVT_HEADER_SIZE)/sizeof(MVT_REC);
				dlog_msg("MVT record not found... adding new (%d bytes record: %d)", iRet, EST_current.inEMVTableRecord);
			}
			else
			{
				dlog_error("No record found for AID %s!", AID);
				AID_index = -1;
				//EST_offset = EST_HEADER_SIZE;
				//MVT_offset = MVT_HEADER_SIZE;
			}
		}
	}
	close(EST_h);
	if(MVT_h && MVT_buf && MVT_pointer >= 0)
	{
		lseek(MVT_h, 0, SEEK_SET);
		read_bytes = read(MVT_h, MVT_buf, MVT_HEADER_SIZE);
		MVT_offset = read_bytes;
		read_bytes = read(MVT_h, MVT_buf, sizeof(MVT_REC));
		memcpy(&MVT_current, MVT_buf, sizeof(MVT_REC));
		while(MVT_pointer && read_bytes > 0)
		{
			MVT_offset += read_bytes;
			read_bytes = read(MVT_h, MVT_buf, sizeof(MVT_REC));
			memcpy(&MVT_current, MVT_buf, sizeof(MVT_REC));
			MVT_pointer--;
		}
		dlog_msg("MVT record number: %d selected", (MVT_offset-MVT_HEADER_SIZE)/sizeof(MVT_REC));
	}
	close(MVT_h);

	dlog_msg("EST offset: %d (rec: %d) MVT offset: %d (rec: %d)", EST_offset, (EST_offset-EST_HEADER_SIZE)/sizeof(EST_REC), MVT_offset, (MVT_offset-MVT_HEADER_SIZE)/sizeof(MVT_REC));

	if (EST_buf) delete [] EST_buf;
	if (MVT_buf) delete [] MVT_buf;

	return AID_index;

}

namespace get_config_tools
{
	inline char * convertToDsp(const char * data, char * buffer)
	{
		int slen = strlen(data);
		memset(buffer, 0, (slen<<1)+1);
		SVC_HEX_2_DSP(data, buffer, strlen(data));
		return buffer;
	}

	unsigned long getConfigTag(unsigned long tag)
	{
		if ((tag & 0xFFFFFF00) == tag) tag >>= 8;
		if ((tag & 0xFFFF0000) == tag) tag >>= 16;
		if ((tag & 0xFF000000) == tag) tag >>= 24;
		return tag;
	}

	template <typename ValueType>
	void appendConfigTag(std::string & conf, unsigned long tag, ValueType val)
	{
		char tmp[256];
		sprintf(tmp, "%X%02X%0*X", getConfigTag(tag), sizeof(val), sizeof(val)<<1, val);
		dlog_msg("Appending next TLV object: %s", tmp);
		conf.append(tmp);
	}

	template <>
	void appendConfigTag<char *>(std::string & conf, unsigned long tag, char * val)
	//void appendConfigTag(std::string & conf, unsigned long tag, char * val)
	{
		char tmp[256];
		tmp[(sizeof tmp)-1] = 0;
		char buf_local[50];
		int slen = strlen(val);
		int i = 0;
		while(i < slen)
		{
			val[i] = toupper(val[i]);
			i++;
		}
		
		if (strspn(val, "0123456789ABCDEF") != slen)
		{
			val = convertToDsp(val, buf_local);
			slen = strlen(val);
		}
		slen = (slen >> 1) + (slen % 2);
		snprintf(tmp, (sizeof tmp)-1, "%X%02X%s", getConfigTag(tag), slen, val);
		dlog_msg("Appending TLV object: %s", tmp);
		conf.append(tmp);
	}

}

std::string cUpdateCfg::getConfig(const char *AID, short CAPKIndex)
{
	using namespace get_config_tools;
	std::string conf;

	// Try loading records, do NOT create new ones!
	//if RID is set we must process ALL est records related to given RID
	
	while(locateConfigRecords(AID, false) > 0)
	{
		if (CAPKIndex)
		{
			dlog_msg("Adding CAPK data only for index: 0x%X (%d)", CAPKIndex, CAPKIndex);
			dlog_msg("Trying pos. 1 index: 0x%X (%d)", EST_current.inPublicKeyIndex1, EST_current.inPublicKeyIndex1);
			if(EST_current.inPublicKeyIndex1 == CAPKIndex)
			{
				appendConfigTag(conf, TAG_8F_AUTH_PUBKEY_INDEX, EST_current.inPublicKeyIndex1);
				appendConfigTag(conf, TAG_DFCC20_CAPK_FILE, EST_current.szCAPKFile1);
				appendConfigTag(conf, TAG_DFAF32_CAPK_EXPIRY, EST_current.szCAPKExpDate1);
				conf.append(getCAPK(CAPKIndex));
			}
			
			dlog_msg("Trying pos. 2 index: 0x%X (%d)", EST_current.inPublicKeyIndex2, EST_current.inPublicKeyIndex2);
			if(EST_current.inPublicKeyIndex2 == CAPKIndex)
			{
				appendConfigTag(conf, TAG_8F_AUTH_PUBKEY_INDEX, EST_current.inPublicKeyIndex2);
				appendConfigTag(conf, TAG_DFCC20_CAPK_FILE, EST_current.szCAPKFile2);
				appendConfigTag(conf, TAG_DFAF32_CAPK_EXPIRY, EST_current.szCAPKExpDate2);
				conf.append(getCAPK(CAPKIndex));
			}
			
			dlog_msg("Trying pos. 3 index: 0x%X (%d)", EST_current.inPublicKeyIndex3, EST_current.inPublicKeyIndex3);
			if(EST_current.inPublicKeyIndex3 == CAPKIndex)
			{
				appendConfigTag(conf, TAG_8F_AUTH_PUBKEY_INDEX, EST_current.inPublicKeyIndex3);
				appendConfigTag(conf, TAG_DFCC20_CAPK_FILE, EST_current.szCAPKFile3);
				appendConfigTag(conf, TAG_DFAF32_CAPK_EXPIRY, EST_current.szCAPKExpDate3);
				conf.append(getCAPK(CAPKIndex));
			}
			
			dlog_msg("Trying pos. 4 index: 0x%X (%d)", EST_current.inPublicKeyIndex4, EST_current.inPublicKeyIndex4);
			if(EST_current.inPublicKeyIndex4 == CAPKIndex)
			{
				appendConfigTag(conf, TAG_8F_AUTH_PUBKEY_INDEX, EST_current.inPublicKeyIndex4);
				appendConfigTag(conf, TAG_DFCC20_CAPK_FILE, EST_current.szCAPKFile4);
				appendConfigTag(conf, TAG_DFAF32_CAPK_EXPIRY, EST_current.szCAPKExpDate4);
				conf.append(getCAPK(CAPKIndex));
			}
			
			dlog_msg("Trying pos. 5 index: 0x%X (%d)", EST_current.inPublicKeyIndex5, EST_current.inPublicKeyIndex5);
			if(EST_current.inPublicKeyIndex5 == CAPKIndex)
			{
				appendConfigTag(conf, TAG_8F_AUTH_PUBKEY_INDEX, EST_current.inPublicKeyIndex5);
				appendConfigTag(conf, TAG_DFCC20_CAPK_FILE, EST_current.szCAPKFile5);
				appendConfigTag(conf, TAG_DFAF32_CAPK_EXPIRY, EST_current.szCAPKExpDate5);
				conf.append(getCAPK(CAPKIndex));
			}
			
			dlog_msg("Trying pos. 6 index: 0x%X (%d)", EST_current.inPublicKeyIndex6, EST_current.inPublicKeyIndex6);
			if(EST_current.inPublicKeyIndex6 == CAPKIndex)
			{
				appendConfigTag(conf, TAG_8F_AUTH_PUBKEY_INDEX, EST_current.inPublicKeyIndex6);
				appendConfigTag(conf, TAG_DFCC20_CAPK_FILE, EST_current.szCAPKFile6);
				appendConfigTag(conf, TAG_DFAF32_CAPK_EXPIRY, EST_current.szCAPKExpDate6);
				conf.append(getCAPK(CAPKIndex));
			}
			
			dlog_msg("Trying pos. 7 index: 0x%X (%d)", EST_current.inPublicKeyIndex7, EST_current.inPublicKeyIndex7);
			if(EST_current.inPublicKeyIndex7 == CAPKIndex)
			{
				appendConfigTag(conf, TAG_8F_AUTH_PUBKEY_INDEX, EST_current.inPublicKeyIndex7);
				appendConfigTag(conf, TAG_DFCC20_CAPK_FILE, EST_current.szCAPKFile7);
				appendConfigTag(conf, TAG_DFAF32_CAPK_EXPIRY, EST_current.szCAPKExpDate7);
				conf.append(getCAPK(CAPKIndex));
			}
			
			dlog_msg("Trying pos. 8 index: 0x%X (%d)", EST_current.inPublicKeyIndex8, EST_current.inPublicKeyIndex8);
			if(EST_current.inPublicKeyIndex8 == CAPKIndex)
			{
				appendConfigTag(conf, TAG_8F_AUTH_PUBKEY_INDEX, EST_current.inPublicKeyIndex8);
				appendConfigTag(conf, TAG_DFCC20_CAPK_FILE, EST_current.szCAPKFile8);
				appendConfigTag(conf, TAG_DFAF32_CAPK_EXPIRY, EST_current.szCAPKExpDate8);
				conf.append(getCAPK(CAPKIndex));
			}
			
			dlog_msg("Trying pos. 9 index: 0x%X (%d)", EST_current.inPublicKeyIndex9, EST_current.inPublicKeyIndex9);
			if(EST_current.inPublicKeyIndex9 == CAPKIndex)
			{
				appendConfigTag(conf, TAG_8F_AUTH_PUBKEY_INDEX, EST_current.inPublicKeyIndex9);
				appendConfigTag(conf, TAG_DFCC20_CAPK_FILE, EST_current.szCAPKFile9);
				appendConfigTag(conf, TAG_DFAF32_CAPK_EXPIRY, EST_current.szCAPKExpDate9);
				conf.append(getCAPK(CAPKIndex));
			}
			
			dlog_msg("Trying pos. 10 index: 0x%X (%d)", EST_current.inPublicKeyIndex10, EST_current.inPublicKeyIndex10);
			if(EST_current.inPublicKeyIndex10 == CAPKIndex)
			{
				appendConfigTag(conf, TAG_8F_AUTH_PUBKEY_INDEX, EST_current.inPublicKeyIndex10);
				appendConfigTag(conf, TAG_DFCC20_CAPK_FILE, EST_current.szCAPKFile10);
				appendConfigTag(conf, TAG_DFAF32_CAPK_EXPIRY, EST_current.szCAPKExpDate10);
				conf.append(getCAPK(CAPKIndex));
			}
			
			dlog_msg("Trying pos. 11 index: 0x%X (%d)", EST_current.inPublicKeyIndex11, EST_current.inPublicKeyIndex11);
			if(EST_current.inPublicKeyIndex11 == CAPKIndex)
			{
				appendConfigTag(conf, TAG_8F_AUTH_PUBKEY_INDEX, EST_current.inPublicKeyIndex11);
				appendConfigTag(conf, TAG_DFCC20_CAPK_FILE, EST_current.szCAPKFile11);
				appendConfigTag(conf, TAG_DFAF32_CAPK_EXPIRY, EST_current.szCAPKExpDate11);
				conf.append(getCAPK(CAPKIndex));
			}
			
			dlog_msg("Trying pos. 12 index: 0x%X (%d)", EST_current.inPublicKeyIndex12, EST_current.inPublicKeyIndex12);
			if(EST_current.inPublicKeyIndex12 == CAPKIndex)
			{
				appendConfigTag(conf, TAG_8F_AUTH_PUBKEY_INDEX, EST_current.inPublicKeyIndex12);
				appendConfigTag(conf, TAG_DFCC20_CAPK_FILE, EST_current.szCAPKFile12);
				appendConfigTag(conf, TAG_DFAF32_CAPK_EXPIRY, EST_current.szCAPKExpDate12);
				conf.append(getCAPK(CAPKIndex));
			}
			
			dlog_msg("Trying pos. 13 index: 0x%X (%d)", EST_current.inPublicKeyIndex13, EST_current.inPublicKeyIndex13);
			if(EST_current.inPublicKeyIndex13 == CAPKIndex)
			{
				appendConfigTag(conf, TAG_8F_AUTH_PUBKEY_INDEX, EST_current.inPublicKeyIndex13);
				appendConfigTag(conf, TAG_DFCC20_CAPK_FILE, EST_current.szCAPKFile13);
				appendConfigTag(conf, TAG_DFAF32_CAPK_EXPIRY, EST_current.szCAPKExpDate13);
				conf.append(getCAPK(CAPKIndex));
			}
			
			dlog_msg("Trying pos. 14 index: 0x%X (%d)", EST_current.inPublicKeyIndex14, EST_current.inPublicKeyIndex14);
			if(EST_current.inPublicKeyIndex14 == CAPKIndex)
			{
				appendConfigTag(conf, TAG_8F_AUTH_PUBKEY_INDEX, EST_current.inPublicKeyIndex14);
				appendConfigTag(conf, TAG_DFCC20_CAPK_FILE, EST_current.szCAPKFile14);
				appendConfigTag(conf, TAG_DFAF32_CAPK_EXPIRY, EST_current.szCAPKExpDate14);
				conf.append(getCAPK(CAPKIndex));
			}
			
			dlog_msg("Trying pos. 15 index: 0x%X (%d)", EST_current.inPublicKeyIndex15, EST_current.inPublicKeyIndex15);
			if(EST_current.inPublicKeyIndex15 == CAPKIndex)
			{
				appendConfigTag(conf, TAG_8F_AUTH_PUBKEY_INDEX, EST_current.inPublicKeyIndex15);
				appendConfigTag(conf, TAG_DFCC20_CAPK_FILE, EST_current.szCAPKFile15);
				appendConfigTag(conf, TAG_DFAF32_CAPK_EXPIRY, EST_current.szCAPKExpDate15);
				conf.append(getCAPK(CAPKIndex));
			}
		}
		else
		{

			if(strlen(AID) > 0)
			{
				dlog_msg("***Adding EST parameters... matching %d record for RID: %s", ESTrecord, currentRID.c_str());

				//checking if we have RID only, so AID's list is ignorred.
				if(strlen(AID) > EMV_RID_SIZE-1)
				{
					dlog_msg("Adding AID specific data");
					if(strlen(EST_current.szSupportedAID1) && (memcmp(EST_current.szSupportedAID1, AID, strlen(AID)) == 0))
					{
						dlog_msg("Using EST definition for AID1: %s", EST_current.szSupportedAID1);
						appendConfigTag(conf, TAG_9F06_APPL_ID, EST_current.szSupportedAID1);
						appendConfigTag(conf, TAG_9F09_TERM_VER_NUM, EST_current.szTermAVN1);
						appendConfigTag(conf, TAG_DFAF11_PARTIAL_SELECTION, EST_current.inPartialNameAllowedFlag1);
						appendConfigTag(conf, TAG_DFAF12_SECOND_AVN, EST_current.szSecondTermAVN1);
						appendConfigTag(conf, TAG_DFAF13_REC_APP_NAME, EST_current.szRecommenedAppNameAID1);
					}
					if(strlen(EST_current.szSupportedAID2) && (memcmp(EST_current.szSupportedAID2, AID, strlen(AID)) == 0))
					{
						dlog_msg("Using EST definition for AID2: %s", EST_current.szSupportedAID2);
						appendConfigTag(conf, TAG_9F06_APPL_ID, EST_current.szSupportedAID2);
						appendConfigTag(conf, TAG_9F09_TERM_VER_NUM, EST_current.szTermAVN2);
						appendConfigTag(conf, TAG_DFAF11_PARTIAL_SELECTION, EST_current.inPartialNameAllowedFlag2);
						appendConfigTag(conf, TAG_DFAF12_SECOND_AVN, EST_current.szSecondTermAVN2);
						appendConfigTag(conf, TAG_DFAF13_REC_APP_NAME, EST_current.szRecommenedAppNameAID2);
					}
					if(strlen(EST_current.szSupportedAID3) && (memcmp(EST_current.szSupportedAID3, AID, strlen(AID)) == 0))
					{
						dlog_msg("Using EST definition for AID3: %s",  EST_current.szSupportedAID3);
						appendConfigTag(conf, TAG_9F06_APPL_ID, EST_current.szSupportedAID3);
						appendConfigTag(conf, TAG_9F09_TERM_VER_NUM, EST_current.szTermAVN3);
						appendConfigTag(conf, TAG_DFAF11_PARTIAL_SELECTION, EST_current.inPartialNameAllowedFlag3);
						appendConfigTag(conf, TAG_DFAF12_SECOND_AVN, EST_current.szSecondTermAVN3);
						appendConfigTag(conf, TAG_DFAF13_REC_APP_NAME, EST_current.szRecommenedAppNameAID3);
					}
					if(strlen(EST_current.szSupportedAID4) && (memcmp(EST_current.szSupportedAID4, AID, strlen(AID)) == 0))
					{
						dlog_msg("Using EST definition for AID4: %s",  EST_current.szSupportedAID4);
						appendConfigTag(conf, TAG_9F06_APPL_ID, EST_current.szSupportedAID4);
						appendConfigTag(conf, TAG_9F09_TERM_VER_NUM, EST_current.szTermAVN4);
						appendConfigTag(conf, TAG_DFAF11_PARTIAL_SELECTION, EST_current.inPartialNameAllowedFlag4);
						appendConfigTag(conf, TAG_DFAF12_SECOND_AVN, EST_current.szSecondTermAVN4);
						appendConfigTag(conf, TAG_DFAF13_REC_APP_NAME, EST_current.szRecommenedAppNameAID4);
					}
					if(strlen(EST_current.szSupportedAID5) && (memcmp(EST_current.szSupportedAID5, AID, strlen(AID)) == 0))
					{
						dlog_msg("Using EST definition for AID5: %s", EST_current.szSupportedAID5);
						appendConfigTag(conf, TAG_9F06_APPL_ID, EST_current.szSupportedAID5);
						appendConfigTag(conf, TAG_9F09_TERM_VER_NUM, EST_current.szTermAVN5);
						appendConfigTag(conf, TAG_DFAF11_PARTIAL_SELECTION, EST_current.inPartialNameAllowedFlag5);
						appendConfigTag(conf, TAG_DFAF12_SECOND_AVN, EST_current.szSecondTermAVN5);
						appendConfigTag(conf, TAG_DFAF13_REC_APP_NAME, EST_current.szRecommenedAppNameAID5);
					}
					if(strlen(EST_current.szSupportedAID6) && (memcmp(EST_current.szSupportedAID6, AID, strlen(AID)) == 0))
					{
						dlog_msg("Using EST definition for AID6: %s", EST_current.szSupportedAID6);
						appendConfigTag(conf, TAG_9F06_APPL_ID, EST_current.szSupportedAID6);
						appendConfigTag(conf, TAG_9F09_TERM_VER_NUM, EST_current.szTermAVN6);
						appendConfigTag(conf, TAG_DFAF11_PARTIAL_SELECTION, EST_current.inPartialNameAllowedFlag6);
						appendConfigTag(conf, TAG_DFAF12_SECOND_AVN, EST_current.szSecondTermAVN6);
						appendConfigTag(conf, TAG_DFAF13_REC_APP_NAME, EST_current.szRecommenedAppNameAID6);
					}
					if(strlen(EST_current.szSupportedAID7) && (memcmp(EST_current.szSupportedAID7, AID, strlen(AID)) == 0))
					{
						dlog_msg("Using EST definition for AID7: %s", EST_current.szSupportedAID7);
						appendConfigTag(conf, TAG_9F06_APPL_ID, EST_current.szSupportedAID7);
						appendConfigTag(conf, TAG_9F09_TERM_VER_NUM, EST_current.szTermAVN7);
						appendConfigTag(conf, TAG_DFAF11_PARTIAL_SELECTION, EST_current.inPartialNameAllowedFlag7);
						appendConfigTag(conf, TAG_DFAF12_SECOND_AVN, EST_current.szSecondTermAVN7);
						appendConfigTag(conf, TAG_DFAF13_REC_APP_NAME, EST_current.szRecommenedAppNameAID7);
					}
					if(strlen(EST_current.szSupportedAID8) && (memcmp(EST_current.szSupportedAID8, AID, strlen(AID)) == 0))
					{
						dlog_msg("Using EST definition for AID8: %s", EST_current.szSupportedAID8);
						appendConfigTag(conf, TAG_9F06_APPL_ID, EST_current.szSupportedAID8);
						appendConfigTag(conf, TAG_9F09_TERM_VER_NUM, EST_current.szTermAVN8);
						appendConfigTag(conf, TAG_DFAF11_PARTIAL_SELECTION, EST_current.inPartialNameAllowedFlag8);
						appendConfigTag(conf, TAG_DFAF12_SECOND_AVN, EST_current.szSecondTermAVN8);
						appendConfigTag(conf, TAG_DFAF13_REC_APP_NAME, EST_current.szRecommenedAppNameAID8);
					}
					if(strlen(EST_current.szSupportedAID9) && (memcmp(EST_current.szSupportedAID9, AID, strlen(AID)) == 0))
					{
						dlog_msg("Using EST definition for AID9: %s", EST_current.szSupportedAID9);
						appendConfigTag(conf, TAG_9F06_APPL_ID, EST_current.szSupportedAID9);
						appendConfigTag(conf, TAG_9F09_TERM_VER_NUM, EST_current.szTermAVN9);
						appendConfigTag(conf, TAG_DFAF11_PARTIAL_SELECTION, EST_current.inPartialNameAllowedFlag9);
						appendConfigTag(conf, TAG_DFAF12_SECOND_AVN, EST_current.szSecondTermAVN9);
						appendConfigTag(conf, TAG_DFAF13_REC_APP_NAME, EST_current.szRecommenedAppNameAID9);
					}
					if(strlen(EST_current.szSupportedAID10) && (memcmp(EST_current.szSupportedAID10, AID, strlen(AID)) == 0))
					{
						dlog_msg("Using EST definition for AID10: %s", EST_current.szSupportedAID10);
						appendConfigTag(conf, TAG_9F06_APPL_ID, EST_current.szSupportedAID10);
						appendConfigTag(conf, TAG_9F09_TERM_VER_NUM, EST_current.szTermAVN10);
						appendConfigTag(conf, TAG_DFAF11_PARTIAL_SELECTION, EST_current.inPartialNameAllowedFlag10);
						appendConfigTag(conf, TAG_DFAF12_SECOND_AVN, EST_current.szSecondTermAVN10);
						appendConfigTag(conf, TAG_DFAF13_REC_APP_NAME, EST_current.szRecommenedAppNameAID10);;
					}
				}
				else
				{
					dlog_msg("Ignoring AID data as RID related data requested: %s", currentRID.c_str());
				}
			}
			dlog_msg("Adding MVT parameters...");
			
			appendConfigTag(conf, TAG_9F1B_TERM_FLOOR_LIMIT, MVT_current.lnEMVFloorLimit);
			appendConfigTag(conf, TAG_DFAF14_RND_SEL_THRESHOLD, MVT_current.lnEMVRSThreshold);
			appendConfigTag(conf, TAG_DFAF15_RND_SEL_PERCENTAGE, MVT_current.inEMVTargetRSPercent);
			appendConfigTag(conf, TAG_DFAF16_RND_MAX_RND_PERCENTAGE, MVT_current.inEMVMaxTargetRSPercent);
			appendConfigTag(conf, TAG_DFAF17_TAC_DEFAULT, MVT_current.szEMVTACDefault);
			appendConfigTag(conf, TAG_DFAF18_TAC_DENIAL, MVT_current.szEMVTACDenial);
			appendConfigTag(conf, TAG_DFAF19_TAC_ONLINE, MVT_current.szEMVTACOnline);
			appendConfigTag(conf, TAG_DFAF1A_DEFAULT_TDOL, MVT_current.szDefaultTDOL);
			appendConfigTag(conf, TAG_DFAF1B_DEFAULT_DDOL, MVT_current.szDefaultDDOL);
			appendConfigTag(conf, TAG_9F1A_TERM_COUNTY_CODE, MVT_current.szEMVTermCountryCode);
			appendConfigTag(conf, TAG_5F2A_TRANS_CURCY_CODE, MVT_current.szEMVTermCurrencyCode);
			appendConfigTag(conf, TAG_5F36_TRANS_CURR_EXP, MVT_current.inEMVTermCurExp);
			appendConfigTag(conf, TAG_9F33_TERM_CAP, MVT_current.szEMVTermCapabilities);
			appendConfigTag(conf, TAG_9F40_TERM_ADTNAL_CAP, MVT_current.szEMVTermAddCapabilities);
			appendConfigTag(conf, TAG_9F35_TERM_TYPE, MVT_current.szEMVTermType);
			appendConfigTag(conf, TAG_9F15_MER_CAT_CODE, MVT_current.szEMVMerchantCategoryCode);
			appendConfigTag(conf, TAG_DFAF1E_MCC, MVT_current.szEMVTerminalCategoryCode);

			if((strlen(AID) == 0) || (AID == NULL))
			{
				dlog_msg("Exiting after addition of default parameters");
				break;
			}

			
		}

		dlog_msg("Config for AID: %s: %s (%d bytes)", AID, conf.c_str(), conf.size());
	}
	

	return conf;
}


int cUpdateCfg::deleteConfig(const char * AID)
{
	int result = 0;
	const int aidLen = strlen(AID);
	const int RID_SIZE = (EMV_RID_SIZE-1);
	const int EST_HEADER_SIZE = 16;
	const int MVT_HEADER_SIZE = 16;
	const char EST_TEMP[] = "EST.DAT.TMP";
	const char MVT_TEMP[] = "MVT.DAT.TMP";

	int EST_copy_h = open(EST_TEMP, O_CREAT | O_TRUNC | O_WRONLY);
	int MVT_copy_h = open(MVT_TEMP, O_CREAT | O_TRUNC | O_WRONLY);
	int EST_h = open(EMV_CONFIG_EST, O_RDWR);
	int MVT_h = open(EMV_CONFIG_MVT, O_RDWR);

	char *EST_buf = new char[sizeof(EST_REC)];
	char *MVT_buf = new char[sizeof(MVT_REC)];

	bool replaceFiles = false;

	std::vector<std::string> capkFiles;

	if (AID && AID[0] != 0)
	{
		if (strlen(AID) == RID_SIZE)
		{
			dlog_msg("Deleting configs for all AIDs from RID %s", AID);
		}
		else
		{
			dlog_msg("Deleting config for AID %s", AID);
		}
	}
	else
	{
		dlog_alert("Clearing the configuration!");
	}

	// Load both MVT and EST records, they are connected
	if (EST_h && MVT_h && EST_copy_h && MVT_copy_h && EST_buf && MVT_buf)
	{
		int read_bytes = read(EST_h, EST_buf, EST_HEADER_SIZE);
		int written_bytes = write(EST_copy_h, EST_buf, EST_HEADER_SIZE);
		read_bytes = read(MVT_h, MVT_buf, MVT_HEADER_SIZE);
		written_bytes = write(MVT_copy_h, MVT_buf, MVT_HEADER_SIZE);
		// Copy default MVT record
		read_bytes = read(MVT_h, MVT_buf, sizeof(MVT_REC));
		write(MVT_copy_h, MVT_buf, read_bytes);

		read_bytes = read(EST_h, EST_buf, sizeof(EST_REC));
		memcpy(&EST_current, EST_buf, sizeof(EST_REC));
		while (read_bytes > 0)
		{
			bool saveThisRecord = false;
			dlog_msg("EST Record for RID '%s'", EST_current.szRID);
			do
			{
				if (AID && AID[0] != 0)
				{
					if (strlen(AID) == RID_SIZE)
					{
						if (!memcmp(EST_current.szRID, AID, RID_SIZE))
						{
							break;
						}
						saveThisRecord = true;
					}
					else
					{
						// Check all supported AIDs in this record
						char * pszAID = 0;
						if (!memcmp(EST_current.szSupportedAID1, AID, aidLen)) pszAID = EST_current.szSupportedAID1;
						else if (!memcmp(EST_current.szSupportedAID2, AID, aidLen)) pszAID = EST_current.szSupportedAID2;
						else if (!memcmp(EST_current.szSupportedAID3, AID, aidLen)) pszAID = EST_current.szSupportedAID3;
						else if (!memcmp(EST_current.szSupportedAID4, AID, aidLen)) pszAID = EST_current.szSupportedAID4;
						else if (!memcmp(EST_current.szSupportedAID5, AID, aidLen)) pszAID = EST_current.szSupportedAID5;
						else if (!memcmp(EST_current.szSupportedAID6, AID, aidLen)) pszAID = EST_current.szSupportedAID6;
						else if (!memcmp(EST_current.szSupportedAID7, AID, aidLen)) pszAID = EST_current.szSupportedAID7;
						else if (!memcmp(EST_current.szSupportedAID8, AID, aidLen)) pszAID = EST_current.szSupportedAID8;
						else if (!memcmp(EST_current.szSupportedAID9, AID, aidLen)) pszAID = EST_current.szSupportedAID9;
						else if (!memcmp(EST_current.szSupportedAID10, AID, aidLen)) pszAID = EST_current.szSupportedAID10;
						if (pszAID)
						{
							memset(pszAID, 0, EMV_MAX_AID_SIZE+1);
							if (strlen(EST_current.szSupportedAID1) ||
									strlen(EST_current.szSupportedAID2) ||
									strlen(EST_current.szSupportedAID3) ||
									strlen(EST_current.szSupportedAID4) ||
									strlen(EST_current.szSupportedAID5) ||
									strlen(EST_current.szSupportedAID6) ||
									strlen(EST_current.szSupportedAID7) ||
									strlen(EST_current.szSupportedAID8) ||
									strlen(EST_current.szSupportedAID9) ||
									strlen(EST_current.szSupportedAID10))
							{
								saveThisRecord = true;
							}
						}
						else
						{
							saveThisRecord = true;
						}
					}
				}
				// else - remove this record
			} while(false);
			if (saveThisRecord)
			{
				// Find MVT record for this one
				int mvtRecOffset = MVT_HEADER_SIZE + ((EST_current.inEMVTableRecord-1) * sizeof(MVT_REC));
				dlog_msg("Record needs saving! Copying MVT index %d", EST_current.inEMVTableRecord);
				lseek(MVT_h, mvtRecOffset, SEEK_SET);
				read_bytes = read(MVT_h, MVT_buf, sizeof(MVT_REC));
				written_bytes = write(MVT_copy_h, MVT_buf, sizeof(MVT_REC));
				mvtRecOffset = lseek(MVT_copy_h, 0, SEEK_END);
				EST_current.inEMVTableRecord = (mvtRecOffset - MVT_HEADER_SIZE) / sizeof(MVT_REC);
				memcpy(EST_buf, &EST_current, sizeof(EST_REC));
				written_bytes = write(EST_copy_h, EST_buf, sizeof(EST_REC));
				// done
			}
			else
			{
				dlog_msg("Record will be removed!");
				// We need to remove CAPK files
				if (strlen(EST_current.szCAPKFile1)) capkFiles.push_back(EST_current.szCAPKFile1);
				if (strlen(EST_current.szCAPKFile2)) capkFiles.push_back(EST_current.szCAPKFile2);
				if (strlen(EST_current.szCAPKFile3)) capkFiles.push_back(EST_current.szCAPKFile3);
				if (strlen(EST_current.szCAPKFile4)) capkFiles.push_back(EST_current.szCAPKFile4);
				if (strlen(EST_current.szCAPKFile5)) capkFiles.push_back(EST_current.szCAPKFile5);
				if (strlen(EST_current.szCAPKFile6)) capkFiles.push_back(EST_current.szCAPKFile6);
				if (strlen(EST_current.szCAPKFile7)) capkFiles.push_back(EST_current.szCAPKFile7);
				if (strlen(EST_current.szCAPKFile8)) capkFiles.push_back(EST_current.szCAPKFile8);
				if (strlen(EST_current.szCAPKFile9)) capkFiles.push_back(EST_current.szCAPKFile9);
				if (strlen(EST_current.szCAPKFile10)) capkFiles.push_back(EST_current.szCAPKFile10);
				if (strlen(EST_current.szCAPKFile11)) capkFiles.push_back(EST_current.szCAPKFile11);
				if (strlen(EST_current.szCAPKFile12)) capkFiles.push_back(EST_current.szCAPKFile12);
				if (strlen(EST_current.szCAPKFile13)) capkFiles.push_back(EST_current.szCAPKFile13);
				if (strlen(EST_current.szCAPKFile14)) capkFiles.push_back(EST_current.szCAPKFile14);
				if (strlen(EST_current.szCAPKFile15)) capkFiles.push_back(EST_current.szCAPKFile15);
			}
			read_bytes = read(EST_h, EST_buf, sizeof(EST_REC));
			if (read_bytes > 0 && read_bytes != sizeof(EST_REC)) dlog_alert("Read error!");
			memcpy(&EST_current, EST_buf, read_bytes);
		}
		// Replace original files
		if (lseek(MVT_copy_h, 0, SEEK_END) <= MVT_HEADER_SIZE &&
				lseek(EST_copy_h, 0, SEEK_END) <= EST_HEADER_SIZE)
		{
			// Create new files
			if (genConfig("default_emv.cfg") <= 0)
			{
				dlog_alert("Generate config failed, creating empty MVT/EST files!");
				setDefaultMVT(&MVT_current);
				written_bytes = write(MVT_copy_h, reinterpret_cast<char *>(&MVT_current), sizeof(MVT_REC));
				setDefaultEST(&EST_current);
				written_bytes = write(EST_copy_h, reinterpret_cast<char *>(&EST_current), sizeof(EST_REC));
			}
		}
	}
	if (EST_h >= 0) close(EST_h);
	if (MVT_h >= 0) close(MVT_h);
	if (EST_copy_h >= 0) close(EST_copy_h);
	if (MVT_copy_h >= 0) close(MVT_copy_h);
	if (EST_buf) delete [] EST_buf;
	if (MVT_buf) delete [] MVT_buf;

	_remove(EMV_CONFIG_EST);
	_remove(EMV_CONFIG_MVT);
	_rename(EST_TEMP, EMV_CONFIG_EST);
	_rename(MVT_TEMP, EMV_CONFIG_MVT);

	while (!capkFiles.empty())
	{
		std::string capk = capkFiles.back();
		capkFiles.pop_back();
		dlog_msg("Removing CAPK file '%s'", capk.c_str());
		_remove(capk.c_str());
	}

	return result;
}

int cUpdateCfg::buildCAPK(void)
{
	std::string capk_file;

	char *mod_str;
	char tmp[20];
	int iRet = 0;
	int exp_size;

	dlog_msg("building new CAPK file for AID %s index 0x%X", g_CardAppConfig.GetCurrentAID().c_str(), CAPK_index);
	if(CAPK_modulus.size() <= 0 || CAPK_exponent.size() <= 0 || CAPK_index == 0xFF)
	{
		dlog_msg("data set not ready...");
		return iRet;
	}
	if (g_CardAppConfig.GetCurrentAID().size() <  EMV_RID_SIZE-1)
	{
		dlog_error("AID undefined or invalid!");
		return iRet;
	}

	mod_str = new char[CAPK_modulus.size()*2+1];

	
	SVC_HEX_2_DSP(CAPK_modulus.data(), mod_str, CAPK_modulus.size());
	sprintf(tmp, "%03d", CAPK_modulus.size());
	capk_file.append(tmp);
	capk_file.append(mod_str, CAPK_modulus.size()*2);
	
	SVC_HEX_2_DSP(CAPK_exponent.data(), mod_str, CAPK_exponent.size());
	sprintf(tmp, "%02d", CAPK_exponent.size());
	capk_file.append(tmp);
	capk_file.append(mod_str, CAPK_exponent.size()*2);
	if (CAPK_hash.size())
	{
		SVC_HEX_2_DSP(CAPK_hash.data(), mod_str, CAPK_hash.size());
		capk_file.append(mod_str, CAPK_hash.size()*2);
	}

	CAPK_file.assign(g_CardAppConfig.GetCurrentAID(), 0, EMV_RID_SIZE-1);
	sprintf(tmp, ".%02X", CAPK_index);
	CAPK_file.append(tmp);
	dlog_msg("Writing CAPK file %s", CAPK_file.c_str());
	
	int h_file = open(CAPK_file.c_str(), O_WRONLY|O_CREAT|O_TRUNC);
	if(h_file)
	{
		iRet = write(h_file, capk_file.c_str(), capk_file.size());
		dlog_msg("Writing CAPK into %s = %d", CAPK_file.c_str(), iRet);
		dlog_msg("CAPK: %s", capk_file.c_str());
		close(h_file);
		memset(tmp,0,sizeof(tmp));
		SVC_HEX_2_DSP(CAPK_expiry.data(), tmp, CAPK_expiry.size());
		updateCAPK(CAPK_index, (char *)CAPK_file.c_str(), -1, tmp);
	}
	delete [] mod_str;
	
	capk_file.clear();
	
	return iRet;

}

#define TMP_BUF_LEN 1024

std::string cUpdateCfg::getCAPK(short index)
{

	int position = -1;
	std::string CAPK_data;
	char filename[EMV_PK_FILE_NAME+1];
	char tmp[TMP_BUF_LEN*2+1];
	
	dlog_msg("Getting CAPK data index %X for %s", index, g_CardAppConfig.GetCurrentAID().c_str());

	
	if(index > 0x00 && index <= 0xFF)
	{
	

		dlog_msg("CAPK1 - index: %02X", EST_current.inPublicKeyIndex1);
		if(EST_current.inPublicKeyIndex1 == index || position == 1)
		{
			strcpy(filename, EST_current.szCAPKFile1);
			//snprintf(tmp, sizeof(tmp), "%X%02X%s", TAG_DFAF32_CAPK_EXPIRY, strlen(EST_current.szCAPKExpDate1)/2, EST_current.szCAPKExpDate1);
			position = 1;
		}
		
		dlog_msg("CAPK2 - index: %02X", EST_current.inPublicKeyIndex2);
		if(EST_current.inPublicKeyIndex2 == index || position == 2)
		{
			strcpy(filename, EST_current.szCAPKFile2);
			//snprintf(tmp, sizeof(tmp), "%X%02X%s", TAG_DFAF32_CAPK_EXPIRY, strlen(EST_current.szCAPKExpDate2)/2, EST_current.szCAPKExpDate2);
			position = 2;
		}
		
		dlog_msg("CAPK3 - index: %02X", EST_current.inPublicKeyIndex3);
		if(EST_current.inPublicKeyIndex3 == index || position == 3)
		{
			strcpy(filename, EST_current.szCAPKFile3);
			//snprintf(tmp, sizeof(tmp), "%X%02X%s", TAG_DFAF32_CAPK_EXPIRY, strlen(EST_current.szCAPKExpDate3)/2, EST_current.szCAPKExpDate3);
			position = 3;
		}
		
		dlog_msg("CAPK4 - index: %02X", EST_current.inPublicKeyIndex4);
		if(EST_current.inPublicKeyIndex4 == index || position == 4)
		{
			strcpy(filename, EST_current.szCAPKFile4);
			//snprintf(tmp, sizeof(tmp), "%X%02X%s", TAG_DFAF32_CAPK_EXPIRY, strlen(EST_current.szCAPKExpDate4)/2, EST_current.szCAPKExpDate4);
			position = 4;
		}
		
		dlog_msg("CAPK5 - index: %02X", EST_current.inPublicKeyIndex5);
		if(EST_current.inPublicKeyIndex5 == index || position == 5)
		{
			strcpy(filename, EST_current.szCAPKFile5);
			//snprintf(tmp, sizeof(tmp), "%X%02X%s", TAG_DFAF32_CAPK_EXPIRY, strlen(EST_current.szCAPKExpDate5)/2, EST_current.szCAPKExpDate5);
			position = 5;
		}
		
		dlog_msg("CAPK6 - index: %02X", EST_current.inPublicKeyIndex6);
		if(EST_current.inPublicKeyIndex6 == index || position == 6)
		{
			strcpy(filename, EST_current.szCAPKFile6);
			//snprintf(tmp, sizeof(tmp), "%X%02X%s", TAG_DFAF32_CAPK_EXPIRY, strlen(EST_current.szCAPKExpDate6)/2, EST_current.szCAPKExpDate6);
			position = 6;
		}
		
		dlog_msg("CAPK7 - index: %02X", EST_current.inPublicKeyIndex7);
		if(EST_current.inPublicKeyIndex7 == index || position == 7)
		{
			strcpy(filename, EST_current.szCAPKFile7);
			//snprintf(tmp, sizeof(tmp), "%X%02X%s", TAG_DFAF32_CAPK_EXPIRY, strlen(EST_current.szCAPKExpDate7)/2, EST_current.szCAPKExpDate7);
			position = 7;
		}
		
		dlog_msg("CAPK8 - index: %02X", EST_current.inPublicKeyIndex8);
		if(EST_current.inPublicKeyIndex8 == index || position == 8)
		{
			strcpy(filename, EST_current.szCAPKFile8);
			//snprintf(tmp, sizeof(tmp), "%X%02X%s", TAG_DFAF32_CAPK_EXPIRY, strlen(EST_current.szCAPKExpDate8)/2, EST_current.szCAPKExpDate8);
			position = 8;
		}
		
		dlog_msg("CAPK9 - index: %02X", EST_current.inPublicKeyIndex9);
		if(EST_current.inPublicKeyIndex9 == index || position == 9)
		{
			strcpy(filename, EST_current.szCAPKFile9);
			//snprintf(tmp, sizeof(tmp), "%X%02X%s", TAG_DFAF32_CAPK_EXPIRY, strlen(EST_current.szCAPKExpDate9)/2, EST_current.szCAPKExpDate9);
			position = 9;
		}
		
		dlog_msg("CAPK10 - index: %02X", EST_current.inPublicKeyIndex10);
		if(EST_current.inPublicKeyIndex10 == index || position == 10)
		{
			strcpy(filename, EST_current.szCAPKFile10);
			//snprintf(tmp, sizeof(tmp), "%X%02X%s", TAG_DFAF32_CAPK_EXPIRY, strlen(EST_current.szCAPKExpDate10)/2, EST_current.szCAPKExpDate10);
			position = 10;
		}
		
		dlog_msg("CAPK11 - index: %02X", EST_current.inPublicKeyIndex11);
		if(EST_current.inPublicKeyIndex11 == index || position == 11)
		{
			strcpy(filename, EST_current.szCAPKFile11);
			//snprintf(tmp, sizeof(tmp), "%X%02X%s", TAG_DFAF32_CAPK_EXPIRY, strlen(EST_current.szCAPKExpDate11)/2, EST_current.szCAPKExpDate11);
			position = 11;
		}
		
		dlog_msg("CAPK12 - index: %02X", EST_current.inPublicKeyIndex12);
		if(EST_current.inPublicKeyIndex12 == index || position == 12)
		{
			strcpy(filename, EST_current.szCAPKFile12);
			//snprintf(tmp, sizeof(tmp), "%X%02X%s", TAG_DFAF32_CAPK_EXPIRY, strlen(EST_current.szCAPKExpDate12)/2, EST_current.szCAPKExpDate12);
			position = 12;
		}
		
		dlog_msg("CAPK13 - index: %02X", EST_current.inPublicKeyIndex13);
		if(EST_current.inPublicKeyIndex13 == index || position == 13)
		{
			strcpy(filename, EST_current.szCAPKFile13);
			//snprintf(tmp, sizeof(tmp), "%X%02X%s", TAG_DFAF32_CAPK_EXPIRY, strlen(EST_current.szCAPKExpDate13)/2, EST_current.szCAPKExpDate13);
			position = 13;
		}
		
		dlog_msg("CAPK14 - index: %02X", EST_current.inPublicKeyIndex14);
		if(EST_current.inPublicKeyIndex14 == index || position == 14)
		{
			strcpy(filename, EST_current.szCAPKFile14);
			//snprintf(tmp, sizeof(tmp), "%X%02X%s", TAG_DFAF32_CAPK_EXPIRY, strlen(EST_current.szCAPKExpDate14)/2, EST_current.szCAPKExpDate14);
			position = 14;
		}
		
		dlog_msg("CAPK15 - index: %02X", EST_current.inPublicKeyIndex15);
		if(EST_current.inPublicKeyIndex15 == index || position == 15)
		{
			strcpy(filename, EST_current.szCAPKFile15);
			//snprintf(tmp, sizeof(tmp), "%X%02X%s", TAG_DFAF32_CAPK_EXPIRY, strlen(EST_current.szCAPKExpDate15)/2, EST_current.szCAPKExpDate15);
			position = 15;
		}

		CAPK_data.clear();
		if(position > 0)
		{
			//CAPK_data.append(tmp);
			dlog_msg("Key index %X located on pos. %d, filename: %s", index, position, filename);
			int CAPK_H = open(filename, O_RDONLY);
			if(CAPK_H >= 0)
			{
				char CAPK_buf[TMP_BUF_LEN];
				int keydatasize = 0;
				memset(CAPK_buf, 0, TMP_BUF_LEN);
				if( (keydatasize = read(CAPK_H, CAPK_buf, TMP_BUF_LEN)) > 0 )
				{
					dlog_msg("Key file content (%d bytes): %s", keydatasize, CAPK_buf );
					// Prepare TLV out of that contents...
					char len[4];
					int size = 0;
					int index = 0;
					char copyTemp;
					memset(len, 0, sizeof(len));
					memcpy(len, CAPK_buf, 3);
					if (sscanf(len, "%d", &size) == 1)
					{
						dlog_msg("Modulus size %d", size);
						copyTemp = CAPK_buf[size * 2 + 3];
						CAPK_buf[size * 2 + 3] = 0;
						if (size > 0xFF) snprintf(tmp, sizeof(tmp), "%X82%04X%s", TAG_DFAF0E_CAPK_MODULUS, size , CAPK_buf+3);
						else if (size > 0x7F) snprintf(tmp, sizeof(tmp), "%X81%02X%s", TAG_DFAF0E_CAPK_MODULUS, size , CAPK_buf+3);
						else snprintf(tmp, sizeof(tmp), "%X%02X%s", TAG_DFAF0E_CAPK_MODULUS, size , CAPK_buf+3);
						CAPK_buf[size * 2 + 3] = copyTemp;
						CAPK_data.append(tmp);
						index += size * 2 + 3;
						memset(len, 0, sizeof(len));
						memcpy(len, CAPK_buf + index, 2);
						if (sscanf(len, "%d", &size) == 1)
						{
							dlog_msg("Exponent size %d", size);
							copyTemp = CAPK_buf[index + size * 2 + 2];
							CAPK_buf[index + size * 2 + 2] = 0;
							snprintf(tmp, sizeof(tmp), "%X%02X%s", TAG_DFAF0F_CAPK_EXPONENT, size, CAPK_buf+index+2);
							CAPK_buf[index + size * 2 + 2] = copyTemp;
							CAPK_data.append(tmp);
							index += size * 2 + 2;
							dlog_msg("Key checksum (%d bytes): %s", strlen(CAPK_buf+index), CAPK_buf+index);
							if (strlen(CAPK_buf+index) >= MAX_HASH_BUFFER*2)
							{
								CAPK_buf[index+MAX_HASH_BUFFER*2] = 0;
								snprintf(tmp, sizeof(tmp), "%X%02X%s", TAG_DFAF10_CAPK_HASH, MAX_HASH_BUFFER, CAPK_buf+index);
								CAPK_data.append(tmp);
							}
						}
					}
				}
				close(CAPK_H);
			}
		}

		dlog_msg("CAPK data ready: %s", CAPK_data.c_str());
	
	}

	return CAPK_data;
	

}

int cUpdateCfg::deleteCAPK(const char * AID, short index)
{
	int result = -1;
	int position = -1;
	std::string capkFile;
	
	if (AID && (strlen(AID) >= EMV_RID_SIZE - 1) && (locateConfigRecords(AID, false) > 0))
	{
		dlog_msg("We have EST record for RID %s, label %s", EST_current.szRID, EST_current.szSchemeLabel);
		result = 0;
		if (index > 0 && index <= 0xFF)
		{
			dlog_msg("CAPK1 - index: %02X", EST_current.inPublicKeyIndex1);
			if(EST_current.inPublicKeyIndex1 == index || position == 1)
			{
				EST_current.inPublicKeyIndex1 = CAPK_FILLER;
				capkFile.assign(EST_current.szCAPKFile1);
				memset(EST_current.szCAPKFile1, 0, EMV_PK_FILE_NAME);
				position = 1;
			}
			
			dlog_msg("CAPK2 - index: %02X", EST_current.inPublicKeyIndex2);
			if(EST_current.inPublicKeyIndex2 == index || position == 2)
			{
				EST_current.inPublicKeyIndex2 = CAPK_FILLER;
				capkFile.assign(EST_current.szCAPKFile2);
				memset(EST_current.szCAPKFile2, 0, EMV_PK_FILE_NAME);
				position = 2;
			}
			
			dlog_msg("CAPK3 - index: %02X", EST_current.inPublicKeyIndex3);
			if(EST_current.inPublicKeyIndex3 == index || position == 3)
			{
				EST_current.inPublicKeyIndex3 = CAPK_FILLER;
				capkFile.assign(EST_current.szCAPKFile3);
				memset(EST_current.szCAPKFile3, 0, EMV_PK_FILE_NAME);
				position = 3;
			}
			
			dlog_msg("CAPK4 - index: %02X", EST_current.inPublicKeyIndex4);
			if(EST_current.inPublicKeyIndex4 == index || position == 4)
			{
				EST_current.inPublicKeyIndex4 = CAPK_FILLER;
				capkFile.assign(EST_current.szCAPKFile4);
				memset(EST_current.szCAPKFile4, 0, EMV_PK_FILE_NAME);
				position = 4;
			}
			
			dlog_msg("CAPK5 - index: %02X", EST_current.inPublicKeyIndex5);
			if(EST_current.inPublicKeyIndex5 == index || position == 5)
			{
				EST_current.inPublicKeyIndex5 = CAPK_FILLER;
				capkFile.assign(EST_current.szCAPKFile5);
				memset(EST_current.szCAPKFile5, 0, EMV_PK_FILE_NAME);
				position = 5;
			}
			
			dlog_msg("CAPK6 - index: %02X", EST_current.inPublicKeyIndex6);
			if(EST_current.inPublicKeyIndex6 == index || position == 6)
			{
				EST_current.inPublicKeyIndex6 = CAPK_FILLER;
				capkFile.assign(EST_current.szCAPKFile6);
				memset(EST_current.szCAPKFile6, 0, EMV_PK_FILE_NAME);
				position = 6;
			}
			
			dlog_msg("CAPK7 - index: %02X", EST_current.inPublicKeyIndex7);
			if(EST_current.inPublicKeyIndex7 == index || position == 7)
			{
				EST_current.inPublicKeyIndex7 = CAPK_FILLER;
				capkFile.assign(EST_current.szCAPKFile7);
				memset(EST_current.szCAPKFile7, 0, EMV_PK_FILE_NAME);
				position = 7;
			}
			
			dlog_msg("CAPK8 - index: %02X", EST_current.inPublicKeyIndex8);
			if(EST_current.inPublicKeyIndex8 == index || position == 8)
			{
				EST_current.inPublicKeyIndex8 = CAPK_FILLER;
				capkFile.assign(EST_current.szCAPKFile8);
				memset(EST_current.szCAPKFile8, 0, EMV_PK_FILE_NAME);
				position = 8;
			}
			
			dlog_msg("CAPK9 - index: %02X", EST_current.inPublicKeyIndex9);
			if(EST_current.inPublicKeyIndex9 == index || position == 9)
			{
				EST_current.inPublicKeyIndex9 = CAPK_FILLER;
				capkFile.assign(EST_current.szCAPKFile9);
				memset(EST_current.szCAPKFile9, 0, EMV_PK_FILE_NAME);
				position = 9;
			}
			
			dlog_msg("CAPK10 - index: %02X", EST_current.inPublicKeyIndex10);
			if(EST_current.inPublicKeyIndex10 == index || position == 10)
			{
				EST_current.inPublicKeyIndex10 = CAPK_FILLER;
				capkFile.assign(EST_current.szCAPKFile10);
				memset(EST_current.szCAPKFile10, 0, EMV_PK_FILE_NAME);
				position = 10;
			}
			
			dlog_msg("CAPK11 - index: %02X", EST_current.inPublicKeyIndex11);
			if(EST_current.inPublicKeyIndex11 == index || position == 11)
			{
				EST_current.inPublicKeyIndex11 = CAPK_FILLER;
				capkFile.assign(EST_current.szCAPKFile11);
				memset(EST_current.szCAPKFile11, 0, EMV_PK_FILE_NAME);
				position = 11;
			}
			
			dlog_msg("CAPK12 - index: %02X", EST_current.inPublicKeyIndex12);
			if(EST_current.inPublicKeyIndex12 == index || position == 12)
			{
				EST_current.inPublicKeyIndex12 = CAPK_FILLER;
				capkFile.assign(EST_current.szCAPKFile12);
				memset(EST_current.szCAPKFile12, 0, EMV_PK_FILE_NAME);
				position = 12;
			}
			
			dlog_msg("CAPK13 - index: %02X", EST_current.inPublicKeyIndex13);
			if(EST_current.inPublicKeyIndex13 == index || position == 13)
			{
				EST_current.inPublicKeyIndex13 = CAPK_FILLER;
				capkFile.assign(EST_current.szCAPKFile13);
				memset(EST_current.szCAPKFile13, 0, EMV_PK_FILE_NAME);
				position = 13;
			}
			
			dlog_msg("CAPK14 - index: %02X", EST_current.inPublicKeyIndex14);
			if(EST_current.inPublicKeyIndex14 == index || position == 14)
			{
				EST_current.inPublicKeyIndex14 = CAPK_FILLER;
				capkFile.assign(EST_current.szCAPKFile14);
				memset(EST_current.szCAPKFile14, 0, EMV_PK_FILE_NAME);
				position = 14;
			}
			
			dlog_msg("CAPK15 - index: %02X", EST_current.inPublicKeyIndex15);
			if(EST_current.inPublicKeyIndex15 == index || position == 15)
			{
				EST_current.inPublicKeyIndex15 = CAPK_FILLER;
				capkFile.assign(EST_current.szCAPKFile15);
				memset(EST_current.szCAPKFile15, 0, EMV_PK_FILE_NAME);
				position = 15;
			}
			if (position > 0)
			{
				_remove(capkFile.c_str());
				dlog_msg("Successfully removed CAPK at position %d file %s", position, capkFile.c_str());
				commitChanges(true);
				result = 1;
			}
			else dlog_error("CAPK doesn't exist");

		}
	}
	
	return result;
}


int cUpdateCfg::updateCAPK(short index, char * filename, int position, char * expiry)
{
	int iRet = -1;
	int empty_record = -1;
	

	//we check all CAPK in current EST record

	dlog_msg("Updating CAPK index %X for %s", index, g_CardAppConfig.GetCurrentAID().c_str());
	if (expiry) dlog_msg("Expiry %s", expiry);
	if (filename) dlog_msg("Filename %s", filename);
	dlog_msg("Index %d, position %d", index, position);

	
	dlog_msg("CAPK1 - index: %02X", EST_current.inPublicKeyIndex1);
	if(EST_current.inPublicKeyIndex1 == index || position == 1)
	{
		if(filename != NULL)
		{
			strncpy(EST_current.szCAPKFile1, filename, EMV_PK_FILE_NAME);
			EST_current.inPublicKeyIndex1 = index;
			strncpy(EST_current.szCAPKExpDate1, expiry, EMV_PK_EXPDATE_SIZE);
		}
		return 1;
	}
	
	dlog_msg("CAPK2 - index: %02X", EST_current.inPublicKeyIndex2);
	if(EST_current.inPublicKeyIndex2 == index || position == 2)
	{
		
		if(filename != NULL)
		{
			strncpy(EST_current.szCAPKFile2, filename, EMV_PK_FILE_NAME);
			EST_current.inPublicKeyIndex2 = index;
			strncpy(EST_current.szCAPKExpDate2, expiry, EMV_PK_EXPDATE_SIZE);
		}
		return 2;
	}
	
	dlog_msg("CAPK3 - index: %02X", EST_current.inPublicKeyIndex3);
	if(EST_current.inPublicKeyIndex3 == index || position == 3)
	{
		
		if(filename != NULL)
		{
			strncpy(EST_current.szCAPKFile3, filename, EMV_PK_FILE_NAME);
			EST_current.inPublicKeyIndex3 = index;
			strncpy(EST_current.szCAPKExpDate3, expiry, EMV_PK_EXPDATE_SIZE);
		}
		return 3;
	}
	
	dlog_msg("CAPK4 - index: %02X", EST_current.inPublicKeyIndex4);
	if(EST_current.inPublicKeyIndex4 == index || position == 4)
	{
		
		if(filename != NULL)
		{
			strncpy(EST_current.szCAPKFile4, filename, EMV_PK_FILE_NAME);
			EST_current.inPublicKeyIndex4 = index;
			strncpy(EST_current.szCAPKExpDate4, expiry, EMV_PK_EXPDATE_SIZE);
		}
		return 4;
	}
	
	dlog_msg("CAPK5 - index: %02X", EST_current.inPublicKeyIndex5);
	if(EST_current.inPublicKeyIndex5 == index || position == 5)
	{
		
		if(filename != NULL)
		{
			strncpy(EST_current.szCAPKFile5, filename, EMV_PK_FILE_NAME);
			EST_current.inPublicKeyIndex5 = index;
			strncpy(EST_current.szCAPKExpDate5, expiry, EMV_PK_EXPDATE_SIZE);
		}
		return 5;
	}

	
	dlog_msg("CAPK6 - index: %02X", EST_current.inPublicKeyIndex6);
	if(EST_current.inPublicKeyIndex6 == index || position == 6)
	{
		
		if(filename != NULL)
		{
			strncpy(EST_current.szCAPKFile6, filename, EMV_PK_FILE_NAME);
			EST_current.inPublicKeyIndex6 = index;
			strncpy(EST_current.szCAPKExpDate6, expiry, EMV_PK_EXPDATE_SIZE);
		}
		return 6;
	}

	
	dlog_msg("CAPK7 - index: %02X", EST_current.inPublicKeyIndex7);
	if(EST_current.inPublicKeyIndex7 == index || position == 7)
	{
		
		if(filename != NULL)
		{
			strncpy(EST_current.szCAPKFile7, filename, EMV_PK_FILE_NAME);
			EST_current.inPublicKeyIndex7 = index;
			strncpy(EST_current.szCAPKExpDate7, expiry, EMV_PK_EXPDATE_SIZE);
		}
		return 7;
	}

	
	dlog_msg("CAPK8 - index: %02X", EST_current.inPublicKeyIndex8);
	if(EST_current.inPublicKeyIndex8 == index || position == 8)
	{
		
		if(filename != NULL)
		{
			strncpy(EST_current.szCAPKFile8, filename, EMV_PK_FILE_NAME);
			EST_current.inPublicKeyIndex8 = index;
			strncpy(EST_current.szCAPKExpDate8, expiry, EMV_PK_EXPDATE_SIZE);
		}
		return 8;
	}

	
	dlog_msg("CAPK9 - index: %02X", EST_current.inPublicKeyIndex9);
	if(EST_current.inPublicKeyIndex9 == index || position == 9)
	{
		
		if(filename != NULL)
		{
			strncpy(EST_current.szCAPKFile9, filename, EMV_PK_FILE_NAME);
			EST_current.inPublicKeyIndex9 = index;
			strncpy(EST_current.szCAPKExpDate9, expiry, EMV_PK_EXPDATE_SIZE);
		}
		return 9;
	}

	
	dlog_msg("CAPK10 - index: %02X", EST_current.inPublicKeyIndex10);
	if(EST_current.inPublicKeyIndex10 == index || position == 10)
	{
		
		if(filename != NULL)
		{
			strncpy(EST_current.szCAPKFile10, filename, EMV_PK_FILE_NAME);
			EST_current.inPublicKeyIndex10 = index;
			strncpy(EST_current.szCAPKExpDate10, expiry, EMV_PK_EXPDATE_SIZE);
		}
		return 10;
	}

	
	dlog_msg("CAPK11 - index: %02X", EST_current.inPublicKeyIndex11);
	if(EST_current.inPublicKeyIndex11 == index || position == 11)
	{
		
		if(filename != NULL)
		{
			strncpy(EST_current.szCAPKFile11, filename, EMV_PK_FILE_NAME);
			EST_current.inPublicKeyIndex11 = index;
			strncpy(EST_current.szCAPKExpDate11, expiry, EMV_PK_EXPDATE_SIZE);
		}
		return 11;
	}

	
	dlog_msg("CAPK12 - index: %02X", EST_current.inPublicKeyIndex12);
	if(EST_current.inPublicKeyIndex12 == index || position == 12)
	{
		
		if(filename != NULL)
		{
			strncpy(EST_current.szCAPKFile12, filename, EMV_PK_FILE_NAME);
			EST_current.inPublicKeyIndex12 = index;
			strncpy(EST_current.szCAPKExpDate12, expiry, EMV_PK_EXPDATE_SIZE);
		}
		return 12;
	}

	
	dlog_msg("CAPK13 - index: %02X", EST_current.inPublicKeyIndex13);
	if(EST_current.inPublicKeyIndex13 == index || position == 13)
	{
		
		if(filename != NULL)
		{
			strncpy(EST_current.szCAPKFile13, filename, EMV_PK_FILE_NAME);
			EST_current.inPublicKeyIndex13 = index;
			strncpy(EST_current.szCAPKExpDate13, expiry, EMV_PK_EXPDATE_SIZE);
		}
		return 13;
	}

	
	dlog_msg("CAPK14 - index: %02X", EST_current.inPublicKeyIndex14);
	if(EST_current.inPublicKeyIndex14 == index || position == 14)
	{
		
		if(filename != NULL)
		{
			strncpy(EST_current.szCAPKFile14, filename, EMV_PK_FILE_NAME);
			EST_current.inPublicKeyIndex14 = index;
			strncpy(EST_current.szCAPKExpDate14, expiry, EMV_PK_EXPDATE_SIZE);
		}
		return 14;
	}

	
	dlog_msg("CAPK15 - index: %02X", EST_current.inPublicKeyIndex15);
	if(EST_current.inPublicKeyIndex15 == index || position == 15)
	{
		
		if(filename != NULL)
		{
			strncpy(EST_current.szCAPKFile15, filename, EMV_PK_FILE_NAME);
			EST_current.inPublicKeyIndex15 = index;
			strncpy(EST_current.szCAPKExpDate15, expiry, EMV_PK_EXPDATE_SIZE);
		}
		return 15;
	}

	//no matching record, trying to locate empty key slot
	dlog_msg("No matching record for index %X", index);
	if(filename != NULL)
	{
		//if((empty_record = updateCAPK(0xFF, NULL, -1, NULL)) == -1)
		if((empty_record = updateCAPK(CAPK_FILLER, NULL, -1, NULL)) == -1)
			empty_record = updateCAPK(0x100, NULL, -1, NULL);

		if(empty_record >= 1 && empty_record <= 15)
		{
			dlog_msg("Adding new key on position %d", empty_record);
			updateCAPK(index, filename, empty_record, expiry);
		}
		else
		{
			dlog_msg("ERROR - no empty slot available");
		}
	}

	return iRet;
	
}


int cUpdateCfg::updateCurrentAID(char *AID, char *ver1, char *ver2, short partial_flag, char *name)

{
	int iRet = 1;
	switch(AID_index)
	{
		case 1:
			if(AID != NULL)
				strcpy(EST_current.szSupportedAID1, AID);
			if(ver1 != NULL)
				strcpy(EST_current.szTermAVN1, ver1);
			if(ver2 != NULL)
				strcpy(EST_current.szSecondTermAVN1, ver2);
			if(partial_flag != 0)
				EST_current.inPartialNameAllowedFlag1 = partial_flag;
			if(name != NULL)
				strcpy(EST_current.szRecommenedAppNameAID1, name);
			break;
		case 2:
			if(AID != NULL)
				strcpy(EST_current.szSupportedAID2, AID);
			if(ver1 != NULL)
				strcpy(EST_current.szTermAVN2, ver1);
			if(ver2 != NULL)
				strcpy(EST_current.szSecondTermAVN2, ver2);
			if(partial_flag != 0)
				EST_current.inPartialNameAllowedFlag2 = partial_flag;
			if(name != NULL)
				strcpy(EST_current.szRecommenedAppNameAID2, name);
			break;
		case 3:
			if(AID != NULL)
				strcpy(EST_current.szSupportedAID3, AID);
			if(ver1 != NULL)
				strcpy(EST_current.szTermAVN3, ver1);
			if(ver2 != NULL)
				strcpy(EST_current.szSecondTermAVN3, ver2);
			if(partial_flag != 0)
				EST_current.inPartialNameAllowedFlag3 = partial_flag;
			if(name != NULL)
				strcpy(EST_current.szRecommenedAppNameAID3, name);
			break;
		case 4:
			if(AID != NULL)
				strcpy(EST_current.szSupportedAID4, AID);
			if(ver1 != NULL)
				strcpy(EST_current.szTermAVN4, ver1);
			if(ver2 != NULL)
				strcpy(EST_current.szSecondTermAVN4, ver2);
			if(partial_flag != 0)
				EST_current.inPartialNameAllowedFlag4 = partial_flag;
			if(name != NULL)
				strcpy(EST_current.szRecommenedAppNameAID4, name);
			break;
		case 5:
			if(AID != NULL)
				strcpy(EST_current.szSupportedAID5, AID);
			if(ver1 != NULL)
				strcpy(EST_current.szTermAVN5, ver1);
			if(ver2 != NULL)
				strcpy(EST_current.szSecondTermAVN5, ver2);
			if(partial_flag != 0)
				EST_current.inPartialNameAllowedFlag5 = partial_flag;
			if(name != NULL)
				strcpy(EST_current.szRecommenedAppNameAID5, name);
			break;
		case 6:
			if(AID != NULL)
				strcpy(EST_current.szSupportedAID6, AID);
			if(ver1 != NULL)
				strcpy(EST_current.szTermAVN6, ver1);
			if(ver2 != NULL)
				strcpy(EST_current.szSecondTermAVN6, ver2);
			if(partial_flag != 0)
				EST_current.inPartialNameAllowedFlag6 = partial_flag;
			if(name != NULL)
				strcpy(EST_current.szRecommenedAppNameAID6, name);
			break;
		case 7:
			if(AID != NULL)
				strcpy(EST_current.szSupportedAID7, AID);
			if(ver1 != NULL)
				strcpy(EST_current.szTermAVN7, ver1);
			if(ver2 != NULL)
				strcpy(EST_current.szSecondTermAVN7, ver2);
			if(partial_flag != 0)
				EST_current.inPartialNameAllowedFlag7 = partial_flag;
			if(name != NULL)
				strcpy(EST_current.szRecommenedAppNameAID7, name);
			break;
		case 8:
			if(AID != NULL)
				strcpy(EST_current.szSupportedAID8, AID);
			if(ver1 != NULL)
				strcpy(EST_current.szTermAVN8, ver1);
			if(ver2 != NULL)
				strcpy(EST_current.szSecondTermAVN8, ver2);
			if(partial_flag != 0)
				EST_current.inPartialNameAllowedFlag8 = partial_flag;
			if(name != NULL)
				strcpy(EST_current.szRecommenedAppNameAID8, name);
			break;
		case 9:
			if(AID != NULL)
				strcpy(EST_current.szSupportedAID9, AID);
			if(ver1 != NULL)
				strcpy(EST_current.szTermAVN9, ver1);
			if(ver2 != NULL)
				strcpy(EST_current.szSecondTermAVN9, ver2);
			if(partial_flag != 0)
				EST_current.inPartialNameAllowedFlag9 = partial_flag;
			if(name != NULL)
				strcpy(EST_current.szRecommenedAppNameAID9, name);
			break;
		case 10:
			if(AID != NULL)
				strcpy(EST_current.szSupportedAID10, AID);
			if(ver1 != NULL)
				strcpy(EST_current.szTermAVN10, ver1);
			if(ver2 != NULL)
				strcpy(EST_current.szSecondTermAVN10, ver2);
			if(partial_flag != 0)
				EST_current.inPartialNameAllowedFlag10 = partial_flag;
			if(name != NULL)
				strcpy(EST_current.szRecommenedAppNameAID10, name);
			break;

		default:
			iRet = 0;
	}
	return iRet;
}


int cUpdateCfg::updateConfig(int tagNum, unsigned char * value, int Len)
{
	using namespace com_verifone_TLVLite;
	int iRet = 0;
	long lVal;
	char value_chr[256];
	dlog_msg("updateConfigTLV: tag: %X, len: %d", tagNum, Len);
	// dlog_hex(value, Len, "VALUE");

	switch(tagNum)
	{
			case TAG_9F1B_TERM_FLOOR_LIMIT:
				{
					ValueConverter<long> val(ConstData(value, Len));
					MVT_current.lnEMVFloorLimit = val.getValue( 0 );
					// MVT_current.lnEMVFloorLimit = (long)*value;
				}
				break;
			case TAG_DFAF14_RND_SEL_THRESHOLD:
				{
					ValueConverter<long> val(ConstData(value, Len));
					MVT_current.lnEMVRSThreshold = val.getValue( 0 );
					// MVT_current.lnEMVRSThreshold = (long)*value;
				}
				break;
			case TAG_DFAF15_RND_SEL_PERCENTAGE:
				{
					ValueConverter<int> val(ConstData(value, Len));
					MVT_current.inEMVTargetRSPercent = val.getValue( 0 );
					// MVT_current.inEMVTargetRSPercent = (int)*value;
				}
				break;
			case TAG_DFAF16_RND_MAX_RND_PERCENTAGE:
				{
					ValueConverter<int> val(ConstData(value, Len));
					MVT_current.inEMVMaxTargetRSPercent = val.getValue( 0 );
					// MVT_current.inEMVMaxTargetRSPercent = (int)*value;
				}
				break;
			case TAG_DFAF17_TAC_DEFAULT:
			case 0xDFDF06:
				if (Len*2 == EMV_TAC_SIZE-1)
					SVC_HEX_2_DSP ((const char *)value, reinterpret_cast<char *>(MVT_current.szEMVTACDefault), Len);
				break;
			case TAG_DFAF18_TAC_DENIAL:
			case 0xDFDF07:
				if (Len*2 == EMV_TAC_SIZE-1)
					SVC_HEX_2_DSP ((const char *)value, reinterpret_cast<char *>(MVT_current.szEMVTACDenial), Len);
				break;
			case TAG_DFAF19_TAC_ONLINE:
			case 0xDFDF08:
				if (Len*2 == EMV_TAC_SIZE-1)
					SVC_HEX_2_DSP ((const char *)value, reinterpret_cast<char *>(MVT_current.szEMVTACOnline), Len);
				break;
			case TAG_DFAF1A_DEFAULT_TDOL:
				if (Len*2 < EMV_MAX_TDOL_SIZE)
				{
					memset(MVT_current.szDefaultTDOL, 0, EMV_MAX_TDOL_SIZE);
					SVC_HEX_2_DSP ((const char *)value, reinterpret_cast<char *>(MVT_current.szDefaultTDOL), Len);
					dlog_msg("default TDOL updated to %s", MVT_current.szDefaultTDOL);
				}
				break;
			case TAG_DFAF1B_DEFAULT_DDOL:
				if (Len*2 < EMV_MAX_DDOL_SIZE)
				{
					memset(MVT_current.szDefaultDDOL, 0, EMV_MAX_DDOL_SIZE);
					SVC_HEX_2_DSP ((const char *)value, reinterpret_cast<char *>(MVT_current.szDefaultDDOL), Len);
					dlog_msg("default DDOL updated to %s", MVT_current.szDefaultDDOL);
				}
				break;
			case TAG_9F1A_TERM_COUNTY_CODE:
				if (Len*2 == EMV_COUNTRY_CODE_SIZE-1)
					SVC_HEX_2_DSP ((const char *)value, reinterpret_cast<char *>(MVT_current.szEMVTermCountryCode), EMV_COUNTRY_CODE_SIZE/2);
				break;
			case TAG_5F2A_TRANS_CURCY_CODE:
				if (Len*2 == EMV_CURRENCY_CODE_SIZE-1)
					SVC_HEX_2_DSP ((const char *)value, reinterpret_cast<char *>(MVT_current.szEMVTermCurrencyCode), EMV_CURRENCY_CODE_SIZE/2);
				break;
			case TAG_5F36_TRANS_CURR_EXP:
				{
					ValueConverter<short> val(ConstData(value, Len));
					MVT_current.inEMVTermCurExp = val.getValue( 0 );
					// MVT_current.inEMVTermCurExp = (short)*value;
				}
				break;
			case TAG_9F40_TERM_ADTNAL_CAP:
				if (Len*2 == EMV_ADD_TERM_CAPABILITIES_SIZE-1)
					SVC_HEX_2_DSP ((const char *)value, reinterpret_cast<char *>(MVT_current.szEMVTermAddCapabilities), EMV_ADD_TERM_CAPABILITIES_SIZE/2);
				break;
			case TAG_9F33_TERM_CAP:
				if (Len*2 == EMV_TERM_CAPABILITIES_SIZE-1)
					SVC_HEX_2_DSP ((const char *)value, reinterpret_cast<char *>(MVT_current.szEMVTermCapabilities), EMV_TERM_CAPABILITIES_SIZE/2);
				break;
			case TAG_9F35_TERM_TYPE:
				if (Len*2 == EMV_TERM_TYPE_SIZE-1)
					SVC_HEX_2_DSP ((const char *)value, reinterpret_cast<char *>(MVT_current.szEMVTermType), EMV_TERM_TYPE_SIZE/2);
				break;
			case TAG_9F15_MER_CAT_CODE:
				if (Len*2 == EMV_MERCH_CAT_CODE_SIZE-1)
					SVC_HEX_2_DSP ((const char *)value, reinterpret_cast<char *>(MVT_current.szEMVMerchantCategoryCode), EMV_MERCH_CAT_CODE_SIZE/2);
				break;
			case TAG_DFAF1E_MCC:
				if (Len*2 < EMV_TERM_CAT_CODE_SIZE)
				{
					memset(MVT_current.szEMVTerminalCategoryCode, 0, EMV_TERM_CAT_CODE_SIZE);
					SVC_HEX_2_DSP ((const char *)value, reinterpret_cast<char *>(MVT_current.szEMVTerminalCategoryCode), EMV_TERM_CAT_CODE_SIZE/2);
				}
				break;
			case TAG_9F09_TERM_VER_NUM:
				if (Len*2 == EMV_AVN_SIZE-1)
				{
					memset(value_chr, 0, sizeof(value_chr));
					SVC_HEX_2_DSP ((const char *)value, reinterpret_cast<char *>(value_chr), EMV_AVN_SIZE/2);
					updateCurrentAID(NULL, value_chr, NULL, 0, NULL);
				}
				break;
			case TAG_DFAF12_SECOND_AVN:
				if (Len*2 == EMV_AVN_SIZE-1)
				{
					memset(value_chr, 0, sizeof(value_chr));
					SVC_HEX_2_DSP ((const char *)value, reinterpret_cast<char *>(value_chr), EMV_AVN_SIZE/2);
					updateCurrentAID(NULL, NULL, value_chr, 0, NULL);
				}
				break;

			case TAG_DFAF11_PARTIAL_SELECTION:
				if (Len > 0 && Len <= 2)
				{
					short partial_flag = 1;
					if(Len == 2)
					{
						ValueConverter<short>val(ConstData(value, Len));
						partial_flag = val.getValue(1);
						//memcpy(reinterpret_cast<char *>(partial_flag), value, Len);
					}
					else if (Len == 1)
					{
						partial_flag = *value;
					}
					// partial_flag is 1 for NOT allowed and 2 for allowed
					++partial_flag;
					updateCurrentAID(NULL, NULL, NULL, partial_flag, NULL);
				}
				break;

			case TAG_DFAF13_REC_APP_NAME:
				if (Len*2 < MAX_APPNAME_LEN)
				{
					memset(value_chr, 0, sizeof(value_chr));
					SVC_HEX_2_DSP ((const char *)value, reinterpret_cast<char *>(value_chr), Len);
					updateCurrentAID(NULL, NULL, NULL, 0, value_chr);
				}
				break;
			//case TAG_8F_AUTH_PUBKEY_INDEX:
			case TAG_9F22_CERT_PUBKEY_IND:
				{
					ValueConverter<short> val(ConstData(value, Len));
					CAPK_index = val.getValue( 0 );
					//CAPK_index = (short)*value;
					buildCAPK();
				}
				break;
			case TAG_DFCC20_CAPK_FILE:
				updateCAPK(CAPK_index, reinterpret_cast<char *>(value), -1, 0);
				break;
			case TAG_DFAF0E_CAPK_MODULUS:
				setCAPKmodulus((char *)value, Len);
				buildCAPK();
				break;
			case TAG_DFAF0F_CAPK_EXPONENT:
				setCAPKexponent((char *)value, Len);
				buildCAPK();
				break;
			case TAG_DFAF10_CAPK_HASH:
				setCAPKhash((char *)value, Len);
				buildCAPK();
				break;
			case TAG_DFAF32_CAPK_EXPIRY:
				setCAPKexpiry((char *)value, Len);
				buildCAPK();
				break;
			case 0xDFDF0C: // ROS Max Target
				if (Len == 4)
				{
					ValueConverter<long> val(ConstData(value, Len));
					MVT_current.inEMVMaxTargetRSPercent = static_cast<short>(val.getValue( 0 ));
				}
				else if (Len == 2)
				{
					ValueConverter<short> val(ConstData(value, Len));
					MVT_current.inEMVMaxTargetRSPercent = val.getValue( 0 );
				}
				break;
			case 0xDFDF0B: // Target RS Percent
				if (Len == 4)
				{
					ValueConverter<long> val(ConstData(value, Len));
					MVT_current.inEMVTargetRSPercent = static_cast<short>(val.getValue( 0 ));
				}
				else
				{
					ValueConverter<short> val(ConstData(value, Len));
					MVT_current.inEMVTargetRSPercent = val.getValue( 0 );
				}
				break;
			case 0xDFDF0A: // ROS Threshold
				{
					ValueConverter<long> val(ConstData(value, Len));
					MVT_current.lnEMVRSThreshold = val.getValue( 0 );
				}
				break;

			default:
				dlog_msg("object not recognised %X ignoring...", tagNum);
				break;
				
	
	}
    
	

	return iRet;

}


int updateConfigTLV(int tagNum, unsigned char * value, int Len)
{

	dlog_msg("updateConfigTLV: tag: %X, len: %d", tagNum, Len);
	
    Ushort Ret = 0;
    srTxnResult TVRTSI;

	char Key[sizeof(int)*2+1];
	char Section[MAX_AID_LEN*2+1];
	char *Value = new char[Len*2+1];

	if(g_CardAppConfig.GetCurrentAID().size() > 0)
	{
		strcpy(Section, g_CardAppConfig.GetCurrentAID().c_str());
	}
	else
	{
		strcpy(Section, EMV_CONFIG_GLOBAL);
	}


	sprintf(Key,"%X", tagNum);
	SVC_HEX_2_DSP((const char *)value, Value, Len);
	Value[Len*2] = 0;

	Ret = ini_puts(Section, Key, Value, EMV_CONFIG_FILE);
	dlog_msg("Section: %s Key: %s Value: %s ret: %d", Section, Key, Value, Ret);
	
	delete [] Value;

	

	return Ret;

}



int checkPrimitiveTLV(int tagNum, unsigned char * value, int Len)
{

	dlog_msg("checkPrimitiveTLV: tag: %X, len: %d", tagNum, Len);
	
    Ushort Ret = 0;
    srTxnResult TVRTSI;


	if(tagNum == TAG_9500_TVR)
	{
		memcpy(TVRTSI.stTVR, value, TVR_SIZE);
		Ret = usEMVSetTxnStatus(&TVRTSI);
	}
	else if(tagNum == TAG_9B00_TSI)
	{
		memcpy(TVRTSI.stTSI, value, TSI_SIZE);
		Ret = usEMVSetTxnStatus(&TVRTSI);
	}
	else if(tagNum == TAG_9F02_AMT_AUTH_NUM)
	{
		//we must add binary representation of numeric value
		char amtDsp[NUMERIC_AMT_SIZE*2 + 1];
		long amtBin;
		SVC_HEX_2_DSP((const char *)value, amtDsp, NUMERIC_AMT_SIZE);
		amtDsp[NUMERIC_AMT_SIZE*2] = 0;
		amtBin = atol(amtDsp);
		usEMVUpdateTLVInCollxn(TAG_81_AMOUNT_AUTH, (byte *)&amtBin, BINARY_AMT_SIZE);
		dlog_msg("TAG %X added with value: %X", TAG_81_AMOUNT_AUTH, amtBin);
		Ret = usEMVUpdateTLVInCollxn(tagNum, value, Len);
	}
	else if(tagNum == TAG_9F03_AMT_OTHER_NUM)
	{
		//we must add binary representation of numeric value
		char amtDsp[NUMERIC_AMT_SIZE*2 + 1];
		long amtBin;
		SVC_HEX_2_DSP((const char *)value, amtDsp, NUMERIC_AMT_SIZE);
		amtDsp[NUMERIC_AMT_SIZE*2] = 0;
		amtBin = atol(amtDsp);
		usEMVUpdateTLVInCollxn(TAG_9F04_AMT_OTHER_BIN, (byte *)&amtBin, BINARY_AMT_SIZE);
		dlog_msg("TAG %X added with value: %X", TAG_9F04_AMT_OTHER_BIN, amtBin);
		Ret = usEMVUpdateTLVInCollxn(tagNum, value, Len);
	}
	else if(tagNum == TAG_9F1B_TERM_FLOOR_LIMIT)
	{
		// We must set floor limit using SetROSParams() as well
		char floor[sizeof(long)*2 + 1];
		long floorBin;
		//assert(Len == sizeof(floorBin));
		SVC_HEX_2_DSP((const char *)value, floor, Len);
		floor[Len*2] = 0;
		//thresholdBin = atol(floor);
		sscanf(floor, "%x", &floorBin);
		inVXEMVAPSetROSParams(floorBin, RS_THRESHOLD, RS_TARGET_PERCENT, RS_MAX_PERCENT);
		dlog_msg("Updated floor limit with value: %d", floorBin);
		if (Len < 4) // Under EMV rules, Floor limit has to be 4 bytes long
		{
			memset(floor, 0, sizeof(floor));
			memcpy(floor + (4-Len), value, Len);
			Ret = usEMVUpdateTLVInCollxn(tagNum, floor, 4);
		}
		else
		{
			Ret = usEMVUpdateTLVInCollxn(tagNum, value, Len);
		}
		// Ret = usEMVUpdateTLVInCollxn(tagNum, value, Len);
	}
	else if((tagNum == 0xDFDF06) || (tagNum == TAG_DFAF17_TAC_DEFAULT)) 	//TAC default
	{
		Len = TVR_SIZE;
		memcpy(TACDEFAULT, value, TVR_SIZE);
		dlog_msg("TAC default updated!");
	}
	else if((tagNum == 0xDFDF07) || (tagNum == TAG_DFAF18_TAC_DENIAL)) 	//TAC denial
	{
		Len = TVR_SIZE;
		memcpy(TACDENIAL, value, TVR_SIZE);
		dlog_msg("TAC denial updated!");
	}
	else if((tagNum == 0xDFDF08) || (tagNum == TAG_DFAF19_TAC_ONLINE)) 	//TAC online
	{
		Len = TVR_SIZE;
		memcpy(TACONLINE, value, TVR_SIZE);
		dlog_msg("TAC online updated!");
	}
	else if(tagNum == TAG_DFAF1A_DEFAULT_TDOL) 	//default TDOL
	{
		if(Len < MAX_DEF_TDOL)
		{
			memcpy(DEFAULT_TDOL, value, Len);
			dlog_msg("default TDOL online updated!");
		}
	}
	else if(tagNum == TAG_DFAF1B_DEFAULT_DDOL) 	//default DDOL
	{
		if(Len < MAX_DEF_DDOL)
		{
			memcpy(DEFAULT_DDOL, value, Len);
			dlog_msg("default DDOL updated!");
		}
	}
	else if(tagNum == 0xDFDF0D) 	//force on line
	{
		Len = TVR_SIZE;
		usEMVSetForceOnlineBit();
		dlog_msg("Transaction forced online!");
	}
	else if((tagNum == 0xDFDF0A) || (tagNum == TAG_DFAF14_RND_SEL_THRESHOLD))	//ROS - Threshold
	{
		char threshold[sizeof(long)*2 + 1];
		long thresholdBin;
		//assert(Len == sizeof(thresholdBin));
		SVC_HEX_2_DSP((const char *)value, threshold, Len);
		threshold[Len*2] = 0;
		//thresholdBin = atol(threshold);
		sscanf(threshold, "%x", &thresholdBin);
		inVXEMVAPSetROSParams(FLOOR_LIMIT, thresholdBin, RS_TARGET_PERCENT, RS_MAX_PERCENT);
		dlog_msg("Set ROS Threshold to %ld (size %d)", thresholdBin, Len);
	}
	else if((tagNum == 0xDFDF0B) || (tagNum == TAG_DFAF15_RND_SEL_PERCENTAGE))	//ROS - Target RS Percent
	{
		char rsTarget[sizeof(long)*2 + 1];
		int rsTargetBin;
		//assert(Len == sizeof(rsTargetBin));
		SVC_HEX_2_DSP((const char *)value, rsTarget, Len);
		rsTarget[Len*2] = 0;
		//rsTargetBin = atol(rsTarget);
		sscanf(rsTarget, "%x", &rsTargetBin);
		inVXEMVAPSetROSParams(FLOOR_LIMIT, RS_THRESHOLD, rsTargetBin, RS_MAX_PERCENT);
		dlog_msg("Set RS Target Percent to %d (size %d)", rsTargetBin, Len);
	}
	else if((tagNum == 0xDFDF0C) || (tagNum == TAG_DFAF16_RND_MAX_RND_PERCENTAGE)) 	//ROS - Max RS Percent
	{
		char rsMaxPercent[sizeof(long)*2 + 1];
		int rsMaxPercentBin;
		//assert(Len == sizeof(rsMaxPercent));
		SVC_HEX_2_DSP((const char *)value, rsMaxPercent, Len);
		rsMaxPercent[Len*2] = 0;
		//rsMaxPercentBin = atol(rsMaxPercent);
		sscanf(rsMaxPercent, "%x", &rsMaxPercentBin);
		inVXEMVAPSetROSParams(FLOOR_LIMIT, RS_THRESHOLD, RS_TARGET_PERCENT, rsMaxPercentBin);
		dlog_msg("Set Max RS Target Percent to %d (size %d)", rsMaxPercentBin, Len);
	}
	else if(tagNum == TAG_DFAF12_SECOND_AVN) 	//second AVN
	{
		if(Len <= MAX_AVN_LEN)
		{
			g_CardAppConfig.SetSecondAVN(value);
			dlog_msg("Second AVN set: %X%X", value[0], value[1]);
		}
	}
	else if(tagNum == TAG_DFAF1D_SELECTION) 	//automatic selection
	{
		g_CardAppConfig.SetAutoFlag(value[0]);
		dlog_msg("Automatic Selection set: %X", value[0]);
	}
	else
	{
		Ret = usEMVUpdateTLVInCollxn(tagNum, value, Len);
	}

	return Ret;

}


int getTLV(int tagNum, unsigned char *tempTLV, unsigned short *TLVlen)
{

	dlog_msg("Processing TAG: %X", tagNum);
    srTxnResult TVRTSI;
	unsigned short tmpLen = 0;

    if(tagNum == TAG_9500_TVR)
    {
		usEMVGetTxnStatus(&TVRTSI);
        memcpy(tempTLV, TVRTSI.stTVR, TVR_SIZE);
        dlog_msg("processing TVR: %02X%02X%02X%02X%02X", tempTLV[0],tempTLV[1],tempTLV[2],tempTLV[3],tempTLV[4]);
        tmpLen = TVR_SIZE;
    }
        else if(tagNum == TAG_9B00_TSI)
        {
		usEMVGetTxnStatus(&TVRTSI);
            memcpy(tempTLV, TVRTSI.stTSI, TSI_SIZE);
            dlog_msg("processing TSI: %02X%02X", tempTLV[0],tempTLV[1]);
            tmpLen = TSI_SIZE;
        }
        else if((tagNum == 0xDFDF06) || (tagNum == TAG_DFAF17_TAC_DEFAULT))     //TAC default
        {
            tmpLen = TVR_SIZE;
            memcpy(tempTLV, TACDEFAULT, TVR_SIZE);
            dlog_msg("adding TAC default");
        }
        else if((tagNum == 0xDFDF07) || (tagNum == TAG_DFAF18_TAC_DENIAL))     //TAC denial
        {
            tmpLen = TVR_SIZE;
            memcpy(tempTLV, TACDENIAL, TVR_SIZE);
            dlog_msg("adding TAC denial");
        }
        else if((tagNum == 0xDFDF08) || (tagNum == TAG_DFAF19_TAC_ONLINE))     //TAC online
        {
            tmpLen = TVR_SIZE;
            memcpy(tempTLV, TACONLINE, TVR_SIZE);
            dlog_msg("adding TAC online");
        }
        else if(tagNum == 0xDFDF09)     //Kernel - Last SW1SW2 from the card
        {
            tmpLen = 0;
            usEMVGetKernelTags(TAG_DF09_LAST_STATUS_WORD, tempTLV, &tmpLen);
            if (!tmpLen)
            {
                usEMVGetTLVFromColxn(TAG_DF09_LAST_STATUS_WORD, tempTLV, &tmpLen);
                if (tmpLen!= 2) tmpLen= 0; // Ignore the value if length != 2 -> means that it's for sure not SW1SW2
            }
        }
        else if((tagNum == 0xDFDF0A) || (tagNum == TAG_DFAF14_RND_SEL_THRESHOLD))     //ROS - Threshold
        {
            tmpLen = sizeof(RS_THRESHOLD);
            memcpy(tempTLV, &RS_THRESHOLD, sizeof(RS_THRESHOLD));
            dlog_msg("Adding ROS Threshold (%d)", RS_THRESHOLD);
        }
        else if((tagNum == 0xDFDF0B) || (tagNum == TAG_DFAF15_RND_SEL_PERCENTAGE))     //ROS - Target RS Percent
        {
            tmpLen = sizeof(RS_TARGET_PERCENT);
            memcpy(tempTLV, &RS_TARGET_PERCENT, sizeof(RS_TARGET_PERCENT));
            dlog_msg("Adding RS Percent (%d)", RS_TARGET_PERCENT);
        }
        else if((tagNum == 0xDFDF0C) || (tagNum == TAG_DFAF16_RND_MAX_RND_PERCENTAGE))    //ROS - Max RS Percent
        {
            tmpLen = sizeof(RS_MAX_PERCENT);
            memcpy(tempTLV, &RS_MAX_PERCENT, sizeof(RS_MAX_PERCENT));
            dlog_msg("Adding RS Max Percent (%d)", RS_MAX_PERCENT);
        }
        else if(tagNum == 0xDFDF29)     //Script result 71
        {
            tmpLen = g_CEMV.GetScriptResult71(tempTLV);
            dlog_msg("Adding Script Result 71");
        }
        else if(tagNum == 0xDFDF2A)     //Script result 72
        {
            tmpLen = g_CEMV.GetScriptResult72(tempTLV);
            dlog_msg("Adding Script Result 72");
        }
        else if(tagNum == 0xDFDF25)     //Reason Online
        {
            unsigned char ro = g_CEMV.GetReasonOnline();
            tmpLen = sizeof(ro);            
            memcpy(tempTLV, &ro, tmpLen);
            dlog_msg("Adding Reason Online (%d)", ro);
        }
        else if(tagNum == 0xDFDF04) // EMV Kernel Version
        {
            unsigned char szEMVVersion[15+1];
            memset(szEMVVersion, 0, sizeof(szEMVVersion));
            usEMVGetVersion(szEMVVersion);
            tmpLen = strlen(reinterpret_cast<char *>(szEMVVersion));
            memcpy(tempTLV, szEMVVersion, tmpLen);
            dlog_msg("Adding EMV Kernel version '%s'", szEMVVersion);
        }
        else if(tagNum == 0xDFDF05) // EMV Kernel Checksums (read from MVT.DAT!!!!)
        {
            MVT_REC rec;
            if (loadMVTRec(&rec))
            {
                tmpLen = strlen(rec.szRFU1);
                memcpy(tempTLV, rec.szRFU1, tmpLen);
                dlog_msg("Adding Kernel checksum '%s'", rec.szRFU1);
            }
            else
            {
                tmpLen = 0;
            }
        }
        else
        {
            tmpLen = 0;
            if (!isTagBlacklisted(tagNum))
            {
                if (EMV_SUCCESS != usEMVGetTLVFromColxn(tagNum, tempTLV, &tmpLen))
                {
                    g_CEMVtrns.GetTag(tagNum, tempTLV, &tmpLen);
                }
            }
       }
	   *TLVlen = tmpLen;
	   return tmpLen;
}

int parseInTLV(unsigned char *buffer, int buflen, bool permanent)
{
    int numObj = 0;
    unsigned char Tag[3];
    int tagNum;
    int Len;
    unsigned char *current;
    unsigned char *script;
    int i;
    int proc_len=0;
    Ushort Ret = 0;
    cUpdateCfg UpdateCfg;

    if(permanent)
    {
        UpdateCfg.locateConfigRecords(g_CardAppConfig.GetCurrentAID().c_str());
    }

    current = buffer;
    dlog_msg("TLV Input data len: %d", buflen);
    while(buflen > proc_len)
    {
        dlog_msg("Processing %d/%d", proc_len, buflen);
        Tag[0] = current[0];
        script = current;
        current++;
        proc_len++;
        
        if((Tag[0] & 0x1F) == 0x1F) //more than 1 byte tag
        {
            Tag[1] = current[0];
            current++;
            proc_len++;
        }
        else
        {
            Tag[1] = 0x00;
        }
        
        //if((Tag[1] & 0x7F) != Tag[1]) //more than 2 byte tag
        if(Tag[1] & 0x80) //more than 2 byte tag
        {
            Tag[2] = current[0];
            current++;
            proc_len++;
        }
        else
        {
            Tag[2] = 0x00;
        }
        
        if(Tag[2] > 0)
        {
            tagNum = Tag[0]*0x10000 + Tag[1]*0x100 + Tag[2];
        }
        else
        {
            tagNum = Tag[0]*0x100 + Tag[1];
        }
        dlog_msg("Processing TAG: %0X", tagNum);
        Len = (int)current[0];
        current++;
        proc_len++;
        if(Len & 0x80)
        {
            //well... we have more than one byte coding len
            i = Len & 0x7F;
            Len = 0;
            while(i)
            {
                // Len+=(int)current[0]*pow((double)0x10, i-1);
                Len += ((int)current[0]) << (8*(i-1));
                i--;
                current++;
                proc_len++;
            }
        }
        if(Tag[0] & 0x20)   //constructed 
        {
            if((tagNum == TAG_ISUER_SCRPT_TEMPL_71) || (tagNum == TAG_ISUER_SCRPT_TEMPL_72))
            {
                //scripts
                dlog_msg("Script type: %X", Tag[0]);

                if (tagNum == TAG_ISUER_SCRPT_TEMPL_71)
                {
                    g_CEMVtrns.script71 = script;
                    g_CEMVtrns.script71_len = Len+(current - script);
                }
                else if (tagNum == TAG_ISUER_SCRPT_TEMPL_72)
                {
                    g_CEMVtrns.script72 = script;
                    g_CEMVtrns.script72_len = Len+(current - script);
                }
            }
            else
            {
                //any other
                dlog_msg("TLV constructed: %02X%02X%02X Len:%X", Tag[0], Tag[1], Tag[2], Len);
                parseInTLV(current, Len, false);
            }
        }
        else                //primitive
        {
            numObj++;
            if(permanent)
            {
                Ret = UpdateCfg.updateConfig(tagNum, current, Len);
            }
            else
            {
                Ret = checkPrimitiveTLV(tagNum, current, Len);
            }
            dlog_msg("TLV primitive: %X Len: %d added: %d", tagNum, Len, Ret);
        }
        current+=Len;
        proc_len+=Len;
    }
    if(permanent && numObj > 0)
    {
        UpdateCfg.commitChanges(g_CardAppConfig.GetCurrentAID().size() > 0);
    }
    return numObj;
}

bool isTagBlacklisted(int tagNum)
{
    Ushort BLACKLIST[] = { TAG_99_PIN_DATA };
    for (int i = 0; i < sizeof(BLACKLIST)/sizeof(BLACKLIST[0]); i++)
    {
        if (BLACKLIST[i] == tagNum)
        {
            dlog_alert("Tag %02X is blacklisted, we won't return it!", tagNum);
            return true;
        }
    }
    return false;
}

int parseOutTLV(unsigned char *bufferIn, int buflenIn, unsigned char *bufferOut, int buflenOut)
{
    
    int numObj = 0;
    unsigned char *currentIn;
    unsigned char *currentOut;
    int i;
    unsigned char Tag[3];
    int tagNum;
    unsigned short TLVlen = 0;
    unsigned char tempTLV[CARD_APP_DEF_MAX_TLV_LEN];
    srTxnResult TVRTSI;
    int Outlen = 0;

    if((buflenIn == 0) || (buflenOut == 0) || (bufferIn == NULL) || (bufferOut == NULL))
        return 0;
    
    currentIn = bufferIn;
    currentOut = bufferOut;
    usEMVGetTxnStatus(&TVRTSI);
    while(bufferIn+buflenIn > currentIn)
    {
        Tag[0] = currentIn[0];
        currentIn++;
       #if 0
        if(Tag[0] & 0x20)   //constructed 
        {
            return -1;      //constructed objects are not allowed
        }
       #endif
        if((Tag[0] & 0x1F) == 0x1F) //more than 1 byte tag
        {
            Tag[1] = currentIn[0];
            currentIn++;
        }
        else
        {
            Tag[1] = 0x00;
        }
        if(Tag[1] & 0x80) //more than 1 byte tag
        {
            Tag[2] = currentIn[0];
            currentIn++;
        }
        else
        {
            Tag[2] = 0x00;
        }
        if(Tag[2] > 0)
        {
            tagNum = Tag[0]*0x10000 + Tag[1]*0x100 + Tag[2];
        }
        else
        {
            tagNum = Tag[0]*0x100 + Tag[1];
        }
        dlog_msg("Processing TAG: %X", tagNum);

        getTLV(tagNum, tempTLV, &TLVlen);
        if(buflenOut - Outlen >= TLVlen + 5)    //max size of tag and its len field
        {
            //there is still place in out buffer
            currentOut[0] = Tag[0];
            currentOut++;
            Outlen++;
            if(Tag[1] > 0)
            {
                currentOut[0] = Tag[1];
                currentOut++;
                Outlen++;
            }
            if(Tag[2] > 0)
            {
                currentOut[0] = Tag[2];
                currentOut++;
                Outlen++;
            }
            if(TLVlen > 0x7F)
            {
                currentOut[0] = 0x81;
                currentOut++;
                Outlen++;
            }
            currentOut[0] = static_cast<unsigned char>(TLVlen);
            currentOut++;
            Outlen++;
            memcpy(currentOut, tempTLV, TLVlen);
            dlog_msg("adding %d bytes, current len: %d - total %d TLV objects", TLVlen, Outlen, numObj);
            currentOut+=TLVlen;
            Outlen+=TLVlen;
            numObj++;
        }
            
    }
    return Outlen;
        
}

int inGetPTermID(char *ptid)
{
    char    tempPTID [EMV_TERM_ID_SIZE + 2] = {0};

    SVC_INFO_PTID (tempPTID);
    strncpy (ptid, tempPTID+1, EMV_TERM_ID_SIZE);
    ptid[EMV_TERM_ID_SIZE+1] = '\0';
    return (EMV_SUCCESS);
}

#ifdef VFI_GUI_GUIAPP
int get_character_resolution(int &x, int &y)
{
	int left, right,top,bottom;

	char stuff_6x[6+1];
    int charSizeRow=0, charSizeCol=0;

	SVC_INFO_DISPLAY(stuff_6x);
	stuff_6x[6]=0;

	y = atoi(&stuff_6x[3]);
	stuff_6x[3] = 0;
	x = atoi(stuff_6x);

	dlog_msg("SVC_INFO_DISPLAY: %d:%d", x, y);

	x = x/8;	//assuming standard 8x8 font
	y = y/8;
	
	return 0;
}
#endif

#ifdef VFI_GUI_GUIAPP
int get_display_resolution(int &x, int &y)
{
    int left, right,top,bottom;
    int textRows, textPixelsY;
    int charSizeRow=0, charSizeCol=0;

    set_display_coordinate_mode(CHARACTER_MODE);
    wherewin(&left, &top, &right, &bottom);
    get_character_size(&charSizeRow, &charSizeCol);

    textRows = bottom + 1 - top;
    textPixelsY = charSizeRow * textRows;

    set_display_coordinate_mode(PIXEL_MODE);
    window(0, 0, WINDOW_MAX, WINDOW_MAX);
    int ret = wherewin(&left, &top, &right, &bottom);
    if(ret<0) return ret;
    int graphPixelsY = bottom + 1 - top;
    int diffY = graphPixelsY - textPixelsY;
    if (diffY > charSizeRow)
    {
        y = textPixelsY;
    }
    else
    {
        y = graphPixelsY;
    }
    x = right + 1 - left;
    set_display_coordinate_mode(CHARACTER_MODE);
    return 0;
}
#endif

bool VerifyPINBypassPassword(int &m_Console, char *amtDsp, long &x_pos, long &y_pos, bool smallScreen, char * addText)
{
    return true;
}

unsigned short getUsrSecurePin(unsigned char *pstPin)
{
    dlog_msg("offline PIN requested!");
    // g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_ENTRY);
    return E_PERFORM_SECURE_PIN;
}

void restoreGuiAppConsole()
{
    //GuiApp_RestoreConsole();
}
/*
#ifdef VFI_GUI_GUIAPP
cPINdisplay::cPINdisplay()
{
	main_msg.clear();
	extra_msg.clear();
	amount_line = 0;
	x = 0;
	y = 0;

}

std::string cPINdisplay::BuildExtraMsg(void)
{
	std::string msg_extra;

	if(g_CardAppConfig.pinScreenTitle != CARD_APP_GUI_ENTER_PIN)
	{
		msg_extra = g_CardAppConfig.GetEnterPINMsg();
	}
	if(extra_msg.size() > 0)
	{
		strcpy(g_CardAppConfig.extraPINmessage, extra_msg.c_str());
	}
	if (strlen(g_CardAppConfig.extraPINmessage) == 0)
	{
		char szAppLabel[MAX_LABEL_LEN+1] = {0}; // 16 bytes plus zero termination
		unsigned short usAppLabelLen = sizeof(szAppLabel) - 1; // = EMV_APP_LABEL_MAX_LEN
	
		g_CardAppConfig.extraPINmessage[sizeof(g_CardAppConfig.extraPINmessage)-1] = 0;
		dlog_msg("Extra PIN message not set, trying to display Application Preferred Name");
		if ( usEMVGetTLVFromColxn( TAG_9F12_APPL_PRE_NAME, (byte *)szAppLabel, &usAppLabelLen ) == EMV_SUCCESS )
		{
			dlog_msg("Preferred App. label %s, len: %u", szAppLabel, usAppLabelLen);
			// Check whether Issuer Code Table Index is present and supported
			unsigned char codeTable = 0;
			unsigned short tempLen = 1;
			if ( usEMVGetTLVFromColxn( TAG_9F11_ISSSUER_CODE_TBL, (byte *)&codeTable, &tempLen ) == EMV_SUCCESS )
			{
				dlog_msg("Code table %d (len %d)", codeTable, tempLen);
				if (codeTable >= 1 && codeTable <= 10)
				{
					unsigned char tempBuf[10];
					memset(tempBuf, 0, sizeof(tempBuf));
					tempLen = sizeof(tempBuf)-1;
					if ( usEMVGetTLVFromColxn( TAG_9F40_TERM_ADTNAL_CAP, (byte *)tempBuf, &tempLen ) == EMV_SUCCESS )
					{
						bool isSupported = false;
						switch (codeTable)
						{
								case 1 : isSupported = tempBuf[4] & 0x01; break;
								case 2 : isSupported = tempBuf[4] & 0x02; break;
								case 3 : isSupported = tempBuf[4] & 0x04; break;
								case 4 : isSupported = tempBuf[4] & 0x08; break;
								case 5 : isSupported = tempBuf[4] & 0x10; break;
								case 6 : isSupported = tempBuf[4] & 0x20; break;
								case 7 : isSupported = tempBuf[4] & 0x40; break;
								case 8 : isSupported = tempBuf[4] & 0x80; break;
								case 9 : isSupported = tempBuf[3] & 0x01; break;
								case 10: isSupported = tempBuf[3] & 0x02; break;
						}
						dlog_msg("Add term caps (2 last bytes) %02X %02X. IsSupported %d", tempBuf[2], tempBuf[3], isSupported);
						if (isSupported)
						{
							int copyLen = (sizeof(g_CardAppConfig.extraPINmessage)-1 > usAppLabelLen ? sizeof(g_CardAppConfig.extraPINmessage)-1 : usAppLabelLen);
							strncpy( g_CardAppConfig.extraPINmessage, szAppLabel, copyLen );
						}
					}
				}
			}
		}
		if (!strlen(g_CardAppConfig.extraPINmessage))
		{
			memset(szAppLabel, 0, sizeof(szAppLabel));
			usAppLabelLen = sizeof(szAppLabel) - 1;
			if ( usEMVGetTLVFromColxn( TAG_50_APPL_LABEL, (byte *)szAppLabel, &usAppLabelLen ) == EMV_SUCCESS )
			{
				dlog_msg("App. label %s, len: %u", szAppLabel, usAppLabelLen);
				int copyLen = (sizeof(g_CardAppConfig.extraPINmessage)-1 > usAppLabelLen ? sizeof(g_CardAppConfig.extraPINmessage)-1 : usAppLabelLen);
				strncpy( g_CardAppConfig.extraPINmessage, szAppLabel, copyLen );
			}	
		}
		if (!strlen(g_CardAppConfig.extraPINmessage))
		{
			dlog_error("App. label cannot be retrieved");
			g_CardAppConfig.extraPINmessage[0] = 0;
		}
		else
		{
			msg_extra.assign(g_CardAppConfig.extraPINmessage);
		}
	}
	return msg_extra;

}

std::string cPINdisplay::BuildAmtMsg(void)
{
	
	uint16_t trn_curr = 0;
	char strPIN[ADD_SCREEN_TEXT+1];
	Ushort dLen =0;
	char amtDsp[ADD_SCREEN_TEXT+1];
	unsigned char amt_num[6];
	unsigned char currExp = 2;
	const currency_code_t *pCurrCode = &M_CURRENCY_CODES[0];
    bool codesUsed = false;
	std::string amt_msg;
	
	if(usEMVGetTLVFromColxn(TAG_9F02_AMT_AUTH_NUM, amt_num, &dLen) == EMV_SUCCESS)
	{
		SVC_HEX_2_DSP((const char *)amt_num, amtDsp, dLen);
		amtDsp[dLen*2] = 0;
		dlog_msg("amount read: %s", amtDsp);
		if(usEMVGetTLVFromColxn(TAG_5F2A_TRANS_CURCY_CODE, amt_num, &dLen) == EMV_SUCCESS)
		{
			//
			SVC_HEX_2_DSP((const char *)amt_num, strPIN, dLen);
			trn_curr = atoi(strPIN);
			int curr_ind=0;
			if (g_CardAppConfig.GetPINDisplayCurrencySymbol())
			{
				while((M_CURRENCY_SYMBOLS[curr_ind].code != trn_curr) && (M_CURRENCY_SYMBOLS[curr_ind].code != 0))
				{
					curr_ind++;
				}
				if (M_CURRENCY_SYMBOLS[curr_ind].code == 0) curr_ind = 0;
				else pCurrCode = &M_CURRENCY_SYMBOLS[curr_ind];
			}
			if (curr_ind == 0)
			{
				while((M_CURRENCY_CODES[curr_ind].code != trn_curr) && (M_CURRENCY_CODES[curr_ind].code != 0))
				{
					curr_ind++;
				}
				pCurrCode = &M_CURRENCY_CODES[curr_ind];
				codesUsed = true;
			}
			dlog_msg("Transaction currency code: %d - '%s'", trn_curr, pCurrCode->name);
			dlog_msg("Curr code, first three bytes: 0x%02X 0x%02X 0x%02X", pCurrCode->name[0], pCurrCode->name[1], pCurrCode->name[2]);
			
		}
		dLen = 1;
		if(usEMVGetTLVFromColxn(TAG_5F36_TRANS_CURR_EXP, &currExp, &dLen) != EMV_SUCCESS)
		{
			dlog_alert("No currency exponent, assuming default");
			currExp = 2;
		}
		
		int ind_i = 0, ind_j = 0, am_len = 0;
		am_len = strlen(const_cast<char*>(amtDsp));
		while((amtDsp[ind_i] == '0') && (ind_i < am_len-(currExp+1)))
			ind_i++;
		while(amtDsp[ind_i] != 0)
		{
			if(am_len - currExp  == ind_i)
				amtDsp[ind_j++] = g_CardAppConfig.GetPINRadixSeparator();
				//amtDsp[ind_j++] = '.';
			if((amtDsp[ind_i] >= '0') && (amtDsp[ind_i] <= '9'))
			{
				amtDsp[ind_j++] = amtDsp[ind_i];
			}
			ind_i++;
		}
		if (g_CardAppConfig.PINDisplayCurrencySymbolLeft( codesUsed ))
		{
			// ind_j consists of amtDsp length
			int currCodeLen = strlen(pCurrCode->name);
			assert(ind_j + currCodeLen < sizeof(amtDsp));
			memmove( amtDsp+currCodeLen+1, amtDsp, ind_j );
			memcpy(amtDsp, pCurrCode->name, currCodeLen);
			amtDsp[currCodeLen] = ' ';
			amtDsp[ind_j+currCodeLen+1] = 0;
		}
		else
		{
			int currCodeLen = strlen(pCurrCode->name);
			amtDsp[ind_j++] = ' ';
			memcpy(amtDsp+ind_j, pCurrCode->name, currCodeLen);
			amtDsp[ind_j+currCodeLen] = 0;
		}
		amt_msg.assign(amtDsp);
		
	}
	return amt_msg;

}


int cPINdisplayDefault::Display(c_encPIN encPIN)
{
	const currency_code_t *pCurrCode = &M_CURRENCY_CODES[0];
    char strPIN[ADD_SCREEN_TEXT+1];
    strPIN[0]=0;
    uint16_t trn_curr = 0;
	std::string amtDsp;
	std::string extDsp;
    
    
	amtDsp = BuildAmtMsg();
    dlog_msg("Amount text: %s", amtDsp.c_str());
	extDsp = BuildExtraMsg();
    dlog_msg("Extra text: %s", extDsp.c_str());
    strPIN[0]=0;

	char tempText[DISPLAY_TEXT+1];
    tempText[DISPLAY_TEXT] = 0;
    strcpy(tempText, "\\c");
    strncpy(tempText+2, extDsp.c_str(), DISPLAY_TEXT-4);
    //strcat(tempText, "\n");
    get_console_handle();
    DisplayText(tempText, strlen(tempText), 1, 1, true);
    strncpy(tempText+2, amtDsp.c_str(), DISPLAY_TEXT-4);
    //strcat(tempText, "\n");
    DisplayText(tempText, strlen(tempText), 1, 2, false);
    {
         char * pText = tempText + 2;
         userPromptLite prompt;
         if (prompt.getPrompt(CARD_APP_GUI_S_PIN, g_CardAppConfig.pinScreenTitle, pText, DISPLAY_TEXT-2 ) <= 0) // -2 to ensure beginning '\c' fits
         {
             dlog_error("Cannot get PIN prompt (number %d), assuming default (Enter PIN)!", g_CardAppConfig.pinScreenTitle);
		 	 if(getPromptLocal(g_CardAppConfig.pinScreenTitle, pText) == 0)
			 	strcpy(pText, "ENTER PIN");
         }
    }
    DisplayText(tempText, strlen(tempText), 1, 3, false);

	return 1;
}

bool cPINdisplayDefault::ComputeEntryXY()
{
    const int maxX = getScreenMaxX();
	const int maxY = getScreenMaxY();
    x = (maxX >> 1) - (M_MAX_PIN_LENGTH_MAX >> 1);
    y = 4;

	dlog_msg("Screen size (x,y): %d:%d coordinates: %d:%d", maxX, maxY, x, y);
    return true;
}

cPINdisplayGuiApp::cPINdisplayGuiApp(): cPINdisplay(), consoleClaimed(false), guiConnected(false), pinExecuted(false)
{
    #if 0
    using namespace com_verifone_guiapi;
    //const char * const CARD_APP_NAME_ = "CARDAPP";
    //const char * const GUI_APP_NAME_ = "GUIAPP";
    if( GuiApp_Connect( (char *)CARD_APP_NAME, 10000 ) == GUI_OKAY )
    {
        guiConnected = true;
        dlog_msg( "Connected from %s to %s", CARD_APP_NAME, GUI_APP_NAME_ );
    }
    #endif
}
cPINdisplayGuiApp::~cPINdisplayGuiApp()
{
    // restore console
    using namespace com_verifone_guiapi;
    dlog_msg("Destroying GuiApp PIN object");
    if (consoleClaimed) GuiApp_RestoreConsole();
    if (guiConnected) GuiApp_Disconnect();
    if (!pinExecuted) g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_DONE);
}
int cPINdisplayGuiApp::Display(c_encPIN encPIN)
{
    using namespace com_verifone_guiapi;
    const currency_code_t *pCurrCode = &M_CURRENCY_CODES[0];
    char strPIN[ADD_SCREEN_TEXT+1];
    strPIN[0]=0;
    uint16_t trn_curr = 0;
    std::string amtDsp;
    std::string extDsp;
    
    pinExecuted = true;
    amtDsp = BuildAmtMsg();
    dlog_msg("Amount text: %s", amtDsp.c_str());
    extDsp = BuildExtraMsg();
    dlog_msg("Extra text: %s", extDsp.c_str());
    strPIN[0]=0;

    if (consoleClaimed)
    {
        // We have to restore the console!!!
        GuiApp_RestoreConsole();
        consoleClaimed = false;
    }
    GuiApp_DisplayPINPromptEx( g_CardAppConfig.pinScreenTitle, const_cast<char *>(extDsp.c_str()), const_cast<char *>(amtDsp.c_str()), &x, &y, CARD_APP_GUI_S_PIN);
    // transfer console
    GuiApp_TransferConsole();
	
	//SB: get_console_handle must be called for Vx600
	//TODO: check if this call will be ok for Vx820
	
    //if (get_console(1) >= 0)
	if(get_console_handle()>0)
    {
        consoleClaimed = true;
        set_display_coordinate_mode(CHARACTER_MODE);
		dlog_msg("SB:console opened successfully");
    } else {
    	dlog_msg("Could not get console handle");
    }
    //write_at("", 0, 9, 14);
    //write_at("", 0, x_pos/x_font+M_MAX_PIN_LENGTH_MAX/2, y_pos/y_font+2);
    return 1;
}
bool cPINdisplayGuiApp::ComputeEntryXY()
{
	using namespace com_verifone_guiapi;

	long x_pos = 0, y_pos = 0, x_font = 0, y_font = 0;
	int m_Console = -1;
	bool result = false;
	if (GuiApp_GetPINBoxCoordinates(&x_pos, &y_pos) == GUI_OKAY)
	{
		GuiApp_TransferConsole();
		if((m_Console = get_console(1)) > 0)
		{
			dlog_msg("Getting console handle %d", m_Console);
			switch(getgrid())
			{
				case 0:
					x_font = 8;
					y_font = 16;
					break;
				case 2:
					x_font = 6;
					y_font = 8;
					break;
				default:
					y_font = 8;
					x_font = 8;
					break;
			}
			//screen_size(xy_screen);
			dlog_msg("Font size: %d:%d", x_font, y_font);
			//write_at("", 0, ((x_screen/x_font)/4)+((M_MAX_PIN_LENGTH_MAX-maxPINLength)/2), y_pos/y_font+2);
			//write_at("", 0, x_pos/x_font+M_MAX_PIN_LENGTH_MAX/2, y_pos/y_font+2);
			x = x_pos/x_font-M_MAX_PIN_LENGTH_MAX/2;
			y = y_pos/y_font+2;
			dlog_msg("Received PIN box coords: %d x %d", x_pos, y_pos);
			dlog_msg("PIN boox coordinates: %d x %d", x, y);
			GuiApp_RestoreConsole();
			result = true;
		}
	}
	return result;
}
#endif

*/

unsigned short getUsrPin(unsigned char *pstPin)
{
    int m_Console = -1;
    uint8_t minPINLength = M_MIN_PIN_LENGTH_MIN;
    uint8_t maxPINLength = M_MAX_PIN_LENGTH_MAX;
    long x_pos = 0, y_pos = 0, x_font = 0, y_font = 0;
    int x_screen = 0, y_screen = 0;
    char xy_screen[2];
    bool codesUsed = false;
    bool smallScreen = false; // Some devices (Vx700, Vx520, ...) have small screen -> Only two lines of text plus PIN box fit
    static const int MAX_ALTERNATE_SCREENS = 2;
    char alternateScreens[MAX_ALTERNATE_SCREENS][ADD_SCREEN_TEXT+1];
    int alternateScreenIndex = 0;
    
    pstPin[0] = 0;
    dlog_msg("PIN entry requested");
    g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_ENTRY);

    Ushort dLen =0;
    unsigned char amt_num[6];
    unsigned char currExp = 2;
    const currency_code_t *pCurrCode = &M_CURRENCY_CODES[0];
    uint16_t trn_curr = 0;
    std::string amtDsp;
    std::string extDsp;
    cPINdisplay * pPINdisplay = g_CEMV.getPINDisplayObj();
    //std::auto_ptr<cPINdisplay> PINdisplay;
    //if (g_CardAppConfig.GuiInterface() == CCardAppConfig::GUI_APP) PINdisplay.reset(new cPINdisplayGuiApp);
    //else PINdisplay.reset(new cPINdisplayDefault);
    
	c_encPIN encPIN;
	
	dlog_msg("getUsrPin start pPinDisplay %p", pPINdisplay);
	
    if (pPINdisplay) pPINdisplay->Display(encPIN);
    
	dlog_msg("getUsrPin end pPinDisplay %p", pPINdisplay);
    
    //here we start actual pin entry
    int key_index=0;
    bool pin_bypassed = false;
    static const long TIMEOUT_EVENT = EVT_TIMER;
    int m_PosTimeoutTimer = -1;

    if(g_CardAppConfig.GetPINtype() == CARD_APP_PIN_ONLINE_VSS)
    {
        unsigned char dummy = 0;
        int retVal;
        PINRESULT pinOUTData;
        int pinStatus;
        PINPARAMETER psKeypadSetup;
        long interestingEvents = EVT_PIPE;
        long detectedEvents = 0;

        psKeypadSetup.ucMin = g_CardAppConfig.GetMinPINLen();
        psKeypadSetup.ucMax = g_CardAppConfig.GetMaxPINLen();
        psKeypadSetup.ucEchoChar = '*';
        psKeypadSetup.ucDefChar = g_CardAppConfig.GetPINblankChar();
        psKeypadSetup.ucOption = g_CardAppConfig.GetPINoption(); // Bit 3 is set by default
        if (g_CardAppConfig.GetOnlinePINCancelAllowed())
            psKeypadSetup.ucOption |= 0x02; // bit 1
        if (g_CardAppConfig.GetOnlinePINEntryType() != CCardAppConfig::PIN_ENTRY_TYPE_MANDATORY_E)
            psKeypadSetup.ucOption |= 0x10; // bit 4
        dlog_msg("ONLINE PIN entry, flags %02X", psKeypadSetup.ucOption);
        iPS_SetPINParameter(&psKeypadSetup);
        iPS_SelectPINAlgo(0x0B); // Always
        retVal=iPS_RequestPINEntry(0, &dummy);
        // Set up PIN entry timeout (if applicable)
        if(g_CardAppConfig.GetPINtimeout() > 0)
        {
            m_PosTimeoutTimer = set_timer(g_CardAppConfig.GetPINtimeout() * 1000, TIMEOUT_EVENT);
            dlog_msg("Entry timeout set to %d", g_CardAppConfig.GetPINtimeout() * 1000);
            interestingEvents |= TIMEOUT_EVENT;
        }
        //read_evt(interestingEvents);
        do 
        {
            // Get PIN Response 
            retVal=iPS_GetPINResponse(&pinStatus, &pinOUTData);
            // Check PIN timeout
            // Check cancellation
            //dlog_msg("Detected events %d", detectedEvents);
            std::string source_task;
            if (com_verifone_ipc::check_pending(source_task) == com_verifone_ipc::IPC_SUCCESS)
            {
                CCardAppConfig::cmd_break_t break_status=g_CardAppConfig.checkCmdBreak();
                if(break_status == CCardAppConfig::CMD_BREAK_CANCEL_E)
                {
                    iPS_CancelPIN();
                    pinStatus = 0xFF;
                    break;
                }
            }
            if (detectedEvents & TIMEOUT_EVENT)
            {
                // Timeout occurred!
                iPS_CancelPIN();
                pinStatus = 0xFE;
                break;
            }
            SVC_WAIT(300);
        } while (pinStatus == 0x02);
        if (m_PosTimeoutTimer >= 0) 
            clr_timer(m_PosTimeoutTimer);
        switch(pinStatus)
        {
             case 0x00:
                dlog_msg("INFO:EnterEncryptPIN: PIN_OK_");
                key_index = pinOUTData.nbPinDigits;
                break;
             case 0x06: // if allowed 
                dlog_msg("INFO:EnterEncryptPIN: PIN_0PIN_");
                key_index = E_USR_PIN_BYPASSED;
                break; // ok, process 
             case 0x05: //user cancel 
                dlog_msg("INFO:EnterEncryptPIN: PIN_ABORT_CANCEL_ %d ", pinOUTData.encPinBlock[0]);
                key_index = static_cast<unsigned short>(E_USR_PIN_CANCELLED);
                break;
             case 0x0A:
                dlog_msg("INFO:EnterEncryptPIN: PIN_ABORT_CLEAR_");
                key_index = static_cast<unsigned short>(E_USR_ABORT);
                break;
             case 0x01: // unknown error ?? 
                dlog_msg("INFO:EnterEncryptPIN: PIN_IDLE_");
                key_index = static_cast<unsigned short>(E_USR_ABORT);
                break;
             case 0xFE: // timeout 
                dlog_msg("INFO:EnterEncryptPIN: PIN_TIMEOUT_");
                key_index = static_cast<unsigned short>(E_USR_ABORT);
                break;
             case 0xFF: // aborted externally 
                dlog_msg("INFO:EnterEncryptPIN: PIN_ABORTED_BY_POS_");
                key_index = static_cast<unsigned short>(E_USR_PIN_CANCELLED);
                break;
             default:
                dlog_msg("INFO:EnterEncryptPIN: PIN response error");
                key_index = static_cast<unsigned short>(E_USR_ABORT);
                break;
        }
        dlog_msg("PIN Processing %d digits", pinOUTData.nbPinDigits);
        
    }
    else if(g_CardAppConfig.GetPINtype() == CARD_APP_SECURE_PIN_OFFLINE)
    {
        dlog_msg("Secure PIN - returning control to EMV Kernel");
        return EMV_SUCCESS;
    }
        
    return key_index;
}

/* For PIN Library only */
unsigned short GetPINStatusDetail() { return g_CEMV.GetPINEntryStatus(); }

void usEMVDisplayErrorPrompt(unsigned short errorID)
{
    unsigned long prompt_index = 0;
    char section_name[10];
    unsigned char x_pos = 0, y_pos = 0;
    int display = 0;
    CCardAppConfig::cmd_break_t break_status;

    switch(errorID)
    {
        case E_PIN_REQD:
            break;
        case E_INVALID_PIN:
            dlog_msg("Incorrect PIN");
            {
                unsigned char pinTryCtr;
                unsigned short pinTryCtrLen = 0;
                usEMVGetTLVFromColxn(TAG_9F17_PIN_TRY_COUNTER, &pinTryCtr, &pinTryCtrLen);
                if (pinTryCtrLen != 1)
                {
                    dlog_alert("Cannot get PIN Try Counter!");
                    pinTryCtr = 3;
                }
                --pinTryCtr;
                usEMVUpdateTLVInCollxn(TAG_9F17_PIN_TRY_COUNTER, &pinTryCtr, pinTryCtrLen);
                dlog_msg("PIN Try Counter == %d", pinTryCtr);
                if (pinTryCtr == 1)
                {
                    dlog_alert("Workaround for Kernel bug!");
                    g_CardAppConfig.pinScreenTitle = CARD_APP_GUI_LAST_PIN;
                    g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_LAST);
                    g_CEMV.SetPINStatus(PIN_ENTRY_STATUS_LAST_TRY);
                }
                else
                {
                    g_CardAppConfig.pinScreenTitle = CARD_APP_GUI_INCORRECT_PIN;
                    g_CEMV.SetPINStatus(PIN_ENTRY_STATUS_BAD_PIN);
                }
            }
            //strcpy(section_name, CARD_APP_GUI_S_PIN);
            //prompt_index = CARD_APP_GUI_INCORRECT_PIN;
            //display = 0;
            break;
        case E_LAST_PIN_TRY:
            dlog_msg("Last PIN try");
            //strcpy(section_name, CARD_APP_GUI_S_PIN);
            //prompt_index = CARD_APP_GUI_LAST_PIN;
            //display = 0;
            g_CardAppConfig.pinScreenTitle = CARD_APP_GUI_LAST_PIN;
            g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_LAST);
            g_CEMV.SetPINStatus(PIN_ENTRY_STATUS_LAST_TRY);
            break;
        case E_PIN_TRY_LT_EXCEED:
            dlog_msg("PIN blocked!");
            g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_BLOCKED);
            break;
        case E_INVALID_CAPK:
            break;
        case E_CAPK_FILE_NOT_FOUND:
            break;
        case E_COMBINED_DDA_AC_FAILED:
            break;
        case E_BLACKLISTED_CARD:
            break;
        case E_NO_ATR:
            break;

        case E_INVALID_KEY_LENGTH:
            break;
        case E_CVM_NOT_PERFORMED:
            break;
        case E_PIN_BLOCKED:
            dlog_msg("PIN blocked!");
            g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_BLOCKED);
            break;
        case UNRECOGNIZED_CVM:
            break;
        case ONLINE_PIN_PERFORMED:
            break;

        case E_INVALID_ATR:
            break;
        case E_APP_BLOCKED:
            break;
        case EMV_PIN_SESSION_IN_PROGRESS:
            dlog_msg("Message received - checking if not cancel...");
            break_status=g_CardAppConfig.checkCmdBreak();
            if(break_status == CCardAppConfig::CMD_BREAK_CANCEL_E)
            {
                dlog_alert("PIN entry cancelled, exiting...");
                g_CEMV.SetPINBypassStatus(EXT_PIN_CNCL);        //JACEK - to be setup
                //key_index = static_cast<unsigned short>(E_USR_ABORT);
                //entry_end = 1;
                //g_CEMV.SetStatusDetail(CARD_APP_STATUS_D_CANCELLED);
                //g_CEMV.SetPINEntryStatus(PIN_ENTRY_STATUS_CANCELLED_POS);
            }
            else if(break_status == CCardAppConfig::CMD_BREAK_PIN_BYPASS_E)
            {
                dlog_alert("Bypassing PIN entry ...");
                g_CEMV.SetPINBypassStatus(EXT_PIN_BYPASS);      //JACEK - to be setup
                //key_index = static_cast<unsigned short>(E_USR_PIN_BYPASSED);
                //entry_end = 1;
                //g_CEMV.SetStatusDetail(CARD_APP_STATUS_D_BYPASS);
                //g_CEMV.SetPINEntryStatus(PIN_ENTRY_STATUS_BYPASSED_POS);
            }
//            SVC_WAIT(100);
            break;
        case E_USR_PIN_CANCELLED:
            dlog_msg("PIN cancelled by Cardholder!");
            break;
        case E_USR_PIN_BYPASSED:
            dlog_msg("PIN bypassed by Cardholder!");
            break;
        case EMV_PIN_SESSION_COMPLETE:
            dlog_msg("PIN session completed!");
/*          if (g_CEMVtrns.beeperThreadID)
            {
                dlog_msg("Killing beeper thread");
                post_user_event(g_CEMVtrns.beeperThreadID, 1); // post any user event, thread will end this way
                g_CEMVtrns.beeperThreadID = 0;
            } */
            //g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_DONE);
            break;
    }
    
    dlog_msg(">>>MODULE PROMPT: %X", errorID);
    if(display)
    {
        if(prompt_index > 0)
        {
            if(g_CardAppConfig.GuiInterface() == CCardAppConfig::GUI_APP)
            {
                dlog_msg("Displaying prompt %X using GUIAPP", errorID);
                
                if(g_CardAppConfig.GetUseDisplay())
                {
                    //JACEK - to be setup
                    /*
                    GuiApp_DisplayPromptSelectSection(prompt_index, section_name, x_pos, y_pos);
                    error_tone();
                    SVC_WAIT(600);
                    error_tone();
                    SVC_WAIT(600);
                    error_tone();
                    SVC_WAIT(600);
                    */
                }
            } 
            else
            {
                dlog_error( "LOCAL GUI SUPPORT NOT YET IMPLEMENTED!" );
            }
        }
    }
    else
    {
        if (prompt_index > 0)
        {
            userPromptLite prompt;
            prompt.getPrompt(section_name, prompt_index, g_CardAppConfig.extraPINmessage, ADD_SCREEN_TEXT+1 );
            dlog_msg("Additional PIN prompt is %s", g_CardAppConfig.extraPINmessage);
        }
    }
}

unsigned short usEMVPerformSignature(void)
{
    dlog_msg("Signature Requested");
    g_CEMV.SetStatusMain(CARD_APP_STATUS_M_SIGNATURE);
    return EMV_SUCCESS;
}

unsigned short usEMVPerformOnlinePIN(void)
{
    dlog_msg("Online PIN requested");
    g_CEMV.SetStatusMain(CARD_APP_STATUS_M_PIN);
    return EMV_SUCCESS;
}

EMVResult usEMVIssAcqCVM(unsigned short issacq, unsigned short *code)
{
    dlog_msg( "emv callback usEMVIssAcqCVM" );
    return(E_CVM_FAILED);
}

EMVBoolean bIsCardBlackListed(byte * pan, unsigned short panLen, byte * panSeqNo, unsigned short panSeqLen)
{
    dlog_msg("Checking blacklist result %d", g_CEMV.isCardBlackListed());
    return g_CEMV.isCardBlackListed();
    //return EMV_FALSE;

}

unsigned short usGetExtAuthDecision(unsigned short usStatusWord)
{
    dlog_alert("External Authenticate has failed, error %04Xh", usStatusWord);
    if (usStatusWord == 0x6985)
    {
        return g_CardAppConfig.GetExtAuthFlag();
    }
    dlog_msg("Returning %04X", EMV_FAILURE);
    return EMV_FAILURE;
}

unsigned short usGetPOSDecision (void)
{
//    SVC_WAIT(100);
    if (g_CEMV.GetPINBypassStatus() != 0)
    {
        dlog_alert("PIN Bypass status changed to %02Xh!", g_CEMV.GetPINBypassStatus());
        return g_CEMV.GetPINBypassStatus();
    }
    return 0;
}


unsigned short usGetTermAVN(srMultiAVN *pszTermAVN, int *iCount)
{
	short TagLen = 0;
	unsigned char AVN[2];
	srMultiAVN *currentAVN;
	int i = 0;
	
	dlog_msg("setting more AVN to check");
	if(usEMVGetTLVFromColxn(TAG_9F09_TERM_VER_NUM, AVN, &TagLen) == EMV_SUCCESS)
	{
		if(*iCount > i)
		{	
			currentAVN = &pszTermAVN[i];
			memcpy(currentAVN->stAVN, AVN, MAX_AVN_LEN);
			i++;
		}
		
	}
	if(*iCount > i)
	{	
		currentAVN = &pszTermAVN[i];
		g_CardAppConfig.GetSecondAVN(AVN);
		memcpy(currentAVN->stAVN, AVN, MAX_AVN_LEN);
		i++;
	}
		
	return EMV_SUCCESS;

}


void InitMinimumTLVset(void)
{
    byte Data[] = {0, 0, 0, 0, 0, 0};
    
    dlog_msg("setting minimum TLV set neceseary for proper operation: %X, %X, %X, %X, %X, %X",
        TAG_81_AMOUNT_AUTH, TAG_9F02_AMT_AUTH_NUM, TAG_9F03_AMT_OTHER_NUM, 
        TAG_9F04_AMT_OTHER_BIN, TAG_5F57_ACCOUNT_TYPE, TAG_9C00_TRAN_TYPE);
    
    usEMVUpdateTLVInCollxn(TAG_81_AMOUNT_AUTH, Data, BINARY_AMT_SIZE);
    usEMVUpdateTLVInCollxn(TAG_9F02_AMT_AUTH_NUM, Data, NUMERIC_AMT_SIZE);
    usEMVUpdateTLVInCollxn(TAG_9F03_AMT_OTHER_NUM, Data, NUMERIC_AMT_SIZE);
    usEMVUpdateTLVInCollxn(TAG_9F04_AMT_OTHER_BIN, Data, BINARY_AMT_SIZE);
    usEMVUpdateTLVInCollxn(TAG_5F57_ACCOUNT_TYPE, Data, TRAN_TYPE_SIZE);
    usEMVUpdateTLVInCollxn(TAG_9C00_TRAN_TYPE, Data, TRAN_TYPE_SIZE);

}

int GetAIDPair(void)
{
    int hAIDHandle  = 0;
    int iHeaderSize     = DAT_HEADER_SIZE;
    char szTempBuff[AID_REC_SIZE] = {0};
    long lnSeekResult   = 0;
    int iAIDIndex = 0;

    hAIDHandle = open(AID_FILENAME,O_RDONLY);

    if(hAIDHandle > 0)
    {
        lnSeekResult = lseek (hAIDHandle, iHeaderSize, SEEK_SET);
        
        if (lnSeekResult != iHeaderSize)  
        {
            close(hAIDHandle);
            return CARD_APP_RET_FAIL;
        }
        memset(g_CardAppConfig.srAID[iAIDIndex].szAID1, 0x00, sizeof(g_CardAppConfig.srAID[iAIDIndex].szAID1)); 
        memset(g_CardAppConfig.srAID[iAIDIndex].szAID2, 0x00, sizeof(g_CardAppConfig.srAID[iAIDIndex].szAID2));

        while((read(hAIDHandle, szTempBuff, AID_REC_SIZE) == AID_REC_SIZE) && (iAIDIndex < MAX_AID_REC))
        {
            strncpy((char*)g_CardAppConfig.srAID[iAIDIndex].szAID1, szTempBuff, AID_SIZE);
            strncpy((char*)g_CardAppConfig.srAID[iAIDIndex].szAID2, szTempBuff+AID_SIZE+1, AID_SIZE);

            iAIDIndex++;

            memset(g_CardAppConfig.srAID[iAIDIndex].szAID1, 0x00, sizeof(g_CardAppConfig.srAID[iAIDIndex].szAID1)); 
            memset(g_CardAppConfig.srAID[iAIDIndex].szAID2, 0x00, sizeof(g_CardAppConfig.srAID[iAIDIndex].szAID2)); 
            memset(szTempBuff, 0, sizeof(szTempBuff));
        }
        close(hAIDHandle);
    }
    else
    {
        memset(g_CardAppConfig.srAID[0].szAID1, 0x00, sizeof(g_CardAppConfig.srAID[0].szAID1)); 
        memset(g_CardAppConfig.srAID[0].szAID2, 0x00, sizeof(g_CardAppConfig.srAID[0].szAID2));
    }
    dlog_msg(">>>GetAIDPair<<< %d/%d AID pairs found", iAIDIndex, MAX_AID_REC);
    return CARD_APP_RET_OK;

}

int processEvents(com_verifone_pml::eventset_t & pml_events, 
                   char aid[], size_t &aidSize,
                   char label[], size_t &labelSize)
{
    int result = 0;
    for (eventset_t::iterator it = pml_events.begin() ; it != pml_events.end(); ++it)
    {
        if (it->event_id == events::icc)
        {
            if(isCardRemoved())
            {
                  dlog_alert("Card removed!");
                  result = APP_SEL_CARD_REMOVED;
                  break;
            }
        }
        else if (it->event_id == events::ipc)
        {
            dlog_msg("Message received - checking if not cancel...");
            CCardAppConfig::cmd_break_t break_status = g_CardAppConfig.getAppSelection(aid, aidSize, label, labelSize);
            if (break_status == CCardAppConfig::CMD_BREAK_CANCEL_E)
            {
                result = APP_SEL_POS_CANCELLED;
                break;
            }
            else if (break_status == CCardAppConfig::CMD_BREAK_NO_BREAK_E)
            {
                result = 1;
            }
        }
        else if (it->event_id == events::cascade)
        {
            eventset_t cascade_events;
            int cascadeEventCount = event_read((*it).evt.cascade.fd, cascade_events);
            result = processEvents(cascade_events, aid, aidSize, label, labelSize);
        }
    }
    pml_events.clear();
    return result;
}
//usCandListModify - as required for UK "Chip&PIN" project
/************************************************************
* usCandListModify: 
*
* This is a callback API which gets invoked 
* from the module during the Application selection. This API
* compares the candidate list received with the structure 
* srAID which contains pair of AIDs. If both the AIDs in a pair
* exists in the candidatelist then the first AID is marked as 
* invalid in the candidatelist
*************************************************************/
unsigned short usCandListModify_Default(srAIDList * candidateList)
{
    int AIDCount = 0;
    int excludedAIDCount = 0;
    int iMatchFound   = 0;
    EMV_AID srEMVAID = {0}, srTempAID = {0};
    int iAIDIndex = 0, iCandListIndex = 0, iIndexToModify = 0;
    char szAID[AID_SIZE+1] = {0};

    dlog_msg("usCandListModify, we have %d AIDs", candidateList->countAID);
    if(candidateList)
    {
        // Take one record of srAID structure, compare the two AIDS
        // across the candidate list. If the matchcount is 2, 
        // i.e; if both the AIDS of one record occur in candidate list then
        // mark the first AID of the pair in srAID as invalid in candidate list
        
        AIDCount = candidateList->countAID;

        //iAIDIndex - index in stAID table
        //iCandListIndex - index in candidate list table

        iAIDIndex = 0;
        while((g_CardAppConfig.srAID[iAIDIndex].szAID1[0] != 0) && (g_CardAppConfig.srAID[iAIDIndex].szAID2[0] != 0))
        {
            iMatchFound = 0;
            dlog_msg("Checking AID pair: %s:%s", g_CardAppConfig.srAID[iAIDIndex].szAID1, g_CardAppConfig.srAID[iAIDIndex].szAID2);
            for (iCandListIndex = 0; iCandListIndex < AIDCount; iCandListIndex++)
            {
                srEMVAID = candidateList->arrAIDList[iCandListIndex];
                
                // Convert from Hex to Ascii
                memset(szAID,0x00,sizeof(szAID));
                SVC_HEX_2_DSP((const char*)srEMVAID.stAID, szAID, srEMVAID.lenOfAID);
                dlog_msg("Processing AID: %s index: %d", szAID, iCandListIndex);

                //typically AID from candidate list len is >= AID from the list so we cut it to the size of the list
                
                if (0 == strncmp(const_cast<char*>(szAID), const_cast<char*>(g_CardAppConfig.srAID[iAIDIndex].szAID1), strlen(const_cast<char*>(g_CardAppConfig.srAID[iAIDIndex].szAID1)))) 
                {
                    iIndexToModify = iCandListIndex;
                    iMatchFound++;
                }
                
                if (0 == strncmp(const_cast<char*>(szAID), const_cast<char*>(g_CardAppConfig.srAID[iAIDIndex].szAID2), strlen(const_cast<char*>(g_CardAppConfig.srAID[iAIDIndex].szAID2))))
                {
                    iMatchFound++;
                }
            }

            if (2 == iMatchFound)
            {
                dlog_msg("Removing AID index %d", iIndexToModify);
                candidateList->arrAIDList[iIndexToModify].bExcludedAID = TRUE;
                ++excludedAIDCount;
                //candidateList->arrAIDList[iIndexToModify].bExcludedAID = TRUE;
                // srTempAID = candidateList->arrAIDList[iIndexToModify];
                // srTempAID.inCandidateList = FALSE;
                //srTempAID.bExcludedAID = TRUE;
                // candidateList->arrAIDList[iIndexToModify] = srTempAID;
            }
            iAIDIndex++;
        }
        if (g_CardAppConfig.IsExternalApplicationSelectionEnabled() && AIDCount - 1 > excludedAIDCount)
        {
            dlog_alert("Performing external application selection!");
            IPCPacket response;
/*          const int APP_LABEL_TS = 1;
            const uint8_t arbyAppLabel[APP_LABEL_TS] = { 0x50 };
            const int APP_PREF_NAME_TS = 2;
            const uint8_t arbyAppPrefName[APP_PREF_NAME_TS] = { 0x9F, 0x12 };
            const int APP_AID_TS = 2;
            const uint8_t arbyAppAID[APP_AID_TS] = { 0x9F, 0x06 }; */

            response.setCmdCode(EXTERNAL_APP_SEL_EVT);
            for (int i=0; i < AIDCount; i++)
            {
                if (candidateList->arrAIDList[i].bExcludedAID == FALSE)
                {
                    using namespace com_verifone_TLVLite;
                    response.addTag(ConstData(arbyAppAID, APP_AID_TS), ConstData(candidateList->arrAIDList[i].stAID, candidateList->arrAIDList[i].lenOfAID));
                    response.addTag(ConstData(arbyAppLabel, APP_LABEL_TS), ConstData(candidateList->arrAIDList[i].stAppLabel, candidateList->arrAIDList[i].appLabel));
                    if (candidateList->arrAIDList[i].appPreferName)
                    {
                        response.addTag(ConstData(arbyAppPrefName, APP_PREF_NAME_TS), ConstData(candidateList->arrAIDList[i].stAppPreferName, candidateList->arrAIDList[i].appPreferName));
                    }
                }
            }
            response.setAppName(g_CardAppConfig.cNotifyName);
            response.send();
            // wait for response here and process it
            char aid[MAX_AID_LEN];
            size_t aidSize = sizeof(aid);
            char label[MAX_LABEL_LEN];
            size_t labelSize = sizeof(label);
            event_item ev_icc;
            ev_icc.event_id = events::icc;
            int result = 0;
            int events_mon = event_open();
            eventset_t pml_events;

            event_ctl(events_mon, ctl::ADD, ev_icc);
            event_ctl(events_mon, ctl::ADD, com_verifone_ipc::get_events_handler(g_CardAppConfig.cNotifyName));
            do
            {
                dlog_msg("APP SEL: Waiting for events");

                if(event_wait(events_mon, pml_events) > 0)
                {
                    dlog_msg("We have events!");
                    result = processEvents(pml_events, aid, aidSize, label, labelSize);
                }
            } while(result == 0);
            event_close(events_mon);
            if (result < 0)
            {
                dlog_alert("Cancel received or card removed, cancelling transaction (clearing all AIDs)!");
                aidSize = labelSize = 0;
                g_CEMV.SetAppSelStatus(APP_SEL_STATUS_CANCELLED);
                for (int i=0; i<AIDCount; i++)
                {
                    if (candidateList->arrAIDList[i].bExcludedAID == FALSE)
                    {
                        candidateList->arrAIDList[i].bExcludedAID = TRUE;
                        ++excludedAIDCount;
                    }
                }
            }
            else if (aidSize && labelSize && !isCardRemoved())
            {
                for (int i=0; i<AIDCount; i++)
                {
                    if (candidateList->arrAIDList[i].bExcludedAID == FALSE)
                    {
                        if (candidateList->arrAIDList[i].lenOfAID != aidSize || memcmp(candidateList->arrAIDList[i].stAID, aid, aidSize) &&
                            candidateList->arrAIDList[i].appLabel != labelSize || memcmp(candidateList->arrAIDList[i].stAppLabel, label, labelSize))
                        {
                            dlog_msg("Removing AID index %d", i);
                            candidateList->arrAIDList[i].bExcludedAID = TRUE;
                            ++excludedAIDCount;
                        }
                    }
                }
            }
        }
        // Only if we have excluded at least one app and there is only one app left 
        if (excludedAIDCount && (AIDCount - 1 == excludedAIDCount))
        {
            // WORKAROUND for Kernel stupid bug - even if one application stays, Kernel still requires confirmation, regardless of real need for it
            // (Confirmation is required ONLY if b8 of Application Priority Indicator is set)
            dlog_msg("App Sel workaround");
            bool shouldBypassSelection = true;
            int i;
            for (i=0; i < AIDCount; i++)
            {
                if (candidateList->arrAIDList[i].bExcludedAID == FALSE)
                {
                    if (candidateList->arrAIDList[i].appPriority & 0x80)
                    {
                        dlog_alert("Confirmation is REQUIRED for app no %d, label '%s'", i, candidateList->arrAIDList[i].stAppLabel);
                        shouldBypassSelection = false;
                        break;
                    }
                }
            }
            if (shouldBypassSelection)
            {
                for (i=0; i < AIDCount; i++)
                {
                    if (candidateList->arrAIDList[i].bExcludedAID == TRUE)
                    {
                        // Re-enable one application and set its label to ' '
                        dlog_msg("Working around Kernel issue, no confirmation is necessary for app %d labeled '%s'", i, candidateList->arrAIDList[i].stAppLabel);
                        strcpy((char *)candidateList->arrAIDList[i].stAppLabel, " "); // Set label to ' '
                        candidateList->arrAIDList[i].appLabel = 1; // adjust label length
                        candidateList->arrAIDList[i].appPriority = 0x8F; // Set to lowest priority, to minimise possibility of selection of this app
                        candidateList->arrAIDList[i].bExcludedAID = FALSE; // Re-enable fake application, so that Menu function gets two apps - one is fake, so it knows to skip selection
                        break;
                    }
                }
            }
        }
    }
    return EMV_SUCCESS; 
}

void SetDefaultFunctionPointers(void)
{
//---------------------------------------------------------------------
// getLastTxnAmt()    GET_LAST_TXN_AMT
//---------------------------------------------------------------------
//    The application should call
//    inVxEMVApSetFunctionality(GET_LAST_TXN_AMT, getLastTxnAmt)
//    API during transaction initialization.
//    This API returns the last transaction amount for a card with
//    the same PAN as the card inserted for the current
//    transaction, and returns an amount equal to 0 if the record
//    does not exist for the same PAN.
    inVxEMVAPSetFunctionality(GET_LAST_TXN_AMT, NULL);

//---------------------------------------------------------------------
// getTerminalParam()    GET_TERMINAL_PARAMETER
//---------------------------------------------------------------------
//    The application should call
//    inVxEMVApSetFunctionality(GET_TERMINAL_PARAMETER, NULL)
//    API during transaction initialization.
//    This API provides all the required data for TAC (Terminal
//    Action Codes), and also contains data for the default DDOL
//    and TDOL.
    inVxEMVAPSetFunctionality(GET_TERMINAL_PARAMETER, NULL);

//---------------------------------------------------------------------
// getCapkExp()    GET_CAPK_DATA
//---------------------------------------------------------------------
//    The application should call
//    inVxEMVApSetFunctionality(GET_CAPK_DATA,NULL)
//    API to use default implementation in the EMV Module.
//    This API obtains the CAPK Modulus and Exponent required
//    for SDA, DDA, CDA and enciphered PIN off-line. This is
//    performed by opening the relevant CAPK configuration file
//    and extracting the key in binary form.
//    usEMV_Set_Functionality(GET_CAPK_DATA, (void *)&getAppCapkExp);
    inVxEMVAPSetFunctionality(GET_CAPK_DATA, NULL);

//---------------------------------------------------------------------
// getUsrPin()    GET_USER_PIN
//---------------------------------------------------------------------
//    The application should call
//    inVxEMVApSetFunctionality(GET_USER_PIN,getUsrPIN)
//    API during transaction initialization.
//    This API is called as a part of CVM. It obtains the PIN to
//    complete CVM processing.
    inVxEMVAPSetFunctionality(GET_USER_PIN, (void *)&getUsrSecurePin);

//---------------------------------------------------------------------
// usEMVDisplayErrorPrompt()    DISPLAY_ERROR_PROMPT
//---------------------------------------------------------------------
//    The application should call
//    inVxEMVApSetFunctionality(DISPLAY_ERROR_PROMPT,
//                              usEMVDisplayErrorPrompt)
//    API during transaction initialization.
//    This API should be implemented by the application.
    inVxEMVAPSetFunctionality(DISPLAY_ERROR_PROMPT, (void *)&usEMVDisplayErrorPrompt);

//---------------------------------------------------------------------
// usEMVPerformSignature()    PERFORM_SIGNATURE
//---------------------------------------------------------------------
//    The application should call
//    inVxEMVApSetFunctionality(PERFORM_SIGN, usEMVPerformSignature)
//    API during transaction initialization.
//    This API should be implemented by the application.
    inVxEMVAPSetFunctionality(PERFORM_SIGNATURE, (void *)&usEMVPerformSignature);

//---------------------------------------------------------------------
// usEMVPerformOnlinePIN()    PERFORM_ONLINE_PIN
//---------------------------------------------------------------------
//    The application should call 
//    inVxEMVApSetFunctionality(PERFORM_ONLINE_PIN,
//                              usEMVPerformOnlinePIN)
//    API during transaction initialization.
//    This API should be implemented by the application.
//    Note: Online PIN validation is not a part of the EMV Module.
    inVxEMVAPSetFunctionality(PERFORM_ONLINE_PIN, (void *)&usEMVPerformOnlinePIN);

//---------------------------------------------------------------------
// usEMVIssAcqCVM()    PERFORM_ISS_ACQ_CVM
//---------------------------------------------------------------------
//    The application should call
//    inVxEMVApSetFunctionality(PERFORM_ISS_ACQ_CV M, usEMVIssAcqCVM)
//    API during transaction initialization.
//    This API should be implemented by the application.
    inVxEMVAPSetFunctionality(PERFORM_ISS_ACQ_CVM, (void*)&usEMVIssAcqCVM);


//---------------------------------------------------------------------
// bIsMultipleOccurencesAllowed()    IS_PARTIAL_SELECT_ALLOWED
//---------------------------------------------------------------------
//    The application should call
//    inVxEMVApSetFunctionality(IS_PARTIAL_SELECT_ALLOWED, NULL)
//    API to use default implementation in the EMV Module.
//    This API is used by the library during the selection to allow
//    partial selection of AIDs. This API should be implemented by
//    the application.
    inVxEMVAPSetFunctionality(IS_PARTIAL_SELECT_ALLOWED, NULL);

    inVxEMVAPSetFunctionality(IS_CARD_BLACKLISTED, (void *)&bIsCardBlackListed);
//    usEMV_Set_Functionality(IS_CARD_BLACKLISTED, (void *)&bIsCardBlackListed);

    inVxEMVAPSetFunctionality(Get_EXT_AUTH_DECISION, (void *)&usGetExtAuthDecision);

    inVxEMVAPSetFunctionality(GET_POS_CVM_DECISION, (void *)&usGetPOSDecision);

    inVxEMVAPSetFunctionality(GET_TERMINAL_AVN, (void *)&usGetTermAVN);
}


bool isCardRemoved()
{
    if(1)
    {
        int stat = Get_Card_State( g_CEMV.ICC_reader );
        if (stat == E_SLOT_INITIALIZE)
        {
            stat = Init_CardSlot( g_CEMV.ICC_reader );
            if (stat == CARDSLOT_SUCCESS)
            {
                stat = Get_Card_State( g_CEMV.ICC_reader );
            }
            else
            {
                dlog_error("Cardslot initialisation error %d", stat);
            }
        }
        if(stat == CARD_ABSENT)
        {
            dlog_msg("CARD REMOVED!");
            return true;
        }
    }
    return false;
}

bool isCardInserted()
{
    if(1)
    {
        int stat = Get_Card_State( g_CEMV.ICC_reader );
        if (stat == E_SLOT_INITIALIZE)
        {
            stat = Init_CardSlot( g_CEMV.ICC_reader );
            if (stat == CARDSLOT_SUCCESS)
            {
                stat = Get_Card_State( g_CEMV.ICC_reader );
            }
            else
            {
                dlog_error("Cardslot initialisation error %d", stat);
            }
        }
        if(stat == CARD_PRESENT)
        {
            dlog_msg("CARD INSERTED!");
            return true;
        }
    }
    return false;
}



int isSeparateGPO()
{
    int res = -1;
    MVT_REC mvt_rec;
    if (!loadMVTRec(&mvt_rec))
    {
        return res;
    }
    dlog_msg("RFU1: %d", mvt_rec.shRFU1);
    if (mvt_rec.shRFU1 & 0x80) res = 1;
    else res = 0;
    dlog_msg("SeparateGPO %d", res);
    return res;
}

#ifdef VFI_GUI_GUIAPP
static void getScreenTextDimensions(int &maxCharsInLine, int &maxLines)
{
    static int x = 0, y = 0;
    if (x == 0 || y == 0)
    {
        int x_font, y_font;
        int x_screen = 0, y_screen = 0;
        switch(getgrid())
        {
            case 0:
                x_font = 8;
                y_font = 16;
                break;
            case 2:
                x_font = 6;
                y_font = 8;
                break;
            default:
                y_font = 8;
                x_font = 8;
                break;
        }
        //get_display_resolution(x_screen, y_screen);
        get_character_resolution(x, y);
		/*
        dlog_msg("Screen res: %d x %d, font x %d, y %d", x_screen, y_screen, x_font, y_font);
		x = x_screen / x_font;
        y = y_screen / y_font;
        */
    }
    maxCharsInLine = x;
    maxLines = y;
}

int getScreenMaxX()
{
    int x, y;
    getScreenTextDimensions(x, y);
    return x;
}

int getScreenMaxY()
{
    int x, y;
    getScreenTextDimensions(x, y);
    return y;
}
#endif

bool setPINParams()
{
    ::srPINParams psParams;

    if (g_CardAppConfig.GetPinX() == 0 || g_CardAppConfig.GetPinY() == 0)
    {
        cPINdisplay * pPINdisplay = g_CEMV.getPINDisplayObj();
        //if (pPINdisplay) pPINdisplay->SetEntryXY(g_CardAppConfig.GetPinX(), g_CardAppConfig.GetPinY());
        if (pPINdisplay)
        {
            dlog_msg("Computing PIN box coordinates");
            pPINdisplay->ComputeEntryXY();
            g_CardAppConfig.SetPinX(pPINdisplay->GetEntryX());
            g_CardAppConfig.SetPinY(pPINdisplay->GetEntryY());
        }
    }
    /*else
    {
        cPINdisplay * pPINdisplay = g_CEMV.getPINDisplayObj();
        if (pPINdisplay) pPINdisplay->SetEntryXY(g_CardAppConfig.GetPinX(), g_CardAppConfig.GetPinY());
    } */
    if (g_CardAppConfig.GetPinX() == 0 || g_CardAppConfig.GetPinY() == 0)
    {
        dlog_msg("Using hardcoded coordinates!");
        g_CardAppConfig.SetPinX(6);
        g_CardAppConfig.SetPinY(6);
    }
    dlog_msg("PIN entry box coords: x = %d, y = %d", g_CardAppConfig.GetPinX(), g_CardAppConfig.GetPinY());

    memset(&psParams, 0, sizeof(psParams));

    psParams.ucMin = 4;
    psParams.ucMax = 12;
    psParams.ucEchoChar = '*';
    psParams.ucDefChar = g_CardAppConfig.GetPINblankChar();
    //psParams.ucDspLine = 4; //g_CardAppConfig.GetPinY();
    //psParams.ucDspCol = 8; //JACEK - fix that!//(maxX- psParams.ucMax)/2;   //g_CardAppConfig.GetPinX();
    psParams.ucDspLine = g_CardAppConfig.GetPinY();
    psParams.ucDspCol = g_CardAppConfig.GetPinX();
    //psParams.ucOption = 0x12;
    psParams.ucOption = g_CardAppConfig.GetPINoption(); // Bit 3 is set by default
    if (g_CardAppConfig.GetOnlinePINCancelAllowed())
        psParams.ucOption |= 0x02; // bit 1
    if (g_CardAppConfig.GetOnlinePINEntryType() != CCardAppConfig::PIN_ENTRY_TYPE_MANDATORY_E)
        psParams.ucOption |= 0x10; // bit 4


    psParams.ulFirstKeyTimeOut = g_CardAppConfig.GetPINfirstcharTimeout() * 1000;
    psParams.ulInterCharTimeOut = g_CardAppConfig.GetPINintercharTimeout() * 1000;
    psParams.ulWaitTime = g_CardAppConfig.GetPINtimeout() * 1000;
    dlog_msg("First char timeout %d, inter char timeout %d, PIN timeout %d", psParams.ulFirstKeyTimeOut, psParams.ulInterCharTimeOut,psParams.ulWaitTime);
    if (g_CardAppConfig.GetPINfirstcharTimeout() && (g_CardAppConfig.GetPINfirstcharTimeout() > g_CardAppConfig.GetPINtimeout()))
    {
        dlog_alert("Kernel bug workaround: First char timeout (%d) is greater than PIN timeout (%d)!", g_CardAppConfig.GetPINfirstcharTimeout(), g_CardAppConfig.GetPINtimeout());
        psParams.ulFirstKeyTimeOut = g_CardAppConfig.GetPINtimeout() * 1000;
    }

    psParams.abortOnPINEntryTimeOut = 1; //always cancel

    if (g_CardAppConfig.IsPINBypassEnabled())
    {
        /*unsigned char bpKey = g_CardAppConfig.GetPINBypassKey();
        dlog_msg("Cardholder PIN bypass enabled, key %02Xh", bpKey);        
        if (bpKey != ENTER_KEY_PINBYPASS && bpKey != CLEAR_KEY_PINBYPASS)
        {
            dlog_alert("Invalid PIN bypass key, changing to default (%02X)", ENTER_KEY_PINBYPASS);
            bpKey = ENTER_KEY_PINBYPASS;
        }
        psParams.ucPINBypassKey = bpKey; */
        psParams.ucPINBypassKey = g_CardAppConfig.GetPINBypassKey();
        if (psParams.ucPINBypassKey == CLEAR_KEY_PINBYPASS)
        {
            psParams.ucOption |= 0x10; // set bit 4
        }
    }
    else
    {
        // Kamil_P1: The below doesn't work - sets bypass key to 0x08 (!!!!)
        //           This is because NO_PINBYPASS is defined as (Ushort) 0x9108
        //psParams.ucPINBypassKey = static_cast<unsigned char>(NO_PINBYPASS);
        psParams.ucPINBypassKey = 0;
    }
    #if 0
    dlog_msg("Setting PIN bypass key to %02X", ENTER_KEY_PINBYPASS);
    psParams.ucPINBypassKey = ENTER_KEY_PINBYPASS;  // Bksp + 1
    #endif
    psParams.ucSubPINBypass = EMV_TRUE;

    //Use this API to set the PIN parameters.
    int result = usEMVSetPinParams(&psParams);
    dlog_msg("usEMVSetPINParams result %04Xh", result);
    return (result == EMV_SUCCESS);
}


void displaySecurePINPrompt()
{
    // display PIN prompt using GuiApp or manually
    dlog_msg("displaySecurePINPrompt");
    g_CardAppConfig.SetPINtype(CARD_APP_SECURE_PIN_OFFLINE);
    g_CEMV.OfflinePINVerification();
    // do we have to run thread?

    if (g_CardAppConfig.GetPINbeeperTimeout() > 0)
    {
        dlog_alert("PIN Beeper is configured, starting beeper thread!");
        g_CEMV.runBeeperThread();
    }
    
    return;
}

#define OVERLAY_SENSE_SCAN          0x02
#define FW_CHECK_IN_PROGRESS        0x02
#define MODEL_VX600		"VX600"

#ifdef VFI_GUI_GUIAPP
/*
int get_console_handle()
{
    int m_console = get_console(1);
    int iRet = 0;
    char model_number[12+1];

    SVC_INFO_MODELNO (model_number);
    model_number[12]=0;
    dlog_msg("MODEL NUMBER: %s", model_number);
    
    if (m_console < 0)
    {
        dlog_alert("get_console() failed, errno %d", errno);
        int consoleOwner = 0;
        m_console = get_owner(DEV_CONSOLE, &consoleOwner);
        dlog_msg("Console owner task id %d, our id %d, handle %d", consoleOwner, get_task_id(), m_console);
        if (consoleOwner == 0)
        {
            m_console = open(DEV_CONSOLE, 0);
            dlog_msg("Console open result %d", m_console);
        }
        else if(consoleOwner != get_task_id())
        {
          //console is not available.. checking for 3 times more...
          int i = 0;
          m_console = -1;
          while(i++ < 4 && m_console < 0)
          {
              if((m_console = open(DEV_CONSOLE, 0)) != -1)
              	break;
              SVC_WAIT(100);
          }
        }
        if(m_console >= 0)
        {
            iRet = cs_set_sleep_state(0);
            dlog_msg("Vx600: cs_set_sleep_state(0) RET: %d", iRet);
        }
    }
    if(m_console >= 0)
    {
        if(memcmp(model_number, MODEL_VX600, strlen(MODEL_VX600)) == 0)
        {
            iRet = cs_overlay_scan(OVERLAY_SENSE_SCAN);
            dlog_msg("vx600: cs_overlay_scan(0x02) RET: %X", iRet);
            int i = 0;
            while(i++ < 4 && iRet != 0)
            {
                if((iRet = cs_overlay_scan(OVERLAY_SENSE_SCAN)) < 0)
                	break;
                SVC_WAIT(100);
            }
            //this code has problems on Vx600 Gen2.5
            
            //if(iRet == 0)
            //{
            //    do
            //    {
            //        iRet = cs_spi_cmd_status();
            //        SVC_WAIT(100); // Be battery-friendly
            //    } while (iRet == 0);
            //}
            //else
            //{
            //    return -1;
            //}
            
            do
            {
                if(((iRet = is_keypad_secure()) & FW_CHECK_IN_PROGRESS) == 0)
                	break;
                
                SVC_WAIT(100); // Be battery-friendly
            } while (iRet & FW_CHECK_IN_PROGRESS);
            dlog_msg("Vx600: is_keypad_secure() RET: %X", iRet);

            iRet = cs_set_sleep_state(0);
            dlog_msg("Vx600: cs_set_sleep_state(0) RET: %d", iRet);
        }
    }

    return m_console;
}
*/
void close_console(void)
{
    int consoleOwner = 0;
    int m_console = get_owner(DEV_CONSOLE, &consoleOwner);

    if (consoleOwner == get_task_id())
    {
        dlog_msg("We have console, closing");
        close(m_console);
    }
}

/* internal function, formats text (\c -> centers, \n\r -> newlines) */
static int FormatText(char *text, int &length, const int max_length)
{
    dlog_msg("Input: '%s'", text);

    char * pCenter = strstr(text, "\\c");
    if (pCenter)
    {
        const int maxX = getScreenMaxX();
        char output[DISPLAY_TEXT+1];
        memset(output, 0, sizeof(output));
        int outputIndex = 0;
        char * pText = text;
        while (pCenter)
        {
            int diff = 0;
            if (*pCenter == '\\')
            {
                diff = 2;
                pCenter += diff;
            }
            else
            {
                diff = 1;
                ++pCenter;
            }
            if (outputIndex >= sizeof(output))
            {
                dlog_error("Overflow!");
                return 0;
            }
            int beforeCenterSize = pCenter - pText - diff;
            if (beforeCenterSize > 0)
            {
                memcpy(output+outputIndex, pText, beforeCenterSize);
                outputIndex += beforeCenterSize;
            }
            if (outputIndex && *(output+outputIndex-1) != static_cast<char>(0x0a))
            {
                // Add newline to center next data
                *(output+outputIndex) = static_cast<char>(0x0a);
                ++outputIndex;
            }
            pText = pCenter;
            pCenter = strstr(pText, "\\c");
            int len;
            if (pCenter)
            {
                len = pCenter - pText;
            }
            else
            {
                pCenter = strchr(pText, '\n');
                if (pCenter) len = pCenter - pText + 1;
                else len = strlen(pText);
            }

            if (diff == 2)
            {
                int numSpaces = (maxX - len) >> 1;
                if (numSpaces + outputIndex + len >= sizeof(output))
                {
                    dlog_error("Overflow!");
                    return 0;
                }
                while (numSpaces--)
                {
                    *(output+outputIndex) = ' ';
                    ++outputIndex;
                }
            }
            memcpy(output+outputIndex, pText, len);
            outputIndex += len;
            pText = pCenter;
        }
        dlog_msg("Output: '%s'", output);
        strncpy(text, output, max_length);
        length = strlen(output);
    }
    return length;
}

int DisplayText(const char * const text, int text_len, bool clear_screen /* = true */)
{
    return DisplayText(text, text_len, -1, -1, clear_screen);
}

int DisplayText(const char * const text, int text_len, int at_x, int at_y, bool clear_screen /* = false */)
{
    int res = -1;
    int m_console = 1;  //Jecek - change structure here
    if (m_console >= 0)
    {
        char textLocal[DISPLAY_TEXT+1];
        textLocal[DISPLAY_TEXT] = 0;
        strncpy(textLocal, text, DISPLAY_TEXT);
        int localLen = strlen(textLocal);
        if (clear_screen) clrscr();
        FormatText(textLocal, localLen, DISPLAY_TEXT);
        if (at_x == -1 || at_y == -1)
            write(m_console, textLocal, localLen);
        else
            write_at(textLocal, localLen, at_x, at_y);
        res = 1;
    }
    return res;
}

int DisplayPromptSelectSection(int prompt_number, const char * const section, bool clear_screen /* = true */)
{
    return DisplayPromptSelectSection(prompt_number, section, -1, -1, clear_screen);
}

int DisplayPromptSelectSection(int prompt_number, const char * const section, int at_x, int at_y, bool clear_screen /* = false */)
{
    int res = -1;
    int m_console = get_console_handle();
    if (m_console >= 0)
    {
        userPromptLite prompt;
        char text[DISPLAY_TEXT+1];
        text[DISPLAY_TEXT] = 0;
        dlog_msg("Display manual prompt: section %s, prompt %d", section, prompt_number);
        if (prompt.getPrompt(section, prompt_number, text, sizeof(text)-1 ) <= 0)
        {
            res = 1;
            switch(prompt_number)
            {
                case CARD_APP_GUI_INSERT: strncpy(text, "\tInsert card", DISPLAY_TEXT); break;
                case CARD_APP_GUI_PLEASE_WAIT: strncpy(text, "\tPlease wait...", DISPLAY_TEXT); break;
                case CARD_APP_GUI_PROCESSING: strncpy(text, "\tProcessing", DISPLAY_TEXT); break;
                case CARD_APP_GUI_REMOVE: strncpy(text, "\tRemove card", DISPLAY_TEXT); break;
                case CARD_APP_GUI_ENTER_KEY: strncpy(text, "\tPress enter", DISPLAY_TEXT); break;
                default: strncpy(text, "\tINVALID SCREEN!", DISPLAY_TEXT); res = 0; break;
            }
        }
        dlog_msg("Retrieved prompt: '%s'", text);
        int text_len = strlen(text);
        if (clear_screen) clrscr();
        FormatText(text, text_len, DISPLAY_TEXT);
        if (at_x == -1 || at_y == -1)
            write(m_console, text, text_len);
        else
            write_at(text, text_len, at_x, at_y);
    }
    else
    {
        dlog_error("Cannot obtain the console!");
    }
    return res;
}
#endif
