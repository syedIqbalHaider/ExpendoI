#ifndef __cplusplus
#  error "This file is for C++ only!"
#endif

/*****************************************************************************
 * 
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/
/**
 * @file       cEMV.cpp
 *
 * @author     Jacek Olbrys
 * 
 * @brief      Implementation of the EMV services.
 *
 *
 * @remarks    This file should be compliant with Verifone EMEA R&D C++ Coding
 *             Standard 1.0.x  
 */

/***************************************************************************
 * Includes
 **************************************************************************/
#include <svc.h>
#include <svc_sec.h>
#include <stdlib.h>
#include <string.h>

#include <liblog/logsys.h>
#include <libfoundation/HWInfo.h>

#include <libpml/pml.h>
#include <libpml/pml_abstracted_api.h>
#ifdef VFI_GUI_GUIAPP
#include "GuiApp.h"
#endif

//#ifdef VFI_GUI_DIRECTGUI
#include "Pin_gui.h"
//#endif

#ifdef VFI_GUI_DIRECTGUI
#include "app_selection.h"
#endif

#include "cCARDAppConfig.h"

extern "C"
{
#include "vxemvap.h"
}

#include "cEMV.h"
#include "cTransaction.h"
#include "EMVtools.h"

// Define this if CARDAPP has to perform DOL length checks (EMV Kernel 5.0.6 doesn't do that)
//#define DOL_CHECKS

/***************************************************************************
 * Using
 **************************************************************************/
using namespace com_verifone_emv;
using namespace com_verifone_util;
using namespace com_verifone_pml;
#ifdef VFI_GUI_GUIAPP
using namespace com_verifone_guiapi;
#endif

/***************************************************************************
 * External global variables
 **************************************************************************/
extern  CCardAppConfig   g_CardAppConfig;
extern CEMVtrns     g_CEMVtrns;
extern CEMV         g_CEMV;

/***************************************************************************
 * Exportable function definitions
 **************************************************************************/

int CheckPINresult(unsigned char SW1, unsigned char SW2)
{
	int result = PIN_ENTRY_STATUS_ERROR; // Default to unrecoverable error
	switch(SW1)
	{
		case 0x90:
			if (SW2 == 0x00) result = PIN_ENTRY_STATUS_OK;
			break;
		case 0x63:
			if ( (SW2 & 0xC0) == 0xC0)
			{
				unsigned char counter = SW2 & ~0xC0;
				usEMVUpdateTLVInCollxn(TAG_9F17_PIN_TRY_COUNTER, &counter, sizeof(counter));
				if(SW2 > 0xC1)
				{
					if (g_CardAppConfig.GetShowIncorrectPIN()) result = PIN_ENTRY_STATUS_BAD_PIN;
					else result = PIN_ENTRY_STATUS_OK;
				}
				else if(SW2 == 0xC1)
				{
					result = PIN_ENTRY_STATUS_LAST_TRY;
				}
				else if(SW2 == 0xC0)
				{
					result = PIN_ENTRY_STATUS_BLOCKED;
				}
			}
			break;
		case 0x69: // either 6983 or 6984
			if (SW2 == 0x83 || SW2 == 0x84) result = PIN_ENTRY_STATUS_BLOCKED;
			break;
		default:
			result = PIN_ENTRY_STATUS_ERROR;
	}
	return result;
}


int verify_pin( unsigned char    ucPinType, 
                unsigned char    ucEntryType,
                unsigned short * pusStatus );

//the above function is neceseary for compatibility with libverifypin library
int iPC_GenerateRandom( unsigned char * rnd)
{
    return GenerateRandom(rnd);
}

int inMenuFunc_mini(char **labels, int numLabels)
{
    dlog_msg("MenuFunc called!");
    return 0;
}

void vdPromptManager_mini(unsigned short inCondition)
{
    dlog_msg(">>>PROMPT MANAGER: %X", inCondition);
}

unsigned short usAmtEntryFunc_mini(unsigned long *lnAmountValue)
{
    dlog_msg("AmtEntryFunct called");
    *lnAmountValue = 0;
    return EMV_SUCCESS;
}

int procEvents(eventset_t pml_events)
{
	int result = -1;
	while (!pml_events.empty())
	{
		event_item event = pml_events.back();
		pml_events.pop_back();
		if (event.event_id == events::cascade)
		{
			eventset_t cascadeEvents;
			if (event_read(event.evt.cascade.fd, cascadeEvents) > 0)
			{
				result = procEvents(cascadeEvents);
			}
		}
		else if (event.event_id == events::icc)
		{
			dlog_alert("Card removed!");
			g_CEMV.SetStatusDetail(CARD_APP_STATUS_D_REMOVED);
			g_CEMV.SetStatusMain(CARD_APP_STATUS_M_STOP);
			result = 1; // card removed
			break;
		}
		else if (event.event_id == events::ipc)
		{
			CCardAppConfig::cmd_break_t break_status = g_CardAppConfig.checkPipeStatus();
			if(break_status == CCardAppConfig::CMD_BREAK_CANCEL_E)
			{
				dlog_alert("App selection cancelled, exiting...");
				g_CEMV.SetStatusDetail(CARD_APP_STATUS_D_CANCELLED);
				g_CEMV.SetStatusMain(CARD_APP_STATUS_M_STOP);
				result = 1; // cancelled
				break;
			}
			else if (break_status == CCardAppConfig::CMD_BREAK_GUI_RESP_E)
			{
				dlog_msg("GUIAPP answer");
				result = 0;
				break;
			}
		}
	}
	return result;
}
int EntryCancelled(void *unused)
{
	//dlog_msg("Selection Cancelled!");
	//PML implementation here
	unsigned long ulEventValue = 0;
	int result = 1;
	bool waiting = true;
	int events_mon = event_open();
	eventset_t pml_events;
	int num_events = 0;
	event_item item_event;
	
	if(events_mon > 0)
	{
		event_item event;
		event.event_id = events::icc;
		event_ctl(events_mon, ctl::ADD, event);
		event_ctl(events_mon, ctl::ADD, com_verifone_ipc::get_events_handler());

		do
		{
			dlog_msg("App Selection - Wait for events");
			int cnt = event_wait(events_mon, pml_events);
			if (cnt > 0)
			{
				result = procEvents(pml_events);
				if (result >= 0) waiting = false;
			}
		} while(waiting);
		event_close(events_mon);
	}
	return (result);
}

int getData_loc(int tagNum,unsigned char * value, int *Len)
{
	int iLen = 0;
	unsigned short dLen = 0;
	usEMVGetTLVFromColxn(tagNum, value, &dLen);
	dlog_msg("getData: TAG: %X, len: %d", tagNum, dLen);

	*Len = (int)dLen;
	return (int)dLen;
}
	
int setData_loc(int tagNum,unsigned char * value,int Len)
{
	if(usEMVUpdateTLVInCollxn(tagNum, value, Len) == EMV_SUCCESS)
		return Len;

	return 0;
}

bool isCardpresent_loc()
{
    return isCardInserted();
}

break_ret_t checkBreak_AppSel_loc()
{
	CCardAppConfig::cmd_break_t break_status = g_CardAppConfig.checkPipeStatus();
	break_ret_t result;
	switch (break_status)
	{
		case CCardAppConfig::CMD_BREAK_CANCEL_E: result = Cancelled; break;
		case CCardAppConfig::CMD_BREAK_GUI_RESP_E: result = GuiAppData; break;
		default: // Ignore other stuff
		case CCardAppConfig::CMD_BREAK_NO_BREAK_E: result = NoData; break;
	}
	return result;
}

break_ret_t checkBreak_PIN_loc()
{
	CCardAppConfig::cmd_break_t break_status = g_CardAppConfig.checkCmdBreak();
	break_ret_t result;
	switch (break_status)
	{
		case CCardAppConfig::CMD_BREAK_CANCEL_E: result = Cancelled; break;
		case CCardAppConfig::CMD_BREAK_PIN_BYPASS_E: result = Bypassed; break;
		default:
		case CCardAppConfig::CMD_BREAK_NO_BREAK_E: result = NoData; break;
	}
	return result;
}

int EntryCallBack_ccard(int num_digits, int status, char* filename, char* filename_warning)
{
	std::string empty;
	int iRet = 0;

	dlog_msg("Number of valid PIN digits: %d, entry status: %d", num_digits, status);
	std::string htmlFile = g_CardAppConfig.GetPINHTML(static_cast<size_t>(num_digits));
	if (htmlFile.size() == 0)
	{
		htmlFile = g_CardAppConfig.GetDefaultPINHTML();
	}
	// HTML has to be defined here... but just in case let's make sure
	if (htmlFile.size() == 0)
	{
		htmlFile.assign("EnterPINstart.html");
	}
	strcpy(filename, htmlFile.c_str());
	dlog_msg("Displaying HTML: %s", filename);

	filename_warning[0] = 0;
	if (g_CardAppConfig.GuiInterface() == CCardAppConfig::GUI_FLEXI)
	{
		switch(g_CEMV.GetPINStatus())
		{
			case PIN_ENTRY_STATUS_BAD_PIN:
				strcpy(filename_warning, "IncorrectPINextra.html");
				break;
			case PIN_ENTRY_STATUS_LAST_TRY:
				strcpy(filename_warning, "LastPINextra.html");
				break;
		}
	}

	return iRet;
}


#ifdef VFI_GUI_GUIAPP

int inMenuFunc_GUIAPP(char **labels, int numLabels)
{
	int iRet = 0;
	int i;
	long evt;

	dlog_msg("MenuFunc called for %d items!", numLabels);

	//Check for Kernel bug - after application selection, Kernel always calls this function, even if it's not necessary ... 
	if (numLabels == 2)
	{
		if (!strcmp(labels[0], " ")) return 2;
		else if (!strcmp(labels[1], " ")) return 1;
	}

	g_CEMVtrns.send_notify(CARD_APP_EVT_APP_SELECT);

	for(i= 0; i< numLabels; i++)
		dlog_msg("Label %d - %s", i+1, labels[i]);
	
	// TODO: Make application selection timeout configurable via some private tag!
	dlog_msg("Select application with timeout %d", g_CardAppConfig.GetApplicationSelectionTimeout()*1000);
	if( GuiApp_Connect( (char *)CARD_APP_NAME, 10000 ) == GUI_OKAY )
	{
	dlog_msg( "Connected from %s to %s", CARD_APP_NAME, GUI_APP_NAME_ );
	}
	iRet = GuiApp_SelectOption("", "", labels, numLabels, g_CardAppConfig.GetApplicationSelectionTimeout()*1000, EntryCancelled, NULL);
	if (iRet >= 0 && iRet < numLabels) 
	{
		iRet++;
	}
	else 
	{
		dlog_alert("Select application failed, error %d", iRet);
		if (iRet == GUI_CANCELLED)
		{
			g_CEMV.SetStatusDetail(CARD_APP_STATUS_D_CANCELLED);
			g_CEMV.SetStatusMain(CARD_APP_STATUS_M_STOP);
		}
	}

	GuiApp_ClearDisplay(0);
	if(g_CardAppConfig.GetUseDisplay())
	{
		GuiApp_DisplayPromptSelectSection(CARD_APP_GUI_PLEASE_WAIT, CARD_APP_GUI_S_PROMPTS, 0, 0);
	}

	g_CEMV.SetAppSelStatus(APP_SEL_STATUS_DONE);
	dlog_msg("Selected option: %d", iRet);
	GuiApp_Disconnect();

	return iRet;
}

int inMenuFunc_Default(char **labels, int numLabels)
{
	int iRet = 0;
	int i;
	event_item ev_ipc, ev_timer, ev_icc, ev_kbd;    
	ev_ipc.event_id = events::ipc;
	ev_ipc.evt.com.fd = -1;
	ev_ipc.evt.com.flags = 0;
	int events_mon = event_open();
	ev_icc.event_id = events::icc;
	ev_kbd.event_id = events::keyboard;
	ev_timer.event_id = events::timer;
	
	eventset_t pml_events;

    dlog_msg("MenuFunc called for %d items!", numLabels);

    //Check for Kernel bug - after application selection, Kernel always calls this function, even if it's not necessary ... 
    if (numLabels == 2)
    {
        if (!strcmp(labels[0], " ")) return 2;
        else if (!strcmp(labels[1], " ")) return 1;
    }

    g_CEMVtrns.send_notify(CARD_APP_EVT_APP_SELECT);

    for(i= 0; i< numLabels; i++)
        dlog_msg("Label %d - %s", i+1, labels[i]);
    
    
    if(1)
    {
        // dlog_error("%s NOT PRESENT", GUI_APP_NAME_);
        
		dlog_msg("Adding events::ipc ret :%X", iRet);
        const int maxLines = getScreenMaxY() > 9 ? 9 : getScreenMaxY(); // we can use numbers 1-9 only
        int start = 0;
        char buffer[DISPLAY_TEXT+1];
        buffer[DISPLAY_TEXT] = 0;
        for( i = 0; i < numLabels && i < maxLines; i++)
        {
            snprintf(buffer, DISPLAY_TEXT, "%d: %s\n", i+1, labels[i]);
            DisplayText(buffer, strlen(buffer), i == 0);
        }

		iRet = event_ctl(events_mon, ctl::ADD, ev_ipc);
		dlog_msg("Adding events::ipc ret :%X", iRet);

		iRet = event_ctl(events_mon, ctl::ADD, ev_icc);
		dlog_msg("Adding events::ipc ret :%X", iRet);

		iRet = event_ctl(events_mon, ctl::ADD, ev_kbd);
		dlog_msg("Adding events::ipc ret :%X", iRet);

				
        if (g_CardAppConfig.GetApplicationSelectionTimeout())
        {
			set_timer(ev_timer, (g_CardAppConfig.GetApplicationSelectionTimeout()*1000)); 
			event_ctl(events_mon, ctl::ADD, ev_timer);
        }
        char key_read = 0;
        bool entry_end = false;
        int m_Console = get_console_handle();
        while(!entry_end)
        {
            
			if(event_wait(events_mon, pml_events))
			{
				for (eventset_t::iterator it = pml_events.begin() ; it != pml_events.end(); ++it)
				{
			   		if (it->event_id == events::timer)
			   		{
						dlog_alert("Application selection timed out!");
		                entry_end = true;
			   		}
					if (it->event_id == events::icc)
			   		{
			   			if(isCardRemoved())
			   			{
							dlog_alert("Card REMOVED! - CANCELING");
                			entry_end = 1;
		                	g_CEMV.SetStatusDetail(CARD_APP_STATUS_D_CANCELLED);
                			g_CEMV.SetStatusMain(CARD_APP_STATUS_M_STOP);
			   			}
			   		}
					if (it->event_id == events::keyboard)
			   		{
						if(read(m_Console, &key_read, 1) > 0)
	                	{
		                    key_read &= 0x7F;
		                    dlog_msg("KEY>>>> %X", key_read);
		                    if (key_read >= '1' && key_read <= '0'+(numLabels < maxLines ? numLabels : maxLines))
		                    {
		                        iRet = key_read+start-'0';
		                        entry_end = true;
		                    }
		                    else if (start > 0 && key_read == '*')
		                    {
		                        start -= maxLines;
		                        for( i = start; i < numLabels && i < start + maxLines; i++)
		                        {
		                            snprintf(buffer, DISPLAY_TEXT, "%d: %s\n", i-start+1, labels[i]);
		                            DisplayText(buffer, strlen(buffer), i == start);
		                        }
		                    }
		                    else if (start + maxLines < numLabels && key_read == '#')
		                    {
		                        start += maxLines;
		                        for( i = start; i < numLabels && i < start + maxLines; i++)
		                        {
		                            snprintf(buffer, DISPLAY_TEXT, "%d: %s\n", i-start+1, labels[i]);
		                            DisplayText(buffer, strlen(buffer), i == start);
		                        }
		                    }
		                    else
		                    {
		                        com_verifone_pml::error_tone();
		                    }
		                }
		            }
    
					if (it->event_id == events::ipc)
			   		{
						dlog_msg("Message received - checking if not cancel...");
                		CCardAppConfig::cmd_break_t break_status = g_CardAppConfig.checkCmdBreak();
		                if(break_status == CCardAppConfig::CMD_BREAK_CANCEL_E)
        		        {
                		    dlog_alert("Application selection cancelled, exiting...");
		                    entry_end = true;
        		            g_CEMV.SetStatusDetail(CARD_APP_STATUS_D_CANCELLED);
                		    g_CEMV.SetStatusMain(CARD_APP_STATUS_M_STOP);
                		}
			   		}

				}
			}
			
        }
        clrscr();
        clr_timer(ev_timer);
        if(g_CardAppConfig.GetUseDisplay())
        {
            DisplayPromptSelectSection(CARD_APP_GUI_PLEASE_WAIT, CARD_APP_GUI_S_PROMPTS);
        }

    }

    g_CEMV.SetAppSelStatus(APP_SEL_STATUS_DONE);
    dlog_msg("Selected option: %d", iRet);
    event_close(events_mon);

    return iRet;
}
#endif

#ifdef VFI_GUI_DIRECTGUI

int inMenuFunc_DIRECTGUI(char **labels, int numLabels)
{
	dlog_msg("MenuFunc called for %d items!", numLabels);

	//Check for Kernel bug - after application selection, Kernel always calls this function, even if it's not necessary ... 
	if (numLabels == 2)
	{
		if (!strcmp(labels[0], " ")) return 2;
		else if (!strcmp(labels[1], " ")) return 1;
	}

	g_CEMVtrns.send_notify(CARD_APP_EVT_APP_SELECT);

	cAppSelDisplayDirectGUI appSel("", "mainmenu.html");
	appSel.setIsICCpresent(isCardpresent_loc);
	appSel.setCheckBreak(checkBreak_AppSel_loc);

	int iRet = appSel.displayApplicationSelection(const_cast<const char **>(labels), numLabels, g_CardAppConfig.GetApplicationSelectionTimeout()*1000);
	
	if (iRet < 0)
	{
		dlog_error("Application selection error");
		switch (iRet)
		{
			case APP_SEL_CARD_REMOVED:
				dlog_msg("Card removed!");
				g_CEMV.SetStatusDetail(CARD_APP_STATUS_D_REMOVED);
				g_CEMV.SetStatusMain(CARD_APP_STATUS_M_STOP);
				break;
			case APP_SEL_CANCELLED:
				dlog_msg("Selection cancelled");
				g_CEMV.SetStatusDetail(CARD_APP_STATUS_D_CANCELLED);
				g_CEMV.SetStatusMain(CARD_APP_STATUS_M_STOP);
				break;
			default:
				// default failure
				g_CEMV.SetStatusDetail(CARD_APP_STATUS_D_GENERAL);
				g_CEMV.SetStatusMain(CARD_APP_STATUS_M_STOP);
				break;
		}
	}
	else
	{
		// this has to be incremented to be compatible with what guiapp version returned...
		// as appSel.displayApplicationSelection indexes items from 0
		++iRet;
	}

	g_CEMV.SetAppSelStatus(APP_SEL_STATUS_DONE);
	dlog_msg("Selected option: %d", iRet);

	return iRet;
}
#endif



#ifdef VFI_GUI_GUIAPP
void vdPromptManager_GUIAPP(unsigned short inCondition)
{
	unsigned long prompt_index = 0;
	char section_name[10];
	unsigned char x_pos = 0, y_pos = 0;
	
	dlog_msg(">>>PROMPT MANAGER: %X", inCondition);

	switch(inCondition)
	{
		case 0:
			break;
		default:
			strcpy(section_name, "emv");
			prompt_index = 1;
			break;
	}
	if(prompt_index > 0)
	{
		dlog_msg("Displaying prompt %X", inCondition);
		if(g_CardAppConfig.GetUseDisplay())
		{
			GuiApp_DisplayPromptSelectSection(prompt_index, section_name, x_pos, y_pos);
		}
	}
}

void vdPromptManager_Default(unsigned short inCondition)
{
    unsigned long prompt_index = 0;
    char section_name[10];
    unsigned char x_pos = 0, y_pos = 0;
    
    dlog_msg(">>>PROMPT MANAGER: %X", inCondition);

    switch(inCondition)
    {
        case 0:
            break;
        default:
            strcpy(section_name, "emv");
            prompt_index = 1;
            break;
    }
    if(prompt_index > 0)
    {
        dlog_msg("Displaying prompt %X", inCondition);
        if(g_CardAppConfig.GetUseDisplay())
        {
            if(1)
            {
                //dlog_error( "LOCAL GUI SUPPORT NOT YET IMPLEMENTED!" );
                DisplayPromptSelectSection(prompt_index, section_name);
            }
        }
    }
}
#endif

#ifdef VFI_GUI_DIRECTGUI
void vdPromptManager_DIRECTGUI(unsigned short inCondition)
{

}
#endif


unsigned short usAmtEntryFunc_Default(unsigned long *lnAmountValue)
{
    dlog_msg("AmtEntryFunct called");
    *lnAmountValue = 0;
    return EMV_SUCCESS;
}

CEMV::CEMV(void)
{
    status_main = CARD_APP_STATUS_M_CONTINUE | CARD_APP_STATUS_M_OK;
    status_detailed = CARD_APP_STATUS_D_OK;
    ICC_reader = CUSTOMER_CARD;
    operation_reg = 0;
    term_decision = FAILED_TO_CONNECT;
    inMenuFunc = NULL;
    vdPromptManager = NULL;
    usAmtEntryFunc = usAmtEntryFunc_Default;
    usCandListModify = usCandListModify_Default;
    is_card_blacklisted = false;
    PIN_type = PIN_TYPE_PLAIN_TEXT;
    PIN_entry_type = ENTRY_TYPE_NORMAL;
    beeperThreadID = 0;
    pPinDisplay = 0;
    PIN_status = PIN_ENTRY_STATUS_NOT_SET;
    external_pin_mode = -1;
}

void CEMV::configureCallbacks(bool guiAppActive)
{
#ifdef VFI_GUI_GUIAPP
    if (guiAppActive)
    {
        inMenuFunc = inMenuFunc_GUIAPP;
        vdPromptManager = vdPromptManager_GUIAPP;
    }
    else
    {
        inMenuFunc = inMenuFunc_Default;
        vdPromptManager = vdPromptManager_Default;
    }
#endif
#ifdef VFI_GUI_DIRECTGUI
    inMenuFunc = inMenuFunc_DIRECTGUI;
    vdPromptManager = vdPromptManager_DIRECTGUI;
#endif
}

bool CEMV::startICCMonitoring()
{
    unsigned long Value;
    RESPONSECODE ret;

    /********* INIT: AT THE BEGINNING OF THE APPLICATION ********/
    /* Open ICC1 and reserve the memory : Tag 0x0188 */
    Value = CUSTOMER_CARD;
    ret = IFD_Set_Capabilities( Tag_Open_ICC, (BYTE*)(&Value));
    if(ret != IFD_Success)
    {
        dlog_error("** IFD_Set_Capabilities err %d **", ret);
        return false;
    }
    return true;
}

int CEMV::checkStatus(int operation)
{
    int mask;
    int result;

    //checking if mandatory steps were done
    switch(operation)
    {
        case CARD_APP_OPER_SELECT:
            if (operation_reg)
            {
                dlog_alert("Operations were already processed, need to repower the card!");
                if (!isCardRemoved()) g_CEMVtrns.wait_for_card(CARD_APP_CARD_ICC, 1, true);
            }
            dlog_msg("Clearing statuses for new transaction...");
            ClearStatus();
            g_CEMVtrns.TLV_data_len = 0;
        case CARD_APP_OPER_TLV:
            mask = 0;
            break;
        case CARD_APP_OPER_GPO:
            mask = CARD_APP_OPER_SELECT;
            break;
        case CARD_APP_OPER_READ:
            mask = CARD_APP_OPER_SELECT | CARD_APP_OPER_GPO;
            break;
        case CARD_APP_OPER_DATA_AUTH:
        case CARD_APP_OPER_VERIFY:
        case CARD_APP_OPER_RESTRICT:
        case CARD_APP_OPER_TRM:
            mask = CARD_APP_OPER_SELECT | CARD_APP_OPER_GPO | CARD_APP_OPER_READ;
            break;
        case CARD_APP_OPER_FIRST_AC:
            mask = CARD_APP_OPER_SELECT | CARD_APP_OPER_GPO | CARD_APP_OPER_READ |
                    CARD_APP_OPER_DATA_AUTH | CARD_APP_OPER_VERIFY | CARD_APP_OPER_TRM | CARD_APP_OPER_RESTRICT;
            break;
        case CARD_APP_OPER_EXT_AUTH:
        case CARD_APP_OPER_SECOND_AC:
        case CARD_APP_OPER_SCRIPT:
            mask = CARD_APP_OPER_SELECT | CARD_APP_OPER_GPO | CARD_APP_OPER_READ |
                    CARD_APP_OPER_DATA_AUTH | CARD_APP_OPER_VERIFY | CARD_APP_OPER_TRM | CARD_APP_OPER_RESTRICT| CARD_APP_OPER_FIRST_AC;
            break;
        default:
            mask = 0xFFFF;
            break;
    }

    //checking if card is in
    if (isCardRemoved())
    {
        // Clear TLV pool
        int iRet = inVXEMVAPTransInit(CARD_APP_MVT_DEFAULT, inGetPTermID);
        dlog_msg("inVXEMVAPTransInit result %d", iRet);
        SetStatusMain(CARD_APP_STATUS_M_ERROR | CARD_APP_STATUS_M_STOP);
        SetStatusDetail(CARD_APP_STATUS_D_REMOVED | CARD_APP_STATUS_D_NO_FALLBACK);
        return CARD_APP_RET_REMOVED;
    }
    //checking if card is PROPER
    else if (g_CEMVtrns.icc_card_type != CARD_APP_CARDTYPE_EMV_SYNC && g_CEMVtrns.icc_card_type != CARD_APP_CARDTYPE_EMV_ASYNC)
    {
        // Bad card is inserted
        dlog_error("Improper card inserted (%d)", g_CEMVtrns.icc_card_type);
        SetStatusMain(CARD_APP_STATUS_M_ERROR | CARD_APP_STATUS_M_STOP);
        SetStatusDetail(CARD_APP_STATUS_D_CHIP_ERROR | CARD_APP_STATUS_D_NO_FALLBACK);
        return CARD_APP_RET_ICC_FAIL;
    }

    //checking if transaction can be continued
    if(GetStatusMain() & CARD_APP_STATUS_M_STOP)
    {
        dlog_msg("checkStatus:Transaction can not be continued:CARD_APP_STATUS_M_STOP");
        
#ifdef VFI_GUI_GUIAPP
        if (g_CardAppConfig.GetUseDisplay())
        {
            if(1)
            {
                DisplayPromptSelectSection(CARD_APP_GUI_REMOVE, CARD_APP_GUI_S_PROMPTS);
            }
        }
#endif        

        mask = 0xFFFF;
        // g_CEMVtrns.icc_card_type = CARD_APP_CARDTYPE_NOT_SET;
        dlog_msg("Closing CardSlot");
        Terminate_CardSlot(ICC_reader, SWITCH_OFF_CARD);
        Close_CardSlot(ICC_reader);
    }
    // checking if aborted
    else
    {
    	std::string source_task;
        if(com_verifone_ipc::check_pending(source_task) == com_verifone_ipc::IPC_SUCCESS) //check if message waits
        {
            CCardAppConfig::cmd_break_t break_status = g_CardAppConfig.checkCmdBreak();
            if(break_status == CCardAppConfig::CMD_BREAK_CANCEL_E)
            {
                dlog_alert("Transaction cancelled by POS, exiting...");
                SetStatusDetail(CARD_APP_STATUS_D_CANCELLED);
                return CARD_APP_RET_CANCELED;
            }
        }
    }

    dlog_msg("Operations register: %X, mask %X", operation_reg, mask);
    result = ((operation_reg & mask) == mask);

    dlog_msg("Checking operation:%X, result:%d", operation, result);
    if (result == 0) return CARD_APP_RET_ORDER;
    return CARD_APP_RET_OK;
}

int CEMV::setResponse(int EMVModuleRet, int operation)
{
    int result = CARD_APP_RET_OK;
    if (EMVModuleRet != 0)
    {
        if (isCardRemoved())
        {
            // Clear TLV pool
            int iRet = inVXEMVAPTransInit(CARD_APP_MVT_DEFAULT, inGetPTermID);
            dlog_msg("inVXEMVAPTransInit result %d", iRet);
            EMVModuleRet = CARD_REMOVED;
        }
    }

    switch(EMVModuleRet)
    {
        case 0:
            SetStatusMain(CARD_APP_STATUS_M_OK | CARD_APP_STATUS_M_CONTINUE);
            SetStatusDetail(CARD_APP_STATUS_D_OK | CARD_APP_STATUS_D_NO_FALLBACK);
            result = CARD_APP_RET_OK;
            break;
        case CARD_REMOVED:
            SetStatusMain(CARD_APP_STATUS_M_OK | CARD_APP_STATUS_M_STOP);
            SetStatusDetail(CARD_APP_STATUS_D_REMOVED| CARD_APP_STATUS_D_NO_FALLBACK);
            result = CARD_APP_RET_REMOVED;
            break;
        case APPL_BLOCKED:
            SetStatusMain(CARD_APP_STATUS_M_OK | CARD_APP_STATUS_M_STOP);
            SetStatusDetail(CARD_APP_STATUS_D_APPL_BLOCKED| CARD_APP_STATUS_D_NO_FALLBACK);
            result = CARD_APP_RET_APP_BLOCKED;
            break;
        case CARD_BLOCKED:
            SetStatusMain(CARD_APP_STATUS_M_OK | CARD_APP_STATUS_M_STOP);
            SetStatusDetail(CARD_APP_STATUS_D_CARD_BLOCKED| CARD_APP_STATUS_D_NO_FALLBACK);
            result = CARD_APP_RET_CARD_BLOCKED;
            break;
        case INVALID_PARAMETER:
            SetStatusMain(CARD_APP_STATUS_M_ERROR| CARD_APP_STATUS_M_STOP);
            SetStatusDetail(CARD_APP_STATUS_D_PARAMETER | CARD_APP_STATUS_D_FALLBACK);
            result = CARD_APP_RET_PARAM;
            break;
        case BAD_ICC_RESPONSE:
            SetStatusMain(CARD_APP_STATUS_M_ERROR | CARD_APP_STATUS_M_STOP);
            SetStatusDetail(CARD_APP_STATUS_D_CHIP_ERROR | CARD_APP_STATUS_D_FALLBACK);
            result = CARD_APP_RET_ICC_FAIL;
            break;
        case ICC_DATA_MISSING:
            SetStatusMain(CARD_APP_STATUS_M_ERROR | CARD_APP_STATUS_M_STOP);
            SetStatusDetail(CARD_APP_STATUS_D_CHIP_ERROR | CARD_APP_STATUS_D_FALLBACK);
            result = CARD_APP_RET_ICC_FAIL;
            break;
        case CANDIDATELIST_EMPTY:
            SetStatusMain(CARD_APP_STATUS_M_OK | CARD_APP_STATUS_M_STOP);
            if (found_matching_apps)
            {
                SetStatusDetail(CARD_APP_STATUS_D_CONF_EMPTY | CARD_APP_STATUS_D_FALLBACK);
            }
            else
            {
                SetStatusDetail(CARD_APP_STATUS_D_EMPTY | CARD_APP_STATUS_D_FALLBACK);
            }
            result = CARD_APP_RET_NO_APPS;
            break;
        case CHIP_ERROR:
            SetStatusMain(CARD_APP_STATUS_M_ERROR | CARD_APP_STATUS_M_STOP);
            SetStatusDetail(CARD_APP_STATUS_D_CHIP_ERROR | CARD_APP_STATUS_D_FALLBACK);
            result = CARD_APP_RET_ICC_FAIL;
            break;
        case BAD_DATA_FORMAT:
            SetStatusMain(CARD_APP_STATUS_M_ERROR | CARD_APP_STATUS_M_STOP);
            SetStatusDetail(CARD_APP_STATUS_D_CHIP_ERROR | CARD_APP_STATUS_D_FALLBACK);
            result = CARD_APP_RET_ICC_FAIL;
            break;
        case TRANS_CANCELLED:
        case EMV_FAILURE:
        case REMOVE_CARD:
            SetStatusMain(CARD_APP_STATUS_M_OK | CARD_APP_STATUS_M_STOP);
            SetStatusDetail(CARD_APP_STATUS_D_CANCELLED | CARD_APP_STATUS_D_NO_FALLBACK);
            result = CARD_APP_RET_CANCELED;
            break;
        case EASY_ENTRY_APPL:
            SetStatusMain(CARD_APP_STATUS_M_OK | CARD_APP_STATUS_M_STOP);
            SetStatusDetail(CARD_APP_STATUS_D_CHIP_ERROR | CARD_APP_STATUS_D_FALLBACK);
            result = CARD_APP_RET_ICC_FAIL;
            break;
        case E_EXPLICIT_SELEC_REQD:
            SetStatusMain(CARD_APP_STATUS_M_OK | CARD_APP_STATUS_M_CONTINUE);
            SetStatusDetail(CARD_APP_STATUS_D_OK | CARD_APP_STATUS_D_FALLBACK);
            result = CARD_APP_RET_OK;
            break;
        case 0x6985:
            SetStatusMain(CARD_APP_STATUS_M_ERROR | CARD_APP_STATUS_M_STOP);
            SetStatusDetail(CARD_APP_STATUS_D_CHIP_ERROR | CARD_APP_STATUS_D_FALLBACK);
            result = CARD_APP_RET_ICC_FAIL;
            break;
        default:
            SetStatusMain(CARD_APP_STATUS_M_ERROR | CARD_APP_STATUS_M_STOP);
            SetStatusDetail(CARD_APP_STATUS_D_GENERAL | CARD_APP_STATUS_D_FALLBACK);
            result = CARD_APP_RET_FAIL;
            break;
    }
    operation_reg |= operation;
    dlog_msg("setResponse:Operation register: %X", operation_reg);
    dlog_msg("setResponse:Status: %0000000X:%0000000X", GetStatusMain(), GetStatusDetail());

    return result;

}

/*
#ifdef VFI_GUI_GUIAPP
int CEMV::OfflinePINVerification(void)
{
    unsigned short status = 0xFFFF;
    int pintype = g_CardAppConfig.GetPINtype();
    if (pintype != CARD_APP_PIN_OFFLINE)
        g_CardAppConfig.SetPINtype(CARD_APP_PIN_OFFLINE);

    dlog_msg("PIN type %d", g_CardAppConfig.GetPINtype());
    //int verify_pin( unsigned char    ucPinType,  unsigned char    ucEntryType, unsigned short * pusStatus )
    int iRet = verify_pin(PIN_type, PIN_entry_type, &status);

    dlog_msg("verify_pin return: %d, status: %X", iRet, status);
    g_CardAppConfig.SetPINtype(pintype);
    usEMVSetKernelTags(TAG_DF09_LAST_STATUS_WORD, sizeof(status), reinterpret_cast<unsigned char *>(&status));

    return iRet;
}
#endif
*/

int CEMV::GetChallenge(unsigned char * icc_unpredictable)
{
	unsigned char GetChallengeCmd[] = "\x00\x84\x00\x00\x00";
	unsigned short GetChallengeCmdSize = sizeof(GetChallengeCmd)-1;
	unsigned short ICC_response_length = 0;
	unsigned char ICC_response[20];
	unsigned long scr_slot = CUSTOMER_CARD;
	int result = Transmit_APDU(scr_slot, GetChallengeCmd, GetChallengeCmdSize, ICC_response, &ICC_response_length);
	if (result != CARDSLOT_SUCCESS)
	{
		dlog_msg("Get Challenge error %d", result);
		return 0xFFFF;
	}
	if (ICC_response_length == 10)
	{
		result = ICC_response[8] << 8;
		result |= ICC_response[9];
		dlog_msg("Get Challenge succeeded, SW1SW2 %04Xh", result);
		dlog_hex(ICC_response, ICC_response_length, "ICC response");
		if (result == 0x9000)
		{
			memcpy(icc_unpredictable, ICC_response, ICC_UNPREDICTABLE_SIZE);
		}
	}
	else if (ICC_response_length == 2)
	{
		result = ICC_response[0] << 8;
		result |= ICC_response[1];
		dlog_error("Get Challenge failed, SW1SW2 %04Xh", result);
	}
	else
	{
		dlog_error("Invalid Get Challenge response: length %d", ICC_response_length);
		result = 0xFFFF;
	}
	return result;
}

int CEMV::OfflinePINVerification(void)
{
	int Ret = PIN_VERIFY_ERROR;
	bool full_verify = true;

	if(PIN_entry_type == ENTRY_TYPE_ATOMIC)
	{
		full_verify = false;
	}

	#ifdef VFI_GUI_GUIAPP
	if (g_CardAppConfig.GuiInterface() == CCardAppConfig::GUI_APP) 
	{
		if(!pPinDisplay) pPinDisplay = new cPINdisplayGuiApp;
		dlog_msg("Using GuiAPP for PIN entry ptr:%X", pPinDisplay);
	}
	#endif
	
	#ifdef VFI_GUI_DIRECTGUI
	if (g_CardAppConfig.GuiInterface() == CCardAppConfig::GUI_FLEXI)
	{
		//support for Flexi pin entry is not yet implemented in libPINgui
		#if 0
		if(!pPinDisplay) pPinDisplay = new cPINdisplayFlexi(g_CardAppConfig.GetPINbeeperTimeout());
		dlog_msg("Using cPINdisplayFlexi for PIN entry ptr:%X", pPinDisplay);
		#else
		if(!pPinDisplay) pPinDisplay = new cPINdisplayDirectGUI(g_CardAppConfig.GetPINbeeperTimeout());
		dlog_msg("Using direcGUI for PIN entry ptr:%X", pPinDisplay);
		if(CEMV::PIN_html.size())
		{
			dlog_msg("Setting PIN entry html file: %s", CEMV::PIN_html.c_str());
			pPinDisplay->SetPINhtml(CEMV::PIN_html);
		}
		#endif
	}
	else
	{
		if(!pPinDisplay) pPinDisplay = new cPINdisplayDirectGUI;
		dlog_msg("Using direcGUI for PIN entry ptr:%X", pPinDisplay);
		
	}
	::setPINParams();
	#endif
	
	c_encPIN encPIN;
	PINPARAMETER psKeypadSetup;

	//if(1)
	do
	{
		dlog_msg("START processing PIn type: %d", g_CardAppConfig.GetPINtype());
		if (g_CardAppConfig.GetPINtype() != CARD_APP_EXTERNAL_PIN_OFFLINE)
		{
			if(g_CEMV.GetPINStatus() == PIN_ENTRY_STATUS_LAST_TRY)
			{
				g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_LAST);
			}
			else if(g_CEMV.GetPINStatus() == PIN_ENTRY_STATUS_BLOCKED)
			{
				g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_BLOCKED);
				dlog_error("PIN is blocked!");
				return PIN_VERIFY_BLOCKED;
			}
			else
			{
				g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_ENTRY);
			}

			if(CARD_APP_SECURE_PIN_OFFLINE != g_CardAppConfig.GetPINtype())
			{
				psKeypadSetup.ucMin = g_CardAppConfig.GetMinPINLen();
				psKeypadSetup.ucMax = g_CardAppConfig.GetMaxPINLen();
				psKeypadSetup.ucEchoChar = '*';
				psKeypadSetup.ucDefChar = g_CardAppConfig.GetPINblankChar();
				psKeypadSetup.ucOption = g_CardAppConfig.GetPINoption(); // Bit 3 is set by default
				
				/*if (g_CardAppConfig.GetOnlinePINCancelAllowed())
				{
					psKeypadSetup.ucOption |= 0x02; // bit 1
				}
				if (g_CardAppConfig.GetOnlinePINEntryType() != CCardAppConfig::PIN_ENTRY_TYPE_MANDATORY_E)
				{
					psKeypadSetup.ucOption |= 0x10; // bit 4
				}*/
				if (g_CardAppConfig.IsPINBypassEnabled())
				{
					unsigned char bypassKey = g_CardAppConfig.GetPINBypassKey();
					if (bypassKey == 0x08) // backspace
					{
						psKeypadSetup.ucOption |= 0x10; // bit 4
					}
					else
					{
						psKeypadSetup.ucOption |= 0x01; // bit 1
					}
					pPinDisplay->setPINparams(psKeypadSetup, bypassKey);
					dlog_msg("PIN bypass enabled bypass key: 0x%X", bypassKey);
				}
				else
				{
					pPinDisplay->setPINparams(psKeypadSetup);
					dlog_msg("PIN bypass disabled");
				}

				dlog_msg("Offline PIN entry, flags %02X", psKeypadSetup.ucOption);
			}
			pPinDisplay->setTLVget(getData_loc);
			pPinDisplay->setTLVset(setData_loc);
			pPinDisplay->setCheckBreak(checkBreak_PIN_loc);
			pPinDisplay->setICCpresent(isCardpresent_loc);
			pPinDisplay->setEntryCallBack(EntryCallBack_ccard);

			pPinDisplay->SetPINprogress(GetPINStatus());
			pPinDisplay->setCurrSymbolLeft(g_CardAppConfig.PINDisplayCurrencySymbolLeft(false));
			pPinDisplay->setDispCurrSymbol(g_CardAppConfig.GetPINDisplayCurrencySymbol());
			pPinDisplay->setPINtimeouts(g_CardAppConfig.GetPINfirstcharTimeout()*1000,
										g_CardAppConfig.GetPINintercharTimeout()*1000,
										g_CardAppConfig.GetPINtimeout()*1000);
			if (g_CardAppConfig.cust_language.size()) pPinDisplay->SetLanguage(g_CardAppConfig.cust_language);
			if(CARD_APP_SECURE_PIN_OFFLINE != g_CardAppConfig.GetPINtype())
			{
				pPinDisplay->SetPINmode(0x0A);
				Ret = pPinDisplay->GetPIN(encPIN);
				dlog_msg("GetPIN result %d", Ret);
				if(Ret == PIN_VERIFY_BYPASS || Ret == PIN_VERIFY_POS_BYPASS)
				{
					g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_BYPASSED);
					SetStatusDetail(CARD_APP_STATUS_D_BYPASS);
					dlog_msg("PIN entry bypassed");
					//EMVlog("PIN entry bypassed");
				}
				else if(Ret == PIN_VERIFY_CANCEL || Ret == PIN_VERIFY_POS_CANCEL)
				{
					g_CEMVtrns.cmd_status = RESP_CODE_PIN_CANCEL;
					g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_CANCELLED);
					SetStatusDetail(CARD_APP_STATUS_D_CANCELLED);
					dlog_msg("PIN entry cancelled");
					//EMVlog("PIN entry cancelled");
				}
				else if(Ret == PIN_VERIFY_TIMEOUT)
				{
					g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_TIMEOUT);
					SetStatusDetail(CARD_APP_STATUS_D_TIMEOUT);
					dlog_msg("PIN entry timeout");
					//EMVlog("PIN entry timeout");
				}
				else if(Ret == PIN_VERIFY_ABORT)
				{
					g_CEMVtrns.cmd_status = RESP_CODE_PIN_CANCEL;
					g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_CANCELLED);
					SetStatusDetail(CARD_APP_STATUS_D_CANCELLED);
					dlog_msg("PIN entry abort, error occurred!");
					//EMVlog("PIN entry abort, error occurred!");
				}
				else
				{
					g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_DONE);
					dlog_msg("PIN entry done");
					//EMVlog("PIN entry done");
				}
				pPinDisplay->Clear();
			}
			else
			{
				dlog_msg("Displaying message for PIN entry (BLR kernel support)...");
				pPinDisplay->Display(encPIN);
				::setPINParams();
				write_at("", 0, g_CardAppConfig.GetPinX(), g_CardAppConfig.GetPinY());
			}
		}
		else
		{
			encPIN.addExternalPIN(external_pin.c_str(), external_pin.size());
			encPIN.addExternalPINMode(external_pin_mode);
			external_pin.clear();
			Ret = PIN_VERIFY_OK;
		}
		
		//checking pin for external kernels
		//if((Ret == PIN_VERIFY_OK) && (g_CardAppConfig.GetPINtype() == CARD_APP_PIN_OFFLINE_EXT))
		if (g_CardAppConfig.GetPINtype() == CARD_APP_PIN_OFFLINE_EXT || g_CardAppConfig.GetPINtype() == CARD_APP_EXTERNAL_PIN_OFFLINE)
		{
			if (Ret == PIN_VERIFY_OK)
			{
				if(PIN_type == PIN_TYPE_ENCIPHERED)
				{
					
					// we need the following tags present. if not present then return error
					//
					// (9f47 OR 9f2e) AND df0e
					//
					// where:
					// 9f47 = ICC Public Key Exponent
					// 9f2e = ICC PIN Encipherment Public Key Exponent
					// df0e = Public key modulus
					
					static unsigned char modulus[MAX_MODULUS_LENGTH];
					static unsigned short modulus_length;
					static unsigned char exponent[4];
					static unsigned short exponent_length;
					
					if( usEMVGetTLVFromColxn((unsigned short)TAG_9F47_ICC_PUBKEY_EXP, exponent, &exponent_length) == EMV_SUCCESS )
					{
						dlog_msg("using tag 9f47");
					}
					else if( usEMVGetTLVFromColxn((unsigned short)TAG_9F2E_ICC_PIN_EXP, exponent, &exponent_length) == EMV_SUCCESS )
					{
						dlog_msg("using tag 9f2e");
					}
					else
					{
						// couldn't find either, return error
						dlog_msg("error tag 9f47 or 9f2e not present");
					}
				
					if( usEMVGetTLVFromColxn((unsigned short)0xdf0e, modulus, &modulus_length) != EMV_SUCCESS )
					{
						dlog_msg("error tag df0e not present");
					}
					dlog_msg("modulus len=%d", modulus_length);
					
					encPIN.addExponent(exponent, exponent_length);
					encPIN.addModulus(modulus, modulus_length);
					
					// Kamil_P1: We need to issue Get Challenge
					unsigned char icc_unpredictable_num[ICC_UNPREDICTABLE_SIZE];
					Ret = GetChallenge(icc_unpredictable_num);
					if (Ret != 0x9000)
					{
						int PIN_result = PIN_ENTRY_STATUS_ERROR;
						SetPINEntryStatus(PIN_result);
						SetPINStatus(PIN_result);
						unsigned short lastSW1SW2 = static_cast<unsigned short>(Ret);
						usEMVSetKernelTags(TAG_DF09_LAST_STATUS_WORD, sizeof(lastSW1SW2), reinterpret_cast<unsigned char *>(&lastSW1SW2));
						Ret = PIN_VERIFY_GET_CHALLENGE;
						SetStatusDetail(CARD_APP_STATUS_D_CHIP_ERROR); // TODO: Check if this code is okay
						break; // We can terminate PIN entry here
					}
					encPIN.addICCrandom(icc_unpredictable_num, ICC_UNPREDICTABLE_SIZE);
				}
				SetStatusDetail(CARD_APP_STATUS_D_OK);
				// if((Ret = encPIN.VerifyPIN()) == 0x9000)
				Ret = encPIN.VerifyPIN();
				{
					unsigned short lastSW1SW2 = static_cast<unsigned short>(Ret);
					usEMVSetKernelTags(TAG_DF09_LAST_STATUS_WORD, sizeof(lastSW1SW2), reinterpret_cast<unsigned char *>(&lastSW1SW2));
				}
				if (Ret == 0x9000)
				{
					Ret = PIN_VERIFY_OK;
					SetPINEntryStatus(PIN_ENTRY_STATUS_OK);
					SetPINStatus(PIN_ENTRY_STATUS_OK);
					SetStatusDetail(CARD_APP_STATUS_D_OK);
					//EMVlog("PIN OK");
				}
				else
				{
					int PIN_result = CheckPINresult((unsigned char)(Ret/0x100), (unsigned char)(Ret & 0xFF));
					SetPINEntryStatus(PIN_result);
					SetPINStatus(PIN_result);
					if (PIN_result == PIN_ENTRY_STATUS_BLOCKED) Ret = PIN_VERIFY_BLOCKED;
					else if (PIN_result == PIN_ENTRY_STATUS_ERROR) Ret = PIN_VERIFY_ABORT;
					// else if (PIN_result == PIN_ENTRY_STATUS_LAST_TRY) Ret = PIN_VERIFY_LAST_TRY;
					else Ret = PIN_VERIFY_ERROR;
				}
			}
			else
			{
				// Error occurred, PIN not entered properly, so reset flag
				SetPINStatus(PIN_ENTRY_STATUS_NOT_SET);
			}
		}
		else
		{
			full_verify = false;
		}
	//}while((full_verify) && ((Ret!=PIN_VERIFY_OK) && (Ret!=PIN_VERIFY_BLOCKED) && (Ret!=PIN_VERIFY_CARD_REMOVED)));
	}while((full_verify) && (Ret==PIN_VERIFY_ERROR));

	if ((g_CardAppConfig.GetPINtype() == CARD_APP_PIN_OFFLINE_EXT) && pPinDisplay)
	{
		pPinDisplay->Clear();
		delete pPinDisplay; pPinDisplay = 0;
	}

	return Ret;
	

}

/*

#ifdef VFI_GUI_DIRECTGUI

int getData_loc(int tagNum,unsigned char * value,int * Len)
{
	return usEMVGetTLVFromColxn(tagNum, value, Len);
}

int setData_loc(int tagNum,unsigned char * value,int Len)
{
	return usEMVUpdateTLVInCollxn(tagNum, value, Len);
}

int CEMV::OfflinePINVerification(void)
{
	int Ret;

    unsigned short status = 0xFFFF;
    int pintype = g_CardAppConfig.GetPINtype();
    if (pintype != CARD_APP_PIN_OFFLINE)
        g_CardAppConfig.SetPINtype(CARD_APP_PIN_OFFLINE);
    dlog_msg("PIN type %d", g_CardAppConfig.GetPINtype());
	
//	cEMVcollection *EMVcollection;

	cPINdisplayDirectGUI PINdisplay;
	
	c_encPIN encPIN;
	PINPARAMETER psKeypadSetup;

//	g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_ENTRY);

//	if(g_CEMV.GetPINStatus() == PIN_ENTRY_STATUS_LAST_TRY)
//	{
//		g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_LAST);
//	}

	if(1)
	{

		psKeypadSetup.ucMin = g_CardAppConfig.GetMinPINLen();
		psKeypadSetup.ucMax = g_CardAppConfig.GetMaxPINLen();
		psKeypadSetup.ucEchoChar = '*';
		psKeypadSetup.ucDefChar = 0x20;
		psKeypadSetup.ucOption = 0x08; // Bit 3 is to be set
		if (g_CardAppConfig.GetOnlinePINCancelAllowed())
		{
			psKeypadSetup.ucOption |= 0x02; // bit 1
		}
		if (g_CardAppConfig.GetOnlinePINEntryType() != CCardAppConfig::PIN_ENTRY_TYPE_MANDATORY_E)
		{
			psKeypadSetup.ucOption |= 0x10; // bit 4
		}
		if (g_CardAppConfig.IsPINBypassEnabled())
		{
			unsigned char bypassKey = g_CardAppConfig.GetPINBypassKey();
			if (bypassKey == 0x08) // backspace
			{
				psKeypadSetup.ucOption |= 0x10; // bit 4
			}
			else
			{
				psKeypadSetup.ucOption |= 0x01; // bit 1
			}
			PINdisplay.setPINparams(psKeypadSetup, bypassKey);
		}
		else
		{
			PINdisplay.setPINparams(psKeypadSetup);
		}

		dlog_msg("Offline PIN entry, flags %02X", psKeypadSetup.ucOption);

//		EMVcollection = &g_EMVcollection;

		PINdisplay.GetTLVData = getData_loc;
		PINdisplay.SetTLVData = setData_loc;
//		PINdisplay.CheckBreak = checkBreak_PIN_loc;
//		PINdisplay.isICCpresent = isCardpresent_loc;
		
//		PINdisplay.SetPINprogress(g_CEMV.GetPINStatus());
		if(PINdisplay.Display(encPIN) != -1)
		{
			PINdisplay.ComputeEntryXY();
			
			Ret = PINdisplay.GetPIN(encPIN);
		}
		PINdisplay.Clear();
	}


	//checking pin for external kernels
	if((Ret == PIN_VERIFY_OK) && (g_CardAppConfig.GetPINtype() == CARD_APP_PIN_OFFLINE_EXT))
	{
		
		if((Ret = encPIN.VerifyPIN()) == 0x9000)
		{
			status = Ret = PIN_VERIFY_OK;
//			g_CEMV.SetPINEntryStatus(PIN_ENTRY_STATUS_OK);
		}
		else
		{
//			g_CEMV.SetPINEntryStatus(CheckPINresult((unsigned char)(Ret & 0xFF), (unsigned char)(Ret/0x100)));
			status = Ret = PIN_VERIFY_ERROR;
		}
	}

    dlog_msg("verify_pin return: %d, status: %X", Ret, status);
    g_CardAppConfig.SetPINtype(pintype);
    usEMVSetKernelTags(TAG_DF09_LAST_STATUS_WORD, sizeof(status), reinterpret_cast<unsigned char *>(&status));
	
	return Ret;
	

}
#endif
*/
int CEMV::SelectApplication(void)
{
    int autoFlag = g_CardAppConfig.GetAutoFlag();
    int iRet = checkStatus(CARD_APP_OPER_SELECT);
    
    if (iRet != CARD_APP_RET_OK) 
        return iRet;
    
    reasonOnline = REASON_ONLINE_UNKNOWN;
    app_sel_status = APP_SEL_STATUS_NOT_SET;
#ifdef VFI_GUI_GUIAPP
    if (g_CardAppConfig.GetUseDisplay())
    {
        if(1)
        {
            DisplayPromptSelectSection(CARD_APP_GUI_PLEASE_WAIT, CARD_APP_GUI_S_PROMPTS);
        }
    }
#endif

    dlog_msg("inVXEMVAPSelectApplication");
    iRet = inVXEMVAPSelectApplication(autoFlag, inMenuFunc, vdPromptManager, usAmtEntryFunc, usCandListModify);
    dlog_msg("inVXEMVAPSelectApplication RET: %d", iRet);
    #ifdef DOL_CHECKS
    if (!checkDolList(TAG_9F38_PDOL, CDOL_MAX_SIZE)) // PDOL_MAX_SIZE doesn't exist!
    {
        dlog_error("Invalid card!");
        iRet = CARD_BLOCKED;
    }
    #endif

    if (app_sel_status == APP_SEL_STATUS_DONE)
    {
        g_CEMVtrns.send_notify(CARD_APP_EVT_APP_SELECT_DONE);
    }

    if (SUCCESS != iRet) 
    {
        if (EMV_FAILURE == iRet)
        {
            iRet = CHIP_ERROR;
        }
        else if(CANDIDATELIST_EMPTY == iRet)
        {
            if (app_sel_status == APP_SEL_STATUS_CANCELLED)
            {
                dlog_msg("Cancel detected");
                iRet = TRANS_CANCELLED;
            }
            else
            {
                static const int AID_CNT = 10;
                srAIDListStatus * paidStatus = 0;
                unsigned short aidcount = AID_CNT;
                unsigned short tablecount = AID_CNT;
                unsigned short blockAidCount = 0;
                unsigned short emvRes = EMV_MORE_DATA;

                dlog_msg("No candidates, checking if blocked");
                do
                {
                    paidStatus = new srAIDListStatus[aidcount];
                    if (!paidStatus)
                    {
                        dlog_error("Cannot allocate memory for usEMVGetAllAIDStatus() call!");
                        emvRes = EMV_FAILURE;
                        break;
                    }
                    emvRes = usEMVGetAllAIDStatus(paidStatus, &aidcount, &blockAidCount);
                    dlog_msg("result %d, aid count %d, blocked aid count %d", emvRes, aidcount, blockAidCount);
                    if (aidcount == tablecount)
                    {
                        delete [] paidStatus; paidStatus = 0;
                        aidcount += AID_CNT;
                        tablecount = aidcount;
                    }
                } while (aidcount == tablecount);
                // dlog_msg("aid count %d, blocked aid count %d", aidcount, blockAidCount);
                if (emvRes != EMV_FAILURE)
                {
                    if (blockAidCount > 0)
                    {
                        iRet = APPL_BLOCKED ;
                    }
                    else if (aidcount > 0)
                    {
                        char tempBuf[100];
                        dlog_msg("Statuses: blocked %d, match found %d, no match found %d", AID_BLOCKED, AID_MATCH_FOUND, AID_NO_MATCH_FOUND);
                        found_matching_apps = false;
                        for (int i=0; i<aidcount; i++)
                        {
                            memset(tempBuf, 0, sizeof(tempBuf));
                            SVC_HEX_2_DSP((char *)paidStatus[i].stAID, tempBuf, paidStatus[i].lenOfAID);
                            dlog_msg("App '%s', status %d", tempBuf, paidStatus[i].status);
                            if (paidStatus[i].status == AID_MATCH_FOUND)
                            {
                                // Match found, but no candidates - probably GPO returned 6985. Card is configured though.
                                dlog_msg("Match found for some app - probably GPO returned 6985");
                                found_matching_apps = true;
                                break;
                            }
                        }
                    }
                }
                if (paidStatus) delete [] paidStatus;
            }
        }
        else if(CHIP_ERROR == iRet)
        {
            Terminate_CardSlot (ICC_reader, SWITCH_OFF_CARD);
            if(isCardRemoved())
            {
                iRet =  CARD_APP_RET_REMOVED;
            }
        }
        else if((APPL_BLOCKED != iRet) && (CARD_BLOCKED != iRet) && (CARD_REMOVED != iRet)&&
            (TRANS_CANCELLED != iRet) &&(BAD_ICC_RESPONSE != iRet) && (INVALID_PARAMETER != iRet)
            && (0x6985 != iRet))
        {
            //general error handling - typically chip error
            iRet =  CARD_APP_RET_FAIL;
        }
        // Set response here
        iRet = setResponse(iRet, CARD_APP_OPER_SELECT);
    }
    else
    {
        setResponse(iRet, CARD_APP_OPER_SELECT);
        iRet = CARD_APP_RET_OK;

        Ushort dLen = 0;
        const int ForNewline = 3;
        char buffer[16+ForNewline+1];
        char * pData = buffer + ForNewline;
        char codeTable = 1;
        if(usEMVGetTLVFromColxn(TAG_9F12_APPL_PRE_NAME, reinterpret_cast<unsigned char *>(pData), &dLen)!=EMV_SUCCESS)
        {
            if(usEMVGetTLVFromColxn(TAG_50_APPL_LABEL, reinterpret_cast<unsigned char *>(pData), &dLen)!=EMV_SUCCESS)
            {
                dlog_msg("Application preffered name and Application label missing");
            }
            else
            {
                pData[dLen]=0;
            }
        }
        else
        {
            //TODO: HERE WE SHOULD CHECK IF CODEPAGE IS SUPPORTED!
            pData[dLen]=0;
            if(usEMVGetTLVFromColxn(TAG_9F11_ISSSUER_CODE_TBL, reinterpret_cast<unsigned char *>(&codeTable), &dLen)==EMV_SUCCESS)
            {
                dlog_msg("Issuer code table index: %d", codeTable); 
            }
        }

#ifdef VFI_GUI_GUIAPP
        if(dLen > 0)
        {
            if (g_CardAppConfig.GetUseDisplay())
            {
                if(1)
                {
                    memcpy(buffer, "\n\\c", ForNewline);
                    DisplayText(const_cast<char *>(buffer), dLen+ForNewline, false); // do not clear the screen
                }
            }
        }
#endif        
        //updating configuration
        dlog_msg("Configuration update: %X", inVXEMVAPGetCardConfig(-1, -1));
    }
    dlog_msg("SelectApplication - RET: %X, Status: %0000000X:%0000000X", iRet, GetStatusMain(), GetStatusDetail());
    
    return iRet;
}

int CEMV::GetProcessingOptions(void)
{
    int iRet = checkStatus(CARD_APP_OPER_GPO);
    
    if (iRet != CARD_APP_RET_OK) 
        return iRet;

    if(isSeparateGPO())
    {
        iRet = setResponse(inVXEMVAPPerformGPO(), CARD_APP_OPER_GPO);
    }
    else
    {
        dlog_msg("GPO performed together with selection...");
        iRet = setResponse(0, CARD_APP_OPER_GPO);
    }
    dlog_msg("GetProcessingOptions - RET: %X, Status: %0000000X:%0000000X", iRet, GetStatusMain(), GetStatusDetail());
    
    return iRet;
}

#define BUF_MAX 100

int CEMV::ReadCardData(void)
{
    int iIndex = 0;
    unsigned short usAux = 0;
    int iRet = checkStatus(CARD_APP_OPER_READ);
    
    if (iRet != CARD_APP_RET_OK) 
        return iRet;

    iRet = inVXEMVAPProcessAFL();

#ifdef DOL_CHECKS
    if (!checkDolList(TAG_8C_CDOL1, CDOL_MAX_SIZE) || !checkDolList(TAG_8D_CDOL2, CDOL_MAX_SIZE) || !checkDolList(TAG_97_TDOL, TDOL_MAX_SIZE) || !checkDolList(TAG_9F49_DDOL, DDOL_MAX_SIZE))
    {
        dlog_error("Invalid card!");
        iRet = CARD_BLOCKED;
    }
#endif

    unsigned char *ptrack2 = new unsigned char[BUF_MAX];
    unsigned char *pPAN = new unsigned char[BUF_MAX];
    //Check if PAN from TLV = PAN present track 2 - if not BAD_TRACK2

    if((ptrack2 != NULL) && (pPAN != NULL))
    {
        dlog_msg("Checking track2 PAN consistency...");
        if (EMV_SUCCESS == usEMVGetTLVFromColxn(TAG_57_TRACK2_EQ_DATA, ptrack2, &usAux))
        {
             dlog_hex(ptrack2, usAux, "Track2 data:");
             iIndex = 6;                //PAN can not be less than 12 digits
             while(iIndex < usAux)      //we look for end of PAN data in track2 data
             {
                if((ptrack2[iIndex] & 0xD0) == 0xD0)
                {
                    ptrack2[iIndex] = 0x00;
                    break;
                }
                if((ptrack2[iIndex] & 0x0D) == 0x0D)
                {
                    ptrack2[iIndex] &= 0xF0;
                    iIndex++;
                    break;
                }
                iIndex++;
             }
             //Get the PAN from the collection
             usAux = 0;
             if(EMV_SUCCESS == usEMVGetTLVFromColxn(TAG_5A_APPL_PAN, pPAN, &usAux))
             {
                dlog_hex(pPAN, usAux, "PAN data:");
                while(pPAN[usAux-1] == 0xFF)
                    usAux--;
                    
                if((pPAN[usAux-1] & 0x0F) == 0x0F)
                    pPAN[usAux-1] &= 0xF0;

                if(iIndex < usAux)
                    iIndex = usAux;
            }
            else
            {
                dlog_msg("Appl PAN (5A) data not present!");
            }
            if(memcmp(pPAN, ptrack2, iIndex) !=0)
            {
                dlog_msg("PAN does not match!!! - ABORTING...");
                iRet = CARD_BLOCKED; // CHIP_ERROR allows fallback, so cannot be used here. We assume the app is blocked. 
            }
        }
        else
        {
            dlog_msg("Track2 (57) data not present!");
        }
    }
    else
    {
        dlog_msg("Memory problem!!!");
    }
    delete [] ptrack2;
    delete [] pPAN;
    iRet = setResponse(iRet, CARD_APP_OPER_READ);
    dlog_msg("ReadCardData - RET: %X, Status: %0000000X:%0000000X", iRet, GetStatusMain(), GetStatusDetail());
    if (iRet == 0)
    {
        // Tags cache
        g_CEMVtrns.CacheTags();
    }
    return CARD_APP_RET_OK;
}

int CEMV::CardholderVerification(void)
{
    int iRet = checkStatus(CARD_APP_OPER_VERIFY);
    
    if (iRet != CARD_APP_RET_OK) 
        return iRet;

    if (pPinDisplay != NULL)
    {
        delete pPinDisplay; pPinDisplay = NULL;
    }

#ifdef VFI_GUI_GUIAPP
    if (g_CardAppConfig.GuiInterface() == CCardAppConfig::GUI_APP) 
    {
        pPinDisplay = new cPINdisplayGuiApp;
        dlog_msg("Using GuiAPP for PIN entry ptr:%X", pPinDisplay);
    }
#endif

#ifdef VFI_GUI_DIRECTGUI
    if (g_CardAppConfig.GuiInterface() == CCardAppConfig::GUI_FLEXI)
    {
        #if 0
        pPinDisplay = new cPINdisplayFlexi();
        #else
        pPinDisplay = new cPINdisplayDirectGUI();
        #endif
    }
    else
    {
        pPinDisplay = new cPINdisplayDirectGUI();
    }
#endif

#ifdef LOGSYS_FLAG
    short dLen;
    unsigned char TermCap[3];
    if(usEMVGetTLVFromColxn(TAG_9F33_TERM_CAP, TermCap, &dLen)==EMV_SUCCESS)
    {
        dlog_msg("Terminal Capabilities (9F33): %02.02X%02.02X%02.02X", TermCap[0], TermCap[1], TermCap[2]);
    }
#endif

    // secure pin...
    int owner = 0;

    if (setPINParams())
    {
        if (SUCCESS != inVXEMVAPSetDisplayPINPrompt(displaySecurePINPrompt))
        {
            dlog_msg("Cannot display PIN prompt!");
        }
        else
        {
            dlog_msg("PIN prompt function registered");
        }
    }
    else
    {
        dlog_msg("Cannot set PIN Parameters!");
    }

    unsigned char pinTryCtr = 3;
    unsigned short pinTryCtrLen = 0;
    usEMVGetTLVFromColxn(TAG_9F17_PIN_TRY_COUNTER, &pinTryCtr, &pinTryCtrLen);
    switch (pinTryCtr)
    {
      default:
        SetPINStatus(PIN_ENTRY_STATUS_OK);
        break;
      //case 2:
      //  g_CEMV.SetPINStatus(PIN_ENTRY_STATUS_BAD_PIN);
      //  break;
      case 1:
        SetPINStatus(PIN_ENTRY_STATUS_LAST_TRY);
        break;
    }

    //SetPINStatus(PIN_ENTRY_STATUS_OK);
    iRet = setResponse(inVXEMVAPVerifyCardholder(NULL), CARD_APP_OPER_VERIFY);
    dlog_msg("CardholderVerification - RET: %X, Status: %0000000X:%0000000X", iRet, GetStatusMain(), GetStatusDetail());
    if (pPinDisplay)
    {
        pPinDisplay->Clear();
        delete pPinDisplay; pPinDisplay = NULL;
    }
    g_CEMVtrns.send_notify(CARD_APP_EVT_PIN_DONE);

    //it seems that pin entry opens and keeps crypto, closing...
    int task_id=0;
    int crypto_handle = get_owner("/dev/crypto", &task_id);
    
    dlog_msg("Crypto opened by %d, handle: %d", task_id, crypto_handle);
    if (task_id == get_task_id() && crypto_handle >= 0)
    {
        dlog_msg("Closing crypto...");
        close(crypto_handle);
    }
#ifdef VFI_GUI_DIRECTGUI
#endif

#ifdef VFI_GUI_GUIAPP
    if (g_CardAppConfig.GetUseDisplay())
    {
        if(g_CardAppConfig.GuiInterface() == CCardAppConfig::GUI_APP)
        {
            //
        }
        else
        {
            DisplayPromptSelectSection(CARD_APP_GUI_PLEASE_WAIT, CARD_APP_GUI_S_PROMPTS);
        }
    }
#endif    
    stopBeeperThread();
    return iRet;
}

int CEMV::ProcessRestrictions(void)
{
    int iRet = checkStatus(CARD_APP_OPER_RESTRICT);
    
    if (iRet != CARD_APP_RET_OK) 
        return iRet;
    
    iRet = setResponse(inVXEMVAPProcRestrictions(), CARD_APP_OPER_RESTRICT);
    dlog_msg("ProcessRestrictions - RET: %X, Status: %0000000X:%0000000X", iRet, GetStatusMain(), GetStatusDetail());
    // Reason Online
    if (reasonOnline == REASON_ONLINE_UNKNOWN)
    {
        srTxnResult TVRTSI;
        unsigned char tvrByte;
        usEMVGetTxnStatus(&TVRTSI);
        tvrByte = TVRTSI.stTVR[1]; // get byte 2
        if (tvrByte & 0x20) // check bit 6 (Card not yet effective)
        {
            reasonOnline = REASON_ONLINE_FORCED_BY_ACCEPTOR;
        }
        else if (tvrByte & 0x40) // check bit 7 (Card Expired)
        {
            reasonOnline = REASON_ONLINE_FORCED_BY_ISSUER;
        }
    }
    return CARD_APP_RET_OK;
}

int CEMV::TerminalRiskManagement(void)
{
    int iRet = checkStatus(CARD_APP_OPER_TRM);
    
    if (iRet != CARD_APP_RET_OK) 
        return iRet;
    
    if (g_CEMV.isCardBlackListed() == EMV_TRUE) reasonOnline = REASON_ONLINE_FORCED_BY_ISSUER;
    iRet = setResponse(inVXEMVAPTermRisk(bIsCardBlackListed), CARD_APP_OPER_TRM);
    dlog_msg("TerminalRiskManagement - RET: %X, Status: %0000000X:%0000000X", iRet, GetStatusMain(), GetStatusDetail());
    // Reason Online
    if (reasonOnline == REASON_ONLINE_UNKNOWN)
    {
        srTxnResult TVRTSI;
        unsigned char tvrByte;
        usEMVGetTxnStatus(&TVRTSI);
        tvrByte = TVRTSI.stTVR[3]; // get byte 4
        if (tvrByte & 0x80) // check bit 8 (transaction exceeds floor limit)
        {
            reasonOnline = REASON_ONLINE_OVER_FLOOR;
        }
        else if (tvrByte & 0x10) // check bit 5 (transaction randomly selected for online)
        {
            reasonOnline = REASON_ONLINE_RANDOM_SELECTION;
        }
        else if ((tvrByte & 0x20) || (tvrByte & 0x40)) // check bits 6/7 (upper/lower consecutive offline limit exceeded)
        {
            reasonOnline = REASON_ONLINE_FORCED_BY_ACCEPTOR;
        }
        else
        {
            tvrByte = TVRTSI.stTVR[1]; // get byte 2
            if (tvrByte & 0x08) // check bit 4 (new card)
            {
                reasonOnline = REASON_ONLINE_FORCED_BY_ISSUER;
            }
        }
    }
    return CARD_APP_RET_OK;
}

int CEMV::DataAuthentication(void)
{
    int iRet = checkStatus(CARD_APP_OPER_DATA_AUTH);
    
    if (iRet != CARD_APP_RET_OK) 
        return iRet;
        
    usEMVSetCDAMode(g_CardAppConfig.GetCDAMode());
    iRet = setResponse(inVXEMVAPDataAuthentication(NULL), CARD_APP_OPER_DATA_AUTH);
    dlog_msg("DataAuthentication - RET: %X, Status: %0000000X:%0000000X", iRet, GetStatusMain(), GetStatusDetail());
        
    return iRet;
}

int CEMV::FirstAC(void)
{
    int iRet = checkStatus(CARD_APP_OPER_FIRST_AC);
    unsigned char CID;
    unsigned short tagLen;

    if(iRet == CARD_APP_RET_ORDER)
    {
        // We know order is improper - still, we shall check whether we're performing Refund and we're done with GPO and previous steps
        if (!checkStatus(CARD_APP_OPER_TRM)) // Check for CARD_APP_OPER_RESTRICT / CARD_APP_OPER_VERIFY / CARD_APP_OPER_DATA_AUTH would do as well
        {
            // We're not past GPO stage - error!
            return CARD_APP_RET_ORDER;
        }
        // Now let's check TranType tag
        Ushort dLen = 0;
        char pData;
        if(usEMVGetTLVFromColxn(TAG_9C00_TRAN_TYPE, reinterpret_cast<unsigned char *>(&pData), &dLen)!=EMV_SUCCESS)
        {
            // Unlikely - no TranType set??
            return CARD_APP_RET_ORDER;
        }
        // Check if TranType is Refund
        dlog_msg("TranType is %02Xh", pData);
        if (dLen != 1 || pData != 32)
        {
            // TranType is not a Refund - error!
            return CARD_APP_RET_ORDER;
        }
        dlog_alert("Allowing 1stGenAC as proper short path for Refunds only - but forcing Decline!");
        term_decision = FORCED_DECLINE;
    }
    else if (iRet != CARD_APP_RET_OK)
        return iRet;
    
    if (term_decision != FORCED_DECLINE && term_decision != FORCED_ONLINE && term_decision != 0)
    {
        term_decision = 0;
    }
    dlog_msg("Terminal decision set %d", term_decision);
    iRet = setResponse(inVXEMVAPFirstGenerateAC(term_decision), CARD_APP_OPER_FIRST_AC);
    dlog_msg("FirstAC - RET: %X, Status: %0000000X:%0000000X", iRet, GetStatusMain(), GetStatusDetail());
    // Reason Online
    if (reasonOnline == REASON_ONLINE_UNKNOWN)
    {
        // WARNING! WARNING!
        // The below won't work because the Kernel always sets ARC to 'Y2'!!!
        // TODO: Kernel must be changed!
        srTxnResult TVRTSI;
        unsigned char tvrByte;
        usEMVGetTxnStatus(&TVRTSI);
        bool allZeroes = false;
        unsigned char arc[2];
        unsigned short dLen = 0;
        if (TVRTSI.stTVR[0] == TVRTSI.stTVR[1] == TVRTSI.stTVR[2] == TVRTSI.stTVR[3] ==TVRTSI.stTVR[4] == 0) allZeroes = true;
        // Y1 = offline approved
        // Z1 = offline declined
        // No value = online
        if (usEMVGetTLVFromColxn(TAG_8A_AUTH_RESP_CODE, arc, &dLen) == EMV_SUCCESS)
        {
            dlog_hex(arc, sizeof(arc), "ARC AUTH RESP CODE");
        }
        if (allZeroes && (dLen == 2 && arc[0] == 'Y' && arc[1] == '1'))
        {
            reasonOnline = REASON_ONLINE_FORCED_BY_IC;
        }
        else if (!allZeroes)
        {
            reasonOnline = REASON_ONLINE_FORCED_BY_PED;
        }
        else
        {
            reasonOnline = REASON_ONLINE_FORCED_BY_ACCEPTOR;
        }
    }
    usEMVGetTLVFromColxn( TAG_9F27_CRYPT_INFO_DATA, &CID, &tagLen );
    dlog_msg("FirstAC - CID %X", CID);
    if((CID & GEN_TC) == GEN_TC)
    {
        SetStatusMain(CARD_APP_STATUS_M_OK | CARD_APP_STATUS_M_STOP);
        SetStatusDetail(CARD_APP_STATUS_D_OK | CARD_APP_STATUS_D_NO_FALLBACK);
        return CARD_APP_RET_APPROVED;
    }
    else if((CID & GEN_ARQC) == GEN_ARQC)
    {
        SetStatusMain(CARD_APP_STATUS_M_OK | CARD_APP_STATUS_M_CONTINUE);
        SetStatusDetail(CARD_APP_STATUS_D_OK | CARD_APP_STATUS_D_NO_FALLBACK);
        return CARD_APP_RET_ONLINE;
    }
    else if((CID & GEN_AAC) == GEN_AAC)
    {
        SetStatusMain(CARD_APP_STATUS_M_OK | CARD_APP_STATUS_M_STOP);
        SetStatusDetail(CARD_APP_STATUS_D_OK | CARD_APP_STATUS_D_NO_FALLBACK);
        return CARD_APP_RET_DECLINED;
    }
    else
    {
        return CARD_APP_RET_FAIL;
    }
    
    return CARD_APP_RET_OK;
}

int CEMV::ExternalAuthenticate(void)
{
    int iRet = checkStatus(CARD_APP_OPER_EXT_AUTH);
    
    if (iRet != CARD_APP_RET_OK) 
        return iRet;
    
    iRet = setResponse(inVXEMVAPAuthIssuer(), CARD_APP_OPER_EXT_AUTH);
    dlog_msg("ExternalAuthenticate - RET: %X, Status: %0000000X:%0000000X", iRet, GetStatusMain(), GetStatusDetail());
    return iRet;
}

int CEMV::ProcessScript(void)
{
    unsigned char scriptID[4];
    int iRet = checkStatus(CARD_APP_OPER_SCRIPT);
    
    if (iRet != CARD_APP_RET_OK) 
        return iRet;
    
    iRet = setResponse(inVXEMVAPProcessScript(scriptID, script_result), CARD_APP_OPER_SCRIPT);
    dlog_msg("ProcessScript - RET: %X, Status: %0000000X:%0000000X", iRet, GetStatusMain(), GetStatusDetail());
    return iRet;
}

int CEMV::SecondAC(void)
{
    unsigned char CID;
    unsigned short tagLen;
    int iRet = checkStatus(CARD_APP_OPER_SECOND_AC);
    
    if (iRet != CARD_APP_RET_OK) 
        return iRet;
    
    iRet = setResponse(inVXEMVAPSecondGenerateAC(term_decision), CARD_APP_OPER_SECOND_AC);
    dlog_msg("SecondAC - RET: %X, Status: %0000000X:%0000000X", iRet, GetStatusMain(), GetStatusDetail());
    usEMVGetTLVFromColxn( TAG_9F27_CRYPT_INFO_DATA, &CID, &tagLen );
    dlog_msg("SecondAC - CID %X", CID);
    SetStatusMain(CARD_APP_STATUS_M_OK | CARD_APP_STATUS_M_STOP);
    SetStatusDetail(CARD_APP_STATUS_D_OK | CARD_APP_STATUS_D_NO_FALLBACK);
    if((CID & GEN_AAC) == GEN_AAC)
    {
        return CARD_APP_RET_DECLINED;
    }
    else if((CID & GEN_TC) == GEN_TC)
    {
        return CARD_APP_RET_APPROVED;
    }
    else
    {
        return CARD_APP_RET_FAIL;
    }
    return CARD_APP_RET_OK;
}

int CEMV::PostOnline(void)
{
    int num_scripts = 0;
    unsigned char CID;
    unsigned short tagLen;

    int iRet = checkStatus(CARD_APP_OPER_SECOND_AC);
    
    if (iRet != CARD_APP_RET_OK) 
        return iRet;

    script_size71 = 0;
    script_size72 = 0;
    if (term_decision != HOST_AUTHORISED && term_decision != HOST_DECLINED && term_decision != FAILED_TO_CONNECT)
    {
        term_decision = FAILED_TO_CONNECT;
    }
    dlog_msg("Terminal decision set %d", term_decision);
    //usEMVSetCDAMode(g_CardAppConfig.GetCDAMode());
//  iRet = setResponse(inVXEMVAPUseHostData(term_decision, script_result, &num_scripts), CARD_APP_OPER_EXT_AUTH | CARD_APP_OPER_SCRIPT | CARD_APP_OPER_SECOND_AC);
    //iRet = inVXEMVAPUseHostData(term_decision, script_result, &num_scripts);
    iRet = inVXEMVAPUseHostData(term_decision, script_result, &num_scripts, g_CEMVtrns.script71, g_CEMVtrns.script71_len, g_CEMVtrns.script72, g_CEMVtrns.script72_len);

    if (iRet == 0x6985) // Problem with External Authentificate?
    {
        char CID;
        Ushort dLen = 0;
        if(usEMVGetTLVFromColxn(TAG_9F27_CRYPT_INFO_DATA, reinterpret_cast<unsigned char *>(&CID), &dLen) == EMV_SUCCESS && CID == 0x80) // ARQC, from 1st Generate
        {
            iRet = inVXEMVAPSecondGenerateAC(term_decision);
            dlog_msg("2nd Gen AC result %d", iRet);
        }
    }
    if (iRet != 0)
    {
        char CID = 0x00; // AAC
        // if (term_decision == 1) CID = 0x40; // TC
        dlog_alert("UseHostData result %04Xh, overriding it to OK and returning %s!", iRet, (CID == 0 ? "AAC" : "TC"));
        usEMVUpdateTLVInCollxn (TAG_9F27_CRYPT_INFO_DATA , (uint8_t *)&CID, 1);
        iRet = 0;
    }
    iRet = setResponse(iRet, CARD_APP_OPER_EXT_AUTH | CARD_APP_OPER_SCRIPT | CARD_APP_OPER_SECOND_AC);
    dlog_msg("PostOnline - RET: %X, Status: %0000000X:%0000000X", iRet, GetStatusMain(), GetStatusDetail());
    //the code below is dedicated for getting script results separately for both script types 71 and 72
    dlog_msg("Issuer scripts processed: %d", num_scripts);
    dlog_hex(script_result, num_scripts*5, "Script results");
    if(num_scripts > 0)
    {
        dlog_msg("script 71 size = %d bytes, script 72 size = %d bytes", g_CEMVtrns.script71_len, g_CEMVtrns.script72_len);
        if(g_CEMVtrns.script71_len == 0)
        {
            dlog_msg("Only script 72 used");
            script_size72 = num_scripts*5;
            memcpy(script_result72, script_result, script_size72);
        }
        else if(g_CEMVtrns.script72_len == 0) 
        {
            dlog_msg("Only script 71 used");
            script_size71 = num_scripts*5;
            memcpy(script_result71, script_result, script_size71);
        }
        else if(g_CEMVtrns.script71_len > 0 && g_CEMVtrns.script72_len > 0)
        {
            //we do "simplified parsing" here by looking for token, it can be changed into full parsing if neceseary
            int found = 0;
            unsigned char token[] = {0x9F, 0x18, 0x04, 0x00, 0x00, 0x00, 0x00};
            for(int i=0; i < num_scripts; i++)
            {
                memcpy(token+3, script_result+1+(i*5), 4);
                //now we must find token in script71.dat or script72.dat
                for(int j=0; j< g_CEMVtrns.script71_len-7; j++)
                {
                    //this routine is "sub-optimal", but due to potential size of data it doesn't make sense to optimize it
                    if(memcmp(token, g_CEMVtrns.script71+j, 7) == 0)
                    {
                        dlog_msg("Script belongs to type 71");
                        memcpy(script_result71+script_size71, script_result+(i*5), 5);
                        script_size71+=5;
                        found = 1;
                        break;
                    }
                }
                if(!found)
                {
                    for(int j=0; j< g_CEMVtrns.script72_len-7; j++)
                    {
                        //this routine is "sub-optimal", but due to potential size of data it doesn't make sense to optimize it
                        if(memcmp(token, g_CEMVtrns.script72+j, 7) == 0)
                        {
                            dlog_msg("Script belongs to type 72");
                            memcpy(script_result72+script_size72, script_result+(i*5), 5);
                            script_size72+=5;
                            found = 1;
                            break;
                        }
                    }
                }
                found = 0;
            }
        }
        g_CEMVtrns.script71 = NULL;
        g_CEMVtrns.script71_len = 0;
        g_CEMVtrns.script72 = NULL;
        g_CEMVtrns.script72_len = 0;
    }
    usEMVGetTLVFromColxn( TAG_9F27_CRYPT_INFO_DATA, &CID, &tagLen );
    dlog_msg("SecondAC - CID %X", CID);
    SetStatusMain(CARD_APP_STATUS_M_OK | CARD_APP_STATUS_M_STOP);
    SetStatusDetail(CARD_APP_STATUS_D_OK | CARD_APP_STATUS_D_NO_FALLBACK);
    if((CID & GEN_AAC) == GEN_AAC)
    {
        return CARD_APP_RET_DECLINED;
    }
    else if((CID & GEN_TC) == GEN_TC)
    {
        return CARD_APP_RET_APPROVED;
    }
    else
    {
        return CARD_APP_RET_FAIL;
    }
    return CARD_APP_RET_OK;
}

int CEMV::AddScript(char *script)
{
    int iRet = CARD_APP_RET_OK;
    return iRet;
}

int CEMV::UpdateTLV(unsigned char *list, int list_len)
{
    int iRet = CARD_APP_RET_OK;
    return iRet;
}

int CEMV::ReadTLV(unsigned char *list, int list_len, unsigned char *out_list, int out_list_len)
{
    int iRet = CARD_APP_RET_OK;
    return iRet;
}

/* beeper thread */
typedef struct 
{
    long timeout;
    int mainTaskID;
} BEEPER_TIMEOUT_PARAMS;

int beeper_timeout_routine(BEEPER_TIMEOUT_PARAMS *params)
{
    static const long EVENT_TIMEOUT = EVT_BIO;
    int result = 0;
    int timerID = 0;

	event_item ev_timer;
	ev_timer.event_id = events::timer;
	int events_mon = event_open();
	eventset_t pml_events;

	int iRet = event_ctl(events_mon, ctl::ADD, ev_timer);
	dlog_msg("Adding events::ipc ret :%X", iRet);

    if (params && params->timeout > 0)
    {
        com_verifone_pml::normal_tone();
        set_timer(ev_timer, params->timeout); 
        event_ctl(events_mon, ctl::ADD, ev_timer);
        //dlog_msg("Waiting for %d milliseconds or interruption", params->timeout);
        do
        {
			if(event_wait(events_mon, pml_events))
			{
				for (eventset_t::iterator it = pml_events.begin() ; it != pml_events.end(); ++it)
				{
			   		if (it->event_id == events::timer)
			   		{
						//dlog_alert("TGK Timed Out!");
                		com_verifone_pml::normal_tone();
                		timerID = set_timer(ev_timer, params->timeout);
			   		}

				}
			}
			//we should consider break on user event reception
            
        } while(true);
    } 
    clr_timer(ev_timer);
    if (params) delete params;
    event_close(events_mon);
    return 0;
}

bool CEMV::runBeeperThread()
{
    if (beeperThreadID == 0)
    {
        BEEPER_TIMEOUT_PARAMS *params = new BEEPER_TIMEOUT_PARAMS;
        params->timeout = g_CardAppConfig.GetPINbeeperTimeout() * 1000; // config timeout is measured in seconds
        // params->mainTaskID = get_task_id();
        beeperThreadID = run_thread(reinterpret_cast<int>(beeper_timeout_routine), reinterpret_cast<int>(params), 1 * 1024);
    }
    return (beeperThreadID != 0);
}

bool CEMV::stopBeeperThread()
{
    if (beeperThreadID)
    {
        dlog_msg("Killing beeper thread");
        post_user_event(beeperThreadID, 1); // post any user event, thread will end this way
        beeperThreadID = 0;
    }
    return true;
}

