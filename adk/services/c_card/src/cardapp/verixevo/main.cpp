#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 *
 * Copyright (C) 2007 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/
/**
 * @file        main.cpp
 *
 * @author      Jacek Olbrys
 *
 * @brief       Card Service - The main module
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C++ Coding
 *              Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/

/*************************************
 Pulling in all required include files, in extern "C" as we don't
 know whether or not the files are CPP defintions or C functions */
#include <svc.h>
#include <stdio.h>

#include <liblog/logsys.h>

#include "cBaseConfig.h"
#include "cCARDAppConfig.h"
#include "tlv_parser.h"

#include "cEMV.h"
#include "cEMVmain.h"

#include <cardslot.h>

#include "cIPCInterface.h"
#include <libipc/ipc.h>
#include <libpml/pml.h>
#include <libpingui.h>

#include <EMVResult.hpp>
#include <EMVCWrappers.h>

#ifdef VFI_GUI_DIRECTGUI
#  ifdef DIRECTGUI2
#    include <html/gui.h>
#  else
#    include <gui/gui.h>
#  endif
#  ifdef DIRECTGUI_PROXY
#    include <libguiclient/guiclient.h>
#  endif
#endif


/***************************************************************************
 * Using
 **************************************************************************/
using namespace com_verifone_ipcpacket;
using namespace com_verifone_cmd;
using namespace com_verifone_emv;
using namespace com_verifone_interface;
using namespace com_verifone_ipc;
using namespace com_verifone_pml;


/***************************************************************************
 * Global Variables
 **************************************************************************/
CCardAppConfig  g_CardAppConfig;
CEMV            g_CEMV;
CEMVtrns        g_CEMVtrns;
cIPCInterface * pInterface = NULL;

extern int monitoring_thread;
const char apLogicalName[] = "CARDAPP";

void register_kernel_version()
{
    static const char * EMV_LIB = "EMV Kernel";
    unsigned char EMVVersion[25+1];
    memset(EMVVersion, 0, sizeof(EMVVersion));
    usEMVGetExtendedVersion(EMVVersion);
    com_verifone_pml::appver::register_library(EMV_LIB, reinterpret_cast<char *>(EMVVersion));
}

/***************************************************************************
 * Master Application - main()
 **************************************************************************/
//Main application
void guiSetup(){
#ifdef VFI_GUI_DIRECTGUI
#  ifndef DIRECTGUI_PROXY
    dlog_msg("Using DirectGUI");
    vfigui::uiReadConfig();
    
    std::string resourceDirectory(apLogicalName);
    std::transform(resourceDirectory.begin(), resourceDirectory.end(), resourceDirectory.begin(), ::tolower);
    
    // setting up resource directory
    std::string currRsrcPath, defaultRsrcPath;
    vfigui::uiGetPropertyString(vfigui::UI_PROP_RESOURCE_PATH, currRsrcPath);
    vfigui::uiGetPropertyString(vfigui::UI_PROP_RESOURCE_DEFAULT_PATH, defaultRsrcPath);

    vfigui::uiSetPropertyString(vfigui::UI_PROP_RESOURCE_PATH, currRsrcPath + "/" + resourceDirectory);
    vfigui::uiSetPropertyString(vfigui::UI_PROP_RESOURCE_DEFAULT_PATH, defaultRsrcPath + "/" + resourceDirectory);
#  else // DIRECTGUI_PROXY defined
    dlog_msg("Using GUIProxy");
    vfiguiproxy::GUIClient::instance().init(); 
    vfiguiproxy::GUIClient::instance().setLanguagesDir("cardapp/langs");
#  endif
#endif
}

#if defined VFI_GUI_DIRECTGUI && !defined DIRECTGUI_PROXY && !defined DIRECTGUI2
#  define MAIN_RETURN_VALUE(value) /*nothing*/
void uiMain(int argc, char *argv[])
#else
#  define MAIN_RETURN_VALUE(value) value
int main(int argc, char *argv[])
#endif
{
    CCommandMan       *pCommandMan   = NULL;
    CCmdEMVmain       *pEMVmainCmd = NULL;

    if ( (_SYS_VERSION>=0x301)			//exported by this SDK 
        &&(_syslib_version()>=0x301) )	//implemented in the OS 
    {
        setFree(FREE_TYPE_NO_VALIDATE);
    }

    LOG_INIT( (char*)apLogicalName, LOGSYS_COMM,LOGSYS_PRINTF_FILTER);
    dlog_msg("*****%s has STARTED - task id: %d*****",  apLogicalName, get_task_id());
    SVC_WAIT(0);

    {
      std::string name(CARDAPP_NAME);
      std::transform(name.begin(), name.end(), name.begin(), ::toupper);

      dlog_msg("calling pml_init....");
      int initRet = com_verifone_pml::pml_init(name.c_str());
      dlog_msg("PML init ret: %d", initRet);
      // add versioning
      com_verifone_pml::appver::add_app_version(CARDAPP_VERSION);
      // Register EMV Kernel version
      register_kernel_version();
      // Register libpingui version
      com_verifone_pin_gui::init();
    }

    //Checking terminal type
    unsigned long EXT_result = 0;

    g_CardAppConfig.SetTerminalType(1);		//must be set later - JACEK

    pCommandMan = new CCommandMan();
    pEMVmainCmd = new CCmdEMVmain();

    pCommandMan->regCmd(INSTALL_VSS_COM, pEMVmainCmd);
    pCommandMan->regCmd(EXECUTE_VSS_MACRO_COM, pEMVmainCmd);
    pCommandMan->regCmd(SUPER_EMV_COM, pEMVmainCmd);
    pCommandMan->regCmd(START_WAITING_COM, pEMVmainCmd);
    pCommandMan->regCmd(MONITOR_ICC_COM, pEMVmainCmd);
    pCommandMan->regCmd(UNSOLICITED_EVENT_COM, pEMVmainCmd);
    pCommandMan->regCmd(SEND_APDU_COM, pEMVmainCmd);
    pCommandMan->regCmd(SET_GET_TLV_COM, pEMVmainCmd);
    pCommandMan->regCmd(SET_PIN_PARAMS_COM, pEMVmainCmd);
    pCommandMan->regCmd(VERIFY_PIN_COM, pEMVmainCmd);
    pCommandMan->regCmd(SET_MESSAGE_COM, pEMVmainCmd);
    pCommandMan->regCmd(L1_BEGIN_TRANSACTION_COM, pEMVmainCmd);
    pCommandMan->regCmd(L1_COMPLETE_TRN_COM, pEMVmainCmd);
    pCommandMan->regCmd(L2_SELECT_READ_COM, pEMVmainCmd);
    pCommandMan->regCmd(L2_TRM_VERIFY_COM, pEMVmainCmd);
    pCommandMan->regCmd(L2_FIRST_AC_COM, pEMVmainCmd);
    pCommandMan->regCmd(L2_COMPLETE_COM, pEMVmainCmd);
    pCommandMan->regCmd(L3_SELECT_COM, pEMVmainCmd);
    pCommandMan->regCmd(L3_GPO_COM, pEMVmainCmd);
    pCommandMan->regCmd(L3_READ_DATA_COM, pEMVmainCmd);
    pCommandMan->regCmd(L3_CARDHOLDER_VERIFY_COM, pEMVmainCmd);
    pCommandMan->regCmd(L3_TRM_COM, pEMVmainCmd);
    pCommandMan->regCmd(L3_DATA_AUTH_COM, pEMVmainCmd);
    pCommandMan->regCmd(L3_FIRST_AC_COM, pEMVmainCmd);
    pCommandMan->regCmd(L3_EXT_AUTH_COM, pEMVmainCmd);
    pCommandMan->regCmd(L3_SCRIPT_PROCESSING_COM, pEMVmainCmd);
    pCommandMan->regCmd(L3_SECOND_AC_COM, pEMVmainCmd);
    pCommandMan->regCmd(MEM_READ_I2C_CARD, pEMVmainCmd);
    pCommandMan->regCmd(MEM_WRITE_I2C_CARD, pEMVmainCmd);
    pCommandMan->regCmd(MEM_READ_MEMORY_CARD, pEMVmainCmd);
    pCommandMan->regCmd(MEM_WRITE_MEMORY_CARD, pEMVmainCmd);
    pCommandMan->regCmd(MEM_UPDATE_MEMORY_CARD, pEMVmainCmd);
    pCommandMan->regCmd(RESET_EMV_CONFIG, pEMVmainCmd);


    pInterface = new cIPCInterface(apLogicalName, pCommandMan);

    if(pInterface == NULL)
    {
        dlog_error( "new() for interface returned NULL!" );
        #if defined VFI_GUI_DIRECTGUI && !defined DIRECTGUI_PROXY
        return;
        #else
        return -1;
        #endif
    }
    if(pInterface->impInitialise(argc,argv) != IPC_SUCCESS)
    {
        dlog_error( "unable to initialise interface" );
        #if defined VFI_GUI_DIRECTGUI && !defined DIRECTGUI_PROXY
        return;
        #else
        return -1;
        #endif
    }
    dlog_msg("SUCCESS: IPC interface initialised");

#ifdef VFI_GUI_DIRECTGUI
    guiSetup();
#endif

    if(g_CardAppConfig.iReadConfig(g_CardAppConfig.getName()) != ESUCCESS) // Initialise configuration
    {
        dlog_error("ERROR: CardAppConfig->init() has failed!");
        // TODO: Should we report an error here?
    }

    dlog_msg("Trident platform detected, enabling ICC events");
    g_CEMV.startICCMonitoring();
#ifdef VFI_GUI_GUIAPP
    // Check if GuiApp is available
    if (com_verifone_ipc::is_task_available(GUI_APP_NAME_))
    {
        dlog_msg("GuiApp is available over IPC!");
        if (com_verifone_ipc::connect_to(GUI_APP_NAME_) == com_verifone_ipc::IPC_SUCCESS)
        {
            dlog_msg("GuiApp is initialized, we're using it!");
            g_CardAppConfig.SetGuiInterface(CCardAppConfig::GUI_APP);
        }
    }
    g_CEMV.configureCallbacks(g_CardAppConfig.GuiInterface() == CCardAppConfig::GUI_APP);
#endif
    g_CardAppConfig.SetExtAuthFlag(EMV_SUCCESS);
    g_CardAppConfig.SetCDAMode(CDA_MODE_3);

#ifdef VFI_GUI_DIRECTGUI
    g_CEMV.configureCallbacks(true);
#endif
    
    //dlog_msg("TERMINAL TYPE DETECTED: %d EXT:%ld", g_CardAppConfig.GetTerminalType(), EXT_result);

    g_CEMVtrns.init_the_kernel();

    pInterface->impWaitRequest();

    return MAIN_RETURN_VALUE(0);
}

