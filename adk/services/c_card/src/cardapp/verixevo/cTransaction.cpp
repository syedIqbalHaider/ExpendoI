#ifndef __cplusplus
#  error "This file is for C++ only!"
#endif

/***************************************************************************
 *
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 *
 **************************************************************************/
/**
 * @file        cTransaction.cpp
 *
 * @author      Jacek Olbrys
 *
 * @brief       Transaction Handler
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C++ Coding
 *              Standard 1.0.x
 */

/***************************************************************************
 * Includes
 **************************************************************************/
#include <svc.h>
#include <svc_sec.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <SLE44x2.h>
#include <EE2K4K.h>

#include <liblog/logsys.h>
#include <libpml/pml.h>
#include <libprompts/user_prompt_lite.hpp>
#include <libfoundation/HWInfo.h>

#ifdef VFI_GUI_GUIAPP
#include "GuiApp.h"
#endif

#include "cEMV.h"
#include <emvTags.hpp>
#include <EMVResult.hpp>
#include "cTransaction.h"
#include "cCARDAppConfig.h"
#include "EMVtools.h"

#include <tlv-lite/ConstData.h>
#include <tlv-lite/ValueConverter.hpp>
#include <tlv-lite/ConstValue.hpp>

extern "C"
{
#include "vxemvap.h"
}

/***************************************************************************
 * Using
 **************************************************************************/
using namespace com_verifone_util;
using namespace com_verifone_guiapi;
using namespace com_verifone_emv;
using namespace com_verifone_pml;

/***************************************************************************
 * external global variables
 **************************************************************************/
extern CCardAppConfig   g_CardAppConfig;
extern CEMV         g_CEMV;
extern CEMVtrns     g_CEMVtrns;
extern EMV_CONFIG EMVGlobConfig;


/***************************************************************************
 * Exportable function definitions
 **************************************************************************/

/***************************************************************************
 * Exportable class members' definitions
 **************************************************************************/

CEMVtrns::CEMVtrns(void)
{
    InTLVlist = NULL;
    InTLV_list_len = 0;
    OutTLVlist = NULL;
    OutTLV_list_len = 0;
    OutTLVdata = NULL;
    OutTLV_data_len = 0;
    monitor_flags = 0;
    script71 = NULL;
    script71_len = 0;
    script72 = NULL;
    script72_len = 0;
    TagCache = 0;
    TagCacheSize = 0;
    cmd_status = RESP_CODE_INVAL;
    icc_card_status = 0;
}

//Needed for Online PIN entry, which could happen before performing transactions and requires the TLV Pool initialised
bool CEMVtrns::init_the_kernel()
{
    int res;
    res = Init_CardSlot( g_CEMV.ICC_reader );
    dlog_msg("Init CardSlot result %d", res);
    res = Set_Card_Type( g_CEMV.ICC_reader, ASYNC, STANDARD_EMV311 );
    dlog_msg("SetCardType result %d", res);
    res = Reset_CardSlot(g_CEMV.ICC_reader, RESET_COLD);
    dlog_msg("Reset CardSlot result %d", res);

    res = inVXEMVAPSCInit(); // To initialise Kernel TLV pool
    dlog_msg("inVXEMVAPSCInit result %d", res);

    res = inVXEMVAPInitCardslot();
    dlog_msg("inVXEMVAPInitCardslot result %d", res);
    res = inVXEMVAPCardInit();
    dlog_msg("inVXEMVAPCardInit result %d", res);

    res = inVXEMVAPTransInit(CARD_APP_MVT_DEFAULT, inGetPTermID);
    dlog_msg("inVXEMVAPTransInit result %d", res);
    // Always assume success, the initialisation is redone anyway during every transaction

    res = Init_CardSlot( g_CEMV.ICC_reader );
    dlog_msg("Card slot init result %d", res);
    usEMVSetBackSpaceKeyFnlty(EMV_FALSE); // Disable Backspace key functionality

    return true;
}

int CEMVtrns::send_notify(int notify_event)
{
    using namespace com_verifone_TLVLite;
    IPCPacket  response;

    if(g_CardAppConfig.GetNotifyStatus())
    {
        dlog_msg("sending notification message: event %d to %s", notify_event, g_CardAppConfig.cNotifyName);
        response.addTag(ConstData(arbyTagEvent, EVENT_TS), MakeBERValue(notify_event));
        response.setCmdCode(UNSOLICITED_EVENT_COM);
        if (notify_event == CARD_APP_EVT_PIN_ENTRY || notify_event == CARD_APP_EVT_PIN_LAST)
        {
            // Add PIN try counter
            unsigned char pinTryCtr;
            unsigned short pinTryCtrLen = 0;
            usEMVGetTLVFromColxn(TAG_9F17_PIN_TRY_COUNTER, &pinTryCtr, &pinTryCtrLen);
            if (pinTryCtrLen == 1)
            {
                const int PIN_TRY_CTR_TS = 2;
                const uint8_t arbyPinTryCtr[PIN_TRY_CTR_TS] = { 0x9F, 0x17 };
                response.addTag(ConstData(arbyPinTryCtr, PIN_TRY_CTR_TS), MakeBERValue(pinTryCtr));
            }
        }
        response.setAppName(g_CardAppConfig.cNotifyName);
        response.send();
        SVC_WAIT(0);
    }
    return(SUCCESS);
}

int CEMVtrns::update_config(int record)
{
    int iRet = 0;
    int numTLV = 0;
    
    iRet = inVXEMVAPGetCardConfig(record,record );
    dlog_msg("inVXEMVAPGetCardConfig RET: %X, record: %d", iRet, record);
    
    numTLV = parseInTLV(InTLVlist, InTLV_list_len, false);
    dlog_msg("Added/Updated %d TLV objects in collection", numTLV);

    TLV_data_len = parseOutTLV(OutTLVlist, OutTLV_list_len, OutTLVdata, OutTLV_data_len);
    dlog_msg("Sent %d bytes of requested data", TLV_data_len);
    return 0;
}

int CEMVtrns::processMagReader(int mag_handle)
{

    if(mag_handle < 0)
    {
        int task_id=0;
        mag_handle = get_owner(DEV_CARD, &task_id);
        dlog_msg("Crypto opened by %d, handle: %d", task_id, mag_handle);
        if (task_id != get_task_id() || mag_handle < 0)
        {
            return CARD_APP_RET_FAIL;
        }
    }

    char track_buffer[2+2+2+CARD_APP_DEF_TRACK1_LEN+CARD_APP_DEF_TRACK2_LEN+CARD_APP_DEF_TRACK3_LEN];
    int read_data = 0;
    int offset = 0;
    read_data=read(mag_handle, track_buffer, 2+2+2+CARD_APP_DEF_TRACK1_LEN+CARD_APP_DEF_TRACK2_LEN+CARD_APP_DEF_TRACK3_LEN);
    dlog_msg("Mag card: %d bytes read", read_data);
	memset(track_status, 0, sizeof(track_status));
	if(read_data)
	{
	    track_status[0] = track_buffer[1];
	    if(track_buffer[1] == 0)
	    {
	         memcpy(track1, track_buffer+2, track_buffer[0]-2);
	         dlog_msg("Track1 present:%s", track1);
	    }
	    offset+=track_buffer[0];
	    track_status[1] = track_buffer[offset+1];
	    if(track_buffer[offset+1] == 0)
	    {
	         memcpy(track2, track_buffer+offset+2, track_buffer[offset+0]-2);
	         dlog_msg("Track2 present:%s", track2);
	    }
	    offset+=track_buffer[offset+0];
	    track_status[2] = track_buffer[offset+1];
	    if(track_buffer[offset+1] == 0)
	    {
	         memcpy(track3, track_buffer+offset+2, track_buffer[offset+0]-2);
	         dlog_msg("Track3 present:%s", track3);
	    }
	}
    return CARD_APP_RET_MAG_OK;
}

int CEMVtrns::processICCReader(int flags /* = 0 */)
{
    const unsigned char GEM_CARD_ID = 0x53;
    int iRet = CARD_APP_RET_ICC_OK;
    short len = 0;
    short offset = 0;
    uint8_t ATR_buffer[CARD_APP_DEF_ATR_LEN];
    memset(ATR_buffer, 0, CARD_APP_DEF_ATR_LEN);
    memset(ATR, 0, CARD_APP_DEF_ATR_LEN);
    icc_card_type = CARD_APP_CARDTYPE_NOT_SET;

    while(true)
    {
        if((len = Set_Card_Type( g_CEMV.ICC_reader, ASYNC, STANDARD_EMV311 )) != CARDSLOT_SUCCESS)
        {
            dlog_msg("Set_Card_Type() has failed (error %d), trying to reinitialise CardSlot", len);
            len = Get_Card_State( g_CEMV.ICC_reader );
            dlog_msg("Get_Card_State() result %d", len);
            if (len == E_SLOT_INITIALIZE)
            {
                len = Init_CardSlot( g_CEMV.ICC_reader );
                dlog_msg("Card slot init result %d", len);
            }
            else if (len != CARD_PRESENT)
            {
                dlog_error("Invalid card state %d", len);
            }
            len = Terminate_CardSlot( g_CEMV.ICC_reader, SWITCH_OFF_CARD );
            dlog_msg("Terminate_CardSlot result %d", len);
            len = Set_Card_Type( g_CEMV.ICC_reader, ASYNC, STANDARD_EMV311 );
        }
        if(len == CARDSLOT_SUCCESS)
        {
            if (flags & WAIT_FOR_CARD_WARM_RESET)
            {
                len = Reset_CardSlot(g_CEMV.ICC_reader, RESET_WARM);
                dlog_msg("Warm reset result %d", len);
                if (len != CARDSLOT_SUCCESS)
                {
                    break;
                }
            }
            else
            {
                len = Reset_CardSlot(g_CEMV.ICC_reader, RESET_COLD);
                if (len != CARDSLOT_SUCCESS)
                {
                    dlog_alert("Reset_CardSlot failed with error %d", len);
                    len = Reset_CardSlot(g_CEMV.ICC_reader, RESET_WARM);
                    dlog_msg("Warm reset result %d", len);
                    if (len != CARDSLOT_SUCCESS)
                    {
                        break;
                    }
                }
            }
            len = Get_Interface_Bytes(g_CEMV.ICC_reader, ATR_buffer);
            if((len != E_INV_PARAM) && (len != E_SLOT_INITIALIZE) && (len != UNINITIALIZED))
            {
                dlog_msg("Interface Bytes: %d", len);
                SVC_HEX_2_DSP((const char*)ATR_buffer, reinterpret_cast<char *>(ATR), len);
                offset = len<<1;
                len = Get_Historical_Bytes(g_CEMV.ICC_reader, ATR_buffer);
                if((len != E_INV_PARAM) && (len != E_SLOT_INITIALIZE) && (len != UNINITIALIZED))
                {
                    dlog_msg("Historical Bytes: %d", len);
                    if(ATR_buffer[0] == GEM_CARD_ID)
                    {
                        icc_card_type = CARD_APP_CARDTYPE_TRADEPRO;
                        dlog_msg("TRADEPRO card detected!");
                    }
                    SVC_HEX_2_DSP((const char *)ATR_buffer, reinterpret_cast<char *>(ATR+offset), len);
                    offset += len<<1;
                }
                if(offset < CARD_APP_DEF_ATR_LEN)
                {
                    ATR[offset] = 0;
                    dlog_msg("ICC Card inserted - ATR len:%d, %s", offset, ATR);
                    icc_card_type = CARD_APP_CARDTYPE_EMV_ASYNC;
                }
                else
                {
                    dlog_error("INTERNAL ERROR!!!");
                }
                iRet = CARD_APP_RET_ICC_OK;
            }
            else
            {
                dlog_error("Get Interface Bytes has failed with code %d", len);
            }
        }
        else
        {
            dlog_alert("Set_Card_Type failed with error %d", len);
            break;
        }
        break;
    }
    if (icc_card_type == CARD_APP_CARDTYPE_NOT_SET)
    {
        while(true)
        {
            // Card still not detected! Try memory cards
            Terminate_CardSlot(g_CEMV.ICC_reader, SWITCH_OFF_CARD);
            if (isCardRemoved())
            {
                dlog_alert("Card inserted event, but no card!");
                iRet = CARD_APP_RET_REMOVED;
                break;
            }
            if (icc_card_type == CARD_APP_CARDTYPE_NOT_SET)
            {
                if (DetectCardType())
                {
                    iRet = CARD_APP_RET_ICC_OK;
                }
                else
                {
                    if (isCardRemoved())
                    {
                        dlog_alert("Card has been removed!");
                        iRet = CARD_APP_RET_REMOVED;
                    }
                    else
                    {
                        icc_card_type = CARD_APP_CARDTYPE_UNKNOWN;
                        iRet = CARD_APP_RET_ICC_FAIL;
                    }
                }
            }
            break;
        }
    }
    if (icc_card_type == CARD_APP_CARDTYPE_NOT_SET)
    {
        dlog_alert("Card removed or unknown chip type!");
        len = Terminate_CardSlot(g_CEMV.ICC_reader, SWITCH_OFF_CARD);
        dlog_msg("Terminate CardSlot returned %d", len);
        Close_CardSlot(g_CEMV.ICC_reader);
    }

    if (iRet == CARD_APP_RET_ICC_OK)
    {
        dlog_msg("Clearing statuses for new transaction");
        g_CEMV.ClearStatus();
    }
    return iRet;
}

int CEMVtrns::monitor_ICC(int flags)
{
    int iRet = CARD_APP_RET_OK;
    int handle_ICC = -1;
    int mag_handle = -1;
    int num_events = 0;
    event_item item_event;
    eventset_t pml_events;
    int events_mon = event_open();
    bool need_to_close_cardslot = false;
    static int card_state = -1;

    if (icc_card_status) return CARD_APP_RET_ICC_OK;

    event_item ev_icc, ev_ipc, ev_mag;
    ev_icc.event_id = events::icc;
    ev_ipc.event_id = events::ipc;
    ev_ipc.evt.com.fd = -1;
    ev_ipc.evt.com.flags = 0;
    ev_mag.event_id = events::mag;

    if(flags & CARD_APP_MON_NOBREAK)
    {
        dlog_msg("Waiting for card status change forever!");
    }
    else
    {
        iRet = event_ctl(events_mon, ctl::ADD, ev_ipc);
        dlog_msg("event_ctl(...ipc) ret: %d", iRet);
    }
    if(events_mon)
    {
        //cleaning data for next swipe/insertion
        memset(track1, 0, sizeof(track1));
        memset(track2, 0, sizeof(track1));
        memset(track3, 0, sizeof(track1));
        memset(track_status, 0, sizeof(track_status));
        memset(ATR, 0, sizeof(ATR));
        if(handle_ICC = open(DEV_ICC1, 0) >= 0)
        {
            iRet = event_ctl(events_mon, ctl::ADD, ev_icc);
            dlog_msg("event_ctl(...icc) ret: %d", iRet);
            dlog_msg("Start monitoring ICC reader...");
            if((mag_handle = open(DEV_CARD, 0)) >= 0)
            {
                iRet = event_ctl(events_mon, ctl::ADD, ev_mag);
                dlog_msg("event_ctl(...mag) ret: %d", iRet);
            }
            int event_count = event_read(events_mon, pml_events);
            if(event_count == 0)
            {
                int curr_state = Get_Card_State( g_CEMV.ICC_reader );
                if (curr_state == E_SLOT_INITIALIZE)
                {
                    dlog_msg("CardSlot not initialized");
                    need_to_close_cardslot = true;
                    curr_state = Init_CardSlot( g_CEMV.ICC_reader );
                    if (curr_state == CARDSLOT_SUCCESS)
                    {
                        curr_state = Get_Card_State( g_CEMV.ICC_reader );
                    }
                    else
                    {
                        dlog_error("Cardslot initialisation error %d", curr_state);
                    }
                }
                if (card_state == -1) card_state = curr_state;
                if (card_state != curr_state)
                {
                    dlog_alert("Card status inconsistency detected! Stored state %d, detected %d (inserted val %d, removed val %d)",
                      card_state, curr_state, CARD_PRESENT, CARD_ABSENT);
                    if (curr_state == CARD_PRESENT || curr_state == CARD_ABSENT)
                    {
                        // simulate insertion / removal event
                        item_event.event_id = events::icc;
                        pml_events.push_back(item_event);
                        event_count = pml_events.size();
                    }
                    else dlog_error("Invalid state %d", curr_state);
                    card_state = curr_state;
                }
                else
                {
                    event_count = event_wait(events_mon, pml_events);
                }
            }
            if(event_count > 0)
            {
                for (eventset_t::iterator it = pml_events.begin() ; it != pml_events.end(); ++it)
                {
                    if (it->event_id == events::ipc)
                    {
                        dlog_msg("EVT_PIPE received");
                        iRet = CARD_APP_RET_CANCELED;
                    }
                    if (it->event_id == events::icc)
                    {
                        if(isCardRemoved())
                        {
                            // Clear TLV pool
                            iRet = inVXEMVAPTransInit(CARD_APP_MVT_DEFAULT, inGetPTermID);
                            dlog_msg("inVXEMVAPTransInit result %d", iRet);
                            ClearTagsCache();
                            vdEMVClearSensitiveData();
                            g_CEMV.SetPINStatus(PIN_ENTRY_STATUS_NOT_SET);
                            iRet = CARD_APP_RET_REMOVED;
                            dlog_msg("EVT_ICC1_REM received");
                            card_state = CARD_ABSENT;
                            icc_card_type = CARD_APP_CARDTYPE_NOT_SET;
                        }
                        else
                        {
                            iRet = CARD_APP_RET_ICC_OK;
                            dlog_msg("EVT_ICC1_INS received");
                            card_state = CARD_PRESENT;
                        }
                    }
                    if (it->event_id == events::mag)
                    {
                        dlog_msg("Card swiped, reading data...");
                        iRet = processMagReader(mag_handle);
                    }
                }
            }
            close(handle_ICC);
            close(mag_handle);
        }
        else
        {
            dlog_msg("ICC reader already in use");
            iRet = CARD_APP_RET_FAIL;
        }
        /*
        iRet = event_ctl(events_mon, ctl::DEL, ev_icc);
        dlog_msg("event_ctl(...icc) ret: %d", iRet);
        iRet = event_ctl(events_mon, ctl::DEL, ev_ipc);
        dlog_msg("event_ctl(...ipc) ret: %d", iRet);
        iRet = event_ctl(events_mon, ctl::DEL, ev_mag);
        dlog_msg("event_ctl(...mag) ret: %d", iRet);
        */
        event_close(events_mon);
        if (need_to_close_cardslot) Close_CardSlot( g_CEMV.ICC_reader );
    }
    
    check_RAM();
    return iRet;
}


int CEMVtrns::wait_for_card(int card_type, unsigned int timeout, bool detect_card_type /* = false */, int icc_flags /* = 0 */)
{
    int iRet = CARD_APP_RET_OK;
    int mag_handle = -1;
    int icc_handle = -1;
    int m_PosTimeoutTimer = -1;
    const unsigned char GEM_CARD_ID = 0x53;
    event_item ev_icc, ev_ipc, ev_mag, ev_timer;    
    ev_icc.event_id = events::icc;
    ev_ipc.event_id = events::ipc;
    ev_ipc.evt.com.fd = -1;
    ev_ipc.evt.com.flags = 0;
    ev_mag.event_id = events::mag;
    int events_mon = event_open();
    eventset_t pml_events;

    iRet = event_ctl(events_mon, ctl::ADD, ev_ipc);
    dlog_msg("Adding events::ipc ret :%X", iRet);

    if(timeout > 0)
    {
        set_timer(ev_timer, timeout); 
        iRet = event_ctl(events_mon, ctl::ADD, ev_timer);
        dlog_msg("Waiting for %d ms, adding events::timer ret :%X", timeout, iRet);
    }
    if(card_type & CARD_APP_CARD_MAGNETIC)
    {
        mag_handle = open(DEV_CARD, 0);
        iRet = event_ctl(events_mon, ctl::ADD, ev_mag);
        dlog_msg("Adding events::mag ret :%X", iRet);
    }
    if(card_type & CARD_APP_CARD_ICC)
    {
        icc_handle = open(DEV_ICC1, 0);
        iRet = event_ctl(events_mon, ctl::ADD, ev_icc);
        dlog_msg("Adding events::icc ret :%X", iRet);
        SetDefaultFunctionPointers();
        GetAIDPair();
        g_CardAppConfig.RestoreDefaults();
       
        if ((iRet = inVXEMVAPSCInit()) != SUCCESS)
        {
            if (iRet == CARDSLOT_FAILURE)
            {
                dlog_error("ICC reader not intialised RET: %X", iRet);
                iRet = CARD_APP_RET_ICC_FAIL;
                dlog_msg("Closing CardSlot");
                // Kamil_P1: WARNING: The following call CRASHES, for example when there is no MVT/EST file on the terminal!!!
                Terminate_CardSlot(g_CEMV.ICC_reader, SWITCH_OFF_CARD);
                Close_CardSlot(g_CEMV.ICC_reader);
            }
            else
            {
                dlog_alert("EMV configuration error!");
                iRet = SUCCESS; // Kamil_P1: Overriding to success to allow improper EMV configurations
            }
        }
        if (iRet == SUCCESS)
        {
            if((iRet = inVXEMVAPTransInit(CARD_APP_MVT_DEFAULT, inGetPTermID)) != SUCCESS)
            {
                InitMinimumTLVset();
                ClearTagsCache();
                vdEMVClearSensitiveData();
                dlog_error("EMV transaction can not be initialised RET: %X", iRet);
                iRet = CARD_APP_RET_ICC_FAIL;
            }
            else
            {
                iRet = inVXEMVAPInitCardslot();
                dlog_msg("inVXEMVAPInitCardslot result %d", iRet);
                dlog_msg("adding ICC event");
                iRet = SUCCESS;
            }
        }
    }

    if(iRet == SUCCESS)
    {
        //cleaning data for next swipe/insertion
        memset(track1, 0, sizeof(track1));
        memset(track2, 0, sizeof(track1));
        memset(track3, 0, sizeof(track1));
        memset(track_status, 0, sizeof(track_status));
        memset(ATR, 0, sizeof(ATR));
        
        dlog_msg("Start waiting for requested devices...");
#ifdef VFI_GUI_GUIAPP
        if (g_CardAppConfig.GetUseDisplay())
        {
            if(g_CardAppConfig.GuiInterface() == CCardAppConfig::GUI_APP)
            {
                //here is the place when new GUI should bew implemented
            }
            else
            {
                DisplayPromptSelectSection(CARD_APP_GUI_INSERT, CARD_APP_GUI_S_PROMPTS);
            }
        }
#endif
        if(isCardInserted())
        {
            dlog_msg("card in reader...detecting...");
            iRet = processICCReader(icc_flags);
        }
        else if(event_wait(events_mon, pml_events))
        {
            for (eventset_t::iterator it = pml_events.begin() ; it != pml_events.end(); ++it)
            {
                dlog_msg("Event id %d", it->event_id);
                if (it->event_id == events::icc)
                {
                    if(isCardRemoved())
                    {
                        // Clear TLV pool
                        iRet = inVXEMVAPTransInit(CARD_APP_MVT_DEFAULT, inGetPTermID);
                        ClearTagsCache();
                        vdEMVClearSensitiveData();
                        dlog_msg("inVXEMVAPTransInit result %d", iRet);
                        icc_card_type = CARD_APP_CARDTYPE_NOT_SET;
                        iRet = CARD_APP_RET_REMOVED;
                    }
                    else
                    {
                        dlog_msg("Card inserted, detecting type...");
                        iRet = processICCReader(icc_flags);
                    }
                }
                if (it->event_id == events::mag)
                {
                    dlog_msg("Closing smart card reader...");
                    Terminate_CardSlot(g_CEMV.ICC_reader, SWITCH_OFF_CARD);
                    Close_CardSlot(g_CEMV.ICC_reader);
                    dlog_msg("Card swiped, reading data...");
                    iRet = processMagReader(mag_handle);
                }
                if (it->event_id == events::ipc)
                {
                    dlog_msg("events::ipc received");
                    iRet = CARD_APP_RET_CANCELED;
                }
                if (it->event_id == events::timer)
                {
                    dlog_msg("events::timer received");
                    dlog_msg("Closing CardSlot");
                    Terminate_CardSlot(g_CEMV.ICC_reader, SWITCH_OFF_CARD);
                    Close_CardSlot(g_CEMV.ICC_reader);
                    iRet = CARD_APP_RET_TIMEOUT;
                }
            }
        }
    }
    else
    {
        dlog_msg("Finishing... nothing requested");
        iRet = CARD_APP_RET_OK;
    }

    if(timeout > 0)
    {
        event_ctl(events_mon, ctl::DEL, ev_timer);
        clr_timer(ev_timer);
    }
    event_close(events_mon);
    check_RAM();
    if(mag_handle >= 0) close(mag_handle);
    if(icc_handle >= 0) close(icc_handle);
    return iRet;
}


int CEMVtrns::EMVtransaction(int opers, CEMV *cEMV)
{
    int iRet = CARD_APP_RET_OK;
    int numTLV = 0;
    int post_mask;
    bool permanent = false;


    //printConfig(EMV_CONFIG_FILE);

    //first we need to get GUI ready
#ifdef VFI_GUI_GUIAPP
    if (g_CardAppConfig.GetUseDisplay())
    {
        if(g_CardAppConfig.GuiInterface() == CCardAppConfig::GUI_APP)
        {
            dlog_msg("SUCCESS: %s present", GUI_APP_NAME_);
            if( GuiApp_Connect( (char *)CARD_APP_NAME, 10000 ) == GUI_OKAY )
            {
                dlog_msg( "Connected from %s to %s", CARD_APP_NAME, GUI_APP_NAME_ );
            }
            GuiApp_DisplayPromptSelectSection(CARD_APP_GUI_PLEASE_WAIT, CARD_APP_GUI_S_PROMPTS, 0, 0);
        }
        else
        {
            DisplayPromptSelectSection(CARD_APP_GUI_PLEASE_WAIT, CARD_APP_GUI_S_PROMPTS);
        }
    }
#endif


    if(opers & CARD_APP_OPER_CONFIG)
    {
        dlog_msg("Adding/reading data to/from config file");
        permanent = true;
    }

    if(!(opers & CARD_APP_OPER_SELECT))
    {
        dlog_msg("EMVtransaction:Operations reg: %X", opers);
        numTLV = parseInTLV(InTLVlist, InTLV_list_len, permanent);
        dlog_msg("Added/Updated %d TLV objects in collection", numTLV);
    }
    else
    {
        dlog_msg("icc card type '%d'", icc_card_type);
        if (icc_card_type == CARD_APP_CARDTYPE_NOT_SET)
        {
            if(g_CardAppConfig.GetSuperEMVWaitForCard())
            {
                // we have to perform initialisation
                wait_for_card(CARD_APP_CARD_ICC, 0, true);
            }
            else
            {
                dlog_alert("Card is NOT inserted, terminating the transaction!");
                return CARD_APP_RET_REMOVED;
            }
        }
        else if (icc_card_type == CARD_APP_CARDTYPE_REQUIRES_POWERUP)
        {
            wait_for_card(CARD_APP_CARD_ICC, 100, g_CardAppConfig.GetWarmResetAfterICCOff());
            icc_card_status = CRD_CODE_CARD_ICC_ENABLED;
        }
    }

    if(opers & (~CARD_APP_OPER_CONFIG))
    {
#ifdef VFI_GUI_GUIAPP
        //first we need to get GUI ready
        if (g_CardAppConfig.GetUseDisplay())
        {
            if(g_CardAppConfig.GuiInterface() == CCardAppConfig::GUI_APP)
            {
                //here we should implementation of new GUI support
            }
            else
            {
                DisplayPromptSelectSection(CARD_APP_GUI_PLEASE_WAIT, CARD_APP_GUI_S_PROMPTS);
            }
        }
#endif

        //now we process all requested operations
        if(opers & CARD_APP_OPER_SELECT)
        {
            dlog_msg("EMV Selection");
            iRet = cEMV->SelectApplication();
            dlog_msg("EMVtransaction:Operations reg: %X", opers);
            if (iRet == CARD_APP_RET_OK)
            {
                numTLV = parseInTLV(InTLVlist, InTLV_list_len, false);
                dlog_msg("Added/Updated %d TLV objects in collection", numTLV);
            }
        }
        if((opers & CARD_APP_OPER_GPO) && (iRet == CARD_APP_RET_OK))
        {
            dlog_msg("EMV GPO");
            iRet = cEMV->GetProcessingOptions();
        }
        if((opers & CARD_APP_OPER_READ) && (iRet == CARD_APP_RET_OK))
        {
            dlog_msg("EMV Read");
            iRet = cEMV->ReadCardData();
        }
        if((opers & CARD_APP_OPER_DATA_AUTH) && (iRet == CARD_APP_RET_OK))
        {
            dlog_msg("EMV Data Auth");
            iRet = cEMV->DataAuthentication();
        }
        if((opers & CARD_APP_OPER_RESTRICT) && (iRet == CARD_APP_RET_OK))
        {
            dlog_msg("EMV Restrictions");
            iRet = cEMV->ProcessRestrictions();
        }
        if((opers & CARD_APP_OPER_TRM) && (iRet == CARD_APP_RET_OK))
        {
            dlog_msg("EMV TRM");
            iRet = cEMV->TerminalRiskManagement();
        }
        if((opers & CARD_APP_OPER_VERIFY) && (iRet == CARD_APP_RET_OK))
        {
            dlog_msg("EMV Cardholder Verification");
            iRet = cEMV->CardholderVerification();
        }

        if((opers & CARD_APP_OPER_FIRST_AC) && (iRet == CARD_APP_RET_OK))
        {
            dlog_msg("EMV First AC");
            iRet = cEMV->FirstAC();
        }

        post_mask = CARD_APP_OPER_EXT_AUTH | CARD_APP_OPER_SECOND_AC | CARD_APP_OPER_SCRIPT;

        if(((opers & post_mask) == post_mask) && (iRet == CARD_APP_RET_OK))
        {
            dlog_msg("EMV UseHostData - opers:%X params:%X", opers, post_mask);
            iRet = cEMV->PostOnline();
        }
        else
        {
            if((opers & CARD_APP_OPER_EXT_AUTH) && (iRet == CARD_APP_RET_OK))
            {
                dlog_msg("EMV External Auth");
                iRet = cEMV->ExternalAuthenticate();
            }
            if((opers & CARD_APP_OPER_SCRIPT) && (iRet == CARD_APP_RET_OK))
            {
                dlog_msg("EMV Script 71");
                iRet = cEMV->ProcessScript();
            }
            if((opers & CARD_APP_OPER_SECOND_AC) && (iRet == CARD_APP_RET_OK))
            {
                dlog_msg("EMV Second AC");
                iRet = cEMV->SecondAC();
            }
            if((opers & CARD_APP_OPER_SCRIPT) && (iRet == CARD_APP_RET_OK))
            {
                dlog_msg("EMV Script 72");
                iRet = cEMV->ProcessScript();
            }

        }
        if(opers & CARD_APP_OPER_PIN_VERIFY)
        {
            if (g_CEMV.isExternalPIN()) g_CardAppConfig.SetPINtype(CARD_APP_EXTERNAL_PIN_OFFLINE);
            else g_CardAppConfig.SetPINtype(CARD_APP_PIN_OFFLINE_EXT);
            dlog_msg("Offline PIN verification for external kernels");
            g_CEMV.ClearStatus(); // Make sure no old statuses interfere
            int crypto_task = 0;
            //int crypto_handle = open("/dev/crypto", 0);
            //crypto_handle = get_owner ("/dev/crypto", &crypto_task);
            unsigned char pinTryCtr = 3;
            unsigned short pinTryCtrLen = 0;
            usEMVGetTLVFromColxn(TAG_9F17_PIN_TRY_COUNTER, &pinTryCtr, &pinTryCtrLen);
            switch (pinTryCtr)
            {
              default:
                if (g_CEMV.GetPINStatus() == PIN_ENTRY_STATUS_NOT_SET || !g_CardAppConfig.GetShowIncorrectPIN())
                {
                    g_CEMV.SetPINStatus(PIN_ENTRY_STATUS_OK);
                }
                else
                {
                    g_CEMV.SetPINStatus(PIN_ENTRY_STATUS_BAD_PIN);
                }
                break;
              case 1:
                g_CEMV.SetPINStatus(PIN_ENTRY_STATUS_LAST_TRY);
                break;
              case 0:
                g_CEMV.SetPINStatus(PIN_ENTRY_STATUS_BLOCKED);
                break;
            }
            iRet = cEMV->OfflinePINVerification();
            int crypto_handle = get_owner("/dev/crypto", &crypto_task);
            if (crypto_task == get_task_id()) close(crypto_handle);
            //close(crypto_handle);
        }
        check_RAM();
        //additional status checking - required for cardslot closure
        cEMV->checkStatus(CARD_APP_OPER_TLV);

#ifdef VFI_GUI_GUIAPP
        if(g_CardAppConfig.GuiInterface() == CCardAppConfig::GUI_APP && g_CardAppConfig.GetUseDisplay())
        {
            dlog_msg("Disconnecting GUIAPP...");
            GuiApp_Disconnect();
        }
#endif
        
        // Finally, perform card presence check
        if (cEMV->GetStatusDetail() & CARD_APP_STATUS_D_REMOVED)
        {
            if(Get_Card_State(g_CEMV.ICC_reader) != E_SLOT_INITIALIZE)
            {
                dlog_alert("Card removal detected, clearing the collxn!");
                inVXEMVAPTransInit(CARD_APP_MVT_DEFAULT, inGetPTermID);
                ClearTagsCache();
                vdEMVClearSensitiveData();

                //Terminate_CardSlot(g_CEMV.ICC_reader, SWITCH_OFF_CARD);
                Close_CardSlot(g_CEMV.ICC_reader);
                TLV_data_len = 0;
            }
        }
        else
        {
            TLV_data_len = parseOutTLV(OutTLVlist, OutTLV_list_len, OutTLVdata, OutTLV_data_len);
        }
    }
    else
    {
        dlog_msg("Adding/Reading TLV objects only");
        TLV_data_len = parseOutTLV(OutTLVlist, OutTLV_list_len, OutTLVdata, OutTLV_data_len);
    }

    if(permanent && numTLV == 0)
    {
        cUpdateCfg UpdateCfg;
        if (g_CardAppConfig.GetAIDOper() == 0)
        {
            dlog_msg("Preparing configuration data for AID: %s", g_CardAppConfig.GetCurrentAID().c_str());
            std::string cfg_data = UpdateCfg.getConfig(g_CardAppConfig.GetCurrentAID().c_str(), g_CardAppConfig.GetCAPKndx());
            //cfg_data+=UpdateCfg.getCAPK(g_CardAppConfig.GetCAPKndx());
            TLV_data_len = cfg_data.size()/2;
            SVC_DSP_2_HEX(cfg_data.c_str(), (char *)OutTLVdata, TLV_data_len);
        }
        else
        {
            if (g_CardAppConfig.GetCAPKndx() != 0) UpdateCfg.deleteCAPK(g_CardAppConfig.GetCurrentAID().c_str(), g_CardAppConfig.GetCAPKndx());
            else UpdateCfg.deleteConfig(g_CardAppConfig.GetCurrentAID().c_str());
        }
    }
    dlog_msg("Sent %d bytes of requested data", TLV_data_len);
    return iRet;
}


/*
 * DetectCardType() only checks for Memory / I2C / TradePro cards!
 * EMV detection is performed in main thread!
 */
bool CEMVtrns::DetectCardType()
{
    // Check SLE442 card type
    do
    {
        if (isCardRemoved()) break;
        // It is assumed here that each method, which attempts to detect a card, cleans afterwards
        // which means, calls Terminate_CardSlot() in case detection fails
        dlog_msg("Trying with SLE44x2...");
        if (sle442.is_card_memory_card(ATR))
        {
            icc_card_type = CARD_APP_CARDTYPE_EMV_SYNC;
            break;
        }
        if (isCardRemoved()) break;
        // Try I2C
        dlog_msg("Trying with I2C...");
        // Try reading it to confirm???
        if (!IsI2CCard()) // LOCAL function!
        {
            dlog_error("No I2C card!");
        }
        else
        {
            // That's it - we have I2C card!
            icc_card_type = CARD_APP_CARDTYPE_I2C;
            ATR[0] = 0;
            break;
        }
        // Further cards if necessary
    } while (false);

    if (icc_card_type != CARD_APP_CARDTYPE_NOT_SET) return true;

    dlog_msg("unknown card type in card slot");
    return false;
}


/*
 * IsI2CCard() tries to detect I2C card
 * As there seem to be no direct way to detect such card, we're trying to read some bytes...
 */
bool CEMVtrns::IsI2CCard()
{
    // As there is no way to get ATR or detect I2C cards in any way, we have to simply try reading that card and hope that's enough...
    short shBytesToRead = 4;
    short shAddressMSB = 0x00;
    short shAddressLSB = 0x00;
    unsigned char ucCmdData[100] = {0};
    unsigned char ucRespData[100] = {0};
    unsigned long ulCmdLen = 0;
    unsigned char ucRetVal = 0;
    unsigned short usRespLen = 0;
    bool result = false;

    // set card type to iso 7816 sync card of type SLE44x2
    ucRetVal = Set_Card_Type( g_CEMV.ICC_reader, EE2K4K, ISO_7816 );
    dlog_msg("Set Card Type returned %d", ucRetVal);
    if (ucRetVal != CARDSLOT_SUCCESS)
    {
        dlog_alert("Cannot initialise I2C!");
    }
    else
    {
        ucRetVal = Reset_CardSlot( g_CEMV.ICC_reader, RESET_COLD );
        dlog_msg("Sync reset returned %d", ucRetVal );
        if (ucRetVal != CARDSLOT_SUCCESS)
        {
            dlog_error("Reset_CardSlot failed!");
        }
        else
        {
            EE2K4K_READ_BYTES(ucCmdData, shAddressMSB, shAddressLSB, shBytesToRead, ulCmdLen);
            ucRetVal = Transmit_APDU(g_CEMV.ICC_reader, ucCmdData, ulCmdLen, ucRespData, &usRespLen);
            if(ucRetVal != CARDSLOT_SUCCESS || usRespLen == 0)
            {
                dlog_alert("Cannot read I2C card, error %d, read count %d!", ucRetVal, usRespLen);
            }
            else
            {
                dlog_hex(ucRespData, usRespLen, "READ FROM CARD");
                dlog_msg("I2C read success, assuming I2C card inserted");
                result = true;
            }
        }
    }
    if (result == false)
    {
        // Terminate cardslot - may be in bad state
        ucRetVal = Terminate_CardSlot(g_CEMV.ICC_reader, SWITCH_OFF_CARD);
        dlog_msg("Terminate CardSlot returned %d", ucRetVal);
        Close_CardSlot(g_CEMV.ICC_reader);
    }
    return result;
}

/*
 * CacheTags() caches sensitive EMV tags as they are removed by stupid EMV Kernel
 */
int CEMVtrns::CacheTags()
{
    static const unsigned short TagsTable[] = 
        {
            TAG_5A_APPL_PAN, TAG_5F24_EXPIRY_DATE, TAG_5F20_CARDHOLDER_NAME, TAG_5F30_SERVICE_CODE,
            TAG_57_TRACK2_EQ_DATA, TAG_90_ISS_PUBKEY_CERT, TAG_9F46_ICC_PUBKEY_CERT, 
            TAG_9F2D_ICC_PIN_CERT, TAG_9F37_UNPRED_NUM,
            TAG_81_AMOUNT_AUTH, TAG_9F02_AMT_AUTH_NUM, TAG_9F03_AMT_OTHER_NUM, TAG_9F04_AMT_OTHER_BIN, 
            TAG_9F3A_AMT_REF_CURR
        };
    static const int TagsTableSize = sizeof(TagsTable) / sizeof(TagsTable[0]);
    int neededSize = 0;
    unsigned short tagSize = 0;
    char buffer[255]; // arbitrary size for temporary buffer
    // First, determine size of all the tags, so that we can encrypt
    for (int i = 0; i < TagsTableSize; i++)
    {
        if (EMV_SUCCESS == usEMVGetTLVFromColxn(TagsTable[i], buffer, &tagSize))
        {
            dlog_msg("Found tag %04X, size %d", TagsTable[i], tagSize);
            neededSize += tagSize + 3; // + 3 to make room for tag and length
        }
    }
    dlog_msg("We need %d bytes", neededSize);
    TagCache = new unsigned char[neededSize];
    if (!TagCache)
    {
        dlog_error("Cannot cache tags, out of memory!");
        return -1;
    }
    unsigned char * pCache = TagCache;
    memset(TagCache, 0, neededSize);
    TagCacheSize = neededSize;
    for (int i = 0; i < TagsTableSize; i++)
    {
        if (EMV_SUCCESS == usEMVGetTLVFromColxn(TagsTable[i], buffer, &tagSize))
        {
            // dlog_msg("Adding tag %04X: %02X %02X", TagsTable[i], static_cast<unsigned char>((TagsTable[i] & 0xFF00) >> 8), static_cast<unsigned char>(TagsTable[i] & 0xFF));
            *pCache++ = static_cast<unsigned char>((TagsTable[i] & 0xFF00) >> 8);
            *pCache++ = static_cast<unsigned char>(TagsTable[i] & 0xFF);
            // dlog_msg("Adding len %d (%d)", tagSize, static_cast<unsigned char>(tagSize));
            *pCache++ = static_cast<unsigned char>(tagSize);
            memcpy(pCache, buffer, tagSize);
            pCache += tagSize;
        }
    }
    dlog_hex(TagCache, neededSize, "TAGS CACHE");
    return 0;
}

/*
 * Gets tag, cached by CacheTags()
 */
int CEMVtrns::GetTag(unsigned short tag, unsigned char * buffer, unsigned short * size)
{
    unsigned short cachedTag;
    unsigned char cachedSize;
    unsigned char * pCache = TagCache;
    //int index = 0;
    int result = E_TAG_NOTFOUND;
    dlog_msg("Looking for cached tag %04X", tag);
    while ((pCache - TagCache) < TagCacheSize)
    {
        if (TagCacheSize - (pCache - TagCache) < 3)
        {
            dlog_error("Malformatted cache!");
            break;
        }
        cachedTag = (*pCache++) << 8;
        cachedTag |= *pCache++;
        cachedSize = *pCache++;
        //index += 3;
        // dlog_msg("Cached tag %04X", cachedTag);
        if (cachedTag == tag)
        {
            dlog_msg("Found!");
            if (TagCacheSize - (pCache - TagCache) < cachedSize)
            {
                dlog_error("Malformatted cache!");
                break;
            }
            *size = cachedSize;
            memcpy(buffer, pCache, *size);
            result = EMV_SUCCESS;
            break;
        }
        pCache += cachedSize;
        //index += cachedSize;
    }
    return result;
}


void CEMVtrns::ClearEMVCollxn()
{
    dlog_msg("Clearing EMV collection");
    ClearTagsCache();
    vdEMVClearSensitiveData();
    inVXEMVAPTransInit(CARD_APP_MVT_DEFAULT, inGetPTermID);
}

void CEMVtrns::getMagData(IPCPacket & response)
{
    using namespace com_verifone_TLVLite;
    if(strlen((const char *)track1))
    {
        dlog_msg("Adding track1...%d bytes", strlen((const char *)track1));
        response.addTag(ConstData(arbyTagTrack1, MAG_TRACK_TS), ConstData(track1, strlen((const char *)track1)));
    }
    if(strlen((const char *)track2))
    {
        dlog_msg("Adding track2...%d bytes", strlen((const char *)track2));
        response.addTag(ConstData(arbyTagTrack2, MAG_TRACK_TS), ConstData(track2, strlen((const char *)track2)));
    }
    if(strlen((const char *)track3))
    {
        dlog_msg("Adding track3...%d bytes", strlen((const char *)track3));
        response.addTag(ConstData(arbyTagTrack3, MAG_TRACK_TS), ConstData(track3, strlen((const char *)track3)));
    }

    if(strlen((const char *)track_status))
    {
        response.addTag(ConstData(arbyTagSwipeStatus, SWIPE_STATUS_TS), ConstData(track_status, sizeof(track_status)));
    }
}
