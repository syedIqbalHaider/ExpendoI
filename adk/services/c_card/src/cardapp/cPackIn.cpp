#ifndef __cplusplus
#error "This file is for C++ only!"
#endif

/*****************************************************************************
 * 
 * Copyright (C) 2007 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/
/**
 * @file        PackIn.cpp
 *
 * @author      Artur Fraczak
 * 
 * @brief       Implementation of Input 7816 Packet and corresponding functions
 *
 *
 * @remarks     This file should be compliant with Verifone EMEA R&D C Coding  
 *              Standard 1.0.0 
 */

/***************************************************************************
 * Includes
 **************************************************************************/
#include <string.h>
#include <svc.h>
#include <liblog/logsys.h>

#include "Packet.h"
#include "cPackIn.h"

/***************************************************************************
 * Using
 **************************************************************************/

/***************************************************************************
 * Module namspace: begin
 **************************************************************************/
namespace com_verifone_pack
{

/**
 * @addtogroup Pack
 * @{ 
 */

/***************************************************************************
 * Exportable function definitions
 **************************************************************************/

/***************************************************************************
 * Exportable class members' definitions
 **************************************************************************/

CPacket::c_card_error_t CPackIn::SetData(char *szSender, uint8_t *pbyData, uint32_t byLen) {

    if( byLen < 2 )
        return EERROR;

    return SetDataPacket(szSender, pbyData[0], pbyData+1, byLen-1 );
}

CPacket::c_card_error_t CPackIn::SetData(uint8_t byCmdCode, char *szSender, uint8_t *pbyData, uint32_t byLen)
{
    return SetDataPacket( szSender, byCmdCode, pbyData, byLen );  
}

CPacket::c_card_error_t CPackIn::getTag(const uint8_t *pbyTag, uint8_t byTagLen, uint8_t **pstValue, uint16_t &len )
{
    BER_TLV *pstBT = pstTLV_find_element( pstHeadListTags, pbyTag, byTagLen );

    if ( NULL == pstBT )
    {
        return ENOTFOUND;
    }

    *pstValue =  pstBT->pbyValue;
    len = pstBT->ulnLen;

    return ESUCCESS;
}

/**
 * @}
 */

/***************************************************************************
 * Module namspace: end
 **************************************************************************/
}

