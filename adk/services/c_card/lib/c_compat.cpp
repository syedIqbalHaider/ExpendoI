/*
 * c_compat.cpp
 *
 *  Created on: 2010-02-02
 *      Author: Lucjan_B1
 */


#include "CARDApp.h"
#include <string.h>

namespace com_verifone_crdapi
{

namespace
{
	cardapp_client *cli = 0;
}

extern "C"
{
	int CardApp_Connect( const char *app_name, unsigned long timeout )
	{
		if(cli)
			return CRD_OKAY;
		cli = new cardapp_client(app_name,timeout);
		if(cli && cli->IsConnected())
		{
			return CRD_OKAY;
		}
		else
			return CRD_INIT_ERROR;
	}

	int iCardAppWaitForCard(int flags, int timeout)
	{
		if(cli)
			return cli->WaitForCard(flags, timeout);
		else
			return CRD_INIT_ERROR;
	}

	int iCardAppAPDU(char * out, char * in)
	{
		if(cli)
			return CRD_OKAY;
		else
			return CRD_INIT_ERROR;
	}

	int iCardAppTLV(int operation, char *TLV, int *TLVlen)
	{
		if(cli)
			return CRD_OKAY;
		else
			return CRD_INIT_ERROR;
	}

	int iCardAppVerify(char *PINkey, int keylen)
	{
		if(cli)
			return CRD_OKAY;
		else
			return CRD_INIT_ERROR;
	}

	/*int iCardAppSetUI(LANGUAGE_SET *language)
	{
		if(cli)
			return CRD_OKAY;
		else
			return CRD_INIT_ERROR;
	}*/

	int iCardApEMV(int operation, const char *to_add, const char *to_get, char *get_here, int buf_len)
	{
		if(cli)
			return cli->EMV(operation, to_add, to_get, get_here, buf_len);
		else
			return CRD_INIT_ERROR;
	}





	void CardApp_Disconnect(void)
	{
		delete cli;
		cli = 0;
	}

} // extern "C"

} // namespace com_verifone_crdapi

