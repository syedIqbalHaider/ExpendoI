/*****************************************************************************
 * 
 * Copyright (C) 2013 by VeriFone, Inc.
 *
 * All rights reserved.  No part of this software may be reproduced,
 * transmitted, transcribed, stored in a retrieval system, or translated
 * into any language or computer language, in any form or by any means,
 * electronic, mechanical, magnetic, optical, chemical, manual or otherwise,
 * without the prior written permission of VeriFone, Inc.
 ***************************************************************************/

/**
 * @                     cardclient.cpp
 *
 * @author            Jacek Olbrys
 * 
 * @brief               Card Service Client API
 *
 * @remarks         This file should be compliant with Verifone EMEA R&D C Coding
 *                         Standard 1.0.0 
 */

/***************************************************************************
 * Includes
 **************************************************************************/

// Verix eVo OS Libraries
#include <svc.h>

// Standard C headers
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <cstring>
#include <string.h>

// ADK Headers
#include <libber-tlv/ber-tlv.h>
#include <liblog/logsys.h>
#include <libipc/ipc.h>
#include <libtags/tags.h>

// ADK Service Headers
#include "CARDApp.h"

// Local headers
#include "cEMV.h"

#include "tlv_parser.h"
#include "cism/cstring.hpp"
#include <lib/tlv_libhelper.hpp>

/*************************************************************************** 
 * Namespace
 **************************************************************************/
using namespace com_verifone_crdapi_hlp;
using namespace com_verifone_crdapi;

namespace com_verifone_crdapi
{

    /* ---------------------------------------------------------------------- */
    //namespace utl = com_verifone_utils;

    const char *cardapp_client::app_name = "CARDAPP";
    
    std::size_t cardapp_client::AsciiToBin(const char * asciiBuf, unsigned char * const binBuf, std::size_t binSize)
    {
        char tmp[3];
        std::size_t iRetVal, i;
        
        if(!(strlen(asciiBuf) % 2))
        {
            for(i = 0; ; ++i, asciiBuf += 2)
            {
                if(!*asciiBuf || i >= binSize)
                    break;

                strncpy(tmp, asciiBuf, 2);
                tmp[2] = 0;
                sscanf(tmp, "%x", &iRetVal);
                binBuf[i] = (unsigned char)iRetVal;
            }
            
            iRetVal = i;
        }
        else
            iRetVal = 0;
        
        return iRetVal;
    }
    
    std::size_t cardapp_client::BinToAscii(const unsigned char * binBuf, std::size_t binLen, char * const asciiBuf, std::size_t asciiSize)
    {
        std::size_t iRetVal, i;
        
        for(i = 0; i < binLen; ++i)
        {
            if(i >= binLen || (2*i) >= asciiSize)
                break;

            sprintf(&asciiBuf[2*i], "%02X", binBuf[i]);
        }
        
        iRetVal = 2*i;
        
        return iRetVal;
    }

    /* ---------------------------------------------------------------------- */
    int cardapp_client::Connect( const char *eapp_name, unsigned long timeout )
    {
        std::strcpy( app_name_crd, eapp_name );

        // TODO: CARDAPP doesn't support Register cmd, thus this must be disabled. This has to be implemented in CARDAPP!!!
        //tlv_libhelper tlvh(CMD_REGISTER, app_name, app_name_crd);
        tlv_libhelper tlvh(CMD_START_WAITING, app_name, app_name_crd);

        // test connection to cardapp
        if( tlvh.check_allive() != 0 )
        {
            dlog_error( "CardApp_Connect error, app '%s' not alive", app_name );
            return CRD_INIT_ERROR;
        }
    #if 0
        return tlvh.send_rcv_simple();
    #else
        return 0;
    #endif
    }

    int cardapp_client::WaitForCard(int flags, int timeout)
    {
        tlv_libhelper tlvh(START_WAITING_COM, app_name, app_name_crd);
    
        com_verifone_TLVLite::ConstData_t ctag = com_verifone_TLVLite::ConstData_t(arbyTagPosTimeout, sizeof(arbyTagPosTimeout));
        tlvh.add_tag(ctag, static_cast<unsigned char>(timeout & 0xFF));

        ctag = com_verifone_TLVLite::ConstData_t(arbyTagProcFlag, sizeof(arbyTagProcFlag));
        tlvh.add_tag(ctag, flags);
        
        return tlvh.send_rcv_simple();
    }
    
    /*
        operation   (INP) : operation code
        to_add      (INP) : list of tags to add (null terminated ASCII string, two hex bytes per binary byte)
        to_get      (INP) : list of tags to get (null terminated ASCII string, two hex bytes per binary byte)
        get_here    (OUT) : buffer to store tags that have been taken from EMV
        buf_len     (INP) : size of 'get_here' buffer
    */
    int cardapp_client::EMV(int operation, const char *to_add, const char *to_get, char *get_here, int buf_len)
    {
        //ctmp
        /*int len;
        unsigned char buf[10];
        char ascii[100];
        
        len = AsciiToBin("1ADFFF0062D3", buf, sizeof(buf));
        dlog_hex(buf, len, "[ascii->bin]");
        
        BinToAscii(buf, len, ascii, sizeof(ascii));
        dlog_msg("ASCII:%s", ascii);
        
        return 0;*/
        //
        
        int iRet;
        std::size_t len;
        uint8_t tags_buf[2 * CARD_APP_MAX_TLV + 2];
        
        tlv_libhelper tlvh(SUPER_EMV_COM, app_name, app_name_crd);
        //tlv_libhelper tlvh(L3_SECOND_AC_COM, app_name, app_name_crd);
        
        len = AsciiToBin(to_add, tags_buf, sizeof(tags_buf));
        com_verifone_TLVLite::ConstData_t ctag = com_verifone_TLVLite::ConstData_t(arbyTagInTLV, sizeof(arbyTagInTLV));
        tlvh.add_tag(ctag, tags_buf, len);

        len = AsciiToBin(to_get, tags_buf, sizeof(tags_buf));
        ctag = com_verifone_TLVLite::ConstData_t(arbyTagOutTLVlist, sizeof(arbyTagOutTLVlist));
        tlvh.add_tag(ctag, tags_buf, len);

        ctag = com_verifone_TLVLite::ConstData_t(arbyTagOperations, sizeof(arbyTagOperations));
        tlvh.add_tag(ctag, operation);
        
        ctag = com_verifone_TLVLite::ConstData_t(arbyTagOutTLV, sizeof(arbyTagOutTLV));
        //iRet = tlvh.send_rcv_string(ctag, reinterpret_cast<char *>(tags_buf), &len, sizeof(tags_buf));
        iRet = tlvh.send_rcv_data(ctag, tags_buf, &len, sizeof(tags_buf));
        dlog_hex(tags_buf, len, "[from send_rcv_string]");
        BinToAscii(tags_buf, len, get_here, buf_len);
        
        return iRet;

        //return tlvh.send_rcv_simple();
    }




    /*int prnapp_client::PrintMessage( const char *text, size_t textLength, unsigned int pos_proto, bool waitforprint, bool waitfortail )
    {
        tlv_libhelper tlvh(CMD_PRINT,app_name,app_name_crd);
        if(text)
          tlvh.add_tag(prnapp_tags::TagPrintMessage,text, textLength);
        tlvh.add_tag(prnapp_tags::TagPOSProtocol,pos_proto);
        tlvh.add_tag(prnapp_tags::TagWaitForPrinting,waitforprint);
        tlvh.add_tag(prnapp_tags::TagWaitForTail,waitfortail);
        return tlvh.send_rcv_simple();
    }*/

/* ---------------------------------------------------------------------- */
}

