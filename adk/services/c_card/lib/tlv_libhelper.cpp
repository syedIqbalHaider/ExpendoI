/* ----------------------------------------------------------------- */
/*
 * tlv_libhelper.cpp
 *
 *  Created on: 2010-03-16
 *      Author: Lucjan_B1
 */
/* ----------------------------------------------------------------- */
#include <svc.h>

#include <cstring>
#include <algorithm>

#include <liblog/logsys.h>
#include <libipc/ipc.h>
#include <libtags/tags.h>
#include <tlv-lite/ConstData.h>
#include <tlv-lite/SafeBuffer.hpp>
#include <tlv-lite/TagLenVal.hpp>
#include <tlv-lite/TagListIter.hpp>
#include <tlv-lite/TLVIterator.hpp>
#include <tlv-lite/SafeBuffer.hpp>
#include <tlv-lite/TagLenVal.hpp>
#include <tlv-lite/EMV/EMVTags.hpp>
#include <tlv-lite/TagListIter.hpp>
#include <tlv-lite/ValueConverter.hpp>

#include "CARDApp.h"

#include <lib/tlv_libhelper.hpp>

/* ----------------------------------------------------------------- */
namespace com_verifone_crdapi_hlp
{
/* ----------------------------------------------------------------- */
//Constructor
tlv_libhelper::tlv_libhelper(unsigned char cmd_, const char *app_name_, const char *app_name_crd_)
    : buffer(tlv_buf+1,sizeof(tlv_buf)-1),cmd(cmd_)
    , app_name(app_name_)
{
        tlv_buf[0] = cmd;
        std::strncpy(app_name_crd,app_name_crd_,sizeof(app_name_crd));
}

/* ----------------------------------------------------------------- */
//Send the acm buffer
int tlv_libhelper::send()
{
    int ret = 0;
    //dlog_alert("Send %d from %s to %s [%02x:%d]" ,buffer.getLength()+1,app_name_crd, app_name,tlv_buf[0],tlv_buf[0]);
    //dlog_hex( tlv_buf, buffer.getLength()+1, "TX" );
    /* NKJT
    if( (ret = acm_send( tlv_buf, buffer.getLength()+1, app_name_crd, app_name )) != 0 )
    {
           dlog_error("error acm_send ret %d", ret);
           return ret;
    }
    */
    return ret;
}

/* ----------------------------------------------------------------- */
//Receive and parse acm response
int tlv_libhelper::receive( parser &p, unsigned long timeout,
        short (*fcancel)(void *),void *param )
{
    using namespace com_verifone_crdapi;
    using namespace com_verifone_TLVLite;
    int rx_length = 0;
    int ret = CRD_OKAY;
    //dlog_msg( "::::receive form %s to %s",app_name_crd, app_name); SVC_WAIT(100);
    char from[APP_NAME_CRD_SIZE];
    /* NKJT;
    while(true)
    {
        for(;;)
        {
            std::strcpy(from,app_name);
            ret = acm_receive( tlv_buf, &rx_length, from, NULL, NULL, sizeof(tlv_buf) );
            if( ret != ERROR_ACM_TIMEOUT ) // received okay or other error
            {
                //dlog_msg("Not timeout code %d",ret);
                break;
            }
            if( fcancel && fcancel(param) != 0 )
            {
                // TODO: There is no way to cancel commands. Replace the below when it's implemented in CARDAPP!!!
                unsigned char cancel_msg = CMD_CANCEL_WAITING;
                if( (ret = acm_send( &cancel_msg, sizeof(cancel_msg),
                        app_name_crd, app_name )) != 0 )
                {
                    dlog_msg( "error sending cancel, acm_send ret %d", ret );
                }
                else
                {
                    dlog_msg("Cancel sent from %s to %s",app_name_crd, app_name);
                    SVC_WAIT(100);
                }
            }
            if( timeout && read_ticks() >= tout_ticks ) //if ulTimeout == 0, no timeout!
            {
                dlog_msg( "receive timeout" );
                ret = ERROR_ACM_TIMEOUT;
                break;
            }
            SVC_WAIT(0);
        }
        if(!ret)
        {
            if(tlv_buf[0] != (cmd|0x80))
            {
                dlog_hex( tlv_buf, rx_length, "RX" );
                dlog_error("Message type mismatch %d != %d!",tlv_buf[0],(cmd|0x80));
            }
            else if(ret>=0 && rx_length>1)
            {
                TLVListWrapper tlvList(tlv_buf+1,rx_length-1);
                for (TLVIterator it = tlvList.begin(); it != tlvList.end(); ++it)
                {
                    if(it.isError())
                    {
                        dlog_error("TLV Error");
                        return CRD_CODE_INVAL;
                    }
                    else
                    {
                        p(*it);
                    }
                }
            }
        }
        break;
    }
*/
    return ret;
}

/* ----------------------------------------------------------------- */
void tlv_libhelper::add_tag(const com_verifone_TLVLite::ConstData_t& tag, const char *txt)
{
    using namespace com_verifone_TLVLite;
    if (txt)
    {
        std::size_t len = std::strlen(txt);
        buffer.append( TagLenVal( tag, ConstData_t(txt,len) ) );
    }
}

/* ----------------------------------------------------------------- */
void tlv_libhelper::add_tag(const com_verifone_TLVLite::ConstData_t& tag, const void *data, std::size_t size)
{
    using namespace com_verifone_TLVLite;

    buffer.append(TagLenVal( tag, ConstData_t(data,size)));
}

/* ----------------------------------------------------------------- */
namespace parsers
{
/* ----------------------------------------------------------------- */
void resp_parser::operator()(com_verifone_TLVLite::TagLenVal const & tlv)
{
    using namespace com_verifone_TLVLite;
    using namespace com_verifone_crdapi;
    
    if(tlv.getTag()==guiapp_tags::TagCode)
    {
        ValueConverter<uint8_t> value(tlv.getData());
        rcode = value.getValue(CRD_FAILED);
        dlog_msg("rcode value %d", rcode);
        set_error(value.isError());
    }
}

/* ----------------------------------------------------------------- */
void str_parser::operator()(com_verifone_TLVLite::TagLenVal const & tlv)
{
    using namespace com_verifone_TLVLite;
    using namespace com_verifone_crdapi;
    using namespace com_verifone_utils;
    
    //ctmp
    {
        const com_verifone_TLVLite::ConstData_t & tmp_tag = tlv.getTag();
        
        dlog_hex(tmp_tag.getByteBuffer(), tmp_tag.getSize(), "[tmp_tag]");
        dlog_hex(tag.getByteBuffer(), tag.getSize(), "[tag]");
    }
    //
    
    if(tlv.getTag()==tag)
    {
         ConstData_t const&  data = tlv.getData();
         str = c_string(static_cast<const char*>(data.getBuffer()),data.getSize());
         dlog_hex(str.c_str(), strlen(str.c_str()), "[str_parser::operator()]");
    }
    else
    {
        resp_parser::operator()(tlv);
        dlog_msg("str_parser::operator(): TAG NOT FOUND, calling resp_parser for Response Code!");
    }
}

/* ----------------------------------------------------------------- */
void data_parser::operator()(com_verifone_TLVLite::TagLenVal const & tlv)
{
    using namespace com_verifone_TLVLite;
    using namespace com_verifone_crdapi;
    using namespace com_verifone_utils;
    
    //ctmp
    {
        const com_verifone_TLVLite::ConstData_t & tmp_tag = tlv.getTag();
        
        dlog_hex(tmp_tag.getByteBuffer(), tmp_tag.getSize(), "[tmp_tag]");
        dlog_hex(tag.getByteBuffer(), tag.getSize(), "[tag]");
    }
    //
    
    if(tlv.getTag()==tag)
    {
        ConstData_t const&  data = tlv.getData();
        if(m_pdata)
        {
            delete [] m_pdata;
            m_pdata = NULL;
        }
        
        m_pdata = new unsigned char[data.getSize()];
        assert(m_pdata);
        memcpy(m_pdata, data.getBuffer(), data.getSize());
        m_size = data.getSize();
    }
    else
    {
        resp_parser::operator()(tlv);
        dlog_msg("str_parser::operator(): TAG NOT FOUND, calling resp_parser for Response Code!");
    }
}

/* ----------------------------------------------------------------- */
}
/* ----------------------------------------------------------------- */
//Check keep alive
int tlv_libhelper::check_allive()
{
    return 0; // NKJT acm_check_app_alive( app_name, KEEP_ALIVE_TIMEOUT );
}

/* ----------------------------------------------------------------- */
int tlv_libhelper::send_rcv_simple()
{
    int ret;
    if((ret=this->send())<0)
    {
       dlog_error("Connect send error %d",ret);
       return ret;
    }
    parsers::resp_parser rp;
    if((ret=this->receive(rp))<0)
    {
       dlog_error("Connect receive error %d",ret);
        return ret;
    }
    ret = rp.get_respcode();
    dlog_alert("Response code is %d",ret);
    return ret;
}

int tlv_libhelper::send_rcv_data(const com_verifone_TLVLite::ConstData_t& data_tag, unsigned char *data, std::size_t * pLen, std::size_t size)
{
    int ret;
    if((ret=this->send())<0)
    {
       dlog_error("Connect send error %d",ret);
       return ret;
    }
    parsers::data_parser dp(data_tag);
    if((ret=this->receive(dp))<0)
    {
       dlog_error("Connect receive error %d",ret);
        return ret;
    }
    
    std::size_t data_size;
    data_size = (dp.get_size() > size) ? size : dp.get_size();
    
    memcpy(data, dp.get_data(), data_size);
    *pLen = data_size;
    
    ret = dp.get_respcode();
    dlog_alert("Response code is %d", ret);
    return ret;
}

/* ----------------------------------------------------------------- */
}
/* ----------------------------------------------------------------- */
