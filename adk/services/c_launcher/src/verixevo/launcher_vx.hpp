/*
 * launcher_vx.hpp
 *
 *  Created on: 12-07-2013
 *      Author: lucck
 */
/* -------------------------------------------------------------- */
#ifndef LAUNCHER_VX_HPP_
#define LAUNCHER_VX_HPP_
/* -------------------------------------------------------------- */
#include <launcher_common.hpp>

/* -------------------------------------------------------------- */
namespace com_verifone_launch {
/* -------------------------------------------------------------- */
class launcher_vx : public launcher_common
{
public:
	launcher_vx();
	~launcher_vx();
protected:
	virtual int scan_files( std::list<std::string> &files );
	//Execute system mapp RETURN app pid
	virtual int sys_exec_app( const std::string &cmdline  );
	//Wait when all child exits
	virtual int wait_for_childs();
	//Sys kill app
	virtual int sys_kill_app( int pid );
	//Wait for APP ready
	virtual int wait_for_app_ready();
	//Notify the complete event
	virtual int post_all_apps_complete( int num_apps );
private:
	sem_t * g_sem_lock;
};

/* -------------------------------------------------------------- */
}
/* -------------------------------------------------------------- */



#endif /* LAUNCHER_VX_HPP_ */
