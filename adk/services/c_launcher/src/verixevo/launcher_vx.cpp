/*
 * launcher_vx.cpp
 *
 *  Created on: 12-07-2013
 *      Author: lucck
 */

/* -------------------------------------------------------------- */
#include <errno.h>

#include <liblog/logsys.h>

#include "launcher_vx.hpp"
#include <libpml/pml_abstracted_api.h>
//#include <boost/tokenizer.hpp>
#include <boost/algorithm/string.hpp>


/* -------------------------------------------------------------- */
namespace com_verifone_launch {
/* -------------------------------------------------------------- */
namespace
{
	static char TASK_LOCK_SEMAPHORE[] = "L_TASK_READY";
}
launcher_vx::launcher_vx(): g_sem_lock(sem_open(TASK_LOCK_SEMAPHORE, 0))
{
	assert(reinterpret_cast<int>(g_sem_lock) != -1); // CRASH if semaphore is not opened!
	sem_init(g_sem_lock, 0);
}
/* -------------------------------------------------------------- */
launcher_vx::~launcher_vx()
{
	if (g_sem_lock)
	{
		sem_close(g_sem_lock);
		g_sem_lock = 0;
	}
}
/* -------------------------------------------------------------- */
//Scan files platform depends
int launcher_vx::scan_files( std::list<std::string> &files )
{
	com_verifone_pml::dir_search ds;
	char fileName[50];
	std::string runFileMask("*.run");
	std::string placesToLook[] = { com_verifone_pml::getRAMRoot(), com_verifone_pml::getFlashRoot() };
	int result = 0;
	
	dlog_msg("Looking for RUN files, mask %s", runFileMask.c_str());
	memset(fileName, 0, sizeof(fileName));
	for (int i = 0; i < sizeof(placesToLook)/sizeof(placesToLook[0]); ++i)
	{
		strncpy(fileName, placesToLook[i].c_str(), sizeof(fileName));
		int found = ds.get_first(runFileMask.c_str(), fileName, sizeof(fileName));
		while (found > 0)
		{
			files.push_back(fileName);
			found = ds.get_next(fileName, sizeof(fileName));
		}
	}

	return 0;
}

/* -------------------------------------------------------------- */
//Execute system mapp RETURN app pid
int launcher_vx::sys_exec_app( const std::string &cmdline )
{
	// We need to separate app from its params
	//boost::char_separator<char> sep(" ");
	//boost::tokenizer<boost::char_separator<char> > tokens(cmdline, sep);
	int result = -1;
	typedef std::list<std::string> tokens;
	tokens tokenList;
	boost::split(tokenList, cmdline, boost::is_any_of(", "), boost::token_compress_on);
	if (tokenList.size() > 0)
	{
		std::string fname = tokenList.front();
		tokenList.pop_front();
		std::string params;
		for (tokens::const_iterator it = tokenList.begin(); it != tokenList.end(); ++it)
		{
			params.append(*it);
			params.append(1, ' ');
		}
		
		if (!fname.size())
		{
			dlog_error("Invalid parameters, no filename");
		}
		else
		{
			/*
				Workaround for file format (./appname)
			*/
			const std::string extension(".OUT");
			const std::string linuxPath("./");
			const std::string verixFlash("F:");
			int pos;
			if ( (pos = fname.find(linuxPath)) != std::string::npos)
			{
				fname.erase(pos, linuxPath.size());
			}
			if ( (pos = fname.find(verixFlash)) == std::string::npos)
			{
				fname.insert(0, verixFlash);
			}
			std::transform(fname.begin(), fname.end(), fname.begin(), toupper);
			if (fname.find(extension) == std::string::npos)
			{
				fname.append(extension);
			}
			dlog_msg("Starting app '%s'", fname.c_str());
			if (params.size()) dlog_msg("... with params '%s'", params.c_str());
			result = run(fname.c_str(), params.c_str());
			if (result != -1)
			{
				dlog_msg("New task %s started, task id %d", fname.c_str(), result);
				// SVC_WAIT(1000); // wait a sec before executing next 
				result = 0;
			}
			else
			{
				dlog_error("Cannot run task, errno %d!", errno);
			}
		}
	}
	else
	{
		dlog_error("Invalid parameters");
	}
	dlog_msg(">>>>STACK: %ld/%ld HEAP: %ld/%ld<<<<<", _stack_max(), _stacksize, _heap_max(), _heapsize);
	return result;
}
/* -------------------------------------------------------------- */
//Wait when all child exits
int launcher_vx::wait_for_childs()
{
	// what should we do here... 
	while (1)
	{
		wait_evt(EVT_USER);
		dlog_alert("User event received (%ld!", read_user_event());
	}
	return 0;
}

/* -------------------------------------------------------------- */
namespace
{
	static char TASK_SEMAPHORE[] = "L_TASK_INIT";
}

//Wait for APP ready
int launcher_vx::wait_for_app_ready()
{
	sem_t * g_semaphore = sem_open(TASK_SEMAPHORE, 0);
	if (reinterpret_cast<int>(g_semaphore) == -1)
	{
		dlog_error("Cannot open global semaphore %s, errno %d", TASK_SEMAPHORE, errno);
		return -1;
	}
	sem_init(g_semaphore, 0);
	// Kamil_P1: There is no method to wait for a semaphore with timeout...
	long cur_time = read_ticks();
	long end_time = cur_time + PROCESS_ACTIVATION_TIMEOUT_MS;
	int sem_state;
	int result;
	while(true)
	{
		sem_state = sem_value(g_semaphore);
		dlog_msg("Sem state %d", sem_state);
		if (sem_state == 2)
		{
			dlog_msg("Task initialized successfully!");
			result = E_SUCCESS;
			break;
		}
		else if (sem_state == -1)
		{
			dlog_error("Error occurred, cannot check semaphore state!");
			result = E_SYSTEM;
			break;
		}
		SVC_WAIT(100);
		cur_time = read_ticks();
		if (cur_time > end_time)
		{
			dlog_error("Waiting timed out!");
			result = E_TIMEOUT;
			break;
		}
	}
	sem_close(g_semaphore);
	return result;
}
/* -------------------------------------------------------------- */
//Sys kill app
int launcher_vx::sys_kill_app( int pid )
{
	//FIXME: Implement this
	return -1;
}
/* -------------------------------------------------------------- */
int launcher_vx::post_all_apps_complete( int num_apps )
{
	for (int i = 0; i < num_apps; i++)
	{
		sem_post(g_sem_lock);
	}
	sem_close(g_sem_lock);
	g_sem_lock = 0;
	return 0;
}
/* -------------------------------------------------------------- */
}
/* -------------------------------------------------------------- */
