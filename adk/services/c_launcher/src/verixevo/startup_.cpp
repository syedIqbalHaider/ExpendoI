/* -------------------------------------------------------------- */
#include <errno.h>
#include "launcher_vx.hpp"
#include <liblog/logsys.h>
/* -------------------------------------------------------------- */
namespace
{
	static const char c_logical_name[] = "STARTUP";
}

/* -------------------------------------------------------------- */
int main (int argc, char *argv[])
{
	LOG_INIT( (char*)c_logical_name , LOGSYS_PIPE, LOGSYS_PRINTF_FILTER );
	com_verifone_launch::launcher_vx the_launcher;
	return the_launcher.process_list();
}
/* -------------------------------------------------------------- */
