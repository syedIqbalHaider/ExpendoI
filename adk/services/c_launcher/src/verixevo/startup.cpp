#include <svc.h>
#include <errno.h>

/* -------------------------------------------------------------- */
#include <liblog/logsys.h>

/* -------------------------------------------------------------- */
int main (int argc, char *argv[])
{
    static char * g_LogicalName = "STARTUP";
    static const char * appToStart[] = {
        "F:GUIAPP.OUT",
        "F:SCAPP.OUT",
        "F:CARDAPP.OUT",
        "F:MAPP.OUT",
        "F:CICAPP.OUT"
    };

    // first of all, start logging :)
    LOG_INIT( g_LogicalName, LOGSYS_PIPE, LOGSYS_PRINTF_FILTER );
    dlog_msg("Hello world from %s", g_LogicalName);

    for (int i = 0; i < sizeof(appToStart)/sizeof(appToStart[0]); ++i)
    {
        dlog_msg("Starting app %s", appToStart[i]);
        int res = run(appToStart[i], NULL);
        dlog_msg("Startup result %d", res);
        if (res < 0) dlog_error(" --> means FAILURE! errno %d", errno);
        else SVC_WAIT(100); // give some time to the task to initialise!
    }
    dlog_msg("Were all done!");
    return 1;
}

/* -------------------------------------------------------------- */
