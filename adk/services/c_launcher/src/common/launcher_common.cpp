/*
 * launcher_common.cpp
 *
 *  Created on: 12-07-2013
 *      Author: lucck
 */
/* -------------------------------------------------------------- */
#include <cstdlib>
#include <fmtlog/fmtlog.hpp>
#include <launcher_common.hpp>
#include <boost/foreach.hpp>
#include <fstream>
#include <vector>
#include <utility>
#include <algorithm>
#include <libminini/minIni.h>
#include <boost/tokenizer.hpp>
#include <boost/utility.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/topological_sort.hpp>
#include <boost/graph/depth_first_search.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/visitors.hpp>
#include <sys/types.h>
/* -------------------------------------------------------------- */
//TODO: Error handling
/* -------------------------------------------------------------- */
namespace com_verifone_launch {
/* -------------------------------------------------------------- */
//Process list files
int launcher_common::process_list(  )
{
	int ret = 0;
	std::list<std::string> files;
	std::vector<app_item> apps;
	std::map<std::string, int> id_to_name;
	ret = scan_files( files );
	if( ret < 0 )
		return ret;
	int curr_idx = 0;
	BOOST_FOREACH( const std::list<std::string>::value_type item, files )
	{
		app_item aitem;
		log_fmt_info( "file %1%", item );
		minIni ini( item );
		aitem.exec = ini.gets( "config", "Exec" );
		if( aitem.exec.empty() )
		{
			log_fmt_warn("Unable to read Exec section");
			return -1;
		}
		aitem.enabled = ini.getbool( "config", "Enabled" , false );
		aitem.version = ini.gets( "config", "Version" );
		aitem.name = ini.gets( "config", "Name" );
		if( aitem.name.empty() )
		{
			log_fmt_warn( "Friendly name not defined" );
			return -1;
		}
		aitem.sha1 = ini.gets( "config", "SHA1" );
		const std::string depends = ini.gets( "config", "Depends" );
		typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
		boost::char_separator<char> sep(" ,");
		tokenizer tokens(depends, sep );
		BOOST_FOREACH(const tokenizer::value_type val, tokens)
		{
			aitem.depends.push_back( val );
		}
		aitem.id = curr_idx++;
		apps.push_back( aitem );
		id_to_name.insert(  std::make_pair(aitem.name, aitem.id) );
		log_fmt_info("Adding name [%1%]",aitem.name);
	}
	//Reorder application dependencies
	if( !ret )
	{
		ret = build_dependencies( apps, id_to_name );
	}
	if( !ret )
	{
		std::list<int> run_pids;
		BOOST_FOREACH( const std::list<app_item>::value_type &item, m_run_apps )
		{
			int cpid = -1;
			//TODO: SHA1 checking
			if( item.sha1.empty() && item.enabled  ) 
			{

				ret = sys_exec_app( item.exec );
				log_fmt_error("Execute image: %1% PID: %2%" , item.exec % ret);

//				iq_update_24082017 start
				while(ret<0)
				{
					ret = sys_exec_app( item.exec );
					log_fmt_error("Execute image: %1% PID: %2%" , item.exec % ret);
					usleep(100);
				}

//				if ( ret < 0 )
//				{
//					log_fmt_error("Unit %1% fail with code %2%",item.name % ret );
//					notify( item.name, ret, ret );
//					break;
//				}

//				iq_update_24082017 end

				run_pids.push_back( ret ); cpid = ret;
				ret = wait_for_app_ready();
				if( ret < 0 )
			    {
					log_fmt_error("AppWait %1% failed with code %2%", item.name % ret );
					notify( item.name, cpid, ret );
					break;
				}
				notify( item.name, cpid, ret );
			}
		}
		if( ret < 0 )  
		{
			log_fmt_error( "Terminating others apps ...");
			BOOST_FOREACH( const std::list<int>::value_type pid, run_pids ) 
			{
				log_fmt_error( "Kill %1%", pid );
				int kret = sys_kill_app( pid );
				if(  kret  < 0 ) {
					log_fmt_error("Unable to kill: %1% errno %2%", pid % kret ); 
				}
			}
		}
		//Notify that all apps are ready
		post_all_apps_complete( run_pids.size() );
		//Wait for all childs
		ret = wait_for_childs();
		log_fmt_info("Wait for childs exit with code %1%", ret );
	}
	return ret;
}
/* -------------------------------------------------------------- */
//Reorder dependencies
int launcher_common::build_dependencies(const std::vector<app_item> &apps, const std::map<std::string, int>& id_to_name )
{
	typedef std::pair<int,int> edge;
	std::vector<edge> used_by;
	BOOST_FOREACH(const std::list<app_item>::value_type &val, apps)
	{
		log_fmt_info("EXEC [%1%]->[%2%]", val.name % val.id );
		BOOST_FOREACH( const std::vector<std::string>::value_type &dep, val.depends )
		{
			//log_fmt_info("DEPENDS %1%", dep );
			std::map<std::string, int>::const_iterator dep_id = id_to_name.find( dep );
			if( dep_id == id_to_name.end() )
			{
				log_fmt_error("Dependecy error [%1%] not found", dep );
				return -1;
			}
			used_by.push_back( std::make_pair(dep_id->second, val.id ) );
		}
	}
	//BOOST_FOREACH(const std::vector<edge>::value_type &val, used_by)
	//{
	//	log_fmt_info("DEP %1% : %2%", val.first % val.second );
	//}
	//Create DAG graph
	typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::bidirectionalS> graph;
	typedef boost::graph_traits<graph>::vertex_descriptor vertex;
	graph g( used_by.begin(), used_by.end(), used_by.size()-1 );
	typedef std::list<vertex> make_order_t;
	make_order_t make_order;
	boost::topological_sort(g, std::front_inserter(make_order));
	BOOST_FOREACH( const make_order_t::value_type &v, make_order )
	{
		//log_fmt_info("After sort order deps %1%", v );
		if( apps.size() > v )
			m_run_apps.push_back( apps[v] ); 
	}
#ifndef NDEBUF
	//TEST ONLY
	BOOST_FOREACH( const std::list<app_item>::value_type &v, m_run_apps )
	{
		log_fmt_info("Exec [%1%] App [%2%]", v.exec % v.name );
	}
	return 0;
#endif
}

/* -------------------------------------------------------------- */
}
/* -------------------------------------------------------------- */


