/* -------------------------------------------------------------- */
#include <errno.h>
#include "launcher_vos.hpp"
#include <fmtlog/fmtlog.hpp>
#include "graphic_utility.hpp"
#include <boost/bind.hpp>
/* -------------------------------------------------------------- */
namespace
{
	static const char c_logical_name[] = "STARTUP";
}

/* -------------------------------------------------------------- */
int main (int argc, char *argv[])
{
	using namespace com_verifone_launch;
	using boost::bind;
	log_fmt_info("Hello from startup app PID [%1%]", getpid());
	launcher_vos the_launcher;
	detail::graphics_utility m_utility;
	the_launcher.register_notify_callback( 
			boost::bind( &detail::graphics_utility::on_new_application,
			boost::ref(m_utility),_1,_2,_3 )
	);
	return the_launcher.process_list();
}
/* -------------------------------------------------------------- */
