/*
 * =====================================================================================
 *
 *       Filename:  graphic_utility.hpp
 *
 *    Description:  Graphics application launcher utility implementation
 *
 *        Version:  1.0
 *        Created:  12/12/2013 03:12:18 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lucjan Bryndza (lb), Lucjan_B1@verifone.com
 *   Organization:  VERIFONE
 *
 * =====================================================================================
 */
#include <boost/noncopyable.hpp>
#include <string>
#include <cstdio>
namespace com_verifone_launch {
namespace detail {

	//Graphics utility 
	class graphics_utility :  private boost::noncopyable {
	public:
		void on_new_application( const std::string& appname, int pid, int status ) {
			std::printf("::::::::::::: app %s pid %i status %i\n", appname.c_str(), pid, status );
		}
	};

} //Detail namespace end 
} // Launch namespace end
