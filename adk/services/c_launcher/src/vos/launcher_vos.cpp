/*
 * launcher_vx.cpp
 *
 *  Created on: 12-07-2013
 *      Author: lucck
 */

/* -------------------------------------------------------------- */
#include "launcher_vos.hpp"
#include <libpml/pml_abstracted_api.h>
#include <fmtlog/fmtlog.hpp>
#include <stdlib.h>
#include <string>
#include <cstring>
#include <fmtlog/fmtlog.hpp>
#include <unistd.h>
#include <boost/noncopyable.hpp>
#include <cstring> 
#include <sys/types.h>
#include <sys/wait.h>
#include <boost/thread/thread_time.hpp>
#include <signal.h>
/* -------------------------------------------------------------- */
namespace pml = com_verifone_pml;

/* -------------------------------------------------------------- */
namespace com_verifone_launch {
/* -------------------------------------------------------------- */
namespace {
/* -------------------------------------------------------------- */
//NOTE: We need to pure C array - We works on current strings
class cmdline_parser : private boost::noncopyable
{
public:
	cmdline_parser(const std::string &cmdline )
	  : m_argc(0)
	{
		int i, spc;
		size_t len;
		char *s,*d;
		/* make a copy to play with */
		char *cmd_line1 = strdup(cmdline.c_str());
		
		/* count items to deal with and trim multiple spaces */
		d = s = cmd_line1; spc = 1; len=std::strlen(cmd_line1);
		for (i=0; i<len;i++,s++) 
		{
			switch (*s) 
			{
				case ' ':
					if (spc) continue;
					*d++ = '\0'; /* replace spaces with zeroes */
					spc = 1;
					break;
				default:
					if (spc) { (m_argc)++; spc = 0; }
					*d++ = *s;
			}
		}
		(*d++) = '\0'; /* line termination */
		/* calc actual size */
		len = d - cmd_line1;
		/* allocate array of poiters */
		m_argv = (char**) malloc(sizeof(char*) * ((m_argc)+1) + len);
		m_argv[m_argc] =  NULL;
		d = reinterpret_cast<char*> (&(m_argv)[(m_argc)+1]);
		memmove(d, cmd_line1, len);
		free(cmd_line1);
		cmd_line1 = d;
		/* scan line again to find all lines starts and register */
		for (i=0; i<m_argc; i++) 
		{
			m_argv[i] = d;
			while (*(d++)) ;
		}
	}
	~cmdline_parser()
	{
		free( m_argv );
	}
	char* const* get_argv() const
	{
		return m_argv;
	}
	int get_argc() const
	{
		return m_argc;
	}
private:
	int m_argc;
	char **m_argv;
};
/* -------------------------------------------------------------- */
}	//Anonymous namespace end
/* -------------------------------------------------------------- */
//Scan files platform depends
int launcher_vos::scan_files( std::list<std::string> &files )
{
	int ret = 0;
	char path[256] = { 0 };
	pml::dir_search searcher;
	strncpy(path, com_verifone_pml::getFlashRoot().c_str(), sizeof(path)-1);
	ret = searcher.get_first("*.run", path, sizeof path ); 
	if( ret < 0 ) return ret;
	files.push_back(path);
	// log_fmt_info("Found %1%", path);
	while( (ret = searcher.get_next( path, sizeof path ) ) )
	{
		if( ret == pml::dir_search::err_system )
		{
			return ret;
		}
		else
		{
			files.push_back(path);
			// log_fmt_info("Found %1%", path);
		}
	}
	return ret;
}
/* -------------------------------------------------------------- */
//Execute system mapp RETURN app pid
int launcher_vos::sys_exec_app( const std::string &cmdline )
{
	const int ret = vfork();
	if( ret == -1 || ret > 0 )
	{
//		sleep(1);
		return ret;
	}
	else if( ret == 0 )
	{
		cmdline_parser cmd( cmdline );
		const int rete = execvp( cmd.get_argv()[0], cmd.get_argv() );
		if( rete == -1 )
		{
			log_fmt_info("execv() failed %1%", rete );
			exit( EXIT_FAILURE );
		}
		else
		{
			log_fmt_info("Process started as PID [%1%]", getpid() );
			//sleep(1);
		}
	}
	return ret;
}
/* -------------------------------------------------------------- */
int launcher_vos::wait_for_app_ready()
{
#ifdef USE_VOS_SEMAPHORES
	boost::system_time const timeout = boost::get_system_time() +  
		boost::posix_time::milliseconds( PROCESS_ACTIVATION_TIMEOUT_MS );
	if( !m_sem.timed_wait( timeout ) ) {
		return E_TIMEOUT;
	} else {
		return E_SUCCESS;
	}
#else
	return E_SUCCESS;
#endif
}
/* -------------------------------------------------------------- */
//Wait when all child exits
int launcher_vos::wait_for_childs()
{
	siginfo_t signalInfo;
	int ret;
	while( (ret=waitid(P_ALL, 0, &signalInfo, WEXITED))==0 )
		log_fmt_info("Child exits %1%", ret);
	return ret;
}

/* -------------------------------------------------------------- */
//Sys kill app
int launcher_vos::sys_kill_app( int pid )
{
	return ::kill( pid, SIGKILL );
}
/* -------------------------------------------------------------- */
//Notify the complete event
int launcher_vos::post_all_apps_complete( int num_apps )
{
#ifdef USE_VOS_SEMAPHORES
	for( int post=0; post < num_apps; ++num_apps ) {
		m_sem_complete.post();
	}
#endif
	return 0;
}
/* -------------------------------------------------------------- */
}
