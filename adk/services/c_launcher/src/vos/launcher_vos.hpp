/*
 * launcher_vx.hpp
 *
 *  Created on: 12-07-2013
*      Author: lucck
 */
/* -------------------------------------------------------------- */
#ifndef LAUNCHER_VOS_HPP_
#define LAUNCHER_VOS_HPP_
/* -------------------------------------------------------------- */
#include <launcher_common.hpp>
#include <boost/interprocess/sync/named_semaphore.hpp>
/* -------------------------------------------------------------- */
namespace com_verifone_launch {
/* -------------------------------------------------------------- */
//Laucher basic vos
class launcher_vos : public launcher_common
{
public:
#ifdef USE_VOS_SEMAPHORES
	//Constructor
	launcher_vos() 
		: m_sem( boost::interprocess::open_or_create, "vfi_app_launcher_sem_from_app" , 0),
		  m_sem_complete( boost::interprocess::open_or_create, "vfi_app_launcher_complete" , 0)
	{}
#endif 
	//Launcher vos destructor
	virtual ~launcher_vos()
	{}
protected:
	virtual int scan_files( std::list<std::string> &files );
	//Execute system mapp RETURN app pid
	virtual int sys_exec_app( const std::string &cmdline  );
	//Wait when all child exits
	virtual int wait_for_childs();
	//Wait for APP ready
	virtual int wait_for_app_ready();
	//Sys kill app
	virtual int sys_kill_app( int pid );
	//Notify the complete event
	virtual int post_all_apps_complete( int num_apps );
private:
#ifdef USE_VOS_SEMAPHORES
	boost::interprocess::named_semaphore m_sem;
	boost::interprocess::named_semaphore m_sem_complete;
#endif
};

/* ------------------------------------------------------------- */
}
/* -------------------------------------------------------------- */



#endif /* LAUNCHER_VX_HPP_ */
