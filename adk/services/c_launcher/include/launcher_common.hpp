/*
 * launcher_common.hpp
 *
 *  Created on: 12-07-2013
 *      Author: lucck
 */
/* -------------------------------------------------------------- */
#ifndef LAUNCHER_COMMON_HPP_
#define LAUNCHER_COMMON_HPP_
/* -------------------------------------------------------------- */
#include <boost/noncopyable.hpp>
#include <list>
#include <string>
#include <vector>
#include <map>
#include <boost/function.hpp>
/* -------------------------------------------------------------- */
namespace com_verifone_launch {
/* -------------------------------------------------------------- */
//Application item
struct app_item
{
	int id;					/* Node identification */
	std::string exec;		/* Application exec */
	bool enabled;			/* Enabled */
	std::string version;	/* VERSION */
	std::string name;		/* name */
	std::vector<std::string> depends;	/* Depends */
	std::string sha1;		/* Sha1 sum */
	int pid;
};
/* -------------------------------------------------------------- */
class launcher_common  : private boost::noncopyable
{
public:
	enum status {
		E_SUCCESS = 0,
		E_SYSTEM = -1,
		E_TIMEOUT = -2
	};
	//Process list files
	int process_list();
	//GUI Callback function app_name pid status 
	typedef boost::function<void(const std::string& ,int , int )> notify_callback_t; 
	//Register callback
	void register_notify_callback( notify_callback_t notify_callback ) {
		if( !m_notify_callback ) {
			m_notify_callback = notify_callback;
		}
	}
	//Process activation timeout 
	static const int PROCESS_ACTIVATION_TIMEOUT_MS = 15000;
protected:
	//Scan files
	virtual int scan_files( std::list<std::string> &files ) = 0;
	//Execute system mapp RETURN app pid
	virtual int sys_exec_app( const std::string &cmdline  ) = 0;
	//Sys kill app
	virtual int sys_kill_app( int pid ) = 0;
	//Wait for app ready
	virtual int wait_for_app_ready() = 0;
	//Wait when all child exits
	virtual int wait_for_childs() = 0;
	//Notify the complete event
	virtual int post_all_apps_complete( int num_apps ) = 0;
private:
	//Notify callback called from application
	void notify( const std::string& app, int pid, int stat ) {
		if( m_notify_callback ) {
			m_notify_callback( app, pid, stat );
		}
	}
	//Reorder app dependencies
	int build_dependencies( const std::vector<app_item> &apps, const std::map<std::string, int>& id_to_name );
private:
	std::list<app_item> m_run_apps;
	notify_callback_t m_notify_callback;
};

/* -------------------------------------------------------------- */
}
/* -------------------------------------------------------------- */
#endif /* LAUNCHER_COMMON_HPP_ */

/* -------------------------------------------------------------- */
