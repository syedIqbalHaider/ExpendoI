/*
 * CLog.h
 *
 *  Created on: Dec 4, 2013
 *      Author: vfisdk
 */

#ifndef CLOG_H_
#define CLOG_H_

#include <iostream>
#include <string>

//using namespace std;
using std::string;
using std::stringstream;
using std::cout;
using std::endl;

#define TO_STR_A( A ) #A
#define TO_STR( A ) TO_STR_A( A )
#define MSG_INFO ":"__FILE__ "->line " TO_STR( __LINE__ ) ": "

class CLog
{
public:
	CLog(string appname);
	void message(string message);
	void uimsg(string message);
	string from_int(int x);

private:
	string app_name;
};

#endif /* CLOG_H_ */
