#ifndef _HIM_H_
#define _HIM_H_

#include <string>

//using namespace std;
using std::string;

namespace LIBCOMMS{
	typedef enum{
		HIM_AUTH_RQST
	}HIM_CMD;

	int Connect(string ip, string port);
	int Disconnect(int handle);
	int SendCommand(string data);
	int RecvResponse(string &response, long timeout_secs);
	string GetId(void);

}
#endif // _HIM_H_
