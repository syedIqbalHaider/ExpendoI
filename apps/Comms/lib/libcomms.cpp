#include <sys/timeb.h>
#include <libipc/ipc.h>
#include <string>
#include <unistd.h>

#include "libcomms.h"

#define COMMS_TASK		"Comms"

#define IPC_TIMEOUT		60

//using namespace std;

namespace LIBCOMMS{

int wait_ipc(string &response);

int Connect(string ip, string port)
{
	com_verifone_ipc::connect_to(COMMS_TASK);

	com_verifone_ipc::send("Connect:"+ip+":"+port,COMMS_TASK);

	string response;
	if(wait_ipc(response) <= 0)
		return -1;

	if(response.compare("Ok") == 0)
		return 0;

	return -2;
}

int Disconnect(int handle)
{
	com_verifone_ipc::send("Disconnect",COMMS_TASK);

	string response;
	if(wait_ipc(response) <= 0)
		return -1;

	if(response.compare("Ok") == 0)
		return 0;

	return -2;
}

int SendCommand(string data)
{
	com_verifone_ipc::send("Data:"+data,COMMS_TASK);

	string response;
	if(wait_ipc(response) <= 0)
		return -1;

	if(response.compare("Ok") == 0)
		return data.length();

	return 0;
}

int RecvResponse(string &response, long timeout_secs)
{
	com_verifone_ipc::send("Rx:", COMMS_TASK);

	string incoming;
	if(wait_ipc(incoming) <= 0)
		return -1;

	if(incoming.compare(0, 3, "Ok:") == 0){
		response = incoming.substr(3, incoming.length()-3);
		return incoming.length()-3;
	}

	return 0;
}

string GetId(void)
{
	return string(COMMS_TASK);
}

// Private functions
int wait_ipc(string &response)
{
	string ipcFrom;
	struct timeb start_tp;
	struct timeb  current_tp;

	ftime(&start_tp);

	while(1){
		if(com_verifone_ipc::receive(response,COMMS_TASK,ipcFrom)==com_verifone_ipc::IPC_SUCCESS){
			break;
		}
/*
		// Check the timeout value
		ftime(&current_tp);
		if(current_tp.time - start_tp.time >= IPC_TIMEOUT)
			return 0;
*/
		usleep(100000);		// Sleep for 100 milli seconds
	}

	return (int)response.length();
}


}
