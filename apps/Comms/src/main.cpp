#include <string>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <iostream>
#include <sys/types.h>
#include <errno.h>
#include <libda/cterminalconfig.h>
#include <libda/cversions.h>
#include <libda/cversionsfile.h>
#include <libda/cconmanager.h>
#include <sys/timeb.h>
#include <libipc/ipc.h>
#include "utils.h"
#include "CLog.h"
#include "Commsver.h"

#define COMMS_TASK		"Comms"

#define COMMS_RX_TIMEOUT		30		// seconds

static CLog log(COMMS_NAME);

//using namespace std;
using namespace Utils;
using namespace com_verifone_terminalconfig;

#include <cstring>      // Needed for memset
#include <sys/socket.h> // Needed for the socket functions
#include <netdb.h>      // Needed for the socket functions
#include <execinfo.h>
#include <signal.h>

#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/x509v3.h>

bool SSL_ENABLE=false;

static int socketfd=-1;
static SSL* g_SSL=NULL;
static SSL_CTX* g_CTX =NULL;
static SSL_SESSION* g_Session=NULL;
bool isSslErrorSyscall=false;

void fault_handler(int signal);
string sanitiseIP(string ip);

void initSSL(void);
SSL_CTX* setup_client_ctx (void);
void initOpenSSL(void);
void infoCallBack (const SSL *s, int where, int ret);
int VerifyCallBack (int ok, X509_STORE_CTX* store);
static void vdGetSignatureType(X509* cert);
static void vdEnumerateCiphers(SSL *ssl);
int sslHandShake(string ip);
long PostConnectionCheck (SSL *ssl, char *host);


int connect(string ip, string port)
{
	int status;
	struct addrinfo host_info;       // The struct that getaddrinfo() fills up with data.
	struct addrinfo *host_info_list; // Pointer to the to the linked list of host_info's.
	fd_set fdset;
	struct timeval tv;

	if(socketfd > 0)
		return 0;	// Already connected

	// Check that we received valid ip and port
	if((ip.length() == 0) || (port.length() == 0)){
		log.message(MSG_INFO "ERROR - Invalid IP or port\n");
		return -1;
	}

	string sip = sanitiseIP(ip);	// Clean up the ip address

	log.message(MSG_INFO "Connecting to " + sip + ":" + port +"\n");

	memset(&host_info, 0, sizeof host_info);
	host_info.ai_family =AF_INET;
	host_info.ai_socktype = SOCK_STREAM;

	status = getaddrinfo(sip.c_str(), port.c_str(), &host_info, &host_info_list);

	if (status != 0){
		log.message(MSG_INFO "getaddrinfo error" + string(gai_strerror(status)) + "\n");
		return -1;
	}

	log.message(MSG_INFO "Creating a socket...\n");

	// The socket descriptor
	socketfd = socket(host_info_list->ai_family, host_info_list->ai_socktype, host_info_list->ai_protocol);

	if (socketfd <= 0){
		log.message(MSG_INFO "socket error\n");
		freeaddrinfo(host_info_list);
		return -2;
	}

	// Set non-blocking
	long arg = fcntl(socketfd, F_GETFL, NULL);
	arg |= O_NONBLOCK;
	fcntl(socketfd, F_SETFL, arg);

	status = connect(socketfd, host_info_list->ai_addr, host_info_list->ai_addrlen);

	if(status < 0){
		if(errno == EINPROGRESS){
			tv.tv_sec = 10;             // 10 second timeout
			tv.tv_usec = 0;
			FD_ZERO(&fdset);
			FD_SET(socketfd, &fdset);

			if(select(socketfd + 1, NULL, &fdset, NULL, &tv) > 0){
				int so_error;
				socklen_t len = sizeof so_error;

				getsockopt(socketfd, SOL_SOCKET, SO_ERROR, &so_error, &len);

				if (so_error) {
					log.message(MSG_INFO "connect error "+UtilsIntToString(so_error)+": "+string(strerror(so_error))+"\n");
					freeaddrinfo(host_info_list);
					close(socketfd);
					socketfd = -1;
					return -3;
				}
			}else{
				log.message(MSG_INFO "Connect timeout!\n");
				freeaddrinfo(host_info_list);
				close(socketfd);
				socketfd = -1;
				return -4;
			}
		}else{
			log.message(MSG_INFO "Connect timeout!\n");
			freeaddrinfo(host_info_list);
			close(socketfd);
			socketfd = -1;
			return -4;
		}
	}

	std::cout << "Connected to " << ip << ":" << port << std::endl;

	// Set blocking again
	arg = fcntl(socketfd, F_GETFL, NULL);
	arg |= ~O_NONBLOCK;
	fcntl(socketfd, F_SETFL, arg);
	freeaddrinfo(host_info_list);

	if(SSL_ENABLE)
	{
		isSslErrorSyscall=false;
	// Do SSL handshake
	//	1 Create SSL context
		if(g_SSL == NULL)
		{
		   if ((g_SSL = SSL_new(g_CTX)) == NULL)
		  {

			 if(g_CTX != NULL)
			 {
			   SSL_CTX_set_info_callback (g_CTX, NULL);
			   SSL_CTX_free (g_CTX);
			   g_CTX = NULL;
			 }

			 log.message(MSG_INFO "SSL CONTEXT ERR\n");
			 sleep(1);
			 return -3;
		  }
		  vdEnumerateCiphers(g_SSL);
		}

		if(sslHandShake(ip))
		{
			log.message(MSG_INFO "SSL Hand shaking fail\n");
//			freeaddrinfo(host_info_list);
			close(socketfd);
			socketfd = -1;
			return -3;
		}
	}
	return 0;

}

int disconnect()
{
	if(SSL_ENABLE)
	{
		int i, inTries, err=0;

	//    if(gSSLResume == 1 )
	//    {
	//         inTries = 5;
	//		 g_Session = SSL_get1_session(g_SSL); /*Collect the session*/
	//	}
	//    else
	//   	{
			 g_Session = NULL;
			 inTries = 1;
	//   	}

	 if(!isSslErrorSyscall && g_SSL != NULL)
	 {
		 log.message(MSG_INFO "SSLshutdown....... \r\n");

		for(i= 0; i < inTries; i++)
		{
			err = SSL_shutdown (g_SSL);
			if( err == 1)// 1 = successfully shutdown
				break;
//			usleep(100000); // 100 ms
		}

//	//	printf( "SSL_shutdown ret =%d Tries=%d", err, ((i==5)?i:i+1));
	 }

		if(g_SSL != NULL)
		{
			log.message(MSG_INFO "Before Freeing SSL\n");

		   SSL_free (g_SSL);
		   g_SSL = NULL;

		   log.message(MSG_INFO "Freeing SSL\n");
		}
	}

	close(socketfd);
	socketfd = -1;

	return 0;
}

#if 0

int send(string data)
{
	std::cout << "send()ing message..."  << std::endl;
	ssize_t bytes_sent;
	string bcd_len;
	char cbcd_len[4+1];
	unsigned char clen[2+1];	//iq_audi_281116 1 added
	com_verifone_terminalconfig::CTerminalConfig termcfg;


	if(socketfd <= 0)
		return 0;		// Not connected


	string isHexHeader;
	isHexHeader=termcfg.getIsHexHeader();

//	iq_audi_281116 bcd length header
	if(atoi(isHexHeader.c_str())!=1)
	{
		memset(cbcd_len,0,sizeof(cbcd_len));
		sprintf(cbcd_len,"%04i",data.length());
		bcd_len=string(cbcd_len);
		UtilStringToBCD(bcd_len,(char *)clen);
	}else
	{
//	iq_audi_281116 header length hex
		clen[0] = data.length()/256;               // High order byte of the message body size
		clen[1] = data.length()%256;               // Low order byte of the message body size
	}
	data.insert(0, (const char*)clen, 2);

	log.message(MSG_INFO "Sending ("+UtilsIntToString(data.length())+" bytes) ["+UtilsHexToString(data.c_str(), data.length())+"]\r\n");

	bytes_sent = send(socketfd, data.c_str(), data.length(), 0);

	if(bytes_sent == (ssize_t)data.length())
		return data.length()-2;

	return 0;
}

int receive(string &response)
{
	ssize_t bytes_received;
	char incoming_data_buffer[1000];
	char bcd_len[2+1];
	string resp;
	int len=0;
	struct timeb start_tp;
	struct timeb  current_tp;
	string isHexHeader;
	com_verifone_terminalconfig::CTerminalConfig termcfg;

	long secs = termcfg.getWaitTime();

	ftime(&start_tp);

	while(1){
		bytes_received = recv(socketfd, incoming_data_buffer,sizeof(incoming_data_buffer), MSG_DONTWAIT);

		// Check the timeout value
		ftime(&current_tp);
//		if(COMMS_RX_TIMEOUT <= current_tp.time - start_tp.time)
		if(secs <= current_tp.time - start_tp.time)
			return 0;

		if((bytes_received == -1) && ((errno == EAGAIN) || (errno == EWOULDBLOCK))){
			 if(resp.length() == 0)
				 continue;
			 else if(len <= (int)resp.length()-2){
				log.message(MSG_INFO "Received complete\n");
				break;
			}
		 }

		if(bytes_received == 0){
			log.message(MSG_INFO "Remote side disconnected\n");
			return 0;
		}else if(bytes_received > 0){
			if(resp.length() == 0){

				isHexHeader=termcfg.getIsHexHeader();

				if(atoi(isHexHeader.c_str())!=1)
				{
					memcpy(bcd_len,incoming_data_buffer,2);
					len = UtilsStringToInt(UtilsBcdToString((const char *)bcd_len, 2));
				}
				else
					len = (incoming_data_buffer[0] << 8) + incoming_data_buffer[1];  // for Hex

				log.message(MSG_INFO "Expecting "+UtilsIntToString(len)+" bytes\n");
			}

			resp.append(incoming_data_buffer, bytes_received);
			log.message(MSG_INFO "Received ("+UtilsIntToString(bytes_received)+" bytes) ["+UtilsHexToString(incoming_data_buffer, bytes_received)+"]\r\n");
		}
	}

	response = resp;
	return response.length();
}

#else
int send(string data)
{
	std::cout << "send()ing message..."  << std::endl;
	ssize_t bytes_sent;
	string bcd_len;
	char cbcd_len[4+1];
	unsigned char clen[2+1];	//iq_audi_281116 1 added
	com_verifone_terminalconfig::CTerminalConfig termcfg;

	if(socketfd <= 0)
		return 0;		// Not connected

	string isHexHeader;
	isHexHeader=termcfg.getIsHexHeader();

//	iq_audi_281116 bcd length header
	if(atoi(isHexHeader.c_str())!=1)
	{
		memset(cbcd_len,0,sizeof(cbcd_len));
		sprintf(cbcd_len,"%04i",data.length());
		bcd_len=string(cbcd_len);
		UtilStringToBCD(bcd_len,(char *)clen);
	}else
	{
//	iq_audi_281116 header length hex
		clen[0] = data.length()/256;               // High order byte of the message body size
		clen[1] = data.length()%256;               // Low order byte of the message body size
	}
	data.insert(0, (const char*)clen, 2);

	log.message(MSG_INFO "Sending ("+UtilsIntToString(data.length())+" bytes) ["+UtilsHexToString(data.c_str(), data.length())+"]\r\n");

	if(SSL_ENABLE)
		bytes_sent=SSL_write (g_SSL,  data.c_str(),  data.length());
	else
		bytes_sent = send(socketfd, data.c_str(), data.length(), 0);

	if(bytes_sent == (ssize_t)data.length())
		return data.length()-2;

	return 0;
}

int receive(string &response)
{
	ssize_t bytes_received;
	char incoming_data_buffer[1000];
	char bcd_len[2+1];
	string resp;
	int len=0;
	struct timeb start_tp;
	struct timeb  current_tp;
	string isHexHeader;
	com_verifone_terminalconfig::CTerminalConfig termcfg;

	long secs = termcfg.getResponseTimeout();

	ftime(&start_tp);

	if(SSL_ENABLE)
	{
		while(1){
			bytes_received = SSL_read (g_SSL, incoming_data_buffer,  sizeof(incoming_data_buffer));

			// Check the timeout value
			ftime(&current_tp);

			if(secs <= current_tp.time - start_tp.time) //timeout
				return 0;

			if(len <= (int)resp.length()-2){
					log.message(MSG_INFO "Received complete\n");
					break;
				}

			if(bytes_received > 0){
				if(resp.length() == 0){

					isHexHeader=termcfg.getIsHexHeader();

					if(atoi(isHexHeader.c_str())!=1)
					{
						memcpy(bcd_len,incoming_data_buffer,2);
						len = UtilsStringToInt(UtilsBcdToString((const char *)bcd_len, 2));
					}
					else
						len = (incoming_data_buffer[0] << 8) + incoming_data_buffer[1];  // for Hex

					log.message(MSG_INFO "Expecting "+UtilsIntToString(len)+" bytes\n");
				}

				resp.append(incoming_data_buffer, bytes_received);
				log.message(MSG_INFO "Received ("+UtilsIntToString(bytes_received)+" bytes) ["+UtilsHexToString(incoming_data_buffer, bytes_received)+"]\r\n");
			}else if(bytes_received <= 0){

				switch (SSL_get_error(g_SSL, bytes_received))
				{
					case SSL_ERROR_NONE:
					case SSL_ERROR_WANT_READ:
					case SSL_ERROR_WANT_WRITE:
						continue;
//			iq_audi_18072017, cause when SSL connection established but host is not connected/disconnect
					case SSL_ERROR_SYSCALL:
						isSslErrorSyscall=true;
						log.message(MSG_INFO "SSL_ERROR_SYSCALL \n");
						return 0;
					case SSL_ERROR_ZERO_RETURN:
					default:
						log.message(MSG_INFO "Remote side disconnected\n");
						return 0;
				}

			}
		}
	}
	else{

		while(1){
			bytes_received = recv(socketfd, incoming_data_buffer,sizeof(incoming_data_buffer), MSG_DONTWAIT);

			// Check the timeout value
			ftime(&current_tp);
	//		if(COMMS_RX_TIMEOUT <= current_tp.time - start_tp.time)
			if(secs <= current_tp.time - start_tp.time)
				return 0;

			if((bytes_received == -1) && ((errno == EAGAIN) || (errno == EWOULDBLOCK))){
				 if(resp.length() == 0)
					 continue;
				 else if(len <= (int)resp.length()-2){
					log.message(MSG_INFO "Received complete\n");
					break;
				}
			 }

			if(bytes_received == 0){
				log.message(MSG_INFO "Remote side disconnected\n");
				return 0;
			}else if(bytes_received > 0){
				if(resp.length() == 0){

					isHexHeader=termcfg.getIsHexHeader();

					if(atoi(isHexHeader.c_str())!=1)
					{
						memcpy(bcd_len,incoming_data_buffer,2);
						len = UtilsStringToInt(UtilsBcdToString((const char *)bcd_len, 2));
					}
					else
						len = (incoming_data_buffer[0] << 8) + incoming_data_buffer[1];  // for Hex

					log.message(MSG_INFO "Expecting "+UtilsIntToString(len)+" bytes\n");
				}

				resp.append(incoming_data_buffer, bytes_received);
				log.message(MSG_INFO "Received ("+UtilsIntToString(bytes_received)+" bytes) ["+UtilsHexToString(incoming_data_buffer, bytes_received)+"]\r\n");
			}
		}
	}

	response = resp;
	return response.length();
}
#endif

pthread_t tVersion;

void *update_versions(void*)
{
	com_verifone_versions::CVersions versions;
	com_verifone_versionsfile::CVersionsFile vfile;

	log.message(MSG_INFO "COMMS start writing versions\r\n");

	vfile.setName("");
	vfile.setVersion(COMMS_VERSION);

	// Keep trying every second, until we succeed
	while(1){
		if(versions.addFile(COMMS_NAME, vfile) == DB_OK)
			break;

		sleep(1);
	}

	log.message(MSG_INFO "COMMS versions complete\r\n");

	return NULL;
}

long PostConnectionCheck (SSL *ssl, char *host)
{
    X509      *cert = NULL;
    int       ok = 0;
	char*	  svrCertStr = NULL;

    GENERAL_NAMES *subjectAltNames = NULL;
    int numalts;
    int i;
    const GENERAL_NAME *pName = NULL;
	long retVal;

    /* Checking the return from SSL_get_peer_certificate here is not strictly
     * necessary.  With our example programs, it is not possible for it to return
     * NULL.  However, it is good form to check the return since it can return NULL
     * if the examples are modified to enable anonymous ciphers or for the server
     * to not require a client certificate.
     */

	// Checking PEER CERTIFICATE
    if(ssl != NULL)
    {
	  cert = SSL_get_peer_certificate (ssl);
	  if (!cert)
	  {
        if (cert)
			X509_free(cert);

		return X509_V_ERR_APPLICATION_VERIFICATION;
	  }
    }
	else
	{
	  log.message(MSG_INFO "SSL is NULL \r\n");
	  return X509_V_ERR_APPLICATION_VERIFICATION;
	}

	if (cert != NULL) {
		log.message(MSG_INFO "SERVER CERTIFICATE\r\n");
		log.message(MSG_INFO "==============================================\r\n");

		svrCertStr = X509_NAME_oneline (X509_get_subject_name (cert), 0, 0);
		log.message(MSG_INFO "Subject: "+SSTR(svrCertStr)+"\r\n");
		free (svrCertStr);

		svrCertStr = X509_NAME_oneline (X509_get_issuer_name (cert), 0, 0);
		log.message(MSG_INFO "Issuer: "+SSTR(svrCertStr)+"\r\n");
		free (svrCertStr);

		log.message(MSG_INFO "==============================================\r\n");
	}
	else
	{
	  log.message(MSG_INFO "Unable to get Certificate! \r\n");
	  return X509_V_ERR_APPLICATION_VERIFICATION;
	}

	 subjectAltNames = ( GENERAL_NAMES* ) X509_get_ext_d2i ( cert, NID_subject_alt_name, NULL, NULL );
     if ( subjectAltNames != NULL )
     {
        // get amount of alternatives, RFC2459 claims there MUST be at least one, but we don't depend on it...
        numalts = sk_GENERAL_NAME_num ( subjectAltNames );
        log.message(MSG_INFO "<inGetAltName> Num Alt Names: "+SSTR(numalts)+"\r\n");

        // loop through all alternative names
        for ( i=0; i < numalts; i++)
        {
            /* get a handle to alternative name number i */
            pName = sk_GENERAL_NAME_value (subjectAltNames, i );
			if(pName == NULL)
				continue;
            switch ( pName->type )
            {
                case GEN_DNS:
					if(pName->d.dNSName->data != NULL)
					{
					 log.message(MSG_INFO "inGetAltName> GEN_DNS: "+SSTR( pName->d.dNSName->data)+"\r\n");
					  if(!strcmp((const char *)pName->d.dNSName->data,host))
					  {
					 	ok=1;
					  }
                      break;
					}
            }

			if(ok ==1)
			{
				break;
			}
        }
		// need to free general names stack.
     	sk_GENERAL_NAME_pop_free(subjectAltNames, GENERAL_NAME_free);
     }
    X509_free(cert);
	retVal = SSL_get_verify_result(ssl);
	log.message(MSG_INFO "SSL_get_verify_result retVal: "+SSTR(retVal)+"\r\n");
    return retVal;
}

int sslHandShake(string ip)
{
	int retVal=-1;
	long g_err;
	struct timeb start_tp, current_tp;
	com_verifone_terminalconfig::CTerminalConfig termcfg;
	long secs = 10;

	ftime(&start_tp);

	SSL_set_fd(g_SSL,socketfd);

	//TRY SESSION RESUMPTION
	if (g_Session != NULL)
	{
		log.message(MSG_INFO "SSL Resuming Session \r\n");
		log.message(MSG_INFO "Session_id_length="+SSTR(g_Session->session_id_length)+" \r\n");
		 //LOG_HEX_PRINTF ("Session ID", g_Session->session_id, g_Session->session_id_length);
		 SSL_set_session(g_SSL, g_Session); /*A nd resume it*/
    }

	// 3 Do the SSL handshake
	while (-1 == retVal)  //this is b/c of non-blocking connection
	{
		// Check the timeout value
		ftime(&current_tp);

		if(secs <= current_tp.time - start_tp.time) //timeout
			break;

	   retVal = SSL_connect (g_SSL);

	   switch (SSL_get_error(g_SSL, retVal))
	   {
	   case SSL_ERROR_WANT_READ:
	   case SSL_ERROR_WANT_WRITE:
	      continue;
	   default:
		   break;
	   }
	}


	log.message(MSG_INFO "SSL_connect retVal: "+SSTR(retVal)+" \r\n");

	if(retVal == -1)
	{
		log.message(MSG_INFO "Connection Failed \r\n");

		if(g_SSL != NULL)
	    {
	      SSL_free (g_SSL);
		  g_SSL = NULL;
	    }

		return -1;
	}

	if ((g_err = PostConnectionCheck (g_SSL, (char *)ip.c_str())) != X509_V_OK)
	{
	   int i, err;
	   char  *pErrStr = NULL;

	    pErrStr = (char *)X509_verify_cert_error_string (g_err);
	    if(pErrStr != NULL)
	    {
	    	log.message(MSG_INFO "Error: PEER CERTIFICATE ["+SSTR(pErrStr)+"] \r\n");
	    }
		else
			log.message(MSG_INFO "No Error String from certificate! \r\n");

		for(i= 0; i < 5; i++)
        {
           err = SSL_shutdown (g_SSL);
	       if( err == 1)
	  	      break;
	       usleep(100000); //100 ms
	    }

		SSL_free (g_SSL);
		g_SSL = NULL;

		log.message(MSG_INFO "PEER CERT ERROR \r\n");
//		sleep(2);
		return -1;
	}


	return 0;
}

static void vdEnumerateCiphers(SSL *ssl)
{
  char *szSSLversion = NULL;

  int index = 0;
  const char *next = NULL;
  char buff[1024];

  szSSLversion = (char *) SSL_get_version(ssl);
  if(szSSLversion != NULL)
  {
	  log.message(MSG_INFO "SSL version="+SSTR(szSSLversion)+"\r\n");
  }

  log.message(MSG_INFO "Available Ciphers= \r\n");
  memset(buff, 0x00, sizeof(buff));
  do
  	{
      next = SSL_get_cipher_list(ssl,index);
      if (next != NULL)
	  {
	     if(index != 0)
		 	strcat(buff, ",");
		 if(strlen(buff) > 1000) //Just to avoid overflow
		 	break;
         strcat(buff, next);
		 index++;
    }
  }
  while (next != NULL);
  if(buff[0] != 0x00)
  {
	  log.message(MSG_INFO + SSTR(buff)+"\r\n");
  }
}

static void vdGetSignatureType(X509* cert)
{
 int signatureNid=0;
 if( (cert != NULL) && (cert->sig_alg != NULL) && (cert->sig_alg->algorithm != NULL) )
 {
	signatureNid = OBJ_obj2nid(cert->sig_alg->algorithm);
	switch(signatureNid)
	{
		case NID_md5WithRSAEncryption:
			log.message(MSG_INFO "Certificate Signature algorithm:md5WithRSAEncryption \r\n");
			break;

		case NID_sha1WithRSAEncryption:
			log.message(MSG_INFO "Certificate Signature algorithm:sha1WithRSAEncryption \r\n");
			break;

		case NID_sha224WithRSAEncryption:
			log.message(MSG_INFO "Certificate Signature algorithm:sha224WithRSAEncryption \r\n");
			break;

		case NID_sha256WithRSAEncryption:
			log.message(MSG_INFO "Certificate Signature algorithm:sha256WithRSAEncryption \r\n");
			break;

		case NID_sha384WithRSAEncryption:
			log.message(MSG_INFO "Certificate Signature algorithm:sha384WithRSAEncryption \r\n");
			break;

		case NID_sha512WithRSAEncryption:
			log.message(MSG_INFO "Certificate Signature algorithm:sha512WithRSAEncryption\r\n");
			break;
   }
 }
}



int VerifyCallBack (int ok, X509_STORE_CTX* store)
{
int   retVal;
long  cbCode;
char chCommName[128];
EVP_PKEY *certPublicKey=NULL;
int nRSAKeyLen=0;
ASN1_TIME *cert_time = NULL;
int signatureNid=0;



#ifdef  X509_STORE_CTX_SET_TIME_STORE_TEST
time_t checkTime;
struct tm stCheckTime;
#endif



#ifdef RAW_DATE_INFO
char szYYMMDDHHMMSSZ[14];
int len;
char *pDate;
#endif

	log.message(MSG_INFO "<VerifyCallBack> app_verify_cb( Code="+SSTR(ok)+" , CertDepth= "+SSTR(store->error_depth)+"\r\n");

//This code shows how to use  X509_STORE_CTX_set_time
#ifdef  X509_STORE_CTX_SET_TIME_STORE_TEST

       memset(&stCheckTime, 0x00, sizeof(stCheckTime));
	   stCheckTime.tm_sec = 0;	   // 0-61
	   stCheckTime.tm_min=0;	   //0-59
	   stCheckTime.tm_hour=12;    //0-23
	   stCheckTime.tm_mday=1;    //1-31
	   stCheckTime.tm_mon=9;	   //0-11
	   stCheckTime.tm_year=116;    //0-..years since 1900
	 //  stCheckTime.tm_wday;    //0-6, days since Sunday
	 //  stCheckTime.tm_yday;    //0-365, days since January 1
	 //  stCheckTime.tm_isdst;   //DST flag

	    checkTime =  mktime(&stCheckTime);

	    log.message(MSG_INFO "CHECK TIME= "+ SSTR(checkTime)+"\r\n");
//        printf("CHECK TIME=%ul\n", checkTime);

        X509_STORE_CTX_set_time(store, 0, checkTime);
#endif


	// ------------------------------------------------------
	// Print information about the certificate being verified
	// ------------------------------------------------------

	cbCode  = X509_STORE_CTX_get_error( store );

	 log.message(MSG_INFO "<VerifyCallBack>   Warning("+ SSTR(cbCode)+") :"+ SSTR(X509_verify_cert_error_string( cbCode ))+"\r\n");
	 log.message(MSG_INFO "<VerifyCallBack>  Chain="+ SSTR(sk_X509_num( store->chain ))+" Error= "+ SSTR(store->error_depth)+"\r\n");


	//Check signature of current certificate
	if( (store->current_cert != NULL) && (store->current_cert->sig_alg != NULL) && (store->current_cert->sig_alg->algorithm != NULL) )
	{
		signatureNid = OBJ_obj2nid(store->current_cert->sig_alg->algorithm);
		switch(signatureNid)
		{
			case NID_md5WithRSAEncryption:
				log.message(MSG_INFO "Certificate Signature algorithm:md5WithRSAEncryption\r\n");
				 break;

			case NID_sha1WithRSAEncryption:
				log.message(MSG_INFO "Certificate Signature algorithm:sha1WithRSAEncryption \r\n");
		    	 break;

			case NID_sha224WithRSAEncryption:
				log.message(MSG_INFO "Certificate Signature algorithm:sha224WithRSAEncryption \r\n");
 				 break;

			case NID_sha256WithRSAEncryption:
				log.message(MSG_INFO "Certificate Signature algorithm:sha256WithRSAEncryption \r\n");
				 break;

			case NID_sha384WithRSAEncryption:
				log.message(MSG_INFO "Certificate Signature algorithm:sha384WithRSAEncryption \r\n");
				 break;

			case NID_sha512WithRSAEncryption:
				log.message(MSG_INFO "Certificate Signature algorithm:sha512WithRSAEncryption \r\n");
				 break;
		}
	}

	memset( chCommName, 0x00, sizeof( chCommName ) );
	errno = 0;
	retVal = X509_NAME_get_text_by_NID(	X509_get_subject_name( store->current_cert ), NID_commonName, chCommName, sizeof( chCommName ) );
   // vdGetMoreCertDetail(store->current_cert);
	if((retVal > 0) && (chCommName != 0))
	{
		log.message(MSG_INFO "<VerifyCallBack>   CN= "+SSTR(chCommName) + "\r\n");
	}
	else
	{
		log.message(MSG_INFO "<VerifyCallBack> X509_NAME_get_text_by_NID ret="+SSTR(retVal)+" errno= " + SSTR(errno)+"\r\n");
		//Get AltName
//		inGetAltName (store->current_cert); //iqbal
	}


    cert_time = X509_get_notBefore( store->current_cert);
	if(cert_time != NULL)
	{
	  char szDateLine[100];

#ifdef RAW_DATE_INFO
	  len = ASN1_STRING_length(cert_time);
	  pDate = (char*)ASN1_STRING_data(cert_time);
	  strncpy(szYYMMDDHHMMSSZ,pDate, len)[len] = 0x00;
	  log.message(MSG_INFO "<VerifyCallBack>   notBefore ("+SSTR(szYYMMDDHHMMSSZ) + ") \r\n");
#endif
	  memset(szDateLine, 0x00, sizeof(szDateLine));
//	  vdGetTimeFromASN1(cert_time, szDateLine);
	  if(szDateLine[0] != 0x00)
	  {
		  log.message(MSG_INFO "<VerifyCallBack>   notBefore ("+SSTR(szDateLine) + ") \r\n");
	  }
	}

	cert_time = X509_get_notAfter( store->current_cert);
	if(cert_time != NULL)
	{
	  char szDateLine[100];
#ifdef RAW_DATE_INFO
	  len = ASN1_STRING_length(cert_time);
	  pDate = (char*)ASN1_STRING_data(cert_time);
	  strncpy(szYYMMDDHHMMSSZ,pDate, len)[len] = 0x00;
	  log.message(MSG_INFO "<VerifyCallBack>   notAfter ("+SSTR(szYYMMDDHHMMSSZ) + ") \r\n");
#endif
	  memset(szDateLine, 0x00, sizeof(szDateLine));
//	  vdGetTimeFromASN1(cert_time, szDateLine);  //iqbal
	  if(szDateLine[0] != 0x00)
	  {
	    log.message(MSG_INFO "<VerifyCallBack>   notAfter ("+SSTR(szDateLine) + ") \r\n");
	  }
	}

	certPublicKey = X509_get_pubkey(sk_X509_value(store->chain,store->error_depth));
	if(NULL != certPublicKey)
	{
		nRSAKeyLen = EVP_PKEY_bits(certPublicKey);

		 log.message(MSG_INFO "<VerifyCallBack>   Cert RSA key length of "+SSTR(nRSAKeyLen)+ " bits \r\n");
		 log.message(MSG_INFO "<VerifyCallBack>   Cert Type= "+SSTR(X509_certificate_type(store->current_cert, certPublicKey)) + " \r\n");
		 log.message(MSG_INFO "<VerifyCallBack>   Cert Version="+SSTR(X509_get_version(store->current_cert)) + " \r\n");
        vdGetSignatureType(store->current_cert);
        log.message(MSG_INFO "<VerifyCallBack>    Cert Serial No= "+SSTR(ASN1_INTEGER_get(X509_get_serialNumber(store->current_cert))) + " \r\n");
		EVP_PKEY_free(certPublicKey);
		certPublicKey=NULL;
	}
    X509_verify_cert_error_string((long) store->error);

	switch (store->error)
	{
			case X509_V_ERR_CERT_NOT_YET_VALID:
			case X509_V_ERR_CERT_HAS_EXPIRED:
				log.message(MSG_INFO "<VerifyCallBack> Certificate validity period check failed! \r\n");
//				printf(  "<VerifyCallBack> Certificate validity period check failed!\n");
				break;

			case X509_V_ERR_ERROR_IN_CERT_NOT_BEFORE_FIELD:
				log.message(MSG_INFO "<VerifyCallBack> Trust verification of issuer failed. Invalid Time format for not before field \r\n");
//				printf(  "<VerifyCallBack> Trust verification of issuer failed. Invalid Time format for not before field\n");
				break;

			case X509_V_ERR_ERROR_IN_CERT_NOT_AFTER_FIELD:
				log.message(MSG_INFO "<VerifyCallBack> Trust verification of issuer failed. Invalid Time format for not after field \r\n");
//				printf(  "<VerifyCallBack> Trust verification of issuer failed. Invalid Time format for not after field\n");
				break;

			case X509_V_ERR_UNABLE_TO_VERIFY_LEAF_SIGNATURE:
			case X509_V_ERR_INVALID_CA:
			case X509_V_ERR_INVALID_PURPOSE:
			case X509_V_ERR_CERT_UNTRUSTED:
			case X509_V_ERR_CERT_REJECTED:
			case X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT_LOCALLY:
			case X509_V_ERR_CRL_SIGNATURE_FAILURE:
			case X509_V_ERR_CERT_REVOKED:
			case X509_V_ERR_UNABLE_TO_GET_CRL:
			case X509_V_ERR_CRL_NOT_YET_VALID:
			case X509_V_ERR_CRL_HAS_EXPIRED:
			case X509_V_ERR_UNABLE_TO_GET_CRL_ISSUER:
			case X509_V_ERR_UNABLE_TO_DECRYPT_CRL_SIGNATURE:
			case X509_V_ERR_ERROR_IN_CRL_LAST_UPDATE_FIELD:
			case X509_V_ERR_ERROR_IN_CRL_NEXT_UPDATE_FIELD:
				log.message(MSG_INFO "<VerifyCallBack> Trust Verification of issuer certificate failed!, error is "+ SSTR(store->error)+" \r\n");
//				printf(  "<VerifyCallBack> Trust Verification of issuer certificate failed!, error is %d\n",store->error);
				break;

			case X509_V_ERR_DEPTH_ZERO_SELF_SIGNED_CERT:
				log.message(MSG_INFO "<VerifyCallBack> Self Signed certificate \r\n");
//				printf(  "<VerifyCallBack> Self Signed certificate\n");
				break;

			case X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT:
				log.message(MSG_INFO "<VerifyCallBack> Trust verification of issuer certificate failed! \r\n");
//				printf(  "<VerifyCallBack> Trust verification of issuer certificate failed!\n");
				break;
		}//end swi

	log.message(MSG_INFO "<VerifyCallBack> returning "+ SSTR(ok)+ "\r\n");
	log.message(MSG_INFO "<VerifyCallBack> -- END \r\n");

//  	printf(  "<VerifyCallBack>   returning %d\n", ok );
//    printf(  "<VerifyCallBack> -- END\n" );

//    if(gPolicy99 == 1)
//    {
//        printf("Policy99 returning in callback\n");
//		ok = 1;
//    }
	return ok;
}



void infoCallBack (const SSL *s, int where, int ret)
{
const char *str;
int w;

    w = where & ~SSL_ST_MASK;

	if (w & SSL_CB_HANDSHAKE_DONE)
		str = "SSL_handshake_done";
	else if (w & SSL_ST_CONNECT)
        str = "SSL_connect";
    else
		if (w & SSL_ST_ACCEPT)
        	str = "SSL_accept";
    	else
        	str = "undefined";

    if (where & SSL_CB_LOOP)
	{
    	log.message(MSG_INFO+SSTR(str)+"  "+SSTR(SSL_state_string_long(s))+ "\r\n");
    }
	else if (where & SSL_CB_ALERT)
	{
       	str = (where & SSL_CB_READ) ? "read" : "write";

       	log.message(MSG_INFO+SSTR(str)+"SSL3 alert : "+SSTR(SSL_alert_type_string_long(ret))+": "+ SSTR(SSL_alert_desc_string_long(ret))+"\r\n");
	}
	else if (where & SSL_CB_EXIT)
	{
		if (ret == 0)
	    {
			log.message(MSG_INFO+SSTR(str)+": failed in "+SSTR(SSL_state_string_long(s))+ "\r\n");
	    }
	    else if (ret < 0)
		{
	    	log.message(MSG_INFO+SSTR(str)+": error in "+SSTR(SSL_state_string_long(s))+ "\r\n");
	    }
	}
}


void initOpenSSL(void)
{
	if(!SSL_library_init())
	{
		log.message(MSG_INFO "Failed to initialize OpenSSL\r\n");
		return;
	}

#ifdef ADD_SHA256
	EVP_add_digest(EVP_sha224());
	EVP_add_digest(EVP_sha256());
	EVP_add_digest(EVP_sha384());
	EVP_add_digest(EVP_sha512());
#endif

	SSL_load_error_strings();

	if(g_CTX!=NULL)
	{
		SSL_CTX_set_info_callback(g_CTX, NULL);
		SSL_CTX_free(g_CTX);
		g_CTX=NULL;
		log.message(MSG_INFO "Freeing CTX   \r\n");
	}

}

#define DEFAULT_CACERT "./flash/net/MEELcrt.pem"

SSL_CTX* setup_client_ctx (void)
{
	SSL_CTX* ctx;
	// NOTE: SSLv23_method problem in linking for now
    //ctx = SSL_CTX_new (TLSv1_client_method());
	ctx = SSL_CTX_new (SSLv23_method ());

	log.message(MSG_INFO "SSL_CTX_new"+ SSTR(ctx) + "\r\n");

	SSL_CTX_set_options(ctx,SSL_OP_NO_SSLv2); //Devadas Work-around to work with latest SSL servers

    /* Auto retry read/write when renegotitation is requested.
     * This only makes sense when the underlying socket is blocking.
     */

#if 0
    SSL_CTX_set_mode(ctx, SSL_MODE_AUTO_RETRY);
#endif
	// Load our CA
	if (SSL_CTX_load_verify_locations(ctx, DEFAULT_CACERT, NULL) != 1)  //Jorge_M1 How to load files ???
		log.message(MSG_INFO "Error loading CA file and/or directory \r\n");

    if (SSL_CTX_set_default_verify_paths(ctx) != 1)
    	log.message(MSG_INFO "Error loading CA default file and/or directory \r\n");

    SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER, VerifyCallBack);
    SSL_CTX_set_verify_depth(ctx, 2);

    log.message(MSG_INFO "setup_client_ctx SUCCESSFULL \r\n");
    return ctx;
}

void initSSL(void)
{
	log.message(MSG_INFO "initSSL ..... \r\n");
	initOpenSSL();
	g_CTX=setup_client_ctx();

	if(!g_CTX)
	{
		log.message(MSG_INFO "Failed to Set CTX \r\n");
		return;
	}

	SSL_CTX_set_info_callback(g_CTX, infoCallBack);

}

int main(int argc, char *argv[])
{
	(void)argc;			// Suppresses -> warning: unused parameter
	(void)argv;			// Suppresses -> warning: unused parameter

	log.message(MSG_INFO "Comms starting...\r\n");

//	pthread_create(&tVersion,NULL,&update_versions,NULL);

	signal(SIGSEGV, fault_handler);

	com_verifone_ipc::init(string(COMMS_TASK));

	string ipcBuffer;
	string ipcFrom;

	CTerminalConfig termcfg;

	if(atoi(termcfg.getSSLEnable().c_str())==1)
	{
		SSL_ENABLE=true;

		//SSL Initialization iq_audi_14042017
		initSSL();
	}


	while(1){
		if(com_verifone_ipc::receive(ipcBuffer,string(""),ipcFrom)==com_verifone_ipc::IPC_SUCCESS){
			string tmpl;

			string recv;

			log.message(MSG_INFO "Message from "+ipcFrom+" ("+UtilsIntToString(ipcBuffer.length())+" bytes) ["+UtilsHexToString(ipcBuffer.c_str(), ipcBuffer.length())+"]\r\n");

			if(ipcBuffer.compare(0, 8, "Connect:") == 0){
				log.message(MSG_INFO "Connect from ["+ipcFrom+"]\r\n");

				// Extract ip from buffer
				size_t ipendpos = ipcBuffer.find_first_of(':', 8);
				string ip = ipcBuffer.substr(8, ipendpos-8);

				// Extract port from buffer
				string port = ipcBuffer.substr(ipendpos+1);

				int iret = connect(ip, port);

				if(iret == 0)
					com_verifone_ipc::send("Ok",ipcFrom);
				else
					com_verifone_ipc::send("Error",ipcFrom);

			}else if(ipcBuffer.compare(0, 10, "Disconnect") == 0){
				log.message(MSG_INFO "Disonnect from ["+ipcFrom+"]\r\n");

				if(disconnect() == 0)
					com_verifone_ipc::send("Ok",ipcFrom);
				else
					com_verifone_ipc::send("Error",ipcFrom);

			}else if(ipcBuffer.compare(0, 5, "Data:") == 0){
				log.message(MSG_INFO "Data from ["+ipcFrom+"]\r\n");

				if(send(ipcBuffer.substr(5, ipcBuffer.length()-5)) == (int)ipcBuffer.length()-5)
					com_verifone_ipc::send("Ok",ipcFrom);
				else
					com_verifone_ipc::send("Error",ipcFrom);

			}else if(ipcBuffer.compare(0, 3, "Rx:") == 0){
				log.message(MSG_INFO "Data from ["+ipcFrom+"]\r\n");

				string incoming;
				if(receive(incoming) > 0)
					com_verifone_ipc::send("Ok:"+incoming,ipcFrom);
				else
					com_verifone_ipc::send("Error",ipcFrom);
			}
		}

		usleep(100000);		// Sleep for 100 milli seconds
	}

	return 0;
}

void fault_handler(int signal)
{
	void *array[10];
	size_t size;

	switch(signal)
	{
		case 1: fprintf(stderr, "Error: SIGHUP	1	Hangup (POSIX)\n"); break;
		case 2: fprintf(stderr, "Error: SIGINT	2	Terminal interrupt (ANSI)\n"); break;
		case 3: fprintf(stderr, "Error: SIGQUIT	3	Terminal quit (POSIX)\n"); break;
		case 4: fprintf(stderr, "Error: SIGILL	4	Illegal instruction (ANSI)\n"); break;
		case 5: fprintf(stderr, "Error: SIGTRAP	5	Trace trap (POSIX)\n"); break;
		case 6: fprintf(stderr, "Error: SIGIOT	6	IOT Trap (4.2 BSD)\n"); break;
		case 7: fprintf(stderr, "Error: SIGBUS	7	BUS error (4.2 BSD)\n"); break;
		case 8: fprintf(stderr, "Error: SIGFPE	8	Floating point exception (ANSI)\n"); break;
		case 9: fprintf(stderr, "Error: SIGKILL	9	Kill(can't be caught or ignored) (POSIX)\n"); break;
		case 10: fprintf(stderr, "Error: SIGUSR1	10	User defined signal 1 (POSIX)\n"); break;
		case 11: fprintf(stderr, "Error: SIGSEGV	11	Invalid memory segment access (ANSI)\n"); break;
		case 12: fprintf(stderr, "Error: SIGUSR2	12	User defined signal 2 (POSIX)\n"); break;
		case 13: fprintf(stderr, "Error: SIGPIPE	13	Write on a pipe with no reader, Broken pipe (POSIX)\n"); break;
		case 14: fprintf(stderr, "Error: SIGALRM	14	Alarm clock (POSIX)\n"); break;
		case 15: fprintf(stderr, "Error: SIGTERM	15	Termination (ANSI)\n"); break;
		case 16: fprintf(stderr, "Error: SIGSTKFLT	16	Stack fault\n"); break;
		case 17: fprintf(stderr, "Error: SIGCHLD	17	Child process has stopped or exited, changed (POSIX)\n"); break;
		case 18: fprintf(stderr, "Error: SIGCONT	18	 Continue executing, if stopped (POSIX)\n"); break;
		case 19: fprintf(stderr, "Error: SIGSTOP	19	Stop executing(can't be caught or ignored) (POSIX)\n"); break;
		case 20: fprintf(stderr, "Error: SIGTSTP	20	Terminal stop signal (POSIX)\n"); break;
		case 21: fprintf(stderr, "Error: SIGTTIN	21	Background process trying to read, from TTY (POSIX)\n"); break;
		case 22: fprintf(stderr, "Error: SIGTTOU	22	Background process trying to write, to TTY (POSIX)\n"); break;
		case 23: fprintf(stderr, "Error: SIGURG	23	Urgent condition on socket (4.2 BSD)\n"); break;
		case 24: fprintf(stderr, "Error: SIGXCPU	24	CPU limit exceeded (4.2 BSD)\n"); break;
		case 25: fprintf(stderr, "Error: SIGXFSZ	25	File size limit exceeded (4.2 BSD)\n"); break;
		case 26: fprintf(stderr, "Error: SIGVTALRM	26	Virtual alarm clock (4.2 BSD)\n"); break;
		case 27: fprintf(stderr, "Error: SIGPROF	27	Profiling alarm clock (4.2 BSD)\n"); break;
		case 28: fprintf(stderr, "Error: SIGWINCH	28	Window size change (4.3 BSD, Sun)\n"); break;
		case 29: fprintf(stderr, "Error: SIGIO	29	I/O now possible (4.2 BSD)\n"); break;
		case 30: fprintf(stderr, "Error: SIGPWR	30	Power failure restart (System V)\n"); break;
	}

	// get void*'s for all entries on the stack
	size = backtrace(array, 10);

	// print out all the frames to stderr
	backtrace_symbols_fd(array, size, STDERR_FILENO);
	exit(1);
}

string sanitiseIP(string ip)
{

	string sanitisedIP;
	unsigned int curpos=0, prevpos=0;

	for (int i=0; i<4; i++){
		curpos = ip.find('.', prevpos);

		string octet = ip.substr(prevpos, curpos-prevpos);
		cout << "octet:"<< octet << endl;

		sanitisedIP.append(UtilsIntToString(UtilsStringToInt(octet)));

		if(curpos == string::npos)
			break;

		sanitisedIP.append(".");
		prevpos = curpos + 1;
	}

	return sanitisedIP;
}
