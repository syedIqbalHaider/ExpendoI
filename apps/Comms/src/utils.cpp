#include <cstring>
#include <sstream>
#include <ctime>
#include <sys/types.h>
#include <sys/timeb.h>
#include "utils.h"

namespace Utils
{

string UtilsBcdToString(const char *data, unsigned int len)
{
	char c;
	stringstream ss;

	for (uint i=0; i<len; i++) {
		if(((data[i] >> 4) & 0x0f) == 0x0f) break;

		c = ((data[i] >> 4) & 0x0f) + '0';
		if(c > '9') break;
		ss << c;

		if((data[i] & 0x0f) == 0x0f) break;

		c = (data[i] & 0x0f) + '0';
		if(c > '9') break;
		ss << c;
	}

	return ss.str();
}


int UtilStringToBCD(string str, char *bcd)
{
	int pos=0, count=0;

	for(string::iterator str_it=str.begin(); str_it!=str.end(); ++str_it){
		char c = *str_it;

		if(!((c >= '0') && (c <= '9')))
			return 0;

		if(count%2==0)
			bcd[pos] = ((char)(c - '0') << 4) & 0xf0;
		else{
			bcd[pos] |= (char)(c - '0');
			pos++;
		}

		count++;
	}

	return pos;
}

int UtilsStringToInt(string str)
{
	int numb;
	istringstream ( str ) >> numb;
	return numb;
}

string UtilsIntToString(int int_val)
{
	stringstream ss;
	ss << int_val;
	return ss.str();
}

string UtilsHexToString(const char *data, unsigned int len)
{
	std::stringstream ss;

	ss << std::hex;
	for (size_t i = 0; len > i; ++i) {
			if(static_cast<unsigned int>(static_cast<unsigned char>(data[i])) <= 0x0f)
				ss << '0';
			ss << static_cast<unsigned int>(static_cast<unsigned char>(data[i]));
	}

	return ss.str();
}

string UtilsBoolToString(bool b)
{
	std::stringstream converter;
	converter << b;
	return converter.str();
}

string UtilsTimestamp()
{
	struct timeb tp;
	ftime(&tp);
	char buf[16];

	memset(buf, 0, sizeof(buf));
	strftime(buf,sizeof(buf),"%Y%m%d%H%M%S%u",localtime(&tp.time));

	return string(buf);
}

}
