#include <libda/cterminalconfig.h>
#include <string.h>
#include "Advice.h"
#include "CLog.h"
#include "TermApp_8583.h"
#include "utils.h"
#include "TermAppSession.h"
#include "HIMISOver.h"
#include "bertlv.h"
#include "values.h"
#include "libda/creceiptno.h"
#include <libda/cbatchmanager.h>
#include <libda/cconmanager.h>
#include <libda/cbatch.h>
#include <libda/ctsn.h>

static CLog log(HIMISO_NAME);

using namespace Utils;
using namespace com_verifone_terminalconfig;
using namespace za_co_verifone_bertlv;
using namespace com_verifone_receiptno;
using namespace com_verifone_tsn;

Advice::Advice(CBatchRec BatchRec):Message("Advice", BatchRec)
{
	int ret;
	com_verifone_batchmanager::CBatchManager batchManager;
	com_verifone_batch::CBatch cbatch;

	ret = batchManager.getBatch(cbatch);

	if (ret != DB_OK){
		log.message(MSG_INFO "Failure retrieving batch record\r\n");
		return;
	}

	batch_no = cbatch.getBatchNo();

}

int Advice::DoAdvice(void)
{
	unsigned char pan[37+1];
	unsigned int pan_len=0;
	char buffer[100];
	ISO_HANDLE isoh, isoh_resp;
	com_verifone_terminalconfig::CTerminalConfig termcfg;

	isoh = IsoCreateList(MTI_1220);

	// Mandatory fields

	int itx_type = UtilsStringToInt(batchrec.getTxCode());
	int iacc_type = UtilsStringToInt(batchrec.getAccType());
	int ito_acc=0;	// TODO: Where does this come from?

	PanEntryMode entry_mode=(PanEntryMode)batchrec.getPanEntryMode();
	//printf("Pan entry mode = %d\r\n",(int)batchrec.getPanEntryMode());

	// Processing code
	if ((itx_type == TX_ISO_REFUND) || (itx_type == TX_ISO_DEPOSIT) || (itx_type == TX_ISO_CORRECTION) || (itx_type == TX_ISO_REFUND) || (itx_type == TX_ISO_PMNT_DEPOSIT))
		sprintf(buffer, "%02X%02d%02d", (int)itx_type, (int)iacc_type, (int)ito_acc);
	else if (itx_type == TX_ISO_TRANSFER)
		sprintf(buffer, "%02X%02d%02d", (int)itx_type, (int)iacc_type, (int)ito_acc);
	else
		sprintf(buffer, "%02X%02d%02d", (int)itx_type, (int)iacc_type, (int)ito_acc);

	IsoAddField(isoh, Processing_code, AddFieldNotApplicable, (unsigned char*)buffer, 6);																// 3

	if(batchrec.getTxAmount()!=0.00) //iq_audi_231116
	{
		char stx_amount[12+1]={0};

		sprintf(stx_amount, "%012ld",batchrec.getTxAmount());
		IsoAddField(isoh, Amount_transaction, AddFieldNotApplicable, (unsigned char*) stx_amount, 12);
	}

//	iq_audi_231116
	string datetime = batchrec.getTxDateTime();
	IsoAddField(isoh, Date_and_time_local_transaction, AddFieldNotApplicable, (unsigned char*) datetime.substr(8, 12).c_str(), 6);	// 12
	IsoAddField(isoh, Date_local_transaction, AddFieldNotApplicable, (unsigned char*) datetime.substr(4, 8).c_str(), 4);	//13

	string str_tsn = zero_pad(batchrec.getTsn(), 6);
	IsoAddField(isoh, System_trace_audit_number, AddFieldNotApplicable, (unsigned char*)str_tsn.c_str(), str_tsn.length());								// 11

	string card_input_mode;
	card_input_mode.clear();

	if ((entry_mode == PEM_SWIPED)  ) {
		card_input_mode.append("02");
	} else if (entry_mode == PEM_ENTERED) {
		card_input_mode.append("01");
	} else if (entry_mode == PEM_INSERTED) {
		card_input_mode.append("05");
	} else if (entry_mode == PEM_TAPPED) {
		card_input_mode.append("07");
	} else if (entry_mode == PEM_FALLBACK) {
		card_input_mode.append("80");
	}

	CARD_AUTH_METHOD cardhAuthMethod=(CARD_AUTH_METHOD)batchrec.getCardhAuthMethod();

	if(cardhAuthMethod==PIN)
		card_input_mode.append("1");
	else
		card_input_mode.append("0");

	IsoAddField(isoh, POS_data_code, AddFieldNotApplicable, (unsigned char*) card_input_mode.c_str(), 3); //22


//	iq_audi_291116 comment field
//	string cbmp = termcfg.getEmvCapabilityBitMap();
//	IsoAddField(isoh, POS_data_code, AddFieldNotApplicable, (unsigned char*) cbmp.substr(0, 15).c_str(), 15);											// 22

//	iq_audi_291116
	string str_tpdu = termcfg.getTpdu();

//	if(batchrec.getDraftCapMode() == ONLINE)
//		IsoAddField(isoh, Function_code, AddFieldNotApplicable, (unsigned char*)str_tpdu.substr(3, 6).c_str(), 3);															// 24
//	else
	IsoAddField(isoh, Function_code, AddFieldNotApplicable, (unsigned char*)str_tpdu.substr(3, 6).c_str(), 3);															// 24

	IsoAddField(isoh, Message_reason_code, AddFieldNotApplicable, (unsigned char*)"00", 2);		// 25

//	iq_audi_291116 comment field
//	com_verifone_batchmanager::CBatchManager batchManager;
//	com_verifone_batch::CBatch cbatch;
//
//	int ret = batchManager.getBatch(cbatch);
//
//	if (ret != 1){
//		log.message(MSG_INFO ":Failure retrieving batch number\r\n");
//		return -1;
//	}
//	string str_batchno = UtilsIntToString(cbatch.getBatchNo());
//	IsoAddField(isoh, Reconciliation_indicator, AddFieldNotApplicable, (unsigned char*) str_batchno.c_str(), str_batchno.length());						// 29

	if ((entry_mode == PEM_SWIPED)  || (entry_mode == PEM_FALLBACK)){
//		iq_audi_241116
		string track2=batchrec.getTrack2();
		uint pos = track2.find('=', 0);
		track2.replace(pos, 1, 1, 'D');
		memset(pan,0,sizeof(pan));
		pan_len=track2.length();
		memcpy(pan,track2.c_str(),pan_len);

		IsoAddField(isoh, Track_2_data, AddFieldNotApplicable, (unsigned char*)pan,pan_len);

		string strPan;
		strPan=track2.substr(0,track2.find("D"));
		pan_len=0;
		pan_len=strPan.length();

		if(pan_len%2)
		{
			strcat((char *)pan,"F");
			pan_len++;
		}

		IsoAddField(isoh, Primary_account_number, AddFieldNotApplicable, (unsigned char*)strPan.c_str(), pan_len);	// 2

		string expiry;
		expiry = track2.substr(pos+1, 4);
		IsoAddField(isoh, Date_expiration, AddFieldNotApplicable, (unsigned char*)expiry.c_str(), 4);		// 14
	}

	if (entry_mode == PEM_INSERTED){
		string emv_track2;
		BuildEmvTrack2(batchrec.getEmvTags(), emv_track2);

//		iq_audi_241116
		uint pos = emv_track2.find('=', 0);
		emv_track2.replace(pos, 1, 1, 'D');
		memset(pan,0,sizeof(pan));
		pan_len=emv_track2.length();
		memcpy(pan,emv_track2.c_str(),pan_len);

		IsoAddField(isoh, Track_2_data, AddFieldNotApplicable, (unsigned char*)pan,pan_len);				// 35
		batchrec.setPan(emv_track2.substr(0,emv_track2.find("D")));

//		iq_audi_241116
		memset(pan,0,sizeof(pan));
		pan_len=0;
		pan_len=batchrec.getPan().length();
		memcpy(pan,batchrec.getPan().c_str(),pan_len);

		if(pan_len%2)
		{
			strcat((char *)pan,"F");
			pan_len++;
		}
		IsoAddField(isoh, Primary_account_number, AddFieldNotApplicable, (unsigned char*)pan, pan_len);		// 2

		string expiry;
		expiry = emv_track2.substr(pos+1, 4);
		IsoAddField(isoh, Date_expiration, AddFieldNotApplicable, (unsigned char*)expiry.c_str(), 4);		// 14
	}

	string rrn = batchrec.getRrn();
	if(rrn.length())
		IsoAddField(isoh, Retrieval_reference_number, AddFieldNotApplicable, (unsigned char*)rrn.c_str(), rrn.length());								// 37

	string authcode = batchrec.getAuthCode();
	if(authcode.length())
		IsoAddField(isoh, Approval_code, AddFieldNotApplicable, (unsigned char*) authcode.c_str(), authcode.length());									// 38

	IsoAddField(isoh, Action_code, AddFieldNotApplicable, (unsigned char*) "00", 2);																	// 39

	string str_tid = space_pad(termcfg.getTerminalNo(), 8);
	IsoAddField(isoh, Terminal_ID, AddFieldNotApplicable, (unsigned char*)str_tid.c_str(), str_tid.length());											// 41

	string str_merchno;
	str_merchno.insert(0, 15, '0');
	str_merchno.replace(15-termcfg.getMerchantNo().length(), termcfg.getMerchantNo().length(), termcfg.getMerchantNo());
	IsoAddField(isoh, Card_acceptor_ID, AddFieldNotApplicable, (unsigned char*)str_merchno.c_str(), str_merchno.length());								// 42

	string f49enable;
	f49enable=termcfg.getF49Enable();
	if(atoi(f49enable.c_str())==1)
		IsoAddField(isoh, Currency_code_transaction, AddFieldNotApplicable, (unsigned char*)batchrec.getTransCurrency().c_str(), 3);														// 49

//	iq_audi_291116 comment field
//	string str_posdata = space_fill(19);
//	str_posdata.replace(0, str_tid.length(), str_tid);
//	str_posdata.replace(8, str_tsn.length(), str_tsn);
//	str_posdata.replace(14, 5, "00000");
//	IsoAddField(isoh, Additional_data_private, POS_data, (unsigned char*)str_posdata.c_str(), 19);														// 48.1
//
//	IsoAddField(isoh, Additional_data_private, Authorisation_profile, (unsigned char*)"12", 2);															// 48.2
//	IsoAddField(isoh, Currency_code_transaction, AddFieldNotApplicable, (unsigned char*)"710", 3);														// 49

/*
	//
	//
	// Optional/Conditional fields
	//
	// add the invoice number
	if(strlen((char*) tx_rec->invoiceno) != 0){
		IsoAddField(isoh, Additional_data_private, Additional_node_data, (unsigned char*) tx_rec->invoiceno, strlen((char*) tx_rec->invoiceno)); // 48.5
	}

	// Manual card entry
	if (tx_rec->pan_entr_mode == CARD_ENTRY_MANUAL) {
		char cExpiryDate[4];

		IsoAddField(isoh, Primary_account_number, AddFieldNotApplicable, (unsigned char*) tx_rec->pan, strlen((char*) tx_rec->pan)); // 2
		cExpiryDate[0] = tx_rec->exp[2];
		cExpiryDate[1] = tx_rec->exp[3];
		cExpiryDate[2] = tx_rec->exp[0];
		cExpiryDate[3] = tx_rec->exp[1];
		IsoAddField(isoh, Date_expiration, AddFieldNotApplicable, (unsigned char*) cExpiryDate, 4); // 14
		//IsoAddField(isoh, Date_expiration, AddFieldNotApplicable, (unsigned char*) tx_rec->exp, 4); // 14
		//IsoAddField(isoh, Card_sequence_number, AddFieldNotApplicable, (unsigned char*) tx_rec->src, 3); // 23
		//sprintf(buffer," %s",tx_rec->CVV);
		IsoAddField(isoh, Additional_data_private, Card_verification_data, (unsigned char*) tx_rec->CVV, 4); // 48.3
	} else if ((tx_rec->pan_entr_mode == CARD_ENTRY_SWIPE) || (tx_rec->pan_entr_mode==CARD_ENTRY_FALLBACK)|| (tx_rec->pan_entr_mode == CARD_ENTRY_CONTACTLESS_MAG)) {
		IsoAddField(isoh, Track_2_data, AddFieldNotApplicable, (unsigned char*) tx_rec->track2, strlen((char*) tx_rec->track2));// 35
	} else if (tx_rec->pan_entr_mode == CARD_ENTRY_EMV || tx_rec->pan_entr_mode == CARD_ENTRY_CONTACTLESS_ICC) {
		IsoAddField(isoh, Primary_account_number, AddFieldNotApplicable, (unsigned char*) tx_rec->pan, strlen((char*) tx_rec->pan)); // 2
		IsoAddField(isoh, Track_2_data, AddFieldNotApplicable, (unsigned char*) tx_rec->track2, strlen((char*) tx_rec->track2));// 35
	}
*/
	if (batchrec.getCashAmount() > 0) {
		sprintf(buffer, "%02d40710C%012d", (unsigned int)UtilsStringToInt(batchrec.getAccType()), (unsigned int)batchrec.getCashAmount());
		IsoAddField(isoh, Additional_amounts, AddFieldNotApplicable, (unsigned char*) buffer, strlen(buffer));											// 54
	}

//		iq_audi_291116 comment field
//	string original_data;
//	if(batchrec.getDraftCapMode() == ONLINE)
//		original_data = "1200";
//	else
//		original_data = "1100";
//
//	original_data.append(zero_pad(batchrec.getTsn(), 6));
//
//	datetime = batchrec.getTxDateTime();
//	original_data.append(datetime.substr(4, 10));
//	original_data.append(11, '0');
//
//	IsoAddField(isoh, Original_data_elements, AddFieldNotApplicable, (unsigned char*) original_data.c_str(), original_data.length()); 					// 56

	if(entry_mode == PEM_INSERTED){
//		iq_audi_291116 comment field
//		IsoAddField(isoh, Card_sequence_number, AddFieldNotApplicable, (unsigned char*)"000", 3);														// 23

		string emv_tags = batchrec.getEmvTags();
		FilterEMVTags(batchrec.getEmvTags(), emv_tags);

		string sicc_data;
		BuildICCData(emv_tags, sicc_data);
//		za_co_verifone_bertlv::BERTLV iccdata;
//		iccdata.appendTag("\xff\x20", sicc_data);
//
//		log.message(MSG_INFO"DBG ICCDATA length "+UtilsIntToString(iccdata.getStringData().length())+" ["+UtilsHexToString(iccdata.getStringData().data(), iccdata.getStringData().length())+"]\n");
//
//		TraceTags(sicc_data);
//		IsoAddField(isoh, ICC_data, AddFieldNotApplicable, (unsigned char*)iccdata.getStringData().data(), iccdata.getStringData().length());										// 55

		IsoAddField(isoh, ICC_data, AddFieldNotApplicable, (unsigned char*)sicc_data.c_str(), sicc_data.length());										// 55
	}

//	iq_audi_291116 comment field
//	if (UtilsStringToInt(batchrec.getBudgetPeriod()) > 0){
//		IsoAddField(isoh, Extended_payment_data, AddFieldNotApplicable, (unsigned char*)batchrec.getBudgetPeriod().c_str(), batchrec.getBudgetPeriod().length());// 67
//	}

	unsigned char *iso_msg;
	unsigned int iso_msg_len;
	iso_msg_len = IsoBuildMsg(isoh, (unsigned char**) &iso_msg);

	IsoDestroyList(&isoh);

	if(iso_msg_len == 0){
		log.message(MSG_INFO "IsoBuildMsg failed\r\n");
		return -1;
	}

	string str_iso_msg = string((const char*)iso_msg, iso_msg_len);

	free(iso_msg);

	log.message(MSG_INFO "Advice ("+UtilsIntToString(str_iso_msg.length())+" bytes) ["+UtilsHexToString(str_iso_msg.c_str(), str_iso_msg.length())+"]\r\n");

	string response;

	if(send_message_auth(str_iso_msg, response, false) < 0){
		log.message(MSG_INFO "Advice failed\r\n");

		return -1;
	}

	log.message(MSG_INFO "Advice ( response ("+UtilsIntToString(response.length())+" bytes) ["+UtilsHexToString(response.c_str(), response.length())+"]\r\n");

	isoh_resp = TaIsoDecode((unsigned char*) response.c_str(), response.length());

	if(isoh_resp < 0){
		log.message(MSG_INFO "TaIsoDecode failed with "+UtilsIntToString(isoh_resp)+"\r\n");
		return -1;
	}

	batchrec.setApproveOnline(true);

	char *field_data=NULL;
	unsigned int field_len;

	if(IsoGetField(isoh_resp, Retrieval_reference_number, AddFieldNotApplicable, (unsigned char**) &field_data, &field_len) < 0){
		log.message(MSG_INFO "IsoGetField failed to get Retrieval_reference_number\r\n");
		IsoDestroyList(&isoh_resp);
		return -1;
	}else{
		string rrn = string(field_data, field_len);
		batchrec.setRrn(rrn);
		setRRN(rrn);
		log.message(MSG_INFO "RRN ["+rrn+"]\r\n");
	}

	free(field_data);

	if(IsoGetField(isoh_resp, Approval_code, AddFieldNotApplicable, (unsigned char**) &field_data, &field_len) < 0){
		log.message(MSG_INFO "IsoGetField failed to get Approval_code\r\n");
//		IsoDestroyList(&isoh_resp);
//		return -1;
		batchrec.setAuthCode(string(""));
	}else{
		string approvalcode = string(field_data, field_len);
		batchrec.setAuthCode(approvalcode);
		setAuthCode(approvalcode);
		log.message(MSG_INFO "Approval code ["+approvalcode+"]\r\n");
	}

	free(field_data);

	if(IsoGetField(isoh_resp, Action_code, AddFieldNotApplicable, (unsigned char**) &field_data, &field_len) < 0){
		log.message(MSG_INFO "IsoGetField failed to get Action_code\r\n");
		IsoDestroyList(&isoh_resp);
		return -1;
	}else{
		string actioncode = MapActionCode(string(field_data, field_len));
		batchrec.setRespCode(actioncode);
		setActionCode(actioncode);
		log.message(MSG_INFO "Action code ["+string(field_data, field_len)+"] mapped --> ["+actioncode+"]\r\n");
	}

	free(field_data);

	IsoDestroyList(&isoh_resp);

	update_batchrec(batchrec);

	return 0;
}

//iq_audi_030117
int Advice::DoBatchUpLoad(int trans_remain_to_upload)
	{
	unsigned char pan[37+1];
	unsigned int pan_len=0;
	char buffer[100];
	ISO_HANDLE isoh, isoh_resp;
	int ito_acc;
	com_verifone_terminalconfig::CTerminalConfig termcfg;

	isoh = IsoCreateList(MTI_0320);

	//
	// Mandatory fields
	//

	int itx_type = UtilsStringToInt(batchrec.getTxCode());
	int iacc_type = UtilsStringToInt(batchrec.getAccType());

	if(trans_remain_to_upload) 	// TODO: Where does this come from?
		ito_acc=01;
	else
		ito_acc=0;

	PanEntryMode entry_mode=(PanEntryMode)batchrec.getPanEntryMode();

	// Processing code
	if ((itx_type == TX_ISO_REFUND) || (itx_type == TX_ISO_DEPOSIT) || (itx_type == TX_ISO_CORRECTION) || (itx_type == TX_ISO_REFUND) || (itx_type == TX_ISO_PMNT_DEPOSIT))
		sprintf(buffer, "%02X%02d%02d", (int)itx_type, (int)iacc_type, (int)ito_acc);
	else if (itx_type == TX_ISO_TRANSFER)
		sprintf(buffer, "%02X%02d%02d", (int)itx_type, (int)iacc_type, (int)ito_acc);
	else
		sprintf(buffer, "%02X%02d%02d", (int)itx_type, (int)iacc_type, (int)ito_acc);

	IsoAddField(isoh, Processing_code, AddFieldNotApplicable, (unsigned char*)buffer, 6);																// 3

	if(batchrec.getTxAmount()!=0.00) //iq_audi_231116
	{
		char stx_amount[12+1]={0};

		sprintf(stx_amount, "%012ld",batchrec.getTxAmount());
		IsoAddField(isoh, Amount_transaction, AddFieldNotApplicable, (unsigned char*) stx_amount, 12);
	}

	string datetime = batchrec.getTxDateTime();

	IsoAddField(isoh, Date_and_time_local_transaction, AddFieldNotApplicable, (unsigned char*) datetime.substr(8, 12).c_str(), 6);	// 12

	//	iq_audi_231116
	IsoAddField(isoh, Date_local_transaction, AddFieldNotApplicable, (unsigned char*) datetime.substr(4, 8).c_str(), 4);	//13

	CTsn ctsn;
	char stan[6+1]={0};
	sprintf(stan, "%06d",ctsn.getNextTsn());

//	com_verifone_receiptno::CReceiptNo recpt;
//	char sequenceNo[6+1] ={0};
//	sprintf(sequenceNo,"%06d",recpt.getCurrentReceiptNo());
	IsoAddField(isoh, System_trace_audit_number, AddFieldNotApplicable, (unsigned char*)stan,6);

	//	iq_audi_270217
	string card_input_mode;
	card_input_mode.clear();

	if ((entry_mode == PEM_SWIPED)  ) {
		card_input_mode.append("02");
	} else if (entry_mode == PEM_ENTERED) {
		card_input_mode.append("01");
	} else if (entry_mode == PEM_INSERTED) {
		card_input_mode.append("05");
	} else if (entry_mode == PEM_TAPPED) {
		card_input_mode.append("07");
	} else if (entry_mode == PEM_FALLBACK) {
		card_input_mode.append("80");
	}

	CARD_AUTH_METHOD cardhAuthMethod=(CARD_AUTH_METHOD)batchrec.getCardhAuthMethod();

	if(cardhAuthMethod==PIN)
		card_input_mode.append("1");
	else
		card_input_mode.append("0");

	IsoAddField(isoh, POS_data_code, AddFieldNotApplicable, (unsigned char*) card_input_mode.c_str(), 3); //22

	//	iq_audi_291116
	string str_tpdu = termcfg.getTpdu();
	IsoAddField(isoh, Function_code, AddFieldNotApplicable, (unsigned char*)str_tpdu.substr(3, 6).c_str(), 3);		// 24

	IsoAddField(isoh, Message_reason_code, AddFieldNotApplicable, (unsigned char*)"00", 2);		// 25

	//	iq_audi_291116 comment field
	//	com_verifone_batchmanager::CBatchManager batchManager;
	//	com_verifone_batch::CBatch cbatch;
	//
	//	int ret = batchManager.getBatch(cbatch);
	//
	//	if (ret != 1){
	//		log.message(MSG_INFO ":Failure retrieving batch number\r\n");
	//		return -1;
	//	}
	//	string str_batchno = UtilsIntToString(cbatch.getBatchNo());
	//	IsoAddField(isoh, Reconciliation_indicator, AddFieldNotApplicable, (unsigned char*) str_batchno.c_str(), str_batchno.length());						// 29

	if ((entry_mode == PEM_SWIPED)  || (entry_mode == PEM_FALLBACK)){
//		iq_audi_241116 start
		string track2=batchrec.getTrack2();

		uint pos = track2.find('=', 0);
		track2.replace(pos, 1, 1, 'D');

		memset(pan,0,sizeof(pan));
		pan_len=track2.length();
		memcpy(pan,track2.c_str(),pan_len);

//		iq_audi_241116 end
		IsoAddField(isoh, Track_2_data, AddFieldNotApplicable, (unsigned char*)pan,pan_len);

		string strPan;
		strPan=track2.substr(0,track2.find("D"));
		pan_len=strPan.length();


		IsoAddField(isoh, Primary_account_number, AddFieldNotApplicable, (unsigned char*)strPan.c_str(), pan_len);		// 2
		IsoAddField(isoh, Date_expiration, AddFieldNotApplicable, (unsigned char*)track2.substr(pos+1,pos+5).c_str(), 4);	// 14
	}

	if (entry_mode == PEM_INSERTED){
		string emv_track2;
		BuildEmvTrack2(batchrec.getEmvTags(), emv_track2);

//		iq_audi_241116
		uint pos = emv_track2.find('=', 0);
		emv_track2.replace(pos, 1, 1, 'D');

		memset(pan,0,sizeof(pan));
		pan_len=emv_track2.length();
		memcpy(pan,emv_track2.c_str(),pan_len);

		IsoAddField(isoh, Track_2_data, AddFieldNotApplicable, (unsigned char*)pan,pan_len);				// 35
		batchrec.setPan(emv_track2.substr(0,emv_track2.find("D")));

		memset(pan,0,sizeof(pan));
		pan_len=batchrec.getPan().length();
		memcpy(pan,batchrec.getPan().c_str(),pan_len);

		IsoAddField(isoh, Primary_account_number, AddFieldNotApplicable, (unsigned char*)pan, pan_len);		// 2
		IsoAddField(isoh, Date_expiration, AddFieldNotApplicable, (unsigned char*)emv_track2.substr(pos+1,pos+4).c_str(), 4);	// 14
	}

	string rrn = batchrec.getRrn();
	if(rrn.length())
		IsoAddField(isoh, Retrieval_reference_number, AddFieldNotApplicable, (unsigned char*)rrn.c_str(), rrn.length());								// 37

	string authcode = batchrec.getAuthCode();
	if(authcode.length())
		IsoAddField(isoh, Approval_code, AddFieldNotApplicable, (unsigned char*) authcode.c_str(), authcode.length());									// 38

	IsoAddField(isoh, Action_code, AddFieldNotApplicable, (unsigned char*) "00", 2);																	// 39

	string str_tid = space_pad(termcfg.getTerminalNo(), 8);
	IsoAddField(isoh, Terminal_ID, AddFieldNotApplicable, (unsigned char*)str_tid.c_str(), str_tid.length());											// 41

	string str_merchno;
	str_merchno.insert(0, 15, '0');
	str_merchno.replace(15-termcfg.getMerchantNo().length(), termcfg.getMerchantNo().length(), termcfg.getMerchantNo());
	IsoAddField(isoh, Card_acceptor_ID, AddFieldNotApplicable, (unsigned char*)str_merchno.c_str(), str_merchno.length());								// 42

	string f49enable;
	f49enable=termcfg.getF49Enable();
	if(atoi(f49enable.c_str())==1)
		IsoAddField(isoh, Currency_code_transaction, AddFieldNotApplicable, (unsigned char*)batchrec.getTransCurrency().c_str(), 3);

	/*
	//
	//
	// Optional/Conditional fields
	//
	// add the invoice number
	if(strlen((char*) tx_rec->invoiceno) != 0){
		IsoAddField(isoh, Additional_data_private, Additional_node_data, (unsigned char*) tx_rec->invoiceno, strlen((char*) tx_rec->invoiceno)); // 48.5
	}

	// Manual card entry
	if (tx_rec->pan_entr_mode == CARD_ENTRY_MANUAL) {
		char cExpiryDate[4];

		IsoAddField(isoh, Primary_account_number, AddFieldNotApplicable, (unsigned char*) tx_rec->pan, strlen((char*) tx_rec->pan)); // 2
		cExpiryDate[0] = tx_rec->exp[2];
		cExpiryDate[1] = tx_rec->exp[3];
		cExpiryDate[2] = tx_rec->exp[0];
		cExpiryDate[3] = tx_rec->exp[1];
		IsoAddField(isoh, Date_expiration, AddFieldNotApplicable, (unsigned char*) cExpiryDate, 4); // 14
		//IsoAddField(isoh, Date_expiration, AddFieldNotApplicable, (unsigned char*) tx_rec->exp, 4); // 14
		//IsoAddField(isoh, Card_sequence_number, AddFieldNotApplicable, (unsigned char*) tx_rec->src, 3); // 23
		//sprintf(buffer," %s",tx_rec->CVV);
		IsoAddField(isoh, Additional_data_private, Card_verification_data, (unsigned char*) tx_rec->CVV, 4); // 48.3
	} else if ((tx_rec->pan_entr_mode == CARD_ENTRY_SWIPE) || (tx_rec->pan_entr_mode==CARD_ENTRY_FALLBACK)|| (tx_rec->pan_entr_mode == CARD_ENTRY_CONTACTLESS_MAG)) {
		IsoAddField(isoh, Track_2_data, AddFieldNotApplicable, (unsigned char*) tx_rec->track2, strlen((char*) tx_rec->track2));// 35
	} else if (tx_rec->pan_entr_mode == CARD_ENTRY_EMV || tx_rec->pan_entr_mode == CARD_ENTRY_CONTACTLESS_ICC) {
		IsoAddField(isoh, Primary_account_number, AddFieldNotApplicable, (unsigned char*) tx_rec->pan, strlen((char*) tx_rec->pan)); // 2
		IsoAddField(isoh, Track_2_data, AddFieldNotApplicable, (unsigned char*) tx_rec->track2, strlen((char*) tx_rec->track2));// 35
	}
	*/
	if (batchrec.getCashAmount() > 0) {
		sprintf(buffer, "%02d40710C%012d", (unsigned int)UtilsStringToInt(batchrec.getAccType()), (unsigned int)batchrec.getCashAmount());
		IsoAddField(isoh, Additional_amounts, AddFieldNotApplicable, (unsigned char*) buffer, strlen(buffer));											// 54
	}

//			iq_audi_291116 comment field
//		string original_data;
//		if(batchrec.getDraftCapMode() == ONLINE)
//			original_data = "1200";
//		else
//			original_data = "1100";
//
//		original_data.append(zero_pad(batchrec.getTsn(), 6));
//
//		datetime = batchrec.getTxDateTime();
//		original_data.append(datetime.substr(4, 10));
//		original_data.append(11, '0');
//
//		IsoAddField(isoh, Original_data_elements, AddFieldNotApplicable, (unsigned char*) original_data.c_str(), original_data.length()); 					// 56

	if(entry_mode == PEM_INSERTED){
	//		iq_audi_291116 comment field
	//		IsoAddField(isoh, Card_sequence_number, AddFieldNotApplicable, (unsigned char*)"000", 3);														// 23

		string emv_tags = batchrec.getEmvTags();
		FilterEMVTags(batchrec.getEmvTags(), emv_tags);

		string sicc_data;
		BuildICCData(emv_tags, sicc_data);
	//		za_co_verifone_bertlv::BERTLV iccdata;
	//		iccdata.appendTag("\xff\x20", sicc_data);

	//		log.message(MSG_INFO"DBG ICCDATA length "+UtilsIntToString(iccdata.getStringData().length())+" ["+UtilsHexToString(iccdata.getStringData().data(), iccdata.getStringData().length())+"]\n");
	//
	//		TraceTags(sicc_data);
	//		IsoAddField(isoh, ICC_data, AddFieldNotApplicable, (unsigned char*)iccdata.getStringData().data(), iccdata.getStringData().length());										// 55

		IsoAddField(isoh, ICC_data, AddFieldNotApplicable, (unsigned char*)sicc_data.c_str(), sicc_data.length());										// 55
	}

	//	iq_audi_291116 comment field
	//	if (UtilsStringToInt(batchrec.getBudgetPeriod()) > 0){
	//		IsoAddField(isoh, Extended_payment_data, AddFieldNotApplicable, (unsigned char*)batchrec.getBudgetPeriod().c_str(), batchrec.getBudgetPeriod().length());// 67
	//	}

	string str_txntype_batchno("0200");
	str_txntype_batchno.append(zero_pad(UtilsIntToString(batch_no),6));
	str_txntype_batchno.insert(10,22,' ');
	IsoAddField(isoh, F_60, AddFieldNotApplicable, (unsigned char*)str_txntype_batchno.c_str(), 22);

	string str_tsn = zero_pad(batchrec.getTsn(), 6);
	IsoAddField(isoh, Hotcard_capacity, AddFieldNotApplicable, (unsigned char*)str_tsn.c_str(), str_tsn.length());	//F62

	unsigned char *iso_msg;
	unsigned int iso_msg_len;
	iso_msg_len = IsoBuildMsg(isoh, (unsigned char**) &iso_msg);

	IsoDestroyList(&isoh);

	if(iso_msg_len == 0){
		log.message(MSG_INFO "IsoBuildMsg failed\r\n");
		return -1;
	}

	string str_iso_msg = string((const char*)iso_msg, iso_msg_len);

	free(iso_msg);

	log.message(MSG_INFO "Advice ("+UtilsIntToString(str_iso_msg.length())+" bytes) ["+UtilsHexToString(str_iso_msg.c_str(), str_iso_msg.length())+"]\r\n");

	string response;

	if(send_message_auth(str_iso_msg, response, false) < 0){
		log.message(MSG_INFO "Advice failed\r\n");

		return -1;
	}

	log.message(MSG_INFO "Advice ( response ("+UtilsIntToString(response.length())+" bytes) ["+UtilsHexToString(response.c_str(), response.length())+"]\r\n");

	isoh_resp = TaIsoDecode((unsigned char*) response.c_str(), response.length());

	if(isoh_resp < 0){
		log.message(MSG_INFO "TaIsoDecode failed with "+UtilsIntToString(isoh_resp)+"\r\n");
		return -1;
	}

	batchrec.setApproveOnline(true);

	char *field_data=NULL;
	unsigned int field_len;

//	iq_audi_020117
//	if(IsoGetField(isoh_resp, Retrieval_reference_number, AddFieldNotApplicable, (unsigned char**) &field_data, &field_len) < 0){
//		log.message(MSG_INFO "IsoGetField failed to get Retrieval_reference_number\r\n");
//		IsoDestroyList(&isoh_resp);
//		return -1;
//	}else{
//		string rrn = string(field_data, field_len);
//		batchrec.setRrn(rrn);
//		setRRN(rrn);
//		log.message(MSG_INFO "RRN ["+rrn+"]\r\n");
//	}
//
//	free(field_data);

//	if(IsoGetField(isoh_resp, Approval_code, AddFieldNotApplicable, (unsigned char**) &field_data, &field_len) < 0){
//		log.message(MSG_INFO "IsoGetField failed to get Approval_code\r\n");
//	//		IsoDestroyList(&isoh_resp);
//	//		return -1;
//		batchrec.setAuthCode(string(""));
//	}else{
//		string approvalcode = string(field_data, field_len);
//		batchrec.setAuthCode(approvalcode);
//		setAuthCode(approvalcode);
//		log.message(MSG_INFO "Approval code ["+approvalcode+"]\r\n");
//	}
//
//	free(field_data);

	if(IsoGetField(isoh_resp, Action_code, AddFieldNotApplicable, (unsigned char**) &field_data, &field_len) < 0){
		log.message(MSG_INFO "IsoGetField failed to get Action_code\r\n");
		IsoDestroyList(&isoh_resp);
		return -1;
	}else{
		string actioncode = MapActionCode(string(field_data, field_len));
		batchrec.setRespCode(actioncode);
		setActionCode(actioncode);
		log.message(MSG_INFO "Action code ["+string(field_data, field_len)+"] mapped --> ["+actioncode+"]\r\n");
	}

	free(field_data);

	IsoDestroyList(&isoh_resp);

	update_batchrec(batchrec);

	return 0;
}

string Advice::getActionCode(void)
{
	return action_code;
}

string Advice::getAuthCode(void)
{
	return auth_code;
}

string Advice::getRRN(void)
{
	return rrn;
}

//
// Private methods
//
void Advice::setActionCode(string value)
{
	action_code = value;
}

void Advice::setAuthCode(string value)
{
	auth_code = value;
}

void Advice::setRRN(string value)
{
	rrn = value;
}
