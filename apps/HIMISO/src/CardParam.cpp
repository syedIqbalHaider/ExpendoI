#include <libda/ccardconfig.h>
#include "CardParam.h"
#include "CLog.h"
#include "utils.h"
#include "HIMISOver.h"

static CLog log(HIMISO_NAME);

#define CP_ACC_DESCR				"flash/CP_acc_dsc.dat"
#define CP_LIMITS_TABLE				"flash/CP_lim_tbl.dat"
#define CP_ACC_PROF					"flash/CP_acc_prf.dat"
#define CP_ALLOWED_TX				"flash/CP_all_tra.dat"
#define CP_CARD_PROF				"flash/CP_crd_prf.dat"
#define CP_CARD_TABLE_DAT_NAME		"flash/CP_crd_prd.dat"
#define CP_ATT_ACC					"flash/CP_att_acc.dat"
#define CP_BIN_DAT_NAME				"flash/CP_bin.dat"
#define CP_PUB_KEY					"flash/CP_pub_key.dat"
#define CP_AID						"flash/CP_aid_prm.dat"
#define CP_AID_OVERR				"flash/CP_aid_over.dat"

enum STATE
{
	STATE_START,
	STATE_ACC_DESCR,
	STATE_LIMITS_TABLE,
	STATE_ACC_PROF,
	STATE_ALLOWED_TX,
	STATE_CARD_PROF,
	STATE_CARD_TABLE,
	STATE_ATT_ACC,
	STATE_BIN_TABLE,
	STATE_PUB_KEY,
	STATE_AID,
	STATE_AID_OVR,
	STATE_COMPLETE,
	STATE_END
};

static STATE state = STATE_START;

void CardParam::ClearVersion(void)
{
	clear_version = true;
}

bool CardParam::isVersionClear(void)
{
	return clear_version;
}

int CardParam::Download()
{
	string version;

	if(!clear_version)
		version = GetVersion();
	else
		version = "0000";

	state = STATE_START;

	while(state != STATE_END){
		switch(state)
		{
			case STATE_START:
				log.message(MSG_INFO "Card Parameters Download Start\n");
				state = STATE_ACC_DESCR;
				break;
			case STATE_ACC_DESCR:
				{
					log.message(MSG_INFO "Account Description table start\n");

					fh = fopen(CP_ACC_DESCR, "w+b");

					if(fh == NULL){
						log.message(MSG_INFO "Card Parameters Parameters failed to open "CP_ACC_DESCR"\n");
						return -1;
					}

					if(PerformAction(version, "acc_dsc") < 0){
						log.message(MSG_INFO "Card parameters download failed!\n");
						state = STATE_END;
						return -1;
					}

					// If we received no data_records i.e. msg_no == 0, then the file is up to date and we can exit right here.
					// No need to download the additional tables.
					if(msg_no == 0){
						log.message(MSG_INFO "Card parameters are up to date!\n");
						state = STATE_END;
						break;
					}

					state = STATE_LIMITS_TABLE;
					break;
				}
			case STATE_LIMITS_TABLE:
				{
					log.message(MSG_INFO "Limits table start\n");

					fh = fopen(CP_LIMITS_TABLE, "w+b");

					if(fh == NULL){
						log.message(MSG_INFO "Card Parameters Parameters failed to open "CP_LIMITS_TABLE"\n");
						return -1;
					}

					if(PerformAction(version, "lim_tbl") < 0){
						log.message(MSG_INFO "Card parameters download failed!\n");
						state = STATE_END;
						return -1;
					}

					state = STATE_ACC_PROF;
					break;
				}
			case STATE_ACC_PROF:
				{
					log.message(MSG_INFO "Account Profile table start\n");

					fh = fopen(CP_ACC_PROF, "w+b");

					if(fh == NULL){
						log.message(MSG_INFO "Card Parameters Parameters failed to open "CP_ACC_PROF"\n");
						return -1;
					}

					if(PerformAction(version, "acc_prf") < 0){
						log.message(MSG_INFO "Card parameters download failed!\n");
						state = STATE_END;
						return -1;
					}

					state = STATE_ALLOWED_TX;
					break;
				}
			case STATE_ALLOWED_TX:
				{
					log.message(MSG_INFO "Allowed Transactions table start\n");

					fh = fopen(CP_ALLOWED_TX, "w+b");

					if(fh == NULL){
						log.message(MSG_INFO "Card Parameters Parameters failed to open "CP_ALLOWED_TX"\n");
						return -1;
					}

					if(PerformAction(version, "all_tra") < 0){
						log.message(MSG_INFO "Card parameters download failed!\n");
						state = STATE_END;
						return -1;
					}

					state = STATE_CARD_PROF;
					break;
				}
			case STATE_CARD_PROF:
				{
					log.message(MSG_INFO "Card Profile table start\n");

					fh = fopen(CP_CARD_PROF, "w+b");

					if(fh == NULL){
						log.message(MSG_INFO "Card Parameters Parameters failed to open "CP_CARD_PROF"\n");
						return -1;
					}

					if(PerformAction(version, "crd_prf") < 0){
						log.message(MSG_INFO "Card parameters download failed!\n");
						state = STATE_END;
						return -1;
					}

					state = STATE_ATT_ACC;
					break;
				}
			case STATE_ATT_ACC:
				{
					log.message(MSG_INFO "Attached Accounts table start\n");

					fh = fopen(CP_ATT_ACC, "w+b");

					if(fh == NULL){
						log.message(MSG_INFO "Card Parameters Parameters failed to open "CP_ATT_ACC"\n");
						return -1;
					}

					if(PerformAction(version, "att_acc") < 0){
						log.message(MSG_INFO "Card parameters download failed!\n");
						state = STATE_END;
						return -1;
					}

					state = STATE_CARD_TABLE;
					break;
				}
			case STATE_CARD_TABLE:
			{
				log.message(MSG_INFO "Card table start\n");

				fh = fopen(CP_CARD_TABLE_DAT_NAME, "w+b");

				if(fh == NULL){
					log.message(MSG_INFO "Card Parameters Parameters failed to open "CP_CARD_TABLE_DAT_NAME"\n");
					return -1;
				}

				if(PerformAction(version, "crd_prd") < 0){
					log.message(MSG_INFO "Card parameters download failed!\n");
					state = STATE_END;
					return -1;
				}

				state = STATE_BIN_TABLE;
				break;
			}
			case STATE_BIN_TABLE:
			{
				log.message(MSG_INFO "BIN table start\n");

				fh = fopen(CP_BIN_DAT_NAME, "w+b");

				if(fh == NULL){
					log.message(MSG_INFO "Card Parameters Parameters failed to open "CP_BIN_DAT_NAME"\n");
					return -1;
				}

				if(PerformAction(version, "bin_tbl") < 0){
					log.message(MSG_INFO "Card parameters download failed!\n");
					state = STATE_END;
					return -1;
				}

				state = STATE_PUB_KEY;
				break;
			}
			case STATE_PUB_KEY:
				{
					log.message(MSG_INFO "Public Key start\n");

					fh = fopen(CP_PUB_KEY, "w+b");

					if(fh == NULL){
						log.message(MSG_INFO "Card Parameters Parameters failed to open "CP_PUB_KEY"\n");
						return -1;
					}

					if(PerformAction(version, "pub_key") < 0){
						log.message(MSG_INFO "Card parameters download failed!\n");
						state = STATE_END;
						return -1;
					}

					state = STATE_AID;
					break;
				}
			case STATE_AID:
				{
					log.message(MSG_INFO "AID start\n");

					fh = fopen(CP_AID, "w+b");

					if(fh == NULL){
						log.message(MSG_INFO "Card Parameters Parameters failed to open "CP_AID"\n");
						return -1;
					}

//					if(PerformAction(version, "aid_prm:tbl_ext:002") < 0){
					if(PerformAction(version, "aid_prm:tbl_ext:001") < 0){
						log.message(MSG_INFO "Card parameters download failed!\n");
						state = STATE_END;
						return -1;
					}

					state = STATE_AID_OVR;
					break;
				}
			case STATE_AID_OVR:
				{
					log.message(MSG_INFO "AID start\n");

					fh = fopen(CP_AID_OVERR, "w+b");

					if(fh == NULL){
						log.message(MSG_INFO "Card Parameters Parameters failed to open "CP_AID_OVERR"\n");
						return -1;
					}

					if(PerformAction(version, "aid_ovr") < 0){
						log.message(MSG_INFO "Card parameters download failed!\n");
						state = STATE_END;
						return -1;
					}

					state = STATE_COMPLETE;
					break;
				}
			case STATE_COMPLETE:
				state = STATE_END;
				break;
			case STATE_END:
				break;
		}
	}

	return status;
}

int CardParam::ResponseData(int msg_no, string data)
{
	(void)msg_no;

	log.message(MSG_INFO "Card Parameters Parameters RX ("+Utils::UtilsIntToString(data.length())+" bytes) ["+data+"]\r\n");

	fwrite(data.data(), 1, data.length(), fh);

	return 0;
}

int CardParam::FileName(int msg_no, string filename)
{
	this->msg_no = msg_no;

	// Write the version number when the first message for a particular file has been received
	if(msg_no == 1){
		uint pos1, pos2;

		if((pos1 = filename.find_first_of(':')) == string::npos)
			return -1;

		if((pos2 = filename.find_first_of(':', pos1+1)) == string::npos)
			return -2;

		string version = filename.substr(pos1+1, pos2-pos1-1);

		if(version.length() != 4)
			return -3;

		if(fh)
			fwrite(version.data(), 1, version.length(), fh);
		else
			return -4;
	}

	return 0;
}

void CardParam::Error(ERRORCODE code)
{
	status = (int)code;

	fclose(fh);

	switch(status){
		case ERROR_CONNECT:
			log.message(MSG_INFO "Card Parameters Parameters Error Connecting\n");
			break;
		case ERROR_SEND:
			log.message(MSG_INFO "Card Parameters Parameters Error Sending\n");
			break;
		case ERROR_TIMEOUT:
			log.message(MSG_INFO "Card Parameters Parameters Error Timeout\n");
			break;
		case ERROR_FORMAT:
			log.message(MSG_INFO "Card Parameters Parameters Error Format\n");
			break;
		case ERROR_MISSING_DATA:
			log.message(MSG_INFO "Card Parameters Parameters Error Missing data\n");
			break;
		case ERROR_FILE_SYSTEM:
			log.message(MSG_INFO "Card Parameters Parameters File System Error\n");
			break;
	}

	state = STATE_END;
}

void CardParam::FileActionComplete(void)
{
	log.message(MSG_INFO "Card Parameters Parameters Download Complete\n");
	fclose(fh);
	status = 0;
}

string CardParam::GetVersion(void)
{
	string version;
	com_verifone_cardconfig::CCardConfig cardcfg;
	cardcfg.getVersion(version);

	if(version.length() == 0)
		version = "0";

	log.message(MSG_INFO "Card Parameters Parameters version number ["+version+"]\n");

	return version;
}

void CardParam::Cleanup(void)
{
	remove(CP_ACC_DESCR);
	remove(CP_LIMITS_TABLE);
	remove(CP_ACC_PROF);
	remove(CP_ALLOWED_TX);
	remove(CP_CARD_PROF);
	remove(CP_CARD_TABLE_DAT_NAME);
	remove(CP_ATT_ACC);
	remove(CP_BIN_DAT_NAME);
	remove(CP_PUB_KEY);
	remove(CP_AID);
	remove(CP_AID_OVERR);
}
