#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "bmp.h"
//#include "trace.h"

typedef struct
{
  unsigned int bitmap_size;  // Size in bytes, of *bitmap
  unsigned long bits;        // Number of bits in the bitmap
  unsigned char *bitmap;     // Actuall bitmap
}BmpInfo;

static BmpInfo bmps[MAX_NO_BITMAPS];
static bool bhandles[MAX_NO_BITMAPS];
static bool bhandles_init = false;

BMPH GetHandle(void);

void BmpInit(void)
{
  int i;

  for(i=0; i<MAX_NO_BITMAPS; i++)
    bhandles[i] = false;

  bhandles_init = true;
}

BMPH BmpCreate(unsigned long no_bits)
{
  unsigned int len, extra;
  BMPH handle;

  handle = GetHandle();

  if(handle < 0)
    return BMP_NO_HANDLE;

  if(no_bits == 0)
    return BMP_NO_HANDLE; // Guard against divide by zero
  
  len = no_bits / 8;
  extra = (unsigned int)no_bits % 8;

  if(extra != 0)
    len++;
  
  bmps[handle].bits = no_bits;
  bmps[handle].bitmap_size = len;

  bmps[handle].bitmap = (unsigned char*)malloc(bmps[handle].bitmap_size);

  if(bmps[handle].bitmap == NULL)
    return BMP_NO_HANDLE;

  return handle;
}

bool BmpDestroy(BMPH *handle)
{
  if((bhandles_init == true) && (bhandles[*handle] == true)) // Check that BmpInit was called and the handle is valid
  {
    bmps[*handle].bitmap_size = 0;
    bmps[*handle].bits = 0;
    bhandles[*handle] = false;
    free(bmps[*handle].bitmap);
  }

  *handle = -1;

  return false;
}

long BmpImport(BMPH handle, unsigned char *data, unsigned long no_bits)
{
  unsigned int len, extra;

  if(!bhandles_init || !bhandles[handle]) // Check that BmpInit was called and the handle is valid
    return BMP_NO_HANDLE;

  if(no_bits == 0)
    return BMP_SIZE_ERROR; // Guard against divide by zero
  
  len = no_bits / 8;
  extra = (unsigned int)no_bits % 8;

  if(extra != 0)
    len++;

  // Check if there is enough space available in bitmap
  if(len > bmps[handle].bitmap_size)
  {
    // If not, then clear all values and reinitialise
    free(bmps[handle].bitmap);
    bmps[handle].bits = 0;
    bmps[handle].bitmap_size = 0;
  
    bmps[handle].bits = no_bits;
    bmps[handle].bitmap_size = len;

    bmps[handle].bitmap = (unsigned char*)malloc(bmps[handle].bitmap_size);

    if(bmps[handle].bitmap == NULL)
      return BMP_MEM_ERROR;
  }

  memcpy(bmps[handle].bitmap, data, bmps[handle].bitmap_size);
  
  return BMP_SUCCESS;
}

long BmpExport(BMPH handle, unsigned char *data, unsigned long max_len_bytes)
{
  unsigned long len;

  if(!bhandles_init || !bhandles[handle]) // Check that BmpInit was called and the handle is valid
      return BMP_NO_HANDLE;

  if((unsigned long)max_len_bytes > bmps[handle].bitmap_size)
    len = bmps[handle].bitmap_size;
  else
    len = max_len_bytes;

  memcpy(data, bmps[handle].bitmap, len);

  return (long)len;
}

unsigned long BmpGetTotalNoBits(BMPH handle, bool *error)
{
  if(!bhandles_init || !bhandles[handle]) // Check that BmpInit was called and the handle is valid
  {
    *error = true;
    return 0;
  }

  return bmps[handle].bits;
}

// TODO: check something fishy here!!
bool BmpIsBitSet(BMPH handle, unsigned long bit_number, bool *error, int direction)
{
  unsigned long bytes=0;  // Number of whole bytes
  unsigned long no_bits=0;   // Remainder bits
  unsigned char buff;
  unsigned char mask_left = 0x01;
  unsigned char mask_right = 0x80;

  if(!bhandles_init || !bhandles[handle]) // Check that BmpInit was called and the handle is valid
  {
    *error = true;
    return false;
  }

  if(bit_number > 0)  // Prevent divide by 0
  {
    no_bits = bit_number % 8;
    bytes = bit_number / 8;
  }
  else
  {
    no_bits = 0;
    bytes = 0;
  }

  // Check that the bit_number is within the range of the bitmask
  if(bytes > bmps[handle].bitmap_size - 1)
    return false;

  buff = bmps[handle].bitmap[bytes];

  switch(direction)
  {
	  case BLEFT:
		  mask_left <<= no_bits;

		  if((buff & mask_left) == mask_left)
			  return true;

		  break;

	  case BRIGHT:
		  mask_right >>= no_bits;

		  if((buff & mask_right) == mask_right)
			  return true;

  		break;
  }

  return false;
}

bool BmpSetBit(BMPH handle, unsigned long bit_number, bool *error, int direction)
{
  unsigned long bytes=0;  // Number of whole bytes
  unsigned long bits=0;   // Remainder bits
  unsigned char mask_left = 0x01;
  unsigned char mask_right = 0x80;

  if(!bhandles_init || !bhandles[handle]) // Check that BmpInit was called and the handle is valid
  {
    *error = true;
    return false;
  }

  if(bit_number > 0)  // Prevent divide by 0
  {
    bits = bit_number % 8;
    bytes = bit_number / 8;
  }

  // Check that the bit_number is within the range of the bitmask
  if(bytes > bmps[handle].bitmap_size - 1)
    return false;

 switch(direction)
  {
	  case BLEFT:
		  mask_left <<= bits;
		  bmps[handle].bitmap[bytes] |= mask_left;

		  break;

	  case BRIGHT:
		  mask_right >>= bits;
		  bmps[handle].bitmap[bytes] |= mask_right;

		break;
  }

  return true;
}

bool BmpClearBit(BMPH handle, unsigned long bit_number, bool *error, int direction)
{
  unsigned long bytes;  // Number of whole bytes
  unsigned long bits;   // Remainder bits
   unsigned char mask_left = 0x01;
  unsigned char mask_right = 0x80;

  if(!bhandles_init || !bhandles[handle]) // Check that BmpInit was called and the handle is valid
  {
    *error = true;
    return false;
  }

  bits = bit_number % 8;
  bytes = bit_number / 8;

  // Check that the bit_number is within the range of the bitmask
  if(bytes > bmps[handle].bitmap_size - 1)
    return false;

  switch(direction)
  {
	  case BLEFT:
		  mask_left <<= bits;  // set the right bit;
		  mask_left = ~mask_left;   // Invert the whole byte

		  bmps[handle].bitmap[bytes] &= mask_left;

		  break;

	  case BRIGHT:
		  mask_right >>= bits;  // set the right bit;
		  mask_right = ~mask_right;   // Invert the whole byte

		  bmps[handle].bitmap[bytes] &= mask_right;

		  break;
  }

  return true;
}

bool BmpClearBitMask(BMPH handle, bool *error)
{

  if(!bhandles_init || !bhandles[handle]) // Check that BmpInit was called and the handle is valid
  {
    *error = true;
    return false;
  }

  memset(bmps[handle].bitmap, 0, bmps[handle].bitmap_size);

  return true;
}

bool BmpAndBit(BMPH handle, unsigned long bit_number, bool value, bool *error, int direction)
{

  if(!bhandles_init || !bhandles[handle]) // Check that BmpInit was called and the handle is valid
  {
    *error = true;
    return false;
  }

  if(BmpIsBitSet(handle, bit_number, error, direction) && value)
    return true;

  if(BmpIsBitSet(handle, bit_number, error, direction) && !value)
  {
    BmpClearBit(handle, bit_number, error, direction);
    return true;
  }

  if(!BmpIsBitSet(handle, bit_number, error, direction) && !value)
    return true;

  if(!BmpIsBitSet(handle, bit_number, error, direction) && value)
  {
    BmpSetBit(handle, bit_number, error, direction);
    return true;
  }

  return true;
}

bool BmpInvertBit(BMPH handle, unsigned long bit_number, bool *error, int direction)
{
  if(!bhandles_init || !bhandles[handle]) // Check that BmpInit was called and the handle is valid
  {
    *error = true;
    return false;
  }

  if(BmpIsBitSet(handle, bit_number, error, direction))
    BmpClearBit(handle, bit_number, error, direction);
  else
    BmpSetBit(handle, bit_number, error, direction);

  return true;
}

bool BmpInvertBitmask(BMPH handle, bool *error)
{
  if(!bhandles_init || !bhandles[handle]) // Check that BmpInit was called and the handle is valid
  {
    *error = true;
    return false;
  }

  *error = true; // Not implemented yet

  return false;
}

// If new_or_old = false, then use the static pointer from previous call
// Stop if error = true
bool BmpRotateBitLeft(BMPH handle, unsigned long start_pos, bool new_or_old, bool *error, int direction)
{
  static unsigned long bit_pos=0;

  if(!bhandles_init || !bhandles[handle]) // Check that BmpInit was called and the handle is valid
  {
    *error = true;
    return false;
  }
  
  *error = false; // Initialise error

  if(new_or_old)
    bit_pos = start_pos;
  else
    ++bit_pos;

  if((bit_pos == 0) || (bit_pos >= (bmps[handle].bitmap_size*8)))
  {
    *error = true;
    return false;
  }
  
  return BmpIsBitSet(handle, bit_pos, error, direction);
}

// If new_or_old = false, then use the static pointer from previous call
// Stop if error = true
bool BmpRotateBitRight(BMPH handle, unsigned long start_pos, bool new_or_old, bool *error, int direction)
{
  static unsigned long bit_pos=0;

  if(!bhandles_init || !bhandles[handle]) // Check that BmpInit was called and the handle is valid
  {
    *error = true;
    return false;
  }
  
  *error = false; // Initialise error

  if(new_or_old)
    bit_pos = start_pos;
  else
    --bit_pos;

  if((bit_pos == 0) || (bit_pos >= (bmps[handle].bitmap_size*8)))
  {
    *error = true;
    return false;
  }
  
  return BmpIsBitSet(handle, bit_pos, error, direction);
}

//
// Private functions
//

BMPH GetHandle(void)
{
  int i;

  for(i=0; i<MAX_NO_BITMAPS; i++)
  {
    if(!bhandles[i])  // If handle is uninitialised, used it
    {
      bhandles[i] = true; // Mark the handle as being in use
      memset(&bmps[i], 0, sizeof(BmpInfo));
      return i;
    }
  }

  return -1;  // No bhandles available
}

void BmpPrintBinary(BMPH handle, BMP_BYTE_ORDER byte_order, char one_symbol, char zero_symbol)
{
  unsigned long nobits=0;
  bool error=0;

  if(!bhandles_init || !bhandles[handle]) // Check that BmpInit was called and the handle is valid
    return;
  
  if(byte_order == MSB_FIRST)
  {
    nobits = bmps[handle].bits;

    while(1)
    {
      if(BmpIsBitSet(handle, nobits, &error, BLEFT))
        printf("1");
      else
        printf("0");

      if(nobits != 0)
        nobits--;
      else 
        break;
    }
  }
  else
  {
    do
    {
      if(BmpIsBitSet(handle, nobits, &error, BRIGHT))
        printf("1");
      else
        printf("0");

      nobits++;
    }while(nobits < bmps[handle].bits);
  }
}

/*
void BmpPrintBinary(BMP_BYTE_ORDER byte_order, char one_symbol, char zero_symbol)
{
  unsigned long nobits=0;

  if(order == MSB_FIRST)
  {
    nobits = bits;

    while(nobits--)
    {
      if(IsBitSet(nobits))
        cout << one_symbol;
      else
        cout << zero_symbol;
    }
  }
  else
  {
    do
    {
      if(IsBitSet(nobits))
        cout << one_symbol;
      else
        cout << zero_symbol;

      nobits++;
    }while(nobits < bits);
  }
}

// Prints the MSB first
void cBitMap::PrintHex(void)
{
  unsigned long bytes = bitmap_size;
    
  while(bytes--)
    cout << "0x" << hex << (int)bitmap[bytes] << ' ';
}

*/
