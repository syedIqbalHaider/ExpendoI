#include <arpa/inet.h>
#include <sstream>
#include <iomanip>
#include <libda/cemvdata.h>
#include <libda/ccardconfig.h>
#include <libda/cterminalconfig.h>
#include <libda/climits.h>
#include "EmvParam.h"
#include "CLog.h"
#include "utils.h"
#include "HIMISOver.h"

static CLog log(HIMISO_NAME);

using namespace Utils;
using namespace com_verifone_emvdata;
using namespace com_verifone_cardconfig;
using namespace com_verifone_terminalconfig;
using namespace com_verifone_limits;

#define PAM_BLOCKS

int EmvParam::UpdateParameters()
{
	CCardConfig cardcfg;

	CTerminalConfig termcfg;

	// Delete all existing ICC parameters
	log.message(MSG_INFO"Deleting EMV parameters....\n");

	BERTLV delparam;
	delparam.appendTag("\xdf\xaf\x01", "\x45");
	delparam.appendTag("\xdf\xaf\x03", "\x21");
	testAndSetBusyFlag();
	deleteEmvParameter(delparam.getStringData());

#ifndef PAM_BLOCKS
	LAST_COMMAND_TO_RESPOND last_command = getLastCommandToRespond();

	if(last_command != DELETE_EMV){
		log.message(MSG_INFO"deleteEmvParameter error occured "+UtilsIntToString(last_command)+"\n");
		return -1;
	}
#endif

	log.message(MSG_INFO"\n");

	BERTLV term_param;
	term_param.appendTag("\xdf\xaf\x01", "\x44");
	term_param.appendTag("\xdf\xaf\x03", "\x21");
	term_param.appendTag("\x5f\x36", "\x02");																// Terminal Currency Exponent
//	term_param.appendTag("\x5f\x2a", "\x08\x40"); //iq_dualcurr_23082017															// EMV Terminal Currency Code
	term_param.appendTag("\x9f\x1a", "\x04\x22");															// Terminal Country Code

	testAndSetBusyFlag();
	updateEmvParameter(term_param.getStringData());

#ifndef PAM_BLOCKS
	LAST_COMMAND_TO_RESPOND last_command = getLastCommandToRespond();

	if(last_command != UPDATE_EMV){
		log.message(MSG_INFO"updateEmvParameter error occured\n");
		return -1;
	}
#endif
	string txCurrencyTag, txCurrencyName;
	term_param.getValForTag("\x5f\x2a", txCurrencyTag);

	if(txCurrencyTag[0]==0x08 && txCurrencyTag[1]==0x40)
		txCurrencyName.append("840");
	else if(txCurrencyTag[0]==0x04 && txCurrencyTag[1]==0x22)
		txCurrencyName.append("422");


	log.message("Done\n");

	set<string> aids = cardcfg.getAids();

	for(std::set<string>::iterator it=aids.begin(); it!=aids.end(); ++it){
		CEmvData emvdata;
		cardcfg.lookupEmvConfig(*it, emvdata,txCurrencyName);

		CAccountProfile acc_prof = emvdata.getAccountProfile();
		CLimits local_limits = acc_prof.getLocalLimits();

		BERTLV param;
		param.appendTag("\xdf\xaf\x01", "\x44");															// Packet type
		param.appendTag("\xdf\xaf\x03", "\x21");															// Active interface=ICC
		param.appendTag("\x4f", UtilsStringToHex(emvdata.getAid()));										// AID/RID
		param.appendTag("\x9f\x09", UtilsStringToHex(emvdata.getApplicationVersion()));						// Terminal Application version no
		param.appendTag("\xdf\xaf\x12", UtilsStringToHex(emvdata.getApplicationVersion()));					// Terminal 2nd Application version no
		unsigned int uiamt = htonl(UtilsStringToInt(emvdata.getThresholdAmt()));
		param.appendTag("\xdf\xaf\x14", string((char*)&uiamt, sizeof(uiamt)));								// EMV Random Selection Threshold
		param.appendTag("\xdf\xaf\x15", UtilsStringToHex(emvdata.getTargetPercentage()));					// EMV Target random selection percentage
		param.appendTag("\xdf\xaf\x16", UtilsStringToHex(emvdata.getTargetPercentageMax()));				// EMV Max target random percentage
		param.appendTag("\xdf\xaf\x17", UtilsStringToHex(emvdata.getTacDefault()));							// TAC Default
		param.appendTag("\xdf\xaf\x18", UtilsStringToHex(emvdata.getTacDecline()));							// TAC Denial
		param.appendTag("\xdf\xaf\x19", UtilsStringToHex(emvdata.getTacOnline()));							// TAC Online/Other
		param.appendTag("\xdf\xaf\x1a", UtilsStringToHex(emvdata.getDefaultTdol()));						// Default TDOL
		param.appendTag("\xdf\xaf\x1b", UtilsStringToHex(emvdata.getDefaultDdol()));						// Default DDOL
//		param.appendTag("\xdf\x3e", string("\x00", 1));														// Set CDA Mode to 0

		if(emvdata.getPartialMatchingAllowed())
			param.appendTag("\xdf\xaf\x11", "\x01");														// Partial Selection enabled
		else
			param.appendTag("\xdf\xaf\x11", string("\x00", 1));												// Partial Selection disabled

		param.appendTag("\x9f\x33", UtilsStringToHex(termcfg.getEmvCapabilityBitMap().substr(15, 6)));		// EMV Terminal Capabilities
		param.appendTag("\x9f\x40", UtilsStringToHex(termcfg.getEmvCapabilityBitMap().substr(21, 10)));		// EMV Additional Terminal Capabilities
//		param.appendTag("\x9f\x15", UtilsStringToHex(termcfg.getMerchantCat()));							// EMV Merchant category code
//		param.appendTag("\x5f\x2a", UtilsStringToHex(termcfg.getCurrencyCode()));		//iq_dualcurr_23082017					// EMV Terminal Currency Code
		unsigned int uifloor = htonl(UtilsStringToInt(local_limits.getFloorLimitAmt()));
		param.appendTag("\x9f\x1b", string((char*)&uifloor, sizeof(uifloor)));								// EMV Floor limit
/*
		char currcode[5];
		UtilStringToBCD(termcfg.getCurrencyCode(), currcode);
//		param.appendTag("\x5f\x2a", string(currcode, 2));													// EMV Terminal Country Code
		param.appendTag("\x9f\x1a", string(currcode, 2));													// Terminal Country Code
*/

		char merchcat[5];
		UtilStringToBCD(termcfg.getMerchantCat(), merchcat);
		param.appendTag("\x9f\x15", string(merchcat, 2));													// EMV Merchant category code

		// Hardcoded values
//		param.appendTag("\x5f\x2a", string("\x08\x40", 2));													// EMV Terminal Country Code
		param.appendTag("\x9f\x1a", string("\x04\x22", 2));													// Terminal Country Code
		param.appendTag("\xdf\xaf\x1c", "\x01");															// Fallback allowed
		param.appendTag("\xdf\xaf\x1d", string("\x00", 1));													// Automatic Application Selection
		param.appendTag("\x5f\x36", "\x02");																// Terminal Currency Exponent

		// This feels dirty, needs to be defined external to the source code
		if(UtilsGetTerminalType() == TT_UX_300){
			log.message(MSG_INFO"Setting terminal type to 25\n");
			param.appendTag("\x9f\x35", "\x25");															// EMV Terminal Type 22-Attended; 25-Unattended
		}else{
			log.message(MSG_INFO"Setting terminal type to 22\n");
			param.appendTag("\x9f\x35", "\x22");															// EMV Terminal Type 22-Attended; 25-Unattended
		}

		/////////////////////////

		traceTags(param.getStringData());

		testAndSetBusyFlag();
		int ret = updateEmvParameter(param.getStringData());
#ifndef PAM_BLOCKS
		LAST_COMMAND_TO_RESPOND last_command = getLastCommandToRespond();

		if(last_command != UPDATE_EMV){
			log.message(MSG_INFO"updateEmvParameter error occured\n");
			return -1;
		}
#endif
		log.message(MSG_INFO"EMV Param update record returned "+UtilsIntToString(ret)+" for "+UtilsIntToString(param.getStringData().length())+" bytes ["+UtilsHexToString(param.getStringData().c_str(), param.getStringData().length())+"]\n");
	}

	// Update CAPK's
		std::map<int,CPublicKey> keys = cardcfg.getCapks();
		std::map<int,CPublicKey>::iterator it2;
		for (it2=keys.begin(); it2!=keys.end(); ++it2) {
			CPublicKey publicKey = it2->second;
			BERTLV capk;

			capk.appendTag("\xdf\xaf\x01", "\x41");												// Packet type
			capk.appendTag("\xdf\xaf\x03", "\x21");												// Active interface=ICC
			capk.appendTag("\x4f", UtilsStringToHex(publicKey.getRid()));							// AID/RID
			capk.appendTag("\x9f\x22", UtilsStringToHex(publicKey.getPublicKeyIndex()));		// CAPK index
			capk.appendTag("\xdf\xaf\x0e", UtilsStringToHex(publicKey.getModulus()));			// Modules
			capk.appendTag("\xdf\xaf\x0f", UtilsStringToHex(publicKey.getExponent()));			// Exponent
			capk.appendTag("\xdf\xaf\x10", UtilsStringToHex(publicKey.getCheckSum()));			// Checksum

			string expiry=publicKey.getExpiry();
			expiry.erase(4, 2);																	// Erase the century from expiry
			capk.appendTag("\xdf\xaf\x32", UtilsStringToHex(expiry));			// Expiry date ddmmyy

			traceTags(capk.getStringData());
			testAndSetBusyFlag();
			int iret = updateCapk(capk.getStringData());
#ifndef PAM_BLOCKS
			LAST_COMMAND_TO_RESPOND last_command = getLastCommandToRespond();

			if(last_command != UPDATE_CAPK){
				log.message(MSG_INFO"updateCapk error occured\n");
				return -1;
			}
#endif
			log.message(MSG_INFO"EMV CAPK update record returned "+UtilsIntToString(iret)+" for "+UtilsIntToString(capk.getStringData().length())+" bytes ["+UtilsHexToString(capk.getStringData().c_str(), capk.getStringData().length())+"]\n");
		}

	log.message(MSG_INFO"EmvParam::UpdateParameters() finished\n");

	return 0;
}
/*
int EmvParam::updateEmvParameterResult(string sStatusCode)
{
	log.message(MSG_INFO"updateEmvParameterResult(): sStatusCode="+UtilsHexToString(sStatusCode.c_str(), sStatusCode.length())+"\n");
	return 0;
}
*/
int EmvParam::updateCapkResult(string sStatusCode)
{
	log.message(MSG_INFO"updateCapkResult(): sStatusCode="+UtilsHexToString(sStatusCode.c_str(), sStatusCode.length())+"\n");
	return 0;
}

void EmvParam::traceTags(string tags)
{
	BERTLV tlv(tags);
	int pos=0;

	while(1){
		string tag, len, val;
		int ret;
		ret = tlv.getTagLenVal(pos, tag, len, val);

		if(ret){
			log.message(MSG_INFO+UtilsHexToString(tag.c_str(), tag.length())+" ("+UtilsHexToString(len.c_str(), len.length())+"):"+UtilsHexToString(val.c_str(), val.length())+"\n");
			pos = ret;
		}
		else
			break;
	}
}
