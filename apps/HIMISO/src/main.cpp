#include <string>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <libtui/tui.h>
#include <iostream>
#include <sys/types.h>
#include <sys/timeb.h>
#include <libda/cterminalconfig.h>
#include <libda/ccardconfig.h>
#include <libda/ccommsconfig.h>
#include <libda/chotcard.h>
#include <libipc/ipc.h>
#include <libda/cconmanager.h>
#include <libda/cbatchmanager.h>
#include <libda/cbatch.h>
#include <libda/cversions.h>
#include <libda/cversionsfile.h>
#include <libda/ctsn.h>

#include "CLog.h"
#include "Auth.h"
#include "void.h"
#include "Mti1604.h"
#include "Reversal.h"
#include "TermParam.h"
#include "CustomParam.h"
#include "DateTimeParam.h"
#include "CardParam.h"
#include "SuperTotals.h"
#include "SpecialBins.h"
#include "CommsParam.h"
#include "MerchMsg.h"
#include "AppBmp.h"
#include "SwDnld.h"
#include "Hotcards.h"
#include "TermAppSession.h"
#include "EmvParam.h"
#include "Banking.h"
#include "utils.h"
#include "bertlv.h"
#include "cstatus.h"
#include "values.h"
#include "signals.h"

#include <time.h>
#include <execinfo.h>
#include <signal.h>

//TODO: Comment this line out when EMV parameter import is fixed.
//#define DISABLE_EMV_IMPORT

//TODO: Comment this line out to prevent the terminal from deleting temporary parameter files
//#define KEEP_TEMP_FILES

//TODO: Comment this line out, to skip downloading of parameters and only import the temporary .dat files
//#define IMPORT_TEMPS_ONLY

#define HIMISO_TASK		"HIMISO"

static CLog log(HIMISO_TASK);

//using namespace std;
using namespace Utils;
using namespace za_co_verifone_tui;
using namespace za_co_verifone_banking;
using namespace com_verifone_status;
using namespace com_verifone_tsn;

int import_parameters(void);
int get_batchrec(CBatchRec &batchrec);
int send_reversal(void);
int get_last_complete_rec(CBatchRec& rec);
int get_last_complete_sale_rec(CBatchRec& rec);
int get_sale_rec(string ecr, CBatchRec& rec);
int comms_error(void);
void fault_handler(int signal);

pthread_t tVersion;

void *update_versions(void*)
{
	com_verifone_versions::CVersions versions;
	com_verifone_versionsfile::CVersionsFile vfile;

	log.message(MSG_INFO "HIMISO start writing versions\r\n");

	vfile.setName("");
	vfile.setVersion(HIMISO_VERSION);

	// Keep trying every second, until we succeed
	while(1){
		if(versions.addFile(HIMISO_NAME, vfile) == DB_OK)
			break;

		sleep(1);
	}

	log.message(MSG_INFO "HIMISO versions complete\r\n");

	return NULL;
}

int main(int argc, char *argv[])
{
	(void)argc;			// Suppresses -> warning: unused parameter
	(void)argv;			// Suppresses -> warning: unused parameter

	SignalsConfigureHandlers();

	log.message(MSG_INFO "HIMISO starting...\r\n");

//	pthread_create(&tVersion,NULL,&update_versions,NULL);

	signal(SIGSEGV, fault_handler);

	com_verifone_ipc::init(string(HIMISO_TASK));

	while(1){
		string ipcBuffer;
		string ipcFrom;

		if(com_verifone_ipc::receive(ipcBuffer,"",ipcFrom)==com_verifone_ipc::IPC_SUCCESS){
			log.message(MSG_INFO "Got message ["+UtilsHexToString((const char*)ipcBuffer.c_str(), ipcBuffer.length())+"]\n");

			if(ipcBuffer.find("Auth") != string::npos){
				int iret;
				bool got_pin_data=false;
				string pinblock;
				string ksn;

				// Check if we received pin data
				if(ipcBuffer.find("Auth:") != string::npos){
					za_co_verifone_bertlv::BERTLV pindata(ipcBuffer.substr(5));
					pindata.getValForTag("\xdf\x6c", pinblock);
					pindata.getValForTag("\xdf\x81\x03", ksn);
					got_pin_data = true;

					log.message(MSG_INFO "PIN block ["+UtilsHexToString((const char*)pinblock.c_str(), pinblock.length())+"]\n");
					log.message(MSG_INFO "      KSN ["+UtilsHexToString((const char*)ksn.c_str(), ksn.length())+"]\n");
				}

				if((iret = send_reversal()) < 0){
					// Check for comms error
					comms_error();

					log.message(MSG_INFO "Failed to send reversal\r\n");
				}else{
					CBatchRec rec;

					if(get_batchrec(rec) < 0){
						TermAppSession::End();		// Make sure the session with server is ended gracefully
						com_verifone_ipc::send("Error",ipcFrom);
						continue;
					}

					CTsn ctsn;
					rec.setTsn(ctsn.getNextTsn());

					if(got_pin_data){
						Auth auth(rec, pinblock, ksn);
						iret = auth.doAuth();
					}else{
						Auth auth(rec);
						iret = auth.doAuth();
					}
				}

				TermAppSession::End();		// Make sure the session with server is ended gracefully

				if(iret < 0){
					// Check for comms error
					int ret;
					ret=comms_error();

					if(ret==-2)
						com_verifone_ipc::send("ConnectError",ipcFrom);
					else if(ret==-3)
						com_verifone_ipc::send("Timeout",ipcFrom);
					else
						com_verifone_ipc::send("Error",ipcFrom);

					log.message(MSG_INFO "Auth error\r\n");
				}else{
					com_verifone_ipc::send("Finished",ipcFrom);
					log.message(MSG_INFO "Auth success\r\n");
				}

				log.message(MSG_INFO "Sent reply to ["+ipcFrom+"]\r\n");

			} else if(ipcBuffer.find("ProdLoad") != string::npos){
				CBatchRec rec;
				int iret;

				if((iret = send_reversal()) < 0){
					log.message(MSG_INFO "Failed to send reversal\r\n");
				}else{
					if(get_batchrec(rec) < 0){
						TermAppSession::End();		// Make sure the session with server is ended gracefully
						com_verifone_ipc::send("Error",ipcFrom);
						continue;
					}

					Mti1604 mtiCmd(rec);
					mtiCmd.doMtiCmd();
				}

				TermAppSession::End();		// Make sure the session with server is ended gracefully

				if(iret < 0){
					// Check for comms error
					int ret;
					ret=comms_error();

					if(ret==-2)
						com_verifone_ipc::send("ConnectError",ipcFrom);
					else if(ret==-3)
						com_verifone_ipc::send("Timeout",ipcFrom);
					else
						com_verifone_ipc::send("Error",ipcFrom);

					log.message(MSG_INFO "ProdLoad error\r\n");
				}else{
					com_verifone_ipc::send("Finished",ipcFrom);
					log.message(MSG_INFO "ProdLoad success\r\n");
				}

				log.message(MSG_INFO "Sent reply to ["+ipcFrom+"]\r\n");
			} else if(ipcBuffer.find("PinChange") != string::npos){
				CBatchRec rec;
				int iret;
				bool got_pin_data=false;
				string pinblock;
				string ksn;
				string new_pinblock;
				string new_ksn;

				// Check if we received pin data
				if(ipcBuffer.find("PinChange:") != string::npos){
					za_co_verifone_bertlv::BERTLV pindata(ipcBuffer.substr(10));
					pindata.getValForTag("\xdf\x6c", pinblock);
					pindata.getValForTag("\xdf\x81\x03", ksn);
					pindata.getValForTag("\xdf\x6d", new_pinblock);
					pindata.getValForTag("\xdf\x81\x04", new_ksn);
					got_pin_data = true;

					log.message(MSG_INFO "PIN block ["+UtilsHexToString((const char*)pinblock.c_str(), pinblock.length())+"]\n");
					log.message(MSG_INFO "      KSN ["+UtilsHexToString((const char*)ksn.c_str(), ksn.length())+"]\n");
					log.message(MSG_INFO "PIN b new ["+UtilsHexToString((const char*)new_pinblock.c_str(), new_pinblock.length())+"]\n");
					log.message(MSG_INFO "  new KSN ["+UtilsHexToString((const char*)new_ksn.c_str(), new_ksn.length())+"]\n");
				}

				if((iret = send_reversal()) < 0){
					log.message(MSG_INFO "Failed to send reversal\r\n");
				}else{
					if(get_batchrec(rec) < 0){
						TermAppSession::End();		// Make sure the session with server is ended gracefully
						com_verifone_ipc::send("Error",ipcFrom);
						continue;
					}

					if(got_pin_data){
						Mti1604 mtiCmd(rec, pinblock, ksn, new_pinblock,new_ksn);
						iret = mtiCmd.doMtiCmd();
					}
				}

				TermAppSession::End();		// Make sure the session with server is ended gracefully

				if(iret < 0){
					// Check for comms error
					int ret;
					ret=comms_error();

					if(ret==-2)
						com_verifone_ipc::send("ConnectError",ipcFrom);
					else if(ret==-3)
						com_verifone_ipc::send("Timeout",ipcFrom);
					else
						com_verifone_ipc::send("Error",ipcFrom);

					log.message(MSG_INFO "PinChange error\r\n");
				}else{
					com_verifone_ipc::send("Finished",ipcFrom);
					log.message(MSG_INFO "PinChange success\r\n");
				}

				log.message(MSG_INFO "Sent reply to ["+ipcFrom+"]\r\n");

			}else if(ipcBuffer.compare("ReverseLast") == 0){
				CBatchRec rec;
				int iret;

				if(get_last_complete_sale_rec(rec) < 0){
					TermAppSession::End();		// Make sure the session with server is ended gracefully
					com_verifone_ipc::send("Error",ipcFrom);
					continue;
				}

				if(rec.isTxCanceled()){
					iret = 0;
				}else{
					Reversal reverse(rec);
					iret = reverse.doReversal();
				}

				TermAppSession::End();		// Make sure the session with server is ended gracefully

				if(iret < 0){
					// Check for comms error
					int ret;
					ret=comms_error();

					if(ret==-2)
						com_verifone_ipc::send("ConnectError",ipcFrom);
					else if(ret==-3)
						com_verifone_ipc::send("Timeout",ipcFrom);
					else
						com_verifone_ipc::send("Error",ipcFrom);

					log.message(MSG_INFO "ReverseLast error\r\n");
				}else{
					com_verifone_ipc::send("Finished",ipcFrom);
					log.message(MSG_INFO "ReverseLast success\r\n");
				}

				log.message(MSG_INFO "Sent reply to ["+ipcFrom+"]\r\n");

			}
			else if(ipcBuffer.find("VoidSale") != string::npos){

				log.message(MSG_INFO "HimISo VoidSale \r\n");
				CBatchRec rec;
				int iret;
				if((iret = send_reversal()) < 0){
					// Check for comms error
					comms_error();
					log.message(MSG_INFO "Failed to send reversal\r\n");
				}else
				{
					string ecr;
					za_co_verifone_bertlv::BERTLV txData(ipcBuffer.substr(8));
					txData.getValForTag("\x6a", ecr);

					ecr=UtilsBcdToString((const char*)ecr.c_str(), (unsigned int)ecr.length());

					if(get_sale_rec(ecr, rec) < 0){
						TermAppSession::End();		// Make sure the session with server is ended gracefully
						com_verifone_ipc::send("Error",ipcFrom);
						continue;
					}

					if(rec.isTxVoided()){
						iret = 0;
					}else{
						Void voidTrx(rec);
						iret = voidTrx.doVoid();
					}
				}

				TermAppSession::End();		// Make sure the session with server is ended gracefully

				if(iret < 0){
					// Check for comms error
					int ret;
					ret=comms_error();

					if(ret==-2)
						com_verifone_ipc::send("ConnectError",ipcFrom);
					else if(ret==-3)
						com_verifone_ipc::send("Timeout",ipcFrom);
					else
						com_verifone_ipc::send("Error",ipcFrom);

					log.message(MSG_INFO "VoidSale error\r\n");
				}else{
					com_verifone_ipc::send("Finished",ipcFrom);
					log.message(MSG_INFO "VoidSale success\r\n");
				}

				log.message(MSG_INFO "Sent reply to ["+ipcFrom+"]\r\n");

			}
			else if(ipcBuffer.compare("ClearReversal") == 0){
				log.message(MSG_INFO "Checking for ClearReversal....\r\n");

				// Check if a reversal exists
				com_verifone_terminalconfig::CTerminalConfig termcfg;
				string tsn = termcfg.getReverselTsn();

				if((tsn.length() == 0) || (Utils::UtilsStringToInt(tsn) == 0)){
					log.message(MSG_INFO "No reversal Found.\r\n");
					com_verifone_ipc::send("Finished",ipcFrom);
					continue;
				}

				log.message(MSG_INFO "Found a reversal tsn "+tsn+"\r\n");

				// Look up the batch record based on the tsn
				com_verifone_batchmanager::CBatchManager batchManager;
				com_verifone_batch::CBatch cbatch;
				com_verifone_batchrec::CBatchRec batchrec;

				int ret = batchManager.getBatch(cbatch);

				if (ret != DB_OK){
					log.message(MSG_INFO "Failure retrieving batch\r\n");
					com_verifone_ipc::send("Error",ipcFrom);
					continue;
				}

				ret = cbatch.readRecFromBatch(batchrec, Utils::UtilsStringToInt(tsn));

				if (ret != DB_OK){
					log.message(MSG_INFO "Failure retrieving batch record\r\n");
					com_verifone_ipc::send("Error",ipcFrom);
					continue;
				}

				Reversal reversal(batchrec);
				int iret = reversal.clearReversal();

				if(iret >= 0){
					termcfg.setReverselTsn(0);
					log.message(MSG_INFO "Reversal tsn cleared\r\n");
				}

				if(iret < 0){
					com_verifone_ipc::send("Error",ipcFrom);
					log.message(MSG_INFO "ClearReversal error\r\n");
				}else{
					com_verifone_ipc::send("Finished",ipcFrom);
					log.message(MSG_INFO "ClearReversal success\r\n");
				}

				log.message(MSG_INFO "Sent reply to ["+ipcFrom+"]\r\n");

			}
			else if(ipcBuffer.compare("PendingReversal") == 0){
				int iret;

				if((iret = send_reversal()) < 0){
					log.message(MSG_INFO "Failed to send reversal\r\n");
				}

				TermAppSession::End();		// Make sure the session with server is ended gracefully

				if(iret < 0){
					// Check for comms error
					int ret;
					ret=comms_error();

					if(ret==-2)
						com_verifone_ipc::send("ConnectError",ipcFrom);
					else if(ret==-3)
						com_verifone_ipc::send("Timeout",ipcFrom);
					else
						com_verifone_ipc::send("Error",ipcFrom);

					log.message(MSG_INFO "Error sending pending reversal\r\n");
				}else{
					com_verifone_ipc::send("Finished",ipcFrom);
					log.message(MSG_INFO "Pending reversal sent\r\n");
				}

				log.message(MSG_INFO "Sent reply to ["+ipcFrom+"]\r\n");

			}else if((ipcBuffer.compare("ParmPart") == 0) || (ipcBuffer.compare("ParmFull") == 0)){
				bool full_dnld=false;

				if(ipcBuffer.compare("ParmFull") == 0)
					full_dnld = true;

				if(full_dnld)
					log.message(MSG_INFO "Full Parameter Download\r\n");
				else
					log.message(MSG_INFO "Partial Parameter Download\r\n");

				CStatus status;
				status.setStatus(PARM_DWNLD_TERMINAL);
#ifndef IMPORT_TEMPS_ONLY
				TermParam term;

				if(term.Download() < 0){
					TermAppSession::End();		// Make sure the session with server is ended gracefully

					// Check for comms error
					int ret;
					ret=comms_error();

					status.setStatus(PARM_DOWNLOAD_COMPLETE);
					status.statusClose();

					if(ret==-2)
						com_verifone_ipc::send("ConnectError",ipcFrom);
					else if(ret==-3)
						com_verifone_ipc::send("Timeout",ipcFrom);
					else
						com_verifone_ipc::send("Error",ipcFrom);

					log.message(MSG_INFO "Terminal Parameter Download failed\r\n");
					continue;
				}

				//status.setStatus(PARM_DWNLD_CUSTOM);
				
				CustomParam cprm;
				if(cprm.Download() < 0){
					TermAppSession::End();		// Make sure the session with server is ended gracefully

					// Check for comms error
					int ret;
					ret=comms_error();

					status.setStatus(PARM_DOWNLOAD_COMPLETE);
					status.statusClose();

					if(ret==-2)
						com_verifone_ipc::send("ConnectError",ipcFrom);
					else if(ret==-3)
						com_verifone_ipc::send("Timeout",ipcFrom);
					else
						com_verifone_ipc::send("Error",ipcFrom);

					log.message(MSG_INFO "Custom Parameter Download failed\r\n");
					continue;
				}

				//status.setStatus(PARM_DWNLD_DATE_TIME);
				
				DateTimeParam dt;
				if(dt.Download() < 0){
					TermAppSession::End();		// Make sure the session with server is ended gracefully

					// Check for comms error
					int ret;
					ret=comms_error();

					status.setStatus(PARM_DOWNLOAD_COMPLETE);
					status.statusClose();

					if(ret==-2)
						com_verifone_ipc::send("ConnectError",ipcFrom);
					else if(ret==-3)
						com_verifone_ipc::send("Timeout",ipcFrom);
					else
						com_verifone_ipc::send("Error",ipcFrom);

					log.message(MSG_INFO "Date/Time Parameter Download failed\r\n");
					com_verifone_ipc::send("Error",ipcFrom);
					continue;
				}
				
				//status.setStatus(PARM_DWNLD_CARDS);
				CardParam cp;

				if(full_dnld)
					cp.ClearVersion();

				if(cp.Download() < 0){
					TermAppSession::End();		// Make sure the session with server is ended gracefully

					cp.Cleanup();

					// Check for comms error
					comms_error();

					status.setStatus(PARM_DOWNLOAD_COMPLETE);
					status.statusClose();

					log.message(MSG_INFO "Card Parameter Download failed\r\n");
					com_verifone_ipc::send("Error",ipcFrom);
					continue;
				}

				SuperTotals sg;
				if(sg.Download() < 0){
					TermAppSession::End();		// Make sure the session with server is ended gracefully

					// Check for comms error
					comms_error();

					status.setStatus(PARM_DOWNLOAD_COMPLETE);
					status.statusClose();

					log.message(MSG_INFO "Super Totals Group Download failed\r\n");
					com_verifone_ipc::send("Error",ipcFrom);
					continue;
				}

				SpecialBins sb;
				if(sb.Download() < 0){
					TermAppSession::End();		// Make sure the session with server is ended gracefully

					// Check for comms error
					comms_error();

					status.setStatus(PARM_DOWNLOAD_COMPLETE);
					status.statusClose();

					log.message(MSG_INFO "Special Bins Download failed\r\n");
					com_verifone_ipc::send("Error",ipcFrom);
					continue;
				}

				//status.setStatus(PARM_DWNLD_COMMS);

				CommsParam cm;
				if(cm.Download() < 0){
					TermAppSession::End();		// Make sure the session with server is ended gracefully

					// Check for comms error
					comms_error();

					status.setStatus(PARM_DOWNLOAD_COMPLETE);
					status.statusClose();

					log.message(MSG_INFO "Comms Param Download failed\r\n");
					com_verifone_ipc::send("Error",ipcFrom);
					continue;
				}

				//status.setStatus(PARM_DWNLD_MERCHANT_MESSAGE);

				MerchMsg mm;
				if(mm.Download() < 0){
					TermAppSession::End();		// Make sure the session with server is ended gracefully

					// Check for comms error
					comms_error();

					status.setStatus(PARM_DOWNLOAD_COMPLETE);
					status.statusClose();

					log.message(MSG_INFO "Message to mechant Download failed\r\n");
					com_verifone_ipc::send("Error",ipcFrom);
					continue;
				}

				//status.setStatus(PARM_DWNLD_APP_BITMAP);

				AppBmp ab;
				if(ab.Download() < 0){
					TermAppSession::End();		// Make sure the session with server is ended gracefully

					// Check for comms error
					comms_error();

					status.setStatus(PARM_DOWNLOAD_COMPLETE);
					status.statusClose();

					log.message(MSG_INFO "Application Bitmap Download failed\r\n");
					com_verifone_ipc::send("Error",ipcFrom);
					continue;
				}
				
				//status.setStatus(PARM_DWNLD_SW_UPDATE);

				SwDnld sd;
				if(sd.Download() < 0){
					TermAppSession::End();		// Make sure the session with server is ended gracefully

					// Check for comms error
					comms_error();

					status.setStatus(PARM_DOWNLOAD_COMPLETE);
					status.statusClose();

					log.message(MSG_INFO "Software Download Parameter Download failed\r\n");
					com_verifone_ipc::send("Error",ipcFrom);
					continue;
				}

				//status.setStatus(PARM_DWNLD_HOTCARDS);

				log.message(MSG_INFO "HOTACTDS!!!!!\r\n");
				
					Hotcards hc(Hotcards::HCT_PARTIAL);

				if(full_dnld)
					hc.ClearVersion();

				if(hc.Download() < 0){
					TermAppSession::End();		// Make sure the session with server is ended gracefully

					// Check for comms error
					comms_error();

					status.setStatus(PARM_DOWNLOAD_COMPLETE);
					status.statusClose();

					log.message(MSG_INFO "Hotcards Download failed\r\n");
					com_verifone_ipc::send("Error",ipcFrom);
					continue;
				}

				TermAppSession::End();		// Make sure the session with server is ended gracefully

#endif // IMPORT_TEMPS_ONLY
				status.setStatus(PARM_IMPORTING);

				// Start the process of importing parameters into the database
				import_parameters();

				status.setStatus(PARM_DOWNLOAD_COMPLETE);
				status.statusClose();

				com_verifone_ipc::send("Finished",ipcFrom);
				log.message(MSG_INFO "Parameters success\r\n");

			}else if(ipcBuffer.compare("Bank") == 0){
				int iret=0;
				CStatus status;

				log.message(MSG_INFO "Batch Settlement command received\n");

				if((iret = send_reversal()) < 0){
					log.message(MSG_INFO "Failed to send reversal\r\n");
				}else{
					status.setStatus(BANKING_STARTED);
					Banking banking;
					iret = banking.DoBanking();

					status.setStatus(HOST_RESPONSE_RECEIVED);
				}

				string return_str = "Finished:";

				if(iret == 0)
					return_str.append("1");
				else
					return_str.append("0");

				TermAppSession::End();		// Make sure the session with server is ended gracefully

				// Check for comms error
				comms_error();

				status.setStatus(PARM_DOWNLOAD_COMPLETE);
				status.statusClose();

				com_verifone_ipc::send(return_str,ipcFrom);
				log.message(MSG_INFO "Banking complete\r\n");
			}
		}

		usleep(100000);
	}

	return 0;
}

int import_parameters(void)
{
	com_verifone_terminalconfig::CTerminalConfig terminalConfig;

	int ret=DB_OK;

	time_t tp_start;
	time_t tp_end;

	tp_start = time(0);

	log.message(MSG_INFO"Terminal Parameters import start\n");

	ret = terminalConfig.importTerminalConfig(TERM_PARM_PATH, ISO);

	if (ret != DB_OK)
		printf("Import Terminal Config Failed\n");
	else
		printf("Import Terminal Config Success\n");

	tp_end = time(0);

	time_t comms_start;
	time_t comms_end;

	comms_start = time(0);

	// Only import comms parameters if terminal; params were successful
	if(ret == DB_OK){
		log.message(MSG_INFO"Comms Parameters import start\n");

		com_verifone_commsconfig::CCommsConfig commsConfig;
		ret = commsConfig.importCommsConfig(COMMS_PARM_PATH,ISO);

		if (ret == DB_OK)
			printf("Import Comms Parameters Failed\n\n");
		else
			printf("Import Comms Parameters Success\n");
	}

	comms_end = time(0);

	log.message(MSG_INFO"Hotcard import start\n");

	time_t hc_start;
	time_t hc_end;

	hc_start = time(0);

	com_verifone_hotcard::CHotcard hotcards;
	ret = hotcards.importHotcards(HOTCARD_PATH,ISO,"HC_add.dat");
	if (ret != DB_OK)
		printf("Import Hotcards Failed\n");
	else
		printf("Import Hotcards Success\n");

	hc_end = time(0);

	time_t cp_start;
	time_t cp_end;

	cp_start = time(0);

	log.message(MSG_INFO"Card Parameters import start\n");

	com_verifone_cardconfig::CCardConfig cardConfig;
	ret = cardConfig.importCardConfig(CARD_PARM_PATH,ISO);

	if (ret == DB_NO_ACTION)
		printf("Import Card Parameters was already up to date\n\n");
	else if (ret == DB_OK)
		printf("Import Card Parameters Success\n");
	else
		printf("Import Card Parameters Failed\n");

	cp_end = time(0);

#ifndef DISABLE_EMV_IMPORT

	time_t emv_start;
	time_t emv_end;

	emv_start = time(0);

	// Only do EMV parameters if card parameters were successful
	if(ret == DB_OK){
		log.message(MSG_INFO"EMV Parameters import start\n");

		ret = cardConfig.importEmvConfig(EMV_PARM_PATH,ISO);

		if (ret == DB_NO_ACTION)
			printf("Import Emv Parameters was already up to date\n\n");
		else if (ret == DB_OK)
			printf("Import Emv Parameters Success\n");
		else
			printf("Import Emv Parameters Failed\n");
	}

	emv_end = time(0);

	time_t emv_kernel_start;
	time_t emv_kernel_end;

	emv_kernel_start = time(0);

	if(ret == DB_OK){
		log.message(MSG_INFO"EMV Kernel Parameters import start\n");

		EmvParam emvparam;
		emvparam.UpdateParameters();

		log.message(MSG_INFO"EMV Kernel Parameters import complete\n");
	}

	emv_kernel_end = time(0);

#endif // DISABLE_EMV_IMPORT

#ifndef KEEP_TEMP_FILES
	// Cleanup all temporary dat files
	AppBmp::Cleanup();
	AppBmp::Cleanup();
	CardParam::Cleanup();
	CommsParam::Cleanup();
	DateTimeParam::Cleanup();
	Hotcards::Cleanup();
	MerchMsg::Cleanup();
	SuperTotals::Cleanup();
	SpecialBins::Cleanup();
	SwDnld::Cleanup();
	TermParam::Cleanup();
#endif // KEEP_TEMP_FILES

	log.message(MSG_INFO"Terminal Parameter import time "+Utils::UtilsIntToString((tp_end-tp_start)/60)+" minutes "+Utils::UtilsIntToString((tp_end-tp_start)%60)+" seconds\n");
	log.message(MSG_INFO"Hotcard import time "+Utils::UtilsIntToString((hc_end-hc_start)/60)+" minutes "+Utils::UtilsIntToString((hc_end-hc_start)%60)+" seconds\n");
	log.message(MSG_INFO"Card Parameters import time "+Utils::UtilsIntToString((cp_end-cp_start)/60)+" minutes "+Utils::UtilsIntToString((cp_end-cp_start)%60)+" seconds\n");
#ifndef DISABLE_EMV_IMPORT
	log.message(MSG_INFO"EMV Parameters import time "+Utils::UtilsIntToString((emv_end-emv_start)/60)+" minutes "+Utils::UtilsIntToString((emv_end-emv_start)%60)+" seconds\n");
	log.message(MSG_INFO"EMV Kernel Parameters import time "+Utils::UtilsIntToString((emv_kernel_end-emv_kernel_start)/60)+" minutes "+Utils::UtilsIntToString((emv_kernel_end-emv_kernel_start)%60)+" seconds\n");
#endif // DISABLE_EMV_IMPORT
	log.message(MSG_INFO"Comms Parameters import time "+Utils::UtilsIntToString((comms_end-comms_start)/60)+" minutes "+Utils::UtilsIntToString((comms_end-comms_start)%60)+" seconds\n");

	return 0;
}

int get_batchrec(CBatchRec &batchrec)
{
	int ret;
	com_verifone_batchmanager::CBatchManager batchManager;
	com_verifone_batch::CBatch cbatch;

	ret = batchManager.getBatch(cbatch);

	if (ret != DB_OK){
		log.message(MSG_INFO "Failure retrieving batch record\r\n");
		return -1;
	}

	return cbatch.readLastRecFromBatch(batchrec);
}

int send_reversal(void)
{
	log.message(MSG_INFO "Checking for reversal....\r\n");

	// Check if a reversal exists
	com_verifone_terminalconfig::CTerminalConfig termcfg;

	string tsn = termcfg.getReverselTsn();

	if((tsn.length() == 0) || (Utils::UtilsStringToInt(tsn) == 0)){
		log.message(MSG_INFO "No reversal required.\r\n");
		return 0;			// No reversal to be sent
	}

	log.message(MSG_INFO "Found a reversal tsn "+tsn+"\r\n");

	// Look up the batch record based on the tsn
	com_verifone_batchmanager::CBatchManager batchManager;
	com_verifone_batch::CBatch cbatch;
	com_verifone_batchrec::CBatchRec batchrec;

	int ret = batchManager.getBatch(cbatch);

	if (ret != DB_OK){
		log.message(MSG_INFO "Failure retrieving batch\r\n");
		return -1;
	}

	ret = cbatch.readRecFromBatch(batchrec, Utils::UtilsStringToInt(tsn));

	if (ret != DB_OK){
		log.message(MSG_INFO "Failure retrieving batch record\r\n");
		return -1;
	}

	log.message(MSG_INFO "Sending reversal for TSN "+tsn+"\r\n");

	Reversal reversal(batchrec);
	int iret = reversal.doReversal();

	if(iret >= 0){
		termcfg.setReverselTsn(0);
		log.message(MSG_INFO "Reversal tsn cleared\r\n");
	}

	return iret;

}

/** This function is no longer used, it has now been replaced with the function
 * 	get_last_complete_sale_rec which only reverses the last financial transaction
 */
int get_last_complete_rec(CBatchRec& rec)
{
	com_verifone_batchmanager::CBatchManager batchManager;
	com_verifone_batch::CBatch cbatch;
	std::map<unsigned int, CBatchRec> batchRecs;

	batchManager.getBatch(cbatch);
	cbatch.getRecords(batchRecs);

	std::map<unsigned int,CBatchRec>::reverse_iterator itr;

	for (itr=batchRecs.rbegin(); itr!=batchRecs.rend(); ++itr) {
		CBatchRec batchRec = itr->second;

		if(!batchRec.isInProgress()){
			rec = batchRec;
			log.message(MSG_INFO "get_last_complete_rec - found it\n");
			return 0;
		}
	}

	log.message(MSG_INFO "get_last_complete_rec did not find a record\r\n");

	return -1;
}

//Get sale record based on ECR number
int get_sale_rec(string ecr, CBatchRec& rec)
{
  com_verifone_batchmanager::CBatchManager batchManager;
  com_verifone_batch::CBatch cbatch;
  std::map<unsigned int, CBatchRec> batchRecs;

  int iso_tx_code;
  batchManager.getBatch(cbatch);
  cbatch.getRecords(batchRecs);

  std::map<unsigned int,CBatchRec>::reverse_iterator itr;

  for (itr=batchRecs.rbegin(); itr!=batchRecs.rend(); ++itr) {
	CBatchRec batchRec = itr->second;

	string ecrRec=Utils::zero_pad(batchRec.getEcrNo(), 8);

	if(ecr==ecrRec &&!batchRec.isInProgress() && (!batchRec.isDeclined()) && !batchRec.isTxCanceled()/* &&
			UtilsStringToInt(batchRec.getRespCode()) == 0*/){
		iso_tx_code = UtilsStringToInt(batchRec.getTxCode());
		if( iso_tx_code == TX_ISO_SALE || iso_tx_code == TX_ISO_VOID /*|| iso_tx_code == TX_ISO_CASH_ADVANCE || iso_tx_code == TX_ISO_SALE_CASH ||
				iso_tx_code == TX_ISO_DEPOSIT || iso_tx_code == TX_ISO_PMNT_DEPOSIT*/ )	{
			  rec = batchRec;
			  log.message(MSG_INFO "iq: Record Found \n");
			  return 0;
		}
	}
  }

  log.message(MSG_INFO "get_sale_rec did not find a record\r\n");

  return -1;
}

int get_last_complete_sale_rec(CBatchRec& rec)
{
	com_verifone_batchmanager::CBatchManager batchManager;
	com_verifone_batch::CBatch cbatch;
	std::map<unsigned int, CBatchRec> batchRecs;

	int iso_tx_code;
	batchManager.getBatch(cbatch);
	cbatch.getRecords(batchRecs);

	std::map<unsigned int,CBatchRec>::reverse_iterator itr;

	for (itr=batchRecs.rbegin(); itr!=batchRecs.rend(); ++itr) {
		CBatchRec batchRec = itr->second;

		if(!batchRec.isInProgress() && UtilsStringToInt(batchRec.getRespCode()) == 0){
			iso_tx_code = UtilsStringToInt(batchRec.getTxCode());
			if( iso_tx_code == TX_ISO_SALE || iso_tx_code == TX_ISO_CASH_ADVANCE || iso_tx_code == TX_ISO_SALE_CASH ||
					iso_tx_code == TX_ISO_DEPOSIT || iso_tx_code == TX_ISO_PMNT_DEPOSIT )	{
					rec = batchRec;
					log.message(MSG_INFO "get_last_complete_sale_rec - found it\n");
					return 0;
			}

		}
	}

	log.message(MSG_INFO "get_last_complete_rec did not find a record\r\n");

	return -1;
}

int comms_error(void)
{
	CStatus status;

	TermAppSession::ERROR error = TermAppSession::GetLastError();

	switch(error)
	{
		case TermAppSession::CONNECT:
			log.message(MSG_INFO "Comms error - connecting.\r\n");
			status.setStatus(COMMS_ERROR_CONNECTING);
			return -2;
			break;
		case TermAppSession::SEND:
			log.message(MSG_INFO "Comms error - sending.\r\n");
			status.setStatus(COMMS_ERROR_SENDING);
			return -2;
			break;
		case TermAppSession::TIMEOUT:
			log.message(MSG_INFO "Comms error - Timeout.\r\n");
			status.setStatus(COMMS_TIMEOUT);
			return -3;
			break;
		case TermAppSession::NONE:
		default:
			log.message(MSG_INFO "Comms succesfull\r\n");
			break;
	}

	sleep(1);

	return 0;
}

void fault_handler(int signal)
{
	void *array[10];
	size_t size;

	switch(signal)
	{
		case 1: fprintf(stderr, "Error: SIGHUP	1	Hangup (POSIX)\n"); break;
		case 2: fprintf(stderr, "Error: SIGINT	2	Terminal interrupt (ANSI)\n"); break;
		case 3: fprintf(stderr, "Error: SIGQUIT	3	Terminal quit (POSIX)\n"); break;
		case 4: fprintf(stderr, "Error: SIGILL	4	Illegal instruction (ANSI)\n"); break;
		case 5: fprintf(stderr, "Error: SIGTRAP	5	Trace trap (POSIX)\n"); break;
		case 6: fprintf(stderr, "Error: SIGIOT	6	IOT Trap (4.2 BSD)\n"); break;
		case 7: fprintf(stderr, "Error: SIGBUS	7	BUS error (4.2 BSD)\n"); break;
		case 8: fprintf(stderr, "Error: SIGFPE	8	Floating point exception (ANSI)\n"); break;
		case 9: fprintf(stderr, "Error: SIGKILL	9	Kill(can't be caught or ignored) (POSIX)\n"); break;
		case 10: fprintf(stderr, "Error: SIGUSR1	10	User defined signal 1 (POSIX)\n"); break;
		case 11: fprintf(stderr, "Error: SIGSEGV	11	Invalid memory segment access (ANSI)\n"); break;
		case 12: fprintf(stderr, "Error: SIGUSR2	12	User defined signal 2 (POSIX)\n"); break;
		case 13: fprintf(stderr, "Error: SIGPIPE	13	Write on a pipe with no reader, Broken pipe (POSIX)\n"); break;
		case 14: fprintf(stderr, "Error: SIGALRM	14	Alarm clock (POSIX)\n"); break;
		case 15: fprintf(stderr, "Error: SIGTERM	15	Termination (ANSI)\n"); break;
		case 16: fprintf(stderr, "Error: SIGSTKFLT	16	Stack fault\n"); break;
		case 17: fprintf(stderr, "Error: SIGCHLD	17	Child process has stopped or exited, changed (POSIX)\n"); break;
		case 18: fprintf(stderr, "Error: SIGCONT	18	 Continue executing, if stopped (POSIX)\n"); break;
		case 19: fprintf(stderr, "Error: SIGSTOP	19	Stop executing(can't be caught or ignored) (POSIX)\n"); break;
		case 20: fprintf(stderr, "Error: SIGTSTP	20	Terminal stop signal (POSIX)\n"); break;
		case 21: fprintf(stderr, "Error: SIGTTIN	21	Background process trying to read, from TTY (POSIX)\n"); break;
		case 22: fprintf(stderr, "Error: SIGTTOU	22	Background process trying to write, to TTY (POSIX)\n"); break;
		case 23: fprintf(stderr, "Error: SIGURG	23	Urgent condition on socket (4.2 BSD)\n"); break;
		case 24: fprintf(stderr, "Error: SIGXCPU	24	CPU limit exceeded (4.2 BSD)\n"); break;
		case 25: fprintf(stderr, "Error: SIGXFSZ	25	File size limit exceeded (4.2 BSD)\n"); break;
		case 26: fprintf(stderr, "Error: SIGVTALRM	26	Virtual alarm clock (4.2 BSD)\n"); break;
		case 27: fprintf(stderr, "Error: SIGPROF	27	Profiling alarm clock (4.2 BSD)\n"); break;
		case 28: fprintf(stderr, "Error: SIGWINCH	28	Window size change (4.3 BSD, Sun)\n"); break;
		case 29: fprintf(stderr, "Error: SIGIO	29	I/O now possible (4.2 BSD)\n"); break;
		case 30: fprintf(stderr, "Error: SIGPWR	30	Power failure restart (System V)\n"); break;
	}

	// get void*'s for all entries on the stack
	size = backtrace(array, 10);

	// print out all the frames to stderr
	backtrace_symbols_fd(array, size, STDERR_FILENO);
	exit(1);
}
