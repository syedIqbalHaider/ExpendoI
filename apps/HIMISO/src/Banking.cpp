#include <libda/cconmanager.h>
#include <libda/cbatchmanager.h>
#include <libda/cbatch.h>
#include "Banking.h"
#include "TermAppSession.h"
#include "CLog.h"
#include "HIMISOver.h"
#include "Advice.h"
#include "utils.h"
#include "Recon.h"
#include "values.h"
#include <libda/creceiptno.h>

static CLog log(HIMISO_NAME);

using namespace Utils;

namespace za_co_verifone_banking
{

int Banking::DoBanking(void)
{
	log.message(MSG_INFO"Doing batch settlement\n");

	int ret;
	com_verifone_batchmanager::CBatchManager batchManager;
	com_verifone_batch::CBatch cbatch;

	ret = batchManager.getBatch(cbatch);

	if (ret != DB_OK){
		log.message(MSG_INFO "Failure retrieving batch record\r\n");
		return -1;
	}

	std::map<unsigned int, com_verifone_batchrec::CBatchRec> records;
	cbatch.getRecords(records);

	// Check that we have transactions to settle
	if(records.size() == 0){
		log.message(MSG_INFO "DoBanking(): Batch "+UtilsIntToString(cbatch.getBatchNo())+" is empty, nothing to do\n");
//		iq_audi_040117
//		return 0;
		return -1;
	}

	TermAppSession *termapp_session = TermAppSession::Establish(TermAppSession::SESSION_AUTH);

	if(termapp_session == NULL){
		log.message(MSG_INFO "DoBanking(): TermAppSession::Establish failed\r\n");
		return -1;
	}

	this->total_no_Upload_trns=0;

	std::map<unsigned int,CBatchRec>::iterator it;
	for (it=records.begin(); it!=records.end(); ++it) {
		CBatchRec batchRec = it->second;

		string tx_code = batchRec.getTxCode();
		int itx_code = UtilsStringToInt(tx_code);

// We dont send advice messages for online draft capture
		if(batchRec.getDraftCapMode() == ONLINE)
		{
			//	iq_audi_270217 start
			if(itx_code == TX_ISO_SALE && (!batchRec.isInProgress()) && (!batchRec.isDeclined())
					&& (!batchRec.isTxCanceled()))
				this->total_no_Upload_trns++;
			//	iq_audi_270217 end

			continue;
		}else
			continue;

		if((itx_code != TX_ISO_SALE))
			continue;

		// Make sure that only completed transactions are settled
		if((!batchRec.isInProgress()) && (!batchRec.isDeclined())&& (!batchRec.isTxCanceled())) {//LF && (batchRec.isApproveOnline() == false)){
			if(send_advice(batchRec) != 0){
				log.message(MSG_INFO "DoBanking(): Failed sending advice\r\n");
				return -1;
			}
		}
	}

	Recon recon;
	if(recon.SendReconAdvice("920000") < 0){
		log.message(MSG_INFO "DoBanking(): Failed sending Recond Advice\r\n");
		return -1;
	}

//	iq_audi_030117
//	if (recon.getActionCode() != 500 && recon.getActionCode() != 501 && recon.getActionCode() != 913) {
//		log.message(MSG_INFO "DoBanking(): Recond Advice was declined\r\n");
//	}

	//	iq_audi_030117
	if (recon.getActionCode() == 95) {
		log.message(MSG_INFO "DoBanking(): Recond Advice was declined\r\n");

//		com_verifone_receiptno::CReceiptNo crcptno;
//		crcptno.getNextReceiptNo();

		for (it=records.begin(); it!=records.end(); ++it) {
			CBatchRec batchRec = it->second;

//			// We dont send advice messages for online draft capture
//			if(batchRec.getDraftCapMode() == ONLINE) continue;

			string tx_code = batchRec.getTxCode();
			int itx_code = UtilsStringToInt(tx_code);

			if(itx_code != TX_ISO_SALE && batchRec.getDraftCapMode() == ONLINE)
				continue;

			// Make sure that only completed transactions are settled
			if((!batchRec.isInProgress()) && (!batchRec.isDeclined())&& (!batchRec.isTxCanceled())) {
				if(send_batch_up_load(batchRec) != 0){
					log.message(MSG_INFO "DoBanking(): Failed sending advice\r\n");
					return -1;
				}

//				com_verifone_receiptno::CReceiptNo crcptno;
//				crcptno.getNextReceiptNo();
			}
		}

		log.message(MSG_INFO "DoBanking(): Sending Again for Reconciliation\r\n");
		Recon reconAgain;
		if(reconAgain.SendReconAdvice("960000") < 0){
			log.message(MSG_INFO "DoBanking(): Failed sending Again Reconciliation\r\n");
			return -1;
		}

		if (reconAgain.getActionCode() != 0)
		{
			log.message(MSG_INFO "DoBanking(): Again Reconciliation failed \r\n");
			return -1;
		}
	}
	else if(recon.getActionCode() != 0)
	{
		log.message(MSG_INFO "DoBanking(): Reconciliation error BATCH no. not authorize \r\n");
		return -1;
	}

	log.message(MSG_INFO "DoBanking(): Recon Success!!!\r\n");

	// Close the batch
	com_verifone_batchmanager::CBatchManager batchman;
	batchManager.closeBatch();

	log.message(MSG_INFO "DoBanking(): Batch "+UtilsIntToString(cbatch.getBatchNo())+" closed\n");

	return 0;
}

int Banking::send_advice(com_verifone_batchrec::CBatchRec batchrec)
{
	log.message(MSG_INFO "Sending advise message\r\n");

	Advice advice(batchrec);

	if(advice.DoAdvice() < 0){
		log.message(MSG_INFO "Sending advise message failed!\r\n");
		return -1;
	}

	log.message(MSG_INFO "Advice: Auth Code   ["+advice.getAuthCode()+"]\r\n");
	log.message(MSG_INFO "Advice: Action Code ["+advice.getActionCode()+"]\r\n");
	log.message(MSG_INFO "Advice: RRN         ["+advice.getRRN()+"]\r\n");

	return 0;
}


int Banking::send_batch_up_load(com_verifone_batchrec::CBatchRec batchrec)
{
	log.message(MSG_INFO "Sending Batchup load message\r\n");

	Advice advice(batchrec);

	total_no_Upload_trns--;

	if(advice.DoBatchUpLoad(total_no_Upload_trns) < 0){
		log.message(MSG_INFO "Batchup load fail failed!\r\n");
		return -1;
	}

	log.message(MSG_INFO "Advice: Auth Code   ["+advice.getAuthCode()+"]\r\n");
	log.message(MSG_INFO "Advice: Action Code ["+advice.getActionCode()+"]\r\n");
	log.message(MSG_INFO "Advice: RRN         ["+advice.getRRN()+"]\r\n");

	return 0;
}


}
