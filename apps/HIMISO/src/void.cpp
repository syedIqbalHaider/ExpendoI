/*
 * void.cpp
 *
 *  Created on: May 10, 2017
 *      Author: vfisdk
 */
#include <sys/timeb.h>
#include <iomanip>
#include <sstream>
#include <libda/cterminalconfig.h>
#include <libipc/ipc.h>
#include <string.h>
#include <boost/algorithm/string.hpp>

#include "void.h"
#include "CLog.h"
#include "TermApp_8583.h"
#include "utils.h"
#include "TermAppSession.h"
#include "HIMISOver.h"
#include "values.h"
#include "libda/creceiptno.h"
#include <libda/ctsn.h>
#include <svcmgr/svc_utility.h>

static CLog log(HIMISO_NAME);

using namespace Utils;
using namespace com_verifone_terminalconfig;
using namespace za_co_verifone_bertlv;
using namespace com_verifone_receiptno;
using namespace com_verifone_tsn;

Void::Void(CBatchRec &cbatchrec):Message("Void", cbatchrec)
{
}

int Void::doVoid(void)
{
	log.message(MSG_INFO "Performing Void\n");

	unsigned char pan[37+1];
	unsigned int pan_len=0;
	char buffer[100];
	ISO_HANDLE isoh, isoh_resp;
	com_verifone_terminalconfig::CTerminalConfig termcfg;

	batchrec.setTxCode(UtilsIntToString(TX_ISO_VOID));

	int itx_type = UtilsStringToInt(batchrec.getTxCode());
	int iacc_type = UtilsStringToInt(batchrec.getAccType());
	int ito_acc=0;	// TODO: Where does this come from?
	std::string emv_CB_data;

	if(batchrec.getDraftCapMode() == ONLINE)
		isoh = IsoCreateList(MTI_1200);
	else
		isoh = IsoCreateList(MTI_1100);

	// Processing code
	if ((itx_type == TX_ISO_REFUND) || (itx_type == TX_ISO_DEPOSIT) || (itx_type == TX_ISO_CORRECTION) || (itx_type == TX_ISO_REFUND) || (itx_type == TX_ISO_PMNT_DEPOSIT))
		sprintf(buffer, "%02X%02d%02d", (int)itx_type, (int)iacc_type, (int)ito_acc);
	else if (itx_type == TX_ISO_TRANSFER)
		sprintf(buffer, "%02X%02d%02d", (int)itx_type, (int)iacc_type, (int)ito_acc);
	else
		sprintf(buffer, "%02X%02d%02d", (int)itx_type, (int)iacc_type, (int)ito_acc);

	IsoAddField(isoh, Processing_code, AddFieldNotApplicable, (unsigned char*)buffer, 6); 																// 3

	if(batchrec.getTxAmount()!=0.00) //iq_audi_231116
	{
		char stx_amount[12+1]={0};

		sprintf(stx_amount, "%012ld",batchrec.getTxAmount());
		IsoAddField(isoh, Amount_transaction, AddFieldNotApplicable, (unsigned char*) stx_amount, 12);								// 4
	}

	string datetime = batchrec.getTxDateTime();

	//	iq_audi_231116 comment field
	//	IsoAddField(isoh, Date_and_time_transmission, AddFieldNotApplicable, (unsigned char*) datetime.substr(4, 10).c_str(), 10);		// 7
	//	IsoAddField(isoh, Date_and_time_local_transaction, AddFieldNotApplicable, (unsigned char*) datetime.substr(2, 12).c_str(), 12);	// 12
	IsoAddField(isoh, Date_and_time_local_transaction, AddFieldNotApplicable, (unsigned char*) datetime.substr(8, 12).c_str(), 6);	// 12

	//	iq_audi_231116
	IsoAddField(isoh, Date_local_transaction, AddFieldNotApplicable, (unsigned char*) datetime.substr(4, 8).c_str(), 4);	//13

	CTsn ctsn;
	char stan[6+1]={0};
	sprintf(stan, "%06d",ctsn.getNextTsn());
	IsoAddField(isoh, System_trace_audit_number, AddFieldNotApplicable, (unsigned char*)stan,6);

	string card_input_mode;
	card_input_mode.clear();
	PanEntryMode entry_mode=(PanEntryMode)batchrec.getPanEntryMode();

	if ((entry_mode == PEM_SWIPED)  ) {
		card_input_mode.append("02");
	} else if (entry_mode == PEM_ENTERED) {
		card_input_mode.append("01");
	} else if (entry_mode == PEM_INSERTED) {
		card_input_mode.append("05");
	} else if (entry_mode == PEM_TAPPED) {
		card_input_mode.append("07");
	} else if (entry_mode == PEM_FALLBACK) {
		card_input_mode.append("80");
	}

	CARD_AUTH_METHOD cardhAuthMethod=(CARD_AUTH_METHOD)batchrec.getCardhAuthMethod();

	if(cardhAuthMethod==PIN)
		card_input_mode.append("1");
	else
		card_input_mode.append("0");

	IsoAddField(isoh, POS_data_code, AddFieldNotApplicable, (unsigned char*) card_input_mode.c_str(), 3);											// 22

	//	iq_audi_291116
	string str_tpdu = termcfg.getTpdu();

	if(batchrec.getDraftCapMode() == ONLINE)
		IsoAddField(isoh, Function_code, AddFieldNotApplicable, (unsigned char*)str_tpdu.substr(3, 6).c_str(), 3);															// 24
	else
		IsoAddField(isoh, Function_code, AddFieldNotApplicable, (unsigned char*)str_tpdu.substr(3, 6).c_str(), 3);															// 24

	IsoAddField(isoh, Message_reason_code, AddFieldNotApplicable, (unsigned char*)"00", 2);		// 25

	com_verifone_batchmanager::CBatchManager batchManager;
	com_verifone_batch::CBatch cbatch;

	int ret = batchManager.getBatch(cbatch);

	if (ret != 1){
		log.message(MSG_INFO ":Failure retrieving batch number\r\n");
		return -1;
	}

	//iq_audi_231116 comment field
	//	string str_batchno = UtilsIntToString(cbatch.getBatchNo());
	//	IsoAddField(isoh, Reconciliation_indicator, AddFieldNotApplicable, (unsigned char*) str_batchno.c_str(), str_batchno.length());						// 29


	if ((entry_mode == PEM_SWIPED) || (entry_mode == PEM_FALLBACK)) {
//		iq_audi_241116 start
		string track2=batchrec.getTrack2();

		uint pos = track2.find('=', 0);
		track2.replace(pos, 1, 1, 'D');

//		memset(pan,0,sizeof(pan));
//		pan_len=track2.length();
//		memcpy(pan,track2.c_str(),pan_len);
//		IsoAddField(isoh, Track_2_data, AddFieldNotApplicable, (unsigned char*)pan,pan_len);

		string strPan;
		strPan=track2.substr(0,track2.find("D"));
		pan_len=strPan.length();

		IsoAddField(isoh, Primary_account_number, AddFieldNotApplicable, (unsigned char*)strPan.c_str(), pan_len);		// 2
		IsoAddField(isoh, Date_expiration, AddFieldNotApplicable, (unsigned char*)track2.substr(pos+1,pos+5).c_str(), 4);	// 14
	}

	if (entry_mode == PEM_INSERTED){
		string emv_track2;
		BuildEmvTrack2(batchrec.getEmvTags(), emv_track2);

//		iq_audi_241116 start
		uint pos = emv_track2.find('=', 0);
		emv_track2.replace(pos, 1, 1, 'D');

//		memset(pan,0,sizeof(pan));
//		pan_len=emv_track2.length();
//		memcpy(pan,emv_track2.c_str(),pan_len);
//		IsoAddField(isoh, Track_2_data, AddFieldNotApplicable, (unsigned char*)pan,pan_len);				// 35

		batchrec.setPan(emv_track2.substr(0,emv_track2.find("D")));

//		iq_audi_241116
		memset(pan,0,sizeof(pan));
		pan_len=batchrec.getPan().length();
		memcpy(pan,batchrec.getPan().c_str(),pan_len);

		IsoAddField(isoh, Primary_account_number, AddFieldNotApplicable, (unsigned char*)pan, pan_len);		// 2
		IsoAddField(isoh, Date_expiration, AddFieldNotApplicable, (unsigned char*)emv_track2.substr(pos+1,pos+5).c_str(), 4);	// 14
	}

	log.message(MSG_INFO "\r\n");
	// Note: below will truncate termid if it is longer than int size - is it safe?
	long itid = (long)UtilsStringToInt(termcfg.getTerminalNo());
	string rrn("P");
	rrn.append(UtilsBase62Encode(itid));
	rrn.append(zero_pad(cbatch.getBatchNo(), 3));
	log.message(MSG_INFO "\r\n");
	std::string sTsn = UtilsIntToString(batchrec.getTsn());
	if(sTsn.length() < 3){
		string tsn = zero_pad(sTsn, 3);
		rrn.append(tsn);
	}else
		rrn.append(sTsn.substr((unsigned int)(sTsn.length()-3), 3));
	log.message(MSG_INFO "\r\n");

	// 		iq_audi_231116 comment field
	//	IsoAddField(isoh, Retrieval_reference_number, AddFieldNotApplicable, (unsigned char*)rrn.c_str(), rrn.length());									// 37

	string str_tid = space_pad(termcfg.getTerminalNo(), 8);
	IsoAddField(isoh, Terminal_ID, AddFieldNotApplicable, (unsigned char*)str_tid.c_str(), str_tid.length());											// 41

	string str_merchno;
	str_merchno.insert(0, 15, '0');
	str_merchno.replace(15-termcfg.getMerchantNo().length(), termcfg.getMerchantNo().length(), termcfg.getMerchantNo());
	IsoAddField(isoh, Card_acceptor_ID, AddFieldNotApplicable, (unsigned char*)str_merchno.c_str(), str_merchno.length());								// 42

	// Manual card entry
	if(entry_mode == PEM_ENTERED) {
	//		iq_audi_241116
		memset(pan,0,sizeof(pan));
		pan_len=batchrec.getPan().length();
		memcpy(pan,batchrec.getPan().c_str(),pan_len);

		IsoAddField(isoh, Primary_account_number, AddFieldNotApplicable, (unsigned char*)pan, pan_len);		// 2

	//		IsoAddField(isoh, Primary_account_number, AddFieldNotApplicable, (unsigned char*)batchrec.getPan().c_str(), batchrec.getPan().length());		// 2
		IsoAddField(isoh, Date_expiration, AddFieldNotApplicable, (unsigned char*)batchrec.getExp().c_str(), batchrec.getExp().length());				// 14

	//		iq_audi_231116 comment field
	//		IsoAddField(isoh, Additional_data_private, Card_verification_data, (unsigned char*)batchrec.getCvv().c_str(), batchrec.getCvv().length());		// 48.3
	}


	//RRN
	IsoAddField(isoh, Retrieval_reference_number, AddFieldNotApplicable, (unsigned char*)batchrec.getRrn().c_str(), 12);	//37

	string f49enable;
	f49enable=termcfg.getF49Enable();
	log.message(MSG_INFO "F49 enable ["+f49enable+"]\r\n");
	if(atoi(f49enable.c_str())==1)
		IsoAddField(isoh, Currency_code_transaction, AddFieldNotApplicable, (unsigned char*)batchrec.getTransCurrency().c_str(), 3);														// 49

	if (batchrec.getCashAmount() > 0) {
		sprintf(buffer, "%02d40710C%012d", (unsigned int)UtilsStringToInt(batchrec.getAccType()), (unsigned int)batchrec.getCashAmount());
		IsoAddField(isoh, Additional_amounts, AddFieldNotApplicable, (unsigned char*) buffer, strlen(buffer));											// 54
	}

	if(entry_mode == PEM_INSERTED){

	////		iq_audi_241116 start
	//		memset(pan,0,sizeof(pan));
	//		pan_len=batchrec.getPan().length();
	//		memcpy(pan,batchrec.getPan().c_str(),pan_len);
	//
	//		if(pan_len%2)
	//		{
	//			strcat((char *)pan,"F");
	//			pan_len++;
	//		}
	////		iq_audi_241116 end
	//
	//		IsoAddField(isoh, Primary_account_number, AddFieldNotApplicable, (unsigned char*)pan, pan_len);		// 2

		string emv_tags = batchrec.getEmvTags();
		FilterEMVTags(batchrec.getEmvTags(), emv_tags);

		// check for tag 0x5f34
		BERTLV emvTags(emv_tags);
		string crdseq;
		if(emvTags.getValForTag("\x5f\x34", crdseq)){
			string ascii_crdseq = UtilsHexToString((char*)crdseq.c_str(), crdseq.length());

		if(ascii_crdseq.length() < 3)
			ascii_crdseq.insert(0, 3-ascii_crdseq.length(), '0');

	//			iq_audi_231116 comment field
	//			IsoAddField(isoh, Card_sequence_number, AddFieldNotApplicable, (unsigned char*)ascii_crdseq.c_str(), 3);														// 23
		}

		string sicc_data;
		BuildICCData(emv_tags, sicc_data);
	//		za_co_verifone_bertlv::BERTLV iccdata;
	//		iccdata.appendTag("\xff\x20", sicc_data);
	//
	//		log.message(MSG_INFO"DBG ICCDATA length "+UtilsIntToString(iccdata.getStringData().length())+" ["+UtilsHexToString(iccdata.getStringData().data(), iccdata.getStringData().length())+"]\n");
	//
	//		TraceTags(sicc_data);
	//		IsoAddField(isoh, ICC_data, AddFieldNotApplicable, (unsigned char*)iccdata.getStringData().data(), iccdata.getStringData().length());										// 55

	//		iq_audi_281116
		log.message(MSG_INFO"DBG ICCDATA length "+UtilsIntToString(sicc_data.length())+" ["+UtilsHexToString(sicc_data.c_str(), sicc_data.length())+"]\n");
		TraceTags(sicc_data);
		IsoAddField(isoh, ICC_data, AddFieldNotApplicable, (unsigned char*)sicc_data.c_str(), sicc_data.length());										// 55
	}

	//	iq_audi_311216
	//	com_verifone_receiptno::CReceiptNo recpt;
	//	char sequenceNo[6+1] ={0};
	//	sprintf(sequenceNo,"%06d",recpt.getCurrentReceiptNo());

	string str_tsn = zero_pad(batchrec.getTsn(), 6);
	IsoAddField(isoh, Hotcard_capacity , AddFieldNotApplicable, (unsigned char*)str_tsn.c_str(), str_tsn.length());								// 11

	unsigned char *iso_msg;
	unsigned int iso_msg_len;
	iso_msg_len = IsoBuildMsg(isoh, (unsigned char**) &iso_msg);

	IsoDestroyList(&isoh);

	if(iso_msg_len == 0){
		log.message(MSG_INFO "IsoBuildMsg failed\r\n");
		return -1;
	}

	string str_iso_msg = string((const char*)iso_msg, iso_msg_len);

	free(iso_msg);

	log.message(MSG_INFO "Void ("+UtilsIntToString(str_iso_msg.length())+" bytes) ["+UtilsHexToString(str_iso_msg.c_str(), str_iso_msg.length())+"]\r\n");

	string response;

	if(send_message_auth(str_iso_msg, response, true) < 0){
		log.message(MSG_INFO "Void failed\r\n");

	//		LogReversal();
		return -1;
	}

	log.message(MSG_INFO "Void response ("+UtilsIntToString(response.length())+" bytes) ["+UtilsHexToString(response.c_str(), response.length())+"]\r\n");

	// Clear the reversal tsn, since clearly we are still here, nothing bad happened.
	termcfg.setReverselTsn(0);
	batchrec.setInProgress(true);	// Reset the temporary complete flag
	update_batchrec(batchrec);

	isoh_resp = TaIsoDecode((unsigned char*) response.c_str(), response.length());

	if(isoh_resp < 0){
		log.message(MSG_INFO "TaIsoDecode failed with "+UtilsIntToString(isoh_resp)+"\r\n");
		return -1;
	}

	char *field_data=NULL;
	unsigned int field_len;

	if(IsoGetField(isoh_resp, Retrieval_reference_number, AddFieldNotApplicable, (unsigned char**) &field_data, &field_len) < 0){
		log.message(MSG_INFO "IsoGetField failed to get Retrieval_reference_number\r\n");
		IsoDestroyList(&isoh_resp);
		return -1;
	}else{
		string rrn = string(field_data, field_len);
		batchrec.setRrn(rrn);
		log.message(MSG_INFO "RRN ["+rrn+"]\r\n");
	}
	free(field_data);

	if(IsoGetField(isoh_resp, Approval_code, AddFieldNotApplicable, (unsigned char**) &field_data, &field_len) < 0){
		log.message(MSG_INFO "IsoGetField failed to get Approval_code\r\n");
		batchrec.setAuthCode(string(""));
	}else{
		string approvalcode = string(field_data, field_len);
		batchrec.setAuthCode(approvalcode);
		log.message(MSG_INFO "Approval code ["+approvalcode+"]\r\n");
	}
	free(field_data);

	if(IsoGetField(isoh_resp, Action_code, AddFieldNotApplicable, (unsigned char**) &field_data, &field_len) < 0){
		log.message(MSG_INFO "IsoGetField failed to get Action_code\r\n");
		IsoDestroyList(&isoh_resp);
		return -1;
	}else{
		string actioncode = MapActionCode(string(field_data, field_len));
		batchrec.setRespCode(actioncode);
		log.message(MSG_INFO "Action code ["+string(field_data, field_len)+"] mapped --> ["+actioncode+"]\r\n");
	}
	free(field_data);

	//iq_audi_291216  get time of response
	string time ;

	if(IsoGetField(isoh_resp, Date_and_time_local_transaction, AddFieldNotApplicable, (unsigned char**) &field_data, &field_len) < 0){
		log.message(MSG_INFO "IsoGetField failed to get Time\r\n");
		IsoDestroyList(&isoh_resp);
		return -1;
	}else{
		time = UtilsBcdToString((const char *)field_data, field_len);
		log.message(MSG_INFO "Time ["+time+"]\r\n");
	}
	free(field_data);

	//iq_audi_291216  update date and time of response
	if(IsoGetField(isoh_resp, Date_local_transaction, AddFieldNotApplicable, (unsigned char**) &field_data, &field_len) < 0){
		log.message(MSG_INFO "IsoGetField failed to get Date\r\n");
		IsoDestroyList(&isoh_resp);
		return -1;
	}else{
		string date =  UtilsBcdToString((const char *)field_data, field_len);
		string dateTime=batchrec.getTxDateTime();

		dateTime.replace(4,date.length(),date);
		dateTime.replace(8,time.length(),time);

		batchrec.setTxDateTime(dateTime);
		log.message(MSG_INFO "Date ["+date+"]\r\n");
		log.message(MSG_INFO "Updated DateTime ["+batchrec.getTxDateTime()+"]\r\n");


		struct utilityDateTime dt;

		memset(&dt, 0, sizeof(struct utilityDateTime));

		dt.tm_sec = Utils::UtilsStringToInt(dateTime.substr(12, 2));	// seconds
		dt.tm_min = Utils::UtilsStringToInt(dateTime.substr(10, 2));	// minutes
		dt.tm_hour = Utils::UtilsStringToInt(dateTime.substr(8, 2));    // hours
		dt.tm_mday = Utils::UtilsStringToInt(dateTime.substr(6, 2));    // day of the month
		dt.tm_mon = Utils::UtilsStringToInt(dateTime.substr(4, 2));     // month
		dt.tm_year = Utils::UtilsStringToInt(dateTime.substr(0, 4));    // year

		utility_settime(&dt);
	}
	free(field_data);

	IsoDestroyList(&isoh_resp);

	if( UtilsStringToInt(batchrec.getRespCode()) == 0){

		batchrec.setTxVoided(true);	// Mark the transaction as Void
		batchrec.setInProgress(false);	// Mark the transaction as complete
		update_batchrec(batchrec);
	}else
	{
		batchrec.setInProgress(false);
		update_batchrec(batchrec);
		log.message(MSG_INFO "iq: Void failed Due Response Code !=0 \r\n");
		return -1;
	}

	return 0;
}
