#include <libda/cterminalconfig.h>
#include "DeviceInfo.h"
#include "HIMISOver.h"

extern "C" {
	#include <platforminfo_api.h>
}

string DeviceInfo::ProtocolVersion()
{
	return "01";
}
#include <iostream>
string DeviceInfo::TermSerial()
{
	char value[255];
	int result;
	unsigned long rsize=0;

	result = platforminfo_get(PI_SERIAL_NUM ,value, sizeof(value), &rsize);

	if(result == PI_OK){
		string raw = string(value, rsize);

		size_t pos;
		while((pos = raw.find_first_of('-')) != string::npos)
			raw.erase(pos, 1);

		// Fore some reason the serial contains trailing spaces. lose them.
		while((pos = raw.find_first_of(' ')) != string::npos)
			raw.erase(pos, 1);

		if(raw.length() > 8)
			return raw.substr(raw.length()-8, 8);

		return raw;
	}

	return "";
}

string DeviceInfo::AppId()
{
	return HIMISO_VERSION;
}

string DeviceInfo::AppLinkTimeStamp()
{
	return "201404011200";
}
