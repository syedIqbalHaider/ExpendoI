#include <iomanip>
#include <sstream>
#include <libda/cconmanager.h>
#include <libda/cterminalconfig.h>
#include <libda/cbatchmanager.h>
#include <libda/cbatch.h>
#include "utils.h"
#include "TermAppSession.h"
#include "HIMISOver.h"
#include "values.h"
#include "Message.h"
#include "CLog.h"
#include "bertlv.h"
#include "cstatus.h"

static CLog log(HIMISO_NAME);

using namespace Utils;
using namespace za_co_verifone_bertlv;
using namespace com_verifone_terminalconfig;

int Message::send_message_auth(std::string &auth, std::string &response, bool isReverse)
{
	com_verifone_status::CStatus status;
	com_verifone_terminalconfig::CTerminalConfig termcfg;

	TermAppSession *termapp_session = TermAppSession::Establish(TermAppSession::SESSION_AUTH);

	if(termapp_session == NULL){
		log.message(MSG_INFO+message_name+": TermAppSession::Establish failed\r\n");
		status.setStatus(com_verifone_status::COMMS_ERROR);
		return -1;
	}

	status.setStatus(com_verifone_status::TRANSACTION_GOING_ONLINE);

	if(termapp_session->Send(auth) <= 0){
		log.message(MSG_INFO+message_name+": termapp_session->Send failed\r\n");
		status.setStatus(com_verifone_status::COMMS_ERROR);
		return -1;
	}

// Store the tsn of this transaction, for reversal purposes, in case the power goes out
	if(isReverse)
	{
		termcfg.setReverselTsn(batchrec.getTsn());
		batchrec.setInProgress(false);	// This will temporarily be marked as complete
		update_batchrec(batchrec);
	}

	status.setStatus(com_verifone_status::WAITING_FOR_RESPONSE);

	if(termapp_session->Receive(response) <= 0){
		log.message(MSG_INFO+message_name+": termapp_session->Receive failed\r\n");
		status.setStatus(com_verifone_status::COMMS_ERROR);
		return -1;
	}

	status.setStatus(com_verifone_status::HOST_RESPONSE_RECEIVED);

	return 0;
}

int Message::send_message_param(std::string &auth, std::string &response)
{
	com_verifone_status::CStatus status;

	TermAppSession *termapp_session = TermAppSession::Establish(TermAppSession::SESSION_PARAM);

	if(termapp_session == NULL){
		log.message(MSG_INFO+message_name+": TermAppSession::Establish failed\r\n");
		status.setStatus(com_verifone_status::COMMS_ERROR);
		return -1;
	}

	status.setStatus(com_verifone_status::TRANSACTION_GOING_ONLINE);

	if(termapp_session->Send(auth) <= 0){
		log.message(MSG_INFO+message_name+": termapp_session->Send\r\n");
		status.setStatus(com_verifone_status::COMMS_ERROR);
		return -2;
	}

	status.setStatus(com_verifone_status::WAITING_FOR_RESPONSE);

	if(termapp_session->Receive(response) <= 0){
		log.message(MSG_INFO+message_name+": termapp_session->Receive\r\n");
		status.setStatus(com_verifone_status::COMMS_ERROR);
		return -3;
	}

	return 0;
}

string Message::zero_pad(int num, int pad_count)
{
	std::ostringstream ss;
	ss << std::setw(pad_count) << std::setfill('0') << num;
	return ss.str();
}

string Message::zero_pad(string str, int pad_count)
{
	std::ostringstream ss;
	ss << std::setw(pad_count) << std::setfill('0') << str;
	return ss.str();
}

string Message::space_fill(int pad_count)
{
	std::ostringstream ss;
	ss << std::setw(pad_count) << std::setfill(' ');
	return ss.str();
}

string Message::space_pad(string str, int pad_count)
{
	std::ostringstream ss;
	ss << std::setw(pad_count) << std::setfill(' ') << str;
	return ss.str();
}

int Message::update_batchrec(CBatchRec &batchrec)
{
	if(!use_batchrec)
		return -1;

	int ret;
	com_verifone_batchmanager::CBatchManager batchManager;
	com_verifone_batch::CBatch cbatch;

	ret = batchManager.getBatch(cbatch);

	if (ret != DB_OK){
		log.message(MSG_INFO+message_name+":Failure retrieving batch record\r\n");
		return -1;
	}

	return cbatch.writeRecToBatch(batchrec);
}

void Message::BuildICCData(string tags, string &icc_data)
{
	BERTLV tlv(tags);
	BERTLV iccdata_tlv;
	string value;
	CTerminalConfig termcfg;

	if(tlv.getValForTag("\x9f\x02", value)){
		iccdata_tlv.appendTag("\x9f\x02", value);
	}
	if(tlv.getValForTag("\x9f\x03", value)){
		iccdata_tlv.appendTag("\x9f\x03", value);
	}
	//iq_audi_180117
//	if(tlv.getValForTag("\x4f", value)){
//		iccdata_tlv.appendTag("\x4f", value);
//	}
	if(tlv.getValForTag("\x82", value)){
		iccdata_tlv.appendTag("\x82", value);
	}
	if(tlv.getValForTag("\x9f\x36", value)){
		iccdata_tlv.appendTag("\x9f\x36", value);
	}
//	if(tlv.getValForTag("\x9f\x07", value)){
//		iccdata_tlv.appendTag("\x9f\x07", value);
//	}
//	if(tlv.getValForTag("\x8a", value)){
//		iccdata_tlv.appendTag("\x8a", value);
//	}
	if(tlv.getValForTag("\xdf\x01", value)){
		iccdata_tlv.appendTag("\xdf\x01", value);
	}
	if(tlv.getValForTag("\xdf\x02", value)){
		iccdata_tlv.appendTag("\xdf\x02", value);
	}
	if(tlv.getValForTag("\x9f\x26", value)){
		iccdata_tlv.appendTag("\x9f\x26", value);
	}
	if(tlv.getValForTag("\x9f\x27", value)){
		iccdata_tlv.appendTag("\x9f\x27", value);
	}
	//iq_audi_180117
//	if(tlv.getValForTag("\x8e", value)){
//		iccdata_tlv.appendTag("\x8e", value);
//	}
//	if(tlv.getValForTag("\x9f\x0d", value)){
//		iccdata_tlv.appendTag("\x9f\x0d", value);
//	}
//	if(tlv.getValForTag("\x9f\x0e", value)){
//		iccdata_tlv.appendTag("\x9f\x0e", value);
//	}
//	if(tlv.getValForTag("\x9f\x0f", value)){
//		iccdata_tlv.appendTag("\x9f\x0f", value);
//	}
	if(tlv.getValForTag("\x9f\x34", value)){
		iccdata_tlv.appendTag("\x9f\x34", value);
	}
	if(tlv.getValForTag("\x9f\x1e", value)){
		iccdata_tlv.appendTag("\x9f\x1e", value);
	}
	if(tlv.getValForTag("\x9f\x10", value)){
		iccdata_tlv.appendTag("\x9f\x10", value);
	}
	if(tlv.getValForTag("\xdf\x09", value)){
		iccdata_tlv.appendTag("\xdf\x09", value);
	}
	if(tlv.getValForTag("\x9f\x09", value)){
		iccdata_tlv.appendTag("\x9f\x09", value);
	}

	if(tlv.getValForTag("\x9f\x33", value)){
		iccdata_tlv.appendTag("\x9f\x33", value);
	}else
	iccdata_tlv.appendTag("\x9f\x33", UtilsStringToHex(termcfg.getEmvCapabilityBitMap().substr(15, 6)));


	if(tlv.getValForTag("\x9f\x1a", value)){
		iccdata_tlv.appendTag("\x9f\x1a", value);
	}
	if(tlv.getValForTag("\x9f\x35", value)){
		iccdata_tlv.appendTag("\x9f\x35", value);
	}
	if(tlv.getValForTag("\x95", value)){
		iccdata_tlv.appendTag("\x95", value);
	}
	if(tlv.getValForTag("\x9f\x53", value)){
		iccdata_tlv.appendTag("\x9f\x53", value);
	}
	if(tlv.getValForTag("\x5f\x2a", value)){
		iccdata_tlv.appendTag("\x5f\x2a", value);
	}
	if(tlv.getValForTag("\x9f\x41", value)){
		iccdata_tlv.appendTag("\x9f\x41", value);
	}
	if(tlv.getValForTag("\x9a", value)){
		iccdata_tlv.appendTag("\x9a", value);
	}
	if(tlv.getValForTag("\x9c", value)){
		iccdata_tlv.appendTag("\x9c", value);
	}
	if(tlv.getValForTag("\x9f\x37", value)){
		iccdata_tlv.appendTag("\x9f\x37", value);
	}
//	if(tlv.getValForTag("\x9f\x6e", value)){
//		iccdata_tlv.appendTag("\x9f\x6e", value);
//	}
	if(tlv.getValForTag("\x9f\x7c", value)){
		iccdata_tlv.appendTag("\x9f\x7c", value);
	}

	//iq_audi_180117 below tags
	if(tlv.getValForTag("\x84", value)){
		iccdata_tlv.appendTag("\x84", value);
	}
	if(tlv.getValForTag("\x5f\x34", value)){
		iccdata_tlv.appendTag("\x5f\x34", value);
	}

	icc_data = iccdata_tlv.getStringData();
}

void Message::BuildEmvTrack2(string tags, string &emv_track2)
{
	BERTLV tlv(tags);
	string value;

	if(tlv.getValForTag("\x57", value)){
		string temp_track2 = UtilsHexToString(value.c_str(), value.length());
		uint pos = temp_track2.find('d', 0);

		if(pos != string::npos)
			temp_track2.replace(pos, 1, 1, '=');
		else{
			// Check if the uppercase 'D' exists
			uint pos = temp_track2.find('D', 0);

			if(pos != string::npos)
				temp_track2.replace(pos, 1, 1, '=');
		}

		// Remove trailing F's
		if((temp_track2.at(temp_track2.length()-1) == 'F') || (temp_track2.at(temp_track2.length()-1) == 'f'))
			temp_track2.resize(temp_track2.length()-1);

		// Track 2 has a maximum length of 37, lets check it.
		if(temp_track2.length() > 37)
			emv_track2 = temp_track2.substr(0, 37);
		else
			emv_track2 = temp_track2;

	}else{
		if(tlv.getValForTag("\x5a", value)){
			emv_track2 = UtilsBcdToString(value.c_str(), value.length());
		}

		emv_track2.append("=");

		if(tlv.getValForTag("\x5f\x24", value)){
			emv_track2.append(UtilsBcdToString(value.c_str(), value.length()));
		}

		if(tlv.getValForTag("\x5f\x30", value)){
			emv_track2.append(UtilsBcdToString(value.c_str(), value.length()));
		}

		if(tlv.getValForTag("\x9f\x20", value)){
			emv_track2.append(UtilsBcdToString(value.c_str(), value.length()));
		}
	}

	log.message(MSG_INFO+message_name+": EMV Track2 ["+emv_track2+"]\n");
}

unsigned long hexStringToLong(const string &len)
{
	unsigned long ulResult=0;
	unsigned int start_index=0, len_count=len.length();

	if(len.at(0) & 0x80){
		start_index = 1;
		len_count = (len.at(0) & 0x7f) + 1;
	}
	for(unsigned int iIndex=start_index;iIndex<len_count
								&& iIndex<len.length();
											iIndex++) {
		ulResult<<=8;
		ulResult+=(unsigned char)len.at(iIndex);
	}

	return(ulResult);
}

void Message::FilterEMVTags(string tags, string &filtered_tags)
{
	BERTLV tlv(tags);
	BERTLV filtered_tlv;
	int pos=0;

	while(1){
		string tag, len, val;
		int ret;
		ret = tlv.getTagLenVal(pos, tag, len, val);
		if(ret){
			// Cant use val.length() below because getTagLenVal may return len == 0 and val not initialised to empty.
			if(hexStringToLong(len) > 0){
				filtered_tlv.appendTag(tag, val);
				log.message(MSG_INFO+UtilsHexToString(tag.c_str(), tag.length())+" ("+UtilsHexToString(len.c_str(), len.length())+"):"+UtilsHexToString(val.c_str(), val.length())+"\n");
			}else{
				log.message(MSG_INFO+UtilsHexToString(tag.c_str(), tag.length())+" ("+UtilsHexToString(len.c_str(), len.length())+"): --DROPPED--\n");
			}

			pos = ret;
		}
		else
			break;
	}

	filtered_tags = filtered_tlv.getStringData();
}

void Message::LogReversal(void)
{
	log.message(MSG_INFO "Logging a reversal....");

	// Check if a reversal exists
	com_verifone_terminalconfig::CTerminalConfig termcfg;

	// Look up the batch record based on the tsn
	com_verifone_batchmanager::CBatchManager batchManager;
	com_verifone_batch::CBatch cbatch;
	com_verifone_batchrec::CBatchRec batchrec;

	int ret = batchManager.getBatch(cbatch);

	if (ret != DB_OK){
		log.message(MSG_INFO "Failure retrieving batch record\r\n");
		return;
	}

	ret = cbatch.readLastRecFromBatch(batchrec);

	if (ret != DB_OK){
		log.message(MSG_INFO "Failure retrieving batch record\r\n");
		return;
	}

	unsigned int tsn = batchrec.getTsn();

	termcfg.setReverselTsn(tsn);
}

void Message::TraceTags(string tags)
{
	BERTLV tlv(tags);
	int pos=0;

	log.message(MSG_INFO+message_name+":EMV Tags:\n");

	while(1){
		string tag, len, val;
		int ret;
		ret = tlv.getTagLenVal(pos, tag, len, val);

		if(ret){
			log.message(MSG_INFO+UtilsHexToString(tag.c_str(), tag.length())+" ("+UtilsHexToString(len.c_str(), len.length())+"):"+UtilsHexToString(val.c_str(), val.length())+"\n");
			pos = ret;
		}
		else
			break;
	}
}

#if 0
std::string Message::MapActionCode(std::string host_ac)
{
	long recv=0;
	istringstream ( host_ac ) >> recv;

	switch (recv) {
	case 0:		return std::string("0"); // Successful
	case 107:	return std::string("1"); // Refer To Card Issuer
	case 108:	return std::string("2"); // Refer To Card Issuer Special
	case 109:	return std::string("3"); // Invalid Merchant
	case 200:	return std::string("4"); // Pick Up
	case 100:	return std::string("5"); // Do Not Honour
	case 913:	return std::string("6"); // Error
	case 207:	return std::string("7"); // Pick Up Special
	case 1:		return std::string("8"); // Honour With ID
	case 923:	return std::string("9"); // Request In Progress
	case 2:		return std::string("10"); // Approved Partial
	case 3:		return std::string("11"); // Approved VIP
	case 902:	return std::string("12"); // Invalid Tran
	case 110:	return std::string("13"); // Invalid Amount
	case 111:	return std::string("14"); // Invalid Card Number
	case 908:	return std::string("15"); // No Such Issuer
	case 4:		return std::string("16"); // Approved Update Track 3
//17 - Already mapped
//18 - Already mapped
	case 903:	return std::string("19"); // Re-enter Transaction
//20 - Already mapped
	case 921:	return std::string("21"); // No Action Taken
	case 909:	return std::string("22"); // Suspected Malfunction
	case 113:	return std::string("23"); // Unacceptable Tran Fee
	case 301:	return std::string("24"); // File Update Not Supported
	case 302:	return std::string("25"); // Unable To Locate Record
	case 308:	return std::string("26"); // Duplicate Record
	case 304:	return std::string("27"); // File Update Edit Error
	case 305:	return std::string("28"); // File Update File Locked
	case 306:	return std::string("29"); // File Update Failed
	case 904:	return std::string("30"); // Format Error
	case 905:	return std::string("31"); // Bank Not Supported
//32 - Already mapped
	case 201:	return std::string("33"); // Expired Card Pick Up
	case 202:	return std::string("34"); // Suspected Fraud Pick Up
	case 203:	return std::string("35"); // Contact Acquirer Pick Up
	case 204:	return std::string("36"); // Restricted Card Pick Up
	case 205:	return std::string("37"); // Call Acquirer Security Pick Up
	case 206:	return std::string("38"); // PIN Tries Exceeded Pick Up
	case 114:	return std::string("39"); // No Credit Account
	case 115:	return std::string("40"); // Function Not Supported
	case 208:	return std::string("41"); // Lost Card
//42 - Already mapped
	case 209:	return std::string("43"); // Stolen Card
//44 - Already mapped
//45 - Already mapped
	case 116:	return std::string("51"); // Not Sufficient Funds
//52 - Already mapped
//53 - Already mapped
	case 101:	return std::string("54"); // Expired Card
	case 117:	return std::string("55"); // Incorrect PIN
	case 118:	return std::string("56"); // No Card Record
	case 119:	return std::string("57"); // Tran Not Permitted Cardholder
	case 120:	return std::string("58"); // Tran Not Permitted Terminal
	case 102:	return std::string("59"); // Suspected Fraud Declined
	case 103:	return std::string("60"); // Contact Acquirer
	case 121:	return std::string("61"); // Exceeds Withdrawal Limit
	case 104:	return std::string("62"); // Restricted Card
	case 122:	return std::string("63"); // Security Violation
//64 - Already mapped
	case 123:	return std::string("65"); // Exceeds Withdrawal Frequency
	case 105:	return std::string("66"); // Call Acquirer Security
//67 - Already mapped
	case 911:	return std::string("68"); // Response Received Too Late
//69 - Already mapped
	case 106:	return std::string("75"); // PIN Tries Exceeded
//76 - Already mapped
//77 - Already mapped
//78 - Already mapped
//79 - Already mapped
	case 906:	return std::string("90"); // Cut-off In Progress
	case 907:	return std::string("91"); // Issuer Or Switch Inoperative
	case 124:	return std::string("93"); // Violation Of Law
//94 - Already mapped
	case 915:	return std::string("95"); // Reconcile Error
//96 - Already mapped
//97 - Already mapped
//98 - Already mapped
	default:	return std::string("60"); // Contact Acquirer
	}
}
#endif

//iq_audi_291216
std::string Message::MapActionCode(std::string host_ac)
{
	long recv=0;
	istringstream ( host_ac ) >> recv;

	switch (recv) {
	case 0:		return std::string("00"); // Successful
	case 1:		return std::string("01"); // Refer To Card Issuer
	case 2:		return std::string("02"); // Refer To Card Issuer Special
	case 3:		return std::string("03"); // Invalid Merchant
	case 4:		return std::string("04"); // Pick Up
	case 5:		return std::string("05"); // Do Not Honour
	case 6:		return std::string("06"); // Error
	case 7:		return std::string("07"); // Pick Up Special
	case 8:		return std::string("08"); // Honour With ID
	case 9:		return std::string("09"); // Request In Progress
	case 10: 	return std::string("10"); // Approved Partial
	case 11: 	return std::string("11"); // Approved VIP
	case 12:	return std::string("12"); // Invalid Tran
	case 13:	return std::string("13"); // Invalid Amount
	case 14:	return std::string("14"); // Invalid Card Number
	case 15:	return std::string("15"); // No Such Issuer
	case 16:	return std::string("16"); // Approved Update Track 3
	case 17:	return std::string("17");	// Customer Cancellation
	case 19:	return std::string("19"); // Re-enter Transaction
	case 20:	return std::string("20");	//Invalid response
	case 21:	return std::string("21"); // No Action Taken
	case 22:	return std::string("22"); // Suspected Malfunction
	case 23:	return std::string("23"); // Unacceptable Tran Fee
	case 24:	return std::string("24"); // File Update Not Supported
	case 25:	return std::string("25"); // Unable To Locate Record
	case 26:	return std::string("26"); // Duplicate Record
	case 27:	return std::string("27"); // File Update Edit Error
	case 28:	return std::string("28"); // File Update File Locked
	case 29:	return std::string("29"); // File Update Failed
	case 30:	return std::string("30"); // Format Error
	case 31:	return std::string("31"); // Bank Not Supported
	case 33:	return std::string("33"); // Expired Card Pick Up
	case 34:	return std::string("34"); // Suspected Fraud Pick Up
	case 35:	return std::string("35"); // Contact Acquirer Pick Up
	case 36:	return std::string("36"); // Restricted Card Pick Up
	case 37:	return std::string("37"); // Call Acquirer Security Pick Up
	case 38:	return std::string("38"); // PIN Tries Exceeded Pick Up
	case 39:	return std::string("39"); // No Credit Account
	case 40:	return std::string("40"); // Function Not Supported
	case 41:	return std::string("41"); // Lost Card
	case 43:	return std::string("43"); // Stolen Card
	case 51:	return std::string("51"); // Not Sufficient Funds
	case 54:	return std::string("54"); // Expired Card
	case 55:	return std::string("55"); // Incorrect PIN
	case 56:	return std::string("56"); // No Card Record
	case 57:	return std::string("57"); // Tran Not Permitted Cardholder
	case 58:	return std::string("58"); // Tran Not Permitted Terminal
	case 59:	return std::string("59"); // Suspected Fraud Declined
	case 60:	return std::string("60"); // Contact Acquirer
	case 61:	return std::string("61"); // Exceeds Withdrawal Limit
	case 62:	return std::string("62"); // Restricted Card
	case 63:	return std::string("63"); // Security Violation
	case 65:	return std::string("65"); // Exceeds Withdrawal Frequency
	case 66:	return std::string("66"); // Call Acquirer Security
	case 68:	return std::string("68"); // Response Received Too Late
	case 75:	return std::string("75"); // PIN Tries Exceeded
	case 81:	return std::string("81"); // PIN Cryptographic Error Found
	case 82:	return std::string("82"); // Negative CAM, dCVV, iCVV, or CVV results
	case 90:	return std::string("90"); // Cut-off In Progress
	case 91:	return std::string("91"); // Issuer Or Switch Inoperative
	case 93:	return std::string("93"); // Violation Of Law
	case 95:	return std::string("95"); // Reconcile Error
	case 96:	return std::string("96"); // Reconcile Error if batch no. not found/match iq_audi_180117
	default:	return std::string("60"); // Contact Acquirer
	}
}

