//#include <svc.h>
#include "DateTimeParam.h"
#include "CLog.h"
#include "utils.h"
#include "HIMISOver.h"

static CLog log(HIMISO_NAME);

#define DT_DAT_NAME		"flash/DT.dat"

int DateTimeParam::Download()
{
	log.message(MSG_INFO "Date/Time Download Start\n");

	fh = fopen(DT_DAT_NAME, "w+b");

	if(fh == NULL){
		log.message(MSG_INFO "Date/Time Parameters failed to open "DT_DAT_NAME"\n");
		return -1;
	}

	return PerformAction();
}

#include <svcmgr/svc_utility.h>
#include <string.h>
#include "utils.h"

int DateTimeParam::ResponseData(int msg_no, string data)
{
	(void)msg_no;

	log.message(MSG_INFO "Date/Time Parameters RX ("+Utils::UtilsIntToString(data.length())+" bytes) ["+data+"]\r\n");
//	[20140612105946:20140612085946]	
#if 0	// This is the right way to set the clock, according to the programmers guide. It crashes due to permissions problems
	string timestamp = data.substr(4, 8);
	timestamp.append(data.substr(0, 4));
	timestamp.append(".");
	timestamp.append(data.substr(12, 2));
	
	log.message(MSG_INFO "Setting time to ["+timestamp+"]\r\n");
	setDateTime((char*)timestamp.c_str());
	setRTC();
#else	// This is non ideal way, since the functions seem to be Ux300 specific
	struct utilityDateTime dt;

	memset(&dt, 0, sizeof(struct utilityDateTime));

	dt.tm_sec = Utils::UtilsStringToInt(data.substr(12, 2));	// seconds
	dt.tm_min = Utils::UtilsStringToInt(data.substr(10, 2));	// minutes
	dt.tm_hour = Utils::UtilsStringToInt(data.substr(8, 2));    // hours
	dt.tm_mday = Utils::UtilsStringToInt(data.substr(6, 2));    // day of the month
	dt.tm_mon = Utils::UtilsStringToInt(data.substr(4, 2));     // month
	dt.tm_year = Utils::UtilsStringToInt(data.substr(0, 4));    // year

	utility_settime(&dt);
#endif
	fwrite(data.data(), 1, data.length(), fh);

	return 0;
}

void DateTimeParam::Error(ERRORCODE code)
{
	status = (int)code;

	fclose(fh);

	switch(status){
		case ERROR_CONNECT:
			log.message(MSG_INFO "Date/Time Parameters Error Connecting\n");
			break;
		case ERROR_SEND:
			log.message(MSG_INFO "Date/Time Parameters Error Sending\n");
			break;
		case ERROR_TIMEOUT:
			log.message(MSG_INFO "Date/Time Parameters Error Timeout\n");
			break;
		case ERROR_FORMAT:
			log.message(MSG_INFO "Date/Time Parameters Error Format\n");
			break;
		case ERROR_MISSING_DATA:
			log.message(MSG_INFO "Date/Time Parameters Error Missing data\n");
			break;
		case ERROR_FILE_SYSTEM:
			log.message(MSG_INFO "Date/Time Parameters File System Error\n");
			break;
	}
}

void DateTimeParam::FileActionComplete(void)
{
	log.message(MSG_INFO "Date/Time Parameters Download Complete\n");
	fclose(fh);
	status = 0;
}

void DateTimeParam::Cleanup(void)
{
	remove(DT_DAT_NAME);
}
