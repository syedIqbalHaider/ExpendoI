#include <sys/timeb.h>
#include <iomanip>
#include <sstream>
#include <libda/cterminalconfig.h>
#include <libipc/ipc.h>
#include <libda/ctsn.h>
#include <string.h>

#include "Reversal.h"
#include "CLog.h"
#include "TermApp_8583.h"
#include "utils.h"
#include "TermAppSession.h"
#include "HIMISOver.h"
#include "values.h"
#include "libda/creceiptno.h"

static CLog log(HIMISO_NAME);

using namespace Utils;
using namespace com_verifone_terminalconfig;
using namespace za_co_verifone_bertlv;
using namespace com_verifone_receiptno;
using namespace com_verifone_tsn;

Reversal::Reversal(CBatchRec &cbatchrec):Message("Reversal", cbatchrec)
{
	record_found = true;
}

int Reversal::doReversal()
{
	log.message(MSG_INFO "Performing reversal\n");

	unsigned char pan[37+1];
	unsigned int pan_len=0;
	char buffer[100];
	ISO_HANDLE isoh, isoh_resp;
	com_verifone_terminalconfig::CTerminalConfig termcfg;

	isoh = IsoCreateList(MTI_1420);

	int itx_type = UtilsStringToInt(batchrec.getTxCode());
	int iacc_type = UtilsStringToInt(batchrec.getAccType());
	int ito_acc=0;	// TODO: Where does this come from?

	// Processing code
	if ((itx_type == TX_ISO_REFUND) || (itx_type == TX_ISO_DEPOSIT) || (itx_type == TX_ISO_CORRECTION) || (itx_type == TX_ISO_REFUND) || (itx_type == TX_ISO_PMNT_DEPOSIT))
		sprintf(buffer, "%02X%02d%02d", (int)itx_type, (int)iacc_type, (int)ito_acc);
	else if (itx_type == TX_ISO_TRANSFER)
		sprintf(buffer, "%02X%02d%02d", (int)itx_type, (int)iacc_type, (int)ito_acc);
	else
		sprintf(buffer, "%02X%02d%02d", (int)itx_type, (int)iacc_type, (int)ito_acc);

	IsoAddField(isoh, Processing_code, AddFieldNotApplicable, (unsigned char*)buffer, 6);																// 3

	if(batchrec.getTxAmount()!=0.00) //iq_audi_231116
	{
		char stx_amount[12+1]={0};

		sprintf(stx_amount, "%012ld",batchrec.getTxAmount());
		IsoAddField(isoh, Amount_transaction, AddFieldNotApplicable, (unsigned char*) stx_amount, 12);
	}

	CTsn ctsn;
	char stan[6+1]={0};
	sprintf(stan, "%06d",ctsn.getNextTsn());

//	com_verifone_receiptno::CReceiptNo recpt;
//	char sequenceNo[6+1] ={0};
//	sprintf(sequenceNo,"%06d",recpt.getCurrentReceiptNo());
	IsoAddField(isoh, System_trace_audit_number, AddFieldNotApplicable, (unsigned char*)stan,6);

	string datetime = UtilsTimestamp();
//	IsoAddField(isoh, Date_and_time_transmission, AddFieldNotApplicable, (unsigned char*) datetime.substr(4, 10).c_str(), 10);		// 7
//	IsoAddField(isoh, Date_and_time_local_transaction, AddFieldNotApplicable, (unsigned char*) datetime.substr(2, 12).c_str(), 12);	// 12

//		iq_audi_281116
	IsoAddField(isoh, Date_and_time_local_transaction, AddFieldNotApplicable, (unsigned char*) datetime.substr(8, 12).c_str(), 6);	// 12
	IsoAddField(isoh, Date_local_transaction, AddFieldNotApplicable, (unsigned char*) datetime.substr(4, 8).c_str(), 4);	//13


	string card_input_mode;
	card_input_mode.clear();
	PanEntryMode entry_mode=(PanEntryMode)batchrec.getPanEntryMode();

	if ((entry_mode == PEM_SWIPED)  ) {
		card_input_mode.append("02");
	} else if (entry_mode == PEM_ENTERED) {
		card_input_mode.append("01");
	} else if (entry_mode == PEM_INSERTED) {
		card_input_mode.append("05");
	} else if (entry_mode == PEM_TAPPED) {
		card_input_mode.append("07");
	} else if (entry_mode == PEM_FALLBACK) {
		card_input_mode.append("80");
	}

	CARD_AUTH_METHOD cardhAuthMethod=(CARD_AUTH_METHOD)batchrec.getCardhAuthMethod();

	if(cardhAuthMethod==PIN)
		card_input_mode.append("1");
	else
		card_input_mode.append("0");

	IsoAddField(isoh, POS_data_code, AddFieldNotApplicable, (unsigned char*) card_input_mode.c_str(), 3); //22

//		iq_audi_291116
	string str_tpdu = termcfg.getTpdu();

	if(batchrec.getDraftCapMode() == ONLINE)
		IsoAddField(isoh, Function_code, AddFieldNotApplicable, (unsigned char*)str_tpdu.substr(3, 6).c_str(), 3);		// 24
	else
		IsoAddField(isoh, Function_code, AddFieldNotApplicable, (unsigned char*)str_tpdu.substr(3, 6).c_str(), 3);		// 24

	IsoAddField(isoh, Message_reason_code, AddFieldNotApplicable, (unsigned char*)"00", 2);		// 25

	com_verifone_batchmanager::CBatchManager batchManager;
	com_verifone_batch::CBatch cbatch;

//	 	iq_audi_231116 comment field
//	int ret = batchManager.getBatch(cbatch);
//
//	if (ret != 1){
//		log.message(MSG_INFO ":Failure retrieving batch number\r\n");
//		return -1;
//	}
//	string str_batchno = UtilsIntToString(cbatch.getBatchNo());
//	IsoAddField(isoh, Reconciliation_indicator, AddFieldNotApplicable, (unsigned char*) str_batchno.c_str(), str_batchno.length());						// 29

	if ((entry_mode == PEM_SWIPED) || (entry_mode == PEM_FALLBACK)) {
//		iq_audi_241116 start
		string track2=batchrec.getTrack2();

		uint pos = track2.find('=', 0);
		track2.replace(pos, 1, 1, 'D');

//		memset(pan,0,sizeof(pan));
//		pan_len=track2.length();
//		memcpy(pan,track2.c_str(),pan_len);
//		IsoAddField(isoh, Track_2_data, AddFieldNotApplicable, (unsigned char*)pan,pan_len);

		string strPan;
		strPan=track2.substr(0,track2.find("D"));
		pan_len=strPan.length();

		IsoAddField(isoh, Primary_account_number, AddFieldNotApplicable, (unsigned char*)strPan.c_str(), pan_len);		// 2
		IsoAddField(isoh, Date_expiration, AddFieldNotApplicable, (unsigned char*)track2.substr(pos+1,pos+5).c_str(), 4);	// 14
	}

	if (entry_mode == PEM_INSERTED){
		string emv_track2;
		BuildEmvTrack2(batchrec.getEmvTags(), emv_track2);

//		iq_audi_241116 start
		uint pos = emv_track2.find('=', 0);
		emv_track2.replace(pos, 1, 1, 'D');

//		memset(pan,0,sizeof(pan));
//		pan_len=emv_track2.length();
//		memcpy(pan,emv_track2.c_str(),pan_len);
//		IsoAddField(isoh, Track_2_data, AddFieldNotApplicable, (unsigned char*)pan,pan_len);				// 35

		batchrec.setPan(emv_track2.substr(0,emv_track2.find("D")));

//		iq_audi_241116
		memset(pan,0,sizeof(pan));
		pan_len=batchrec.getPan().length();
		memcpy(pan,batchrec.getPan().c_str(),pan_len);

		IsoAddField(isoh, Primary_account_number, AddFieldNotApplicable, (unsigned char*)pan, pan_len);		// 2
		IsoAddField(isoh, Date_expiration, AddFieldNotApplicable, (unsigned char*)emv_track2.substr(pos+1,pos+5).c_str(), 4);	// 14
	}

//	iq_audi_231116 comment field
//	string rrn = batchrec.getRrn();
//	if(rrn.length())
//		IsoAddField(isoh, Retrieval_reference_number, AddFieldNotApplicable, (unsigned char*)rrn.c_str(), rrn.length());								// 37
//
//	IsoAddField(isoh, Action_code, AddFieldNotApplicable, (unsigned char*)"911", 3);																	// 39

	string str_tid = space_pad(termcfg.getTerminalNo(), 8);
	IsoAddField(isoh, Terminal_ID, AddFieldNotApplicable, (unsigned char*)str_tid.c_str(), str_tid.length());											// 41

	string str_merchno;
	str_merchno.insert(0, 15, '0');
	str_merchno.replace(15-termcfg.getMerchantNo().length(), termcfg.getMerchantNo().length(), termcfg.getMerchantNo());
	IsoAddField(isoh, Card_acceptor_ID, AddFieldNotApplicable, (unsigned char*)str_merchno.c_str(), str_merchno.length());								// 42

//	iq_audi_281116 comment field
//	string str_posdata = space_fill(19);
//	str_posdata.replace(0, str_tid.length(), str_tid);
//	str_posdata.replace(8, str_tsn.length(), str_tsn);
//	str_posdata.replace(14, 5, "00000");
//	IsoAddField(isoh, Additional_data_private, POS_data, (unsigned char*)str_posdata.c_str(), 19);														// 48.1

	// Manual card entry
	if(entry_mode == PEM_ENTERED) {
//		iq_audi_241116
		memset(pan,0,sizeof(pan));
		pan_len=batchrec.getPan().length();
		memcpy(pan,batchrec.getPan().c_str(),pan_len);

		IsoAddField(isoh, Primary_account_number, AddFieldNotApplicable, (unsigned char*)pan, pan_len);		// 2
		IsoAddField(isoh, Date_expiration, AddFieldNotApplicable, (unsigned char*)batchrec.getExp().c_str(), batchrec.getExp().length());				// 14
	}

	string f49enable;
	f49enable=termcfg.getF49Enable();
	if(atoi(f49enable.c_str())==1)
		IsoAddField(isoh, Currency_code_transaction, AddFieldNotApplicable, (unsigned char*)batchrec.getTransCurrency().c_str(), 3);														// 49

//	iq_audi_281116 comment field
//	if (batchrec.getCashAmount() > 0) {
//		sprintf(buffer, "%02d40710C%012d", (unsigned int)UtilsStringToInt(batchrec.getAccType()), (unsigned int)batchrec.getCashAmount());
//		IsoAddField(isoh, Additional_amounts, AddFieldNotApplicable, (unsigned char*) buffer, strlen(buffer));											// 54
//	}

	if(entry_mode == PEM_INSERTED){
		string emv_tags = batchrec.getEmvTags();
		FilterEMVTags(batchrec.getEmvTags(), emv_tags);

		string sicc_data;
		BuildICCData(emv_tags, sicc_data);
//		za_co_verifone_bertlv::BERTLV iccdata;
//		iccdata.appendTag("\xff\x20", sicc_data);
//
//		log.message(MSG_INFO"DBG ICCDATA length "+UtilsIntToString(iccdata.getStringData().length())+" ["+UtilsHexToString(iccdata.getStringData().data(), iccdata.getStringData().length())+"]\n");
//
//		TraceTags(sicc_data);
//		IsoAddField(isoh, ICC_data, AddFieldNotApplicable, (unsigned char*)iccdata.getStringData().data(), iccdata.getStringData().length());			// 55

//		iq_audi_281116
		log.message(MSG_INFO"DBG ICCDATA length "+UtilsIntToString(sicc_data.length())+" ["+UtilsHexToString(sicc_data.c_str(), sicc_data.length())+"]\n");
		TraceTags(sicc_data);
		IsoAddField(isoh, ICC_data, AddFieldNotApplicable, (unsigned char*)sicc_data.c_str(), sicc_data.length());										// 55

	}

//			iq_audi_281116 comment field
//	if (UtilsStringToInt(batchrec.getBudgetPeriod()) > 0){
//		IsoAddField(isoh, Extended_payment_data, AddFieldNotApplicable, (unsigned char*)batchrec.getBudgetPeriod().c_str(), batchrec.getBudgetPeriod().length());// 67
//	}
//
//	string original_data;
//	if(batchrec.getDraftCapMode() == ONLINE)
//		original_data = "1200";
//	else
//		original_data = "1100";
//
//	original_data.append(zero_pad(batchrec.getTsn(), 6));
//
//	datetime = batchrec.getTxDateTime();
//	if(datetime.length()){
//
//		original_data.append(datetime.substr(4, 10));
//		original_data.append(11, '0');
//	}
//
//	IsoAddField(isoh, Original_data_elements, AddFieldNotApplicable, (unsigned char*) original_data.c_str(), original_data.length()); 					// 56
//
//
//	if (UtilsStringToInt(batchrec.getBudgetPeriod()) > 0){
//		IsoAddField(isoh, Extended_payment_data, AddFieldNotApplicable, (unsigned char*)batchrec.getBudgetPeriod().c_str(), batchrec.getBudgetPeriod().length());// 67
//	}

	string str_tsn = zero_pad(batchrec.getTsn(), 6);
	IsoAddField(isoh, Hotcard_capacity , AddFieldNotApplicable, (unsigned char*)str_tsn.c_str(), str_tsn.length());								// 11

	unsigned char *iso_msg;
	unsigned int iso_msg_len;
	iso_msg_len = IsoBuildMsg(isoh, (unsigned char**) &iso_msg);

	IsoDestroyList(&isoh);

	if(iso_msg_len == 0){
		log.message(MSG_INFO "Reversal IsoBuildMsg failed\r\n");
		return -1;
	}

	string str_iso_msg = string((const char*)iso_msg, iso_msg_len);

	free(iso_msg);

	log.message(MSG_INFO "Reversal ("+UtilsIntToString(str_iso_msg.length())+" bytes) ["+UtilsHexToString(str_iso_msg.c_str(), str_iso_msg.length())+"]\r\n");

	string response;

	if(send_message_auth(str_iso_msg, response,false) < 0){
		log.message(MSG_INFO "Reversal failed\r\n");
		return -1;
	}

	log.message(MSG_INFO "Reversal response ("+UtilsIntToString(response.length())+" bytes) ["+UtilsHexToString(response.c_str(), response.length())+"]\r\n");

	isoh_resp = TaIsoDecode((unsigned char*) response.c_str(), response.length());

	if(isoh_resp < 0){
		log.message(MSG_INFO "Reversal TaIsoDecode failed with "+UtilsIntToString(isoh_resp)+"\r\n");
		return -1;
	}

	char *field_data=NULL;
	unsigned int field_len;

	if(IsoGetField(isoh_resp, Action_code, AddFieldNotApplicable, (unsigned char**) &field_data, &field_len) < 0){
		log.message(MSG_INFO "IsoGetField failed to get Action_code\r\n");
		IsoDestroyList(&isoh_resp);
		return -1;
	}else{
		string actioncode = MapActionCode(string(field_data, field_len));
		batchrec.setRespCode(actioncode);
		log.message(MSG_INFO "Action code ["+string(field_data, field_len)+"] mapped --> ["+actioncode+"]\r\n");
	}
	free(field_data);

	if( UtilsStringToInt(batchrec.getRespCode()) == 0){

		if(UtilsStringToInt(batchrec.getTxCode())==TX_ISO_VOID)
		{
			batchrec.setTxCode(UtilsIntToString(TX_ISO_SALE));
		}
		else
			batchrec.setTxCanceled(true);	// Mark the transaction as canceled

		batchrec.setInProgress(false);	// Mark the transaction as complete

		update_batchrec(batchrec);
	}else
	{
		log.message(MSG_INFO "iq: Reversal failed Due Response Code !=0 \r\n");
		return -1;
	}

	IsoDestroyList(&isoh_resp);

//	com_verifone_receiptno::CReceiptNo crcptno;
//	crcptno.getNextReceiptNo();

	return 0;
}

int Reversal::clearReversal()
{
	batchrec.setTxCanceled(true);	// Mark the transaction as canceled
	batchrec.setInProgress(false);	// Mark the transaction as complete
	update_batchrec(batchrec);

	return 0;
}

//
// Private methods
//
