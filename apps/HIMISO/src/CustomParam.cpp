#include "CustomParam.h"
#include "CLog.h"
#include "utils.h"
#include "HIMISOver.h"

static CLog log(HIMISO_NAME);

#define CT_DAT_NAME		"flash/CT.dat"

int CustomParam::Download()
{
	log.message(MSG_INFO "Custom Parameters Download Start\n");

	fh = fopen(CT_DAT_NAME, "w+b");

	if(fh == NULL){
		log.message(MSG_INFO "Custom Parameters failed to open "CT_DAT_NAME"\n");
		return -1;
	}

	return PerformAction();
}

int CustomParam::ResponseData(int msg_no, string data)
{
	(void)msg_no;

	log.message(MSG_INFO "Custom Parameters RX ("+Utils::UtilsIntToString(data.length())+" bytes) ["+data+"]\r\n");

	fwrite(data.data(), 1, data.length(), fh);

	return 0;
}

int CustomParam::FileName(int msg_no, string filename)
{
	(void)filename;
	// Write the version number when the first message for a particular file has been received
	if(msg_no == 1){
		if(fh)
			fwrite("0000", 1, 4, fh);
		else
			return -4;
/*
 		The actual responses received from the test system does not conform to the spec in terms of the verion number that is required.
 		As such, a hardcoded version no of '0000' is used

		int pos1, pos2;

		if((pos1 = filename.find_first_of(':')) == string::npos)
			return -1;

		if((pos2 = filename.find_first_of(':', pos1+1)) == string::npos)
			return -2;

		string version = filename.substr(pos1+1, pos2-pos1-1);

		if(version.length() != 4)
			return -3;

		if(fh)
			fwrite(version.data(), 1, version.length(), fh);
		else
			return -4;
*/
	}

	return 0;
}

void CustomParam::Error(ERRORCODE code)
{
	status = (int)code;

	fclose(fh);

	switch(status){
		case ERROR_CONNECT:
			log.message(MSG_INFO "Custom Parameters Error Connecting\n");
			break;
		case ERROR_SEND:
			log.message(MSG_INFO "Custom Parameters Error Sending\n");
			break;
		case ERROR_TIMEOUT:
			log.message(MSG_INFO "Custom Parameters Error Timeout\n");
			break;
		case ERROR_FORMAT:
			log.message(MSG_INFO "Custom Parameters Error Format\n");
			break;
		case ERROR_MISSING_DATA:
			log.message(MSG_INFO "Custom Parameters Error Missing data\n");
			break;
		case ERROR_FILE_SYSTEM:
			log.message(MSG_INFO "Custom Parameters File System Error\n");
			break;
	}
}

void CustomParam::FileActionComplete(void)
{
	log.message(MSG_INFO "Custom Parameters Download Complete\n");
	fclose(fh);
	status = 0;
}

void CustomParam::Cleanup(void)
{
	remove(CT_DAT_NAME);
}
