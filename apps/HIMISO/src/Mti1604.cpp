#include <sys/timeb.h>
#include <iomanip>
#include <sstream>
#include <libda/cterminalconfig.h>
#include <libipc/ipc.h>
#include <string.h>
#include <boost/algorithm/string.hpp>

#include "Mti1604.h"
#include "CLog.h"
#include "TermApp_8583.h"
#include "utils.h"
#include "TermAppSession.h"
#include "HIMISOver.h"
#include "values.h"


static CLog log(HIMISO_NAME);

using namespace Utils;
using namespace com_verifone_terminalconfig;
using namespace za_co_verifone_bertlv;

#define LOAD_TAG "\xff\x02"

int Mti1604::doMtiCmd(void)
{
	log.message(MSG_INFO "Performing MTI1604\n");

	char buffer[100];
	ISO_HANDLE isoh, isoh_resp;
	com_verifone_terminalconfig::CTerminalConfig termcfg;

	int itx_type = UtilsStringToInt(batchrec.getTxCode());
	int iacc_type = UtilsStringToInt(batchrec.getAccType());
	int ito_acc=0;	// TODO: Where does this come from?
	std::string emv_CB_data = "";

	if (itx_type == TX_ISO_PIN_CHANGE || itx_type == TX_ISO_LOAD_PRODUCT) {
		isoh = IsoCreateList(MTI_1604);
	}


	// Processing code
	sprintf(buffer, "%02X%02d%02d", (int)itx_type, (int)iacc_type, (int)ito_acc);
	IsoAddField(isoh, Processing_code, AddFieldNotApplicable, (unsigned char*)buffer, 6);																// 3

	string stx_amount = UtilsIntToString(batchrec.getTxAmount());
	IsoAddField(isoh, Amount_transaction, AddFieldNotApplicable, (unsigned char*) stx_amount.c_str(), stx_amount.length());								// 4

	string datetime = batchrec.getTxDateTime();
	IsoAddField(isoh, Date_and_time_transmission, AddFieldNotApplicable, (unsigned char*) datetime.substr(4, 10).c_str(), 10);							// 7
	IsoAddField(isoh, Date_and_time_local_transaction, AddFieldNotApplicable, (unsigned char*) datetime.substr(2, 12).c_str(), 12);						// 12

	string str_tsn = zero_pad(batchrec.getTsn(), 6);
	IsoAddField(isoh, System_trace_audit_number, AddFieldNotApplicable, (unsigned char*)str_tsn.c_str(), str_tsn.length());								// 11

	int card_input_mode=0;
	PanEntryMode entry_mode=(PanEntryMode)batchrec.getPanEntryMode();

	if ((entry_mode == PEM_SWIPED)  || (entry_mode == PEM_FALLBACK)) {
		card_input_mode = 2;
	} else if (entry_mode == PEM_ENTERED) {
		card_input_mode = 1;
	} else if (entry_mode == PEM_INSERTED) {
		card_input_mode = 5;
	} else if (entry_mode == PEM_TAPPED) {
		card_input_mode = 7;
	}

//	sprintf(buffer, "510101%i5414C001", card_input_mode);
	string cbmp = termcfg.getEmvCapabilityBitMap();
	cbmp.replace(6, 1, 1, (char)(card_input_mode+'0'));
	IsoAddField(isoh, POS_data_code, AddFieldNotApplicable, (unsigned char*) cbmp.substr(0, 15).c_str(), 15);											// 22

	if (itx_type == TX_ISO_PIN_CHANGE) {
		IsoAddField(isoh, Function_code, AddFieldNotApplicable, (unsigned char*)"691", 3);																// 24
		IsoAddField(isoh, Message_reason_code, AddFieldNotApplicable, (unsigned char*)"1005", 4);														// 25
	} else if (itx_type == TX_ISO_LOAD_PRODUCT) {
		IsoAddField(isoh, Function_code, AddFieldNotApplicable, (unsigned char*)"691", 3);																// 24
		IsoAddField(isoh, Message_reason_code, AddFieldNotApplicable, (unsigned char*)"1005", 4);														// 25
	}

	com_verifone_batchmanager::CBatchManager batchManager;
	com_verifone_batch::CBatch cbatch;

	int ret = batchManager.getBatch(cbatch);

	if (ret != 1){
		log.message(MSG_INFO ":Failure retrieving batch number\r\n");
		return -1;
	}

	string str_batchno = UtilsIntToString(cbatch.getBatchNo());
	IsoAddField(isoh, Reconciliation_indicator, AddFieldNotApplicable, (unsigned char*) str_batchno.c_str(), str_batchno.length());						// 29

	if ((entry_mode == PEM_SWIPED)  || (entry_mode == PEM_FALLBACK)) {
		IsoAddField(isoh, Track_2_data, AddFieldNotApplicable, (unsigned char*)batchrec.getTrack2().c_str(), batchrec.getTrack2().length());			// 35
	}

	if (entry_mode == PEM_INSERTED){
		string emv_track2;
		BuildEmvTrack2(batchrec.getEmvTags(), emv_track2);
		IsoAddField(isoh, Track_2_data, AddFieldNotApplicable, (unsigned char*)emv_track2.c_str(), emv_track2.length());								// 35

		BERTLV tlv(batchrec.getEmvTags());
		string value;
		tlv.getValForTag(string("\xcb",1), value);
		//log.message(MSG_INFO "Auth Emv ("+UtilsIntToString(batchrec.getEmvTags().length())+" bytes) ["+UtilsHexToString(batchrec.getEmvTags().c_str(), batchrec.getEmvTags().length())+"]\r\n");
		//log.message(MSG_INFO "Auth CBVal ("+UtilsIntToString(value.length())+" bytes) ["+UtilsHexToString(value.c_str(), value.length())+"]\r\n");
		if(value.length()>0){
			string sCB = UtilsHexToString(value.c_str(), value.length());
			emv_CB_data = "211ABSA:ICCReq216CB06" + sCB;
		}
		log.message(MSG_INFO "Auth CB ("+UtilsIntToString(emv_CB_data.length())+" bytes) ["+UtilsHexToString(emv_CB_data.c_str(), emv_CB_data.length())+"]\r\n");
	}

	// Note: below will truncate termid if it is longer than int size - is it safe?
	long itid = (long)UtilsStringToInt(termcfg.getTerminalNo());
	string rrn("P");
	rrn.append(UtilsBase62Encode(itid));
	rrn.append(zero_pad(cbatch.getBatchNo(), 3));

	std::string sTsn = UtilsIntToString(batchrec.getTsn());
	if(sTsn.length() < 3){
		string tsn = zero_pad(sTsn, 3);
		rrn.append(tsn);
	}else
		rrn.append(sTsn.substr((unsigned int)(sTsn.length()-3), 3));

	IsoAddField(isoh, Retrieval_reference_number, AddFieldNotApplicable, (unsigned char*)rrn.c_str(), rrn.length());									// 37

	string str_tid = space_pad(termcfg.getTerminalNo(), 8);
	IsoAddField(isoh, Terminal_ID, AddFieldNotApplicable, (unsigned char*)str_tid.c_str(), str_tid.length());											// 41

	string str_merchno;
	str_merchno.insert(0, 15, '0');
	str_merchno.replace(15-termcfg.getMerchantNo().length(), termcfg.getMerchantNo().length(), termcfg.getMerchantNo());
	IsoAddField(isoh, Card_acceptor_ID, AddFieldNotApplicable, (unsigned char*)str_merchno.c_str(), str_merchno.length());								// 42

	string str_posdata = space_fill(19);
	str_posdata.replace(0, str_tid.length(), str_tid);
	str_posdata.replace(8, str_tsn.length(), str_tsn);
//	str_posdata.replace(14, 5, "00000");
	str_posdata.replace(14, 5, "000  ");
	IsoAddField(isoh, Additional_data_private, POS_data, (unsigned char*)str_posdata.c_str(), 19);														// 48.1

	if (itx_type == TX_ISO_LOAD_PRODUCT) {
		IsoAddField(isoh, Additional_data_private, Extended_transaction_type, (unsigned char*)"3302", 4); // 48.4

		//log.message(MSG_INFO "Load Emv ("+UtilsIntToString(batchrec.getEmvTags().length())+" bytes) ["+UtilsHexToString(batchrec.getEmvTags().c_str(), batchrec.getEmvTags().length())+"]\r\n");
		BERTLV tlv(batchrec.getEmvTags());
		string value;
		string sBuf = "212ABSA:SERVICE13LVP";
		tlv.getValForTag(string(LOAD_TAG,2), value);
		//log.message(MSG_INFO "Prod Val ("+UtilsIntToString(value.length())+" bytes) ["+UtilsHexToString(value.c_str(), value.length())+"]\r\n");
		if(value.length()>0){
			string nDotAscii = UtilsHexToString(value.c_str(), value.length());
			sBuf += "212ABSA:EMVSCR2";
			string sLen = UtilsIntToString(nDotAscii.length());
			sBuf += UtilsIntToString(sLen.length()) + sLen + nDotAscii;
		}

		log.message(MSG_INFO "ProdLoad add=" + sBuf + "\n");
		IsoAddField(isoh, Additional_data_private, Structured_Data, (unsigned char *) sBuf.c_str(), sBuf.length()); // 48.16
	} else {
		IsoAddField(isoh, Additional_data_private, Structured_Data, (unsigned char*)"212ABSA:SERVICE13LVP", 20);										// 48.16

	}

	//
	// Optional fields
	//

	// add the invoice number
	if(batchrec.getInvoiceNo().length() != 0){
		IsoAddField(isoh, Additional_data_private, Additional_node_data, (unsigned char*)batchrec.getInvoiceNo().c_str(), batchrec.getInvoiceNo().length());// 48.5
	}

	// Manual card entry
	if(entry_mode == PEM_ENTERED) {
		IsoAddField(isoh, Primary_account_number, AddFieldNotApplicable, (unsigned char*)batchrec.getPan().c_str(), batchrec.getPan().length());		// 2
		IsoAddField(isoh, Date_expiration, AddFieldNotApplicable, (unsigned char*)batchrec.getExp().c_str(), batchrec.getExp().length());				// 14
		IsoAddField(isoh, Additional_data_private, Card_verification_data, (unsigned char*)batchrec.getCvv().c_str(), batchrec.getCvv().length());		// 48.3
	}

	IsoAddField(isoh, Currency_code_transaction, AddFieldNotApplicable, (unsigned char*)"710", 3);														// 49

	//if(got_pin_data){
	if((itx_type != TX_ISO_PIN_CHANGE) && got_pin_data){
		IsoAddField(isoh, PIN_data, AddFieldNotApplicable, (unsigned char*)apinblock.c_str(), 8);														// 52
		IsoAddField(isoh, Security_related_control_information, AddFieldNotApplicable, (unsigned char*)aksn.substr(2).c_str(), 8);						// 53
	}

	if (itx_type == TX_ISO_PIN_CHANGE) {
		IsoAddField(isoh, PIN_data, AddFieldNotApplicable, (unsigned char*)apinblock.c_str(), 8); 														// 52
		string newPinBlockData = "\x01";
		newPinBlockData.append(new_pinblock.substr(0,8));
		newPinBlockData.append(aksn.substr(2,8));
		newPinBlockData.append(new_ksn.substr(2,8));
		newPinBlockData.append(23, '\0');

		IsoAddField(isoh, Security_related_control_information, AddFieldNotApplicable,
				(unsigned char*)newPinBlockData.c_str(), 48); 																				// 53
	}

//	if (batchrec.getCashAmount() > 0) {
//		sprintf(buffer, "%02d40710C%012d", (unsigned int)UtilsStringToInt(batchrec.getAccType()), (unsigned int)batchrec.getCashAmount());
//		IsoAddField(isoh, Additional_amounts, AddFieldNotApplicable, (unsigned char*) buffer, strlen(buffer));											// 54
//	}

	if(entry_mode == PEM_INSERTED){
		IsoAddField(isoh, Primary_account_number, AddFieldNotApplicable, (unsigned char*)batchrec.getPan().c_str(), batchrec.getPan().length());		// 2

		//log.message(MSG_INFO "Emv ("+UtilsIntToString(batchrec.getEmvTags().length())+" bytes) ["+UtilsHexToString(batchrec.getEmvTags().c_str(), batchrec.getEmvTags().length())+"]\r\n");

		string emv_tags = batchrec.getEmvTags();
		FilterEMVTags(batchrec.getEmvTags(), emv_tags);

		// check for tag 0x5f34
		BERTLV emvTags(emv_tags);
		string crdseq;
		if(emvTags.getValForTag("\x5f\x34", crdseq)){
			string ascii_crdseq = UtilsHexToString((char*)crdseq.c_str(), crdseq.length());

			if(ascii_crdseq.length() < 3)
				ascii_crdseq.insert(0, 3-ascii_crdseq.length(), '0');

			IsoAddField(isoh, Card_sequence_number, AddFieldNotApplicable, (unsigned char*)ascii_crdseq.c_str(), 3);														// 23
		}

		string sicc_data;
		BuildICCData(emv_tags, sicc_data);
//		za_co_verifone_bertlv::BERTLV iccdata;
//		iccdata.appendTag("\xff\x20", sicc_data);
//
//		log.message(MSG_INFO"DBG ICCDATA length "+UtilsIntToString(iccdata.getStringData().length())+" ["+UtilsHexToString(iccdata.getStringData().data(), iccdata.getStringData().length())+"]\n");
//
//		TraceTags(sicc_data);
//		IsoAddField(isoh, ICC_data, AddFieldNotApplicable, (unsigned char*)iccdata.getStringData().data(), iccdata.getStringData().length());										// 55

//		iq_audi_281116
		log.message(MSG_INFO"DBG ICCDATA length "+UtilsIntToString(sicc_data.length())+" ["+UtilsHexToString(sicc_data.c_str(), sicc_data.length())+"]\n");
		TraceTags(sicc_data);
		IsoAddField(isoh, ICC_data, AddFieldNotApplicable, (unsigned char*)sicc_data.c_str(), sicc_data.length());										// 55
	}

//	if (UtilsStringToInt(batchrec.getBudgetPeriod()) > 0){
//		IsoAddField(isoh, Extended_payment_data, AddFieldNotApplicable, (unsigned char*)batchrec.getBudgetPeriod().c_str(), batchrec.getBudgetPeriod().length());// 67
//	}

	unsigned char *iso_msg;
	unsigned int iso_msg_len;
	iso_msg_len = IsoBuildMsg(isoh, (unsigned char**) &iso_msg);

	IsoDestroyList(&isoh);

	if(iso_msg_len == 0){
		log.message(MSG_INFO "IsoBuildMsg failed\r\n");
		return -1;
	}

	string str_iso_msg = string((const char*)iso_msg, iso_msg_len);

	free(iso_msg);

	log.message(MSG_INFO "MTI 1604 ("+UtilsIntToString(str_iso_msg.length())+" bytes) ["+UtilsHexToString(str_iso_msg.c_str(), str_iso_msg.length())+"]\r\n");

	string response;

	if(send_message_auth(str_iso_msg, response,false) < 0){
		log.message(MSG_INFO "Mti1604 failed\r\n");

		// Log the tsn to the termcfg db
		termcfg.setTerminalSeqNumber(UtilsStringToInt(str_tsn));

		return -1;
	}

	log.message(MSG_INFO "Mti1604 response ("+UtilsIntToString(response.length())+" bytes) ["+UtilsHexToString(response.c_str(), response.length())+"]\r\n");

	isoh_resp = TaIsoDecode((unsigned char*) response.c_str(), response.length());

	if(isoh_resp < 0){
		log.message(MSG_INFO "TaIsoDecode failed with "+UtilsIntToString(isoh_resp)+"\r\n");
		return -1;
	}

	char *field_data=NULL;
	unsigned int field_len;

//	if(IsoGetField(isoh_resp, Retrieval_reference_number, AddFieldNotApplicable, (unsigned char**) &field_data, &field_len) < 0){
//		log.message(MSG_INFO "IsoGetField failed to get Retrieval_reference_number\r\n");
//		IsoDestroyList(&isoh_resp);
//		return -1;
//	}else{
//		string rrn = string(field_data, field_len);
//		batchrec.setRrn(rrn);
//		log.message(MSG_INFO "RRN ["+rrn+"]\r\n");
//	}
//
//	free(field_data);

	if(IsoGetField(isoh_resp, Approval_code, AddFieldNotApplicable, (unsigned char**) &field_data, &field_len) < 0){
		log.message(MSG_INFO "IsoGetField failed to get Approval_code\r\n");
		batchrec.setAuthCode(string(""));
	}else{
		string approvalcode = string(field_data, field_len);
		batchrec.setAuthCode(approvalcode);
		log.message(MSG_INFO "Approval code ["+approvalcode+"]\r\n");
	}
	free(field_data);

	if(IsoGetField(isoh_resp, Action_code, AddFieldNotApplicable, (unsigned char**) &field_data, &field_len) < 0){
		log.message(MSG_INFO "IsoGetField failed to get Action_code\r\n");
		IsoDestroyList(&isoh_resp);
		return -1;
	}else{
		string actioncode = MapActionCode(string(field_data, field_len));
		batchrec.setRespCode(actioncode);
		log.message(MSG_INFO "Action code ["+string(field_data, field_len)+"] mapped --> ["+actioncode+"]\r\n");
	}
	free(field_data);

	// Check for Card and Transaction fees
	if (IsoGetField(isoh_resp, Additional_data_private, Structured_Data, (unsigned char**) &field_data, &field_len) == 0){
	    // 16   Date conversion                       (173):
		// [218Postilion:MetaData262211ABSA:AccNum11119ABSA:CFEE111211ABSA:AccTyp11119ABSA:LFEE111211ABSA:AccNum216543552200336101819ABSA:CFEE1500000211ABSA:AccTyp123019ABSA:LFEE1500200]
		// Use GetLolLong because not same as BERTLV
	    long lCardFee = 2; //GetLolLong("ABSA:CFEE",field_data,field_len);
	    long lLoadFee = 1; //GetLolLong("ABSA:LFEE",field_data,field_len);
	    long totFee = lCardFee+lLoadFee;

		batchrec.setCardFees(lCardFee);
		batchrec.setLoadFees(lLoadFee);

		// Subtract fees from TxAmount; are we sure this code will be exuted only once?
		if (totFee>0) {
		    long txAm = batchrec.getTxAmount() - totFee;
		    if (txAm>=0) {
				batchrec.setTxAmount(txAm);
		    }
		}
		log.message(MSG_INFO "Card Fee ["+UtilsLongToString(lCardFee)+"]\r\n");
		log.message(MSG_INFO "Load Fee ["+UtilsLongToString(lLoadFee)+"]\r\n");


//		// What about the scripts for cash load - no, used for product load only
//		iNewScripts=get_scripts(tx_rec,field_data,field_len);
//		//iLength=GetLolScript("ABSA:SCRIPT1",pcData,uiLength,ucScript);
//		//iLength=GetLolScript("ABSA:SCRIPT2",pcData,uiLength,ucScript);
	}
	free(field_data);

	// extract DE55 if present
	// ff212e8a02303072177215861384da00cb0e000000083290066f74c0417fcf35910aff2a8473d20989b000109 ...
	if(IsoGetField(isoh_resp, ICC_data, AddFieldNotApplicable, (unsigned char**) &field_data, &field_len) == 0){
		BERTLV icc_data_raw = string(field_data, field_len);
		string icc_data;

		log.message(MSG_INFO "ICC Data "+UtilsIntToString(icc_data.length())+" bytes ["+UtilsHexToString(field_data, field_len)+"]\r\n");

		if(icc_data_raw.getValForTag("\xff\x21", icc_data) <= 0){
			log.message(MSG_INFO "IsoGetField failed to decode tag 0xFF21 in ICC_data\r\n");
			//IsoDestroyList(&isoh_resp);
			//return -1;
		} else {

			BERTLV icc_data_tlv(icc_data);
			string iss_script1, iss_script2, iss_auth;

			// Stupid tags are double tagged!
			icc_data_tlv.getValForTag("\x71", iss_script1);
			BERTLV iss_script1_tlv(iss_script1);

			// Check if the tlv is double tagged, it happens!
			if((iss_script1.length() > 0) && (iss_script1.at(0) == 0x71)){
				// Unpack it again
				iss_script1.clear();
				iss_script1_tlv.getValForTag("\x71", iss_script1);
			}

			// Stupid tags are double tagged!
			icc_data_tlv.getValForTag("\x72", iss_script2);
			BERTLV iss_script2_tlv(iss_script2);

			// Check if the tlv is double tagged, it happens!
			if((iss_script2.length() > 0) && (iss_script2.at(0) == 0x72)){
				// Unpack it again
				iss_script2.clear();
				iss_script2_tlv.getValForTag("\x72", iss_script2);
			}

			icc_data_tlv.getValForTag("\x91", iss_auth);

			// Append response tags to emv tags in DB
			BERTLV original_icc_data(batchrec.getEmvTags());

			if(iss_script1.length()) original_icc_data.addTag("\x71", iss_script1);
			if(iss_script2.length()) original_icc_data.addTag("\x72", iss_script2);
			if(iss_auth.length()) original_icc_data.addTag("\x91", iss_auth);

			batchrec.setEmvTags(original_icc_data.getStringData());
		}
	}
	free(field_data);

//
//	}

	IsoDestroyList(&isoh_resp);

	update_batchrec(batchrec);

	return 0;
}
