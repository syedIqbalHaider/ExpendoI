#include "MerchMsg.h"
#include "CLog.h"
#include "utils.h"
#include "HIMISOver.h"

static CLog log(HIMISO_NAME);

#define MM_DAT_NAME		"flash/MM.dat"

int MerchMsg::Download()
{
	log.message(MSG_INFO "Message to merchant Download Start\n");

	fh = fopen(MM_DAT_NAME, "w+b");

	if(fh == NULL){
		log.message(MSG_INFO "Message to merchant failed to open "MM_DAT_NAME"\n");
		return -1;
	}

	return PerformAction();
}

int MerchMsg::ResponseData(int msg_no, string data)
{
	(void)msg_no;

	log.message(MSG_INFO "Message to merchant RX ("+Utils::UtilsIntToString(data.length())+" bytes) ["+data+"]\r\n");

	fwrite(data.data(), 1, data.length(), fh);

	return 0;
}

int MerchMsg::FileName(int msg_no, string filename)
{
	// Write the version number when the first message for a particular file has been received
	if(msg_no == 1){
		int pos1, pos2;

		if((pos1 = filename.find_first_of(':')) == (int)string::npos)
			return -1;

		if((pos2 = filename.find_first_of(':', pos1+1)) == (int)string::npos)
			return -2;

		string version = filename.substr(pos1+1, pos2-pos1-1);

		if(version.length() != 4)
			return -3;

		if(fh)
			fwrite(version.data(), 1, version.length(), fh);
		else
			return -4;
	}

	return 0;
}

void MerchMsg::Error(ERRORCODE code)
{
	status = (int)code;

	fclose(fh);

	switch(status){
		case ERROR_CONNECT:
			log.message(MSG_INFO "Message to merchant Error Connecting\n");
			break;
		case ERROR_SEND:
			log.message(MSG_INFO "Message to merchant Error Sending\n");
			break;
		case ERROR_TIMEOUT:
			log.message(MSG_INFO "Message to merchant Error Timeout\n");
			break;
		case ERROR_FORMAT:
			log.message(MSG_INFO "Message to merchant Error Format\n");
			break;
		case ERROR_MISSING_DATA:
			log.message(MSG_INFO "Message to merchant Error Missing data\n");
			break;
		case ERROR_FILE_SYSTEM:
			log.message(MSG_INFO "Message to merchant File System Error\n");
			break;
	}
}

void MerchMsg::FileActionComplete(void)
{
	log.message(MSG_INFO "Message to merchant Download Complete\n");
	fclose(fh);
	status = 0;
}

void MerchMsg::Cleanup(void)
{
	remove(MM_DAT_NAME);
}

