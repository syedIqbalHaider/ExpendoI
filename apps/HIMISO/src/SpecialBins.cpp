#include "SpecialBins.h"
#include "CLog.h"
#include "utils.h"
#include "HIMISOver.h"

static CLog log(HIMISO_NAME);

#define SB_DAT_NAME		"flash/SB.dat"

int SpecialBins::Download()
{
	log.message(MSG_INFO "Special Bins Download Start\n");

	fh = fopen(SB_DAT_NAME, "w+b");

	if(fh == NULL){
		log.message(MSG_INFO "Special Bins failed to open "SB_DAT_NAME"\n");
		return -1;
	}

	return PerformAction();
}

int SpecialBins::ResponseData(int msg_no, string data)
{
	(void)msg_no;

	log.message(MSG_INFO "Special Bins RX ("+Utils::UtilsIntToString(data.length())+" bytes) ["+data+"]\r\n");

	fwrite(data.data(), 1, data.length(), fh);

	return 0;
}

int SpecialBins::FileName(int msg_no, string filename)
{
	// Write the version number when the first message for a particular file has been received
	if(msg_no == 1){
		uint pos1, pos2;

		if((pos1 = filename.find_first_of(':')) == string::npos)
			return -1;

		if((pos2 = filename.find_first_of(':', pos1+1)) == string::npos)
			return -2;

		string version = filename.substr(pos1+1, pos2-pos1-1);

		if(version.length() != 4)
			return -3;

		if(fh)
			fwrite(version.data(), 1, version.length(), fh);
		else
			return -4;
	}

	return 0;
}

void SpecialBins::Error(ERRORCODE code)
{
	status = (int)code;

	fclose(fh);

	switch(status){
		case ERROR_CONNECT:
			log.message(MSG_INFO "Special Bins Error Connecting\n");
			break;
		case ERROR_SEND:
			log.message(MSG_INFO "Special Bins Error Sending\n");
			break;
		case ERROR_TIMEOUT:
			log.message(MSG_INFO "Special Bins Error Timeout\n");
			break;
		case ERROR_FORMAT:
			log.message(MSG_INFO "Special Bins Error Format\n");
			break;
		case ERROR_MISSING_DATA:
			log.message(MSG_INFO "Special Bins Error Missing data\n");
			break;
		case ERROR_FILE_SYSTEM:
			log.message(MSG_INFO "Special Bins File System Error\n");
			break;
	}
}

void SpecialBins::FileActionComplete(void)
{
	log.message(MSG_INFO "Special Bins Download Complete\n");
	fclose(fh);
	status = 0;
}

void SpecialBins::Cleanup(void)
{
	remove(SB_DAT_NAME);
}
