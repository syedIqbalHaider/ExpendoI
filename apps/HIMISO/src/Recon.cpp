#include <libda/cconmanager.h>
#include <libda/cbatchmanager.h>
#include <libda/cbatch.h>
#include <libda/cterminalconfig.h>
#include "libda/creceiptno.h"
#include <libda/ctsn.h>
#include "Recon.h"
#include "TermApp_8583.h"
#include "utils.h"
#include "CLog.h"
#include "HIMISOver.h"
#include "values.h"

static CLog log(HIMISO_NAME);

using namespace Utils;
using namespace com_verifone_terminalconfig;
using namespace com_verifone_receiptno;
using namespace com_verifone_tsn;

Recon::Recon(void):Message("ReconAdvice")
{
	int ret;
	com_verifone_batchmanager::CBatchManager batchManager;
	com_verifone_batch::CBatch cbatch;

	ret = batchManager.getBatch(cbatch);

	if (ret != DB_OK){
		log.message(MSG_INFO "Failure retrieving batch record\r\n");
		return;
	}

	batch_no = cbatch.getBatchNo();

	std::map<unsigned int, com_verifone_batchrec::CBatchRec> records;
	cbatch.getRecords(records);

	action_code = -1;

	credits_count = 0;
	credit_reversals = 0;
	credits_total_amount = 0;
	credits_reversals_total_amount = 0;

	debits_count = 0;
	debits_reversals_count = 0;
	debits_total_amount = 0;
	debits_reversals_total_amount = 0;

	credits_count_second_curr = 0;
	credit_reversals_second_curr = 0;
	credits_total_amount_second_curr = 0;
	credits_reversals_total_amount_second_curr = 0;

	debits_count_second_curr = 0;
	debits_reversals_count_second_curr = 0;
	debits_total_amount_second_curr = 0;
	debits_reversals_total_amount_second_curr = 0;

	authorisations = 0;
	authorisation_reversals = 0;
	authorisations_total_amount = 0;
	authorisation_reversals_total_amount = 0;

	std::map<unsigned int,CBatchRec>::iterator it;
	for (it=records.begin(); it!=records.end(); ++it) {
		CBatchRec batchRec = it->second;

		// Make sure that only completed transactions are processed
		if(batchRec.isInProgress() || (batchRec.isDeclined()) || (batchRec.isTxCanceled()))
			continue;

		string tx_code = batchRec.getTxCode();
		int itx_code = UtilsStringToInt(tx_code);

		if((itx_code != TX_ISO_SALE))
			continue;

		if((itx_code >= 00) && (itx_code <= 19)){

			if(batchRec.getTransCurrency().compare("840")==0) //USD
			{
				debits_count++;
				debits_total_amount += batchRec.getTxAmount()+batchRec.getCashAmount();
			}else  //LBP
			{
				debits_count_second_curr++;
				debits_total_amount_second_curr += batchRec.getTxAmount()+batchRec.getCashAmount();
			}

		}else if((itx_code >= 20) && (itx_code <= 29)){

			if(batchRec.getTransCurrency().compare("840")==0) //USD
			{
				credits_count++;
				credits_total_amount += batchRec.getTxAmount()+batchRec.getCashAmount();
			}else  //LBP
			{
				credits_count_second_curr++;
				credits_total_amount_second_curr += batchRec.getTxAmount()+batchRec.getCashAmount();
			}
		}

		if(batchRec.getDraftCapMode() == OFFLINE){
			authorisations++;
			authorisations_total_amount += batchRec.getTxAmount()+batchRec.getCashAmount();;
		}
	}
}

int Recon::SendReconAdvice(const char *processCode)
{
	ISO_HANDLE isoh, isoh_resp;
	com_verifone_terminalconfig::CTerminalConfig termcfg;

	isoh = IsoCreateList(MTI_1520);

//	string datetime = UtilsTimestamp();
//	IsoAddField(isoh, Date_and_time_transmission, AddFieldNotApplicable, (unsigned char*) datetime.substr(4, 10).c_str(), 10);							// 7
//	IsoAddField(isoh, Date_and_time_local_transaction, AddFieldNotApplicable, (unsigned char*) datetime.substr(2, 12).c_str(), 12);						// 12

	IsoAddField(isoh, Processing_code, AddFieldNotApplicable, (unsigned char*)processCode, 6);

//	com_verifone_tsn::CTsn ctsn;
//	int tsn = ctsn.getNextTsn();
//	string str_tsn = UtilsIntToString(tsn);
//	IsoAddField(isoh, System_trace_audit_number, AddFieldNotApplicable, (unsigned char*)str_tsn.c_str(), str_tsn.length());								// 11

//	iq_audi_030117
//	com_verifone_receiptno::CReceiptNo recpt;
//	char sequenceNo[6+1] ={0};
//	sprintf(sequenceNo,"%06d",recpt.getCurrentReceiptNo());
	CTsn ctsn;
	char stan[6+1]={0};
	sprintf(stan, "%06d",ctsn.getNextTsn());
	IsoAddField(isoh, System_trace_audit_number, AddFieldNotApplicable, (unsigned char*)stan,6);

//	iq_audi_030117
	string str_tpdu = termcfg.getTpdu();
	IsoAddField(isoh, Function_code, AddFieldNotApplicable, (unsigned char*)str_tpdu.substr(7, 10).c_str(), 3);															// 24

	string str_tid = space_pad(termcfg.getTerminalNo(), 8);
	IsoAddField(isoh, Terminal_ID, AddFieldNotApplicable, (unsigned char*)str_tid.c_str(), str_tid.length());											// 41

	string str_merchno;
	str_merchno.insert(0, 15, '0');
	str_merchno.replace(15-termcfg.getMerchantNo().length(), termcfg.getMerchantNo().length(), termcfg.getMerchantNo());
	IsoAddField(isoh, Card_acceptor_ID, AddFieldNotApplicable, (unsigned char*)str_merchno.c_str(), str_merchno.length());								// 42

	string str_batchno = zero_pad(UtilsIntToString(batch_no),6);
//	IsoAddField(isoh, Reconciliation_indicator, AddFieldNotApplicable, (unsigned char*) str_batchno.c_str(), str_batchno.length());						// 29
	IsoAddField(isoh, F_60, AddFieldNotApplicable, (unsigned char*) str_batchno.c_str(), str_batchno.length());						// 60

	//iq_audi_030117 comment below
//	IsoAddField(isoh, Currency_code_reconciliation, AddFieldNotApplicable, (unsigned char*)"710", 3);													// 50
//
//	string str_credits_count = UtilsIntToString((int)credits_count);
//	IsoAddField(isoh, Credits_number, AddFieldNotApplicable, (unsigned char*) str_credits_count.c_str(), str_credits_count.length());					// 74
//	IsoAddField(isoh, Credits_reversal_number, AddFieldNotApplicable, (unsigned char*) "0", 1);															// 75
//
//	string str_debits_count = UtilsIntToString((int)debits_count);
//	IsoAddField(isoh, Debits_number, AddFieldNotApplicable, (unsigned char*) str_debits_count.c_str(), str_debits_count.length());						// 76
//	IsoAddField(isoh, Debits_reversal_number, AddFieldNotApplicable, (unsigned char*) "0", 1);
//
//	string str_auths_count = UtilsIntToString((int)authorisations);
//	IsoAddField(isoh, Authorisations_number, AddFieldNotApplicable, (unsigned char*) str_auths_count.c_str(), str_auths_count.length());				// 81
//
//	IsoAddField(isoh, Credits_amount, AddFieldNotApplicable, (unsigned char*) zero_pad((int)credits_total_amount, 12).c_str(), 12);						// 86
//	IsoAddField(isoh, Debits_amount, AddFieldNotApplicable, (unsigned char*) zero_pad((int)debits_total_amount, 12).c_str(), 12);						// 88
//
//	long amount_net_recon = debits_total_amount - credits_total_amount;
//	string str_amount_net_recon;
//
//	if (amount_net_recon > 0)
//		str_amount_net_recon = "D";
//	else{
//		str_amount_net_recon = "C";
//		amount_net_recon *= -1;
//	}
//
//	str_amount_net_recon.append(zero_pad((int)amount_net_recon, 16));
//
//	IsoAddField(isoh, Amount_net_reconciliation, AddFieldNotApplicable, (unsigned char*) str_amount_net_recon.c_str(), str_amount_net_recon.length());

	string str_sale_totals("422",3);  //LBP
	str_sale_totals.append(zero_pad(UtilsIntToString((int)debits_count_second_curr),3));
	str_sale_totals.append(zero_pad((int)debits_total_amount_second_curr, 12).c_str());

	str_sale_totals.append(zero_pad((int)0, 15).c_str());
	str_sale_totals.append(zero_pad((int)0, 15).c_str());
	str_sale_totals.append(zero_pad((int)0, 15).c_str());

	str_sale_totals.append("840",3);  //USD
	str_sale_totals.append(zero_pad(UtilsIntToString((int)debits_count),3));
	str_sale_totals.append(zero_pad((int)debits_total_amount, 12).c_str());

	str_sale_totals.append(zero_pad((int)0, 15).c_str());
	str_sale_totals.append(zero_pad((int)0, 15).c_str());
	str_sale_totals.append(zero_pad((int)0, 15).c_str());

	IsoAddField(isoh, F_63, AddFieldNotApplicable, (unsigned char*) str_sale_totals.c_str(), 126);						// 63



	unsigned char *iso_msg;
	unsigned int iso_msg_len;
	iso_msg_len = IsoBuildMsg(isoh, (unsigned char**) &iso_msg);

	IsoDestroyList(&isoh);

	if(iso_msg_len == 0){
		log.message(MSG_INFO "IsoBuildMsg failed\r\n");
		return -1;
	}

	string str_iso_msg = string((const char*)iso_msg, iso_msg_len);

	free(iso_msg);

	log.message(MSG_INFO "ReconAdvice ("+UtilsIntToString(str_iso_msg.length())+" bytes) ["+UtilsHexToString(str_iso_msg.c_str(), str_iso_msg.length())+"]\r\n");

	string response;

	if(send_message_param(str_iso_msg, response) < 0){
		log.message(MSG_INFO "ReconAdvice failed\r\n");

		// Log the tsn to the termcfg db
//		termcfg.setTerminalSeqNumber(UtilsStringToInt(str_tsn));	//iq_audi_030117

		return -1;
	}

	log.message(MSG_INFO "ReconAdvice ( response ("+UtilsIntToString(response.length())+" bytes) ["+UtilsHexToString(response.c_str(), response.length())+"]\r\n");

	isoh_resp = TaIsoDecode((unsigned char*) response.c_str(), response.length());

	if(isoh_resp < 0){
		log.message(MSG_INFO "TaIsoDecode failed with "+UtilsIntToString(isoh_resp)+"\r\n");
		return -1;
	}

	char *field_data=NULL;
	unsigned int field_len;

	if(IsoGetField(isoh_resp, Action_code, AddFieldNotApplicable, (unsigned char**) &field_data, &field_len) < 0){
		log.message(MSG_INFO "IsoGetField failed to get Action_code\r\n");
		IsoDestroyList(&isoh_resp);
		return -1;
	}else{
		string actioncode = MapActionCode(string(field_data, field_len));
		action_code=UtilsStringToInt(actioncode);
		log.message(MSG_INFO "Action code ["+string(field_data, field_len)+"] mapped --> ["+actioncode+"]\r\n");
	}

	free(field_data);

	return 0;
}

unsigned int Recon::getNoDebits(void)
{
	return debits_count;
}

unsigned int Recon::getNoDebitsSecondCurrency(void)
{
	return debits_count_second_curr;
}

unsigned int Recon::getNoCredits(void)
{
	return credits_count;
}

unsigned int Recon::getNoCreditsSecondCurrency(void)
{
	return credits_count_second_curr;
}

long Recon::getDebitsTotals(void)
{
	return debits_total_amount;
}

long Recon::getDebitsTotalsSecondCurrency(void)
{
	return debits_total_amount_second_curr;
}

long Recon::getCreditsTotals(void)
{
	return credits_total_amount;
}

long Recon::getCreditsTotalsSecondCurrency(void)
{
	return credits_total_amount_second_curr;
}

int Recon::getActionCode(void)
{
	return action_code;
}
