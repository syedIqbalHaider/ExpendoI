#include <sstream>
#include <iomanip>
#include <libda/cterminalconfig.h>
#include <libda/ctsn.h>
//#include <libcomms.h>
#include "FileAction.h"
#include "DeviceInfo.h"
#include "TermApp_8583.h"
#include "TermAppSession.h"
#include "CLog.h"
#include "utils.h"
#include "Message.h"
#include "HIMISOver.h"
#include "cstatus.h"

static CLog log(HIMISO_NAME);

using namespace Utils;
using namespace com_verifone_status;

int FileAction::PerformAction(void)
{
	return PerformAction("", "", false);
}

int FileAction::PerformAction(string file_version, string table_name, bool confirm_completion)
{
	int iret = 0;
	int previous_msg_no=get_msg_no(type);
	int finished=0;
	ISO_HANDLE isoh, isoh_resp;
	com_verifone_terminalconfig::CTerminalConfig termcfg;

	DeviceInfo info;
	string appid = zero_pad(info.AppId(), 15);
	string dev_sw_id = 	info.ProtocolVersion() +
						zero_pad(info.TermSerial().length(), 2) + info.TermSerial() +
						zero_pad(appid.length(), 2) + appid +
						info.AppLinkTimeStamp();

	while(1)
	{
		isoh = IsoCreateList(MTI_1304);

		com_verifone_tsn::CTsn ctsn;
		string str_tsn = zero_pad(ctsn.getNextTsn(), 6);
		IsoAddField(isoh, System_trace_audit_number, AddFieldNotApplicable, (unsigned char*)str_tsn.c_str(), str_tsn.length());								// 11

		string timestamp = UtilsTimestamp();
		IsoAddField(isoh, Date_and_time_local_transaction, AddFieldNotApplicable, (unsigned char*) timestamp.substr(2, 12).c_str(), 12);						// 12

		if(finished){
			if(!confirm_completion){
				// We will not send confirmation
				log.message(MSG_INFO "Will not send confirmation for this type\r\n");
				IsoDestroyList(&isoh);
				break;
			}

			IsoAddField(isoh, Function_code, AddFieldNotApplicable, (unsigned char*)"383", 3);																// 24
		}
		else
			IsoAddField(isoh, Function_code, AddFieldNotApplicable, (unsigned char*)"305", 3);																// 24

		string str_tid = space_pad(termcfg.getTerminalNo(), 8);
		IsoAddField(isoh, Terminal_ID, AddFieldNotApplicable, (unsigned char*)str_tid.c_str(), str_tid.length());											// 41

		string str_merchno;
		str_merchno.insert(0, 15, '0');
		str_merchno.replace(15-termcfg.getMerchantNo().length(), termcfg.getMerchantNo().length(), termcfg.getMerchantNo());
		IsoAddField(isoh, Card_acceptor_ID, AddFieldNotApplicable, (unsigned char*)str_merchno.c_str(), str_merchno.length());								// 42

		string str_msgno = UtilsIntToString(previous_msg_no);
		IsoAddField(isoh, Message_number, AddFieldNotApplicable, (unsigned char*)str_msgno.c_str(), str_msgno.length());									// 71

		IsoAddField(isoh, Data_record, AddFieldNotApplicable, (unsigned char*)dev_sw_id.c_str(), dev_sw_id.length());										// 72

		string file_name;
		CStatus status;

		switch(type){
			case FN_PARAMETERS:
				file_name = "TP";
				status.setStatus(PARM_DWNLD_TERMINAL);
				break;
			case FN_CUST_PARAMETERS:
				file_name = "CT";
				//status.setStatus(PARM_DWNLD_CUSTOM);
				break;
			case FN_DATE_TIME:
				file_name = "DT";
				//status.setStatus(PARM_DWNLD_DATE_TIME);
				break;
			case FN_CARD:
			{
				string tmp_ver = zero_pad(file_version, 4);
				file_name = "CP:"+tmp_ver+":"+table_name;
				//status.setStatus(PARM_DWNLD_CARDS);
				break;
			}
			case FN_SUPER_TOTALS:
				file_name = "CP:0000:spr_tot";
				//status.setStatus(PARM_DWNLD_CARDS);
				break;
			case FN_SPECIAL_BINS:
				file_name = "SB:0000:spc_bin";
				//status.setStatus(PARM_DWNLD_CARDS);
				break;
			case FN_COMMS_PARAMS:
				file_name = "CM:0000:com_prm";
				//status.setStatus(PARM_DWNLD_COMMS);
				break;
			case FN_MERCH_MSG:
				file_name = "MM:0000:msg_mrc";
				//status.setStatus(PARM_DWNLD_MERCHANT_MESSAGE);
				break;
			case FN_APP_BMP:
				file_name = "AB:0000:app_map";
				//status.setStatus(PARM_DWNLD_APP_BITMAP);
				break;
			case FN_SW_DNLD:
				file_name = "SD:0000:sft_dld";
				//status.setStatus(PARM_DWNLD_SW_UPDATE);
				break;
			case FN_HC_FULL:
			case FN_HC_PART:
			{
				string tmp_ver = zero_pad(file_version, 4);
				file_name = "HC:"+tmp_ver+":"+table_name+":0";
				//status.setStatus(PARM_DWNLD_HOTCARDS);
				break;
			}
			default:
				log.message(MSG_INFO "IsoBuildMsg failed\r\n");
				Error(ERROR_FORMAT);
				return -1;
		}

		// For the file action confirmation of successful download, override the filename with the name that was received from the host
		if(finished)
			file_name = filename;

		IsoAddField(isoh, File_name, AddFieldNotApplicable, (unsigned char*)file_name.c_str(), file_name.length());										// 101

		unsigned char *iso_msg;
		unsigned int iso_msg_len;
		iso_msg_len = IsoBuildMsg(isoh, (unsigned char**) &iso_msg);

		IsoDestroyList(&isoh);

		if(iso_msg_len == 0){
			log.message(MSG_INFO "IsoBuildMsg failed\r\n");
			Error(ERROR_FORMAT);
			return -2;
		}

		string str_iso_msg = string((const char*)iso_msg, iso_msg_len);

		free(iso_msg);

		log.message(MSG_INFO "File Action request ("+UtilsIntToString(str_iso_msg.length())+" bytes)\r\n");

		string str_iso_resp;
		iret = send_message_param(str_iso_msg, str_iso_resp);

		if(iret < 0){
			switch(iret)
			{
				case -1:
					log.message(MSG_INFO "send_message_param failed with "+UtilsIntToString(isoh_resp)+"\r\n");
					Error(ERROR_CONNECT);
					break;
				case -2:
					log.message(MSG_INFO "send_message_param failed with "+UtilsIntToString(isoh_resp)+"\r\n");
					Error(ERROR_SEND);
					break;
				case -3:
					log.message(MSG_INFO "send_message_param failed with "+UtilsIntToString(isoh_resp)+"\r\n");
					Error(ERROR_TIMEOUT);
					break;
			}
			iret = -8;
			goto ErrRet;
		}

		isoh_resp = TaIsoDecode((unsigned char*) str_iso_resp.c_str(), str_iso_resp.length());

		if(isoh_resp < 0){
			log.message(MSG_INFO "TaIsoDecode failed with "+UtilsIntToString(isoh_resp)+"\r\n");
			Error(ERROR_FORMAT);
			iret = -8;
			goto ErrRet;
		}

		if(finished)
			break;					// Finished

		char *field_data=NULL;
		unsigned int field_len;

		if(IsoGetField(isoh_resp, Action_code, AddFieldNotApplicable, (unsigned char**) &field_data, &field_len) < 0){
			log.message(MSG_INFO "IsoGetField failed to get Action_code\r\n");
			IsoDestroyList(&isoh_resp);
			return -1;
		}else{
			string actioncode = string(field_data, field_len);
			log.message(MSG_INFO "Action code ["+actioncode+"]\r\n");

			if((UtilsStringToInt(actioncode) != 300) && (UtilsStringToInt(actioncode) != 302)){
				log.message(MSG_INFO "File action request was declined!!!\r\n");

				free(field_data);
				IsoDestroyList(&isoh_resp);

				Error(ERROR_FORMAT);
				iret = -8;
				goto ErrRet;
			}
		}
		free(field_data);

		if(IsoGetField(isoh_resp, Function_code, AddFieldNotApplicable, (unsigned char**) &field_data, &field_len) < 0){
			log.message(MSG_INFO "IsoGetField failed to get Function_code\r\n");
			IsoDestroyList(&isoh_resp);
			Error(ERROR_FORMAT);
			iret = -9;
			goto ErrRet;
		}else{
			string func_code = string(field_data, field_len);

			log.message(MSG_INFO "Function code ["+func_code+"]\r\n");

			if(func_code.compare("301") == 0){
				log.message(MSG_INFO "Add record\r\n");
			}else if(func_code.compare("303") == 0){
				log.message(MSG_INFO "Delete record\r\n");
			}else if(func_code.compare("305") == 0){
				log.message(MSG_INFO "Replace data\r\n");
			}else if((func_code.compare("306") == 0) || (func_code.compare("380") == 0)){
				log.message(MSG_INFO "Expecting more data\r\n");
			}else if(func_code.compare("381") == 0){
				log.message(MSG_INFO "No more data expected\r\n");
				set_msg_no(type, 0);
				finished = 1;
			}else if(func_code.compare("382") == 0){
				log.message(MSG_INFO "No data\r\n");
				set_msg_no(type, 0);
				finished = 1;
			}else{
				log.message(MSG_INFO "Function code ["+func_code+"] unknown\n");
				Error(ERROR_FORMAT);
				iret = -10;
				goto ErrRet;
			}
		}

		free(field_data);

		if(!finished){
			if(IsoGetField(isoh_resp, Message_number, AddFieldNotApplicable, (unsigned char**) &field_data, &field_len) < 0){
				log.message(MSG_INFO "IsoGetField failed to get Message_number\r\n");
				IsoDestroyList(&isoh_resp);
				Error(ERROR_FORMAT);
				iret = -11;
				goto ErrRet;
			}else{
				string msgno = string(field_data, field_len);
				int temp = UtilsStringToInt(msgno);
				log.message(MSG_INFO "Message no ["+msgno+"]\r\n");

// TODO: Add status feedback to show that a messages has been received.

				if(temp != previous_msg_no + 1){
					log.message(MSG_INFO "Error - message no ["+msgno+"] is out of sequence\r\n");
					Error(ERROR_FORMAT);
					iret = -12;
					goto ErrRet;
				}

				set_msg_no(type, temp);	// Store the message number, in case we have to resume

				previous_msg_no = temp;
			}

			free(field_data);
		}

		if(IsoGetField(isoh_resp, File_name, AddFieldNotApplicable, (unsigned char**) &field_data, &field_len) >= 0){
			string incoming = string(field_data, field_len);
			free(field_data);

			log.message(MSG_INFO "Filename record ("+UtilsIntToString(incoming.length())+" bytes) ["+incoming+"]\r\n");

			if(FileName(previous_msg_no, incoming) < 0){
				log.message(MSG_INFO "Error - FileName failed\r\n");
				Error(ERROR_FILE_SYSTEM);
				iret = -13;
				goto ErrRet;
			}

			filename = incoming;
		}

		if(IsoGetField(isoh_resp, Data_record, AddFieldNotApplicable, (unsigned char**) &field_data, &field_len) < 0){
			// We will not treat the absense of this filed as a formatting error, since Termapp seems happy to send responses without it
			log.message(MSG_INFO "IsoGetField failed to get Data_record\r\n");
		}else{
			string incoming = string(field_data, field_len);
//			log.message(MSG_INFO "Data record ("+UtilsIntToString(incoming.length())+" bytes) ["+UtilsHexToString(incoming.c_str(), incoming.length())+"]\r\n");
			log.message(MSG_INFO "Data record ("+UtilsIntToString(incoming.length())+" bytes)\r\n");

			if(ResponseData(previous_msg_no, incoming)){
				log.message(MSG_INFO "Error - ResponseData failed\r\n");
				Error(ERROR_FILE_SYSTEM);
				iret = -14;
				goto ErrRet;
			}
		}

		free(field_data);

		IsoDestroyList(&isoh_resp);
/*
		if(finished)
			break;					// Finished
*/
	}

	FileActionComplete();

	return 0;

ErrRet:
	TermAppSession::End();

	return iret;
}

int FileAction::get_msg_no(FILENAME filename)
{
	int msgno=0;

	if((filename == FN_HC_FULL) || (filename == FN_HC_PART)){
		FILE *fh=NULL;

		fh = fopen("flash/hcmsg_no.dat", "rb");

		if (fh == NULL)
			return 0;

		fread(&msgno, 1, sizeof(int), fh);

		fclose(fh);
	}

	return msgno;
}

int FileAction::set_msg_no(FILENAME filename, int msg_no)
{
	if((filename == FN_HC_FULL) || (filename == FN_HC_PART)){
		FILE *fh=NULL;

		fh = fopen("flash/hcmsg_no.dat", "wb");

		if (fh == NULL)
			return -1;

		fwrite(&msg_no, 1, sizeof(int), fh);

		fclose(fh);
	}

	return 0;
}
