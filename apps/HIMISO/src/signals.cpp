#include <iostream>
#include <csignal>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <execinfo.h>
#include <ctime>
#include "HIMISOver.h"

#define LOG_PATH	"logs/"
#define APP_NAME	HIMISO_NAME
#define APP_VERSION HIMISO_VERSION

void signalHandler(int signum);

void SignalsConfigureHandlers(void)
{
	if (signal(SIGSTOP  , signalHandler) == SIG_ERR)   std::cout << "Cannot catch signal SIGSTOP  " << std::endl;
	if (signal(SIGHUP   , signalHandler) == SIG_ERR)   std::cout << "Cannot catch signal SIGHUP   " << std::endl;
	if (signal(SIGINT   , signalHandler) == SIG_ERR)   std::cout << "Cannot catch signal SIGINT   " << std::endl;
	if (signal(SIGQUIT  , signalHandler) == SIG_ERR)   std::cout << "Cannot catch signal SIGQUIT  " << std::endl;
	if (signal(SIGILL   , signalHandler) == SIG_ERR)   std::cout << "Cannot catch signal SIGILL   " << std::endl;
	if (signal(SIGABRT  , signalHandler) == SIG_ERR)   std::cout << "Cannot catch signal SIGABRT  " << std::endl;
	if (signal(SIGFPE   , signalHandler) == SIG_ERR)   std::cout << "Cannot catch signal SIGFPE   " << std::endl;
	if (signal(SIGKILL  , signalHandler) == SIG_ERR)   std::cout << "Cannot catch signal SIGKILL  " << std::endl;
	if (signal(SIGSEGV  , signalHandler) == SIG_ERR)   std::cout << "Cannot catch signal SIGSEGV  " << std::endl;
	if (signal(SIGPIPE  , signalHandler) == SIG_ERR)   std::cout << "Cannot catch signal SIGPIPE  " << std::endl;
	if (signal(SIGALRM  , signalHandler) == SIG_ERR)   std::cout << "Cannot catch signal SIGALRM  " << std::endl;
	if (signal(SIGTERM  , signalHandler) == SIG_ERR)   std::cout << "Cannot catch signal SIGTERM  " << std::endl;
	if (signal(SIGUSR1  , signalHandler) == SIG_ERR)   std::cout << "Cannot catch signal SIGUSR1  " << std::endl;
	if (signal(SIGUSR2  , signalHandler) == SIG_ERR)   std::cout << "Cannot catch signal SIGUSR2  " << std::endl;
//SIGCHLD
//SIGCONT
	if (signal(SIGTSTP  , signalHandler) == SIG_ERR)   std::cout << "Cannot catch signal SIGTSTP  " << std::endl;
	if (signal(SIGTTIN  , signalHandler) == SIG_ERR)   std::cout << "Cannot catch signal SIGTTIN  " << std::endl;
	if (signal(SIGTTOU  , signalHandler) == SIG_ERR)   std::cout << "Cannot catch signal SIGTTOU  " << std::endl;
	if (signal(SIGBUS   , signalHandler) == SIG_ERR)   std::cout << "Cannot catch signal SIGBUS   " << std::endl;
	if (signal(SIGPOLL  , signalHandler) == SIG_ERR)   std::cout << "Cannot catch signal SIGPOLL  " << std::endl;
	if (signal(SIGPROF  , signalHandler) == SIG_ERR)   std::cout << "Cannot catch signal SIGPROF  " << std::endl;
	if (signal(SIGSYS   , signalHandler) == SIG_ERR)   std::cout << "Cannot catch signal SIGSYS   " << std::endl;
	if (signal(SIGTRAP  , signalHandler) == SIG_ERR)   std::cout << "Cannot catch signal SIGTRAP  " << std::endl;
//SIGURG
	if (signal(SIGVTALRM, signalHandler) == SIG_ERR)   std::cout << "Cannot catch signal SIGVTALRM" << std::endl;
	if (signal(SIGXCPU  , signalHandler) == SIG_ERR)   std::cout << "Cannot catch signal SIGXCPU  " << std::endl;
	if (signal(SIGXFSZ  , signalHandler) == SIG_ERR)   std::cout << "Cannot catch signal SIGXFSZ  " << std::endl;
	if (signal(SIGIOT   , signalHandler) == SIG_ERR)   std::cout << "Cannot catch signal SIGIOT   " << std::endl;
//if (signal(SIGEMT, signalHandler) == SIG_ERR) std::cout << "Cannot catch signal SIGl(SIGEMT,< std::endl;
	if (signal(SIGSTKFLT, signalHandler) == SIG_ERR)   std::cout << "Cannot catch signal SIGSTKFLT" << std::endl;
	if (signal(SIGIO    , signalHandler) == SIG_ERR)   std::cout << "Cannot catch signal SIGIO    " << std::endl;
	if (signal(SIGCLD   , signalHandler) == SIG_ERR)   std::cout << "Cannot catch signal SIGCLD   " << std::endl;
	if (signal(SIGPWR   , signalHandler) == SIG_ERR)   std::cout << "Cannot catch signal SIGPWR   " << std::endl;
//if (signal(SIGINFO, signalHandler) == SIG_ERR) std::cout << "Cannot catch signal SIl(SIGINFO<< std::endl;
//if (signal(SIGLOST, signalHandler) == SIG_ERR) std::cout << "Cannot catch signal SIl(SIGLOST<< std::endl;
//SIGWINCH
	if (signal(SIGUNUSED  , signalHandler) == SIG_ERR) std::cout << "Cannot catch signal SIGUNUSED" << std::endl;
}

void signalHandler(int signum)
{
	if ((signum == SIGINT) || (signum == SIGQUIT)){
		std::cout << std::endl << "User terminated" << std::endl;
		exit(0);
	}

	switch(signum)
	{
		case 1: fprintf(stderr, "Error: SIGHUP	1	Hangup (POSIX)\n"); break;
		case 2: fprintf(stderr, "Error: SIGINT	2	Terminal interrupt (ANSI)\n"); break;
		case 3: fprintf(stderr, "Error: SIGQUIT	3	Terminal quit (POSIX)\n"); break;
		case 4: fprintf(stderr, "Error: SIGILL	4	Illegal instruction (ANSI)\n"); break;
		case 5: fprintf(stderr, "Error: SIGTRAP	5	Trace trap (POSIX)\n"); break;
		case 6: fprintf(stderr, "Error: SIGIOT	6	IOT Trap (4.2 BSD)\n"); break;
		case 7: fprintf(stderr, "Error: SIGBUS	7	BUS error (4.2 BSD)\n"); break;
		case 8: fprintf(stderr, "Error: SIGFPE	8	Floating point exception (ANSI)\n"); break;
		case 9: fprintf(stderr, "Error: SIGKILL	9	Kill(can't be caught or ignored) (POSIX)\n"); break;
		case 10: fprintf(stderr, "Error: SIGUSR1	10	User defined signal 1 (POSIX)\n"); break;
		case 11: fprintf(stderr, "Error: SIGSEGV	11	Invalid memory segment access (ANSI)\n"); break;
		case 12: fprintf(stderr, "Error: SIGUSR2	12	User defined signal 2 (POSIX)\n"); break;
		case 13: fprintf(stderr, "Error: SIGPIPE	13	Write on a pipe with no reader, Broken pipe (POSIX)\n"); break;
		case 14: fprintf(stderr, "Error: SIGALRM	14	Alarm clock (POSIX)\n"); break;
		case 15: fprintf(stderr, "Error: SIGTERM	15	Termination (ANSI)\n"); break;
		case 16: fprintf(stderr, "Error: SIGSTKFLT	16	Stack fault\n"); break;
		case 17: fprintf(stderr, "Error: SIGCHLD	17	Child process has stopped or exited, changed (POSIX)\n"); break;
		case 18: fprintf(stderr, "Error: SIGCONT	18	 Continue executing, if stopped (POSIX)\n"); break;
		case 19: fprintf(stderr, "Error: SIGSTOP	19	Stop executing(can't be caught or ignored) (POSIX)\n"); break;
		case 20: fprintf(stderr, "Error: SIGTSTP	20	Terminal stop signal (POSIX)\n"); break;
		case 21: fprintf(stderr, "Error: SIGTTIN	21	Background process trying to read, from TTY (POSIX)\n"); break;
		case 22: fprintf(stderr, "Error: SIGTTOU	22	Background process trying to write, to TTY (POSIX)\n"); break;
		case 23: fprintf(stderr, "Error: SIGURG	23	Urgent condition on socket (4.2 BSD)\n"); break;
		case 24: fprintf(stderr, "Error: SIGXCPU	24	CPU limit exceeded (4.2 BSD)\n"); break;
		case 25: fprintf(stderr, "Error: SIGXFSZ	25	File size limit exceeded (4.2 BSD)\n"); break;
		case 26: fprintf(stderr, "Error: SIGVTALRM	26	Virtual alarm clock (4.2 BSD)\n"); break;
		case 27: fprintf(stderr, "Error: SIGPROF	27	Profiling alarm clock (4.2 BSD)\n"); break;
		case 28: fprintf(stderr, "Error: SIGWINCH	28	Window size change (4.3 BSD, Sun)\n"); break;
		case 29: fprintf(stderr, "Error: SIGIO	29	I/O now possible (4.2 BSD)\n"); break;
		case 30: fprintf(stderr, "Error: SIGPWR	30	Power failure restart (System V)\n"); break;
	}

	void *array[30];
	size_t size;

	std::cout << "Interrupt signal (" << signum << ") received." << std::endl;

	// get void*'s for all entries on the stack
	size = backtrace(array, 30);

	// Generate a log filename that contains a timestamp
	time_t t = time(0);   // get time now
	struct tm * now = localtime( & t );
	char filename[40], buffer[50];
	sprintf(filename, "%sLog_%s-%d%02d%02d%02d%02d%02d.log", LOG_PATH, APP_NAME, (now->tm_year + 1900), (now->tm_mon + 1), now->tm_mday, now->tm_hour, now->tm_min, now->tm_sec);

	int fh = open(filename,  O_WRONLY|O_CREAT|O_TRUNC,S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);

	// NOTE: The first two entries in the output, is actually for the signal handler itself.
	// Signalled got generated at the third line of the output
	sprintf(buffer, "Error: signal %d:\n", signum);
	write(fh, buffer, strlen(buffer));

	sprintf(buffer, "%s version %s:\n", APP_NAME, APP_VERSION);
	write(fh, buffer, strlen(buffer));

	fprintf(stderr, "Error: signal %d:\n", signum);

	backtrace_symbols_fd(array, size, STDERR_FILENO);
	backtrace_symbols_fd(array, size, fh);

	fprintf(stderr, "Error logged to file: %s\n", filename);

	close(fh);

	exit(signum);
}


