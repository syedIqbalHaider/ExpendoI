#include <stddef.h>
#include <libcomms.h>
#include <libda/ccommsconfig.h>
#include <libda/ccommssettings.h>
#include <libda/ccommslog.h>
#include "utils.h"
#include "CLog.h"
#include "HIMISOver.h"
#include "cstatus.h"
#include "TermAppSession.h"

#include <svcmgr/svc_net.h>

TermAppSession* TermAppSession::single_instance = NULL;	// Static pointer to single instance
TermAppSession::ERROR TermAppSession::last_error = TermAppSession::NONE;
static int conn_stat=0;

static CLog log(HIMISO_NAME);

using namespace Utils;
using namespace com_verifone_status;

TermAppSession* TermAppSession::Establish(SESSION_TYPE type)
{
	if(!single_instance)
		single_instance = new TermAppSession(type);

	if(!conn_stat){
		single_instance->End();
		return NULL;
	}

	return single_instance;
}

void TermAppSession::End()
{
	delete single_instance;
	single_instance = NULL;
}

int TermAppSession::Send(string data)
{
	int ret = LIBCOMMS::SendCommand(data);

	if(ret <= 0){
		last_error = TermAppSession::SEND;
	}

	bytes_sent += data.length();

	return ret;
}

int TermAppSession::Receive(string &response)
{
	string incoming;
	int iret = LIBCOMMS::RecvResponse(incoming, receive_timeout);

	if(iret > 0){
//		log.message(MSG_INFO "TermAppSession::Receive ("+UtilsIntToString(incoming.length())+" bytes) ["+UtilsHexToString(incoming.c_str(), incoming.length())+"]\r\n");
		response = string(incoming.data()+2, iret-2);
		bytes_recv += response.length();
	}else{
		last_error = TermAppSession::TIMEOUT;
	}

	return iret;
}

TermAppSession::TermAppSession(SESSION_TYPE type)
{
	com_verifone_commsconfig::CCommsConfig comms_cfg;
	com_verifone_commssettings::CCommsSettings settings;
	com_verifone_commssettings::CCommsSettings::COMMS_TYPE comms_type;
	bool isPrimaryCom=true;
	bool isSecondaryIp=false;

	CStatus status;
	status.setStatus(COMMS_CONNECTING);

	while(1)
	{
		bytes_sent = 0;
		bytes_recv = 0;
		timestamp = UtilsTimestamp();

		tp_start = time(0);

		last_error = TermAppSession::NONE;

		if(!isSecondaryIp){
			if(type == SESSION_AUTH){
				log.message(MSG_INFO "Establishing TermAppSession for AUTH\n");

				if(isPrimaryCom)
					comms_cfg.getPriAuthCommsSettings(settings);
				else
					comms_cfg.getSecAuthCommsSettings(settings);

			}else if(type == SESSION_PARAM){
				log.message(MSG_INFO "Establishing TermAppSession for PARAMETERS\n");

				if(isPrimaryCom)
					comms_cfg.getPriParamCommsSettings(settings);
				else
					comms_cfg.getSecParamCommsSettings(settings);
			}

			session_type = type;
			comms_type = settings.getCommsType();
		}

		switch(comms_type)
		{
			case com_verifone_commssettings::CCommsSettings::ETHERNET:
				receive_timeout = 60;

				if(net_interfaceUp("eth0", NET_IP_V4))
					log.message(MSG_INFO "ETH UP FAILED  \r\n");
				else
					log.message(MSG_INFO "ETH UP SUCCESSFUL  \r\n");

				if(!isSecondaryIp)
				{
					ip = settings.getIp();
					port = settings.getPort();
				}else
				{
					ip = settings.getIp2();
					port = settings.getPort2();
				}

				log.message(MSG_INFO "TermAppSession connecting to "+ip+" "+port+"\n");

				if(LIBCOMMS::Connect(ip, port) == 0)
				{
					conn_stat = 1;
					return;
				}
				else if(!isSecondaryIp)
					isSecondaryIp=true;
				else if(isPrimaryCom)
				{
					isPrimaryCom=false;
					isSecondaryIp=false;
				}
				else{
					conn_stat = 0;
					last_error = TermAppSession::CONNECT;
					return;
				}

				break;

			case com_verifone_commssettings::CCommsSettings::GPRS:
				receive_timeout = 60;

				if(net_interfaceUp("ppp1", NET_IP_V4))
					log.message(MSG_INFO "GPRS UP FAILED  \r\n");
				else
					log.message(MSG_INFO "GPRS UP SUCCESSFUL  \r\n");

				if(!isSecondaryIp)
				{
					ip = settings.getIp();
					port = settings.getPort();
				}else
				{
					ip = settings.getIp2();
					port = settings.getPort2();
				}

				log.message(MSG_INFO "TermAppSession GPRS connecting to "+ip+" "+port+"\n");

				if(LIBCOMMS::Connect(ip, port) == 0)
				{
					conn_stat = 1;
					return;
				}
				else if(!isSecondaryIp)
					isSecondaryIp=true;
				else if(isPrimaryCom)
				{
					isPrimaryCom=false;
					isSecondaryIp=false;
				}
				else{
					conn_stat = 0;
					last_error = TermAppSession::CONNECT;
					return;
				}

				break;
			default:// not handled!
				return;
				break;
		}
	}
}

TermAppSession::ERROR TermAppSession::GetLastError()
{
	return last_error;
}

int TermAppSession::writeCommsLog()
{
	com_verifone_commslog::CCommsLog::CommsType type=com_verifone_commslog::CCommsLog::CT_AUTH;
	com_verifone_commslog::CCommsLog::CommsStatus status=com_verifone_commslog::CCommsLog::CS_DISCONNECT;

	switch(session_type)
	{
		case SESSION_AUTH:	type = com_verifone_commslog::CCommsLog::CT_AUTH; break;
		case SESSION_PARAM:	type = com_verifone_commslog::CCommsLog::CT_PARAM; break;
	}

	if(last_error == TIMEOUT)
		status = com_verifone_commslog::CCommsLog::CS_TIMEOUT;
	else if(last_error == CONNECT)
		status = com_verifone_commslog::CCommsLog::CS_NO_CONNECT;
	else if((last_error == SEND) || (last_error == TIMEOUT))
		status = com_verifone_commslog::CCommsLog::CS_TIMEOUT;

	tp_end = time(0);

	return com_verifone_commslog::CCommsLog::Log(type, status, timestamp, ip, port, (tp_end-tp_start), bytes_sent, bytes_recv);
}

TermAppSession::~TermAppSession()
{
	writeCommsLog();
	LIBCOMMS::Disconnect(0);
}
