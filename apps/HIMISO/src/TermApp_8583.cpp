#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "bmp.h"
#include "TermApp_8583.h"
#include "utils.h"
#include "libda/cterminalconfig.h"

//using namespace std;
using namespace Utils;
using namespace com_verifone_terminalconfig;

#ifdef ISO8583_DBG
#define ISO8583Dbg TraceString
#else
#define ISO8583Dbg(...){};
#endif // ISO8583_DBG

typedef enum
{
  ELEMENT,        // Refers to normal elements
  ADDITIONAL      // Refers to composite elements in the additional data elements field 48
}ELEMENT_TYPE;

typedef enum
{
//  BITMAP,
	BMP,
	FIXED_SIZE,
	LLVAR,
	LLLVAR,
	LLLLVAR,
	LLLLLVAR,
	LLLLLLVAR,
	YYMMDD,
	YYMMDDHHMMSS,
	MMDD,
	YYMM,
	CCYYMMDDTHHMMSS,
	MMDDHHMMSS,
	HHMMSS,
	CCYYMMDD,
	FIXED_SIZE_ASCII,		// iq_audi_281116  for fixed lenght of ASCII data fields
	NO_DATA_SIZE
}FORMAT;

typedef enum
{
	BINARY,
	NUMERIC,
	ALPHA_NUMERIC,
	ALPHA_NUM_SPEC,
	ALPHA_NUM_SPEC_BINARY,
	NUMERIC_SPECIAL,
	X_NUMERIC,
	Z_TRACK,
	//iq_audi_2016
	BCD_NUMERIC,		//must not use with variable length
	NUMERIC_SPECIAL_BCD,	//use for variable length bcd data
	NO_ISO_DATA
}ATTR;

typedef struct
{   
    FORMAT      format;
    ATTR          attribute;
    long          size;
}BITS_ATTR;

typedef struct
{
  ELEMENT_TYPE element_type;
  BITS_ATTR bit_attribs;
  unsigned int bit_no;
  unsigned char *field_data;
  unsigned int field_data_len;
}DataElement;


const BITS_ATTR bits_attr[127] = {
                        {FIXED_SIZE   , BINARY                , 8},     // 1
                        {LLVAR        , Z_TRACK   			  , 19},    // 2		//BCD , it is actually not a Z-track but treat pan
                        {FIXED_SIZE   , BCD_NUMERIC           , 6},     // 3		//BCD
                        {FIXED_SIZE   , BCD_NUMERIC           , 12},    // 4		//BCD
                        {FIXED_SIZE   , NUMERIC               , 12},    // 5
                        {FIXED_SIZE   , NUMERIC               , 12},    // 6
                        {FIXED_SIZE   , BCD_NUMERIC           , 10},    // 7
                        {FIXED_SIZE   , NUMERIC               , 8},     // 8
                        {FIXED_SIZE   , NUMERIC               , 8},     // 9
                        {FIXED_SIZE   , NUMERIC               , 8},     // 10
                        {FIXED_SIZE   , BCD_NUMERIC		      , 6},     // 11		//BCD
                        {HHMMSS 	  , BCD_NUMERIC           , 6},    // 12		//BCD
                        {MMDD         , BCD_NUMERIC           , 4},     // 13		//BCD
                        {YYMM         , BCD_NUMERIC           , 4},     // 14		//BCD
                        {FIXED_SIZE   , NUMERIC               , 6},     // 15
                        {FIXED_SIZE   , NUMERIC               , 4},     // 16
                        {FIXED_SIZE   , NUMERIC               , 4},     // 17
                        {FIXED_SIZE   , NUMERIC               , 4},     // 18
                        {FIXED_SIZE   , NUMERIC               , 3},     // 19
                        {FIXED_SIZE   , NUMERIC               , 3},     // 20
                        {FIXED_SIZE   , NUMERIC               , 3},     // 21
                        {FIXED_SIZE   , BCD_NUMERIC           , 3},    // 22		//BCD
                        {FIXED_SIZE   , BCD_NUMERIC           , 3},     // 23		//BCD
                        {FIXED_SIZE   , BCD_NUMERIC           , 3},     // 24		//BCD
                        {FIXED_SIZE   , BCD_NUMERIC           , 2},     // 25		//BCD
                        {FIXED_SIZE   , NUMERIC               , 4},     // 26
                        {FIXED_SIZE   , NUMERIC               , 1},     // 27
                        {YYMMDD       , NUMERIC               , 6},     // 28
                        {FIXED_SIZE   , NUMERIC               , 3},     // 29
                        {FIXED_SIZE   , NUMERIC               , 24},    // 30
                        {LLVAR        , ALPHA_NUM_SPEC        , 99},    // 31
                        {LLVAR        , NUMERIC               , 11},    // 32
                        {LLVAR        , NUMERIC               , 11},    // 33
                        {LLVAR        , NUMERIC_SPECIAL       , 28},    // 34
                        {LLVAR        , Z_TRACK               , 37},    // 35
                        {LLLVAR       , NUMERIC_SPECIAL       , 104},   // 36
                        {FIXED_SIZE_ASCII   , ALPHA_NUMERIC   , 12},    // 37
                        {FIXED_SIZE_ASCII   , NUMERIC_SPECIAL , 6},     // 38
                        {FIXED_SIZE_ASCII   , NUMERIC         ,2},     // 39
                        {FIXED_SIZE_ASCII   , NUMERIC         , 3},     // 40
                        {FIXED_SIZE_ASCII   , ALPHA_NUM_SPEC  , 8},     // 41
                        {FIXED_SIZE_ASCII   , NUMERIC         , 15},    // 42
                        {LLVAR        , ALPHA_NUM_SPEC        , 99},    // 43
                        {LLVAR        , ALPHA_NUM_SPEC        , 99},    // 44
                        {LLVAR        , ALPHA_NUM_SPEC        , 76},    // 45
                        {LLLVAR       , ALPHA_NUM_SPEC        , 204},   // 46
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 47
                        {LLLLVAR      , BINARY                , 9999},  // 48
                        {FIXED_SIZE   , BCD_NUMERIC           , 3},     // 49		//BCD
                        {FIXED_SIZE   , NUMERIC               , 3},     // 50
                        {FIXED_SIZE   , ALPHA_NUMERIC         , 3},     // 51
                        {FIXED_SIZE   , BINARY                , 8},     // 52		//BCD
                        {LLVAR        , BINARY                , 48},    // 53
                        {LLLVAR       , ALPHA_NUM_SPEC        , 120},   // 54
                        {LLLVAR       , BINARY                , 999},   // 55
                        {LLVAR        , NUMERIC_SPECIAL       , 35},    // 56
                        {FIXED_SIZE   , NUMERIC               , 3},     // 57
                        {LLVAR        , NUMERIC               , 11},    // 58
                        {LLLVAR       , ALPHA_NUM_SPEC        , 200},   // 59
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 60
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 61
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 62
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 63
                        {FIXED_SIZE   , BINARY                , 8},     // 64
                        {FIXED_SIZE   , BINARY                , 8},     // 65
                        {LLLVAR       , ALPHA_NUM_SPEC        , 204},   // 66
                        {FIXED_SIZE   , NUMERIC               , 2},     // 67
                        {FIXED_SIZE   , NUMERIC               , 3},     // 68
                        {FIXED_SIZE   , NUMERIC               , 3},     // 69
                        {FIXED_SIZE   , NUMERIC               , 3},     // 70
                        {FIXED_SIZE   , NUMERIC               , 8},     // 71
                        {LLLVAR       , ALPHA_NUM_SPEC_BINARY , 999},   // 72
                        {FIXED_SIZE   , NUMERIC               , 6},     // 73
                        {FIXED_SIZE   , NUMERIC               , 10},    // 74
                        {FIXED_SIZE   , NUMERIC               , 10},    // 75
                        {FIXED_SIZE   , NUMERIC               , 10},    // 76
                        {FIXED_SIZE   , NUMERIC               , 10},    // 77
                        {FIXED_SIZE   , NUMERIC               , 10},    // 78
                        {FIXED_SIZE   , NUMERIC               , 10},    // 79
                        {FIXED_SIZE   , NUMERIC               , 10},    // 80
                        {FIXED_SIZE   , NUMERIC               , 10},    // 81
                        {FIXED_SIZE   , NUMERIC               , 10},    // 82
                        {FIXED_SIZE   , NUMERIC               , 10},    // 83
                        {FIXED_SIZE   , NUMERIC               , 10},    // 84
                        {FIXED_SIZE   , NUMERIC               , 10},    // 85
                        {FIXED_SIZE   , NUMERIC               , 16},    // 86
                        {FIXED_SIZE   , NUMERIC               , 16},    // 87
                        {FIXED_SIZE   , NUMERIC               , 16},    // 88
                        {FIXED_SIZE   , NUMERIC               , 16},    // 89
                        {FIXED_SIZE   , NUMERIC               , 10},    // 90
                        {FIXED_SIZE   , NUMERIC               , 3},     // 91
                        {FIXED_SIZE   , NUMERIC               , 3},     // 92
                        {LLVAR        , NUMERIC               , 11},    // 93
                        {LLVAR        , NUMERIC               , 11},    // 94
                        {LLVAR        , ALPHA_NUM_SPEC        , 99},    // 95
                        {LLLVAR       , BINARY                , 999},   // 96
                        {FIXED_SIZE   , X_NUMERIC             , 16},    // 97
                        {FIXED_SIZE   , ALPHA_NUM_SPEC        , 25},    // 98
                        {FIXED_SIZE   , ALPHA_NUMERIC         , 11},    // 99
                        {LLVAR        , NUMERIC               , 11},    // 100
                        {LLVAR        , ALPHA_NUM_SPEC        , 99},    // 101
                        {LLVAR        , ALPHA_NUM_SPEC        , 28},    // 102
                        {LLVAR        , ALPHA_NUM_SPEC        , 28},    // 103
                        {LLLLVAR      , ALPHA_NUM_SPEC_BINARY , 9999},  // 104
                        {FIXED_SIZE   , NUMERIC               , 16},    // 105
                        {FIXED_SIZE   , NUMERIC               , 16},    // 106
                        {FIXED_SIZE   , NUMERIC               , 10},    // 107
                        {FIXED_SIZE   , NUMERIC               , 10},    // 108
                        {LLVAR        , ALPHA_NUM_SPEC        , 84},    // 109
                        {LLVAR        , ALPHA_NUM_SPEC        , 84},    // 110
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 111
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 112
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 113
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 114
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 115
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 116
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 117
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 118
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 119
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 120
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 121
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 122
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 123
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 124
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 125
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 126
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999}};  // 127

#if 0	// original
const BITS_ATTR bits_attr[127] = {
                        {FIXED_SIZE   , BINARY                , 8},     // 1
                        {LLVAR        , NUMERIC_SPECIAL       , 19},    // 2
                        {FIXED_SIZE   , NUMERIC               , 6},     // 3
                        {FIXED_SIZE   , NUMERIC               , 12},    // 4
                        {FIXED_SIZE   , NUMERIC               , 12},    // 5
                        {FIXED_SIZE   , NUMERIC               , 12},    // 6
                        {FIXED_SIZE   , NUMERIC               , 10},    // 7
                        {FIXED_SIZE   , NUMERIC               , 8},     // 8
                        {FIXED_SIZE   , NUMERIC               , 8},     // 9
                        {FIXED_SIZE   , NUMERIC               , 8},     // 10
                        {FIXED_SIZE   , NUMERIC               , 6},     // 11
                        {YYMMDDHHMMSS , NUMERIC               , 12},    // 12
                        {FIXED_SIZE   , NUMERIC               , 4},     // 13
                        {YYMM         , NUMERIC               , 4},     // 14
                        {FIXED_SIZE   , NUMERIC               , 6},     // 15
                        {FIXED_SIZE   , NUMERIC               , 4},     // 16
                        {FIXED_SIZE   , NUMERIC               , 4},     // 17
                        {FIXED_SIZE   , NUMERIC               , 4},     // 18
                        {FIXED_SIZE   , NUMERIC               , 3},     // 19
                        {FIXED_SIZE   , NUMERIC               , 3},     // 20
                        {FIXED_SIZE   , NUMERIC               , 3},     // 21
                        {FIXED_SIZE   , ALPHA_NUMERIC         , 15},    // 22
                        {FIXED_SIZE   , NUMERIC               , 3},     // 23
                        {FIXED_SIZE   , NUMERIC               , 3},     // 24
                        {FIXED_SIZE   , NUMERIC               , 4},     // 25
                        {FIXED_SIZE   , NUMERIC               , 4},     // 26
                        {FIXED_SIZE   , NUMERIC               , 1},     // 27
//                        {FIXED_SIZE   , YYMMDD                , 6},     // 28
                        {YYMMDD       , NUMERIC               , 6},     // 28
                        {FIXED_SIZE   , NUMERIC               , 3},     // 29
                        {FIXED_SIZE   , NUMERIC               , 24},    // 30
                        {LLVAR        , ALPHA_NUM_SPEC        , 99},    // 31
                        {LLVAR        , NUMERIC               , 11},    // 32
                        {LLVAR        , NUMERIC               , 11},    // 33
                        {LLVAR        , NUMERIC_SPECIAL       , 28},    // 34
                        {LLVAR        , Z_TRACK               , 37},    // 35
                        {LLLVAR       , NUMERIC_SPECIAL       , 104},   // 36
                        {FIXED_SIZE   , ALPHA_NUMERIC         , 12},    // 37
                        {FIXED_SIZE   , NUMERIC_SPECIAL       , 6},     // 38
                        {FIXED_SIZE   , NUMERIC               , 3},     // 39
                        {FIXED_SIZE   , NUMERIC               , 3},     // 40
                        {FIXED_SIZE   , ALPHA_NUM_SPEC        , 8},     // 41
                        {FIXED_SIZE   , NUMERIC               , 15},    // 42
                        {LLVAR        , ALPHA_NUM_SPEC        , 99},    // 43
                        {LLVAR        , ALPHA_NUM_SPEC        , 99},    // 44
                        {LLVAR        , ALPHA_NUM_SPEC        , 76},    // 45
                        {LLLVAR       , ALPHA_NUM_SPEC        , 204},   // 46
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 47
                        {LLLLVAR      , BINARY                , 9999},  // 48
                        {FIXED_SIZE   , NUMERIC               , 3},     // 49
                        {FIXED_SIZE   , NUMERIC               , 3},     // 50
                        {FIXED_SIZE   , ALPHA_NUMERIC         , 3},     // 51
                        {FIXED_SIZE   , BINARY                , 8},     // 52
                        {LLVAR        , BINARY                , 48},    // 53
                        {LLLVAR       , ALPHA_NUM_SPEC        , 120},   // 54
                        {LLLVAR       , BINARY                , 999},   // 55
                        {LLVAR        , NUMERIC_SPECIAL       , 35},    // 56
                        {FIXED_SIZE   , NUMERIC               , 3},     // 57
                        {LLVAR        , NUMERIC               , 11},    // 58
                        {LLLVAR       , ALPHA_NUM_SPEC        , 200},   // 59
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 60
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 61
                        {LLLVAR       , NUMERIC               , 5},     // 62
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 63
                        {FIXED_SIZE   , BINARY                , 8},     // 64
                        {FIXED_SIZE   , BINARY                , 8},     // 65
                        {LLLVAR       , ALPHA_NUM_SPEC        , 204},   // 66
                        {FIXED_SIZE   , NUMERIC               , 2},     // 67
                        {FIXED_SIZE   , NUMERIC               , 3},     // 68
                        {FIXED_SIZE   , NUMERIC               , 3},     // 69
                        {FIXED_SIZE   , NUMERIC               , 3},     // 70
                        {FIXED_SIZE   , NUMERIC               , 8},     // 71
                        {LLLVAR       , ALPHA_NUM_SPEC_BINARY , 999},   // 72
                        {FIXED_SIZE   , NUMERIC               , 6},     // 73
                        {FIXED_SIZE   , NUMERIC               , 10},    // 74
                        {FIXED_SIZE   , NUMERIC               , 10},    // 75
                        {FIXED_SIZE   , NUMERIC               , 10},    // 76
                        {FIXED_SIZE   , NUMERIC               , 10},    // 77
                        {FIXED_SIZE   , NUMERIC               , 10},    // 78
                        {FIXED_SIZE   , NUMERIC               , 10},    // 79
                        {FIXED_SIZE   , NUMERIC               , 10},    // 80
                        {FIXED_SIZE   , NUMERIC               , 10},    // 81
                        {FIXED_SIZE   , NUMERIC               , 10},    // 82
                        {FIXED_SIZE   , NUMERIC               , 10},    // 83
                        {FIXED_SIZE   , NUMERIC               , 10},    // 84
                        {FIXED_SIZE   , NUMERIC               , 10},    // 85
                        {FIXED_SIZE   , NUMERIC               , 16},    // 86
                        {FIXED_SIZE   , NUMERIC               , 16},    // 87
                        {FIXED_SIZE   , NUMERIC               , 16},    // 88
                        {FIXED_SIZE   , NUMERIC               , 16},    // 89
                        {FIXED_SIZE   , NUMERIC               , 10},    // 90
                        {FIXED_SIZE   , NUMERIC               , 3},     // 91
                        {FIXED_SIZE   , NUMERIC               , 3},     // 92
                        {LLVAR        , NUMERIC               , 11},    // 93
                        {LLVAR        , NUMERIC               , 11},    // 94
                        {LLVAR        , ALPHA_NUM_SPEC        , 99},    // 95
                        {LLLVAR       , BINARY                , 999},   // 96
                        {FIXED_SIZE   , X_NUMERIC             , 16},    // 97
                        {FIXED_SIZE   , ALPHA_NUM_SPEC        , 25},    // 98
                        {FIXED_SIZE   , ALPHA_NUMERIC         , 11},    // 99
                        {LLVAR        , NUMERIC               , 11},    // 100
                        {LLVAR        , ALPHA_NUM_SPEC        , 99},    // 101
                        {LLVAR        , ALPHA_NUM_SPEC        , 28},    // 102
                        {LLVAR        , ALPHA_NUM_SPEC        , 28},    // 103
                        {LLLLVAR      , ALPHA_NUM_SPEC_BINARY , 9999},  // 104
                        {FIXED_SIZE   , NUMERIC               , 16},    // 105
                        {FIXED_SIZE   , NUMERIC               , 16},    // 106
                        {FIXED_SIZE   , NUMERIC               , 10},    // 107
                        {FIXED_SIZE   , NUMERIC               , 10},    // 108
                        {LLVAR        , ALPHA_NUM_SPEC        , 84},    // 109
                        {LLVAR        , ALPHA_NUM_SPEC        , 84},    // 110
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 111
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 112
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 113
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 114
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 115
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 116
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 117
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 118
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 119
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 120
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 121
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 122
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 123
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 124
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 125
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 126
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999}};  // 127
#endif


const BITS_ATTR bits_attr_add_data[16] = {
                        {FIXED_SIZE   , ALPHA_NUM_SPEC        , 19},    // 48-F0.1
                        {FIXED_SIZE   , ALPHA_NUMERIC         , 2},     // 48-F0.2
                        {FIXED_SIZE   , ALPHA_NUMERIC         , 4},     // 48-F0.3
                        {FIXED_SIZE   , NUMERIC               , 4},     // 48-F0.4
                        {LLLVAR       , ALPHA_NUM_SPEC        , 255},   // 48-F0.5
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 48-F0.6
                        {FIXED_SIZE   , ALPHA_NUM_SPEC        , 48},    // 48-F0.7
                        {LLVAR        , ALPHA_NUM_SPEC        , 50},    // 48-F0.8
                        {FIXED_SIZE   , ALPHA_NUM_SPEC        , 29},    // 48-F0.9
                        {FIXED_SIZE   , ALPHA_NUM_SPEC        , 1},     // 48-F0.10
                        {FIXED_SIZE   , ALPHA_NUM_SPEC        , 1},     // 48-F0.11
                        {LLLVAR       , ALPHA_NUM_SPEC        , 999},   // 48-F0.12
                        {FIXED_SIZE   , ALPHA_NUM_SPEC        , 31},    // 48-F0.13
                        {FIXED_SIZE   , ALPHA_NUM_SPEC        , 253},   // 48-F0.14
                        {LLVAR        , ALPHA_NUM_SPEC        , 28},    // 48-F0.15
                        {LLLLVAR      , ALPHA_NUM_SPEC        , 9999}}; // 48-F0.16

unsigned int get_additional_data_length(ISO_HANDLE handle);
unsigned int build_additional_data_field(ISO_HANDLE handle, unsigned char **msg);
unsigned int DecodeAdditionalData(ISO_HANDLE handle, unsigned char *msg, unsigned int length);
unsigned int PrintFormattedData(ISO_HANDLE handle, ELEMENT_TYPE el_type, unsigned int bitno, unsigned char *data, unsigned int data_offset);
unsigned int GetDataLen(BITS_ATTR *attr, unsigned char *data, unsigned int data_offset);
static char *TaIsoGetDescr(BIT_FIELD bit_field);
static char *TaIsoGetAddDataDescr(BIT_FIELDS_ADD_DATA bit_field);

// Message field defenitions
// This list defines which fields get populated for every Message Type Identifier (MTI)
const unsigned int mti_1100_def[]={2, 3, 4, 7, 11, 12, 14, 22, 23, 24, 28, 29, 35, 41, 42, 46, 48, 49, 52, 53, 54, 55, 56, 67, 104};
const unsigned int mti_1200_def[]={2, 3, 4, 7, 11, 12, 14, 22, 23, 24, 28, 29, 35, 41, 42, 46, 48, 49, 52, 53, 54, 55, 56, 67, 104};
const unsigned int mti_1220_def[]={2, 3, 4, 7, 11, 12, 14, 22, 23, 24, 25, 29, 35, 37, 38, 39, 41, 42, 48, 49, 52, 53, 54, 55, 56, 67};
const unsigned int mti_1304_def[]={11, 24, 41, 42, 71, 72, 101};
const unsigned int mti_1420_def[]={2, 3, 4, 7, 11, 12, 14, 22, 23, 24, 25, 29, 35, 39, 41, 42, 48, 49, 54, 55, 67};
const unsigned int mti_1520_def[]={7,11,12,24, 29, 41, 42, 50, 74, 75, 76, 77, 81, 86, 88, 97};

////////////////////////////////////////////////
// Linked list variables
////////////////////////////////////////////////

#define MAX_LISTS     20

#define INVALID_HANDLE    -1

typedef int LIST_HANDLE;

typedef struct _list_value
{
  unsigned char *data;
  unsigned int size;
  unsigned int no;
  unsigned int mark;
  ELEMENT_TYPE element_type;
  struct _list_value *next;
  struct _list_value *prev;
}TaListValue;

int TaListInit(void);
LIST_HANDLE TaListCreate(MTI mti);
int TaListAdd(LIST_HANDLE handle, unsigned char *data, unsigned int size);
TaListValue *TaListGet(LIST_HANDLE handle, unsigned int no);
TaListValue *TaListGetByCount(LIST_HANDLE handle, unsigned int count);
unsigned int TaListGetCount(LIST_HANDLE handle);
int TaListDestroy(LIST_HANDLE *handle);
int TaListDeleteMarked(LIST_HANDLE handle);
int TaListDeleteNonMarked(LIST_HANDLE handle);

typedef struct
{
  MTI mti;
  TaListValue *first;
  TaListValue *last;
  unsigned int item_count;
  unsigned int occupied;
}TaList;

TaList ta_lists[MAX_LISTS];

static int init_flag=0;

ISO_HANDLE IsoCreateList(MTI mti)
{
  LIST_HANDLE listh;

  if(init_flag == 0)
  {
    TaListInit();
    init_flag = 1;
    BmpInit();
  }

  listh = TaListCreate(mti);

  if(listh == INVALID_HANDLE)
    return ISO_INVALID_HANDLE;

  return (ISO_HANDLE)listh;
}

void IsoDestroyList(ISO_HANDLE *handle)
{
  unsigned int count, i;
  TaListValue *list_val;
  DataElement dat_element;

  // Free the individual message field data before destroying the list
  count = TaListGetCount((LIST_HANDLE)*handle);

  for(i=0; i<count; i++)
  {
    memset(&dat_element, 0, sizeof(DataElement));  

    list_val = TaListGet((LIST_HANDLE)*handle, i+1);

    memcpy(&dat_element, list_val->data, sizeof(DataElement));

    free(dat_element.field_data);
  }

  TaListDestroy((LIST_HANDLE*)handle);
}

typedef enum
{
  PAD_NONE,
  PAD_SPACE_LEFT,
  PAD_SPACE_RIGHT,
  PAD_0_LEFT,
  PAD_0_RIGHT
}PADDING_SPEC;

int IsoDelField(ISO_HANDLE handle, BIT_FIELD bit_field, BIT_FIELDS_ADD_DATA add_bit_field)
{
	TaListValue *list_value=NULL;
	DataElement *data_element;
	ELEMENT_TYPE element_type=ELEMENT;
	
	if(handle<0 || ta_lists[handle].occupied==0) return(-1);
	
	if(bit_field==Additional_data_private) element_type=ADDITIONAL;

	list_value=ta_lists[handle].first;
	while(list_value) {
		data_element=(DataElement *)list_value->data;
		if((element_type==ELEMENT && data_element->bit_no==bit_field) || (element_type==ADDITIONAL && data_element->bit_no==add_bit_field)) {
			if(data_element->field_data) free(data_element->field_data);
			free(list_value->data);
			if(list_value->prev) list_value->prev->next=list_value->next; else ta_lists[handle].first=list_value->next;
			if(list_value->next) list_value->next->prev=list_value->prev; else ta_lists[handle].last=list_value->prev;
			ta_lists[handle].item_count--;
			break;
		}
		list_value=list_value->next;
	}
	
	return(ISO_SUCCESS);
}

int IsoAddField(ISO_HANDLE handle, BIT_FIELD bit_field, BIT_FIELDS_ADD_DATA add_bit_field, unsigned char *data, unsigned int len)
{
  unsigned char *msg_data=data;   // This pointer might be replaced to point somewhere else
  char length_str[10];
  unsigned int msg_len=0, i, bcd_len=0, append_byte=0;
  DataElement dat_element;
  ELEMENT_TYPE el_type;
  unsigned char *msg_data_bcd;
  char bcd_data[128];
  string buff;

  int varLen=0;
  char length_char[10]={0};
  string length;


  // Check that normal elements and additional elements are used properly
  if(((bit_field != Additional_data_private) && (add_bit_field != AddFieldNotApplicable)) ||
    ((bit_field == Additional_data_private) && (add_bit_field == AddFieldNotApplicable)))
    return ISO_BITFIELD_SPEC_ERROR;

  memset(&dat_element, 0, sizeof(DataElement));
  memset(length_str, 0, sizeof(length_str));

  if(bit_field == Additional_data_private)
  {
    el_type = ADDITIONAL;
    memcpy(&dat_element.bit_attribs, &bits_attr_add_data[add_bit_field-1], sizeof(BITS_ATTR));
  }
  else
  {
    el_type = ELEMENT;
    memcpy(&dat_element.bit_attribs, &bits_attr[bit_field-1], sizeof(BITS_ATTR));
  }

  // Perform padding where needed
  switch(dat_element.bit_attribs.attribute)
  {

    case BINARY:
    case ALPHA_NUM_SPEC_BINARY:
          msg_data = (unsigned char*)malloc(len+3);

          if(msg_data == NULL)
            return ISO_MEM_ERROR;

          memset(msg_data, 0, len+2);
		  memcpy(msg_data, data, len);
         break;

    case BCD_NUMERIC:	//iq_audi_191116
    	if(dat_element.bit_attribs.size%2)
    		append_byte=1;

		msg_data = (unsigned char*)malloc(dat_element.bit_attribs.size+append_byte+3);
		msg_data_bcd=(unsigned char*)malloc(dat_element.bit_attribs.size+append_byte+3); //iq_audi_231116

		if(msg_data == NULL) return ISO_MEM_ERROR;
		if(msg_data_bcd == NULL) return ISO_MEM_ERROR;

		memset(msg_data, 0, dat_element.bit_attribs.size+append_byte+2);
		memset(msg_data_bcd, 0, dat_element.bit_attribs.size+append_byte+2);

		if((dat_element.bit_attribs.size+append_byte) > (long)len)
		{
			memset(msg_data_bcd, '0', dat_element.bit_attribs.size+2);
			memcpy(&msg_data_bcd[dat_element.bit_attribs.size+append_byte-len], data, len);
		}
		else
			memcpy(msg_data_bcd, data, dat_element.bit_attribs.size);

			memset(bcd_data,0,sizeof(bcd_data));

			buff=string((char *)msg_data_bcd);
			len=UtilStringToBCD(buff, bcd_data);
			memcpy(msg_data, bcd_data, len);

//			cout << "iq  Len : "<< len  << "   String DATa: "<< buff.c_str()<< endl;
			cout << "iq  Len : "<< len  << "   BCD DATa: "<< UtilsBcdToString(bcd_data,len)<< endl;

		break;
    case NUMERIC:
            msg_data = (unsigned char*)malloc(dat_element.bit_attribs.size+3);

            if(msg_data == NULL)
              return ISO_MEM_ERROR;

            memset(msg_data, 0, dat_element.bit_attribs.size+2);

            if(dat_element.bit_attribs.size > (long)len)
            {
              memset(msg_data, '0', dat_element.bit_attribs.size);
              memcpy(&msg_data[dat_element.bit_attribs.size-len], data, len);
              len = dat_element.bit_attribs.size;
            }
            else
              memcpy(msg_data, data, dat_element.bit_attribs.size);

            break;  // Pay attention to the position of this break statement!!!
    case X_NUMERIC:
          msg_data = (unsigned char*)malloc(dat_element.bit_attribs.size+3);

          if(msg_data == NULL)
            return ISO_MEM_ERROR;

          memset(msg_data, 0, dat_element.bit_attribs.size+3);
          msg_data[0] = data[0];
          memset(&msg_data[1], '0', dat_element.bit_attribs.size);
          memcpy(&msg_data[dat_element.bit_attribs.size-len+2], &data[1], len-1);

          // Move all the bytes to the left, to get rid of the 'X' portion and be left with just the data
          for(i=1; i<=len; i++)
            data[i-1] = data[i];

          len--;  // Decrement the length, to reflect the left shift
          dat_element.bit_attribs.size++; // Increment to account for the 'X' portion of the format
          break;
    case ALPHA_NUMERIC:
    case ALPHA_NUM_SPEC:
    case NUMERIC_SPECIAL:
//    case Z_TRACK:
    case NO_ISO_DATA:
          msg_data = (unsigned char*)malloc(len+3);
          if(msg_data == NULL) return ISO_MEM_ERROR;
          memset(msg_data, 0, len+2);
          memcpy(msg_data, data, len);
          break;

    case NUMERIC_SPECIAL_BCD:
		msg_data = (unsigned char*)malloc(len+3);
		msg_data_bcd = (unsigned char*)malloc(len+3);

		if(msg_data == NULL) return ISO_MEM_ERROR;
		if(msg_data_bcd == NULL) return ISO_MEM_ERROR;

		memset(msg_data, 0, len+2);
		memset(msg_data_bcd, 0, len+2);
		memset(bcd_data,0,sizeof(bcd_data));

		memcpy(msg_data_bcd, data, len);
		buff=string((char *)msg_data_bcd);
		len=UtilStringToBCD(buff, bcd_data);
		memcpy(msg_data, bcd_data, len);

		cout << "iq  Len : "<< len  << "   BCD DATa: "<< UtilsBcdToString(bcd_data,len)<< endl;
		bcd_len=len; //b/c 1 byte contain two values
		break;

    case Z_TRACK:
   		msg_data = (unsigned char*)malloc(len+3);
   		msg_data_bcd = (unsigned char*)malloc(len+3);

   		if(msg_data == NULL) return ISO_MEM_ERROR;
   		if(msg_data_bcd == NULL) return ISO_MEM_ERROR;

   		memset(msg_data, 0, len+2);
   		memset(msg_data_bcd, 0, len+2);
   		memset(bcd_data,0,sizeof(bcd_data));

   		memcpy(msg_data_bcd, data, len);
   		buff=string((char *)msg_data_bcd);

   		bcd_len=UtilStringToBCDForPan(buff, bcd_data);
   		memcpy(msg_data, bcd_data, bcd_len);

   		cout << "iq  Len : "<< bcd_len  << "   BCD DATa: "<< UtilsBcdToString(bcd_data,len)<< endl;
//   		bcd_len=0;
   		break;



  }
  // Note that the code below failed on Transit Product Load (crashed) - causes TaListAdd below to hang.
  // The error came because we added the field twice in the ProductLoad. However we comment below out anyway
  // because it looks suspect and is nit used at present.

//  // handle multiple additions to de48.16
//  if(el_type==ADDITIONAL && bit_field==Additional_data_private && add_bit_field==Structured_Data) {
//	int iRc;
//	unsigned char *pucData;
//	unsigned int uiDataLength;
//	iRc=IsoGetField(handle,bit_field,add_bit_field,&pucData,&uiDataLength);
//	if(iRc==0) {
//      IsoDelField(handle,bit_field,add_bit_field);
//      free(msg_data);
//      msg_data=(unsigned char*)malloc(uiDataLength-4+len+3);
//      if(msg_data==NULL) return(ISO_MEM_ERROR);
//      memset(msg_data,0,uiDataLength-4+len+3);
//      memcpy(msg_data,&pucData[4],uiDataLength-4);
//      memcpy(&msg_data[uiDataLength-4],data,len);
//      free(pucData);
//      len=len+uiDataLength-4;
//	}
//  }


  // Format the data
  switch(dat_element.bit_attribs.format)
  {
    case LLVAR:
    	if(dat_element.bit_attribs.attribute==Z_TRACK)
    	{
    		sprintf(length_char, "%02i", len);
    		length=string(length_char);
			UtilStringToBCD(length,length_str );
			msg_len = strlen(length_str)+bcd_len;
    	}
    	else
    	{
    		sprintf(length_char, "%02i", len+bcd_len);
    	 	length=string(length_char);
			UtilStringToBCD(length,length_str );
			msg_len = strlen(length_str)+len;
    	}
    	varLen=1;
        break;
    case LLLVAR:
        sprintf(length_char, "%04i", len+bcd_len);  //iq_audi_281116 03 change to 04
        length=string(length_char);
		UtilStringToBCD(length, length_str);
        msg_len = 2+len;
        varLen=2;
        break;
    case LLLLVAR:
        sprintf(length_char, "%04i", len+bcd_len);
        length=string(length_char);
		UtilStringToBCD(length,length_str );
        msg_len = strlen(length_str)+len;
        break;
    case LLLLLVAR:
        sprintf(length_char, "%05i", len+bcd_len);
        length=string(length_char);
		UtilStringToBCD(length,length_str );
        msg_len = strlen(length_str)+len;
        break;
    case LLLLLLVAR:
        sprintf(length_char, "%06i", len+bcd_len);
        length=string(length_char);
		UtilStringToBCD(length,length_str );
        msg_len = strlen(length_str)+len;
        break;
    case BMP:
    case FIXED_SIZE:
    case YYMMDD:
    case YYMMDDHHMMSS:
    case MMDD:
    case YYMM:
    case CCYYMMDDTHHMMSS:
    case MMDDHHMMSS:
    case HHMMSS:
    case CCYYMMDD:
    case FIXED_SIZE_ASCII:
    	if(buff.length()>0)
    		msg_len = len;
    	else
    		msg_len = dat_element.bit_attribs.size;

        break;

	break;
    case NO_DATA_SIZE:
        break;
  }

  dat_element.field_data = (unsigned char*)malloc(msg_len+3);

  if(dat_element.field_data == NULL)
  {
    if(dat_element.bit_attribs.format == BMP)// || dat_element.bit_attribs.format == ALPHA_NUM_SPEC_BINARY)
      free(msg_data);

    return ISO_MEM_ERROR;
  }

  memset(dat_element.field_data, 0, msg_len+2);

  // If there is a length indicator, then copy it first
//  if(strlen() > 0)
  if(dat_element.bit_attribs.format==LLVAR || dat_element.bit_attribs.format==LLLVAR)
  {
    memcpy(dat_element.field_data,length_str ,varLen);
  }

  memcpy(&dat_element.field_data[varLen], msg_data, msg_len);

  dat_element.field_data_len = msg_len;

  if(el_type == ELEMENT)
    dat_element.bit_no = (unsigned int)bit_field;
  else
    dat_element.bit_no = (unsigned int)add_bit_field;

  dat_element.element_type = el_type;

  // Add it to the list
  TaListAdd((LIST_HANDLE)handle, (unsigned char*)&dat_element, sizeof(DataElement));

  free(msg_data);

  //iq_audi_231116
  if(dat_element.bit_attribs.attribute==BCD_NUMERIC  ||
		  dat_element.bit_attribs.attribute==NUMERIC_SPECIAL_BCD)
  {
	  free(msg_data_bcd);
  	  msg_data_bcd=NULL;
  }
  return ISO_SUCCESS;
}

unsigned int IsoBuildMsg(ISO_HANDLE handle, unsigned char **msg)
{
  unsigned char additional_data_field_present=0, *add_data=NULL;
  unsigned int i, j, msg_len=7, count=0, pos=7, add_data_len=0;		//iq_audi_221116 pos change from 5 to 7, msg_len change 15 to 7 if use secondary bitmap at again at 8
  string buf;
  TaListValue *list_val;
  DataElement dat_element;
  BMPH msg_bmp;
  bool error=0;
  com_verifone_terminalconfig::CTerminalConfig termcfg;

// TODO: Do validation that the required fields have been populated

  msg_bmp = BmpCreate(64);  // Create a 64 bit bitmap  iq_audi_231116 change 128 to 64

  BmpClearBitMask(msg_bmp, &error);

  // First the determine the length of the final message
  count = TaListGetCount((LIST_HANDLE)handle);

  for(j=1; j<=63; j++)  // iq_audi_231116 change 127 to 63
  {
    for(i=0; i<count; i++)
    {
      list_val = TaListGet((LIST_HANDLE)handle, i+1);

      memset(&dat_element, 0, sizeof(DataElement));  
      memcpy(&dat_element, list_val->data, sizeof(DataElement));

      if(dat_element.element_type == ELEMENT)
      {
        if(dat_element.bit_no == j)
        {
          BmpSetBit(msg_bmp, (unsigned long)dat_element.bit_no-1, &error, BRIGHT);

          msg_len += dat_element.field_data_len;
        }
      }
      else
      {
        BmpSetBit(msg_bmp, (unsigned long)48-1, &error, BRIGHT);
        additional_data_field_present = 1;
      }
    }
  }

  if(additional_data_field_present)
    add_data_len = get_additional_data_length(handle);

  msg_len += add_data_len;

  *msg = (unsigned char*)malloc(msg_len+17);

  if(*msg == NULL)
  {
    if(add_data != NULL)
      free(add_data);

    BmpDestroy(&msg_bmp);

    return 0;
  }

//  iq_audi_281116 header of message
  string str_tpdu = termcfg.getTpdu();

  switch(ta_lists[handle].mti)
  {
    case MTI_1100:
    	  str_tpdu.append("0100",4);
		  buf = Utils::UtilsStringToHex(str_tpdu);
          memcpy((char*)*msg,buf.c_str(),7);
          break;
    case MTI_1200:
    	  str_tpdu.append("0200",4);
		  buf = Utils::UtilsStringToHex(str_tpdu);
		  memcpy((char*)*msg,buf.c_str(),7);
          break;
    case MTI_1220:
    	  str_tpdu.append("0220",4);
		  buf = Utils::UtilsStringToHex(str_tpdu);
		  memcpy((char*)*msg,buf.c_str(),7);
          break;
//          iq_audi_030117 start
    case MTI_0320:
		  str_tpdu.append("0320",4);
		  buf = Utils::UtilsStringToHex(str_tpdu);
		  memcpy((char*)*msg,buf.c_str(),7);
		  break;
//        iq_audi_030117 end
    case MTI_1304:
          sprintf((char*)*msg, "B1304");
          break;
    case MTI_1314:
          sprintf((char*)*msg, "B1314");
          break;
    case MTI_1420:
		  str_tpdu.append("0400",4);
		  buf = Utils::UtilsStringToHex(str_tpdu);
		  memcpy((char*)*msg,buf.c_str(),7);
          break;
    case MTI_1520:
    	  str_tpdu.append("0500",4);
    	  buf = Utils::UtilsStringToHex(str_tpdu);
		  memcpy((char*)*msg,buf.c_str(),7);
          break;
    case MTI_1604:
          sprintf((char*)*msg, "B1604");
          break;
    case MTI_1624:
          sprintf((char*)*msg, "B1624");
          break;
  }

  ISO8583Dbg("IsoBuildMsg(): Building MTI %s\n", *msg+1);
  cout << "IsoBuildMsg(): Building MTI " << UtilsBcdToString((const char *)*msg,7)<< endl;

//  BmpSetBit(msg_bmp, 0, &error, BRIGHT);		//iq_audi_251116 there is no need of secondary bitmap

  BmpExport(msg_bmp, *msg+pos, 8); // iq_audi_231116 change 16 to 8
  BmpDestroy(&msg_bmp);

  pos += 8;   // iq_audi_231116 change 16 to 8

  for(j=1; j<=63; j++) //  iq_audi_231116 change 127 to 63
  {
    for(i=0; i<count; i++)
    {
      list_val = TaListGet((LIST_HANDLE)handle, i+1);

      memset(&dat_element, 0, sizeof(DataElement));  
      memcpy(&dat_element, list_val->data, sizeof(DataElement));

      if(dat_element.element_type == ELEMENT)
      {
        if(j == Additional_data_private)
        {
          build_additional_data_field(handle, &add_data);
          memcpy(*msg+pos, add_data, add_data_len);
          pos += add_data_len;

          if(add_data != NULL)
          {
            free(add_data);
            add_data = NULL;
          }
          break;
        }
        else if(dat_element.bit_no == j)
        {
          char buffer[5];
          sprintf(buffer, "%03i", dat_element.field_data_len);

          if(dat_element.bit_attribs.attribute == BINARY || dat_element.bit_attribs.attribute == ALPHA_NUM_SPEC_BINARY)
          {
            ISO8583Dbg("%l %s (%s):[%h]\n", (long)dat_element.bit_no, TaIsoGetDescr((BIT_FIELD)dat_element.bit_no), buffer, dat_element.field_data, dat_element.field_data_len);
            cout <<dat_element.bit_no<<" "<<TaIsoGetDescr((BIT_FIELD)dat_element.bit_no)<<" ("<<dat_element.field_data_len<<"): ["<<UtilsHexToString((const char*)dat_element.field_data, dat_element.field_data_len)<<"]"<<endl;
          }
          else if(dat_element.bit_attribs.attribute == BCD_NUMERIC || dat_element.bit_attribs.attribute == NUMERIC_SPECIAL_BCD ||
        		  dat_element.bit_attribs.attribute == Z_TRACK || dat_element.bit_attribs.attribute == ALPHA_NUM_SPEC) // iq_audi_231116
		   {
//			 ISO8583Dbg("%l %s (%s):[%s]\n", (long)dat_element.bit_no, TaIsoGetDescr((BIT_FIELD)dat_element.bit_no), buffer, dat_element.field_data);
        	 cout <<dat_element.bit_no<<" "<<TaIsoGetDescr((BIT_FIELD)dat_element.bit_no)<<" ("<<dat_element.field_data_len<<"): ["<<UtilsBcdToString((const char *)dat_element.field_data, dat_element.field_data_len )<<"]"<<endl;
		   }
          else
          {
            ISO8583Dbg("%l %s (%s):[%s]\n", (long)dat_element.bit_no, TaIsoGetDescr((BIT_FIELD)dat_element.bit_no), buffer, dat_element.field_data);
            cout <<dat_element.bit_no<<" "<<TaIsoGetDescr((BIT_FIELD)dat_element.bit_no)<<" ("<<dat_element.field_data_len<<"): ["<<dat_element.field_data<<"]"<<endl;
          }

          memcpy(*msg+pos, dat_element.field_data, dat_element.field_data_len);
          pos += dat_element.field_data_len;
        }
      }
    }
  }

  if(add_data != NULL)
    free(add_data);

  //ISO8583Dbg("IsoBuildMsg(): ISO message length=%l\n", (long)pos);

//  return msg_len;
return pos;
}

int IsoGetField(ISO_HANDLE handle, BIT_FIELD bit_field, BIT_FIELDS_ADD_DATA add_bit_field, unsigned char **data, unsigned int *len)
{
  unsigned int i, item_count=0;
  TaListValue *item;
  DataElement dat_element;

  *data = NULL;
  *len = 0;

  // Get a count of the number of fields in the message
  item_count = TaListGetCount(handle);

  if(item_count == 0)
    return -1;

	for (i = 1; i <= item_count; i++) {
    item = TaListGet(handle, i);

		if (item != NULL) {
			if (item->size == sizeof(DataElement)) {
        memcpy(&dat_element, item->data, item->size);

				if (dat_element.element_type == ADDITIONAL && dat_element.bit_no == add_bit_field) {
          *data = (unsigned char*)malloc(dat_element.field_data_len);

					if (*data != NULL) {
            memcpy(*data, dat_element.field_data, dat_element.field_data_len);
            *len = dat_element.field_data_len;
						return 0;
					} else {
						if (dat_element.bit_no == (unsigned int) bit_field) {
							*data = (unsigned char*)malloc(dat_element.field_data_len);

							if (*data != NULL) {
								memcpy(*data, dat_element.field_data, dat_element.field_data_len);
								*len = dat_element.field_data_len;
            return 0;
          }
        }
      }
				} else {
					memcpy(&dat_element, item->data, item->size);

					if (dat_element.bit_no == (unsigned int) bit_field) {
						*data = (unsigned char*)malloc(dat_element.field_data_len);

						if (*data != NULL) {
							memcpy(*data, dat_element.field_data, dat_element.field_data_len);
							*len = dat_element.field_data_len;

							return 0;
    }

  }
				}
			}
		}
	}
  return -1;
}

///
/// Local functions
///

unsigned int get_additional_data_length(ISO_HANDLE handle)
{
  unsigned int i, j, msg_len=9, count=0;
  TaListValue *list_val;
  DataElement dat_element;

  // First the determine the length of the final message
  count = TaListGetCount((LIST_HANDLE)handle);

  for(j=1; j<=16; j++)
  {
    for(i=0; i<count; i++)
    {
      list_val = TaListGet((LIST_HANDLE)handle, i+1);

      memset(&dat_element, 0, sizeof(DataElement));  
      memcpy(&dat_element, list_val->data, sizeof(DataElement));

      if((dat_element.element_type == ADDITIONAL) && (dat_element.bit_no == j))
        msg_len += dat_element.field_data_len;
    }
  }

  return msg_len;
}

unsigned int build_additional_data_field(ISO_HANDLE handle, unsigned char **msg)
{
  unsigned int i, j, msg_len=9, count=0, pos=7;
  TaListValue *list_val;
  DataElement dat_element;
  BMPH msg_bmp;
  bool error=0;

  msg_bmp = BmpCreate(16);  // Create a 64 bit bitmap

  BmpClearBitMask(msg_bmp, &error);

  // First the determine the length of the final message
  count = TaListGetCount((LIST_HANDLE)handle);

  for(j=1; j<=16; j++)
  {
    for(i=0; i<count; i++)
    {
      list_val = TaListGet((LIST_HANDLE)handle, i+1);

      memset(&dat_element, 0, sizeof(DataElement));  
      memcpy(&dat_element, list_val->data, sizeof(DataElement));

      if((dat_element.element_type == ADDITIONAL) && (dat_element.bit_no == j))
      {
        BmpSetBit(msg_bmp, (unsigned long)dat_element.bit_no-1, &error, BRIGHT);

        msg_len += dat_element.field_data_len;
      }
    }
  }

  *msg = (unsigned char*)malloc(msg_len+1);

  if(*msg == NULL)
  {
    BmpDestroy(&msg_bmp);
    return 0;
  }

//  sprintf((char*)*msg, "%04i\xf0%02i", msg_len-4, msg_len-9);
  sprintf((char*)*msg, "%04i\xf0%02i",msg_len-4,msg_len-9);
//  (*msg)[5]=(unsigned char )((msg_len-7)/256);
//  (*msg)[6]=(unsigned char )((msg_len-7)%256);
  ISO8583Dbg("48  [%h]\n",*msg,7);
cout <<"48 ["<<UtilsHexToString((const char*)*msg, 7)<<"]"<<endl;
  BmpExport(msg_bmp, *msg+pos, 2);
  pos += 2;

  for(j=1; j<=16; j++)
  {
    for(i=0; i<count; i++)
    {
      list_val = TaListGet((LIST_HANDLE)handle, i+1);

      memset(&dat_element, 0, sizeof(DataElement));  
      memcpy(&dat_element, list_val->data, sizeof(DataElement));

      if((dat_element.element_type == ADDITIONAL) && (dat_element.bit_no == j))
      {
        char buffer[5];
        sprintf(buffer, "%03i", dat_element.field_data_len);
        ISO8583Dbg("48.%l %s (%s):[%s]\n", (long)dat_element.bit_no, TaIsoGetAddDataDescr((BIT_FIELDS_ADD_DATA)dat_element.bit_no), buffer, dat_element.field_data);
cout <<"48."<<dat_element.bit_no<<" "<<TaIsoGetDescr((BIT_FIELD)dat_element.bit_no)<<" ("<<dat_element.field_data_len<<"): ["<<UtilsHexToString((const char*)dat_element.field_data, dat_element.field_data_len)<<"]"<<endl;
        memcpy(*msg+pos, dat_element.field_data, dat_element.field_data_len);
        pos += dat_element.field_data_len;
      }
    }
  }

  BmpDestroy(&msg_bmp);
  
  ISO8583Dbg("48  [%h]\n",*msg,msg_len);

  return msg_len;
}

ISO_HANDLE TaIsoDecode(unsigned char *msg, unsigned int msg_len)
{
	(void)msg_len;				// Remove compiler warnings of unused parameter
  char bcd_mti[2+1];//, fmt_type=0;
  string  mti;
  unsigned int pos=1+4, bmp_len=0, i; //iq_audi_281116 +4 for source destination address
  bool error=0;
  BMPH bh;
  ISO_HANDLE iso_handle;
  MTI resp_mti=MTI_1314;

  if(msg_len <= 25)
	  return ISO_INVALID_HANDLE;	// 21 is the minimum length of an ISO msg

  if(/*(msg[0] != 'A') && (msg[0] != 'B')&& */(msg[0] != 0x60) ) //iq_audi_281116
  {
    ISO8583Dbg("Unknown message data type\r\n");
    return ISO_INVALID_HANDLE;
  }

//  memset(mti, 0, sizeof(mti));
  memset(bcd_mti, 0, sizeof(bcd_mti));
  memcpy(bcd_mti, &msg[pos], 2);  //iq_audi_281116 change 4 to 2
  pos += 2;

  mti=UtilsBcdToString(bcd_mti, 2);


  ISO8583Dbg("TaIsoDecode(): Decode %s\n",mti.c_str());

  if(memcmp(mti.c_str(), "1314", 4) == 0)
    resp_mti = MTI_1314;

  iso_handle = IsoCreateList(resp_mti);

  if(iso_handle == ISO_INVALID_HANDLE)
	  return ISO_INVALID_HANDLE;

  //ISO8583Dbg("Message number: %s\r\n", mti);
  cout<<"TaIsoDecode(): Decode "<<mti.c_str()<<endl;

  //
  // Extract the message bitmap
  //
  
  // Check if first bit is set, indicating that the bitmap is 16 bytes
  if(msg[pos] & 0x80)
  {
    bh = BmpCreate(128);

    BmpImport(bh, &msg[pos], 128);

    pos += 16;
    bmp_len = 128;
  }
  else
  {
    bh = BmpCreate(64);

    BmpImport(bh, &msg[pos], 64);
    pos += 8;

    bmp_len = 64;
  }

  for(i=2; i<bmp_len; i++)
  {
    if(BmpIsBitSet(bh, (unsigned long)(i-1), &error, BRIGHT))
    {
      if(i == Additional_data_private)
      {
        // Decode the additional data field 48
        char buffer[5];
        unsigned int len=0, ret=0;

        memset(buffer, 0, sizeof(buffer));
        memcpy(buffer, &msg[pos], 4);
        len = atoi(buffer);
        pos += 4;

        if((ret = DecodeAdditionalData(iso_handle, &msg[pos], len)) == 0)
        {
          //ISO8583Dbg("!!!Unable to decode field 48!!!\r\n");

          BmpDestroy(&bh);

          return -2;
        }

        pos += ret;
      }
      else
        pos += PrintFormattedData(iso_handle, ELEMENT, i, msg, pos);
    }
  }

  BmpDestroy(&bh);

  ISO8583Dbg("\n");
  
  return iso_handle;
}

unsigned int DecodeAdditionalData(ISO_HANDLE handle, unsigned char *msg, unsigned int length)
{
	(void)length;				// Remove compiler warnings of unused parameter

  char buffer[3];
  unsigned int pos=0, i;
  bool error=0;
  BMPH bh;

  if(msg[pos] != 0xf0) // Check for Postillion's data set 
    return 0;

  pos++;

  memcpy(buffer, &msg[pos], 2);
  buffer[2] = 0;

  pos += 2;

  bh = BmpCreate(16);

  BmpImport(bh, &msg[pos], 16);
  pos += 2;

  for(i=1; i<=16; i++)
  {
    if(BmpIsBitSet(bh, (unsigned long)i-1, &error, BRIGHT))
    {
      ISO8583Dbg("F0.%l %s: ", (long)i, TaIsoGetAddDataDescr((BIT_FIELDS_ADD_DATA)(i)));
cout <<"F0."<<i<<" "<<TaIsoGetDescr((BIT_FIELD)i)<<": "<<endl;
      pos += PrintFormattedData(handle, ADDITIONAL, i, msg, pos);
    }
  }

  BmpDestroy(&bh);

  return pos;
}

unsigned int PrintFormattedData(ISO_HANDLE handle, ELEMENT_TYPE el_type, unsigned int bitno, unsigned char *data, unsigned int data_offset)
{
  char len_bytes[8], *ascii_buff=NULL, buffer[10];
  BITS_ATTR attributes;
  unsigned int len=0, data_len=0;
  DataElement dat_element;
  string str_len;

  memset(&dat_element, 0, sizeof(DataElement));
  memset(buffer, 0, sizeof(buffer));

  if(el_type == ELEMENT)
  {
     TaIsoGetDescr((BIT_FIELD)bitno);
     memcpy(&attributes, &bits_attr[bitno-1], sizeof(BITS_ATTR));
  }
  else
  {
    sprintf(buffer, "48.");
    TaIsoGetAddDataDescr((BIT_FIELDS_ADD_DATA)bitno);
    memcpy(&attributes, &bits_attr_add_data[bitno-1], sizeof(BITS_ATTR));
  }

  len = GetDataLen(&attributes, data, data_offset);

  if(len)
  { // Field has dynamic length
    memset(len_bytes, 0, sizeof(len_bytes));
    memcpy(len_bytes, &data[data_offset], len);

    str_len=UtilsBcdToString(len_bytes,len); //iq_audi_281116

    data_offset += len;
//    data_len = atol(len_bytes);
    data_len = UtilsStringToInt(str_len);
  }
  else
  {		// Field has fixed length
//	  iq_audi_281116 start
	  if(attributes.size%2 && attributes.format!=FIXED_SIZE_ASCII)
		  data_len = (attributes.size+1);
	  else
		  data_len = attributes.size;

	  if(attributes.format!=FIXED_SIZE_ASCII)
		  data_len/=2;
//	  iq_audi_281116 end

//	  data_len = attributes.size;
  }
  switch(attributes.attribute)
  {
    case BINARY:
    case ALPHA_NUM_SPEC_BINARY:
    case BCD_NUMERIC: //iq_audi_191116 will be handle in future
    case NUMERIC_SPECIAL_BCD:
		  ascii_buff = (char*)malloc((unsigned int)(data_len+1));

		  memset(ascii_buff, 0, (unsigned int)(data_len+1));
		  memcpy(ascii_buff, &data[data_offset], data_len);

		  sprintf(len_bytes, "%03d", data_len);
		  ISO8583Dbg("%s%l %s (%s):*[%h]\n", buffer, (long)bitno, TaIsoGetDescr((BIT_FIELD)bitno),  len_bytes, (unsigned char*)ascii_buff, data_len);
cout <<bitno<<" "<<TaIsoGetDescr((BIT_FIELD)bitno)<<"("<<len_bytes<<"): ["<<UtilsHexToString((const char*)ascii_buff, data_len)<<"]"<<endl;
                  break;
    case X_NUMERIC:
                  data_len++; // Add one for the 'x' in x+n..
//    case Z_TRACK:
    case NUMERIC:
    case ALPHA_NUMERIC:
    case ALPHA_NUM_SPEC:
    //case ALPHA_NUM_SPEC_BINARY:
    case NUMERIC_SPECIAL:
    case NO_ISO_DATA:
		  ascii_buff = (char*)malloc((unsigned int)(data_len+1));

		  memset(ascii_buff, 0, (unsigned int)(data_len+1));
		  memcpy(ascii_buff, &data[data_offset], data_len);

		  sprintf(len_bytes, "%03d", data_len);
		  ISO8583Dbg("%s%l %s (%s):[%s]\n", buffer, (long)bitno ,TaIsoGetDescr((BIT_FIELD)bitno), len_bytes, ascii_buff);
		  cout <<bitno<<" "<<TaIsoGetDescr((BIT_FIELD)bitno)<<"("<<len_bytes<<"): ["<<ascii_buff<<"]"<<endl;
		  break;

    case Z_TRACK:

		if(data_len%2)
			data_len+=1;

		data_len/=2;


		  ascii_buff = (char*)malloc((unsigned int)(data_len+1));

		  memset(ascii_buff, 0, (unsigned int)(data_len+1));
		  memcpy(ascii_buff, &data[data_offset], data_len);

		  sprintf(len_bytes, "%02d", data_len);
		  ISO8583Dbg("%s%l %s (%s):[%s]\n", buffer, (long)bitno ,TaIsoGetDescr((BIT_FIELD)bitno), len_bytes, ascii_buff);
cout <<bitno<<" "<<TaIsoGetDescr((BIT_FIELD)bitno)<<"("<<len_bytes<<"): ["<<UtilsBcdToString(ascii_buff,data_len).c_str()<<"]"<<endl;
                  break;
  }

  memcpy(&dat_element.bit_attribs, &attributes, sizeof(BITS_ATTR));
  dat_element.bit_no = bitno;
  dat_element.element_type = el_type;
  dat_element.field_data_len = data_len;
  dat_element.field_data = (unsigned char*)ascii_buff;

  TaListAdd((LIST_HANDLE)handle, (unsigned char*)&dat_element, sizeof(DataElement));

  return data_len+len;
}

unsigned int GetDataLen(BITS_ATTR *attr, unsigned char *data, unsigned int data_offset)
{
	(void)data;				// Remove compiler warnings of unused parameter
	(void)data_offset;		// Remove compiler warnings of unused parameter

  switch(attr->format)
  {
  	case LLVAR:
        return 1;  //iq_audi_281116 change 2 to 1
    case LLLVAR:
        return 2;	 //iq_audi_281116 change 3 to 2
    case LLLLVAR:
        return 4;
    case LLLLLVAR:
        return 5;
    case LLLLLLVAR:
        return 6;
    default:
    	return 0;
  }

  return 0;
}

static char *TaIsoGetDescr(BIT_FIELD bit_field)
{
  switch(bit_field)
  {
    case Extended_Bitmap                        : return (char*)"   Message Bitmap                        ";// 1
    case Primary_account_number                 : return (char*)"   Primary account number                ";// 2
    case Processing_code                        : return (char*)"   Processing code                       ";// 3
    case Amount_transaction                     : return (char*)"   Amount transaction                    ";// 4
    case Amount_reconciliation                  : return (char*)"   Amount reconciliation                 ";// 5
    case Reserved_for_future_use1               : return (char*)"   Reserved for future use               ";// 6
    case Date_and_time_transmission             : return (char*)"   Date and time transmission            ";// 7
    case Reserved_for_future_use2               : return (char*)"   Reserved for future use               ";// 8
    case Conversion_rate_reconciliation         : return (char*)"   Conversion rate reconciliation        ";// 9
    case Reserved_for_future_use3               : return (char*)"  Reserved for future use                ";// 10
    case System_trace_audit_number              : return (char*)"  System trace audit number             ";// 11
    case Date_and_time_local_transaction        : return (char*)"  Date and time local transaction       ";// 12
    case Date_local_transaction               	: return (char*)"  Date local transaction                ";// 13
    case Date_expiration                        : return (char*)"  Date expiration                       ";// 14
    case Reserved_for_future_use5               : return (char*)"  Reserved for future use               ";// 15
    case Date_conversion                        : return (char*)"  Date conversion                       ";// 16
    case Reserved_for_future_use6               : return (char*)"  Reserved for future use               ";// 17
    case Reserved_for_future_use7               : return (char*)"  Reserved for future use               ";// 18
    case Reserved_for_future_use8               : return (char*)"  Reserved for future use               ";// 19
    case Reserved_for_future_use9               : return (char*)"  Reserved for future use               ";// 20
    case Reserved_for_future_use10              : return (char*)"  Reserved for future use               ";// 21
    case POS_data_code                          : return (char*)"  POS data code                         ";// 22
    case Card_sequence_number                   : return (char*)"  Card sequence number                  ";// 23
    case Function_code                          : return (char*)"  Function code                         ";// 24
    case Message_reason_code                    : return (char*)"  Message reason code                   ";// 25
    case Reserved_for_future_use11              : return (char*)"  Reserved for future use               ";// 26
    case Approval_code_length                   : return (char*)"  Approval code length                  ";// 27
    case Date_reconciliation                    : return (char*)"  Date reconciliation                   ";// 28
    case Reconciliation_indicator               : return (char*)"  Reconciliation indicator              ";// 29
    case Amounts_original                       : return (char*)"  Amounts original                      ";// 30
    case Reserved_for_future_use12              : return (char*)"  Reserved for future use               ";// 31
    case Acquiring_institution_ID_code          : return (char*)"  Acquiring institution ID code         ";// 32
    case Reserved_for_future_use13              : return (char*)"  Reserved for future use               ";// 33
    case Reserved_for_future_use14              : return (char*)"  Reserved for future use               ";// 34
    case Track_2_data                           : return (char*)"  Track 2 data                          ";// 35
    case Reserved_for_future_use15              : return (char*)"  Reserved for future use               ";// 36
    case Retrieval_reference_number             : return (char*)"  Retrieval reference number            ";// 37
    case Approval_code                          : return (char*)"  Approval code                         ";// 38
    case Action_code                            : return (char*)"  Action code                           ";// 39
    case Service_code                           : return (char*)"  Service code                          ";// 40
    case Terminal_ID                            : return (char*)"  Terminal ID                           ";// 41
    case Card_acceptor_ID                       : return (char*)"  Card acceptor ID                      ";// 42
    case Reserved_for_future_use16              : return (char*)"  Reserved for future use               ";// 43
    case Additional_response_data               : return (char*)"  Additional response data              ";// 44
    case Track_1_data                           : return (char*)"  Track 1 data                          ";// 45
    case Amounts_fees                           : return (char*)"  Amounts fees                          ";// 46
    case Reserved_for_future_use17              : return (char*)"  Reserved for future use               ";// 47
    case Additional_data_private                : return (char*)"  Additional data private               ";// 48
    case Currency_code_transaction              : return (char*)"  Currency code transaction             ";// 49
    case Currency_code_reconciliation           : return (char*)"  Currency code reconciliation          ";// 50
    case Reserved_for_future_use18              : return (char*)"  Reserved for future use an 3          ";// 51
    case PIN_data                               : return (char*)"  PIN data                              ";// 52
    case Security_related_control_information   : return (char*)"  Security related control information  ";// 53
    case Additional_amounts                     : return (char*)"  Additional amounts                    ";// 54
    case ICC_data                               : return (char*)"  ICC data                              ";// 55
    case Original_data_elements                 : return (char*)"  Original data elements                ";// 56
    case Authorisation_life_cycle_code          : return (char*)"  Authorisation life cycle code         ";// 57
    case Authorising_agent_institution_ID_code  : return (char*)"  Authorising agent institution ID code ";// 58
    case Transport_echo_data                    : return (char*)"  Transport echo data                   ";// 59
    case F_60              						: return (char*)"  Field 60					             ";// 60
    case Reserved_for_future_use20              : return (char*)"  Reserved for future use               ";// 61
    case Hotcard_capacity                       : return (char*)"  Hotcard capacity                      ";// 62
    case F_63						            : return (char*)"  Field 63 				             ";// 63
    case Reserved_for_future_use22              : return (char*)"  Reserved for future use               ";// 64
    case Reserved_for_future_use23              : return (char*)"  Reserved for future use               ";// 65
    case Amounts_original_fees                  : return (char*)"  Amounts original fees                 ";// 66
    case Extended_payment_data                  : return (char*)"  Extended payment data                 ";// 67
    case Reserved_for_future_use24              : return (char*)"  Reserved for future use               ";// 68
    case Reserved_for_future_use25              : return (char*)"  Reserved for future use               ";// 69
    case Reserved_for_future_use26              : return (char*)"  Reserved for future use               ";// 60
    case Message_number                         : return (char*)"  Message number                        ";// 71
    case Data_record                            : return (char*)"  Data record                           ";// 72
    case Reserved_for_future_use27              : return (char*)"  Reserved for future use               ";// 73
    case Credits_number                         : return (char*)"  Credits number                        ";// 74
    case Credits_reversal_number                : return (char*)"  Credits reversal number               ";// 75
    case Debits_number                          : return (char*)"  Debits number                         ";// 76
    case Debits_reversal_number                 : return (char*)"  Debits reversal number                ";// 77
    case Reserved_for_future_use28              : return (char*)"  Reserved for future use               ";// 78
    case Reserved_for_future_use29              : return (char*)"  Reserved for future use               ";// 79
    case Reserved_for_future_use30              : return (char*)"  Reserved for future use               ";// 70
    case Authorisations_number                  : return (char*)"  Authorisations number                 ";// 81
    case Reserved_for_future_use31              : return (char*)"  Reserved for future use               ";// 82
    case Reserved_for_future_use32              : return (char*)"  Reserved for future use               ";// 83
    case Reserved_for_future_use33              : return (char*)"  Reserved for future use               ";// 84
    case Reserved_for_future_use34              : return (char*)"  Reserved for future use               ";// 85
    case Credits_amount                         : return (char*)"  Credits amount                        ";// 86
    case Credits_reversal_amount                : return (char*)"  Credits reversal amount               ";// 87
    case Debits_amount                          : return (char*)"  Debits amount                         ";// 88
    case Debits_reversal_amount                 : return (char*)"  Debits reversal amount                ";// 89
    case Authorisations_reversal_number         : return (char*)"  Authorisations reversal number        ";// 90
    case Reserved_for_future_use35              : return (char*)"  Reserved for future use               ";// 91
    case Reserved_for_future_use36              : return (char*)"  Reserved for future use               ";// 92
    case Reserved_for_future_use37              : return (char*)"  Reserved for future use               ";// 93
    case Reserved_for_future_use38              : return (char*)"  Reserved for future use               ";// 94
    case Reserved_for_future_use39              : return (char*)"  Reserved for future use               ";// 95
    case Reserved_for_future_use40              : return (char*)"  Reserved for future use               ";// 96
    case Amount_net_reconciliation              : return (char*)"  Amount net reconciliation             ";// 97
    case Reserved_for_future_use41              : return (char*)"  Reserved for future use               ";// 98
    case Reserved_for_future_use42              : return (char*)"  Reserved for future use               ";// 99
    case Receiving_institution_ID_code          : return (char*)"  Receiving institution ID code        ";// 100
    case File_name                              : return (char*)"  File name                            ";// 101
    case Account_identification_1               : return (char*)"  Account identification 1             ";// 102
    case Account_identification_2               : return (char*)"  Account identification 2             ";// 103
    case Transaction_description                : return (char*)"  Transaction description              ";// 104
    case Reserved_for_future_use43              : return (char*)"  Reserved for future use              ";// 105
    case Reserved_for_future_use44              : return (char*)"  Reserved for future use              ";// 106
    case Reserved_for_future_use45              : return (char*)"  Reserved for future use              ";// 107
    case Reserved_for_future_use46              : return (char*)"  Reserved for future use              ";// 108
    case Credits_fee_amounts                    : return (char*)"  Credits fee amounts                  ";// 109
    case Debits_fee_amounts                     : return (char*)"  Debits fee amounts                   ";// 110
    case Reserved_for_future_use47              : return (char*)"  Reserved for future use              ";// 111
    case Reserved_for_future_use48              : return (char*)"  Reserved for future use              ";// 112
    case Reserved_for_future_use49              : return (char*)"  Reserved for future use              ";// 113
    case Reserved_for_future_use50              : return (char*)"  Reserved for future use              ";// 114
    case Reserved_for_future_use51              : return (char*)"  Reserved for future use              ";// 115
    case Reserved_for_future_use52              : return (char*)"  Reserved for future use              ";// 116
    case Reserved_for_future_use53              : return (char*)"  Reserved for future use              ";// 117
    case Reserved_for_future_use54              : return (char*)"  Reserved for future use              ";// 118
    case Reserved_for_future_use55              : return (char*)"  Reserved for future use              ";// 119
    case Reserved_for_future_use56              : return (char*)"  Reserved for future use              ";// 120
    case Reserved_for_future_use57              : return (char*)"  Reserved for future use              ";// 121
    case Reserved_for_future_use58              : return (char*)"  Reserved for future use              ";// 122
    case Reserved_for_future_use59              : return (char*)"  Reserved for future use              ";// 123
    case Reserved_for_future_use60              : return (char*)"  Reserved for future use              ";// 124
    case Reserved_for_future_use61              : return (char*)"  Reserved for future use              ";// 125
    case Reserved_for_future_use62              : return (char*)"  Reserved for future use              ";// 126
    case Reserved_for_future_use63              : return (char*)"  Reserved for future use              ";// 127
  }  
  
  return NULL;  
}  

static char *TaIsoGetAddDataDescr(BIT_FIELDS_ADD_DATA bit_field)
{
  switch(bit_field)
  { 
    case AddFieldNotApplicable                  : return (char*)"Not Applicable                        ";
    case POS_data                               : return (char*)"POS data                              "; // 48-F0.1
    case Authorisation_profile                  : return (char*)"Authorisation profile                 "; // 48-F0.2
    case Card_verification_data                 : return (char*)"Card verification data                "; // 48-F0.3
    case Extended_transaction_type              : return (char*)"Extended transaction type             "; // 48-F0.4
    case Additional_node_data                   : return (char*)"Additional node data                  "; // 48-F0.5
    case Inquiry_response_data                  : return (char*)"Inquiry response data                 "; // 48-F0.6
    case Routing_information                    : return (char*)"Routing information                   "; // 48-F0.7
    case Cardholder_information                 : return (char*)"Cardholder information                "; // 48-F0.8
    case Address_verification_data              : return (char*)"Address verification data             "; // 48-F0.9
    case Card_verification_result               : return (char*)"Card verification result              "; // 48-F0.10
    case Address_verification_result            : return (char*)"Address verification result           "; // 48-F0.11
    case Retention_data                         : return (char*)"Retention data                        "; // 48-F0.12
    case Bank_Details                           : return (char*)"Bank Details                          "; // 48-F0.13
    case Payee_name_and_address                 : return (char*)"Payee name and address                "; // 48-F0.14
    case Payer_account_identification           : return (char*)"Payer account identification          "; // 48-F0.15
    case Structured_Data                        : return (char*)"Structured Data                       "; // 48-F0.16
  }

  return NULL;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                         Linked list code                                                   //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int ta_check_duplicate(LIST_HANDLE handle, unsigned char *data, unsigned int size);

int TaListInit(void)
{
  int i;

  for(i=0; i<MAX_LISTS; i++)
    memset(&ta_lists[i], 0, sizeof(TaList));

  return 0;
}

LIST_HANDLE TaListCreate(MTI mti)
{
  int i;

  for(i=0; i<MAX_LISTS; i++)
  {
    if(ta_lists[i].occupied == 0)
    {
      ta_lists[i].occupied = 1;
      ta_lists[i].mti = mti;
      return (LIST_HANDLE)i;
    }
  }

  return INVALID_HANDLE;
}

int TaListAdd(LIST_HANDLE handle, unsigned char *data, unsigned int size)
{
  unsigned int uiret;
  TaListValue *list_value=NULL;
  
  if((handle < 0) || (ta_lists[handle].occupied == 0) || (data == NULL) || (size == 0))
    return -1;

  uiret = ta_check_duplicate(handle, data, size);

  if(uiret > 0)
  {
//    printf("ListAdd(): Duplicate in list %i at no %i\n", handle, uiret);
    return uiret;
  }
  
  list_value = (TaListValue*)malloc(sizeof(TaListValue));

  if(list_value == NULL)
    return -3;

  list_value->data = (unsigned char*)malloc(size);

  if(list_value->data == NULL)
  {
    free(list_value);
    return -4;
  }

  memcpy(list_value->data, data, size);
  list_value->size = size;
  list_value->mark = 0;
  if(ta_lists[handle].first == NULL)
  {
    ta_lists[handle].item_count = 1;
    list_value->no = 1;

    list_value->next = NULL;
    list_value->prev = NULL;

    ta_lists[handle].first = list_value;
    ta_lists[handle].last = list_value;
  }
  else
  {
    ta_lists[handle].item_count++;
    list_value->no = ta_lists[handle].item_count;

    list_value->next = NULL;
    list_value->prev = ta_lists[handle].last;

    ta_lists[handle].last->next = list_value;
    ta_lists[handle].last = list_value;
  }

  return 0;
}

TaListValue *TaListGet(LIST_HANDLE handle, unsigned int no)
{
  TaListValue *list_value=NULL;

  if((handle < 0) || (ta_lists[handle].occupied == 0) || (no == 0))
    return NULL;

  list_value = ta_lists[handle].first;

  while(list_value != NULL)
  {
    if(list_value->no == no)
      return list_value;

    list_value = list_value->next;
  }

  return NULL;
}

TaListValue *TaListGetByCount(LIST_HANDLE handle, unsigned int count)
{
  unsigned int i;
  TaListValue *list_value=NULL;

  if((handle < 0) || (ta_lists[handle].occupied == 0) || (count == 0))
    return NULL;

  list_value = ta_lists[handle].first;

  for(i=1; i<=count; i++)
  {
    if((i==count) || (list_value == NULL))
      break;
      
    list_value = list_value->next;
  }

  return list_value;
}

unsigned int TaListGetCount(LIST_HANDLE handle)
{
  if((handle < 0) || (ta_lists[handle].occupied == 0))
    return 0;

  return ta_lists[handle].item_count;
}

int TaListDestroy(LIST_HANDLE *handle)
{
  TaListValue *list_value=NULL;

  if((*handle < 0) || (ta_lists[*handle].occupied == 0))
    return -1;

  list_value = ta_lists[*handle].first;

  while(list_value  != NULL)
  {
    ta_lists[*handle].first = ta_lists[*handle].first->next;

    free(list_value->data);
    free(list_value);

    list_value = ta_lists[*handle].first;
  }

  memset(&ta_lists[*handle], 0, sizeof(TaListValue));

  *handle = INVALID_HANDLE;

  return 0;
}

int TaListDeleteMarked(LIST_HANDLE handle)
{
  TaListValue *list_value=NULL, *marked_list_value=NULL;

  if((handle < 0) || (ta_lists[handle].occupied == 0))
    return -1;

  list_value = ta_lists[handle].first;

  while(list_value != NULL)
  {
    marked_list_value = list_value;

    if(marked_list_value->mark != 0)
    {
if(marked_list_value->no == 1)
  marked_list_value->no = marked_list_value->no;

      list_value = marked_list_value->next;

      if(marked_list_value->prev != NULL)
        marked_list_value->prev->next = marked_list_value->next;
      else
      {
        list_value->prev = NULL;
        ta_lists[handle].first = list_value;
      }

      if(marked_list_value->next != NULL)
        marked_list_value->next->prev = marked_list_value->prev;
      else
      {
        list_value->next = NULL;
        ta_lists[handle].last = list_value;
      }

      free(marked_list_value->data);
      free(marked_list_value);

      ta_lists[handle].item_count--;
    }
    else
      list_value = list_value->next;
  }

  return 0;
}

int TaListDeleteNonMarked(LIST_HANDLE handle)
{
  TaListValue *list_value=NULL, *marked_list_value=NULL;

  if((handle < 0) || (ta_lists[handle].occupied == 0))
    return -1;

  list_value = ta_lists[handle].first;

  while(list_value != NULL)
  {
    marked_list_value = list_value;

    if(marked_list_value->mark == 0)
    {
if(marked_list_value->no == 1)
  marked_list_value->no = marked_list_value->no;

      list_value = marked_list_value->next;

      if(marked_list_value->prev != NULL)
        marked_list_value->prev->next = marked_list_value->next;

      if(marked_list_value->next != NULL)
        marked_list_value->next->prev = marked_list_value->prev;

      free(marked_list_value->data);
      free(marked_list_value);

      ta_lists[handle].item_count--;
    }
    else
      list_value = list_value->next;
  }

  return 0;
}

// Local functions
int ta_check_duplicate(LIST_HANDLE handle, unsigned char *data, unsigned int size)
{
  unsigned int i;
  TaListValue *list_value=NULL;

  if((handle < 0) || (ta_lists[handle].occupied == 0) || (data == NULL) || (size == 0))
    return -1;

  for(i=1; i<=ta_lists[handle].item_count; i++)
  {
    list_value = TaListGet(handle, i);

    if((list_value->size == size) && (memcmp(list_value->data, data, size) == 0))
      return i;
  }

  return 0;
}


