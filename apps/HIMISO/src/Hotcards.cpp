#include <libda/chotcard.h>
#include "Hotcards.h"
#include "CLog.h"
#include "utils.h"
#include "HIMISOver.h"

static CLog log(HIMISO_NAME);

#define HC_FULL_DAT_NAME		"flash/HC.dat"
#define HC_ADD_DAT_NAME			"flash/HC_add.dat"
#define HC_DEL_DAT_NAME			"flash/HC_del.dat"

FileAction::FILENAME get_FILENAME(Hotcards::HC_TYPE type);

Hotcards::Hotcards(HC_TYPE type):FileAction(get_FILENAME(type))
{
	status = -1;
	fh = NULL;
	clear_version = false;
}

void Hotcards::ClearVersion(void)
{
	clear_version = true;
}

bool Hotcards::isVersionClear(void)
{
	return clear_version;
}

int Hotcards::Download()
{
	string version;

	if(!clear_version)
		version = GetVersion();
	else
		version = "0000";

	log.message(MSG_INFO "Hotcards Download Start\n");

	return PerformAction(version, "hot_crd");
}

int Hotcards::ResponseData(int msg_no, string data)
{
	(void)msg_no;

	log.message(MSG_INFO "Hotcards RX ("+Utils::UtilsIntToString(data.length())+" bytes)\n");// ["+data+"]\r\n");

	if(fh)
		fwrite(data.data(), 1, data.length(), fh);
	else
		return -1;

	return 0;
}

int Hotcards::FileName(int msg_no, string filename)
{
	(void)msg_no;

	log.message(MSG_INFO "Hotcard filename ["+filename+"]\r\n");

	string new_filename;

	if(filename.at(filename.length()-1) == 'A')
		new_filename = HC_ADD_DAT_NAME;
	else if(filename.at(filename.length()-1) == 'D')
		new_filename = HC_DEL_DAT_NAME;
	else if(filename.at(filename.length()-1) == '0')
		new_filename = HC_FULL_DAT_NAME;
	else
		return -1;

	if((cur_filename.length() == 0) || (cur_filename.compare(new_filename) != 0)){
		// This is a new file

		if(fh)
			fclose(fh);

		cur_filename = new_filename;

		log.message(MSG_INFO "Hotcards opening file "+cur_filename+"\n");

		fh = fopen(cur_filename.c_str(), "w+b");

		if(fh == NULL){
			log.message(MSG_INFO "Hotcards failed to open "+cur_filename+"\n");
			return -1;
		}

		if(msg_no == 1){
			uint pos1, pos2;

			if((pos1 = filename.find_first_of(':')) == string::npos){
				log.message(MSG_INFO "HC VERSION ERROR 1\n");
				return -1;
			}

			if((pos2 = filename.find_first_of(':', pos1+1)) == string::npos){
				log.message(MSG_INFO "HC VERSION ERROR 1\n");
				return -2;
			}

			string version = filename.substr(pos1+1, pos2-pos1-1);

			if(version.length() != 4){
				log.message(MSG_INFO "HC VERSION ERROR 1\n");
				return -3;
			}

			fwrite(version.data(), 1, version.length(), fh);
		}
	}

	return 0;
}

void Hotcards::Error(ERRORCODE code)
{
	status = (int)code;

	if(fh)
		fclose(fh);

	switch(status){
		case ERROR_CONNECT:
			log.message(MSG_INFO "Hotcards Error Connecting\n");
			break;
		case ERROR_SEND:
			log.message(MSG_INFO "Hotcards Error Sending\n");
			break;
		case ERROR_TIMEOUT:
			log.message(MSG_INFO "Hotcards Error Timeout\n");
			break;
		case ERROR_FORMAT:
			log.message(MSG_INFO "Hotcards Error Format\n");
			break;
		case ERROR_MISSING_DATA:
			log.message(MSG_INFO "Hotcards Error Missing data\n");
			break;
		case ERROR_FILE_SYSTEM:
			log.message(MSG_INFO "Hotcards File System Error\n");
			break;
	}
}

void Hotcards::FileActionComplete(void)
{
	log.message(MSG_INFO "Hotcards Download Complete\n");

	if(fh)
		fclose(fh);

	status = 0;

	remove("flash/hcmsg_no.dat");
}

FileAction::FILENAME get_FILENAME(Hotcards::HC_TYPE type)
{
	if(type == Hotcards::HCT_FULL)
		return FileAction::FN_HC_FULL;
	else
		return FileAction::FN_HC_PART;
}

string Hotcards::GetVersion(void)
{
	string version;
	com_verifone_hotcard::CHotcard hotcards;
	hotcards.getVersion(version);

	if(version.length() == 0)
		version = "0";

	log.message(MSG_INFO "Hotcards version number ["+version+"]\n");

	return version;
}

void Hotcards::Cleanup(void)
{
	remove(HC_FULL_DAT_NAME);
	remove(HC_ADD_DAT_NAME);
	remove(HC_DEL_DAT_NAME);
}
