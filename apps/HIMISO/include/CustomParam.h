#ifndef _CUSTOMPARAM_H_
#define _CUSTOMPARAM_H_

#include <cstdio>
#include "FileAction.h"

class CustomParam : public FileAction
{
public:
	CustomParam():FileAction(FN_CUST_PARAMETERS){status=-1;fh=NULL;}
	int Download();
	static void Cleanup(void);

private:
	int status;
	FILE *fh;

	int ResponseData(int msg_no, string data);
	int FileName(int msg_no, string filename);
	void Error(ERRORCODE code);
	void FileActionComplete(void);
};

#endif // _TERMPARAM_H_
