#ifndef _MERCH_MSG_H_
#define _MERCH_MSG_H_

#include <cstdio>
#include "FileAction.h"

class MerchMsg : public FileAction
{
public:
	MerchMsg():FileAction(FN_MERCH_MSG){status=-1;}
	int Download();
	static void Cleanup(void);

private:
	int status;
	FILE *fh;

	int ResponseData(int msg_no, string data);
	int FileName(int msg_no, string filename);
	void Error(ERRORCODE code);
	void FileActionComplete(void);
};

#endif // _MERCH_MSG_H_
