#ifndef _MESSAGE_H_
#define _MESSAGE_H_

#include <string>
#include <libda/cbatchmanager.h>
#include <libda/cbatchrec.h>
#include "bertlv.h"

using namespace com_verifone_batchrec;

class Message
{
public:
	Message(string name, CBatchRec record):batchrec(record),message_name(name),use_batchrec(true){};
	Message(string name):message_name(name),use_batchrec(true){};

protected:
	int send_message_auth(std::string &auth, std::string &response, bool isReverse);
	int send_message_param(std::string &auth, std::string &response);

	std::string zero_pad(int num, int pad_count);
	std::string zero_pad(string str, int pad_count);
	std::string space_fill(int pad_count);
	std::string space_pad(std::string str, int pad_count);
	int update_batchrec(CBatchRec &batchrec);
	void BuildICCData(string tags, string &icc_data);
	void BuildEmvTrack2(string tags, string &emv_track2);
	void FilterEMVTags(string tags, string &filtered_tags);
	void LogReversal(void);
	void TraceTags(string tags);
	std::string MapActionCode(std::string host_ac);

	CBatchRec batchrec;

private:
	string message_name;
	bool use_batchrec;
};

#endif // _MESSAGE_H_
