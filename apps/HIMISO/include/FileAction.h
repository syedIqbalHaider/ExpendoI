#ifndef _FILE_ACTION_H_
#define _FILE_ACTION_H_

#include <string>
#include "Message.h"
#include <iostream>
//using namespace std;
using std::cout;

class FileAction:public Message
{
public:
	// File names used by file action message
	enum FILENAME
	{
		FN_PARAMETERS,
		FN_CUST_PARAMETERS,
		FN_DATE_TIME,
		FN_CARD,
		FN_SUPER_TOTALS,
		FN_SPECIAL_BINS,
		FN_COMMS_PARAMS,
		FN_MERCH_MSG,
		FN_APP_BMP,
		FN_SW_DNLD,
		FN_HC_FULL,
		FN_HC_PART
	};

	FileAction(FILENAME type):Message("FileAction"),type(type){}

	int PerformAction(void);
	int PerformAction(string file_version, string table_name, bool confirm_completion=true);

	// Negative return value will abort the process. Return 0 for success
	virtual int ResponseData(int msg_no, string data)=0;
	virtual int FileName(int msg_no, string filename){(void)msg_no;(void)filename;return 0;};	// Used where a filename is returned by the host
	virtual void FileActionComplete(void)=0;

	typedef enum{
		ERROR_CONNECT,
		ERROR_SEND,
		ERROR_TIMEOUT,
		ERROR_FORMAT,
		ERROR_MISSING_DATA,
		ERROR_FILE_SYSTEM
	}ERRORCODE;

	virtual void Error(ERRORCODE code)=0;

private:
	FILENAME type;
	string filename;
	int get_msg_no(FILENAME filename);
	int set_msg_no(FILENAME filename, int msg_no);

};

#endif // _FILE_ACTION_H_
