#ifndef _BERTLV_H_
#define _BERTLV_H_

#include <string>
#include <map>
//using namespace std;
using std::string;
using std::map;
using std::basic_string;

namespace za_co_verifone_bertlv {
	class BERTLV {
		public:
			BERTLV() { };
			BERTLV(const string &data);
			~BERTLV() { };
			int getTagLenVal(int startPosition,string &tag,string &len,string &val);
			void resetPosition();
			int getNextPosition();
			int getValForTag(const string &tag,string &val);
			int addTag(const string &tag,const string &val);
			int appendTag(const string &tag,const string &val);	// Same as addTag, but does not check for duplicates
			int updateTag(const string &tag,const string &val);
			int replaceTags(const string &tagString);
			int deleteTag(const string &tag);
			string getStringData();
			string getAsciiHex();
		private:
			string bertlvData;
			int bertlvIndex;
			int getTag(int startPosition,string &tag);
			int getLen(int startPosition,string &len);
			unsigned long berLenToLong(const string &len);
			void longToBerLen(unsigned long ulLength,string &len);
	};
}

#endif
