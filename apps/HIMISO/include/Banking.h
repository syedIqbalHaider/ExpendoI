#ifndef _BANKING_H_
#define _BANKING_H_

#include <libda/cbatchrec.h>

namespace za_co_verifone_banking
{

class Banking{
	enum{
		BATCH_TYPE_CURRENT,
		BATCH_TYPE_PREVIOUS
	}BATCH_TYPE;

public:
	Banking(){};
	int DoBanking(void);

private:
	int send_advice(com_verifone_batchrec::CBatchRec batchrec);
	int send_batch_up_load(com_verifone_batchrec::CBatchRec batchrec);

	int total_no_Upload_trns;
};

}

#endif // _BANKING_H_
