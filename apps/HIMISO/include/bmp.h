#ifndef _BMP_H_
#define _BMP_H_

typedef enum
{
  MSB_FIRST, 
  LSB_FIRST
}BMP_BYTE_ORDER;

#define MAX_NO_BITMAPS          20  // Maximum number of bit streams supported

typedef int BMPH;                   // Handle to a bit stream

// Error codes
#define BMP_SUCCESS              0
#define BMP_NO_HANDLE           -1
#define BMP_SIZE_ERROR          -2
#define BMP_MEM_ERROR           -3

// direction to shift the bits
typedef enum
{
	BLEFT,
	BRIGHT
}BIT_DIRECTION;

// Initialises the handles
void BmpInit(void);

// Allocate enough memory to hold no_bits
BMPH BmpCreate(unsigned long no_bits);

// Releases resources
bool BmpDestroy(BMPH *handle);

// Converts a string to a bit stream
long BmpImport(BMPH handle, unsigned char *data, unsigned long no_bits);

// Converts a  bit stream to a string
long BmpExport(BMPH handle, unsigned char *data, unsigned long max_len_bytes);

// Gets the total number of bits in the bit stream
unsigned long BmpGetTotalNoBits(BMPH handle, bool *error);

// Check if a particular bit in the stream is set
bool BmpIsBitSet(BMPH handle, unsigned long bit_number, bool *error, int direction);

// Sets a prticular bit in the bit stream
bool BmpSetBit(BMPH handle, unsigned long bit_number, bool *error, int direction);

// Clears a particular bit in the bit stream
bool BmpClearBit(BMPH handle, unsigned long bit_number, bool *error, int direction);

// Sets all the bits in the bit srteam to 0
bool BmpClearBitMask(BMPH handle, bool *error);

// Logical and between an external bit value and a bit in the bit stream
bool BmpAndBit(BMPH handle, unsigned long bit_number, bool value, bool *error, int direction);

// Invert a particular in the bit stream
bool BmpInvertBit(BMPH handle, unsigned long bit_number, bool *error, int direction);

// Inverts the whole bit stream
bool BmpInvertBitmask(BMPH handle, bool *error);


bool BmpRotateBitLeft(BMPH handle, unsigned long start_pos, bool new_or_old, bool *error, int direction);

bool BmpRotateBitRight(BMPH handle, unsigned long start_pos, bool new_or_old, bool *error, int direction);

void BmpPrintBinary(BMPH handle, BMP_BYTE_ORDER byte_order, char one_symbol, char zero_symbol);   // Prints the bitmap in binary form

//void BmpPrintHex(void);   // Prints the bitmap in binary form

#endif
