#ifndef _TERMAPP_SESSION_H_
#define _TERMAPP_SESSION_H_

#include <string>
#include <cstring>
#include <sys/types.h>
#include <sys/timeb.h>
#include <libda/ccommssettings.h>

//using namespace std;
using std::string;
using std::basic_string;

class TermAppSession
{
public:
	enum ERROR
	{
		NONE,
		CONNECT,
		SEND,
		TIMEOUT
	};

	enum SESSION_TYPE
	{
		SESSION_AUTH,
		SESSION_PARAM
	};

private:
	static TermAppSession* single_instance;

	static ERROR last_error;

	int receive_timeout;					// seconds

	TermAppSession(SESSION_TYPE type);		// Make sure it cannot be instantiated without calling Establish()
	int status();							// Negative value indicates connection error, 0 for success
	int writeCommsLog();

	string ip;
	string port;

	// For logging purposes
	SESSION_TYPE session_type;
	unsigned int bytes_sent;
	unsigned int bytes_recv;
	time_t tp_start;
	time_t tp_end;
	string timestamp;

public:
	static TermAppSession* Establish(SESSION_TYPE type);
	static void End();

	int Send(string data);
	int Receive(string &response);
	static ERROR GetLastError();

	~TermAppSession();
};

#endif // _TERMAPP_SESSION_H_
