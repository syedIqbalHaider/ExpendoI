#ifndef _COMMS_PARAM_H_
#define _COMMS_PARAM_H_

#include <cstdio>
#include "FileAction.h"

class CommsParam : public FileAction
{
public:
	CommsParam():FileAction(FN_COMMS_PARAMS){status=-1;}
	int Download();
	static void Cleanup(void);

private:
	int status;
	FILE *fh;

	int ResponseData(int msg_no, string data);
	int FileName(int msg_no, string filename);
	void Error(ERRORCODE code);
	void FileActionComplete(void);
};

#endif // _COMMS_PARAM_H_
