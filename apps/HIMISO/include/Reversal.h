#ifndef _REVERSAL_H_
#define _REVERSAL_H_

#include <string>
#include <libda/cbatchmanager.h>
#include <libda/cbatchrec.h>
#include "Message.h"

using namespace com_verifone_batchrec;

class Reversal:public Message
{
public:
	Reversal(CBatchRec &batchrec);
	int doReversal();
	int clearReversal(void);

private:
	bool record_found;
};

#endif // _REVERSAL_H_
