#ifndef _SPECIAL_BINS_H_
#define _SPECIAL_BINS_H_

#include <cstdio>
#include "FileAction.h"

class SpecialBins : public FileAction
{
public:
	SpecialBins():FileAction(FN_SPECIAL_BINS){status=-1;}
	int Download();
	static void Cleanup(void);

private:
	int status;
	FILE *fh;

	int ResponseData(int msg_no, string data);
	int FileName(int msg_no, string filename);
	void Error(ERRORCODE code);
	void FileActionComplete(void);
};

#endif // _SPECIAL_BINS_H_
