#ifndef _DATE_TIME_PARAM_H_
#define _DATE_TIME_PARAM_H_

#include <cstdio>
#include "FileAction.h"

class DateTimeParam : public FileAction
{
public:
	DateTimeParam():FileAction(FN_DATE_TIME){status=-1;}
	int Download();
	static void Cleanup(void);

private:
	int status;
	FILE *fh;

	int ResponseData(int msg_no, string data);
	void Error(ERRORCODE code);
	void FileActionComplete(void);
};

#endif // _DATE_TIME_PARAM_H_
