#ifndef _SW_DNLD_H_
#define _SW_DNLD_H_

#include <cstdio>
#include "FileAction.h"

class SwDnld : public FileAction
{
public:
	SwDnld():FileAction(FN_SW_DNLD){status=-1;}
	int Download();
	static void Cleanup(void);

private:
	int status;
	FILE *fh;

	int ResponseData(int msg_no, string data);
	int FileName(int msg_no, string filename);
	void Error(ERRORCODE code);
	void FileActionComplete(void);
};

#endif // _SW_DNLD_H_
