#ifndef _TERMPARAM_H_
#define _TERMPARAM_H_

#include <cstdio>
#include "FileAction.h"

class TermParam : public FileAction
{
public:
	TermParam():FileAction(FN_PARAMETERS){status=-1;fh=NULL;}
	int Download();
	static void Cleanup(void);

private:
	int status;
	FILE *fh;

	int ResponseData(int msg_no, string data);
	int FileName(int msg_no, string filename);
	void Error(ERRORCODE code);
	void FileActionComplete(void);
};

#endif // _TERMPARAM_H_
