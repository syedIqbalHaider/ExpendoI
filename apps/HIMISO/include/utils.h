#ifndef _UTILS_H_
#define _UTILS_H_

#include <string>
#include <sstream>
#include <map>

//using namespace std;
using std::istringstream;
using std::stringstream;
using std::string;
using std::map;


namespace Utils{

#define UTILS_ASCII_HEX( x ) dynamic_cast< std::ostringstream & >( std::ostringstream() << std::dec << x ).str()

typedef enum
{
	TT_UNKNOWN,
	TT_MX_925,
	TT_UX_300
}TERMINAL_TYPE;

int UtilStringToBCD(string str, char *bcd);
int UtilStringToBCDForPan(string str, char *bcd); //iqbal_audi_251116
string UtilsBcdToString(const char *data, unsigned int len);
int UtilsStringToInt(string str);
string UtilsIntToString(int int_val);
string UtilsLongToString(long long_val);
string UtilsHexToString(const char *data, unsigned int len);
string UtilsStringToHex(string input);
string UtilsBoolToString(bool b);
string UtilsTimestamp();
string UtilsBase62Encode(long lInput);
TERMINAL_TYPE UtilsGetTerminalType(void);
string zero_pad(string str, int pad_count);

}

#endif // _UTILS_H_
