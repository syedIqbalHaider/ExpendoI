#ifndef _CARD_PARAM_H_
#define _CARD_PARAM_H_

#include <cstdio>
#include "FileAction.h"

class CardParam : public FileAction
{
public:
	CardParam():FileAction(FN_CARD){status=-1;clear_version=false;msg_no=0;}
	int Download();
	static void Cleanup(void);
	void ClearVersion(void);
	bool isVersionClear(void);

private:
	int status;
	FILE *fh;
	bool clear_version;
	int msg_no;

	int ResponseData(int msg_no, string data);
	int FileName(int msg_no, string filename);
	void Error(ERRORCODE code);
	void FileActionComplete(void);
	string GetVersion(void);
};

#endif // _CARD_PARAM_H_
