#ifndef _VALUES_H_
#define _VALUES_H_

#define TX_ISO_SALE                0x00
#define TX_ISO_CASH_ADVANCE        0x01
#define TX_ISO_VOID                0x02
#define TX_ISO_SALE_CASH           0x09
#define TX_ISO_REFUND              0x20
#define TX_ISO_DEPOSIT             0x21
#define TX_ISO_CORRECTION          0x22
#define TX_ISO_BAL_ENQUIRY         0x31
#define TX_ISO_DCC_LOOKUP		   0x32
#define TX_ISO_ACC_STAT		       0x38
#define TX_ISO_TRANSFER			   0x40
#define TX_ISO_PMNT_DEPOSIT        0x51
#define TX_ISO_PIN_CHANGE          0x92
#define TX_ISO_LOAD_PRODUCT        0x91

typedef enum
{
	PEM_UNKNOWN=0,
	PEM_SWIPED,
	PEM_ENTERED,
	PEM_INSERTED,
	PEM_TAPPED,
	PEM_FALLBACK
}PanEntryMode;

#endif // _VALUES_H_
