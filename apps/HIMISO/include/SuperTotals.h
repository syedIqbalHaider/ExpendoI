#ifndef _SUPER_TOTALS_H_
#define _SUPER_TOTALS_H_

#include <cstdio>
#include "FileAction.h"

class SuperTotals : public FileAction
{
public:
	SuperTotals():FileAction(FN_SUPER_TOTALS){status=-1;}
	int Download();
	static void Cleanup(void);

private:
	int status;
	FILE *fh;

	int ResponseData(int msg_no, string data);
	int FileName(int msg_no, string filename);
	void Error(ERRORCODE code);
	void FileActionComplete(void);
};

#endif // _SUPER_TOTALS_H_
