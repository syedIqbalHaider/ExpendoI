#ifndef _RECONADVICE_H_
#define _RECONADVICE_H_

#include "Message.h"

class Recon:public Message
{
public:
	Recon(void);
	int SendReconAdvice(const char *processCode);

	unsigned int getNoDebits(void);
	unsigned int getNoDebitsSecondCurrency(void);
	unsigned int getNoCredits(void);
	unsigned int getNoCreditsSecondCurrency(void);

	long getDebitsTotals(void);
	long getDebitsTotalsSecondCurrency(void);
	long getCreditsTotals(void);
	long getCreditsTotalsSecondCurrency(void);

	int getActionCode(void);

private:
	unsigned int credits_count, credit_reversals;
	long credits_total_amount, credits_reversals_total_amount;

	unsigned int credits_count_second_curr, credit_reversals_second_curr;
	long credits_total_amount_second_curr, credits_reversals_total_amount_second_curr;

	unsigned int debits_count, debits_reversals_count;
	long debits_total_amount, debits_reversals_total_amount;

	unsigned int debits_count_second_curr, debits_reversals_count_second_curr;
	long debits_total_amount_second_curr, debits_reversals_total_amount_second_curr;

	unsigned int authorisations, authorisation_reversals;
	long authorisations_total_amount, authorisation_reversals_total_amount;

	int action_code;

	int batch_no;
};

#endif // _RECONADVICE_H_
