#ifndef AUTH_H_
#define AUTH_H_

#include <string>
#include "Message.h"

//using namespace std;

class Auth:public Message
{
public:
	Auth(CBatchRec &batchrec):Message("Auth", batchrec),got_pin_data(false){}
	Auth(CBatchRec &batchrec, string pinblock, string ksn):Message("Auth", batchrec), apinblock(pinblock),aksn(ksn),got_pin_data(true){}

	int doAuth(void);

private:
	string apinblock;
	string aksn;
	bool got_pin_data;

	void getCustomPrms (map<string, string> &custPrms);
};

#endif /* AUTH_H_ */
