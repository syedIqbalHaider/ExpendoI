#ifndef _HOTCARDS_H_
#define _HOTCARDS_H_

#include <cstdio>
#include "FileAction.h"

class Hotcards : public FileAction
{
public:
	enum HC_TYPE
	{
		HCT_FULL,
		HCT_PARTIAL
	};

	Hotcards(HC_TYPE type);
	int Download();
	static void Cleanup(void);
	void ClearVersion(void);
	bool isVersionClear(void);

private:
	Hotcards();

	HC_TYPE hc_type;
	int status;
	string cur_filename;
	FILE *fh;
	bool clear_version;

	int ResponseData(int msg_no, string data);
	int FileName(int msg_no, string filename);
	void Error(ERRORCODE code);
	void FileActionComplete(void);
	string GetVersion(void);
};

#endif // _SW_DNLD_H_
