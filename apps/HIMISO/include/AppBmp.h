#ifndef _APP_BMP_H_
#define _APP_BMP_H_

#include <cstdio>
#include "FileAction.h"

class AppBmp : public FileAction
{
public:
	AppBmp():FileAction(FN_APP_BMP){status=-1;}
	int Download();
	static void Cleanup(void);
private:
	int status;
	FILE *fh;

	int ResponseData(int msg_no, string data);
	int FileName(int msg_no, string filename);
	void Error(ERRORCODE code);
	void FileActionComplete(void);
};

#endif // _APP_BMP_H_
