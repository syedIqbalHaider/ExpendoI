#include <string>

//using namespace std;
using std::string;

class DeviceInfo
{
public:

	string ProtocolVersion();
	string TermSerial();
	string AppId();
	string AppLinkTimeStamp();

};
