#ifndef _ADVICE_H_
#define _ADVICE_H_

#include <string>
#include <libda/cbatchrec.h>
#include "Message.h"

using namespace com_verifone_batchrec;

class Advice:public Message
{
public:
	Advice(CBatchRec BatchRec);
	int DoAdvice(void);
	int DoBatchUpLoad(int trans_remain_to_upload);
	string getActionCode(void);
	string getAuthCode(void);
	string getRRN(void);

private:
	string action_code;
	string auth_code;
	string rrn;
	int batch_no;

	void setActionCode(string value);
	void setAuthCode(string value);
	void setRRN(string value);

};

#endif // _ADVICE_H_
