#ifndef _TERMAPP_ISO_H_
#define _TERMAPP_ISO_H_

#include <iostream>

using std::endl;
using std::cout;

typedef enum
{
  MTI_1100,
  MTI_1200,
  MTI_1220,
  MTI_0320,	//iqbal_audi_030117
  MTI_1304, 
  MTI_1314,
  MTI_1420,
  MTI_1520,
  MTI_1604,
  MTI_1624
}MTI;

typedef int ISO_HANDLE;

typedef enum
{
  Extended_Bitmap=1,                            // 1
  Primary_account_number,                       // 2
  Processing_code,                              // 3
  Amount_transaction,                           // 4
  Amount_reconciliation,                        // 5
  Reserved_for_future_use1,                     // 6
  Date_and_time_transmission,                   // 7
  Reserved_for_future_use2,                     // 8
  Conversion_rate_reconciliation,               // 9
  Reserved_for_future_use3,                     // 10
  System_trace_audit_number,                    // 11
  Date_and_time_local_transaction,              // 12 for version 87 only for time
  Date_local_transaction,                     	// 13	iqbal_audi_241116 date only
  Date_expiration,                              // 14                         
  Reserved_for_future_use5,                     // 15
  Date_conversion,                              // 16                         
  Reserved_for_future_use6,                     // 17
  Reserved_for_future_use7,                     // 18
  Reserved_for_future_use8,                     // 19
  Reserved_for_future_use9,                     // 20
  Reserved_for_future_use10,                    // 21
  POS_data_code,                                // 22
  Card_sequence_number,                         // 23
  Function_code,                                // 24
  Message_reason_code,                          // 25
  Reserved_for_future_use11,                    // 26
  Approval_code_length,                         // 27
  Date_reconciliation,                          // 28
  Reconciliation_indicator,                     // 29
  Amounts_original,                             // 30
  Reserved_for_future_use12,                    // 31
  Acquiring_institution_ID_code,                // 32
  Reserved_for_future_use13,                    // 33
  Reserved_for_future_use14,                    // 34
  Track_2_data,                                 // 35
  Reserved_for_future_use15,                    // 36
  Retrieval_reference_number,                   // 37
  Approval_code,                                // 38
  Action_code,                                  // 39
  Service_code,                                 // 40
  Terminal_ID,                                  // 41
  Card_acceptor_ID,                             // 42
  Reserved_for_future_use16,                    // 43 
  Additional_response_data,                     // 44
  Track_1_data,                                 // 45
  Amounts_fees,                                 // 46
  Reserved_for_future_use17,                    // 47
  Additional_data_private,                      // 48
  Currency_code_transaction,                    // 49
  Currency_code_reconciliation,                 // 50
  Reserved_for_future_use18,                    // 51
  PIN_data,                                     // 52
  Security_related_control_information,         // 53
  Additional_amounts,                           // 54
  ICC_data,                                     // 55
  Original_data_elements,                       // 56
  Authorisation_life_cycle_code,                // 57
  Authorising_agent_institution_ID_code,        // 58
  Transport_echo_data,                          // 59
  F_60,                    						// 60
  Reserved_for_future_use20,                    // 61
  Hotcard_capacity,                             // 62
  F_63,                    						// 63
  Reserved_for_future_use22,                    // 64
  Reserved_for_future_use23,                    // 65
  Amounts_original_fees,                        // 66
  Extended_payment_data,                        // 67
  Reserved_for_future_use24,                    // 68
  Reserved_for_future_use25,                    // 69
  Reserved_for_future_use26,                    // 70
  Message_number,                               // 71
  Data_record,                                  // 72
  Reserved_for_future_use27,                    // 73
  Credits_number,                               // 74
  Credits_reversal_number,                      // 75
  Debits_number,                                // 76
  Debits_reversal_number,                       // 77
  Reserved_for_future_use28,                    // 78
  Reserved_for_future_use29,                    // 79
  Reserved_for_future_use30,                    // 80
  Authorisations_number,                        // 81
  Reserved_for_future_use31,                    // 82
  Reserved_for_future_use32,                    // 83
  Reserved_for_future_use33,                    // 84
  Reserved_for_future_use34,                    // 85
  Credits_amount,                               // 86
  Credits_reversal_amount,                      // 87
  Debits_amount,                                // 88
  Debits_reversal_amount,                       // 89
  Authorisations_reversal_number,               // 90
  Reserved_for_future_use35,                    // 91
  Reserved_for_future_use36,                    // 92
  Reserved_for_future_use37,                    // 93
  Reserved_for_future_use38,                    // 94
  Reserved_for_future_use39,                    // 95
  Reserved_for_future_use40,                    // 96
  Amount_net_reconciliation,                    // 97
  Reserved_for_future_use41,                    // 98
  Reserved_for_future_use42,                    // 99
  Receiving_institution_ID_code,                // 100
  File_name,                                    // 101
  Account_identification_1,                     // 102
  Account_identification_2,                     // 103
  Transaction_description,                      // 104
  Reserved_for_future_use43,                    // 105
  Reserved_for_future_use44,                    // 106
  Reserved_for_future_use45,                    // 107
  Reserved_for_future_use46,                    // 108
  Credits_fee_amounts,                          // 109
  Debits_fee_amounts,                           // 110
  Reserved_for_future_use47,                    // 111
  Reserved_for_future_use48,                    // 112
  Reserved_for_future_use49,                    // 113
  Reserved_for_future_use50,                    // 114
  Reserved_for_future_use51,                    // 115
  Reserved_for_future_use52,                    // 116
  Reserved_for_future_use53,                    // 117
  Reserved_for_future_use54,                    // 118
  Reserved_for_future_use55,                    // 119
  Reserved_for_future_use56,                    // 120
  Reserved_for_future_use57,                    // 121
  Reserved_for_future_use58,                    // 122
  Reserved_for_future_use59,                    // 123
  Reserved_for_future_use60,                    // 124
  Reserved_for_future_use61,                    // 125
  Reserved_for_future_use62,                    // 126
  Reserved_for_future_use63                     // 127
}BIT_FIELD;

typedef enum
{
  AddFieldNotApplicable,
  POS_data=1,                                   // 48-F0.1
  Authorisation_profile,                        // 48-F0.2
  Card_verification_data,                       // 48-F0.3
  Extended_transaction_type,                    // 48-F0.4
  Additional_node_data,                         // 48-F0.5
  Inquiry_response_data,                        // 48-F0.6
  Routing_information,                          // 48-F0.7
  Cardholder_information,                       // 48-F0.8
  Address_verification_data,                    // 48-F0.9
  Card_verification_result,                     // 48-F0.10
  Address_verification_result,                  // 48-F0.11
  Retention_data,                               // 48-F0.12
  Bank_Details,                                 // 48-F0.13
  Payee_name_and_address,                       // 48-F0.14
  Payer_account_identification,                 // 48-F0.15
  Structured_Data                               // 48-F0.16
}BIT_FIELDS_ADD_DATA;

//
// Field constant defenitions
// 
typedef enum
{
  DT_GOODS_AND_SERVICES               =0,
  DT_CASH_WITHDRAWEL                  =1,
  DT_ADJUSTMENT                       =2,
  DT_CHQ_VERIFICATION                 =4,
  DT_GOODS_AND_SERVICES_WITH_CASHBACK =9,
  DT_GENERAL_DEBIT                    =12,
  CR_RETURNS                          =20,
  CR_ADJUSTMENT                       =22,
  CR_GENERAL_CREDIT                   =25,
  CR_MERCHANDISE_RQST                 =28,
  IN_AVAIL_FUNDS                      =30,
  IN_BALANCE_INQ                      =31,
  IN_GEN_INQ                          =32,
  IN_FULL_STATEMENT                   =35,
  IN_MERCHANDISE_INQ                  =36,
  IN_CARD_VERIF                       =37,
  IN_MINI_STATEMENT                   =38,
  IN_LINKED_ACC                       =39,
  TR_GENERAL_TFER                     =42,
  AD_GENERAL_ADMIN                    =91,
  AD_DEAD_END                         =93
}PROCESSING_CODE;

#define ISO_SUCCESS             0
#define ISO_INVALID_HANDLE      -1
#define ISO_MEM_ERROR           -2
#define ISO_BITFIELD_SPEC_ERROR -3

//
// Function prototypes
//

///*******************************************************************************
/// Function Name: IsoCreateList
/// 
/// Description: Creates a a linked list in memory that will contain the individual
///              fields used to build the message.
///
///              NOTE: Call IsoDestroyList() to release the memory occupied by the 
///                    list.
/// 
/// Parameters:
/// [IN] MTI mti: Indication of the Message Type Indicator.
///
/// Return values:
/// Returns a handle of type ISO_HANDLE to the list where the individual fields 
/// will be stored.
/// 
///*******************************************************************************
ISO_HANDLE IsoCreateList(MTI mti);

///*******************************************************************************
/// Function Name: IsoDestroyList
/// 
/// Description: Releases memory occupied by the message linked list.
/// 
/// Parameters:
/// [IN/OUT] ISO_HANDLE *handle: Pointer to the linked list handle.
///
/// Return values:
/// None
/// 
///*******************************************************************************
void IsoDestroyList(ISO_HANDLE *handle);

///*******************************************************************************
/// Function Name: IsoAddField
/// 
/// Description: Formats the data according to the spec and stores it in the linked 
///              list for later retrieval. The *data parameter is expected to be in 
///              ASCII format, regardles of the format according to the spec.
/// 
/// Parameters:
/// [IN] ISO_HANDLE handle  : Pointer to the linked list handle.
/// [IN] BIT_FIELD bit_field: The BIT_FIELD enuramted value of the number of the 
///                           field in the message (Please see NOTE below).
/// [IN] BIT_FIELDS_ADD_DATA add_bit_field: The BIT_FIELDS_ADD_DATA enumerated value 
///                                         of the additional data field (Please see 
///                                         NOTE below).
/// [IN] unsigned char *data: Pointer to the data to be populated in the field 
///                           specified by bit_field and/or add_bit_field.
/// [IN] unsigned int len   : The length of the buffer pointed to by *data.
///
/// Return values:
/// ISO_SUCCESS: Field was added
/// ISO_BITFIELD_SPEC_ERROR: (Please see NOTE below)
/// ISO_MEM_ERROR: An error occured when trying to allocate dynamic memory.
///
/// NOTE:
/// The IsoAddField() function does strict validation in the way the BIT_FIELD and 
/// BIT_FIELDS_ADD_DATA enumrators are used. BIT_FIELDS_ADD_DATA is not allowed to 
/// have any value other than AddFieldNotApplicable, when BIT_FIELD has any other 
/// value than Additional_data_private. When any of the additional data fields are to be 
/// populated then pass Additional_data_private into bit_field and use add_bit_field 
/// to specify the bit number for the additional data field. Failure to do so, will 
/// cause IsoAddField() to return ISO_BITFIELD_SPEC_ERROR indication that an invalid 
/// message will result from the given bit fields.
/// 
///*******************************************************************************
int IsoAddField(ISO_HANDLE handle, BIT_FIELD bit_field, BIT_FIELDS_ADD_DATA add_bit_field, unsigned char *data, unsigned int len);

///*******************************************************************************
/// Function Name: IsoBuildMsg
/// 
/// Description: Builds a TermApp ISO8583 message according to the fields passed 
///              using IsoAddField.
/// 
/// Parameters:
/// [IN] ISO_HANDLE *handle: Pointer to the linked list handle.
/// [IN/OUT] unsigned char **msg: Address of the pointer where memory will be 
///                               allocated to store the message buffer. Please 
///                               use AbsMemFree to release this memory when the 
///                               buffer is no longer needed.
///
/// Return values:
/// The length in bytes, of the message pointed to by *msg or 0 if an error occured.
/// 
///*******************************************************************************
unsigned int IsoBuildMsg(ISO_HANDLE handle, unsigned char **msg);

ISO_HANDLE TaIsoDecode(unsigned char *msg, unsigned int msg_len);

int IsoGetField(ISO_HANDLE handle, BIT_FIELD bit_field, BIT_FIELDS_ADD_DATA add_bit_field, unsigned char **data, unsigned int *len);

#endif // _TERMAPP_ISO_H_
