#ifndef _EMV_PARAM_H_
#define _EMV_PARAM_H_

#include <string>
#include <libpam.h>

#include "HIMISOver.h"

//using namespace std;
using std::string;
using std::set;

class EmvParam : public PAM
{
public:
	EmvParam():PAM(HIMISO_NAME, true){}
	int UpdateParameters();

private:

//	int updateEmvParameterResult(string sStatusCode);
	int updateCapkResult(string sStatusCode);

	void traceTags(string tags);
};

#endif // _EMV_PARAM_H_
