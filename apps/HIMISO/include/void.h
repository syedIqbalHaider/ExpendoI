/*
 * void.h
 *
 *  Created on: May 10, 2017
 *      Author: vfisdk
 */

#ifndef APPS_HIMISO_INCLUDE_VOID_H_
#define APPS_HIMISO_INCLUDE_VOID_H_


#include <string>
#include "Message.h"

//using namespace std;

class Void:public Message
{
public:
	Void(CBatchRec &batchrec);

	int doVoid(void);

private:

};


#endif /* APPS_HIMISO_INCLUDE_VOID_H_ */
