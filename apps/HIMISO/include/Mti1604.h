#ifndef MTI1604_H_
#define MTI1604_H_

#include <string>
#include "Message.h"

//using namespace std;

class Mti1604:public Message
{
public:
	Mti1604(CBatchRec &batchrec, string pinblock, string ksn,string newPinblock, string newKsn)
		:Message("PinChange", batchrec), apinblock(pinblock),aksn(ksn),
		 new_pinblock(newPinblock),new_ksn(newKsn),got_pin_data(true){}

	Mti1604(CBatchRec &batchrec):Message("ProdLoad", batchrec),got_pin_data(false) {}

	int doMtiCmd(void);

private:
	string apinblock;
	string aksn;
	string new_pinblock;
	string new_ksn;
	bool got_pin_data;

	void getCustomPrms (map<string, string> &custPrms);
};

#endif /* MTI1604_H_ */
