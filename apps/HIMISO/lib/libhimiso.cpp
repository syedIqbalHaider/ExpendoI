#include <sys/timeb.h>
#include <libipc/ipc.h>
#include <string>
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <iomanip>

#include "libhim.h"
#include "libhimisover.h"
#include "CLog.h"
#include "bertlv.h"

#define HIM_TASK		string("HIMISO")

static CLog log(LIBHIM_NAME);

string UtilsIntToString(int int_val, int pad_count);

//using namespace std;

namespace za_co_vfi_Him
{

Him::Him()
{
//	log.message(MSG_INFO "libhim initialising\n");
//	log.message(MSG_INFO "libhim initialising complete\n");
}

Him::~Him()
{
	log.message(MSG_INFO "libhim destroyed\n");
}

int Him::DoHimCommand(string logName, string cmdToSend, string &response)
{
	log.message(MSG_INFO + logName + "\n");

	pmlret = com_verifone_ipc::connect_to(HIM_TASK);

	if(pmlret < 0){
		stringstream ss;
		ss << MSG_INFO << logName << " exiting due to pml error " << pmlret <<  endl;
		log.message(ss.str());
		return -1;
	}

	string ipcFrom;
	int iret = 0;

	log.message(MSG_INFO"Him::DoHimCommand(): cmdToSend ["+cmdToSend+"]\n");

	com_verifone_ipc::send(cmdToSend,HIM_TASK);

	while(1){
		if(com_verifone_ipc::receive(response,HIM_TASK,ipcFrom)==com_verifone_ipc::IPC_SUCCESS){

			log.message(MSG_INFO"Him::DoHimCommand(): response ["+response+"]\n");

			if(response.compare("Error") == 0) {
				log.message(MSG_INFO + logName + ": Error received\n");
				iret = -1;
			}

			if(response.compare("ConnectError") == 0) {
				log.message(MSG_INFO + logName + ": ConnectError received\n");
				iret = -2;
			}

			if(response.compare("Timeout") == 0) {
				log.message(MSG_INFO + logName + ": Timeout received\n");
				iret = -3;
			}

			break;
		}

		usleep(100000);
	}

	com_verifone_ipc::disconnect(HIM_TASK);

	return iret;
}

int Him::Authorise(void)
{
	string response;
	return DoHimCommand("HimAuthorise","Auth",response);
}

int Him::Authorise(string pinblock, string ksn)
{
	log.message(MSG_INFO "HimAuthorise with pin data\n");

	string response;
	za_co_verifone_bertlv::BERTLV pintlv;
	pintlv.addTag("\xdf\x6c", pinblock);
	pintlv.addTag("\xdf\x81\x03", ksn);

	string command("Auth:");
	command.append(pintlv.getStringData());

	return DoHimCommand("HimAuthWithPin",command,response);
}

int Him::ProductLoad(void)
{
	string response;
	return DoHimCommand("HimProductLoad","ProdLoad",response);
}

int Him::PinChange(string pinblock, string ksn, string new_pinblock, string new_ksn)
{
	log.message(MSG_INFO "PinChange with pin data\n");

	string response;
	za_co_verifone_bertlv::BERTLV pintlv;
	pintlv.addTag("\xdf\x6c", pinblock);
	pintlv.addTag("\xdf\x81\x03", ksn);
	pintlv.addTag("\xdf\x6d", new_pinblock); // correct to select random tag for next two prms?
	pintlv.addTag("\xdf\x81\x04", new_ksn);

	string command("PinChange:");
	command.append(pintlv.getStringData());

	return DoHimCommand("HimPinChange",command,response);
}

int Him::ProcessPendingReversal(void)
{
	string response;
	return DoHimCommand("HimPendingReversal","PendingReversal",response);
}

int Him::ReverseLast(void)
{
	string response;
	return DoHimCommand("HimReverseLast","ReverseLast",response);
}

int Him::VoidSale(string ecr)
{
	string response;

	za_co_verifone_bertlv::BERTLV txData;
	txData.addTag("\x6a", ecr);

	string command("VoidSale");
	command.append(txData.getStringData());

	return DoHimCommand("HimVoidSale",command,response);
}

int Him::ClearReversal(void)
{
	string response;
	return DoHimCommand("HimClearReversal","ClearReversal",response);
}

int Him::FullParameters(void)
{
	string response;
	int iret = DoHimCommand("HimFullParameters","ParmFull",response);

	if(response.compare("Finished") == 0){
		log.message(MSG_INFO "FullParameters(): Finished\n");
		iret = 0;
	}

	log.message(MSG_INFO "FullParameters(): finished with "+UtilsIntToString(iret, 0)+"\n");

	return iret;
}

int Him::PartialParameters(void)
{
	string response;
	int iret = DoHimCommand("HimPartialParameters","ParmPart",response);

	if(response.compare("Finished") == 0){
		log.message(MSG_INFO "FullParameters(): Finished\n");
		iret = 0;
	}
	return iret;
}

int Him::DoBanking(void)
{
	string response;
	int iret = DoHimCommand("HimDoBanking","Bank",response);

	// For now we do our own response parsing to remain consistent with history
	// but this could probably be moved also?
	iret = -1;
	if(response.find("Error") != string::npos){
		//log.message(MSG_INFO "DoBanking(): Error received\n");
		iret = 1;
	}else if(response.find("Finished")  != string::npos){
		log.message(MSG_INFO "DoBanking(): Finished\n");

		if(response.at(response.length()-1) == '1')
			iret = 0;	// Success
	}else{
		log.message(MSG_INFO "DoBanking(): Unexpected response ["+response+"]\n");
	}
	return iret;
}

}

string UtilsIntToString(int int_val, int pad_count)
{
	stringstream ss;
	ss << std::setw(pad_count) << std::setfill('0') << int_val;
	return ss.str();
}
