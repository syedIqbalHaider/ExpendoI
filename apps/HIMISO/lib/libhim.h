#ifndef _HIM_H_
#define _HIM_H_

#include <string>

//using namespace std;
using std::string;
using std::endl;

namespace za_co_vfi_Him
{

class Him
{
public:
	Him();
	virtual ~Him();

	int Authorise(void);
	int Authorise(string pinblock, string ksn);
	int ProductLoad(void);
	int PinChange(string pinblock, string ksn, string new_pinblock, string new_ksn);
	int ProcessPendingReversal(void);
	int ReverseLast(void);
	int VoidSale(string ecr);
	int FullParameters(void);
	int PartialParameters(void);
	int DoBanking(void);
	int ClearReversal(void);		//iqbal_audi_280217

	virtual void Feedback(string message){(void)message;}

private:
	int DoHimCommand(string logName, string cmdToSend, string &response);
	int pmlret;
};

}
#endif // _HIM_H_
