#ifdef VFI_PLATFORM_VOS
	#include <unistd.h>
	#include <pthread.h>
#else
	#include <svc.h>
	#include <posix/pthread.h>
#endif
#include <liblog/logsys.h> 
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <libpml/pml.h>
#include <libpml/pml_abstracted_api.h>
#include <libpml/pml_port.h>
#include <libipc/ipc.h>
#include <gui/gui.h>
#include <bertlv.h>
#include <iostream>

#define MY_TASK_NAME "GUIBRIDGEAPP"

//using namespace std;
using std::cout;
using std::endl;
using namespace za_co_verifone_bertlv;
using namespace vfigui;

static bool bCancel=false;
static bool bProcessing=false;
static bool bTimeout=false;
static com_verifone_pml::eventset_t tPmlEvents;
static int hEvent;

int ProcessEvents(com_verifone_pml::eventset_t tPmlEvents);

void ActiveKeyboard()
{
#if 0
	int hasCapacitiveKeypad;
	int handle;
	int iRc;
	
	uiGetPropertyInt(UI_DEVICE_HAS_CAP_TOUCH_KEYPAD,&hasCapacitiveKeypad);
	dlog_msg("UI_DEVICE_HAS_CAP_TOUCH_KEYPAD [%d]",hasCapacitiveKeypad);
	if(hasCapacitiveKeypad) {
		handle=open(DEV_COM1E,0);
		dlog_msg("DEV_COM1E [%d]",handle);
		if(handle>=0) {
			if(iap_control_function(handle,IAP_CONTROL_KEYPAD_WAKE)>=0) {
				dlog_msg("iap_control_function: success");
			} else {
				dlog_msg("iap_control_function: failure");
			}
			close(handle);
		}
	}
#endif
}

void DeactiveKeyboard()
{
#if 0
	int hasCapacitiveKeypad;
	int handle;
	int iRc;
	
	uiGetPropertyInt(UI_DEVICE_HAS_CAP_TOUCH_KEYPAD,&hasCapacitiveKeypad);
	dlog_msg("UI_DEVICE_HAS_CAP_TOUCH_KEYPAD [%d]",hasCapacitiveKeypad);
	if(hasCapacitiveKeypad) {
		handle=open(DEV_COM1E,0);
		dlog_msg("DEV_COM1E [%d]",handle);
		if(handle>=0) {
			if(iap_control_function(handle,IAP_CONTROL_KEYPAD_SLEEP)>=0) {
				dlog_msg("iap_control_function: success");
			} else {
				dlog_msg("iap_control_function: failure");
			}
			close(handle);
		}
	}
#endif
}

void ReplaceSpacesWithNbsp(string &sValue)
{
	if(sValue.length()==0) {
		sValue="&nbsp;";
	} else {
		string sNewValue;
		for(unsigned int iIndex=0;iIndex<sValue.length();iIndex++) {
			if(sValue.at(iIndex)==' ') {
				sNewValue+="&nbsp;"; 
			}else {
				sNewValue+=sValue.substr(iIndex,1);
			}
		}
		sValue=sNewValue;
	}
}

int DisplayMessage(const string &sFrom,BERTLV &tlvIn)
{
	string dfcc71,dfcc72,dfcc73,dfaf26,dfaf31,dfaf34;
	
	tlvIn.getValForTag("\xDF\xCC\x71",dfcc71);
	tlvIn.getValForTag("\xDF\xCC\x72",dfcc72);
	tlvIn.getValForTag("\xDF\xCC\x73",dfcc73);
	tlvIn.getValForTag("\xDF\xAF\x26",dfaf26);
	tlvIn.getValForTag("\xDF\xAF\x31",dfaf31);
	tlvIn.getValForTag("\xDF\xAF\x34",dfaf34);
	
	if(dfaf34.length()>0) {	// html
		uiDisplay(dfaf34);
	} else {				// legacy text
		string sText;
		ReplaceSpacesWithNbsp(dfcc71);
		ReplaceSpacesWithNbsp(dfcc72);
		ReplaceSpacesWithNbsp(dfcc73);
		ReplaceSpacesWithNbsp(dfaf26);
		sText+="<p style='font-size:12'>"+dfcc71+"</p>";
		sText+="<p style='font-size:12'>"+dfcc72+"</p>";
		sText+="<p style='font-size:12'>"+dfcc73+"</p>";
		sText+="<p style='font-size:12'>"+dfaf26+"</p>";
		uiDisplay(sText);
	}
	
	// suppress response
	if(dfaf31.length()==1 && dfaf31.at(0)==0x01) return(1);
	
	// respond
	BERTLV resTlv;
	resTlv.addTag("\xDF\xAF\x01","\xB1");
	resTlv.addTag("\xDF\xAF\x04",string("\x00",1));
	return(com_verifone_ipc::send(resTlv.getStringData(),sFrom)?0:1);
}

bool DisplayMenuTimeoutCancel(void *pData)
{
	int iTimeout=*(int *)pData;
	int iEventCount;
	
	// processing
	bProcessing=true;
	
	// process ipc packets
	iEventCount=com_verifone_pml::event_wait(hEvent,tPmlEvents,0);
	if(iEventCount>0||tPmlEvents.size()>0) {
		dlog_msg("DisplayMenuTimeoutCancel: iEventCount=%d",iEventCount);
		bCancel=false;
		bProcessing=false;
		return(false);
	}
	
	// check for cancel
	if(bCancel) {
		dlog_msg("DisplayMenuTimeoutCancel: cancelled");
		bCancel=false;
		bProcessing=false;
		return(false);
	}
	
	// check timeout
	if(iTimeout!=0 && read_ticks()>iTimeout) {
		dlog_msg("DisplayMenuTimeoutCancel: timeout");
		bCancel=false;
		bProcessing=false;
		bTimeout=true;
		return(false);
	}
	
	// still processing
	return(true);
}

int DisplayMenu(const string sFrom,BERTLV &tlvIn)
{
	#define MAXMENUITEMS 20
	string dfaf02,dfaf28,dfaf29;
	struct UIMenuEntry menu[MAXMENUITEMS];
	size_t tPosition;
	BERTLV resTlv;
	int iTimeout;
	int iIndex;
	int iRc;

	tlvIn.getValForTag("\xDF\xAF\x02",dfaf02);	// timeout
	tlvIn.getValForTag("\xDF\xAF\x28",dfaf28);	// title
	tlvIn.getValForTag("\xDF\xAF\x29",dfaf29);	// options
	
	iIndex=0;
	while((tPosition=dfaf29.find(":"))!=std::string::npos && iIndex<MAXMENUITEMS) {
		menu[iIndex].value=iIndex;
		menu[iIndex].options=0;
		menu[iIndex++].text=dfaf29.substr(0,tPosition);
		dfaf29.erase(0,tPosition+1);
	}
	if(dfaf29.length()>0 && iIndex<MAXMENUITEMS) {
		menu[iIndex].value=iIndex;
		menu[iIndex].options=0;
		menu[iIndex++].text=dfaf29;
	}
	
	if(iIndex==0 || dfaf28.length()==0) {
		resTlv.addTag("\xDF\xAF\x01","\xB2");
		resTlv.addTag("\xDF\xAF\x04","\x11");
		return(com_verifone_ipc::send(resTlv.getStringData(),sFrom)?0:1);
	}
	
	bTimeout=false;
	iTimeout=0;
	if(dfaf02.length()==1 && dfaf02.at(0)>0) iTimeout=read_ticks()+((int)dfaf02.at(0)*1000);

	ActiveKeyboard();
	iRc=uiMenu("menu",dfaf28,menu,iIndex,0,DisplayMenuTimeoutCancel,&iTimeout);
	bProcessing=false;
	DeactiveKeyboard();
	
	if(iRc>=0) {
		string sSelectedIndex=" ";
		sSelectedIndex.at(0)=iRc;
		resTlv.addTag("\xDF\xAF\x01","\xB2");
		resTlv.addTag("\xDF\xAF\x04",string("\x00",1));
		resTlv.addTag("\xDF\xAF\x2A",sSelectedIndex);
		resTlv.addTag("\xDF\xAF\x2B",menu[iRc].text);
		return(com_verifone_ipc::send(resTlv.getStringData(),sFrom)?0:1);
	}

	resTlv.addTag("\xDF\xAF\x01","\xB2");
	if(bTimeout) resTlv.addTag("\xDF\xAF\x04","\x01"); else resTlv.addTag("\xDF\xAF\x04","\x04");
	return(com_verifone_ipc::send(resTlv.getStringData(),sFrom)?0:1);
}

bool DisplayTemplateTimeoutCancel(void *pData)
{
	int iTimeout=*(int *)pData;
	int iEventCount;
	
	// processing
	bProcessing=true;
	
	// process ipc packets
	iEventCount=com_verifone_pml::event_wait(hEvent,tPmlEvents,0);
	if(iEventCount>0||tPmlEvents.size()>0) {
		dlog_msg("DisplayTemplateTimeoutCancel: iEventCount=%d",iEventCount);
		bCancel=false;
		bProcessing=false;
		return(false);
	}
	
	// check for cancel
	if(bCancel) {
		dlog_msg("DisplayTemplateTimeoutCancel: cancelled");
		bCancel=false;
		bProcessing=false;
		return(false);
	}
	
	// check timeout
	if(iTimeout!=0 && read_ticks()>iTimeout) {
		dlog_msg("DisplayTemplateTimeoutCancel: timeout");
		bCancel=false;
		bProcessing=false;
		bTimeout=true;
		return(false);
	}
	
	// still processing
	return(true);
}

int DisplayTemplate(const string sFrom,BERTLV &tlvIn)
{
	string dfaf02,dfaf2c,dfaf2d,dfaf2e,dfaf31;
	string sHtml;
	map<string,string> mapValues;
	BERTLV resTlv;
	size_t tPosition;
	int iTimeout;
	int iRc;
	
	tlvIn.getValForTag("\xDF\xAF\x02",dfaf02);	// timeout
	tlvIn.getValForTag("\xDF\xAF\x2c",dfaf2c);	// template id
	tlvIn.getValForTag("\xDF\xAF\x2e",dfaf2e);	// input fields
	tlvIn.getValForTag("\xDF\xAF\x31",dfaf31);	// suppress response
	
	if(dfaf2c.length()!=1) {
		resTlv.addTag("\xDF\xAF\x01","\xB3");
		resTlv.addTag("\xDF\xAF\x04","\x11");
		return(com_verifone_ipc::send(resTlv.getStringData(),sFrom)?0:1);
	}
	
	switch(dfaf2c.at(0)) {
		case 0x10:	// manual card (cancel/timeout) returns PAN,EXP,CVV
			sHtml="manualcard.html";
			tPosition=dfaf2e.find("AMOUNT=");
			if(tPosition!=std::string::npos) mapValues["AMOUNT"]=dfaf2e.substr(tPosition+7);
			break;
		case 0x11:	// insert card (cancel/timeout)
			sHtml="insert.html";
			tPosition=dfaf2e.find("AMOUNT=");
			if(tPosition!=std::string::npos) mapValues["AMOUNT"]=dfaf2e.substr(tPosition+7);
			break;
		case 0x12:	// swipe card (cancel/timeout)
			sHtml="swipe.html";
			tPosition=dfaf2e.find("AMOUNT=");
			if(tPosition!=std::string::npos) mapValues["AMOUNT"]=dfaf2e.substr(tPosition+7);
			break;
		case 0x13:	// insert or swipe card (cancel/timeout)
			sHtml="insertswipe.html";
			tPosition=dfaf2e.find("AMOUNT=");
			if(tPosition!=std::string::npos) mapValues["AMOUNT"]=dfaf2e.substr(tPosition+7);
			break;
		case 0x14:	// insert or manual card (cancel/timeout/enter)
			sHtml="insertmanual.html";
			tPosition=dfaf2e.find("AMOUNT=");
			if(tPosition!=std::string::npos) mapValues["AMOUNT"]=dfaf2e.substr(tPosition+7);
			break;
		case 0x15:	// swipe or manual card (cancel/timeout/enter)
			sHtml="swipemanual.html";
			tPosition=dfaf2e.find("AMOUNT=");
			if(tPosition!=std::string::npos) mapValues["AMOUNT"]=dfaf2e.substr(tPosition+7);
			break;
		case 0x16:	// insert, swipe or manual card (cancel/timeout/enter)
			sHtml="insertswipemanual.html";
			tPosition=dfaf2e.find("AMOUNT=");
			if(tPosition!=std::string::npos) mapValues["AMOUNT"]=dfaf2e.substr(tPosition+7);
			break;
		case 0x17:	// enter for manual card (cancel/timeout/enter)
			sHtml="manual.html";
			break;
		case 0x18:	// remove card
			sHtml="remove.html";
			break;
		case 0x20:	// please wait
			sHtml="pleasewait.html";
			break;
		case 0x21:	// confirm amount (cancel/timeout) input AMOUNT
			sHtml="confirmamount.html";
			tPosition=dfaf2e.find("AMOUNT=");
			if(tPosition!=std::string::npos) mapValues["AMOUNT"]=dfaf2e.substr(tPosition+7);
			break;
		case 0x22:	// thank you
			sHtml="thankyou.html";
			break;
		case 0x23:	// processing
			sHtml="processing.html";
			break;
		case 0x24:	// enter odometer (cancel/timeout) returns ODOMETER
			sHtml="odometer.html";
			break;
		case 0x25:	// card blocked (cancel/timeout/enter)
			sHtml="cardblocked.html";
			break;
		case 0x26:	// last 4 digits (cancel/timeout) returns DIGITS
			sHtml="last4.html";
			break;
		case 0x27:	// idle display input VERSION
			sHtml="idle.html";
			tPosition=dfaf2e.find("VERSION=");
			if(tPosition!=std::string::npos) mapValues["VERSION"]=dfaf2e.substr(tPosition+8);
			break;
		default:	// unknown template
			resTlv.addTag("\xDF\xAF\x01","\xB3");
			resTlv.addTag("\xDF\xAF\x04","\x13");
			return(com_verifone_ipc::send(resTlv.getStringData(),sFrom)?0:1);
	}
	
	bTimeout=false;
	iTimeout=0;
	if(dfaf02.length()==1 && dfaf02.at(0)>0) iTimeout=read_ticks()+((int)dfaf02.at(0)*1000);

	ActiveKeyboard();
	iRc=uiInvokeURL(mapValues,sHtml,DisplayTemplateTimeoutCancel,&iTimeout);
	bProcessing=false;
	DeactiveKeyboard();
	
	// suppress response
	if(dfaf31.length()==1 && dfaf31.at(0)==0x01) return(1);
	
	// respond
	resTlv.addTag("\xDF\xAF\x01","\xB3");
	if(iRc<0) {
		if(bTimeout) resTlv.addTag("\xDF\xAF\x04","\x01"); else resTlv.addTag("\xDF\xAF\x04","\x04");
	} else {
		resTlv.addTag("\xDF\xAF\x04",string("\x00",1));
		switch(dfaf2c.at(0)) {
			case 0x10:	// PAN,EXP,CVV
				dfaf2d="PAN="+mapValues["PAN"];
				dfaf2d+=":EXP="+mapValues["EXP"];
				dfaf2d+=":CVV="+mapValues["CVV"];
				resTlv.addTag("\xDF\xAF\x2D",dfaf2d);
				break;
			case 0x24:	// ODOMETER
				dfaf2d="ODOMETER="+mapValues["ODOMETER"];
				resTlv.addTag("\xDF\xAF\x2D",dfaf2d);
				break;
			case 0x26:	// DIGITS
				dfaf2d="DIGITS="+mapValues["DIGITS"];
				resTlv.addTag("\xDF\xAF\x2D",dfaf2d);
				break;
		}
	}

	return(com_verifone_ipc::send(resTlv.getStringData(),sFrom)?0:1);
}

int CancelTransaction(const string sFrom)
{
	if(bProcessing) bCancel=true;
	
	return(1);
}

int ProcessEvents(com_verifone_pml::eventset_t tPmlEvents)
{
	string sInBuffer,sFrom;
	int iRc=0;
	
	while(!tPmlEvents.empty()) {
		com_verifone_pml::event_item tEvent=tPmlEvents.back();
		tPmlEvents.pop_back();
		if(tEvent.event_id==com_verifone_pml::events::cascade) {
			dlog_msg("Got a Cascade event");
			com_verifone_pml::eventset_t tCascadeEvents;
			if(event_read(tEvent.evt.cascade.fd,tCascadeEvents)>0) iRc=ProcessEvents(tCascadeEvents);
		} else if(tEvent.event_id==com_verifone_pml::events::ipc) {
			dlog_msg("Got an IPC event");

			while(com_verifone_ipc::check_pending(string(""))==com_verifone_ipc::IPC_SUCCESS) {
				com_verifone_ipc::receive(sInBuffer,string(""),sFrom);
				dlog_msg("IPC Received from %s",sFrom.c_str());
				dlog_hex(sInBuffer.c_str(),sInBuffer.length(),"sInBuffer");
				BERTLV tlvIn(sInBuffer);
				string dfaf01;
				tlvIn.getValForTag("\xDF\xAF\x01",dfaf01);
				if(dfaf01.length()==1) {
					switch(dfaf01.at(0)) {
						case 0x31:	// display message request
							iRc=DisplayMessage(sFrom,tlvIn);
							break;
						case 0x32:	// display menu request
							iRc=DisplayMenu(sFrom,tlvIn);
							break;
						case 0x33:	// display template request
							iRc=DisplayTemplate(sFrom,tlvIn);
							break;
						case 0x72:	// cancel transaction request
							iRc=CancelTransaction(sFrom);
							break;
						default:	// unknown request
							BERTLV resTlv;
							dfaf01.at(0)^=0x80;
							resTlv.addTag(string("\xDF\xAF\x01"),dfaf01);
							resTlv.addTag(string("\xDF\xAF\x04"),string("\x12"));
							iRc=com_verifone_ipc::send(resTlv.getStringData(),sFrom)?0:1;
							break;
					}
				}
			}
		}
	}

	return(iRc);
}

void uiMain(int argc, char *argv[])
{
	(void)argc;(void)argv;
	string sFrom;
	string sInBuffer;
	int iEventCount;
	int iRc;

	// initialise logging
	dlog_init(MY_TASK_NAME);
	dlog_msg("started %s, task no %d",MY_TASK_NAME,get_task_id());
	
	// init ipc
	iRc=com_verifone_ipc::init(string(MY_TASK_NAME));
	if(iRc<0){
		dlog_msg("ERROR: ipc init");
		return;
	}

	// read directgui config
	uiReadConfig();
#if 0	
	// hack for directgui bug
	char szPathDevice[4+12+1];
	memset(szPathDevice,0,sizeof(szPathDevice));
	memcpy(szPathDevice,"www/",4);
	SVC_INFO_MODELNO(&szPathDevice[4]);
	for(int iIndex=sizeof(szPathDevice)-1;iIndex>4;iIndex--) {
		if(szPathDevice[iIndex]<=' ') szPathDevice[iIndex]=0; else break;
	}
	dlog_msg("szPathDevice=[%s]",szPathDevice);
	uiSetPropertyString(UI_PROP_RESOURCE_PATH,szPathDevice);
	// hack for directgui bug end
#endif	
	// directgui debug
	char szBuffer[255+1];
	memset(szBuffer,0,sizeof(szBuffer));
	iRc=uiGetPropertyString(UI_PROP_RESOURCE_PATH,szBuffer,sizeof(szBuffer));
	dlog_msg("UI_PROP_RESOURCE_PATH=%d [%s]",iRc,szBuffer);
	memset(szBuffer,0,sizeof(szBuffer));
	iRc=uiGetPropertyString(UI_PROP_FILE_PREFIX,szBuffer,sizeof(szBuffer));
	dlog_msg("UI_PROP_FILE_PREFIX=%d [%s]",iRc,szBuffer);
	memset(szBuffer,0,sizeof(szBuffer));
	iRc=uiGetPropertyString(UI_PROP_RESOURCE_DEFAULT_PATH,szBuffer,sizeof(szBuffer));
	dlog_msg("UI_PROP_RESOURCE_DEFAULT_PATH=%d [%s]",iRc,szBuffer);
   
	// set working directory
	com_verifone_pml::set_working_directory(com_verifone_pml::getRAMRoot().c_str());

	// init pml
	iRc=com_verifone_pml::pml_init(MY_TASK_NAME);
	if(iRc<0) return;

	// init event handler
	hEvent=com_verifone_pml::event_open();
	if(hEvent<0){
		dlog_msg("ERROR: pml init");
		return;
	}

	// add ipc event
	com_verifone_pml::event_ctl(hEvent,com_verifone_pml::ctl::ADD,com_verifone_ipc::get_events_handler());
	
	// main loop
	while(1) {
		iEventCount=com_verifone_pml::event_wait(hEvent,tPmlEvents);
		if(iEventCount>0||tPmlEvents.size()>0) {
			iRc=ProcessEvents(tPmlEvents);
		}
		SVC_WAIT(100);
	}

	// we should never get here
	return;
}

int main(int argc, char *argv[])
{
	uiMain(argc, argv);
	return 0;
}


