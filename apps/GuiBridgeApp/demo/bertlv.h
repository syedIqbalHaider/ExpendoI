#ifndef _BERTLV_H_
#define _BERTLV_H_

#include <string>

using namespace std;

namespace za_co_verifone_bertlv {
	class BERTLV {
		public:
			BERTLV() { bertlvIndex=0; };
			BERTLV(const string &data);
			~BERTLV() { };
			void clear();
			int getTagLenVal(int startPosition,string &tag,string &len,string &val);
			void resetPosition();
			int getNextPosition();
			int getValForTag(const string &tag,string &val);
			int getLongForTag(const string &tag,long &lValue);
			int getShortForTag(const string &tag,short &sValue);
			int getByteForTag(const string &tag,char &cValue);
			int addTag(const string &tag,const string &val);
			int updateTag(const string &tag,const string &val);
			int deleteTag(const string &tag);
			string getStringData();
		private:
			string bertlvData;
			int bertlvIndex;
			int getTag(int startPosition,string &tag);
			int getLen(int startPosition,string &len);
			unsigned long berLenToLong(const string &len);
			void longToBerLen(unsigned long ulLength,string &len);
	};
}

#endif
