#ifdef VFI_PLATFORM_VOS
	#include <unistd.h>
#else
	#include <svc.h>
#endif
#include <liblog/logsys.h> 
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <libpml/pml.h>
#include <libpml/pml_abstracted_api.h>
#include <libpml/pml_port.h>
#include <libipc/ipc.h>
#include <posix/pthread.h>
#include <bertlv.h>

#define MY_TASK_NAME "GUIBRIDGEDEMOAPP"
#define GUI_TASK_NAME "GUIBRIDGEAPP"

//using namespace std;
using namespace za_co_verifone_bertlv;

int ProcessEvents(com_verifone_pml::eventset_t tPmlEvents)
{
	string sInBuffer,sFrom;
	int iRc=0;
	
	while(!tPmlEvents.empty()) {
		com_verifone_pml::event_item tEvent=tPmlEvents.back();
		tPmlEvents.pop_back();
		if(tEvent.event_id==com_verifone_pml::events::cascade) {
			dlog_msg("Got a Cascade event");
			com_verifone_pml::eventset_t tCascadeEvents;
			if(event_read(tEvent.evt.cascade.fd,tCascadeEvents)>0) iRc=ProcessEvents(tCascadeEvents);
		} else if(tEvent.event_id==com_verifone_pml::events::ipc) {
			dlog_msg("Got an IPC event");
			while(com_verifone_ipc::check_pending(string(""))==com_verifone_ipc::IPC_SUCCESS) {
				com_verifone_ipc::receive(sInBuffer,string(""),sFrom);
				dlog_msg("IPC Received from %s",sFrom.c_str());
				dlog_hex(sInBuffer.c_str(),sInBuffer.length(),"sInBuffer");
			}
		}
	}

	return(iRc);
}

int main(int argc, char *argv[])
{
	com_verifone_pml::eventset_t tPmlEvents;
	BERTLV tlvIn;
	string sFrom;
	string sInBuffer;
	int iEventCount;
	int hEvent;
	int iRc;

	// initialise logging
//	LOG_INIT(MY_TASK_NAME,LOGSYS_COMM,LOGSYS_PRINTF_FILTER);
	dlog_init(MY_TASK_NAME);
	dlog_msg("started %s, task no %d",MY_TASK_NAME,get_task_id());
	
	// set working directory
	com_verifone_pml::set_working_directory(com_verifone_pml::getRAMRoot().c_str());

	// init ipc
	iRc=com_verifone_ipc::init(string(MY_TASK_NAME));
	if(iRc<0) return(-1);

	// init pml
	iRc=com_verifone_pml::pml_init(MY_TASK_NAME);
	if(iRc<0) return(-2);

	// init event handler
	hEvent=com_verifone_pml::event_open();
	if(hEvent<0) return(-3);

	// add ipc event
	com_verifone_pml::event_ctl(hEvent,com_verifone_pml::ctl::ADD,com_verifone_ipc::get_events_handler());
	
	// connect to guibridgeapp
	iRc=com_verifone_ipc::connect_to(string(GUI_TASK_NAME));
	if(iRc!=0) return(-4);
	
	// test menu
// 	tlvIn.addTag("\xDF\xAF\x01","\x32");
// 	tlvIn.addTag("\xDF\xAF\x02","\x1E");
// 	tlvIn.addTag("\xDF\xAF\x28","TEST MENU");
// 	tlvIn.addTag("\xDF\xAF\x29","OPTION 1:OPTION 2:OPTION 3:OPTION 4:OPTION 5:OPTION 6:OPTION 7:OPTION 8:OPTION 9:OPTION 10:OPTION 11:OPTION 12");
// 	iRc=com_verifone_ipc::send(tlvIn.getStringData(),GUI_TASK_NAME);
	
	// test templates
	tlvIn.addTag("\xDF\xAF\x01","\x33");
	tlvIn.addTag("\xDF\xAF\x02","\x1E");
	tlvIn.addTag("\xDF\xAF\x2c","\x10");
	iRc=com_verifone_ipc::send(tlvIn.getStringData(),GUI_TASK_NAME);
		
	// main loop
	while(1) {
		iEventCount=com_verifone_pml::event_wait(hEvent,tPmlEvents);
		if(iEventCount>0||tPmlEvents.size()>0) {
			iRc=ProcessEvents(tPmlEvents);
		}
		SVC_WAIT(1);
	}

	// we should never get here
	return(0);
}
