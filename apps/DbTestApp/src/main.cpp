#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>

#include "libda/cterminalconfig.h"
#include "libda/himdef.h"
#include "libda/cusermanager.h"
#include "libda/ctsn.h"
#include "libda/ccommsconfig.h"
#include "libda/ccommssettings.h"
#include "libda/chotcard.h"
#include "libda/ccardconfig.h"
#include "libda/cemvdata.h"
#include "libda/ccarddata.h"
#include "libda/cversions.h"
#include "libda/cversionsfile.h"
#include "libda/cbatch.h"
#include "libda/cbatchmanager.h"
#include "libda/cbatchrec.h"
#include "libda/creceiptno.h"
#include "libda/cvelocitycheck.h"
#include "libda/cpaninfo.h"


using namespace com_verifone_terminalconfig;
using namespace com_verifone_himdef;
using namespace com_verifone_usermanager;
using namespace com_verifone_user;
using namespace com_verifone_tsn;
using namespace com_verifone_commsconfig;
using namespace com_verifone_commssettings;
using namespace com_verifone_hotcard;
using namespace com_verifone_cardconfig;
using namespace com_verifone_versions;
using namespace com_verifone_versionsfile;
using namespace com_verifone_batch;
using namespace com_verifone_batchmanager;
using namespace com_verifone_batchrec;
using namespace com_verifone_receiptno;
using namespace com_verifone_velocitycheck;
using namespace com_verifone_paninfo;


void TestHotcards();
void TestTerminalParameters();
void TestUsers();
void TestTsn();
void TestCommsParameters();
void TestCardParameters();
void TestEmvParameters();
void TestImportCommsConfigFromFiles();
void TestImportCardConfigFromFiles();
void TestImportEmvConfigFromFiles();
void TestImportHotcardFromFiles();
void TestImportTerminalConfigFromFiles();
void TestVersions();
void TestBatch();
void TestReceiptNo();
void TestVelocity();

void TestIsoParameterImport();


/*
 * arm-unknown-linux-gnueabi-g++ main.cpp -I/home/vfisdk/git/ExpendoI/adk/lib/libda/include -L/home/vfisdk/git/ExpendoI/adk/lib/libda/Platform_vos_gcc/debug/Lib -lda -o DbTestApp
 *
 */

int main()
{

	//TestVersions();
	//TestTerminalParameters();
	//TestIsoParameterImport();
	//TestBatch();
	//TestVelocity();
	//TestReceiptNo();


	//TestImportTerminalConfigFromFiles();
	//TestImportHotcardFromFiles();
	TestImportEmvConfigFromFiles();
	TestImportCardConfigFromFiles();
	//TestImportCommsConfigFromFiles();

	TestEmvParameters();
	//TestCardParameters();
	//TestCommsParameters();
	//TestTsn();
	//TestUsers();
	//TestTerminalParameters();
	return(0);
}

void TestIsoParameterImport() {
/*	CTerminalConfig terminalConfig;
	int ret = terminalConfig.importTerminalConfig(TERM_PARM_PATH, ISO);
	if (ret != DB_OK)
		printf("Import Terminal Config Failed\n");
	else
		printf("Import Terminal Config Success\n");

	CHotcard hotcards;
	int ret = hotcards.importHotcards(HOTCARD_PATH,ISO,"HC_add.dat");
	if (ret != DB_OK)
		printf("Import Hotcards Failed\n");
	else
		printf("Import Hotcards Success\n");

	CCardConfig cardConfig;
	int ret = cardConfig.importCardConfig(CARD_PARM_PATH,ISO);
	if (ret != DB_OK)
		printf("Import Card Parameters Failed\n");
	else
		printf("Import Card Parameters Success\n");*/

	CCardConfig cardConfig;
	int ret = cardConfig.importEmvConfig(EMV_PARM_PATH,ISO);
	if (ret != DB_OK)
		printf("Import Emv Parameters Failed\n");
	else
		printf("Import Emv Parameters Success\n");


	CCommsConfig commsConfig;
	ret = commsConfig.importCommsConfig(COMMS_PARM_PATH,ISO);
	if (ret != DB_OK)
		printf("Import Comms Parameters Failed\n\n");
	else
		printf("Import Comms Parameters Success\n");

}

void TestBatch() {
	CBatchManager batchManager;
	CBatch batch;

	int ret = batchManager.getBatch(batch);
	if (ret != DB_OK)
		printf("Error getting batch\n");

	std::map<unsigned int, CBatchRec> batchRecs;

	ret = batch.getRecords(batchRecs);
    if (ret != DB_OK)
    	printf("Error getting records\n");

    std::map<unsigned int,CBatchRec>::iterator it;
	for (it=batchRecs.begin(); it!=batchRecs.end(); ++it) {
		CBatchRec batchRec = it->second;
		printf("Amount = %lu\n",batchRec.getTxAmount());
	}


	/////////////////////// Batch Functions ///////////////////////////////

/*	int ret = batchManager.getBatch(batch);
	if (ret != DB_OK)
		printf("Error getting batch\n");

	printf("Batch no           - %d\n",batch.getBatchNo());
	printf("Date Time Opened   - %s\n",batch.getDateTimeOpened().c_str());
	printf("Date Time Closed   - %s\n",batch.getDateTimeClosed().c_str());
	printf("State              - %s\n",batch.getState()?"CLOSED":"OPEN");
	printf(" \n");

	CBatchRec batchRec;


	batchRec.setSupervisorName("Leon Test");
	batchRec.setSupervisorId("2000");
	batchRec.setTsn(1);
	batchRec.setTxAmount(15000);
	batchRec.setApproveOnline(true);
	COnlineStatus status;
	status.setManualEntry(true);
	batchRec.setOnlStatus(status);
	batchRec.setInProgress(false);
	batchRec.setPan("123456789");
	char bin[5] = {0x01,0x02,0x00,0x04,0x07};
	std::string tmp( reinterpret_cast<char const*>(bin), 5 ) ;
	batchRec.setEmvTags(tmp);


	ret = batch.writeRecToBatch(batchRec);
	if (ret != DB_OK)
		printf("Error adding record to batch\n");
	else
		printf("Successfully added record to batch!!!\n");

	ret = batch.readLastRecFromBatch(batchRec);
		if (ret != DB_OK)
		printf("Error reading last record from batch\n");
	else
		printf("Successfully read last record from batch!!!\n");


	printf("Supervisor Name     - %s\n",batchRec.getSupervisorName().c_str());
	printf("Supervisor Id       - %s\n",batchRec.getSupervisorId().c_str());
	printf("TSN                 - %d\n",batchRec.getTsn());
	printf("Tx Amount           - %lu\n",batchRec.getTxAmount());
	printf("In progress         - %s\n",batchRec.isInProgress()?"true":"false");
	printf("PAN                 - %s\n",batchRec.getPan().c_str());

	printf(" \n");

	ret = batchManager.closeBatch();
	if (ret != DB_OK)
		printf("Error closing batch\n");
	else
		printf("Closed Batch no %d and opened a new batch\n",batch.getBatchNo());

	printf(" \n");

	ret = batchManager.getPreviousBatch(batch);
	if (ret != DB_OK)
		printf("Error getting previous batch\n");
	else
		printf("Getting previous batch no %d!!!\n",batch.getBatchNo());

	printf("Batch no           - %d\n",batch.getBatchNo());
	printf("Date Time Opened   - %s\n",batch.getDateTimeOpened().c_str());
	printf("Date Time Closed   - %s\n",batch.getDateTimeClosed().c_str());
	printf("State              - %s\n",batch.getState()?"CLOSED":"OPEN");

	printf(" \n");

	ret = batchManager.getBatch(batch);
	if (ret != DB_OK)
		printf("Error getting current batch\n");
	else
		printf("Getting new batch no %d!!!\n",batch.getBatchNo());

	printf("Batch no           - %d\n",batch.getBatchNo());
	printf("Date Time Opened   - %s\n",batch.getDateTimeOpened().c_str());
	printf("Date Time Closed   - %s\n",batch.getDateTimeClosed().c_str());
	printf("State              - %s\n",batch.getState()?"CLOSED":"OPEN");

	printf(" \n");

	ret = batchManager.resetBatchNo(1);
	if (ret != DB_OK)
		printf("Error resetting batch no\n");
	else
		printf("Batch no reset to 1!!!\n");

	printf(" \n");
	/////////////////////// Batch Record Functions ///////////////////////////////

	// get your batch and add the record to batch
	ret = batchManager.getBatch(batch);
	if (ret != DB_OK)
		printf("Error getting previous batch\n");
	else
		printf("Getting current batch no %d!!!\n",batch.getBatchNo());


	CBatchRec batchRec;
	batchRec.setSupervisorName("Leon Test");
	batchRec.setSupervisorId("2000");
	batchRec.setTsn(1);
	batchRec.setTxAmount(15000);
	batchRec.setApproveOnline(true);
	COnlineStatus status;
	status.setManualEntry(true);
	batchRec.setOnlStatus(status);
	batchRec.setInProgress(false);


	ret = batch.writeRecToBatch(batchRec);
	if (ret != DB_OK)
		printf("Error adding record to batch\n");
	else
		printf("Successfully added record to batch!!!\n");


	batchRec.setSupervisorName("Leon Test2");
	batchRec.setSupervisorId("1000");
	batchRec.setTsn(2);
	batchRec.setTxAmount(30000);
	batchRec.setApproveOnline(false);
	status.setManualEntry(false);
	batchRec.setOnlStatus(status);
	batchRec.setInProgress(true);

	ret = batch.writeRecToBatch(batchRec);
	if (ret != DB_OK)
		printf("Error adding record to batch\n");
	else
		printf("Successfully added record to batch!!!\n");


	printf(" \n");

	// get current record

	ret = batch.readLastRecFromBatch(batchRec);
	if (ret != DB_OK)
		printf("Error reading last record from batch\n");
	else
		printf("Successfully read last record from batch!!!\n");


	printf("Supervisor Name     - %s\n",batchRec.getSupervisorName().c_str());
	printf("Supervisor Id       - %s\n",batchRec.getSupervisorId().c_str());
	printf("TSN                 - %d\n",batchRec.getTsn());
	printf("Tx Amount           - %lu\n",batchRec.getTxAmount());
	printf("In progress         - %s\n",batchRec.isInProgress()?"true":"false");

	printf(" \n");
	// change a record and write it back
	batchRec.setInProgress(false);
	ret = batch.writeRecToBatch(batchRec);
	if (ret != DB_OK)
		printf("Error writing back a record\n");
	else
		printf("Successfully changing a batch record!!!\n");

	// get record by tsn

	ret = batch.readRecFromBatch(batchRec,1);
	if (ret != DB_OK)
		printf("Error reading record from batch\n");
	else
		printf("Successfully read lrecord from batch with tsn = 1!!\n");


	printf("Supervisor Name     - %s\n",batchRec.getSupervisorName().c_str());
	printf("Supervisor Id       - %s\n",batchRec.getSupervisorId().c_str());
	printf("TSN                 - %d\n",batchRec.getTsn());
	printf("Tx Amount           - %lu\n",batchRec.getTxAmount());
	printf("In progress         - %s\n",batchRec.isInProgress()?"true":"false");
*/
}

void TestVersions() {
	CVersions versions;
	CHotcard hotcard;

	std::string version;
	//int ret =  versions = hotcard.getVersion(version);

	//printf("Version = %s\n",versions);

	// delete an app
/*	versions.deleteApp("testapp");
	versions.deleteApp("testapp2");

	// check if app exists
	std::map<int,CVersionsFile> files;
	int ret = versions.getFiles("testapp",files);
	if (ret != DB_OK)
		printf("App does not exist\n");

	// add files
	CVersionsFile file;

	file.setName("file1.dat");
	file.setPath("/home/usr1");
	file.setVersion("2014-03-07 10:12:15");
	versions.addFile("testapp",file);

	file.setName("file2.dat");
	file.setPath("/home/usr1");
	file.setVersion("2014-03-02 13:12:15");
	versions.addFile("testapp",file);

	file.setName("file1.dat");
	file.setPath("/home/usr1");
	file.setVersion("2014-03-07 14:12:15");
	versions.addFile("testapp2",file);

	file.setName("file2.dat");
	file.setPath("/home/usr1");
	file.setVersion("2014-03-02 15:12:15");
	versions.addFile("testapp2",file);

	// get all the apps
	std::set<std::string> apps;
	ret = versions.getApps(apps);
	if (ret != DB_OK)
		printf("No Apps in versions table\n");

	std::set<std::string>::iterator it;
	for (it=apps.begin(); it!=apps.end(); ++it) {
		std::string appName = *it;
		printf("App Name       - %s\n",appName.c_str());

		// get the files
		std::map<int,CVersionsFile> files;
		ret = versions.getFiles(appName,files);
		if (ret != DB_OK)
			printf("No files for this app\n");
		else {
			std::map<int,CVersionsFile>::iterator it2;
			for (it2=files.begin(); it2!=files.end(); ++it2) {
				CVersionsFile file = it2->second;
				printf("File Name      - %s\n",file.getName().c_str());
				printf("Path           - %s\n",file.getPath().c_str());
				printf("Version        - %s\n",file.getVersion().c_str());
			}
		}
		printf("---------------------------------\n");

	}*/

}

void TestImportTerminalConfigFromFiles() {
	CTerminalConfig terminalConfig;
	int ret = terminalConfig.importTerminalConfig(TERM_PARM_PATH, ALIENO);
	if (ret != DB_OK)
		printf("Import Terminal Config Failed\n");
	else
		printf("Import Terminal Config Success\n");

	TestTerminalParameters();

}

void TestImportHotcardFromFiles() {
	CHotcard hotcards;
	int ret = hotcards.importHotcards(HOTCARD_PATH,ALIENO,"hotcards.dat");
	if (ret != DB_OK)
		printf("Import Hotcards Failed\n");
	else
		printf("Import Hotcards Success\n");

	TestHotcards();
}

void TestImportEmvConfigFromFiles() {
	CCardConfig cardConfig;
	int ret = cardConfig.importEmvConfig(EMV_PARM_PATH,ISO);
	if (ret != DB_OK)
		printf("Import Emv Parameters Failed\n");
	else
		printf("Import Emv Parameters Success\n");

	//TestEmvParameters();
}

void TestImportCardConfigFromFiles() {
	CCardConfig cardConfig;
	int ret = cardConfig.importCardConfig(CARD_PARM_PATH,ISO);
	if (ret != DB_OK)
		printf("Import Card Parameters Failed\n");
	else
		printf("Import Card Parameters Success\n");

	//TestCardParameters();
}

void TestImportCommsConfigFromFiles() {
	CCommsConfig commsConfig;
	CCommsSettings commsSettings;

	/** for this test place the COMMS.DAT file in usr1 directory . COMMS.DAT is in him_files/alieno directory in git**/

	int ret = commsConfig.importCommsConfig(COMMS_PARM_PATH,ALIENO);
	if (ret != DB_OK)
		printf("Could not import comms parameters into database from Alieno file format\n\n");
	else
		printf("Successfully imported comms parameters into database from Alieno file format\n");

	TestCommsParameters();
}

void TestEmvParameters() {

	printf("------------------EMV PARAMETERS---------------------\n");

	CCardConfig cardConfig;
	CEmvData emvdata;
	int ret;
	ret = cardConfig.lookupAidOverride("407817","A0000000041010",emvdata);
	if (ret == DB_FAIL) {
			printf("No override");
			ret = cardConfig.lookupEmvConfig("a0000000043060",emvdata);
			if (ret == DB_FAIL)
				printf("AID lookup failed");
	}

	printf("------------AID--------------\n");
	printf("AID                       - %s\n",emvdata.getAid().c_str());
	printf("Application Version       - %s\n",emvdata.getApplicationVersion().c_str());
	printf("DDOL                      - %s\n",emvdata.getDefaultDdol().c_str());
	printf("TDOL                      - %s\n",emvdata.getDefaultTdol().c_str());
	printf("Emv Scheme Code           - %s\n",emvdata.getEmvSchemeCode().c_str());
	printf("Totals Group Name         - %s\n",emvdata.getTotalsGroupName().c_str());
	printf("Super Totals Group Name   - %s\n",emvdata.getSuperTotalsGroupName().c_str());
	printf("TAC Decline               - %s\n",emvdata.getTacDecline().c_str());
	printf("TAC Default               - %s\n",emvdata.getTacDefault().c_str());
	printf("TAC Online                - %s\n",emvdata.getTacOnline().c_str());
	printf("Target Percentage         - %s\n",emvdata.getTargetPercentage().c_str());
	printf("Target Percentage Max     - %s\n",emvdata.getTargetPercentageMax().c_str());
	printf("Threshold Amt             - %s\n",emvdata.getThresholdAmt().c_str());
	printf("Card Data Input Mode      - %s\n",emvdata.getCardDataInputMode().c_str());

	std::map<std::string,CPublicKey> keys = emvdata.getPublicKeys();
	std::map<std::string,CPublicKey>::iterator it;
	for (it=keys.begin(); it!=keys.end(); ++it) {
		CPublicKey publicKey = it->second;
		printf("----------Public Key---------\n");
		printf("RID                      - %s\n",publicKey.getRid().c_str());
		printf("Activation               - %s\n",publicKey.getActivation().c_str());
		printf("Expiry                   - %s\n",publicKey.getExpiry().c_str());
		printf("Checksum                 - %s\n",publicKey.getCheckSum().c_str());
		printf("Exponent                 - %s\n",publicKey.getExponent().c_str());
		printf("Hash Algorithm Indicator - %s\n",publicKey.getHashAlgorithIndicator().c_str());
		printf("Key Algorithm Indicator  - %s\n",publicKey.getKeyAlgorithmIndicator().c_str());
		printf("Modulus                  - %s\n",publicKey.getModulus().c_str());
		printf("Key Index                - %s\n",publicKey.getPublicKeyIndex().c_str());
		printf("\n");
	}

	CAccountProfile accountProfile = emvdata.getAccountProfile();
	printf("------Account Profile------\n");
	printf("Code                   - %s\n",accountProfile.getCode().c_str());
	printf("Description            - %s\n",accountProfile.getDescription().c_str());
	printf("Budget Min             - %s\n",accountProfile.getBudgetAmountMin().c_str());
	printf("Allow Budget           - %s\n",accountProfile.isAllowBudget()?"true":"false");
	printf("Man Pan Entry          - %s\n",accountProfile.isAllowManualEntry()?"true":"false");
	printf("Voice Approval         - %s\n",accountProfile.isAllowVoiceApproval()?"true":"false");
	printf("Check Card value       - %s\n",accountProfile.isCheckCardValue()?"true":"false");
	printf("Draft Capture Mode     - %s\n",accountProfile.isDraftCaptureMode()?"true":"false");
	printf("Require Pin Entry      - %s\n",accountProfile.isRequirePinEntry()?"true":"false");
	printf("Require Signature      - %s\n",accountProfile.isRequireSignature()?"true":"false");
	printf("Skim Card Value        - %s\n",accountProfile.isSkimCardValue()?"true":"false");

	printf("\n");
	printf("---Allowed Transactions--\n");
	std::set<std::string> allowedTransactions = accountProfile.getAllowedTransactions();
	std::set<std::string>::iterator it2;
	for (it2=allowedTransactions.begin(); it2!=allowedTransactions.end(); ++it2) {
		std::string allowTx = *it2;
		printf("Transaction Type       - %s\n",allowTx.c_str());
	}
	printf("\n");

	CLimits limit = accountProfile.getLocalLimits();
	printf("-------Local Limit---------\n");
	printf("Cashback Amt Max       - %s\n",limit.getCashbackAmtMax().c_str());
	printf("Floor Limit Amt        - %s\n",limit.getFloorLimitAmt().c_str());
	printf("Transaction Amt Max    - %s\n",limit.getTransactionAmtMax().c_str());
	printf("\n");
	limit = accountProfile.getEmvFallBackLimits();
	printf("-----Emv Fallback Limit----\n");
	printf("Cashback Amt Max       - %s\n",limit.getCashbackAmtMax().c_str());
	printf("Floor Limit Amt        - %s\n",limit.getFloorLimitAmt().c_str());
	printf("Transaction Amt Max    - %s\n",limit.getTransactionAmtMax().c_str());
	printf("\n");
	limit = accountProfile.getOfflineLimits();
	printf("-------Offline Limit-------\n");
	printf("Cashback Amt Max       - %s\n",limit.getCashbackAmtMax().c_str());
	printf("Floor Limit Amt        - %s\n",limit.getFloorLimitAmt().c_str());
	printf("Transaction Amt Max    - %s\n",limit.getTransactionAmtMax().c_str());
	printf("\n");
	limit = accountProfile.getContactLessLimits();
	printf("-----Contactless Limit-----\n");
	printf("Cashback Amt Max       - %s\n",limit.getCashbackAmtMax().c_str());
	printf("Floor Limit Amt        - %s\n",limit.getFloorLimitAmt().c_str());
	printf("Transaction Amt Max    - %s\n",limit.getTransactionAmtMax().c_str());

	printf("\n\n\n");
}



void TestCardParameters() {
	printf("-----------------CARD PARAMETERS---------------------\n");
	CCardConfig cardConfig;
	CCardData cardData;


	int ret = cardConfig.lookupCardConfig("542808",cardData);
	if (ret == DB_FAIL)
		printf("Bin lookup failed\n");

	printf("-------------BIN-----------\n");
	printf("BIN                    - %s\n",cardData.getBin().c_str());
	printf("Card Issued Name       - %s\n",cardData.getCardIssuedName().c_str());
	printf("Card Length            - %d\n",cardData.getCardLength());

	CCardProfile  cardProfile = cardData.getCardProfile();
	printf("-------Card Profile--------\n");
	printf("Pin Len Max            - %d\n",cardProfile.getPinLenMax());
	printf("Pin Len Min            - %d\n",cardProfile.getPinLenMin());
	printf("Voice Appr Text        - %s\n",cardProfile.getVoiceApprovalText().c_str());
	printf("Active                 - %s\n",cardProfile.isActive()?"true":"false");
	printf("Check Expiry date      - %s\n",cardProfile.isCheckExpiryDate()?"true":"false");
	printf("Check Luhn             - %s\n",cardProfile.isCheckLuhn()?"true":"false");
	printf("Check Service Code     - %s\n",cardProfile.isCheckServiceCode()?"true":"false");
	printf("Check Expiry date      - %s\n",cardProfile.isCheckExpiryDate()?"true":"false");
	printf("Foreign Card           - %s\n",cardProfile.isForeignCard()?"true":"false");
	printf("\n");
	std::map<std::string,CAccountProfile> accounts = cardData.getAccounts();
	std::map<std::string,CAccountProfile>::iterator it;
	for (it=accounts.begin(); it!=accounts.end(); ++it) {
		CAccountProfile accountProfile = it->second;
		printf("-------------Attached Account-------------\n");
		printf("Code                   - %s\n",accountProfile.getCode().c_str());
		printf("Description            - %s\n",accountProfile.getDescription().c_str());
		printf("Budget Min             - %s\n",accountProfile.getBudgetAmountMin().c_str());
		printf("Totals Group Name      - %s\n",accountProfile.getTotalsGroupName().c_str());
		printf("Super Totals Group Name- %s\n",accountProfile.getSuperTotalsGroupName().c_str());
		printf("Allow Budget           - %s\n",accountProfile.isAllowBudget()?"true":"false");
		printf("Man Pan Entry          - %s\n",accountProfile.isAllowManualEntry()?"true":"false");
		printf("Voice Approval         - %s\n",accountProfile.isAllowVoiceApproval()?"true":"false");
		printf("Check Card value       - %s\n",accountProfile.isCheckCardValue()?"true":"false");
		printf("Draft Capture Mode     - %s\n",accountProfile.isDraftCaptureMode()?"true":"false");
		printf("Require Pin Entry      - %s\n",accountProfile.isRequirePinEntry()?"true":"false");
		printf("Require Signature      - %s\n",accountProfile.isRequireSignature()?"true":"false");
		printf("Skim Card Value        - %s\n",accountProfile.isSkimCardValue()?"true":"false");

		printf("\n");
		printf("---Allowed Transactions--\n");
		std::set<std::string> allowedTransactions = accountProfile.getAllowedTransactions();
		std::set<std::string>::iterator it2;
		for (it2=allowedTransactions.begin(); it2!=allowedTransactions.end(); ++it2) {
			std::string allowTx = *it2;
			printf("Transaction Type       - %s\n",allowTx.c_str());
		}
		printf("\n");


		CLimits limit = accountProfile.getLocalLimits();
		printf("-------Local Limit---------\n");
		printf("Cashback Amt Max       - %s\n",limit.getCashbackAmtMax().c_str());
		printf("Floor Limit Amt        - %s\n",limit.getFloorLimitAmt().c_str());
		printf("Transaction Amt Max    - %s\n",limit.getTransactionAmtMax().c_str());
		printf("\n");
		limit = accountProfile.getEmvFallBackLimits();
		printf("-----Emv Fallback Limit----\n");
		printf("Cashback Amt Max       - %s\n",limit.getCashbackAmtMax().c_str());
		printf("Floor Limit Amt        - %s\n",limit.getFloorLimitAmt().c_str());
		printf("Transaction Amt Max    - %s\n",limit.getTransactionAmtMax().c_str());
		printf("\n");
		limit = accountProfile.getOfflineLimits();
		printf("-------Offline Limit-------\n");
		printf("Cashback Amt Max       - %s\n",limit.getCashbackAmtMax().c_str());
		printf("Floor Limit Amt        - %s\n",limit.getFloorLimitAmt().c_str());
		printf("Transaction Amt Max    - %s\n",limit.getTransactionAmtMax().c_str());
		printf("\n");
		limit = accountProfile.getContactLessLimits();
		printf("-----Contactless Limit-----\n");
		printf("Cashback Amt Max       - %s\n",limit.getCashbackAmtMax().c_str());
		printf("Floor Limit Amt        - %s\n",limit.getFloorLimitAmt().c_str());
		printf("Transaction Amt Max    - %s\n",limit.getTransactionAmtMax().c_str());
		printf("\n");
	}
}

void TestHotcards() {
	CHotcard hotcard;

	if (hotcard.isHotCard("123456789"))
		printf("Hotcard!!!!!!\n");
	else
		printf("Not Hotcard!!!!!!\n");

	hotcard.deleteHotCard("123456789");

	if (hotcard.isHotCard("123456789"))
		printf("Hotcard!!!!!!\n");
	else
		printf("Not Hotcard!!!!!!\n");

	hotcard.addHotCard("123456789");

	if (hotcard.isHotCard("123456789"))
		printf("Hotcard!!!!!!\n");
	else
		printf("Not Hotcard!!!!!!\n");

	printf("--------------------------------------------------\n\n\n");
}

void TestCommsParameters() {

	printf("---------------COMMS PARAMETERS---------------------\n");


	CCommsConfig commsConfig;
	CCommsSettings commsSettings;

	///////////////// Setup wizard //////////////////////////
	commsSettings.setCommsType(CCommsSettings::ETHERNET);
	commsSettings.setIp("192.168.0.15");
	commsSettings.setPort("21");
	commsConfig.setPriParamCommsSettings(commsSettings);

	commsSettings.setCommsType(CCommsSettings::GPRS);
	commsSettings.setIp("192.168.0.254");
	commsSettings.setPort("57000");
	commsSettings.setApn("misc.xlink.co.za");
	commsConfig.setSecParamCommsSettings(commsSettings);
	////////////////////////////////////////////////////////

	int ret = commsConfig.getPriAuthCommsSettings(commsSettings);
	if (ret != DB_OK)
		printf("Could not retrieve comms settings for primary auths\n");

	printf("PRIMARY AUTH COMMS\n");
	switch (commsSettings.getCommsType()) {
		case CCommsSettings::ETHERNET:
			printf("Type         - ETHERNET\n");
			printf("IP           - %s\n",commsSettings.getIp().c_str());
			printf("Port         - %s\n",commsSettings.getPort().c_str());
			break;
		case CCommsSettings::GPRS:
			printf("Type         - GPRS\n");
			printf("IP           - %s\n",commsSettings.getIp().c_str());
			printf("Port         - %s\n",commsSettings.getPort().c_str());
			printf("APN          - %s\n",commsSettings.getApn().c_str());
			break;
		case CCommsSettings::SPEEDLINK:
			printf("Type         - SPEEDLINK\n");
			printf("Phone Number - %s\n",commsSettings.getPhoneNumber().c_str());
			printf("DTE          - %s\n",commsSettings.getDte().c_str());
			printf("NUI          - %s\n",commsSettings.getNui().c_str());
			break;
		case CCommsSettings::QUICKCONNECT:
			printf("Type         - QUICKCONNECT\n");
			printf("Phone Number - %s\n",commsSettings.getPhoneNumber().c_str());
			printf("DTE          - %s\n",commsSettings.getDte().c_str());
			printf("NUI          - %s\n",commsSettings.getNui().c_str());
			break;
		case CCommsSettings::RADIOPAD:
			printf("Type         - RADIOPAD\n");
			printf("DTE          - %s\n",commsSettings.getDte().c_str());
			break;
		default:
			printf("Unknown Type\n");
			break;
	}

	ret = commsConfig.getSecAuthCommsSettings(commsSettings);
	if (ret != DB_OK)
		printf("Could not retrieve comms settings for sec auths\n");

	printf("\n");
	printf("SECONDARY AUTH COMMS\n");
	switch (commsSettings.getCommsType()) {
		case CCommsSettings::ETHERNET:
			printf("Type         - ETHERNET\n");
			printf("IP           - %s\n",commsSettings.getIp().c_str());
			printf("Port         - %s\n",commsSettings.getPort().c_str());
			break;
		case CCommsSettings::GPRS:
			printf("Type         - GPRS\n");
			printf("IP           - %s\n",commsSettings.getIp().c_str());
			printf("Port         - %s\n",commsSettings.getPort().c_str());
			printf("APN          - %s\n",commsSettings.getApn().c_str());
			break;
		case CCommsSettings::SPEEDLINK:
			printf("Type         - SPEEDLINK\n");
			printf("Phone Number - %s\n",commsSettings.getPhoneNumber().c_str());
			printf("DTE          - %s\n",commsSettings.getDte().c_str());
			printf("NUI          - %s\n",commsSettings.getNui().c_str());
			break;
		case CCommsSettings::QUICKCONNECT:
			printf("Type         - QUICKCONNECT\n");
			printf("Phone Number - %s\n",commsSettings.getPhoneNumber().c_str());
			printf("DTE          - %s\n",commsSettings.getDte().c_str());
			printf("NUI          - %s\n",commsSettings.getNui().c_str());
			break;
		case CCommsSettings::RADIOPAD:
			printf("Type         - RADIOPAD\n");
			printf("DTE          - %s\n",commsSettings.getDte().c_str());
			break;
		default:
			printf("Unknown Type\n");
			break;
	}

	printf("\n");

	ret = commsConfig.getPriParamCommsSettings(commsSettings);
	if (ret != DB_OK)
		printf("Could not retrieve comms settings for primary parms\n");

	printf("PRIMARY PARAM COMMS\n");
	switch (commsSettings.getCommsType()) {
		case CCommsSettings::ETHERNET:
			printf("Type         - ETHERNET\n");
			printf("IP           - %s\n",commsSettings.getIp().c_str());
			printf("Port         - %s\n",commsSettings.getPort().c_str());
			break;
		case CCommsSettings::GPRS:
			printf("Type         - GPRS\n");
			printf("IP           - %s\n",commsSettings.getIp().c_str());
			printf("Port         - %s\n",commsSettings.getPort().c_str());
			printf("APN          - %s\n",commsSettings.getApn().c_str());
			break;
		case CCommsSettings::SPEEDLINK:
			printf("Type         - SPEEDLINK\n");
			printf("Phone Number - %s\n",commsSettings.getPhoneNumber().c_str());
			printf("DTE          - %s\n",commsSettings.getDte().c_str());
			printf("NUI          - %s\n",commsSettings.getNui().c_str());
			break;
		case CCommsSettings::QUICKCONNECT:
			printf("Type         - QUICKCONNECT\n");
			printf("Phone Number - %s\n",commsSettings.getPhoneNumber().c_str());
			printf("DTE          - %s\n",commsSettings.getDte().c_str());
			printf("NUI          - %s\n",commsSettings.getNui().c_str());
			break;
		case CCommsSettings::RADIOPAD:
			printf("Type         - RADIOPAD\n");
			printf("DTE          - %s\n",commsSettings.getDte().c_str());
			break;
		default:
			printf("Unknown Type\n");
			break;
	}

	ret = commsConfig.getSecParamCommsSettings(commsSettings);
	if (ret != DB_OK)
		printf("Could not retrieve comms settings for sec parms\n");

	printf("\n");
	printf("SECONDARY PARAM COMMS\n");
	switch (commsSettings.getCommsType()) {
		case CCommsSettings::ETHERNET:
			printf("Type         - ETHERNET\n");
			printf("IP           - %s\n",commsSettings.getIp().c_str());
			printf("Port         - %s\n",commsSettings.getPort().c_str());
			break;
		case CCommsSettings::GPRS:
			printf("Type         - GPRS\n");
			printf("IP           - %s\n",commsSettings.getIp().c_str());
			printf("Port         - %s\n",commsSettings.getPort().c_str());
			printf("APN          - %s\n",commsSettings.getApn().c_str());
			break;
		case CCommsSettings::SPEEDLINK:
			printf("Type         - SPEEDLINK\n");
			printf("Phone Number - %s\n",commsSettings.getPhoneNumber().c_str());
			printf("DTE          - %s\n",commsSettings.getDte().c_str());
			printf("NUI          - %s\n",commsSettings.getNui().c_str());
			break;
		case CCommsSettings::QUICKCONNECT:
			printf("Type         - QUICKCONNECT\n");
			printf("Phone Number - %s\n",commsSettings.getPhoneNumber().c_str());
			printf("DTE          - %s\n",commsSettings.getDte().c_str());
			printf("NUI          - %s\n",commsSettings.getNui().c_str());
			break;
		case CCommsSettings::RADIOPAD:
			printf("Type         - RADIOPAD\n");
			printf("DTE          - %s\n",commsSettings.getDte().c_str());
			break;
		default:
			printf("Unknown Type\n");
			break;
	}
	printf("\n");

}

void TestVelocity() {
	printf("----------------------Velocity---------------------------\n");
	CVelocityCheck velocityCheck;
	CPanInfo panInfo;

	// check a pan
	panInfo.setPan("123456789");
	int ret = velocityCheck.checkPan(panInfo);
	if (ret != DB_OK)
		printf("Pan not present!!!\n");
	else {
		printf("Found pan!!!\n");
		printf("   %s\n",panInfo.getPan().c_str());
		printf("   %d\n",panInfo.getCounter());
		printf("   %lu\n",panInfo.getTimeStamp());
		printf("   %lu\n",panInfo.getLastUsedTimestamp());
	}

	printf("\n");

	// Add a pan
	panInfo.setPan("123456789");
	panInfo.setCounter(1);
	panInfo.setTimeStamp(21232312);
	ret = velocityCheck.insertPan(panInfo);
	if (ret != DB_OK)
		printf("Error adding pan to velocity table\n!!!");
	else
		printf("Successfully added pan to velocity table\n");

	printf("\n");

	// check the pan
	panInfo.setPan("123456789");
	ret = velocityCheck.checkPan(panInfo);
	if (ret != DB_OK)
		printf("Pan not present\n!!!");
	else {
		printf("Found pan!!!\n");
		printf("PAN                 - %s\n",panInfo.getPan().c_str());
		printf("Counter             - %d\n",panInfo.getCounter());
		printf("Timestamp           - %lu\n",panInfo.getTimeStamp());
		printf("Lat Used Timestamp  - %lu\n",panInfo.getLastUsedTimestamp());
	}
		printf("\n");

	// UPDATE THE PAN

	panInfo.setCounter(panInfo.getCounter()+1);
	panInfo.setLastUsedTimestamp(1213123);
	ret = velocityCheck.updatePan(panInfo);
	if (ret != DB_OK)
		printf("Error updating velocity table\n!!!");
	else
		printf("Successfully updated velocity table\n!!!");


	// REMOVE PANS FROM VELOCITY TABLE
	std::map<std::string,CPanInfo> pans; // pan,CInfo
	ret = velocityCheck.getAllPans(pans);
	if (pans.size() > 0)
			printf("-------- All pans-------- \n");
	std::map<std::string,CPanInfo>::iterator it;
	for (it=pans.begin(); it!=pans.end(); ++it) {
		CPanInfo panInfo = it->second;
		printf("PAN                 - %s\n",panInfo.getPan().c_str());
		printf("Counter             - %d\n",panInfo.getCounter());
		printf("Timestamp           - %lu\n",panInfo.getTimeStamp());
		printf("Lat Used Timestamp  - %lu\n",panInfo.getLastUsedTimestamp());

		ret = velocityCheck.removePan(panInfo.getPan());
		if (ret != DB_OK)
			printf("Error removing pan from velocity table\n!!!");
		else
			printf("Successfully removed pan from velocity table\n!!!");
	}
}

void TestReceiptNo() {
	printf("----------------------ReceiptNo---------------------------\n");

	CReceiptNo receiptNo;

	printf("Current ReceiptNo  - %d\n",receiptNo.getCurrentReceiptNo());
	printf("Next ReceiptNo     - %d\n",receiptNo.getNextReceiptNo());
	printf("Next ReceiptNo     - %d\n",receiptNo.getNextReceiptNo());

	printf(" \n");
	printf("Reset Receipt No\n");
	printf(" \n");
	// rest tsn
	receiptNo.resetReceiptNo(1);
	printf("Current ReceiptNo  - %d\n",receiptNo.getCurrentReceiptNo());
	printf("Next ReceiptNo     - %d\n",receiptNo.getNextReceiptNo());
	printf("Next ReceiptNo     - %d\n",receiptNo.getNextReceiptNo());
	printf("\n");
}

void TestTsn() {
	printf("----------------------TSN---------------------------\n");

	CTsn tsn;

	printf("Current TSN  - %d\n",tsn.getCurrentTsn());
	printf("Next TSN     - %d\n",tsn.getNextTsn());
	printf("Next TSN     - %d\n",tsn.getNextTsn());

	// rest tsn
	tsn.resetTsn(1);
	printf("Current TSN  - %d\n",tsn.getCurrentTsn());
	printf("Next TSN     - %d\n",tsn.getNextTsn());
	printf("Next TSN     - %d\n",tsn.getNextTsn());
	printf("\n");
}

void TestUsers() {
	printf("-------------------------USERS-----------------------\n");

	CUserManager userManager;
	CUser user;

	user.setName("leon");
	user.setId("1000");
	user.setPin("1234");
	user.setBankingAllowed(true);
	user.setPinRequired(true);
	user.setRefundsAllowed(false);
	user.setUserType(CUser::CASHIER);

	int ret = userManager.addUser(user);
	if (ret == DB_OK)
		printf("User successfully added\n");
	else
		printf("Failed adding user\n");

	// check pin
	ret= userManager.verifyPinById("1000","1234");
	if (ret == DB_OK)
			printf("Pin correct\n");
		else
			printf("Pin incorrect\n");

	ret = userManager.getUserById("1000",user);
	if (ret == DB_OK) {
		printf("Successfully retrieved user by id\n");
		printf("User name        - %s\n",user.getName().c_str());
		printf("User id          - %s\n",user.getId().c_str());
		printf("Pin              - %s\n",user.getPin().c_str());
		printf("Type             - %d\n",user.getUserType());
		printf("Refunds Allowed  - %s\n",user.isRefundsAllowed()?"true":"false");
		printf("Pin required     - %s\n",user.isPinRequired()?"true":"false");
		printf("Banking Allowed  - %s\n",user.isBankingAllowed()?"true":"false");
		printf("Date Added       - %s\n\n",user.getDateAdded().c_str());
	} else
		printf("Failed retrieving user by id\n");

	user.setUserType(CUser::MANAGER);
	ret = userManager.updateUser(user);
	if (ret == DB_OK)
		printf("Successfully updated user\n\n");

	ret = userManager.getUserByName("leon",user);
	if (ret == DB_OK) {
		printf("Successfully retrieved user by name\n");
		printf("User name        - %s\n",user.getName().c_str());
		printf("User id          - %s\n",user.getId().c_str());
		printf("Pin              - %s\n",user.getPin().c_str());
		printf("Type             - %d\n",user.getUserType());
		printf("Refunds Allowed  - %s\n",user.isRefundsAllowed()?"true":"false");
		printf("Pin required     - %s\n",user.isPinRequired()?"true":"false");
		printf("Banking Allowed  - %s\n",user.isBankingAllowed()?"true":"false");
		printf("Date Added       - %s\n\n",user.getDateAdded().c_str());
	} else
		printf("Failed retrieving user by name\n");


	userManager.deleteUserById("1000");
	if (ret == DB_OK)
		printf("Successfully deleted user by id\n\n");
	else
		printf("Failed deleting user by id\n\n");

	ret = userManager.getUserByName("Leon",user);
	if (ret == DB_OK)
		printf("Successfully retrieved user by name\n\n");
	else
		printf("User does not exist\n\n");

	user.setName("leon");
	user.setId("1000");
	user.setPin("1234");
	user.setBankingAllowed(true);
	user.setPinRequired(true);
	user.setRefundsAllowed(false);
	user.setUserType(CUser::SUPERVISOR);

	ret = userManager.addUser(user);
	if (ret == DB_OK)
		printf("User successfully added\n");

	user.setName("dieter");
	user.setId("2000");
	user.setPin("56789");
	user.setBankingAllowed(false);
	user.setPinRequired(true);
	user.setRefundsAllowed(false);
	user.setUserType(CUser::MANAGER);

	ret = userManager.addUser(user);
	if (ret == DB_OK)
		printf("User successfully added\n");


	std::map<std::string,CUser> users; // id,Cuser
	ret = userManager.getAllUsers(users);
	if (users.size() > 0)
			printf("-------- All users-------- \n");
	std::map<std::string,CUser>::iterator it;
	for (it=users.begin(); it!=users.end(); ++it) {
		CUser user = it->second;
		printf("User name        - %s\n",user.getName().c_str());
		printf("User id          - %s\n",user.getId().c_str());
		printf("Pin              - %s\n",user.getPin().c_str());
		printf("Type             - %d\n",user.getUserType());
		printf("Refunds Allowed  - %s\n",user.isRefundsAllowed()?"true":"false");
		printf("Pin required     - %s\n",user.isPinRequired()?"true":"false");
		printf("Banking Allowed  - %s\n",user.isBankingAllowed()?"true":"false");
		printf("Date Added       - %s\n\n",user.getDateAdded().c_str());
	}


	users.clear();
	ret = userManager.getAllManagers(users);
	if (users.size() > 0)
			printf("-------- All managers-------- \n");
	for (it=users.begin(); it!=users.end(); ++it) {
		CUser user = it->second;
		printf("User name        - %s\n",user.getName().c_str());
		printf("User id          - %s\n",user.getId().c_str());
		printf("Pin              - %s\n",user.getPin().c_str());
		printf("Type             - %d\n",user.getUserType());
		printf("Refunds Allowed  - %s\n",user.isRefundsAllowed()?"true":"false");
		printf("Pin required     - %s\n",user.isPinRequired()?"true":"false");
		printf("Banking Allowed  - %s\n",user.isBankingAllowed()?"true":"false");
		printf("Date Added       - %s\n\n",user.getDateAdded().c_str());
	}


	// or you can get a specific user by id
	user = users["2000"];

	users.clear();
	ret = userManager.getAllSupervisors(users);
	if (users.size() > 0)
		printf("-------- All supervisors-------- \n");
	for (it=users.begin(); it!=users.end(); ++it) {
		CUser user = it->second;
		printf("User name        - %s\n",user.getName().c_str());
		printf("User id          - %s\n",user.getId().c_str());
		printf("Pin              - %s\n",user.getPin().c_str());
		printf("Type             - %d\n",user.getUserType());
		printf("Refunds Allowed  - %s\n",user.isRefundsAllowed()?"true":"false");
		printf("Pin required     - %s\n",user.isPinRequired()?"true":"false");
		printf("Baniking Allowed - %s\n",user.isBankingAllowed()?"true":"false");
		printf("Date Added       - %s\n\n",user.getDateAdded().c_str());
	}


	users.clear();
	userManager.getAllCashiers(users);
	if (users.size() > 0)
		printf("-------- All cashiers ----------\n");
	for (it=users.begin(); it!=users.end(); ++it) {
		CUser user = it->second;
		printf("User name        - %s\n",user.getName().c_str());
		printf("User id          - %s\n",user.getId().c_str());
		printf("Pin              - %s\n",user.getPin().c_str());
		printf("Type             - %d\n",user.getUserType());
		printf("Refunds Allowed  - %s\n",user.isRefundsAllowed()?"true":"false");
		printf("Pin required     - %s\n",user.isPinRequired()?"true":"false");
		printf("Banking Allowed  - %s\n",user.isBankingAllowed()?"true":"false");
		printf("Date Added       - %s\n\n",user.getDateAdded().c_str());
	}
}


void TestTerminalParameters()
{
	printf("-------------TERMINAL PARAMETERS---------------------\n");

	CTerminalConfig terminalConfig;


	// Import cofig from file
	terminalConfig.importTerminalConfig(TERM_PARM_PATH,ISO);

	printf("Batch max                - %d\n",terminalConfig.getBatchMax());
	printf("Connection timeout       - %d\n",terminalConfig.getConnectTimeout());
	printf("Currency Code            - %s\n",terminalConfig.getCurrencyCode().c_str());
	printf("Currency Symbol          - %s\n",terminalConfig.getCurrencySymbol().c_str());
	printf("Dial Type                - %s\n",terminalConfig.getDailType().c_str());
	printf("Logical Terminal Id      - %s\n",terminalConfig.getLogicalTermId().c_str());
	printf("Max Settle Conn Attempts - %d\n",terminalConfig.getMaxSettleConnectionAttempts());
	printf("Menu Type                - %s\n",terminalConfig.getMenuType().c_str());
	printf("Merchant Cat             - %s\n",terminalConfig.getMerchantCat().c_str());
	printf("Merchant Name            - %s\n",terminalConfig.getMerchantName().c_str());
	printf("Merchant No              - %s\n",terminalConfig.getMerchantNo().c_str());
	printf("Oil Max                  - %lu\n",terminalConfig.getOilMax());
	printf("Param Dwnld Time         - %s\n",terminalConfig.getParamDnldTime().c_str());
	printf("Response Timeout         - %d\n",terminalConfig.getResponseTimeout());
	printf("Session Key              - %s\n",terminalConfig.getSessionKey().c_str());
	printf("Settlement Time          - %s\n",terminalConfig.getSettlementTime().c_str());
	printf("Spdh Terminal Id         - %s\n",terminalConfig.getSpdhTerminalId().c_str());
	printf("Switchboard Code         - %s\n",terminalConfig.getSwitchBoardCode().c_str());
	printf("Terminal Name            - %s\n",terminalConfig.getTerminalName().c_str());
	printf("Terminal No              - %s\n",terminalConfig.getTerminalNo().c_str());
	printf("Trailer text             - %s\n",terminalConfig.getTrailerText().c_str());
	printf("Velocity Cnt             - %d\n",terminalConfig.getVelocityCnt());
	printf("Velocity Period          - %d\n",terminalConfig.getVelocityPeriod());
	printf("Wait Time                - %d\n",terminalConfig.getWaitTime());
	//printf("Last Batch No            - %d\n",terminalConfig.getLastBatchNo());
	printf("EMV Capabilities         - %s\n",terminalConfig.getEmvCapabilityBitMap().c_str());
	printf("Terminal Function Bitmap - %s\n",terminalConfig.getTerminalFunctionBitMap().c_str());
	printf("Terminal Menu Bitmap     - %s\n",terminalConfig.getTerminalMenuBitMap().c_str());
	printf("Allow Cheque Verif       - %s\n",terminalConfig.isAllowChequeVerification()?"true":"false");
	printf("Allow Manual Banking     - %s\n",terminalConfig.isAllowManualBanking()?"true":"false");
	printf("Allow Manual Pan         - %s\n",terminalConfig.isAllowmanualPan()?"true":"false");
	printf("Attendants Enabled       - %s\n",terminalConfig.isAttendantsEnabled()?"true":"false");
	printf("Message To Merchant      - %s\n",terminalConfig.getMessageToMerchant().c_str());
	printf("NUI                      - %s\n",terminalConfig.getNUI().c_str());

	printf("--------------------------------------------------\n\n\n");

}
