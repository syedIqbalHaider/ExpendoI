#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <libpml/pml.h>
#include <libpml/pml_abstracted_api.h>
#include <libpml/pml_port.h>
#include <libipc/ipc.h>

#define MY_TASK_NAME "TESTAPP"
#define OTHER_TASK_NAME "CPAM"

using namespace std;

int main()
{
	com_verifone_pml::eventset_t tPmlEvents;
	string sFrom;
	string sInBuffer;
	int iEventCount;
	int hEvent;
	int iRc;

	// set working directory (VOS only)
	com_verifone_pml::set_working_directory(com_verifone_pml::getRAMRoot().c_str());

	// init ipc
	iRc=com_verifone_ipc::init(string(MY_TASK_NAME));
	if(iRc<0) return(-1);

	// init pml
	iRc=com_verifone_pml::pml_init(MY_TASK_NAME);
	if(iRc<0) return(-2);

	// init event handler
	hEvent=com_verifone_pml::event_open();
	if(hEvent<0) return(-3);

	// add ipc event
	com_verifone_pml::event_ctl(hEvent,com_verifone_pml::ctl::ADD,com_verifone_ipc::get_events_handler());

	// connect to other task
	printf("Connecting to %s\n",OTHER_TASK_NAME);
	while(!com_verifone_ipc::is_task_available(string(OTHER_TASK_NAME))) sleep(1);
	printf("Available\n");
	iRc=com_verifone_ipc::connect_to(string(OTHER_TASK_NAME));
	printf("connect_to=%d\n",iRc);

	// transaction status
	iRc=com_verifone_ipc::send(string("\xdf\xaf\x01\x01\x71"),string(OTHER_TASK_NAME));
	// start transaction request
//	iRc=com_verifone_ipc::send(string("\xdf\xaf\x01\x01\x21\xdf\xaf\x02\x01\x3c\xdf\xaf\x03\x01\x21\xff\xcc\x10\x17\x9c\x01\x00\x9f\x02\x06\x00\x00\x00\x00\x02\x00\x9a\x03\x14\x01\x03\x9f\x21\x03\x08\x33\x00\xdf\xcc\x11\x06\x4f\x5a\x9f\x27\x9f\x34",52),string(OTHER_TASK_NAME));
	// modify limits response
//	iRc=com_verifone_ipc::send(string("\xdf\xaf\x01\x01\x23\xdf\xaf\x04\x01\x08"),string(OTHER_TASK_NAME));
	// remove card
//	iRc=com_verifone_ipc::send(string("\xdf\xaf\x01\x01\x25"),string(OTHER_TASK_NAME));
	printf("send=%d\n",iRc);

	// main loop
	while(1) {
		iEventCount=com_verifone_pml::event_wait(hEvent,tPmlEvents);
		if(iEventCount>0||tPmlEvents.size()>0) {
			while(!tPmlEvents.empty()) {
				com_verifone_pml::event_item tEventItem=tPmlEvents.back();
				tPmlEvents.pop_back();
				if(tEventItem.event_id==com_verifone_pml::events::ipc) {
					while(com_verifone_ipc::check_pending(string(""))==com_verifone_ipc::IPC_SUCCESS) {
						com_verifone_ipc::receive(sInBuffer,string(""),sFrom);
						printf("IPC from %s:\n",sFrom.c_str());
						for(int i=0;i<sInBuffer.length();i++) printf("%02X",sInBuffer.at(i));
						printf("\n");
						// process sInBuffer here
						if(sInBuffer.at(0)==0x5b) {
							iRc=com_verifone_ipc::send(string("\xC8\xDF\xCC\x56\x00\x00\x00\x07\xDF\xCC\x60\x00\x00\x00\x00",15),string(OTHER_TASK_NAME));
							printf("send3=%d\n",iRc);
						}
					}
				}
			}
		}
	}

	// we should never get here
	return(0);
}
