/*
 * CUpload.h
 *
 *  Created on: Dec 10, 2013
 *      Author: dietero2Verifone.com
 */

#ifndef CUPLOAD_H_
#define CUPLOAD_H_

#include <string>
#include <curl/curl.h>
#include <stdio.h>

//using namespace std;
using std::string;

namespace com_verifone_Upload
{
	class CUpload
	{
	public:
		CUpload():curl_multi_handle(NULL){}
		CUpload(CURLM *multi_handle, string server_url, string root);
		~CUpload();

		int performVersionsFileUpload(void);

	private:
		CURLM *curl_multi_handle;
		CURL *curl;
		string url;
		string root;

		int file_list(FILE *fh, string dir);
		int build_versions_file(void);
		string get_serial(void);
	};
}

#endif /* CUPLOAD_H_ */
