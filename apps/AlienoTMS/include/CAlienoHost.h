/*
 * CAlienoHost.h
 *
 *  Created on: Dec 10, 2013
 *      Author: dietero2@verifone.com
 */

#ifndef CALIENOHOST_H_
#define CALIENOHOST_H_

#include <CDownload.h>
#include <CUpload.h>
#include <curl/curl.h>
#include <libminini/minIni.h>
#include <string>

using namespace com_verifone_Download;
using namespace com_verifone_Upload;
//using namespace std;

namespace com_verifone_AlienoHost
{
	class CAlienoHost
	{
	public:
		CAlienoHost();
		~CAlienoHost();

		int process(void);

	private:
		CDownload download;
		CUpload upload;

		CURLM *curl_multi_H;
		CURLcode curl_ret;

		string url;
		string username;
		string password;
		string rootpath;

		int setup_host_params(void);
		int setup_user_params(void);
	};
}

#endif /* CALIENOHOST_H_ */
