/*
 * CDownload.h
 *
 *  Created on: Dec 10, 2013
 *      Author: vfisdk
 */

#ifndef CDOWNLOAD_H_
#define CDOWNLOAD_H_

#include <stdio.h>
#include <list>
#include <string>
#include <curl/curl.h>

//using namespace std;
using std::string;

namespace com_verifone_Download
{
	typedef struct{
		char filename[255];
		char abs_filename[255]; // filename without path
		FILE *fh;
		CURL *curl;
		int flag_to_dl_path;
	}DL_FILE_PARAMATERS;

	class CDownload
	{
	public:
		CDownload():curl_multi_handle(NULL){}
		CDownload(CURLM *multi_handle, string server_url);
		~CDownload();

		int performDownload(void);

	private:
		CURLM *curl_multi_handle;
		string url;

		std::list<DL_FILE_PARAMATERS> files_list;

		int get_files_list(void);
		int process_files_list(void);
		int get_files(void);
		int add_file_to_multi_list(DL_FILE_PARAMATERS *file_params);
		int install_software(void);
		string get_serial(void);
		string get_date(void);
	};
}

#endif /* CDOWNLOAD_H_ */
