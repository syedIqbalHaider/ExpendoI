#ifndef _ALIENOTMS_H_
#define _ALIENOTMS_H_

#ifdef _VOS_
	#define ALIENO_INI	"flash/alieno.ini"
#else
	#define ALIENO_INI	"alieno.ini"
#endif // _VOS_

#endif // _ALIENOTMS_H_
