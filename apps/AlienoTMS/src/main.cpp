/*
 * main.cpp
 *
 *  Created on: Dec 4, 2013
 *      Author: dietero2@verifone.com
 */
#include <CAlienoHost.h>
#include <string.h>

#include "CLog.h"
#include "AlienoTMSver.h"

using std::cout;
using std::endl;


using namespace com_verifone_AlienoHost;

int main(int argc, char *argv[])
{
	if((argc == 2) && (strcmp(argv[1], "-v") == 0)){
		cout << MAPP_VERSION << endl;
		return 0;
	}

	CAlienoHost alieno_host;

	alieno_host.process();

	return 0;
}
