/*
 * CLog.cpp
 *
 *  Created on: Dec 4, 2013
 *      Author: dietero2@verifone.com
 */

/*
 * This class is meant to abstract logging functionality into a central place where the final logging mechanism can be switched between
 * whichever medium is relevant on the platform.
 */

#include <iostream>
#include <string>
#include <sstream>
#include "CLog.h"

//using namespace std;

void CLog::message(string message)
{
#ifdef DEBUG
	cout << message;
#else
	(void)message;	// Suppresses -> warning: unused parameter
#endif
}

void CLog::uimsg(string message)
{
	cout << message;
	cout.flush();
}

string CLog::from_int(int x)
{
	stringstream ss;
	ss << x;
	return ss.str();
}


