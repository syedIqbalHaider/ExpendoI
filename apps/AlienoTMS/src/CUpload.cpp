/*
 * CUpload.cpp
 *
 *  Created on: Dec 10, 2013
 *      Author: dietero2@verifone.com
 */
#include <sys/stat.h>
#include <errno.h>
#include <AlienoTMSver.h>
#include <CUpload.h>
#include <CLog.h>
#include <cstring>
#include <syslog.h>
#include <boost/filesystem.hpp>
#include <sstream>

extern "C" {
#ifdef _VOS_
	#include <platforminfo_api.h>
#endif // _VOS_
}

#define ALIENO_VERSIONS_FILE_LOCAL	"versions_full.txt"
#define ALIENO_VERSIONS_FILE_RMT	"versions_full_on_server.txt"

/*
#ifdef DEBUG
	#define CURL_VERBOSITY				1L
#else
	#define CURL_VERBOSITY				0L
#endif // DEBUG
*/
#define CURL_VERBOSITY				1L	// Leave debug messages on

#define MAX_WAIT_MSECS				500

#ifndef SSTR
	#define SSTR( x ) dynamic_cast< std::ostringstream & >( std::ostringstream() << std::dec << x ).str()
#endif

static CLog log;

//using namespace std;
using namespace boost::filesystem;

namespace com_verifone_Upload
{
	typedef struct{
		char *filename;
		FILE *fh;
	}UL_FILE_PARAMATERS;

	static size_t read_callback(void *ptr, size_t size, size_t nmemb, void *stream);

	CUpload::CUpload(CURLM *multi_handle, string server_url, string root){
		curl_multi_handle = multi_handle;
		url = server_url;
		CUpload::root = root;

		if(CUpload::root.length() > 0)
			CUpload::root.append("/");

		openlog(MAPP_NAME, LOG_PID|LOG_NOWAIT, LOG_USER);
		syslog(LOG_INFO, "Iniating upload.");

		log.uimsg("Checking current versions.\n");
	}

	CUpload::~CUpload()
	{
/*
		if(curl)
			curl_easy_cleanup(curl);
*/
		syslog(LOG_INFO, "Upload complete");
//		log.uimsg("Version checking complete.\n");
		closelog();
	}

	int CUpload::performVersionsFileUpload(void)
	{
		struct stat file_info;
		int still_running=0, res, retval=0;
		UL_FILE_PARAMATERS file_params;

		curl = NULL;

		build_versions_file();

		if(stat(ALIENO_VERSIONS_FILE_LOCAL, &file_info)) {
			log.uimsg("Upload failed - could not open "ALIENO_VERSIONS_FILE_LOCAL". Reason: "+string(strerror(errno))+"\n");
			syslog(LOG_INFO, "Upload failed - could not open "ALIENO_VERSIONS_FILE_LOCAL". Reason: %s", strerror(errno));
			return -1;
		}

		curl = curl_easy_init();

		if(curl){
			curl_easy_setopt(curl, CURLOPT_FTP_CREATE_MISSING_DIRS, 1L);
			curl_easy_setopt(curl, CURLOPT_READFUNCTION, read_callback);	// Setup the read callback
			curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);						// Enable uploading

			string remote_file = root + get_serial() + "/" ALIENO_VERSIONS_FILE_LOCAL;
			url.append(remote_file);

			curl_easy_setopt(curl, CURLOPT_URL, url.c_str());				// Specify server URL
			curl_easy_setopt(curl, CURLOPT_VERBOSE, CURL_VERBOSITY);		// Talk to me!

			file_params.filename = (char*)ALIENO_VERSIONS_FILE_LOCAL;
			file_params.fh = NULL;
			curl_easy_setopt(curl, CURLOPT_READDATA, &file_params);				// Provide file parameters to be used by read callback

			//log.uimsg("Uploading " + std::string(file_params.filename) + "...\n");
			log.uimsg("Uploading " + url + "...\n");

			res = curl_multi_add_handle(curl_multi_handle, curl);
			//log.uimsg("After res=" + SSTR(res)+ "\n");
			if(res != CURLM_OK) {
				log.message(MSG_INFO "error: curl_multi_add_handle() returned error:" + SSTR(res) + "\n");
				retval = -1;
				goto CLEANUP;
			}

			do {
				int numfds=0;

				res = curl_multi_wait(curl_multi_handle, NULL, 0, MAX_WAIT_MSECS, &numfds);
				//log.uimsg("After 2 res=" + SSTR(res) + "\n");

				if(res != CURLM_OK) {
					log.message(MSG_INFO "error: curl_multi_wait() returned error\n");
					retval = -1;
					goto CLEANUP;
				}

				res = curl_multi_perform(curl_multi_handle, &still_running);
				//log.uimsg("After 3 res=" + SSTR(res)+ "\n");

				if(res != CURLM_OK) {
					log.message(MSG_INFO "error: curl_multi_perform() returned error\n");
					retval = -1;
					goto CLEANUP;
				}
			} while(still_running);

			CURLMsg *curl_msg;
			int msg_count=0;

			curl_msg = curl_multi_info_read(curl_multi_handle, &msg_count);

			if(curl_msg){
				if((curl_msg->msg == CURLMSG_DONE) && (curl_msg->data.result >= 200) && (curl_msg->data.result < 300)){//(curl_msg->data.result != CURLE_OK)){
					syslog(LOG_INFO, "Upload failed with %d\n", curl_msg->data.result);
					log.uimsg("Version checking failed!\n");
					retval = -1;
					goto CLEANUP;
				}else{
					log.uimsg("Upload complete\n");
					if (curl_msg->msg == CURLMSG_DONE)
						syslog(LOG_INFO, "Upload returned with code %d\n", curl_msg->data.result);
				}
			}
			else
				log.message("curl_msg is NULL\n");
		}
CLEANUP:
		curl_multi_remove_handle(curl_multi_handle, curl);
		curl_easy_cleanup(curl);

		if(file_params.fh)
			fclose(file_params.fh);

		return retval;
	}


	int CUpload::file_list(FILE *fh, string dir)
	{
		int ret=0;
		path pdir = dir;

		if(!exists(pdir))
			return -1;

		directory_iterator end_dir_iter;

		for(directory_iterator dir_iter(pdir); dir_iter != end_dir_iter; ++dir_iter){
			if(is_directory(dir_iter->status())){
				if((ret = file_list(fh, dir_iter->path().string())) < 0)
					return ret;
			}else{
				path pcurrent = dir_iter->path();
				string strpath = pcurrent.string();
				struct stat file_info;
				stringstream ss;

				if(stat(strpath.c_str(), &file_info) == 0){
					struct tm *currt = localtime(&file_info.st_mtime);

					ss << strpath;
					ss << ',';
					ss << static_cast<off_t>(file_info.st_size);
					ss << ',';
					ss << static_cast<unsigned int>(1900+currt->tm_year);
					if(currt->tm_mon+1 < 9) ss << '0';
					ss << static_cast<unsigned int>(currt->tm_mon+1);
					ss << static_cast<unsigned int>(currt->tm_mday);
					ss << static_cast<unsigned int>(currt->tm_hour);
					ss << static_cast<unsigned int>(currt->tm_min);
					ss << static_cast<unsigned int>(currt->tm_sec);
					ss << ';';

					fprintf(fh, "%s\r\n", ss.str().c_str());
				}else
					fprintf(fh, "Could not stat %s\r\n", strpath.c_str());
			}
		}

		return 0;
	}


	int CUpload::build_versions_file(void)
	{
		FILE *fh=NULL;

		fh = fopen(ALIENO_VERSIONS_FILE_LOCAL, "wb");

		if(fh == NULL){
			log.uimsg("%s(): Upload failed - could not open "ALIENO_VERSIONS_FILE_LOCAL". Reason: "+string(strerror(errno))+"\n");
			syslog(LOG_INFO, "%s(): Upload failed - could not open "ALIENO_VERSIONS_FILE_LOCAL". Reason: %s",__func__, strerror(errno));
		}

#ifdef _VOS_
		char value[255];
		int result;
		unsigned int ival=0;
		unsigned long rsize=0;

		result = platforminfo_get(PI_SERIAL_NUM ,value, sizeof(value), &rsize);
		if (result == PI_OK) fprintf(fh, "Serial Number: %s\r\n", value);

		fprintf(fh, "LSBEGIN\r\n");		// Indicate the beginning of the file listing
		file_list(fh, ".");
		fprintf(fh, "LSEND\r\n");		// Indicate the end of the file listing

		result = platforminfo_get(PI_VERSION_KERNEL ,value, sizeof(value), &rsize);
		if (result == PI_OK) fprintf(fh, "Kernel Version: %s\r\n", value);

		result = platforminfo_get(PI_VERSION_RFS ,value, sizeof(value), &rsize);
		if (result == PI_OK) fprintf(fh, "RFS: %s\r\n", value);

		result = platforminfo_get(PI_VERSION_SECURITY_RFS ,value, sizeof(value), &rsize);
		if (result == PI_OK) fprintf(fh, "Security RFS: %s\r\n", value);

		result = platforminfo_get(PI_VERSION_VAULT ,value, sizeof(value), &rsize);
		if (result == PI_OK) fprintf(fh, "Vault: %s\r\n", value);

		result = platforminfo_get(PI_VERSION_VAULT_PEDGUARD ,value, sizeof(value), &rsize);
		if (result == PI_OK) fprintf(fh, "Vault PedGuard: %s\r\n", value);

		result = platforminfo_get(PI_VERSION_SBI ,value, sizeof(value), &rsize);
		if (result == PI_OK) fprintf(fh, "SBI: %s\r\n", value);

		result = platforminfo_get(PI_VERSION_CIB ,value, sizeof(value), &rsize);
		if (result == PI_OK) fprintf(fh, "CBI: %s\r\n", value);

		result = platforminfo_get(PI_VERSION_MIB ,value, sizeof(value), &rsize);
		if (result == PI_OK) fprintf(fh, "MIB: %s\r\n", value);

		result = platforminfo_get(PI_VERSION_UBOOT ,value, sizeof(value), &rsize);
		if (result == PI_OK) fprintf(fh, "UBOOT: %s\r\n", value);

		result = platforminfo_get(PI_VERSION_RELEASE ,value, sizeof(value), &rsize);
		if (result == PI_OK) fprintf(fh, "Release: %s\r\n", value);

		result = platforminfo_get(PI_VERSION_PLATFORMINFO ,value, sizeof(value), &rsize);
		if (result == PI_OK) fprintf(fh, "Platform Info: %s\r\n", value);

		result = platforminfo_get(PI_VERSION_CONTACTLESS ,value, sizeof(value), &rsize);
		if (result == PI_OK) fprintf(fh, "Contactless: %s\r\n", value);

		result = platforminfo_get(PI_MAC_ADDRESS ,value, sizeof(value), &rsize);
		if (result == PI_OK) fprintf(fh, "MAC: %s\r\n", value);

		result = platforminfo_get(PI_MODEL_MX ,&ival, sizeof(ival), &rsize);
		if (result == PI_OK) fprintf(fh, "Model: %d\r\n", ival);

		result = platforminfo_get(PI_UNIT_ID ,value, sizeof(value), &rsize);
		if (result == PI_OK) fprintf(fh, "PTID: %s\r\n", value);

		result = platforminfo_get(PI_RAM_SIZE ,&ival, sizeof(ival), &rsize);
		if (result == PI_OK) fprintf(fh, "RAM Size: %d\r\n", ival);

		result = platforminfo_get(PI_FLASH_INFO ,value, sizeof(value), &rsize);
		if (result == PI_OK) fprintf(fh, "Flash Size: %s\r\n", value);

		PI_flash_info_st flash_info;
		result = platforminfo_get(PI_FLASH_INFO ,&flash_info, sizeof(PI_flash_info_st), &rsize);
		if (result == PI_OK){
			fprintf(fh, "Flash Total: %lu\r\n", flash_info.total);
			fprintf(fh, "Flash Used: %lu (%lu%%)\r\n", flash_info.used, flash_info.percent_used);
			fprintf(fh, "Flash Free: %lu\r\n", flash_info.free);
		}

		result = platforminfo_get(PI_IO_MODULE_CONFIG ,&ival, sizeof(ival), &rsize);
		if (result == PI_OK) fprintf(fh, "IO Module Config: %d\r\n", ival);
/*
		SecinsPkgInfo pkg_info;
		result = platforminfo_get(PI_PACKAGES_LIST_ENTRY ,&pkg_info, sizeof(SecinsPkgInfo), &rsize);
		if (result == PI_OK){
			fprintf(fh, "DBG sizeof=%d rsize=%d\r\n", sizeof(pkg_info), rsize);

			fprintf(fh, "Name: %s\r\n", pkg_info.name);
			fprintf(fh, "Version: %s\r\n", pkg_info.version);
			fprintf(fh, "User: %s\r\n", pkg_info.user);
			fprintf(fh, "Signer: %s\r\n", pkg_info.signer);
			fprintf(fh, "Type: %s\r\n", pkg_info.type);
			fprintf(fh, "Bundle: %s\r\n", pkg_info.bundlename);
			fprintf(fh, "Bundle Version: %s\r\n", pkg_info.bundleversion);
			fprintf(fh, "Bundle Date: %s\r\n", pkg_info.bundledate);
		}
*/
		PI_uart_matrix_st uart_matrix;
		result = platforminfo_get(PI_IO_MODULE_CONFIG ,&uart_matrix, sizeof(PI_uart_matrix_st), &rsize);
		if (result == PI_OK){
			fprintf(fh, "COM1 [%s]\r\n", uart_matrix.com1);
			fprintf(fh, "COM2 [%s]\r\n", uart_matrix.com2);
			fprintf(fh, "COM3 [%s]\r\n", uart_matrix.com3);
			fprintf(fh, "COM4 [%s]\r\n", uart_matrix.com4);
			fprintf(fh, "COM5 [%s]\r\n", uart_matrix.com5);
		}

		PI_display_info_st disp_info;
		result = platforminfo_get(PI_DISPLAY_INFO ,&disp_info, sizeof(PI_display_info_st), &rsize);
		if (result == PI_OK){
			fprintf(fh, "Pixels per Row: %d\r\n", disp_info.pixels_per_row);
			fprintf(fh, "Pixels per Col: %d\r\n", disp_info.pixels_per_col);
			fprintf(fh, "Type: %d\r\n", disp_info.type);
			fprintf(fh, "Name: %s\r\n", disp_info.name);
			fprintf(fh, "Interface Type: %d\r\n", disp_info.interface_type);
		}

		PI_external_battery_info_st ext_batt_info;
		result = platforminfo_get(PI_EXTERNAL_BATTERY_INFO ,&ival, sizeof(ival), &rsize);
		if (result == PI_OK){
			fprintf(fh, "Voltage: %lu\r\n", ext_batt_info.voltage);
			fprintf(fh, "Status: %lu\r\n", ext_batt_info.status);
		}

		result = platforminfo_get(PI_VERSION_SRED ,value, sizeof(value), &rsize);
		if (result == PI_OK) fprintf(fh, "SRED Version: %s\r\n", value);

		result = platforminfo_get(PI_VERSION_OPENPROTOCOL ,value, sizeof(value), &rsize);
		if (result == PI_OK) fprintf(fh, "Open Protocol Version: %s\r\n", value);

		result = platforminfo_get(PI_WIFI_MAC_ADDRESS ,value, sizeof(value), &rsize);
		if (result == PI_OK) fprintf(fh, "Wifi MAC: %s\r\n", value);

		result = platforminfo_get(PI_BT_MAC_ADDRESS ,value, sizeof(value), &rsize);
		if (result == PI_OK) fprintf(fh, "Bluetooth MAC: %s\r\n", value);

		result = platforminfo_get(PI_DEVICE_SVID ,&ival, sizeof(ival), &rsize);
		if (result == PI_OK) fprintf(fh, "Device SVID: %d\r\n", ival);

		result = platforminfo_get(PI_VERSION_VHQ ,value, sizeof(value), &rsize);
		if (result == PI_OK) fprintf(fh, "VHQ Version: %s\r\n", value);

		result = platforminfo_get(PI_CIB_PRODUCT_NUMBER_STR ,value, sizeof(value), &rsize);
		if (result == PI_OK) fprintf(fh, "CIB Product No: %s\r\n", value);

		result = platforminfo_get(PI_MIB_PART_NUMBER ,value, sizeof(value), &rsize);
		if (result == PI_OK) fprintf(fh, "MIB Part No: %s\r\n", value);

		result = platforminfo_get(PI_MIB_MODEL_NUM ,value, sizeof(value), &rsize);
		if (result == PI_OK) fprintf(fh, "MIB Model No: %s\r\n", value);

		result = platforminfo_get(PI_MIB_COUNTRY ,value, sizeof(value), &rsize);
		if (result == PI_OK) fprintf(fh, "MIB Country: %s\r\n", value);

		PI_ethernet_info_st eth_info;
		result = platforminfo_get(PI_ETHERNET_INFO , &eth_info, sizeof(PI_ethernet_info_st), &rsize);
		if (result == PI_OK){
			fprintf(fh, "Ethernet Exists: %d\r\n", eth_info.exist);
			fprintf(fh, "Ethernet Type: %d\r\n", eth_info.type);
			fprintf(fh, "Ethernet Link Up: %d\r\n", eth_info.link_up);
			fprintf(fh, "Ethernet MAC: %s\r\n", eth_info.mac);
		}

		PI_printer_info_st prn_info;
		result = platforminfo_get(PI_PRINTER_INFO ,&prn_info, sizeof(PI_printer_info_st), &rsize);
		if (result == PI_OK){
			fprintf(fh, "Printer Exists: %d\r\n", prn_info.exist);
			fprintf(fh, "Printer Type: %d\r\n", prn_info.type);
		}

		PI_modem_info_st modem_info;
		result = platforminfo_get(PI_PRINTER_INFO ,&modem_info, sizeof(PI_modem_info_st), &rsize);
		if (result == PI_OK){
			fprintf(fh, "Modem Exists: %d\r\n", modem_info.exist);
			fprintf(fh, "Modem Type: %d\r\n", modem_info.type);
			fprintf(fh, "Modem port: %d\r\n", modem_info.port);
		}

		PI_msr_info_st msr_info;
		result = platforminfo_get(PI_PRINTER_INFO ,&msr_info, sizeof(PI_msr_info_st), &rsize);
		if (result == PI_OK){
			fprintf(fh, "MSR Exists: %d\r\n", msr_info.exist);
			fprintf(fh, "MSR Type: %d\r\n", msr_info.type);
			fprintf(fh, "MSR port: %d\r\n", msr_info.port);
		}

		PI_ctls_info_st ctls_info;
		result = platforminfo_get(PI_CTLS_INFO ,&ctls_info, sizeof(PI_ctls_info_st), &rsize);
		if (result == PI_OK){
			fprintf(fh, "CTLS Exists: %d\r\n", ctls_info.exist);
			fprintf(fh, "CTLS Type: %d\r\n", ctls_info.type);
			fprintf(fh, "CTLS port: %d\r\n", ctls_info.port);
			fprintf(fh, "CTLS cfg ID: %d\r\n", ctls_info.config_id);
		}

		PI_smart_card_info_st sc_info;
		result = platforminfo_get(PI_SMART_CARD_INFO ,&sc_info, sizeof(PI_smart_card_info_st), &rsize);
		if (result == PI_OK){
			fprintf(fh, "Smartcard Exists: %d\r\n", sc_info.exist);
			fprintf(fh, "Smartcard Type: %d\r\n", sc_info.type);
			fprintf(fh, "Smartcard port: %d\r\n", sc_info.port);
		}

		PI_wrenchman_info_st wr_info;
		result = platforminfo_get(PI_WRENCHMAN_INFO ,&wr_info, sizeof(PI_wrenchman_info_st), &rsize);
		if (result == PI_OK){
			fprintf(fh, "Wrenchman Exists: %d\r\n", wr_info.exist);
			fprintf(fh, "Wrenchman Type: %d\r\n", wr_info.type);
			fprintf(fh, "Wrenchman port: %d\r\n", wr_info.port);
		}

		result = platforminfo_get(PI_DEVICE_CHIP_ID ,value, sizeof(value), &rsize);
		if (result == PI_OK) fprintf(fh, "Device Chip ID: %s\r\n", value);

		PI_io_module_com_state_st io_comm_state;
		result = platforminfo_get(PI_IO_MODULE_COM_STATE ,&io_comm_state, sizeof(PI_io_module_com_state_st), &rsize);
		if (result == PI_OK){
			fprintf(fh, "COM1 Attached: %d\r\n", io_comm_state.com1_attached);
			fprintf(fh, "COM2 Attached: %d\r\n", io_comm_state.com2_attached);
			fprintf(fh, "COM3 Attached: %d\r\n", io_comm_state.com3_attached);
		}

		result = platforminfo_get(PI_CIB_MODEL_ID_STR ,value, sizeof(value), &rsize);
		if (result == PI_OK) fprintf(fh, "Model ID: %s\r\n", value);

		PI_eeproms_info_st eeprom_info;
		result = platforminfo_get(PI_EEPROMS_INFO ,&eeprom_info, sizeof(PI_eeproms_info_st), &rsize);
		if (result == PI_OK){
			PI_eeprom_data_st main_eeprom, io_eeprom;
			memcpy(&main_eeprom, &eeprom_info.eeprom[0], sizeof(PI_eeprom_data_st));
			memcpy(&io_eeprom, &eeprom_info.eeprom[1], sizeof(PI_eeprom_data_st));

			fprintf(fh, "Main Eeprom Type: %d\r\n", main_eeprom.type);
			fprintf(fh, "Main Eeprom Port: %d\r\n", main_eeprom.port);
			fprintf(fh, "Main Eeprom Address: %d\r\n", main_eeprom.address);
			fprintf(fh, "Main Eeprom Size: %d\r\n", main_eeprom.size);

			fprintf(fh, "IO Eeprom Type: %d\r\n", io_eeprom.type);
			fprintf(fh, "IO Eeprom Port: %d\r\n", io_eeprom.port);
			fprintf(fh, "IO Eeprom Address: %d\r\n", io_eeprom.address);
			fprintf(fh, "IO Eeprom Size: %d\r\n", io_eeprom.size);
		}

		PI_io_module_info_st io_module_info;
		result = platforminfo_get(PI_IO_MODULE_INFO ,&io_module_info, sizeof(PI_io_module_info_st), &rsize);
		if (result == PI_OK){
			fprintf(fh, "IO Module Type: %d\r\n", io_module_info.type);
			fprintf(fh, "IO Module Rev: %s\r\n", io_module_info.hwrv);
		}

		PI_sdio_info_st sd_info;
		result = platforminfo_get(PI_SDIO_INFO ,&sd_info, sizeof(PI_sdio_info_st), &rsize);
		if (result == PI_OK){
			fprintf(fh, "SD B Exists: %d\r\n", sd_info.sdio_b_exist);
			fprintf(fh, "SD A Detected: %d\r\n", sd_info.sdio_a_detected);
			fprintf(fh, "SD B Detected: %d\r\n", sd_info.sdio_b_detected);
		}

		PI_keypad_info_st keypad_info;
		result = platforminfo_get(PI_KEYPAD_INFO ,&keypad_info, sizeof(PI_keypad_info_st), &rsize);
		if (result == PI_OK){
			fprintf(fh, "Keypad Exists: %d\r\n", keypad_info.exist);
			fprintf(fh, "Keypad Type: %d\r\n", keypad_info.type);
			fprintf(fh, "Keypad Port: %d\r\n", keypad_info.port);
			fprintf(fh, "Keypad No Std Keys: %d\r\n", keypad_info.num_standard_keys);
			fprintf(fh, "Func Keypad Exists: %d\r\n", keypad_info.func_keypad_exist);
		}
#else
		fprintf(fh, "LSBEGIN\r\n");		// Indicate the beginning of the file listing
		file_list(fh, "./");
		fprintf(fh, "LSEND\r\n");		// Indicate the end of the file listing


		fprintf(fh, "No information available on this platform\r\n");
#endif // _VOS_

		fclose(fh);

		return 0;
	}

	string CUpload::get_serial(void){
#ifdef _VOS_
		char value[20];
		int result;
		unsigned long rsize=0;

		result = platforminfo_get(PI_SERIAL_NUM ,value, sizeof(value), &rsize);
		if (result == PI_OK){
			while((rsize >= 1) && (value[rsize-1] == ' ')) rsize--;		// Remove pesky trailing spaces
			return string(value, rsize);
		}
		else return string("NA");
#else
		return string("123-456-789");
#endif // _VOS_
	}

	static size_t read_callback(void *ptr, size_t size, size_t nmemb, void *stream)
	{
		UL_FILE_PARAMATERS *params = (UL_FILE_PARAMATERS*)stream;

		if(params->fh == NULL){
			log.message(MSG_INFO "read_callback(): Opening file "+string(params->filename)+"\n");
			params->fh = fopen(params->filename, "rb");
		}

		if(params->fh == NULL){
			log.message(MSG_INFO "read_callback(): File open failed\n");
			return CURL_READFUNC_ABORT;
		}

		size_t bytes_read = fread(ptr, size, nmemb, (FILE*)params->fh);

		// If we reached the end of file, then close the handle
		if(bytes_read <= 0){
			log.message(MSG_INFO "ftp_save_file() closing file "+string(params->filename)+"\n");
			fclose(params->fh);
			params->fh = NULL;
		}else{
			char str_size[100];
			sprintf(str_size, "%d", bytes_read);
			log.message(MSG_INFO "ftp_save_file() reading " + string(str_size) + " bytes\n");
		}

		return bytes_read;
	}
}
