/*
 * CDownload.cpp
 *
 *  Created on: Dec 10, 2013
 *      Author: dietero2@verifone.com
 */
#include <stdlib.h>
#include <AlienoTMSver.h>
#include <CDownload.h>
#include <CLog.h>
#include <cstring>
#include <syslog.h>
#include <sys/stat.h>
#include <boost/filesystem.hpp>
#include <ctime>
#include <sstream>

extern "C" {
#ifdef _VOS_
	#include <platforminfo_api.h>
#endif // _VOS_
}

#define USE_CURL_INTERNAL_FILE_WRITE

#define ALIENO_FILES_LIST_FILENAME		"files.txt"

#ifdef _VOS_
	#define DL_PATH		"/mnt/flash/install/dl/"
#else
	#define DL_PATH		"./"
#endif // _VOS_

#define CURL_VERBOSITY					1L

#define MAX_WAIT_MSECS					500

//using namespace std;
using namespace boost::filesystem;

static CLog log;

namespace com_verifone_Download
{
	static int progress_callback(void *clientp, double dltotal, double dlnow, double ultotal, double ulnow);

	CDownload::CDownload(CURLM *multi_handle, string server_url)
	{
		curl_multi_handle = multi_handle;
		url = server_url;

		openlog(MAPP_NAME, LOG_PID|LOG_NOWAIT, LOG_USER);
		log.uimsg("Initiating download.\n");
		std::string msg = "Initiating download from: " + server_url;
		syslog(LOG_INFO, msg.c_str());
	}

	CDownload::~CDownload()
	{
		syslog(LOG_INFO, "Download complete");
//		log.uimsg("Download complete\n");
		closelog();
	}

	int CDownload::performDownload(void)
	{
		get_files_list();

		if(process_files_list()){
			if(get_files()){
				return -1;
			}else{
				install_software();
			}
		}

		return 0;
	}


#ifndef USE_CURL_INTERNAL_FILE_WRITE
	static size_t ftp_save_file( char *ptr, size_t size, size_t nmemb, void *userdata);

	static size_t ftp_save_file( char *ptr, size_t size, size_t nmemb, void *userdata)
	{
		DL_FILE_PARAMATERS *params = (DL_FILE_PARAMATERS*)userdata;

//		log.message("ftp_save_file() "+string(params->filename)+"\n");

		if(params->fh == NULL){
			log.message("ftp_save_file(): Opening file " + string(params->filename) + "\n");
			params->fh = fopen(params->filename, "wb");
		}

		if(params->fh == NULL){
			log.message("ftp_save_file(): Opening file failed\n");
			return CURL_READFUNC_ABORT;
		}

		size_t bytes_written = fwrite(ptr, size, nmemb, params->fh);

		char str_size[100];
		sprintf(str_size, "%d", bytes_written);
		log.message("ftp_save_file() writing " + string(str_size) + " bytes to "+string(params->filename)+"\n");

		if(bytes_written <= 0){
			fclose(params->fh);
			params->fh = NULL;
		}

		return bytes_written;
	}
#endif // USE_INTERNAL_FILE_WRITE

	int CDownload::get_files_list(void)
	{
		int still_running=0;

		DL_FILE_PARAMATERS files_param;

		memset(&files_param, 0, sizeof(DL_FILE_PARAMATERS));
		strcpy(files_param.filename, ALIENO_FILES_LIST_FILENAME);
		strcpy(files_param.abs_filename, ALIENO_FILES_LIST_FILENAME);
		files_param.flag_to_dl_path = 0;

		add_file_to_multi_list(&files_param);

		do {
			int numfds=0;

			curl_multi_perform(curl_multi_handle, &still_running);

			CURLMcode res = curl_multi_wait(curl_multi_handle, NULL, 0, MAX_WAIT_MSECS, &numfds);
			if(res != CURLM_OK) {
				log.message("error: curl_multi_wait() returned error\n");
				return -1;
			}
		} while(still_running);

		if(files_param.fh){
			log.message("get_files_list closing file "+string(files_param.filename)+"\n");
			fclose(files_param.fh);
		}

		curl_multi_remove_handle(curl_multi_handle, files_param.curl);
		curl_easy_cleanup(files_param.curl);

		log.message("get_files_list finished\n");

		return 0;
	}

	// Return the number of files in the list of files to be downloaded
	int CDownload::process_files_list(void)
	{
		if(files_list.size() == 0){
			FILE *fh=NULL;

			fh = fopen(ALIENO_FILES_LIST_FILENAME, "rb");

			if(fh == NULL){
				log.message("process_files_list() "+string(ALIENO_FILES_LIST_FILENAME)+" does not exist\n");
				return 0;
			}

			while(1){
				DL_FILE_PARAMATERS file_params;

				memset(&file_params, 0, sizeof(DL_FILE_PARAMATERS));

				file_params.curl = NULL;
				file_params.fh = NULL;
				file_params.flag_to_dl_path = 1;

				if(fgets(file_params.filename, sizeof(file_params.filename), fh) == NULL){
					fclose(fh);
					break;
				}

				// Lose the carriage \r or \n at the end of line
				if((file_params.filename[strlen(file_params.filename)-1] == '\n') || (file_params.filename[strlen(file_params.filename)-1] == '\r'))
					file_params.filename[strlen(file_params.filename)-1] = 0;

				// Lose the carriage \r or \n at the end of line
				if((file_params.filename[strlen(file_params.filename)-1] == '\n') || (file_params.filename[strlen(file_params.filename)-1] == '\r'))
					file_params.filename[strlen(file_params.filename)-1] = 0;

				if(strlen(file_params.filename) == 0)
					continue;

				// Check for variable $SERNO and subsistute terminal serial number
				string infilename = string(file_params.filename);
				size_t pos;

				if((pos=infilename.find("$SERIAL")) != string::npos){
					string serno = get_serial();
					infilename.replace(pos, 7, serno);
					sprintf(file_params.filename, "%s", infilename.c_str());
					log.message(MSG_INFO "Replaced $SERIAL to get ["+infilename+"]\r\n");
				}

				// Check for variable $DATE and subsisute with date
				if((pos=infilename.find("$DATE")) != string::npos){
					string datestr = get_date();
					infilename.replace(pos, 5, datestr);
					sprintf(file_params.filename, "%s", infilename.c_str());
					log.message(MSG_INFO "Replaced $DATE to get ["+infilename+"]\r\n");
				}

				path pfilename = string(file_params.filename);
				path pname = pfilename.filename();
				memcpy(file_params.abs_filename, pname.string().c_str(), pname.string().length());

				log.message(MSG_INFO "DEBUG:"+ string(file_params.abs_filename)+"\r\n");

				files_list.push_back(file_params);

				add_file_to_multi_list((DL_FILE_PARAMATERS*)&files_list.back());
			}
		}

		char str_size[100];
		sprintf(str_size, "%d", files_list.size());
		log.message("process_files_list() " + string(str_size) + " files to download\n");
		log.uimsg("Downloading "+string(str_size)+" files\n");

		return files_list.size();
	}

	int CDownload::get_files(void)
	{
		int still_running=0;
		list<DL_FILE_PARAMATERS>::iterator iter;

		do {
			int numfds=0;

			curl_multi_perform(curl_multi_handle, &still_running);

			int res = curl_multi_wait(curl_multi_handle, NULL, 0, MAX_WAIT_MSECS, &numfds);
			if(res != CURLM_OK) {
				log.message("CDownload::get_files() error: curl_multi_wait() returned error\n");
				return -1;
			}
		} while(still_running);

		for(list<DL_FILE_PARAMATERS>::iterator iter = files_list.begin(); iter != files_list.end(); iter++){
			DL_FILE_PARAMATERS files_param = *iter;

			if(files_param.fh){
				log.message("CDownload::get_files() closing file "+string(files_param.filename)+"\n");
				fclose(files_param.fh);
			}

			if(files_param.curl){
				char dl_path_filename[255];
				struct stat file_info;

				memset(dl_path_filename, 0, sizeof(dl_path_filename));

				if(files_param.flag_to_dl_path)	// Prepend the dl path name to the filename
					memcpy(dl_path_filename, DL_PATH, strlen(DL_PATH));

				memcpy(&dl_path_filename[strlen(dl_path_filename)], files_param.abs_filename, strlen(files_param.abs_filename));

				if(stat(dl_path_filename, &file_info)){
					syslog(LOG_ERR, "File %s download failed!", dl_path_filename);
					log.uimsg("File "+string(dl_path_filename)+" download failed!\n");
				}else{
					log.message("CDownload::get_files() removing handle for file " + string(files_param.filename) + "\n");
					syslog(LOG_INFO, "File %s of size %d downloaded", dl_path_filename, (int)file_info.st_size);

					char buffer[255];
					sprintf(buffer, "File %s of size %d downloaded", dl_path_filename, (int)file_info.st_size);
					log.uimsg(string(buffer)+"\n");
				}

				log.message("CDownload::get_files() removing handle for file " + string(files_param.filename) + "\n");

				curl_multi_remove_handle(curl_multi_handle, files_param.curl);
				curl_easy_cleanup(files_param.curl);
			}else
				log.message("CDownload::get_files() empty file param detected!\n");
		}

		log.message("CDownload::get_files() finished\n");

		return 0;
	}

	int CDownload::add_file_to_multi_list(DL_FILE_PARAMATERS *file_params)
	{
		string file_url;

		file_params->curl = curl_easy_init();

//		file_url = url + "/" + file_params->filename;
		file_url = url + file_params->filename;
		log.message("File to download:"+file_url+"\n");
/*
		// Create the path if it does not exist
		path p = file_params->filename;
		path pdir = p.remove_filename();	// Get the directory without filename portion

		if(!pdir.empty()){
			create_directories(pdir);
		}
*/
		curl_easy_setopt(file_params->curl, CURLOPT_NOPROGRESS, 0L);								// Progress display
		curl_easy_setopt(file_params->curl, CURLOPT_URL, file_url.c_str());								// Specify server URL
		curl_easy_setopt(file_params->curl, CURLOPT_VERBOSE, CURL_VERBOSITY);							// Talk to me!

		curl_easy_setopt(file_params->curl, CURLOPT_PROGRESSFUNCTION, progress_callback);
		curl_easy_setopt(file_params->curl, CURLOPT_NOPROGRESS, 0L);								// Progress display
		curl_easy_setopt(file_params->curl, CURLOPT_PROGRESSDATA, file_params->filename);

#ifdef USE_CURL_INTERNAL_FILE_WRITE
		// Uses the built in file writing functionality
		char dl_path_filename[255];
		memset(dl_path_filename, 0, sizeof(dl_path_filename));

		if(file_params->flag_to_dl_path)	// Prepend the dl path name to the filename
			memcpy(dl_path_filename, DL_PATH, strlen(DL_PATH));

		memcpy(&dl_path_filename[strlen(dl_path_filename)], file_params->abs_filename, strlen(file_params->abs_filename));
		log.message(MSG_INFO "DEBUG:"+string(dl_path_filename)+" abs:"+string(file_params->abs_filename)+"\r\n");

		file_params->fh = fopen(dl_path_filename, "wb");
		curl_easy_setopt(file_params->curl, CURLOPT_WRITEDATA, (void*)file_params->fh);					// Provide file parameters to be used by read callback
		curl_easy_setopt(file_params->curl, CURLOPT_WRITEFUNCTION, NULL);
#else
		// Uses our own callback function to write to the file
		curl_easy_setopt(file_params->curl, CURLOPT_WRITEDATA, (void*)file_params);						// Provide file parameters to be used by read callback
		curl_easy_setopt(file_params->curl, CURLOPT_WRITEFUNCTION, ftp_save_file);						// Setup the read callback
#endif
		curl_multi_add_handle(curl_multi_handle, file_params->curl);

		return 0;
	}

	static int progress_callback(void *clientp, double dltotal, double dlnow, double ultotal, double ulnow)
	{
		(void)ultotal;			// Suppresses -> warning: unused parameter
		(void)ulnow;			// Suppresses -> warning: unused parameter

		static int percentage=0;
		DL_FILE_PARAMATERS *fp=(DL_FILE_PARAMATERS*)clientp;

		if(dltotal == 0)
			percentage = 0;
/*
		if((dlnow == 0) && (dltotal == 0))
			log.uimsg(string((char*)fp->filename) + "\n");
*/
		if((int)((double)(100*dlnow)/dltotal) != percentage){
			percentage = (int)((double)(100*dlnow)/dltotal);
			log.uimsg(string((char*)fp->filename) + " " + log.from_int(percentage) + "%\n");
		}
		return 0;
	}


#ifdef _VOS_
#include <libsecins.h>
	int CDownload::install_software(void)
	{
		int outflags=0;

		log.uimsg("Installing software\r\n");

		Secins_install_software(&outflags, 0);

		if(outflags & SECINS_REBOOT_REQD_BIT){
			log.uimsg("Rebooting...\r\n");
			Secins_reboot();
		}

		if(outflags & SECINS_RESTART_APPS_REQD_BIT){
			log.uimsg("Application restart required...\r\n");

			char soutflags[10];
			sprintf(soutflags, "%d", outflags);
			log.message("CDownload::install_software(): outflags "+string(soutflags)+"\r\n");
			Secins_reboot();
		}

		return 0;
	}
#else
	int CDownload::install_software(void)
	{
		return 0;
	}
#endif // _VOS_

	string CDownload::get_serial(void){
#ifdef _VOS_
		char value[20];
		int result;
		unsigned long rsize=0;

		result = platforminfo_get(PI_SERIAL_NUM ,value, sizeof(value), &rsize);
		if (result == PI_OK){
			while((rsize >= 1) && (value[rsize-1] == ' ')) rsize--;		// Remove pesky trailing spaces
			return string(value, rsize);
		}
		else return string("NA");
#else
		return string("123-456-789");
#endif // _VOS_
	}

	string CDownload::get_date(void)
	{
		time_t current = time(0);
		struct tm *currt = localtime(&current);
		stringstream ss;
		ss << static_cast<unsigned int>(1900+currt->tm_year);
		if(currt->tm_mon+1 < 9) ss << '0';
		ss << static_cast<unsigned int>(currt->tm_mon+1);
		ss << static_cast<unsigned int>(currt->tm_mday);
		return ss.str();
	}
}
