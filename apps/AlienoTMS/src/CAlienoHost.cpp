/*
 * CAlineoHost.cpp
 *
 *  Created on: Dec 10, 2013
 *      Author: dietero2@verifone.com
 */
#include <string.h>
#include <CAlienoHost.h>
#include <CLog.h>
#include <curl/curl.h>
#include <AlienoTMS.h>

static CLog log;

//using namespace std;

namespace com_verifone_AlienoHost
{
	CAlienoHost::CAlienoHost()
	{
		curl_global_init(CURL_GLOBAL_DEFAULT);
		curl_multi_H = curl_multi_init();

		char *curlversion = curl_version();
		log.message("Curl version " + string(curlversion) + "\n");

		if(curl_multi_H){
			setup_user_params();
			setup_host_params();
		}
	}

	CAlienoHost::~CAlienoHost()
	{
		curl_multi_cleanup(curl_multi_H);
		curl_global_cleanup();
	}

	int CAlienoHost::process(void)
	{
		if(curl_multi_H){
			CUpload lupload(curl_multi_H, url, rootpath);
			upload = lupload;

			// TODO: build version file

			if(upload.performVersionsFileUpload() < 0){
				log.uimsg("Failed to perform version checking.\n");
				return -1;
			}

			// Perform downloads
			CDownload ldownload(curl_multi_H, url);
			download = ldownload;

			download.performDownload();

			return 0;
		}

		return -1;
	}

	int CAlienoHost::setup_host_params(void)
	{
		char value[255];
		int val_len=0;

		// Read the IP address
		memset(value, 0, sizeof(value));
		val_len = ini_gets("Alieno", "IP"," ",value,sizeof(value),ALIENO_INI);

		if(val_len == 0){
			log.uimsg("Unable to retrieve IP address from ini\n");
			return -1;
		}

		url = string("ftp://"+username+":"+password+"@");
		url.append(value);

		log.message("URL:"+ url +"\n");

		// Read the IP address
		memset(value, 0, sizeof(value));
		val_len = ini_gets("Alieno", "ROOTPATH","",value,sizeof(value),ALIENO_INI);

		if(val_len == 0){
//			log.uimsg("Unable to retrieve ROOTPATH from ini\n");
			return -1;
		}

		rootpath = string(value);

//		curl_multi_setopt(curl_multi_H, CURLOPT_URL, url.c_str());

		return 0;
	}

	int CAlienoHost::setup_user_params(void)
	{
		char value[255];
		int val_len=0;

		// Get the username and password
		memset(value, 0, sizeof(value));
		val_len = ini_gets("Alieno", "USER"," ",value,sizeof(value),ALIENO_INI);

		if(val_len == 0){
			log.uimsg("Unable to retrieve USER from ini\n");
			return -1;
		}

		username = string(value);
		log.message("Username:"+ username + "\n");

		memset(value, 0, sizeof(value));
		val_len = ini_gets("Alieno", "PASSWORD","anonymous",value,sizeof(value),ALIENO_INI);

		if(val_len == 0){
			log.uimsg("Unable to retrieve PASSWORD from ini\n");
			return -1;
		}

		password = string(value);
		log.message("Password:"+ password + "\n");

		return 0;
	}
}


