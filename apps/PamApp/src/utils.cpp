#include <cstring>
#include <sstream>
#include <iomanip>
#include <ctime>
#include <cstdlib>
#include <sys/types.h>
#include <sys/timeb.h>
#include <platforminfo_api.h>
#include "utils.h"
#include <stdio.h>

namespace Utils
{

int UtilStringToBCD(string str, char *bcd)
{
	int pos=0, count=0;

	for(string::iterator str_it=str.begin(); str_it!=str.end(); ++str_it){
		char c = *str_it;

		if(!((c >= '0') && (c <= '9')))
			return 0;

		if(count%2==0)
			bcd[pos] = ((char)(c - '0') << 4) & 0xf0;
		else{
			bcd[pos] |= (char)(c - '0');
			pos++;
		}

		count++;
	}

	// If string is of uneven length, then add 0x0f
	if(count%2){
		bcd[pos] |= (char)0x0f;
		pos++;
	}

	return pos;
}

string UtilsBcdToString(const char *data, unsigned int len)
{
	char c;
	stringstream ss;

	for (unsigned int i=0; i<len; i++) {
		if(((data[i] >> 4) & 0x0f) == 0x0f) break;

		c = ((data[i] >> 4) & 0x0f) + '0';
		if(c > '9') break;
		ss << c;

		if((data[i] & 0x0f) == 0x0f) break;

		c = (data[i] & 0x0f) + '0';
		if(c > '9') break;
		ss << c;
	}

	return ss.str();
}

int UtilsStringToInt(string str)
{
	int numb;
	istringstream ( str ) >> numb;
	return numb;
}

long UtilsStringToLong(string str)
{
	long numb;
	istringstream ( str ) >> numb;
	return numb;
}

long long UtilsStringToLongLong(string str)
{
	long long numb;

	istringstream ( str ) >> numb;
	return numb;
}

string UtilsIntToString(int int_val, int pad_count)
{
	stringstream ss;
	ss << std::setw(pad_count) << std::setfill('0') << int_val;
	return ss.str();
}

string UtilsLongToString(long long_val)
{
	stringstream ss;
	ss << long_val;
	return ss.str();
}

string UtilsHexToString(const char *data, unsigned int len)
{
	std::stringstream ss;

	ss << std::hex;
	for (size_t i = 0; i < len; ++i) {
			if(static_cast<unsigned int>(static_cast<unsigned char>(data[i])) <= 0x0f)
				ss << '0';
			ss << static_cast<unsigned int>(static_cast<unsigned char>(data[i]));
	}

	return ss.str();
}

string UtilsStringToHex(string src)
{
	char temp;
	string sRet;

	for (unsigned i = 0; i < src.length(); i++) {
		if((src.at(i) >= '0') && (src.at(i) <= '9'))
			temp = (src.at(i) - '0') << 4;
		else
			temp = (toupper(src.at(i)) - '7') << 4;

		i++;

		if((src.at(i) >= '0') && (src.at(i) <= '9'))
			temp |= src.at(i) - '0';
		else
			temp |= toupper(src.at(i)) - '7';

		sRet.append(1, temp);
	}

	return sRet;
}

string UtilsBoolToString(bool b)
{
	std::stringstream converter;
	converter << b;
	return converter.str();
}

string UtilsTimestamp()
{
	struct timeb tp;
	ftime(&tp);
	char buf[16];

	memset(buf, 0, sizeof(buf));
	strftime(buf,sizeof(buf),"%Y%m%d%H%M%S%u",localtime(&tp.time));

	return string(buf);
}

TERMINAL_TYPE UtilsGetTerminalType(void)
{
	char buffer[20];
	unsigned long rlen=0;

	memset(buffer, 0, sizeof(buffer));

	if(platforminfo_get(PI_MIB_MODEL_NUM ,buffer, (unsigned long)sizeof(buffer), &rlen) == PI_OK){
		if(memcmp(buffer, "UX300", 5) == 0)
			return TT_UX_300;
		else if((memcmp(buffer, "MX 925", 6) == 0) || (memcmp(buffer, "MX925", 5) == 0))
			return TT_MX_925;
	}

	return TT_UNKNOWN;
}

/*
 * iq_audi_19072017
 * Converts String amount into string decimal places
 * e.g 10000 to 100.00
 * amount = Amount Input
 * decimalPlaces = Decimal places required in amount
 * amountOut = Amount Out
 * currencySign = Currency sign
 */
void UtilsAmountInsertDecimal(const char *amount,int decimalPlaces,char *amountOut ,char *currencySign)
{
   unsigned int i = 0, decimalLocation=0, j=0;
   unsigned int offset = 0, currlen=0;
   char stringBuff[32]={0};

   for (i = 0; i < strlen(amount); i++)
   {
      /* Cut leading zeros */

      if (amount[i] == '0')
      {
//         stringBuff[i] = ' ';
      }
      else
      {
         offset = i;
         break;
      }
   }

   decimalLocation=12-offset-decimalPlaces-1;

   /* Shift */

  for (i = offset; i < strlen(amount)+2; i++)
  {
	  stringBuff[i - offset+j] = amount[i];

     if(decimalLocation==i-offset)
     {
        stringBuff[i - offset+1+j] ='.';
        j+=1;
     }
  }

  stringBuff[i - offset+j] = 0;

  if(currencySign!=NULL)
  {
	   strcpy(amountOut,currencySign);
	   currlen =strlen(currencySign);
  }

  if(strlen(stringBuff)< 3)
	  sprintf(amountOut+currlen,"0.%02d",atoi(stringBuff));
  else
	  memcpy(amountOut+currlen,stringBuff,strlen(stringBuff));

  amountOut[strlen(amountOut)]=NULL;
}

/*
 * iq_audi_24082017
 * Converts String amount into remove decimal places
 * e.g 10000 to 100
 * amount = Amount Input
 * decimalPlaces = Remove Decimal places values
 * amountOut = Amount Out
 * currencySign = Currency sign
 */
void UtilsAmountRemoveDecimal(const char *amount,int decimalPlaces,char *amountOut ,char *currencySign)
{
   unsigned int i = 0, decimalLocation=0;
   unsigned int offset = 0, currlen=0;
   char stringBuff[32]={0};

   for (i = 0; i < strlen(amount); i++)
   {
	  /* Cut leading zeros */

	  if (amount[i] == '0')
	  {
//         stringBuff[i] = ' ';
	  }
	  else
	  {
		 offset = i;
		 break;
	  }
   }

   decimalLocation=12-offset-decimalPlaces;

   /* Shift */

  for (i = offset; i < strlen(amount)+2; i++)
  {
	  stringBuff[i - offset] = amount[i];

	 if(decimalLocation==i-offset)
		break;
  }

  stringBuff[i - offset] = 0;

  if(currencySign!=NULL)
  {
	   strcpy(amountOut,currencySign);
	   currlen =strlen(currencySign);
  }

  memcpy(amountOut+currlen,stringBuff,strlen(stringBuff));
  amountOut[strlen(amountOut)]=NULL;
}


}
