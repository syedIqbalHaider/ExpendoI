#ifdef VFI_PLATFORM_VOS
	#include <unistd.h>
//	#include <posix/pthread.h>
#else
	#include <svc.h>
#endif
#include <liblog/logsys.h> 
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <libpml/pml.h>
#include <libpml/pml_abstracted_api.h>
#include <libpml/pml_port.h>
#include <libipc/ipc.h>
#include <bertlv.h>
#include <utils.h>

#ifdef VFI_PLATFORM_VOS
	#include <libda/cversions.h>
	#include <libda/cversionsfile.h>
	#include <libda/cconmanager.h>
#endif // VFI_PLATFORM_VOS

#include "PamAppver.h"

#define MY_TASK_NAME "CPAM"
#define CARD_SERVICE_TASK_NAME "CARDAPP"
#define GUI_SERVICE_TASK_NAME "GUIBRIDGEAPP"

typedef enum {
	IDLE,
	AWAITING_MEDIA,
	AWAITING_PAYMENT_INSTRUMENT,
	AWAITING_CARD_REMOVAL,
	EMV_PROCESS_1,
	EMV_PROCESS_2,
	EMV_ONLINE,
	SETGET_TLV,
	APDU,
	SET_CONFIGURATION,
	GET_CONFIGURATION,
	DELETE_CONFIGURATION,
	SET_CAPK,
	GET_CAPK,
	DELETE_CAPK,
	MANUAL_ENTRY,
	MANUAL_ENTRY_2,
	CR_FALLBACK_AWAITING_CARD_REMOVAL
} TRANSACTION_STATE;

typedef enum {
	STATE_UNKNOWN,
	NO_CARD_IN_SLOT,
	CARD_IN_SLOT
} CARDSLOT_STATE;

typedef enum {
	OK=0x00,
	TIMEOUT,

	CANCELLED=0x04,
	UNABLE_TO_GO_ONLINE,
	FORCE_ONLINE,
	FORCE_DECLINE,
	NO_DECISION,
	FALLBACK_MAG,
	FALLBACK_MANUAL,
	MAG_READ_ERROR,

	SEQUENCE_ERROR=0x10,
	MISSING_PARAMETER,
	UNKNOWN_PACKET_TYPE,
	UNKNOWN_PACKET,
	CONSOLE_ERROR,
	NOT_FOUND,
	CARDSTATE_UNKNOWN,
    PIN_VERIFY_ERROR,
	CANDIDATE_LIST_EMPTY,
	CARD_BLOCKED,
	APP_BLOCKED,

	STATE_IDLE=0x80,
	STATE_AWAITING_MEDIA,
	STATE_AWAITING_PAYMENT_INSTRUMENT,
	STATE_AWAITING_MODIFY_LIMITS_RESPONSE,
	STATE_EMV_PROCESSING,
	STATE_AWAITING_COMPLETE_TRANSACTION,
	STATE_TLV_PROCESSING,
	STATE_APDU_PROCESSING,

	GENERAL_ERROR=0xff
} STATUS_CODE;

//using namespace std;
using namespace za_co_verifone_bertlv;
using namespace Utils;
//using namespace com_verifone_pml;

// Uncomment line below to enable logging of EMV parameter updates
//#define EMVCFG_LOG

#ifdef EMVCFG_LOG

#include <fstream>
#include <sstream>
#include <iomanip>
#include <sys/types.h>
#include <sys/timeb.h>

#define TO_STR_A( A ) #A
#define TO_STR( A ) TO_STR_A( A )
#define MSG_INFO ":"__FILE__ "->line " TO_STR( __LINE__ ) ": "

static std::ofstream logfile ("emvcfg.log", std::ofstream::binary);

string timeStamp()
{
	struct timeb tp;
	ftime(&tp);
	char buf[16];

	memset(buf, 0, sizeof(buf));
	strftime(buf,sizeof(buf),"%Y%m%d%H%M%S%u",localtime(&tp.time));

	return string(buf);
}

void mylog(string data)
{
	logfile.write(data.c_str(), data.length());
	logfile.flush();
}

void mylog_hex(string id, string data)
{
	stringstream sslen;
	sslen << data.length();

	mylog(timeStamp()+":"+id+" length "+sslen.str()+" [");

	std::stringstream ss;
	size_t len=data.length();

	ss << std::hex;
	for (size_t i = 0; len > i; ++i) {
			if(static_cast<unsigned int>(static_cast<unsigned char>(data[i])) <= 0x0f)
				ss << '0';
			ss << static_cast<unsigned int>(static_cast<unsigned char>(data[i]));
	}

	mylog(ss.str());
	mylog("]\r\n");

	logfile.flush();
}
#else

#define mylog_hex(x, y){};
#define mylog(x){};

#endif // EMVCFG_LOG

int sendPacket(string packetData, string sendTo)
{
	return(com_verifone_ipc::send(packetData,sendTo));
}

int GUITemplate(char cTemplateIdentifier,char cTimeout,char *pszInputFields,char cSuppressResponse)
{
	string templateIdentifier,timeout,inputFields;
	BERTLV dtTlv;
	
	templateIdentifier=string(1,cTemplateIdentifier);
	if(cTimeout>0) timeout=string(1,cTimeout);
	if(pszInputFields && pszInputFields[0]!=0) inputFields=string(pszInputFields);
	
	dtTlv.addTag(string("\xDF\xAF\x01"),string("\x33"));
	dtTlv.addTag(string("\xDF\xAF\x2C"),templateIdentifier);
	if(timeout.length()==1) dtTlv.addTag(string("\xDF\xAF\x02"),timeout);
	if(inputFields.length()>0) dtTlv.addTag(string("\xDF\xAF\x2E"),inputFields);
	if(cSuppressResponse) dtTlv.addTag(string("\xDF\xAF\x31"),string("\x01"));
	
	return(sendPacket(dtTlv.getStringData(),string(GUI_SERVICE_TASK_NAME))?0:1);
}

class CPATransaction {
	public:
		CPATransaction();
		~CPATransaction();
		int processPacket(string &inPacket,string &receivedFrom);
		int processTimer();
		int setGlobalEventHandle(int hEvent);
	private:
		int processApplicationPacket(string &inPacket);
		int processCardServicePacket(string &inPacket);
		int processGuiServicePacket(string &inPacket);
		int startTransaction(BERTLV &inTlv);
		int modifyLimits(BERTLV &inTlv);
		int completeTransaction(BERTLV &inTlv);
		int readMedia(BERTLV &inTlv);
		int removeCard(BERTLV &inTlv);
		int setgetTlv(BERTLV &inTlv);
		int updateEmvParameter(BERTLV &inTlv);
		int deleteEmvParameter(BERTLV &inTlv);
		int readEmvParameter(BERTLV &inTlv);
		int updateCapk(BERTLV &inTlv);
		int readCapk(BERTLV &inTlv);
		int deleteCapk(BERTLV &inTlv);
		int apdu(BERTLV &inTlv);
		int transactionStatus();
		int cancelTransaction(bool noReply=false);
		int sendStatusPacket(string &sendTo,char cCmd,STATUS_CODE statusCode);
		int getValueForField(const string &inFields,const string &whichField,string &value);
		int setTimer(long lTimeout);
		int clrTimer();
		bool hasCombinedReader();
	private:
		TRANSACTION_STATE currentState;
		TRANSACTION_STATE previousState;
		CARDSLOT_STATE cardslotState;
		string timeOut;
		string activeInterfaces;
		string inputTlvObjects;
		string requestedTlvObjects;
		string applicationName;
		com_verifone_pml::event_item pmlTimer;
		char cCmdUsingTimer;
		int ghEvent;
};

CPATransaction::CPATransaction()
{
	currentState=IDLE;
	previousState=IDLE;
	cardslotState=STATE_UNKNOWN;
	memset(&pmlTimer,0,sizeof(com_verifone_pml::event_item));
	cCmdUsingTimer=0;
	ghEvent=-1;
}

CPATransaction::~CPATransaction()
{
}

int CPATransaction::processPacket(string &inPacket,string &receivedFrom)
{
//	char buff[240]={0};
	// process messages from card service
	if(receivedFrom.compare(string(CARD_SERVICE_TASK_NAME))==0) {
		return(processCardServicePacket(inPacket));
	}

	// process messages from gui service
	if(receivedFrom.compare(string(GUI_SERVICE_TASK_NAME))==0) {
		return(processGuiServicePacket(inPacket));
	}

	// process messages from application
	if(currentState==IDLE) applicationName=receivedFrom;
	if(receivedFrom.compare(applicationName)==0) {
//		printf("processApplicationPacket inPacket [");
//		memcpy(buff,inPacket.c_str(),inPacket.length());
//		for(int i=0; i<inPacket.length();i++)
//		{
//		printf("%02x",buff[i]);
//		}
//		printf("processApplicationPacket inPacket ]\r\n");
		return(processApplicationPacket(inPacket));
	}

	// transaction in progress from another application - send error response
	BERTLV inTlv(inPacket);
	string cmd;
	if(inTlv.getValForTag(string("\xDF\xAF\x01"),cmd) && cmd.length()==1) {
		return(sendStatusPacket(receivedFrom,cmd.at(0),GENERAL_ERROR));
	}

	// packet error
	return(0);
}

int CPATransaction::processTimer()
{
	if(!pmlTimer.evt.tmr.id) return(0);

	clrTimer();

	if(currentState!=IDLE) {
		currentState=IDLE;
		sendStatusPacket(applicationName,cCmdUsingTimer,TIMEOUT);
	}

	cCmdUsingTimer=0;

	return(1);
}

int CPATransaction::processApplicationPacket(string &inPacket)
{
	BERTLV inTlv(inPacket);
	string cmd;

	mylog_hex("processApplicationPacket inPacket - ", inPacket);

	if(inTlv.getValForTag(string("\xDF\xAF\x01"),cmd) && cmd.length()==1) {
		switch(cmd.at(0)) {
			case 0x21:	// start transaction
				return(startTransaction(inTlv));
			case 0x22:	// complete transaction
				return(completeTransaction(inTlv));
			case 0x23:	// modify limits
				return(modifyLimits(inTlv));
			case 0x24:	// apdu
				return(apdu(inTlv));
			case 0x25:	// remove card
				return(removeCard(inTlv));
			case 0x26:	// read media
				return(readMedia(inTlv));
			case 0x27:	// set get tlv
				return(setgetTlv(inTlv));
			case 0x41:	// update capk
				return(updateCapk(inTlv));
			case 0x42:	// delete capk
				return(deleteCapk(inTlv));
			case 0x43:	// read capk
				return(readCapk(inTlv));
			case 0x44:	// update emv parameter
				return(updateEmvParameter(inTlv));
			case 0x45:	// delete emv parameter
				return(deleteEmvParameter(inTlv));
			case 0x46:	// read emv parameter
				return(readEmvParameter(inTlv));
			case 0x71:	// transaction status
				return(transactionStatus());
			case 0x72:	// cancel transaction
				return(cancelTransaction());
		}
	}

	// packet error
	return(0);
}

int CPATransaction::sendStatusPacket(string &sendTo,char cCmd,STATUS_CODE statusCode)
{
	string response("\xDF\xAF\x01\x01\x00\xDF\xAF\x04\x01\x00",10);

	response.at(4)=cCmd^0x80;
	response.at(9)=statusCode;

	mylog_hex("sendStatusPacket - ", response);

	return(com_verifone_ipc::send(response,sendTo)?0:1);
}

int CPATransaction::readMedia(BERTLV &inTlv)
{
	if(currentState!=IDLE) return(sendStatusPacket(applicationName,0x26,SEQUENCE_ERROR));

//	printf("iq: readMedia \r\n");

	string dfaf02,activeInterfaces;
	inTlv.getValForTag(string("\xDF\xAF\x02"),dfaf02);
	inTlv.getValForTag(string("\xDF\xAF\x03"),activeInterfaces);

	// get active interfaces
	bool icc=false, mag=false ,manual=false;
	for(int iIndex=0;iIndex<(int)activeInterfaces.length();iIndex++) {
		if(activeInterfaces.at(iIndex)==0x21) icc=true;
		if(activeInterfaces.at(iIndex)==0x23) mag=true;
		if(activeInterfaces.at(iIndex)==0x25) manual=true;
	}

	// check for at least one active interface
	if(!icc && !mag) return(sendStatusPacket(applicationName,0x26,MISSING_PARAMETER));

	// show insert/swipe/manual screen (as per active interfaces)
	if(icc && !mag && !manual) GUITemplate(0x11,timeOut.length()==1?timeOut.at(0):0,0,0);
	if(!icc && mag && !manual) GUITemplate(0x12,timeOut.length()==1?timeOut.at(0):0,0,0);
	if(icc && mag && !manual) GUITemplate(0x13,timeOut.length()==1?timeOut.at(0):0,0,0);
	if(icc && !mag && manual) GUITemplate(0x14,timeOut.length()==1?timeOut.at(0):0,0,0);
	if(!icc && mag && manual) GUITemplate(0x15,timeOut.length()==1?timeOut.at(0):0,0,0);
	if(icc && mag && manual) GUITemplate(0x16,timeOut.length()==1?timeOut.at(0):0,0,0);
	if(!icc && !mag && manual) GUITemplate(0x17,timeOut.length()==1?timeOut.at(0):0,0,0);
	if(!icc && !mag && manual) GUITemplate(0x10,timeOut.length()==1?timeOut.at(0):0,0,0);

	currentState=AWAITING_MEDIA;
	string cmd91("\x5B\xDF\x1F\x01\x00",5);
	if(dfaf02.length()==1) cmd91.at(4)=dfaf02.at(0);
	if(icc && !mag) cmd91.append(string("\xDF\xCC\x58\x04\x00\x00\x00\x10",8));
	if(!icc && mag) cmd91.append(string("\xDF\xCC\x58\x04\x00\x00\x00\x20",8));

	mylog_hex("readMedia - ", cmd91);

	return(com_verifone_ipc::send(cmd91,string(CARD_SERVICE_TASK_NAME))?0:1);
}

int CPATransaction::removeCard(BERTLV &inTlv)
{
	if(currentState!=IDLE) return(sendStatusPacket(applicationName,0x25,SEQUENCE_ERROR));
	if(cardslotState==NO_CARD_IN_SLOT) return(sendStatusPacket(applicationName,0x25,OK));
	if(cardslotState==STATE_UNKNOWN) return(sendStatusPacket(applicationName,0x25,CARDSTATE_UNKNOWN));

	// show remove card
	GUITemplate(0x18,0,0,1);

	string dfaf02;
	if(inTlv.getValForTag(string("\xDF\xAF\x02"),dfaf02) && dfaf02.length()==1) {
		if(!pmlTimer.evt.tmr.id) {
			cCmdUsingTimer=0x25;
			setTimer(dfaf02.at(0)*1000L);
		}
	}

	currentState=AWAITING_CARD_REMOVAL;

	return(1);
}

int CPATransaction::startTransaction(BERTLV &inTlv)
{
	int iRc=1;

//	printf("iq: startTransaction \r\n");

	inTlv.getValForTag(string("\xDF\xAF\x02"),timeOut);

	if(inTlv.getValForTag(string("\xDF\xAF\x03"),activeInterfaces)==0) {
		// no active interface - send error response
		return(sendStatusPacket(applicationName,0x21,MISSING_PARAMETER));
	}

	if(inTlv.getValForTag(string("\xFF\xCC\x10"),inputTlvObjects)==0) {
		// no input tlv - send error response
		return(sendStatusPacket(applicationName,0x21,MISSING_PARAMETER));
	}

	if(inTlv.getValForTag(string("\xDF\xCC\x11"),requestedTlvObjects)==0) {
		// no requested tlv dol - send error response
		return(sendStatusPacket(applicationName,0x21,MISSING_PARAMETER));
	}

	// add tlvs we need into the dol if they are missing (4F, 5A, 9F27 and 9F34)
	bool tag4F=false, tag5A=false, tag9F27=false, tag9F34=false;
	for(int iIndex=0;iIndex<(int)requestedTlvObjects.length();) {
		if(requestedTlvObjects.at(iIndex)==0x4f) tag4F=true;
		if(requestedTlvObjects.at(iIndex)==0x5a) tag5A=true;
		if(requestedTlvObjects.at(iIndex)==0x9f && requestedTlvObjects.at(iIndex+1)==0x27) tag9F27=true;
		if(requestedTlvObjects.at(iIndex)==0x9f && requestedTlvObjects.at(iIndex+1)==0x34) tag9F34=true;
		if((requestedTlvObjects.at(iIndex)&0x1f)==0x1f) {
			iIndex++;
			while((requestedTlvObjects.at(iIndex)&0x80)==0x80 && iIndex<(int)requestedTlvObjects.length()) iIndex++;
		}
		iIndex++;
	}
	if(!tag4F) requestedTlvObjects.append("\x4F");
	if(!tag5A) requestedTlvObjects.append("\x5A");
	if(!tag9F27) requestedTlvObjects.append("\x9F\x27");
	if(!tag9F34) requestedTlvObjects.append("\x9F\x34");

	// get active interfaces
	bool icc=false, mag=false ,manual=false;
	for(int iIndex=0;iIndex<(int)activeInterfaces.length();iIndex++) {
		if(activeInterfaces.at(iIndex)==0x21) icc=true;
		if(activeInterfaces.at(iIndex)==0x23) mag=true;
		if(activeInterfaces.at(iIndex)==0x25) manual=true;
	}

	BERTLV inputTags(inputTlvObjects);
	string amountBcd, prompt, txCurrency;
	char  currencyName[5]={0}, amountAscii[13+1]={0},amountBuff[16]={0};

	if(inputTags.getValForTag("\x9f\x02", amountBcd)){

		sprintf(amountAscii,"%02x%02x%02x%02x%02x%02x"
				,amountBcd[0],amountBcd[1],amountBcd[2],amountBcd[3],amountBcd[4],amountBcd[5]);

		inputTags.getValForTag("\x5f\x2a", txCurrency);

		if(txCurrency[0]==0x08 && txCurrency[1]==0x40)
		{
			memcpy(currencyName,"USD ",4);
			Utils::UtilsAmountInsertDecimal(amountAscii,2,amountBuff,currencyName);
		}
		else if(txCurrency[0]==0x04 && txCurrency[1]==0x22)
		{
			memcpy(currencyName,"LBP ",4);
			Utils::UtilsAmountRemoveDecimal(amountAscii,2,amountBuff,currencyName);
		}

//		printf("iq:  Amount  [%s] \r\n",amountBuff);

		prompt = "AMOUNT="+SSTR(amountBuff);
	}

	// check for at least one active interface
	if(!icc && !mag && !manual) return(sendStatusPacket(applicationName,0x21,MISSING_PARAMETER));

	// show insert/swipe/manual screen (as per active interfaces)
	if(icc && !mag && !manual) GUITemplate(0x11,timeOut.length()==1?timeOut.at(0):0,(char *)prompt.c_str(),0);
	if(!icc && mag && !manual) GUITemplate(0x12,timeOut.length()==1?timeOut.at(0):0,(char *)prompt.c_str(),0);
	if(icc && mag && !manual) GUITemplate(0x13,timeOut.length()==1?timeOut.at(0):0,(char *)prompt.c_str(),0);
	if(icc && !mag && manual) GUITemplate(0x14,timeOut.length()==1?timeOut.at(0):0,(char *)prompt.c_str(),0);
	if(!icc && mag && manual) GUITemplate(0x15,timeOut.length()==1?timeOut.at(0):0,(char *)prompt.c_str(),0);
	if(icc && mag && manual) GUITemplate(0x16,timeOut.length()==1?timeOut.at(0):0,(char *)prompt.c_str(),0);
	if(!icc && !mag && manual) GUITemplate(0x17,timeOut.length()==1?timeOut.at(0):0,(char *)prompt.c_str(),0);
	if(!icc && !mag && manual) GUITemplate(0x10,timeOut.length()==1?timeOut.at(0):0,(char *)prompt.c_str(),0);

	// setup barcode trigger event if needed

	// setup start waiting for the card (91) to card service if needed
	if(icc || mag) {
		string cmd91("\x5B\xDF\x1F\x01\x00",5);
		if(timeOut.length()==1) cmd91.at(4)=timeOut.at(0);
		if(icc && !mag) cmd91.append(string("\xDF\xCC\x58\x04\x00\x00\x00\x10",8));
		if(!icc && mag) cmd91.append(string("\xDF\xCC\x58\x04\x00\x00\x00\x20",8));

		mylog_hex("startTransaction - ", cmd91);

		iRc=com_verifone_ipc::send(cmd91,string(CARD_SERVICE_TASK_NAME))?0:1;
	}

	// set state
	currentState=AWAITING_PAYMENT_INSTRUMENT;

	return(iRc);
}

int CPATransaction::modifyLimits(BERTLV &inTlv)
{
	string dfaf04,tag9f1b,dfaf14,dfaf15,dfaf16;

	mylog_hex("modifyLimits() Indata - ", inTlv.getStringData());

	inTlv.getValForTag(string("\xDF\xAF\x04"),dfaf04);
	inTlv.getValForTag(string("\x9F\x1B"),tag9f1b);		// floor limit
	inTlv.getValForTag(string("\xDF\xAF\x14"),dfaf14);	// random selection threshold - not currently used
	inTlv.getValForTag(string("\xDF\xAF\x15"),dfaf15);	// random selection target percentage - not currently used
	inTlv.getValForTag(string("\xDF\xAF\x16"),dfaf16);	// random selection maximum percentage - not currently used

	if(tag9f1b.length()>0) {
		BERTLV rosTlv(inputTlvObjects);
		rosTlv.updateTag(string("\x9F\x1B"),tag9f1b);
		inputTlvObjects=rosTlv.getStringData();
	}

	BERTLV superTlv;
	superTlv.addTag(string("\xDF\xCC\x56"),string("\x00\x00\x00\xF8",4));
	superTlv.addTag(string("\xDF\xCC\x60"),string("\x00\x00\x00\x00",4));
	if(dfaf04.length()==1) {
		if(dfaf04.at(0)==0x06){
			superTlv.updateTag(string("\xDF\xCC\x60"),string("\x00\x00\x00\x72",4));
			BERTLV ffcc10(inputTlvObjects);
			ffcc10.updateTag("\xDF\x40", "\x01");
			inputTlvObjects = ffcc10.getStringData();
		}

		if(dfaf04.at(0)==0x07) superTlv.updateTag(string("\xDF\xCC\x60"),string("\x00\x00\x00\x73",4));
	}
	if(inputTlvObjects.length()>0) superTlv.addTag(string("\xFF\xCC\x10"),inputTlvObjects);
	if(requestedTlvObjects.length()>0) superTlv.addTag(string("\xDF\xCC\x11"),requestedTlvObjects);
	string superEMV("\xC8");
	superEMV.append(superTlv.getStringData());
	mylog_hex("modifyLimits()", superEMV);

	mylog_hex("modifyLimits - ", superEMV);

	return(com_verifone_ipc::send(superEMV,string(CARD_SERVICE_TASK_NAME))?0:1);
}

int CPATransaction::completeTransaction(BERTLV &inTlv)
{
	string dfaf04,dfaf02,ffcc10,dfcc11;

	inTlv.getValForTag(string("\xDF\xAF\x04"),dfaf04);
	inTlv.getValForTag(string("\xDF\xAF\x02"),dfaf02);
	inTlv.getValForTag(string("\xFF\xCC\x10"),ffcc10);
	inTlv.getValForTag(string("\xDF\xCC\x11"),dfcc11);

	if(currentState!=EMV_ONLINE) return(sendStatusPacket(applicationName,0x22,SEQUENCE_ERROR));
	if(dfaf04.length()==0 || dfcc11.length()==0) return(sendStatusPacket(applicationName,0x22,MISSING_PARAMETER));

	BERTLV superTlv;
	superTlv.addTag(string("\xDF\xCC\x56"),string("\x00\x00\x07\x00",4));
	superTlv.addTag(string("\xDF\xCC\x60"),string("\x00\x00\x00\x01",4));
	if(dfaf04.at(0)==0x07) superTlv.updateTag(string("\xDF\xCC\x60"),string("\x00\x00\x00\x02",4));
	if(dfaf04.at(0)==0x05) superTlv.updateTag(string("\xDF\xCC\x60"),string("\x00\x00\x00\x03",4));
	if(ffcc10.length()>0) superTlv.addTag(string("\xFF\xCC\x10"),ffcc10);
	superTlv.addTag(string("\xDF\xCC\x11"),dfcc11);
	string superEMV("\xC8");
	superEMV.append(superTlv.getStringData());
	dlog_msg("completeTransaction",MY_TASK_NAME,get_task_id());
	mylog_hex("completeTransaction() Outdata - ", superEMV);
	return(com_verifone_ipc::send(superEMV,string(CARD_SERVICE_TASK_NAME))?0:1);
}

int CPATransaction::setgetTlv(BERTLV &inTlv)
{
	string dfaf03,ffcc10,dfcc11;

	inTlv.getValForTag(string("\xDF\xAF\x03"),dfaf03);
	inTlv.getValForTag(string("\xFF\xCC\x10"),ffcc10);
	inTlv.getValForTag(string("\xDF\xCC\x11"),dfcc11);

	if(currentState!=IDLE) return(sendStatusPacket(applicationName,0x27,SEQUENCE_ERROR));

	if(dfaf03.length()<1 || (ffcc10.length()==0 && dfcc11.length()==0)) return(sendStatusPacket(applicationName,0x27,MISSING_PARAMETER));

	currentState=SETGET_TLV;
	BERTLV gsTlv;
	if(ffcc10.length()) gsTlv.addTag(string("\xFF\xCC\x10"),ffcc10);
	if(dfcc11.length()) gsTlv.addTag(string("\xDF\xCC\x11"),dfcc11);
	string gsCmd("\x5F");
	gsCmd.append(gsTlv.getStringData());

	mylog_hex("setgetTlv - ", gsCmd);

	return(com_verifone_ipc::send(gsCmd,string(CARD_SERVICE_TASK_NAME))?0:1);
}

int CPATransaction::apdu(BERTLV &inTlv)
{
	string dfaf0c,ffcc20;

	inTlv.getValForTag(string("\xDF\xAF\x0C"),dfaf0c);
	inTlv.getValForTag(string("\xFF\xCC\x20"),ffcc20);

	if(ffcc20.length()==0) return(sendStatusPacket(applicationName,0x24,MISSING_PARAMETER));

	previousState=currentState;
	currentState=APDU;
	BERTLV apduTlv;
	apduTlv.addTag(string("\xFF\xCC\x20"),ffcc20);
	string apduCmd("\x5E");
	apduCmd.append(apduTlv.getStringData());

	mylog_hex("apdu - ", apduCmd);

	return(com_verifone_ipc::send(apduCmd,string(CARD_SERVICE_TASK_NAME))?0:1);
}

int CPATransaction::updateEmvParameter(BERTLV &inTlv)
{
	BERTLV outTlv,scpTlv;

	if(currentState!=IDLE) return(sendStatusPacket(applicationName,0x44,SEQUENCE_ERROR));

	scpTlv.addTag(string("\xDF\xCC\x56"),string("\x00\x00\x80\x00",4));
	string tag4f;
	if(inTlv.getValForTag(string("\x4F"),tag4f)) {
                scpTlv.addTag(string("\x4F"),tag4f);
                scpTlv.addTag(string("\x9F\x06"),tag4f);
	}
	
	int iPosition=0;
	inTlv.resetPosition();
	string ffcc10,tag,len,val;
	while(1) {
		inTlv.getTagLenVal(iPosition,tag,len,val);
		if(tag.compare(string("\xDF\xAF\x01"))!=0 && tag.compare(string("\xDF\xAF\x03"))!=0 && tag.compare(string("\x4F"))!=0) {
			ffcc10.append(tag);
			ffcc10.append(len);
			ffcc10.append(val);
		}
		iPosition=inTlv.getNextPosition();
		if(iPosition==0) break;
	}
	//if(ffcc10.length()>0)
	scpTlv.addTag(string("\xFF\xCC\x10"),ffcc10);
	
	currentState=SET_CONFIGURATION;
 	string scpCmd("\xC8");
 	scpCmd.append(scpTlv.getStringData());
 	dlog_hex(scpCmd.c_str(), scpCmd.length(), "updateEmvParameter");
 	mylog_hex("updateEmvParameter()", scpCmd);
 	return(com_verifone_ipc::send(scpCmd,string(CARD_SERVICE_TASK_NAME))?0:1);
}

int CPATransaction::deleteEmvParameter(BERTLV &inTlv)
{
	BERTLV outTlv,resTlv,scpTlv;
	string rid;
	
	dlog_msg ("Delete EMV Parameter");
	if(currentState!=IDLE) return(sendStatusPacket(applicationName,0x45,SEQUENCE_ERROR));
	
	inTlv.getValForTag(string("\x4F"),rid);
	
	scpTlv.addTag(string("\xDF\xCC\x56"),string("\x00\x00\x80\x00",4));
	if(inTlv.getValForTag(string("\x4F"),rid)) {
                scpTlv.addTag(string("\x4F"),rid);
                scpTlv.addTag(string("\x9F\x06"),rid);
	}
	scpTlv.addTag(string("\xDF\x81\x0D"),string("\x00\x00\x00\x01",4));
	
	currentState=DELETE_CONFIGURATION;
	string scpCmd("\xC8");
	scpCmd.append(scpTlv.getStringData());
	dlog_hex(scpCmd.c_str(), scpCmd.length(), "deleteEmvParameter");
	mylog_hex("deleteEmvParameter()", scpCmd);
	return(com_verifone_ipc::send(scpCmd,string(CARD_SERVICE_TASK_NAME))?0:1);
}

int CPATransaction::readEmvParameter(BERTLV &inTlv)
{
	BERTLV outTlv,resTlv,scpTlv;
	string ffcc11,rid;
	
	if(currentState!=IDLE) return(sendStatusPacket(applicationName,0x46,SEQUENCE_ERROR));
	
	inTlv.getValForTag(string("\x4F"),rid);
	
	scpTlv.addTag(string("\xDF\xCC\x56"),string("\x00\x00\x80\x00",4));
	if(inTlv.getValForTag(string("\x4F"),rid)) {
                scpTlv.addTag(string("\x4F"),rid);
                scpTlv.addTag(string("\x9F\x06"),rid);
	}
	
	string scpCmd("\xC8");
	scpCmd.append(scpTlv.getStringData());
	currentState=GET_CONFIGURATION;
	return(com_verifone_ipc::send(scpCmd,string(CARD_SERVICE_TASK_NAME))?0:1);
}

int CPATransaction::updateCapk(BERTLV &inTlv)
{
	BERTLV outTlv,scpTlv,ffcc10;
	string rid,index,modulus,exponent,checksum,expdate;
	
	if(currentState!=IDLE) return(sendStatusPacket(applicationName,0x41,SEQUENCE_ERROR));
	
	inTlv.getValForTag(string("\x4F"),rid);
    
    if(rid.length()==0)
        inTlv.getValForTag(string("\xDF\xAF\x0D"),rid);
    
	inTlv.getValForTag(string("\x9F\x22"),index);
	inTlv.getValForTag(string("\xDF\xAF\x0E"),modulus);
	inTlv.getValForTag(string("\xDF\xAF\x0F"),exponent);
	inTlv.getValForTag(string("\xDF\xAF\x10"),checksum);
	inTlv.getValForTag(string("\xDF\xAF\x32"),expdate);

	if(rid.length()==0 || index.length()==0 || modulus.length()==0 || exponent.length()==0 || checksum.length()==0 || expdate.length()==0) return(sendStatusPacket(applicationName,0x41,MISSING_PARAMETER));

	scpTlv.addTag(string("\xDF\xCC\x56"),string("\x00\x00\x80\x00",4));
	scpTlv.addTag(string("\x4F"),rid);
	scpTlv.addTag(string("\x9F\x06"),rid);
	ffcc10.addTag(string("\x9F\x22"),index);
	ffcc10.addTag(string("\xDF\xAF\x0E"),modulus);
	ffcc10.addTag(string("\xDF\xAF\x0F"),exponent);
	ffcc10.addTag(string("\xDF\xAF\x10"),checksum);
	ffcc10.addTag(string("\xDF\xAF\x32"),expdate);
	scpTlv.addTag(string("\xFF\xCC\x10"),ffcc10.getStringData());

 	string scpCmd("\xC8");
 	scpCmd.append(scpTlv.getStringData());
 	currentState=SET_CAPK;
	dlog_hex(scpCmd.c_str(), scpCmd.length(), "updateCapk");
	mylog_hex("updateCapk()", scpCmd);
 	return(com_verifone_ipc::send(scpCmd,string(CARD_SERVICE_TASK_NAME))?0:1);
}

int CPATransaction::readCapk(BERTLV &inTlv)
{
	BERTLV outTlv,scpTlv,resTlv;
	string rid,index,ffcc11;
	
	if(currentState!=IDLE) return(sendStatusPacket(applicationName,0x43,SEQUENCE_ERROR));
	
	inTlv.getValForTag(string("\x4F"),rid);
	inTlv.getValForTag(string("\x9F\x22"),index);
	
	if(rid.length()==0 || index.length()==0) return(sendStatusPacket(applicationName,0x43,MISSING_PARAMETER));
	
	scpTlv.addTag(string("\xDF\xCC\x56"),string("\x00\x00\x80\x00",4));
	scpTlv.addTag(string("\x4F"),rid);
	scpTlv.addTag(string("\x9F\x06"),rid);
	scpTlv.addTag(string("\x9F\x22"),index);
	
	currentState=GET_CAPK;
	string scpCmd("\xC8");
	scpCmd.append(scpTlv.getStringData());
	dlog_hex(scpCmd.c_str(), scpCmd.length(), "readCapk");
	return(com_verifone_ipc::send(scpCmd,string(CARD_SERVICE_TASK_NAME))?0:1);
}

int CPATransaction::deleteCapk(BERTLV &inTlv)
{
	BERTLV outTlv,scpTlv,ffcc10;
	string rid,index;
	
	if(currentState!=IDLE) return(sendStatusPacket(applicationName,0x42,SEQUENCE_ERROR));
	
	inTlv.getValForTag(string("\x4F"),rid);
	inTlv.getValForTag(string("\x9F\x22"),index);
	
	if(rid.length()==0 || index.length()==0) return(sendStatusPacket(applicationName,0x42,MISSING_PARAMETER));
	
	scpTlv.addTag(string("\xDF\xCC\x56"),string("\x00\x00\x80\x00",4));
	scpTlv.addTag(string("\x4F"),rid);
	scpTlv.addTag(string("\x9F\x06"),rid);
	ffcc10.addTag(string("\x9F\x22"),index);
	ffcc10.addTag(string("\xDF\xAF\x0E"),string(""));
	scpTlv.addTag(string("\xDF\x81\x0D"),string("\x00\x00\x00\x01",4));

	currentState=DELETE_CAPK;
	string scpCmd("\xC8");
	scpCmd.append(scpTlv.getStringData());
	dlog_hex(scpCmd.c_str(), scpCmd.length(), "deleteCapk");
	mylog_hex("deleteCapk()", scpCmd);
	return(com_verifone_ipc::send(scpCmd,string(CARD_SERVICE_TASK_NAME))?0:1);
}

int CPATransaction::transactionStatus()
{
	BERTLV resTlv;

	resTlv.addTag(string("\xDF\xAF\x01"),string("\xF1"));
	resTlv.addTag(string("\xDF\xAF\x04"),string("\x80"));

	switch(currentState) {
		case AWAITING_MEDIA:
			resTlv.updateTag(string("\xDF\xAF\x04"),string("\x81"));
			break;
		case AWAITING_PAYMENT_INSTRUMENT:
			resTlv.updateTag(string("\xDF\xAF\x04"),string("\x82"));
			break;
		case AWAITING_CARD_REMOVAL:
			resTlv.updateTag(string("\xDF\xAF\x04"),string("\x83"));
			break;
		case EMV_PROCESS_1:
			resTlv.updateTag(string("\xDF\xAF\x04"),string("\x84"));
			break;
		case EMV_PROCESS_2:
			resTlv.updateTag(string("\xDF\xAF\x04"),string("\x85"));
			break;
		case EMV_ONLINE:
			resTlv.updateTag(string("\xDF\xAF\x04"),string("\x86"));
			break;
		case SETGET_TLV:
			resTlv.updateTag(string("\xDF\xAF\x04"),string("\x87"));
			break;
		case APDU:
			resTlv.updateTag(string("\xDF\xAF\x04"),string("\x88"));
			break;
		case IDLE:
		default:
			break;
	}

	return(com_verifone_ipc::send(resTlv.getStringData(),applicationName)?0:1);
}

int CPATransaction::cancelTransaction(bool noReply)
{
	// set back to idle state
	TRANSACTION_STATE state=currentState;
	currentState=IDLE;

	// cancel pending timeout
	clrTimer();
	cCmdUsingTimer=0x00;

	// cancel waiting for card
	if(state==AWAITING_MEDIA || state==AWAITING_PAYMENT_INSTRUMENT || state==AWAITING_CARD_REMOVAL || state==EMV_PROCESS_1 ||state==EMV_PROCESS_2 || state==EMV_ONLINE) {
		string cancelCmd("\x5A\xDF\x1F\x01\x00\xDF\x2A\x01\x80",9);

		mylog_hex("cancelTransaction - ", cancelCmd);

		com_verifone_ipc::send(cancelCmd,string(CARD_SERVICE_TASK_NAME));
	}

	// don't send a reply upstream
	if(noReply) return(1);

	// send cancel reply
	BERTLV resTlv;
	resTlv.addTag(string("\xDF\xAF\x01"),string("\xF2"));
	resTlv.addTag(string("\xDF\xAF\x04"),string("\x00",1));
	return(com_verifone_ipc::send(resTlv.getStringData(),applicationName)?0:1);
}

int CPATransaction::setGlobalEventHandle(int hEvent)
{
	ghEvent=hEvent;

	return(1);
}

int CPATransaction::setTimer(long lTimeout)
{
	if(pmlTimer.evt.tmr.id) return(0);

	com_verifone_pml::set_timer(pmlTimer,lTimeout);
	com_verifone_pml::event_ctl(ghEvent,com_verifone_pml::ctl::ADD,pmlTimer);

	return(1);
}

int CPATransaction::clrTimer()
{
	if(!pmlTimer.evt.tmr.id) return(0);

	com_verifone_pml::clr_timer(pmlTimer);
	com_verifone_pml::event_ctl(ghEvent,com_verifone_pml::ctl::DEL,pmlTimer);
	memset(&pmlTimer,0,sizeof(com_verifone_pml::event_item));

	return(1);
}

bool CPATransaction::hasCombinedReader()
{
	char cModel[12];
	
	com_verifone_pml::svcInfoModelNum(cModel,sizeof(cModel));
	
	if(memcmp(cModel,"UX",2)==0) return(true);
	
	return(false);
}

int CPATransaction::processCardServicePacket(string &inPacket)
{
	if(inPacket.length()==0) return(0);

	dlog_hex(inPacket.c_str(), inPacket.length(), "processCardServicePacket");
	mylog_hex("processCardServicePacket()", inPacket);

//	printf("iq: processCardServicePacket \r\n");

	char cCmd=inPacket.at(0);
	BERTLV inTlv(inPacket.substr(1));

	string df30;
	if(inTlv.getValForTag(string("\xDF\x30"),df30) && df30.length()==1) {
		if(df30.at(0)==0x63) cardslotState=CARD_IN_SLOT;
		if(df30.at(0)==0x64) cardslotState=NO_CARD_IN_SLOT;
	}

//	printf("iqbal Card State: [%d]",currentState);
//	printf("iqbal cCmd State: [%d]",cCmd);

	switch(currentState) {
		case AWAITING_MEDIA:
			if(cCmd==91) {
//				GUITemplate(0x20,0,0,1);
				currentState=IDLE;
				string dfcc61,dfcc42,dfcc41,dfcc43,dfdf6e;
				inTlv.getValForTag(string("\xDF\xCC\x61"),dfcc61);
				inTlv.getValForTag(string("\xDF\xCC\x42"),dfcc42);
				inTlv.getValForTag(string("\xDF\xCC\x41"),dfcc41);
				inTlv.getValForTag(string("\xDF\xCC\x43"),dfcc43);
				inTlv.getValForTag(string("\xDF\xDF\x6E"),dfdf6e);
				// timeout
				if(df30.length()==1 && df30.at(0)==2){
					return(sendStatusPacket(applicationName,0x26,TIMEOUT));
				}

				// icc or mag
				BERTLV resTlv;
				resTlv.addTag(string("\xDF\xAF\x01"),string("\xA6"));
				resTlv.addTag(string("\xDF\xAF\x04"),string("\x00",1));
				resTlv.addTag(string("\xDF\xAF\x03"),string("\x23"));
				if(dfcc61.length()>0) {
					cardslotState=CARD_IN_SLOT;
					resTlv.updateTag(string("\xDF\xAF\x03"),string("\x21"));
				}

				if((dfcc41.length() || dfcc42.length() || dfcc43.length()) && dfdf6e.length()==0) {
					if(dfcc41.length()) dfdf6e.append(string("\x00",1)); else dfdf6e.append(string("\x01"));
					if(dfcc42.length()) dfdf6e.append(string("\x00",1)); else dfdf6e.append(string("\x01"));
					if(dfcc43.length()) dfdf6e.append(string("\x00",1)); else dfdf6e.append(string("\x01"));
					resTlv.addTag(string("\xDF\xDF\x6E"),dfdf6e);
				}
				string result(resTlv.getStringData());
				result.append(inTlv.getStringData());

				mylog_hex("processCardServicePacket AWAITING_MEDIA - ", result);

				return(com_verifone_ipc::send(result,applicationName)?0:1);
			}
			break;
		case AWAITING_PAYMENT_INSTRUMENT:
			if(cCmd==91) {
				string dfcc61,dfcc42,dfcc41,dfcc43,dfdf6e;
				inTlv.getValForTag(string("\xDF\xCC\x61"),dfcc61);
				inTlv.getValForTag(string("\xDF\xCC\x42"),dfcc42);
				inTlv.getValForTag(string("\xDF\xCC\x41"),dfcc41);
				inTlv.getValForTag(string("\xDF\xCC\x43"),dfcc43);
				inTlv.getValForTag(string("\xDF\xDF\x6E"),dfdf6e);
				if(df30.length()==1) {
					if(df30.at(0)==1) {	// fallback to mag processing
						cardslotState=CARD_IN_SLOT;
						if(hasCombinedReader()) {
							GUITemplate(0x18,0,0,1);
							com_verifone_pml::normal_tone();
							currentState=CR_FALLBACK_AWAITING_CARD_REMOVAL;
							break;
						} else {
							currentState=IDLE;
							com_verifone_pml::error_tone();
							return(sendStatusPacket(applicationName,0x21,FALLBACK_MAG));
						}
					}
					if(df30.at(0)==0x67 && dfdf6e.length()==3 && dfdf6e.at(1)>=0x01) {	// fallback to manual processing if track 2 present and in error
						com_verifone_pml::error_tone();
						currentState=IDLE;
						return(sendStatusPacket(applicationName,0x21,FALLBACK_MANUAL));
					}
					if(df30.at(0)==2) {	// timeout
						currentState=IDLE;
						return(sendStatusPacket(applicationName,0x21,TIMEOUT));
					}
				}
				GUITemplate(0x20,0,0,1);
				com_verifone_pml::normal_tone();
				// icc
				if(dfcc61.length()>0) {
					currentState=EMV_PROCESS_1;
					cardslotState=CARD_IN_SLOT;
					BERTLV superTlv;
					superTlv.addTag(string("\xDF\xCC\x56"),string("\x00\x00\x00\x07",4));
					superTlv.addTag(string("\xDF\xCC\x60"),string("\x00\x00\x00\x00",4));
					if(inputTlvObjects.length()>0) superTlv.addTag(string("\xFF\xCC\x10"),inputTlvObjects);
					if(requestedTlvObjects.length()>0) superTlv.addTag(string("\xDF\xCC\x11"),requestedTlvObjects);
					string superEMV("\xC8");
					superEMV.append(superTlv.getStringData());
					dlog_hex(superEMV.c_str(), superEMV.length(), "Got card insert - sending superEMV command to cardapp");

					mylog_hex("processCardServicePacket AWAITING_PAYMENT_INSTRUMENT superEMV - ", superEMV);

					return(com_verifone_ipc::send(superEMV,string(CARD_SERVICE_TASK_NAME))?0:1);
				}
				// mag
				if(dfcc41.length() || dfcc42.length() || dfcc43.length()) {
					currentState=IDLE;
					BERTLV magTlv;
					magTlv.addTag(string("\xDF\xAF\x01"),string("\xA1"));
					magTlv.addTag(string("\xDF\xAF\x04"),string("\x00",1));
					magTlv.addTag(string("\xDF\xAF\x03"),string("\x23"));
					if(dfcc41.length()>0) magTlv.addTag(string("\xDF\xCC\x41"),dfcc41);
					if(dfcc42.length()>0) magTlv.addTag(string("\xDF\xCC\x42"),dfcc42);
					if(dfcc43.length()>0) magTlv.addTag(string("\xDF\xCC\x43"),dfcc43);
					if(dfdf6e.length()==0) {
						if(dfcc41.length()) dfdf6e.append(string("\x00",1)); else dfdf6e.append(string("\x01"));
						if(dfcc42.length()) dfdf6e.append(string("\x00",1)); else dfdf6e.append(string("\x01"));
						if(dfcc43.length()) dfdf6e.append(string("\x00",1)); else dfdf6e.append(string("\x01"));
					}
					magTlv.addTag(string("\xDF\xDF\x6E"),dfdf6e);

					mylog_hex("processCardServicePacket AWAITING_PAYMENT_INSTRUMENT superEMV - ", magTlv.getStringData());

					return(com_verifone_ipc::send(magTlv.getStringData(),applicationName)?0:1);
				}

				if(hasCombinedReader()) {
					GUITemplate(0x18,0,0,1);
					com_verifone_pml::normal_tone();
					currentState=CR_FALLBACK_AWAITING_CARD_REMOVAL;
					break;
				}

				mylog("processCardServicePacket AWAITING_PAYMENT_INSTRUMENT should not get here!!!\n");
			}
			break;
		case AWAITING_CARD_REMOVAL:
			if((cCmd==92 || cCmd==93) && cardslotState==NO_CARD_IN_SLOT) {
				GUITemplate(0x22,0,0,1);
				currentState=IDLE;
				if(pmlTimer.evt.tmr.id && cCmdUsingTimer==0x25) {
					clrTimer();
					cCmdUsingTimer=0x00;
				}
				return(sendStatusPacket(applicationName,0x25,OK));
			}
			break;
		case CR_FALLBACK_AWAITING_CARD_REMOVAL:
			if((cCmd==92 || cCmd==93) && cardslotState==NO_CARD_IN_SLOT) {
				GUITemplate(0x22,0,0,1);
				currentState=IDLE;
				string dfcc42,dfcc41,dfcc43,dfdf6e;
				inTlv.getValForTag(string("\xDF\xCC\x42"),dfcc42);
				inTlv.getValForTag(string("\xDF\xCC\x41"),dfcc41);
				inTlv.getValForTag(string("\xDF\xCC\x43"),dfcc43);
				inTlv.getValForTag(string("\xDF\xDF\x6E"),dfdf6e);
				BERTLV magTlv;
				magTlv.addTag(string("\xDF\xAF\x01"),string("\xA1"));
				magTlv.addTag(string("\xDF\xAF\x04"),string("\x00",1));
				magTlv.addTag(string("\xDF\xAF\x03"),string("\x23"));
				if(dfcc41.length() || dfcc42.length() || dfcc43.length()) {
					if(dfcc41.length()>0) magTlv.addTag(string("\xDF\xCC\x41"),dfcc41);
					if(dfcc42.length()>0) magTlv.addTag(string("\xDF\xCC\x42"),dfcc42);
					if(dfcc43.length()>0) magTlv.addTag(string("\xDF\xCC\x43"),dfcc43);
					if(dfdf6e.length()==0) {
						if(dfcc41.length()) dfdf6e.append(string("\x00",1)); else dfdf6e.append(string("\x01"));
						if(dfcc42.length()) dfdf6e.append(string("\x00",1)); else dfdf6e.append(string("\x01"));
						if(dfcc43.length()) dfdf6e.append(string("\x00",1)); else dfdf6e.append(string("\x01"));
					}
					magTlv.addTag(string("\xDF\xDF\x6E"),dfdf6e);
				}else{
					magTlv.updateTag(string("\xDF\xAF\x04"),string("\x0B", 1));
				}
				return(com_verifone_ipc::send(magTlv.getStringData(),applicationName)?0:1);
			}
			break;
		case EMV_PROCESS_1:
			if(cCmd==200) {
				currentState=EMV_PROCESS_2;
				// modify limits request
				string ffcc11,dfcc01;
				if(df30.at(0)==0x68) {	// Card Blocked in vxemvapdef.h
					com_verifone_pml::error_tone();
					cardslotState=CARD_IN_SLOT;
					currentState=IDLE;
					GUITemplate(0x25,0,0,1);
					SVC_WAIT (2000);
					return(sendStatusPacket(applicationName,0x21,CARD_BLOCKED));
				}
				if(df30.at(0)==0x69) {	// App Blocked in vxemvapdef.h
					com_verifone_pml::error_tone();
					cardslotState=CARD_IN_SLOT;
					currentState=IDLE;
					GUITemplate(0x25,0,0,1);
					SVC_WAIT (2000);
					return(sendStatusPacket(applicationName,0x21,APP_BLOCKED));
				}

				inTlv.getValForTag(string("\xDF\xCC\x01"),dfcc01);
				if(dfcc01.length()==4 && (dfcc01.at(1)&0x01)==0x01 && (dfcc01.at(2)&0x80)!=0x80) {   // card blocked, fallback not allowed
					com_verifone_pml::error_tone();
					cardslotState=CARD_IN_SLOT;
					currentState=IDLE;
					GUITemplate(0x25,0,0,1);
					SVC_WAIT(2000);
					return(sendStatusPacket(applicationName,0x21,CARD_BLOCKED));
				}

				if(inTlv.getValForTag(string("\xFF\xCC\x11"),ffcc11)) {
					BERTLV emvTags(ffcc11);
					string tag4f,tag5a,tag9f4d;
					emvTags.getValForTag(string("\x4F"),tag4f);
					emvTags.getValForTag(string("\x5A"),tag5a);
					emvTags.getValForTag(string("\x9F\x4D"),tag9f4d);
					if(tag4f.length()>0 && tag5a.length()>0) {
						BERTLV mlTags(ffcc11);
						mlTags.addTag(string("\xDF\xAF\x01"),string("\xA3"));
						// modify limits response will continue super emv

						mylog_hex("processCardServicePacket EMV_PROCESS_1 - ", mlTags.getStringData());

						return(com_verifone_ipc::send(mlTags.getStringData(),applicationName)?0:1);
					} else {
						com_verifone_pml::error_tone();
						cardslotState=CARD_IN_SLOT;
						currentState=IDLE;
						return(sendStatusPacket(applicationName,0x21,FALLBACK_MAG));
					}
				}
				// required tags for modify limits were missing so we abort with empty candidate list
				currentState=IDLE;
				return(sendStatusPacket(applicationName,0x21,CANDIDATE_LIST_EMPTY));
			}
			break;
		case EMV_PROCESS_2:
			if(cCmd==200) {
				GUITemplate(0x20,0,0,1);
				currentState=IDLE;
				string emvData;
				if(inTlv.getValForTag(string("\xFF\xCC\x11"),emvData)) {
					BERTLV emvTlv(emvData);
					string tag9f27;
					if(emvTlv.getValForTag(string("\x9F\x27"),tag9f27)) {
						if(tag9f27.length()==1 && (tag9f27.at(0)&0xc0)==0x80) {
							currentState=EMV_ONLINE;
						}
					}
				}
				// super emv result up to 1st gen - send start txn complete
				BERTLV resTlv;
				resTlv.addTag(string("\xDF\xAF\x01"),string("\xA1"));
				resTlv.addTag(string("\xDF\xAF\x04"),string("\x00",1));
				resTlv.addTag(string("\xDF\xAF\x03"),string("\x21"));
				if(df30.at(0)==20) {		// offline pin cancelled
					resTlv.updateTag(string("\xDF\xAF\x04"),string("\x04"));
				} else if(df30.at(0)==2) {	// timeout
					resTlv.updateTag(string("\xDF\xAF\x04"),string("\x01"));
				}
				string ffcc11;
				inTlv.getValForTag(string("\xFF\xCC\x11"),ffcc11);
				if(ffcc11.length()>0) resTlv.addTag(string("\xFF\xCC\x11"),ffcc11);

				mylog_hex("processCardServicePacket EMV_PROCESS_2 - ", resTlv.getStringData());

				return(com_verifone_ipc::send(resTlv.getStringData(),applicationName)?0:1);
			}
			break;
		case EMV_ONLINE:
			if(cCmd==200) {
				// super emv after ext auth, 2nd gen and script processing - send complete txn resp
				currentState=IDLE;
				BERTLV resTlv;
				resTlv.addTag(string("\xDF\xAF\x01"),string("\xA2"));
				resTlv.addTag(string("\xDF\xAF\x04"),string("\x00",1));
				string ffcc11;
				inTlv.getValForTag(string("\xFF\xCC\x11"),ffcc11);
				if(ffcc11.length()>0) resTlv.addTag(string("\xFF\xCC\x11"),ffcc11);

				mylog_hex("processCardServicePacket EMV_ONLINE - ", resTlv.getStringData());

				return(com_verifone_ipc::send(resTlv.getStringData(),applicationName)?0:1);
			}
			break;
		case SETGET_TLV:
			if(cCmd==95) {
				currentState=IDLE;
				BERTLV resTlv;
				resTlv.addTag(string("\xDF\xAF\x01"),string("\xA7"));
				resTlv.addTag(string("\xDF\xAF\x04"),string("\x00",1));
				string ffcc11;
				inTlv.getValForTag(string("\xFF\xCC\x11"),ffcc11);
				if(ffcc11.length()>0) resTlv.addTag(string("\xFF\xCC\x11"),ffcc11);

				mylog_hex("processCardServicePacket SETGET_TLV - ", resTlv.getStringData());

				return(com_verifone_ipc::send(resTlv.getStringData(),applicationName)?0:1);
			}
			break;
		case APDU:
			if(cCmd==94) {
				currentState=previousState;
				previousState=IDLE;
				BERTLV resTlv;
				resTlv.addTag(string("\xDF\xAF\x01"),string("\xA4"));
				resTlv.addTag(string("\xDF\xAF\x04"),string("\x00",1));
				string ffcc21;
				inTlv.getValForTag(string("\xFF\xCC\x21"),ffcc21);
				if(ffcc21.length()>0) resTlv.addTag(string("\xFF\xCC\x21"),ffcc21);

				mylog_hex("processCardServicePacket APDU - ", resTlv.getStringData());

				return(com_verifone_ipc::send(resTlv.getStringData(),applicationName)?0:1);
			}
			break;
		case SET_CONFIGURATION:
			if(cCmd==200) {
				currentState=IDLE;
				BERTLV resTlv;
				resTlv.addTag(string("\xDF\xAF\x01"),string("\xC4"));
				resTlv.addTag(string("\xDF\xAF\x04"),string("\x00",1));
				string df30;
				inTlv.getValForTag(string("\xDF\x30"),df30);
				if(df30.length()>0) {
					if(df30.at(0)!=0) {
						resTlv.updateTag(string("\xDF\xAF\x04"),string("\xFF"));
						resTlv.addTag(string("\xDF\x30"),df30);
					}
				}
				string resCmd(resTlv.getStringData());

				mylog_hex("processCardServicePacket SET_CONFIGURATION - ", resCmd);

				return(com_verifone_ipc::send(resCmd,applicationName)?0:1);
			}
			break;
		case GET_CONFIGURATION:
			if(cCmd==200) {
				dlog_msg ("Config Response from Card App");
				currentState=IDLE;
				BERTLV resTlv;
				resTlv.addTag(string("\xDF\xAF\x01"),string("\xC6"));

				string df30,ffcc11;
				inTlv.getValForTag(string("\xDF\x30"),df30);
				inTlv.getValForTag(string("\xFF\xCC\x11"),ffcc11);
				if(df30.length()>0) {
					if(df30.at(0)!=0) {
						resTlv.updateTag(string("\xDF\xAF\x04"),string("\xFF"));
						resTlv.addTag(string("\xDF\x30"),df30);
					}
				}

				if(ffcc11.length()>0)
					resTlv.addTag(string("\xDF\xAF\x04"),string("\x00",1));
				else
					resTlv.addTag(string("\xDF\xAF\x04"),string("\x15"));

				string resCmd(resTlv.getStringData());
				if(ffcc11.length()>0) resCmd.append(ffcc11);

				mylog_hex("processCardServicePacket GET_CONFIGURATION - ", resCmd);

				return(com_verifone_ipc::send(resCmd,applicationName)?0:1);
			}
			break;
		case DELETE_CONFIGURATION:
			if(cCmd==200) {
				currentState=IDLE;
				BERTLV resTlv;
				resTlv.addTag(string("\xDF\xAF\x01"),string("\xC4"));
				resTlv.addTag(string("\xDF\xAF\x04"),string("\x00",1));
				string df30;
				inTlv.getValForTag(string("\xDF\x30"),df30);
				if(df30.length()>0) {
					if(df30.at(0)!=0) {
						resTlv.updateTag(string("\xDF\xAF\x04"),string("\xFF"));
						resTlv.addTag(string("\xDF\x30"),df30);
					}
				}
				string resCmd(resTlv.getStringData());

				mylog_hex("processCardServicePacket DELETE_CONFIGURATION - ", resCmd);

				return(com_verifone_ipc::send(resCmd,applicationName)?0:1);
			}
			break;
		case SET_CAPK:
			if(cCmd==200) {
				currentState=IDLE;
				BERTLV resTlv;
				resTlv.addTag(string("\xDF\xAF\x01"),string("\xC1"));
				resTlv.addTag(string("\xDF\xAF\x04"),string("\x00",1));
				string df30;
				inTlv.getValForTag(string("\xDF\x30"),df30);
				if(df30.length()>0) {
					if(df30.at(0)!=0) {
						resTlv.updateTag(string("\xDF\xAF\x04"),string("\xFF"));
						resTlv.addTag(string("\xDF\x30"),df30);
					}
				}
				string resCmd(resTlv.getStringData());

				mylog_hex("processCardServicePacket SET_CAPK - ", resCmd);

				return(com_verifone_ipc::send(resCmd,applicationName)?0:1);
			}
			break;
		case GET_CAPK:
			if(cCmd==200) {
				dlog_msg ("Get CAPK");
				currentState=IDLE;
				BERTLV resTlv;
				resTlv.addTag(string("\xDF\xAF\x01"),string("\xC3"));

				string df30,ffcc11;
				inTlv.getValForTag(string("\xDF\x30"),df30);
				inTlv.getValForTag(string("\xFF\xCC\x11"),ffcc11);
				if(df30.length()>0) {
					if(df30.at(0)!=0) {
						resTlv.updateTag(string("\xDF\xAF\x04"),string("\xFF"));
						resTlv.addTag(string("\xDF\x30"),df30);
					}
				}

				if(ffcc11.length()>0)
					resTlv.addTag(string("\xDF\xAF\x04"),string("\x00",1));
				else
					resTlv.addTag(string("\xDF\xAF\x04"),string("\x15"));

				string resCmd(resTlv.getStringData());
				if(ffcc11.length()>0)
					resCmd.append(ffcc11);

				mylog_hex("processCardServicePacket GET_CAPK - ", resCmd);

				return(com_verifone_ipc::send(resCmd,applicationName)?0:1);
			}
			break;
		case DELETE_CAPK:
			if(cCmd==200) {
				currentState=IDLE;
				BERTLV resTlv;
				resTlv.addTag(string("\xDF\xAF\x01"),string("\xC1"));
				resTlv.addTag(string("\xDF\xAF\x04"),string("\x00",1));
				string df30;
				inTlv.getValForTag(string("\xDF\x30"),df30);
				if(df30.length()>0) {
					if(df30.at(0)!=0) {
						resTlv.updateTag(string("\xDF\xAF\x04"),string("\xFF"));
						resTlv.addTag(string("\xDF\x30"),df30);
					}
				}
				string resCmd(resTlv.getStringData());

				mylog_hex("processCardServicePacket DELETE_CAPK - ", resCmd);

				return(com_verifone_ipc::send(resCmd,applicationName)?0:1);
			}
			break;
		case IDLE:
		default:
			break;
	}

	return(0);
}

int CPATransaction::processGuiServicePacket(string &inPacket)
{
	BERTLV inTlv(inPacket);
	string cmd,status;
	inTlv.getValForTag(string("\xDF\xAF\x01"),cmd);
	inTlv.getValForTag(string("\xDF\xAF\x04"),status);
	
//	printf("iq: processGuiServicePacket \r\n");

	switch(currentState) {
		case AWAITING_PAYMENT_INSTRUMENT:
			if(cmd.length()==1 && cmd.at(0)==0xb3) {
				if(status.length()==1) {
					if(status.at(0)==0x04) {	//cancelled
						cancelTransaction(true);
						currentState=IDLE;
						return(sendStatusPacket(applicationName,0x21,CANCELLED));
					}
					if(status.at(0)==0x01) {	//timeout
						cancelTransaction(true);
						currentState=IDLE;
						return(sendStatusPacket(applicationName,0x21,TIMEOUT));
					}
					if(status.at(0)==0x00) {	//enter - manual entry
						cancelTransaction(true);
						currentState=MANUAL_ENTRY;
						return(GUITemplate(0x10,60,0,0));
					}
				}
			}
			break;
		case MANUAL_ENTRY:
			if(cmd.length()==1 && cmd.at(0)==0xb3) {
				if(status.length()==1) {
					if(status.at(0)==0x04) {	//cancelled
						currentState=IDLE;
						return(sendStatusPacket(applicationName,0x21,CANCELLED));
					}
					if(status.at(0)==0x01) {	//timeout
						currentState=IDLE;
						return(sendStatusPacket(applicationName,0x21,TIMEOUT));
					}
					if(status.at(0)==0x00) {	// card data available PAN,EXP,CVV
						BERTLV resTlv;
						resTlv.addTag(string("\xDF\xAF\x01"),string("\xA1"));
						resTlv.addTag(string("\xDF\xAF\x04"),string("\x00",1));
						resTlv.addTag(string("\xDF\xAF\x03"),string("\x25"));
						string dfaf2d;
						inTlv.getValForTag(string("\xDF\xAF\x2D"),dfaf2d);
						string dfaf05,dfaf06,dfaf07;
						getValueForField(dfaf2d,string("PAN="),dfaf05);
						getValueForField(dfaf2d,string("EXP="),dfaf06);
						getValueForField(dfaf2d,string("CVV="),dfaf07);
						if(dfaf05.length()>0) resTlv.addTag(string("\xDF\xAF\x05"),dfaf05);
						if(dfaf06.length()>0) resTlv.addTag(string("\xDF\xAF\x06"),dfaf06);
						if(dfaf07.length()>0) resTlv.addTag(string("\xDF\xAF\x07"),dfaf07);
						return(sendPacket(resTlv.getStringData(),applicationName)?0:1);
					}
				}
			}
			break;
		case AWAITING_MEDIA:
			if(cmd.length()==1 && cmd.at(0)==0xb3) {
				if(status.length()==1) {
					if(status.at(0)==0x04) {	//cancelled
						cancelTransaction(true);
						currentState=IDLE;
						return(sendStatusPacket(applicationName,0x26,CANCELLED));
					}
					if(status.at(0)==0x01) {	//timeout
						cancelTransaction(true);
						currentState=IDLE;
						return(sendStatusPacket(applicationName,0x26,TIMEOUT));
					}
					if(status.at(0)==0x00) {	//enter - manual entry
						cancelTransaction(true);
						currentState=MANUAL_ENTRY_2;
						return(GUITemplate(0x10,60,0,0));
					}
				}
			}
			break;
		case MANUAL_ENTRY_2:
			if(cmd.length()==1 && cmd.at(0)==0xb3) {
				if(status.length()==1) {
					if(status.at(0)==0x04) {	//cancelled
						currentState=IDLE;
						return(sendStatusPacket(applicationName,0x26,CANCELLED));
					}
					if(status.at(0)==0x01) {	//timeout
						currentState=IDLE;
						return(sendStatusPacket(applicationName,0x26,TIMEOUT));
					}
					if(status.at(0)==0x00) {	// card data available PAN,EXP,CVV
						BERTLV resTlv;
						resTlv.addTag(string("\xDF\xAF\x01"),string("\xA6"));
						resTlv.addTag(string("\xDF\xAF\x04"),string("\x00",1));
						resTlv.addTag(string("\xDF\xAF\x03"),string("\x25"));
						string dfaf2d;
						inTlv.getValForTag(string("\xDF\xAF\x2D"),dfaf2d);
						string dfaf05,dfaf06,dfaf07;
						getValueForField(dfaf2d,string("PAN="),dfaf05);
						getValueForField(dfaf2d,string("EXP="),dfaf06);
						getValueForField(dfaf2d,string("CVV="),dfaf07);
						if(dfaf05.length()>0) resTlv.addTag(string("\xDF\xAF\x05"),dfaf05);
						if(dfaf06.length()>0) resTlv.addTag(string("\xDF\xAF\x06"),dfaf06);
						if(dfaf07.length()>0) resTlv.addTag(string("\xDF\xAF\x07"),dfaf07);
						return(sendPacket(resTlv.getStringData(),applicationName)?0:1);
					}
				}
			}
			break;
	}

	return(0);
}

int CPATransaction::getValueForField(const string &inFields,const string &whichField,string &value)
{
	unsigned int uiStart,uiEnd;
	
	// no input data
	if(inFields.length()==0) return(0);
	
	// find field
	uiStart=inFields.find(whichField);
	
	// field not found
	if(uiStart==string::npos) return(0);
	
	// skip field name
	uiStart+=whichField.length();
	
	// find field terminator
	uiEnd=inFields.find(string(":"),uiStart);
	if(uiEnd==string::npos) uiEnd=inFields.length();
	
	// get value
	value=inFields.substr(uiStart,uiEnd-uiStart);
	
	// return length
	return(uiEnd-uiStart);
}

static CPATransaction transaction;

int ProcessEvents(com_verifone_pml::eventset_t tPmlEvents)
{
	string sInBuffer,sFrom;
	int iRc=0;
	
	while(!tPmlEvents.empty()) {
		com_verifone_pml::event_item tEvent=tPmlEvents.back();
		tPmlEvents.pop_back();
		if(tEvent.event_id==com_verifone_pml::events::cascade) {
			dlog_msg("Got a Cascade event");
			com_verifone_pml::eventset_t tCascadeEvents;
			if(event_read(tEvent.evt.cascade.fd,tCascadeEvents)>0) iRc=ProcessEvents(tCascadeEvents);
		} else if(tEvent.event_id==com_verifone_pml::events::ipc) {
			dlog_msg("Got an IPC event");

			if(com_verifone_ipc::receive(sInBuffer,string(""),sFrom)==com_verifone_ipc::IPC_SUCCESS) {
				dlog_msg("IPC Received from %s",sFrom.c_str());
				dlog_hex(sInBuffer.c_str(),sInBuffer.length(),"sInBuffer");
				iRc=transaction.processPacket(sInBuffer,sFrom);
			}
		} else if(tEvent.event_id==com_verifone_pml::events::timer) {
			dlog_msg("Got a Timer event");
			iRc=transaction.processTimer();
		}
	}

	return(iRc);
}

void update_versions(void)
{
	com_verifone_versions::CVersions versions;
	com_verifone_versionsfile::CVersionsFile vfile;

	vfile.setName("");
	vfile.setVersion(PAM_VERSION);
	versions.addFile(PAM_NAME, vfile);
}

#ifdef VFI_PLATFORM_VOS
pthread_t tVersion;

void *update_versions(void*)
{
	com_verifone_versions::CVersions versions;
	com_verifone_versionsfile::CVersionsFile vfile;

	dlog_msg("PamApp start writing versions\r\n");

	vfile.setName("");
	vfile.setVersion(PAM_VERSION);

	// Keep trying every second, until we succeed
	while(1){
		if(versions.addFile(PAM_NAME, vfile) == DB_OK)
			break;

		sleep(1);
	}

	dlog_msg("PamApp versions complete\r\n");

	return NULL;
}
#endif // VFI_PLATFORM_VOS
#include <libpml/pml.h>
int main(int argc, char *argv[])
{
	(void)argc;(void)argv; // ignore startup prms

	com_verifone_pml::eventset_t tPmlEvents;
	string sFrom;
	string sInBuffer;
	int iEventCount;
	int hEvent;
	int iRc;

	// initialise logging
//	LOG_INIT(MY_TASK_NAME,LOGSYS_COMM,LOGSYS_PRINTF_FILTER);
	dlog_init(MY_TASK_NAME);
	dlog_msg("started %s, task no %d",MY_TASK_NAME,get_task_id());

#ifdef VFI_PLATFORM_VOS
//	pthread_create(&tVersion,NULL,&update_versions,NULL);
#endif // VFI_PLATFORM_VOS

	// set working directory
	com_verifone_pml::set_working_directory(com_verifone_pml::getRAMRoot().c_str());

	// init ipc
	iRc=com_verifone_ipc::init(string(MY_TASK_NAME));
	if(iRc<0){
		dlog_msg("ERROR: initialising IPC failed with %d", iRc);
		printf("ERROR: initialising IPC failed with %d", iRc);
		return(-1);
	}

	// init pml
	iRc=com_verifone_pml::pml_init(MY_TASK_NAME);
	if(iRc<0){
		dlog_msg("ERROR: initialising PML failed with %d", iRc);
		printf("ERROR: initialising PML failed with %d", iRc);
		return(-2);
	}

	// init event handler
	hEvent=com_verifone_pml::event_open();
	if(hEvent<0){
		dlog_msg("ERROR: opening pml event handler failed with %d", hEvent);
		printf("ERROR: opening pml event handler failed with %d", hEvent);
		return(-3);
	}

	transaction.setGlobalEventHandle(hEvent);

	// add ipc event
	com_verifone_pml::event_ctl(hEvent,com_verifone_pml::ctl::ADD,com_verifone_ipc::get_events_handler());
	
	// connect to gui
//	iRc=com_verifone_ipc::connect_to(string(GUI_SERVICE_TASK_NAME));
//	if(iRc!=0){
//		dlog_msg("ERROR: ipc connect to GUI service failed with %d", iRc);
//		printf("ERROR: ipc connect to GUI service failed with %d", iRc);
//		return(-4);
//	}

	while(com_verifone_ipc::connect_to(string(GUI_SERVICE_TASK_NAME)))
	{
		dlog_msg("ERROR: ipc connect to GUI service failed with %d", iRc);
		printf("ERROR: ipc connect to GUI service failed with %d", iRc);
		sleep(1);
	}


	// wait for card app
	while(!com_verifone_ipc::is_task_available(string(CARD_SERVICE_TASK_NAME))) {
		com_verifone_pml::normal_tone();
		SVC_WAIT(1000);
	}

	// connect to card app
//	iRc=com_verifone_ipc::connect_to(string(CARD_SERVICE_TASK_NAME));
//	if(iRc!=0){
//		dlog_msg("ERROR: ipc connect to CaradApp service failed with %d", iRc);
//		printf("ERROR: ipc connect to CaradApp service failed with %d", iRc);
//		return(-5);
//	}

	while(com_verifone_ipc::connect_to(string(CARD_SERVICE_TASK_NAME)))
	{
		dlog_msg("ERROR: ipc connect to CaradApp service failed with %d", iRc);
		printf("ERROR: ipc connect to CaradApp service failed with %d", iRc);
		sleep(1);
	}


	// start reader monitoring (92)
	iRc=com_verifone_ipc::send(string("\x5C"),string(CARD_SERVICE_TASK_NAME));
	if(iRc!=0){
		dlog_msg("ERROR: ipc send to CaradApp service failed with %d", iRc);
		printf("ERROR: ipc send to CaradApp service failed with %d", iRc);
		return(-6);
	}
	
	// main loop
	while(1) {
		iEventCount=com_verifone_pml::event_wait(hEvent,tPmlEvents);
		if(iEventCount>0||tPmlEvents.size()>0) {
			iRc=ProcessEvents(tPmlEvents);
		}
		SVC_WAIT(100);
	}

	// we should never get here
	return(0);
}
