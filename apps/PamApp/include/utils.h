#ifndef _UTILS_H_
#define _UTILS_H_

#include <string>
#include <sstream>

//using namespace std;
using std::string;
using std::stringstream;
using std::istringstream;

namespace Utils{

#define UTILS_ASCII_HEX( x ) dynamic_cast< std::ostringstream & >( std::ostringstream() << std::dec << x ).str()

int UtilStringToBCD(string str, char *bcd);
string UtilsBcdToString(const char *data, unsigned int len);
int UtilsStringToInt(string str);
long UtilsStringToLong(string str);
long long UtilsStringToLongLong(string str);
string UtilsIntToString(int int_val, int pad_count=0);
string UtilsLongToString(long long_val);
string UtilsHexToString(const char *data, unsigned int len);
string UtilsStringToHex(string src);
string UtilsBoolToString(bool b);
string UtilsTimestamp();
void UtilsAmountInsertDecimal(const char *amount,int decimalPlaces,char *amountOut ,char *currencySign);
void UtilsAmountRemoveDecimal(const char *amount,int decimalPlaces,char *amountOut ,char *currencySign);

typedef enum
{
	TT_UNKNOWN,
	TT_MX_925,
	TT_UX_300
}TERMINAL_TYPE;

TERMINAL_TYPE UtilsGetTerminalType(void);

}

#endif // _UTILS_H_
