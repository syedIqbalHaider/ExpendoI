#include <string>
#include "bertlv.h"

//using namespace std;
using namespace za_co_verifone_bertlv;

BERTLV::BERTLV(const string &data)
{
	bertlvData=data;
	bertlvIndex=0;
}

void BERTLV::clear()
{
	bertlvData.clear();
	bertlvIndex=0;
}

int BERTLV::getTag(int startPosition,string &tag)
{
	int iIndex=0;
	
	while(startPosition+iIndex<(int)bertlvData.length()) {
		if(iIndex==0 && (bertlvData.at(startPosition+iIndex)&0x1f)!=0x1f) break;
		if(iIndex>0 && (bertlvData.at(startPosition+iIndex)&0x80)!=0x80) break;
		iIndex++;
	}
	iIndex++;
	
	if(startPosition+iIndex>(int)bertlvData.length()) return(0);
	
	tag=bertlvData.substr(startPosition,iIndex);
	
	return(startPosition+iIndex);
}

int BERTLV::getLen(int startPosition,string &len)
{
	int iIndex=0;
	
	if(startPosition>=(int)bertlvData.length()) return(0);
	if((bertlvData.at(startPosition)&0x80)==0x80) {
		iIndex=1+(bertlvData.at(startPosition)&0x7f);
	} else {
		iIndex=1;
	}
	
	len=bertlvData.substr(startPosition,iIndex);
	
	return(startPosition+iIndex);
}

unsigned long BERTLV::berLenToLong(const string &len)
{
	unsigned long ulResult=0;
	
	if(len.length()==1) {
		ulResult=len.at(0);
	} else {
		for(int iIndex=1;iIndex<(int)len.length();iIndex++) {
			ulResult<<=8;
			ulResult+=len.at(iIndex);
		}
	}
	
	return(ulResult);
}

int BERTLV::getTagLenVal(int startPosition,string &tag,string &len,string &val)
{
	int iLenPos;
	int iValPos;
	unsigned long ulValLength;
	
	if(startPosition>(int)bertlvData.length()) return(0);
	
	iLenPos=getTag(startPosition,tag);
	if(iLenPos==0) return(0);
	
	iValPos=getLen(iLenPos,len);
	if(iValPos==0) return(0);
	
	ulValLength=berLenToLong(len);
	val=bertlvData.substr(iValPos,ulValLength);
	
	return(iValPos+ulValLength);
}

void BERTLV::resetPosition()
{
	bertlvIndex=0;
}

int BERTLV::getNextPosition()
{
	string tag,len,val;
	
	bertlvIndex=getTagLenVal(bertlvIndex,tag,len,val);
	
	return(bertlvIndex);
}

int BERTLV::getValForTag(const string &tag,string &val)
{
	string myTag,myLen,myVal;
	int iPos;
	int iSaveBertlvIndex=bertlvIndex;
	
	iPos=0;
	val.clear();
	resetPosition();
	while(1) {
		getTagLenVal(iPos,myTag,myLen,myVal);
		if(tag==myTag) {
			bertlvIndex=iSaveBertlvIndex;
			val=myVal;
			return(1);
		}
		iPos=getNextPosition();
		if(iPos==0) break;
	}
	
	bertlvIndex=iSaveBertlvIndex;	
	return(0);
}

int BERTLV::getLongForTag(const string &tag,long &lValue)
{
	int iIndex;
	string val;
	
	if(!getValForTag(tag,val)) return(0);
	
	lValue=0;
	for(iIndex=0;iIndex<(int)val.length();iIndex++) {
		lValue<<=8;
		lValue+=val.at(iIndex);
	}
	
	return(1);
}

int BERTLV::getShortForTag(const string &tag,short &sValue)
{
	int iIndex;
	string val;
	
	if(!getValForTag(tag,val)) return(0);
	
	sValue=0;
	for(iIndex=0;iIndex<(int)val.length();iIndex++) {
		sValue<<=8;
		sValue+=val.at(iIndex);
	}
	
	return(1);
}

int BERTLV::getByteForTag(const string &tag,char &cValue)
{
	string val;
	
	if(!getValForTag(tag,val)) return(0);
	
	cValue=val.at(0);
	
	return(1);
}

void BERTLV::longToBerLen(unsigned long ulLength,string &len)
{
	char cLength[5];
	
	if(ulLength<0x80) {
		cLength[0]=ulLength;
		len=string(cLength,1);
	} else if(ulLength<0x100) {
		cLength[0]=0x81;
		cLength[1]=ulLength;
		len=string(cLength,2);
	} else if(ulLength<0x10000) {
		cLength[0]=0x82;
		cLength[1]=(ulLength>>8)&0xff;
		cLength[2]=ulLength&0xff;
		len=string(cLength,3);
	} else if(ulLength<0x1000000) {
		cLength[0]=0x83;
		cLength[1]=(ulLength>>16)&0xff;
		cLength[2]=(ulLength>>8)&0xff;
		cLength[3]=ulLength&0xff;
		len=string(cLength,4);
	} else {
		cLength[0]=0x84;
		cLength[1]=(ulLength>>25)&0xff;
		cLength[2]=(ulLength>>16)&0xff;
		cLength[3]=(ulLength>>8)&0xff;
		cLength[4]=ulLength&0xff;
		len=string(cLength,5);
	}
}

int BERTLV::addTag(const string &tag,const string &val)
{
	string myVal,berLen;
	
	if(getValForTag(tag,myVal)) return(0);
	
	longToBerLen(val.length(),berLen);
	
	bertlvData=bertlvData+tag+berLen+val;
	return(1);
}

int BERTLV::updateTag(const string &tag,const string &val)
{
	deleteTag(tag);
	return(addTag(tag,val));
}

int BERTLV::deleteTag(const string &tag)
{
	string myTag,myLen,myVal;
	int iPos;
	int iRc;
	
	iPos=0;
	resetPosition();
	while(1) {
		iRc=getTagLenVal(iPos,myTag,myLen,myVal);
		if(tag==myTag) {
			bertlvData=bertlvData.substr(0,iPos)+bertlvData.substr(iRc);
			return(1);
		}
		iPos=getNextPosition();
		if(iPos==0) break;
	}
	
	return(0);
}

string BERTLV::getStringData()
{
	return(bertlvData);
}
