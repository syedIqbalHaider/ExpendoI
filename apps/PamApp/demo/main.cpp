#ifdef VFI_PLATFORM_VOS
	#include <unistd.h>
	#include <pthread.h>
	#include <arpa/inet.h>
#else
	#include <svc.h>
#endif
#include <liblog/logsys.h> 
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <libpml/pml.h>
#include <libpml/pml_abstracted_api.h>
#include <libpml/pml_port.h>
#include <libipc/ipc.h>
#include <bertlv.h>
#include <libpam.h>

#define MY_TASK_NAME "PAMDEMO"

//using namespace std;
using namespace za_co_verifone_bertlv;

class myPAM : public PAM {
	public:
		myPAM(string yourAppName) : PAM(yourAppName) { }
		~myPAM();
		int startTransactionResult(string sStatusCode,string sActiveInterface,string sEmvTlv,string sTrack1,string sTrack2,string sTrack3,string sSwipeStatus,string sManualPan,string sManualExpiryDate,string sManualCvv,string sBarcodeData);
		int modifyLimitsRequest(string sAid,string sPan,string sLogEntry);
		int completeTransactionResult(string sStatusCode,string sEmvTlv);
		int apduResult(string sStatusCode,string sApduResponse) {(void)sStatusCode;(void)sApduResponse; return(-1); }
		int readMediaResult(string sStatusCode,string sActiveInterface,string sAtr,string sTrack1,string sTrack2,string sTrack3,string sSwipeStatus,string sManualPan,string sManualExpiryDate,string sManualCvv,string sBarcodeData);
		int removeCardResult(string sStatusCode);
		int setGetTlvResult(string sStatusCode,string sEmvTlv) {(void)sStatusCode;(void)sEmvTlv;return(-1); }
		int transactionStatusResult(string sStatusCode);
		int cancelTransactionResult(string sStatusCode);
};

myPAM::~myPAM()
{
}

int myPAM::transactionStatusResult(string sStatusCode)
{
	dlog_msg("transactionStatusResult(%d)",sStatusCode.at(0));
	
	return(0);
}

int myPAM::cancelTransactionResult(string sStatusCode)
{
	dlog_msg("cancelTransactionResult(%d)",sStatusCode.at(0));
	
	return(0);
}

int myPAM::readMediaResult(string sStatusCode,string sActiveInterface,string sAtr,string sTrack1,string sTrack2,string sTrack3,string sSwipeStatus,string sManualPan,string sManualExpiryDate,string sManualCvv,string sBarcodeData)
{
	(void)sTrack1;
	(void)sTrack3;
	(void)sSwipeStatus;
	(void)sManualPan;
	(void)sManualExpiryDate;
	(void)sManualCvv;
	(void)sBarcodeData;
	dlog_msg("readMediaResult(%d)",sStatusCode.at(0));
	if(sActiveInterface.length()>0){dlog_msg("sActiveInterface=%02X",sActiveInterface.at(0));}
	if(sAtr.length()>0){dlog_hex(sAtr.c_str(),sAtr.length(),"sAtr");}
	if(sTrack2.length()>0){dlog_hex(sTrack2.c_str(),sTrack2.length(),"sTrack2");}
	
	return(0);
}

int myPAM::startTransactionResult(string sStatusCode,string sActiveInterface,string sEmvTlv,string sTrack1,string sTrack2,string sTrack3,string sSwipeStatus,string sManualPan,string sManualExpiryDate,string sManualCvv,string sBarcodeData)
{
	dlog_msg("startTransactionResult(%d)",sStatusCode.at(0));
	if(sActiveInterface.length()>0) dlog_msg("sActiveInterface=%02X",sActiveInterface.at(0));
	if(sEmvTlv.length()>0) dlog_hex(sEmvTlv.c_str(),sEmvTlv.length(),"sEmvTlv");
	if(sTrack2.length()>0) dlog_hex(sTrack2.c_str(),sTrack2.length(),"sTrack2");
	if(sManualPan.length()>0) dlog_hex(sManualPan.c_str(),sManualPan.length(),"sManualPan");
	if(sManualExpiryDate.length()>0) dlog_hex(sManualExpiryDate.c_str(),sManualExpiryDate.length(),"sManualExpiryDate");
	if(sManualCvv.length()>0) dlog_hex(sManualCvv.c_str(),sManualCvv.length(),"sManualCvv");
	
	return(0);
}

int myPAM::modifyLimitsRequest(string sAid,string sPan,string sLogEntry)
{
	dlog_msg("modifyLimitsRequest()");
	if(sAid.length()>0) dlog_hex(sAid.c_str(),sAid.length(),"sAid");
	if(sPan.length()>0) dlog_hex(sPan.c_str(),sPan.length(),"sPan");
	if(sLogEntry.length()>0) dlog_hex(sLogEntry.c_str(),sLogEntry.length(),"sLogEntry");
	
	return(0);
}

int myPAM::completeTransactionResult(string sStatusCode,string sEmvTlv)
{
	dlog_msg("completeTransactionResult(%d)",sStatusCode.at(0));
	if(sEmvTlv.length()>0) dlog_hex(sEmvTlv.c_str(),sEmvTlv.length(),"sEmvTlv");
	
	return(0);
}

int myPAM::removeCardResult(string sStatusCode)
{
	dlog_msg("removeCardResult(%d)",sStatusCode.at(0));
	
	return(0);
}

int main()
{
	int iRc;
	
	// initialise logging
//	LOG_INIT(MY_TASK_NAME,LOGSYS_COMM,LOGSYS_PRINTF_FILTER);
	dlog_init(MY_TASK_NAME);
	dlog_msg("started %s, task no %d",MY_TASK_NAME,get_task_id());
	
	myPAM pam(string(MY_TASK_NAME));
	dlog_msg("pam(%s)",MY_TASK_NAME);

	SVC_WAIT(1000);

	while(1) {	
		// start transaction
		pam.getLastCommandToRespond();
		pam.testAndSetBusyFlag();
		iRc=pam.startTransaction(string("\x21\x23\x25"),string("\x9C\x01\x00\x9A\x03\x14\x02\x05\x9F\x21\x03\x15\x46\x52\x9F\x02\x06\x00\x00\x00\x00\x01\x01",23),string("\x4F\x82\x9F\x36\x9F\x07\x9F\x27\x9F\x26\x9F\x34\x9F\x0D\x9F\x0E\x9F\x0F\x9F\x1A\x9F\x35\x95\x9F\x53\x9F\x2A\x9A\x9F\x41\x9F\x37\x5A\x5F\x34"));
		dlog_msg("pam.startTransaction()=%d",iRc);
		pam.testAndSetBusyFlag();
		iRc=pam.startTransaction(string("\x21\x23\x25"),string("\x9C\x01\x00\x9A\x03\x14\x02\x05\x9F\x21\x03\x15\x46\x52\x9F\x02\x06\x00\x00\x00\x00\x01\x01",23),string("\x4F\x82\x9F\x36\x9F\x07\x9F\x27\x9F\x26\x9F\x34\x9F\x0D\x9F\x0E\x9F\x0F\x9F\x1A\x9F\x35\x95\x9F\x53\x9F\x2A\x9A\x9F\x41\x9F\x37\x5A\x5F\x34"));
		dlog_msg("pam.startTransaction()=%d",iRc);
		
		// possible modify limits
		pam.testAndSetBusyFlag();
		if(pam.getLastCommandToRespond()==MODIFY_LIMITS) {
			iRc=pam.modifyLimits(string(PAM_NO_DECISION),10000);
			dlog_msg("pam.modifyLimits()=%d",iRc);
			pam.testAndSetBusyFlag();
		} else {
			dlog_hex(pam.berResult.getStringData().c_str(),pam.berResult.getStringData().length(),"pam.berResult");
			string sActiveInterface;
			pam.berResult.getValForTag("\xDF\xAF\x03",sActiveInterface);
			dlog_hex(sActiveInterface.c_str(),sActiveInterface.length(),"sActiveInterface");
			
			string sEmvData;
			pam.berResult.getValForTag("\xFF\xCC\x11",sEmvData);
			BERTLV berEmvData(sEmvData);
			dlog_hex(sEmvData.c_str(),sEmvData.length(),"sEmvData");
			
			string sTag9f27;
			pam.berResult.getValForTag("\x9F\x27",sTag9f27);
			dlog_hex(sTag9f27.c_str(),sTag9f27.length(),"sTag9f27");
			
			// complete transaction
			if(sTag9f27.length()==1 && (sTag9f27.at(0)&0xc0)==0x80) {
				dlog_msg("online EMV transaction");
				iRc=pam.completeTransaction(PAM_UNABLE_TO_GO_ONLINE,"\xDF\xAF\x01\x22\xDF\xAF\x04\x01\x05","\x4F\x82\x9F\x36\x9F\x07\x9F\x27\x9F\x26\x9F\x34\x9F\x0D\x9F\x0E\x9F\x0F\x9F\x1A\x9F\x35\x95\x9F\x53\x9F\x2A\x9A\x9F\x41\x9F\x37\x5A\x5F\x34");
				dlog_msg("pam.completeTransaction()=%d",iRc);
				pam.testAndSetBusyFlag();
			}
		}
		
		// remove card
		iRc=pam.removeCard();
		dlog_msg("pam.removeCard()=%d",iRc);
		
		// wait a bit
		pam.testAndSetBusyFlag();
		pam.clearBusyFlag();
		SVC_WAIT(5000);
	}
	
	return(0);
}
