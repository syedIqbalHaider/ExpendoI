#ifdef VFI_PLATFORM_VOS
	#include <unistd.h>
	#include <pthread.h>
	#include <arpa/inet.h>
#else
	#include <svc.h>
#endif
#include <liblog/logsys.h> 
#include <stdio.h>
#include <libpml/pml.h>
#include <libpml/pml_abstracted_api.h>
#include <libpml/pml_port.h>
#include <libipc/ipc.h>
#include <bertlv.h>
#include <iostream>
#include "libpam.h"

using namespace za_co_verifone_bertlv;
//using namespace std;

#ifdef VFI_PLATFORM_VOS
static void *PAMThread(void *parm)
#else
static void PAMThread(int parm)
#endif
{
	PAM *pam=(PAM *)parm;
	
	#ifdef VFI_PLATFORM_VOS
	return(pam->myThread(NULL));
	#else
	return(pam->myThread(0));
	#endif
}

PAM::PAM(string sYourAppName,bool bWaitForResponse)
{
	int iRc;

	// initialise logging
//	LOG_INIT(sYourAppName.c_str(),LOGSYS_COMM,LOGSYS_PRINTF_FILTER);
	dlog_init(sYourAppName.c_str());
	dlog_msg("started %s, task no %d",sYourAppName.c_str(),get_task_id());
	
	// init properties
	iSendBusy=0;
	iThreadRunning=0;
	iBusyFlag=0;
	tLastCommandToRespond=NONE;
	tLastCommandToComplete=NONE;
	bSyncMode=bWaitForResponse;
	bAwaitingResponse=false;

	// create thread name
	sAppName=sYourAppName+"_pam";

	// start thread
	#ifdef VFI_PLATFORM_VOS
	int iret = pthread_create(&myThreadId,NULL,PAMThread,this);

	if(iret != 0){
		cout << "----->!!!!!PAM::PAM thread failed to start!!!!!<------" << endl;
	}

	#else
	myThreadId=run_thread((int)PAMThread,(int)this,4096);
	#endif

	cout << "PAM::PAM waiting for thread to start ..." << endl;

#ifdef VFI_PLATFORM_VOS
	while(!iThreadRunning) usleep(10000);	// Give IPC and pml time to initialise

	cout << "PAMlib thread running"<<endl;
#endif
}

PAM::~PAM()
{
	cout << "PAMlib desctructor"<<endl;

	iThreadRunning=0;
	sSendData="";
	com_verifone_pml::event_raise(sAppName.c_str(),pmlUserEvent);

#ifdef VFI_PLATFORM_VOS
		void *status;

		int iret = pthread_join(myThreadId, &status);

		if(iret){
			cout << "~PAM thread failed to be joined" << endl;
		}else{
			cout << "~PAM thread finished" << endl;
		}
#else
		SVC_WAIT(1000);
#endif // VFI_PLATFORM_VOS
}

int PAM::pamAvailable()
{
	return(iThreadRunning);
}

int PAM::send(string sData)
{
	while(iSendBusy){
#ifdef VFI_PLATFORM_VERIXEVO
		SVC_WAIT(500);
#else
		usleep(10000);
#endif
	}

	if(sData.length() > 0){
		bAwaitingResponse=true;
		iSendBusy=1;
	}

	sSendData=sData;
	com_verifone_pml::event_raise(sAppName.c_str(),pmlUserEvent);

	if(bSyncMode) {
		while(bAwaitingResponse) {
#ifdef VFI_PLATFORM_VERIXEVO
			SVC_WAIT(50);
#else
			usleep(10000);
#endif
		}
	}
	
	return(0);
}

int PAM::startTransaction(string sActiveInterfaces,string sInputTlv,string sRequestedTlv,short sTimeout)
{
	BERTLV berTlv;

	berTlv.addTag(string("\xdf\xaf\x01"),string("\x21"));
	if(sTimeout>0 && sTimeout<256) {
		string stTimeout("\xff");
		stTimeout.at(0)=sTimeout;
		berTlv.addTag(string("\xdf\xaf\x02"),stTimeout);
	}
	berTlv.addTag(string("\xdf\xaf\x03"),sActiveInterfaces);
	berTlv.addTag(string("\xff\xcc\x10"),sInputTlv);
	berTlv.addTag(string("\xdf\xcc\x11"),sRequestedTlv);

	return(send(berTlv.getStringData()));
}

int PAM::modifyLimits(string sDecision)
{
	BERTLV berTlv;

	printf("Decision = %02X\r\n",sDecision.c_str()[0]);

	berTlv.addTag(string("\xdf\xaf\x01"),string("\x23"));
	berTlv.addTag(string("\xdf\xaf\x04"),sDecision);


	return(send(berTlv.getStringData()));
}

int PAM::modifyLimits(string sDecision,long lFloorLimit)
{
	BERTLV berTlv;
	unsigned char ucFloorLimit[4];

	berTlv.addTag(string("\xdf\xaf\x01"),string("\x23"));
	berTlv.addTag(string("\xdf\xaf\x04"),sDecision);
#ifdef VFI_PLATFORM_VOS
	lFloorLimit=htonl(lFloorLimit);
#endif
	memcpy(ucFloorLimit,&lFloorLimit,sizeof(ucFloorLimit));
	berTlv.addTag(string("\x9f\x1b"),string((char *)ucFloorLimit,sizeof(ucFloorLimit)));

	return(send(berTlv.getStringData()));
}

int PAM::completeTransaction(string sOnlineResult,string sInputTlv,string sRequestedTlv,short sTimeout)
{
	BERTLV berTlv;

	berTlv.addTag(string("\xdf\xaf\x01"),string("\x22"));
	berTlv.addTag(string("\xdf\xaf\x04"),sOnlineResult);
	if(sTimeout>0 && sTimeout<256) {
		string stTimeout("\xff");
		stTimeout.at(0)=sTimeout;
		berTlv.addTag(string("\xdf\xaf\x02"),stTimeout);
	}
	berTlv.addTag(string("\xff\xcc\x10"),sInputTlv);
	berTlv.addTag(string("\xdf\xcc\x11"),sRequestedTlv);

	return(send(berTlv.getStringData()));
}

int PAM::apdu(unsigned char ucSlotNumber,string sApdu)
{
	BERTLV berTlv;

	berTlv.addTag(string("\xdf\xaf\x01"),string("\x24"));
	berTlv.addTag(string("\xdf\xaf\x0c"),string((char *)&ucSlotNumber,1));
	berTlv.addTag(string("\xff\xcc\x20"),sApdu);

	return(send(berTlv.getStringData()));
}

int PAM::readMedia(string sActiveInterfaces,short sTimeout)
{
	BERTLV berTlv;

	berTlv.addTag(string("\xdf\xaf\x01"),string("\x26"));
	berTlv.addTag(string("\xdf\xaf\x03"),sActiveInterfaces);
	if(sTimeout>0 && sTimeout<256) {
		string stTimeout("\xff");
		stTimeout.at(0)=sTimeout;
		berTlv.addTag(string("\xdf\xaf\x02"),stTimeout);
	}

	return(send(berTlv.getStringData()));
}

int PAM::removeCard(short sTimeout)
{
	BERTLV berTlv;

	berTlv.addTag(string("\xdf\xaf\x01"),string("\x25"));
	if(sTimeout>0 && sTimeout<256) {
		string stTimeout("\xff");
		stTimeout.at(0)=sTimeout;
		berTlv.addTag(string("\xdf\xaf\x02"),stTimeout);
	}

	return(send(berTlv.getStringData()));
}

int PAM::setGetTlv(string sActiveInterface,string sInputTlv,string sRequestedTlv)
{
	BERTLV berTlv;

	berTlv.addTag(string("\xdf\xaf\x01"),string("\x27"));
	berTlv.addTag(string("\xdf\xaf\x03"),sActiveInterface);
	berTlv.addTag(string("\xff\xcc\x10"),sInputTlv);
	berTlv.addTag(string("\xdf\xcc\x11"),sRequestedTlv);

	return(send(berTlv.getStringData()));
}

int PAM::updateEmvParameter(string sInputTlv)
{
	return(send(sInputTlv));
}

int PAM::deleteEmvParameter(string sInputTlv)
{
	return(send(sInputTlv));
}

int PAM::readEmvParameter(string sInputTlv)
{
	return(send(sInputTlv));
}

int PAM::updateCapk(string sInputTlv)
{
	return(send(sInputTlv));
}

int PAM::deleteCapk(string sInputTlv)
{
	return(send(sInputTlv));
}

int PAM::readCapk(string sInputTlv)
{
	return(send(sInputTlv));
}

int PAM::transactionStatus()
{
	BERTLV berTlv;

	berTlv.addTag(string("\xdf\xaf\x01"),string("\x71"));

	return(send(berTlv.getStringData()));
}

int PAM::cancelTransaction()
{
	BERTLV berTlv;

	berTlv.addTag(string("\xdf\xaf\x01"),string("\x72"));

	return(send(berTlv.getStringData()));
}

int PAM::testAndSetBusyFlag()
{
	while(iBusyFlag) SVC_WAIT(10);
	iBusyFlag=1;
	
	dlog_msg("testAndSetBusyFlag: OK");
	return(0);
}

int PAM::clearBusyFlag()
{
	iBusyFlag=0;
	
	dlog_msg("clearBusyFlag: OK");
	return(0);
}

LAST_COMMAND_TO_RESPOND PAM::getLastCommandToRespond()
{
	LAST_COMMAND_TO_RESPOND tRc;
	
	tRc=tLastCommandToRespond;
	tLastCommandToRespond=NONE;
	
//	dlog_msg("getLastCommandToRespond: %d",(int)tRc);
	return(tRc);
}

LAST_COMMAND_TO_RESPOND PAM::getLastCommandToComplete()
{
	LAST_COMMAND_TO_RESPOND tRc;
	
	tRc=tLastCommandToComplete;
	tLastCommandToComplete=NONE;
	
//	dlog_msg("getLastCommandToComplete: %d",(int)tRc);
	return(tRc);
}

int PAM::processEvents(com_verifone_pml::eventset_t tPmlEvents)
{
	string sInBuffer,sFrom;
	int iRc=0;
	
	dlog_msg("processEvents()");
//	printf("iq: processEvents \r\n");

	while(!tPmlEvents.empty()) {
		com_verifone_pml::event_item tEvent=tPmlEvents.back();
		tPmlEvents.pop_back();
		if(tEvent.event_id==com_verifone_pml::events::cascade) {
			dlog_msg("Got a Cascade event");
//			printf("iq: Got a Cascade event \r\n");
			com_verifone_pml::eventset_t tCascadeEvents;
			if(event_read(tEvent.evt.cascade.fd,tCascadeEvents)>0) iRc=processEvents(tCascadeEvents);
		} else if(tEvent.event_id==com_verifone_pml::events::ipc) {
			dlog_msg("Got an IPC event");
//			printf("iq: Got an IPC event \r\n");
			if(com_verifone_ipc::receive(sInBuffer,string(PAM_SERVICE_TASK_NAME),sFrom)==com_verifone_ipc::IPC_SUCCESS) {
				BERTLV berTlv(sInBuffer);
				berResult=berTlv;
				string sPacketType,sStatusCode;
				string sAid,sPan;
				string sActiveInterface,sEmvTlv,sTrack1,sTrack2,sTrack3,sSwipeStatus;
				string sManualPan,sManualExpiryDate,sManualCvv;
				string sBarcodeData,sApduResponse,sAtr;
				berTlv.getValForTag(string("\xdf\xaf\x01"),sPacketType);
				berTlv.getValForTag(string("\xdf\xaf\x04"),sStatusCode);
				if(sPacketType.length()==1) {
					dlog_msg("sPacketType=%02x",sPacketType.at(0));

//					printf("iqbal sPacketType[%02x] \r\n",sPacketType.at(0));

					switch(sPacketType.at(0)) {
						case 0xa1:	// start transaction response
							berTlv.getValForTag(string("\xdf\xaf\x03"),sActiveInterface);
							berTlv.getValForTag(string("\xff\xcc\x11"),sEmvTlv);
							berTlv.getValForTag(string("\xdf\xcc\x41"),sTrack1);
							berTlv.getValForTag(string("\xdf\xcc\x42"),sTrack2);
							berTlv.getValForTag(string("\xdf\xcc\x43"),sTrack3);
							berTlv.getValForTag(string("\xdf\xdf\x6e"),sSwipeStatus);
							berTlv.getValForTag(string("\xdf\xaf\x05"),sManualPan);
							berTlv.getValForTag(string("\xdf\xaf\x06"),sManualExpiryDate);
							berTlv.getValForTag(string("\xdf\xaf\x07"),sManualCvv);
							berTlv.getValForTag(string("\xdf\xaf\x08"),sBarcodeData);

							tLastCommandToRespond=START_TRANSACTION;
							startTransactionResult(sStatusCode,sActiveInterface,sEmvTlv,sTrack1,sTrack2,sTrack3,sSwipeStatus,sManualPan,sManualExpiryDate,sManualCvv,sBarcodeData);
							tLastCommandToComplete=START_TRANSACTION;
							clearBusyFlag();
							break;
						case 0xa2:	// complete transaction response
							berTlv.getValForTag(string("\xff\xcc\x11"),sEmvTlv);
							tLastCommandToRespond=COMPLETE_TRANSACTION;
							completeTransactionResult(sStatusCode,sEmvTlv);
							tLastCommandToComplete=COMPLETE_TRANSACTION;
							clearBusyFlag();
							break;
						case 0xa3:	// modify limits request
							berTlv.getValForTag(string("\x4f"),sAid);
							berTlv.getValForTag(string("\x5a"),sPan);
							tLastCommandToRespond=MODIFY_LIMITS;
							//dlog_msg("sLogEntry=%d",sLogEntry.length());
							modifyLimitsRequest(sAid,sPan,sInBuffer);
							tLastCommandToComplete=MODIFY_LIMITS;
							clearBusyFlag();
							break;
						case 0xa4:	// apdu response
							berTlv.getValForTag(string("\xff\xcc\x21"),sApduResponse);
							tLastCommandToRespond=APDU;
							apduResult(sStatusCode,sApduResponse);
							tLastCommandToComplete=APDU;
							clearBusyFlag();
							break;
						case 0xa5:	// remove card response
							tLastCommandToRespond=REMOVE_CARD;
							removeCardResult(sStatusCode);
							tLastCommandToComplete=REMOVE_CARD;
							clearBusyFlag();
							break;
						case 0xa6:	// read media response
							berTlv.getValForTag(string("\xdf\xaf\x03"),sActiveInterface);
							berTlv.getValForTag(string("\xdf\xcc\x03"),sAtr);
							berTlv.getValForTag(string("\xdf\xcc\x41"),sTrack1);
							berTlv.getValForTag(string("\xdf\xcc\x42"),sTrack2);
							berTlv.getValForTag(string("\xdf\xcc\x43"),sTrack3);
							berTlv.getValForTag(string("\xdf\xdf\x6e"),sSwipeStatus);
							berTlv.getValForTag(string("\xdf\xaf\x05"),sManualPan);
							berTlv.getValForTag(string("\xdf\xaf\x06"),sManualExpiryDate);
							berTlv.getValForTag(string("\xdf\xaf\x07"),sManualCvv);
							berTlv.getValForTag(string("\xdf\xaf\x08"),sBarcodeData);

							tLastCommandToRespond=READ_MEDIA;
							readMediaResult(sStatusCode,sActiveInterface,sAtr,sTrack1,sTrack2,sTrack3,sSwipeStatus,sManualPan,sManualExpiryDate,sManualCvv,sBarcodeData);
							tLastCommandToComplete=READ_MEDIA;
							clearBusyFlag();
							break;
						case 0xa7:	// set get tlv response
							berTlv.getValForTag(string("\xff\xcc\x11"),sEmvTlv);
							tLastCommandToRespond=SET_GET_TLV;
							setGetTlvResult(sStatusCode,sEmvTlv);
							tLastCommandToComplete=SET_GET_TLV;
							clearBusyFlag();
							break;
						case 0xc1:	// update capk response
							tLastCommandToRespond=UPDATE_CAPK;
							updateCapkResult(sStatusCode);
							tLastCommandToComplete=UPDATE_CAPK;
							clearBusyFlag();
							break;
						case 0xc2:	// delete capk response
							tLastCommandToRespond=DELETE_CAPK;
							deleteCapkResult(sStatusCode);
							tLastCommandToComplete=DELETE_CAPK;
							clearBusyFlag();
							break;
						case 0xc3:	// read capk response
							tLastCommandToRespond=READ_CAPK;
							readCapkResult(sStatusCode,berTlv.getStringData());
							tLastCommandToComplete=READ_CAPK;
							clearBusyFlag();
							break;
						case 0xc4:	// update emv parameter
							tLastCommandToRespond=UPDATE_EMV;
							updateEmvParameterResult(sStatusCode);
							tLastCommandToComplete=UPDATE_EMV;
							clearBusyFlag();
							break;
						case 0xc5:	// delete emv parameter
							tLastCommandToRespond=DELETE_EMV;
							deleteEmvParameterResult(sStatusCode);
							tLastCommandToComplete=DELETE_EMV;
							clearBusyFlag();
							break;
						case 0xc6:	// read emv parameter
							tLastCommandToRespond=READ_EMV;
							readEmvParameterResult(sStatusCode,berTlv.getStringData());
							tLastCommandToComplete=READ_EMV;
							clearBusyFlag();
							break;
						case 0xf1:	// transaction status response
							tLastCommandToRespond=TRANSACTION_STATUS;
							transactionStatusResult(sStatusCode);
							tLastCommandToComplete=TRANSACTION_STATUS;
							clearBusyFlag();
							break;
						case 0xf2:	// cancel transaction response
							tLastCommandToRespond=CANCEL_TRANSACTION;
							cancelTransactionResult(sStatusCode);
							tLastCommandToComplete=CANCEL_TRANSACTION;
							clearBusyFlag();
							break;
						default:	// unknown
							tLastCommandToRespond=NONE;
							tLastCommandToComplete=NONE;
							clearBusyFlag();
							break;
					}
				}
			}

			bAwaitingResponse=false;
		} else if(tEvent.event_id==com_verifone_pml::events::user1) {
			dlog_msg("Got a User1 event");
//			printf("iq: Got a User1 event [%s] \r\n",sSendData.c_str());
			if(sSendData.length()>0) {
				com_verifone_ipc::send(sSendData,string(PAM_SERVICE_TASK_NAME));
				iSendBusy=0;
			}
		}
	}

	return(iRc);
}
#include <unistd.h>

#ifdef VFI_PLATFORM_VOS
void *PAM::myThread(void *parm)
#else
void PAM::myThread(int parm)
#endif
{
	com_verifone_pml::eventset_t tPmlEvents;
	int iEventCount;
	int iRc;

#ifdef VFI_PLATFORM_VOS
	char cwd[256];
	getcwd(cwd, sizeof(cwd));
#endif

	// set working directory
	com_verifone_pml::set_working_directory(com_verifone_pml::getRAMRoot().c_str());

	// init pml
	iRc=com_verifone_pml::pml_init(sAppName.c_str());
	dlog_msg("pml_init(%s)=%d",sAppName.c_str(),iRc);

	if(iRc<0) {
#ifdef VFI_PLATFORM_VOS
		dlog_msg("myThread failed to init pml");
		return(NULL);
#else
		return;
#endif
	}

	// init event handler
	hEvent=com_verifone_pml::event_open();
	dlog_msg("event_open handle=%d",hEvent);

	if(hEvent<0) {
#ifdef VFI_PLATFORM_VOS
		dlog_msg("myThread failed to open pml");
		return(NULL);
#else
		return;
#endif
	}

	// add user1 event
	pmlUserEvent.event_id=com_verifone_pml::events::user1;
	pmlUserEvent.evt.user.flags=1;
	pmlUserEvent.evt.user.ext_flags=1;
	com_verifone_pml::event_ctl(hEvent,com_verifone_pml::ctl::ADD,pmlUserEvent);

	// init ipc
	iRc=com_verifone_ipc::init(sAppName);
	dlog_msg("com_verifone_ipc::init=%d",iRc);
	if(iRc<0) {
#ifdef VFI_PLATFORM_VOS
		dlog_msg("myThread failed to init ipc");
		return(NULL);
#else
		return;
#endif
	}

	// add ipc event
	com_verifone_pml::event_ctl(hEvent,com_verifone_pml::ctl::ADD,com_verifone_ipc::get_events_handler());
#ifdef VFI_PLATFORM_VOS
	chdir(cwd);
#endif

	// wait for pam to be available
	while(!com_verifone_ipc::is_task_available(string(PAM_SERVICE_TASK_NAME))) SVC_WAIT(1000);
	dlog_msg("%s task is available",PAM_SERVICE_TASK_NAME);

	// connect to pam
	iRc=com_verifone_ipc::connect_to(string(PAM_SERVICE_TASK_NAME));
	dlog_msg("com_verifone_ipc::connect_to(%s)=%d",PAM_SERVICE_TASK_NAME,iRc);
	if(iRc!=0) {
#ifdef VFI_PLATFORM_VOS
		dlog_msg("myThread failed to init ipc");
		return(NULL);
#else
		return;
#endif
	}

	// indicate successful thread startup
	iThreadRunning=1;
	iThreadFinished = 0;
	dlog_msg("ThreadRunning");

	// main loop
	while(1) {
		iEventCount=com_verifone_pml::event_wait(hEvent,tPmlEvents);

		if(!iThreadRunning)
			break;

		dlog_msg("iEventCount=%d, tPmlEvents.size()=%d",iEventCount,tPmlEvents.size());
//		printf("iq: iEventCount=%d, tPmlEvents.size()=%d \r\n",iEventCount,tPmlEvents.size());

		if(iThreadRunning && (iEventCount>0||tPmlEvents.size()>0)) {
			iRc=processEvents(tPmlEvents);
		}

		usleep(10000);
	}

//	if(hEvent>=0)
	com_verifone_pml::event_close(hEvent);
	com_verifone_pml::pml_deinit(sAppName.c_str());
	com_verifone_ipc::disconnect(string(PAM_SERVICE_TASK_NAME));
	com_verifone_ipc::deinit(0);

	iThreadFinished = 1;

	#ifdef VFI_PLATFORM_VOS
	return(NULL);
	#endif
}
