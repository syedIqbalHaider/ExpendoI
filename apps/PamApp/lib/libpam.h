#ifndef _LIBPAM_H_
#define _LIBPAM_H_

#include <libpml/pml.h>
#include <bertlv.h>
#include <string>
#include <iostream>

using std::string;
using std::cout;
using std::endl;

// pam task name
#define PAM_SERVICE_TASK_NAME	"CPAM"

// active interfaces
#define PAM_CONTACT_ICC			"\x21"
#define PAM_CONTACTLESS_ICC		"\x22"
#define PAM_MAGSTRIPE			"\x23"
#define PAM_BARCODE				"\x24"
#define PAM_MANUAL				"\x25"

// modify limits decision / online result
#define PAM_OK					"\x00"
#define PAM_UNABLE_TO_GO_ONLINE	"\x05"
#define PAM_FORCE_ONLINE		"\x06"
#define PAM_FORCE_DECLINE		"\x07"
#define PAM_NO_DECISION			"\x08"

// verifone africa private tags
#define VA_TAG_PACKET_TYPE										"\xDF\xAF\x01"
#define VA_TAG_TIMEOUT											"\xDF\xAF\x02"
#define VA_TAG_ACTIVE_INTERFACE									"\xDF\xAF\x03"
#define VA_TAG_STATUS_CODE										"\xDF\xAF\x04"
#define VA_TAG_MANUAL_PAN										"\xDF\xAF\x05"
#define VA_TAG_MANUAL_EXPIRY_DATE								"\xDF\xAF\x06"
#define VA_TAG_MANUAL_CVV										"\xDF\xAF\x07"
#define VA_TAG_BARCODE_DATA										"\xDF\xAF\x08"
#define VA_TAG_ICC_SLOT											"\xDF\xAF\x0C"
#define VA_TAG_CAPK_MODULUS										"\xDF\xAF\x0E"
#define VA_TAG_CAPK_EXPONENT									"\xDF\xAF\x0F"
#define VA_TAG_CAPK_CHECKSUM									"\xDF\xAF\x10"
#define VA_TAG_PARTIAL_SELECTION								"\xDF\xAF\x11"
#define VA_TAG_SECOND_TERMINAL_APPLICATION_VERSION_NUMBER		"\xDF\xAF\x12"
#define VA_TAG_RECOMMENDED_APPLICATION_NAME						"\xDF\xAF\x13"
#define VA_TAG_RANDOM_SELECTION_THRESHOLD						"\xDF\xAF\x14"
#define VA_TAG_TARGET_RANDOM_SELECTION_PERCENTAGE				"\xDF\xAF\x15"
#define VA_TAG_MAXIMUM_RANDOM_TARGET_SELECTION_PERCENTAGE		"\xDF\xAF\x16"
#define VA_TAG_TAC_DEFAULT										"\xDF\xAF\x17"
#define VA_TAG_TAC_DENIAL										"\xDF\xAF\x18"
#define VA_TAG_TAC_ONLINE										"\xDF\xAF\x19"
#define VA_TAG_DEFAULT_TDOL										"\xDF\xAF\x1A"
#define VA_TAG_DEFAULT_DDOL										"\xDF\xAF\x1B"
#define VA_TAG_FALLBACK_ALLOWED									"\xDF\xAF\x1C"
#define VA_TAG_AUTOMATIC_APPLICATION_SELECTION					"\xDF\xAF\x1D"
#define VA_TAG_TERMINAL_CATEGORY_CODE							"\xDF\xAF\x1E"
#define VA_TAG_CONTACTLESS_LIMIT								"\xDF\xAF\x1F"
#define VA_TAG_CONTACTLESS_CVM_REQUIRED_LIMIT					"\xDF\xAF\x20"
#define VA_TAG_CONTACTLESS_APPLICATION_FLOW						"\xDF\xAF\x21"
#define VA_TAG_CONTACTLESS_DISABLE_PPSE							"\xDF\xAF\x22"
#define VA_TAG_CONTACTLESS_VISA_TTQ								"\xDF\xAF\x23"
#define VA_TAG_CONTACTLESS_STATUS_CHECK							"\xDF\xAF\x24"
#define VA_TAG_CONTACTLESS_FORCE_MSTRIPE						"\xDF\xAF\x25"
#define VA_TAG_TEXT_LINE_4										"\xDF\xAF\x26"
#define VA_TAG_MENU_TITLE										"\xDF\xAF\x28"
#define VA_TAG_MENU_OPTIONS										"\xDF\xAF\x29"
#define VA_TAG_MENU_SELECTED_ITEM_INDEX							"\xDF\xAF\x2A"
#define VA_TAG_MENU_SELECTED_ITEM_TEXT							"\xDF\xAF\x2B"
#define VA_TAG_TEMPLATE_IDENTIFIER								"\xDF\xAF\x2C"
#define VA_TAG_TEMPLATE_RETURNED_FIELDS							"\xDF\xAF\x2D"
#define VA_TAG_TEMPLATE_INPUT_FIELDS							"\xDF\xAF\x2E"
#define VA_TAG_PIN_VERIFY_FLAG									"\xDF\xAF\x2F"
#define VA_TAG_TASK_ID											"\xDF\xAF\x30"
#define VA_TAG_SUPPRESS_RESPONSE								"\xDF\xAF\x31"
#define VA_TAG_CAPK_EXPIRY_DATE									"\xDF\xAF\x32"
#define VA_TAG_GUI_HTML											"\xDF\xAF\x34"
#define VA_TAG_PRINT_COMMAND									"\xDF\xAF\x35"
#define VA_TAG_PRINT_DATA										"\xDF\xAF\x36"
#define VA_TAG_STATUS_CODE_DESCRIPTION							"\xDF\xAF\x37"
#define VA_TAG_SUPPRESS_KSN_INCREMENT							"\xDF\xAF\x38"
#define VA_TAG_FILE_COMMAND										"\xDF\xAF\x40"
#define VA_TAG_FILE_DATA										"\xDF\xAF\x41"
#define VA_TAG_FILE_CRC											"\xDF\xAF\x42"
#define VA_TAG_SYSTEM_CTLS_TAPS_BEFORE_FALLBACK					"\xDF\xAF\x50"
#define VA_TAG_SYSTEM_INSERTS_BEFORE_FALLBACK					"\xDF\xAF\x51"
#define VA_TAG_SYSTEM_SWIPES_BEFORE_FALLBACK					"\xDF\xAF\x52"
#define VA_TAG_SYSTEM_DEFAULT_TIMEOUT							"\xDF\xAF\x53"
#define VA_TAG_SYSTEM_SERIAL_PORT_NAME							"\xDF\xAF\x54"
#define VA_TAG_SYSTEM_SERIAL_PORT_BITRATE						"\xDF\xAF\x55"
#define VA_TAG_SYSTEM_TCP_HOST_NAME_OR_IP						"\xDF\xAF\x56"
#define VA_TAG_SYSTEM_TCP_HOST_PORT								"\xDF\xAF\x57"
#define VA_TAG_SYSTEM_DEFAULT_KEY_SCHEME						"\xDF\xAF\x58"
#define VA_TAG_SYSTEM_COMMS_TYPE								"\xDF\xAF\x59"
#define VA_TAG_SYSTEM_USE_SSL									"\xDF\xAF\x5A"
#define VA_TAG_SYSTEM_TERMINAL_SERIAL_NUMBER					"\xDF\xAF\x60"
#define VA_TAG_SYSTEM_TERMINAL_DATE_TIME						"\xDF\xAF\x61"
#define VA_TAG_SYSTEM_VERSION									"\xDF\xAF\x62"
#define VA_TAG_SYSTEM_USER_DATA_1								"\xDF\xAF\x70"
#define VA_TAG_SYSTEM_USER_DATA_2								"\xDF\xAF\x71"
#define VA_TAG_P2PE_HASH_BLOCK									"\xDF\xAF\x7C"
#define VA_TAG_P2PE_VOLATILE_ENCRYPTED_SENSITIVE_DATA_BLOCK		"\xDF\xAF\x7D"
#define VA_TAG_P2PE_PERSISTED_ENCRYPTED_SENSITIVE_DATA_BLOCK	"\xDF\xAF\x7E"
#define VA_TAG_P2PE_SENSITIVE_KEY_BLOCK							"\xDF\xAF\x7F"
#define VA_TAG_P2PE_DATA										"\xFF\xAF\x01"

//using namespace std;
using namespace za_co_verifone_bertlv;

typedef enum {
	NONE,
	START_TRANSACTION,
	MODIFY_LIMITS,
	COMPLETE_TRANSACTION,
	APDU,
	READ_MEDIA,
	REMOVE_CARD,
	SET_GET_TLV,
	TRANSACTION_STATUS,
	CANCEL_TRANSACTION,
	UPDATE_CAPK,
	DELETE_CAPK,
	READ_CAPK,
	UPDATE_EMV,
	DELETE_EMV,
	READ_EMV
} LAST_COMMAND_TO_RESPOND;

class PAM {
	public:
		PAM(string sYourAppName,bool bWaitForResponse=false);
		virtual ~PAM();
		int startTransaction(string sActiveInterfaces,string sInputTlv,string sRequestedTlv,short sTimeout=0);
		virtual int startTransactionResult(string sStatusCode,string sActiveInterface,string sEmvTlv,string sTrack1,string sTrack2,string sTrack3,string sSwipeStatus,string sManualPan,string sManualExpiryDate,string sManualCvv,string sBarcodeData) {
			(void) sStatusCode;(void) sActiveInterface;(void) sEmvTlv;(void) sTrack1;(void) sTrack2;(void) sTrack3;(void) sSwipeStatus;(void) sManualPan;(void) sManualExpiryDate;(void) sManualCvv;(void) sBarcodeData;
			return(-1);
		}
		int modifyLimits(string sDecision);
		int modifyLimits(string sDecision,long lFloorLimit);
		virtual int modifyLimitsRequest(string sAid,string sPan,string sEmvTlv) { (void) sAid;(void) sPan;(void)sEmvTlv;	return(-1);	}
		int completeTransaction(string sOnlineResult,string sInputTlv,string sRequestedTlv,short sTimeout=0);
		virtual int completeTransactionResult(string sStatusCode,string sEmvTlv) { (void) sStatusCode;(void) sEmvTlv;return(-1);}
		int apdu(unsigned char ucSlotNumber,string sApdu);
		virtual int apduResult(string sStatusCode,string sApduResponse) {(void) sStatusCode;(void) sApduResponse;return(-1);}
		int readMedia(string sActiveInterfaces,short sTimeout=0);
		virtual int readMediaResult(string sStatusCode,string sActiveInterface,string sAtr,string sTrack1,string sTrack2,string sTrack3,string sSwipeStatus,string sManualPan,string sManualExpiryDate,string sManualCvv,string sBarcodeData) {
			(void) sStatusCode;(void) sActiveInterface;(void) sAtr;(void) sTrack1;(void) sTrack2;(void) sTrack3;(void) sSwipeStatus;(void) sManualPan;(void) sManualExpiryDate;(void) sManualCvv;(void) sBarcodeData;
			return(-1);
		}
		int removeCard(short sTimeout=0);
		virtual int removeCardResult(string sStatusCode) { (void)sStatusCode; return(-1); }
		int setGetTlv(string sActiveInterface,string sInputTlv,string sRequestedTlv);
		virtual int setGetTlvResult(string sStatusCode,string sEmvTlv) {  (void) sStatusCode;(void) sEmvTlv;return(-1);}
		int transactionStatus();
		virtual int transactionStatusResult(string sStatusCode) { (void)sStatusCode; return(-1); }
		int cancelTransaction();
		virtual int cancelTransactionResult(string sStatusCode) { (void)sStatusCode; return(-1); }
		int updateEmvParameter(string sInputTlv);
		virtual int updateEmvParameterResult(string sStatusCode) { (void)sStatusCode; return(-1); }
		int deleteEmvParameter(string sInputTlv);
		virtual int deleteEmvParameterResult(string sStatusCode) { (void)sStatusCode; return(-1); }
		int readEmvParameter(string sInputTlv);
		virtual int readEmvParameterResult(string sStatusCode,string sEmvTlv) {  (void) sStatusCode;(void) sEmvTlv;return(-1);}
		int updateCapk(string sInputTlv);
		virtual int updateCapkResult(string sStatusCode) { (void)sStatusCode; return(-1); }
		int deleteCapk(string sInputTlv);
		virtual int deleteCapkResult(string sStatusCode) { (void)sStatusCode; return(-1); }
		int readCapk(string sInputTlv);
		virtual int readCapkResult(string sStatusCode,string sEmvTlv) {  (void) sStatusCode;(void) sEmvTlv;return(-1);}
		int testAndSetBusyFlag();
		int clearBusyFlag();
		LAST_COMMAND_TO_RESPOND getLastCommandToRespond();
		LAST_COMMAND_TO_RESPOND getLastCommandToComplete();
		#ifdef VFI_PLATFORM_VOS
		void *myThread(void *parm);
		#else
		void myThread(int parm);
		#endif
		int pamAvailable();
		BERTLV berResult;
	private:
		void ignoreUnusedPrms(string p1,string p2,string p3,string p4,string p5,string p6,string p7,string p8,string p9) {
			(void)p1;(void)p2;(void)p3;(void)p4;(void)p5;(void)p6;(void)p7;(void)p8;(void)p9;
		}
		int processEvents(com_verifone_pml::eventset_t tPmlEvents);
		int send(string sData);
		int iSendBusy;
		string sSendData;
		string sAppName;
		#ifdef VFI_PLATFORM_VOS
		pthread_t myThreadId;
		#else
		int myThreadId;
		#endif
		int hEvent;
		com_verifone_pml::event_item pmlUserEvent;
		int iThreadRunning;
		int iThreadFinished;
		int iBusyFlag;
		LAST_COMMAND_TO_RESPOND tLastCommandToRespond;
		LAST_COMMAND_TO_RESPOND tLastCommandToComplete;
		bool bSyncMode;
		bool bAwaitingResponse;
};

#endif

