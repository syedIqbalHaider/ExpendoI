#include <stdio.h>
#include <string.h>
//#include <pthread.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <libipc/ipc.h>
#include "CLog.h"
#include "libalarm.h"
#include "TestAlarmAppver.h"

//using namespace vfigui;
using namespace IdleAlarmNS;

#define TESTALARM "TestAlarm"

//IdleAlarmNS::IdleAlarm masterAlarms(ALARM_MASTER);

//static CLog log;

//void testShow(std::string testNm,std::string testResult, bool nextNr = true);
//void testGetUniqueId();
//void testLocalAlarm();
//void testAlarmSetRemoteNoLocalId();
//void testAlarmSetRemoteWithLocalId();
//	testClearRemote();
//	testAlarmTriggerLocal();
//	testAlarmTriggerRemote();

class IdleAlarmTest : public IdleAlarm
{
public:
	int testNr;

	IdleAlarmTest(const std::string local_app, const std::string master_app = ALARM_MASTER)
		: IdleAlarm(local_app,master_app)
	{
		testNr = 0;
	}

	// Private access testing (works only if private made public in class declaration)

	void testShow(std::string testNm,std::string testResult, bool nextNr = true) {
		if (nextNr)
			testNm = SSTR(++testNr) + " " + testNm;
		printf("%s\t\t - %s\n",testNm.c_str(),testResult.c_str());
	}


	virtual void alarmAction(std::string alarmId, stdStringMap &alarmData) {
		std::string locId = alarmId;
		if (locKeyFor.find(alarmId) != locKeyFor.end()) {
			locId = locKeyFor.find(alarmId)->second;
		}

		testShow("AlarmAction"," (" + locId +")" );

		std::string sData = "empty";
		if (!alarmData.empty()) sData = "\n";
		for (stdStringMap::iterator it = alarmData.begin(); it != alarmData.end(); ++it) {
			sData += "    " + it->first + " = " + it->second + "\n";
		}
		testShow("   Data:",sData,false);
	}


	int showAlarms(bool mustShow=true) {
		//typedef std::multimap<time_t,std::string> ::iterator timeIt;
		std::string sAlarm = "\n" + getAlarmList();
		if (mustShow) testShow("   Alarms:",sAlarm,false);
		if (locKeyFor.size() > timeAlarmMap.size())
			return locKeyFor.size();
		else
			return timeAlarmMap.size();
	}

	std::string assertExp(bool &bRes, bool bExp,std::string msg) {
		bRes = bRes && bExp;
		if (bExp)
			return "Ok";
		else
			return "Failed (" +msg + ")";
	}

	int testGetUniqueId() {
		bool isOk = true;
		testShow("----------testGetUniqueId","",false);
		std::string sId = getUniqueAlarmId(ALTEST_NAME,"[ownid]");
		testShow("GetOwnUniqueId",assertExp(isOk,sId.find("[ownid]")>0,"OwnId not used"));

		sleep(1);
		sId = getUniqueAlarmId(ALTEST_NAME,"");
		testShow("GetNotOwnUniqueId",assertExp(isOk,sId.find(ALTEST_NAME)>0,"Test name not used"));

		return (isOk && (0 == showAlarms(false))) ? 1 : 0;
	}

	void testLocalAlarm() {

		time_t t = time(NULL);
		testShow("----------testLocalAlarm","",false);

		std::string alId = alarmAddLocal(ALTEST_NAME,t);
		testShow("addLocalAlarm1",alId);
		showAlarms();
		bool wasCleared = alarmClear(ALTEST_NAME,alId,false);
		testShow("clearLocalAlarm",wasCleared ? "Ok" : "FAILURE!");
		showAlarms();

		std::string alIdA = alarmAddLocal(ALTEST_NAME,t);
		testShow("addLocalAlarm2",alIdA);
		sleep(1);
		t = time(NULL);
		std::string alIdB = alarmAddLocal(ALTEST_NAME,t);
		testShow("addLocalAlarm3",alIdB);
		showAlarms();

		testShow("Trigger1 ",alIdA);
		alarmTrigger(alIdA,false);
		testShow("Trigger2 ",alIdB);
		alarmTrigger(alIdB,false);

		wasCleared = alarmClear(ALTEST_NAME,"",false);
		testShow("clearLocalAlarmAll",wasCleared ? "Ok" : "FAILURE!");
		showAlarms();
	}

	void testAlarmSetRemoteNoLocalId() {

		testShow("----------testAlarmSetRemoteNoLocalId","",false);
		time_t t = time(NULL) + 3600;

		//sleep(1);
		std::string alId = alarmSet(t);
		testShow("addRemoteNoLocalId",alId);
		showAlarms();
		bool wasCleared = alarmClear(alId);
		testShow("clearRemoteNoLocal",wasCleared ? "Ok" : "FAILURE!");
		showAlarms();

		//sleep(1);
		t = time(NULL) + 3605;
		testShow("alarmSetNoLocalId1",alarmSet(t));
		//sleep(1);
		t = time(NULL) + 3610;
		testShow("alarmSetNoLocalId2",alarmSet(t));
		showAlarms();
		wasCleared = alarmClear("");
		testShow("clearRemoteAlarmAll",wasCleared ? "Ok" : "FAILURE!");
		showAlarms();
	}

	void testAlarmSetRemoteWithLocalId() {

		testShow("----------testAlarmSetRemoteWithLocalId","",false);
		time_t t = time(NULL) + 3600;

		std::string alId = alarmSet(t,"OwnAA");
		bool isOk = true;
		testShow("alarmSetWithLocalId1",assertExp(isOk,alId.find("OwnAA")>0,"OwnAA not used"));
		showAlarms();
		bool wasCleared = alarmClear("OwnAA");
		testShow("clearRemoteWithLocal",wasCleared ? "Ok" : "FAILURE!");
		showAlarms();

		t = time(NULL) + 3650;
		alId = alarmSet(t,"OwnBB");
		testShow("alarmSetWithLocalId2",assertExp(isOk,alId.find("OwnBB")>0,"OwnBB not used"));

		t = time(NULL) + 3655;
		alId = alarmSet(t,"OwnCC");
		testShow("alarmSetWithLocalId3",assertExp(isOk,alId.find("OwnCC")>0,"OwnCC not used"));
		showAlarms();
		wasCleared = alarmClear("");
		testShow("clearRemoteWithLocalAlarmAll",wasCleared ? "Ok" : "FAILURE!");
		showAlarms();
	}

	int WaitResponse(const std::string app_name, const long timeout_secs, std::string &response, std::string &ipcFrom)
	{
		(void) app_name;
		struct timeb start_tp;
		struct timeb current_tp;

		ftime(&start_tp);

		response = "";
		//int i =0;
		while(1){
			//printf(("Waiting for " + app_name + "...\n").c_str());
			//printf((SSTR(++i)+"\n").c_str());
			if(com_verifone_ipc::check_pending(""/*app_name*/)==com_verifone_ipc::IPC_SUCCESS){
				if((com_verifone_ipc::receive(response,""/*app_name*/,ipcFrom)==com_verifone_ipc::IPC_SUCCESS)){
					break;
				}
			}

			// Check the timeout value
			ftime(&current_tp);
			if(timeout_secs <= current_tp.time - start_tp.time)
				return 0;
			sleep(1);
			//usleep(150*1000); // sleep for 150 ms
		}
		//printf("\n");
		return (int)response.length();
	}

	void testAlarmTriggerRemote() {
		testShow("----------testAlarmTriggerRemote","",false);
		time_t t = time(NULL) + 10;
		bool isOk = true;

		std::string alId = alarmSet(t,"OwnAA");
		t = time(NULL) + 20;
		alarmSet(t,"OwnBB");
		showAlarms();

		// Wait until 2 triggers has happened
		std::string response;
		std::string ipcFrom;
		sleep(1);
		testShow("testAlarmTriggerRemote1","Wait for trigger OwnAA",false);
		int len = WaitResponse(appMaster,50, response, ipcFrom);
		if (len <= 0)
			response = "Error: First Trigger timeout after 50 secs";
		else {
			response = alarmProcessIPC(ipcFrom,response);
		}
		testShow("testAlarmTriggerRemote1",assertExp(isOk,response == "Ok",response));
		response = "";
		ipcFrom = "";
		testShow("testAlarmTriggerRemote1","Wait for trigger OwnBB",false);
		len = WaitResponse(appMaster,50, response, ipcFrom);
		if (len <= 0)
			response = "Error: 2nd Trigger timeout after 50 secs.";
		else
			response = alarmProcessIPC(ipcFrom,response);

		testShow("testAlarmTriggerRemote2",assertExp(isOk,response == "Ok",response));
	}
};

int main()
{
	com_verifone_ipc::init(ALTEST_NAME);

	IdleAlarmTest test(ALTEST_NAME);
	// Initialize

	// Test
	test.testGetUniqueId();
	test.testLocalAlarm();
	test.testAlarmSetRemoteNoLocalId();
	test.testAlarmSetRemoteWithLocalId();
//	testAlarmTriggerLocal();
	test.testAlarmTriggerRemote();

	sleep(5);// allow remote to process last IPC response

	return(0);
}

