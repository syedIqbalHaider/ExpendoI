#include <string>
#include "bertlv.h"

//using namespace std;
using namespace za_co_verifone_bertlv;

BERTLV::BERTLV(const string &data)
{
	bertlvData=data;
}

int BERTLV::getTag(int startPosition,string &tag)
{
	unsigned int iIndex=0;
	
	while(startPosition+iIndex<bertlvData.length()) {
		if(iIndex==0 && (bertlvData.at(startPosition+iIndex)&0x1f)!=0x1f) break;
		if(iIndex>0 && (bertlvData.at(startPosition+iIndex)&0x80)!=0x80) break;
		iIndex++;
	}
	iIndex++;
	
	if(startPosition+iIndex>bertlvData.length()) return(0);
	
	tag=bertlvData.substr(startPosition,iIndex);
	
	return(startPosition+iIndex);
}

int BERTLV::getLen(unsigned int startPosition,string &len)
{
	unsigned int iIndex=0;
	
	if(startPosition>=bertlvData.length()) return(0);
	if((bertlvData.at(startPosition)&0x80)==0x80) {
		iIndex=1+(bertlvData.at(startPosition)&0x7f);
	} else {
		iIndex=1;
	}
	
	len=bertlvData.substr(startPosition,iIndex);
	
	return(startPosition+iIndex);
}

unsigned long BERTLV::berLenToLong(const string &len)
{
	unsigned long ulResult=0;
	
	for(unsigned int iIndex=0;iIndex<len.length();iIndex++) {
		ulResult<<=8;
		ulResult+=len.at(iIndex);
	}
	
	return(ulResult);
}

int BERTLV::getTagLenVal(unsigned int startPosition,string &tag,string &len,string &val)
{
	unsigned int iLenPos;
	unsigned int iValPos;
	unsigned long ulValLength;
	
	if(startPosition>bertlvData.length()) return(0);
	
	iLenPos=getTag(startPosition,tag);
	if(iLenPos==0) return(0);
	
	iValPos=getLen(iLenPos,len);
	if(iValPos==0) return(0);
	
	ulValLength=berLenToLong(len);
	val=bertlvData.substr(iValPos,ulValLength);
	
	return(iValPos+ulValLength);
}

void BERTLV::resetPosition()
{
	bertlvIndex=0;
}

int BERTLV::getNextPosition()
{
	string tag,len,val;
	
	bertlvIndex=getTagLenVal(bertlvIndex,tag,len,val);
	
	return(bertlvIndex);
}

int BERTLV::getValForTag(const string &tag,string &val)
{
	string myTag,myLen,myVal;
	int iPos;
	int iSaveBertlvIndex=bertlvIndex;
	
	iPos=0;
	resetPosition();
	while(1) {
		getTagLenVal(iPos,myTag,myLen,myVal);
		if(tag==myTag) {
			bertlvIndex=iSaveBertlvIndex;
			val=myVal;
			return(1);
		}
		iPos=getNextPosition();
		if(iPos==0) break;
	}
	
	bertlvIndex=iSaveBertlvIndex;	
	return(0);
}

void BERTLV::longToBerLen(unsigned long ulLength,string &len)
{
	char cLength[5];
	
	if(ulLength<0x80) {
		cLength[0]=(char)ulLength;
		len=string(cLength,1);
	} else if(ulLength<0x100) {
		cLength[0]=0x81;
		cLength[1]=(char)ulLength;
		len=string(cLength,2);
	} else if(ulLength<0x10000) {
		cLength[0]=0x82;
		cLength[1]=(ulLength>>8)&0xff;
		cLength[2]=ulLength&0xff;
		len=string(cLength,3);
	} else if(ulLength<0x1000000) {
		cLength[0]=0x83;
		cLength[1]=(ulLength>>16)&0xff;
		cLength[2]=(ulLength>>8)&0xff;
		cLength[3]=ulLength&0xff;
		len=string(cLength,4);
	} else {
		cLength[0]=0x84;
		cLength[1]=(ulLength>>25)&0xff;
		cLength[2]=(ulLength>>16)&0xff;
		cLength[3]=(ulLength>>8)&0xff;
		cLength[4]=ulLength&0xff;
		len=string(cLength,5);
	}
}

int BERTLV::addTag(const string &tag,const string &val)
{
	string myVal,berLen;
	
	if(getValForTag(tag,myVal)) return(0);
	
	longToBerLen(val.length(),berLen);
	
	bertlvData=bertlvData+tag+berLen+val;
	return(1);
}

int BERTLV::updateTag(const string &tag,const string &val)
{
	deleteTag(tag);
	return(addTag(tag,val));
}

int BERTLV::deleteTag(const string &tag)
{
	string myTag,myLen,myVal;
	int iPos;
	int iRc;
	
	iPos=0;
	resetPosition();
	while(1) {
		iRc=getTagLenVal(iPos,myTag,myLen,myVal);
		if(tag==myTag) {
			bertlvData=bertlvData.substr(0,iPos)+bertlvData.substr(iRc);
			return(1);
		}
		iPos=getNextPosition();
		if(iPos==0) break;
	}
	
	return(0);
}
