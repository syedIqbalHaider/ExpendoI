#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <libpml/pml.h>
#include <libipc/ipc.h>
#include <gui/gui.h>
#include "Serial.h"
#include "bertlv.h"
// Compile: 
// arm-unknown-linux-gnueabi-g++ hello.cpp -I./ -c
//
// Link:
// arm-unknown-linux-gnueabi-g++ -g  -o hello.out hello.o libfbdisplay.a liblog.a

#define MY_TASK_NAME "SERIALAPP"

//using namespace std;
using namespace za_co_verifone_bertlv;

int main()
{
	int iRc;
	int iState=0;
	char szCode[2+1];
	char szDescription[41+1];
	long lAmount=0;
	unsigned int uiResultLen;
	unsigned char ucResult[2048];

	string sFrom;
	string sInBuffer;

	// Open serial Port and Start Thread
	iRc=SerialOpen();
	if(iRc<0) return(-1);

	// init ipc 
	iRc=com_verifone_ipc::init(string(MY_TASK_NAME));
	if(iRc<0) return(-2);

	// main loop
	while(1) {
		sFrom.clear();
		while(com_verifone_ipc::check_pending(sFrom)==com_verifone_ipc::IPC_SUCCESS) {
			if(com_verifone_ipc::receive(sInBuffer,sFrom,sFrom)==com_verifone_ipc::IPC_SUCCESS){
				BERTLV result(sInBuffer);
				string sTagDFAF01;
				string sTagFFCC10;
				string sTag9C;
				string sTag9F02;

				result.getValForTag(string("\xDF\xAF\x01"),sTagDFAF01);
				result.getValForTag(string("\xFF\xCC\x10"),sTagFFCC10);

				if(sTagFFCC10.length()>0){
					BERTLV emv(sTagFFCC10);

					emv.getValForTag(string("\x9C"),sTag9C);
					emv.getValForTag(string("\x9F\x02"),sTag9F02);
				}

				// ***************************************************
				// Process the Transaction
				// ***************************************************
				if(sTagDFAF01.at(0)==0x21&&iState==0){
					if(sTag9C.at(0)==0x00){
						char szTemp[12+1];

						sprintf(szTemp,"%02X%02X%02X%02X%02X%02X",sTag9F02.at(0),sTag9F02.at(1),sTag9F02.at(2),sTag9F02.at(3),sTag9F02.at(4),sTag9F02.at(5));
						lAmount=atol(szTemp);

						iState=1;

						SerialPerformTransacion(lAmount);
					}else if(sTag9C.at(0)==0x20){
						iState=1;
						SerialReverseTransaction();
					}
				}else if(sTagDFAF01.at(0)==0x71){
					// ***************************************************
					// Send back the status
					// ***************************************************
					SerialGetTransactionStatus((unsigned char *)szCode,(unsigned char *)szDescription);
					if(strlen(szCode)&&strlen(szDescription)){
						int iIndex;
						int iCode;
						char szMessage[2048+1];

						iIndex=0;

						sprintf(&szMessage[iIndex],"\xDF\xAF\x01\x01\xF1");
						iIndex+=5;

						iCode=atoi(szCode);
						sprintf(&szMessage[iIndex],"\xDF\xAF\x04\x01%c",iCode);
						iIndex+=5;

						sprintf(&szMessage[iIndex],"\xDF\xAF\x32%c",strlen(szDescription));
						iIndex+=4;

						sprintf(&szMessage[iIndex],"%s",szDescription);
						iIndex+=strlen(szDescription);

						com_verifone_ipc::send(string(szMessage,iIndex),sFrom);
					}
				}else if(iState==1){
					// ***************************************************
					// Return IPC with Busy
					// ***************************************************
					int iIndex;
					char szMessage[2048+1];

					memset(szMessage,0,sizeof(szMessage));

					iIndex=0;

					sprintf(&szMessage[iIndex],"\xDF\xAF\x01\x01\xA1");
					iIndex+=5;

					memcpy(&szMessage[iIndex],"\xDF\xAF\x04\x01\x31",5);
					iIndex+=5;

					com_verifone_ipc::send(string(szMessage,iIndex),sFrom);
				}

			}
		}

		// *********************************************************
		// This will unpack all messages received on the serial port
		// Do Not remove
		// Needs to be here to ensure non blocking serialApp
		// *********************************************************
		memset(szCode,0,sizeof(szCode));
		memset(szDescription,0,sizeof(szDescription));
		iRc=SerialUnpackMessages(ucResult,&uiResultLen);
		if(iRc==1&&iState==1&&uiResultLen>0){
			com_verifone_ipc::send(string((char *)ucResult,uiResultLen),sFrom);
			iState=0;
		}

	}

	// we should never get here
	return(0);
}

