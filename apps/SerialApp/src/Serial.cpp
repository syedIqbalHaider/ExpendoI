#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <svc.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <Serial.h>
#include <pthread.h>

#define ETX 0x03
#define STX 0x02
#define ACK 0x06
#define NAK 0x15
#define EOT 0x04
#define ESC 0x1B

pthread_t tSerial;

int iThreadRunning=0;					// State of the Thread
int gCommsHandleKiosk=-1;				// Global Serial Comms Handle
int giSerialPacketRxLen = 0;			// Length of the Packet Received
unsigned char gucSerialPacketRx[512];	// Data Received
int giSerialPacketTxLen=0;				// Length of Data Sent
unsigned char gucSerialPacketTx[512]; 	// Data Sent
int giPacketAvailable=0;
char gszCode[2+1];
char giszDescription[40+1];

// This function does the following:
// Opens the serial port "/dev/ttyAMA1"
// Set the port to 19200 8N1 no parity
// Start the serial thread
int SerialOpen(void)
{
    int iRc;
    struct termios options;

    //gCommsHandleKiosk=open("/dev/ttyAMA1",O_RDWR|O_NOCTTY|O_NDELAY);
    gCommsHandleKiosk=open("/dev/ttyUSB0",O_RDWR|O_NOCTTY|O_NDELAY);
    if(gCommsHandleKiosk<0){
    	return(-1);
    }
    memset(&options,0,sizeof(options));
//    options.c_cflag=B19200|CS8|CLOCAL|CREAD;
    options.c_cflag=B115200|CS8|CLOCAL|CREAD;
    options.c_iflag=IGNPAR;
    tcsetattr(gCommsHandleKiosk,TCSANOW,&options);

    iRc=pthread_create(&tSerial,NULL,&SerialThread,NULL);
	if (iRc!=0){
		return(-2);
	}
    sleep(1);

    return(1);
}

// This function for the following:
// Closes the serial port
// Stops the thread
int SerialClose(void)
{
	close(gCommsHandleKiosk);
	iThreadRunning=0;
	return(1);
}

// This function in the serial thread
// DO NOT MAKE CHANGES EVER
void *SerialThread(void */*arg*/)
{
	int iByteRead=0;
	static int iState=0;
    static unsigned char ucLrc=0;
    static bool bEsc=false;
    static unsigned char ucPacket[8192];
    static int iPacketIndex=0;
    static int iTxRetry=0;
    static unsigned char ucTxPacket[8192];
    static unsigned int iTxPacketLength=0;
    static unsigned char ucTxTrace[8192];
    unsigned char ucData;
	time_t tExpire=0;
	
	iThreadRunning=1;

	while(iThreadRunning==1){
		iByteRead=read(gCommsHandleKiosk, (char *) &ucData, 1);
		if(iByteRead==1){
        	if(ucData==STX) {
            	iState=0;
            }
            switch(iState) {
            	case 0:	// wait stx
                	if(ucData==STX) {
                        bEsc=false;
                        ucLrc=0;
                        iPacketIndex=0;
                        memset(ucPacket,0,sizeof(ucPacket));
                    	iState=1;
                    }
                    break;
                case 1:	// data, wait etx
                	if(ucData==ETX) {
                    	iState=2;
                        break;
                    }
                    if(ucData==ESC) {
                    	bEsc=true;
                        break;
                    }
                    if(bEsc) {
                    	bEsc=false;
                        ucData-=(unsigned char)0x20;
                    }
                    ucLrc^=ucData;
                    ucPacket[iPacketIndex++]=ucData;
                	break;
                case 2:	// lrc
                    if(ucData==ESC) {
                    	bEsc=true;
                        break;
                    }
                    if(bEsc) {
                    	bEsc=false;
                        ucData-=(unsigned char)0x20;
                    }else
                    if(ucData==ucLrc) {
                    	if(giPacketAvailable==1){
                    		write(gCommsHandleKiosk, "\x04", 1);
                    	}else{
                    		write(gCommsHandleKiosk, "\x06", 1);
                    		memcpy(gucSerialPacketRx,ucPacket,iPacketIndex);
                    		giSerialPacketRxLen=iPacketIndex;
                    		giPacketAvailable=1;
                    	}
                    } else {
                    	write(gCommsHandleKiosk, "\x15", 1);
                    }
                    iState=0;
                	break;
                case 3:	// waiting for a ack/nak for tx packet
                	if(ucData==ACK) {
                		giSerialPacketTxLen=0;
                    	iState=0;
                        break;
                    } else if(ucData==NAK) {
                        if(--iTxRetry>0) {
                        	write(gCommsHandleKiosk, ucTxPacket, iTxPacketLength);
                            tExpire=time(NULL)+2;
                        } else {
                        	giSerialPacketTxLen=0;
                        	iState=0;
                        }
                    }
                	break;
            }
        } else if(iState==0 && giSerialPacketTxLen>0) {
        	iTxRetry=3;
            iState=3;
            ucLrc=0;
            ucTxPacket[0]=STX;
            iTxPacketLength=1;
            for(int iIndex=0;iIndex<giSerialPacketTxLen;iIndex++) {
            	ucData=gucSerialPacketTx[iIndex];
            	ucLrc^=ucData;
                if(ucData==STX || ucData==ETX || ucData==ESC) {
					ucTxPacket[iTxPacketLength++]=ESC;
                    ucData+=(unsigned char)0x20;
                }
                ucTxPacket[iTxPacketLength++]=ucData;
            }
            ucTxPacket[iTxPacketLength++]=ucTxTrace[giSerialPacketTxLen+1]=ETX;
            if(ucLrc==STX || ucLrc==ETX || ucLrc==ESC) {
            	ucTxPacket[iTxPacketLength++]=ESC;
                ucLrc+=(unsigned char)0x20;
            }
            ucTxPacket[iTxPacketLength++]=ucLrc;
            write(gCommsHandleKiosk, ucTxPacket, iTxPacketLength);
            tExpire=time(NULL)+2;
        } else if(iState==3 && time(NULL)>tExpire) {
            if(--iTxRetry>0) {
                write(gCommsHandleKiosk, ucTxPacket, iTxPacketLength);
            	tExpire=time(NULL)+2;
            } else {
            	giSerialPacketTxLen=0;
                iState=0;
            }
        } else {
        	sleep(1);
        }

	}
	return(NULL);
}

// This function build the Transaction request
// Serial interface specification v2.20
int SerialPerformTransacion(long lAmount)
{
	int iLocalLength=0;

	memset(gucSerialPacketTx,0,sizeof(gucSerialPacketTx));

	gucSerialPacketTx[iLocalLength]='C';
	iLocalLength++;
	memcpy(&gucSerialPacketTx[iLocalLength],"00",2);
	iLocalLength+=2;
	sprintf((char *)&gucSerialPacketTx[iLocalLength],"%012ld",lAmount);
	iLocalLength+=12;
	memcpy(&gucSerialPacketTx[iLocalLength],"                     ",21);
	iLocalLength+=21;

	giSerialPacketTxLen=iLocalLength;
	
	return(1);

}

// This function populates the status code and description for IPC
// Serial interface specification v2.20
int SerialGetTransactionStatus(unsigned char *ucCode, unsigned char *pszDescription)
{
	memcpy(ucCode,gszCode,strlen(gszCode));
	memcpy(pszDescription,giszDescription,strlen(giszDescription));
	return(1);

}
// This function build the financial completion status message
// Serial interface specification v2.20
int SerialReverseTransaction(void)
{
	int iLocalLength=0;

	memset(gucSerialPacketTx,0,sizeof(gucSerialPacketTx));

	gucSerialPacketTx[iLocalLength]='F';
	iLocalLength++;
	memcpy(&gucSerialPacketTx[iLocalLength],"01",2);
	iLocalLength+=2;

	giSerialPacketTxLen=iLocalLength;
	
	return(1);
}

// This function unpacks the responses from the serial thread found in
// Serial interface specification v2.20
// and rebuild the responses according to
// ServusI interface Specification Version 1.9.
int SerialUnpackMessages(unsigned char *pucResult,unsigned int *puiLength)
{
	int iRc=0;

	if(giPacketAvailable==1){
		if (memcmp(&gucSerialPacketRx[0], "A", 1) == 0){
			giPacketAvailable=0;
		}
		if (memcmp(&gucSerialPacketRx[0], "C", 1) == 0){
			int iIndex;
			char szMessage[2048+1];

			memset(szMessage,0,sizeof(szMessage));
			iIndex=0;
			sprintf(&szMessage[iIndex],"\xDF\xAF\x01\x01\xA1");
			iIndex+=5;

			if(memcmp(&gucSerialPacketRx[1],"01",2)==0){
				memcpy(&szMessage[iIndex],"\xDF\xAF\x04\x01\x00",5);
				iIndex+=5;

				sprintf(&szMessage[iIndex],"\xFF\xCC\x11%c",(giSerialPacketRxLen-5));
				iIndex+=4;

				memcpy(&szMessage[iIndex],&gucSerialPacketRx[5],giSerialPacketRxLen-5);
				iIndex+=giSerialPacketRxLen-5;

			}else{
				sprintf(&szMessage[iIndex],"\xDF\xAF\x04\x01\x30");
				iIndex+=5;

			}
			memcpy(pucResult,szMessage,iIndex);
			*puiLength=iIndex;
			giPacketAvailable=0;
			iRc=1;
		}
		if (memcmp(&gucSerialPacketRx[0], "D", 1) == 0){
			giPacketAvailable=0;
		}
		if (memcmp(&gucSerialPacketRx[0], "E", 1) == 0){
			int iCode;
			memset(gszCode,0,sizeof(gszCode));
			memset(giszDescription,0,sizeof(giszDescription));
			memcpy(gszCode,&gucSerialPacketRx[1],2);
			iCode=atoi(gszCode);
			switch (iCode) {
				case 0:
					memcpy(giszDescription,"Terminal Ready",strlen("Terminal Ready"));
				break;
				case 1:
					memcpy(giszDescription,"Terminal Off-line",strlen("Terminal Off-line"));
				break;
				case 2:
					memcpy(giszDescription,"Transaction Going On-line",strlen("Transaction Going On-line"));
				break;
				case 3:
					memcpy(giszDescription,"Waiting For Card Insert",strlen("Waiting For Card Insert"));
				break;
				case 4:
					memcpy(giszDescription,"Card Inserted",strlen("Card Inserted"));
				break;
				case 5:
					memcpy(giszDescription,"Card Removed",strlen("Card Removed"));
				break;
				case 6:
					memcpy(giszDescription,"Card Swiped",strlen("Card Swiped"));
				break;
				case 7:
					memcpy(giszDescription,"Communication Error",strlen("Communication Error"));
				break;
				case 8:
					memcpy(giszDescription,"Waiting For Host Response",strlen("Waiting For Host Response"));
				break;
				case 9:
					memcpy(giszDescription,"Host Response Received",strlen("Host Response Received"));
				break;
				case 10:
					memcpy(giszDescription,"Terminal Busy",strlen("Terminal Busy"));
				break;
				case 11:
					memcpy(giszDescription,"Settlement Starting",strlen("Settlement Starting"));
				break;
				case 12:
					memcpy(giszDescription,"Settlement Complete",strlen("Settlement Complete"));
				break;
				case 13:
					memcpy(giszDescription,"Awaiting Card holder Verification",strlen("Awaiting Card holder Verification"));
				break;
				case 14:
					memcpy(giszDescription,"Awaiting Amount Confirmation",strlen("Awaiting Amount Confirmation"));
				break;
				case 15:
					memcpy(giszDescription,"Awaiting EMV Card Removal",strlen("Awaiting EMV Card Removal"));
				break;
				case 16:
					memcpy(giszDescription,"Card Velocity Exceeded",strlen("Card Velocity Exceeded"));
				break;
				case 17:
					memcpy(giszDescription,"Awaiting Account Selection",strlen("Awaiting Account Selection"));
				break;
				case 18:
					memcpy(giszDescription,"Awaiting Budget Type Selection",strlen("Awaiting Budget Type Selection"));
				break;
				case 19:
					memcpy(giszDescription,"Awaiting Budget Period Selection",strlen("Awaiting Budget Period Selection"));
				break;
				case 20:
					memcpy(giszDescription,"Batch File Empty",strlen("Batch File Empty"));
				break;
				case 21:
					memcpy(giszDescription,"Parameter Download Starting",strlen("Parameter Download Starting"));
				break;
				case 22:
					memcpy(giszDescription,"Parameter Download Complete",strlen("Parameter Download Complete"));
				break;
				default:
					memcpy(giszDescription,"Unknown",strlen("Unknown"));
				break;
			}
			giPacketAvailable=0;
		}
		if (memcmp(&gucSerialPacketRx[0], "F", 1) == 0){
			int iIndex;
			char szMessage[2048+1];

			memset(szMessage,0,sizeof(szMessage));
			iIndex=0;
			sprintf(&szMessage[iIndex],"\xDF\xAF\x01\x01\xA1");
			iIndex+=5;

			if(memcmp(&gucSerialPacketRx[1],"00",2)==0){
				memcpy(&szMessage[iIndex],"\xDF\xAF\x04\x01\x00",5);
			}else{
				sprintf(&szMessage[iIndex],"\xDF\xAF\x04\x01\x30");

			}
			iIndex+=5;
			memcpy(pucResult,szMessage,iIndex);
			*puiLength=iIndex;
			giPacketAvailable=0;
			iRc=1;
		}
	}
    return(iRc);
}

// END OF FILE

