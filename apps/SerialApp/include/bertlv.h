#ifndef _BERTLV_H_
#define _BERTLV_H_

#include <string>

//using namespace std;
using std::string;

namespace za_co_verifone_bertlv {
	class BERTLV {
		public:
			BERTLV() { };
			BERTLV(const string &data);
			~BERTLV() { };
			int getTagLenVal(unsigned int startPosition,string &tag,string &len,string &val);
			void resetPosition();
			int getNextPosition();
			int getValForTag(const string &tag,string &val);
			int addTag(const string &tag,const string &val);
			int updateTag(const string &tag,const string &val);
			int deleteTag(const string &tag);
		private:
			string bertlvData;
			int bertlvIndex;
			int getTag(int startPosition,string &tag);
			int getLen(unsigned int startPosition,string &len);
			unsigned long berLenToLong(const string &len);
			void longToBerLen(unsigned long ulLength,string &len);
	};
}

#endif
