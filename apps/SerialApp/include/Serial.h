int SerialOpen(void);
int SerialClose(void);
void *SerialThread(void */*arg*/);
int SerialPerformTransacion(long lAmount);
int SerialGetTransactionStatus(unsigned char *ucCode, unsigned char *pszDescription);
int SerialReverseTransaction(void);
int SerialUnpackMessages(unsigned char *pucResult,unsigned int *puiLength);
// END OF FILE


