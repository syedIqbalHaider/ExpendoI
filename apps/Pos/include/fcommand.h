#ifndef FCOMMAND_H_
#define FCOMMAND_H_

#include "posmessage.h"

using namespace com_verifone_posmessage;

namespace com_verifone_fcommand {

	class FCommand : public PosMessage {
		public:
			FCommand();

			bool unpack(unsigned char *request,unsigned int length);
			bool process(void);
			bool pack(void);
			bool pack(char *respCode);

		private:
			bool encapsulate(void);
			bool unpackRespTLVs(std::map<std::string, std::string> &txresult);
	};
}

#endif /* FCOMMAND_H_ */
