/*
 * CLog.h
 *
 *  Created on: Dec 4, 2013
 *      Author: vfisdk
 */

#ifndef CLOG_H_
#define CLOG_H_

#include <iostream>
#include <string>

#define TO_STR_A( A ) #A
#define TO_STR( A ) TO_STR_A( A )
#define MSG_INFO ":"__FILE__ "->line " TO_STR( __LINE__ ) ": "

class CLog
{
public:
	CLog(std::string appname);
	void message(std::string message);
	void uimsg(std::string message);
	std::string from_int(int x);

private:
	std::string app_name;
};

#endif /* CLOG_H_ */
