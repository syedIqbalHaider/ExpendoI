#ifndef POSMESSAGE_H_
#define POSMESSAGE_H_
#include <map>
#include "bertlv.h"
#include "clistener.h"
//#include <libda/cbatchrec.h>

using namespace com_verifone_listener;
using namespace za_co_verifone_bertlv;
//using namespace com_verifone_batchrec;

namespace com_verifone_posmessage {

extern CListener listener;

	enum REQ_TYPE {
		GET_STATUS,
		GET_ACQUIRER_DATA,
		TRANSACTION_REQUEST,
		REVERSAL,
		TRANSACTION_LIST,
		SETTLEMENT_TOTALS,
		VOID
	};


	class PosMessage {

		public:
			PosMessage();
			virtual ~PosMessage() {}

			virtual bool unpack(unsigned char *request,unsigned int length) =0;
			virtual bool pack(void)=0;
			virtual bool process(void) =0;

			int getResponseLen(void);
			unsigned char* getResponse(void);

			bool isBusyCannotProcess(void);
			bool isDoNotShowCardRemoval(void);
			bool isCheckCardRemovalFlag(void);
			bool isTransferComplete(void);
			bool isBankingInProgress(void);
			bool isParametersInProgress(void);
			REQ_TYPE getRequestType(void);

			//iqbal_audi_211216
			void ecrSaleDataDisplay(void);
			void ecrSettlementDataDisplay(bool isBatchProcessSucessfully);
			void versionDetails(void);

		protected:
			virtual bool encapsulate(void)=0;
			virtual bool unpackRespTLVs(std::map<std::string, std::string> &txresult) = 0;

			bool unpackReqTLVs(std::string tlvs);

			bool packBusyCannotProcess(void);

			std::string hexToAscii(unsigned char *in,int inLen);
			std::string bcdToString(const char *data, unsigned int len);
			void swopBuffer(char *in, char *out);
			string zero_pad(int num, int pad_count);
			string zero_pad(string str, int pad_count);	//iqbal_audi_271216
			string space_pad(string str, int pad_count);
			void FilterEMVTags(string tags, string &filtered_tags);

			//iqbal_audi_271216
			int ecrData( unsigned char buff[],const unsigned char * tag, int const tagLen, bool const seperator,
					const unsigned char * value, int const valueLen, bool const lineSeperator);


			std::string serialNo;
			std::string merchantNo;
			std::string terminalId;
			std::string amount;
			std::string amountText;
			std::string status;
			std::string reasonCode;
			std::string requestTlvs;
			BERTLV 		responseTlvs;

			//iqbal_audi_271226
			std::string rrn;
			std::string authCode;
			std::string ecrNo;
			std::string batchNo;
			std::string sequenceNo;
			std::string maskPan;
			std::string dateTime;
			std::string responseCode;
			std::string cardType;
			std::string aid;
			std::string tvr;
			std::string tc;
			std::string respMesg;
			std::string txCurreny;

			bool isTxVoided;

			char cardEntry[1+1];


			char txType[3];
			int responseLen;
			bool doNotShowCardRemoval;
			static bool busyCannotProcess;
			REQ_TYPE reqType;
			unsigned char response[8192];

			bool checkCardRemovalFlag;
			bool transferComplete;
			bool bankingInProgress;
			bool parametersInProgress;
			bool hcommandEnabled;
			bool isTxnApproved;
			std::string refNum;
	};
}



#endif /* POSMESSAGE_H_ */
