#ifndef HCOMMAND_H_
#define HCOMMAND_H_

#include "posmessage.h"

using namespace com_verifone_posmessage;

namespace com_verifone_hcommand {

	class HCommand : public PosMessage {
		public:
			HCommand();

			bool unpack(unsigned char *request,unsigned int length);
			bool process(void);
			bool pack(void);
			bool pack(std::string &refNum);

		private:
			bool encapsulate(void);
			bool unpackRespTLVs(std::map<std::string, std::string> &txresult);
	};
}

#endif /* HCOMMAND_H_ */
