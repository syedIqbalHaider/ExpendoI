#ifndef CCOMMAND_H_
#define CCOMMAND_H_

#include "posmessage.h"

using namespace com_verifone_posmessage;

namespace com_verifone_ccommand {

	class CCommand : public PosMessage {
		public:
			CCommand();

			bool unpack(unsigned char *request,unsigned int length);
			bool process(void);
			bool pack(void);

		private:
			bool encapsulate(void);
			bool unpackRespTLVs(std::map<std::string, std::string> &txresult);
	};
}
#endif /* CCOMMAND_H_ */
