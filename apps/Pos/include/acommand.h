
#ifndef ACOMMAND_H_
#define ACOMMAND_H_

#include "posmessage.h"

using namespace com_verifone_posmessage;

namespace com_verifone_acommand {

	enum INPUT_CODES {
		GET_SERIAL,
		SEND_CURRENT_BATCH,
		GET_CURRENT_BATCH_REPORT,
		GET_PREVIOUS_BATCH_REPORT,
		PERFORM_PARAMETER_DOWNLOAD,
		REPRINT_LAST_RECEIPT,
		TRANSIT_BATCH_SETTLEMENT,
		RESET_POS_POLLING,
		CANCEL_CARD_INSERT_SWIPE,
		CLEAR_REVERSAL,
		UPDATE_BATCH_NUMBER,
		CLEAR_BATCH,
	};

	class ACommand : public PosMessage {
		public:
			ACommand();

			bool unpack(unsigned char *request,unsigned int length);
			bool process(void);
			bool pack(void);

		private:
			bool encapsulate(void);
			bool unpackRespTLVs(std::map<std::string, std::string> &txresult);
			bool updateBatchNo();
			bool updateParameters(void);
			bool setParameters(unsigned const char tag[], unsigned const char value[], CCommsSettings *comSettingsPrim, CCommsSettings *comSettingsSec);
			bool clearBatch(void);
			void clearDB(void);
			std::string paramData;

			char apn[64];
			char username[32];
			char password[32];
			char terminal_ip[16+1];
			char netmask[16+1];
			char defaultGw[16+1];
			char dns1[16+1];
			char dns2[16+1];
			int isDhcp;

			bool isDefaultCurrUSD;
			std::string saleMinAmt;
			std::string saleTxAmtMax;
			std::string fallbackMinAmt;
			std::string fallbackTxAmtMax;

			std::string saleMinAmtCurr2;
			std::string saleTxAmtMaxCurr2;
			std::string fallbackMinAmtCurr2;
			std::string fallbackTxAmtMaxCurr2;

	};
}
#endif /* ACOMMAND_H_ */
