
#ifndef SCOMMAND_H_
#define SCOMMAND_H_

#include "posmessage.h"

using namespace com_verifone_posmessage;

namespace com_verifone_scommand {

	class SCommand : public PosMessage {
		public:
			SCommand();

			bool unpack(unsigned char *request,unsigned int length);
			bool process(void);
			bool pack(void);

		private:
			bool encapsulate(void);
			bool unpackRespTLVs(std::map<std::string, std::string> &txresult);

			unsigned int credits_count, credit_reversals;
			long credits_total_amount, credits_reversals_total_amount;

			unsigned int debits_count, debits_reversals_count;
			long debits_total_amount, debits_reversals_total_amount;

			unsigned int authorisations, authorisation_reversals;
			long authorisations_total_amount, authorisation_reversals_total_amount;

			int action_code;

			std::map<string, long>superTotals;

			int batch_no;
	};
}
#endif // SCOMMAND_H_
