#ifndef BCOMMAND_H_
#define BCOMMAND_H_

#include "posmessage.h"
#include <iomanip>

using namespace com_verifone_posmessage;
using std::stringstream;
using std::setw;

namespace com_verifone_bcommand {

	class BCommand : public PosMessage {
		public:
			BCommand();

			bool unpack(unsigned char *request,unsigned int length);
			bool process(void);
			bool pack(void);

		private:
			bool encapsulate(void);
			bool unpackRespTLVs(std::map<std::string, std::string> &txresult);
	};
}
#endif /* BCOMMAND_H_ */
