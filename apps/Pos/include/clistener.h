#ifndef CLISTENER_H_
#define CLISTENER_H_

#define POS_TASK "Pos"
#define POS_MAIN_TASK "PosMain"
#define POS_STATUS_TASK "PosStatus"

namespace com_verifone_listener {

	enum TYPE {
		SERIAL,
		SOCKET
	};

	class CListener {
		public:
			CListener();
			bool ecrListen(TYPE connType, std::string port);
			bool ecrRead(char *buf, int len);
			bool ecrWrite(unsigned char *buf, int len);
		private:
			int ecrHandle;
			std::string port;
			TYPE connType;
	};
}
#endif /* CLISTENER_H_ */
