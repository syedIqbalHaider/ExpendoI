
#ifndef ECOMMAND_H_
#define ECOMMAND_H_

#include "posmessage.h"

using namespace com_verifone_posmessage;

namespace com_verifone_ecommand {

	class ECommand : public PosMessage {
		public:
			ECommand();

			bool unpack(unsigned char *request,unsigned int length);
			bool process(void);
			bool pack(void);
			bool pack(char *respCode);

		private:
			bool encapsulate(void);
			bool unpackRespTLVs(std::map<std::string, std::string> &txresult);
	};
}




#endif /* ECOMMAND_H_ */
