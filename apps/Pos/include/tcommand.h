#ifndef TCOMMAND_H_
#define TCOMMAND_H_

#include <vector>
#include <libda/cbatchrec.h>
#include "posmessage.h"

using namespace com_verifone_posmessage;

namespace com_verifone_tcommand {

	enum TLIST_TYPE
	{
		TLIST_CURRENT_BATCH,
		TLIST_PREVIOUS_BATCH,
		TLIST_INVALID
	};

	enum TREC_FIELD
	{
		TXREC_ONLINE,
		TXREC_BATCHNO,
		TXREC_TSN,
		TXREC_RCPTNO,
		TXREC_RESPCODE,
		TXREC_AUTHCODE,
		TXREC_SALE_AMNT,
		TXREC_CASH_AMNT,
		TXREC_PAN,
		TXREC_TIMESTAMP,
		TXREC_ISO_CODE,
		TXREC_RRN,
		TXREC_REASON_CODE,
		TXREC_EMV_DATA,
		TXREC_CARD_EXPIRY,
		TXREC_CARD_FEES,
		TXREC_TRANSACTION_FEES,
		TXREC_ECR_NUMBER
	};

	class TRecord
	{
		public:
			TRecord(com_verifone_batchrec::CBatchRec batchrec, int theBatchNo);
			bool WentOnline(void);
			int BatchNo(void);
			int Tsn(void);
			int RcptNo(void);
			string ResponseCode(void);
			string AuthCode(void);
			string SaleAmount(void);
			string CashAmount(void);
			string Pan(void);
			string TimeStamp(void);
			string ISOCode(void);
			string Rrn(void);

		private:
			map<int,string>txdata;

			bool went_online;

			int batchno;
			int tsn;
			int rcptno;
			int ecrno;

			string auth_code;
			string response_code;
			string tx_sale_amount;
			string tx_cash_amount;
			string pan;
			string start_timestamp;
			string iso_tx_code;
			string rrn;
			string reason_code;
			string emv_data;
			string cardExpiryDate;
			string cardFees;
			string transactionFees;
	};

	class TCommand : public PosMessage {
		public:
			TCommand();

			bool unpack(unsigned char *request,unsigned int length);
			bool process(void);
			bool pack(void);

		private:
			bool encapsulate(void);
			bool unpackRespTLVs(std::map<std::string, std::string> &txresult);

			TLIST_TYPE tlist_type;

			int batchno;
			int tx_count;
			unsigned int pack_counter;

			vector<TRecord>respData;
	};
}


#endif // TCOMMAND_H_
