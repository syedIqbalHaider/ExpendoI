#include <fcntl.h>
#include <errno.h>
#include <svc.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sstream>
#include <algorithm>
#include <libda/cterminalconfig.h>
#include <libipc/ipc.h>
#include <libcpa.h>
#include <iomanip>
#include "clog.h"
#include "posmessage.h"
#include "bertlv.h"
#include "ccommand.h"
#include "cstatus.h"
#include "utils.h"

#define FINANCIAL_REQUEST 0
#define CARD_DATA_ENQUIRY 32
#define CASH_LOAD 84
#define LOAD_TRANSIT_PRODUCT 86
#define PIN_CHANGE 92


extern "C" {
	#include <platforminfo_api.h>
}

#define SSTR( x ) dynamic_cast< std::ostringstream & >( std::ostringstream() << std::dec << x ).str()

#define ETX 0x03
#define STX 0x02
#define ACK 0x06
#define NAK 0x15
#define EOT 0x04
#define ESC 0x1B

static CLog log(POS_TASK);

using namespace za_co_verifone_bertlv;
using namespace com_verifone_status;

namespace com_verifone_ccommand
{
	CCommand::CCommand(){}

	bool CCommand::unpack(unsigned char *request,unsigned int length) {
		log.message(MSG_INFO "Received a C command\r\n");
		char amount[12+1],amountText[21], ecrNumber[8+1]={0};
		char *tlvs;

		memset(amount,0,sizeof(amount));
		memset(amountText,0,sizeof(amountText));
		memset(ecrNumber,0,sizeof(ecrNumber));

		if (length < 35) {
			log.message(MSG_INFO "Request C incomplete\r\n");
			return false;
		}

		txType[2] = 0x00;
		memcpy(txType,request,2);

		switch (atoi(txType)) {
			case 00: //sale
				reqType = TRANSACTION_REQUEST;
				checkCardRemovalFlag = true;
				break;
			case 02: //void
				log.message(MSG_INFO "iq: Start Void\r\n");
				reqType = VOID;
				break;
		}

		memcpy(amount,&request[2],12);
		memcpy(amountText,&request[14+17],3);

		this->amount = SSTR(amount);
		this->amountText = SSTR(amountText);

		log.message(MSG_INFO "Received Amount - "+this->amount+"\r\n");
		log.message(MSG_INFO "Received Amount Text - "+this->amountText+"\r\n");

		if (length-35>0) {
			log.message(MSG_INFO "Received TLVs\r\n");
			tlvs = (char *)malloc(length-35);
			if (tlvs != NULL) {
				memcpy(tlvs,&request[35],length-35);
				std::string tmp(reinterpret_cast<const char *>(tlvs), length-35);
				unpackReqTLVs(tmp);
			}

			free(tlvs);
		}
		return true;
	}

	bool CCommand::pack() {
		log.message(MSG_INFO "Pack a C command response\r\n");

		if (busyCannotProcess) {
			packBusyCannotProcess();
			return true;
		}

		responseLen = 5;

		response[0] = 0x43;
		response[1] = status.c_str()[0];
		response[2] = status.c_str()[1];
		if (reasonCode.length() == 2) {
			response[3] = reasonCode.c_str()[0];
			response[4] = reasonCode.c_str()[1];
		} else {
			response[3] = 0x33;
			response[4] = 0x30;
		}

		if (responseTlvs.getStringData().length() > 0) {
			// Add response TLVs
			memcpy(&response[5],responseTlvs.getStringData().c_str(),responseTlvs.getStringData().length());
			responseLen += responseTlvs.getStringData().length();
		}

		encapsulate();
		return true;
	}

	bool CCommand::encapsulate() {
		unsigned char tmpMessage[8192];
		unsigned char ucData;
		unsigned char ucLrc=0;
		unsigned int newLen;

		if(response == NULL) {
			log.message(MSG_INFO "response is empty\r\n");
			return false;
		}

		if (busyCannotProcess)
			return true;

		memcpy(tmpMessage,response,responseLen);
		response[0]=STX;
		newLen=1;
		for(int index=0;index<responseLen;index++) {
			ucData=tmpMessage[index];
			ucLrc^=ucData;
			if(ucData==STX || ucData==ETX || ucData==ESC) {
				response[newLen++]=ESC;
				ucData+=(unsigned char)0x20;
			}
			response[newLen++]=ucData;
		}
		response[newLen++]=ETX;
		if(ucLrc==STX || ucLrc==ETX || ucLrc==ESC) {
			response[newLen++]=ESC;
			ucLrc+=(unsigned char)0x20;
		}
		response[newLen++]=ucLrc;
		responseLen = newLen;

		return true;
	}

	bool CCommand::process(void) {
		std::string ipcFrom;
		std::string req,resp;
		std::map<std::string, std::string> txresult;
		std::map<std::string,std::string>::iterator it;
		int pmlret;

		log.message(MSG_INFO "Process messages to CPA\r\n");

		// check if terminal is busy with banking
		CStatus statusClass;
		OPERATION_STATUS statusCode = statusClass.getOperation();

		switch (statusCode) {
			case OP_STAT_PARAM_DOWNLOAD:
			case OP_STAT_BANKING:
				busyCannotProcess = true;
				break;
			default:
				busyCannotProcess = false;
				break;
		}

		switch (reqType) {
			case GET_STATUS:
			case GET_ACQUIRER_DATA:
				// nothing to do
				break;
			case REVERSAL:
				if (!busyCannotProcess) {
					// todo
				}
				break;
			case TRANSACTION_REQUEST:
				if (!busyCannotProcess) {
					pmlret = CpaApp::DoPosPayment(amount, string(txType, 3), amountText, requestTlvs, hcommandEnabled, txresult);

					if (txresult.size() > 0)
						unpackRespTLVs(txresult);

					if (pmlret != PMT_SUCCESS) {
						checkCardRemovalFlag = true;
						doNotShowCardRemoval = false;
					}

					// add the tlv data if exists
					switch(pmlret) {
						case PMT_SUCCESS:
							log.message(MSG_INFO "DoPayment returned PMT_SUCCESS\n");
							status = "01";
							break;
						case PMT_CANCELLED:
							log.message(MSG_INFO "DoPayment returned PMT_CANCELLED\n");
							status = "02";
							break;
						case PMT_DECLINED:
							log.message(MSG_INFO "DoPayment returned PMT_DECLINED\n");
							status = "02";
							break;
						case PMT_PROCESSING_ERROR:
							log.message(MSG_INFO "DoPayment returned PMT_PROCESSING_ERROR\n");
							status = "02";
							break;
						case PMT_BUSY_CANNOT_PROCESS:
							log.message(MSG_INFO "Busy Cannot Process\n");
							busyCannotProcess = true;
							break;
						default:
							log.message(MSG_INFO "Unknown Response\n");
							status = "02";
							break;
					}
				}

				break;
			case VOID:
				if (!busyCannotProcess) {
				pmlret = CpaApp::DoPosVoid(amount, string(txType, 3), amountText,ecrNo, txresult);

				if (txresult.size() > 0)
					unpackRespTLVs(txresult);

				// add the tlv data if exists
				switch(pmlret) {
					case PMT_SUCCESS:
						log.message(MSG_INFO "Void returned PMT_SUCCESS\n");
						status = "01";
						break;
					case PMT_CANCELLED:
						log.message(MSG_INFO "Void returned PMT_CANCELLED\n");
						status = "02";
						break;
					case PMT_DECLINED:
						log.message(MSG_INFO "Void returned PMT_DECLINED\n");
						status = "02";
						break;
					case PMT_PROCESSING_ERROR:
						log.message(MSG_INFO "Void returned PMT_PROCESSING_ERROR\n");
						status = "02";
						break;
					case PMT_BUSY_CANNOT_PROCESS:
						log.message(MSG_INFO "Busy Cannot Process\n");
						busyCannotProcess = true;
						break;
					default:
						log.message(MSG_INFO "Unknown Response\n");
						status = "02";
						break;
				}
			}

				break;
			default:
				log.message(MSG_INFO "ERROR NOT IMPLEMENTED\r\n");
				break;
		}

		return true;
	}

	bool CCommand::unpackRespTLVs(std::map<std::string, std::string> &txresult) {
		std::map<std::string,std::string>::iterator it;

		std::string pan,transactionFees,cardFees,appEffectiveDate,cardExpiryDate,cardType,accType,superId,cashierId,merchName, onlineFlag, inputEntity, rcptNo, src;
		std::string ff1,ff2,ff3,scf,uvf1,uvf2,uvf3,gvf,cardBalance,emvPan;
		unsigned int  intTagVal;
		char charTagVal[4];
		char swopBuf[4];

		for (it=txresult.begin(); it!=txresult.end(); ++it) {
			std::string name = it->first;
			std::string value = it->second;

			log.message(MSG_INFO "Name  - "+name+"\r\n");
			if (name != "EmvData")	// Messes up the debug log
				log.message(MSG_INFO "Value - "+value+"\r\n");


			 if (name == "AuthorisationCode") {	//iq_audi_271216
				this->authCode = value;
			}else if(name == "TimeStamp") {	//iq_audi_271216
				this->dateTime = value;
			}

			// Ignore values with 0 length
			if(value.length() == 0)
				continue;

			if (name == "EmvData") {
				log.message(MSG_INFO "Received EMV data from CPA - "+value+"\r\n");
				string emvdata = Utils::UtilsStringToHex(value);
				BERTLV emvTlv(emvdata);
				string filteredTags;

				FilterEMVTags(emvdata, filteredTags);
				responseTlvs.addTag(std::string("\xDF\x02"),filteredTags);


				emvTlv.getValForTag("\x84", this->aid);
				emvTlv.getValForTag("\x9f\x26", this->tc);
				emvTlv.getValForTag("\x95", this->tvr);

				emvTlv.getValForTag("\x9f\x50", cardBalance);

				if (!cardBalance.empty()) {
					cardBalance = hexToAscii((unsigned char*)cardBalance.c_str(),cardBalance.length());
					responseTlvs.addTag(std::string("\xDF\x06"),cardBalance);
				}

			} else if (name == "ReceiptNo" ){
				intTagVal = atoi(value.c_str());
				memcpy(charTagVal,&intTagVal,4);
				swopBuffer(charTagVal,swopBuf);
				responseTlvs.addTag(std::string("\xDF\x40"),string(swopBuf, 4));
			} else if (name == "Tsn" ){
				this->sequenceNo=value;
				intTagVal = atoi(value.c_str());
				memcpy(charTagVal,&intTagVal,4);
				swopBuffer(charTagVal,swopBuf);
				responseTlvs.addTag(std::string("\xDF\x41"),string(swopBuf, 4));
			} else if (name == "BatchNo"){
				this->batchNo=value;
				intTagVal = atoi(value.c_str());
				memcpy(charTagVal,&intTagVal,4);
				swopBuffer(charTagVal,swopBuf);
				responseTlvs.addTag(std::string("\xDF\x42"),string(swopBuf, 4));
			} else if (name == "ItemNo"){
				intTagVal = atoi(value.c_str());
				memcpy(charTagVal,&intTagVal,4);
				swopBuffer(charTagVal,swopBuf);
				responseTlvs.addTag(std::string("\xDF\x43"),string(swopBuf, 4));
			} else if (name == "AuthorisationCode"){
				intTagVal = atoi(value.c_str());
				memcpy(charTagVal,&intTagVal,4);
				swopBuffer(charTagVal,swopBuf);
				responseTlvs.addTag(std::string("\xDF\x44"),string(swopBuf, 4));
			} else if (name == "ResponseCode"){
				this->responseCode=value;
				intTagVal = atoi(value.c_str());
				memcpy(charTagVal,&intTagVal,4);
				swopBuffer(charTagVal,swopBuf);
				responseTlvs.addTag(std::string("\xDF\x45"),string(swopBuf, 4));
			}else if (name == "ServiceRestrictionCode"){
				intTagVal = atoi(value.c_str());
				memcpy(charTagVal,&intTagVal,4);
				swopBuffer(charTagVal,swopBuf);
				responseTlvs.addTag(std::string("\xDF\x46"),string(swopBuf, 4));
			} else if (name == "ReasonCode") {
				reasonCode = value;
			} else if (name == "CardExpiryDate"){
				if (cardExpiryDate.empty())
					cardExpiryDate = value;
			} else if (name == "CardFees"){
				cardFees = value;
			} else if (name == "TransactionFees"){
				transactionFees = value;
			} else if (name == "Pan"){
				this->maskPan=value;
				pan = value;
			} else if (name == "CardType"){
				cardType = value;
				this->cardType=value;
			} else if (name == "AccType"){
				accType = string(1, atoi(value.c_str()));
			} else if (name == "Supervisor"){
				superId = value;
			} else if (name == "Operator"){
				cashierId = value;
			} else if (name == "MerchantName"){
				merchName = value;
			} else if (name == "WentOnline"){
				onlineFlag = value;
			} else if (name == "InputType"){
				memcpy(cardEntry,value.c_str(),1);
				inputEntity = value;
			} else if (name == "Rrn") {	//iq_audi_271216
				rrn = value;
			}else if(name == "EcrNumber") {	//iq_audi_271216
				this->ecrNo = value;
			}else if(name == "TxCurrency") {	//iq_dualcurr_23082017
				this->txCurreny = value;
			}
			else if(name == "RespMessage") {	//iq_audi_060317
				this->respMesg = value;
			}
			else if(name == "IsTrxVoided") {	//iq_audi_060317

				if(value==string("Yes"))
					this->isTxVoided = true;
			}
			else if(name == "Approved") {	//iq_audi_160717
				this->isTxnApproved = atoi(value.c_str());
			}
		}

		if (atoi(txType) == FINANCIAL_REQUEST ||
			atoi(txType) == CASH_LOAD ||
			atoi(txType) == LOAD_TRANSIT_PRODUCT  ||
			atoi(txType) == CARD_DATA_ENQUIRY  ||
			atoi(txType) == PIN_CHANGE) {
				BERTLV printTlvs;

				if (!pan.empty())
					printTlvs.addTag(std::string("\xDF\xAF\x00", 3),pan);

				if (!cardExpiryDate.empty())
					printTlvs.addTag(std::string("\xDF\xAF\x01"),cardExpiryDate);

				if (!appEffectiveDate.empty())
					printTlvs.addTag(std::string("\xDF\xAF\x02"),appEffectiveDate);

				if (!cardFees.empty())
					printTlvs.addTag(std::string("\xDF\xAF\x03"),cardFees);

				if (!transactionFees.empty())
					printTlvs.addTag(std::string("\xDF\xAF\x04"),transactionFees);

				if(!cardType.empty())
					printTlvs.addTag(std::string("\xDF\xAF\x05"),cardType);

				if(!accType.empty())
					printTlvs.addTag(std::string("\xDF\xAF\x06"),accType);

				if(!superId.empty())
					printTlvs.addTag(std::string("\xDF\xAF\x07"),superId);

				if(!cashierId.empty())
					printTlvs.addTag(std::string("\xDF\xAF\x08"),cashierId);

				if(!merchName.empty())
					printTlvs.addTag(std::string("\xDF\xAF\x09"),merchName);

				if(!onlineFlag.empty())
					printTlvs.addTag(std::string("\xDF\xAF\x0A"),onlineFlag);

				if(!inputEntity.empty())
					printTlvs.addTag(std::string("\xDF\xAF\x0B"),inputEntity);

				responseTlvs.addTag(std::string("\xFF\x01"),printTlvs.getStringData());
		}

		return true;
	}
}
