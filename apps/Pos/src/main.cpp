#include <fcntl.h>
#include <errno.h>
#include <svc.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sstream>
#include <libipc/ipc.h>
#include <libda/creceiptno.h>	//iq_audi_261216
#include <libda/cterminalconfig.h>
#include <libcpa.h>
#include <platforminfo_api.h>
#include "gui/gui.h"

#ifdef VFI_PLATFORM_VOS
	#include <libda/cversions.h>
	#include <libda/cversionsfile.h>
	#include <libda/cconmanager.h>
#endif // VFI_PLATFORM_VOS

#include "clog.h"
#include "clistener.h"
#include "posmessage.h"
#include "acommand.h"
#include "bcommand.h"
#include "ccommand.h"
#include "ecommand.h"
#include "fcommand.h"
#include "hcommand.h"
#include "tcommand.h"
#include "scommand.h"
#include "cstatus.h"
#include "clistener.h"
#include "Posver.h"
#include "utils.h"
#include "signals.h"
#include <sys/timeb.h>


extern "C" {    //Extern C ugly hack VFI sdk doesn't provide C++ guard
#include <svcmgr/svc_powermngt.h>
#include <libsecins.h>
}


#define ETX 0x03
#define STX 0x02
#define ACK 0x06
#define NAK 0x15
#define EOT 0x04
#define ESC 0x1B

#define REBOOTING_SCREEN 	"<br><center> <h5>Rebooting ......</h5> <center>"

#define SSTR( x ) dynamic_cast< std::ostringstream & >( std::ostringstream() << std::dec << x ).str()

static CLog log(POS_TASK);
//static CListener listener;	iq_audi_291216 make extern
pthread_t tSerial;
pthread_t tStatus;
pthread_t tVersion;

struct arg_struct {
	unsigned char rxPacket[8192];
    unsigned int  rxLen;
};

typedef enum
{
	TT_UNKNOWN,
	TT_MX_925,
	TT_UX_300
}TERMINAL_TYPE;

static unsigned char txPacket[8192];
static int txPacketLen;
static int iState=0;
static int iTxRetry=0;
static time_t tExpire=0;

static bool busy=false;
static bool doNotShowCardRemoval=false;
static bool receivedAck=true;
static bool resultReceived = false;
static bool parametersInProgress=false;
static bool bankingInProgress=false;
static bool sentStartStatus=false;
static bool ignoreAcks=false;
static string currRefNum;

using namespace com_verifone_listener;
using namespace com_verifone_posmessage;
using namespace com_verifone_acommand;
using namespace com_verifone_bcommand;
using namespace com_verifone_ccommand;
using namespace com_verifone_ecommand;
using namespace com_verifone_fcommand;
using namespace com_verifone_hcommand;
using namespace com_verifone_tcommand;
using namespace com_verifone_scommand;
using namespace com_verifone_status;
using namespace com_verifone_listener;
using namespace com_verifone_receiptno;
using namespace com_verifone_terminalconfig;
using namespace vfigui;

static REQ_TYPE reqType=GET_STATUS;

void* ProcessMessageThread(void *inData);
void* StatusThread(void*);
void ResetValues(void);
TERMINAL_TYPE UtilsGetTerminalType(void);

string UtilsStringToHex(string src);

#ifdef VFI_PLATFORM_VOS
void *update_versions(void*)
{
	com_verifone_versions::CVersions versions;
	com_verifone_versionsfile::CVersionsFile vfile;

	log.message(MSG_INFO "Pos start writing versions\r\n");

	vfile.setName("");
	vfile.setVersion(POS_VERSION);

	// Keep trying every second, until we succeed
	while(1){
		if(versions.addFile(POS_NAME, vfile) == DB_OK)
			break;

		sleep(1);
	}

	log.message(MSG_INFO "Pos versions complete\r\n");

	return NULL;
}
#endif // VFI_PLATFORM_VOS

#if 0	//iq_audi_161216
int main(int argc, char *argv[])
{
	unsigned char ucLrc=0;
	bool bEsc=false;
	unsigned char ucPacket[8192];
	struct arg_struct *argStruct;
	int iPacketIndex=0;
	unsigned char ucData;
	TERMINAL_TYPE terminalType;
	//char tmpChar[3];
	std::string port;
	int cmdLinePort=-1;
	unsigned int loop_count = 0;

	//std::string tmpString;
	//std::map<std::string, std::string> txresult;

	if (argc>1) {
		cmdLinePort = atoi(argv[1]);
		log.message(MSG_INFO "Passing port number from command line\r\n");
	}

	log.message(MSG_INFO "POS starting...\r\n");

	srand (time(NULL));		// Seed the psuedo random number generator with the current time.

	SignalsConfigureHandlers();

#ifdef VFI_PLATFORM_VOS
//	pthread_create(&tVersion,NULL,&update_versions,NULL);
#endif // VFI_PLATFORM_VOS

	terminalType = UtilsGetTerminalType();
	if (cmdLinePort == -1) {
		if (terminalType == TT_UX_300)
			port  = "/dev/ttyAMA0";
		else
			port  = "/dev/ttyAMA1";
	} else {
		port = SSTR(cmdLinePort);
	}

	while (!listener.ecrListen(SERIAL,port)) {
		log.message(MSG_INFO "Cannot open port\r\n");
		sleep(1);
	}

	pthread_create(&tStatus,NULL,&StatusThread,NULL);

	com_verifone_ipc::init(string(POS_MAIN_TASK));

	string  tmpString;
	char tmpChar[2]={0};
// main loop
	while(1){

		tmpString.clear();
		//log.message(MSG_INFO "State - "+SSTR(iState)+"\n");
		if(listener.ecrRead((char *) &ucData,1)) {

			log.message(MSG_INFO "Read char\n");
	    	sprintf(tmpChar,"%02X", ucData);
			tmpString += tmpChar;
			log.message(MSG_INFO "Received - "+tmpString+"\n");

			if (iState == 3 && busy) {
				if(ucData!=ACK) {
					log.message(MSG_INFO "Waiting for ack - discard byte!!\r\n");
					continue;
				}
			} else if(ucData==STX) {
            	iState=0;
            }

//			string flagstr = busy?"1\n":"0\n";
//			log.message(MSG_INFO "busy="+flagstr);

        	/*sprintf(tmpChar,"%02X", ucData);
        	tmpString += tmpChar;
        	log.message(MSG_INFO "Received - "+tmpString+"\n");
        	tmpString.clear();*/

            switch(iState) {
            	case 0:	// wait stx
                	if(ucData==STX) {
                        bEsc=false;
                        ucLrc=0;
                        iPacketIndex=0;
                        memset(ucPacket,0,sizeof(ucPacket));
                    	iState=1;
                    	log.message(MSG_INFO "STX\n");
                    }
                    break;
                case 1:	// data, wait etx
                	if(ucData==ETX) {
//                		log.message(MSG_INFO "ETX\n");
                    	iState=2;
                        break;
                    }
                    if(ucData==ESC) {
                    	bEsc=true;
                        break;
                    }
                    if(bEsc) {
                    	bEsc=false;
                        ucData-=(unsigned char)0x20;
                    }
//                    log.message(MSG_INFO "WAIT ETX\n");
                    ucLrc^=ucData;
                    ucPacket[iPacketIndex++]=ucData;
                	break;
                case 2:	// lrc
                    if(ucData==ESC) {
                    	bEsc=true;
                    	//log.message(MSG_INFO "ESC\n");
                        break;
                    } else {
                    	if(bEsc) {
                    		bEsc=false;
                    		ucData-=(unsigned char)0x20;
                    	}
                    	if(ucData==ucLrc) {
							log.message(MSG_INFO "Received a packet!!!\r\n");
							listener.ecrWrite((unsigned char *)"\x06",1);

							// check if terminal is busy
							CStatus statusClass;
							OPERATION_STATUS statusCode = statusClass.getOperation();

							if(statusCode != OP_STAT_IDLE){
								log.message(MSG_INFO "Terminal busy with operation status "+Utils::UtilsIntToString(statusCode)+", cannot process\r\n");
								busy = true;
							}else{
								ResetValues();
							}

							if (memcmp(&ucPacket[0],"A", 1) == 0 && memcmp(&ucPacket[1],"0", 1) == 0 && memcmp(&ucPacket[2],"8", 1) == 0){
								std::map<std::string, std::string> txresult;
								doNotShowCardRemoval = false;
								log.message(MSG_INFO "Send Cancel Card Insert Swipe\r\n");
								int pmlret = CpaApp::DoPosCancelWaitingForCardInput();
								if (pmlret != PMT_SUCCESS) {
									log.message(MSG_INFO "Cancel Card Insert Swipe failed\r\n");
								}
								if (iState != 3)
									iState=0;
							} else if (memcmp(&ucPacket[0],"H", 1) == 0){
								CStatus stat;
								log.message(MSG_INFO "Received H request\r\n");
								if (memcmp(&ucPacket[1],"0", 1) == 0 && memcmp(&ucPacket[2],"0", 1) == 0) {
									stat.setStatus(CONTINUE_WITH_EMV_COMPLETE_SUCCESS);
								} else {
									stat.setStatus(CONTINUE_WITH_EMV_COMPLETE_FAIL);
								}
								iState=0;
							} else if (busy) {
								listener.ecrWrite((unsigned char *)"\x04",1);
								log.message(MSG_INFO "Application is already processing a packet send EOT!!\r\n");
								if (iState != 3)
									iState=0;
							} else {
								argStruct = (arg_struct *)malloc(sizeof(arg_struct));

								if (argStruct == NULL){
									log.message(MSG_INFO "!!!Memory allocation failure!!!\r\n");

									char resp[3];
									ECommand posMessage;
									CStatus stat;

									stat.setStatus(TERMINAL_OFFLINE);

									sprintf(resp,"%02d",TERMINAL_OFFLINE);
									posMessage.pack(resp);
									listener.ecrWrite(posMessage.getResponse(),posMessage.getResponseLen());

									iState=0;
									break;
								}

								memcpy(argStruct->rxPacket,ucPacket,iPacketIndex);
								argStruct->rxLen = iPacketIndex;

								busy = true;
								iState=0;

								loop_count++;
								log.message(MSG_INFO "-------------------------------------------------> Loop count "+Utils::UtilsIntToString(loop_count)+"\n");
/*
								pthread_attr_t attr;

								int ret = pthread_attr_init(&attr);
								if (ret != 0){
									log.message(MSG_INFO "!!!! ERROR STARTING pthread_attr_init  - ERROR "+Utils::UtilsIntToString(ret)+" !!!!\r\n");
									continue;	// Continue to do what?!
								}

								// Thread has to be detached since we won't be joining with it.
								// Not doing so, will result in a zombie hanging around after the thread exits.
								pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

								int pret = pthread_create(&tSerial,&attr,&ProcessMessageThread,(void *)argStruct);

								pthread_attr_destroy(&attr);	// Destroy thread attribute resources

								if(pret != 0){
									log.message(MSG_INFO "!!!! ERROR STARTING ProcessMessageThread  - ERROR "+Utils::UtilsIntToString(pret)+" !!!!\r\n");
								}
*/

								int pret = pthread_create(&tSerial,NULL,&ProcessMessageThread,(void *)argStruct);

								if(pret != 0){
									log.message(MSG_INFO "!!!! ERROR STARTING ProcessMessageThread  - ERROR "+Utils::UtilsIntToString(pret)+" !!!!\r\n");
								}

								pthread_detach(tSerial);
							}
                    	} else {
							log.message(MSG_INFO "NAKing\r\n");
							listener.ecrWrite((unsigned char *)"\x15",1);
							iState=0;
                    	}
                    }
                	break;
                case 3:	// waiting for a ack/nak for tx packet
                	if(ucData==ACK) {
                		log.message(MSG_INFO "Ack received!!\r\n");
                		if (!ignoreAcks) {
							receivedAck = true;
							resultReceived = true;
							ResetValues();
                		}
                        break;
                    } else if(ucData==NAK) {
                        if(--iTxRetry>0) {
                        	listener.ecrWrite(txPacket, txPacketLen);
                            tExpire=time(NULL)+2;
                        } else {
                        	listener.ecrWrite((unsigned char *)"\x04",1);
                        	log.message(MSG_INFO "No ack received -  send EOT!!\r\n");
                        	ResetValues();
                        	busy=false;
                        }
                    }
                	break;
            }
        } else if(iState==3 && time(NULL)>tExpire) {
            if(--iTxRetry>0) {
            	log.message(MSG_INFO "Set expiry timer!!\r\n");
            	listener.ecrWrite(txPacket, txPacketLen);
            	tExpire=time(NULL)+2;
            } else {
            	listener.ecrWrite((unsigned char *)"\x04",1);
            	log.message(MSG_INFO "No ack received -  send EOT!!\r\n");
            	receivedAck = false;
            	resultReceived = true;
            	ResetValues();
            	busy=false;
            }
        } else {
        	usleep(500000);
//        	log.message(MSG_INFO "Pos waiting...\n");
        }
	}
}
#endif


#if 1
int main(int argc, char *argv[])
{
	unsigned char ucLrc=0;
	bool bEsc=false;
	unsigned char ucPacket[8192];
	unsigned char temp[4096];
	struct arg_struct *argStruct;
	int iPacketIndex=0;
	unsigned char ucData;
	TERMINAL_TYPE terminalType;
	char tmpChar[3];
	std::string port;
	int cmdLinePort=-1;
	unsigned int loop_count = 0;
	bool ret=false;
	std::string tmpString;
	com_verifone_terminalconfig::CTerminalConfig termcfg;
//	struct timeb start_tp;
//	struct timeb  current_tp;

	if (argc>1) {
		cmdLinePort = atoi(argv[1]);
		log.message(MSG_INFO "Passing port number from command line\r\n");
	}

	log.message(MSG_INFO "POS starting...\r\n");

	srand (time(NULL));		// Seed the psuedo random number generator with the current time.

	SignalsConfigureHandlers();

#ifdef VFI_PLATFORM_VOS
//	pthread_create(&tVersion,NULL,&update_versions,NULL);
#endif // VFI_PLATFORM_VOS

	terminalType = UtilsGetTerminalType();
	if (cmdLinePort == -1) {
		if (terminalType == TT_UX_300)
			port  = "/dev/ttyAMA0";
		else
			port  = "/dev/ttyAMA1";
	} else {
		port = SSTR(cmdLinePort);
	}

	while (!listener.ecrListen(SERIAL,port)) {
		log.message(MSG_INFO "Cannot open port\r\n");
		sleep(1);
	}

	pthread_create(&tStatus,NULL,&StatusThread,NULL);
	com_verifone_ipc::init(string(POS_MAIN_TASK));

//	Power management
    svc_power_apm_config apm_config;
	apm_config.item_mask = APMCONFIG_M_NETWORK_STATE|APMCONFIG_M_APM_STATE | APMCONFIG_M_APM_INIT_MODE | APMCONFIG_M_INACTIVITY_TIME
	| APMCONFIG_M_STANDBY_TIMEOUT | APMCONFIG_M_TIME_TO_SHUTDOWN | APMCONFIG_M_SHUTDOWN_ISO_HIBER| APMCONFIG_M_WAKEUP_DEVICES;
	apm_config.apm_state = 0; 				//Power management OFF, 1=ON
	apm_config.apm_init_mode = 3 ; 			//0 - default (Standby); 1 - Standby; 2 - Suspend; 3 - Hibernate.If apm_state=1
	apm_config.inactivity_time = 1; 		// 1 min
	apm_config.standby_timeout = 1; 		// 1 min
	apm_config.time_to_shutdown = 1; 		//1 min
	apm_config.shutdown_iso_hibernate = 0; 	//Use hibernate instead of shutdown
	apm_config.wakeup_devices = PM_WAKE_DEVICE_MCU_CARDIN | PM_WAKE_DEVICE_MCU_MDBWAKE
	| PM_WAKE_DEVICE_UART0 /* | PM_WAKE_DEVICE_MCU_PROXIMITY | PM_WAKE_DEVICE_EXTGRP5 | PM_WAKE_DEVICE_KEYPAD */;
//	apm_config.network_on = 1;
//	apm_config.auto_mode = 0;
//	apm_config.auto_mode_hibernate = 0;
//	apm_config.auto_wakeup_time = 0;
//	apm_config.auto_sleep_time = 0;

	int ret_init = powermngt_SetConfig(&apm_config);
	log.message(MSG_INFO "Wake-up devices initalization returned:["+SSTR(ret_init)+"]\r\n");

// main loop
	while(1){
		//log.message(MSG_INFO "State - "+SSTR(iState)+"\n");

		ret=listener.ecrRead((char *) &ucData,1);


//		if(ret)
//		{
//			char testchar[3]={0};
//			sprintf(testchar,"%02X", ucData);
//			log.message(MSG_INFO "testchar    [ "+SSTR(testchar)+"]\r\n");
//		}

		if((ret || (iState==1 && !ret))) {		//iq_audi_191216, iState we use as for trans Start

			if(ret){

				if (iState == 3 && busy) {
					log.message(MSG_INFO "Transaction In Process - discard Message!!\r\n");
					continue;
				}

				if(iState==0)
				{
					bEsc=false;
					ucLrc=0;
					iPacketIndex=0;
					memset(ucPacket,0,sizeof(ucPacket));
					iState=1;
					log.message(MSG_INFO "STX\r\n");
				}

				ucPacket[iPacketIndex++]=ucData;
			}
			else{
				log.message(MSG_INFO "iq: COMPLETE DATA RECEIVED\r\n");

				log.message(MSG_INFO "Data: "+SSTR(ucPacket)+"\r\n");

				for(int i=0; i<iPacketIndex-1; i++)		//-1 ,b/c last digit is lrc
					ucLrc^=ucPacket[i];

				ucData=ucPacket[iPacketIndex-1];
				iState=2;

//				iq_audi_191216
//				if (iState == 3 && busy) {
//					if(ucData!=ACK) {
//						log.message(MSG_INFO "Waiting for ack - discard byte!!\r\n");
//						continue;
//					}
//				} else if(ucData==STX) {
//					iState=0;
//				}

//				string flagstr = busy?"1\n":"0\n";
//				log.message(MSG_INFO "busy="+flagstr);

				sprintf(tmpChar,"%02X", ucData);
				tmpString += tmpChar;
				log.message(MSG_INFO "LRC - "+tmpString+"\r\n");
				tmpString.clear();

				switch(iState) {
					case 0:	// wait stx
						if(ucData==STX) {
							bEsc=false;
							ucLrc=0;
							iPacketIndex=0;
							memset(ucPacket,0,sizeof(ucPacket));
							iState=1;
							log.message(MSG_INFO "STX\n");
						}
						break;
					case 1:	// data, wait etx
						if(ucData==ETX) {
	//                		log.message(MSG_INFO "ETX\n");
							iState=2;
							break;
						}
						if(ucData==ESC) {
							bEsc=true;
							break;
						}
						if(bEsc) {
							bEsc=false;
							ucData-=(unsigned char)0x20;
						}
	//                    log.message(MSG_INFO "WAIT ETX\n");
						ucLrc^=ucData;
						ucPacket[iPacketIndex++]=ucData;
						break;
					case 2:	// lrc
//						if(ucData==ESC) {
//							bEsc=true;
//							//log.message(MSG_INFO "ESC\n");
//							break;
//						} else {
//							if(bEsc) {
//								bEsc=false;
//								ucData-=(unsigned char)0x20;
//							}
							if(ucData==ucLrc && ucPacket[0]==STX && ucPacket[iPacketIndex-2]==ETX) {
								log.message(MSG_INFO "Received a packet!!!\r\n");

								if(memcmp(ucPacket+1,"IsConnected",11)==0)
								{
									listener.ecrWrite((unsigned char *)"\x02\x6F\x6B\x03\x05",5);	//send Acknowledgement
									iState=3;
									continue;
								}
								else if(memcmp(ucPacket+1,"Version",7)==0)
								{
									listener.ecrWrite((unsigned char *)"\x02\x6F\x6B\x03\x05",5);	//send Acknowledgement

									PosMessage *message;
									message->versionDetails();
									iState=3;
									continue;
								}
								else if(memcmp(ucPacket+1,"Reboot",6)==0)
								{
									listener.ecrWrite((unsigned char *)"\x02\x6F\x6B\x03\x05",5);	//send Acknowledgement

									uiDisplay(REBOOTING_SCREEN);
									Secins_reboot();
									iState=3;
									continue;
								}
								else if(memcmp(ucPacket+1,"ClearBatch",10)==0)
								{
									memset(ucPacket,0,sizeof(ucPacket));
									memcpy(ucPacket,"A11",3);
								}
								else if(memcmp(ucPacket+13,"Sale",4)==0)
								{
									memset(temp,0,sizeof(temp));
									memcpy(temp,ucPacket,iPacketIndex);
									memset(ucPacket,0,sizeof(ucPacket));

									memcpy(ucPacket,"C00",3);	//command C
									memcpy(ucPacket+3,temp+26,12);
									memset(ucPacket+15,0x30,17);		//text of amount, no use in current app

									if(iPacketIndex<43) //currency (if less use default currency for backward compatability
										memcpy(ucPacket+32,(const char*)termcfg.getCurrencyCode().c_str(),3);	//currency
									else
										memcpy(ucPacket+32,temp+38,3);

									memcpy(ucPacket+36,"\xDF\x40\x04",3);		//tlv Data, contain ECR Number

									string ecrNumber;
									string ecrNumHex;
									char ecrNumberChar[8+1]={0};
									ecrNumber.clear();

									memcpy(ecrNumberChar,temp+18,8);
									ecrNumber.append(ecrNumberChar,8);
									ecrNumHex=UtilsStringToHex(ecrNumber);
									memcpy(ucPacket+39,ecrNumHex.c_str(),4);

									iPacketIndex=43;

									com_verifone_receiptno::CEcrNumber ecrNo;
									ecrNo.setEcrNo(atoi(ecrNumberChar));
//									cout<< "ECR Number main: " << ecrNo.getEcrNo()<<endl;
								}
								else if(memcmp(ucPacket+13,"Void",4)==0)
								{
									memset(temp,0,sizeof(temp));
									memcpy(temp,ucPacket,iPacketIndex);
									memset(ucPacket,0,sizeof(ucPacket));

									memcpy(ucPacket,"C02",3);	//command C
									memcpy(ucPacket+3,temp+26,12);
									memset(ucPacket+15,0x30,17);		//text of amount, no use in current app

									if(iPacketIndex<43) //currency (if less use default currency for backward compatability
										memcpy(ucPacket+32,(const char*)termcfg.getCurrencyCode().c_str(),3);	//currency
									else
										memcpy(ucPacket+32,temp+38,3);

									memcpy(ucPacket+36,"\xDF\x40\x04",3);		//tlv Data, contain ECR Number

									string ecrNumber;
									string ecrNumHex;
									char ecrNumberChar[8+1]={0};
									ecrNumber.clear();

									memcpy(ecrNumberChar,temp+18,8);
									ecrNumber.append(ecrNumberChar,8);
									ecrNumHex=UtilsStringToHex(ecrNumber);
									memcpy(ucPacket+39,ecrNumHex.c_str(),4);

									iPacketIndex=43;

//									com_verifone_receiptno::CEcrNumber ecrNo;
//									ecrNo.setEcrNo(atoi(ecrNumberChar));
//									cout<< "ECR Number main: " << ecrNo.getEcrNo()<<endl;
								}
								else if(memcmp(ucPacket+13,"Settlement",10)==0)
								{
									memset(ucPacket,0,sizeof(ucPacket));
									memcpy(ucPacket,"A01",3);
								}
								else if(memcmp(ucPacket+13,"CancelCard",10)==0)
								{
									memset(ucPacket,0,sizeof(ucPacket));
									memcpy(ucPacket,"A08",3);
								}
								else if(memcmp(ucPacket+1,"UpdateParameter",15)==0)
								{
									memset(temp,0,sizeof(temp));
									memcpy(temp,ucPacket,iPacketIndex);
									memset(ucPacket,0,sizeof(ucPacket));

									memcpy(ucPacket,"A04",3);
									memcpy(ucPacket+3,temp+17,strlen((char *)temp)-17);
								}
								else if(memcmp(ucPacket+1,"UpdateBatchNo",13)==0)
								{
									memset(temp,0,sizeof(temp));
									memcpy(temp,ucPacket,iPacketIndex);
									memset(ucPacket,0,sizeof(ucPacket));

									memcpy(ucPacket,"A10",3);
									memcpy(ucPacket+3,temp+15,strlen((char *)temp)-15);
								}
								else if(memcmp(ucPacket+1,"GotoSleep",9)==0)
								{
									powermngt_Hibernate();

//									log.message(MSG_INFO "Hibernate ret ["+SSTR(ret)+"\r\n");

//									powermngt_Suspend();
									iState=3;
									continue;
								}
								else if(memcmp(ucPacket+13,"ClearReversal",13)==0)
								{
									memset(ucPacket,0,sizeof(ucPacket));
									memcpy(ucPacket,"A09",3);
								}
								else
								{

									log.message(MSG_INFO "INVALID COMMAND !!!\r\n");
									iState=3;
									continue;
								}

								// check if terminal is busy
								CStatus statusClass;
								OPERATION_STATUS statusCode = statusClass.getOperation();

								if(statusCode != OP_STAT_IDLE){
									log.message(MSG_INFO "Terminal busy with operation status "+Utils::UtilsIntToString(statusCode)+", cannot process\r\n");
									busy = true;
								}else{
									ResetValues();
								}

								if (memcmp(&ucPacket[0],"A", 1) == 0 && memcmp(&ucPacket[1],"0", 1) == 0 && memcmp(&ucPacket[2],"8", 1) == 0){
									std::map<std::string, std::string> txresult;
									doNotShowCardRemoval = false;
									log.message(MSG_INFO "Send Cancel Card Insert Swipe\r\n");
									int pmlret = CpaApp::DoPosCancelWaitingForCardInput();
									if (pmlret != PMT_SUCCESS) {
										log.message(MSG_INFO "Cancel Card Insert Swipe failed\r\n");
									}
									if (iState != 3)
										iState=0;
								} else if (memcmp(&ucPacket[0],"H", 1) == 0){
									CStatus stat;
									log.message(MSG_INFO "Received H request\r\n");
									if (memcmp(&ucPacket[1],"0", 1) == 0 && memcmp(&ucPacket[2],"0", 1) == 0) {
										stat.setStatus(CONTINUE_WITH_EMV_COMPLETE_SUCCESS);
									} else {
										stat.setStatus(CONTINUE_WITH_EMV_COMPLETE_FAIL);
									}
									iState=0;
								} else if (busy) {
//									listener.ecrWrite((unsigned char *)"\x04",1);
									log.message(MSG_INFO "Application is already processing a packet send EOT!!\r\n");
									if (iState != 3)
										iState=0;
								} else {
									argStruct = (arg_struct *)malloc(sizeof(arg_struct));

									if (argStruct == NULL){
										log.message(MSG_INFO "!!!Memory allocation failure!!!\r\n");

										char resp[3];
										ECommand posMessage;
										CStatus stat;

										stat.setStatus(TERMINAL_OFFLINE);

										sprintf(resp,"%02d",TERMINAL_OFFLINE);
										posMessage.pack(resp);
//										listener.ecrWrite(posMessage.getResponse(),posMessage.getResponseLen());

										iState=0;
										break;
									}

									listener.ecrWrite((unsigned char *)"\x02\x6F\x6B\x03\x05",5);	//send Acknowledgement

									memcpy(argStruct->rxPacket,ucPacket,iPacketIndex);
									argStruct->rxLen = iPacketIndex;

									busy = true;
									iState=0;

									loop_count++;
									log.message(MSG_INFO "-------------------------------------------------> Loop count "+Utils::UtilsIntToString(loop_count)+"\n");

									int pret = pthread_create(&tSerial,NULL,&ProcessMessageThread,(void *)argStruct);

									if(pret != 0){
										log.message(MSG_INFO "!!!! ERROR STARTING ProcessMessageThread  - ERROR "+Utils::UtilsIntToString(pret)+" !!!!\r\n");
									}

									pthread_detach(tSerial);
								}
							} else {

//								log.message(MSG_INFO "NAKing\r\n");
//								listener.ecrWrite((unsigned char *)"\x15",1);
								log.message(MSG_INFO "Incorrect LRC/no STX/no ETX \r\n");
								ResetValues();

							}
//						}
						break;
					case 3:	// waiting for a ack/nak for tx packet
						//iq_audi_161216 there is no ACK mechanism from KIOSK side  so this part no use
//						if(ucData==ACK) {
//							log.message(MSG_INFO "Ack received!!\r\n");
//							if (!ignoreAcks) {
//								receivedAck = true;
//								resultReceived = true;
//								ResetValues();
//							}
//							break;
//						} else if(ucData==NAK) {
//							if(--iTxRetry>0) {
//								tExpire=time(NULL)+2;
//							} else {
//								log.message(MSG_INFO "No ack received -  send EOT!!\r\n");
//								ResetValues();
//								busy=false;
//							}
//						}
						break;
				}
			}
        } else if(iState==3 /*&& time(NULL)>tExpire*/) {
//            if(--iTxRetry>0) {
//            	log.message(MSG_INFO "Set expiry timer!!\r\n");
//            	listener.ecrWrite(txPacket, txPacketLen);
//            	tExpire=time(NULL)+2;
//            } else {
//            	listener.ecrWrite((unsigned char *)"\x04",1);
//            	log.message(MSG_INFO "No ack received -  send EOT!!\r\n");
//            	receivedAck = false;
//            	resultReceived = true;
//            	ResetValues();
//            	busy=false;
//            }

        	memset(ucPacket,0,sizeof(ucPacket));

        	receivedAck = true;
			resultReceived = true;
			ResetValues();

		// Idle-State
			listener.ecrWrite((unsigned char *)"\x02\x49\x64\x6C\x65\x2D\x53\x74\x61\x74\x65\x03\x5F",13);
			log.message(MSG_INFO "iq: **************TRANSACTION COMPLETE*********!\r\n");
        }
        else {
        	usleep(500000);
        }
	}
}
#endif

void ResetValues(void) {
	log.message(MSG_INFO "Call reset values!\r\n");

	txPacketLen = 0;
	iTxRetry=0;
	iState=0;
	parametersInProgress=false;
	bankingInProgress=false;
	sentStartStatus = false;
	busy = false;

	CpaApp::DoPosOnIdleScreen();

}

volatile STATUS prevStatus=TERMINAL_READY;

void *ProcessMessageThread(void *inData) {

	unsigned char rxPacket[8192];
    unsigned int  rxLen;
	char tmpChar[3];
	std::string tmpString;
	int iret;
	struct arg_struct *data = (struct arg_struct *)inData;
	ignoreAcks = true;
	CStatus stat;

	string random_task_id = std::string(POS_TASK) + Utils::UtilsIntToString(rand());
	iret = com_verifone_ipc::init(random_task_id);
	if (iret != 0) {
		log.message(MSG_INFO "Cannot not open ipc. error - "+SSTR(iret)+"\r\n");
		return NULL;
	}

	log.message(MSG_INFO "IPC Initialized for task "+random_task_id+" ...\r\n");


	memcpy(rxPacket,data->rxPacket,data->rxLen);
	rxLen = data->rxLen;
	free(inData);

	ACommand acomm;
	BCommand bcomm;
	CCommand ccomm;
	FCommand fcomm;
	TCommand tcomm;
	SCommand scomm;
	PosMessage *message;

	if (rxPacket[0] == 'A'){
		message = &acomm;
	} else if (rxPacket[0] == 'B'){
		message = &bcomm;
	} else if (rxPacket[0] == 'C'){
		message = &ccomm;
	} else if (rxPacket[0] == 'T'){
		message = &tcomm;
	} else if (rxPacket[0] == 'S'){
		message = &scomm;
	} else if (rxPacket[0] == 'F'){
		message = &fcomm;
	} else {
		log.message(MSG_INFO "Received NOT IMPLEMENTED command ["+string((const char*)rxPacket)+"]\r\n");
		ResetValues();
		receivedAck = true;
		resultReceived = false;
		ignoreAcks = false;
		busy=false;
		com_verifone_ipc::deinit(0);
		return NULL;
	}

	if (!message->unpack(&rxPacket[1],rxLen-1)) {
		log.message(MSG_INFO "Failed to unpack incoming ECR message\r\n");
//		listener.ecrWrite((unsigned char *)"\x04",1);
		ResetValues();
		receivedAck = true;
		resultReceived = false;
		ignoreAcks = false;
		busy=false;
		com_verifone_ipc::deinit(0);
		return NULL;
	}

	parametersInProgress = message->isParametersInProgress();

	if(parametersInProgress){
		log.message(MSG_INFO "Parameter download is in progress\n");
	}

	bankingInProgress = message->isBankingInProgress();

	if(bankingInProgress){
		log.message(MSG_INFO "Banking is in progress\n");
	}

	reqType = message->getRequestType();

	// Communicate with ExpendoI
	if (!message->process()) {
		log.message(MSG_INFO "Failed to process ExpendoI data\r\n");

//		listener.ecrWrite((unsigned char *)"\x04",1);
		ResetValues();
		receivedAck = true;
		resultReceived = false;
		ignoreAcks = false;
		busy=false;
		com_verifone_ipc::deinit(0);

		return NULL;
	}

	if (message->isCheckCardRemovalFlag())
		doNotShowCardRemoval = message->isDoNotShowCardRemoval();

	STATUS status=TERMINAL_READY;
	char resp[3];

	status = stat.getStatus();

	if (status == WAITING_FOR_CARD_INSERT ||
						(status != prevStatus && status != SENT_H_COMMAND
								&& status != CONTINUE_WITH_EMV_COMPLETE_SUCCESS
								&& status != CONTINUE_WITH_EMV_COMPLETE_FAIL)) {
		log.message(MSG_INFO "Send status ["+SSTR(status)+"]\n");

		ECommand posMessage;
		sprintf(resp,"%02d",status);
		posMessage.pack(resp);
		//Check that statuses still the same
//		if (busy && ignoreAcks == true && txPacketLen == 0 && (reqType == TRANSACTION_REQUEST || reqType == com_verifone_posmessage::REVERSAL)) //iq_audi_201216
//			listener.ecrWrite(posMessage.getResponse(),posMessage.getResponseLen());
	}

	// pack the response message to POS
	ignoreAcks = false;
	do {
		if (!message->pack()) {
			log.message(MSG_INFO "Failed to pack outgoing ECR message\r\n");
//			listener.ecrWrite((unsigned char *)"\x04",1);  //iq_audi_201216

			ResetValues();
			receivedAck = true;
			resultReceived = false;
			ignoreAcks = false;
			busy=false;
			com_verifone_ipc::deinit(0);

			return NULL;
		}

		tmpString.clear();
		for(int i=0;i<message->getResponseLen();i++) {
			sprintf(tmpChar,"%02X", message->getResponse()[i]);
			tmpString += tmpChar;
		}
//		log.message(MSG_INFO "Sending back to ECR\r\n");
//		log.message(MSG_INFO ""+tmpString+"\r\n");

		if (message->getResponseLen() > 0) {
			if (message->isBusyCannotProcess() && (reqType != GET_STATUS && reqType != GET_ACQUIRER_DATA)) {
				ResetValues();
//				listener.ecrWrite(message->getResponse(),message->getResponseLen());
				log.message(MSG_INFO "1 \r\n");
				break;
			} else {
				memcpy(txPacket,message->getResponse(),message->getResponseLen());
				txPacketLen = message->getResponseLen();

//				log.message(MSG_INFO ""+SSTR(message->getResponse())+"\r\n");

				if(message->getResponse()[1]=='C')
					message->ecrSaleDataDisplay();	//iq_audi_201216
				else if(message->getResponse()[1]=='A' && parametersInProgress) //Parameters Update Successfully
				{
					listener.ecrWrite((unsigned char *)"\x02\x50\x61\x72\x61\x6D\x65\x74\x65\x72\x73\x20\x55\x70\x64\x61\x74\x65\x20\x53\x75\x63\x63\x65\x73\x73\x66\x75\x6C\x6C\x79\x03\x23",33);

					uiDisplay(REBOOTING_SCREEN);
					Secins_reboot();
				}
				else if(message->getResponse()[1]=='A' && bankingInProgress)
					message->ecrSettlementDataDisplay(true);
				else if(message->getResponse()[1]=='A' ) //Batch Number Update Successfully
					listener.ecrWrite((unsigned char *)"\x02\x42\x61\x74\x63\x68\x20\x4E\x75\x6D\x62\x65\x72\x20\x55\x70\x64\x61\x74\x65\x20\x53\x75\x63\x63\x65\x73\x73\x66\x75\x6C\x6C\x79\x03\x46",35);

				iState=3;
				iTxRetry=6;
				tExpire=time(NULL)+2;
			}
//			listener.ecrWrite(message->getResponse(),message->getResponseLen());

		} else {
			log.message(MSG_INFO "Sending EOT to ecr. Response len = 0\r\n");
//			listener.ecrWrite((unsigned char *)"\x04",1); //iq_audi_201216
			break;
		}

		while  (!resultReceived)
			usleep(100000);

		if (!receivedAck)
			break;

		receivedAck = true;
		resultReceived = false;
	} while (!message->isTransferComplete());

	receivedAck = true;
	resultReceived = false;
	busy=false;

	// busy is still true - wait for ack
	com_verifone_ipc::deinit(0);
	log.message(MSG_INFO "ProcessMessageThread thread exiting\r\n");

	return NULL;
}

#define STATUS_THREAD_SLEEP_TIME_US		250000		// Sleep time in micro seconds
#define STATUS_THREAD_5SECS				5000000 	// Amount of micro seconds to sleep for 5 seconds to elapse

void* StatusThread(void*) {

	char resp[3];
	unsigned int sleep_counter = 1;

	int iret = com_verifone_ipc::init(std::string(POS_STATUS_TASK));
	if (iret != 0) {
		log.message(MSG_INFO "Cannot not open ipc. error - "+SSTR(iret)+"\r\n");
		return NULL;
	}

	STATUS status=TERMINAL_READY;

	while (1) {
		usleep(STATUS_THREAD_SLEEP_TIME_US);

		if(sleep_counter < STATUS_THREAD_5SECS){
			sleep_counter += STATUS_THREAD_SLEEP_TIME_US;
		}else{
			sleep_counter = 0;
		}

		if (busy && (ignoreAcks == true) && (txPacketLen == 0) && (reqType == com_verifone_posmessage::TRANSACTION_REQUEST || reqType == com_verifone_posmessage::REVERSAL)) {
			string ipcBuffer, ipcFrom;
			if(com_verifone_ipc::receive(ipcBuffer,"STATUS", ipcFrom)==com_verifone_ipc::IPC_SUCCESS){
				status = (STATUS)atoi(ipcBuffer.c_str());
				log.message(MSG_INFO "PosStatus received status ["+SSTR(resp)+"]\n");
			}

			if(status == prevStatus){	// If the status did not change

#if 0		// Optional sending of busy status at 5 second intervals

				if(sleep_counter == 0){	// If the 5 second interval elapsed
					sprintf(resp,"%02d", TERMINAL_BUSY);

					log.message(MSG_INFO "Send status ["+SSTR(resp)+"]\n");

					ECommand posMessage;
					posMessage.pack(resp);
					listener.ecrWrite(posMessage.getResponse(),posMessage.getResponseLen());
				}
#endif

			}else{
				log.message(MSG_INFO "Send status ["+SSTR(status)+"]\n");
				//iq_audi_261216
				if(status == WAITING_FOR_CARD_INSERT )	//Card Prompt
					listener.ecrWrite((unsigned char *)"\x02\x43\x61\x72\x64\x20\x50\x72\x6F\x6D\x70\x74\x03\x31",14);
				else if(status == COMMS_CONNECTING) //Connecting Bank Host
					listener.ecrWrite((unsigned char *)"\x02\x43\x6F\x6E\x6E\x65\x63\x74\x69\x6E\x67\x20\x42\x61\x6E\x6B\x20\x48\x6F\x73\x74\x03\x39",23);
				else if(status == TRANSACTION_GOING_ONLINE) //Transacting
					listener.ecrWrite((unsigned char *)"\x02\x54\x72\x61\x6E\x73\x61\x63\x74\x69\x6E\x67\x03\x4D",14);
				else if(status == WAITING_CARD_REMOVAL) //Waiting For Card Removal
					listener.ecrWrite((unsigned char *)"\x02\x57\x61\x69\x74\x69\x6E\x67\x20\x46\x6F\x72\x20\x43\x61\x72\x64\x20\x52\x65\x6D\x6F\x76\x61\x6C\x03\x4B",27);
				else if(status == CARD_REMOVED) //Card Removed
					listener.ecrWrite((unsigned char *)"\x02\x43\x61\x72\x64\x20\x52\x65\x6D\x6F\x76\x65\x64\x03\x57",15);
//				else if(status == TRANSACTION_GOING_ONLINE) //Sending Data
//					listener.ecrWrite((unsigned char *)"\x02\x53\x65\x6E\x64\x69\x6E\x67\x20\x44\x61\x74\x61\x03\x4D",15);
//				else if(status == WAITING_FOR_RESPONSE) //Receiving Data
//					listener.ecrWrite((unsigned char *)"\x02\x52\x65\x63\x65\x69\x76\x69\x6E\x67\x20\x44\x61\x74\x61\x03\x5F",17);
				else if(status == ENTER_PIN) //PIN Prompt
					listener.ecrWrite((unsigned char *)"\x02\x50\x49\x4E\x20\x50\x72\x6F\x6D\x70\x74\x03\x52",13);

				prevStatus = status;
				sprintf(resp,"%02d",status);

				ECommand posMessage;
				posMessage.pack(resp);
//				listener.ecrWrite(posMessage.getResponse(),posMessage.getResponseLen());

				sleep_counter = 0;		// Clear the sleep counter
			}
		} else if (busy && ignoreAcks == true && txPacketLen == 0 && reqType == GET_STATUS && (bankingInProgress || parametersInProgress)) {
			if (bankingInProgress && !sentStartStatus){
				sprintf(resp,"%02d",BANKING_STARTED);
				sentStartStatus = true;
			}else if (parametersInProgress && !sentStartStatus){
				sprintf(resp,"%02d",PARM_DOWNLOAD_STARTING);
				sentStartStatus = true;
			}else if(sleep_counter == 0)
				sprintf(resp,"%02d",TERMINAL_BUSY);
			else{
				continue;
			}

			log.message(MSG_INFO "Send status ["+SSTR(resp)+"]\n");

			ECommand posMessage;
			posMessage.pack(resp);
//			listener.ecrWrite(posMessage.getResponse(),posMessage.getResponseLen());
			sleep_counter = 0;		// Clear the sleep counter
		} else {

			if (doNotShowCardRemoval && (sleep_counter == 0)) {
				log.message(MSG_INFO "Send ENQ\n");
//				listener.ecrWrite((unsigned char *)"\x05",1);
				sleep_counter = 0;		// Clear the sleep counter
			}
		}
	}

	return NULL;
}

TERMINAL_TYPE UtilsGetTerminalType(void)
{
	char buffer[20];
	unsigned long rlen=0;

	memset(buffer, 0, sizeof(buffer));

	if(platforminfo_get(PI_MIB_MODEL_NUM ,buffer, (unsigned long)sizeof(buffer), &rlen) == PI_OK){
		if(memcmp(buffer, "UX300", 5) == 0)
			return TT_UX_300;
		else if(memcmp(buffer, "MX925", 5) == 0)
			return TT_MX_925;
	}

	return TT_UNKNOWN;
}

string UtilsStringToHex(string src)
{
	char temp;
	string sRet;

	for (unsigned i = 0; i < src.length(); i++) {
		if((src.at(i) >= '0') && (src.at(i) <= '9'))
			temp = (src.at(i) - '0') << 4;
		else
			temp = (toupper(src.at(i)) - '7') << 4;

		i++;

		if((src.at(i) >= '0') && (src.at(i) <= '9'))
			temp |= src.at(i) - '0';
		else
			temp |= toupper(src.at(i)) - '7';

		sRet.append(1, temp);
	}

	return sRet;
}


