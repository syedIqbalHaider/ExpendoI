#include <fcntl.h>
#include <errno.h>
#include <svc.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sstream>
#include <algorithm>
#include <libda/cterminalconfig.h>
#include <libipc/ipc.h>
#include <libcpa.h>
#include <iomanip>
#include "clog.h"
#include "posmessage.h"
#include "bertlv.h"
#include "bcommand.h"
#include <libda/cterminalconfig.h>
#include "cstatus.h"


extern "C" {
	#include <platforminfo_api.h>
}

#define SSTR( x ) dynamic_cast< std::ostringstream & >( std::ostringstream() << std::dec << x ).str()

#define ETX 0x03
#define STX 0x02
#define ACK 0x06
#define NAK 0x15
#define EOT 0x04
#define ESC 0x1B

static CLog log(POS_TASK);

using namespace za_co_verifone_bertlv;
using namespace com_verifone_status;
using namespace com_verifone_terminalconfig;

namespace com_verifone_bcommand
{
	BCommand::BCommand(){}

	bool BCommand::unpack(unsigned char *request,unsigned int length) {
		(void)request;			// Suppresses -> warning: unused parameter
		(void)length;			// Suppresses -> warning: unused parameter

		stringstream ss;
		log.message(MSG_INFO "Received an B command\r\n");

		reqType = GET_ACQUIRER_DATA;

		CTerminalConfig terminalConfig;

		ss << setw(15) << terminalConfig.getMerchantNo();
		merchantNo = ss.str();

		ss.str(std::string());

		ss << setw(8) << terminalConfig.getTerminalNo();
		terminalId = ss.str();
		return true;
	}

	bool BCommand::pack() {
		log.message(MSG_INFO "Pack a B command response\r\n");
		responseLen = 26;

		response[0] = 0x42;
		if (!busyCannotProcess) {
			response[1] = 0x30;
			response[2] = 0x30;
		} else {
			response[1] = 0x30;
			response[2] = 0x32;
		}

		memcpy(&response[3],merchantNo.c_str(),15);
		memcpy(&response[18],terminalId.c_str(),8);
		encapsulate();
		return true;
	}

	bool BCommand::encapsulate() {
		unsigned char tmpMessage[8192];
		unsigned char ucData;
		unsigned char ucLrc=0;
		unsigned int newLen;

		if(response == NULL) {
			log.message(MSG_INFO "response is empty\r\n");
			return false;
		}

		memcpy(tmpMessage,response,responseLen);
		response[0]=STX;
		newLen=1;
		for(int index=0;index<responseLen;index++) {
			ucData=tmpMessage[index];
			ucLrc^=ucData;
			if(ucData==STX || ucData==ETX || ucData==ESC) {
				response[newLen++]=ESC;
				ucData+=(unsigned char)0x20;
			}
			response[newLen++]=ucData;
		}
		response[newLen++]=ETX;
		if(ucLrc==STX || ucLrc==ETX || ucLrc==ESC) {
			response[newLen++]=ESC;
			ucLrc+=(unsigned char)0x20;
		}
		response[newLen++]=ucLrc;
		responseLen = newLen;

		return true;
	}

	bool BCommand::process(void) {
		// check if terminal is busy with banking
		CStatus statusClass;
		OPERATION_STATUS statusCode = statusClass.getOperation();

		switch (statusCode) {
			case OP_STAT_PARAM_DOWNLOAD:
			case OP_STAT_BANKING:
				busyCannotProcess = true;
				break;
			default:
				busyCannotProcess = false;
				break;
		}

		return true;
	}

	bool BCommand::unpackRespTLVs(std::map<std::string, std::string> &txresult) {
		(void)txresult;			// Suppresses -> warning: unused parameter
		return true;
	}

}
