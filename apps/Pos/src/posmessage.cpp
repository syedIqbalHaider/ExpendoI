#include <fcntl.h>
#include <errno.h>
#include <svc.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sstream>
#include <algorithm>
#include <libda/cterminalconfig.h>
#include <libda/caccountprofile.h>
#include <libipc/ipc.h>
#include <libcpa.h>
#include <iomanip>
#include "clog.h"
#include "posmessage.h"
#include "bertlv.h"
#include <sysinfo.h>

//iq_audi_030117
#include <libda/cconmanager.h>
#include <libda/cbatchmanager.h>
#include <libda/cbatch.h>
#include "utils.h"

static CLog log(POS_TASK);

#define ETX 0x03
#define STX 0x02
#define ACK 0x06
#define NAK 0x15
#define EOT 0x04
#define ESC 0x1B

using namespace com_verifone_posmessage;
using namespace com_verifone_terminalconfig;
using namespace com_verifone_accountprofile ;
using namespace za_co_verifone_bertlv;
using namespace com_verifone_batch;
using namespace vfisysinfo;
using std::cout;

namespace com_verifone_posmessage
{

	CListener listener;

	// Initialise static properties
	bool PosMessage::busyCannotProcess = false;

	PosMessage::PosMessage():responseLen(0),doNotShowCardRemoval(false),reqType(GET_STATUS),checkCardRemovalFlag(false), transferComplete(true), bankingInProgress(false),parametersInProgress(false),hcommandEnabled(false){}

	int PosMessage::getResponseLen(void) {
		return responseLen;
	}

	unsigned char* PosMessage::getResponse(void) {
		return response;
	}

	bool PosMessage::isBusyCannotProcess(void) {
		return busyCannotProcess;
	}

	bool PosMessage::isDoNotShowCardRemoval(void) {
		return doNotShowCardRemoval;
	}

	bool PosMessage::isCheckCardRemovalFlag(void) {
		return checkCardRemovalFlag;
	}

	REQ_TYPE PosMessage::getRequestType(void) {
		return reqType;
	}

	bool PosMessage::isTransferComplete(void) {
		return transferComplete;
	}

	bool PosMessage::packBusyCannotProcess(void) {
		log.message(MSG_INFO "Pack a Busy Cannot process\r\n");
		std::string asciiPan;
		responseLen = 1;
		response[0] = 0x04;
		return true;
	}

	bool PosMessage::isBankingInProgress(void){
		return bankingInProgress;
	}

	bool PosMessage::isParametersInProgress(void) {
		return parametersInProgress;
	}

	bool PosMessage::unpackReqTLVs(std::string tlvs) {
		int pos = 0;
		BERTLV berTlv(tlvs);
		BERTLV tmpTlv;
		BERTLV doNotremovecard;
		BERTLV prodLoadTlv;
		BERTLV validatePanTlv;
		std::string tag,val,prodLoad;
		char tChar;

		if  (tlvs.length() < 2)
			return true;

		log.message(MSG_INFO "TLV   : "+hexToAscii((unsigned char*)tlvs.c_str(),tlvs.length())+"\r\n");

		tmpTlv.clear();
		while (true) {
			if (berTlv.getTag(pos,tag) == 0)
				break;

			log.message(MSG_INFO "TAG   : "+hexToAscii((unsigned char*)tag.c_str(),tag.length())+"\r\n");
			if (berTlv.getValForTag(tag,val) ==0)
				break;

			log.message(MSG_INFO "VAL   : "+hexToAscii((unsigned char*)val.c_str(),val.length())+"\r\n");
			pos = berTlv.getNextPosition();

			tChar = tag.c_str()[1] ;
			if (tChar == 0x00) {
				validatePanTlv.addTag(std::string("\x5A"),val);
				requestTlvs.append(validatePanTlv.getStringData());
			} else if (tChar == 0x01) {
				//todo
				log.message(MSG_INFO "TLV Not Implemented\r\n");
			} else if (tChar == 0x02) {
				tmpTlv.addTag(std::string("\xFF\x02"),val);
			} else if (tChar == 0x08) {
				//todo
				log.message(MSG_INFO "TLV Not Implemented\r\n");
			} else if (tChar == 0x09) {
				refNum = val;
			} else if (tChar == 0x12) {
				doNotremovecard.addTag(std::string("\xFF\xA1\x10"),val);
				requestTlvs.append(doNotremovecard.getStringData());
				doNotShowCardRemoval = true;
			} else if (tChar == 0x14) {
				if (atoi(val.c_str()) == 1) {
					hcommandEnabled = true; // H command enabled
					log.message(MSG_INFO "H Command enabled\r\n");
					}
			}else if (tChar == 0x40) {
				ecrNo = hexToAscii((unsigned char*)val.c_str(),val.length());
				log.message(MSG_INFO "ECR number ["+ ecrNo+"]\r\n");
			}
		}

		if (!tmpTlv.getStringData().empty()) {
			prodLoadTlv.addTag(std::string("\xFF\xA1\x11"),tmpTlv.getStringData());
			requestTlvs.append(prodLoadTlv.getStringData());
		}

		return true;
	}

	std::string PosMessage::bcdToString(const char *data, unsigned int len)
	{
		char c;
		stringstream ss;

		for (unsigned int i=0; i<len; i++) {
			if(((data[i] >> 4) & 0x0f) == 0x0f) break;

			c = ((data[i] >> 4) & 0x0f) + '0';
			if(c > '9') break;
			ss << c;

			if((data[i] & 0x0f) == 0x0f) break;

			c = (data[i] & 0x0f) + '0';
			if(c > '9') break;
			ss << c;
		}

		return ss.str();
	}

	std::string PosMessage::hexToAscii(unsigned char *in,int inLen) {
		char tmpCharBuf[3];
		std::string tmpString;
		for(int i=0;i<inLen;i++) {
			sprintf(tmpCharBuf,"%02X", in[i]);
			tmpString += tmpCharBuf;
		}

		return tmpString;
	}

	void PosMessage::swopBuffer(char *in, char *out) {
		out[0] = in[3];
		out[1] = in[2];
		out[2] = in[1];
		out[3] = in[0];
	}

	string PosMessage::zero_pad(int num, int pad_count)
	{
		std::ostringstream ss;
		ss << std::setw(pad_count) << std::setfill('0') << num;
		return ss.str();
	}

	string PosMessage::zero_pad(string str, int pad_count)
	{
		std::ostringstream ss;
		ss << std::setw(pad_count) << std::setfill('0') << str;
		return ss.str();
	}

	string PosMessage::space_pad(string str, int pad_count)
	{
		std::ostringstream ss;
		ss << std::setw(pad_count) << std::setfill(' ') << str;
		return ss.str();
	}

	unsigned long hexStringToLong(const string &len)
	{
		unsigned long ulResult=0;
		unsigned int start_index=0, len_count=len.length();

		if(len.at(0) & 0x80){
			start_index = 1;
			len_count = (len.at(0) & 0x7f) + 1;
		}
		for(unsigned int iIndex=start_index;iIndex<len_count
									&& iIndex<len.length();
												iIndex++) {
			ulResult<<=8;
			ulResult+=(unsigned char)len.at(iIndex);
		}

		return(ulResult);
	}

	void PosMessage::FilterEMVTags(string tags, string &filtered_tags)
	{
		BERTLV tlv(tags);
		BERTLV filtered_tlv;
		int pos=0;

		while(1){
			string tag, len, val;
			int ret;
			ret = tlv.getTagLenVal(pos, tag, len, val);
			if(ret){
				// Cant use val.length() below because getTagLenVal may return len == 0 and val not initialised to empty.
				if(hexStringToLong(len) > 0){
					filtered_tlv.appendTag(tag, val);
					log.message(MSG_INFO+hexToAscii((unsigned char*)tag.c_str(), tag.length())+" ("+hexToAscii((unsigned char*)len.c_str(), len.length())+"):"+hexToAscii((unsigned char*)val.c_str(), val.length())+"\n");
				}else{
					log.message(MSG_INFO+hexToAscii((unsigned char*)tag.c_str(), tag.length())+" ("+hexToAscii((unsigned char*)len.c_str(), len.length())+"): --DROPPED--\n");
				}

				pos = ret;
			}
			else
				break;
		}

		filtered_tags = filtered_tlv.getStringData();
	}


	void PosMessage::ecrSaleDataDisplay(void)
	{
		unsigned char buffer[1024];
		unsigned char temp[64]={0};
		unsigned char date[10+1]={0};
		unsigned char time[8+1]={0};
		unsigned char ucLrc=0;
		const int SEPARATORS=3;
		string value;
		int bufLen=0;

		memcpy(buffer, "\x02",1);		//STX
		memcpy(buffer+1,"TxnResp=",8);
		bufLen=9;

	//	Date
		memset(temp,0,sizeof(temp));

		if(dateTime.length()==0)
			dateTime = Utils::UtilsTimestamp();

		memcpy(temp,dateTime.c_str(),14);

		date[0]=temp[6]; date[1]=temp[7];
		date[2]='/';
		date[3]=temp[4]; date[4]=temp[5];
		date[5]='/';
		date[6]=temp[0]; date[7]=temp[1]; date[8]=temp[2]; date[9]=temp[3];

		ecrData(buffer+bufLen,(unsigned char *)"Date",4,true,date,10,true);
		bufLen+=4+10+SEPARATORS;

	//	Time
		time[0]=temp[8]; time[1]=temp[9];
		time[2]=':';
		time[3]=temp[10]; time[4]=temp[11];
		time[5]=':';
		time[6]=temp[12]; time[7]=temp[13];

		ecrData(buffer+bufLen,(unsigned char *)"Time",4,true,time,8,true);
		bufLen+=4+8+SEPARATORS;

	//	Terminal ID
		CTerminalConfig terminalConfig;
		value.clear();
		value=zero_pad(terminalConfig.getTerminalNo(),8);
		ecrData(buffer+bufLen,(unsigned char *)"TID",3,true,(unsigned char *)value.c_str(),8,true);
		bufLen+=3+8+SEPARATORS;

	//	Merchant ID
		value.clear();
		value=zero_pad(terminalConfig.getMerchantNo(),15);
		ecrData(buffer+bufLen,(unsigned char *)"MID",3,true,(unsigned char *)value.c_str(),15,true);
		bufLen+=3+15+SEPARATORS;

	//	Transaction Type
		memset(temp,0,sizeof(temp));

		if(reqType==VOID)
			memcpy(temp,"Void",sizeof("Void"));
		else
			memcpy(temp,"Sale",sizeof("Sale"));

		ecrData(buffer+bufLen,(unsigned char *)"Txn",3,true,temp ,strlen((char *)temp),true);
		bufLen+=3+strlen((char *)temp)+SEPARATORS;

	//	Batch Number
		value.clear();
		value=zero_pad(this->batchNo,6);
		ecrData(buffer+bufLen,(unsigned char *)"Batch",5,true,(unsigned char *)value.c_str(),6,true);
		bufLen+=5+6+SEPARATORS;

	//	Invoice Number
		value.clear();
		value=zero_pad(this->sequenceNo,6);
		ecrData(buffer+bufLen,(unsigned char *)"Invoice",7,true,(unsigned char *)value.c_str(),6,true);
		bufLen+=7+6+SEPARATORS;

	//	Ecr Reference Number
		value.clear();
		value=zero_pad(this->ecrNo,8);
		ecrData(buffer+bufLen,(unsigned char *)"EcrRefNo",8,true,(unsigned char *)value.c_str(),8,true);
		bufLen+=8+8+SEPARATORS;

	//	PAN
		if(maskPan.length()!=0)
			maskPan.replace(0, maskPan.length()-4,maskPan.length()-4, '*');

		memset(temp,0,sizeof(temp));
		ecrData(buffer+bufLen,(unsigned char *)"Card",4,true,(unsigned char *)maskPan.c_str(),maskPan.length(),true);
		bufLen+=4+maskPan.length()+SEPARATORS;

	//	Card Mode
		memset(temp,0,sizeof(temp));
		if(memcmp(cardEntry,"E",1)==0)
			memcpy(temp,"Manual",strlen("Manual"));
		else if(memcmp(cardEntry,"F",1)==0)
			memcpy(temp,"Fallback",strlen("Fallback"));
		else if(memcmp(cardEntry,"S",1)==0)
			memcpy(temp,"Swipe",strlen("Swipe"));
		else if(memcmp(cardEntry,"I",1)==0)
			memcpy(temp,"Chip",strlen("Chip"));
		else if(memcmp(cardEntry,"T",1)==0)
			memcpy(temp,"Contactless",strlen("Contactless"));
		else
			memcpy(temp,"No Card",strlen("No Card"));

		ecrData(buffer+bufLen,(unsigned char *)"Entry",5,true,temp,strlen((char *)temp),true);
		bufLen+=5+strlen((char *)temp)+SEPARATORS;

	//	Amount
		ecrData(buffer+bufLen,(unsigned char *)"Amount",6,true,(unsigned char *)this->amount.c_str(),this->amount.length(),true);
		bufLen+=6+this->amount.length()+SEPARATORS;

	//	Response Code
		value.clear();

		if(respMesg.empty() || (!isTxnApproved && atoi((const char *)this->responseCode.c_str())==0))
			value=SSTR("-1");
		else
			value=zero_pad(this->responseCode,2);

		ecrData(buffer+bufLen,(unsigned char *)"RespCode",8,true,(unsigned char *)value.c_str(),2,true);
		bufLen+=8+2+SEPARATORS;

	//	Response Message
		if(respMesg.empty())
			respMesg=SSTR("Cancelled");
		else if(!isTxnApproved && strcmp(respMesg.c_str(),"Approved")==0)
			respMesg=SSTR("Declined");

		ecrData(buffer+bufLen,(unsigned char *)"RespMsg",7,true,(unsigned char *)this->respMesg.c_str(),this->respMesg.length(),true);
		bufLen+=7+this->respMesg.length()+SEPARATORS;


	//	Retrieval Reference Number
		value.clear();
		value=space_pad(this->rrn,12);
		ecrData(buffer+bufLen,(unsigned char *)"RRN",3,true,(unsigned char *)value.c_str(),12,true);
		bufLen+=3+12+SEPARATORS;

	//	AuthID
		value.clear();
		value=space_pad(this->authCode,6);
		cout << "iq  Auth ID  " << this->authCode<< endl;
		ecrData(buffer+bufLen,(unsigned char *)"Auth",4,true,(unsigned char *)value.c_str(),6,true);
		bufLen+=4+6+SEPARATORS;

	//	CVM
		CAccountProfile accprof;
		memset(temp,0,sizeof(temp));

		if(isTxVoided)
			memcpy(temp,"N/A",3);
		else if(accprof.isRequirePinEntry())
			memcpy(temp,"PIN",3);
		else
			memcpy(temp,"NOCVM",5);

		ecrData(buffer+bufLen,(unsigned char *)"CVM",3,true,temp,strlen((char *)temp),true);
		bufLen+=strlen((char *)temp)+3+SEPARATORS;

	//	ICC Data
		if(memcmp(cardEntry,"I",1)==0)
		{
			value.clear();
			value=Utils::UtilsHexToString(aid.c_str(),aid.length());
			ecrData(buffer+bufLen,(unsigned char *)"AID",3,true,(unsigned char *)value.c_str(),value.length(),true);
			bufLen+=3+value.length()+SEPARATORS;

			value.clear();
			value=Utils::UtilsHexToString(tvr.c_str(),tvr.length());
			ecrData(buffer+bufLen,(unsigned char *)"TVR",3,true,(unsigned char *)value.c_str(),value.length(),true);
			bufLen+=3+value.length()+SEPARATORS;

			value.clear();
			value=Utils::UtilsHexToString(tc.c_str(),tc.length());
			ecrData(buffer+bufLen,(unsigned char *)"TC",2,true,(unsigned char *)value.c_str(),value.length(),true);
			bufLen+=2+value.length()+SEPARATORS;
		}else
		{
			ecrData(buffer+bufLen,(unsigned char *)"AID",3,true,(unsigned char *)"N/A",3,true);
			bufLen+=3+3+SEPARATORS;

			ecrData(buffer+bufLen,(unsigned char *)"TVR",3,true,(unsigned char *)"N/A",3,true);
			bufLen+=3+3+SEPARATORS;

			ecrData(buffer+bufLen,(unsigned char *)"TC",2,true,(unsigned char *)"N/A",3,true);
			bufLen+=2+3+SEPARATORS;
		}

	//  Card Issuer Name
		if(cardType.length())
		{
			ecrData(buffer+bufLen,(unsigned char *)"CardIssuer",10,true,(unsigned char *)cardType.c_str(),cardType.length(),true);
			bufLen+=cardType.length()+10+SEPARATORS;
		}else
		{
			ecrData(buffer+bufLen,(unsigned char *)"CardIssuer",10,true,(unsigned char *)"N/A",3,true);
			bufLen+=3+10+SEPARATORS;
		}


		memcpy(buffer+bufLen,"\x03",1);		//ETX
		bufLen+=1;

		for(int i=0; i<bufLen; i++)		// lrc
			ucLrc^=buffer[i];

		memcpy(buffer+bufLen,&ucLrc,1);
		bufLen+=1;

		listener.ecrWrite(buffer,bufLen);

	}

	void PosMessage::ecrSettlementDataDisplay(bool isBatchProcessSucessfully)
	{
		unsigned char buffer[1024];
		unsigned char temp[64]={0};
		unsigned char date[10+1]={0};
		unsigned char time[8+1]={0};
		unsigned char ucLrc=0;
		const int SEPARATORS=3;
		string value;
		int bufLen=0;

		memcpy(buffer, "\x02",1);		//STX
		memcpy(buffer+1,"SettlementResp=",15);
		bufLen=16;

		int ret;
		com_verifone_batchmanager::CBatchManager batchManager;
		com_verifone_batch::CBatch cbatch;
		int debits_count=0, debits_count_second_curr=0, debits_total_amount=0,debits_total_amount_second_curr=0;
		int	credits_count=0,credits_total_amount=0, credits_total_amount_second_curr=0, credits_count_second_curr=0;
		int authorisations=0, authorisations_total_amount=0;

		if(isBatchProcessSucessfully==true)
			ret = batchManager.getPreviousBatch(cbatch);
		else
			ret = batchManager.getBatch(cbatch);

		if (ret != DB_OK){
			log.message(MSG_INFO "Failure retrieving previous  batch record\r\n");
			return;
		}

		std::map<unsigned int, com_verifone_batchrec::CBatchRec> records;
		cbatch.getRecords(records);

		std::map<unsigned int,CBatchRec>::iterator it;
		for (it=records.begin(); it!=records.end(); ++it) {
			CBatchRec batchRec = it->second;

//			 Make sure that only completed transactions are processed
			if(batchRec.isInProgress() || (batchRec.isDeclined())
					|| batchRec.isTxCanceled()) continue;

			string tx_code = batchRec.getTxCode();
			int itx_code = Utils::UtilsStringToInt(tx_code);

			if(itx_code != 0x00 && itx_code != 0x20 && itx_code != 0x02)	//Sale=0x00 Refund=0x20
				continue;

			if(itx_code == 0x00){
				if(batchRec.getTransCurrency().compare("840")==0) //USD
				{
					debits_count++;
					debits_total_amount += batchRec.getTxAmount()+batchRec.getCashAmount();
				}else  //LBP
				{
					debits_count_second_curr++;
					debits_total_amount_second_curr += batchRec.getTxAmount()+batchRec.getCashAmount();
				}
			}else if((itx_code == 0x20) || (itx_code == 0x02)){

				if(batchRec.getTransCurrency().compare("840")==0) //USD
				{
					credits_count++;
					credits_total_amount += batchRec.getTxAmount()+batchRec.getCashAmount();
				}else  //LBP
				{
					credits_count_second_curr++;
					credits_total_amount_second_curr += batchRec.getTxAmount()+batchRec.getCashAmount();
				}
			}

			if(batchRec.getDraftCapMode() == OFFLINE){
				authorisations++;
				authorisations_total_amount += batchRec.getTxAmount()+batchRec.getCashAmount();;
			}
		}

	//	Date
		memset(temp,0,sizeof(temp));
		dateTime = Utils::UtilsTimestamp();
		memcpy(temp,dateTime.c_str(),14);
		date[0]=temp[6]; date[1]=temp[7];
		date[2]='/';
		date[3]=temp[4]; date[4]=temp[5];
		date[5]='/';
		date[6]=temp[0]; date[7]=temp[1]; date[8]=temp[2]; date[9]=temp[3];

		ecrData(buffer+bufLen,(unsigned char *)"Date",4,true,date,10,true);
		bufLen+=4+10+SEPARATORS;

	//	Time
		time[0]=temp[8]; time[1]=temp[9];
		time[2]=':';
		time[3]=temp[10]; time[4]=temp[11];
		time[5]=':';
		time[6]=temp[12]; time[7]=temp[13];

		ecrData(buffer+bufLen,(unsigned char *)"Time",4,true,time,8,true);
		bufLen+=4+8+SEPARATORS;

	//	Terminal ID
		CTerminalConfig terminalConfig;
		value.clear();
		value=zero_pad(terminalConfig.getTerminalNo(),8);
		ecrData(buffer+bufLen,(unsigned char *)"TID",3,true,(unsigned char *)value.c_str(),8,true);
		bufLen+=3+8+SEPARATORS;

	//	Merchant ID
		value.clear();
		value=zero_pad(terminalConfig.getMerchantNo(),15);
		ecrData(buffer+bufLen,(unsigned char *)"MID",3,true,(unsigned char *)value.c_str(),15,true);
		bufLen+=3+15+SEPARATORS;

	//	Transaction Type
		memset(temp,0,sizeof(temp));
		memcpy(temp,"Settlement",sizeof("Settlement"));
		ecrData(buffer+bufLen,(unsigned char *)"Txn",3,true,temp ,strlen((char *)temp),true);
		bufLen+=3+strlen((char *)temp)+SEPARATORS;

	//	Batch Number
		value.clear();
		value=zero_pad(cbatch.getBatchNo(),6);
		ecrData(buffer+bufLen,(unsigned char *)"Batch",5,true,(unsigned char *)value.c_str(),6,true);
		bufLen+=5+6+SEPARATORS;

	//	Currency LBP
		ecrData(buffer+bufLen,(unsigned char *)"First Currency",strlen("First Currency"),true,(unsigned char *)"LBP",3,true);
		bufLen+=strlen("First Currency")+strlen("LBP")+SEPARATORS;

	//	Sale Amount
		value.clear();
		value=Utils::UtilsIntToString(debits_total_amount_second_curr,12);
		ecrData(buffer+bufLen,(unsigned char *)"Sale Amount",strlen("Sale Amount"),true,(unsigned char *)value.c_str(),value.length(),true);
		bufLen+=strlen("Sale Amount")+value.length()+SEPARATORS;

	//	Sale Count
		value.clear();
		value=Utils::UtilsIntToString(debits_count_second_curr,6);
		ecrData(buffer+bufLen,(unsigned char *)"Sale Count",strlen("Sale Count"),true,(unsigned char *)value.c_str(),value.length(),true);
		bufLen+=strlen("Sale Count")+value.length()+SEPARATORS;

	//	Refund Amount
		ecrData(buffer+bufLen,(unsigned char *)"Refund Amount",strlen("Refund Amount"),true,(unsigned char *)"000000000000",12,true);
		bufLen+=strlen("Refund Amount")+12+SEPARATORS;

	//	Refund Count
		ecrData(buffer+bufLen,(unsigned char *)"Refund Count",strlen("Refund Count"),true,(unsigned char *)"000000",6,true);
		bufLen+=strlen("Refund Count")+6+SEPARATORS;

	//	Void Amount
		value.clear();
		value=Utils::UtilsIntToString(credits_total_amount_second_curr,12);
		ecrData(buffer+bufLen,(unsigned char *)"Void Amount",strlen("Void Amount"),true,(unsigned char *)value.c_str(),value.length(),true);
		bufLen+=strlen("Void Amount")+value.length()+SEPARATORS;

	//	Void Count
		value.clear();
		value=Utils::UtilsIntToString(credits_count_second_curr,6);
		ecrData(buffer+bufLen,(unsigned char *)"Void Count",strlen("Void Count"),true,(unsigned char *)value.c_str(),value.length(),true);
		bufLen+=strlen("Void Count")+value.length()+SEPARATORS;

	//	Total Amount
		value.clear();
		value=Utils::UtilsIntToString(debits_total_amount_second_curr,12);
		ecrData(buffer+bufLen,(unsigned char *)"Total Amount",strlen("Total Amount"),true,(unsigned char *)value.c_str(),value.length(),true);
		bufLen+=strlen("Total Amount")+value.length()+SEPARATORS;

	//	Total Count
		value.clear();
		value=Utils::UtilsIntToString(debits_count_second_curr,6);
		ecrData(buffer+bufLen,(unsigned char *)"Total Count",strlen("Total Count"),true,(unsigned char *)value.c_str(),value.length(),true);
		bufLen+=strlen("Total Count")+value.length()+SEPARATORS;

	//	Currency USD
		ecrData(buffer+bufLen,(unsigned char *)"Second Currency",strlen("Second Currency"),true,(unsigned char *)"USD",3,true);
		bufLen+=strlen("Second Currency")+strlen("USD")+SEPARATORS;

	//	Sale Amount
		value.clear();
		value=Utils::UtilsIntToString(debits_total_amount,12);
		ecrData(buffer+bufLen,(unsigned char *)"Sale Amount",strlen("Sale Amount"),true,(unsigned char *)value.c_str(),value.length(),true);
		bufLen+=strlen("Sale Amount")+value.length()+SEPARATORS;

	//	Sale Count
		value.clear();
		value=Utils::UtilsIntToString(debits_count,6);
		ecrData(buffer+bufLen,(unsigned char *)"Sale Count",strlen("Sale Count"),true,(unsigned char *)value.c_str(),value.length(),true);
		bufLen+=strlen("Sale Count")+value.length()+SEPARATORS;

	//	Refund Amount
		ecrData(buffer+bufLen,(unsigned char *)"Refund Amount",strlen("Refund Amount"),true,(unsigned char *)"000000000000",12,true);
		bufLen+=strlen("Refund Amount")+12+SEPARATORS;

	//	Refund Count
		ecrData(buffer+bufLen,(unsigned char *)"Refund Count",strlen("Refund Count"),true,(unsigned char *)"000000",6,true);
		bufLen+=strlen("Refund Count")+6+SEPARATORS;

	//	Void Amount
		value.clear();
		value=Utils::UtilsIntToString(credits_total_amount,12);
		ecrData(buffer+bufLen,(unsigned char *)"Void Amount",strlen("Void Amount"),true,(unsigned char *)value.c_str(),value.length(),true);
		bufLen+=strlen("Void Amount")+value.length()+SEPARATORS;

	//	Void Count
		value.clear();
		value=Utils::UtilsIntToString(credits_count,6);
		ecrData(buffer+bufLen,(unsigned char *)"Void Count",strlen("Void Count"),true,(unsigned char *)value.c_str(),value.length(),true);
		bufLen+=strlen("Void Count")+value.length()+SEPARATORS;

	//	Total Amount
		value.clear();
		value=Utils::UtilsIntToString(debits_total_amount,12);
		ecrData(buffer+bufLen,(unsigned char *)"Total Amount",strlen("Total Amount"),true,(unsigned char *)value.c_str(),value.length(),true);
		bufLen+=strlen("Total Amount")+value.length()+SEPARATORS;

	//	Total Count
		value.clear();
		value=Utils::UtilsIntToString(debits_count,6);
		ecrData(buffer+bufLen,(unsigned char *)"Total Count",strlen("Total Count"),true,(unsigned char *)value.c_str(),value.length(),true);
		bufLen+=strlen("Total Count")+value.length()+SEPARATORS;

	//	Response Message
		memset(temp,0,sizeof(temp));
		if(isBatchProcessSucessfully==true)
			memcpy(temp,"GBOK",sizeof("GBOK"));		//GBOK=good batch ok
		else
			memcpy(temp,"Declined",sizeof("Declined"));

		ecrData(buffer+bufLen,(unsigned char *)"RespMsg",7,true,temp,strlen((char *)temp),true);
		bufLen+=7+strlen((char *)temp)+SEPARATORS;

		memcpy(buffer+bufLen,"\x03",1);		//ETX
		bufLen+=1;

		for(int i=0; i<bufLen; i++)		// lrc
			ucLrc^=buffer[i];

		memcpy(buffer+bufLen,&ucLrc,1);
		bufLen+=1;

//		char buf[16+1]={0};
//		cout<<"*********** SETTLEMENT ECR LOGS ************"<<endl;
//		for(int i=0; i<bufLen; i+=8)
//		{
//
//			sprintf(buf,"%02x%02x%02x%02x%02x%02x%02x%02x",buffer[i],buffer[i+1],buffer[i+2],buffer[i+3],buffer[i+4],buffer[i+5],buffer[i+6],buffer[i+7]);
//			cout<< buf<<endl;
//			memset(buf,0,sizeof(buf));
//		}
//		cout<<"***********END SETTLEMENT ECR LOGS ************"<<endl;

		listener.ecrWrite(buffer,bufLen);
	}

	int
	PosMessage::ecrData( unsigned char buff[],
			 const unsigned char * tag, int const tagLen, bool const seperator, const unsigned char * value
			, int const valueLen, bool const lineSeperator)
	{
		if (tagLen)
			memcpy(buff, tag, tagLen);

		if (seperator)
			memcpy(buff+tagLen, "=", 1);

		if (valueLen)
			memcpy(buff+tagLen+1, value, valueLen);

		if (lineSeperator)
			memcpy(buff+tagLen+1+valueLen, "\r\n", 2);

		return true;
	}

	 void PosMessage::versionDetails()
	 {
	 	char hardwareModelName[32]={0}, hardwareSerialNum[32]={0}, osVersion[32]={0};
	 	string emvKernelVer, sdkVer, adkVer, appVer;
	 	unsigned char buffer[1024], ucLrc=0;
		const int SEPARATORS=3;
		int bufLen=0;

		memcpy(buffer, "\x02",1);		//STX
		memcpy(buffer+1,"Details=",8);
		bufLen=9;

	//	Hardware Name
		sysGetPropertyString(SYS_PROP_HW_MODEL_NAME,hardwareModelName,sizeof(hardwareModelName));
		ecrData(buffer+bufLen,(unsigned char *)"Hardware Name",strlen("Hardware Name"),true,(unsigned char *)hardwareModelName,strlen(hardwareModelName),true);
		bufLen+=strlen("Hardware Name")+strlen(hardwareModelName)+SEPARATORS;

	//	Serial Number
		sysGetPropertyString(SYS_PROP_HW_SERIALNO,hardwareSerialNum,sizeof(hardwareSerialNum));
		ecrData(buffer+bufLen,(unsigned char *)"Serial Number",strlen("Serial Number"),true,(unsigned char *)hardwareSerialNum,strlen(hardwareSerialNum),true);
		bufLen+=strlen("Serial Number")+strlen(hardwareSerialNum)+SEPARATORS;

	//	VOS version
		sysGetPropertyString(SYS_PROP_OS_VERSION,osVersion,sizeof(osVersion));
		ecrData(buffer+bufLen,(unsigned char *)"VOS Version",strlen("VOS Version"),true,(unsigned char *)osVersion,strlen(osVersion),true);
		bufLen+=strlen("VOS Version")+strlen(osVersion)+SEPARATORS;

	//	EMV Kernel Version
	 	CTerminalConfig termcfg;
	 	emvKernelVer=termcfg.getEmvKernelVersion();
		ecrData(buffer+bufLen,(unsigned char *)"EMV Kernel Version",strlen("EMV Kernel Version"),true,(unsigned char *)emvKernelVer.c_str(),emvKernelVer.length(),true);
		bufLen+=strlen("EMV Kernel Version")+emvKernelVer.length()+SEPARATORS;

   //   SDK Version
		sdkVer=termcfg.getSdkVersion();
		ecrData(buffer+bufLen,(unsigned char *)"SDK Version",strlen("SDK Version"),true,(unsigned char *)sdkVer.c_str(),sdkVer.length(),true);
		bufLen+=strlen("SDK Version")+sdkVer.length()+SEPARATORS;

   //   ADK Version
		adkVer=termcfg.getAdkVersion();
		ecrData(buffer+bufLen,(unsigned char *)"ADK Version",strlen("ADK Version"),true,(unsigned char *)adkVer.c_str(),adkVer.length(),true);
		bufLen+=strlen("ADK Version")+adkVer.length()+SEPARATORS;

   //   Application Version
		appVer=termcfg.getAppVersion();
		ecrData(buffer+bufLen,(unsigned char *)"Application Version",strlen("Application Version"),true,(unsigned char *)appVer.c_str(),appVer.length(),true);
		bufLen+=strlen("Application Version")+appVer.length()+SEPARATORS;

		memcpy(buffer+bufLen,"\x03",1);		//ETX
		bufLen+=1;

		for(int i=0; i<bufLen; i++)		// lrc
			ucLrc^=buffer[i];

		memcpy(buffer+bufLen,&ucLrc,1);
		bufLen+=1;

		listener.ecrWrite(buffer,bufLen);
	 }



}

