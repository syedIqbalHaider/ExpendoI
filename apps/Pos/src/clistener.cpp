#include <fcntl.h>
#include <errno.h>
#include <svc.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include "clog.h"
#include "clistener.h"

static CLog log(POS_TASK);

using namespace com_verifone_listener;

namespace com_verifone_listener
{
	CListener::CListener():ecrHandle(-1),port("/dev/ttyAMA0"),connType(SERIAL) {}

	bool CListener::ecrListen(TYPE connType, std::string port) {
		this->port = port;
		this->connType = connType;

		switch (connType) {
			case SERIAL:
			{
				struct termios options;
				ecrHandle=open(port.c_str(),O_RDWR|O_NOCTTY|O_NDELAY);
				if(ecrHandle<0){
					log.message(MSG_INFO"Error opening serial port\n");
					return(false);
				}

				log.message(MSG_INFO "Opened port "+port+"\n");

				memset(&options,0,sizeof(options));
//				options.c_cflag=B19200|CS8|CLOCAL|CREAD;
				options.c_cflag=B115200|CS8|CLOCAL|CREAD;
				options.c_iflag=IGNPAR;
				tcsetattr(ecrHandle,TCSANOW,&options);
				return true;
			}
			case SOCKET:
			{
				 int newsockfd, portno;
				 socklen_t clilen;
				 struct sockaddr_in serv_addr, cli_addr;

				 ecrHandle = socket(AF_INET, SOCK_STREAM, 0);
				 if (ecrHandle < 0) {
					log.message(MSG_INFO"Error creating socket\n");
					return false;
				 }
				 bzero((char *) &serv_addr, sizeof(serv_addr));
				 portno = atoi(port.c_str());
				 serv_addr.sin_family = AF_INET;
				 serv_addr.sin_addr.s_addr = INADDR_ANY;
				 serv_addr.sin_port = htons(portno);
				 if (bind(ecrHandle, (struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) {
					 log.message(MSG_INFO"Error binding\n");
					 return false;
				 }
				 listen(ecrHandle,100);
				 clilen = sizeof(cli_addr);
				 newsockfd = accept(ecrHandle,(struct sockaddr *) &cli_addr,&clilen);
				 if (newsockfd < 0) {
					 log.message(MSG_INFO"Error accept\n");
					 return false;
				 }
				 return true;
			}
			default:
				return true;
		}
	}

	bool CListener::ecrRead(char *buf, int len) {
		switch (connType) {
			case SERIAL:
			{
//				if (read(ecrHandle,buf,len) != 1) {
				if (read(ecrHandle,buf,len) <= 0) {
					// waiting for data
					return false;
				}

				break;
			}
			case SOCKET:
			{
				if (TEMP_FAILURE_RETRY(recv(ecrHandle, buf, 1,0)) != 1) {
					// log.message(MSG_INFO"Read failed from tcp port\n");
					return false;
				}

				break;
			}
		}
		return true;
	}

	bool CListener::ecrWrite(unsigned char *buf, int len) {
		switch (connType) {
			case SERIAL:
			{
				if (write(ecrHandle,buf,len) != len) {
					log.message(MSG_INFO"Write failed to serial port\n");
					return false;
				}

				break;
			}
			case SOCKET:
			{
				if (send(ecrHandle,buf,len, 0) != len) {
					log.message(MSG_INFO"Write failed to tcp port\n");
					return false;
				}

				break;
			}
		}
		return true;
	}
}
