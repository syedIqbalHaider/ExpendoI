#include <fcntl.h>
#include <errno.h>
#include <svc.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sstream>
#include <algorithm>
#include <libda/cterminalconfig.h>
#include <libipc/ipc.h>
#include <libcpa.h>
#include <iomanip>
#include "clog.h"
#include "posmessage.h"
#include "bertlv.h"
#include "ecommand.h"
#include "cstatus.h"


extern "C" {
	#include <platforminfo_api.h>
}

#define SSTR( x ) dynamic_cast< std::ostringstream & >( std::ostringstream() << std::dec << x ).str()

#define ETX 0x03
#define STX 0x02
#define ACK 0x06
#define NAK 0x15
#define EOT 0x04
#define ESC 0x1B

static CLog log(POS_TASK);

using namespace za_co_verifone_bertlv;
using namespace com_verifone_status;

namespace com_verifone_ecommand
{
	ECommand::ECommand(){}

	bool ECommand::unpack(unsigned char *request,unsigned int length) {
		(void)request;			// Suppresses -> warning: unused parameter
		(void)length;			// Suppresses -> warning: unused parameter
		return true;
	}

	bool ECommand::pack() {
		log.message(MSG_INFO "Pack an E command response\r\n");
		responseLen = 3;
		response[0] = 0x45;
		response[1] = 0x00;
		response[2] = 0x00;
		encapsulate();

		return true;
	}

	bool ECommand::pack(char *respCode) {
		log.message(MSG_INFO "Pack an E command response\r\n");
		responseLen = 3;
		response[0] = 0x45;
		response[1] = respCode[0];
		response[2] = respCode[1];
		encapsulate();

		return true;
	}

	bool ECommand::encapsulate() {
		unsigned char tmpMessage[8192];
		unsigned char ucData;
		unsigned char ucLrc=0;
		unsigned int newLen;

		if(response == NULL) {
			log.message(MSG_INFO "response is empty\r\n");
			return false;
		}

		memcpy(tmpMessage,response,responseLen);
		response[0]=STX;
		newLen=1;
		for(int index=0;index<responseLen;index++) {
			ucData=tmpMessage[index];
			ucLrc^=ucData;
			if(ucData==STX || ucData==ETX || ucData==ESC) {
				response[newLen++]=ESC;
				ucData+=(unsigned char)0x20;
			}
			response[newLen++]=ucData;
		}
		response[newLen++]=ETX;
		if(ucLrc==STX || ucLrc==ETX || ucLrc==ESC) {
			response[newLen++]=ESC;
			ucLrc+=(unsigned char)0x20;
		}
		response[newLen++]=ucLrc;
		responseLen = newLen;

		return true;
	}

	bool ECommand::process(void) {
		// check if terminal is busy with banking
		CStatus statusClass;
		OPERATION_STATUS statusCode = statusClass.getOperation();

		switch (statusCode) {
			case OP_STAT_PARAM_DOWNLOAD:
			case OP_STAT_BANKING:
				busyCannotProcess = true;
				break;
			default:
				busyCannotProcess = false;
				break;
		}

		return true;
	}

	bool ECommand::unpackRespTLVs(std::map<std::string, std::string> &txresult) {
		(void)txresult;			// Suppresses -> warning: unused parameter
		return true;
	}

}
