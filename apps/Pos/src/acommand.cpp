#include <fcntl.h>
#include <errno.h>
#include <svc.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sstream>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <libda/cterminalconfig.h>
#include "libda/cbatchmanager.h"
#include "libda/ccommsconfig.h"
#include "libda/creceiptno.h"
#include "libda/cconmanager.h"
#include "libda/ctsn.h"
#include <libipc/ipc.h>
#include <libcpa.h>
#include <iomanip>
#include "clog.h"
#include "posmessage.h"
#include "bertlv.h"
#include "acommand.h"
#include "ecommand.h"
#include "cstatus.h"
#include "utils.h"
#include "gui/gui.h"

#include <svcmgr/svc_net.h>


extern "C" {
	#include <platforminfo_api.h>
	#include <libsecins.h>
}

#define SSTR( x ) dynamic_cast< std::ostringstream & >( std::ostringstream() << std::dec << x ).str()

#define ETX 0x03
#define STX 0x02
#define ACK 0x06
#define NAK 0x15
#define EOT 0x04
#define ESC 0x1B

#define REBOOTING_SCREEN 			"<br><center> <h5>Rebooting ......</h5> <center>"
#define BATCH_NUM_UPDATE_SCREEN 	"<br><center> <h5>Batch Number Updated</h5> <center>"
#define FAILED_SCREEN 				"<br><center> <h5>Failed to Update</h5> <center>"
#define PARA_UPDATE_SCREEN			"<br><center> <h5>Updating Parameters .....</h5> <center>"
#define SETTLE_FIRST_SCREEN			"<br><center> <h5>Please Settle First </h5> <center>"
#define BATCH_CLEAR_SCREEN			"<br><center> <h5>Batch Cleared Successfully </h5> <center>"

static CLog log(POS_TASK);

using namespace za_co_verifone_bertlv;
using namespace com_verifone_status;
using namespace com_verifone_ecommand;
using namespace com_verifone_terminalconfig;
using namespace com_verifone_batchmanager;
using namespace com_verifone_commsconfig;
using namespace com_verifone_conmanager;
using namespace com_verifone_receiptno;
using namespace com_verifone_tsn;
using namespace Utils;
using namespace vfigui;

namespace com_verifone_acommand
{
	ACommand::ACommand(){}

	bool ACommand::unpack(unsigned char *request,unsigned int length) {
		char value[20];
		int result;
		unsigned long rsize=0;

		log.message(MSG_INFO "Received an A command\r\n");
		log.message(MSG_INFO ""+hexToAscii(request,length)+"\r\n");

		reqType = GET_STATUS;

		if (length < 2) {
			log.message(MSG_INFO "Request A incomplete\r\n");
			return false;
		}

		result = platforminfo_get(PI_SERIAL_NUM ,value, sizeof(value), &rsize);
		if (result == PI_OK){
			std::string result;
			while((rsize >= 1) && (value[rsize-1] == ' ')) rsize--;		// Remove pesky trailing spaces
			result = std::string(value, rsize);
			std::remove_copy(result.begin(), result.end(), std::back_inserter(serialNo), '-');
			serialNo = serialNo.substr(1);
		} else
			serialNo = std::string("00000000");

		log.message(MSG_INFO "Serial number - "+serialNo+"\r\n");

		txType[2] = 0x00;
		memcpy(txType,request,2);
		switch ((INPUT_CODES)atoi(txType)) {
			case GET_SERIAL:
				log.message(MSG_INFO "Received a GET STATUS command\r\n");
				break;
			case SEND_CURRENT_BATCH:
				log.message(MSG_INFO "Received a Banking command\r\n");
				bankingInProgress = true;
				break;
			case GET_CURRENT_BATCH_REPORT:
				break;
			case GET_PREVIOUS_BATCH_REPORT:
				break;
			case PERFORM_PARAMETER_DOWNLOAD:
				log.message(MSG_INFO "Received a Parameters command\r\n");
				parametersInProgress = true;
				this->paramData=SSTR(request);
				break;
			case REPRINT_LAST_RECEIPT:
				break;
			case TRANSIT_BATCH_SETTLEMENT:
				break;
			case RESET_POS_POLLING:
				log.message(MSG_INFO "Received a Reset Pos Polling\r\n");
				doNotShowCardRemoval = false;
				checkCardRemovalFlag = true;
				break;
			case CANCEL_CARD_INSERT_SWIPE:
				log.message(MSG_INFO "Received a Cancel card Swipe command\r\n");
				break;
			case CLEAR_REVERSAL:
				log.message(MSG_INFO "Received a Clear Reversal command\r\n");
				break;
			case UPDATE_BATCH_NUMBER:
				log.message(MSG_INFO "Received a Batch Number Update command\r\n");
				this->paramData=SSTR(request);
				break;
			case CLEAR_BATCH:
				log.message(MSG_INFO "Received a Clear Batch command\r\n");
				break;
		}
		return true;
	}

	bool ACommand::pack() {
		log.message(MSG_INFO "Pack an A command response\r\n");
		std::string asciiPan;
		responseLen = 11;

		response[0] = 0x41;
		if (!busyCannotProcess) {
			response[1] = 0x30;
			response[2] = 0x30;
		} else {
			response[1] = 0x30;
			response[2] = 0x32;
		}
		memcpy(&response[3],serialNo.c_str(),8);
		encapsulate();
		return true;
	}

	bool ACommand::encapsulate() {
		unsigned char tmpMessage[8192];
		unsigned char ucData;
		unsigned char ucLrc=0;
		unsigned int newLen;

		if(responseLen == 0) {
			log.message(MSG_INFO "response is empty\r\n");
			return false;
		}

		memcpy(tmpMessage,response,responseLen);
		response[0]=STX;
		newLen=1;
		for(int index=0;index<responseLen;index++) {
			ucData=tmpMessage[index];
			ucLrc^=ucData;
			if(ucData==STX || ucData==ETX || ucData==ESC) {
				response[newLen++]=ESC;
				ucData+=(unsigned char)0x20;
			}
			response[newLen++]=ucData;
		}
		response[newLen++]=ETX;
		if(ucLrc==STX || ucLrc==ETX || ucLrc==ESC) {
			response[newLen++]=ESC;
			ucLrc+=(unsigned char)0x20;
		}
		response[newLen++]=ucLrc;
		responseLen = newLen;

		return true;
	}

	bool ACommand::process(void) {

		CStatus statusClass;
		std::map<std::string, std::string> txresult;
		int pmlret;

		// check if terminal is busy with banking
		OPERATION_STATUS statusCode = statusClass.getOperation();

		log.message(MSG_INFO "getOperation() returned "+Utils::UtilsIntToString(statusCode)+"\n");

		switch (statusCode) {
			case OP_STAT_PARAM_DOWNLOAD:
			case OP_STAT_BANKING:
				busyCannotProcess = true;
				break;
			default:
				busyCannotProcess = false;
				break;
		}

		if(busyCannotProcess == false) {

			switch ((INPUT_CODES)atoi(txType)) {
				case SEND_CURRENT_BATCH:
					log.message(MSG_INFO "Send banking Request\r\n");
					pmlret = CpaApp::DoPosBanking();
					if (pmlret != PMT_SUCCESS) {
						log.message(MSG_INFO "Banking failed\r\n");
						//iq_audi_040117
						this->ecrSettlementDataDisplay(false);
						return false;
					}
					break;
				case PERFORM_PARAMETER_DOWNLOAD:
					log.message(MSG_INFO "Send parameter Request\r\n");
//					pmlret = CpaApp::DoPosParameters();  //iq_audi_230217

					uiDisplay("<br><center> <h5>Please Settle First </h5> <center>");

					pmlret=updateParameters();

					if (pmlret != PMT_SUCCESS) {
						log.message(MSG_INFO "Parameters failed\r\n");
						listener.ecrWrite((unsigned char *)"\x02\x50\x61\x72\x61\x6D\x65\x74\x65\x72\x73\x20\x55\x70\x64\x61\x74\x65\x20\x46\x61\x69\x6C\x03\x28",25); //Parameters Update Fail
						return false;
					}

					break;

				case CLEAR_BATCH:
					clearBatch();
					break;

				case REPRINT_LAST_RECEIPT:
					break;

				case RESET_POS_POLLING:
					log.message(MSG_INFO "Send remove card\r\n");
					pmlret = CpaApp::DoPosCancelWaitingForCardInput();
					if (pmlret != PMT_SUCCESS) {
						log.message(MSG_INFO "Send remove card\r\n");
					}
					break;

				case CLEAR_REVERSAL:
					log.message(MSG_INFO "Send Clear Reversal\r\n");
					pmlret = CpaApp::DoPosClearReversal();
					if (pmlret != PMT_SUCCESS) {
						log.message(MSG_INFO "Clear Reversal Fail \r\n");
					}
					break;

				case UPDATE_BATCH_NUMBER:
					pmlret=updateBatchNo();

					if(!pmlret)
					{
						listener.ecrWrite((unsigned char *)"\x02\x42\x61\x74\x63\x68\x20\x6E\x75\x6D\x62\x65\x72\x20\x66\x61\x69\x6C\x65\x64\x20\x74\x6F\x20\x75\x70\x64\x61\x74\x65\x03\x57",32); //Batch number failed to update
						return false;
					}
					break;
				default:
					log.message(MSG_INFO "Received invalid txType "+Utils::UtilsIntToString(atoi(txType))+"\n");
					break;
			}
		}else{
			log.message(MSG_INFO"Busy, cannot process\n");
		}

		return true;
	}

	bool ACommand::unpackRespTLVs(std::map<std::string, std::string> &txresult) {
		(void)txresult;			// Suppresses -> warning: unused parameter
		return true;
	}

	void ACommand::clearDB(void)
	{
		FILE *fh=NULL;
		struct stat file_status;

		fh = fopen("flash/dbupdate1.dat", "r");  //if fail to open return NULL

		if(fh == NULL){
			// Update has not been performed, remove database
			log.message(MSG_INFO "checkDatabaseUpdate() - Removing out of date database...\n");

			remove(DB_PATH);

			fh = fopen("flash/dbupdate1.dat", "wb");
		}

		fclose(fh);

//	 Copy the file from RAM to flash
		if(stat(DB_PATH, &file_status) == 0)
		{
			std::ifstream source(DB_PATH_ORG, std::ios::binary);
			std::ofstream dest(DB_PATH, std::ios::binary);

			dest << source.rdbuf();

			dest.close();
			source.close();
		}
	}

	bool ACommand::updateBatchNo()
	{
		int ret=0;
		CBatchManager batchManager;
		unsigned char batchNum[6+1]={0};
		com_verifone_batch::CBatch cbatch;
		std::map<unsigned int, com_verifone_batchrec::CBatchRec> records;

		com_verifone_terminalconfig::CTerminalConfig termcfg;

		string tsn = termcfg.getReverselTsn();

		if(atoi(tsn.c_str())){

			uiDisplay(SETTLE_FIRST_SCREEN);
			sleep(5);

			log.message(MSG_INFO "Reversal Found.\r\n");
			return false;
		}

		ret = batchManager.getBatch(cbatch);

		if (ret != 1){
			log.message(MSG_INFO "Failure retrieving batch record\r\n");
			return false;
		}

		cbatch.getRecords(records);

		// Check that we have transactions to settle
		if(records.size() != 0){
			log.message(MSG_INFO "updateParameters(): Batch "+UtilsIntToString(cbatch.getBatchNo())+" is not empty\n");

			uiDisplay(SETTLE_FIRST_SCREEN);

			sleep(5);

			return false;
		}

		memcpy(batchNum, paramData.c_str()+2,6);

		if(batchManager.resetBatchNo(atoi((const char *)batchNum))==true)
		{
			uiDisplay(BATCH_NUM_UPDATE_SCREEN);
			return true;
		}

		uiDisplay(FAILED_SCREEN);
		return false;
	}

	bool ACommand::clearBatch(void)
	{
		// Close the batch
		com_verifone_batchmanager::CBatchManager batchManager;
		batchManager.closeBatch();

		uiDisplay(BATCH_CLEAR_SCREEN);

		return 0;
	}

	bool ACommand::updateParameters(void)
	{
		uint tagSeparator=0;
		uint valSeparator=0;
		uint valSeparatorLast=1;
		unsigned char tag[64];
		unsigned char value[128];
		unsigned char isStx[1+1]={0};
		CCommsConfig commscfg;
		CCommsSettings comSettingsPrim, comSettingsSec;
		com_verifone_terminalconfig::CTerminalConfig termcfg;
		CConManager conManager;

		string tsn = termcfg.getReverselTsn();

		if(atoi(tsn.c_str())){

			uiDisplay(SETTLE_FIRST_SCREEN);
			sleep(5);

			log.message(MSG_INFO "Reversal Found.\r\n");
			return -1;
		}

		com_verifone_batch::CBatch cbatch;
		com_verifone_batchmanager::CBatchManager batchManager;

		int	ret = batchManager.getBatch(cbatch);

		if (ret != 1){
			log.message(MSG_INFO "Failure retrieving batch record\r\n");
			return -1;
		}

		std::map<unsigned int, com_verifone_batchrec::CBatchRec> records;
		cbatch.getRecords(records);

		// Check that we have transactions to settle
		if(records.size() != 0){
			log.message(MSG_INFO "updateParameters(): Batch "+UtilsIntToString(cbatch.getBatchNo())+" is not empty\n");

			uiDisplay(SETTLE_FIRST_SCREEN);

			sleep(5);

			return -1;
		}

		uiDisplay(PARA_UPDATE_SCREEN);

		clearDB();

		while(isStx[0]!=0x03)
		{
			memset(tag,0,sizeof(tag));
			memset(value,0,sizeof(value));

			tagSeparator=paramData.find('=', valSeparator);
			valSeparator=paramData.find(0x1C, tagSeparator);

			memcpy(tag,paramData.substr(valSeparatorLast+1,tagSeparator).c_str(),tagSeparator-valSeparatorLast-1);
			memcpy(value,paramData.substr(tagSeparator+1,valSeparator).c_str(),valSeparator-tagSeparator-1);

//			cout << "iq: tagSeparator ["<<tagSeparator<<"]"<<endl;
//			cout << "iq: valSeparator ["<<valSeparator<<"]"<<endl;
//			cout << "iq: tag ["<<tag <<"]"<<endl;
//			cout << "iq: value ["<<value<<"]"<<endl;

			setParameters(tag, value,&comSettingsPrim,&comSettingsSec);

			valSeparatorLast=valSeparator;
			memcpy(isStx,paramData.substr(valSeparator+1,valSeparator+1).c_str(),1);
		}

		if(isDefaultCurrUSD)
		{
			conManager.updateLimits(SSTR("0"),SSTR("0"),saleTxAmtMax,saleMinAmt,SSTR("1"));  //index=1 for sale
			conManager.updateLimits(SSTR("0"),SSTR("0"),fallbackTxAmtMax,fallbackMinAmt,SSTR("0"));  //index=0 forfallback
			conManager.updateLimits(SSTR("0"),SSTR("0"),saleTxAmtMaxCurr2,saleMinAmtCurr2,SSTR("3"));  //index=3 Second Currency for sale
			conManager.updateLimits(SSTR("0"),SSTR("0"),fallbackTxAmtMaxCurr2,fallbackMinAmtCurr2,SSTR("4"));  //index=4 for Second Currency fallback
		}else
		{
			conManager.updateLimits(SSTR("0"),SSTR("0"),saleTxAmtMax,saleMinAmt,SSTR("3"));
			conManager.updateLimits(SSTR("0"),SSTR("0"),fallbackTxAmtMax,fallbackMinAmt,SSTR("4"));
			conManager.updateLimits(SSTR("0"),SSTR("0"),saleTxAmtMaxCurr2,saleMinAmtCurr2,SSTR("1"));
			conManager.updateLimits(SSTR("0"),SSTR("0"),fallbackTxAmtMaxCurr2,fallbackMinAmtCurr2,SSTR("0"));
		}

		if(commscfg.setPriSettleCommsSettings(comSettingsPrim)!=true)
			return 1;

		if(commscfg.setSecSettleCommsSettings(comSettingsSec)!=true)
			return 1;

		if(commscfg.setPriAuthCommsSettings(comSettingsPrim)!=true)
			return 1;

		if(commscfg.setSecAuthCommsSettings(comSettingsSec)!=true)
			return 1;

		if(comSettingsPrim.getCommsType()==CCommsSettings::GPRS ||
				comSettingsSec.getCommsType()==CCommsSettings::GPRS )
		{

			net_interfaceDown("ppp1",NET_IP_V4);

//			setting PPP configurations
			struct netIfconfig gprsConfig;
			struct netGprsInfo gprsSettings;

			log.message(MSG_INFO "user : "+ SSTR(username) +  "\r\n");
			log.message(MSG_INFO "password : "+ SSTR(password) +  "\r\n");
			log.message(MSG_INFO "apn : "+ SSTR(apn) +  "\r\n");

			gprsConfig=net_interfaceGet((char *)NET_PPP_GPRS_DEFAULT);

			memset(gprsConfig.auth,0,sizeof(gprsConfig.auth));
			memset(gprsConfig.user,0,sizeof(gprsConfig.user));
			memset(gprsConfig.password,0,sizeof(gprsConfig.password));

			memcpy(gprsConfig.interface,NET_PPP_GPRS_DEFAULT ,strlen(NET_PPP_GPRS_DEFAULT));
			memcpy(gprsConfig.pppPort,NET_GPRS_LAYER,strlen(NET_GPRS_LAYER));
			memcpy(gprsConfig.auth,"auto",strlen("auto"));
			strcpy(gprsConfig.user,username);
			strcpy(gprsConfig.password,password);

//			if(comSettingsPrim.getCommsType()==CCommsSettings::GPRS)
				gprsConfig.activate =1;
//			else
//				gprsConfig.activate =0;

			if(net_interfaceSet( gprsConfig ))
				log.message(MSG_INFO "GPRS SETUP FAILED  \r\n");
			else
				log.message(MSG_INFO "GPRS SETUP SUCCESSFUL  \r\n");

//			settings of GPRS APN
			gprsSettings= net_interfaceGprsGet();
			strcpy(gprsSettings.APN,apn);
			gprsSettings.reg_mode=NET_GPRS_REG_AUTO;

			 if(net_interfaceGprsSet(gprsSettings))
				log.message(MSG_INFO "GPRS APN SETUP FAILED  \r\n");
			else
				log.message(MSG_INFO "GPRS APN SETUP SUCCESSFUL  \r\n");
		}

			net_interfaceDown("eth0",NET_IP_V4);

//			setting ETH0 configurations
			struct netIfconfig ethConfig;

			log.message(MSG_INFO "Terminal Ip : "+ SSTR(terminal_ip) +  "\r\n");
			log.message(MSG_INFO "NetMask : "+ SSTR(netmask) +  "\r\n");
			log.message(MSG_INFO "Gw	 : "+ SSTR(defaultGw) +  "\r\n");
			log.message(MSG_INFO "DNS1	 : "+ SSTR(dns1) +  "\r\n");
			log.message(MSG_INFO "DNS2	 : "+ SSTR(dns2) +  "\r\n");

			ethConfig=net_interfaceGet((char *)NET_ETHERNET_DEFAULT);

			memset(ethConfig.local_ip,0,sizeof(ethConfig.local_ip));
			memset(ethConfig.netmask,0 ,sizeof(ethConfig.netmask));
			memset(ethConfig.gateway,0,sizeof(ethConfig.gateway));
			memset(ethConfig.dns1,0,sizeof(ethConfig.dns1));
			memset(ethConfig.dns2,0,sizeof(ethConfig.dns2));

			memcpy(ethConfig.interface,NET_ETHERNET_DEFAULT,strlen(NET_ETHERNET_DEFAULT));

			memcpy(ethConfig.local_ip,terminal_ip,strlen(terminal_ip));
			memcpy(ethConfig.netmask,netmask ,strlen(netmask));
			memcpy(ethConfig.gateway,defaultGw,strlen(defaultGw));
			memcpy(ethConfig.dns1,dns1,strlen(dns1));
			memcpy(ethConfig.dns2,dns2,strlen(dns2));
			ethConfig.usedhcp=isDhcp;

//			if(comSettingsPrim.getCommsType()==CCommsSettings::ETHERNET || comSettingsSec.getCommsType()==CCommsSettings::ETHERNET )
				ethConfig.activate =1;
//			else
//				ethConfig.activate =0;

			if(net_interfaceSet( ethConfig ))
				log.message(MSG_INFO "ETH SETUP FAILED  \r\n");
			else
				log.message(MSG_INFO "ETH SETUP SUCCESSFUL  \r\n");

		return 0;
	}

 bool ACommand::setParameters(unsigned const char tag[], unsigned const char value[], CCommsSettings *comSettingsPrim, CCommsSettings *comSettingsSec)
	{
		CTerminalConfig termcfg;
		CBatchManager batchManager;
		CReceiptNo recpt;
		CTsn tsn;

		if(strcasecmp("TID",(const char *)tag)==0)
			termcfg.setTerminalNo(SSTR(value));
		else if(strcasecmp("MID",(const char *)tag)==0)
			termcfg.setMerchantNo(SSTR(value));
		else if(strcasecmp("TPDU",(const char *)tag)==0)
			termcfg.setTpdu(SSTR(value));
		else if(strcasecmp("MSG_HEADER_HEX",(const char *)tag)==0)
			termcfg.setIsHexHeader(SSTR(value));
		else if(strcasecmp("F49_ENABLE",(const char *)tag)==0)
			termcfg.setF49Enable(SSTR(value));
		else if(strcasecmp("SSL_ENABLE",(const char *)tag)==0)
			termcfg.setSSLEnable(SSTR(value));
		else if(strcasecmp("TPK",(const char *)tag)==0)
			termcfg.setTpk(SSTR(value));
		else if(strcasecmp("MERCHANT_NAME",(const char *)tag)==0)
			termcfg.setMerchantName(SSTR(value));
		else if(strcasecmp("SEQUENCE_NO",(const char *)tag)==0)
			recpt.resetReceiptNo(atoi((const char *)value));
		else if(strcasecmp("BATCH_NO",(const char *)tag)==0)
			batchManager.resetBatchNo(atoi((const char *)value));
		else if(strcasecmp("INVOICE_NO",(const char *)tag)==0)
			tsn.resetTsn(atoi((const char *)value));
		else if(strcasecmp("RESP_WAIT_TIME",(const char *)tag)==0)
			termcfg.setResponseTimeout(atoi((const char *)value));
		else if(strcasecmp("FALLBACK_ENABLE",(const char *)tag)==0)
			termcfg.setIsfallbackAllowed(SSTR(value));
		else if(strcasecmp("DEFAULT_CURRENCY",(const char *)tag)==0)
		{
			if(strcasecmp((const char *)value,"LBP")==0)
			{
				termcfg.setCurrencyCode(SSTR("422"));
				isDefaultCurrUSD=false;
			}
			else
			{
				termcfg.setCurrencyCode(SSTR("840"));
				isDefaultCurrUSD=true;
			}
		}
		else if(strcasecmp("DEFAULT_CURR_SALE_MIN_AMT",(const char *)tag)==0)
			saleMinAmt=SSTR(value);
		else if(strcasecmp("DEFAULT_CURR_SALE_MAX_AMT",(const char *)tag)==0)
			saleTxAmtMax=SSTR(value);
		else if(strcasecmp("DEFAULT_CURR_FALLBACK_MIN_AMT",(const char *)tag)==0)
			fallbackMinAmt=SSTR(value);
		else if(strcasecmp("DEFAULT_CURR_FALLBACK_MAX_AMT",(const char *)tag)==0)
			fallbackTxAmtMax=SSTR(value);
		else if(strcasecmp("CURR2_SALE_MIN_AMT",(const char *)tag)==0)
			saleMinAmtCurr2=SSTR(value);
		else if(strcasecmp("CURR2_SALE_MAX_AMT",(const char *)tag)==0)
			saleTxAmtMaxCurr2=SSTR(value);
		else if(strcasecmp("CURR2_FALLBACK_MIN_AMT",(const char *)tag)==0)
			fallbackMinAmtCurr2=SSTR(value);
		else if(strcasecmp("CURR2_FALLBACK_MAX_AMT",(const char *)tag)==0)
			fallbackTxAmtMaxCurr2=SSTR(value);
		else if(strcasecmp("PRIMARY_COM_TYPE",(const char *)tag)==0)
		{
			if(strcasecmp("ETHERNET",(const char *)value)==0)
				comSettingsPrim->setCommsType(CCommsSettings::ETHERNET);
			else if(strcasecmp("GPRS",(const char *)value)==0)
				comSettingsPrim->setCommsType(CCommsSettings::GPRS);
			else if(strcasecmp("PSTN",(const char *)value)==0)
				comSettingsPrim->setCommsType(CCommsSettings::DIRECTDIAL);
			else
				comSettingsPrim->setCommsType(CCommsSettings::ETHERNET);
		}
		else if(strcasecmp("SECONDARY_COM_TYPE",(const char *)tag)==0)
		{
			if(strcasecmp("ETHERNET",(const char *)value)==0)
				comSettingsSec->setCommsType(CCommsSettings::ETHERNET);
			else if(strcasecmp("GPRS",(const char *)value)==0)
				comSettingsSec->setCommsType(CCommsSettings::GPRS);
			else if(strcasecmp("PSTN",(const char *)value)==0)
				comSettingsSec->setCommsType(CCommsSettings::DIRECTDIAL);
			else
				comSettingsSec->setCommsType(CCommsSettings::ETHERNET);
		}
		else if(strcasecmp("PRIMARY_COM_PRI_IP",(const char *)tag)==0)
			comSettingsPrim->setIp(SSTR(value));
		else if(strcasecmp("SECONDARY_COM_PRI_IP",(const char *)tag)==0)
			comSettingsSec->setIp(SSTR(value));
		else if(strcasecmp("PRIMARY_COM_PRI_PORT",(const char *)tag)==0)
			comSettingsPrim->setPort(SSTR(value));
		else if(strcasecmp("SECONDARY_COM_PRI_PORT",(const char *)tag)==0)
			comSettingsSec->setPort(SSTR(value));
		else if(strcasecmp("PRIMARY_COM_SEC_IP",(const char *)tag)==0)
			comSettingsPrim->setIp2(SSTR(value));
		else if(strcasecmp("SECONDARY_COM_SEC_IP",(const char *)tag)==0)
			comSettingsSec->setIp2(SSTR(value));
		else if(strcasecmp("PRIMARY_COM_SEC_PORT",(const char *)tag)==0)
			comSettingsPrim->setPort2(SSTR(value));
		else if(strcasecmp("SECONDARY_COM_SEC_PORT",(const char *)tag)==0)
			comSettingsSec->setPort2(SSTR(value));
		else if(strcasecmp("GPRS_APN",(const char *)tag)==0)
			memcpy(apn,value,strlen((const char *)value));
		else if(strcasecmp("GPRS_USER",(const char *)tag)==0)
			memcpy(username,value,strlen((const char *)value));
		else if(strcasecmp("GPRS_PASSWORD",(const char *)tag)==0)
			memcpy(password,value,strlen((const char *)value));
		else if(strcasecmp("ETH_TERMINAL_IP",(const char *)tag)==0)
			memcpy(terminal_ip,value,strlen((const char *)value));
		else if(strcasecmp("ETH_NETMASK",(const char *)tag)==0)
			memcpy(netmask,value,strlen((const char *)value));
		else if(strcasecmp("ETH_DEFAULT_GW",(const char *)tag)==0)
			memcpy(defaultGw,value,strlen((const char *)value));
		else if(strcasecmp("ETH_DNS1",(const char *)tag)==0)
			memcpy(dns1,value,strlen((const char *)value));
		else if(strcasecmp("ETH_DNS2",(const char *)tag)==0)
			memcpy(dns2,value,strlen((const char *)value));
		else if(strcasecmp("ETH_DHCP_ENABLE",(const char *)tag)==0)
			isDhcp=atoi((const char *)value);
		else
			log.message(MSG_INFO "Not Set Parameter Tag = ["+SSTR(tag) +"]\r\n");

		return true;
	}

}
