#include <cstring>
#include <cstdlib>
#include <libda/cconmanager.h>
#include <libda/cbatchmanager.h>
#include <libda/cbatch.h>
#include <stdexcept>
#include "tcommand.h"
#include "clog.h"
#include "utils.h"
#include "cstatus.h"

#define ETX 0x03
#define STX 0x02
#define ACK 0x06
#define NAK 0x15
#define EOT 0x04
#define ESC 0x1B

//using namespace std;
using namespace Utils;
using namespace com_verifone_status;

namespace com_verifone_tcommand{

static CLog log(POS_TASK);

TCommand::TCommand()
{
	batchno = 0;
	tx_count = 0;
	transferComplete = false;
	pack_counter = 0;
	tlist_type = TLIST_INVALID;
}

bool TCommand::unpack(unsigned char *request,unsigned int length)
{
	char value[20];
	int type;

	log.message(MSG_INFO "Received a T command\r\n");
	log.message(MSG_INFO ""+hexToAscii(request,length)+"\r\n");

	reqType = TRANSACTION_LIST;

	if (length < 2) {
		log.message(MSG_INFO "Request T incomplete\r\n");
		return false;
	}

	memset(value, 0, sizeof(value));
	memcpy(value, request, 2);
	type = atoi(value);

	switch (type)
	{
		case TLIST_CURRENT_BATCH:
			log.message(MSG_INFO "Received a TRANSACTION_LIST TLIST_CURRENT_BATCH command\r\n");
			tlist_type = TLIST_CURRENT_BATCH;
			break;
		case TLIST_PREVIOUS_BATCH:
			log.message(MSG_INFO "Received a TRANSACTION_LIST TLIST_PREVIOUS_BATCH command\r\n");
			tlist_type = TLIST_PREVIOUS_BATCH;
			break;
		default:
			tlist_type = TLIST_INVALID;
			return false;
	}

	return true;
}

bool TCommand::process(void)
{
	// check if terminal is busy with banking
	CStatus statusClass;
	OPERATION_STATUS statusCode = statusClass.getOperation();

	switch (statusCode) {
		case OP_STAT_PARAM_DOWNLOAD:
		case OP_STAT_BANKING:
			busyCannotProcess = true;
			break;
		default:
			busyCannotProcess = false;
			break;
	}

	if(busyCannotProcess)
		return true;

	com_verifone_batchmanager::CBatchManager batchManager;
	com_verifone_batch::CBatch cbatch;

	int ret;

	if(tlist_type == TLIST_CURRENT_BATCH)
		ret = batchManager.getBatch(cbatch);
	else
		ret = batchManager.getPreviousBatch(cbatch);

	if (ret != DB_OK) {
		log.message((MSG_INFO "Error getting batch, code=") + SSTR(ret) + "\n");

	} else {
		std::map<unsigned int, com_verifone_batchrec::CBatchRec> records;
		ret = cbatch.getRecords(records);

		batchno = cbatch.getBatchNo();

		// Check that we have transactions to settle
		if(records.size() == 0){
			log.message(MSG_INFO "Batch "+UtilsIntToString(batchno)+" is empty, nothing to do\n");
			return true;

		}else {
			batchno = cbatch.getBatchNo();

			std::map<unsigned int,CBatchRec>::iterator it;

			for (it=records.begin(); it!=records.end(); ++it) {
				CBatchRec batchRec = it->second;

				if(batchRec.isInProgress() || batchRec.isTxCanceled())
					continue;

				// Ignore transaction that were declined
				if(batchRec.isDeclined()) continue;

				TRecord trec(batchRec,batchno);
				respData.push_back(trec);
			}
		}
	}

	return true;
}

bool TCommand::pack(void)
{
	if(respData.size() == 0){
		// If there are no transactions
		string resp = "T"+zero_pad(batchno,5)+zero_pad(0, 4)+zero_pad(0, 4);
		memcpy(response, resp.c_str(), resp.length());
		responseLen = resp.length();
		encapsulate();
		transferComplete = true;
		return true;
	}

	try{
		TRecord trec = respData.at(pack_counter);
		BERTLV respTlv;

		respTlv.addTag("\xdf\x40", UtilsIntToString(trec.RcptNo()));
		respTlv.addTag("\xdf\x41", UtilsIntToString(trec.Tsn()));
		respTlv.addTag("\xdf\x44", trec.AuthCode());
		respTlv.addTag("\xdf\x45", trec.Pan());
		respTlv.addTag("\xdf\x46", trec.TimeStamp());
		respTlv.addTag("\xdf\x47", trec.Rrn());
		respTlv.addTag("\xdf\x48", trec.ISOCode());
		respTlv.addTag("\xdf\x49", trec.SaleAmount());
		respTlv.addTag("\xdf\x4a", trec.CashAmount());

		string resp = "T"+zero_pad(batchno,5)+zero_pad(pack_counter+1, 4)+zero_pad(respData.size(), 4);
		resp.append(respTlv.getStringData());

		memcpy(response, resp.c_str(), resp.length());
		responseLen = resp.length();

	}catch (const std::out_of_range& oor) {
		log.message(MSG_INFO "Out of Range error: "+string(oor.what())+"\n");
		return false;
	}

	pack_counter++;

	if(pack_counter >= respData.size())
		transferComplete = true;

	encapsulate();

	return true;
}


bool TCommand::encapsulate(void)
{
	unsigned char tmpMessage[8192];
	unsigned char ucData;
	unsigned char ucLrc=0;
	unsigned int newLen;

	if(responseLen == 0) {
		log.message(MSG_INFO "response is empty\r\n");
		return false;
	}

	memcpy(tmpMessage,response,responseLen);
	response[0]=STX;
	newLen=1;
	for(int index=0;index<responseLen;index++) {
		ucData=tmpMessage[index];
		ucLrc^=ucData;
		if(ucData==STX || ucData==ETX || ucData==ESC) {
			response[newLen++]=ESC;
			ucData+=(unsigned char)0x20;
		}
		response[newLen++]=ucData;
	}
	response[newLen++]=ETX;
	if(ucLrc==STX || ucLrc==ETX || ucLrc==ESC) {
		response[newLen++]=ESC;
		ucLrc+=(unsigned char)0x20;
	}
	response[newLen++]=ucLrc;
	responseLen = newLen;

	return true;
}

bool TCommand::unpackRespTLVs(std::map<std::string, std::string> &txresult)
{
	(void)txresult;			// Suppresses -> warning: unused parameter
	return false;
}

//
// Transaction Record class
//

TRecord::TRecord(com_verifone_batchrec::CBatchRec batchrec, int theBatchNo)
{
	if(batchrec.getAuthMode() == ONLINE){
		went_online = true;
		txdata[TXREC_ONLINE] = "1";
	}else
		txdata[TXREC_ONLINE] = "0";

	batchno = theBatchNo;
	txdata[TXREC_BATCHNO] = UtilsIntToString(batchno);

	tsn = batchrec.getBatchTxNo();
	txdata[TXREC_TSN] = UtilsIntToString(tsn);

	rcptno = UtilsStringToInt(batchrec.getReceiptNo());
	txdata[TXREC_RCPTNO] = batchrec.getReceiptNo();

	//iq_audi_301216
	ecrno = UtilsStringToInt(batchrec.getEcrNo());
	txdata[TXREC_ECR_NUMBER] = batchrec.getEcrNo();

	response_code = batchrec.getRespCode();
	if(response_code.length()) txdata[TXREC_RESPCODE] = response_code;

	auth_code = batchrec.getAuthCode();
	if(auth_code.length()) txdata[TXREC_AUTHCODE] = auth_code;

	tx_sale_amount = UtilsIntToString(batchrec.getTxAmount());
	txdata[TXREC_SALE_AMNT] = tx_sale_amount;

	tx_cash_amount = UtilsIntToString(batchrec.getCashAmount());
	if(tx_cash_amount.length() && (tx_cash_amount.compare("0") != 0)) txdata[TXREC_CASH_AMNT] = tx_cash_amount;

	pan = batchrec.getPan();
	pan.replace(6, (size_t)(pan.length()-10), (size_t)(pan.length()-10), '*');
	if(pan.length()) txdata[TXREC_PAN] = pan;

	start_timestamp = batchrec.getTxDateTime();
	txdata[TXREC_TIMESTAMP] = start_timestamp;

	iso_tx_code = batchrec.getTxCode();
	txdata[TXREC_ISO_CODE] = iso_tx_code;

	rrn = batchrec.getRrn();
	if(rrn.length()) txdata[TXREC_RRN] = rrn;

	if (atoi(response_code.c_str()) == 0)
		reason_code = "00";
	else
		reason_code = "30"; // todo

	txdata[TXREC_REASON_CODE] = reason_code;

	if (batchrec.getEmvTags().length() > 0) {
		emv_data = batchrec.getEmvTags();
		txdata[TXREC_EMV_DATA] = emv_data;
	}

	cardExpiryDate = batchrec.getExp();
	txdata[TXREC_CARD_EXPIRY] = reason_code;

	cardFees = batchrec.getCardFees();
	txdata[TXREC_CARD_FEES] = cardFees;

	transactionFees = batchrec.getLoadFees();
	txdata[TXREC_TRANSACTION_FEES] = transactionFees;
}

bool TRecord::WentOnline(void)
{
	return went_online;
}

int TRecord::BatchNo(void)
{
	return batchno;
}

int TRecord::Tsn(void)
{
	return tsn;
}

int TRecord::RcptNo(void)
{
	return rcptno;
}

string TRecord::ResponseCode(void)
{
	return response_code;
}

string TRecord::AuthCode(void)
{
	return auth_code;
}

string TRecord::SaleAmount(void)
{
	return tx_sale_amount;
}

string TRecord::CashAmount(void)
{
	return tx_cash_amount;
}

string TRecord::Pan(void)
{
	return pan;
}

string TRecord::TimeStamp(void)
{
	return start_timestamp;
}

string TRecord::ISOCode(void)
{
	return iso_tx_code;
}

string TRecord::Rrn(void)
{
	return rrn;
}

}
