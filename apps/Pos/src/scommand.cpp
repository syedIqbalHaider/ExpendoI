#include <fcntl.h>
#include <errno.h>
#include <svc.h>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <unistd.h>
#include <sstream>
#include <algorithm>
#include <map>
#include <libda/cterminalconfig.h>
#include <libda/cconmanager.h>
#include <libda/cbatchmanager.h>
#include <libda/cbatch.h>
#include <libipc/ipc.h>
#include <libcpa.h>
#include <iomanip>
#include "clog.h"
#include "posmessage.h"
#include "bertlv.h"
#include "scommand.h"
#include "cstatus.h"
#include "utils.h"

extern "C" {
	#include <platforminfo_api.h>
}

#define SSTR( x ) dynamic_cast< std::ostringstream & >( std::ostringstream() << std::dec << x ).str()

#define ETX 0x03
#define STX 0x02
#define ACK 0x06
#define NAK 0x15
#define EOT 0x04
#define ESC 0x1B

#define TX_ISO_SALE                0x00
#define TX_ISO_CASH_ADVANCE        0x01
#define TX_ISO_SALE_CASH           0x09
#define TX_ISO_REFUND              0x20
#define TX_ISO_DEPOSIT             0x21
#define TX_ISO_CORRECTION          0x22
#define TX_ISO_BAL_ENQUIRY         0x31
#define TX_ISO_DCC_LOOKUP		   0x32
#define TX_ISO_ACC_STAT		       0x38
#define TX_ISO_TRANSFER			   0x40
#define TX_ISO_PMNT_DEPOSIT        0x51
#define TX_ISO_PIN_CHANGE          0x92
#define TX_ISO_LOAD_PRODUCT        0x91

static CLog log(POS_TASK);

using namespace za_co_verifone_bertlv;
using namespace com_verifone_status;
//using namespace std;
using namespace Utils;

namespace com_verifone_scommand
{
	SCommand::SCommand()
	{
		credits_count = 0;
		credit_reversals = 0;
		credits_total_amount = 0;
		credits_reversals_total_amount = 0;

		debits_count = 0;
		debits_reversals_count = 0;
		debits_total_amount = 0;
		debits_reversals_total_amount = 0;

		authorisations = 0;
		authorisation_reversals = 0;
		authorisations_total_amount = 0;
		authorisation_reversals_total_amount = 0;
	}

	bool SCommand::unpack(unsigned char *request,unsigned int length) {
		(void)request;			// Suppresses -> warning: unused parameter
		(void)length;			// Suppresses -> warning: unused parameter
		log.message(MSG_INFO "Received a S command\r\n");
		reqType = SETTLEMENT_TOTALS;
		return true;
	}

	bool SCommand::process(void) {
		int ret;

		// check if terminal is busy with banking
		CStatus statusClass;
		OPERATION_STATUS statusCode = statusClass.getOperation();

		switch (statusCode) {
			case OP_STAT_PARAM_DOWNLOAD:
			case OP_STAT_BANKING:
				busyCannotProcess = true;
				break;
			default:
				busyCannotProcess = false;
				break;
		}

		if(busyCannotProcess)
			return true;

		com_verifone_batchmanager::CBatchManager batchManager;
		com_verifone_batch::CBatch cbatch;

		ret = batchManager.getPreviousBatch(cbatch);

		if (ret != DB_OK){
			log.message(MSG_INFO "Failure retrieving batch record\r\n");
			return false;
		}

		batch_no = cbatch.getBatchNo();

		std::map<unsigned int, com_verifone_batchrec::CBatchRec> records;
		cbatch.getRecords(records);

		std::map<unsigned int,CBatchRec>::iterator it;
		for (it=records.begin(); it!=records.end(); ++it) {
			CBatchRec batchRec = it->second;

			// Make sure that only completed transactions are processed
			if(batchRec.isInProgress() || batchRec.isTxCanceled()) continue;

			// Ignore transaction that were declined
			if(batchRec.isDeclined()) continue;

			string tx_code = batchRec.getTxCode();
			int itx_code = UtilsStringToInt(tx_code);

			if((itx_code == TX_ISO_CORRECTION) || (itx_code == TX_ISO_PIN_CHANGE) || (itx_code == TX_ISO_LOAD_PRODUCT) || (itx_code == TX_ISO_BAL_ENQUIRY) || (itx_code == TX_ISO_ACC_STAT))
				continue;

			if((itx_code >= 00) && (itx_code <= 19)){
				debits_count++;
				debits_total_amount = batchRec.getTxAmount()+batchRec.getCashAmount();
			}else if((itx_code >= 20) && (itx_code <= 29)){
				credits_count++;
				credits_total_amount = batchRec.getTxAmount()+batchRec.getCashAmount();
			}

			if(batchRec.isApproveOnline()){
				authorisations++;
				authorisations_total_amount = batchRec.getTxAmount()+batchRec.getCashAmount();;
			}

			superTotals[batchRec.getTotalsGroup()] += batchRec.getTxAmount()+batchRec.getCashAmount();
		}

		return true;
	}

	bool SCommand::pack() {
		string resp = "S"+zero_pad(batch_no,5);
		resp.append(zero_pad(debits_count,5));
		resp.append(zero_pad(debits_total_amount,12));
		resp.append(zero_pad(credits_count,5));
		resp.append(zero_pad(credits_total_amount,12));
		resp.append(zero_pad(authorisations,5));
		resp.append(zero_pad(authorisations_total_amount,12));

		BERTLV stTlv;
		for (std::map<string, long>::iterator it=superTotals.begin(); it!=superTotals.end(); ++it){
			BERTLV stgroupTlv;

			stgroupTlv.addTag("\xdf\xaf\x10", it->first);
			stgroupTlv.addTag("\xdf\xaf\x11", resp.append(zero_pad(it->second,12)));

			stTlv.addTag("\xdf\x4b", stgroupTlv.getStringData());
		}

		resp.append(stTlv.getStringData());

		memcpy(response, resp.c_str(), resp.length());
		responseLen = resp.length();

		encapsulate();

		return true;
	}

	bool SCommand::encapsulate() {
		unsigned char tmpMessage[8192];
		unsigned char ucData;
		unsigned char ucLrc=0;
		unsigned int newLen;

		if(responseLen == 0) {
			log.message(MSG_INFO "response is empty\r\n");
			return false;
		}

		memcpy(tmpMessage,response,responseLen);
		response[0]=STX;
		newLen=1;
		for(int index=0;index<responseLen;index++) {
			ucData=tmpMessage[index];
			ucLrc^=ucData;
			if(ucData==STX || ucData==ETX || ucData==ESC) {
				response[newLen++]=ESC;
				ucData+=(unsigned char)0x20;
			}
			response[newLen++]=ucData;
		}
		response[newLen++]=ETX;
		if(ucLrc==STX || ucLrc==ETX || ucLrc==ESC) {
			response[newLen++]=ESC;
			ucLrc+=(unsigned char)0x20;
		}
		response[newLen++]=ucLrc;
		responseLen = newLen;

		return true;
	}

	bool SCommand::unpackRespTLVs(std::map<std::string, std::string> &txresult) {
		(void)txresult;			// Suppresses -> warning: unused parameter
		return true;
	}

}
