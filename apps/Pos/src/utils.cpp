#include <cstring>
#include <sstream>
#include <iomanip>
#include <ctime>
#include <sys/types.h>
#include <sys/timeb.h>
#include <platforminfo_api.h>
#include "utils.h"

namespace Utils
{

int UtilStringToBCD(string str, char *bcd)
{
	int pos=0, count=0;

	for(string::iterator str_it=str.begin(); str_it!=str.end(); ++str_it){
		char c = *str_it;

		if(!((c >= '0') && (c <= '9')))
			return 0;

		if(count%2==0)
			bcd[pos] = ((char)(c - '0') << 4) & 0xf0;
		else{
			bcd[pos] |= (char)(c - '0');
			pos++;
		}

		count++;
	}

	// If string is of uneven length, then add 0x0f
	if(count%2){
		bcd[pos] |= (char)0x0f;
		pos++;
	}

	return pos;
}

string UtilsBcdToString(const char *data, unsigned int len)
{
	char c;
	stringstream ss;

	for (int i=0; i<len; i++) {
		if(((data[i] >> 4) & 0x0f) == 0x0f) break;

		c = ((data[i] >> 4) & 0x0f) + '0';
		if(c > '9') break;
		ss << c;

		if((data[i] & 0x0f) == 0x0f) break;

		c = (data[i] & 0x0f) + '0';
		if(c > '9') break;
		ss << c;
	}

	return ss.str();
}

int UtilsStringToInt(string str)
{
	int numb;
	istringstream ( str ) >> numb;
	return numb;
}

string UtilsIntToString(int int_val, int pad_count)
{
	stringstream ss;
	ss << std::setw(pad_count) << std::setfill('0') << int_val;
	return ss.str();
}

string UtilsLongToString(long long_val)
{
	stringstream ss;
	ss << long_val;
	return ss.str();
}

string UtilsHexToString(const char *data, unsigned int len)
{
	std::stringstream ss;

	ss << std::hex;
	for (size_t i = 0; len > i; ++i) {
			if(static_cast<unsigned int>(static_cast<unsigned char>(data[i])) <= 0x0f)
				ss << '0';
			ss << static_cast<unsigned int>(static_cast<unsigned char>(data[i]));
	}

	return ss.str();
}

string UtilsStringToHex(string src)
{
	char temp;
	string sRet;

	for (unsigned i = 0; i < src.length(); i++) {
		if((src.at(i) >= '0') && (src.at(i) <= '9'))
			temp = (src.at(i) - '0') << 4;
		else
			temp = (toupper(src.at(i)) - '7') << 4;

		i++;

		if((src.at(i) >= '0') && (src.at(i) <= '9'))
			temp |= src.at(i) - '0';
		else
			temp |= toupper(src.at(i)) - '7';

		sRet.append(1, temp);
	}

	return sRet;
}

string UtilsBoolToString(bool b)
{
	std::stringstream converter;
	converter << b;
	return converter.str();
}

string UtilsTimestamp()
{
	struct timeb tp;
	ftime(&tp);
	char buf[16];

	memset(buf, 0, sizeof(buf));
	strftime(buf,sizeof(buf),"%Y%m%d%H%M%S%u",localtime(&tp.time));

	return string(buf);
}

TERMINAL_TYPE UtilsGetTerminalType(void)
{
	char buffer[20];
	unsigned long rlen=0;

	memset(buffer, 0, sizeof(buffer));

	if(platforminfo_get(PI_MIB_MODEL_NUM ,buffer, (unsigned long)sizeof(buffer), &rlen) == PI_OK){
		if(memcmp(buffer, "UX300", 5) == 0)
			return TT_UX_300;
		else if(memcmp(buffer, "MX925", 5) == 0)
			return TT_MX_925;
	}

	return TT_UNKNOWN;
}


}
