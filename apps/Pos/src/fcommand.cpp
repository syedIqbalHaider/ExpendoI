#include <fcntl.h>
#include <errno.h>
#include <svc.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sstream>
#include <algorithm>
#include <libda/cterminalconfig.h>
#include <libipc/ipc.h>
#include <libcpa.h>
#include <iomanip>
#include "clog.h"
#include "posmessage.h"
#include "bertlv.h"
#include "fcommand.h"
#include "cstatus.h"


extern "C" {
	#include <platforminfo_api.h>
}

#define SSTR( x ) dynamic_cast< std::ostringstream & >( std::ostringstream() << std::dec << x ).str()

#define ETX 0x03
#define STX 0x02
#define ACK 0x06
#define NAK 0x15
#define EOT 0x04
#define ESC 0x1B

static CLog log(POS_TASK);

using namespace za_co_verifone_bertlv;
using namespace com_verifone_status;

namespace com_verifone_fcommand
{
	FCommand::FCommand(){}

	bool FCommand::unpack(unsigned char *request,unsigned int length) {
		log.message(MSG_INFO "Received a F command\r\n");

		if (length < 2) {
			log.message(MSG_INFO "Request F incomplete\r\n");
			return false;
		}

		reqType = REVERSAL;
		txType[2] = 0x00;
		memcpy(txType,request,2);

		return true;
	}

	bool FCommand::pack() {
		log.message(MSG_INFO "Pack a F command response\r\n");

		if (busyCannotProcess) {
			packBusyCannotProcess();
			return true;
		}

		responseLen = 3;

		response[0] = 0x46;
		response[1] = status[0];
		response[2] = status[1];

		encapsulate();
		return true;
	}

	bool FCommand::encapsulate() {
		unsigned char tmpMessage[8192];
		unsigned char ucData;
		unsigned char ucLrc=0;
		unsigned int newLen;

		if(response == NULL) {
			log.message(MSG_INFO "response is empty\r\n");
			return false;
		}

		if (busyCannotProcess)
			return true;

		memcpy(tmpMessage,response,responseLen);
		response[0]=STX;
		newLen=1;
		for(int index=0;index<responseLen;index++) {
			ucData=tmpMessage[index];
			ucLrc^=ucData;
			if(ucData==STX || ucData==ETX || ucData==ESC) {
				response[newLen++]=ESC;
				ucData+=(unsigned char)0x20;
			}
			response[newLen++]=ucData;
		}
		response[newLen++]=ETX;
		if(ucLrc==STX || ucLrc==ETX || ucLrc==ESC) {
			response[newLen++]=ESC;
			ucLrc+=(unsigned char)0x20;
		}
		response[newLen++]=ucLrc;
		responseLen = newLen;

		return true;
	}

	bool FCommand::process(void) {
		std::string ipcFrom;
		std::string req,resp;
		std::map<std::string, std::string> txresult;
		std::map<std::string,std::string>::iterator it;
		int pmlret;

		log.message(MSG_INFO "Process messages to CPA\r\n");

		// check if terminal is busy with banking
		CStatus statusClass;
		OPERATION_STATUS statusCode = statusClass.getOperation();

		switch (statusCode) {
			case OP_STAT_PARAM_DOWNLOAD:
			case OP_STAT_BANKING:
				busyCannotProcess = true;
				break;
			default:
				busyCannotProcess = false;
				break;
		}

		if (!busyCannotProcess) {
			pmlret = CpaApp::DoPosReversal();
			switch(pmlret) {
				case PMT_SUCCESS:
					log.message(MSG_INFO "Reversal returned Success\n");
					status = "00";
					break;
				case PMT_CANCELLED:
				case PMT_DECLINED:
				case PMT_PROCESSING_ERROR:
				case PMT_BUSY_CANNOT_PROCESS:
				default:
					log.message(MSG_INFO "Reversal returned Failed\n");
					status = "01";
					break;
			}
		}
		return true;
	}

	bool FCommand::unpackRespTLVs(std::map<std::string, std::string> &txresult) {
		(void)txresult;			// Suppresses -> warning: unused parameter
		return true;
	}
}
