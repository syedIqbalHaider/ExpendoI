#include <libtui/tui.h>
#include <libda/cterminalconfig.h>
#include <libda/cbatchmanager.h>
#include <libda/cbatch.h>
#include <libda/ccommssettings.h>
#include <libda/ccommsconfig.h>
#include <libda/ccardconfig.h>
#include <libda/chotcard.h>
#include <gui/gui.h>
#include <gui/jsobject.h>
#include "CLog.h"
#include "info.h"
#include "IdleAppver.h"
#include "utils.h"

extern "C" {
	#include <platforminfo_api.h>
}

#define CFG_TERM_SERIAL						"term_serial"
#define CFG_TERM_NAME						"term_name"
#define CFG_TERM_ID							"term_id"
#define CFG_MERCH_NAME						"merch_name"
#define CFG_MERCH_ID						"merch_id"
#define CFG_ETHER_MAC						"ether_mac"
#define CFG_ETHER_STATUS					"ether_status"
#define CFG_BATCH_NO						"batch_no"
#define CFG_AUTH_IP							"auth_ip"
#define CFG_AUTH_PORT						"auth_port"
#define CFG_PARAM_IP						"param_ip"
#define CFG_PARAM_PORT						"param_port"
#define CFG_SETTLE_IP						"settle_ip"
#define CFG_SETTLE_PORT						"settle_port"
#define CFG_SOFT_IP							"soft_ip"
#define CFG_SOFT_PORT						"soft_port"
#define CFG_AUTO_PARAM						"auto_param"
#define CFG_AUTO_SETTLE						"auto_settle"
#define CFG_HC_VERSION						"hc_version"
#define CFG_CARD_VERSION					"card_version"

static CLog log(IDLEAPP_NAME);

//using namespace std;
using namespace vfigui;
using namespace za_co_verifone_tui;
using namespace com_verifone_terminalconfig;
using namespace com_verifone_batchmanager;
using namespace com_verifone_commsconfig;
using namespace com_verifone_commssettings;
using namespace Utils;

int get_comms_config_values(map<string,string> &values);
int get_config_values(map<string,string> &values);
//void showAbout();
void settingsMenu (void);
int comms_info(void);

int InfoScreen(void)
{
	string info_tmpl;
	map<string,string> values;

	TuiTemplate("bgimage.tmpl", "InfoSelected", info_tmpl);

	get_config_values(values);

	while(1){
		int res = uiInvoke(values,info_tmpl);

		if (res == 11)
			settingsMenu();
		else if (res == 12)
			comms_info();
		else
			return 0;
	}

	return 0;
}

int comms_info(void)
{
	string info_tmpl;
	map<string,string> values;

	TuiTemplate("bgimage.tmpl", "CommsInfo", info_tmpl);

	get_comms_config_values(values);

	uiInvoke(values,info_tmpl);

	return 0;
}

int get_comms_config_values(map<string,string> &values)
{
	CCommsConfig setting;
	CTerminalConfig termcfg;
	CCommsSettings auth_config, param_config, settle_config, sofware_config;

	setting.getPriAuthCommsSettings(auth_config);
	setting.getPriSettleCommsSettings(param_config);
	setting.getPriParamCommsSettings(settle_config);
	setting.getPriSoftwareDwlndCommsSettings(sofware_config);

	string settle_time = termcfg.getSettlementTime();
	string param_time = termcfg.getParamDnldTime();

	values[CFG_AUTH_IP] = auth_config.getIp();
	values[CFG_AUTH_PORT] = auth_config.getPort();
	values[CFG_PARAM_IP] = param_config.getIp();
	values[CFG_PARAM_PORT] = param_config.getPort();
	values[CFG_SETTLE_IP] = settle_config.getIp();
	values[CFG_SETTLE_PORT] = settle_config.getPort();
	values[CFG_SOFT_IP] = sofware_config.getIp();
	values[CFG_SOFT_PORT] = sofware_config.getPort();
	values[CFG_AUTO_PARAM] = param_time;
	values[CFG_AUTO_SETTLE] = settle_time;

	return 0;
}

int get_config_values(map<string,string> &values)
{
	char value[255];
	int result;
	unsigned long rsize=0;
	CTerminalConfig termcfg;
	CBatchManager batchManager;
	CBatch batch;
	com_verifone_hotcard::CHotcard hotcard;
	com_verifone_cardconfig::CCardConfig cardcfg;

	result = platforminfo_get(PI_SERIAL_NUM ,value, sizeof(value), &rsize);
	if (result == PI_OK) values[CFG_TERM_SERIAL] = string(value,rsize);

	values[CFG_TERM_NAME] = termcfg.getTerminalName();
	values[CFG_TERM_ID] = termcfg.getTerminalNo();
	values[CFG_MERCH_NAME] = termcfg.getMerchantName();
	values[CFG_MERCH_ID] = termcfg.getMerchantNo();

	batchManager.getBatch(batch);
	values[CFG_BATCH_NO] = UtilsIntToString(batch.getBatchNo());

	string hc_version;
	hotcard.getVersion(hc_version);

	string card_version;
	cardcfg.getVersion(card_version);

	values[CFG_HC_VERSION] = hc_version;
	values[CFG_CARD_VERSION] = card_version;

	log.message(MSG_INFO"Terminal name:"+values[CFG_TERM_NAME]+"\n");
	log.message(MSG_INFO"Terminal ID  :"+values[CFG_TERM_ID]+"\n");
	log.message(MSG_INFO"Merchant name:"+values[CFG_MERCH_NAME]+"\n");
	log.message(MSG_INFO"Merchant ID  :"+values[CFG_MERCH_ID]+"\n");

	PI_ethernet_info_st eth_info;
	result = platforminfo_get(PI_ETHERNET_INFO , &eth_info, sizeof(PI_ethernet_info_st), &rsize);
	if (result == PI_OK){
		if(eth_info.exist){
			string mac = eth_info.mac;
			const char *mac_seperator=":";
			mac.insert(2, mac_seperator);
			mac.insert(5, mac_seperator);
			mac.insert(8, mac_seperator);
			mac.insert(11, mac_seperator);
			mac.insert(14, mac_seperator);

			values[CFG_ETHER_MAC] = mac;
			values[CFG_ETHER_STATUS] = eth_info.link_up ? "Up" : "Down";
		}
	}

	return 0;
}

