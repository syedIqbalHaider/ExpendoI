#include "gui/gui.h"
#include <string>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
//#include "gui/jsobject.h"
//#include "libtinyxml/tinyxml.h"
#include <libtui/tui.h>
#include <libminini/minIni.h>
//#include "sqlite3.h"
#include <liblog/dlogger.h>

//#include <libda/ctsn.h>
#include <libda/cconmanager.h>
#include <libda/cterminalconfig.h>
#include <libda/cbatchmanager.h>

//#include<sys/socket.h>
//#include<netinet/in.h>
//#include <netdb.h>
#include <string.h>
#include <fstream>
#include "sys/stat.h"
#include "xmlmenu.h"
#include "IdleAppver.h"

#include <sys/types.h>
#include <sys/timeb.h>
#include <libsecins.h>
#ifdef VFI_PLATFORM_VOS
#include <unistd.h>
#endif
#include "CLog.h"
#include "libhim.h"
#include "utils.h"

extern "C" {
#include <platforminfo_api.h>
}

using namespace vfigui;
using namespace com_verifone_conmanager;
using namespace za_co_verifone_tui;
using namespace za_co_verifone_xmlmenu;
using namespace za_co_vfi_Him;

#define ALIENO_INI "flash/alieno.ini"
//#define TICIBOX_INI "flash/ticibox.ini"

void setDefaultLayout();
void setMsgLayout(int left, /* left position in pixels (+=width if negative) */
int right, /* right position in pixels (+=width if negative) */
int bottom = -1 /* bottom position in pixels (+=height if negative) */
);

typedef std::map<std::string, std::string> stdStringMap;
CConManager *conMng = NULL;
std::string GLOBAL_STATUS_HTML = "";
std::string GLOBAL_LOADING_HTML = "";
static CLog log(IDLEAPP_NAME);
//const std::string GLOBAL_STATUS_HTML          =  "<div style='margin-top:90px;' align='center'"
//				"><div align='center' style='color:#17345E; font-size:19px; font-weight:bold;'>"
//				"<<msg>></div><div style='margin-top:10px'></div>"
//				//"<img border='0' src='../images/Kiosk buttons/ajax-loader.gif'>"
//				"</div>";
//const std::string GLOBAL_LOADING_HTML          = "<div style='margin-top:110px;' align='center'>"
//		"<img border='0' src='../images/Kiosk buttons/ajax-loader.gif'>"
//		"</div>";

/**
 * Substitute all occurences of fromStr with toStr in inStr, returns a reference to inStr
 * @param inStr
 * @param fromStr
 * @param toStr
 */
std::string& substAll(std::string& inStr, const std::string& fromStr,
		const std::string& toStr) {
	if (!fromStr.empty()) {
		size_t pos = 0;
		while ((pos = inStr.find(fromStr, pos)) != std::string::npos) {
			inStr.replace(pos, fromStr.length(), toStr);
			pos += toStr.length(); // prevent replace a with ba to go into loop
		}
	}
	return inStr;
}

/**
 * Returns the substring between the end of svBeg and the beginning of svEnd.
 * We search for svEnd only from after svBeg; Therefore we will find
 * a substring delimited by two identical chars (e.g. aaa"bc"ddd ).
 * If either of the demarkating strings is not found, we return "".
 * We return the first match found.
 * @param svIn   The string to be searched
 * @param svBeg  The start string - empty implies beginning
 * @param svEnd  The end string - empty implies end
 * @return The selected substring (see description)
 */
std::string subString(const char * svIn, const char *svBeg, const char *svEnd) {
	const char *pSt, *pEnd;

	std::string svRes = ""; //strcpy(svRes,"");

	if ((svBeg == NULL) || (svEnd == NULL))
		return svRes;

	if (*svBeg)
		pSt = strstr(svIn, svBeg);
	else
		pSt = svIn;

	if (pSt == NULL)
		return svRes;                        // Could not find start
	else {
		pSt += strlen(svBeg);
		if (!(*pSt))
			return svRes;         // at end of input
		else if (!(*svEnd)) {
			svRes = pSt;
			return svRes;  // will return all to end of string
		} else {
			pEnd = strstr(pSt, svEnd);
			if (pEnd == NULL)
				return svRes;           //Could not find end or empty substring
			else {
				svRes = std::string(pSt, pEnd - pSt);
				//svRes[pEnd-pSt] = 0;
			}
		}
	}
	return svRes;
}

/**
 * Substitute in html all occurences of the keys AS VARIABLES in mapReplaceValues with the corresponding value.
 * Note that this allows the key values to have html tags that are interpreted - not possible if we use directgui to replace.
 * Note also that this make no dif on time (seemingly)
 * @param html
 * @param mapReplaceValues
 * @param keys
 * @return
 */
std::string& substHtmlVars(std::string& html,
		std::map<std::string, std::string> &mapReplaceValues) {
	if (!html.empty()) {

		for (stdStringMap::iterator it = mapReplaceValues.begin();
				it != mapReplaceValues.end(); it++) {
			if (!it->first.empty()) {
				std::string sVar = "<?var " + it->first + "?>";
				//dlog_msg(" %s  => %s",sVar.c_str(),it->second.c_str());
				substAll(html, sVar, it->second);
				//dlog_msg("hlen=%d",html.length());
			}
		}
		//dlog_msg("htmls=%d",html.length());
	}
	//dlog_msg("htmls=%s",html.substr(0,500).c_str());
	return html;
}

//void showMessage(const std::string &html) {
//	//dlog_msg(("MSG:" + html).c_str(),0);
//    int request_id=uiConfirmAsync(0,"confirm",html);
//    if(request_id>=0) {
//    	while(uiConfirmWait(request_id,1000)==UI_ERR_WAIT_TIMEOUT) {
//    		//printf(".");
//    		//dlog_msg(".");
//    	}
//   }
//}

int showMsg(const char *templ, const std::string &html, int secs,
		int left = 150, int right = -150, int bot = -30) {
	int ret = 0;
	setMsgLayout(left, right, bot);
	//std::string templateString;
	//TuiTemplate(templ, templateString);

	//showMessage("<br><p align='center'><table style='width: 80%; height: 80%' border='1'' ><tr><td>" + html + "</td></tr></table></p>");
	//std::string msg ="<br><p align='center'><table style='width: 80%; height: 80%' border='1'' ><tr><td>" + html + "</td></tr></table></p>";
	std::string msg = "<div>" + html + "</div>";
	int request_id = uiConfirmAsync(UI_REGION_DEFAULT, templ, msg);
	if (request_id >= 0) {
		ret = uiConfirmWait(request_id, secs * 1000);
		if (ret == UI_ERR_WAIT_TIMEOUT) {
			uiDisplay(UI_REGION_DEFAULT, GLOBAL_STATUS_HTML); // invalidate region
			//uiConfirmWait(request_id,0); // pop queue
		}
	}

	setDefaultLayout();
	return ret;
}
int showMsg(const std::string &html) {
	return showMsg("confirm", html, 45);
}

int showYesNoMsg(const std::string &html) {
	return showMsg("yesno", html, 45);
}

void showStatus(const std::string &statMsg) {
	std::string msg = GLOBAL_STATUS_HTML;
	substAll(msg, "<<msg>>", statMsg);
	uiDisplay(UI_REGION_DEFAULT, msg);
}

std::string getHtmlTableHeader(std::string caption) {
	return (std::string(
			"\n"
					"    <table summary=\"Table Info.\"\n"
					"style='border:1px solid #000; border-collapse:collapse;font-size:70%;color:magenta'>\n"
					//"style='border:1px solid #000; border-collapse:collapse;font-family:arial,sans-serif; font-size:70%;'>\n"
					"      <caption>").append(caption).append("</caption>"
			"<thead></thead>\n"
			"<tfoot></tfoot>\n"
			"<tbody>\n"));
}

std::string getHtmlFooter() {
	return " </tbody>\n"
			"    </table>\n"
			"\n"
//        "</body>\n"
//        "</html>";
	;
}

static int addHtmlTableRow(void *vRecEnv, int nrColumns, char **argv,
		char **azColName) {
	stdStringMap &recEnv = *((stdStringMap *) vRecEnv);
	int i;
	std::string sTbl = recEnv["table"];
	//static std::vector<std::string> req_idx
	if (sTbl.empty()) {
		sTbl = getHtmlTableHeader(recEnv["TableName"]);
		sTbl.append("<tr>");
		for (i = 0; i < nrColumns; i++) {
			sTbl.append("<td><b>").append(azColName[i]).append("</b></td>");
		}
		sTbl.append("</tr>\n");
	}
	sTbl.append("<tr>");
	for (i = 0; i < nrColumns; i++) {
//		if (strcmp(azColName[i],"image")==0)
//			sTbl.append("<td>").append(argv[i] ? argv[i] : "NULL").append("</td>");
//		else
		sTbl.append("<td>").append(argv[i] ? argv[i] : "NULL").append("</td>");
	}
	sTbl.append("</tr>\n");
	recEnv["table"] = sTbl;
	//printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
	return 0;
}

std::string dbGetTableHtml(std::string sqlTable, int pos = 0, int nrRecs = 50,
		bool bTranspose = false) {
	int rc;
	stdStringMap recEnv;

	sqlite3 *db = conMng->getDbConnection();

	if (!db) {
		const char *DBPATH = "";    		//getDBPath().c_str();
		dlog_msg("Can't open database nm=%s, msg=: %s\n", DBPATH,
				sqlite3_errmsg(db));
		recEnv["table"] = uiPrint("Can't open database nm=%s, msg=: %s\n",
				DBPATH, sqlite3_errmsg(db));
	} else {
		char *errMsg = 0;
		std::string sql = "select * from " + sqlTable + " LIMIT "
				+ SSTR(nrRecs) + " OFFSET " + SSTR(pos); // only show first 50
		recEnv["TableName"] = sqlTable;
		if (bTranspose) {
			std::vector<std::vector<std::string> > records;
			conMng->getDBRecords(sql, records, nrRecs, false);
			std::string sTbl = getHtmlTableHeader(sqlTable);
			for (uint j = 0; j < records[0].size(); j++) {
				sTbl.append("<tr>");
				for (uint i = 0; i < records.size(); i++) {
					std::string s = records[i][j];
					if (i == 0)
						sTbl.append("<td><b>").append(s).append("</b></td>");
					else
						sTbl.append("<td>").append(s != "" ? s : "NULL").append(
								"</td>");
				}
				sTbl.append("</tr>\n");
			}
			recEnv["table"] = uiPrint(
					"<span style='color:green'>Qry=%S</span><br>\n%s",
					sql.c_str(), sTbl.c_str());
		} else {
			rc = sqlite3_exec(db, sql.c_str(), addHtmlTableRow, &recEnv,
					&errMsg);
			if (rc != SQLITE_OK) {
				dlog_msg("SQL error: %s\n", errMsg);
				//recEnv["table"] = uiPrint("%S",errMsg);
				recEnv["table"] = uiPrint(
						"<span style='color:red'>Qry=%S<br>\n%S</span><br>\n",
						sql.c_str(), errMsg);
				sqlite3_free(errMsg);
			} else {
				//dlog_msg("tbl=%s",recEnv["table"].c_str());
				recEnv["table"] = uiPrint(
						"<span style='color:green'>Qry=%S</span><br>\n%s",
						sql.c_str(), recEnv["table"].c_str());
				//dlog_msg("tbl=%s",recEnv["table"].c_str());
			}
		}

	}
	//dao.close(db);
	//db = 0;
	return recEnv["table"] + getHtmlFooter();
}

void dbShowTable(std::string command) {
//    std::string respHtml = std::string("<h4>Table <span style='color:blue'>");
//	respHtml += uiPrint("%S",command.c_str()) + "</span></h4>" +
//				   std::string("<h4>Response</h4><span style='color:#ff0000>");
//	respHtml += s=
	dbGetTableHtml(command) + "</span>";
//	showMsg(respHtml);
//    //showMessage(respHtml);
//
	std::string title =
			"Table " + uiPrint("%S", command.c_str())
					+ ".<button style='font-weight:normal; border-width:0px; background-color:#dd8215;"
							" padding:8px; font-size:14px; color:#fff' action='return 28'>PgUp</button>"
							".<button style='font-weight:normal; border-width:0px; background-color:#dd8215;"
							" padding:8px; font-size:14px; color:#fff' action='return 29'>PgDn</button>"
							".<button style='font-weight:normal; border-width:0px; background-color:#dd8215;"
							" padding:8px; font-size:14px; color:#fff' action='return 32'>TransPose</button>"
//			".<button style='font-weight:normal; border-width:0px; background-color:#dd8215;"
//			" padding:8px; font-size:14px; color:#fff' action='return 30'>Left</button>"
//			".<button style='font-weight:normal; border-width:0px; background-color:#dd8215;"
//			" padding:8px; font-size:14px; color:#fff' action='return 31'>Right</button>"
					;

	std::string html;
	TuiTemplate("bgimage.tmpl", "ScrollDetail", html);

	// Replace with contents
	std::map<std::string, std::string> msgValues;
	msgValues["detttl"] = title;
	substHtmlVars(html, msgValues);

	std::string htmlOrg = html;
	bool stayHere = true;
	bool readNext = true;
	bool bTranspose = false;
	int pos = 0;
	int nrRecs = 20;
	while (stayHere) {
		if (readNext) {
			uiDisplay(UI_REGION_DEFAULT, GLOBAL_LOADING_HTML);
			msgValues["detmsg"] = std::string("<span style='color:#ff0000>")
					+ dbGetTableHtml(command, pos, nrRecs, bTranspose)
					+ "</span>";
			html = htmlOrg;
			substHtmlVars(html, msgValues);
		}
		readNext = true;
		int rc = uiInvoke(UI_REGION_DEFAULT, msgValues, html);

		switch (rc) {
		case 28: {
			if (pos == 0)
				readNext = false;
			else {
				pos -= nrRecs;
				if (pos < 0)
					pos = 0;
			}
			break;
		}
		case 29:
			pos += nrRecs;
			break;
		case 32:
			bTranspose = !bTranspose;
			break;
			//case 30: pos +=nrRecs; break;
			//case 31: pos +=nrRecs; break;
		case 0:
			stayHere = false;
			break;
		default:
			readNext = false;
			break;
		}
	}
	setDefaultLayout();
}

int addDBMenuEntry(void *vRecEnv, int nrColumns, char **argv,
		char **azColName) {
	(void) argv;
	(void) azColName;
	(void) nrColumns;			// get rid of compiler warnings

	// $100 if you understand below right away :-) Just want to get back to the calling struct.
	struct UIMenuEntry (&menu)[] = *(struct UIMenuEntry (*)[]) vRecEnv;
	int i = ++(menu[0].value);
	// "select tbl_name from sqlite_master
	std::string sTbl = argv[0] ? argv[0] : "";
	std::string sNrRecs = "";
	if (!sTbl.empty()) {
		// sqlite3 *db = conMng->getDbConnection();

		sTbl += " ("
				+ conMng->getDBFirstField("SELECT count(*) as cnt from " + sTbl,
						"0") + " recs)";
	}

	menu[i].options = 0;
	menu[i].value = i;
	menu[i].text = std::string(SSTR(i)) + (i < 10 ? ".   " : ". ") + sTbl;
	return 0;
}

void showDBMenu() {
	std::string sMsg = "";
	//int rc = sqlite3_open(DBPATH, &db);
	sqlite3 *db = conMng->getDbConnection();

	if (!db) {
		sMsg = uiPrint("Can't open database msg=%s\n", sqlite3_errmsg(db));
		dlog_msg("%s",sMsg.c_str());
	} else {
		char *errMsg = 0;
		std::string sNrTables =
				conMng->getDBFirstField(
						"SELECT count(*) as cnt from sqlite_master where type = 'table'",
						"0");
		//dlog_msg("DB nr of tables = %s",sNrTables.c_str());
		uint nrTables = strtol(sNrTables.c_str(), NULL, 10);
		struct UIMenuEntry menu[nrTables + 1];
		menu[0].options = 0;
		menu[0].text = "0.   Back";			//sNrTables + " tables";
		menu[0].value = 0;
		std::string sql =
				"select tbl_name from sqlite_master where type = 'table' order by tbl_name asc";
		int rc = sqlite3_exec(db, sql.c_str(), addDBMenuEntry, &menu, &errMsg);
		if (rc != SQLITE_OK) {
			sMsg = uiPrint("SQL error: rc=%d,msg=%s<br>\nQry was: %s", rc,
					errMsg, sql.c_str());
			dlog_msg("%s", sMsg.c_str());
			//showMessage(sMsg);
			showMsg(sMsg);
			sqlite3_free(errMsg);
		} else {
			// Display the DB menu
			menu[0].value = 0;
			int s = 0;
			while (1) {
				s = uiMenu("menu",
						"<h4>Database Tables (" + sNrTables + ")</h4>", menu,
						sizeof(menu) / sizeof(menu[0]), s >= 0 ? s : 0);
				//printf("s=%d\n", s);
				if (s == UI_ERR_CONNECTION_LOST)
					break;
				if ((s < 0) || ((uint) s) > nrTables)
					break;

				std::string command = subString(menu[s].text.c_str(), " ",
						" (");
				if (command.empty())
					command = subString(menu[s].text.c_str(), " ", "");
				command.erase(0, command.find_first_not_of(" \n\r\t"));
				if (command.empty())
					command = menu[s].text;
				if (command.find("Back") != std::string::npos)
					break;
				else
					dbShowTable(command);
			}
		}

	}
	//dao.close(db);
	//db = 0;
}

//void setOwnSettingsLayout() {
//
//	const struct UIRegion reg_statusbar[]={	{2, 0, 0, gui_info.screen_width/2, STATUS_REG_HEIGHT},  	// button bar
// 	 	 	 								{1, (gui_info.screen_width/2)+1, 0, gui_info.screen_width, STATUS_REG_HEIGHT},  	// status bar
//	         	 	 	 	 	 	 	 	{UI_REGION_DEFAULT, 0, STATUS_REG_HEIGHT+1,gui_info.screen_width, gui_info.screen_height}}; // main screen
//
//
//	const struct UIRegion reg_no_statusbar[] = {
//			{ UI_REGION_DEFAULT, 0, 0, -1,-1 }  // main screen
//	};
//	uiLayout(reg_no_statusbar,
//			sizeof(reg_no_statusbar) / sizeof(reg_no_statusbar[0]));
//
//}

/**
 * Read file to fileData, starting at begPos for binary files.
 * @param fileName
 * @param fileData
 * @param begPos - position from beginning of file; if negative it is from the end of the file
 * @param readLen
 * @param isBinary
 * @return
 */
std::string & readToString(std::string fileName, std::string &fileData,
		long &begPos, long readLen = 0, bool isBinary = true) {
	std::ifstream inFs(fileName.c_str(),
			isBinary ? std::ios::binary | std::ios::in : std::ios::in);

	fileData = "";

	if (!inFs) {
		std::string sMsg = uiPrint("readToString: failed to open file: %s",
				fileName.c_str());
		dlog_msg("%s", sMsg.c_str());

	} else if (isBinary) {
		inFs.seekg(0, std::ios::end);
		long len = inFs.tellg();
		if (begPos < 0)
			begPos = len + begPos;
		if (begPos < 0)
			begPos = 0;

		if (readLen > 0) {
			if ((begPos + readLen) > len)
				len = len - begPos;
			else
				len = readLen;
			if (len <= 0)
				len = 0;
		}
		char *htmBytes = new (std::nothrow) char[len + 1];
		if (len > 0) {
			if (htmBytes == NULL) {
				dlog_msg("File to big to read into mem, size=%d, nm=%s", len,
						fileName.c_str());
				len = 0;
			} else {
				inFs.seekg(begPos, std::ios::beg);
				//uint res =
				inFs.read(htmBytes, len);
				//dlog_msg("DoAsync read=%d",res);
			}
		}
		htmBytes[len] = 0;
		inFs.close();
		fileData = htmBytes;
		delete[] htmBytes;
	} else {
		std::string buf = "";
		while (inFs.good()) {
			getline(inFs, buf, '$');
			if ((readLen <= (long) 0)
					|| (((long) fileData.length() + (long) buf.length())
							<= readLen))
				fileData += buf;
			else if ((((long) fileData.length() + (long) buf.length()) > readLen)) {
				fileData += buf.substr(0, readLen - fileData.length());
				break;
			}
		}
		inFs.close();
	}
	return fileData;
}

/**
 * Populate memVals with the current memory sizes and the difference from the previous (or original)
 * memory size. Specifically: memVals[x] will have value for x and memVals[xDif] will have difference
 * from previous value stored.
 * @param memVals - contains values
 * @param update - if true memVals[x] will contain latest value; if false it will not be updated (but the dif will be)
 * @param logKey - specify what should be logged - all if "*", none if empty, only value for x if x;
 * 				log only values where dif>0 or at least 1 entry.
 */
bool getMemUsage(std::map<std::string, int> &memVals, bool update = true,
		std::string logKey = "*") {
	// Log Memory Usage
	//  * VmPeak: Peak virtual memory size.
	//  * VmSize: Virtual memory size.
	//  * VmLck: Locked memory size (see mlock(3)).
	//  * VmHWM: Peak resident set size ("high water mark").
	//  * VmRSS: Resident set size.
	//  * VmData, VmStk, VmExe: Size of data, stack, and text segments.
	//  * VmLib: Shared library code size.
	//  * VmPTE: Page table entries size (since Linux 2.6.10).
	//  * Threads: Number of threads in process containing this thread.	std::string sStat = "";
	//	VmPeak:	    3876 kB
	//	VmSize:	    3876 kB
	//	VmLck:	       0 kB
	//	VmPin:	       0 kB
	//	VmHWM:	     272 kB
	//	VmRSS:	     272 kB
	//	VmData:	     156 kB
	//	VmStk:	     136 kB
	//	VmExe:	      44 kB
	//	VmLib:	    1828 kB
	//	VmPTE:	      16 kB
	//	VmSwap:	       0 kB
	//	Threads:	1

	std::string sStat;
	std::string sKeys[11] = { "VmPeak", "VmSize", "VmLck", "VmHWM", "VmData",
			"VmStk", "VmExe", "VmLib", "VmPTE", "VmRSS", "MemFree" };
	int sz = 11;
	bool found = false;
	long begPos = 0;

	//readToString("/proc/self/status",sStat,begPos,0,false); // Current app memory
	readToString("/proc/meminfo", sStat, begPos, 0, false);  // Overall free mem
	for (int i = 0; i < sz; i++) {
		std::string s = sKeys[i];
		std::string sVal = subString(sStat.c_str(), s.c_str(), " kB");
		if (!sVal.empty()) {
			int vLast = 0;
			int dLast = 0;
			if (memVals.find(s) != memVals.end()) {
				vLast = memVals[s];
				dLast = memVals[s + "Dif"];
			}
			int vNow = strtol(sVal.c_str() + 1, NULL, 10);
			if (update) {
				memVals[s] = vNow;
			}
			memVals[s + "Dif"] = vNow - vLast;

			if (!logKey.empty() && (logKey == "*" || logKey == s)) {
				int mDif = 0;
				if (memVals.find("mindif") != memVals.end())
					mDif = memVals["mindif"];
				int dif = memVals[s + "Dif"];
				int nDif = dif - dLast;
				int nw = memVals[s];
				if (!update)
					nw += dif;
				// Execute the if below anyway if you want to log all the values anyway
				if ((nDif > 0)) { // || (!found && (i+1==sz))) {
					found = true;
					dlog_msg(" - after %d min: %s=%d,org%s%d,delta=%d", mDif,
							s.c_str(), nw, (dif>=0?"+":"-"), dif, nDif);
				}
			}
		}
	}
	return found;
}

int showDetail(const std::string &title, const std::string &text,
		bool mustRestoreLayout = true) {
	std::map<std::string, std::string> msgValues;

	msgValues["detttl"] = title;

	// We allow html commands in the title part
	std::string html = "";
	//readToString(resPath + htmlDetailInfo,html);
	//html = getBodyPart(html);

	//cout << "DBG " << "DetailTUI: " <<
	TuiTemplate("bgimage.tmpl", "ScrollDetail", html);	// << endl;

	substHtmlVars(html, msgValues);

	// Ignore html commands in message part
	msgValues["detmsg"] = text;

	//setMsgLayout(100,-100);

	int rc = uiInvoke(UI_REGION_DEFAULT, msgValues, html);
//    int request_id =  ownInvokeAsync(UI_REGION_DEFAULT, msgValues, html);
//    int rc = UI_ERR_TIMEOUT;
//    while (rc == UI_ERR_TIMEOUT) {
//    	rc = ownInvokeWait(request_id, msgValues, 120*1000); // timout after 120 secs
//    	//dlog_msg("req_id=%d,rc=%d",request_id,rc);
//    }

	if (mustRestoreLayout)
		setDefaultLayout();

	return rc;
}

void viewLogFile(std::string filePath) {
	// We only view last 3000 lines
	//char *htmBytes = new char[3000+1];
	std::string logData = "";
	int nrChars = 2400;
	long pos = -nrChars;

	std::string title =
			"<button style='font-weight:normal; border-width:0px; background-color:#dd8215;"
					" padding:8px; font-size:14px; color:#fff' action='return 26'>Home</button>"
					".<button style='font-weight:normal; border-width:0px; background-color:#dd8215;"
					" padding:8px; font-size:14px; color:#fff' action='return 27'>End</button>"
					".<button style='font-weight:normal; border-width:0px; background-color:#dd8215;"
					" padding:8px; font-size:14px; color:#fff' action='return 28'>PgUp</button>"
					".<button style='font-weight:normal; border-width:0px; background-color:#dd8215;"
					" padding:8px; font-size:14px; color:#fff' action='return 29'>PgDn</button>";

	bool stayHere = true;
	bool readFile = true;
	while (stayHere) {
		if (readFile)
			readToString(filePath, logData, pos, nrChars);
		//readToString(apLogicalName + SSTR(".log"),logData,pos,nrChars);
		readFile = true;
		//long pEnd = pos+nrChars;
		//while(pos > 1 && logData.at[pos-1] != '\n') pos--;
		//while(pEnd > 1 && logData.at[pEnd-1] != '\n') pEnd--;
		std::string ttl = "Log File " + filePath + " (" + (pos < 0 ? "E" : "")
				+ SSTR(pos) + " to " + (pos + nrChars < 0 ? "E" : "")
				+ SSTR(pos + nrChars) + ")" + title;
		int rc = showDetail(ttl, logData, false);	//uiPrint("%C",htmBytes));
		switch (rc) {
		case 26:
			pos = 0;
			break;
		case 27:
			pos = -nrChars;
			break;
		case 28: {
			if (pos == 0)
				readFile = false;
			else {
				pos -= nrChars;
				if (pos < 0)
					pos = 0;
			}
			break;
		}
		case 29:
			pos += nrChars;
			break;
		case 0:
			stayHere = false;
			break;
		default:
			readFile = false;
			break;
		}
	}
	setDefaultLayout();
}

void SetSoftwareIp() {
	char ipAdr[255];
	char port[255];
	int ip_len = 0;
	int port_len = 0;

	// Read the IP address
	memset(ipAdr, 0, sizeof(ipAdr));
	memset(port, 0, sizeof(port));
	ip_len = ini_gets("Alieno", "IP", " ", ipAdr, sizeof(ipAdr), ALIENO_INI);
	port_len = ini_gets("Alieno", "PORT", " ", port, sizeof(port), ALIENO_INI);
	if (port_len == 0) {
		strncpy(port, "21", 3);
	}

	if (ip_len == 0) {
		dlog_msg("Unable to retrieve IP address from ini\n");
	} else {
		std::string ipOnly = subString(ipAdr, "", "/");
		std::string ipApp = "";
		dlog_msg("iponly=%s", ipOnly.c_str());
		if (ipOnly.empty()) {
			ipOnly = ipAdr;
		} else {
			ipApp = subString(ipAdr, "/", "");
			if (!ipApp.empty())
				ipApp = "/" + ipApp;
		}

		std::vector<std::string> ipVal(2);
		ipVal[0] = ipOnly;
		ipVal[1] = port;
		std::string htmlStr =
				"<h4>Software Updates</h4><br>"
						"Please enter the ip address and port number for software downloads "
						"using the keypad. Select 1 twice to get a dot.<br><br>\n"
						"IP Address: <input size='15' type='text' maxlength='15' allowed_chars='0123456789.'><br>\n"
						"Port:  <input size='4' type='text' maxlength='4' allowed_chars='0123456789'>";
		int r = uiInput("input", ipVal, htmlStr);
		if (r == UI_ERR_OK) {
			//softIp = ipVal[0];
			//val_len = ini_gets("Alieno", "IP"," ",value,sizeof(value),ALIENO_INI);
			std::string ipFull = ipVal[0] + ipApp;
			ini_puts("Alieno", "IP", ipFull.c_str(), ALIENO_INI);
			ini_puts("Alieno", "PORT", ipVal[1].c_str(), ALIENO_INI);
		}
	}
}

void pipeExec(const char* syscmd) {

//	setTiciLayout();
	dlog_msg("Executing: %s", syscmd);
	FILE* fp = popen(syscmd, "r");
	std::string sMsg = "Check for updates, may take a while ...\n";	// + std::string(syscmd);
	showStatus(sMsg);
	if (fp) {
		char buf[128];
		std::string sLast = "";
		while (!feof(fp)) {
			if (fgets(buf, 128, fp) != NULL) {
				std::string s = buf;
				showStatus(s);
				if ((s != sLast) && (s != std::string("\n")) && !s.empty()) {
					if (s.find("%") == std::string::npos) {
						sMsg += sLast + s;
						sLast = "";
					} else
						sLast = s;
				} else
					sLast = s;
				//dlog_msg("read %d",sMsg.length());
			}
		}
		pclose(fp);
	} else {
		sMsg = "Could not execute " + std::string(syscmd);
	}
	dlog_msg("Update check returned without rebooting, msg=\n%s", sMsg.c_str());

	showDetail("Software Update Result",/*"Note: You may need to reboot your device.\n" +*/
			sMsg);
}

void doSoftwareUpdate() {

	int r =
			showYesNoMsg(
					"<div style='color:#000; font-size:19px; font-weight:bold;'>"
							"Check for Software Updates?"
							"</div>"
							"<div style='font-size:16px; font-weight:normal; margin-top:25px;'>"
							"You have opted to check for software updates. <br>"
//						 "The terminal will be rebooted afterwards even if no updates were found."
							"<div style='margin-top:10px;'></div>"
							"Do you want to continue and check for updates?</div>");
	if (r == 1) {
		pipeExec("./AlienoTMS");			// 2>&1");
		//showStatus("Rebooting ...");
		//Secins_reboot();
	}

}

std::string get_serial(void) {
//#ifdef _VOS_
	char value[20];
	int result;
	unsigned long rsize = 0;

	result = platforminfo_get(PI_SERIAL_NUM, value, sizeof(value), &rsize);
	if (result == PI_OK) {
		while ((rsize >= 1) && (value[rsize - 1] == ' '))
			rsize--;		// Remove pesky trailing spaces
		return std::string(value, rsize);
	} else
		return std::string("NA");
//#else
//	return string("123-456-789");
//#endif // _VOS_
}

void showAbout() {
	std::string sDetail = "";

	// Current time
	{
		struct timeb tp;
		ftime(&tp);
		char buf[64];
		strftime(buf, sizeof(buf), "%d.%m.%Y - %H:%M:%S", localtime(&tp.time));
		sDetail += "Date & time = " + SSTR(buf) + "\n\n";
	}
	sDetail += std::string("Terminal Serial = ") + get_serial() + "\n";

	//sDetail += std::string("Database ID = ") + vaneApi->getVaneKioskId() + "\n";
	//sDetail += "Ticket Printer IP & Port = " + vaneApi->getTicketPrinterIP() + ":" + vaneApi->getTicketPrinterPort()  + "\n";
	//sDetail += "Receipt Printer IP & Port = " + vaneApi->getReceiptPrinterIP() + ":" + vaneApi->getReceiptPrinterPort()  + "\n";

	// Get software download address & port
	{
		char ipAdr[255];
		char port[255];
		int ip_len = 0;
		int port_len = 0;

		// Read the IP address
		memset(ipAdr, 0, sizeof(ipAdr));
		memset(port, 0, sizeof(port));
		ip_len = ini_gets("Alieno", "IP", " ", ipAdr, sizeof(ipAdr),
				ALIENO_INI);
		port_len = ini_gets("Alieno", "PORT", " ", port, sizeof(port),
				ALIENO_INI);
		if (ip_len > 0 && port_len > 0)
			sDetail += "Software Download IP & Port = " + SSTR(ipAdr) + ":"
					+ SSTR(port) + "\n";
	}

	//Get executable's date
	{
		char sName[20];
		struct stat st;
		//ode_t mode = S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH;
		if (stat("./CPA", &st) == 0) { // use apLogicalName?
			struct tm lt;
			time_t t = (time_t) (st.st_mtim.tv_sec); //(time_t)(st.st_mtime);
			localtime_r(&t, &lt);
			strftime(sName, 20, "%F %T", &lt);
			sName[19] = 0;
			sDetail += "Application timestamp (CPA) = " + SSTR(sName);
		}
	}

	// Get Software Download Version
	{
		FILE* fp = popen("./AlienoTMS -v", "r");
		std::string sVers = "";
		if (fp) {
			char buf[128];
			while (!feof(fp)) {
				if (fgets(buf, 128, fp) != NULL) {
					showStatus(std::string(buf));
					sVers += buf;
				}
			}
			pclose(fp);
		}
		if (sVers.empty())
			sVers = "unknown";
		sDetail += "\nAlienoTMS Version: " + sVers;
	}

	// Os version
	{
		std::string sVers = "";
		long pos = 0;
		readToString("/proc/version", sVers, pos, 0, false);
		//dlog_msg("vers=%s",sVers.c_str());
		if (!sVers.empty())
			if (sVers.find("(") != std::string::npos)
				sVers = subString(sVers.c_str(), "", "(");
		sDetail += "\n\nOS Version:\n" + sVers;
	}

//	// Memory Usage for Program
//	{
//		std::string sStat = "";
//		readToString("/proc/self/status",sStat,0,0,false);
//		//dlog_msg("%d,status=%s",sStat.length(),sStat.c_str());
//		sStat = subString(sStat.c_str(),"VmPeak:","SigQ:");
//		if (!sStat.empty())
//			sDetail += "\n\nTicibox Memory Usage:\nVmPeak" + sStat;
//	}

	// Memory Usage Overall
	{
		std::string sStat = "";
		long pos = 0;

		readToString("/proc/meminfo", sStat, pos, 0, false);
		//dlog_msg("%d,status=%s",sStat.length(),sStat.c_str());
		if (!sStat.empty())
			sDetail += "\n\nTerminal Memory Usage:\n" + sStat;
	}

	showDetail("About Expendo" /*+ std::string(TICIVERSION)*/, sDetail);

}

int superMenuAction(int selValue, std::string menuId) {
	(void) menuId;

	char *cmdArg[3];
	log.message(MSG_INFO"menuAction():" + menuId + ": selValue ->"+Utils::UtilsIntToString(selValue)+"\n");
	int retVal = UI_ERR_BACK;

	switch (selValue) {
	case 300:
		doSoftwareUpdate();

//		iq_audi_03082017
//#ifdef VFI_PLATFORM_VOS
//		cmdArg[0]="scapp.out";
//		cmdArg[1]="keyloading";
//		cmdArg[2]=NULL;
//
//		if(vfork()==0)
//		{
//			execvp(cmdArg[0],cmdArg);
//		}
//#endif
		break;
	case 301:   // Force Reinstall
	{
		com_verifone_terminalconfig::CTerminalConfig termcfg;
		termcfg.setSetupComplete(false);
		int option = 0;

// TODO: If batch exists, do settlement first
		// Reset batch number to 1
		com_verifone_batchmanager::CBatchManager batchManager;

		if( batchManager.getNoRecordsInBatch() != 0){
			option = showYesNoMsg(
								"<div style='color:#000; font-size:19px; font-weight:bold;'>"
										"Force Re-install?"
										"</div>"
										"<div style='font-size:16px; font-weight:normal; margin-top:25px;'>"
										"There are un-banked transactions on record. <br>"
										"<div style='margin-top:10px;'></div>"
										"Do you want to settle them first?</div>");

			if( option == 1 ){
				Him him;
				if( him.DoBanking() != 0 ){
					option = showYesNoMsg(
								"<div style='color:#000; font-size:19px; font-weight:bold;'>"
										"Force Re-install?"
										"</div>"
										"<div style='font-size:18px; font-weight:normal; margin-top:28px;'>"
										"Banking Failed. <br>"
										"<div style='margin-top:10px;'></div>"
										"Do you want to re-install anyway?</div>");
				}
				if( option != 1 )
					break;
			}

		}
		batchManager.closeBatch();
		batchManager.resetBatchNo(1);

		// Uncomment below if you want to prompt for new values BEFORE the reboot.
		//runStartupSetup();

		// Remove the databse file completely
		remove(DB_PATH);
		showStatus("Rebooting ...");
		Secins_reboot();
		break;
	}
	case 302: {
		showStatus("Rebooting ...");
		Secins_reboot();
	}
	case 303: {  // Advanced menu

		if(Utils::UtilsGetTerminalType() == Utils::TT_UX_300){
			XmlMenu xm("SUP_3");
			selValue = xm.selectXmlMenu(0, superMenuAction, true);
		}else{
			std::vector<std::string> value(1);
			std::string htmlStr =
					"<h4>Advanced Settings</h4><br>"
					"Please enter Password ('0'-'9') "
					"<input size='6' minlength='4' maxlength='6' name='pwd' type='password' mask='*' allowed_chars='1234567890'>";

			int r = uiInput("input", value, htmlStr);

			log.message(MSG_INFO "r="+Utils::UtilsIntToString(r)+"\r\n");

			if ((r == UI_ERR_OK) && (strcmp("123321", value[0].c_str()) == 0)){
				XmlMenu xm("SUP_3");
				selValue = xm.selectXmlMenu(0, superMenuAction, true);
			}
		}
		break;
	}
	case 309:
		showAbout();
		break;
	case 400:
		SetSoftwareIp();
		break;
	case 401:
		showDBMenu();
		break;
	case 410:
		viewLogFile("/home/usr1/logs/startup.log");
		break;
	case 411:
		viewLogFile("/home/usr1/logs/emv_log");
		break;
	case 412:
		viewLogFile("/var/log/messages");
		break;
	}
	log.message(MSG_INFO"menuAction() return:" + SSTR(selValue) + "\n");

	return retVal;
}

void settingsMenu(void) {

	setDefaultLayout();

	TuiTemplate("wheelimg", GLOBAL_LOADING_HTML);
	TuiTemplate("wheelstatusimg", GLOBAL_STATUS_HTML);

	printf("%s\n", GLOBAL_LOADING_HTML.c_str());

	conMng = new CConManager();

	XmlMenu xm("SUP");
	xm.selectXmlMenu(0, superMenuAction, true);

	conMng->close();
	delete conMng;

}

