#include <string>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sstream>
#include <iostream>
#include <fstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/timeb.h>
#include <gui/gui.h>
#include <gui/jsobject.h>
#include <libtui/tui.h>
#include <libipc/ipc.h>
#include "libda/cterminalconfig.h"
#include <libda/ccommsconfig.h>
#include <libda/cversions.h>
#include <libda/cversionsfile.h>
#include <libda/cconmanager.h>
#include <libsecins.h>
#include <svcmgr/svc_net.h>

#include "xmlmenu.h"
#include "info.h"
#include "CLog.h"
#include "libgoods.h"
#include "libalarm.h"
#include "cstatus.h"
#include "libcpa.h"
#include "IdleAppver.h"
#include "utils.h"

//#define TRACK_MEM_USAGE

#define IDLE_TIME_DELAY_US		100000

#define CPA_TASK		"CPA"
#define CLIENT_TASK		"Client"

typedef struct{
	int screen_width;
	int screen_height;
	int default_font_size;
}GuiInfo;

static GuiInfo gui_info;

typedef struct{
	GuiInfo gui_info;
}StatusThread;

static int status;
static com_verifone_status::OPERATION_STATUS operation_status=com_verifone_status::OP_STAT_IDLE;

/* Ux */
#define UX_STATUS_REG_HEIGHT	8  	// 55
#define UX_POPUP_MARGIN		2 		// 50
#define UX_POPUP_WIDTH		48 // 600
#define UX_POPUP_HEIGHT		48		// 300

/* MX */
#define MX_STATUS_REG_HEIGHT	55
#define MX_POPUP_MARGIN		50
#define MX_POPUP_WIDTH			600
#define MX_POPUP_HEIGHT		300

//using namespace std;
using namespace Utils;
using namespace vfigui;
using namespace za_co_verifone_tui;
using namespace za_co_verifone_xmlmenu;
using namespace com_verifone_terminalconfig;
using namespace com_verifone_status;

static CLog log(IDLEAPP_NAME);
static int info_pressed=0;
static int sysmode_button_pressed=0;
static bool return_to_idle=false;
static bool isIdleMoment = false;
static bool redoIdle = false;
static IdleAlarmNS::IdleAlarm * ipcAlarm;
void setDefaultLayout(void);
void parseSetStatus(std::string request,std::string &displayMessage);
bool getMemUsage( std::map<std::string,int> &memVals, bool update=true, std::string logKey = "*");
void settingsMenu (void);

void *status_tfunc(void *ptr)
{
	(void)ptr;			// Suppresses -> warning: unused parameter

	int rqst_id=UI_ERR_ABORT;
	string tmpl;
	bool forward_status=false;

	// Register an IPC channel that will listen for feedback messages that will be displayed in this popup window
	com_verifone_ipc::init("STATUS");

	// Connect to the Pos status thread, to forward status messages to
//	if(com_verifone_ipc::connect_to("PosStatus") < 0){
//		log.message(MSG_INFO "Failed to connect to PosStatus\n");
//	}else
//		forward_status = true;

		while(com_verifone_ipc::connect_to("PosStatus") < 0){
			log.message(MSG_INFO "Failed to connect to PosStatus\n");
			sleep(1);
		}

		log.message(MSG_INFO "Connected to PosStatus\n");
		forward_status = true;

	while(1) {
		string ipcBuffer;
		string ipcSourceTask,ipcAppFrom;

		if(com_verifone_ipc::receive(ipcBuffer,ipcSourceTask,ipcAppFrom)==com_verifone_ipc::IPC_SUCCESS){

//			log.message(MSG_INFO "STATUS Message: ["+ipcBuffer+"] from ["+ipcAppFrom+"]\n");

			if(ipcBuffer.compare("CLOSE STATUS") == 0){
				com_verifone_ipc::send("OK",ipcAppFrom);
				//setDefaultLayout();
				redoIdle = true;
				//uiDisplayWait(rqst_id, 0);
				//uiDisplay(3,"");
				if (rqst_id >= 0) uiInvokeCancel(rqst_id);
				rqst_id = UI_ERR_ABORT;

			} else if (ipcBuffer.compare("GET STATUS") == 0) {
				com_verifone_ipc::send(SSTR(status),ipcAppFrom);
			} else if (ipcBuffer.compare("GET OPERATION") == 0) {
				com_verifone_ipc::send(UtilsIntToString(operation_status),ipcAppFrom);
			}else if (ipcBuffer.find("SET STATUS") == 0){
				std::string displayString;
				com_verifone_ipc::send("OK",ipcAppFrom);

				parseSetStatus(ipcBuffer,displayString);

				// Pass the status code to the Pos app, in case it needs to be sent out
				if(forward_status)
					com_verifone_ipc::send(SSTR(status),"PosStatus");

				if (!displayString.empty()) {
					map<string,string> values;
					values["Message"] = displayString;

					//setPopupLayout();

					string popup_templ;
					int ret = TuiTemplate("Popup.tmpl", "PopupMsg", popup_templ);
					stringstream ss;
					ss << ret;

//					uiInvoke(3, values, popup_templ);

					if(rqst_id >= 0){
						// Make sure any previous display are cleared first
						//uiDisplayWait(rqst_id, 0);
						uiInvokeCancel(rqst_id);
					}
					//rqst_id = uiDisplayAsync("bgimage",tmpl);


					rqst_id = uiInvokeAsync(UI_REGION_DEFAULT/*3*/, values, popup_templ);
					sleep(3);
					if(rqst_id>=0) uiInvokeWait(rqst_id, 0);
				}

			}
		}

		if(rqst_id>=0){
			// Make sure any previous display are cleared first
			//uiInvokeCancel(rqst_id);
			//uiDisplayWait(rqst_id, 0);
			//log.message(MSG_INFO"invokewait,id=" + SSTR(rqst_id));
			uiInvokeWait(rqst_id, 0);
		}

		usleep(IDLE_TIME_DELAY_US);
	}
	return 0;
}

void *buttonbar_tfunc(void *ptr)
{
	(void)ptr;			// Suppresses -> warning: unused parameter

	int ret;
	string tmpl;

	TuiTemplate("status_menu", tmpl);

	while(1) {
		ret = uiDisplay(2,tmpl);

		if(ret == 1){
			log.message("Button pressed\r\n");
			info_pressed = 1;
		}

		usleep(IDLE_TIME_DELAY_US);
	}

	return 0;
}

void *statusbar_tfunc(void *ptr)
{
	(void)ptr;			// Suppresses -> warning: unused parameter

	bool first=true;
	string tmpl;
	struct netGsmRssi gprsSignalStrength;
	int gprsSigUpdate=5;

#ifdef TRACK_MEM_USAGE
	//         If this happen data will be logged to the /var/logs/messages file
	//		   (logging only occurs if the mem size change after)
	struct timeb tStart;
	ftime(&tStart); // get start time
	std::map<std::string,int> memVals;
	getMemUsage(memVals);

#endif // TRACK_MEM_USAGE

	if(isGprsExist)
		TuiTemplate("statusGprs", tmpl);
	else
		TuiTemplate("status", tmpl);

	while(1) {
		struct timeb tp;
		ftime(&tp);

		if(!first) {
			usleep((1000-tp.millitm)*1000); // wait for beginning of next second
			tp.time++;
			gprsSigUpdate++;
		}

		first=false;
		char buf[64];

		if(UtilsGetTerminalType() == TT_UX_300 && isGprsExist)
		{
			strftime(buf,sizeof(buf),"%d %b %y - %H:%M",localtime(&tp.time));	//%B=fullname ,Y= fullyear Dont display seconds on Ux

			if(gprsSigUpdate>=5) //get GPRS signals strength each after 5 seconds
			{
				gprsSignalStrength= net_gprsGetRssi();
//				log.message("GPRS signals Strength : "+SSTR(gprsSignalStrength.rssi)+"\r\n");
//				log.message(MSG_INFO "net_gprsGetStatus.... "+ SSTR(net_gprsGetStatus())+" \r\n");

				if(net_gprsGetStatus()!=NET_GSM_STT_GPRS_CONNECTED)
				{
//					net_gprsPowerOnModule();
					net_interfaceUp((char *)"ppp1", NET_IP_V4);
					log.message(MSG_INFO "Bringing Up GPRS....  \r\n");
				}
				gprsSigUpdate=0;
			}

			sprintf(buf+strlen(buf),"&nbsp;&nbsp;&nbsp; G  %02.0f%%",3.2*(float)gprsSignalStrength.rssi);
		}
		else
			strftime(buf,sizeof(buf),"%d %b %Y - %H:%M",localtime(&tp.time));
//			strftime(buf,sizeof(buf),"%d %B %Y - %H:%M:%S",localtime(&tp.time));

		uiDisplay(1,uiPrint(tmpl.c_str() ,buf));

#ifdef TRACK_MEM_USAG
		lCnt++;
		ftime(&tNow);
		long newSecsLapsed = ((tNow.time-tStart.time)*1000+tNow.millitm-tStart.millitm)/1000;
		if ((newSecsLapsed % 60 == 0) && (newSecsLapsed != secsLapsed)) {
			// Log Memory Usage
			//log.message(MSG_INFO "Mem usage changes after " + SSTR(newSecsLapsed-secsLapsed)
			//		+ " secs," + SSTR(lCnt) + " loops\n");
			memVals["mindif"] = (int)((newSecsLapsed-secsLapsed)/60);
			if (getMemUsage(memVals,updMem,"*")) { ;//VmRSS"); // will log changes to log file
				secsLapsed = newSecsLapsed;
			}
			//if (lCnt % 50 == 0) { dlog_msg("lcnt=%d",lCnt); }
			updMem = false; // only update first time
			if (newSecsLapsed > 50000) newSecsLapsed = 0;
			if (lCnt>5000) lCnt = 0;
		}
#endif // TRACK_MEM_USAGE



		usleep(IDLE_TIME_DELAY_US);
	}

	return 0;
}

void *alarm_thread_func(void *ptr)
{
	(void)ptr;

	com_verifone_ipc::init(string(ALARM_MASTER));
	IdleAlarmNS::IdleAlarm masterAlarms(ALARM_MASTER);
	ipcAlarm = &masterAlarms;

	// We check and process incoming IPC commands even if alarm is active

	while(1) {
		std::string ipcFrom, ipcBuffer;

		if(com_verifone_ipc::receive(ipcBuffer,ipcFrom,ipcFrom)==com_verifone_ipc::IPC_SUCCESS){
			//log.message(MSG_INFO "Got message ["+UtilsHexToString((const char*)ipcBuffer.c_str(), (unsigned int)ipcBuffer.length())+"]\r\n");

			std::string resp = masterAlarms.alarmProcessIPC(ipcFrom,ipcBuffer);
			if (resp.empty()) {
				//process_command(ipcFrom, ipcBuffer);
				log.message((MSG_INFO  "Error: Unknown/unexpected command received by master from ") + ipcFrom
						+ ":["+UtilsHexToString((const char*)ipcBuffer.c_str(), (unsigned int)ipcBuffer.length())+"]\r\n");
			} else if (resp.find("Error") == 0) {
				log.message((MSG_INFO  "Error response received by master from ") + ipcFrom + ":["+resp+"]\r\n");
			} else {
				// IdleApp alarm command processed correctly, do nothing
			}
			redoIdle = true;
		}

		if((isIdleMoment) && (operation_status == OP_STAT_IDLE)) {
			isIdleMoment = false;
			masterAlarms.alarmCheck(); // will only trigger an alarm if none is triggered already
			//isIdleMoment = true;
		}

		usleep(IDLE_TIME_DELAY_US);
	}
	return 0;
}

void setMsgLayout(  int left,      /* left position in pixels (+=width if negative) */
					int right,     /* right position in pixels (+=width if negative) */
					int bottom = -1  /* bottom position in pixels (+=height if negative) */
				 )
{
	if(UtilsGetTerminalType() == TT_UX_300){
		const struct UIRegion ownLayout[] = {
		// id, left, 					top, 					right, 					 bottom
		{3, UX_POPUP_MARGIN,			UX_STATUS_REG_HEIGHT+UX_POPUP_MARGIN,	gui_info.screen_width-UX_POPUP_MARGIN, 	gui_info.screen_height-UX_POPUP_MARGIN},  	// feedback popup
		{1, 0							,0, 					gui_info.screen_width, 	 UX_STATUS_REG_HEIGHT+2},  	// status bar //iq_audi_06072017 add 2
		{UI_REGION_DEFAULT, left, 		UX_STATUS_REG_HEIGHT+1+2,	right,					 bottom} 				// main screen //iq_audi_06072017 add 2
		};

		vfigui::uiLayout(ownLayout,	sizeof(ownLayout) / sizeof(ownLayout[0]));
	}else{
		const struct UIRegion ownLayout[] = {
		//id, left                   , top                                 , right                                , bottom
		{3, MX_POPUP_MARGIN          , MX_STATUS_REG_HEIGHT+MX_POPUP_MARGIN, gui_info.screen_width-MX_POPUP_MARGIN, gui_info.screen_height-MX_POPUP_MARGIN},  	// feedback popup
		{2, 0                        , 0                                   , 450            , MX_STATUS_REG_HEIGHT},  	// button bar
		{1, 450                      , 0                                   , 800            , MX_STATUS_REG_HEIGHT},  	// status bar
		{UI_REGION_DEFAULT           , left                                , MX_STATUS_REG_HEIGHT                 , right, bottom} 				// main screen
		};

		vfigui::uiLayout(ownLayout,	sizeof(ownLayout) / sizeof(ownLayout[0]));
	}
}
void setDefaultLayout() {
	//  		left, 		right		   bottom									 top
	setMsgLayout(0, gui_info.screen_width+1, gui_info.screen_height+1);
}

void checkDatabaseUpdate(void)
{
	FILE *fh=NULL;

	fh = fopen("flash/dbupdate1.dat", "r");  //if fail to open return NULL

	if(fh == NULL){
		// Update has not been performed, remove database
		log.message(MSG_INFO "checkDatabaseUpdate() - Removing out of date database...\n");

		remove(DB_PATH);

		fh = fopen("flash/dbupdate1.dat", "wb");
	}

	fclose(fh);

	log.message(MSG_INFO "checkDatabaseUpdate() - Done\n");
}

void checkDatabase(void) {
	// Check if the databse has to be updated
	checkDatabaseUpdate();

	// if the master db exist copy to flash and delete the master

	// Check if the file exists
	struct stat file_status;

	if(stat(DB_PATH, &file_status) == 0)
	{
		// Check that the file is not 0 size
		if(file_status.st_size > 0){
			return;		// Nothing to do
		}
	}

	// Copy the file from RAM to flash
    std::ifstream source(DB_PATH_ORG, std::ios::binary);
	std::ofstream dest(DB_PATH, std::ios::binary);

	dest << source.rdbuf();

	dest.close();
	source.close();

	remove(DB_PATH_ORG);
}

void runStartupSetup() {
	// Check if setup needs to be run
	using namespace com_verifone_terminalconfig;

	checkDatabase();

	// Get current values
	std::string merchId = "";
	std::string termId = "";
	bool isComplete;
	{   // need to free this before doing setup - hence inside the {}
		CTerminalConfig termcfg;
		merchId = termcfg.getMerchantNo();
		termId = termcfg.getTerminalNo();
		isComplete = termcfg.isSetupComplete();
	}
	//isComplete = false;
	// if (!isComplete || merchId.empty() || termId.empty()) {

	CpaApp::DoSetup();

	// If we get here Setup() must be succesfull
	if (!isComplete) {
		CTerminalConfig termcfg;
		std::string merchId = termcfg.getMerchantNo(); // we do not use database merchant
		std::string termId = termcfg.getTerminalNo(); // same for terminal
		log.message(MSG_INFO"Setup new: merchId=" + merchId + ",termId=" + termId + "\n");
	} else {
		log.message(MSG_INFO"Setup already: merchId=" + merchId + ",termId=" + termId + ",compl=" + SSTR(isComplete) + "\n");
	}

}

void parseSetStatus(std::string request,std::string &displayMessage) {

	istringstream ss(request.substr(string("SET STATUS:").length()));
	log.message(MSG_INFO "Set status to "+ss.str()+"\r\n");

	STATUS statusCode = (STATUS)UtilsStringToInt(ss.str());

	displayMessage.clear();

	switch (statusCode) {
		case TERMINAL_READY:
			status = statusCode;
			break;
		case TERMINAL_OFFLINE:
			status = statusCode;
			break;
		case TRANSACTION_GOING_ONLINE:
			status = statusCode;
			break;
		case WAITING_FOR_CARD_INSERT:
			status = statusCode;
			break;
		case CARD_INSERTED:
			status = statusCode;
			break;
		case CARD_REMOVED:
			status = statusCode;
			break;
		case CARD_SWIPED:
			status = statusCode;
			break;
		case COMMS_ERROR:
			status = statusCode;
			break;
		case WAITING_FOR_RESPONSE:
			status = statusCode;
			displayMessage = "Authorising\nPlease Wait";
			break;
		case HOST_RESPONSE_RECEIVED:
			status = statusCode;
			break;
		case TERMINAL_BUSY:
			status = statusCode;
			break;
		case BANKING_STARTED:
			status = statusCode;
			operation_status = OP_STAT_BANKING;
			break;
		case BANKING_COMPLETE:
			status = statusCode;
			operation_status = OP_STAT_IDLE;
			break;
		case WAITING_CARD_HOLDER_VERIFICATION:
			status = statusCode;
			break;
		case WAITING_AMOUNT_CONFIRMATION:
			status = statusCode;
			break;
		case WAITING_CARD_REMOVAL:
			status = statusCode;
			break;
		case VELOCITY_EXCEEDED:
			status = statusCode;
			break;
		case WAITING_ACCOUNT_SELECTION:
			status = statusCode;
			break;
		case WAITING_BUDGET_TYPE_SELECTION:
			status = statusCode;
			break;
		case WAITING_BUDGET_PERIOD_SELECTION:
			status = statusCode;
			break;
		case BATCH_FILE_EMPTY:
			status = statusCode;
			break;
		case PARM_DOWNLOAD_STARTING:
			status = statusCode;
			operation_status = OP_STAT_PARAM_DOWNLOAD;
			break;
		case PARM_DOWNLOAD_COMPLETE:
			status = statusCode;
			operation_status = OP_STAT_IDLE;
			break;
		case RESERVED1:
			status = statusCode;
			break;
		case RESERVED2:
			status = statusCode;
			break;
		case WRONG_CARD_TYPE:
			status = statusCode;
			break;
		case PIN_MISMATCH:
			status = statusCode;
			break;
		case ENTER_PIN:
			status = statusCode;
			break;
		case PIN_ENTERED:
			status = statusCode;
			break;
		case INCORRECT_PIN:
			status = statusCode;
			break;
		case SCRIPT_SUCCESSFUL:
			status = statusCode;
			break;
		case DO_NOT_REMOVE_CARD:
			status = statusCode;
			break;
		case NO_COMMS_CABLE:
			status = statusCode;
			break;
		case BALANCE_EXCEEDED_ALLOWED_VALUE:
			status = statusCode;
			break;
		case BALANCE_BELOW_ALLOWED_VALUE:
			status = statusCode;
			break;
		case PAYMENT_NOT_ALLOWED:
			status = statusCode;
			break;
		case INVALID_CARD:
			status = statusCode;
			break;
		case NOT_PIN_BASED_CARD:
			status = statusCode;
			break;
		case CARD_EXPIRED:
			status = statusCode;
			break;
		case CARD_NOT_EXPIRED:
			status = statusCode;
			break;
		case SCRIPT_DECLINED:
			status = statusCode;
			break;
		case CARD_AUTH_FAILED:
			status = statusCode;
			break;
		case CHIP_NOT_READ:
			status = statusCode;
			break;
		case APP_BLOCKED:
			status = statusCode;
			break;
		case APP_EXPIRED:
			status = statusCode;
			break;
		case BAD_EXPIRY_DATE:
			status = statusCode;
			break;
		case PAN_MISMATCH:
			status = statusCode;
			break;
		case IPC_ERROR:
			status = statusCode;
			break;
		case PARM_DWNLD_TERMINAL:
			displayMessage = "Downloading Terminal Parameters";
			status = statusCode;
			operation_status = OP_STAT_PARAM_DOWNLOAD;
			break;
		case PARM_DWNLD_CUSTOM:
			displayMessage = "Downloading Custom Parameters";
			break;
		case PARM_DWNLD_DATE_TIME:
			displayMessage = "Downloading Date and Time";
			break;
		case PARM_DWNLD_CARDS:
			displayMessage = "Downloading Card Parameters";
			break;
		case PARM_DWNLD_COMMS:
			displayMessage = "Downloading Comms Parameters";
			break;
		case PARM_DWNLD_MERCHANT_MESSAGE:
			displayMessage = "Downloading Merchant Message";
			break;
		case PARM_DWNLD_APP_BITMAP:
			displayMessage = "Downloading App Bitmap";
			break;
		case PARM_DWNLD_SW_UPDATE:
			displayMessage = "Downloading SW Update Parameters";
			break;
		case PARM_DWNLD_HOTCARDS:
			displayMessage = "Downloading Hotcards";
			break;
		case PARM_IMPORTING:
			displayMessage = "Importing Parameters";
			break;
		case RETURN_TO_IDLE:
			return_to_idle = true;
			break;
		case WAIT_FOR_H_COMMAND:
			status = statusCode;
			break;
		case SENT_H_COMMAND:
			status = statusCode;
			break;
		case CONTINUE_WITH_EMV_COMPLETE_SUCCESS:
			status = statusCode;
			break;
		case CONTINUE_WITH_EMV_COMPLETE_FAIL:
			status = statusCode;
			break;
		case COMMS_CONNECTING:
			status = statusCode;
			displayMessage = "Connecting...";
			break;
		case COMMS_ERROR_CONNECTING:
			displayMessage = "Error Connecting";
			break;
		case COMMS_ERROR_SENDING:
			displayMessage = "Error Sending";
			break;
		case COMMS_TIMEOUT:
			displayMessage = "Communications Timeout";
			break;
		case TRANSACTION_START:
			operation_status = OP_STAT_TRANSACTING;
			break;
		case TRANSACTION_COMPLETE:
			operation_status = OP_STAT_IDLE;
			break;
		default:
			log.message(MSG_INFO "STATUS NOT IMPLEMENTED\r\n");
			break;
	}
}

#include <libhim.h>

class testclass:public za_co_vfi_Him::Him
{
public:
	testclass():Him(){}
	void Feedback(string message){ (void) message;}
};
/*
int menuAction(int selValue, std::string menuId) {
	(void)menuId; // get rid of warning
	CStatus status;
	switch (selValue) {
		case 101:
		{
			testclass tst;
			status.setStatus(BANKING_STARTED);

			if(tst.DoBanking() == 0)
				CpaApp::DoReport(REPORT_PREVIOUS_BATCH);	// By the time we get here, the batch has closed and moved, so we print the previous batch

			status.setStatus(BANKING_COMPLETE);
			break;
		}
		case 110:
		{
			testclass tst;
			status.setStatus(PARM_DOWNLOAD_STARTING);

			if(tst.FullParameters() < 0){

			}

			status.setStatus(PARM_DOWNLOAD_COMPLETE);
			break;
		}
		case 111:
		{
			testclass tst;
			status.setStatus(PARM_DOWNLOAD_STARTING);
			tst.PartialParameters();
			status.setStatus(PARM_DOWNLOAD_COMPLETE);
			break;
		}
		case 120: log.message(MSG_INFO"Users to be implemented\n"); break;
		case 121:
		{
			log.message(MSG_INFO"Config selected; temporarily used for Setup\n");
			runStartupSetup();
			break;
		}
		case 122:
		{
			log.message(MSG_INFO"Send IPC to do BatchReport ...\n");
			selValue =  CpaApp::DoMenu("CPM_2_1");
			break;
		}
		case 201:
		{
			log.message(MSG_INFO"Send IPC to do CPA MENU ...\n");
			selValue =  CpaApp::DoMenu("CPM");
			break;
		}
	}

	return selValue;
}
*/
pthread_t tVersion;

void *update_versions(void*)
{
	com_verifone_versions::CVersions versions;
	com_verifone_versionsfile::CVersionsFile vfile;

	log.message(MSG_INFO "IdleApp start writing versions\r\n");

	vfile.setName("");
	vfile.setVersion(IDLEAPP_VERSION);
	versions.addFile(IDLEAPP_NAME, vfile);

	while(1){	// Keep trying every second, until we succeed
		if(versions.addFile(IDLEAPP_NAME, vfile) == DB_OK) break;
		sleep(1);
	}

	// Bundle versions
	SecinsBdlInfo bdlinfo;

	while(Secins_read_bdllist_entry(&bdlinfo, sizeof(SecinsBdlInfo))){
		com_verifone_versionsfile::CVersionsFile bdlvfile;
		bdlvfile.setName("");
		bdlvfile.setVersion(string(bdlinfo.version));
		while(1){	// Keep trying every second, until we succeed
			if(versions.addFile(string(bdlinfo.name), bdlvfile) == DB_OK) break;
			sleep(1);
		}
	}

	SecinsPkgInfo pkginfo;

	while(Secins_read_pkglist_entry(&pkginfo, sizeof(SecinsPkgInfo))){
		com_verifone_versionsfile::CVersionsFile pkgvfile;
		pkgvfile.setName("");
		pkgvfile.setVersion(string(pkginfo.version));
		while(1){	// Keep trying every second, until we succeed
			if(versions.addFile(string(pkginfo.name), pkgvfile) == DB_OK) break;
			sleep(1);
		}
	}

	com_verifone_versionsfile::CVersionsFile guifile;
	guifile.setName("");
	guifile.setVersion(string(uiLibVersion()));
	while(1){	// Keep trying every second, until we succeed
		if(versions.addFile("DirectGui", guifile) == DB_OK) break;
		sleep(1);
	}

	log.message(MSG_INFO "IdleApp versions complete\r\n");

	return NULL;
}

pthread_t sysmode_button_thread_id;

void *setup_sysmode_button(void *parm)
{
	(void)parm;		// Gets rid of compiler warning

	if(UtilsGetTerminalType() != TT_UX_300)
		return NULL;

	FILE *fh = fopen ( "/sys/devices/platform/sysmode-button/wake_apps", "rb");
	if(fh != NULL)
	{
		char value=0;
		int num=0;

		while(1){
			num = fread (&value, 1, 1, fh);
			if((isIdleMoment) && (operation_status == OP_STAT_IDLE) && (num == 0)){
				log.message(MSG_INFO "Sysmode button pressed\n");
				sysmode_button_pressed = 1;
			}
			usleep(IDLE_TIME_DELAY_US);
		}
	}else{
		log.message(MSG_INFO "failed to open /sys/devices/platform/sysmode-button/wake_apps\r\n");
	}

	return NULL;	// Should never get here
}


bool IsPrimaryOrSecComGprs()
{
	com_verifone_commsconfig::CCommsConfig comms_cfg;
	com_verifone_commssettings::CCommsSettings settings;

	comms_cfg.getPriAuthCommsSettings(settings);
	if(settings.getCommsType()==CCommsSettings::GPRS)
		return true;

	comms_cfg.getSecAuthCommsSettings(settings);
	if(settings.getCommsType()==CCommsSettings::GPRS)
		return true;

	isGprsExist=false;
	return false;

}

void uiMain(int argc, char *argv[])
{
	(void)argc;			// Suppresses -> warning: unused parameter
	(void)argv;			// Suppresses -> warning: unused parameter

	int goods_connect=-1, client_connect=-1;
	int display_idle_timeout = 1;

	log.message(MSG_INFO "IdleApp starting...\r\n");


	com_verifone_ipc::init(std::string(IDLEAPP_NAME));

	int ret = TuiInit(IDLEAPP_NAME);

	if(ret < 0){
		char buffer[10];
		sprintf(buffer, "%d", ret);
		log.message(MSG_INFO "TuiInit returned "+string(buffer)+"\r\n");
	}

	uiSetPropertyString (UI_PROP_RESOURCE_PATH, "flash/IdleApprsc/");
	uiReadConfig();

//	pthread_create(&tVersion,NULL,&update_versions,NULL);

	// Setup the thread that will handle the status bar
	StatusThread status_info;
	pthread_t status_pthread, buttonbar_pthread, alarm_thread, popup_pthread;

	// Read some config parameters
	uiGetPropertyInt(UI_DEVICE_WIDTH, &gui_info.screen_width);
	uiGetPropertyInt(UI_DEVICE_HEIGHT, &gui_info.screen_height);
	uiGetPropertyInt(UI_PROP_DEFAULT_FONT_SIZE,&gui_info.default_font_size);

	log.message(MSG_INFO"Display width :"+UtilsIntToString(gui_info.screen_width)+"\r\n");
	log.message(MSG_INFO"Display height:"+UtilsIntToString(gui_info.screen_height)+"\r\n");
	log.message(MSG_INFO"Font size     :"+UtilsIntToString(gui_info.default_font_size)+"\r\n");

	setDefaultLayout();

	if(IsGprsExist()) //iq_audi_06072017
	{
		if(IsPrimaryOrSecComGprs())
		{
			if(net_gprsPowerOnModule())
				log.message(MSG_INFO "GPRS Module Up FAILED  \r\n");
			else
				log.message(MSG_INFO "GPRS Module UP SUCCESSFUL  \r\n");

			if(net_interfaceUp((char *)"ppp1", NET_IP_V4))
				log.message(MSG_INFO "GPRS UP FAILED  \r\n");
			else
				log.message(MSG_INFO "GPRS UP SUCCESSFUL  \r\n");
		}
	}

	pthread_create(&popup_pthread,0,status_tfunc,NULL);
	pthread_create(&buttonbar_pthread,0,buttonbar_tfunc,NULL);
	pthread_create(&status_pthread,0,statusbar_tfunc,&status_info);
	pthread_create(&alarm_thread,0,alarm_thread_func,NULL);

	//iq_audi_16082017 no current need of this thread
//	pthread_create(&sysmode_button_thread_id,NULL,setup_sysmode_button,NULL);

	runStartupSetup();

	std::string ipcBuffer;
	std::string ipcFrom;
	goods_connect = GoodsApp::Connect();

	if(goods_connect != 0){
		log.message(MSG_INFO "Connect to Goods failed!\r\n");
	}

	while(1){
		int iret=-1, rqst_id;
		string tmpl;

		// Provides the ability for an ExpendoI client app to override the default idle display
		if(UtilsFileExist((char*)"flash/IdleApprsc/idle.html")){
			rqst_id = uiInvokeURLAsync("idle.html");
		}else{
			TuiTemplate("IdleScreen", tmpl);
			rqst_id = uiDisplayAsync("bgimage",tmpl);
		}

		redoIdle = false;

		while(1){
			int done=0;

			if(client_connect == -1){
				client_connect = com_verifone_ipc::connect_to(CLIENT_TASK);

				if(client_connect != 0){
					client_connect = -1;
				}else{
					display_idle_timeout = 100;
					log.message(MSG_INFO "Client connected\r\n");
				}
			}

			iret = uiDisplayWait(rqst_id, display_idle_timeout);

			if(iret == UI_ERR_TIMEOUT){
				break;
			}

			// First handle any status bar buttons that might have been pressed
			if(info_pressed){
				info_pressed = 0;

				InfoScreen();

				break;
			}

			if(sysmode_button_pressed){
				sysmode_button_pressed = 0;
				settingsMenu();
				break;
			}

			if(return_to_idle){
				// This is used when the Pos interface is finished and the terminal needs to refresh the idle display
				return_to_idle = false;
				break;
			}

			if(iret <= 0){
				// Assume idle mode and check and trigger alarm if necessary (we may want to wait for a longer time ??)
				isIdleMoment = true;
				// allow idle alarmcheck in thread for ? ms
				//log.message("-idle");
				usleep(200*1000);
				isIdleMoment = false;

			}else{
				// Handle landing page actions
				switch(iret){
					case 21:
					{
						log.message(MSG_INFO "Goods command\n");

						string resp;
						if(GoodsApp::SendCommand(GoodsApp::GOODS_TX_RQST, resp, 0) == 0){

							while(GoodsApp::WaitResponse(resp, 1) == 0)
								log.message(MSG_INFO "Waiting for " + GoodsApp::GetId() + "...\r\n");

							log.message(MSG_INFO "Received  " + resp +  " from " + GoodsApp::GetId() + "...\r\n");
							done = 1;
						}
						break;
					}
					case 22:
					{
						log.message(MSG_INFO "Settings command\n");

						operation_status = OP_STAT_TRANSACTING;

						CpaApp::DoMenu("CPM");
/*
						XmlMenu xm("SM");
						int r = xm.selectXmlMenu(0,menuAction,true);
						log.message(MSG_INFO " after select, rc=" + UtilsIntToString(r) + "\n");
*/
						operation_status = OP_STAT_IDLE;

						done = 1;
						break;
					}
					default:
						log.message(MSG_INFO "Client command "+UtilsIntToString(iret)+"\n");

						if(client_connect == 0){
							string cmd = "Go:"+UtilsIntToString(iret);
							string response;
							string ipcFrom;
							struct timeb start_tp;
							struct timeb  current_tp;

							ftime(&start_tp);

							com_verifone_ipc::send(cmd,CLIENT_TASK);

							while(1){
								if(com_verifone_ipc::receive(response,CLIENT_TASK,ipcFrom)==com_verifone_ipc::IPC_SUCCESS){
									break;
								}
								// Check the timeout value
								ftime(&current_tp);
#if 0	// Skip timeout checking for now
								if(timeout_secs <= current_tp.time - start_tp.time)
									break;
#endif
								usleep(100000);
							}

							done = 1;
						}
						break;
				}
			}

			if(done || redoIdle)
				break;
		}

		if (iret == UI_ERR_TIMEOUT) {
			// Assume idle mode and check and trigger alarm if necessary (we may want to wait for a longer time ??)
			isIdleMoment = true;
			// allow idle alarmcheck in thread for ? ms
			//log.message("-idle");
			usleep(200*1000);
			isIdleMoment = false;
		}

		if (rqst_id >= 0) {
			uiInvokeCancel(rqst_id);
		}
		//uiDisplayWait(rqst_id, 0);	// Invalidate the rqst_id

		usleep(IDLE_TIME_DELAY_US);		// Sleep for 100 milli seconds
		//log.message(MSG_INFO "Redisplay Idle,p=" + SSTR(redoIdle)+ "\n");

	}
}

int main(int argc, char *argv[])
{
	uiMain(argc, argv);
	return 0;
}

