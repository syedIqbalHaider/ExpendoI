#ifndef _LIBALARM_H_
#define _LIBALARM_H_

#include <map>
#include <sstream>

using std::string;
using std::istringstream;
using std::endl;

#ifndef SSTR
	#define SSTR( x ) dynamic_cast< std::ostringstream & >( std::ostringstream() << std::dec << x ).str()
#endif

namespace IdleAlarmNS
{
	typedef  std::map<std::string,std::string> stdStringMap;

	// Internal Tag values
	#define TAG_IDLE						"\xFF\xB1"

	// IDLE commands
	#define CMD_SIZE				3
	// Send TO master
	#define IDLE_CMD_ALARM_SET		TAG_IDLE"\x01"			// set idle alarm
	#define IDLE_CMD_ALARM_CLR		TAG_IDLE"\x04"			// alarm must be removed
	// Send FROM master
	#define IDLE_CMD_ALARM_IID		TAG_IDLE"\x02"			// alarm id returned
	#define IDLE_CMD_ALARM_TRG		TAG_IDLE"\x03"			// alarm was triggered

	// IDLE command parameter tags
	#define IDLE_PARAM_TX_IID				"\x9f\x01"		// system wide uniqueid for the alarm
	#define IDLE_PARAM_TX_STIME				"\x9f\x02"		// start time for the alarm
	//#define IDLE_PARAM_TX_LID				"\x9f\x03"		// local app's id for the alarm

	#define ALARM_MASTER	"AlarmApp"

}

namespace IdleAlarmNS {

/**
 * Provide an alarm set library for apps. The virtual function alarmAction is called
 * when an alarm is triggered. Note:
 *  - Alarms are normally only triggered when we are in an idle state.
 *  - The IPC remote comms are handled internally; alarmProcessIPC() should be called to check for
 *    and process data received.
 *
 * The basic sequence of calls in normal processing are
 *        Local App                                        Master App (Idle App)
 *        ---------                                        ---------------------
 *        IdleAlarm(local_name,idleapp_name)
 *
 *        alarmSet(startTime) --> IDLE_CMD_ALARM_SET --->  alarmAddLocal()
 *                            <-- IDLE_CMD_ALARM_IID <---
 *                                     ...
 *                                              		   alarmCheck()
 *        alarmTrigger()      <-- IDLE_CMD_ALARM_TRG <---  alarmTrigger()
 *                                                         set activeAlarmId
 *                            --> IDLE_CMD_ALARM_CLR --->  alarmClear()
 *                                                         clear activeAlarmId
 *
 *  Other Standalone commands possible on app side:
 *
 *        alarmClear()        --> IDLE_CMD_ALARM_CLR --->  alarmClear()
 *
 *        alarmTrigger()
 */
class IdleAlarm
{
public:
	IdleAlarm(const std::string local_app, const std::string master_app = ALARM_MASTER);
	virtual ~IdleAlarm();

	std::string alarmSet(time_t startDateTime,const std::string localAlarmId = "");	// returns systemwide unique id for this alarm; can optionally also give own name for alarm (both will identify)

	std::string alarmSet(time_t startDateTime,const stdStringMap &alarmData,   // note that alarmData is local to the app setting alarm; not persisted and a copy of the data is made
						 const std::string localAlarmId = "");

	bool alarmClear(const std::string alarmId = "");					// Empty prm implies clear all alarms set from this app

	void alarmTrigger(const std::string alarmId, bool mustClear = true);						// Trigger alarm from code regardless of actual time

	std::string alarmProcessIPC(const std::string received_from, const std::string cmd); // return empty string if not-applicable, else result or error message starting with Error

	bool isSameAlarm(std::string localName, std::string libName);

	// Below will be called if alarm is triggered via alarmTrigger or via alarmProcessIPC
	// When this return the idle app will assume the alarm was processed
	virtual void alarmAction(std::string alarmId, stdStringMap &alarmData)
							{ (void)alarmId; (void)alarmData; };

	// The following methods are normally called only when used inside the Master (IdleApp)

	void alarmCheck(); // check if alarms are to be triggered and trigger if necessary

	bool isAlarmTriggerActive();


protected:
	std::string getAlarmList();

	bool isMaster;
	static long alarmIdNr;
	std::string appName;											// Local app name
	std::string appMaster;											// Master app name
	std::map<std::string,stdStringMap> alarmDataMap;     			// Contains optional extra alarm data; indexed by alarm id
	stdStringMap remKeyFor;     									// Contains local alarms; indexed by local alarm id, gives remote Id
	stdStringMap locKeyFor;     									// Contains all alarms; indexed by remote alarm id, gives local id (which can also be remote id)
	std::multimap<time_t,std::string> timeAlarmMap;     			// Indexed and ordered by time_t, gives unique remote_id
	std::string activeAlarmId;   									// Alarm triggered and active; will be empty if completed
	std::string alarmAddLocal(const std::string alarmAppName, time_t t);
	std::string getUniqueAlarmId(const std::string alarmAppName, const std::string locId = "");	// can optionally provide own local id
	bool sendIdToApp(std::string remApp, const char *cmd, std::string sId);
	int returnUniqueId(const std::string ipcDest, const std::string uniqueId);
	bool alarmClear(const std::string alarmAppName, const std::string alarmId, bool mustDoRemote = true);
};




}

#endif // _LIBALARM_H_
