#include <sys/timeb.h>
#include <libipc/ipc.h>
#include <unistd.h>

#include "IdleAppver.h"
#include "libalarm.h"
#include "bertlv.h"
#include "CLog.h"
#include "string.h"

using namespace IdleAlarmNS;
using namespace za_co_verifone_bertlv;

static CLog log("IDLEAPPLIB");

namespace IdleAlarmNS {

const int timeout_secs = 30;

long IdleAlarm::alarmIdNr = 0;

std::string subString(const char * svIn, const char *svBeg, const char *svEnd);

// Instantiates and open connection to Master if we are NOT the master
// Note that com_verifone_ipc::init  should have been called already
IdleAlarm::IdleAlarm(const std::string local_app, const std::string master_app)
{
		//	log.message(MSG_INFO "IdleAlarm initialising\n");
	appName = local_app;
	appMaster = master_app;
	isMaster = (appName == appMaster);
	activeAlarmId = "";
	log.message((MSG_INFO "IdleAlarm M=") + appMaster + ",L=" + appName + "\r\n");
	if (!isMaster) {
		int ipcRet = com_verifone_ipc::connect_to(appMaster);
		if(ipcRet != com_verifone_ipc::IPC_SUCCESS){
			std::stringstream ss;
			ss << MSG_INFO << "Failed to connect to " << appMaster << ", errcode=" << ipcRet <<  endl;
			log.message(ss.str());
		}
		log.message(MSG_INFO + SSTR(" libalarm connected to ") + appMaster + "\n");
	}
}

IdleAlarm::~IdleAlarm()
{
	if (!isMaster) {
		com_verifone_ipc::disconnect(appMaster);
		log.message(MSG_INFO " libalarm disconnected from " + appMaster + "\n");
	}
}

bool IdleAlarm::isAlarmTriggerActive() {
	return (!activeAlarmId.empty());
}

std::string buildIPCcommand(std::string tag, std::string data) {
	BERTLV tlv;
	tlv.addTag(tag, data);
	return tlv.getStringData();
}

int WaitResponse(const std::string app_name, const long timeout_secs, std::string &response, std::string &ipcFrom)
{
	struct timeb start_tp;
	struct timeb current_tp;

	ftime(&start_tp);

	response = "";
	while(1){
		if((com_verifone_ipc::receive(response,app_name,ipcFrom)==com_verifone_ipc::IPC_SUCCESS)){
			break;
		}

		// Check the timeout value
		ftime(&current_tp);
		if(timeout_secs <= current_tp.time - start_tp.time)
			return 0;

		usleep(100000); // sleep for 100 ms
	}

	return (int)response.length();
}

std::string UtilsHexToString(const char *data, unsigned int len)
{
	std::stringstream ss;

	ss << std::hex;
	for (size_t i = 0; len > i; ++i) {
			if(static_cast<unsigned int>(static_cast<unsigned char>(data[i])) <= 0x0f)
				ss << '0';
			ss << static_cast<unsigned int>(static_cast<unsigned char>(data[i]));
	}

	return ss.str();
}


/**
 * Returns the substring between the end of svBeg and the beginning of svEnd.
 * We search for svEnd only from after svBeg; Therefore we will find
 * a substring delimited by two identical chars (e.g. aaa"bc"ddd ).
 * If either of the demarkating strings is not found, we return "".
 * We return the first match found.
 * @param svIn   The string to be searched
 * @param svBeg  The start string - empty implies beginning
 * @param svEnd  The end string - empty implies end
 * @return The selected substring (see description)
 */
std::string subString(const char * svIn, const char *svBeg, const char *svEnd)
{
   const char *pSt, *pEnd;

   std::string svRes = "";//strcpy(svRes,"");

   if ((svBeg == NULL) || (svEnd == NULL)) return svRes;

   if (*svBeg)
     pSt = strstr(svIn, svBeg);
   else
     pSt = svIn;

   if (pSt == NULL)
      return svRes;                        // Could not find start
   else
   {
      pSt += strlen(svBeg);
      if(! (*pSt))
        return svRes;         // at end of input
      else if (!(*svEnd)) {
          svRes = pSt;
          return svRes;  // will return all to end of string
      }
      else
      {
         pEnd =  strstr(pSt,svEnd);
         if (pEnd == NULL)
            return svRes;           //Could not find end or empty substring
         else {
             svRes = std::string(pSt,pEnd-pSt);
             //svRes[pEnd-pSt] = 0;
         }
      }
   }
   return svRes;
}



/**
 * Returns unique id.
 * Note: For now we keep everything in mem - should we use the db id in the future?
 * @param alarmAppName - Master will use this to identify all alarms belonging to an app
 * @param locId - if provided will replace generated id; result can always be identified as
 * 				  belonging to calling app; but calling app must make sure it is unique inside this app
 * 				  (see alarmSet)
 * 				- if not provided will always be unique over all apps
 * @return
 */
std::string IdleAlarm::getUniqueAlarmId(const std::string alarmAppName, const std::string locId) {
	std::string alarmId = locId;
	if (alarmId.empty()) {
		long id = ++alarmIdNr;
		alarmId = SSTR(id);
	}
	std::string sId = (isMaster ? "M" : "L") + SSTR(".") + alarmAppName + "." + alarmId;
	return sId;
}

std::string IdleAlarm::alarmAddLocal(const std::string alarmAppName, time_t t) {
	std::string sUniqueAlarmId = getUniqueAlarmId(alarmAppName);

	// timeAlarmMap map is ordered by time and allow duplicates on time
	timeAlarmMap.insert(std::make_pair(t,sUniqueAlarmId));

	locKeyFor[sUniqueAlarmId] = sUniqueAlarmId;

	log.message((MSG_INFO "alarmAddLocal,id=")+sUniqueAlarmId+"\r\n");

	return sUniqueAlarmId;
}

/**
 * Set an alarm at the given time and return a systemwide unique alarm identifier.
 * @param startDateTime
 * @param localAlarmId - uniquely identifies alarm if provided; can be empty.
 * 						 Gives error if non-empty and not unique.
 * @return system wide unique alarm identifier or message starting with "Error:" on failure
 */
std::string IdleAlarm::alarmSet(time_t startDateTime,const std::string localAlarmId) {
	stdStringMap emptyMap;
	return alarmSet(startDateTime,emptyMap,localAlarmId);
}

/**
 * Set an alarm at the given time and return a systemwide unique alarm identifier. Provides the alarmData
 * as parameter when the alarm is triggered.
 * @param startDateTime
 * @param alarmData    - passed on alarm trigger action
 * @param localAlarmId - uniquely identifies alarm if provided in later alarm calls; can be empty
 * 						 Gives error if non-empty and not unique.
 * @return system wide unique alarm identifier or message starting with "Error" on failure
 */
std::string IdleAlarm::alarmSet(time_t startDateTime, const stdStringMap &alarmData,
								const std::string localAlarmId)
{
	//log.message(MSG_INFO "SetIdleAlarm\n");
	std::string retVal = "";
	std::string alarmKey = getUniqueAlarmId(appName,localAlarmId);

	// Verify unique alarmid if provided
	if (!alarmKey.empty() && locKeyFor.find(alarmKey) != locKeyFor.end()) {
		retVal = "Error: Alarm id is not unique: " + alarmKey;
		return retVal;
	}

	// Connect to idleapp
	std::string responseId = "";
	std::string ipcCmd = "send";

	// Build Command containing local alarmId & time
	std::string timeString = SSTR(startDateTime);
	std::string alarmCommand = buildIPCcommand(IDLE_PARAM_TX_STIME,timeString);
	alarmCommand = buildIPCcommand(std::string(IDLE_CMD_ALARM_SET, CMD_SIZE), alarmCommand);

	// Now send the command
	std::string ipcFrom;
	//log.message("alarmSet sending to " + appMaster + ", time=" + timeString + "...\n");
	int ipcRet = com_verifone_ipc::send(alarmCommand,appMaster);

	if(ipcRet == com_verifone_ipc::IPC_SUCCESS){
		// Wait until response or timeout
		int len = WaitResponse(appMaster,timeout_secs, responseId, ipcFrom);
		if (len <= 0) responseId = "Error: SetIdleAlarm timeout on waiting for " + appMaster + " after " + SSTR(timeout_secs) + " secs.";

		// Process response
		if (responseId.find("Error") != 0) {
			// set response to id received
			responseId = alarmProcessIPC(ipcFrom,responseId);

			if (responseId.find("Error") != 0) {
				// Save alarmdata (if any) locally
				if (!alarmData.empty())
					alarmDataMap[responseId] = alarmData;
				if (localAlarmId.empty()) {
					locKeyFor[responseId] = responseId;
				} else {
					locKeyFor[responseId] = alarmKey;
					remKeyFor[alarmKey] = responseId;
				}
				//timeAlarmMap[timeString] = responseId;
			}
		}
	}
	if(ipcRet != com_verifone_ipc::IPC_SUCCESS){
		std::stringstream ss;
		ss << MSG_INFO << "alarmSet failed on ipc cmd " << ipcCmd << " to " << appMaster << ", errcode=" << ipcRet <<  endl;
		log.message(ss.str());
		responseId = "Error: alarmSet failed wth ipc error code" + SSTR(ipcRet);
	}

	return responseId;

	// Note: alternate implementation:
	//       1) Instead of sending to idleapp, just write alarm values to alarm_table in database with
	//       	unique app_id; return the pk_id as the unique identifier.
	//       2) Just send a ALARM_REFRESH command to IPC to force immediate rereading of alarms
	//       3) Idle App sends alarm_trigger when applicable; idle app clears all non-persistant alarms
	//			on restart (possibly same for other apps)
}

bool IdleAlarm::sendIdToApp(std::string remApp, const char *cmd, std::string sId) {

	// Build Command and send id
	std::string sCmd = "CLR";
	if (SSTR(cmd) == SSTR(IDLE_CMD_ALARM_TRG))
		sCmd = "TRG";
	else if (SSTR(cmd) == SSTR(IDLE_CMD_ALARM_IID))
		sCmd = "IID";
	//log.message(MSG_INFO + SSTR("Sending command " + sCmd + " with id [") + sId + "] to " + remApp + "\r\n");

	std::string alarmCommand = buildIPCcommand(IDLE_PARAM_TX_IID,sId);
	alarmCommand = buildIPCcommand(std::string(cmd,CMD_SIZE),alarmCommand);

	// Now send the command
	std::string ipcCmd = "send";
	int ipcRet = com_verifone_ipc::send(alarmCommand,remApp);
	// NOTE: We do not wait for a response

	if(ipcRet != com_verifone_ipc::IPC_SUCCESS){
		stringstream ss;
		ss << MSG_INFO << "sendIdToApp failed on ipc cmd " << ipcCmd << " to " << remApp
						<< ", errcode=" << ipcRet <<  endl;
		log.message(ss.str());
		return false;
	}
	return  true;
}

std::string getAlarmApp(const std::string alarmId) {
	return subString(alarmId.c_str(),".",".");
}

bool IdleAlarm::alarmClear(const std::string alarmId) {
   return alarmClear(appName, alarmId,true);
}

/**
 * Clear the alarm; returns true if matching alarm(s) was found and cleared
 * @param alarmId - if empty we clear ALL alarms set by THIS app
 */

/**
 * @param alarmAppName
 * @param alarmId - if empty we clear ALL alarms set by THIS app
 * @param mustDoRemote
 * @return
 */
bool IdleAlarm::alarmClear(const std::string alarmAppName, const std::string alarmId, bool mustDoRemote) {
	typedef stdStringMap ::iterator smapIt;
	typedef std::multimap<time_t,std::string> ::iterator timeIt;

	bool wasCleared = true;
	if (alarmId.empty()) {
		// Clear All alarms set by alarmAppName
		if (mustDoRemote) {
			// note: clearId may also be ""
			wasCleared = sendIdToApp(appMaster, IDLE_CMD_ALARM_CLR,"");
		}
		//std::string alarmAppName = getAlarmApp(alarmId);
		if (!alarmAppName.empty()) {
			for (timeIt it = timeAlarmMap.begin(); it != timeAlarmMap.end();) {
				if (alarmAppName == getAlarmApp(it->second)) {
					timeIt itt = it;
					itt++;
					timeAlarmMap.erase(it);
					it = itt;
				} else
					++it;
			}
			for (smapIt it = remKeyFor.begin(); it != remKeyFor.end(); ) {
				if (alarmAppName == getAlarmApp(it->second)) {
					smapIt itt = it;
					itt++;
					remKeyFor.erase(it);
					it = itt;
				} else
					++it;
			}
			for (smapIt it = locKeyFor.begin(); it != locKeyFor.end(); ) {
				if (alarmAppName == getAlarmApp(it->first)) {
					smapIt itt = it;
					itt++;
					locKeyFor.erase(it);
					it = itt;
				} else
					++it;
			}
			typedef std::map<std::string,stdStringMap>::iterator dataIt;
			for (dataIt it = alarmDataMap.begin(); it != alarmDataMap.end(); ) {
				if (alarmAppName == getAlarmApp(it->first)) {
					dataIt itt = it;
					itt++;
					alarmDataMap.erase(it);
					it = itt;
				} else
					++it;
			}

			if (isAlarmTriggerActive() && (alarmAppName == getAlarmApp(activeAlarmId)))
				activeAlarmId = "";
		}
	} else {
		// Clear only given alarm
		wasCleared = false;

		// First get locId and remKey
		std::string locId = "";
		std::string remKey = "";
		smapIt it = locKeyFor.find(alarmId);
		if (it == locKeyFor.end()) {
			// alarmId not found anywhere, now treat as if it was in format of originally provided local alarmId
			locId = getUniqueAlarmId(alarmAppName,alarmId);
			smapIt itr = remKeyFor.find(locId);
			if (itr != remKeyFor.end()) remKey = itr->second;
		} else {
			remKey = alarmId;
			locId = it->second;
		}
		if (remKey.empty()) remKey = alarmId;

		// Send remote clear if applicable
		if (!remKey.empty() && mustDoRemote) {
			wasCleared = sendIdToApp(appMaster, IDLE_CMD_ALARM_CLR,remKey);
		}

		// Now clear locally
		if (!remKey.empty() && locKeyFor.find(remKey) != locKeyFor.end())
			locKeyFor.erase(remKey);
		if (!locId.empty() && remKeyFor.find(locId) != remKeyFor.end())
			remKeyFor.erase(locId);
		if (!remKey.empty() && alarmDataMap.find(remKey) != alarmDataMap.end())
			alarmDataMap.erase(remKey);
		for (timeIt it = timeAlarmMap.begin(); it != timeAlarmMap.end(); it++) {
			if ((!remKey.empty() && it->second == remKey) || (!locId.empty() && (it->second == locId))) {
				timeAlarmMap.erase(it);
				wasCleared = true;
				break;
			}
		}
		if (isAlarmTriggerActive() && ((activeAlarmId == remKey) || (activeAlarmId == locId)))
			activeAlarmId = "";
		//log.message("---CLR: loc=" + locId + ", rem=" + remKey + ",alAct=" + activeAlarmId + "\n");
	}


	return wasCleared;
}

/**
 * Trigger the alarm with the given id - can be local or remote unique id.
 * @param alarmId
 * @param mustClear = true if not provided
 */
void IdleAlarm::alarmTrigger(const std::string alarmId, bool mustClear) {
	std::string remKey = alarmId;
	if (remKeyFor.find(alarmId) != remKeyFor.end()) {
		remKey = remKeyFor.find(alarmId)->second;
	}
	std::string alarmAppName = getAlarmApp(alarmId);
	activeAlarmId =  remKey;
	//log.message("Trigger on id " + alarmId + ",remKey=" + remKey + ",app=" + alarmAppName + (isMaster ? ",M" : ",L") + "\n");


	if (isMaster) {
		// On master we clear before the time because we do not want to repeat alarm if
		// app does not come back
		if (mustClear) alarmClear(alarmAppName, remKey, false);
		sendIdToApp(alarmAppName, IDLE_CMD_ALARM_TRG,remKey);

	} else {
		// On Local App we clear afterwards because we need to confirm to Master that
		// we are done

		stdStringMap alarmData;
		if (alarmDataMap.find(remKey) != alarmDataMap.end()) alarmData = alarmDataMap[remKey];

		// Find local id
		std::string locId = alarmId;
		if (locKeyFor.find(alarmId) != locKeyFor.end()) {
			locId = locKeyFor.find(alarmId)->second;
			if (locId.empty()) locId=alarmId;
		}
//		else {
//			// alarmId not found anywhere, now treat as if it was in format of originally provided local alarmId
//			locId = getUniqueAlarmId(alarmAppName,alarmId);
//			if (remKeyFor.find(locId) == remKeyFor.end()) {
//				locId = alarmId; // reset if not found
//			}
//		}

		// Call with local id if one was provided
		alarmAction(locId,alarmData);
		if (mustClear) alarmClear(remKey);
	}
}


/**
 * Check if command is an IPC IdleAlarm command (starting with TAG_IDLE) and process accordingly.
 * Can be used both in idleApp and in other apps - forms the state engine.
 *
 * Note: We assume that we are still connected and can send to received_from.
 * @param received_from
 * @param cmd
 * @return Empty string if not applicable, "Error <msg>" if process error, else the non-empty process
 *         result (typically the id or "Ok")
 */
std::string IdleAlarm::alarmProcessIPC(std::string received_from, std::string cmd) {
	const char *IDLECMDS[] = { "", "SET", "IID", "TRG", "CLR" };
	std::string idRes = "";
	if ((!received_from.empty()) /* && (received_from.compare(appMaster) != 0) */
		&& (cmd.length()>=3) && (cmd.substr(0,2).compare(TAG_IDLE) == 0)) {

		//log.message((MSG_INFO  " Got AlarmIPC:[") + SSTR((isMaster ? "M:" : "L:")) + IDLECMDS[cmd[2]] + ":"
		//		+ UtilsHexToString((const char*)cmd.c_str(), (unsigned int)cmd.length())+"]\r\n");

		std::string tagCmd = cmd.substr(0,3);
		std::string idleData;
		// Get data
		{
			BERTLV berTlv(cmd);
			berTlv.getValForTag(tagCmd,idleData); // read the data regardless of command.
		}
		BERTLV berTlv(idleData);

		if (tagCmd.compare(IDLE_CMD_ALARM_SET) == 0) {
			// Set the alarm locally (this part runs on master typically)
			std::string sTime;
			berTlv.getValForTag(IDLE_PARAM_TX_STIME,sTime);

			//Convert time
			time_t t;
			istringstream ( sTime ) >> t;

			idRes = alarmAddLocal(received_from, t);
			sendIdToApp(received_from,IDLE_CMD_ALARM_IID,idRes);

		} else if (tagCmd.compare(IDLE_CMD_ALARM_IID) == 0) {
			// Runs on slave, just return the IdleApp Id that was received
			std::string sId;
			berTlv.getValForTag(IDLE_PARAM_TX_IID,sId);
			if (sId.empty()) {
				idRes = "Error: Empty alarm id.";
			} else {
				idRes = sId;
			}
		} else if (tagCmd.compare(IDLE_CMD_ALARM_TRG) == 0) {
			// Trigger the alarm, runs on slave
			std::string sId;
			berTlv.getValForTag(IDLE_PARAM_TX_IID,sId);
			if (sId.empty()) {
				idRes = "Error: TRG - Empty alarm id.";
			} else {
				alarmTrigger(sId);
				idRes = "Ok";
			}
		} else if (tagCmd.compare(IDLE_CMD_ALARM_CLR) == 0) {
			// Clear the alarm, runs on Master
			std::string sId;
			berTlv.getValForTag(IDLE_PARAM_TX_IID,sId);
			alarmClear(received_from,sId,false);
			idRes = "Ok"; // we do not confirm a clear command
		}
	}

	// For debug purposes:
	// log.message(MSG_INFO + SSTR((isMaster ? " MasterAlarms:\n" : "LocalAlarms:\n")) + getAlarmList() + "\n");

	return idRes;
}

void IdleAlarm::alarmCheck() {
	//log.message("-- check alarm -- ");
	// Check for expired alarm and send trigger if necessary
	if (!timeAlarmMap.empty()) {
		// No active alarm
		std::multimap<time_t,std::string>::iterator it = timeAlarmMap.begin(); // will give the oldest (i.e. first to go) alarm;
		time_t tNow = time(NULL);
		//log.message("Now:" + SSTR(tNow) + ",First=" + SSTR(it->first) + ",Act=" + SSTR(isAlarmTriggerActive()));
		if (!isAlarmTriggerActive() && it->first <= tNow) {
			//log.message("\r\n");
			alarmTrigger(it->second,true); // will set activeAlarmId
		}
	}
	//log.message("\r\n");
}

// Used for debugging - returns a string that list all the alarms in readable format
std::string IdleAlarm::getAlarmList() {
	typedef std::multimap<time_t,std::string> ::iterator timeIt;
	std::string sAlarm = "";
	int nr = 0;
	if (timeAlarmMap.empty() && !locKeyFor.empty()) {
		for (stdStringMap::iterator it = locKeyFor.begin(); it != locKeyFor.end(); ++it) {
			std::string sId = it->first;
			std::string sLocId = it->second;
			if (sLocId == sId)
				sLocId = "";
			else
				sLocId = " (" + sLocId + ")";
			sAlarm += "    A" + SSTR(++nr) + ". [" + sId + "] = " + sLocId + "\n";
		}
	} else {
		for (timeIt it = timeAlarmMap.begin(); it != timeAlarmMap.end(); ++it) {
			time_t t = it->first;
			std::string sId = it->second;
			std::string sLocId = locKeyFor[sId];
			if (sLocId == sId)
				sLocId = "";
			else
				sLocId = " (" + sLocId + ")";
			char buf[64];
			strftime(buf, sizeof(buf), "%d.%m.%Y - %H:%M:%S", localtime(&t));
			sAlarm += "    A" + SSTR(++nr) + ". [" + sId + "] = " + SSTR(buf) + sLocId + "\n";
		}
	}
	return sAlarm;
}

bool IdleAlarm::isSameAlarm(std::string localName, std::string libName) {

	if (localName == libName)
		return true;
	else {
		std::string unId = getUniqueAlarmId(appName,localName);
		if (unId == libName)
			return true;
		else if (locKeyFor.find(libName) != locKeyFor.end()) {
			std::string locId = locKeyFor.find(libName)->second;
			if ((locId == localName) || (locId == unId))
				return true;
		}
	}
	return false;
}

}
