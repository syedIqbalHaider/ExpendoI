#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <iomanip>
#include <unistd.h>
#include <libipc/ipc.h>
#include "cstatus.h"
#include "libstatusver.h"
#include "CLog.h"

static CLog log(LIBSTATUS_NAME);
#define DEST_APP_NAME "STATUS"

#define IDLE_TIME_DELAY_US		100000

#define SSTR( x ) dynamic_cast< std::ostringstream & >( std::ostringstream() << std::dec << x ).str()
namespace com_verifone_status
{
	void CStatus::statusClose(void) {

		log.message(MSG_INFO "Close display\n");

		if(com_verifone_ipc::connect_to(DEST_APP_NAME) < 0){
			log.message(MSG_INFO "Failed to connect to STATUS\n");
			return;
		}

		std::string request = "CLOSE STATUS";
		if(com_verifone_ipc::send(request,DEST_APP_NAME) != com_verifone_ipc::IPC_SUCCESS){
			log.message(MSG_INFO "statusClose ipc send failed\n");
			return;
		}

		std::string response, ipcFrom;
		while(1){
			if(com_verifone_ipc::receive(response,DEST_APP_NAME,ipcFrom)==com_verifone_ipc::IPC_SUCCESS){
				if(response.compare("OK") != 0)
					log.message(MSG_INFO "Invalid response received on CLOSE STATUS ["+response+"]\n");
				break;
			}

			usleep(IDLE_TIME_DELAY_US);
		}

		com_verifone_ipc::disconnect(DEST_APP_NAME);

		return;
	}

	void CStatus::setStatus(STATUS code) {
		log.message(MSG_INFO "Set Status "+SSTR(code)+"\n");

		if(com_verifone_ipc::connect_to(DEST_APP_NAME) < 0){
			log.message(MSG_INFO "Failed to connect to STATUS\n");
			return;
		}

		std::string request = "SET STATUS:"+SSTR(code);
		if(com_verifone_ipc::send(request,DEST_APP_NAME) != com_verifone_ipc::IPC_SUCCESS){
			log.message(MSG_INFO "CStatus::setStatus ipc send failed\n");
			return;
		}

		std::string response, ipcFrom;

		while(1){
			if(com_verifone_ipc::receive(response,DEST_APP_NAME,ipcFrom)==com_verifone_ipc::IPC_SUCCESS){
				if(response.compare("OK") != 0)
					log.message(MSG_INFO "Invalid response received on SET STATUS ["+response+"]\n");
				break;
			}

			usleep(IDLE_TIME_DELAY_US);
		}

		com_verifone_ipc::disconnect(DEST_APP_NAME);

		return;
	}

	STATUS CStatus::getStatus(void) {
		log.message(MSG_INFO "Get Status\n");

		if(com_verifone_ipc::connect_to(DEST_APP_NAME) < 0){
			log.message(MSG_INFO "Failed to connect to STATUS\n");
			return IPC_ERROR;
		}

		std::string request = "GET STATUS";
		if(com_verifone_ipc::send(request,DEST_APP_NAME) != com_verifone_ipc::IPC_SUCCESS){
			log.message(MSG_INFO "CStatus::getStatus ipc send failed\n");
			return TERMINAL_OFFLINE;
		}

		std::string response, ipcFrom;

		while(1){
			if(com_verifone_ipc::receive(response, DEST_APP_NAME, ipcFrom)==com_verifone_ipc::IPC_SUCCESS){
				break;
			}

			usleep(IDLE_TIME_DELAY_US);
		}

		com_verifone_ipc::disconnect(DEST_APP_NAME);

		return (STATUS)atoi(response.c_str());
	}

	OPERATION_STATUS CStatus::getOperation(void){
		log.message(MSG_INFO "Get Operation\n");

		if(com_verifone_ipc::connect_to(DEST_APP_NAME) < 0){
			log.message(MSG_INFO "Failed to connect to STATUS\n");
			return OP_STAT_IDLE;
		}

		std::string request = "GET OPERATION";
		if(com_verifone_ipc::send(request,DEST_APP_NAME) != com_verifone_ipc::IPC_SUCCESS){
			log.message(MSG_INFO "CStatus::getOperation ipc send failed\n");
			return OP_STAT_IDLE;
		}

		std::string response, ipcFrom;

		while(1){
			if(com_verifone_ipc::receive(response,DEST_APP_NAME,ipcFrom)==com_verifone_ipc::IPC_SUCCESS){
				break;
			}

			usleep(IDLE_TIME_DELAY_US);
		}

		com_verifone_ipc::disconnect(DEST_APP_NAME);

		return (OPERATION_STATUS)atoi(response.c_str());
	}
}

