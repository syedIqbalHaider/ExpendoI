#include <sys/timeb.h>
#include <libipc/ipc.h>
#include <string>
#include <unistd.h>

#include "libgoods.h"

#define GOODS_TASK		"Goods"

//using namespace std;

namespace GoodsApp{

int Connect(void)
{
	return com_verifone_ipc::connect_to(GOODS_TASK);
}

int SendCommand(GOODS_CMD cmd, string &response, long timeout_secs)
{
	string ipcFrom;
	struct timeb start_tp;
	struct timeb  current_tp;

	ftime(&start_tp);

	com_verifone_ipc::send("Go",GOODS_TASK);

	if(timeout_secs == 0)
		return 0;

	while(1){
		if(com_verifone_ipc::check_pending(GOODS_TASK)==com_verifone_ipc::IPC_SUCCESS){
			if(com_verifone_ipc::receive(response,GOODS_TASK,ipcFrom)==com_verifone_ipc::IPC_SUCCESS){
				break;
			}
		}

		// Check the timeout value
		ftime(&current_tp);
		if(timeout_secs <= current_tp.time - start_tp.time)
			return 0;

	}

	return (int)response.length();
}

int WaitResponse(string &response, long timeout_secs)
{
	string ipcFrom;
	struct timeb start_tp;
	struct timeb  current_tp;

	ftime(&start_tp);

	while(1){
		if(com_verifone_ipc::check_pending(GOODS_TASK)==com_verifone_ipc::IPC_SUCCESS){
			if((com_verifone_ipc::receive(response,GOODS_TASK,ipcFrom)==com_verifone_ipc::IPC_SUCCESS)){
				break;
			}
		}

		// Check the timeout value
		ftime(&current_tp);
		if(timeout_secs <= current_tp.time - start_tp.time)
			return 0;
	}

	return (int)response.length();
}

string GetId(void)
{
	return string(GOODS_TASK);
}

}
