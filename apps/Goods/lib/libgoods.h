#ifndef _GOODS_H_
#define _GOODS_H_

#include <string>

//using namespace std;
using std::string;

namespace GoodsApp{
	typedef enum{
		GOODS_TX_RQST
	}GOODS_CMD;

	int Connect(void);
	int SendCommand(GOODS_CMD cmd, string &response, long timeout_secs);
	int WaitResponse(string &response, long timeout_secs);
	string GetId(void);

}
#endif // _GOODS_H_
