#include <string>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <libtui/tui.h>
#include <iostream>
#include <sys/types.h>
#include <sys/timeb.h>
#include <libda/cterminalconfig.h>
#include <libda/cversions.h>
#include <libda/cversionsfile.h>
#include <libda/cconmanager.h>
#include <gui/gui.h>
#include <gui/jsobject.h>
#include <libipc/ipc.h>
#include <libcpa.h>

#include "CLog.h"
#include "Goodsver.h"

#define GOODS_TASK		"Goods"

static CLog log(GOODS_NAME);

//using namespace std;
using namespace vfigui;
using namespace za_co_verifone_tui;

pthread_t tVersion;

void *update_versions(void*)
{
	com_verifone_versions::CVersions versions;
	com_verifone_versionsfile::CVersionsFile vfile;

	log.message(MSG_INFO "Goods start writing versions\r\n");

	vfile.setName("");
	vfile.setVersion(GOODS_VERSION);

	// Keep trying every second, until we succeed
	while(1){
		if(versions.addFile(GOODS_NAME, vfile) == DB_OK)
			break;

		sleep(1);
	}

	log.message(MSG_INFO "Goods versions complete\r\n");

	return NULL;
}

int get_selection(void)
{
	string tmpl;
	int iret, rqst_id;

	TuiTemplate("select", tmpl);

	rqst_id = uiDisplayAsync("bgimage",tmpl);

	iret = uiDisplayWait(rqst_id, 30000);

	if(iret == UI_ERR_TIMEOUT){
		return 0;
	}

	return iret;
}

void uiMain(int argc, char *argv[])
{
	(void)argc;			// Suppresses -> warning: unused parameter
	(void)argv;			// Suppresses -> warning: unused parameter

	log.message(MSG_INFO "Goods n Services starting...\r\n");

//	pthread_create(&tVersion,NULL,&update_versions,NULL);

	int ret = TuiInit("Goods");

	if(ret < 0){
		char buffer[10];
		sprintf(buffer, "%d", ret);
		log.message(MSG_INFO "TuiInit returned "+string(buffer)+"\r\n");
	}

	uiSetPropertyString (UI_PROP_RESOURCE_PATH, "flash/Goodsrsc/");
	uiReadConfig();


	com_verifone_ipc::init(string(GOODS_TASK));

//	CpaApp::Connect();

	string ipcBuffer;
	string ipcFrom;

	while(1){
		while(com_verifone_ipc::check_pending(string(""))==com_verifone_ipc::IPC_SUCCESS){
			if(com_verifone_ipc::receive(ipcBuffer,string(""),ipcFrom)==com_verifone_ipc::IPC_SUCCESS){
				string tmpl;
				int iret;

				log.message(MSG_INFO "Got message ["+ipcBuffer+"]\r\n");

				int select = get_selection();

				if(select == 1){
					TuiTemplate("goods", tmpl);

					log.message(MSG_INFO + tmpl);

					vector<string> value(1);
					value[0]="0";

					iret = uiInput("input",value,tmpl);
					if(iret == UI_ERR_OK) {
						log.message(MSG_INFO "Amount " + value[0] + " was entered\r\n");

						map<string, string> txresult;

						int pmnt_ret = CpaApp::DoPayment(value[0], txresult);

						switch(pmnt_ret){
							case PMT_SUCCESS:
								log.message(MSG_INFO "DoPayment returned PMT_SUCCESS\n");
								break;
							case PMT_CANCELLED:
								log.message(MSG_INFO "DoPayment returned PMT_CANCELLED\n");
								break;
							case PMT_DECLINED:
								log.message(MSG_INFO "DoPayment returned PMT_DECLINED\n");
								break;
							case PMT_PROCESSING_ERROR:
								log.message(MSG_INFO "DoPayment returned PMT_PROCESSING_ERROR\n");
								break;
						}

#ifdef DEBUG
						for(map<string,string>::iterator it=txresult.begin(); it!=txresult.end(); ++it){
							log.message(MSG_INFO+it->first+"->"+it->second+"\n");
						}
#endif // DEBUG
					}
				}else if(select == 2){
					int pmnt_ret = CpaApp::DoPosReversal();

					switch(pmnt_ret){
						case PMT_SUCCESS:
							log.message(MSG_INFO "DoPayment returned PMT_SUCCESS\n");
							break;
						case PMT_CANCELLED:
							log.message(MSG_INFO "DoPayment returned PMT_CANCELLED\n");
							break;
						case PMT_DECLINED:
							log.message(MSG_INFO "DoPayment returned PMT_DECLINED\n");
							break;
						case PMT_PROCESSING_ERROR:
							log.message(MSG_INFO "DoPayment returned PMT_PROCESSING_ERROR\n");
							break;
					}
				}

				com_verifone_ipc::send("Finished",ipcFrom);
				log.message(MSG_INFO "Sent reply to ["+ipcFrom+"]\r\n");
			}
		}

		usleep(100000);		// Sleep for 100 milli seconds
	}
}

int main(int argc, char *argv[])
{
	uiMain(argc, argv);

	return 0;
}
