#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <libpml/pml.h>
#include <libpml/pml_abstracted_api.h>
#include <libpml/pml_port.h>
#include <libipc/ipc.h>
#include <iostream>

#define MY_TASK_NAME "IPCTESTAPP"
#define OTHER_TASK_NAME "CPAM"

using namespace std;

bool iThreadFinished = false;
bool iThreadRunning = false;
pthread_t myThreadId;
pthread_t myPamSimThreadId;
pthread_t myPmlThreadId;

bool pamsimthreadexited=true;
bool pamsimthreadrunning=false;
static void *PAMSimThread(void *parm)
{
	int iRc;

	iRc=com_verifone_ipc::init(string("PAMTHREAD"));

	if(iRc<0){
		cout << "IPC init failed" << std::endl;
		return NULL;
	}

	string ipcBuffer;
	string ipcFrom;
	string ipcFromPos;

	pamsimthreadrunning = true;
	pamsimthreadexited = false;

	while(pamsimthreadrunning){
		if(com_verifone_ipc::check_pending(string(""))==com_verifone_ipc::IPC_SUCCESS){
			if(com_verifone_ipc::receive(ipcBuffer,string(""),ipcFrom)==com_verifone_ipc::IPC_SUCCESS){
				cout << ipcFrom << " Rx: [" << ipcBuffer << "]" << endl;
				com_verifone_ipc::send("<--Pong",ipcFrom);
			}
		}
	}

	com_verifone_ipc::deinit(0);

	pamsimthreadexited = true;
	pamsimthreadrunning = false;

	return NULL;
}

static void *TestThread(void *parm)
{
	int iRc;

	iRc=com_verifone_ipc::init(string("THREAD"));

	if(iRc<0){
		cout << "IPC init failed" << std::endl;
		return NULL;
	}

	string ipcBuffer;
	string ipcFrom;
	string ipcFromPos;

	iThreadRunning = true;
	iThreadFinished = false;

	while(iThreadRunning){
		if(com_verifone_ipc::check_pending(string(""))==com_verifone_ipc::IPC_SUCCESS){
			if(com_verifone_ipc::receive(ipcBuffer,string(""),ipcFrom)==com_verifone_ipc::IPC_SUCCESS){
				cout << ipcFrom << " Rx: [" << ipcBuffer << "]" << endl;
				com_verifone_ipc::send("<--Pong",ipcFrom);
			}
		}
	}

	com_verifone_ipc::disconnect(string("THREAD"));
	com_verifone_ipc::deinit(0);

	iThreadFinished = true;
	iThreadRunning = false;

	return NULL;
}

static void *TestPmlThread(void *parm)
{
	com_verifone_pml::eventset_t tPmlEvents;
	int iEventCount;
	int iRc;
	char cwd[256];
	int hEvent;
	com_verifone_pml::event_item pmlUserEvent;

	getcwd(cwd, sizeof(cwd));

	// set working directory
	com_verifone_pml::set_working_directory(com_verifone_pml::getRAMRoot().c_str());

	// init pml
	iRc=com_verifone_pml::pml_init("PMLTHREAD");

	if(iRc<0) {
		cout << "myThread failed to init pml"<<endl;
		return(NULL);
	}

	// init event handler
	hEvent=com_verifone_pml::event_open();

	if(hEvent<0) {
		cout << "myThread failed to open pml"<<endl;
		return(NULL);
	}

	// add user1 event
	pmlUserEvent.event_id=com_verifone_pml::events::user1;
	pmlUserEvent.evt.user.flags=1;
	pmlUserEvent.evt.user.ext_flags=1;
	com_verifone_pml::event_ctl(hEvent,com_verifone_pml::ctl::ADD,pmlUserEvent);

	// init ipc
	iRc=com_verifone_ipc::init("PMLTHREAD");

	if(iRc<0) {
		cout << "myThread failed to init ipc"<<endl;
		return(NULL);
	}

	// add ipc event
	com_verifone_pml::event_ctl(hEvent,com_verifone_pml::ctl::ADD,com_verifone_ipc::get_events_handler());
	chdir(cwd);

	// wait for pam to be available
	while(!com_verifone_ipc::is_task_available("PAMTHREAD")) sleep(1);

	// connect to pam
	iRc=com_verifone_ipc::connect_to(string("PAMTHREAD"));

	if(iRc!=0) {
		cout << "myThread failed to connect ipc"<<endl;
		return(NULL);
	}

	// indicate successful thread startup
	iThreadRunning=true;

	// main loop
	while(iThreadRunning) {
		iEventCount=com_verifone_pml::event_wait(hEvent,tPmlEvents);

		if(iEventCount>0||tPmlEvents.size()>0) {
			while(!tPmlEvents.empty()) {
				string sInBuffer,sFrom;

				com_verifone_pml::event_item tEvent=tPmlEvents.back();

				tPmlEvents.pop_back();

				if(tEvent.event_id==com_verifone_pml::events::ipc) {
					sFrom.clear();

					while(com_verifone_ipc::check_pending(string(""))==com_verifone_ipc::IPC_SUCCESS) {
						sInBuffer.clear();
						com_verifone_ipc::receive(sInBuffer,string(""),sFrom);

						cout << sFrom << " Rx: [" << sInBuffer << "]" << endl;

						com_verifone_ipc::send("<--Pong",sFrom);
					}
				}
			}
		}

		usleep(10000);
	}

	if(hEvent>=0)
		com_verifone_pml::event_close(hEvent);

	com_verifone_pml::pml_deinit("PMLTHREAD");
	com_verifone_ipc::disconnect(string("PAMTHREAD"));
	com_verifone_ipc::deinit(0);

	iThreadFinished = true;

	cout << "PMLTHREAD exiting"<<endl;

	return(NULL);
}

int send()
{
	com_verifone_pml::eventset_t tPmlEvents;
	string sFrom;
	string sInBuffer;
	int iEventCount;
	int hEvent;
	int iRc;

	string ipcBuffer;
	string ipcFrom;
	string ipcFromPos;

	iRc=com_verifone_ipc::init(string("APP"));

	pthread_create(&myPmlThreadId,NULL,TestPmlThread,NULL);

	while(!iThreadRunning) usleep(10000);

	cout << "Thread running" << endl;

	const char *destination = "PMLTHREAD";

	iRc = com_verifone_ipc::connect_to(destination);

	if(iRc != 0){
		cout << "Failed to connect to " << string(destination) << endl;
		return -1;
	}

	for(int i=0; i<10; i++){
		com_verifone_ipc::send("Ping-->",destination);

		while(1){
			if(com_verifone_ipc::check_pending(string(""))==com_verifone_ipc::IPC_SUCCESS){
				if(com_verifone_ipc::receive(ipcBuffer,string(""),ipcFrom)==com_verifone_ipc::IPC_SUCCESS){
					cout << ipcFrom << " Rx: [" << ipcBuffer << "]" << endl;
					break;
				}
			}
		}
	}

	cout << "DBG" << endl;

	iThreadRunning = false;
	com_verifone_ipc::send("",destination);
	while(!iThreadFinished) usleep(10000);

	cout << "Thread exited" << endl;

	com_verifone_ipc::deinit(0);

	return 0;
}

int main()
{
	pthread_create(&myPamSimThreadId,NULL,PAMSimThread,NULL);
	while(!pamsimthreadrunning) usleep(10000);

	for(int i=0; i<50; i++){
		cout << "--------------------------- Loop no " << i << " -------------------" << std::endl;
		send();

		usleep(10000);
	}

	pamsimthreadrunning = false;
	while(!pamsimthreadexited) usleep(10000);
/*
	// set working directory (VOS only)
	com_verifone_pml::set_working_directory(com_verifone_pml::getRAMRoot().c_str());

	for(int i=0; i<50;i++){
		cout << "--------------------------- Loop no " << i << " -------------------" << std::endl;
		// init pml
		iRc=com_verifone_pml::pml_init(MY_TASK_NAME);
		if(iRc<0){
			cout << "pml init failed" << std::endl;
			return(-1);
		}

		cout << "Step 1" << std::endl;

		// init ipc
		iRc=com_verifone_ipc::init(string(MY_TASK_NAME));
		if(iRc<0){
			cout << "IPC init failed" << std::endl;
			return(-1);
		}

		cout << "Step 2" << std::endl;

		// init event handler
		hEvent=com_verifone_pml::event_open();
		if(hEvent<0){
			cout << "pml event_open failed" << std::endl;
			return(-1);
		}

		cout << "InitComplete" << std::endl;

		com_verifone_pml::event_close(hEvent);
		com_verifone_pml::pml_deinit(MY_TASK_NAME);
//		com_verifone_ipc::disconnect(string(PAM_SERVICE_TASK_NAME));
		com_verifone_ipc::deinit(0);

		cout << "De-initComplete" << std::endl;
	}
*/
	return(0);
}
