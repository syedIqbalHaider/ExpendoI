#include <sys/timeb.h>
#include <libipc/ipc.h>
#include <string>
#include <iomanip>
#include <map>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include "libcpa.h"
#include "bertlv.h"
#include "utils.h"
#include "CLog.h"
#include "libcpaver.h"
#include "../include/CPAver.h"
#include "CPATags.h"

//using namespace std;
using namespace Utils;
//using namespace CPACommand;
using namespace CPATags;
using namespace za_co_verifone_bertlv;

static CLog log(LIBCPA_NAME);
#define POS_INT_NAME		"POSINT"
/*
namespace CPACommand
{
	// Data keys
	#define TX_AMOUNT		"TxAmount"				// Number of cents, numeric only - Maximum length of 12

	// Command id's
	typedef enum{
		CPA_TX_RQST,
		CPA_POS_TX_RQST,
		CPA_PURCHASE_RQST,
		CPA_BANKING_RQST,
		CPA_REPORT_CURRENT_BATCH,
		CPA_REPORT_PREVIOUS_BATCH
	}CPA_COMMAND;
}

using namespace CPACommand;
*/
namespace CpaApp{

int parse_response(string response, map<string, string> &txresp);

int DoMenu(std::string menuCode)
{
	log.message(MSG_INFO "DoMenu:" + menuCode + "\n");

	int pmlret = com_verifone_ipc::connect_to(CPA_NAME);

	if(pmlret < 0){
		stringstream ss;
		ss << MSG_INFO << "DoMenu exiting due to pml error " << pmlret <<  endl;
		log.message(ss.str());
	}

	string ipcFrom;
	string response;

	com_verifone_ipc::send("Menu:" + menuCode,CPA_NAME);

	while(1){
		if(com_verifone_ipc::receive(response,CPA_NAME,ipcFrom)==com_verifone_ipc::IPC_SUCCESS){
			log.message(MSG_INFO "libcpa - DoMenu(): response ["+response+"]\n");
			break;
		}
		usleep(100000);		// Sleep for 100 milli seconds
	}

	com_verifone_ipc::disconnect(CPA_NAME);

	if (response.compare(0,14,"Finished:Menu=") == 0) {
		response = response.substr(14);
	}

	return UtilsStringToInt(response);
}

int DoSetup(void)
{
	log.message(MSG_INFO "DoSetup\n");

	int pmlret;

	while((pmlret=com_verifone_ipc::connect_to(CPA_NAME))<0)
	{
			stringstream ss;
			ss << MSG_INFO << "DoSetup exiting due to pml error " << pmlret <<  endl;
			log.message(ss.str());
			sleep(1); //sleep for 1 sec
	}

	log.message(MSG_INFO "DoSetup Connected to CPA \n");

	string ipcFrom;
	string response;

	com_verifone_ipc::send("Setup",CPA_NAME);

	while(1){
		if(com_verifone_ipc::receive(response,CPA_NAME,ipcFrom)==com_verifone_ipc::IPC_SUCCESS){
			log.message(MSG_INFO "libcpa - DoSetup(): response ["+response+"]\n");
			break;
		}

		usleep(100000);		// Sleep for 100 milli seconds
	}

	com_verifone_ipc::disconnect(CPA_NAME);
	if (response.compare(0,15,"Finished:Setup=") == 0) {
		response = response.substr(15);
	}

	return UtilsStringToInt(response);
}

int DoPayment(string amount)
{
	map<string, string> notused;
	return DoPayment(amount, notused);
}

int DoPayment(string amount, map<string, string> &txresult)
{
	log.message(MSG_INFO "DoPayment\n");

	if(com_verifone_ipc::connect_to(CPA_NAME) < 0){
		log.message(MSG_INFO "Failed to connect to CPA\n");
		return PMT_PROCESSING_ERROR;
	}

	char bcd[12];
	int len;
	memset(bcd, 0, sizeof(bcd));

	if((len=UtilStringToBCD(amount, bcd)) == 0)
		return PMT_PROCESSING_ERROR;

	string txcmd, txdata;
	BERTLV tlv;
	tlv.addTag(CMD_PARAM_TX_AMOUNT, string(bcd, len));
	txdata.append(tlv.getStringData());

	BERTLV cpaTlv;
	txcmd = string(CPA_CMD_PURCHASE_RQST, CMD_SIZE);
	cpaTlv.addTag(txcmd, txdata);

	com_verifone_ipc::send(cpaTlv.getStringData(),CPA_NAME);

	string response, ipcFrom;
	while(1){
		if(com_verifone_ipc::receive(response,CPA_NAME,ipcFrom)==com_verifone_ipc::IPC_SUCCESS){
			break;
		}

		usleep(100000);		// Sleep for 100 milli seconds
	}

	com_verifone_ipc::disconnect(CPA_NAME);

	log.message(MSG_INFO "Received ["+response+"]\n");

	return parse_response(response, txresult);
}

int DoReport(CPA_REPORT report)
{
	BERTLV cmdtlv;

	switch(report)
	{
		case REPORT_CURRENT_BATCH:
			cmdtlv.addTag(string(CPA_CMD_REP_CURR_BATCH, CMD_SIZE), "");
			break;
		case REPORT_PREVIOUS_BATCH:
			cmdtlv.addTag(string(CPA_CMD_REP_PREV_BATCH, CMD_SIZE), "");
			break;
		default:
			log.message(MSG_INFO "Invalid command parameter\n");
			return PMT_PROCESSING_ERROR;

	}

	if(com_verifone_ipc::connect_to(CPA_NAME) < 0){
		log.message(MSG_INFO "Failed to connect to CPA\n");
		return PMT_PROCESSING_ERROR;
	}

	com_verifone_ipc::send(cmdtlv.getStringData(),CPA_NAME);

	string response, ipcFrom;
	while(1){
		if(com_verifone_ipc::receive(response,CPA_NAME,ipcFrom)==com_verifone_ipc::IPC_SUCCESS){
			break;
		}

		usleep(100000);		// Sleep for 100 milli seconds
	}

	com_verifone_ipc::disconnect(CPA_NAME);

	return 0;
}

int DoPosGetCPAStatus(void) {

	//log.message(MSG_INFO "DoPosGetCPAStatus\n");
	if(com_verifone_ipc::connect_to(POS_INT_NAME) < 0){
		log.message(MSG_INFO "Failed to connect to CPA\n");
		return PMT_PROCESSING_ERROR;
	}

	string txdata;

	// Construct command
	BERTLV cpaTlv;
	cpaTlv.addTag(string(CPA_CMD_POS_STATUS_RQST, CMD_SIZE), txdata);

	com_verifone_ipc::send(cpaTlv.getStringData(),POS_INT_NAME);

	string response, ipcFrom;
	while(1){
		if(com_verifone_ipc::receive(response,POS_INT_NAME,ipcFrom)==com_verifone_ipc::IPC_SUCCESS){
			break;
		}

		usleep(100000);		// Sleep for 100 milli seconds
	}

	com_verifone_ipc::disconnect(POS_INT_NAME);

	if (response.compare(0,7,"Status:") == 0) {
		response = response.substr(7);
	}

	return UtilsStringToInt(response);
}

int DoPosCancelWaitingForCardInput(void) {

	if(com_verifone_ipc::connect_to(POS_INT_NAME) < 0){
		log.message(MSG_INFO "Failed to connect to CPA\n");
		return PMT_PROCESSING_ERROR;
	}

	string txdata;

	// Construct command
	BERTLV cpaTlv;
	cpaTlv.addTag(string(CPA_CMD_POS_CANCEL_RQST, CMD_SIZE), txdata);

	com_verifone_ipc::send(cpaTlv.getStringData(),POS_INT_NAME);

	string response, ipcFrom;
	while(1){
		if(com_verifone_ipc::receive(response,POS_INT_NAME,ipcFrom)==com_verifone_ipc::IPC_SUCCESS){
			break;
		}

		usleep(100000);		// Sleep for 100 milli seconds
	}

	com_verifone_ipc::disconnect(POS_INT_NAME);

	return PMT_SUCCESS;
}


int DoPosOnIdleScreen(void) {

	if(com_verifone_ipc::connect_to(POS_INT_NAME) < 0){
		log.message(MSG_INFO "Failed to connect to CPA\n");
		return PMT_PROCESSING_ERROR;
	}

	string txdata;

	// Construct command
	BERTLV cpaTlv;
	cpaTlv.addTag(string(CPA_CMD_POS_RETURN_IDLE, CMD_SIZE), txdata);

	com_verifone_ipc::send(cpaTlv.getStringData(),POS_INT_NAME);

	string response, ipcFrom;
	while(1){
		if(com_verifone_ipc::receive(response,POS_INT_NAME,ipcFrom)==com_verifone_ipc::IPC_SUCCESS){
			break;
		}

		usleep(100000);		// Sleep for 100 milli seconds
	}

	com_verifone_ipc::disconnect(POS_INT_NAME);

	return PMT_SUCCESS;
}



int DoPosClearReversal(void) {

	if(com_verifone_ipc::connect_to(POS_INT_NAME) < 0){
		log.message(MSG_INFO "Failed to connect to CPA\n");
		return PMT_PROCESSING_ERROR;
	}

	string txdata;

	// Construct command
	BERTLV cpaTlv;
	cpaTlv.addTag(string(CPA_CMD_POS_CLEAR_REVERSAL, CMD_SIZE), txdata);

	com_verifone_ipc::send(cpaTlv.getStringData(),POS_INT_NAME);

	string response, ipcFrom;
	while(1){
		if(com_verifone_ipc::receive(response,POS_INT_NAME,ipcFrom)==com_verifone_ipc::IPC_SUCCESS){
			break;
		}

		usleep(100000);		// Sleep for 100 milli seconds
	}

	com_verifone_ipc::disconnect(POS_INT_NAME);

	return PMT_SUCCESS;
}

int DoPosBanking(void) {

	if(com_verifone_ipc::connect_to(POS_INT_NAME) < 0){
		log.message(MSG_INFO "Failed to connect to CPA\n");
		return PMT_PROCESSING_ERROR;
	}

	string txdata;

	// Construct command
	BERTLV cpaTlv;
	cpaTlv.addTag(string(CPA_CMD_POS_BANKING_RQST, CMD_SIZE), txdata);

	com_verifone_ipc::send(cpaTlv.getStringData(),POS_INT_NAME);

	string response, ipcFrom;
	while(1){
		if(com_verifone_ipc::receive(response,POS_INT_NAME,ipcFrom)==com_verifone_ipc::IPC_SUCCESS){
			break;
		}

		usleep(100000);		// Sleep for 100 milli seconds
	}

	com_verifone_ipc::disconnect(POS_INT_NAME);

	if (response.compare(0,7,"Result:") == 0) {
			response = response.substr(7);
	}

	if (response.compare("0") == 0)
		return PMT_SUCCESS;
	else
		return PMT_PROCESSING_ERROR;
}

int DoPosParameters(void) {

	if(com_verifone_ipc::connect_to(POS_INT_NAME) < 0){
		log.message(MSG_INFO "Failed to connect to CPA\n");
		return PMT_PROCESSING_ERROR;
	}

	string txdata;

	// Construct command
	BERTLV cpaTlv;
	cpaTlv.addTag(string(CPA_CMD_POS_PARAMETER_RQST, CMD_SIZE), txdata);

	com_verifone_ipc::send(cpaTlv.getStringData(),POS_INT_NAME);

	string response, ipcFrom;
	while(1){
		if(com_verifone_ipc::receive(response,POS_INT_NAME,ipcFrom)==com_verifone_ipc::IPC_SUCCESS){
			break;
		}

		usleep(100000);		// Sleep for 100 milli seconds
	}

	com_verifone_ipc::disconnect(POS_INT_NAME);

	if (response.compare(0,7,"Result:") == 0) {
		response = response.substr(7);
	}

	if (response.compare("0") == 0)
		return PMT_SUCCESS;
	else
		return PMT_PROCESSING_ERROR;
}

int DoPosReversal(void) {

	if(com_verifone_ipc::connect_to(POS_INT_NAME) < 0){
		log.message(MSG_INFO "Failed to connect to CPA\n");
		return PMT_PROCESSING_ERROR;
	}

	string txdata;

	// Construct command
	BERTLV cpaTlv;
	cpaTlv.addTag(string(CPA_CMD_POS_REVERSAL_RQST, CMD_SIZE), txdata);

	com_verifone_ipc::send(cpaTlv.getStringData(),POS_INT_NAME);

	string response, ipcFrom;
	while(1){
		if(com_verifone_ipc::receive(response,POS_INT_NAME,ipcFrom)==com_verifone_ipc::IPC_SUCCESS){
			break;
		}

		usleep(100000);		// Sleep for 100 milli seconds
	}

	com_verifone_ipc::disconnect(POS_INT_NAME);

	if (response.compare(0,7,"Result:") == 0) {
		response = response.substr(7);
	}

	if (response.compare("0") == 0)
		return PMT_SUCCESS;
	else
		return PMT_PROCESSING_ERROR;
}

int DoPosPayment(string amount, std::string tx_type, string description, string transaction_data,
		bool hCommandEnabled, map<string, string> &txresult)
{
	log.message(MSG_INFO "DoPosPayment\n");

	if(com_verifone_ipc::connect_to(POS_INT_NAME) < 0){
		log.message(MSG_INFO "Failed to connect to CPA\n");
		return PMT_PROCESSING_ERROR;
	}

	char bcd[12];
	int len;
	memset(bcd, 0, sizeof(bcd));

	if((len=UtilStringToBCD(amount, bcd)) == 0)
		return PMT_PROCESSING_ERROR;

	string txdata;

	// Add amount
	BERTLV tlv;
	tlv.addTag(CMD_PARAM_TX_AMOUNT, string(bcd, len));
	txdata.append(tlv.getStringData());

// Add Transaction Currency
	BERTLV crTlv;
	crTlv.addTag(CMD_PARAM_TX_CURRENCY, description.c_str());
	txdata.append(crTlv.getStringData());

	// Add Transaction type
	BERTLV txTlv;
	txTlv.addTag(CMD_PARAM_TX_TYPE, tx_type);
	txdata.append(txTlv.getStringData());

	// Add HCommand enabled
	BERTLV hcTlv;
	hcTlv.addTag(CMD_PARAM_TX_H_COMMAND_ENABLED, hCommandEnabled?"1":"0");
	txdata.append(hcTlv.getStringData());

	// Add tlv's
	txdata.append(transaction_data);

	// Construct command
	BERTLV cpaTlv;
	cpaTlv.addTag(string(CPA_CMD_POS_TX_RQST, CMD_SIZE), txdata);
	com_verifone_ipc::send(cpaTlv.getStringData(),POS_INT_NAME);

	string response, ipcFrom;
	while(1){
		if(com_verifone_ipc::receive(response,POS_INT_NAME,ipcFrom)==com_verifone_ipc::IPC_SUCCESS){
			break;
		}

		usleep(100000);		// Sleep for 100 milli seconds
	}

	com_verifone_ipc::disconnect(POS_INT_NAME);

	log.message(MSG_INFO "Received ["+response+"]\n");

	return parse_response(response, txresult);
}

int DoPosVoid(string amount, std::string tx_type, string description, string ecr, map<string, string> &txresult)
{
	log.message(MSG_INFO "DoPosVoid\n");

	if(com_verifone_ipc::connect_to(POS_INT_NAME) < 0){
		log.message(MSG_INFO "Failed to connect to CPA\n");
		return PMT_PROCESSING_ERROR;
	}

	char bcd[12], bcdEcr[4]={0};
	int len;
	memset(bcd, 0, sizeof(bcd));

	if((len=UtilStringToBCD(amount, bcd)) == 0)
		return PMT_PROCESSING_ERROR;

	if(UtilStringToBCD(ecr, bcdEcr) == 0)
		return PMT_PROCESSING_ERROR;

	string txdata;

	// Add amount
	BERTLV tlv;
	tlv.addTag(CMD_PARAM_TX_AMOUNT, string(bcd, len));
	txdata.append(tlv.getStringData());

	// Add Transaction Currency
	BERTLV crTlv;
	crTlv.addTag(CMD_PARAM_TX_CURRENCY, description.c_str());
	txdata.append(crTlv.getStringData());

	// Add Transaction type
	BERTLV txTlv;
	txTlv.addTag(CMD_PARAM_TX_TYPE, tx_type);
	txdata.append(txTlv.getStringData());

	// Add Transaction type
	BERTLV ecrTlv;
	ecrTlv.addTag(CMD_PARAM_ECR_NUMBER, string(bcdEcr, 4));
	txdata.append(ecrTlv.getStringData());

	// Construct command
	BERTLV cpaTlv;
	cpaTlv.addTag(string(CPA_CMD_POS_VOID_RQST, CMD_SIZE), txdata);

	com_verifone_ipc::send(cpaTlv.getStringData(),POS_INT_NAME);

	string response, ipcFrom;
	while(1){
		if(com_verifone_ipc::receive(response,POS_INT_NAME,ipcFrom)==com_verifone_ipc::IPC_SUCCESS){
			break;
		}

		usleep(100000);		// Sleep for 100 milli seconds
	}

//	com_verifone_ipc::disconnect(POS_INT_NAME);
//
//	if (response.compare(0,7,"Result:") == 0) {
//		response = response.substr(7);
//	}
//
//	if (response.compare("0") == 0)
//		return PMT_SUCCESS;
//	else
//		return PMT_PROCESSING_ERROR;

	com_verifone_ipc::disconnect(POS_INT_NAME);

	log.message(MSG_INFO "Received ["+response+"]\n");

	return parse_response(response, txresult);
}



int parse_response(string response, map<string, string> &txresp)
{
	if((response.compare(0, 5, "Error") == 0) || (response.compare(0, 9, "Finished:") != 0))
		return PMT_PROCESSING_ERROR;

	istringstream ss(response.substr(9));
	string pair;

	while(getline(ss, pair, ';'))
	{
		int pos = (int)pair.find("=");

		if(pos <= 0)
			return PMT_PROCESSING_ERROR;

		txresp[pair.substr(0, pos)] = pair.substr(pos+1);
	}

	// Check if the transaction was cancelled
	map<string,string>::iterator it;

	it = txresp.find("Busy");
		if(it != txresp.end())
			return PMT_BUSY_CANNOT_PROCESS;	// Cannot process message at this time


	it = txresp.find("Cancelled");
	if(it != txresp.end())
		return PMT_CANCELLED;	// Don't bother checking the value of cancelled flag, it's existence already means 'Cancelled=1'

	it = txresp.find(TXRES_COMPLETE);
	if(it != txresp.end()){
		string val = it->second;

		if(val.compare("0") == 0)
			return PMT_CANCELLED;
	}

	// Check if the reason code indicates that the card declined, then return PMT_DECLINED
	it = txresp.find(TXRES_REASON_CODE);
	if((it != txresp.end()) && (string(it->second).compare("10") == 0))
		return PMT_DECLINED;

	it = txresp.find(TXRES_APPROVED);
	if((it != txresp.end()) && (string(it->second).at(0) == '1'))
		return PMT_SUCCESS;

	return PMT_DECLINED;
}
}
