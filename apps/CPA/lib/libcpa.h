#ifndef _LIBCPA_H_
#define _LIBCPA_H_

#include <string>
#include <map>
#include <iterator>
/*
namespace CPACommand
{
	// Data keys
	#define TX_AMOUNT		"TxAmount"				// Number of cents, numeric only - Maximum length of 12

	// Command id's
	typedef enum{
		CPA_TX_RQST,
		CPA_POS_TX_RQST,
		CPA_PURCHASE_RQST,
		CPA_BANKING_RQST,
		CPA_REPORT_CURRENT_BATCH,
		CPA_REPORT_PREVIOUS_BATCH
	}CPA_COMMAND;
}
*/
//using namespace std;
//using namespace CPACommand;
using std::iterator;
using std::map;
using std::endl;
using std::string;

typedef enum{
	REPORT_CURRENT_BATCH,
	REPORT_PREVIOUS_BATCH
}CPA_REPORT;

#define TX_FIN_RQST												0
#define TX_CASH_WITHDRAWEL										1
#define TX_SALE_AND_CASH										9
#define TX_REFUND												20
#define TX_PRE_DEPOSIT											30
#define TX_ONLINE_BAL_ENQ										31
#define TX_TRANSIT_DATA_ENQ										32
#define TX__OFFLINE_BAL_ENQ										33
#define TX_ONLINE_TRANSIT_BALANCE_ENQ							38
#define TX_VOUCHER_BAL_ENQ										39
#define TX_NOMINATED_ACC_XFER									40
#define TX_CARD_TO_CARD_XFER									41
#define TX_FUNDS_XFER											42
#define TX_CONFIRM_AMOUNT										80
#define TX_CARD_ACTIVATION										81
#define TX_CONFIRM_PAN											82
#define TX_VOUCHER_SALE											83
#define TX_CASH_LOAD											84
#define TX_LOAD_VOUCHER											85
#define TX_LOAD_TRANSIT_PRODUCT									86
#define TX_UNBLOCK_APP											88
#define TX_CONSESSION_UPDATE									89
#define TX_EXPIRE_CARD											90
#define TX_COPY_CARD											91
#define TX_PIN_CHANGE											92
#define TX_MINI_STATEMENT										93
#define TX_ACCOUNT_STATEMENT									94
#define TX_CARD_DIAGNOSTIC										95

#define PMT_SUCCESS						0
#define PMT_CANCELLED					1
#define PMT_DECLINED					2
#define PMT_PROCESSING_ERROR			3
#define PMT_BUSY_CANNOT_PROCESS			4

#define TXRES_COMPLETE		"IsComplete"
#define TXRES_ONLINE		"WentOnline"
#define TXRES_BATCHNO		"BatchNo"
#define TXRES_TSN			"Tsn"
#define TXRES_RCPTNO		"ReceiptNo"
#define TXRES_RESPCODE		"ResponseCode"
#define TXRES_AUTHCODE		"AuthorisationCode"
#define TXRES_SALE_AMNT		"SaleAmount"
#define TXRES_CASH_AMNT		"CashAmount"
#define TXRES_PAN			"Pan"
#define TXRES_TIMESTAMP		"TimeStamp"
#define TXRES_ISO_CODE		"IsoCode"
#define TXRES_RRN			"Rrn"
#define TXRES_REASON_CODE	"ReasonCode"
#define TXRES_EMV_DATA  	"EmvData"
#define TXRES_APPROVED		"Approved"

namespace CpaApp{
	int DoMenu(std::string menuCode);
	int DoSetup(void);
	int DoPayment(string amount);
	int DoPayment(string amount, map<string, string> &txresult);
	int DoReport(CPA_REPORT report);

	int DoPosPayment(string amount, std::string tx_type, string description, string transaction_data, bool hCommandEnabled, map<string, string> &txresult);
	int DoPosVoid(string amount, std::string tx_type, string description, string ecr, map<string, string> &txresult);
	int DoPosGetCPAStatus(void);
	int DoPosCancelWaitingForCardInput(void);
	int DoPosOnIdleScreen(void);
	int DoPosBanking(void);
	int DoPosParameters(void);
	int DoPosReversal(void);
	int DoPosClearReversal(void) ; //iqbal_audi_280217
}

#endif // _LIBCPA_H_
