#include <sstream>
#include "menubmp.h"

#include <iostream>
#include <string>
//using namespace std;

namespace za_co_vfi_MenuBmp
{

bool MenuBmp::SalesPurchases()
{
	return MenuBmp::is_set(8);
}

bool MenuBmp::ReturnsRefunds()
{
	return MenuBmp::is_set(9);
}

bool MenuBmp::ChangePin()
{
	return MenuBmp::is_set(10);
}

bool MenuBmp::CorrectionsReverse()
{
	return MenuBmp::is_set(11);
}

bool MenuBmp::BalanceEnquiry()
{
	return MenuBmp::is_set(12);
}

bool MenuBmp::DepositPayment()
{
	return MenuBmp::is_set(13);
}

bool MenuBmp::CashWithdrawal()
{
	return MenuBmp::is_set(14);
}

bool MenuBmp::SalesPurchasesWithCashback()
{
	return MenuBmp::is_set(15);
}

bool MenuBmp::CardActivation()
{
	return MenuBmp::is_set(16);
}

bool MenuBmp::MpadFundsLoad()
{
	return MenuBmp::is_set(17);
}

bool MenuBmp::MpadFundsUnload()
{
	return MenuBmp::is_set(18);
}

bool MenuBmp::MpadStatement()
{
	return MenuBmp::is_set(19);
}

bool MenuBmp::MpadBalanceEnquiry()
{
	return MenuBmp::is_set(20);
}

bool MenuBmp::PreAuth()
{
	return MenuBmp::is_set(21);
}

bool MenuBmp::PreAuthCompletion()
{
	return MenuBmp::is_set(22);
}

bool MenuBmp::PreAuthCancel()
{
	return MenuBmp::is_set(23);
}

bool MenuBmp::VoiceOverride()
{
	return MenuBmp::is_set(24);
}

bool MenuBmp::QuasiCashWithdrawal()
{
	return MenuBmp::is_set(25);
}

bool MenuBmp::CardIssue()
{
	return MenuBmp::is_set(26);
}

bool MenuBmp::CardReload()
{
	return MenuBmp::is_set(27);
}

bool MenuBmp::GiftCardBalanceEnquiry()
{
	return MenuBmp::is_set(28);
}

bool MenuBmp::OrderReserve()
{
	return MenuBmp::is_set(29);
}

bool MenuBmp::OrderFulfill()
{
	return MenuBmp::is_set(30);
}

bool MenuBmp::Redemption()
{
	return MenuBmp::is_set(31);
}

int MenuBmp::BitCount()
{
	int count=0;

	for(int i=8; i<32; i++){
		if(this->is_set(i))
			count++;
	}

	return count;
}

// Private methods
bool MenuBmp::is_set(unsigned int bitno)
{
	// Check that our bitmap string is the correct length
	if(bmpstr.length() != 8)
		return false;

	int value;
	string subval = bmpstr.substr(size_t(8-((bitno/8)*2)), 2);
	istringstream convert(subval);
	convert >> hex >> value;
//cout << "pos " << bitno << " at " << bitno/8 << " bitno " << bitno%8 << " substr " << (8-((bitno/8)*2)) << " val " << value << " subval " << subval << endl;
	if(value & (1<< (bitno%8)))
		return true;
	else
		return false;
}
/*
8 - ((bitno/8)*2)
011B0001
*/
}

