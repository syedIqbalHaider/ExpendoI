#include <unistd.h>
#include <gui/gui.h>
#include <libtui/tui.h>
#include <sys/types.h>
#include "CLog.h"
#include "string.h"
#include "xmlmenu.h"
//#include "CPAver.h"
//#include "utils.h"

using namespace vfigui;
using namespace za_co_verifone_tui;
using namespace za_co_verifone_xmlmenu;

static CLog log("XMLMENU");


namespace za_co_verifone_xmlmenu {

std::string subString(const char * svIn, const char *svBeg, const char *svEnd);


/**
 * Returns the substring between the end of svBeg and the beginning of svEnd.
 * We search for svEnd only from after svBeg; Therefore we will find
 * a substring delimited by two identical chars (e.g. aaa"bc"ddd ).
 * If either of the demarkating strings is not found, we return "".
 * We return the first match found.
 * @param svIn   The string to be searched
 * @param svBeg  The start string - empty implies beginning
 * @param svEnd  The end string - empty implies end
 * @return The selected substring (see description)
 */
std::string subString(const char * svIn, const char *svBeg, const char *svEnd)
{
   const char *pSt, *pEnd;

   std::string svRes = "";

   if ((svBeg == NULL) || (svEnd == NULL)) return svRes;

   if (*svBeg)
     pSt = strstr(svIn, svBeg);
   else
     pSt = svIn;

   if (pSt == NULL)
      return svRes;                        // Could not find start
   else
   {
      pSt += strlen(svBeg);
      if(! (*pSt))
        return svRes;         // at end of input
      else if (!(*svEnd)) {
          svRes = pSt;
          return svRes;  // will return all to end of string
      }
      else
      {
         pEnd =  strstr(pSt,svEnd);
         if (pEnd == NULL)
            return svRes;           //Could not find end or empty substring
         else {
             svRes = std::string(pSt,pEnd-pSt);
         }
      }
   }
   return svRes;
}

std::string subString(const std::string svIn, const std::string svBeg, const std::string svEnd) {
	return subString(svIn.c_str(),svBeg.c_str(),svEnd.c_str());
}


}

XmlMenu::XmlMenu(const std::string menuPrefix) {
	mPrefix = menuPrefix;
	menuSz = 0;
	mHeader = "";

#ifdef XM_USE_HEAP
	mXmenu = NULL;
#endif
}

XmlMenu::~XmlMenu() {

#ifdef XM_USE_HEAP
	if (mXmenu != NULL) {
		delete[] mXmenu;
	}
#endif
}

int UtilsStringToInt(string str)
{
	int numb;
	istringstream ( str ) >> numb;
	return numb;
}

int XmlMenu::populateMenu(std::string prefix, bool mustAddMenu) {
	std::vector<std::string> mnuItems;

	int i = 0;

	// Get menu items
	while (1) {
		std::string sId = prefix + XM_SEP + SSTR(i);
		std::string itm = TuiString(sId);
		if (itm.empty()) break;

		mnuItems.push_back(itm);
		mnuIds.push_back(sId);
		i++;
	}
	// Build menu
	menuSz = mnuItems.size();
	if (menuSz>XM_MAX_MENU) {
		log.message(MSG_INFO"Nr of entries for menu " + prefix + " too high (" + SSTR(menuSz)
				+ "), limited to " + SSTR(XM_MAX_MENU) + "\n");
		menuSz = XM_MAX_MENU;
	}
	log.message(MSG_INFO"Populate menu " + prefix + ",size=" + SSTR(menuSz) + "\n");
	if (menuSz > 0) {
		mHeader = TuiString(prefix + XM_HEADER);
#ifdef XM_USE_HEAP
		mXmenu = new UIMenuEntry[menuSz];
#endif
		for(int j=0;j<menuSz;j++){
			std::string txt = mnuItems[j];
			std::string retCode = subString(txt,"R",":");
			std::string menTxt = subString(txt,":","");
			if (menTxt.empty()) menTxt = txt;
			if (mustAddMenu) {
				if (menuSz>9 && j<9)
					menTxt = "0" + SSTR(j+1) + ".  " + menTxt;
				else
					menTxt = SSTR(j+1) + ".  " + menTxt;
			}
			//log.message(MSG_INFO + SSTR(j) + ":" + menTxt + "\n");
			mXmenu[j].text = menTxt;
			mXmenu[j].options = 0;
			if (retCode.empty()) {
				mXmenu[j].value = 10000+j;
			} else
				mXmenu[j].value = UtilsStringToInt(retCode);
		}
	}

	return menuSz;
}

int XmlMenu::selectXmlMenu(int initialIdx, xmenuCallback menuCallBack, bool mustAddMenu)
{
	//if (mXmenu == NULL) {
	if (menuSz <= 0) {
		populateMenu(mPrefix, mustAddMenu);
	}

	int iNr = 0;
	//bool isTensGiven = false;
	int s=initialIdx;
	if (menuSz <=0) s = UI_ERR_BACK;
	int lastSel=0;

	while(s>=0) {
		lastSel = (s>=0 && s < menuSz)?(s):0;
		//log.message(MSG_INFO  " menu " + mHeader + ",size=" + SSTR(menuSz) + ", ini=" + SSTR(sel) + "\n");
		s=uiMenu("menu",mHeader,mXmenu,menuSz, mXmenu[lastSel].value);
		string sLog = mHeader + ",size=" + SSTR(menuSz) + ", ini=" + SSTR(lastSel)
						+", sel code=" + SSTR(s) + ",iNr=" + SSTR(iNr) + ",--->";
		for (int i=0; i<menuSz;i++) {
			if (mXmenu[i].value == s) {
				sLog += mXmenu[i].text;
				break;
			}
		}
		sLog += "\n";
		log.message(MSG_INFO  " " + sLog);
		if (s == UI_ERR_ABORT) {
			// Cancel will go up and return from top level
			s = -1;
		} else if (s == UI_ERR_BACK) {
			if (iNr != 0) {
				iNr = 0;
				s = lastSel;
			} else {
				return UI_ERR_BACK;
			}
		} else if ((s == UI_ERR_TIMEOUT) || (s == UI_ERR_WAIT_TIMEOUT)){
			// Back go up one level
			return 1;
		} else {
			//Check for keyboard shortcuts and replace with menu entry values
			std::string selMenuId = "";
			bool isDone = true;
			if ((s >= 0) && (s <= 9) && (iNr+s < menuSz+10)) {
				if (menuSz > 9) {
					if (iNr == 0) {
						iNr = 10 + s*10;
						isDone = false;
					} else {
						iNr += s-11;
					}
				} else if (s==0) {
					iNr = 0;
					isDone = false;
				} else
					iNr = s-1;
				if (isDone) {
					if ((iNr >= 0) && (iNr < menuSz)) {
						selMenuId = mnuIds[iNr];
						s = mXmenu[iNr].value;
					}
					//log.message(MSG_INFO  " B menu " + mHeader + ",size=" + SSTR(menuSz) + ", ini=" + SSTR(lastSel)
					//		+", selected code=" + SSTR(s) + ",iNr=" + SSTR(iNr) + "\n");
					iNr = 0;
				}
				//log.message(MSG_INFO  " C menu " + mHeader + ",size=" + SSTR(menuSz) + ", ini=" + SSTR(lastSel)
				//		+", selected code=" + SSTR(s) + ",iNr=" + SSTR(iNr) + "\n");
			} else if (s >= menuSz && s <= 9) {
			    // ignore keypad numerics higher than menu size
				iNr = 0;
			}

			if (s>=10000) {
				// We should have a submenu
				std::string newPrefix = "";
				uint mNr = s - 10000;
				if (mNr <= mnuIds.size()) {
					//std::string newHeader = mXmenu[mNr].text;
					newPrefix = mnuIds[mNr];
				}
				if (!newPrefix.empty()) {
					XmlMenu newMenu(newPrefix);
					s = newMenu.selectXmlMenu(0,menuCallBack,mustAddMenu);
					if (s == UI_ERR_BACK) {
						// we selected back in submenu so we came back to this level
						// and should highlight its entry in this menu (the parent menu)
						s = mNr;
					}
				} else {
					s = mNr; // no submenu entry: we do nothing
					log.message(MSG_INFO  " menu " + mHeader +", selected code=" + SSTR(s) + ",empty\n");
				}
			} else if (!isDone) {
				s = lastSel;
			} else {
				int mIdx = 0;
				// find first matching index
				for (int i=0; i<menuSz;i++) {
					if (mXmenu[i].value == s) {
						selMenuId = mnuIds[i];
						mIdx = i;
						break;
					}
				}
				if (menuCallBack == NULL)
					return s;
				else {
#ifdef XM_USE_HEAP
					// Note: we delete the menu  before calling the call back because keeping it
					// caused errors inside the call back when new screen functions were used.
					// Note: is this still true??
					if (mXmenu != NULL) {
						delete[] mXmenu;
						mXmenu = NULL;
						menuSz = 0;
					}
#endif
					s = menuCallBack(s,selMenuId);
					iNr = 0;
					if (s == UI_ERR_BACK) {
						// we selected back in submenu so we came back to this level
						// and should highlight its entry in this menu (the parent menu)
						s = mIdx;
					}
					//log.message("AftCallback,mIdx=" + SSTR(mIdx) + ",s=" +SSTR(s)+"\n");

#ifdef XM_USE_HEAP
					// Repopulate menu if used again
					if ((s >= 0) && (mXmenu == NULL)) {
						populateMenu(mPrefix, mustAddMenu);
					}
#endif
				}
			}
		}
	}
	return s;
}

