//#include <set>
#include <unistd.h>
#include <gui/gui.h>
#include <gui/jsobject.h>
#include <libtui/tui.h>
#include <libda/cusermanager.h>

#include "CAuthUser.h"
//#include "utils.h"
#include "CLog.h"
#include "CPAver.h"

//using namespace std;
//using namespace Utils;
using namespace vfigui;
using namespace za_co_verifone_tui;
using namespace com_verifone_usermanager;

static CLog log(CPA_NAME);

namespace com_verifone_user
{

CAuthUser::CAuthUser()
{
	mIsAuthenticated = false;

}

bool CAuthUser::isAuthenticated() {
	return mIsAuthenticated;
}

bool CAuthUser::isBankingAllowed() {
	return mUser.isBankingAllowed();
}

std::string& CAuthUser::getDateAdded() {
	return mUser.getDateAdded();
}

std::string& CAuthUser::getId() {
	return mUser.getId();
}

std::string& CAuthUser::getName() {
	return mUser.getName();
}

std::string& CAuthUser::getPin() {
	return mUser.getPin();
}

bool CAuthUser::isPinRequired() {
	return mUser.isPinRequired();
}

bool CAuthUser::isRefundsAllowed() {
	return mUser.isRefundsAllowed();
}

CUser::USER_TYPE CAuthUser::getUserType() {
	return mUser.getUserType();
}

void dumpMap(std::map<std::string, std::string> &mapValues) {
	int i=0;
	for(std::map<std::string, std::string>::iterator it = mapValues.begin(); it != mapValues.end(); it++) {
		log.message("m" + SSTR(i++)+". "+it->first + "=" + it->second+"\n");
		//dlog_msg(" %s=%s",it->first.c_str(),it->second.c_str());
	}
}
int CAuthUser::authenticate() {
	std::string tmpl;

	std::map<string,string> values;
	values["varPW"] = "";//"123";
	values["usname"] = "";//123";

	TuiTemplate("bgimage.tmpl", "UserLogin", tmpl);

	bool stayHere = true;
	while (stayHere) {

		//values["screentitle"] = "Login";
		//values["subtitle"] = "Supervisor Login";

		int rc = uiInvoke(values,tmpl); // should we use asynch methods?

		switch (rc) {
			//case 0: break;
			case UI_ERR_WAIT_TIMEOUT:
			case UI_ERR_TIMEOUT: 			break;
			case 1: stayHere = false;		break;
			case 0: {
				std::string nm = values["usname"];
				std::string pw = values["varPW"];
				CUserManager userMng;

				if (userMng.verifyPinByname(nm,pw)) {
					if (DB_OK != userMng.getUserByName(nm,mUser)) {
						log.message("Could not read details for user" + SSTR(nm) + "\n");
					} else {
						log.message("User " + SSTR(nm) + "logged in.\n");
						//log.message("U.getName="+mUser.getName()+"\n");
						//log.message("U.getId="+mUser.getId()+"\n");
						//log.message("U.getUserType="+SSTR(mUser.getUserType())+"\n");
						//log.message("U.isBankingAllowed="+SSTR(mUser.isBankingAllowed())+"\n");
						//log.message("U.isPinRequired="+SSTR(mUser.isPinRequired())+"\n");
						//log.message("U.isRefundsAllowed="+SSTR(mUser.isRefundsAllowed())+"\n");
						mIsAuthenticated = true;
						stayHere = false;
					}
				} else {
					log.message("Failed to verify user " + SSTR(nm) + "\n");
				}

				dumpMap(values);
				break;
			}
		}
		usleep(100*1000);
	}
	log.message("end auth\n");
	return 0;
}

}
