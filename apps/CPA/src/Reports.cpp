#include <iostream>
#include <sstream>
#include <libda/cterminalconfig.h>
#include <libda/cconmanager.h>
#include <libda/cbatchmanager.h>
#include <libda/chotcard.h>
#include <libda/ccardconfig.h>
#include <libda/climits.h>
#include <libtui/tui.h>
#include <sys/types.h>
#include <sys/timeb.h>
#include <string.h>
#include <algorithm>
#include <unistd.h>

#include "Printer.h"
#include "Reports.h"
#include "utils.h"
#include "Transaction.h"
#include "CLog.h"
#include "bertlv.h"
#include "Operator.h"
#include "menubmp.h"
#include "TrackData.h"
#include "Operator.h"

extern "C" {
	#include <platforminfo_api.h>
}

static CLog log(CPA_NAME);

//using namespace std;
using namespace za_co_verifone_tui;
using namespace za_co_vfi_prn;
using namespace Utils;
using namespace za_co_vfi_Transaction;
using namespace za_co_verifone_bertlv;
using namespace  za_co_vfi_reports;
using namespace com_verifone_batchmanager;

namespace  za_co_vfi_reports
{

int printHeaders(void);

class Report: public Prn
{
public:
	Report(std::vector<std::string>&lines);
	int print(bool advanceAtEnd = false);
	int print(std::vector<std::string>&lines, bool advanceAtEnd = false);
	void advanceAtEnd(void);
	int feedlines(int count);
	int cut(void);

private:
	std::vector<std::string>rpt_lines;
};

Report::Report(std::vector<std::string>&lines)
{
	rpt_lines = lines;
}

int Report::print(std::vector<std::string>&lines, bool mustAdvanceAtEnd)
{
	rpt_lines = lines;
	return print(mustAdvanceAtEnd);
}

int Report::print(bool mustAdvanceAtEnd)
{
	if(!status()) {
		log.message(MSG_INFO "Printer not available\n");
		return -1;	// Printer not available
	}
	int width = getWidth();
	std::string prnline;

	for(vector<string>::iterator it=rpt_lines.begin(); it!=rpt_lines.end(); ++it){
		std::string line = *it;
		size_t pos=0;
//cout << "line:" << line << endl;
		int lenAfter = width-line.length()+2;
		if((pos = line.find("\\C")) != std::string::npos){
			line.erase(pos, 2);
			int len = lenAfter/2;
			if (len > 0) // only insert if line shorter
				prnline.insert(prnline.length(),len, ' ');
			prnline.append(line);

		}else if((pos = line.find("\\R")) != std::string::npos){
			line.erase(pos, 2);
			int len = width-prnline.length();
			if ((len > 0) && (lenAfter>=0)) {
				prnline.insert(prnline.length(), len, ' ');
				prnline.replace(lenAfter, line.length(), line);
			} else if (lenAfter >= 0) // not enough space; just append
				prnline.append(line);

		}else{
			prnline.append(line);

		}

		if(prnline.find("\n") != std::string::npos){
			// Uncomment below to get print report send to log file
			// log.message("Print:" + prnline);

			if(Prn::print(prnline) <= 0)
				return -1;

			prnline.clear();
		}
	}

	if (mustAdvanceAtEnd) advanceAtEnd();

	return 0;
}

void Report::advanceAtEnd(void) {
	Prn::feed(5);
	Prn::cut();
}

void addIfNonEmpty(std::map<std::string, std::string> &values, std::string key, std::string val) {
	if (!key.empty()) {
		if (!val.empty())
			values[key] = val;
		else if (values.find(key) != values.end()) // clear existing empty values
			values.erase(key);
	}
}

std::string getCurrentDateTime() {
	time_t     now = time(0);
	struct tm  tstruct;
	char       buf[80];
	tstruct = *localtime(&now);
	strftime(buf, sizeof(buf), "%Y-%m-%d %X", &tstruct);

	return buf;
}

std::string getFmtAmount(std::string sCur,int iAmount) {
	string amount;

	if (iAmount == 0)
		amount = "000";
	else{
		amount = UtilsIntToString(iAmount);

		if(iAmount < 100)
			amount.insert(0, "0");

		if(iAmount < 10)
			amount.insert(0, "0");
	}

	amount.insert(0, sCur);
	amount.insert(amount.length()-2, ".");

	return amount;
}

Receipt::Receipt(CBatchRec &batch)
{
	batchrec = batch;

	struct timeb tp;
	ftime(&tp);
	char buf[16];

	memset(buf, 0, sizeof(buf));
	strftime(buf,sizeof(buf),"%04Y/%02m/%02d",localtime(&tp.time));
	datestr = string(buf);

	strftime(buf,sizeof(buf),"%02H:%02M:%02S",localtime(&tp.time));
	timestr = string(buf);
}

int Receipt::format(void)
{
	std::string sCur = "R";
	com_verifone_terminalconfig::CTerminalConfig termcfg;
	com_verifone_hotcard::CHotcard hotcard;

	iso_tx_code = UtilsStringToInt(batchrec.getTxCode());
	signature_required = batchrec.isSignatureRequired();

	string NOTYETDONE = "______";

	string amount = getFmtAmount(sCur,batchrec.getTxAmount());
	values["Amount"] = amount;

	// Populate Header Values
	addIfNonEmpty(values, "Header1", termcfg.getMerchantName());
	addIfNonEmpty(values, "Header2", "");
	addIfNonEmpty(values, "Header3", "");
	addIfNonEmpty(values, "Header4", "");
	addIfNonEmpty(values, "MerchNo", termcfg.getMerchantNo());
	addIfNonEmpty(values, "Tid", termcfg.getTerminalNo());

	// Populate Receipt Itself

	//addIfNonEmpty(values,"RecpType",RECEIPT_TYPE);
	addIfNonEmpty(values, "RecpNo", batchrec.getReceiptNo());
	std::string sDt = batchrec.getTxDateTime();
	addIfNonEmpty(values, "Date", datestr);
	addIfNonEmpty(values, "Time", timestr);

	//addIfNonEmpty(values,"ReprintHeading","");
	//addIfNonEmpty(values,"ReprintDate","");
	//addIfNonEmpty(values,"ReprintTime","");
	addIfNonEmpty(values, "Tsn", SSTR(batchrec.getTsn()));
	string hcno;
	hotcard.getVersion(hcno);
	addIfNonEmpty(values, "HcNo",hcno);
	addIfNonEmpty(values, "AuthCode", " " + batchrec.getAuthCode());
	addIfNonEmpty(values, "ApprMode", batchrec.isApproveOnline() ? "1" : "0");
	com_verifone_batchmanager::CBatchManager batchManager;
	com_verifone_batch::CBatch cbatch;
	batchManager.getBatch(cbatch);
	addIfNonEmpty(values, "BatchNo", SSTR(cbatch.getBatchNo()));
	addIfNonEmpty(values, "BatchTxnCount", SSTR(getTransactionCount()));
	addIfNonEmpty(values, "SrcCode", batchrec.getSrc());
	addIfNonEmpty(values, "VasFlag", "N");
	addIfNonEmpty(values, "UserName", batchrec.getCashierName());
	addIfNonEmpty(values, "UserId", batchrec.getCashierId());
	addIfNonEmpty(values, "PinVerif", batchrec.isCashierPinVerified() ? "Y" : "N") ;
	addIfNonEmpty(values, "WaiterId", batchrec.getWaiterId());
	addIfNonEmpty(values, "TableNo", batchrec.getTableNo());
	addIfNonEmpty(values, "AccType", batchrec.getAccType());

	if (UtilsStringToInt(batchrec.getBudgetPeriod()) != 0)
		addIfNonEmpty(values, "BudgetPeriod", batchrec.getBudgetPeriod());

	string entrymode;
	AuthMode authmode = (AuthMode)batchrec.getAuthMode();

	switch(authmode)
	{
		case za_co_vfi_Transaction::TERMINAL:		entrymode = "T"; break;
		case za_co_vfi_Transaction::HOST:			entrymode = "H"; break;
		case za_co_vfi_Transaction::VOICE:			entrymode = "V"; break;
		case za_co_vfi_Transaction::DEF_ANALYSIS:	entrymode = "D"; break;
	}

	PAN_ENTRY_MODE emode = batchrec.getPanEntryMode();

	switch(emode)
	{
		case SWIPED:	entrymode.append("S"); break;
		case ENTERED:	entrymode.append("M"); break;
		case INSERTED:	entrymode.append("I"); break;
		case TAPPED:	entrymode.append("T"); break;
		case FALLBACK:	entrymode.append("F"); break;
		case UNKNOWN: break;
	}

	addIfNonEmpty(values, "EntryMode", entrymode);

	string masked_pan=batchrec.getPan();
	if(masked_pan.length()){
		masked_pan.replace(6, masked_pan.length()-10, masked_pan.length()-10, '*');
		addIfNonEmpty(values, "Pan", masked_pan);
	}

	//addIfNonEmpty(values,"MaskedPan","");
	addIfNonEmpty(values, "RRN", batchrec.getRrn());
	//addIfNonEmpty(values,"RcsTxType","");
	//addIfNonEmpty(values,"RcsFlag",batchrec.isRcsCard() ? " " : "");
	//addIfNonEmpty(values,"EmvDataFlag",batchrec.getEmvTags());

	string emvtlv = batchrec.getEmvTags();
	if(emvtlv.length()){
		BERTLV tlv(emvtlv);
		string value;
		if(tlv.getValForTag("\x4f", value))	values["Aid"]=UtilsHexToString(value.c_str(), value.length());
		if(tlv.getValForTag("\x9b", value))	values["Tsi"]=UtilsHexToString(value.c_str(), value.length());
		if(tlv.getValForTag("\x95", value))	values["Tvr"]=UtilsHexToString(value.c_str(), value.length());
//		addIfNonEmpty(values,"FTvr",NOTYETDONE);
		if(tlv.getValForTag("\x9f\x26", value))	values["EmvAc"]=UtilsHexToString(value.c_str(), value.length());
		if(tlv.getValForTag("\x9f\x10", value))	values["EmvIap"]=UtilsHexToString(value.c_str(), value.length());
//		if(tlv.getValForTag("\x50", value))	values["AppLabel"]=UtilsHexToString(value.c_str(), value.length());
		if(tlv.getValForTag("\x50", value))	values["AppLabel"]=value;
		if(tlv.getValForTag("\x5f\x25", value))	values["AppEffDate"]=UtilsHexToString(value.c_str(), value.length());
		if(tlv.getValForTag("\x5f\x24", value))	values["AppExpDate"]=UtilsHexToString(value.c_str(), value.length());
	}

	//addIfNonEmpty(values,"PREVAL","");
	//addIfNonEmpty(values,"Desc0","");
	//addIfNonEmpty(values,"TotAmt0","");
	//addIfNonEmpty(values,"Quantity0","");
	//addIfNonEmpty(values,"Type0","");
	//addIfNonEmpty(values,"UnitPrice0","");

	std::string sTm = UtilsTimestamp();
	if (sTm.substr(0, 4) == batchrec.getExp()) {
		addIfNonEmpty(values, "ExpFlag", " ");
	}

	//addIfNonEmpty(values,"VoiceAppHeading","");
	//addIfNonEmpty(values,"ImprintAppHeading1","");
	//addIfNonEmpty(values,"ImprintAppHeading2","");
	//addIfNonEmpty(values,"CanTxType","");
	//addIfNonEmpty(values,"CanRecNo","");
	//addIfNonEmpty(values,"FuelType","");
	//addIfNonEmpty(values,"Liters",SSTR(batchrec.getLitres()));
	//addIfNonEmpty(values,"UnitPrice","");
	//addIfNonEmpty(values,"ProductHeader","");
	//addIfNonEmpty(values,"ProdType1","");
	//addIfNonEmpty(values,"ProdAmt1","");
	//addIfNonEmpty(values,"ProdType1","");
	//addIfNonEmpty(values,"EmptyLine","");

	if(batchrec.isTxCanceled()){
		addIfNonEmpty(values,"EmptyLine"," ");
		addIfNonEmpty(values,"ReversalFlag"," ");
	}

	addIfNonEmpty(values,"TxTypeNL"," ");

	if((iso_tx_code == TX_ISO_SALE) || (iso_tx_code == TX_ISO_SALE_CASH)){
		string str_tx = TuiString("TxSale");
		addIfNonEmpty(values,"TxType",str_tx);
		addIfNonEmpty(values,"SaleAmount",amount);
	}

	if((iso_tx_code == TX_ISO_CASH_ADVANCE) || (iso_tx_code == TX_ISO_SALE_CASH)){
		string str_tx = TuiString("TxCash");
		addIfNonEmpty(values,"TxType",str_tx);
		string sAmt = getFmtAmount(sCur,batchrec.getTxAmount());
		addIfNonEmpty(values,"CashAmount",sAmt);
	}
/*
	addIfNonEmpty(values,"TxType",batchrec.getTxType());
	addIfNonEmpty(values,"SaleAmount",amount);
	addIfNonEmpty(values,"CashBFlag","");
	addIfNonEmpty(values,"CashAmount","");
*/
	if (batchrec.getTipAmount() != 0) {
		addIfNonEmpty(values,"TipF"," ");
		string sAmt = getFmtAmount(sCur,batchrec.getTipAmount());
		addIfNonEmpty(values,"TipAmount",sAmt);
	}
/*
	addIfNonEmpty(values,"OilF","");
	addIfNonEmpty(values,"OilAmount","");
*/
	if(iso_tx_code == TX_ISO_LOAD_PRODUCT){
		addIfNonEmpty(values,"PLoadF"," ");
		addIfNonEmpty(values,"PLoadAmount",amount);
	}

	if(iso_tx_code == TX_ISO_DEPOSIT){
		addIfNonEmpty(values,"CLoadF"," ");
		long amt = batchrec.getTxAmount() - batchrec.getCardFees() - batchrec.getLoadFees();
		addIfNonEmpty(values,"CLoadAmount",getFmtAmount(sCur,amt));
	}

	if (batchrec.getCardFees() != 0) {
		addIfNonEmpty(values,"CFeeF"," ");
		string sAmt = getFmtAmount(sCur,batchrec.getCardFees());
		addIfNonEmpty(values,"CardFee",sAmt);
	}

	if (batchrec.getLoadFees() != 0) {
		addIfNonEmpty(values,"LFeeF"," ");
		string sAmt = getFmtAmount(sCur,batchrec.getLoadFees());
		addIfNonEmpty(values,"LoadFee",sAmt);
	}

	addIfNonEmpty(values,"UnderLine","_______________");
	addIfNonEmpty(values,"TotalAmount",amount);
	//addIfNonEmpty(values,"ODO","");
	//addIfNonEmpty(values,"Usage","");
	//addIfNonEmpty(values,"DID","");
	//addIfNonEmpty(values,"FNO","");
	//addIfNonEmpty(values,"JNO","");
	//addIfNonEmpty(values,"InvNoFlag","");
	addIfNonEmpty(values, "InvNo", batchrec.getInvoiceNo());
	//addIfNonEmpty(values,"RefNoFlag1","");
	//addIfNonEmpty(values,"RefNo1",batchrec.getRefNo1());

	// -- The Footer --
	addIfNonEmpty(values,"FooterLineFlag"," ");
	addIfNonEmpty(values,"FooterMsgFlag",""); // do not print
	addIfNonEmpty(values,"TrailerText", " ");

	return 0;
}

//void Receipt::addIfNonEmpty(map<string, string> &values, string key, string val) {
//	if (!key.empty()) {
//		if (!val.empty())
//			values[key] = val;
//		else if (values.find(key) != values.end()) // clear existing empty values
//			values.erase(key);
//	}
//}

int Receipt::merchantCopy(bool reprint)
{
	if(values.size() == 0)
		format();

	vector<string>lines;

	values["MerchCopyFlag"] = " ";

	if(reprint){
		values["ReprintHeading"] = " ";
		values["ReprintDate"] = datestr;
		values["ReprintTime"] = timestr;
	}

	if(TuiReport("RcptHeader", lines, values) == 0)
	{log.message(MSG_INFO"\r\n");
		return -1;
	}

	if(TuiReport("TxnReceipt", lines, values) == 0)
	{log.message(MSG_INFO"\r\n");
		return -1;
	}

	addIfNonEmpty(values,"FooterLineFlag","");
	addIfNonEmpty(values,"FooterMsgFlag"," "); // show message

	if(TuiReport("RcptFooter", lines, values) == 0)
	{log.message(MSG_INFO"\r\n");
		return -1;
	}

	values.erase("MerchCopyFlag");

	Report report(lines);
log.message(MSG_INFO"\r\n");
	return report.print(true);
}

int Receipt::customerCopy(bool reprint)
{
	com_verifone_terminalconfig::CTerminalConfig termcfg;

	if(values.size() == 0)
		format();

	vector<string>lines;

	values["CustCopyFlag"] = " ";

	if(reprint){
		values["ReprintHeading"] = " ";
		values["ReprintDate"] = datestr;
		values["ReprintTime"] = timestr;
	}

	addIfNonEmpty(values, "Cardholder", batchrec.getCardholderName());
	addIfNonEmpty(values, "SigFlag", " ");
	addIfNonEmpty(values,"TrailerText", termcfg.getTrailerText());

	if(TuiReport("RcptHeader", lines, values) == 0)
			return -1;

	if(TuiReport("TxnReceipt", lines, values) == 0)
				return -1;

	if(signature_required)
	{
		if(TuiReport("RcptSigPanel", lines, values) == 0)
					return -1;
	}

	addIfNonEmpty(values,"FooterLineFlag","");
	addIfNonEmpty(values,"FooterMsgFlag"," "); // show message

	if(TuiReport("RcptFooter", lines, values) == 0)
		return -1;

	Report report(lines);

	return report.print(true);
}

int Receipt::declined(void)
{
	if(values.size() == 0)
		format();

	vector<string>lines;

	string statusstr=TuiString("StatusDecl");
	statusstr.append(" "+batchrec.getRespCode());

	values["Status"] = statusstr;

	string statusmsgstr;
	int respcode = UtilsStringToInt(batchrec.getRespCode());

	switch(respcode)
	{
		case 200:
		case 207: values["StatusMsg"] = TuiString("StatusHold"); break;
		case 206: values["StatusMsg"] = TuiString("StatusPinRetry"); break;
		case 117: values["StatusMsg"] = TuiString("StatusPinInv"); break;
		case 121: values["StatusMsg"] = TuiString("StatusDailyLimit"); break;
		case 909: values["StatusMsg"] = TuiString("StatusSysMal"); break;
		case 911: values["StatusMsg"] = TuiString("StatusTechErr"); break;
	}

	if(TuiReport("RcptHeader", lines, values) == 0)
		return -1;

	if(TuiReport("TxnReceipt", lines, values) == 0)
		return -1;

	Report report(lines);

	return report.print(true);
}

int Receipt::getTransactionCount(void)
{
	CBatchManager batchManager;
	CBatch batch;
	int ret = batchManager.getBatch(batch);
	int tx_count = 0;

	if (ret != DB_OK) {
		log.message((MSG_INFO "Error getting previous batch, code=") + SSTR(ret) + "\n");
		return 0;
	}

	std::map<unsigned int, CBatchRec> batchRecs;

	ret = batch.getRecords(batchRecs);

	if (ret != DB_OK){
		log.message((MSG_INFO "Error getting batch records, code=") + SSTR(ret) + "\n");
		za_co_vfi_Operator::Operator op;
		op.BatchNoxTx();
	}else{
		std::map<unsigned int,CBatchRec>::iterator it;

		for (it=batchRecs.begin(); it!=batchRecs.end(); ++it) {
			CBatchRec batchRec = it->second;

			if(batchRec.isInProgress() || batchRec.isTxCanceled())
				continue;

			tx_count++;
		}
	}

	return tx_count;
}

// ---------------------------- Batch Report ----------------------------

BatchReport::BatchReport() {
}

int BatchReport::currentBatch(void) {
	return doBatch(true);
}
int BatchReport::previousBatch(void) {
	return doBatch(false);
}

int BatchReport::doBatch(bool isCurBatch) {
    CBatchManager batchManager;
    CBatch batch;
    int net_total=0, total_purchases=0, total_cashbacks=0, total_refunds=0, total_reversals=0, total_payments=0, tx_count=0;
    int apprCnt = 0;
    int declCnt = 0;
    int ret = isCurBatch ? batchManager.getBatch(batch) : batchManager.getPreviousBatch(batch);

    if (ret != DB_OK) {
        log.message((MSG_INFO "Error getting previous batch, code=") + SSTR(ret) + "\n");
        za_co_vfi_Operator::Operator op;
        op.BatchNoxTx();
	} else {
		std::map<unsigned int, CBatchRec> batchRecs;
		ret = batch.getRecords(batchRecs);

		if (ret != DB_OK){
			log.message((MSG_INFO "Error getting batch records, code=") + SSTR(ret) + "\n");
			za_co_vfi_Operator::Operator op;
			op.BatchNoxTx();
		}else{
			std::map<unsigned int,CBatchRec>::iterator it;

			for (it=batchRecs.begin(); it!=batchRecs.end(); ++it) {
				CBatchRec batchRec = it->second;

				if(batchRec.isInProgress() || batchRec.isTxCanceled())
					continue;

				tx_count++;

				if(batchRec.isApproved()){
					apprCnt++;
				} else if(batchRec.isDeclined())
					declCnt++;
				else
					log.message(MSG_INFO "WARNING: Transaction neither approved nor declined!\n");
			}

			printHeaders(batch, tx_count);

			// Print the transactions
			if (tx_count > 0) {
				// TxSummary
				printBatchHeader(declCnt,"BDecl");

				// Print declined transactions
				for (it=batchRecs.begin(); it!=batchRecs.end(); ++it) {
					CBatchRec batchRec = it->second;

					if(batchRec.isInProgress() || batchRec.isTxCanceled())
						continue;

					if (batchRec.isDeclined())
						printTx(batchRec);
				}

				// PRint approved transactions
				printBatchHeader(apprCnt,"BAppr");
				for (it=batchRecs.begin(); it!=batchRecs.end(); ++it) {
					CBatchRec batchRec = it->second;

					if(batchRec.isInProgress() || batchRec.isTxCanceled())
						continue;

					if(batchRec.isApproved()) {
						printTx(batchRec);

						string tx_code = batchRec.getTxCode();
						int itx_code = UtilsStringToInt(tx_code);

						if((itx_code == TX_ISO_SALE) || (itx_code == TX_ISO_SALE_CASH))
							total_purchases += batchRec.getTxAmount();

						if((itx_code == TX_ISO_CASH_ADVANCE) || (itx_code == TX_ISO_SALE_CASH))
							total_cashbacks += batchRec.getCashAmount();

						if((itx_code == TX_ISO_REFUND) || (itx_code == TX_ISO_CORRECTION))
							total_refunds += batchRec.getTxAmount() + batchRec.getCashAmount();

						if((itx_code == TX_ISO_DEPOSIT) || (itx_code == TX_ISO_PMNT_DEPOSIT))
							total_payments=0;

	//					total_reversals
					}
				}
			}

			net_total = total_purchases + total_payments - total_cashbacks - total_refunds - total_reversals;

			string sum_tot_heading = TuiString("TotalsSummaryHeader");

			map<string, string>values;
			vector<string>lines;

			// TotalsSummary - skipped for now
			//		addIfNonEmpty(values, "LfFlag", "");
			//		addIfNonEmpty(values, "UserId", "");
			//		addIfNonEmpty(values, "UserName", "");
			addIfNonEmpty(values, "SummaryHeading", sum_tot_heading);
			addIfNonEmpty(values, "TotalSales", getFmtAmount("R", net_total));
			addIfNonEmpty(values, "TotalCashb", getFmtAmount("R", total_cashbacks));
			addIfNonEmpty(values, "TotalReversals", getFmtAmount("R", total_reversals));
			//		addIfNonEmpty(values, "TotalCorrections", "");
			addIfNonEmpty(values, "TotalPayments", getFmtAmount("R", total_payments));
			addIfNonEmpty(values, "TotalAmount", getFmtAmount("R", net_total));
			//		addIfNonEmpty(values, "cor_flag", "");
			//		addIfNonEmpty(values, "rev_flag", "");
			//		addIfNonEmpty(values, "TotalsGroup", "");

			TuiReport("TotalsSummary", lines, values);

			{ // Keep report local
				za_co_vfi_reports::Report report(lines);
				report.print(false); // must be false for footer below.
				lines.clear();
			}

			printFooter(isCurBatch);

			// Uncomment below to add support for supertotals
			// (Already prints report, except use TotalsGroup instead of supertotals
			//  need to look at TODO places

			//if (!isCurBatch)
			printSuperTotals(batchRecs);

			// Moved advanceAtEnd here from BatchFooter because
			// SuperTotals comes after the footer

			za_co_vfi_reports::Report report(lines);
			report.advanceAtEnd();
		}
	}

    return ret;
}


int BatchReport::printHeaders(CBatch &batch, size_t sz)
{
	string NOTYETDONE = "______";
	map<string, string>values;
	com_verifone_terminalconfig::CTerminalConfig termcfg;

	struct timeb tp;
	ftime(&tp);
	char buf[16];
	string datestr, timestr;

	memset(buf, 0, sizeof(buf));
	strftime(buf,sizeof(buf),"%04Y/%02m/%02d",localtime(&tp.time));
	datestr = string(buf);

	strftime(buf,sizeof(buf),"%02H:%02M:%02S",localtime(&tp.time));
	timestr = string(buf);

	// Populate Header Values
	addIfNonEmpty(values, "Header1", termcfg.getMerchantName());
	addIfNonEmpty(values, "Header2", "");
	addIfNonEmpty(values, "Header3", "");
	addIfNonEmpty(values, "Header4", "");
	std::string sDt = getCurrentDateTime();
	//log.message("Date="+SSTR(sDt.length())+","+sDt+"\n");
	addIfNonEmpty(values, "DateTime", sDt);

	addIfNonEmpty(values, "Date", datestr);
	addIfNonEmpty(values, "Time", timestr);

	addIfNonEmpty(values, "MerchNo", termcfg.getMerchantNo());
	addIfNonEmpty(values, "Tid", termcfg.getTerminalNo());

	// BatchHeader
	addIfNonEmpty(values, "BatchNo", SSTR(batch.getBatchNo()));
	addIfNonEmpty(values, "BatchCnt", SSTR(sz));
	// Format of getDateTimeOpened: strftime(buf, sizeof(buf), "%Y-%m-%d %X", &tstruct);
	std::string dt = batch.getDateTimeOpened();
	addIfNonEmpty(values, "OpenBatchDate", dt.length()>=10 ? dt.substr(0,10) : dt);
	addIfNonEmpty(values, "OpenBatchTime", dt.length()>10 ? dt.substr(11) : "");
	dt = batch.getDateTimeClosed();
	addIfNonEmpty(values, "BatchDate", dt.length()>=10 ? dt.substr(0,10) : dt);
	addIfNonEmpty(values, "BatchTime", dt.length()>10 ? dt.substr(11) : "");

	// Print the headers (we also use receipt header for batchrep)
	{
		vector<string>lines;
		//log.message(SSTR(MSG_INFO "Print Headers") + "\n");

		if(TuiReport("RcptHeader", lines, values) == 0)
			return -1;

		if(TuiReport("BatchHeader", lines, values) == 0)
			return -1;

		if (sz == 0) {
			if(TuiReport("BatchEmpty", lines, values) == 0)
				return -1;
		}

		za_co_vfi_reports::Report report(lines);
		report.print();
	}

	return 0;
}

int BatchReport::printBatchHeader(int cnt, std::string headerPhrase) {
	map<string, string>values;
	addIfNonEmpty(values,headerPhrase+"Cnt",SSTR(cnt));
	addIfNonEmpty(values,headerPhrase+"Flag"," ");

	std::vector<std::string>lines;
	if(TuiReport("Batch" + headerPhrase + "Header", lines, values) == 0)
		return -1;

	za_co_vfi_reports::Report report(lines);
	return report.print();
}

// Uncomment below to add support for supertotals
// (Already prints report, except use TotalsGroup instead of supertotals
//  need to look at TODO places
void BatchReport::printSuperTotals(std::map<unsigned int, CBatchRec>  &batchRecs) {

	log.message(MSG_INFO "Print Super Totals\n");

	if (batchRecs.size() <= 0)
		return;

	map<string, string>values;
	vector<string>lines;
	string sum_tot_heading = TuiString("CardTotalsHeader");

	addIfNonEmpty(values, "SummaryHeading", sum_tot_heading);

	std::map<unsigned int,CBatchRec>::iterator it;
	std::vector<std::string> grps;

	// Get the groups
	for (it=batchRecs.begin(); it!=batchRecs.end(); ++it) {
		CBatchRec batchRec = it->second;

		if(batchRec.isApproved()) {
			// TODO: Below should be replaced by the supertotals group
			std::string grp = batchRec.getTotalsGroup();
			if (grp.empty()) grp = "No Group";
			// End todo

			if (std::find(grps.begin(), grps.end(), grp) == grps.end())
				grps.push_back(grp);
		}
	}
	// No sorting done at present - should come here

	// Now print each groups totals
	for (uint i=0; i<grps.size(); i++) {
		int net_total=0, total_purchases=0, total_cashbacks=0, total_refunds=0, total_reversals=0, total_payments=0;
		std::string sGrp = grps[i];

		for (it=batchRecs.begin(); it!=batchRecs.end(); ++it) {
			CBatchRec batchRec = it->second;
			// TODO: Below should be replaced by the supertotals group
			std::string grp = batchRec.getTotalsGroup();
			if (grp.empty()) grp = "No Group";
			// End todo

			if(batchRec.isApproved() && (grp == sGrp)) {

				string tx_code = batchRec.getTxCode();
				int itx_code = UtilsStringToInt(tx_code);

				if((itx_code == TX_ISO_SALE) || (itx_code == TX_ISO_SALE_CASH))
					total_purchases += batchRec.getTxAmount();

				if((itx_code == TX_ISO_CASH_ADVANCE) || (itx_code == TX_ISO_SALE_CASH))
					total_cashbacks += batchRec.getCashAmount();

				if((itx_code == TX_ISO_REFUND) || (itx_code == TX_ISO_CORRECTION))
					total_refunds += batchRec.getTxAmount() + batchRec.getCashAmount();

				if((itx_code == TX_ISO_DEPOSIT) || (itx_code == TX_ISO_PMNT_DEPOSIT))
					total_payments=0;

				//					total_reversals
			}
		}
		net_total = total_purchases + total_payments - total_cashbacks - total_refunds - total_reversals;

		// We have totals, now print
		// TotalsSummary - skipped for now
		//		addIfNonEmpty(values, "LfFlag", "");
		//		addIfNonEmpty(values, "UserId", "");
		//		addIfNonEmpty(values, "UserName", "");
		// addIfNonEmpty(values, "SummaryHeading", sum_tot_heading);
		addIfNonEmpty(values, "TotalsGroup", sGrp);
		addIfNonEmpty(values, "TotalSales", getFmtAmount("R", net_total));
		addIfNonEmpty(values, "TotalCashb", getFmtAmount("R", total_cashbacks));
		addIfNonEmpty(values, "TotalReversals", getFmtAmount("R", total_reversals));
		//		addIfNonEmpty(values, "TotalCorrections", "");
		addIfNonEmpty(values, "TotalPayments", getFmtAmount("R", total_payments));
		addIfNonEmpty(values, "TotalAmount", getFmtAmount("R", net_total));
		//		addIfNonEmpty(values, "cor_flag", "");
		//		addIfNonEmpty(values, "rev_flag", "");
		//		addIfNonEmpty(values, "TotalsGroup", "");

		{ // Make sure reports context is cleared each iteration
			TuiReport("TotalsSummary", lines, values);

			za_co_vfi_reports::Report report(lines);
			report.print(false);
		}
		lines.clear();
	}
}

int BatchReport::printFooter(bool isCurBatch) {
	std::vector<std::string>lines;
	// -- Now print the Footer - only if Previous Batch --
	if (!isCurBatch) {
		map<string, string>values;
		addIfNonEmpty(values,"BFooterFlag"," ");
		//addIfNonEmpty(values,"BFooterFlag",""); // do not print

		if(TuiReport("BatchFooter", lines, values) == 0)
			return -1;

		za_co_vfi_reports::Report report(lines);
		report.print();
	}

	return 0;
}

int BatchReport::printTx(CBatchRec &batchrec) {
	map<string, string>values;

//	// Clear all values
//	addIfNonEmpty(values, "Date", "");
//	addIfNonEmpty(values, "Time", "");
//	addIfNonEmpty(values, "AuthCode", "");
//	addIfNonEmpty(values, "RespCode", "");
//	addIfNonEmpty(values, "ApprMode", "");
//	addIfNonEmpty(values, "RecpNo", "");
//	addIfNonEmpty(values, "Tsn", "");
//	addIfNonEmpty(values, "RRN", "");
//	addIfNonEmpty(values, "RefNoFlag1", "");
//	addIfNonEmpty(values, "RefNo1", "");
//	addIfNonEmpty(values, "RefNoFlag2", "");
//	addIfNonEmpty(values, "RefNo2", "");
//	addIfNonEmpty(values, "Pan", "");
//	addIfNonEmpty(values, "BudgetPeriod", "");
//	addIfNonEmpty(values, "TxType", "");
//	addIfNonEmpty(values, "TotalAmount", "");
//	addIfNonEmpty(values, "CashFlag", "");
//	addIfNonEmpty(values, "CashAmount", "");
//	addIfNonEmpty(values, "EmvDeclined", "");

	// Now populate

	string sCur = "R";
	string amount = getFmtAmount(sCur,batchrec.getTxAmount());
	values["Amount"] = amount;

	// TxSummary
	std::string sDt = batchrec.getTxDateTime();
	addIfNonEmpty(values, "Date", sDt.length()>=8 ? sDt.substr(0, 8) : sDt);
	addIfNonEmpty(values, "Time", sDt.length()>=12 ? sDt.substr(8, 4) : "");
	//addIfNonEmpty(values, "HcNo",NOTYETDONE);
	addIfNonEmpty(values, "AuthCode", " " + batchrec.getAuthCode());
	addIfNonEmpty(values, "ApprMode", batchrec.isApproveOnline() ? "1" : "0");
	addIfNonEmpty(values, "RecpNo", batchrec.getReceiptNo());
	addIfNonEmpty(values, "Tsn", SSTR(batchrec.getTsn()));

	if(batchrec.getRrn().length())
		addIfNonEmpty(values, "RRN", batchrec.getRrn());
	else
		addIfNonEmpty(values, "RRN", " ");

	if(UtilsStringToInt(batchrec.getRespCode()) >= 0)
		addIfNonEmpty(values, "RespCode", batchrec.getRespCode());
/*
	PAN_ENTRY_MODE emode = batchrec.getPanEntryMode();

	switch(emode)
	{
		case SWIPED:	addIfNonEmpty(values, "EntryMode", "S"); break;
		case ENTERED:	addIfNonEmpty(values, "EntryMode", "M"); break;
		case INSERTED:	addIfNonEmpty(values, "EntryMode", "I"); break;
		case TAPPED:	addIfNonEmpty(values, "EntryMode", "T"); break;
		case FALLBACK:	addIfNonEmpty(values, "EntryMode", "F"); break;
		case UNKNOWN: break;
	}
*/
	string entrymode;
	AuthMode authmode = (AuthMode)batchrec.getAuthMode();

	switch(authmode)
	{
		case za_co_vfi_Transaction::TERMINAL:		entrymode = "T"; break;
		case za_co_vfi_Transaction::HOST:			entrymode = "H"; break;
		case za_co_vfi_Transaction::VOICE:			entrymode = "V"; break;
		case za_co_vfi_Transaction::DEF_ANALYSIS:	entrymode = "D"; break;
	}

	PAN_ENTRY_MODE emode = batchrec.getPanEntryMode();

	switch(emode)
	{
		case SWIPED:	entrymode.append("S"); break;
		case ENTERED:	entrymode.append("M"); break;
		case INSERTED:	entrymode.append("I"); break;
		case TAPPED:	entrymode.append("T"); break;
		case FALLBACK:	entrymode.append("F"); break;
		case UNKNOWN: break;
	}

	addIfNonEmpty(values, "EntryMode", entrymode);
//////////////////////////////////////////////
	// references
	addIfNonEmpty(values, "RefNoFlag1", batchrec.getRefNo1().empty() ? "" : " ");
	addIfNonEmpty(values, "RefNo1", batchrec.getRefNo1());
	addIfNonEmpty(values, "RefNoFlag2", batchrec.getRefNo2().empty() ? "" : " ");
	addIfNonEmpty(values, "RefNo2", batchrec.getRefNo2());

	string masked_pan=batchrec.getPan();
	if(masked_pan.length()){
		masked_pan.replace(6, masked_pan.length()-10, masked_pan.length()-10, '*');
		addIfNonEmpty(values, "Pan", masked_pan);
	}

	if (batchrec.getBudgetPeriod() != "0")
		addIfNonEmpty(values, "BudgetPeriod", batchrec.getBudgetPeriod());

	std::string iso_tx_code = SSTR(batchrec.getTxCode());
	if((iso_tx_code == SSTR(TX_ISO_SALE)) || (iso_tx_code == SSTR(TX_ISO_SALE_CASH))){
		string str_tx = TuiString("TxSale");
		addIfNonEmpty(values,"TxType",str_tx);
		addIfNonEmpty(values,"TotalAmount",amount);
	}

	if((iso_tx_code == SSTR(TX_ISO_CASH_ADVANCE)) || (iso_tx_code == SSTR(TX_ISO_SALE_CASH))){
		string str_tx = TuiString("TxCash");
		addIfNonEmpty(values,"TxType",str_tx);

		string cash_amount = getFmtAmount(sCur,batchrec.getTxAmount());
		addIfNonEmpty(values,"CashAmount",cash_amount);
		addIfNonEmpty(values, "CashFlag", " ");
	}
	// addIfNonEmpty(values, "EmvDeclined", "");

	// Finally we print
	{
		vector<string>lines;

		if(TuiReport("TxSummary", lines, values) == 0)
			return -1;

		za_co_vfi_reports::Report report(lines);
		report.print();
	}
	return 0;

}

int EmvDataLogReport::doReport(void) {

    CBatchManager batchManager;
    CBatch batch;

    int ret = batchManager.getBatch(batch);
    if (ret != DB_OK) {
        log.message((MSG_INFO "Error getting batch, code=") + SSTR(ret) + "\n");
        za_co_vfi_Operator::Operator op;
        op.BatchNoxTx();
	} else {
		std::map<unsigned int, CBatchRec> batchRecs;
		ret = batch.getRecords(batchRecs);

		if (ret != DB_OK){
			log.message((MSG_INFO "Error getting batch records, code=") + SSTR(ret) + "\n");
			za_co_vfi_Operator::Operator op;
			op.BatchNoxTx();
		}else{
			//printHeaders(batch, batchRecs.size());

			if (batchRecs.size() > 0) {
				map<string, string>values;
				vector<string>lines;

				addIfNonEmpty(values, "LastEFlag", " ");
				if(TuiReport("LastEmvDataLogHeader", lines, values) == 0)
					return -1;


				std::map<unsigned int,CBatchRec>::reverse_iterator  it;

				for (it=batchRecs.rbegin(); it!= batchRecs.rend(); ++it) {
					CBatchRec batchRec = it->second;

					if(batchRec.isInProgress() || batchRec.isTxCanceled()|| (batchRec.getPanEntryMode() != INSERTED) ) {
						//log.message(MSG_INFO "skip " + getFmtAmount("R",batchRec.getTxAmount()) + "\n");
						continue;
					}

					string emvtlv = batchRec.getEmvTags();
					BERTLV tlv(emvtlv);
					int pos=0;

					while(1){
						string tag, len, val;
						int ret;
						ret = tlv.getTagLenVal(pos, tag, len, val);

						if(ret){
							string line = UtilsHexToString(tag.c_str(), tag.length())+" ("+UtilsHexToString(len.c_str(), len.length())+"):"
									+UtilsHexToString(val.c_str(), val.length())+"\n";
							lines.push_back(line);

							//log.message(MSG_INFO+line);

							pos = ret;
						}
						else
							break;
					}

					break; // exit after first found
				}

				za_co_vfi_reports::Report report(lines);
				report.print(true);
			}

		}
	}

	return 0;
};


//
// Setup reports
//

int SetupReport::MessageToMerchantReport(void) {
	com_verifone_terminalconfig::CTerminalConfig termcfg;
	string msg = termcfg.getMessageToMerchant();

	if (msg.empty()) {
		return -1;
	} else {
		map<string, string>values;
		vector<string>lines;

		addIfNonEmpty(values, "AddMsg", termcfg.getMessageToMerchant());
		if(TuiReport("MsgToMerch", lines, values) == 0)
			return -1;

		za_co_vfi_reports::Report report(lines);
		return report.print(true);
	}
}

int SetupReport::ConfigReport(bool sigPanel)
{
	map<string, string>values;
	com_verifone_terminalconfig::CTerminalConfig termcfg;

	printHeaders();

	char value[255];
	int result=0;
	unsigned long rsize=0;

	result = platforminfo_get(PI_SERIAL_NUM ,value, sizeof(value), &rsize);
	if (result == PI_OK) addIfNonEmpty(values, "SerialNo", string(value,rsize));

	addIfNonEmpty(values, "FlagSettleReq", "");
	addIfNonEmpty(values, "AutoParamTime", termcfg.getParamDnldTime());
	addIfNonEmpty(values, "AutoSettleTime", termcfg.getSettlementTime());
	addIfNonEmpty(values, "MaxBatch", UtilsIntToString(termcfg.getBatchMax()));

	string term_func_bmp = termcfg.getTerminalFunctionBitMap();
	char byte1 = term_func_bmp.at(0);

	if (byte1 & 1){
		log.message(MSG_INFO "Manual PAN entry is enabled\n");
		addIfNonEmpty(values, "ManPan", TuiString("EnabledStr"));
	}else{
		log.message(MSG_INFO "Manual PAN entry is disabled\n");
		addIfNonEmpty(values, "ManPan", TuiString("DisabledStr"));
	}

	addIfNonEmpty(values, "TrailerText", termcfg.getTrailerText());

	za_co_vfi_MenuBmp::MenuBmp menu(termcfg.getTerminalMenuBitMap());
	string txtypes;
	if(menu.SalesPurchases()) 				txtypes.append(TuiString("MenuPurchase")+"\r\n");
	if(menu.ReturnsRefunds()) 				txtypes.append(TuiString("MenuRefund")+"\r\n");
	if(menu.ChangePin()) 					txtypes.append(TuiString("MenuChangePin")+"\r\n");
	if(menu.CorrectionsReverse()) 			txtypes.append(TuiString("MenuCorrection")+"\r\n");
	if(menu.BalanceEnquiry()) 				txtypes.append(TuiString("MenuBalEnq")+"\r\n");
	if(menu.DepositPayment()) 				txtypes.append(TuiString("MenuPayment")+"\r\n");
	if(menu.CashWithdrawal()) 				txtypes.append(TuiString("MenuCash")+"\r\n");
	if(menu.SalesPurchasesWithCashback()) 	txtypes.append(TuiString("MenuSaleCash")+"\r\n");
	if(menu.CardActivation()) 				txtypes.append(TuiString("MenuCardAct")+"\r\n");
	if(menu.MpadFundsLoad()) 				txtypes.append(TuiString("MenuFundsLoad")+"\r\n");
	if(menu.MpadFundsUnload()) 				txtypes.append(TuiString("MenuFundsUnload")+"\r\n");
	if(menu.MpadStatement()) 				txtypes.append(TuiString("MenuMPSatement")+"\r\n");
	if(menu.MpadBalanceEnquiry()) 			txtypes.append(TuiString("MenuMPBalEnq")+"\r\n");
	if(menu.PreAuth()) 						txtypes.append(TuiString("MenuPreAuth")+"\r\n");
	if(menu.PreAuthCompletion()) 			txtypes.append(TuiString("MenuPreAuthCompl")+"\r\n");
	if(menu.PreAuthCancel()) 				txtypes.append(TuiString("MenuPreAuthCancel")+"\r\n");
	if(menu.VoiceOverride()) 				txtypes.append(TuiString("MenuVoiceOverride")+"\r\n");
	if(menu.QuasiCashWithdrawal()) 			txtypes.append(TuiString("MenuQuasiCash")+"\r\n");
	if(menu.CardIssue()) 					txtypes.append(TuiString("MenuCardIssue")+"\r\n");
	if(menu.CardReload()) 					txtypes.append(TuiString("MenuCardReload")+"\r\n");
	if(menu.GiftCardBalanceEnquiry()) 		txtypes.append(TuiString("MenuGiftBalEnq")+"\r\n");
	if(menu.OrderReserve()) 				txtypes.append(TuiString("MenuOrderRes")+"\r\n");
	if(menu.OrderFulfill()) 				txtypes.append(TuiString("MenuOrderFulF")+"\r\n");
	if(menu.Redemption()) 					txtypes.append(TuiString("MenuRedeem")+"\r\n");

	addIfNonEmpty(values, "TxTypes", txtypes);

	addIfNonEmpty(values, "AddMsg", termcfg.getMessageToMerchant());

	CBatchManager batchManager;
	CBatch batch;
	batchManager.getBatch(batch);

	addIfNonEmpty(values, "BatchNo", UtilsIntToString(batch.getBatchNo()));

	// Count the number of completed transactions in the batch
	int tx_count=0;
	std::map<unsigned int, CBatchRec>records;
	batch.getRecords(records);

	std::map<unsigned int, CBatchRec>::iterator it;

	for(it=records.begin(); it != records.end(); ++it){
		CBatchRec rec = it->second;

		if(!rec.isInProgress() && !rec.isTxCanceled())
			tx_count++;
	}

	addIfNonEmpty(values, "TxCount", UtilsIntToString(tx_count));

	com_verifone_hotcard::CHotcard hotcard;
	string hc_version;
	hotcard.getVersion(hc_version);
	addIfNonEmpty(values, "HcFileNo", hc_version);
	addIfNonEmpty(values, "HcDate", "");
	addIfNonEmpty(values, "HcTotal", UtilsIntToString(hotcard.getRecordCount()));

	vector<string>lines;

	if(TuiReport("SetupReport", lines, values) == 0)
		return -1;

	za_co_vfi_reports::Report report(lines);
	report.print();

	if(sigPanel){
		lines.clear();
		values.clear();

		string cust = TuiString("CustStr");

		addIfNonEmpty(values, "Cardholder", cust);
		addIfNonEmpty(values, "SigFlag", " ");

		if(TuiReport("RcptSigPanel", lines, values) == 0)
				return -1;

		report.print(lines);

		lines.clear();
		values.clear();

		string tech = TuiString("TechStr");

		addIfNonEmpty(values, "Cardholder", tech);
		addIfNonEmpty(values, "SigFlag", " ");

		if(TuiReport("RcptSigPanel", lines, values) == 0)
				return -1;

		za_co_vfi_reports::Report report2(lines);
		report.print(lines);
	}

	report.advanceAtEnd();

	return 0;
}

int printHeaders(void)
{
	string NOTYETDONE = "______";
	map<string, string>values;
	com_verifone_terminalconfig::CTerminalConfig termcfg;

	// Populate Header Values
	addIfNonEmpty(values, "Header1", termcfg.getMerchantName());
	addIfNonEmpty(values, "Header2", "");
	addIfNonEmpty(values, "Header3", "");
	addIfNonEmpty(values, "Header4", "");
	std::string sDt = getCurrentDateTime();

	struct timeb tp;
	ftime(&tp);
	char bufdate[15], buftime[15];

	memset(bufdate, 0, sizeof(bufdate));
	memset(buftime, 0, sizeof(buftime));

	strftime(bufdate,sizeof(bufdate),"%Y/%02m/%02d",localtime(&tp.time));
	strftime(buftime,sizeof(buftime),"%02H:%02M:%02S",localtime(&tp.time));

	addIfNonEmpty(values, "Date", string(bufdate));
	addIfNonEmpty(values, "Time", string(buftime));
/*
	addIfNonEmpty(values, "TermType", NOTYETDONE);
	addIfNonEmpty(values, "AppBuildNo", NOTYETDONE);
	addIfNonEmpty(values, "AppVersion", NOTYETDONE);
*/
	addIfNonEmpty(values, "MerchNo", termcfg.getMerchantNo());
	addIfNonEmpty(values, "Tid", termcfg.getTerminalNo());

	// Finally we print
	{
		vector<string>lines;

		if(TuiReport("RcptHeader", lines, values) == 0)
			return -1;

		za_co_vfi_reports::Report report(lines);
		report.print();
	}

	return 0;
}

int CardParameterReport::CardParamReport(void)
{
	bad_swipe = false;

	testAndSetBusyFlag();
	readMedia(string("\x23\x25"),30);

	while(1){
		LAST_COMMAND_TO_RESPOND last_command = getLastCommandToComplete();

		// First check the status of the last command
		if((last_command != NONE) && (pam_status != 0)){
			log.message(MSG_INFO "Error occured, pam_status=" + UtilsIntToString(pam_status) + "\n");
			clearBusyFlag();
			return -1;
		}

		if(last_command == READ_MEDIA){
			if(bad_swipe){
				za_co_vfi_Operator::Operator::displayErrorMsg("CardReadError");
				return 1;
			}else{
				break;
			}
		}

		usleep(100*1000);		// Sleep for 100 milli seconds nto prevent tight loop
	}

	log.message(MSG_INFO "Pan retrieved [" + track_data.pan() + "\n");

	com_verifone_cardconfig::CCardConfig cardcfg;
	com_verifone_carddata::CCardData cardData;
	com_verifone_terminalconfig::CTerminalConfig termcfg;

	if(cardcfg.lookupCardConfig(track_data.pan(), cardData, termcfg.getCurrencyCode()) <= 0){
		log.message(MSG_INFO "Card not found\n");
		za_co_vfi_Operator::Operator::displayErrorMsg("CardNotFoundError");
		return 1;
	}

	com_verifone_cardprofile::CCardProfile card_prof = cardData.getCardProfile();

	map<string, string>values;
	addIfNonEmpty(values, "Pan", track_data.pan());
	addIfNonEmpty(values, "Bin", cardData.getBin());

	cardData.isExcluded() ? addIfNonEmpty(values, "Excluded", TuiString("StrYes")): addIfNonEmpty(values, "Excluded", TuiString("StrNo"));
	card_prof.isActive() ? addIfNonEmpty(values, "Active", TuiString("StrYes")): addIfNonEmpty(values, "Active", TuiString("StrNo"));
	card_prof.isForeignCard() ? addIfNonEmpty(values, "Foreign", TuiString("StrYes")): addIfNonEmpty(values, "Foreign", TuiString("StrNo"));

	card_prof.isCheckServiceCode() ? addIfNonEmpty(values, "Src", TuiString("StrYes")): addIfNonEmpty(values, "Src", TuiString("StrNo"));
	card_prof.isCheckExpiryDate() ? addIfNonEmpty(values, "Exp", TuiString("StrYes")): addIfNonEmpty(values, "Exp", TuiString("StrNo"));
	card_prof.isCheckLuhn() ? addIfNonEmpty(values, "Luhn", TuiString("StrYes")): addIfNonEmpty(values, "Luhn", TuiString("StrNo"));
	card_prof.isForeignCard() ? addIfNonEmpty(values, "Foreign", TuiString("StrYes")): addIfNonEmpty(values, "Foreign", TuiString("StrNo"));

	addIfNonEmpty(values, "PanLen", UtilsIntToString(cardData.getCardLength()));
	addIfNonEmpty(values, "MinPin", UtilsIntToString(card_prof.getPinLenMin()));
	addIfNonEmpty(values, "MaxPin", UtilsIntToString(card_prof.getPinLenMax()));
//addIfNonEmpty(values, "CardProf", cardData.cardProfile.active);

	vector<string>lines;

	if(TuiReport("CardReport", lines, values) == 0)
		return -1;

	printHeaders();

	za_co_vfi_reports::Report report(lines);
	report.print();
	lines.clear();

	std::map<std::string,CAccountProfile> accounts = cardData.getAccounts();
	std::map<std::string,CAccountProfile>::iterator it;
	int counter = 0;

	for (it=accounts.begin(); it!=accounts.end(); ++it) {
		map<string, string>acc_prof_values;
		CAccountProfile accountProfile = it->second;

		counter++;
		addIfNonEmpty(acc_prof_values, "count", UtilsIntToString(counter));
		addIfNonEmpty(acc_prof_values, "total", UtilsIntToString(accounts.size()));

		addIfNonEmpty(acc_prof_values, "AccType", accountProfile.getCode());
		addIfNonEmpty(acc_prof_values, "TotGrp", accountProfile.getTotalsGroupName());
		addIfNonEmpty(acc_prof_values, "SupTotGrp", accountProfile.getSuperTotalsGroupName());
		accountProfile.isDraftCaptureMode() ? addIfNonEmpty(acc_prof_values, "DraftCap", TuiString("StrOnl")): addIfNonEmpty(acc_prof_values, "DraftCap", TuiString("StrOffl"));

		accountProfile.isAllowVoiceApproval() ? addIfNonEmpty(acc_prof_values, "VoiceAppr", TuiString("StrYes")): addIfNonEmpty(acc_prof_values, "VoiceAppr", TuiString("StrNo"));
		accountProfile.isAllowManualEntry() ? addIfNonEmpty(acc_prof_values, "ManEnt", TuiString("StrYes")): addIfNonEmpty(acc_prof_values, "ManEnt", TuiString("StrNo"));
		accountProfile.isRequirePinEntry() ? addIfNonEmpty(acc_prof_values, "PinReq", TuiString("StrYes")): addIfNonEmpty(acc_prof_values, "PinReq", TuiString("StrNo"));
		accountProfile.isRequireSignature()? addIfNonEmpty(acc_prof_values, "SigReq", TuiString("StrYes")): addIfNonEmpty(acc_prof_values, "SigReq", TuiString("StrNo"));
		accountProfile.isAllowBudget() ? addIfNonEmpty(acc_prof_values, "BudgAll", TuiString("StrYes")): addIfNonEmpty(acc_prof_values, "BudgAll", TuiString("StrNo"));

		addIfNonEmpty(acc_prof_values, "MinBudg", accountProfile.getBudgetAmountMin());

		std::set<std::string> allowedTransactions = accountProfile.getAllowedTransactions();
		std::set<std::string>::iterator it2;
		string allowed_tx;

		for (it2=allowedTransactions.begin(); it2!=allowedTransactions.end(); ++it2) {
			std::string allowTx = *it2;

			allowed_tx.append(allowTx + ", ");
			log.message("Transaction Type       - " + allowTx + "\n");
		}

		allowed_tx.resize(allowed_tx.length()-2);
		addIfNonEmpty(acc_prof_values, "TxAll", allowed_tx);

		com_verifone_limits::CLimits limit = accountProfile.getLocalLimits();
		addIfNonEmpty(acc_prof_values, "LlCash", limit.getCashbackAmtMax());
		addIfNonEmpty(acc_prof_values, "LlFl", limit.getFloorLimitAmt());
		addIfNonEmpty(acc_prof_values, "LlMax", limit.getTransactionAmtMax());
		addIfNonEmpty(acc_prof_values, "ElCash", limit.getCashbackAmtMax());
		addIfNonEmpty(acc_prof_values, "ElFl", limit.getFloorLimitAmt());
		addIfNonEmpty(acc_prof_values, "ElMax", limit.getTransactionAmtMax());
		addIfNonEmpty(acc_prof_values, "OlCash", limit.getCashbackAmtMax());
		addIfNonEmpty(acc_prof_values, "OlFl", limit.getFloorLimitAmt());
		addIfNonEmpty(acc_prof_values, "OlMax", limit.getTransactionAmtMax());
		addIfNonEmpty(acc_prof_values, "ClCash", limit.getCashbackAmtMax());
		addIfNonEmpty(acc_prof_values, "ClFl", limit.getFloorLimitAmt());
		addIfNonEmpty(acc_prof_values, "ClMax", limit.getTransactionAmtMax());

		vector<string>acc_prof_lines;

		if(TuiReport("AttAccReport", acc_prof_lines, acc_prof_values) == 0)
			return -1;

//		za_co_vfi_reports::Report report(acc_prof_lines);
		report.print(acc_prof_lines);
	}

	report.advanceAtEnd();

	return 0;
}

int CardParameterReport::readMediaResult(string sStatusCode, string sActiveInterface,
				string sAtr, string sTrack1, string sTrack2, string sTrack3,
				string sSwipeStatus, string sManualPan, string sManualExpiryDate,
				string sManualCvv, string sBarcodeData)
{
	(void)sActiveInterface;
	(void)sAtr;
	(void)sSwipeStatus;
	(void)sManualPan;
	(void)sManualExpiryDate;
	(void)sManualCvv;
	(void)sBarcodeData;

	pam_status = (int)sStatusCode.at(0);

	log.message(MSG_INFO "---> pam_status		: [" + UtilsHexToString(sStatusCode.c_str(), sStatusCode.length()) + "]\r\n");
	log.message(MSG_INFO "---> sSwipeStatus		: [" + UtilsHexToString(sSwipeStatus.c_str(), sSwipeStatus.length()) + "]\r\n");

	if(pam_status != 0){
		log.message(MSG_INFO "---> sStatusCode		: [" + UtilsHexToString(sStatusCode.c_str(), sStatusCode.length()) + "]\r\n");
		return -1;
	}

	if(sSwipeStatus.at(0) != 0){
		bad_swipe = true;
		log.message(MSG_INFO "---> Bad swipe\r\n");
		return 0;
	}

	if(sActiveInterface.at(0) == 0x23){
			log.message(MSG_INFO "---> Magnetic card swiped\r\n");
			log.message(MSG_INFO "---> Track1			: [" + sTrack1 + "]\n");
			log.message(MSG_INFO "---> Track2			: [" + sTrack2 + "]\n");
			log.message(MSG_INFO "---> Track3			: [" + sTrack3 + "]\n");

			if(sTrack2.length()){
				TrackData mag(sTrack1, sTrack2, sTrack3);
				track_data = mag;
			}
	}

	return 0;
}

}
