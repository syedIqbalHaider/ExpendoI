#include <string>
#include <unistd.h>
#include <gui/gui.h>
#include <gui/jsobject.h>
#include <libtui/tui.h>
#include <libda/ccardconfig.h>
#include <libda/chotcard.h>
#include <libda/cterminalconfig.h>

#include "Transaction.h"
#include "utils.h"
#include "CLog.h"
#include "Operator.h"
#include "CPAver.h"
#include "string.h"
#include "cstatus.h"
static CLog log(CPA_NAME);

//using namespace std;
using namespace Utils;
using namespace vfigui;
using namespace za_co_verifone_tui;
using namespace za_co_vfi_Operator;
using namespace com_verifone_hotcard;
using namespace com_verifone_status;

namespace za_co_vfi_Transaction
{

OfflineMini::~OfflineMini()
{
}

int OfflineMini::DoTransaction(map<string, string>&txdata, EMVTxType txType)
{
	(void)txdata;
	log.message(MSG_INFO "OfflineMini - DoTransaction\r\n");

	initBatchRec();

	string interfaces;
	//	interfaces.append(1, (char)INTF_MAG);
	//	interfaces.append(1, (char)INTF_MANUAL);
	interfaces.append(1, (char)INTF_ICC);

	log.message(MSG_INFO "interfaces enable ["+UtilsHexToString(interfaces.c_str(), interfaces.length())+"]\n");

	statementEntries.clear();
	apdMode = APD_NONE;
	Operator op;

	CStatus status;
	status.setStatus(WAITING_FOR_CARD_INSERT);

	dont_remove_card = false;
	//import_txdata(txdata);
	for(map<string,string>::iterator it=txdata.begin(); it != txdata.end(); ++it){
		if(it->first.compare(TX_DATA_TX_DONT_REMOVE_CARD) == 0){
			string dRC = it->second;
			if (dRC.compare("1") == 0)
				dont_remove_card = true;

			log.message(MSG_INFO "-->" + string(TX_DATA_TX_DONT_REMOVE_CARD) + ":" + dRC + "\r\n");
		}
	}

	wait_for_h_command_response = false;
	for(map<string,string>::iterator it=txdata.begin(); it != txdata.end(); ++it){
		if(it->first.compare(TX_DATA_TX_H_CMD_ENABLED) == 0){
			string dRC = it->second;
			if (dRC.compare("1") == 0)
				wait_for_h_command_response = true;

			log.message(MSG_INFO "-->" + string(TX_DATA_TX_H_CMD_ENABLED) + ":" + dRC + "\r\n");
		}
	}

	string sInputTlv = getEMVTagTxType(txType).getStringData()+
			getEMVTagTxDate().getStringData()+
			getEMVTagTxTime().getStringData();

	string sRequestedTlv =  string("\x9f\x02"\
			"\x9f\x03"\
			"\x4f"\
			"\x82"\
			"\x9f\x36"\
			"\x9f\x07"\
			"\x8a"\
			"\xdf\x01"\
			"\xdf\x02"\
			"\x9f\x26"\
			"\x9f\x27"\
			"\x8e"\
			"\x9f\x34"\
			"\x9f\x1e"\
			"\x9f\x0d"\
			"\x9f\x0e"\
			"\x9f\x0f"\
			"\x9f\x10"\
			"\xdf\x09"\
			"\x9f\x09"\
			"\x9f\x33"\
			"\x9f\x1a"\
			"\x9f\x35"\
			"\x95"\
			"\x9f\x53"\
			"\x5f\x2a"\
			"\x9a"\
			"\x9f\x41"\
			"\x9f\x4d"\
			"\x9c"\
			"\x9f\x37"\
			"\xcb"\
			"\x9f\x6e"\
			"\x9f\x7c"\
			"\x5a"\
			"\x50"\
			"\x57"\
			"\x5f\x20"\
			"\x5f\x24"\
			"\x5f\x25"\
			"\x5f\x30"\
			"\x5f\x34"\
			"\x9f\x20");

	testAndSetBusyFlag();
	int iRc=startTransaction(interfaces,sInputTlv, sRequestedTlv);
	int cnt=0;

	log.message(MSG_INFO "pam.startTransaction()="+log.from_int(iRc)+"\r\n");

	while(1){
		LAST_COMMAND_TO_RESPOND last_command = getLastCommandToComplete();
		cnt++;

		// First check the status of the last command
		if((last_command != NONE) && (pam_status != 0)){
			log.message(MSG_INFO "Error occured, pam_status=" + UtilsIntToString(pam_status) + "\n");
			removeCardPrompt();
			return -1;
		}

	   if(last_command == MODIFY_LIMITS){
		   log.message(MSG_INFO "pam.getLastCommandToRespond()=MODIFY_LIMITS\r\n");
		   recNr = 0;
		   maxRecs = 1;
		   emvSendOfflineMiniStat();
		   cnt=0;
	   }else if(last_command == CANCEL_TRANSACTION){
			log.message(MSG_INFO "Cancelled transaction remotely\r\n");

			dont_remove_card = false;		// In case it was set

			return -1;

	   }else if(last_command == APDU){
		   log.message(MSG_INFO "pam.getLastCommandToRespond()=APDU\r\n");
		   log.message(MSG_INFO "---> ApduResult:          : [" + UtilsHexToString(emv_apdu_result.c_str(), emv_apdu_result.length()) + "]\r\n");

		   string sTag = emv_apdu_result.substr(0,2);
		   if (sTag == string("\x9f\x4f")) apdMode = APD_GOT_FMT;
		   else
			   apdMode = APD_GOT_REC;

		   switch (apdMode)
		   {
			   case APD_NONE: break;
			   case APD_GOT_FMT: { // tag 4f
					   // ApduResult:  [9f4f119f27019f02065f2a029a039f36029f52069000]

					   dataDesc = "";
					   // Get data from emv_apdu_result
					   if ((emv_apdu_result.length()>=8) && (sTag == emv_apdu_result.substr(0,2))
									   && (emv_apdu_result.substr(emv_apdu_result.length()-2,1) == "\x90")) {
							   BERTLV  tlv(emv_apdu_result);
							   string sVal  = "";
							   if (tlv.getValForTag(sTag,sVal)) {
									   dataDesc = sVal;
									   recNr = 1;
									   // Logentry should have been returned in ModifyLimitsRequest
									   if (logEntry.length()>=2) {
											   sfi = logEntry.at(0);
											   maxRecs = logEntry.at(1);

											   emvSendOfflineMiniStatRecNr(recNr,sfi);
											   cnt=0;
									   } else {
											   log.message(MSG_INFO "No log entries on card.\n");
											   return -1;
									   }
							   }
					   }
					   if (dataDesc.length() == 0) {
							   log.message(MSG_INFO "Value for tag [" + UtilsHexToString(sTag.c_str(),sTag.length()) + "] not found\n");
							   return -1;
					   }
					   break;
			   }
			   case APD_GOT_REC: { // tag B2
					   // ApduResult:         : [000000000000000978140708028e8500030423019000]
					   if ((emv_apdu_result.length()>=8) //&& (sTag == emv_apdu_result.substr(0,2))
									   && (emv_apdu_result.substr(emv_apdu_result.length()-2,1) == "\x90")) {
							   //log.message(MSG_INFO "minstat maxRecs="+log.from_int(maxRecs)+",nr="+ log.from_int(recNr) +"\r\n");

							   addMiniStatementLine(recNr, dataDesc,emv_apdu_result);

							   if (recNr >= maxRecs) {
									   // end
									   testAndSetBusyFlag();
									   iRc=modifyLimits(PAM_FORCE_DECLINE);
									   log.message(MSG_INFO "pam.modifyLimits()="+log.from_int(iRc)+"\r\n");
							   } else {
									   recNr++;
									   emvSendOfflineMiniStatRecNr(recNr,sfi);
							   }
							   cnt=0;
					   }
					   break;
			   }
			   case APD_DONE: {
					   break;
			   }
		   } // end switch APDUmode

		}else if(last_command == START_TRANSACTION){
		   log.message(MSG_INFO "pam.getLastCommandToRespond()=START_TRANSACTION\r\n");

		   op.miniStatDisp(statementEntries);

		   txCompleteOffline(EMV_TX_OFFLINE_MIN_STAT); // Busy flag is set inside this in emvcomplete
		   return 0; // we ignore return code
		   break;
		}

		usleep(100*1000);		// Sleep for 100 milli seconds
	}

	return -1;
}

int OfflineMini::addMiniStatementLine(int nr, string dDesc, string sNextApdu)
{
	// Code below copied from working C code version. Did not try
	// to convert to c++

	unsigned char uc9F02[6];  // 000000001000
	unsigned char uc9A[3];
	unsigned char uc9F27;
	memset(uc9F02, 0, sizeof(uc9F02));
	memset(&uc9F27, 0, sizeof(uc9F27));
	memset(uc9A, 0, sizeof(uc9A));

	// Parse input data flags
	for (uint j = 0/*3*/, k = 0; j < dDesc.length() - 2;) {
		short usTag = dDesc.at(j++) << 8;
		if ((usTag & 0x1f00) == 0x1f00) {
			usTag |= dDesc.at(j++);
		}
		//log.message(MSG_INFO " - Tag: [" + UtilsHexToString((const char*)(&usTag),sizeof(usTag)) + "]\n");

		unsigned char ucTagLen = dDesc.at(j++);

		if (usTag == (short)0x9F02) {
			memcpy(uc9F02, &sNextApdu[k], ucTagLen);
			//log.message(MSG_INFO " - 9f02: [" + UtilsHexToString((const char*)(&uc9F02),sizeof(uc9F02)) + "]\n");
		}
		if (usTag == (short)0x9F27) {
			memcpy(&uc9F27, &sNextApdu[k], ucTagLen);
			//log.message(MSG_INFO " - 9f27: [" + UtilsHexToString((const char*)(&uc9F27),sizeof(uc9F27)) + "]\n");
		}
		if (usTag == (short)0x9A00) {
			memcpy(uc9A, &sNextApdu[k], ucTagLen);
			//log.message(MSG_INFO " - 9a: [" + UtilsHexToString((const char*)(&uc9A),sizeof(uc9A)) + "]\n");
		}
		k += ucTagLen;
	}

	// Get formatted Amount  [000000000010000978140708056d8500030430019000]
	string sAmount = "R"; // hardcoded!
	for (int iIndex = 0, iLeadinZero = 1; iIndex < 6; iIndex++) {
		if (iLeadinZero && uc9F02[iIndex] == 0){
			if(iIndex==4) sAmount += "0.";
			if(iIndex==5)  sAmount += "00";
			continue;
		}
		if (iLeadinZero && (uc9F02[iIndex] & 0xF0) == 0){
			if(iIndex==5)  sAmount += "0";
			char c = uc9F02[iIndex] & 0x0F ;
			sAmount += UtilsHexToString(&c,1);
		}else{
			sAmount += UtilsHexToString((const char *)(&uc9F02[iIndex]),1);
		}
		iLeadinZero = 0;
		if (iIndex == 4) sAmount += ".";
	}
	if (sAmount.length() < 3)
		sAmount = "R0.00";

	// Add entry
	string sNr = UtilsIntToString(nr);
	statementEntries["NrRecs"] = sNr;
	statementEntries["txAm" + sNr] = sAmount;

	//Add Date and Transaction Result
	statementEntries["txDt"+sNr] = UtilsHexToString((const char *)uc9A,3);
	statementEntries["txRes"+sNr] = ((uc9F27 & 0xC0) == 0x40 ? "A" : "D");

	string sLine =  statementEntries["txDt"+sNr] + "         "
					+ statementEntries["txRes"+sNr] + " "  + sAmount;
	log.message(MSG_INFO "MS Line " + sNr + ": " + sLine + "\r\n");

	return 0;
}

int OfflineMini::emvSendOfflineMiniStat(void)
{
	std::string sApdu = string("\x80\xca\x9f\x4f\x00",5);
	log.message(MSG_INFO "emvOfflineMiniStat:  [" + UtilsHexToString(sApdu.c_str(), sApdu.length()) + "]\r\n");

	testAndSetBusyFlag();
	int iRes = apdu(0,sApdu);

	return iRes;
}

int OfflineMini::emvSendOfflineMiniStatRecNr(char nr, char sfi)
{
	std::string sApdu = string("\x00\xb2.\x04\x00",5);
	sApdu.at(2) = nr;
	sApdu.at(3) |= (sfi <<3);
	log.message(MSG_INFO "emvOfflineMiniStatRecNr:  [" + UtilsHexToString(sApdu.c_str(), sApdu.length()) + "]\r\n");

	testAndSetBusyFlag();
	int iRes = apdu(0,sApdu);

	return iRes;
}


}
