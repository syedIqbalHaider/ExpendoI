#include <fcntl.h>
#include <unistd.h>
#include "Printer.h"

namespace za_co_vfi_prn
{

Prn::Prn()
{
	fd = open("/dev/lp0", O_RDWR);

	if(fd)
		initialise();
}

Prn::~Prn()
{
	if(fd > 0)
		close(fd);
}

bool Prn::status()
{
/*
	int ret;
	string cmd("\x1d\x72\x33");
	write(fd, cmd.c_str(), 1);

	char buffer[1];
	ret = read(fd, buffer, sizeof(buffer));
	return (int)buffer[0];
*/
	if(fd <= 0)
		return false;

	return true;
}

int Prn::justify_centre(bool mode)
{
	string cmd;

	if(mode)
		cmd.append("\x1b\x61\x31");
	else
		cmd.append("\x1b\x61\x30");

	return write(fd, cmd.c_str(), cmd.length());

}

int Prn::justify_right(bool mode)
{
	string cmd;

	if(mode)
		cmd.append("\x1b\x61\x32");
	else
		cmd.append("\x1b\x61\x30");

	return write(fd, cmd.c_str(), cmd.length());

}

int Prn::initialise(void)
{
	string cmd("\x1b\x40\x0c\x1b\x53");
	return write(fd, cmd.c_str(), 5);
}

int Prn::underline(bool mode)
{
	string cmd;

	if(mode)
		cmd.append("\x1b\x2d\x31");
	else
		cmd.append("\x1b\x2d\x30");

	return write(fd, cmd.c_str(), cmd.length());
}

int Prn::bold(bool mode)
{
	string cmd;

	if(mode)
		cmd.append("\x1b\x47\x01");
	else
		cmd.append("\x1b\x47\x00");

	return write(fd, cmd.c_str(), 3);
}

int Prn::inverse(bool mode)
{
	string cmd;

	if(mode)
		cmd.append("\x1d\x42\x01");
	else
		cmd.append("\x1d\x42\x00");

	return write(fd, cmd.c_str(), 3);
}

int Prn::cut(void)
{
	string cmd("\x1d\x56\x01");
	return write(fd, cmd.c_str(), 3);
}

int Prn::upsidedown(bool mode)
{
	string cmd;

	if(mode)
		cmd.append("\x1b\x7b\x01");
	else
		cmd.append("\x1b\x7b\x00");

	return write(fd, cmd.c_str(), 3);
}

int Prn::print(string msg)
{
	return write(fd, msg.c_str(), msg.length());
}

int Prn::feed(int no)
{
	string cmd("\x1b\x64");
	cmd.append(string((char*)&no, 1));
	return write(fd, cmd.c_str(), 3);
}

int Prn::getWidth(void)
{
	return 42;
}

}
