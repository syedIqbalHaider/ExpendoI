#include <set>
#include <unistd.h>
#include <gui/gui.h>
#include <gui/jsobject.h>
#include <libtui/tui.h>
#include "xmlmenu.h"
//#include <libipc/ipc.h>
#include <libda/cterminalconfig.h>
#include <libda/ccommssettings.h>
#include <libda/ccommsconfig.h>
#include <libhim.h>

#include <libda/cconmanager.h>
#include <libda/cbatchmanager.h>
#include <libda/cbatch.h>

#include "CAuthUser.h"
#include "Operator.h"
#include "CPAver.h"
#include "utils.h"
#include "CLog.h"
#include "Reports.h"
#include "cstatus.h"
#include "alarm.h"

static CLog log(CPA_NAME);
//using namespace std;
using namespace Utils;
using namespace vfigui;
using namespace za_co_verifone_tui;
using namespace za_co_verifone_xmlmenu;
using namespace com_verifone_terminalconfig;
using namespace com_verifone_user;
using namespace com_verifone_batchmanager;
using namespace com_verifone_status;
using namespace za_co_vfi_reports;
using namespace com_verifone_commsconfig;
using namespace com_verifone_commssettings;
using namespace com_verifone_terminalconfig;

#include <iostream>
//using namespace std;

void addToConnectTime(bool isParameters);

namespace za_co_vfi_Operator{


//iq_audi_291216
std::string MsgRespCode(int respCode)
{

	switch (respCode) {
	case 0:		return std::string("Approved"); // Successful
	case 1:		return std::string("Refer To Card Issuer"); // Refer To Card Issuer
	case 2:		return std::string("Refer To Card Issuer Special"); // Refer To Card Issuer Special
	case 3:		return std::string("Invalid Merchant"); // Invalid Merchant
	case 4:		return std::string("Pick Up"); // Pick Up
	case 5:		return std::string("Do Not Honour"); // Do Not Honour
	case 6:		return std::string("Error"); // Error
	case 7:		return std::string("Pick Up Special"); // Pick Up Special
	case 8:		return std::string("Honour With ID"); // Honour With ID
	case 9:		return std::string("Request In Progress"); // Request In Progress
	case 10: 	return std::string("Approved Partial"); // Approved Partial
	case 11: 	return std::string("Approved VIP"); // Approved VIP
	case 12:	return std::string("Invalid Tran"); // Invalid Tran
	case 13:	return std::string("Invalid Amount"); // Invalid Amount
	case 14:	return std::string("Invalid Card Number"); // Invalid Card Number
	case 15:	return std::string("No Such Issuer"); // No Such Issuer
	case 16:	return std::string("Approved Update Track 3"); // Approved Update Track 3
	case 17:	return std::string("Customer Cancellation");	// Customer Cancellation
	case 19:	return std::string("Re-enter Transaction"); // Re-enter Transaction
	case 20:	return std::string("Invalid response");	//Invalid response
	case 21:	return std::string("No Action Taken"); // No Action Taken
	case 22:	return std::string("Suspected Malfunction"); // Suspected Malfunction
	case 23:	return std::string("Unacceptable Tran Fee"); // Unacceptable Tran Fee
	case 24:	return std::string("File Update Not Supported"); // File Update Not Supported
	case 25:	return std::string("Unable To Locate Record"); // Unable To Locate Record
	case 26:	return std::string("Duplicate Record"); // Duplicate Record
	case 27:	return std::string("File Update Edit Error"); // File Update Edit Error
	case 28:	return std::string("File Update File Locked"); // File Update File Locked
	case 29:	return std::string("File Update Failed"); // File Update Failed
	case 30:	return std::string("Format Error"); // Format Error
	case 31:	return std::string("Bank Not Supported"); // Bank Not Supported
	case 33:	return std::string("Expired Card Pick Up"); // Expired Card Pick Up
	case 34:	return std::string("Suspected Fraud Pick Up"); // Suspected Fraud Pick Up
	case 35:	return std::string("Contact Acquirer Pick Up"); // Contact Acquirer Pick Up
	case 36:	return std::string("Restricted Card Pick Up"); // Restricted Card Pick Up
	case 37:	return std::string("Call Acquirer Security Pick Up"); // Call Acquirer Security Pick Up
	case 38:	return std::string("PIN Tries Exceeded Pick Up"); // PIN Tries Exceeded Pick Up
	case 39:	return std::string("No Credit Account"); // No Credit Account
	case 40:	return std::string("Function Not Supported"); // Function Not Supported
	case 41:	return std::string("Lost Card"); // Lost Card
	case 43:	return std::string("Stolen Card"); // Stolen Card
	case 51:	return std::string("Not Sufficient Funds"); // Not Sufficient Funds
	case 54:	return std::string("Expired Card"); // Expired Card
	case 55:	return std::string("Incorrect PIN"); // Incorrect PIN
	case 56:	return std::string("No Card Record"); // No Card Record
	case 57:	return std::string("Tran Not Permitted Cardholder"); // Tran Not Permitted Cardholder
	case 58:	return std::string("Tran Not Permitted Terminal"); // Tran Not Permitted Terminal
	case 59:	return std::string("Suspected Fraud Declined"); // Suspected Fraud Declined
	case 60:	return std::string("Contact Acquirer"); // Contact Acquirer
	case 61:	return std::string("Exceeds Withdrawal Limit"); // Exceeds Withdrawal Limit
	case 62:	return std::string("Restricted Card"); // Restricted Card
	case 63:	return std::string("Security Violation"); // Security Violation
	case 65:	return std::string("Exceeds Withdrawal Frequency"); // Exceeds Withdrawal Frequency
	case 66:	return std::string("Call Acquirer Security"); // Call Acquirer Security
	case 68:	return std::string("Response Received Too Late"); // Response Received Too Late
	case 75:	return std::string("PIN Tries Exceeded"); // PIN Tries Exceeded
	case 81:	return std::string("PIN Cryptographic Error Found"); // PIN Cryptographic Error Found
	case 82:	return std::string("Negative CAM, dCVV, iCVV, or CVV results"); // Negative CAM, dCVV, iCVV, or CVV results
	case 90:	return std::string("Cut-off In Progress"); // Cut-off In Progress
	case 91:	return std::string("Issuer Or Switch Inoperative"); // Issuer Or Switch Inoperative
	case 93:	return std::string("Violation Of Law"); // Violation Of Law
	case 95:	return std::string("Reconcile Error"); // Reconcile Error
	case 96:	return std::string("Reconcile Error if batch no. not found/match"); // Reconcile Error if batch no. not found/match iq_audi_180117
	default:	return std::string("Default Error"); // Contact Acquirer
	}
}











void checkAuthenticated(CAuthUser &cu) {

	if (!cu.isAuthenticated()) {
		int r = cu.authenticate();

		if (r == 0) {

			log.message("isAuthenticated=" + SSTR(cu.isAuthenticated()) + "\r\n");
			log.message("isBankingAllowed=" + SSTR(cu.isBankingAllowed())+ "\r\n");
			log.message("getDateAdded=" + SSTR(cu.getDateAdded())+ "\r\n");
			log.message("getId=" + SSTR(cu.getId())+ "\r\n");
			log.message("getName=" + SSTR(cu.getName())+ "\r\n");
			log.message("getPin=" + SSTR(cu.getPin())+ "\r\n");
			log.message("isPinRequired=" + SSTR(cu.isPinRequired())+ "\r\n");
			log.message("isRefundsAllowed=" + SSTR(cu.isRefundsAllowed())+ "\r\n");
			log.message("getUserType=" + SSTR(cu.getUserType())+ "\r\n");
		}

//		// Test BatchReport
//		using namespace za_co_vfi_reports;
//		BatchReport br;
//
//		//br.previousBatch();
//		br.currentBatch();
//

	}
}


Operator::Operator()
{
	//CAuthUser cu;
	//checkAuthenticated(cu);
}


int Operator::accountSelection(std::map<std::string, com_verifone_accountprofile::CAccountProfile>&accounts)
{
	int count=0;
	struct UIMenuEntry  *txmenu = new UIMenuEntry[accounts.size()];

	for(map<std::string, com_verifone_accountprofile::CAccountProfile>::iterator it=accounts.begin(); it != accounts.end(); ++it){
		CAccountProfile acc = it->second;
		txmenu[count].text = acc.getDescription();
		txmenu[count].value = count+1;
		txmenu[count].options = 0;
		count++;
	}

	// If there is only one account, then select it by default
	if(count == 1)
		return 1;

	string menu_header = TuiString("AccSelectMenuHeader");
	int selection = uiMenu("menu", menu_header, txmenu, count, 1);

	delete[] txmenu;

	if(selection > 0)
		return selection;

	return 0;
}

int Operator::budgetPeriodSelection(void)
{
	const string per[] = {"0", "6", "12", "18", "24", "36", "48", "52", "60"};
	int sz = sizeof(per)/sizeof(per[0]);
	struct UIMenuEntry *budget_menu = new UIMenuEntry[sz];
	int count=0;

	for (int i=0;i<sz;i++) {
		string id = per[i];
		id.append("months");
		budget_menu[count].text = TuiString(id);
		budget_menu[count].value = UtilsStringToInt(per[i]);
		budget_menu[count].options = 0;

		count++;
	}

	string menu_header = TuiString("BudgetPeriodHeader");
	int selection = uiMenu("menu", menu_header, budget_menu, count, 1);

	delete[] budget_menu;

	if(selection >= 0)
		return selection;

	return -1;	// Tx canceled
}

std::string Operator::displayErrorMsg(string id)
{
	string message, error_tmpl;
	message = TuiString(id);

	map<string,string> values;
	values["Message"] = message;

	TuiTemplate("bgimage.tmpl", "ErrorMessage", error_tmpl);

	uiInvoke(values,error_tmpl);

	return message; // return error message for possible use
}

void Operator::waitDisp(void)
{
	string message;
	TuiTemplate("WaitDisp", message);
	uiDisplay("bgimage",message);
}

void Operator::clearReversalDisp(void)
{
	string message;
	TuiTemplate("ClReverDisp", message);
	uiDisplay("bgimage",message);
}

void Operator::approvedDisp(void)
{
	string message;
	TuiTemplate("ApprovedDisp", message);
	uiDisplayAsync("bgimage",message);
}

void Operator::pinChangedDisp(void)
{
	string message;
	TuiTemplate("PinChangedDisp", message);
	uiDisplayAsync("bgimage",message);
}

void Operator::balanceDisp(string sBalance)
{
	string html;
	map<string,string> values;
	values["balance"] = sBalance;

	TuiTemplate("bgimage.tmpl", "BalanceDisp", html);
	uiInvoke(values,html);
}

void Operator::miniStatDisp(map<string,string> &statementEntries)
{
	// Note that the nth entry is defined in the map by the names
	//  txDt[n],txRes[n],txAm[n] - e.g.  tcDt7,txRes7,txAm7
	// Nr of records is given by entry NrRecs

	log.message(MSG_INFO "display ministat\n");

	string sLine =  statementEntries["txDt1"] + "         "
					+ statementEntries["txRes1"] + " "  + statementEntries["txAm1"];
	log.message(MSG_INFO "MS Line 1:" + sLine + "\r\n");

	string html;
	TuiTemplate("bgimage.tmpl", "MiniStatDisp", html);
	uiInvoke(statementEntries,html);
}

int reprintReceipt(int receiptNo)
{
	int ret;
	bool match_found=false;
	com_verifone_batchmanager::CBatchManager batchManager;
	com_verifone_batch::CBatch cbatch;
	com_verifone_batchrec::CBatchRec batchrec;

	ret = batchManager.getBatch(cbatch);

	if (ret != DB_OK){
		log.message(MSG_INFO "reprintReceipt(): Failure retrieving batch record\r\n");
		return -1;
	}

	std::map<unsigned int, com_verifone_batchrec::CBatchRec> records;
	cbatch.getRecords(records);

	std::map<unsigned int,CBatchRec>::iterator it;
	for (it=records.begin(); it!=records.end(); ++it) {
		CBatchRec batchRec = it->second;

		string str_receipt_no = batchRec.getReceiptNo();

		// Check for specific record
		if((receiptNo > 0) && (UtilsStringToInt(str_receipt_no) == receiptNo)){
			match_found = true;
			batchrec = batchRec;
			break;
		}else if((receiptNo == 0) && (!batchRec.isInProgress())){
			match_found = true;
			batchrec = batchRec;
		}
	}

	if(!match_found){
		log.message(MSG_INFO "reprintReceipt(): No match found for receipt number "+UtilsIntToString(receiptNo)+"\n");
		return -2;
	}

	Receipt receipt(batchrec);

	if(batchrec.isApproved()){
		if(receipt.merchantCopy(true) < 0)
				log.message(MSG_INFO "reprintReceipt(): Receipt merchant copy failed\n");

		if(receipt.customerCopy(true) < 0)
			log.message(MSG_INFO "reprintReceipt(): Receipt customer copy failed\n");
	}else{
		receipt.declined();
	}

	return 0;
}

void Operator::declinedDisp(int response_code)
{
	string tmpl;

	map<string,string> values;
	values["RespCode"] = UtilsIntToString(response_code);

	TuiTemplate("bgimage.tmpl", "DeclineDisp", tmpl);

	uiInvoke(values,tmpl);
}

void Operator::declinedRespDisp(string response_msg)
{
	string tmpl;

	map<string,string> values;
	values["RespMsg"] = response_msg;

	TuiTemplate("bgimage.tmpl", "DeclineDispResp", tmpl);

	uiInvoke(values,tmpl);
}



void Operator::bankingFailed(void)
{
	string message;
	TuiTemplate("BankingFailedDisp", message);
	uiDisplayAsync("bgimage",message);
}

void Operator::batchFull(void)
{
	string message;
	TuiTemplate("BatchFullDisp", message);
	uiDisplayAsync("bgimage",message);
}

void Operator::batchEmpty(void)
{
	string message;
	TuiTemplate("BatchEmptyDisp", message);

	//	uiDisplayAsync("bgimage",message);
	uiDisplay("bgimage",message);
}

void Operator::BatchNoxTx(void)
{
	string message;
	TuiTemplate("BatchNoxTx", message);
	uiDisplay("bgimage",message);
}

int Operator::getCCD(string &ccd)
{
	string tmpl;
	int iret;

	log.message(MSG_INFO "getCCD() start\n");

	TuiTemplate("CCDInput", tmpl);

	log.message(MSG_INFO + tmpl);

	vector<string> value(1);

	iret = uiInput("bgimage",value,tmpl);
	if(iret == UI_ERR_OK) {
		log.message(MSG_INFO "getCCD() [" + value[0] + "]\n");
		ccd = value[0];
		return 0;
	}

	log.message(MSG_INFO "getCCD() return error\n");

	return -1;
}
// Consolidate 5 download calls over here because we need to print and clear
// merchant msg afterwards
int Operator::doParameterDownload(CStatus &status, bool isFull, bool mustPrintMessage) {
	int retCode;
	if (isFull)
		log.message(MSG_INFO"Full Parameter Download ...\n");
	else
		log.message(MSG_INFO"Partial Parameter Download ...\n");

	za_co_vfi_Him::Him him;
	status.setStatus(PARM_DOWNLOAD_STARTING);
	if (isFull)
		retCode = him.FullParameters();
	else
		retCode = him.PartialParameters();

	status.setStatus(PARM_DOWNLOAD_COMPLETE);

	if (retCode) {
		log.message(MSG_INFO"PrmDownloadError.\n");
	} else {
		log.message(MSG_INFO"Downloading parameters ... done\r\n");
	}

	// Log merchant message if any and delete afterwards
	if (mustPrintMessage && !retCode) {
		SetupReport setup_rprt;
		if (setup_rprt.MessageToMerchantReport() == 0) {
			CTerminalConfig termcfg;
			termcfg.setMessageToMerchant("");
		}
	}
	return retCode;
}

int menuAction(int selValue, std::string menuId) {
	(void)menuId;
	CStatus status;
	int ret;
	log.message(MSG_INFO"menuAction():" + menuId + "\n");

	switch (selValue) {
		case 101:
			{
				log.message(MSG_INFO"Settlement selected\n");

				za_co_vfi_Operator::Operator op;
				status.setStatus(BANKING_STARTED);
				ret = op.doSettlement();
				status.setStatus(BANKING_COMPLETE);
				return ret;
			}
		case 102:	addToConnectTime(true); break;
		case 103:	addToConnectTime(false); break;
		case 110:
		case 111: {
				za_co_vfi_Operator::Operator op;
				bool isFull = (selValue == 110);
				op.doParameterDownload(status,isFull, true);

				AlarmResetParmAlarm();

				break;
			}
		case 120: log.message(MSG_INFO"Users to be implemented\n"); break;
		case 121: {
			SetupReport setup_rprt;
			setup_rprt.ConfigReport(false);

			break;
		}
		case 122: {
			CardParameterReport card_rprt;

			while(card_rprt.CardParamReport() > 0);

			break;
		}
		case 123: {
			EmvDataLogReport emvRep;
			emvRep.doReport();

			break;
		}
		case 131:
		case 132: {
					BatchReport br;
					if (selValue == 131)
						br.currentBatch();
					else
						br.previousBatch();
					selValue = 0;

			break;
		}
		case 230:
			if(reprintReceipt(0) < 0){
				Operator op;
				op.BatchNoxTx();
			}

			break;
		case 231:
// TODO: Get the receipt number
			if(reprintReceipt(0) < 0){
				Operator op;
				op.BatchNoxTx();
			}

			break;
	}
	log.message(MSG_INFO"menuAction() return:" + SSTR(selValue) + "\n");

	return selValue;
}

std::string Operator::doOperatorMenu(std::string command) {
	std::string menuCode = command.substr(5);
	if (menuCode.empty()) menuCode = "SM";
	XmlMenu xm(menuCode);
	int r = xm.selectXmlMenu(0,menuAction,true);

	return UtilsIntToString(r);
}

int Operator::doSettlement(void)
{
	// This code will be replaced by more efficient tx count
	int ret;
	com_verifone_batchmanager::CBatchManager batchManager;
	com_verifone_batch::CBatch cbatch;

	ret = batchManager.getBatch(cbatch);

	if (ret != DB_OK){
		log.message(MSG_INFO "Failure retrieving batch record\r\n");
		return -1;
	}

	std::map<unsigned int, com_verifone_batchrec::CBatchRec> records;
	cbatch.getRecords(records);

	// Check that we have transactions to settle
	if(records.size() == 0){
		log.message(MSG_INFO "DoBanking(): Batch "+UtilsIntToString(cbatch.getBatchNo())+" is empty, nothing to do\n");
		batchEmpty();
		return 0;
	}
	////////////////////

	za_co_vfi_Him::Him him;

	if(him.DoBanking() == 0){
		log.message(MSG_INFO "Printing batch report\n");
		za_co_vfi_reports::BatchReport brprt;
		brprt.previousBatch();
	}else{
		log.message(MSG_INFO "Banking failed\n");
		bankingFailed();
		return -1;
	}
	return 0;
}

int Operator::doCpaSetup(void) {
	CTerminalConfig termcfg;
	int ret = 0;

	if (!termcfg.isSetupComplete()){
		if(UtilsGetTerminalType() == TT_MX_925)
			ret = do_mx_setup();
		else if(UtilsGetTerminalType() == TT_UX_300)
			ret = do_ux_setup();
	}

	return ret;
}

typedef enum{
	SS_MERCH_ID,
	SS_TERM_ID,
	SS_IP,
	SS_PORT,
	SS_COMPLETE
}SETUP_STATE;

int Operator::do_ux_setup(void)
{
	CTerminalConfig termcfg;
	CCommsConfig commscfg;
	CCommsSettings prmSettings;
	std::map<string,string> values;

	commscfg.getPriParamCommsSettings(prmSettings);

	values["merchantid"] = termcfg.getMerchantNo();
	values["terminalid"] = termcfg.getTerminalNo();
	std::string ip = prmSettings.getIp();
	if (ip.empty()) ip = "0.0.0.0";
	values["ipaddress"] = ip;
	values["port"] = prmSettings.getPort();

	SETUP_STATE state=SS_MERCH_ID;

	while(1){
		string tmpl;
		int ret;

		switch(state){
			case SS_MERCH_ID:	ret = TuiTemplate("setup.tmpl", "SetupMerchId", tmpl); break;
			case SS_TERM_ID:	ret = TuiTemplate("setup.tmpl", "TerminalId", tmpl); break;
			case SS_IP:			ret = TuiTemplate("setup.tmpl", "Ip", tmpl); break;
			case SS_PORT:		ret = TuiTemplate("setup.tmpl", "Port", tmpl); break;
			case SS_COMPLETE:   break;
		}

		ret = uiInvoke(values,tmpl);

		switch(state){
			case SS_MERCH_ID:
				if(ret == 1) state = SS_TERM_ID;
				break;
			case SS_TERM_ID:
				if(ret == 1) state = SS_IP;
				else if(ret == 0) state = SS_MERCH_ID;
				break;
			case SS_IP:
				if(ret == 1) state = SS_PORT;
				else if(ret == 0) state = SS_TERM_ID;
				break;
			case SS_PORT:
				if(ret == 1) state = SS_COMPLETE;
				else if(ret == 0) state = SS_IP;
				break;
			case SS_COMPLETE:   break;
		}

		string merchantId = values["merchantid"];
		string terminalId = values["terminalid"];
		string ipaddress = values["ipaddress"];
		string port = values["port"];

		if(!merchantId.empty())
			termcfg.setMerchantNo(merchantId);

		if(!terminalId.empty())
			termcfg.setTerminalNo(terminalId);

		if(!ipaddress.empty())
			prmSettings.setIp(ipaddress);

		if(!port.empty())
			prmSettings.setPort(port);

		commscfg.setPriParamCommsSettings(prmSettings);

		// Check for when we are finished
		if((ret == 1) && (state == SS_COMPLETE)){
			za_co_vfi_Him::Him him;

			log.message(MSG_INFO"Downloading parameters ...\n");

			if (him.FullParameters()) {
				log.message(MSG_INFO"FirstPrmDownloadError.\n");

				termcfg.setSetupComplete(false);

				TuiTemplate("SetupError", tmpl);
				uiDisplay("bgimage",tmpl);

				state = SS_MERCH_ID;

			} else {
				// Success so we set the Complete flag
				termcfg.setSetupComplete(true);

				SetupReport rprt;
				rprt.ConfigReport(true);

				break;
			}

			log.message(MSG_INFO"Downloading parameters ... done\r\n");
		}

	}

	return 0;
}

int Operator::do_mx_setup(void) {
	int rc = 0;
	CTerminalConfig termcfg;
	CCommsConfig commscfg;
	CCommsSettings prmSettings;
	std::map<string,string> values;

	commscfg.getPriParamCommsSettings(prmSettings);

	values["merchantid"] = termcfg.getMerchantNo();
	values["terminalid"] = termcfg.getTerminalNo();
	std::string ip = prmSettings.getIp();
	if (ip.empty()) ip = "0.0.0.0";
	values["ipaddress"] = ip;
	values["port"] = prmSettings.getPort();

	std::string tmpl;
	rc = TuiTemplate("bgimage.tmpl", "SetupDetail", tmpl);
	if (rc != 0) {
		while(1) {
			uiDisplay("<center>Cannot read template.<br> Please contact your IT department.</center>");
			usleep(100*1000);		// Sleep for 10 milli seconds
		}
	}

	bool stayHere = true;
	rc = 0;
	while (stayHere) {

		rc = uiInvoke(values,tmpl); // should we use asynch methods?

		switch (rc) {
			//case 0: break;
			case UI_ERR_WAIT_TIMEOUT:
			case UI_ERR_TIMEOUT:
				break;
			//case 1: stayHere = false;		break; // cancel not supported
			case 0: {
				std::string merchantId = values["merchantid"];
				std::string terminalId = values["terminalid"];
				std::string ipaddress = values["ipaddress"];
				std::string port = values["port"];

				// Validate
				values["errmsg"] = "";
				bool isValidated = true;
				if (merchantId.empty() || (merchantId == "???")) {
					isValidated = false;
					values["merchantid"] = "???";
				}
				if (terminalId.empty() || (terminalId == "???")) {
					isValidated = false;
					values["terminalid"] = "???";
				}
				if (port.empty() || (port == "???")) {
					isValidated = false;
					values["port"] = "???";
				}
				if (ipaddress.empty() || (ipaddress == "0.0.0.0") ) {
					isValidated = false;
					values["ipaddress"] = "0.0.0.0";
				}

				// Set values
				if (isValidated) {
					log.message(MSG_INFO"saving setup values,compl=" + SSTR(termcfg.isSetupComplete()) + "\n");
					termcfg.setMerchantNo(merchantId);
					termcfg.setTerminalNo(terminalId);

					prmSettings.setIp(ipaddress);
					prmSettings.setPort(port);
					commscfg.setPriParamCommsSettings(prmSettings);

					// Download parameters
					{
						// Workaround so that popup messages when downloading does not get
						// hidden when you click on them
						uiDisplay("");
						//waitDisp(); // this fails - display  wheel in front all the time

						CStatus status;
						if (doParameterDownload(status,true,false)) {
							termcfg.setSetupComplete(false);
							values["errmsg"] = TuiString("FirstPrmDownloadError");
							displayErrorMsg("FirstPrmDownloadError");
						} else {
							// Success so we set the Complete flag
							termcfg.setSetupComplete(true);
							rc = 0;
							stayHere = false;

							SetupReport rprt;
							rprt.ConfigReport(true);
						}
					}
				} else {
					values["errmsg"] = TuiString("ValuesEnteredError");
					log.message(MSG_INFO"ValuesEnteredError.\n");
				}
				//dumpMap(values);
				break;
			}
		}
		usleep(100*1000);
	}

	std::string sRet = UtilsIntToString(rc);
	log.message("end CpaSetup, rc=" + sRet);
	return rc;
}


}
