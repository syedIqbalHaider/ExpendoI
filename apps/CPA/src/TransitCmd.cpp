#include <string>
#include <unistd.h>
#include <gui/gui.h>
#include <gui/jsobject.h>
#include <libtui/tui.h>
#include <libda/ccardconfig.h>
#include <libda/chotcard.h>
#include <libda/cterminalconfig.h>


#include "Transaction.h"
#include "utils.h"
#include "CLog.h"
#include "Operator.h"
#include "CPAver.h"
#include "cstatus.h"

static CLog log(CPA_NAME);

//using namespace std;
using namespace Utils;
using namespace vfigui;
using namespace za_co_verifone_tui;
using namespace za_co_vfi_Operator;
using namespace com_verifone_hotcard;
using namespace com_verifone_status;

namespace za_co_vfi_Transaction
{

TransitCmd::~TransitCmd()
{
}

int TransitCmd::DoTransaction(map<string, string>&txdata, EMVTxType txType)
{
	log.message(MSG_INFO "Transit - DoTransaction\r\n");

	initBatchRec();

	string interfaces;

	if(emv_fallback_required){
		emvFallback();
		log.message(MSG_INFO "emvFallback() not allowed\n");
		return -1;
	}else
		interfaces.append(1, (char)INTF_ICC);

	log.message(MSG_INFO "interfaces enable ["+UtilsHexToString(interfaces.c_str(), interfaces.length())+"]\n");

	iso_tx_code = txType; // for now we have txtype=iso code and use this to avoid creating several classes

	Operator op;

	dont_remove_card = false;
	import_txdata(txdata);

	CStatus status;
	status.setStatus(WAITING_FOR_CARD_INSERT);

	getLastCommandToRespond();

	string sInputTlv = getEMVTagTxType(txType).getStringData()+
			getEMVTagTxDate().getStringData()+
			getEMVTagTxTime().getStringData();

	string sRequestedTlv =  string("\x9f\x02"\
			"\x9f\x03"\
			"\x4f"\
			"\x82"\
			"\x9f\x36"\
			"\x9f\x07"\
			"\x8a"\
			"\xdf\x01"\
			"\xdf\x02"\
			"\x9f\x26"\
			"\x9f\x27"\
			"\x8e"\
			"\x9f\x34"\
			"\x9f\x1e"\
			"\x9f\x0d"\
			"\x9f\x0e"\
			"\x9f\x0f"\
			"\x9f\x10"\
			"\xdf\x09"\
			"\x9f\x09"\
			"\x9f\x33"\
			"\x9f\x1a"\
			"\x9f\x35"\
			"\x95"\
			"\x9f\x53"\
			"\x5f\x2a"\
			"\x9a"\
			"\x9f\x41"\
			"\x9c"\
			"\x9f\x37"\
			"\xcb"\
			"\x9f\x6e"\
			"\x9f\x7c"\
			"\x5a"\
			"\x50"\
			"\x57"\
			"\x5f\x20"\
			"\x5f\x24"\
			"\x5f\x25"\
			"\x5f\x30"\
			"\x5f\x34"\
			"\x9f\x20");

	if(txType != EMV_TX_PIN_CHANGE)
	{
		sInputTlv += getEMVTagTxAmount().getStringData();
		sInputTlv += getEMVTagTxCurreny().getStringData();
	}

	testAndSetBusyFlag();
	int iRc=startTransaction(interfaces,sInputTlv, sRequestedTlv);

	log.message(MSG_INFO "pam.startTransaction()="+log.from_int(iRc)+"\r\n");

	while(1){
		LAST_COMMAND_TO_RESPOND last_command = getLastCommandToComplete();

		// First check the status of the last command
		if((last_command != NONE) && (pam_status != 0)){
			log.message(MSG_INFO "Error occured, pam_status=" + UtilsIntToString(pam_status) + "\n");
			removeCardPrompt();
			return -1;
		}

		if(last_command == MODIFY_LIMITS){
			log.message(MSG_INFO "pam.getLastCommandToRespond()=MODIFY_LIMITS\r\n");

			if(pan_validation_failed){
				testAndSetBusyFlag();
				cancelTransaction();
				log.message(MSG_INFO "Canceling because pan validation failed.\r\n");
				continue;
			}

			iRc = emvSendApduCB();
			log.message(MSG_INFO "pam.modifyLimits,emvSendApduCB()="+log.from_int(iRc)+"\r\n");

		}else if(last_command == CANCEL_TRANSACTION){
			log.message(MSG_INFO "Cancelled transaction remotely\r\n");

			dont_remove_card = false;		// In case it was set

			return -1;

		} else if(last_command == APDU){
			log.message(MSG_INFO "pam.getLastCommandToRespond()=APDU\r\n");
			log.message(MSG_INFO "<--- ApduResult:          : [" + UtilsHexToString(emv_apdu_result.c_str(), emv_apdu_result.length()) + "]\r\n");

			if(emv_apdu_result.at(0) == '\xcb'){
				iRc = processApduData("\xcb");
				emvSendApdu9F50();
				continue;
			}else{
				// Get balance from emv_apdu_result
				string sTag;

				if (emv_apdu_result.length() >= 2)
					sTag = emv_apdu_result.substr(0,2);

				if ((emv_apdu_result.length()>=8) && (sTag.compare("\x9f\x50") == 0) && (emv_apdu_result.substr(emv_apdu_result.length()-2,1) == "\x90")) {
					tag_9f_50 = emv_apdu_result.substr(0, emv_apdu_result.length()-2);
					log.message(MSG_INFO "9F50 [" + UtilsHexToString(tag_9f_50.c_str(), tag_9f_50.length()) + "]\r\n");
				}
			}
			// Note - we ignore tag not present (i.e iRc != 0)

			testAndSetBusyFlag();
			iRc=modifyLimits(PAM_FORCE_ONLINE,0);
			log.message(MSG_INFO "pam.modifyLimits()="+log.from_int(iRc)+"\r\n");

        }else if(last_command == START_TRANSACTION)
		{
			log.message(MSG_INFO "pam.getLastCommandToRespond()=START_TRANSACTION\r\n");

			// Check if fallback processing need to be done
			if(emv_fallback_required){
				log.message(MSG_INFO "START_TRANSACTION says do fallback to mag, canceling transaction instead.\n");
				emv_fallback_required = false; // We're not going to do fallback on a balance enquiry
				return 1;
			}

			log.message(MSG_INFO "<--- EMVADD: [" + UtilsHexToString(emv_tag_data.c_str(), emv_tag_data.length()) + "]\r\n");
			// Replace emvtags with apdu values
			BERTLV tlv(emvtlv);
			tlv.replaceTags(emv_tag_data);

			// Add tag 9f50 if it is present
			if(tag_9f_50.length() > 0){
				BERTLV card_balance(tag_9f_50);

				string tag, len, val;
				card_balance.getTagLenVal(0, tag, len, val);
				tlv.appendTag(tag, val);
			}

			emvtlv = tlv.getStringData();
			batchrec.setEmvTags(emvtlv);

			log.message(MSG_INFO "New EMV tags [" + UtilsHexToString(tlv.getStringData().c_str(), tlv.getStringData().length()) + "]\r\n");

			//
			// Complete the transaction
			//
			if(txComplete(wait_for_h_command_response) < 0)
				return -1;
			else
				return 0;
		}

		usleep(100*1000);		// Sleep for 100 milli seconds nto prevent tight loop
	}

	return -1;
}


//
// Protected methods
//

int TransitCmd::import_txdata(map<string, string>&txdata)
{
	int count=0;

	for(map<string,string>::iterator it=txdata.begin(); it != txdata.end(); ++it){
		if(it->first.compare(TX_DATA_TX_AMOUNT) == 0){
			string amt = it->second;
//			tx_sale_amount = UtilsHexToString(amt.c_str(), amt.length());
			tx_sale_amount = amt;
			log.message(MSG_INFO "-->" + string(TX_DATA_TX_AMOUNT) + ":" + tx_sale_amount + "\r\n");
		}else if(it->first.compare(TX_DATA_TX_CURRENCY) == 0){
			string curr = it->second;
			txn_currency = curr;
			log.message(MSG_INFO "-->" + string(TX_DATA_TX_CURRENCY) + ":" + txn_currency + "\r\n");
		}
		else if(it->first.compare(TX_DATA_TX_DONT_REMOVE_CARD) == 0){
			string dRC = it->second;
			if (dRC.compare("1") == 0)
				dont_remove_card = true;

			log.message(MSG_INFO "-->" + string(TX_DATA_TX_DONT_REMOVE_CARD) + ":" + dRC + "\r\n");
		} else if(it->first.compare(TX_DATA_TX_PRODUCT) == 0){
			// The data is already in surrounding ff02 tag (packed by serial interface)
			emv_tag_data =  it->second;
			log.message(MSG_INFO "-->" + string(TX_DATA_TX_PRODUCT) + ": ["	+ UtilsHexToString(it->second.c_str(), it->second.length()) + "]\r\n");
		} else if(it->first.compare(TX_DATA_TX_H_CMD_ENABLED) == 0){
			string dRC = it->second;
			if (dRC.compare("1") == 0)
				wait_for_h_command_response = true;

			log.message(MSG_INFO "-->" + string(TX_DATA_TX_H_CMD_ENABLED) + ":" + dRC + "\r\n");
		} else if(it->first.compare(TX_DATA_VALIDATE_PAN) == 0){
			required_pan = it->second;
			validate_pan = true;
			log.message(MSG_INFO "-->" + string(TX_DATA_VALIDATE_PAN) + ":" + required_pan + "\r\n");
		}
	}

	return count;
}


int TransitCmd::emvSendApduCB(void)
{
	std::string sApdu = string("\x80\xca\x00\xcb\x00",5);
	log.message(MSG_INFO "emvSendApduCB:  [" + UtilsHexToString(sApdu.c_str(), sApdu.length()) + "]\r\n");

	testAndSetBusyFlag();
	int iRes = apdu(0,sApdu);

	return iRes;
}

int TransitCmd::emvSendApdu9F50(void)
{
	std::string sApdu = string("\x80\xca\x9f\x50\x00",5);
	log.message(MSG_INFO "emvSendApduCB:  [" + UtilsHexToString(sApdu.c_str(), sApdu.length()) + "]\r\n");

	testAndSetBusyFlag();
	int iRes = apdu(0,sApdu);

	return iRes;
}

int TransitCmd::apduResult(string sStatusCode,string sApduResponse) {
	log.message(MSG_INFO "apduResult status:<--[" + UtilsHexToString(sStatusCode.c_str(), sStatusCode.length()) + "]\r\n");
	emv_apdu_status = sStatusCode;
	emv_apdu_result = sApduResponse;
	return(-1);
}


// sTagExpected should be length 1
int TransitCmd::processApduData(std::string sTagExpected) {
	string tagData = "";

	// Get data from emv_apdu_result
	std::string sTagResp =  (emv_apdu_result.length()>=2) ? emv_apdu_result.substr(0,2) : "";

	log.message(MSG_INFO "<---- DEBUG sTagResp ["+ UtilsHexToString(sTagResp.c_str(), sTagResp.length()) + "]\r\n");

	if (sTagExpected == string("\xcb"))
		sTagResp = sTagResp.substr(0,1);
	else if (sTagResp.length() > 1)
		sTagResp = sTagResp.substr(1,1);

	if (sTagResp == string("\x6a\x88")) {

		// tag not found, append 0 length tag
		tagData = sTagExpected.substr(0,1) + string("\x00",1);

	} else  if ((emv_apdu_result.length()>=2) && (sTagResp == sTagExpected)	&& (emv_apdu_result.substr(emv_apdu_result.length()-2,1) == "\x90")) {

		// tag found, use everything upto before \x90
		tagData = emv_apdu_result.substr(0,emv_apdu_result.length()-2);
	}

	if (tagData.length() == 0) {
		log.message(MSG_INFO "Value for tag [" + UtilsHexToString(sTagExpected.c_str(),sTagExpected.length()) + "] not found, set 0 length.\n");
		return -1;
	} else {
		log.message(MSG_INFO "<---- Tag " + UtilsHexToString(sTagExpected.c_str(), sTagExpected.length()) + " [" + UtilsHexToString(tagData.c_str(), tagData.length()) + "]\r\n");
	}

	emv_tag_data.append(tagData.substr(0,tagData.length()));

	log.message(MSG_INFO "<--- APDTAG: [" + UtilsHexToString(emv_tag_data.c_str(), emv_tag_data.length()) + "]\r\n");

	return 0;
}


}
