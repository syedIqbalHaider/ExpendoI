#include <string>
#include <sstream>
#include <string.h>
#include <sys/timeb.h>

#include "TrackData.h"
#include "utils.h"

#include <iostream>

//using namespace std;
using namespace Utils;

namespace za_co_vfi_TrackData
{
	string TrackData::pan(void)
	{
		string pan;

		if(manual)
			return manpan;

		unsigned long end_pos = track2.find("=");

		if(end_pos == string::npos)
			return "";

		pan = track2.substr(1, end_pos - 1);

		return pan;
	}

	string TrackData::expiry(void)
	{
		string expiry;

		if(manual)
			return manexpiry;

		unsigned long eq_pos = track2.find("=");

		if(eq_pos == string::npos)
			return "";

		expiry = track2.substr(eq_pos+1, 4);

		return expiry;
	}

	string TrackData::CVV(void)
	{
		return mancvv;
	}



	int TrackData::toInt(const char c)
	{
	    return c-'0';
	}

	int TrackData::isLuhnVarified( const char *id)
	{
	    bool is_odd_dgt = true;
	    int s = 0;
	    const char *cp;

	    for(cp=id; *cp; cp++);
	    while(cp > id) {
	        --cp;
	        int k = toInt(*cp);
	        if (is_odd_dgt) {
	            s += k;
	        }
	        else {
	            s += (k!=9)? (2*k)%9 : 9;
	        }
		is_odd_dgt = !is_odd_dgt;
	    }
	    return 0 == s%10;
	}

	string TrackData::cardholder(void)
	{
		// Extract the ascii hex between start and end sentinel
		unsigned long start = track1.find_first_of('%', 0);
		unsigned long end = track1.find_first_of('?', start);
		string temp_track1 = track1.substr(start+1, end-start-1);;

		// Convert ascii hex data and extract cardholder name
cout << "TRACK DBG [" << temp_track1 << "]" << endl;
//		string tr1 = UtilsStringToHex(temp_track1);
		string tr1 = temp_track1;
		if((start = tr1.find_first_of('^', 0)) == string::npos)
			return "";

		if((end = tr1.find_first_of('^', start+1)) == string::npos)
			return "";

		return tr1.substr(start+1, end-start-1);
//		return UtilsHexToString(tr1.substr(start+1, end-start-1).c_str(), tr1.substr(start+1, end-start-1).length());
	}

	string TrackData::src(void)
	{
		string code;

		unsigned long eq_pos = track2.find("=");

		if(eq_pos == string::npos)
			return "";

		code = track2.substr(eq_pos+5, 3);

		return code;
	}

	bool TrackData::isExpired(void)
	{
		struct timeb tp;
		ftime(&tp);
		char buf[5];

		memset(buf, 0, sizeof(buf));
		strftime(buf,sizeof(buf),"%y%m",localtime(&tp.time));

		int cur, card;
		istringstream (string(buf)) >> cur;
		istringstream (expiry()) >> card;

		if(card < cur)
			return true;

		return false;
	}



	int IccValidation::toInt(const char c)
	{
	    return c-'0';
	}

	int IccValidation::isLuhnVarified( const char *id)
	{
	    bool is_odd_dgt = true;
	    int s = 0;
	    const char *cp;

	    for(cp=id; *cp; cp++);
	    while(cp > id) {
	        --cp;
	        int k = toInt(*cp);
	        if (is_odd_dgt) {
	            s += k;
	        }
	        else {
	            s += (k!=9)? (2*k)%9 : 9;
	        }
		is_odd_dgt = !is_odd_dgt;
	    }
	    return 0 == s%10;
	}



	bool TrackData::isManualEntered(void)
	{
		return manual;
	}

	string TrackData::getTrack1(void)
	{
		return track1;
	}

	string TrackData::getTrack2(void)
	{
		return track2;
	}

	string TrackData::getTrack3(void)
	{
		return track3;
	}


	bool IccValidation::isExpired(string expiryDate)
	{
		struct timeb tp;
		ftime(&tp);
		char buf[5];

		memset(buf, 0, sizeof(buf));
		strftime(buf,sizeof(buf),"%y%m",localtime(&tp.time));

		int cur, card;
		istringstream (string(buf)) >> cur;
		istringstream (expiryDate) >> card;

		if(card < cur)
			return true;

		return false;
	}

}
