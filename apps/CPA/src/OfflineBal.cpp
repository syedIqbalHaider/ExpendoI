#include <string>
#include <unistd.h>
#include <gui/gui.h>
#include <gui/jsobject.h>
#include <libtui/tui.h>
#include <libda/ccardconfig.h>
#include <libda/chotcard.h>
#include <libda/cterminalconfig.h>

#include "Transaction.h"
#include "utils.h"
#include "CLog.h"
#include "Operator.h"
#include "CPAver.h"
#include "cstatus.h"

static CLog log(CPA_NAME);

//using namespace std;
using namespace Utils;
using namespace vfigui;
using namespace za_co_verifone_tui;
using namespace za_co_vfi_Operator;
using namespace com_verifone_hotcard;
using namespace com_verifone_status;

namespace za_co_vfi_Transaction
{

string getBalanceAmount(const string sVal)
{
	string sBal;
	sBal=UtilsHexToString(sVal.c_str(),sVal.length());
	int bal=UtilsStringToInt(sBal);
	if (bal<=0)
		sBal = "0.00";
	else {
		sBal = UtilsIntToString(bal);
		if (sBal.length()==1) sBal = "0.0" + sBal;
		else if (sBal.length()==2) sBal = "0." + sBal;
		else
			sBal.insert(sBal.length()-2,".");
	}
	return sBal;
}

OfflineBal::~OfflineBal()
{
}

int OfflineBal::DoTransaction(map<string, string>&txdata, EMVTxType txType)
{
	(void)txdata;
	log.message(MSG_INFO "Transit - DoTransaction\r\n");

	initBatchRec();

	string interfaces;
	//	interfaces.append(1, (char)INTF_MAG);
	//	interfaces.append(1, (char)INTF_MANUAL);
	interfaces.append(1, (char)INTF_ICC);

	log.message(MSG_INFO "interfaces enable ["+UtilsHexToString(interfaces.c_str(), interfaces.length())+"]\n");

	//iso_tx_code = TX_ISO_BAL_ENQUIRY;

	Operator op;

	CStatus status;
	status.setStatus(WAITING_FOR_CARD_INSERT);

	dont_remove_card = false;
	//import_txdata(txdata);
	for(map<string,string>::iterator it=txdata.begin(); it != txdata.end(); ++it){
		if(it->first.compare(TX_DATA_TX_DONT_REMOVE_CARD) == 0){
			string dRC = it->second;
			if (dRC.compare("1") == 0)
				dont_remove_card = true;

			log.message(MSG_INFO "-->" + string(TX_DATA_TX_DONT_REMOVE_CARD) + ":" + dRC + "\r\n");
		}
	}

	wait_for_h_command_response = false;
	for(map<string,string>::iterator it=txdata.begin(); it != txdata.end(); ++it){
		if(it->first.compare(TX_DATA_TX_H_CMD_ENABLED) == 0){
			string dRC = it->second;
			if (dRC.compare("1") == 0)
				wait_for_h_command_response = true;

			log.message(MSG_INFO "-->" + string(TX_DATA_TX_H_CMD_ENABLED) + ":" + dRC + "\r\n");
		}
	}

	string sInputTlv = getEMVTagTxType(txType).getStringData()+
			getEMVTagTxDate().getStringData()+
			getEMVTagTxTime().getStringData();
	string sRequestedTlv =  string("\x9f\x02"\
			"\x9f\x03"\
			"\x4f"\
			"\x82"\
			"\x9f\x36"\
			"\x9f\x07"\
			"\x8a"\
			"\xdf\x01"\
			"\xdf\x02"\
			"\x9f\x26"\
			"\x9f\x27"\
			"\x8e"\
			"\x9f\x34"\
			"\x9f\x1e"\
			"\x9f\x0d"\
			"\x9f\x0e"\
			"\x9f\x0f"\
			"\x9f\x10"\
			"\xdf\x09"\
			"\x9f\x09"\
			"\x9f\x33"\
			"\x9f\x1a"\
			"\x9f\x35"\
			"\x95"\
			"\x9f\x53"\
			"\x5f\x2a"\
			"\x9a"\
			"\x9f\x41"\
			"\x9c"\
			"\x9f\x37"\
			"\xcb"\
			"\x9f\x6e"\
			"\x9f\x7c"\
			"\x5a"\
			"\x50"\
			"\x57"\
			"\x5f\x20"\
			"\x5f\x24"\
			"\x5f\x25"\
			"\x5f\x30"\
			"\x5f\x34"\
			"\x9f\x20");

	testAndSetBusyFlag();
	int iRc=startTransaction(interfaces,sInputTlv, sRequestedTlv);

	log.message(MSG_INFO "pam.startTransaction()="+log.from_int(iRc)+"\r\n");

	while(1){
		LAST_COMMAND_TO_RESPOND last_command = getLastCommandToComplete();

		// First check the status of the last command
		if((last_command != NONE) && (pam_status != 0)){
			log.message(MSG_INFO "Error occured, pam_status=" + UtilsIntToString(pam_status) + "\n");
			removeCardPrompt();
			return -1;
		}

		if(last_command == MODIFY_LIMITS){
					log.message(MSG_INFO "pam.getLastCommandToRespond()=MODIFY_LIMITS\r\n");

					string sApdu = string("\x80\xca\x9f\x50\x00",5);
					log.message(MSG_INFO "emvOfflineBalEnq:  [" + UtilsHexToString(sApdu.c_str(), sApdu.length()) + "]\r\n");

					testAndSetBusyFlag();
					//int iRes =
					apdu(0,sApdu);

		} else if(last_command == CANCEL_TRANSACTION){
			log.message(MSG_INFO "Cancelled transaction remotely\r\n");

			dont_remove_card = false;		// In case it was set

			return -1;

		}else if(last_command == APDU){
					log.message(MSG_INFO "pam.getLastCommandToRespond()=APDU\r\n");
					log.message(MSG_INFO "---> ApduResult:		: [" + UtilsHexToString(emv_apdu_result.c_str(), emv_apdu_result.length()) + "]\r\n");

					//apduResult status:  [00]
					// ApduResult:          : [9f50060000000000009000]
					string sTag = string("\x9f\x50");

					sBalance = "";
					// Get balance from emv_apdu_result
					if ((emv_apdu_result.length()>=8) && (sTag == emv_apdu_result.substr(0,2))
							&& (emv_apdu_result.substr(emv_apdu_result.length()-2,1) == "\x90")) {
						BERTLV  tlv(emv_apdu_result);
						string sVal  = "0";
						if (tlv.getValForTag(sTag,sVal))
							sBalance = getBalanceAmount(sVal);

						log.message(MSG_INFO "Offline balance is ["+sBalance+"]\r\n");
					}
					log.message(MSG_INFO "---> Sending:		: [" + UtilsHexToString(string(PAM_FORCE_DECLINE,1).c_str(), 1) + "]\r\n");
					testAndSetBusyFlag();
					iRc=modifyLimits(PAM_FORCE_DECLINE);
					log.message(MSG_INFO "pam.modifyLimits()="+log.from_int(iRc)+"\r\n");

		}else if(last_command == START_TRANSACTION)
				{
					log.message(MSG_INFO "pam.getLastCommandToRespond()=START_TRANSACTION\r\n");

					// Check if fallback processing need to be done
					if(emv_fallback_required){
						log.message(MSG_INFO "START_TRANSACTION says do fallback to mag, canceling transaction instead.\n");
						emv_fallback_required = false; // We're not going to do fallback on a balance enquiry
						return 1;
					}

					op.balanceDisp(sBalance);

					txCompleteOffline(EMV_TX_OFFLINE_BAL_ENQ); // Busy flag is set inside this in emvcomplete

					return 0; // we ignore return code
				}

		usleep(100*1000);		// Sleep for 100 milli seconds nto prevent tight loop
	}

	return -1;
}

}
