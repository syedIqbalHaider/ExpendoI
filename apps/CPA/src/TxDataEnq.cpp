#include <string>
#include <unistd.h>
#include <gui/gui.h>
#include <gui/jsobject.h>
#include <libtui/tui.h>
//#include <libda/ccardconfig.h>
#include <libda/cterminalconfig.h>

#include "Transaction.h"
#include "utils.h"
#include "CLog.h"
#include "Operator.h"
#include "CPAver.h"
#include "string.h"
#include "cstatus.h"
static CLog log(CPA_NAME);

//using namespace std;
using namespace Utils;
using namespace vfigui;
using namespace za_co_verifone_tui;
using namespace za_co_vfi_Operator;
using namespace com_verifone_status;

namespace za_co_vfi_Transaction
{
static string EnqTags[] = { "","\x50","\x70","\x71","\x72",/*"\x7B",*/"\x75","\x76","\x77","\x78",
							string("\x00\xC0\x00\x00\x00",5),"" };

TxDataEnq::~TxDataEnq()
{
}

int TxDataEnq::DoTransaction(map<string, string>&txdata, EMVTxType txType)
{
	(void)txdata;
	log.message(MSG_INFO "TxDataEnq - DoTransaction\r\n");

	initBatchRec();

	string interfaces;

	if(emv_fallback_required){
		emvFallback();
		log.message(MSG_INFO "emvFallback() not allowed\n");
		return -1;
	}else
		interfaces.append(1, (char)INTF_ICC);

	log.message(MSG_INFO "interfaces enable ["+UtilsHexToString(interfaces.c_str(), interfaces.length())+"]\n");

//	apdMode = APD_TAG_CB;
	apdMode = APD_TAG_9F50;

	string sInputTlv = getEMVTagTxType(txType).getStringData()+
			getEMVTagTxDate().getStringData()+
			getEMVTagTxTime().getStringData();

	string sRequestedTlv =  string("\x9f\x02"\
			"\x9f\x03"\
			"\x4f"\
			"\x82"\
			"\x9f\x36"\
			"\x9f\x07"\
			"\x8a"\
			"\xdf\x01"\
			"\xdf\x02"\
			"\x9f\x26"\
			"\x9f\x27"\
			"\x8e"\
			"\x9f\x34"\
			"\x9f\x1e"\
			"\x9f\x0d"\
			"\x9f\x0e"\
			"\x9f\x0f"\
			"\x9f\x10"\
			"\xdf\x09"\
			"\x9f\x09"\
			"\x9f\x33"\
			"\x9f\x1a"\
			"\x9f\x35"\
			"\x95"\
			"\x9f\x53"\
			"\x5f\x2a"\
			"\x9a"\
			"\x9f\x41"\
			"\x9f\x4d"\
			"\x9c"\
			"\x9f\x37"\
			"\xcb"\
			"\x9f\x6e"\
			"\x9f\x7b"\
			"\x9f\x7c"\
			"\x5a"\
			"\x50"\
			"\x57"\
			"\x5f\x20"\
			"\x5f\x24"\
			"\x5f\x25"\
			"\x5f\x30"\
			"\x5f\x34"\
			"\x9f\x20");

	getLastCommandToRespond();
	testAndSetBusyFlag();
	int iRc=startTransaction(interfaces,sInputTlv, sRequestedTlv);
	int cnt=0;

	dont_remove_card = false;

	CStatus status;
	status.setStatus(WAITING_FOR_CARD_INSERT);

	for(map<string,string>::iterator it=txdata.begin(); it != txdata.end(); ++it){
		if(it->first.compare(TX_DATA_TX_DONT_REMOVE_CARD) == 0){
			string dRC = it->second;
			if (dRC.compare("1") == 0)
				dont_remove_card = true;

			log.message(MSG_INFO "-->" + string(TX_DATA_TX_DONT_REMOVE_CARD) + ":" + dRC + "\r\n");
		}
	}

	wait_for_h_command_response = false;
	for(map<string,string>::iterator it=txdata.begin(); it != txdata.end(); ++it){
		if(it->first.compare(TX_DATA_TX_H_CMD_ENABLED) == 0){
			string dRC = it->second;
			if (dRC.compare("1") == 0)
				wait_for_h_command_response = true;

			log.message(MSG_INFO "-->" + string(TX_DATA_TX_H_CMD_ENABLED) + ":" + dRC + "\r\n");
		}
	}

	log.message(MSG_INFO "pam.startTransaction()="+log.from_int(iRc)+"\r\n");

	while(1){
		LAST_COMMAND_TO_RESPOND last_command = getLastCommandToComplete();
		cnt++;

		// First check the status of the last command
		if((last_command != NONE) && (pam_status != 0)){
			log.message(MSG_INFO "Error occured, pam_status=" + UtilsIntToString(pam_status) + "\n");
			removeCardPrompt();
			return -1;
		}

		if(last_command == MODIFY_LIMITS){
			log.message(MSG_INFO "pam.getLastCommandToRespond()=MODIFY_LIMITS\r\n");

			// Ask for Terminal Capabilities  - commented out for now
			//  setGetTlv(string sActiveInterface,string sInputTlv,string sRequestedTlv)
			// setGetTlv(string("\x21","","\x9f33");
			// Below should move to set_get_tlv once we support it

			// We have to explicitly set inserted mode because the cancelTransaction below causes it
			// to never be set and writeBatch need this  mode
			entry_mode = PEM_INSERTED;

//			iRc = emvSendApduCB();
			iRc = emvSendApdu9F50();
			log.message(MSG_INFO "pam.modifyLimits,emvSendApduCB()="+log.from_int(iRc)+"\r\n");

		} else if(last_command == CANCEL_TRANSACTION){
			log.message(MSG_INFO "Cancelled transaction remotely\r\n");

			dont_remove_card = false;		// In case it was set

			return -1;

		} else if(last_command == SET_GET_TLV){
			// Save terminal capability
			log.message(MSG_INFO "pam.getLastCommandToRespond()=SET_GET_TLV\r\n");

		}else if(last_command == APDU){
			log.message(MSG_INFO "pam.getLastCommandToRespond()=APDU\r\n");
			log.message(MSG_INFO "<--- ApduResult:          : [" + UtilsHexToString(emv_apdu_result.c_str(), emv_apdu_result.length()) + "]\r\n");

			std::string sTag =  (emv_apdu_result.length()>=2) ? emv_apdu_result.substr(0,2) : "";

			if ((sTag.length() > 0) && (sTag.at(0) == '\x61')) {
				  emvSendApduCommand(APD_TAG_C0);
			} else {

				iRc = processApduData(EnqTags[apdMode]);
				// Note - we ignore tag not present (i.e iRc != 0)

				if (apdMode < APD_TAG_78) {
					  apdMode = (ApduMode)((int)apdMode+1);
					  emvSendApduCommand(apdMode);
				} else {
					// All APDU commands done, now get out

					//					Below was used when we still ask for pin
					//					testAndSetBusyFlag();
					//					iRc=modifyLimits(PAM_FORCE_DECLINE);
					//					log.message(MSG_INFO "pam.modifyLimits()="+log.from_int(iRc)+"\r\n");

					//removeCardPrompt(); // sets busy flag inside
					testAndSetBusyFlag();
					cancelTransaction(); // no need to use set busy as removeCardPrompt did this

					removeCardPrompt(); // sets busy flag inside


					// Replace emvtags with apdu values
					BERTLV tlv(emvtlv);
					tlv.replaceTags(emv_tag_data);
					emvtlv = tlv.getStringData();

					log.message(MSG_INFO "APDU end - emvtlv:  [" + UtilsHexToString(emvtlv.c_str(), emvtlv.length()) + "]\r\n");

					txCompleteOffline(EMV_TX_TRANS_DATA_ENQ);
					return 0; // we ignore return code

				}
			}

//		}else if(last_command == START_TRANSACTION){
//			log.message(MSG_INFO "pam.getLastCommandToRespond()=START_TRANSACTION\r\n");
//
//			// Replace emvtags with apdu values
//			BERTLV tlv(emvtlv);
//			tlv.replaceTags(emv_tag_data);
//			emvtlv = tlv.getStringData();
//
//			log.message(MSG_INFO "APDU end - emvtlv:  [" + UtilsHexToString(emvtlv.c_str(), emvtlv.length()) + "]\r\n");
//
//			txCompleteOffline(EMV_TX_TRANS_DATA_ENQ,dont_remove_card);
//
//			return 0; // we ignore return code
		}else if(last_command == START_TRANSACTION){
			log.message(MSG_INFO "pam.getLastCommandToRespond()=START_TRANSACTION\r\n");

			// Check if fallback processing need to be done
			if(emv_fallback_required){
				log.message(MSG_INFO "START_TRANSACTION says do fallback to mag, canceling transaction instead.\n");
				emv_fallback_required = false; // We're not going to do fallback on a balance enquiry
				return 1;
			}
		}

		usleep(100*1000);		// Sleep for 100 milli seconds
	}

	return -1;
}

int TxDataEnq::emvSendApduCommand(ApduMode apdTag)
{
	std::string sApdu = "";

	sApdu = string("\x80\xca\x9f") + EnqTags[apdTag] +  string("\x00",1);
	log.message(MSG_INFO "emvSendApduCommand:-->[" + UtilsHexToString(sApdu.c_str(), sApdu.length()) + "]\r\n");

	testAndSetBusyFlag();
	int iRes = apdu(0,sApdu);

	return iRes;
}

} // end namespace
