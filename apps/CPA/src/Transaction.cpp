#include <libda/ctsn.h>
#include <libda/cconmanager.h>
#include <libda/conlinestatus.h>
#include <libda/cbatchmanager.h>
#include <libda/creceiptno.h>
#include <libda/cbatch.h>
#include <libda/cterminalconfig.h>
#include <libda/cvelocitycheck.h>
#include <libtui/tui.h>
#include <libipc/ipc.h>
#include <sstream>
#include <iomanip>
#include <unistd.h>
#include <sys/types.h>
#include <sys/timeb.h>
#include <cstring>

#include "Transaction.h"
#include "utils.h"
#include "cstatus.h"
#include "CLog.h"
#include "Operator.h"
#include "Reports.h"
#include "CPAver.h"
#include "security.h"

static CLog log(CPA_NAME);

using namespace Utils;
using namespace za_co_vfi_Operator;
using namespace com_verifone_tsn;
using namespace com_verifone_onlinestatus;
using namespace za_co_vfi_reports;
using namespace za_co_verifone_tui;
using namespace com_verifone_status;

namespace za_co_vfi_Transaction
{

Transaction::Transaction(int iso_code):PAM(CPA_NAME)
{
	tx_cash_amount = "0";
	tx_sale_amount = "0";

	auth_code = "";

	logEntry = "";

	batchno = 0;
	rcptno = 0;
	ecrno=0;		//iq_audi010117
	tsn = 0;
	iso_tx_code = iso_code;
	acc_type = 0;
	budget_period = 0;								// Number of months
	account_profile_index = 0;
	tx_complete = false;
	dont_remove_card = false;
	wait_for_h_command_response = false;

	response_code = -1;
	tx_reason_code = 30;	// Default to 'Financial Transaction Failed', as per Serial Interface Spec

	dcm = DCM_INVALID;
	completion_code = false;
	capture_flag = NOT_CAPTURED;
	entry_mode = PEM_UNKNOWN;
	auth_mode = TERMINAL;

	emv_fallback_required = false;
	emv_fallback = false;
	manual_entry_required = false;
	above_floor_limit = false;
	will_go_online = false;
	went_online = false;
	signature_required = false;
	velocity_count_exceeded = false;
	velocity_period_exceeded = false;
	card_approved_offline = false;

	// Service code flags
	check_src = false;
	icc_card = false;
	atm_card = false;
	foreign_card = false;
	force_online = false;
	pin_required = false;
	exclude_cash = false;
	global_approved_flag = false;
	global_declined_flag = false;

	validate_pan = false;
	pan_validation_failed = false;

	cancel_transaction = false;
}

int Transaction::txComplete(bool waitForHCommandresponse)
{
	log.message(MSG_INFO "txComplete() start\n");

	string respMesg;
	int himRet=0;
	com_verifone_receiptno::CEcrNumber ecrNo;
	ecrno=ecrNo.getEcrNo();
//	cout<< "ECR Number 1:" << ecrno<<endl;


	acc_type = UtilsStringToInt(accprof.getCode());
	log.message(MSG_INFO "acc_type=" + UtilsIntToString(acc_type) + "\r\n");

	if((entry_mode == PEM_SWIPED) || (entry_mode == PEM_FALLBACK))
		checkSrc();

	will_go_online = checkOnline();		// This will fill in the pin_required flag

	// At this point, check if the transaction was perhaps declined by the card already
	if(global_declined_flag){
		log.message(MSG_INFO "txComplete() Abandoning transaction...\n");
		emvComplete(string(PAM_FORCE_DECLINE, 1));
		removeCardPrompt();
		return -1;
	}

	// Check if pin entry is required
	if(pin_required){
		log.message(MSG_INFO "txComplete() Performing PIN entry\n");

		if(getPin(NORMAL_PIN) < 0){
			log.message(MSG_INFO "txComplete() PIN entry cancelled\n");
			emvComplete(string(PAM_FORCE_DECLINE, 1));

			batchrec.setRespMsg(SSTR("Cancelled"));
			writeBatch();
			removeCardPrompt();
			return -1;
		}
	}

	if ((iso_tx_code == TX_ISO_PIN_CHANGE) || (iso_tx_code == TX_ISO_DEPOSIT)) {
		force_online = true;
	}

	string pinblock1,pinblock2;
	string ksn1,ksn2;
	if (iso_tx_code == TX_ISO_PIN_CHANGE) {

		//log.message(MSG_INFO "pin change 1\n");
		if(getPin(FIRST_PIN) < 0){
			log.message(MSG_INFO "txComplete() PIN entry cancelled\n");
			emvComplete(string(PAM_FORCE_DECLINE, 1));
			removeCardPrompt();
			return -1;
		} else {
			pinblock1 = pinblock;
			ksn1 = ksn;
		}

		//log.message(MSG_INFO "pin change 2\n");
		if(getPin(CONFIRM_PIN) < 0){
			log.message(MSG_INFO "txComplete() PIN entry cancelled\n");
			emvComplete(string(PAM_FORCE_DECLINE, 1));
			removeCardPrompt();
			return -1;
		} else {
			pinblock2 = pinblock;
			ksn2 = ksn;
		}
	}

	//iq_audi280217
	com_verifone_terminalconfig::CTerminalConfig termcfg;
	CTsn ctsn;

	if(Utils::UtilsStringToInt(termcfg.getReverselTsn())==0) //if no reversal increment 1
		tsn = ctsn.getCurrentTsn()+1;
	else
		tsn = ctsn.getCurrentTsn()+2;

//	CStatus status;
	if(will_go_online){
		log.message(MSG_INFO "txComplete() going online\n");

		auth_mode = HOST;

		writeBatch();	// Write the batch record, will be needed by the HIM

		// Note on 14/7: himRet below currently never returns negative because
		// timeout code is commented out (host timeout 60secs was bigger than ipc timeout)
		if (iso_tx_code == TX_ISO_PIN_CHANGE) {
			himRet = PinChange(pinblock1, ksn1, pinblock2, ksn2);
		} else if (iso_tx_code == TX_ISO_LOAD_PRODUCT) {
			himRet = ProductLoad();
		} else if(pin_required)
			himRet = Authorise(pinblock, ksn);
		else
			himRet = Authorise();

		readBatch();	// Get updated batch record from db

		Operator op;
		op.waitDisp();

		if(himRet < 0){
			log.message(MSG_INFO "txComplete() Authorisation failed\n");

			if(entry_mode == PEM_INSERTED){
				// If this is an EMV tx, then let the card know that we failed to go online
				// TestAndSet is called inside emvComplete()

				if((iso_tx_code == TX_ISO_SALE) || (iso_tx_code == TX_ISO_CASH_ADVANCE) || (iso_tx_code == TX_ISO_SALE_CASH))
					emvComplete(string(PAM_UNABLE_TO_GO_ONLINE, 1));
				else
					emvComplete(string(PAM_FORCE_DECLINE, 1));

			}
		}else{
			log.message(MSG_INFO "txComplete() response code "+batchrec.getRespCode()+"\n");
			response_code = UtilsStringToInt(batchrec.getRespCode());
			auth_code = batchrec.getAuthCode();

			went_online = true;

			if(response_code == 0)
				global_approved_flag = true;
			else
				global_declined_flag = true;
		}
	}else{
		auth_mode = TERMINAL;

		if(global_declined_flag){
			log.message(MSG_INFO "txComplete() declined offline\n");
		}else{
			log.message(MSG_INFO "txComplete() approved offline\n");
	/* TODO: Add auth code from card profile
			CCardProfile card_prof = cardData.getCardProfile();
			auth_code =
	*/
			response_code = 0;
			global_approved_flag = true;
			writeBatch();
		}
	}

	draftCapture();

	Operator op;
/*
	if (global_approved_flag && (iso_tx_code == TX_ISO_BAL_ENQUIRY)) {
		log.message(MSG_INFO "txComplete() Online Balance = ["+UtilsLongToString(batchrec.getTxAmount())+"]\r\n");
		op.balanceDisp(getAmount(UtilsLongToString(batchrec.getTxAmount())));
	}
*/

	if(entry_mode == PEM_INSERTED){
		if (waitForHCommandresponse) {
			CStatus stat;
			STATUS status=CONTINUE_WITH_EMV_COMPLETE_FAIL; // default

			stat.setStatus(WAIT_FOR_H_COMMAND);
			int cnt=0;
			do {
				sleep(1);
				status = stat.getStatus();
			} while (cnt++ < 10 && status != CONTINUE_WITH_EMV_COMPLETE_SUCCESS && status != CONTINUE_WITH_EMV_COMPLETE_FAIL);

			if (status == CONTINUE_WITH_EMV_COMPLETE_SUCCESS)
				emvComplete(string(PAM_OK, 1));//,dontRemoveCard);
			else
				emvComplete(string(PAM_FORCE_DECLINE, 1));//,dontRemoveCard);
		} else {
			if(auth_mode == HOST){		// Dont do this if card approved offline
				if(global_declined_flag)
					emvComplete(string(PAM_FORCE_DECLINE, 1));
				else
					emvComplete(string(PAM_OK, 1));
			}
		}
	}

	// Check response code and display
	if(global_approved_flag){
		tx_reason_code = 0;

		if (iso_tx_code == TX_ISO_PIN_CHANGE) {
			op.pinChangedDisp();
		} else if (iso_tx_code != TX_ISO_BAL_ENQUIRY){
			op.approvedDisp();
		}

		respMesg=SSTR("Approved");
		sleep(1);	// Display result for a second
	}
	else {
		if(response_code==0) // for case in which decline by chip, iq_audi_18072017
			ProcessPendingReversal();


		op.declinedDisp(response_code);

		if(himRet==-2)
			respMesg=SSTR("Connection Failed");
		else if(himRet==-3)
			respMesg=SSTR("Timeout");
		else
		{
			respMesg=MsgRespCode(response_code);
			op.declinedRespDisp(respMesg);
//			respMesg=SSTR("Declined");
			sleep(4);	// Display result for 4 seconds
		}

	}

//	sleep(4);	// Display result for 4 seconds
	batchrec.setRespMsg(respMesg);

	removeCardPrompt();

	com_verifone_receiptno::CReceiptNo crcptno;
	rcptno = crcptno.getNextReceiptNo();

	//
	// At this point we consider complete and all tx related information has been recorded
	//
	tx_complete = true;
	writeBatch();

//	updateVelocity();   //iq_audi060317 there is no need of velocity

	printReceipt();

	log.message(MSG_INFO "txComplete() end\n");

	return 0;
}


int Transaction::txCompleteOffline(EMVTxType txType)
{
	log.message(MSG_INFO "txCompleteOffline() start\n");

	CTsn ctsn;
	tsn = ctsn.getNextTsn();
	response_code = 0;
	global_approved_flag = true;
	tx_reason_code = 0;

	writeBatch();
	draftCapture();

	if((entry_mode == PEM_INSERTED) && (txType != EMV_TX_TRANS_DATA_ENQ) && (txType != EMV_TX_OFFLINE_BAL_ENQ) && (txType != EMV_TX_OFFLINE_MIN_STAT)){
		// If this is an EMV tx, then let the card know that we failed to go online
		emvComplete(string(PAM_FORCE_DECLINE, 1));
	}

	// At this point we consider complete and all tx related information has been recorded
	tx_complete = true;

	writeBatch(); // completed now

	log.message(MSG_INFO "txCompleteOffline() end\n");

	return 0;
}

/*
 * Return codes:
 * Return < 0 : Error
 * Return == 0: Card declined
 * Return == 1: Car approved
 */
int Transaction::emvComplete(string status)
{
	emvtlv = batchrec.getEmvTags();
	BERTLV tlv(emvtlv);
	string authResponse;

	BERTLV completeTlv;

	completeTlv.appendTag("\xdf\xaf\x01", string("\x22"));
	completeTlv.appendTag("\xdf\xaf\x04", status);

	string str_resp_code = batchrec.getRespCode();

	unsigned int iresp_code = translateISORespCode((unsigned int)UtilsStringToInt(str_resp_code));
	completeTlv.appendTag("\x8a", UtilsIntToString((long)iresp_code, 2));
//	completeTlv.appendTag("\x8a", string(str_resp_code).substr(0, 2));

	// Get Issuer Authentication Data
	string issuerAuth;

	if((tlv.getValForTag("\x91", issuerAuth) <= 0) || (issuerAuth.length() == 0)){
		log.message(MSG_INFO "emvComplete(): Expected emv Tlv 0x91 (Issuer Authentication Data) not found\n");
	}else{
		log.message(MSG_INFO "emvComplete(): 0x91 (issuerAuth) = ["+UtilsHexToString(issuerAuth.c_str(), issuerAuth.length())+"]\n");
		completeTlv.appendTag("\x91", issuerAuth);
	}

	// Get Issuer Script Template 1
	string issuerScript1;

	if((tlv.getValForTag("\x71", issuerScript1) <= 0) || (issuerScript1.length() == 0)){
		log.message(MSG_INFO "emvComplete(): Expected emv Tlv 0x71 (Issuer Script 1) not found\n");
	}else{
		log.message(MSG_INFO "emvComplete(): 0x71 (issuerScript1) = ["+UtilsHexToString(issuerScript1.c_str(), issuerScript1.length())+"]\n");
		completeTlv.appendTag("\x71", issuerScript1);
	}

	// Get Issuer Script Template 2
	string issuerScript2;

	if((tlv.getValForTag("\x72", issuerScript2) <= 0) || (issuerScript2.length() == 0)){
		log.message(MSG_INFO "emvComplete(): Expected emv Tlv 0x72 (Issuer Script 2) not found\n");
	}else{
		log.message(MSG_INFO "emvComplete(): 0x72 (issuerScript2) = ["+UtilsHexToString(issuerScript2.c_str(), issuerScript2.length())+"]\n");
		completeTlv.appendTag("\x72", issuerScript2);
	}

	// Get Issuer Application Data
	string iap;

	if((tlv.getValForTag("\x9f\x10", iap) <= 0) || (iap.length() == 0)){
		log.message(MSG_INFO "emvComplete(): Expected emv Tlv 0x9f10 (Issuer Application Data) not found\n");
	}else{
		log.message(MSG_INFO "emvComplete(): 0x9f10 (Issuer Application Data) = ["+UtilsHexToString(iap.c_str(), iap.length())+"]\n");
		completeTlv.appendTag("\x9f\x10", iap);
	}

	// Get Service Restriction Code
	string src;

	if((tlv.getValForTag("\x5f\x30", src) <= 0) || (src.length() == 0)){
		// Get service code from tag 0x57 if tag 0x5f30 does not exist
		string bcd_discretionary_data;
		if(tlv.getValForTag("\x57", bcd_discretionary_data)){
			string discretionary_data = Utils::UtilsHexToString(bcd_discretionary_data.c_str(), bcd_discretionary_data.length());
			unsigned long eq_pos = discretionary_data.find("d");

			if(eq_pos != string::npos){
				service_restriction_code = discretionary_data.substr(eq_pos+5, 3);
				log.message(MSG_INFO "emvComplete(): 0x57 (Service Restriction Code) = ["+service_restriction_code+"]\n");
			}
		}

		if(service_restriction_code.length() == 0){
			log.message(MSG_INFO "emvComplete(): Service Restriction Code not found\n");
		}
	}else{
		service_restriction_code = UtilsHexToString(src.c_str(), src.length()).substr(1, (src.length()*2)-1);
		log.message(MSG_INFO "emvComplete(): 0x5f30 (Service Restriction Code) = ["+service_restriction_code+"]\n");
	}

	log.message(MSG_INFO "emvComplete(): EMV Complete tags:\n");
	TraceEMVTags(completeTlv.getStringData());

	int iRc;
	testAndSetBusyFlag();
	iRc = completeTransaction(status, completeTlv.getStringData(), "\x4f"
																	"\x82"
																	"\x8a"
																	"\x9f\x36"
																	"\x9f\x07"
																	"\x9f\x26"
																	"\x9f\x27"
																	"\x9f\x34"
																	"\x9f\x10"
																	"\x9f\x0d"
																	"\x9f\x0e"
																	"\x9f\x0f"
																	"\x9f\x1a"
																	"\x9f\x35"
																	"\x95"
																	"\x9f\x53"
																	"\x5f\x2a"		// Should probably be 5f2a?
																	"\x9a"
																	"\x9b"
																	"\x9f\x41"
																	"\x9f\x37"
																	"\x5a"
																	"\x5f\x34");

	log.message(MSG_INFO "pam.completeTransaction()="+log.from_int(iRc)+"\r\n");

	// Waiting for completeTransactionResult() to happen
	testAndSetBusyFlag();
	clearBusyFlag();

	LAST_COMMAND_TO_RESPOND last_command = getLastCommandToComplete();
	if(last_command != COMPLETE_TRANSACTION){
		log.message(MSG_INFO "emvComplete(): Unexpected PAM response on COMPLETE_TRANSACTION\n");
		return -1;
	}

	// Check if the card declined
	int iret = getEMVCompleteTagCID();

	if(iret == 1){
		log.message(MSG_INFO "emvComplete(): !!! CARD APPROVED !!!\n");

		global_approved_flag = true;
		global_declined_flag = false;

		if(will_go_online && !went_online)
			auth_mode = DEF_ANALYSIS;
	}else{
		if((iret) == 0){
			log.message(MSG_INFO "emvComplete(): !!! CARD DECLINED !!!\n");
		}else{
			log.message(MSG_INFO "emvComplete(): !!! UNEXPECTED RESPONSE !!!\n");
		}
		
		log.message(MSG_INFO "emvComplete(): iso_tx_code="+UtilsIntToString(iso_tx_code)+"\n");

		if (((iso_tx_code == TX_ISO_SALE) || (iso_tx_code == TX_ISO_DEPOSIT) || (iso_tx_code == TX_ISO_CASH_ADVANCE) || (iso_tx_code == TX_ISO_SALE_CASH)) && (went_online && global_approved_flag)){
			// Store the TSN of the transaction in termcfg db for reversal
			com_verifone_terminalconfig::CTerminalConfig termcfg;
			termcfg.setReverselTsn(batchrec.getTsn());
			log.message(MSG_INFO "emvComplete(): Set reversal tsn\n");
		} else {
			log.message(MSG_INFO "emvComplete(): No reversal needed\n");
		}

		if(went_online)
			tx_reason_code = 10;	// As per the Serial Interface Specification
		else
			tx_reason_code = 9;	// As per the Serial Interface Specification

		global_approved_flag = false;
		global_declined_flag = true;
	}

	// Preserve card balance if it is present
	string balance;
	tlv.getValForTag("\x9f\50", balance);
	tlv.replaceTags(emv_complete_tlv);

	if(balance.length() > 0)
		tlv.addTag("\x9f\50", balance);

	emvtlv = tlv.getStringData();	// Overwrite emv tags with what is contained at the end

	return 0;
}

BERTLV Transaction::getEMVTagTxType(EMVTxType tx_type)
{
	BERTLV tx_tlv;
	tx_tlv.addTag("\x9c", string((char*)&tx_type, 1));
	return tx_tlv;
}

BERTLV Transaction::getEMVTagTxDate(void)
{
	BERTLV tx_date;
	char bcd_date[3];

	if(start_timestamp.length() == 0)
		start_timestamp = UtilsTimestamp();

	UtilStringToBCD(start_timestamp.substr(2, 6), bcd_date);
	tx_date.addTag("\x9a", string(bcd_date, 3));
	return tx_date;
}

BERTLV Transaction::getEMVTagTxTime(void)
{
	BERTLV tx_time;
	char bcd_time[3];

	if(start_timestamp.length() == 0)
		start_timestamp = UtilsTimestamp();

	UtilStringToBCD(start_timestamp.substr(8, 6), bcd_time);
	tx_time.addTag("\x9f\x21", string(bcd_time, 3));
	return tx_time;
}

BERTLV Transaction::getEMVTagTxAmount(void)
{
//	int int_sale_amount;
	//int int_cash_amount;
//	int int_tx_amount;
	char bcd_amount[6];

	//comment iq_audi_19072017 below code have int conversion cause problem for large value
//	int_sale_amount = UtilsStringToInt(tx_sale_amount);
//	//int_cash_amount = UtilsStringToInt(tx_sale_amount);
//	int_tx_amount = int_sale_amount;// + int_cash_amount;
//
//	string tot_amount = UtilsIntToString(int_tx_amount);
//
//	if(tot_amount.length() > 12)
//		tot_amount.resize(12);
//
//	tot_amount.insert((size_t)0, (size_t)(12-tot_amount.length()), '0');	// Pad with leading 0's
	UtilStringToBCD(tx_sale_amount, bcd_amount);

	BERTLV amount_tlv;
	amount_tlv.addTag("\x9f\x02", string(bcd_amount, 6));

	return amount_tlv;
}


BERTLV Transaction::getEMVTagTxCurreny(void)
{
	char bcd_currency[4]={0};
	UtilStringToBCD(zero_pad(txn_currency,4), bcd_currency);

	BERTLV currency_tlv;
	currency_tlv.addTag("\x5f\x2a", string(bcd_currency, 2));

	return currency_tlv;
}


unsigned int Transaction::getEMVTagCID(void)
{
	BERTLV tlv(emvtlv);
	string cid;

	if((tlv.getValForTag("\x9f\x27", cid) <= 0) || (cid.length() == 0)){
		log.message(MSG_INFO "Expected emv TLV CID not found\n");
		return 0; // -1 replaced because we will then skip card declined
	}

	unsigned int iret = (unsigned int)cid.at(0) & 0x000000ff;

	return iret >> 6;
}

unsigned int Transaction::getEMVCompleteTagCID(void)
{
	BERTLV tlv(emv_complete_tlv);
	string cid;
	if((tlv.getValForTag("\x9f\x27", cid) <= 0) || (cid.length() == 0)){
		log.message(MSG_INFO "Expected emv TLV CID not found\n");
		return 0;
	}

	unsigned int iret = (unsigned int)cid.at(0) & 0x000000ff;

	return iret >> 6;
}

#if 0
int Transaction::getPin(PinType type)
{
	int iret = 0;
	string pin_prompt;
	string prompt;

	log.message(MSG_INFO "getPin()\n");

	CStatus status;
	status.setStatus(ENTER_PIN);

	// Without a PAN, we cannot proceed
	if(pan.length() == 0){
		log.message(MSG_INFO "getPin(): PAN length is 0\n");
		return -1;
	}

	string stripped_pan = pan.substr(pan.length()-13, 12);								// Example: XXXXXX123456789012X

	// Convert PAN to bcd and pad f's at the end
	char cpan[20];
	int cpan_len = UtilStringToBCD(stripped_pan, cpan);
	std::ostringstream ss;
	ss << std::setw(10) << std::left << std::setfill('\xff') << string(cpan, cpan_len);

	BERTLV sc_tlv;
	sc_tlv.addTag("\xdf\x1f",string("\x00",1));											// Timeout
	sc_tlv.addTag("\xdf\x23","\x02");													// Key scheme
	sc_tlv.addTag("\xdf\x81\x10",string("\x00",1));										// Lock
	sc_tlv.addTag("\xdf\x36",ss.str());													// PAN
	sc_tlv.addTag("\xdf\x05",string("\x00",1));											// PIN try flag
	sc_tlv.addTag("\xdf\xed\x08",string("\x00",1));										// PIN block format ISO-0

	//following code have bug iq_audi_20072017
//	int iAmount = UtilsStringToInt(tx_sale_amount);
//	if( iAmount > 0){
//
//		amount = UtilsIntToString(iAmount);
//
//		if(iAmount < 100)
//			amount.insert(0, "0");
//
//		if(iAmount < 10)
//			amount.insert(0, "0");
//
//		amount.insert(0, "$");	//currency symbol iq_audi220117
//		amount.insert(amount.length()-2, ".");
//	}
	char amountBuff[16]={0};
	Utils::UtilsAmountInsertDecimal((const char *)tx_sale_amount.c_str(),2,amountBuff,"$ ");

	string amount(amountBuff);

	if(type == CONFIRM_PIN){
		pin_prompt = TuiString("ConfirmPin");
	}else if(type == FIRST_PIN){
		pin_prompt = TuiString("EnterNewPin");
	}else{
		pin_prompt = TuiString("EnterPin");
	}

	// This is a hack, since the tagds do not work according to the documentation
	prompt = amount.append("\n"+pin_prompt);
	sc_tlv.addTag("\xdf\x81\x1b",prompt);

	string sc_command;
	sc_command.append("\x18");															// Command: 24
	sc_command.append(sc_tlv.getStringData());

	int pmlret = com_verifone_ipc::connect_to("SCAPP");

	if(pmlret < 0){
		log.message(MSG_INFO "getPin(): getPin exiting due to pml error "+UtilsIntToString(pmlret)+"\n");
	}

	log.message(MSG_INFO "getPin(): send ["+UtilsHexToString((const char*)sc_command.c_str(), sc_command.length())+"]\n");
	com_verifone_ipc::send(sc_command,"SCAPP");

	while(1){
		string ipcFrom;
		string response;

		if(com_verifone_ipc::receive(response,"SCAPP",ipcFrom)==com_verifone_ipc::IPC_SUCCESS){
			log.message(MSG_INFO "getPin(): response ["+UtilsHexToString((const char*)response.c_str(), response.length())+"]\n");

			// Check the status of the pin entry process
			string status;
			BERTLV resp_tlv(response.substr(1));
			resp_tlv.getValForTag("\xdf\x30", status);

			if(status.length() && (status.at(0) == 0)){
				log.message(MSG_INFO "getPin(): PIN entry returned success\n");
				resp_tlv.getValForTag("\xdf\x6c", pinblock);
				resp_tlv.getValForTag("\xdf\x81\x03", ksn);

				log.message(MSG_INFO "getPin(): PIN block ["+UtilsHexToString((const char*)pinblock.c_str(), pinblock.length())+"]\n");
				log.message(MSG_INFO "getPin():       KSN ["+UtilsHexToString((const char*)ksn.c_str(), ksn.length())+"]\n");
			}else{
				log.message(MSG_INFO "getPin(): PIN entry failed with "+UtilsIntToString((int)status.at(0))+"\n");
				iret = -1;
			}
			break;
		}

		if(cancel_transaction)
			break;

		usleep(100000);
	}

	if(cancel_transaction){
		 // cancel security service
		BERTLV secTlv;
		string ipcFrom;
		string response;

		secTlv.addTag(string("\xDF\x1F"),string("\x00",1));
		secTlv.addTag(string("\xDF\x2A"),string("\x80"));
		string secCmd("\x24");
		secCmd.append(secTlv.getStringData());

		com_verifone_ipc::send(secCmd,string("SCAPP"));

		while(1){
			if(com_verifone_ipc::receive(response,"SCAPP",ipcFrom)==com_verifone_ipc::IPC_SUCCESS){
				break;
			}
		}

		log.message(MSG_INFO "getPin(): cancel command sent to SCAPP\n");
		iret = -1;
	}

	com_verifone_ipc::disconnect(CPA_NAME);

	return iret;
}
#else  //replace scapp with following
int Transaction::getPin(PinType type)
{
	int iret = 0;
	string pin_prompt;
	string prompt;
	unsigned char TpkGiske[121]={0}, ucPinBlock[16+1]={0}, currencyName[5]={0};
	com_verifone_terminalconfig::CTerminalConfig termcfg;

	log.message(MSG_INFO "getPin()\n");

	CStatus status;
	status.setStatus(ENTER_PIN);

	// Without a PAN, we cannot proceed
	if(pan.length() == 0){
		log.message(MSG_INFO "getPin(): PAN length is 0\n");
		return -1;
	}

	char amountBuff[16]={0};

	if(txn_currency.compare("840")==0)
	{
		memcpy(currencyName,"USD ",4);
		Utils::UtilsAmountInsertDecimal((const char *)tx_sale_amount.c_str(),2,amountBuff,(char *)currencyName);
	}
	else if(txn_currency.compare("422")==0)
	{
		memcpy(currencyName,"LBP ",4);
		Utils::UtilsAmountRemoveDecimal((const char *)tx_sale_amount.c_str(),2,amountBuff,(char *)currencyName);
	}


	string amount(amountBuff);

	memcpy(TpkGiske,termcfg.getTpk().c_str(),120);

	iret=ippPinEncrypt(TpkGiske,pan,amount, ucPinBlock);

	log.message(MSG_INFO "PIN Block ["+SSTR(ucPinBlock)+"] \r\n");

	pinblock=UtilsStringToHex(SSTR(ucPinBlock));

	BERTLV resp_tlv;

	if(iret || cancel_transaction)
		return  -1;

	status.setStatus(PIN_ENTERED);

	return iret;
}
#endif
int Transaction::emvFallback(void)
{
	log.message(MSG_INFO "emvFallback()\n");

	removeCardPrompt();
	emv_fallback_required = false;

	return 0;
}

int Transaction::manualEntry(void)
{
	return 0;
}

int Transaction::removeCardPrompt(void)
{
	CStatus stat;

	if (dont_remove_card){
//		stat.setStatus(DO_NOT_REMOVE_CARD);
		return 0;
	}
log.message(MSG_INFO "Transaction - removeCardPrompt\r\n");
	stat.setStatus(WAITING_CARD_REMOVAL);

	testAndSetBusyFlag();
	removeCard(0);

	// Waiting for removeCardResult() to happen
	testAndSetBusyFlag();
	clearBusyFlag();

	LAST_COMMAND_TO_RESPOND last_command = getLastCommandToComplete();

	if(last_command == CANCEL_TRANSACTION){
		log.message(MSG_INFO "Cancelled transaction remotely\r\n");

		return -1;

	}

	// Sanity check
	if(last_command != REMOVE_CARD){
		log.message(MSG_INFO "removeCardPrompt(): Unexpected reponse "+UtilsIntToString((int)last_command)+"\n");
		return -1;
	}

	dont_remove_card = true;	// Ignore any further remove card requests, since the card has now been removed

	return 0;
}

bool Transaction::ccdCheck(void){

//TODO: This check should be skipped when we are in unattended mode.

	if((entry_mode != PEM_SWIPED) || (entry_mode == PEM_FALLBACK))
		return true;						// Only perform the check for swiped cards

	if(pin_required)
		return true;						// Only perform check on card that don't require PIN

	if(acc_type != 30)
		return true;						// Only perform check on credit cards

	if(track_data.pan().length() == 0)
		return true;						// Only perform the check if we have a PAN available

	string ccd;
	Operator op;
	int pan_length = track_data.pan().length();

	if(op.getCCD(ccd) < 0)
		return false;						// Check fails if CCD entry was canceled

	if(track_data.pan().compare(pan_length-4, 4, ccd) == 0){
		return true;
	}

	log.message(MSG_INFO "ccdCheck() failed to match " + ccd + " with " + track_data.pan() + "\n");

	return false;
}

void Transaction::printCardData(CCardData cdata)
{
	log.message("-------------BIN-----------\n");
	log.message("BIN                    - " + cdata.getBin() + "\n");
	log.message("Card Issued Name       - " + cdata.getCardIssuedName() + "\n");
	log.message("Card Length            - " + UtilsIntToString(cdata.getCardLength()) + "\n");

	CCardProfile  cardProfile = cdata.getCardProfile();
	log.message("-------Card Profile--------\n");
	log.message("Pin Len Max            - " + UtilsIntToString(cardProfile.getPinLenMax()) + "\n");
	log.message("Pin Len Min            - " + UtilsIntToString(cardProfile.getPinLenMin()) + "\n");
	log.message("Voice Appr Text        - " + cardProfile.getVoiceApprovalText() + "\n");
	log.message("Active                 - " + UtilsBoolToString(cardProfile.isActive()) + "\n");
	log.message("Check Expiry date      - " + UtilsBoolToString(cardProfile.isCheckExpiryDate()) + "\n");
	log.message("Check Luhn             - " + UtilsBoolToString(cardProfile.isCheckLuhn()) + "\n");
	log.message("Check Service Code     - " + UtilsBoolToString(cardProfile.isCheckServiceCode()) + "\n");
	log.message("Foreign Card           - " + UtilsBoolToString(cardProfile.isForeignCard()) + "\n");
	log.message(" \n");

	std::map<std::string,CAccountProfile> accounts = cdata.getAccounts();
	std::map<std::string,CAccountProfile>::iterator it;

	for (it=accounts.begin(); it!=accounts.end(); ++it) {
		CAccountProfile accountProfile = it->second;
		log.message("-------------Attached Account-------------\n");
		log.message("Code                   - " + accountProfile.getCode() + "\n");
		log.message("Description            - " + accountProfile.getDescription() + "\n");
		log.message("Budget Min             - " + accountProfile.getBudgetAmountMin() + "\n");
		log.message("Totals Group Name      - " + accountProfile.getTotalsGroupName() + "\n");
		log.message("Super Totals Group Name- " + accountProfile.getSuperTotalsGroupName() + "\n");
		log.message("Allow Budget           - " + UtilsBoolToString(accountProfile.isAllowBudget()) + "\n");
		log.message("Man Pan Entry          - " + UtilsBoolToString(accountProfile.isAllowManualEntry()) + "\n");
		log.message("Voice Approval         - " + UtilsBoolToString(accountProfile.isAllowVoiceApproval()) + "\n");
		log.message("Check Card value       - " + UtilsBoolToString(accountProfile.isCheckCardValue()) + "\n");
		log.message("Draft Capture Mode     - " + UtilsBoolToString(accountProfile.isDraftCaptureMode()) + "\n");
		log.message("Require Pin Entry      - " + UtilsBoolToString(accountProfile.isRequirePinEntry()) + "\n");
		log.message("Require Signature      - " + UtilsBoolToString(accountProfile.isRequireSignature()) + "\n");
		log.message("Skim Card Value        - " + UtilsBoolToString(accountProfile.isSkimCardValue()) + "\n");

		log.message(" \n");
		log.message("---Allowed Transactions--\n");

		std::set<std::string> allowedTransactions = accountProfile.getAllowedTransactions();
		std::set<std::string>::iterator it2;

		for (it2=allowedTransactions.begin(); it2!=allowedTransactions.end(); ++it2) {
			std::string allowTx = *it2;
			log.message("Transaction Type       - " + allowTx + "\n");
		}

		log.message(" \n");

		CLimits limit = accountProfile.getLocalLimits();
		log.message("-------Local Limit---------\n");
		log.message("Cashback Amt Max       - " + limit.getCashbackAmtMax() + "\n");
		log.message("Floor Limit Amt        - " + limit.getFloorLimitAmt() + "\n");
		log.message("Transaction Amt Min    - " + limit.getTransactionAmtMin() + "\n");
		log.message("Transaction Amt Max    - " + limit.getTransactionAmtMax() + "\n");
		log.message(" \n");

		limit = accountProfile.getEmvFallBackLimits();

		log.message("-----Emv Fallback Limit----\n");
		log.message("Cashback Amt Max       - " + limit.getCashbackAmtMax() + "\n");
		log.message("Floor Limit Amt        - " + limit.getFloorLimitAmt() + "\n");
		log.message("Transaction Amt Min    - " + limit.getTransactionAmtMin() + "\n");
		log.message("Transaction Amt Max    - " + limit.getTransactionAmtMax() + "\n");
		log.message(" \n");

		limit = accountProfile.getOfflineLimits();

		log.message("-------Offline Limit-------\n");
		log.message("Cashback Amt Max       - " + limit.getCashbackAmtMax() + "\n");
		log.message("Floor Limit Amt        - " + limit.getFloorLimitAmt() + "\n");
		log.message("Transaction Amt Min    - " + limit.getTransactionAmtMin() + "\n");
		log.message("Transaction Amt Max    - " + limit.getTransactionAmtMax() + "\n");
		log.message(" \n");
		limit = accountProfile.getContactLessLimits();
		log.message("-----Contactless Limit-----\n");
		log.message("Cashback Amt Max       - " + limit.getCashbackAmtMax() + "\n");
		log.message("Floor Limit Amt        - " + limit.getFloorLimitAmt() + "\n");
		log.message("Transaction Amt Min    - " + limit.getTransactionAmtMin() + "\n");
		log.message("Transaction Amt Max    - " + limit.getTransactionAmtMax() + "\n");
		log.message(" \n");
	}
}

void Transaction::printAccProf(CAccountProfile accountProfile)
{
	log.message("-------------Account Profile-------------\n");
	log.message("Code                   - " + accountProfile.getCode() + "\n");
	log.message("Description            - " + accountProfile.getDescription() + "\n");
	log.message("Budget Min             - " + accountProfile.getBudgetAmountMin() + "\n");
	log.message("Totals Group Name      - " + accountProfile.getTotalsGroupName() + "\n");
	log.message("Super Totals Group Name- " + accountProfile.getSuperTotalsGroupName() + "\n");
	log.message("Allow Budget           - " + UtilsBoolToString(accountProfile.isAllowBudget()) + "\n");
	log.message("Man Pan Entry          - " + UtilsBoolToString(accountProfile.isAllowManualEntry()) + "\n");
	log.message("Voice Approval         - " + UtilsBoolToString(accountProfile.isAllowVoiceApproval()) + "\n");
	log.message("Check Card value       - " + UtilsBoolToString(accountProfile.isCheckCardValue()) + "\n");
	log.message("Draft Capture Mode     - " + UtilsBoolToString(accountProfile.isDraftCaptureMode()) + "\n");
	log.message("Require Pin Entry      - " + UtilsBoolToString(accountProfile.isRequirePinEntry()) + "\n");
	log.message("Require Signature      - " + UtilsBoolToString(accountProfile.isRequireSignature()) + "\n");
	log.message("Skim Card Value        - " + UtilsBoolToString(accountProfile.isSkimCardValue()) + "\n");

	log.message(" \n");
	log.message("---Allowed Transactions--\n");

	std::set<std::string> allowedTransactions = accountProfile.getAllowedTransactions();
	std::set<std::string>::iterator it2;

	for (it2=allowedTransactions.begin(); it2!=allowedTransactions.end(); ++it2) {
		std::string allowTx = *it2;
		log.message("Transaction Type       - " + allowTx + "\n");
	}

	log.message(" \n");

	CLimits limit = accountProfile.getLocalLimits();
	log.message("-------Local Limit---------\n");
	log.message("Cashback Amt Max       - " + limit.getCashbackAmtMax() + "\n");
	log.message("Floor Limit Amt        - " + limit.getFloorLimitAmt() + "\n");
	log.message("Transaction Amt Min    - " + limit.getTransactionAmtMin() + "\n");
	log.message("Transaction Amt Max    - " + limit.getTransactionAmtMax() + "\n");
	log.message(" \n");

	limit = accountProfile.getEmvFallBackLimits();

	log.message("-----Emv Fallback Limit----\n");
	log.message("Cashback Amt Max       - " + limit.getCashbackAmtMax() + "\n");
	log.message("Floor Limit Amt        - " + limit.getFloorLimitAmt() + "\n");
	log.message("Transaction Amt Min    - " + limit.getTransactionAmtMin() + "\n");
	log.message("Transaction Amt Max    - " + limit.getTransactionAmtMax() + "\n");
	log.message(" \n");

	limit = accountProfile.getOfflineLimits();

	log.message("-------Offline Limit-------\n");
	log.message("Cashback Amt Max       - " + limit.getCashbackAmtMax() + "\n");
	log.message("Floor Limit Amt        - " + limit.getFloorLimitAmt() + "\n");
	log.message("Transaction Amt Min    - " + limit.getTransactionAmtMin() + "\n");
	log.message("Transaction Amt Max    - " + limit.getTransactionAmtMax() + "\n");
	log.message(" \n");
	limit = accountProfile.getContactLessLimits();
	log.message("-----Contactless Limit-----\n");
	log.message("Cashback Amt Max       - " + limit.getCashbackAmtMax() + "\n");
	log.message("Floor Limit Amt        - " + limit.getFloorLimitAmt() + "\n");
	log.message("Transaction Amt Min    - " + limit.getTransactionAmtMin() + "\n");
	log.message("Transaction Amt Max    - " + limit.getTransactionAmtMax() + "\n");
	log.message(" \n");

}

void Transaction::checkSrc(void)
{
	service_restriction_code = track_data.src();

	// check for ICC flag regardless of what card parameters is saying
	//  if((batch_rec.src[1] == '2') || (batch_rec.src[1] == '6'))
	if ((service_restriction_code.at(0) == '2') || (service_restriction_code.at(0) == '6')){
		icc_card = true;
	}
	if (service_restriction_code.at(2) == '3') atm_card = true;

	if ((service_restriction_code.at(0) == '5') || (service_restriction_code.at(0) == '6')) foreign_card = true;

	if (service_restriction_code.at(1) == '2') force_online = true;

	switch (service_restriction_code.at(2)) {
		case '0':
		case '5':
		case '6':
		case '7':
			pin_required = true;
		  break;
	}

	if ((service_restriction_code.at(2) == '2') || (service_restriction_code.at(2) == '5')) exclude_cash = true;

	log.message("SRC Check:\n");
	log.message("ICC card     - " + UtilsBoolToString(icc_card) + "\n");
	log.message("ATM card     - " + UtilsBoolToString(atm_card) + "\n");
	log.message("Foreign card - " + UtilsBoolToString(foreign_card) + "\n");
	log.message("Force Online - " + UtilsBoolToString(force_online) + "\n");
	log.message("PIN Required - " + UtilsBoolToString(pin_required) + "\n");
	log.message("Exclude Cash - " + UtilsBoolToString(exclude_cash) + "\n");
}

bool Transaction::checkOnline(void)
{
	if(entry_mode == PEM_INSERTED){
		int iret = getEMVTagCID();

		if((iret) == 0){
			log.message(MSG_INFO "checkOnline(): !!! CARD DECLINED !!!\n");
			global_approved_flag = false;
			global_declined_flag = true;
		}else if(iret == 1){
			log.message(MSG_INFO "checkOnline(): !!! CARD APPROVED !!!\n");
			card_approved_offline = true;

		}else if(iret == 2){
			log.message(MSG_INFO "checkOnline(): !!! CARD SAYS GO ONLINE !!!\n");
			force_online = true;

			BERTLV tlvEmv(emvtlv);
			string sCVMResult;

			if(tlvEmv.getValForTag("\x9F\x34",sCVMResult) && (sCVMResult.length()==3)){
				unsigned char ucCVMPerformed=sCVMResult.at(0)&0x3f;
				if(ucCVMPerformed==0x02){
					pin_required=true;
					log.message(MSG_INFO "checkOnline():  Card requires PIN\n");
				}

				if(ucCVMPerformed==0x03||ucCVMPerformed==0x05||ucCVMPerformed==0x1e){
					signature_required=true;
					log.message(MSG_INFO "checkOnline():  Card requires signature\n");
				}
			}

			emv_complete_tlv = emvtlv;	// Copy tags because it is needed by EMVComplete()
		}else{
			log.message(MSG_INFO "checkOnline(): !!! UNEXPECTED RESPONSE !!!\n");
			force_online = true;
		}

		if(global_declined_flag)
			return false;

	}else if(entry_mode == PEM_ENTERED){
		log.message(MSG_INFO "checkOnline(): Manual Card entered must go online\n");
		return true;
	}else if(entry_mode == PEM_FALLBACK){
		log.message(MSG_INFO "checkOnline(): Fallback to mag must go online\n");
		return true;
	}

	if(force_online){
		log.message(MSG_INFO "checkOnline(): Transaction forced online\n");
		return true;
	}

	if(velocity_count_exceeded || velocity_period_exceeded){
		log.message(MSG_INFO "checkOnline(): Velocity rules says must go online\n");
		return true;
	}

	if(above_floor_limit){
		log.message(MSG_INFO "checkOnline(): Velocity rules says must go online\n");
		return true;
	}

	return false;
}

void Transaction::draftCapture(void)
{
	if (dcm == DCM_ONLINE) {
		if ((iso_tx_code == TX_ISO_CORRECTION) || (iso_tx_code == TX_ISO_REFUND)) {
			completion_code = true;
			capture_flag = ALREADY_CAPTURED;
		} else {
			completion_code = false;
			capture_flag = COMPLETION_TO_BE_SEND;
		}

	} else {
		if (iso_tx_code == TX_ISO_CORRECTION)
		{
			completion_code = true;
			capture_flag = ALREADY_CAPTURED;
		} else {
			completion_code = false;
			capture_flag = NOT_CAPTURED;
		}

	}
}

void Transaction::writeBatch(void)
{
	int ret;
	com_verifone_batchmanager::CBatchManager batchManager;
	com_verifone_batch::CBatch cbatch;

	if((ret = batchManager.getBatch(cbatch)) == DB_OK){
		batchno = cbatch.getBatchNo();
	}

	batchrec.setAccProfIndex(UtilsIntToString(account_profile_index));
	batchrec.setAccType(UtilsIntToString(acc_type));
	batchrec.setApproveOnline(went_online);
	batchrec.setApproved(global_approved_flag);
	batchrec.setAuthCode(auth_code);
	batchrec.setAuthMode((com_verifone_batchrec::AUTH_MODE)auth_mode);

	batchrec.setBatchDbId(cbatch.getBatchDbId());
	batchrec.setBatchTxNo(tsn);
	batchrec.setBudgetPeriod(UtilsIntToString(budget_period));
//	batchrec.setCanceledReceiptNo();
//	batchrec.setCanceledTxType();
//	batchrec.setCancelledTsn();
	batchrec.setCaptureFlag(capture_flag);
//	batchrec.setCardhAuthMethod();
	batchrec.setCardholderName(track_data.cardholder());
	batchrec.setCashAmount(UtilsStringToInt(tx_cash_amount));
	batchrec.setCashExcludeFlag(exclude_cash);
//	batchrec.setCashierId();
//	batchrec.setCashierName();
//	batchrec.setCashierPinVerified();
	batchrec.setCompletionCode(string(completion_code, 1));
//	batchrec.setCvv();
	batchrec.setDeclined(global_declined_flag);
//	batchrec.setDemoTx();

	if(dcm == DCM_ONLINE)
		batchrec.setDraftCapMode(ONLINE);
	else
		batchrec.setDraftCapMode(OFFLINE);

	if(entry_mode == PEM_INSERTED)
		batchrec.setEmvTags(emvtlv);

	batchrec.setFleetCard(false);
//	batchrec.setFraudInd();
	batchrec.setIccFlag(icc_card);
//	batchrec.setInvoiceNo();
//	batchrec.setLitres();
//	batchrec.setNonIsoRespCode();
//	batchrec.setOdoReading();
	COnlineStatus os;
//	os.setEmvFallbackToMag();
	os.setFloorLimit(above_floor_limit);
//	os.setManualEntry(manual_entry);
	os.setNotHotcard(true);
	os.setSrcCode(check_src);
//	os.setVelocity();
	batchrec.setOnlStatus(os);
//	batchrec.setOrigTxDateTime();
	batchrec.setPan(pan);
	batchrec.setPanEntryMode((PAN_ENTRY_MODE)entry_mode);
	batchrec.setPayRequest(false);
//	batchrec.setProductInfo();
//	batchrec.setRcsCard();

	if(pan_validation_failed)
		tx_reason_code = 15;

	batchrec.setReason(UtilsIntToString(tx_reason_code));	// As per the Serial Interface Specification

	batchrec.setReceiptNo(UtilsIntToString(rcptno));
	batchrec.setEcrNo(UtilsIntToString(ecrno));
//	batchrec.setRefNo1();
//	batchrec.setRefNo2();
//	batchrec.setRepeatTsn();
	batchrec.setRespCode(UtilsIntToString(response_code));
//	batchrec.setRrn();
	batchrec.setSignatureRequired(signature_required);
//	batchrec.setSpdhSeqNo();
//	batchrec.setSpdhTsn();
	batchrec.setSrc(service_restriction_code);
//	batchrec.setSupervisorId();
//	batchrec.setSupervisorName();
//	batchrec.setTableNo();
//	batchrec.setTimeoutReversal();
//	batchrec.setTipAmount();
	batchrec.setTotalsGroup(accprof.getTotalsGroupName());
	if(track_data.getTrack2().length()){
		batchrec.setExp(track_data.expiry());
		batchrec.setTrack2(track_data.getTrack2().substr(1, track_data.getTrack2().length()-3));
	}
//	batchrec.setTransmissionNum();
	batchrec.setTsn(tsn);
	batchrec.setTxAmount(UtilsStringToInt(tx_sale_amount));
	batchrec.setTransCurrency(txn_currency);
//	batchrec.setTxCanceled();
	batchrec.setTxCode(UtilsIntToString(iso_tx_code));

//	batchrec.setTxDateTime(start_timestamp);
	if(!tx_complete)batchrec.setTxDateTime(start_timestamp);	//iq_audi301216

	batchrec.setTxForceOffline(!force_online);
	batchrec.setTxForceOnline(force_online);
//	batchrec.setTxProducts();
	batchrec.setTxType(UtilsIntToString(iso_tx_code));
//	batchrec.setWaiterId();

	if(tx_complete) batchrec.setInProgress(false);

	cbatch.writeRecToBatch(batchrec);

}

void Transaction::readBatch(void)
{
	int ret;
	com_verifone_batchmanager::CBatchManager batchManager;
	com_verifone_batch::CBatch cbatch;

	ret = batchManager.getBatch(cbatch);

	if (ret != DB_OK){
		log.message(MSG_INFO "Failure retrieving batch record\r\n");
		return;
	}

	cbatch.readLastRecFromBatch(batchrec);
}

int Transaction::printReceipt(void) {
	Receipt receipt(batchrec);

	if(global_approved_flag){
		if(receipt.merchantCopy(false) < 0)
			log.message(MSG_INFO "Receipt merchant copy failed\n");

		if(receipt.customerCopy(false) < 0)
			log.message(MSG_INFO "Receipt customer copy failed\n");
	}else{
		if(receipt.declined() < 0)
			log.message(MSG_INFO "Receipt declined copy failed\n");
	}

	return 0;
}

void Transaction::Feedback(string message)
{
	(void) message;
	log.message(MSG_INFO "HimFeedback()\n");
}

void Transaction::TraceEMVTags(string tags)
{
	BERTLV tlv(tags);
	int pos=0;

	while(1){
		string tag, len, val;
		int ret;
		ret = tlv.getTagLenVal(pos, tag, len, val);

		if(ret){
			log.message(MSG_INFO+UtilsHexToString(tag.c_str(), tag.length())+" ("+UtilsHexToString(len.c_str(), len.length())+"):"+UtilsHexToString(val.c_str(), val.length())+"\n");
			pos = ret;
		}
		else
			break;
	}

}

//
// PAM virtual function implementations
//
int Transaction::startTransactionResult(string sStatusCode,string sActiveInterface,string sEmvTlv,string sTrack1,string sTrack2,string sTrack3,string sSwipeStatus,string sManualPan,string sManualExpiryDate,string sManualCvv,string sBarcodeData)
{
	(void)sSwipeStatus;
	(void)sManualPan;
	(void)sManualExpiryDate;
	(void)sManualCvv;
	(void)sBarcodeData;

	CStatus stat;

	log.message(MSG_INFO "Transaction - startTransactionResult\r\n");

	pam_status = 0;

	char status = sStatusCode.at(0);
	log.message(MSG_INFO "---> sStatusCode		: [" + UtilsHexToString(sStatusCode.c_str(), sStatusCode.length()) + "]\r\n");

	if(status == 9){
		log.message(MSG_INFO "---> Magstripe fallback required\n");
		emv_fallback_required = true;
		return 0;
	}else if(status == 0x0a){
		log.message(MSG_INFO "---> Manual card entry required\n");
		manual_entry_required = true;
		return 0;
	}else if(status != 0){
		log.message(MSG_INFO "---> Cancelling\n");
		pam_status = (int)sStatusCode.at(0);
		return -1;
	}

	log.message(MSG_INFO "---> sActiveInterface	: [" + UtilsHexToString(sActiveInterface.c_str(), sActiveInterface.length()) + "]\r\n");

	if(sActiveInterface.at(0) == 0x21){
		log.message(MSG_INFO "---> ICC inserted\r\n");

		// Check that we received any emv data
		if(sEmvTlv.length() == 0){
			log.message(MSG_INFO "---> No EMV data received\r\n");
			pam_status = 1;
			return -1;
		}

		emvtlv = sEmvTlv;
		entry_mode = PEM_INSERTED;

		log.message(MSG_INFO "---> EmvTlv			: [" + UtilsHexToString(emvtlv.c_str(), emvtlv.length()) + "]\r\n");
		TraceEMVTags(emvtlv);

	}else if(sActiveInterface.at(0) == 0x22){
		log.message(MSG_INFO "Contactless tapped\r\n");

		entry_mode = PEM_TAPPED;
	}else if(sActiveInterface.at(0) == 0x23){
		stat.setStatus(CARD_SWIPED);
		log.message(MSG_INFO "---> Magnetic card swiped\r\n");
		log.message(MSG_INFO "---> Track1			: [" + sTrack1 + "]\n");
		log.message(MSG_INFO "---> Track2			: [" + sTrack2 + "]\n");
		log.message(MSG_INFO "---> Track3			: [" + sTrack3 + "]\n");

		if(emv_fallback){
			entry_mode = PEM_FALLBACK;
			log.message(MSG_INFO "---> This is a fallback to mag transaction!\n");
		}else
			entry_mode = PEM_SWIPED;

		if(sTrack2.length()){
			TrackData mag(sTrack1, sTrack2, sTrack3);
			track_data = mag;
			pan = track_data.pan();
		}

		checkSrc();

//		iq_audi010317
//		// On the Ux300 with combined reader, we want to treat an ICC card at this point, as a fallback transaction.
//		if(UtilsGetTerminalType() == TT_UX_300){
//			emv_fallback = true;
//			entry_mode = PEM_FALLBACK;
//			log.message(MSG_INFO "---> This is a fallback to mag transaction on a combined reader!\n");
//		}

		checkVelocity();

	}else if(sActiveInterface.at(0) == 0x24){
		log.message(MSG_INFO "Barcode scanned\r\n");

	}else if(sActiveInterface.at(0) == 0x25){
		log.message(MSG_INFO "---> Manual PAN entered\r\n");
		log.message(MSG_INFO "---> Pan			: [" + sManualPan + "]\n");
		log.message(MSG_INFO "---> CVV			: [" + sManualCvv + "]\n");
		log.message(MSG_INFO "---> Expiry Date	: [" + sManualExpiryDate + "]\n");

		if(emv_fallback){
			entry_mode = PEM_FALLBACK;
			log.message(MSG_INFO "---> This is a fallback to mag transaction!\n");
		}else
			entry_mode = PEM_ENTERED;

		TrackData card(sManualPan, sManualExpiryDate, sManualCvv, "");
		track_data = card;
		pan = track_data.pan();
	}

	return 0;
}


int Transaction::modifyLimitsRequest(string sAid,string sPan, string sEmvTlv)
{
	string sLogEntry;
	BERTLV tlv(sEmvTlv);
	tlv.getValForTag(string("\x9f\x4d"),sLogEntry);

	CStatus stat;
	stat.setStatus(CARD_INSERTED);

	pam_status = 0;

	logEntry = sLogEntry;
	emvtlv = sEmvTlv;

	log.message(MSG_INFO "Transaction - modifyLimitsRequest\r\n");
	log.message(MSG_INFO "---> sAid: [" + UtilsHexToString(sAid.c_str(), sAid.length()) + "]\r\n");
	log.message(MSG_INFO "---> sPan: [" + UtilsHexToString(sPan.c_str(), sPan.length()) + "]\r\n");
	log.message(MSG_INFO "---> sLog: [" + UtilsHexToString(logEntry.c_str(), logEntry.length()) + "]\r\n");
	log.message(MSG_INFO "---> emvtlv: [" + UtilsHexToString(emvtlv.c_str(), emvtlv.length()) + "]\r\n");

	CCardConfig cardcfg;

	pan = UtilsBcdToString(sPan.c_str(), sPan.length());

	// Check if PAN validation needs to be done
	if(validate_pan && pan.compare(required_pan) != 0){
		log.message(MSG_INFO "PAN validation failed!\r\n");
		Operator op;
		op.displayErrorMsg("CardNotFoundError");
		pan_validation_failed = true;

		// In this case, we are going to record the transaction in the database, so that the reason code can be returned to kiosk
		writeBatch();

		return 0;
	}

	//
	// BIN lookup
	//
	CCardData cardData;
	if (cardcfg.lookupCardConfig(UtilsHexToString(sPan.c_str(), sPan.length()),cardData, txn_currency) != DB_OK)
	{
		log.message(MSG_INFO "iq: No BIN found" );
		Operator op;
		op.displayErrorMsg("CardNotFoundError");
		pam_status = (int)0x15;	// Not found
		return -1;
	}

	// check aid override
	if(cardcfg.lookupAidOverride(UtilsHexToString(sPan.c_str(), sPan.length()),UtilsHexToString(sAid.c_str(), sAid.length()), emvcfg, txn_currency) <= 0){
		log.message(MSG_INFO "No AID override found for AID ["+UtilsHexToString(sAid.c_str(), sAid.length())+"]\r\n");
		if(cardcfg.lookupEmvConfig(UtilsHexToString(sAid.c_str(), sAid.length()), emvcfg, txn_currency) <= 0){
//			log.message(MSG_INFO "iqbal: CardNotFoundError" );
			Operator op;
			op.displayErrorMsg("CardNotFoundError");
			pam_status = (int)0x15;	// Not found
			return -1;
		}
	} else
		log.message(MSG_INFO "AID override found!!!\r\n");

	accprof = emvcfg.getAccountProfile();
	acc_type = UtilsStringToInt(emvcfg.getAccountProfile().getCode());
	accprof.setTotalsGroupName(emvcfg.getTotalsGroupName());
	accprof.setSuperTotalsGroupName(emvcfg.getSuperTotalsGroupName());

	printAccProf(accprof);

	checkVelocity();

	return 0;
}

int Transaction::completeTransactionResult(string sStatusCode,string sEmvTlv)
{
	(void)sStatusCode;
	(void)sEmvTlv;

	log.message(MSG_INFO "Transaction - completeTransactionResult\r\n");
	pam_status = (int)sStatusCode.at(0);

	if(pam_status != 0){
		log.message(MSG_INFO "---> sStatusCode		: [" + UtilsHexToString(sStatusCode.c_str(), sStatusCode.length()) + "]\r\n");
		return -1;
	}

	log.message(MSG_INFO     "---> sEmvTlv		    :\n");
	TraceEMVTags(sEmvTlv);
	emv_complete_tlv = sEmvTlv;

	return 0;
}

int Transaction::readMediaResult(string sStatusCode,string sActiveInterface,string sAtr,string sTrack1,string sTrack2,string sTrack3,string sSwipeStatus,string sManualPan,string sManualExpiryDate,string sManualCvv,string sBarcodeData)
{
	(void)sStatusCode;
	(void)sActiveInterface;
	(void)sAtr;
	(void)sTrack1;
	(void)sTrack2;
	(void)sTrack3;
	(void)sSwipeStatus;
	(void)sManualPan;
	(void)sManualExpiryDate;
	(void)sManualCvv;
	(void)sBarcodeData;

	log.message(MSG_INFO "Transaction - readMediaResult\r\n");

	pam_status = sStatusCode.at(0);

	if(pam_status != 0){
		log.message(MSG_INFO "---> sStatusCode		: [" + UtilsHexToString(sStatusCode.c_str(), sStatusCode.length()) + "]\r\n");
		return -1;
	}

	log.message(MSG_INFO "---> Track1: [" + UtilsBcdToString(sTrack1.c_str(), sTrack1.length()) + "]\r\n");
	log.message(MSG_INFO "---> Track2: [" + UtilsBcdToString(sTrack2.c_str(), sTrack2.length()) + "]\r\n");
	log.message(MSG_INFO "---> Track3: [" + UtilsBcdToString(sTrack3.c_str(), sTrack3.length()) + "]\r\n");

	return 0;
}

int Transaction::removeCardResult(string sStatusCode)
{
	log.message(MSG_INFO "Transaction - removeCardResult\r\n");

	pam_status = sStatusCode.at(0);

	if(pam_status != 0){
		log.message(MSG_INFO "---> sStatusCode		: [" + UtilsHexToString(sStatusCode.c_str(), sStatusCode.length()) + "]\r\n");
		return -1;
	}

	CStatus status;
	status.setStatus(CARD_REMOVED);
	return 0;
}

int Transaction::transactionStatusResult(string sStatusCode)
{
	log.message(MSG_INFO "Transaction - transactionStatusResult\r\n");

	pam_status = sStatusCode.at(0);

	if(pam_status != 0){
		log.message(MSG_INFO "---> sStatusCode		: [" + UtilsHexToString(sStatusCode.c_str(), sStatusCode.length()) + "]\r\n");
		return -1;
	}

	return 0;
}

int Transaction::cancelTransactionResult(string sStatusCode)
{
	log.message(MSG_INFO "Transaction - cancelTransactionResult\r\n");

	cancel_transaction = true;
	pam_status = sStatusCode.at(0);

	if(pam_status != 0){
		log.message(MSG_INFO "---> sStatusCode		: [" + UtilsHexToString(sStatusCode.c_str(), sStatusCode.length()) + "]\r\n");
		return -1;
	}

	return 0;
}

void Transaction::initBatchRec(void)
{
// DO - 20150220: This seems unnecessary and has the side effect of leaving an empty record behind, if the transaction is canceled early.
//	writeBatch();
}

#define SECONDS_IN_24H	86400

int Transaction::checkVelocity(void)
{
	com_verifone_terminalconfig::CTerminalConfig termcfg;
	com_verifone_velocitycheck::CVelocityCheck velocity;
	int vcount = termcfg.getVelocityCnt();
	int vperiod = termcfg.getVelocityPeriod();
	struct timeb tp;
	ftime(&tp);

	log.message(MSG_INFO "Velocity check...\n");
	log.message(MSG_INFO "Configured period "+UtilsIntToString(vperiod)+"\n");
	log.message(MSG_INFO "Configured count "+UtilsIntToString(vcount)+"\n");

	paninfo.setPan(pan);

	if(velocity.checkPan(paninfo) == DB_OK){
		// Found pan
		log.message(MSG_INFO "Velocity check found pan ["+pan+"]\n");

		long lastused = paninfo.getTimeStamp();
		paninfo.setLastUsedTimestamp(lastused);

		if((long)tp.time - lastused > SECONDS_IN_24H){
			// Card was not used here for 24h, reset counters as if first used
			paninfo.setCounter(0);
			paninfo.setLastUsedTimestamp(0);

			log.message(MSG_INFO "Velocity: pan ["+pan+"] not used in 24h\n");
		}
	}else{
		// Pan not found, create new pan info
		log.message(MSG_INFO "Velocity check says first use of pan ["+pan+"]\n");

		paninfo.setCounter(0);
		paninfo.setLastUsedTimestamp(0);
	}

	int pcount = paninfo.getCounter()+1;
	paninfo.setCounter(pcount);
	paninfo.setTimeStamp((long)tp.time);				// This will be a problem in 2038 ;-)

	long lastused = paninfo.getTimeStamp();

	if(lastused > 0){
		char buf[16];
		memset(buf, 0, sizeof(buf));
		strftime(buf,sizeof(buf),"%Y%m%d%H%M%S%u",localtime((const time_t*)&lastused));

		log.message(MSG_INFO "Velocity: Last used "+string(buf)+"\n");
	}

	log.message(MSG_INFO "Velocity: Timestamp "+UtilsLongToString(paninfo.getTimeStamp())+"\n");
	log.message(MSG_INFO "Velocity: Count "+UtilsIntToString(paninfo.getCounter())+"\n");

	if((long)tp.time - paninfo.getLastUsedTimestamp() < SECONDS_IN_24H){
		log.message(MSG_INFO "Velocity period exceeded\n");
		velocity_period_exceeded = true;
	}

	if(((long)tp.time - paninfo.getLastUsedTimestamp() < vperiod) && (pcount >= vcount)){
		log.message(MSG_INFO "Velocity count exceeded\n");
		velocity_count_exceeded = true;
	}

	return 0;
}

int Transaction::updateVelocity(void)
{
	com_verifone_velocitycheck::CVelocityCheck velocity;

	velocity.updatePan(paninfo);

	return 0;
}

bool Transaction::isMagEnabled(void)
{
/*
	com_verifone_terminalconfig::CTerminalConfig termcfg;

	string emvcapability = termcfg.getEmvCapabilityBitMap();
*/

	return true;
//	return false;
}

bool Transaction::isManualEntryEnabled(void)
{
	com_verifone_terminalconfig::CTerminalConfig termcfg;

	if(!isMagEnabled())
		return false;		// If mag is disabled, then also don't allow manual entry

#if 0	// Manual PAN is disabled for Ticibox, because it hangs on the Mx925
	string term_func_bmp = termcfg.getTerminalFunctionBitMap();
	char byte1 = term_func_bmp.at(0);

	if (byte1 & 1){
		log.message(MSG_INFO "Manual PAN entry is enabled\n");
		return true;
	}
#endif

	log.message(MSG_INFO "Manual PAN entry is disabled\n");

	return false;
}

string Transaction::getAmount(const string sVal)
{
	string sBal=sVal;

	int bal=UtilsStringToInt(sBal);
	if (bal<=0)
		sBal = "0.00";
	else {
		sBal = UtilsIntToString(bal);
		if (sBal.length()==1) sBal = "0.0" + sBal;
		else if (sBal.length()==2) sBal = "0." + sBal;
		else
			sBal.insert(sBal.length()-2,".");
	}
	return sBal;
}


string Transaction::getTxCurrency(void){

	return txn_currency;

}

unsigned int Transaction::translateISORespCode(unsigned int recv_resp)
{
		switch (recv_resp) {
		case 0:
						return 0; // Successful
		case 1:
						return 1; // Refer To Card Issuer
		case 2:
						return 2; // Refer To Card Issuer Special
		case 3:
						return 3; // Invalid Merchant
		case 4:
						return 4; // Pick Up
		case 5:
						return 5; // Do Not Honour
		case 6:
						return 6; // Error
		case 7:
						return 7; // Pick Up Special
		case 8:
						return 8; // Honour With ID
		case 9:
						return 9; // Request In Progress
		case 10:
						return 10; // Approved Partial
		case 11:
						return 11; // Approved VIP
		case 12:
						return 12; // Invalid Tran
		case 13:
						return 13; // Invalid Amount
		case 14:
						return 14; // Invalid Card Number
		case 15:
						return 15; // No Such Issuer
		case 16:
						return 16; // Approved Update Track 3
		case 19:
						return 19; // Re-enter Transaction
		case 21:
						return 21; // No Action Taken
		case 22:
						return 22; // Suspected Malfunction
		case 23:
						return 23; // Unacceptable Tran Fee
		case 24:
						return 24; // File Update Not Supported
		case 25:
						return 25; // Unable To Locate Record
		case 26:
						return 26; // Duplicate Record
		case 27:
						return 27; // File Update Edit Error
		case 28:
						return 28; // File Update File Locked
		case 29:
						return 29; // File Update Failed
		case 30:
						return 30; // Format Error
		case 31:
						return 31; // Bank Not Supported
		case 33:
						return 33; // Expired Card Pick Up
		case 34:
						return 34; // Suspected Fraud Pick Up
		case 35:
						return 35; // Contact Acquirer Pick Up
		case 36:
						return 36; // Restricted Card Pick Up
		case 37:
						return 37; // Call Acquirer Security Pick Up
		case 38:
						return 38; // PIN Tries Exceeded Pick Up
		case 39:
						return 39; // No Credit Account
		case 40:
						return 40; // Function Not Supported
		case 41:
						return 41; // Lost Card
		case 43:
						return 43; // Stolen Card
		case 51:
						return 51; // Not Sufficient Funds
		case 54:
						return 54; // Expired Card
		case 55:
						return 55; // Incorrect PIN
		case 56:
						return 56; // No Card Record
		case 57:
						return 57; // Tran Not Permitted Cardholder
		case 58:
						return 58; // Tran Not Permitted Terminal
		case 59:
						return 59; // Suspected Fraud Declined
		case 60:
						return 60; // Contact Acquirer
		case 61:
						return 61; // Exceeds Withdrawal Limit
		case 62:
						return 62; // Restricted Card
		case 63:
						return 63; // Security Violation
		case 65:
						return 65; // Exceeds Withdrawal Frequency
		case 66:
						return 66; // Call Acquirer Security
		case 68:
						return 68; // Response Received Too Late
		case 75:
						return 75; // PIN Tries Exceeded
		case 90:
						return 90; // Cut-off In Progress
		case 91:
						return 91; // Issuer Or Switch Inoperative
		case 93:
						return 93; // Violation Of Law
		case 95:
						return 95; // Reconcile Error
		default:
						return 60; // Contact Acquirer
	}
}

}
