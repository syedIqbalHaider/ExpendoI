/*
 * CLog.cpp
 *
 *  Created on: Dec 4, 2013
 *      Author: dietero2@verifone.com
 */

/*
 * This class is meant to abstract logging functionality into a central place where the final logging mechanism can be switched between
 * whichever medium is relevant on the platform.
 */
#include <liblog/logsys.h>
#include <iostream>
#include <string>
#include <sstream>
#include "CLog.h"

//using namespace std;
//#undef DEBUG
CLog::CLog(string appname)
{
	app_name = appname;

#ifdef DEBUG
	dlog_init(app_name.c_str());
#endif
}

void CLog::message(string message)
{
#ifdef DEBUG
	cout << app_name << message;

	// Lose the trailing \r\n
	int l = message.length()-1;
	if (l>0 && message.at(l) == '\n') l--;
	if (l>0 && message.at(l) == '\r') l--;
	if (l>=0) message.resize(l+1);
	dlog_msg(message.c_str());
#else
	(void)message;	// Suppresses -> warning: unused parameter
#endif
}

void CLog::uimsg(string message)
{
	cout << app_name << message;
	cout.flush();
}

string CLog::from_int(int x)
{
	stringstream ss;
	ss << x;
	return ss.str();
}


