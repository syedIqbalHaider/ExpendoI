#include <libda/cconmanager.h>
#include <libda/cbatchmanager.h>
#include <libda/cbatch.h>
#include <libda/ccardconfig.h>
#include <libda/ccarddata.h>
#include <libda/cterminalconfig.h>
#include "Transaction.h"
#include "TxResult.h"
#include "utils.h"
#include "bertlv.h"
#include "Operator.h"
#include <iostream>
#include <iomanip>

using namespace Utils;
using namespace za_co_vfi_Transaction;
using namespace za_co_vfi_Operator;

namespace za_co_verifone_txResult
{

TxResult::TxResult(void)
{
	is_complete = false;
	went_online = false;

	batchno = -1;
	tsn = -1;
	rcptno = -1;

//	cout<< "iq: TxResult"<<endl;

	int ret;
	com_verifone_batchmanager::CBatchManager batchManager;
	com_verifone_batch::CBatch cbatch;

	ret = batchManager.getBatch(cbatch);

	if (ret != DB_OK)
		return;

	CBatchRec batchrec;
	cbatch.readLastRecFromBatch(batchrec);

	if(batchrec.isInProgress()){
		txdata[TXRES_COMPLETE] = "0";
		return;					// Not a valid transaction
	}

	is_complete = true;
	txdata[TXRES_COMPLETE] = "1";

	if(batchrec.isApproveOnline()){
		went_online = true;
		txdata[TXRES_ONLINE] = "1";
	}else
		txdata[TXRES_ONLINE] = "0";

	batchno = cbatch.getBatchNo();
	txdata[TXRES_BATCHNO] = UtilsIntToString(batchno);

	tsn = batchrec.getBatchTxNo();
	txdata[TXRES_TSN] = UtilsIntToString(tsn);

	rcptno = UtilsStringToInt(batchrec.getReceiptNo());
	txdata[TXRES_RCPTNO] = batchrec.getReceiptNo();

	ecrno = UtilsStringToInt(batchrec.getEcrNo());		//iq_audi_301216
	txdata[TXRES_ECR_NUMBER] = batchrec.getEcrNo();

	response_code = batchrec.getRespCode();
	if(response_code.length() && (UtilsStringToInt(response_code) > 0)){
		txdata[TXRES_RESPCODE] = response_code;
	}

	string str_reason_code = batchrec.getReason();

	if(UtilsStringToInt(str_reason_code) <= 9)
		reason_code = "0" + str_reason_code;
	else
		reason_code = str_reason_code;

	if(reason_code.length() == 0){
		if(response_code.length() && (atoi(response_code.c_str()) == 0))
			reason_code = "00";
		else
			reason_code = "30";
	}

	txdata[TXRES_REASON_CODE] = reason_code;
	txdata[TXRES_RESP_MSG] = batchrec.getRespMsg();

	auth_code = batchrec.getAuthCode();
	if(auth_code.length()) txdata[TXRES_AUTHCODE] = auth_code;

	tx_currency = batchrec.getTransCurrency();
	txdata[TXRES_CURREYNCY] = tx_currency;

	tx_sale_amount = UtilsIntToString(batchrec.getTxAmount());
	txdata[TXRES_SALE_AMNT] = tx_sale_amount;

	tx_cash_amount = UtilsIntToString(batchrec.getCashAmount());
	if(tx_cash_amount.length() && (tx_cash_amount.compare("0") != 0)) txdata[TXRES_CASH_AMNT] = tx_cash_amount;

	pan = batchrec.getPan();
	if(pan.length()){
		pan.replace(6, (size_t)(pan.length()-10), (size_t)(pan.length()-10), '*');
		txdata[TXRES_PAN] = pan;
	}

	start_timestamp = batchrec.getTxDateTime();
	txdata[TXRES_TIMESTAMP] = start_timestamp;

	iso_tx_code = batchrec.getTxCode();
	txdata[TXRES_ISO_CODE] = iso_tx_code;

	rrn = batchrec.getRrn();
	if(rrn.length()) txdata[TXRES_RRN] = rrn;

	if (batchrec.getEmvTags().length() > 0) {
		emv_data = batchrec.getEmvTags();
		txdata[TXRES_EMV_DATA] = UtilsHexToString(emv_data.c_str(), emv_data.length());
	}

	cardExpiryDate = batchrec.getExp();
	txdata[TXRES_CARD_EXPIRY] = reason_code;

	std::ostringstream ss_card_fees;
	ss_card_fees << std::setw(12) << std::setfill('0') << batchrec.getCardFees();
	cardFees = ss_card_fees.str();
	txdata[TXRES_CARD_FEES] = cardFees;

	std::ostringstream ss_tx_fees;
	ss_tx_fees << std::setw(12) << std::setfill('0') << batchrec.getLoadFees();
	transactionFees = ss_tx_fees.str();
	txdata[TXRES_TRANSACTION_FEES] = transactionFees;

	if(emv_data.length() > 0){
		za_co_verifone_bertlv::BERTLV emvtlv = emv_data;
		emvtlv.getValForTag("\x50", card_type);
	}
	else{
		com_verifone_cardconfig::CCardConfig cardcfg;
		com_verifone_carddata::CCardData cardData;
		cardcfg.lookupCardConfig(batchrec.getPan(), cardData, batchrec.getTransCurrency());
		card_type = cardData.getCardIssuedName();
	}

	txdata[TXRES_CARD_TYPE] = card_type;

	acc_type = batchrec.getAccType();
	txdata[TXRES_ACC_TYPE] = acc_type;

	supervisor_id = batchrec.getSupervisorId();
	txdata[TXRES_SUPERVISOR] = supervisor_id;

	operator_id = batchrec.getCashierId();
	txdata[TXRES_OPERATOR] = operator_id;

	com_verifone_terminalconfig::CTerminalConfig termcfg;
	merchant_name = termcfg.getMerchantName();
	txdata[TXRES_MERCHANT_NAME] = merchant_name;

	if(batchrec.isApproved())
		txdata[TXRES_APPROVED] = "1";
	else
		txdata[TXRES_APPROVED] = "0";

	txdata[TXRES_SRC] = batchrec.getSrc();

	PAN_ENTRY_MODE entry_mode = batchrec.getPanEntryMode();

	switch(entry_mode)
	{
		case PEM_SWIPED: 	txdata[TXRES_INPUT_TYPE] = "S"; break;
		case PEM_ENTERED: 	txdata[TXRES_INPUT_TYPE] = "E"; break;
		case PEM_INSERTED: 	txdata[TXRES_INPUT_TYPE] = "I"; break;
		case PEM_TAPPED: 	txdata[TXRES_INPUT_TYPE] = "T"; break;
		case PEM_FALLBACK: 	txdata[TXRES_INPUT_TYPE] = "F"; break;
		default			 :	txdata[TXRES_INPUT_TYPE] = "N"; break; //iq_audi_190117 N means no
	}
}

TxResult::TxResult(CBatchRec batchrec)
{
	is_complete = false;
	went_online = false;

	batchno = -1;
	tsn = -1;
	rcptno = -1;

////	cout<< "iq: TxResult"<<endl;
//
//	int ret;
//	com_verifone_batchmanager::CBatchManager batchManager;
	com_verifone_batch::CBatch cbatch;

//	ret = batchManager.getBatch(cbatch);
//
//	if (ret != DB_OK)
//		return;
//
//	CBatchRec batchrec;
//	cbatch.readLastRecFromBatch(batchrec);

	if(batchrec.isInProgress()){
		txdata[TXRES_COMPLETE] = "0";
		return;					// Not a valid transaction
	}

	is_complete = true;
	txdata[TXRES_COMPLETE] = "1";

	if(batchrec.isApproveOnline()){
		went_online = true;
		txdata[TXRES_ONLINE] = "1";
	}else
		txdata[TXRES_ONLINE] = "0";

//	batchno = cbatch.getBatchNo();
//	txdata[TXRES_BATCHNO] = UtilsIntToString(batchno);

	tsn = batchrec.getBatchTxNo();
	txdata[TXRES_TSN] = UtilsIntToString(tsn);

	rcptno = UtilsStringToInt(batchrec.getReceiptNo());
	txdata[TXRES_RCPTNO] = batchrec.getReceiptNo();

	ecrno = UtilsStringToInt(batchrec.getEcrNo());		//iq_audi_301216
	txdata[TXRES_ECR_NUMBER] = batchrec.getEcrNo();

	response_code = batchrec.getRespCode();
	if(response_code.length() && (UtilsStringToInt(response_code) > 0)){
		txdata[TXRES_RESPCODE] = response_code;
	}

	string str_reason_code = batchrec.getReason();

	if(UtilsStringToInt(str_reason_code) <= 9)
		reason_code = "0" + str_reason_code;
	else
		reason_code = str_reason_code;

	if(reason_code.length() == 0){
		if(response_code.length() && (atoi(response_code.c_str()) == 0))
			reason_code = "00";
		else
			reason_code = "30";
	}

	txdata[TXRES_REASON_CODE] = reason_code;
	txdata[TXRES_RESP_MSG] =za_co_vfi_Operator::MsgRespCode(UtilsStringToInt(reason_code));

	if(batchrec.isTxVoided())
		txdata[TXRES_IS_VOIDED]=string("Yes");

	auth_code = batchrec.getAuthCode();
	if(auth_code.length()) txdata[TXRES_AUTHCODE] = auth_code;

	tx_currency = batchrec.getTransCurrency();
	txdata[TXRES_CURREYNCY] = tx_currency;

	tx_sale_amount = UtilsIntToString(batchrec.getTxAmount());
	txdata[TXRES_SALE_AMNT] = tx_sale_amount;

	tx_cash_amount = UtilsIntToString(batchrec.getCashAmount());
	if(tx_cash_amount.length() && (tx_cash_amount.compare("0") != 0)) txdata[TXRES_CASH_AMNT] = tx_cash_amount;

	pan = batchrec.getPan();
	if(pan.length()){
		pan.replace(6, (size_t)(pan.length()-10), (size_t)(pan.length()-10), '*');
		txdata[TXRES_PAN] = pan;
	}

	start_timestamp = batchrec.getTxDateTime();
	txdata[TXRES_TIMESTAMP] = start_timestamp;

	iso_tx_code = batchrec.getTxCode();
	txdata[TXRES_ISO_CODE] = iso_tx_code;

	rrn = batchrec.getRrn();
	if(rrn.length()) txdata[TXRES_RRN] = rrn;

	if (batchrec.getEmvTags().length() > 0) {
		emv_data = batchrec.getEmvTags();
		txdata[TXRES_EMV_DATA] = UtilsHexToString(emv_data.c_str(), emv_data.length());
	}

	cardExpiryDate = batchrec.getExp();
	txdata[TXRES_CARD_EXPIRY] = reason_code;

	std::ostringstream ss_card_fees;
	ss_card_fees << std::setw(12) << std::setfill('0') << batchrec.getCardFees();
	cardFees = ss_card_fees.str();
	txdata[TXRES_CARD_FEES] = cardFees;

	std::ostringstream ss_tx_fees;
	ss_tx_fees << std::setw(12) << std::setfill('0') << batchrec.getLoadFees();
	transactionFees = ss_tx_fees.str();
	txdata[TXRES_TRANSACTION_FEES] = transactionFees;

	if(emv_data.length() > 0){
		za_co_verifone_bertlv::BERTLV emvtlv = emv_data;
		emvtlv.getValForTag("\x50", card_type);
	}
	else{
		com_verifone_cardconfig::CCardConfig cardcfg;
		com_verifone_carddata::CCardData cardData;
		cardcfg.lookupCardConfig(batchrec.getPan(), cardData, batchrec.getTransCurrency());
		card_type = cardData.getCardIssuedName();
	}

	txdata[TXRES_CARD_TYPE] = card_type;

	acc_type = batchrec.getAccType();
	txdata[TXRES_ACC_TYPE] = acc_type;

	supervisor_id = batchrec.getSupervisorId();
	txdata[TXRES_SUPERVISOR] = supervisor_id;

	operator_id = batchrec.getCashierId();
	txdata[TXRES_OPERATOR] = operator_id;

	com_verifone_terminalconfig::CTerminalConfig termcfg;
	merchant_name = termcfg.getMerchantName();
	txdata[TXRES_MERCHANT_NAME] = merchant_name;

	if(batchrec.isApproved())
		txdata[TXRES_APPROVED] = "1";
	else
		txdata[TXRES_APPROVED] = "0";

	txdata[TXRES_SRC] = batchrec.getSrc();

	PAN_ENTRY_MODE entry_mode = batchrec.getPanEntryMode();

	switch(entry_mode)
	{
		case PEM_SWIPED: 	txdata[TXRES_INPUT_TYPE] = "S"; break;
		case PEM_ENTERED: 	txdata[TXRES_INPUT_TYPE] = "E"; break;
		case PEM_INSERTED: 	txdata[TXRES_INPUT_TYPE] = "I"; break;
		case PEM_TAPPED: 	txdata[TXRES_INPUT_TYPE] = "T"; break;
		case PEM_FALLBACK: 	txdata[TXRES_INPUT_TYPE] = "F"; break;
		default			 :	txdata[TXRES_INPUT_TYPE] = "N"; break; //iq_audi_190117 N means no
	}
}




bool TxResult::IsComplete(void)
{
	return is_complete;
}

bool TxResult::WentOnline(void)
{
	return went_online;
}

int TxResult::BatchNo(void)
{
	return batchno;
}

int TxResult::Tsn(void)
{
	return tsn;
}

int TxResult::RcptNo(void)
{
	return rcptno;
}

string TxResult::ResponseCode(void)
{
	return response_code;
}

string TxResult::AuthCode(void)
{
	return auth_code;
}

string TxResult::SaleAmount(void)
{
	return tx_sale_amount;
}

string TxResult::CashAmount(void)
{
	return tx_cash_amount;
}

string TxResult::Pan(void)
{
	return pan;
}

string TxResult::TimeStamp(void)
{
	return start_timestamp;
}

string TxResult::ISOCode(void)
{
	return iso_tx_code;
}

string TxResult::Rrn(void)
{
	return rrn;
}

string TxResult::CardType(void)
{
	return card_type;
}

string TxResult::AccType(void)
{
	return acc_type;
}

string TxResult::Supervisor(void)
{
	return supervisor_id;
}

string TxResult::Cashier(void)
{
	return operator_id;
}

string TxResult::MerchantName(void)
{
	return merchant_name;
}

}
/*
024330313030
DF44040001175E
DF42040000000A
DF028199
9F0206000000000100
9F0306000000000000
8A00
DF0100
DF0200
8E1C000000000000000042014403410342035E031F030000000000000000
9F1E00
DF0900
9F09020002
9F3303E0F8C8
5F2A020710
9C0100
CB00
9F6E00
9F7C00
50104465626974204D617374657243617264
57135576970000083797D15126200000001523000F
5F2403151231
5F2503130901
5F30020620
9F2000
910AF3EA0BDF400400000014
DF4104000000EA
FF014E
DFAF00103535373639372A2A2A2A2A2A33373937
DFAF01023030
DFAF030C303030303030303030303030
DFAF040C303030303030303030303030
DFAF05104465626974204D617374657243617264
03C9
*/
