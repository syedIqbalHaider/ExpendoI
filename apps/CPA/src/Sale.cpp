#include <string>
#include <unistd.h>
#include <gui/gui.h>
#include <gui/jsobject.h>
#include <libtui/tui.h>
#include <libda/ccardconfig.h>
#include <libda/chotcard.h>
#include <libda/cterminalconfig.h>

#include "Transaction.h"
#include "utils.h"
#include "CLog.h"
#include "Operator.h"
#include "CPAver.h"
#include "cstatus.h"

static CLog log(CPA_NAME);

//using namespace std;
using namespace Utils;
using namespace vfigui;
using namespace za_co_verifone_tui;
using namespace za_co_vfi_Operator;
using namespace com_verifone_hotcard;
using namespace com_verifone_status;
using namespace com_verifone_terminalconfig;

namespace za_co_vfi_Transaction
{

Sale::~Sale()
{
}

int Sale::DoTransaction(map<string, string>&txdata)
{
	int iRc;
	Operator op;
	string value;
	com_verifone_terminalconfig::CTerminalConfig termcfg;

	log.message(MSG_INFO "Sale - DoTransaction\r\n");

	import_txdata(txdata);

	initBatchRec();

	string interfaces;

	if(isMagEnabled())
		interfaces.append(1, (char)INTF_MAG);

	if(isManualEntryEnabled())
		interfaces.append(1, (char)INTF_MANUAL);

	if(emv_fallback_required){
		if(emvFallback() < 0){
			log.message(MSG_INFO "emvFallback() failed\n");
			return -1;
		}

		emv_fallback = true;	// Indicate that this transaction is a fallback transaction
	}else
		interfaces.append(1, (char)INTF_ICC);

	log.message(MSG_INFO "interfaces enable ["+UtilsHexToString(interfaces.c_str(), interfaces.length())+"]\n");

	CStatus status;
	status.setStatus(WAITING_FOR_CARD_INSERT);

	testAndSetBusyFlag();

	//8f tag capk from icc

	iRc=startTransaction(interfaces, getEMVTagTxType(EMV_TX_SALE).getStringData()+
												getEMVTagTxDate().getStringData()+
												getEMVTagTxTime().getStringData()+
												getEMVTagTxAmount().getStringData()+
												getEMVTagTxCurreny().getStringData(),
												string("\x9f\x02"\
														"\x9f\x03"\
														"\x4f"
														"\x84"\
														"\x82"\
														"\x9f\x36"\
														"\x9f\x07"\
														"\x8a"\
														"\xdf\x01"\
														"\xdf\x02"\
														"\x9f\x26"\
														"\x9f\x27"\
														"\x8e"\
														"\x9f\x34"\
														"\x9f\x1e"\
														"\x9f\x0d"\
														"\x9f\x0e"\
														"\x9f\x0f"\
														"\x9f\x10"\
														"\xdf\x09"\
														"\x9f\x09"\
														"\x9f\x33"\
														"\x9f\x1a"\
														"\x9f\x35"\
														"\x95"\
														"\x9f\x53"\
														"\x5f\x2a"\
														"\x9a"\
														"\x9b"\
														"\x9f\x41"\
														"\x9c"\
														"\x9f\x37"\
														"\xcb"\
														"\x9f\x6e"\
														"\x9f\x7c"\
														"\x5a"\
														"\x50"\
														"\x57"\
														"\x5f\x20"\
														"\x5f\x24"\
														"\x5f\x25"\
														"\x5f\x30"\
														"\x5f\x34"\
														"\x9f\x20"),termcfg.getWaitTime());

	log.message(MSG_INFO "pam.startTransaction()="+log.from_int(iRc)+"\r\n");

	while(1){
		LAST_COMMAND_TO_RESPOND last_command = getLastCommandToComplete();

		// First check the status of the last command
		if((last_command != NONE) && (pam_status != 0)){
			log.message(MSG_INFO "Error occured, pam_status=" + UtilsIntToString(pam_status) + "\n");
			removeCardPrompt();
			return -1;
		}

		if(last_command == START_TRANSACTION)
		{
			log.message(MSG_INFO "pam.getLastCommandToRespond()=START_TRANSACTION\r\n");

			// Check if fallback processing need to be done
			if(emv_fallback_required)
				return 1;

			// Check if card swipe was bad
			if(manual_entry_required){
				Operator op;
				op.displayErrorMsg("CardReadError");
				manual_entry_required = false;
				return 1;
			}

			if((entry_mode == PEM_ENTERED) || (entry_mode == PEM_SWIPED) || (entry_mode == PEM_FALLBACK)){
				log.message(MSG_INFO "PAN:["+track_data.pan()+"]\r\n");
				log.message(MSG_INFO "Expiry:["+track_data.expiry()+"]\r\n");
				log.message(MSG_INFO "Cardholder:["+track_data.cardholder()+"]\r\n");

//				iq_audi_190117
				string is_fallback_allowed;
				is_fallback_allowed=termcfg.getIsfallbackAllowed();
				if((entry_mode == PEM_FALLBACK)&& atoi(is_fallback_allowed.c_str())==0){
					log.message(MSG_INFO "Fallback not allowed!\n");
					op.displayErrorMsg("InsertCard");
					return 1;
				}

				// Only perform this check if insert was an enable interface
				// This is part of fallback
				if((entry_mode != PEM_FALLBACK) && (icc_card) && (interfaces.find((char)INTF_ICC) != string::npos)){
					log.message(MSG_INFO "ICC card was swiped!\n");
					op.displayErrorMsg("InsertCard");
					return 1;
				}

				// BIN lookup
				CCardConfig cardcfg;
				if(cardcfg.lookupCardConfig(track_data.pan(), cardData, txn_currency) <= 0){
					log.message(MSG_INFO "Card not found\n");
					op.displayErrorMsg("ServiceError");
					return -1;
				}

				printCardData(cardData);

#if 1	// Used for testing purposes
				//
				// Check expiry date
				//
				if((cardData.getCardProfile().isCheckExpiryDate()) && (track_data.isExpired())){
					op.displayErrorMsg("ExpiredError");
					return -1;
				}
#endif
//				//iq_audi_100117
//				//Check Luhn
				if((cardData.getCardProfile().isCheckLuhn()) && (!track_data.isLuhnVarified(track_data.pan().c_str()))){
					op.displayErrorMsg("InvalidCard");
					return -1;
				}

				// Check if card is active
				//
				if(!cardData.getCardProfile().isActive()){
					op.displayErrorMsg("NotActiveError");
					return -1;
				}

				//
				// Account selection
				//
				if((account_profile_index = op.accountSelection(cardData.getAccounts())) == 0){
					log.message(MSG_INFO "Account selection canceled\n");
//							break;
					return -1;
				}

				log.message(MSG_INFO "account_profile_index=" + UtilsIntToString(account_profile_index) + "\n");

				accprof = cardData.getAccountAt(account_profile_index-1);
				pin_required = accprof.isRequirePinEntry();

				//
				// Do CCD validation if relevant
				//
				if(!ccdCheck()){
					op.displayErrorMsg("CCDError");
					return -1;
				}

				//
				// Check if transaction is allowed
				//
				if(!isServiceAllowed()){
					op.displayErrorMsg("ServiceError");
					return -1;
				}

				//
				// Check amount Maximum limits
				//
				iRc=checkLimits();
				if(iRc){

					if(iRc==1)
						op.displayErrorMsg("LimitErrorMin");
					else
						op.displayErrorMsg("LimitError");

					sleep(3);
					return -1;
				}

				//
				// Do hotcard check
				//
				CHotcard hc;
				if(hc.isHotCard(track_data.pan())){
					op.displayErrorMsg("HotcardError");
					return -1;
				}

			}

			//iq_audi_100117
			if(entry_mode == PEM_INSERTED){

				BERTLV tlv(emvtlv);
				CCardProfile iccCardData;

				if(iccCardData.isCheckExpiryDate()){
					string expiryDate;
					expiryDate.clear();
					if(tlv.getValForTag("\x5f\x24", value))
						expiryDate=UtilsBcdToString(value.c_str(), 2);

					log.message(MSG_INFO "Expiry:["+expiryDate+"]\r\n");

					if(iccValidation.isExpired(expiryDate))
					{
						op.displayErrorMsg("ExpiredError");
						return -1;
					}
				}

				//iq_audi_100117
				//Check Luhn
				if(iccCardData.isCheckLuhn()){

					string sPan;
					sPan.clear();
					if(tlv.getValForTag("\x5a", value))
						sPan=UtilsBcdToString(value.c_str(), value.length());

					log.message(MSG_INFO "Pan:["+sPan+"]\r\n");


					if(!iccValidation.isLuhnVarified(sPan.c_str()))
					{
						op.displayErrorMsg("InvalidCard");
						return -1;
					}
				}

			}


			//
			// Get budget period, if it is allowed
			//
			doBudget();

			if(budget_period < 0){
				log.message(MSG_INFO "Transaction cancelled\n");
				return -1;
			}

			//
			// Set flags required for completion
			//
			check_src = accprof.isCheckCardValue();
			if(accprof.isDraftCaptureMode()) dcm = DCM_OFFLINE; else dcm = DCM_ONLINE;
			signature_required = accprof.isRequireSignature();

			op.waitDisp();

			//
			// Complete the transaction
			//
			if(txComplete(wait_for_h_command_response) < 0)
				return -1;

			return 0;
		}else if(last_command == CANCEL_TRANSACTION){
			log.message(MSG_INFO "Cancelled transaction remotely\r\n");

			dont_remove_card = false;		// In case it was set

			return -1;

		} else if(last_command == MODIFY_LIMITS){
			log.message(MSG_INFO "pam.getLastCommandToRespond()=MODIFY_LIMITS\n");

			testAndSetBusyFlag();

			//
			// Check amount limits
			//
			iRc=checkLimits();
			if(iRc){

				if(iRc==1)
					op.displayErrorMsg("LimitErrorMin");
				else
					op.displayErrorMsg("LimitError");

				sleep(3);
				modifyLimits(PAM_FORCE_DECLINE);
				return -1;
			}
			//
			// Do hotcard check
			//
			CHotcard hc;
			if(hc.isHotCard(pan)){
				op.displayErrorMsg("HotcardError");
				log.message(MSG_INFO "pam.modifyLimits() Hotcard!\n");
				iRc=modifyLimits(PAM_FORCE_DECLINE);

			}else{
				if(velocity_count_exceeded || velocity_period_exceeded){
					log.message(MSG_INFO "pam.modifyLimits() Velocity exceeded (PAM_FORCE_ONLINE)\n");
					modifyLimits(PAM_FORCE_ONLINE);
				}else if(above_floor_limit){
					log.message(MSG_INFO "pam.modifyLimits() Above floor limit (PAM_NO_DECISION)\n");

					CLimits limits = accprof.getLocalLimits();
					long floor_limit = UtilsStringToLong(limits.getFloorLimitAmt());
					iRc=modifyLimits(string(PAM_NO_DECISION), floor_limit);

				}else{
					log.message(MSG_INFO "pam.modifyLimits() Sending PAM_OK");
					iRc=modifyLimits(PAM_OK);
				}
			}

			log.message(MSG_INFO "pam.modifyLimits()="+log.from_int(iRc)+"\n");
		}

		usleep(100*1000);		// Sleep for 100 milli seconds nto prevent tight loop
	}

	return -1;
}

//
// Private methods
//

bool Sale::isServiceAllowed(void)
{
	bool bRet=false;

	set<string>all_tx = accprof.getAllowedTransactions() ;
	for(set<string>::iterator it=all_tx.begin(); it!=all_tx.end(); ++it){
		string tx = *it;

		if(iso_tx_code == UtilsStringToInt(tx))
			return true;
	}

	return bRet;
}

int Sale::doBudget(void)
{
	if(!accprof.isAllowBudget())
		return 0;	// This is not an error

	// Check minimum budget amount
	int imin_budget_limit = UtilsStringToInt(accprof.getBudgetAmountMin());
	int iamount = UtilsStringToInt(tx_sale_amount);
	if(iamount < imin_budget_limit)
		return 0;

	Operator op;
	budget_period = op.budgetPeriodSelection();

	log.message(MSG_INFO "doBudget - budget_period=" + UtilsIntToString(budget_period) + "\n");

	return 0;
}

int Sale::checkLimits(void)
{
	CLimits limits;

	if(emv_fallback)
		limits = accprof.getEmvFallBackLimits();
	else
		limits = accprof.getLocalLimits();

	long long account_limit_min = UtilsStringToLongLong(limits.getTransactionAmtMin());
	long long account_limit_max = UtilsStringToLongLong(limits.getTransactionAmtMax());
	long long floor_limit = UtilsStringToLongLong(limits.getFloorLimitAmt());
	long long iamount = UtilsStringToLongLong(tx_sale_amount);

	if(iamount < account_limit_min)
		return 1;

	if(iamount > account_limit_max)
		return 2;

	if(iamount > floor_limit){
		log.message(MSG_INFO "Floor limit of " + UtilsIntToString(floor_limit) + " exceeded\n");
		above_floor_limit = true;
	}

	return 0;
}

int Sale::import_txdata(map<string, string>&txdata)
{
	int count=0;

	for(map<string,string>::iterator it=txdata.begin(); it != txdata.end(); ++it){
		if(it->first.compare(TX_DATA_TX_AMOUNT) == 0){
			string amt = it->second;
			tx_sale_amount = amt;
			log.message(MSG_INFO "-->" + string(TX_DATA_TX_AMOUNT) + ":" + tx_sale_amount + "\r\n");
		}else if(it->first.compare(TX_DATA_TX_CURRENCY) == 0){
			string curr = it->second;
			txn_currency = curr;
			log.message(MSG_INFO "-->" + string(TX_DATA_TX_CURRENCY) + ":" + txn_currency + "\r\n");
		}
		else if(it->first.compare(TX_DATA_TX_DONT_REMOVE_CARD) == 0){
			string dRC = it->second;
			if (dRC.compare("1") == 0)
				dont_remove_card = true;

			log.message(MSG_INFO "-->" + string(TX_DATA_TX_DONT_REMOVE_CARD) + ":" + dRC + "\r\n");
		} else if(it->first.compare(TX_DATA_TX_H_CMD_ENABLED) == 0){
			string dRC = it->second;
			if (dRC.compare("1") == 0)
				wait_for_h_command_response = true;

			log.message(MSG_INFO "-->" + string(TX_DATA_TX_H_CMD_ENABLED) + ":" + dRC + "\r\n");
		}
	}

	return count;
}

}
