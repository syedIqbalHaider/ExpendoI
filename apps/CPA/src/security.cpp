/*
 * security.cpp
 *
 *  Created on: Aug 8, 2017
 *      Author: vfisdk
 */

#include <vficom.h>

#include <gui/gui.h>
//#include <log/liblog.h> // ADK Logging
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <svc_sec.h>
#include <errno.h>

#include <sstream>

#include "security.h"
#include "CPAver.h"
#include "CLog.h"
#include "utils.h"

static CLog log(CPA_NAME);

using namespace com_verifone_seccmd;
using namespace Utils;

#define SSTR( x ) dynamic_cast< std::ostringstream & >( std::ostringstream() << std::dec << x ).str()

int ippPinEncrypt(unsigned char *WorkingKey_GISKE, std::string pan, std::string amount, uint8_t *pinBlock)
{
	int inResult=0;
	unsigned char szPlainData[512];
	unsigned char szCipheredData[512];
	unsigned char pInputDataAgain[512];
	unsigned char ucPINBLOCK[8];
	unsigned char ucIV[8];
	char szCardPAN[16+1];
	bool isMSK=true;

	uint8_t errCode=0;
    Ksn_v2 stKSN_v2;
	unsigned char pucTemp[21];
	unsigned char KeyWorkingKey[121]={0};

	inResult = Sec_Init();
	log.message(MSG_INFO "-----Sec_Init: "+SSTR(inResult)+"\n");

	if(inResult)
		return -1;

//	vdGetVersions();


	if(isMSK)
	{
		//**********************************************************ADK-SEC IPP legacy-- ***************************************************************************************
		sprintf((char *)KeyWorkingKey, "%s",WorkingKey_GISKE);

//		log.message(MSG_INFO "GISKE Working Key ["+SSTR(KeyWorkingKey)+"] \r\n");

		inResult = inUpdateAdditionalKeys(HID04, KEYSET01, KEY_TYPE_TPK_FOR_PIN, 	 KeyWorkingKey, 120); 		//WK for PIN encryption

		if(inResult)
			return -1;

//		************************IPP MS PIN encryption

		inResult = Sec_SetKSId(HID04, KEYSET01);

		log.message(MSG_INFO "-----Sec_SetKSId HID04 KEYSET01 ["+SSTR(inResult)+"]\n");

		if(inResult)  //Master Key not found
			return -1;

		inResult = Sec_SelectHostConf(HID04, &errCode, 5000);

		log.message(MSG_INFO "-----Sec_SelectHostConf 01 ret= ["+SSTR(inResult)+"]  errno = ["+ SSTR(errCode)+"] \r\n");

		if(inResult)
			return -1;

		inResult = inIPPEnterAndEncryptPIN(HID04, (Ksn_v2 *)NULL, pan, amount, pinBlock);

	}else
	{
//		errCode=0;
//
//		stKSN_v2.KSNLen=  KSN_SIZE;
//		std::string key = "0D8E7C40CDFEE93C1111111111111111";
//		std::string ksn = "C552AE5B5DF000020000";
//		memset(pucTemp, 0x00, sizeof(pucTemp));
//		fAsc2Bcd((unsigned char *)ksn.c_str(), pucTemp, KSN_SIZE);
//
//		stKSN_v2.KSN = pucTemp;
//		inResult=Sec_UpdateKey  (HID05, KEY_TYPE_DUKPT, 0, (char *)key.c_str(), key.size(), stKSN_v2, &errCode, 20000);
//
//		LOGF_TRACE("-----inUpdateDUKPTKeys [ret %x - errcode=%d]", inResult, errCode);
//
//		//************************IPP DUKPT PIN encyption
//		inResult = Sec_SetKSId(HID05, KEYSET01);
//		LOGF_TRACE("-----Sec_SetKSId HID05 KEYSET01 [ret %d]", inResult);
//		inResult=Sec_SelectHostConf(HID05, &errCode);
//		LOGF_TRACE("-----Sec_SelectHostConf 05 [ret %d - errno: %d]",inResult, errCode);
//
//		u_char KSN_digits[21] = "";
//		stKSN_v2.KSN=KSN_digits;
//		stKSN_v2.KSNLen=20;
//		inResult = inIPPEnterAndEncryptPIN(HID05, &stKSN_v2);
//		LOGF_TRACE("-----inIPPEnterAndEncryptPIN ret= %d",inResult);
	}

	 Sec_Destroy( );

	return inResult;
}

void vdGetVersions()
{
  int ret;
  uint8_t errCode=0;
  std::string versions;
  const char *APIVersion;

  ret = Sec_GetVersions(&versions, &errCode, TIMEOUT);
  log.message(MSG_INFO "-----Sec_GetVersions() ret = ["+SSTR(ret)+"] errcode= [ "+SSTR(errCode)+"] \r\n");

  if(ret == 0){
	  log.message(MSG_INFO "-----Version Info: "+versions+"\n");
  }
  APIVersion = Sec_GetVersion();
  log.message(MSG_INFO "-----API Lib Version: "+SSTR(APIVersion)+"\n");
}

int inUpdateAdditionalKeys(unsigned char ucHostId, unsigned int ucKeySetId, unsigned char inKeyType, unsigned char * pucKey, int inKeySize)
{
	uint8_t errCode=0;
	int inRet;

	inRet = Sec_UpdateKey(ucHostId, inKeyType, (char *)pucKey, (u_long)inKeySize, &errCode, TIMEOUT); // HostID, KeySetID, KeyType, KeyData
	log.message(MSG_INFO "-----Sec_SetKSId: "+SSTR(inRet)+"\n");

	if(inRet)
		return -1;
	else
		return 0;
}

int inIPPEnterAndEncryptPIN(int inHostId, Ksn_v2* stKSN, std::string PANfield, std::string AmountText, uint8_t* pinBlock)
{
	int inResult=0;
	uint8_t errCode=0;
	PINParams_v3 stPINParameter;
	PINBlockParams stPINBlockParams;
	PropData propData;
	EncPINBlock stPINBlock;
	unsigned char ucPB[25];
	uint8_t ucPinBlock[16+1]={0};

	std::string PINMessageText;
	std::string AddScreenText;
	std::string AddScreenText2;

	PINMessageText = "Enter PIN";

//	if (stKSN == NULL) //IPP MS
//		AddScreenText = "IPP Master Session";
//	else //DUKPT
//		AddScreenText = "IPP DUKPT";
//	//AddScreenText2 = "Add Text 2";

	memset(&stPINParameter, 0x00, sizeof(PINParams_v3));
	memset(&stPINBlockParams, 0x00, sizeof(PINBlockParams));
	memset(&propData, 0x00, sizeof(PropData));
	memset(&stPINBlock, 0x00, sizeof(EncPINBlock)); // ------------ set pinPara -------------
	memset(ucPB,0,sizeof(ucPB));

    std::string PinTextHtmlPath = "scapp/EnterPIN.html";

	stPINParameter.pPinTextHtmlPath = (unsigned char *) PinTextHtmlPath.c_str();

	stPINParameter.PinEntryType = 0;// PIN EntryType
	stPINParameter.PinTryFlag = 0;// PIN Try Flag
	stPINParameter.PINMessageText = (u_char *)PINMessageText.c_str();// PIN Message Text
	stPINParameter.AmountText = (u_char *)AmountText.c_str();// Amount Text
	stPINParameter.AddScreenText = (u_char *)AddScreenText.c_str();// Additional Screen Text
	stPINParameter.AddScreenText2 = (u_char *)AddScreenText2.c_str();// Additional Screen Text 2
	stPINParameter.MinPINLength = 4;// Minimum PIN length
	stPINParameter.MaxPINLength = 6;// Maximum PIN length
	stPINParameter.PinCancel = 0;// PIN cancel
	// ------------ set pinBlockPara -------------
	stPINBlockParams.pPAN = (unsigned char *)PANfield.c_str();
//	Only use pSTAN if the PINblock format is 1 = ISO-1
//    stPINBlockParams.pSTAN = (unsigned char *)"1";// STAN

	stPINBlockParams.PinBlockFormat = 0;// PIN Block Format
	// ------------ set Propriety Data (propData) -------------
	propData.pPropData = NULL;
	propData.uiPropDataLen = 0;
	// ------- set encrypted PIN Block (output) parameter -----------
	stPINBlock.ucPINBlockLen = 25;
	stPINBlock.pPINBlock = (uint8_t *)ucPB;

	if (stPINBlock.pPINBlock == NULL)
		return 1;

	memset(stPINBlock.pPINBlock, 0, stPINBlock.ucPINBlockLen);
	inResult = Sec_EnterAndEncryptPIN(inHostId, &stPINParameter, &stPINBlockParams, 1, propData, &stPINBlock, stKSN, &errCode, 20000);

	log.message(MSG_INFO "Sec_EnterAndEncryptPIN ["+SSTR(inResult)+"]  errno = ["+ SSTR(errCode)+"] \r\n");
	printf("Sec_EnterAndEncryptPIN PB Len = [%d] \r\n",stPINBlock.ucPINBlockLen);
	log.message(MSG_INFO "PINBlock ["+SSTR(stPINBlock.pPINBlock)+"] \r\n");


	if(!inResult && (stPINBlock.ucPINBlockLen==20))
	{
		memcpy(pinBlock,stPINBlock.pPINBlock+4,16);

	}else
	{
		log.message(MSG_INFO "Pin Cancelled \r\n");

		inResult=-1;
	}

	return inResult;
}


