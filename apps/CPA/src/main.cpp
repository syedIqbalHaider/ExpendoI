#include <string>
#include <new>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <iostream>
#include <sys/types.h>
#include <sys/timeb.h>
#include <gui/gui.h>
#include <gui/jsobject.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <sstream>
#include <fcntl.h>
#include <iostream>
#include <execinfo.h>
#include <signal.h>

#include <liblog/logsys.h>
#include <libtui/tui.h>
//#include <libtui/xmlmenu.h>
#include <libipc/ipc.h>
#include <libda/cterminalconfig.h>
#include <libhim.h>
#include <libpam.h>
#include <libda/cbatchmanager.h>
#include <libda/cbatch.h>
#include <libda/cconmanager.h>
#include <libda/cversions.h>
#include <libda/cversionsfile.h>

#include "CLog.h"
#include "utils.h"
#include "CPATags.h"
#include "CPAver.h"
#include "bertlv.h"
#include "menubmp.h"
#include "Transaction.h"
//#include "CAuthUser.h"
#include "libalarm.h"
#include "cstatus.h"
#include "Reports.h"
#include "Operator.h"
#include "TxResult.h"
#include "libalarm.h"
#include "libcpa.h"
#include "libhim.h"
#include "alarm.h"
#include "signals.h"

static CLog log(CPA_NAME);
#define POS_INT_NAME		"POSINT"

//using namespace std;
using namespace vfigui;
using namespace za_co_vfi_Him;
using namespace za_co_verifone_tui;
//using namespace za_co_verifone_xmlmenu;
using namespace Utils;
using namespace CPATags;
using namespace za_co_verifone_bertlv;
using namespace com_verifone_terminalconfig;
using namespace com_verifone_status;
using namespace za_co_vfi_MenuBmp;
using namespace za_co_vfi_Transaction;
//using namespace com_verifone_user;
using namespace za_co_vfi_reports;
using namespace IdleAlarmNS;
using namespace com_verifone_status;

void fault_handler(int signal);
void *CommandThread(void *parm);
void *PosInterfaceThread(void *parm);
string get_tx_response(void);
string get_tx_responseBatch(CBatchRec batchrec);
int doTransitCommands(int txType,map<string,string> &txdata);

int process_pos_tx(map<string,string>&txdata);
int process_command(string received_from, string command);
int tx_select(map<string,string>&txdata);
bool maxBatchExceeded(void);
int get_last_complete_rec(CBatchRec& rec);
int get_sale_rec(string ecr, CBatchRec& rec);
//void checkAuthenticated(CAuthUser &cu);
//std::string doMenuAction(std::string command);

#define SSTR( x ) dynamic_cast< std::ostringstream & >( std::ostringstream() << std::dec << x ).str()


static Transaction *currTx;
static int retrySettleCount = -1;
static int retryPrmCount = -1;

const std::string AL_PRMDWN = "AlPrmDwn";
const std::string AL_SETTLE = "AlSettle";
const std::string AL_SETPRM = "AlSetPrm";

class IdleAlarmCPA : public IdleAlarm
{

public:
	IdleAlarmCPA(const std::string local_app, const std::string master_app = ALARM_MASTER)
		: IdleAlarm(local_app,master_app)
	{
	}

	void setAlarm(std::string alarmName, std::string aTime ) {
		// Replace time now with specified time
		int iTime = UtilsStringToInt(aTime);
		time_t tNow = time(NULL);
		time_t t = tNow;
		struct tm ts;
		//struct tm  *tt =
		localtime_r(&t, &ts);
		ts.tm_hour = iTime/100;
		ts.tm_min = iTime % 100;
		t = mktime(&ts);
		if (t<=tNow) t += 24*60*60; // add 24 hours

		log.message(MSG_INFO + alarmName + "=" + aTime +",hm=" + SSTR(ts.tm_hour) + "\n");

		alarmClear(alarmName);
		alarmSet(t,alarmName);

		char buf[64];
		strftime(buf, sizeof(buf), "%d.%m.%Y - %H:%M:%S", localtime(&t));
		log.message(MSG_INFO"Set alarm " + alarmName + ",t=" + SSTR(buf) + "\n");
	}

	// Note: We use fixed ids for the alarms to simplify things;
	// if required we could save the id returned by alarmSet to work
	// with the system generated ids.

	void setParameterAlarms() {
		CTerminalConfig termcfg;
		std::string pTime = termcfg.getParamDnldTime();
		std::string sTime = termcfg.getSettlementTime();
		bool isParamTime = (pTime.length() == 4) && (pTime != "0000");
		bool isSettleTime = (sTime.length() == 4) && (sTime != "0000");
		std::string sCmd = AL_SETTLE;

		if (isSettleTime && isParamTime && (sTime == pTime)) {
			isParamTime = false;
			sCmd = AL_SETPRM;
		}

		if (isParamTime) {
			setAlarm(AL_PRMDWN,pTime);
		}
		if (isSettleTime) {
			if (sCmd == AL_SETTLE)
				alarmClear(AL_SETPRM);
			else
				alarmClear(AL_SETTLE);

			setAlarm(sCmd,sTime);
		}

	}

	int resetFailedDownloadAlarm(CTerminalConfig &termcfg, std::string alarmName, int &retryCount) {

		// get retry and time to wait
		int retries = termcfg.getMaxSettleConnectionAttempts();
		if (retryCount == -1) {
			// First Retry - set retrycount and schedule first
			retryCount = retries;
		} else if (retryCount == 0) {
			// Retries exceeded - reset retryCount to -1 and schedule for one day later
			retryCount = -1;
			log.message((MSG_INFO "Download retries exceeded (") + UtilsIntToString(retries) + "), reschedule one day later.\n");
			setParameterAlarms(); 	// Reset prm and settle alarms; will add 24h if past today's time
		}

		if (retryCount > 0) {
			// Retries left - decrement retrycount and schedule next retry for wait period later
			retryCount--;

			// Add wait minutes to current time
			long secs = termcfg.getWaitTime();
			secs *= 60;
			time_t t = time(NULL);
			t += secs;

			char buf[10];
			strftime(buf, sizeof(buf), "%H%M", localtime(&t));
			log.message((MSG_INFO "Download failed, reschedule alarm ") + alarmName + " "
					+ UtilsIntToString(secs/60) + " min later, cnt=" + UtilsIntToString(retryCount) +".\n");

			setAlarm(alarmName + "_retry",buf);
		}
		return retryCount;
	}

	virtual void alarmAction(std::string alarmId, stdStringMap &alarmData) {
		(void)alarmData;

		log.message(MSG_INFO"Triger on id " + alarmId  + "\n");
		CTerminalConfig termcfg;
		std::string testSettle[] = {AL_SETTLE, AL_SETPRM, AL_SETTLE + "_retry", AL_SETPRM + "_retry" };

		for (uint i=0; i<4;i++) {
			if (isSameAlarm(testSettle[i],alarmId))  {
				za_co_vfi_Operator::Operator op;
				if (op.doSettlement()) {
					uint r = testSettle[i].rfind("_retry");
					if (r== std::string::npos) r = testSettle[i].length();
					resetFailedDownloadAlarm(termcfg,testSettle[i].substr(0,r), retrySettleCount);
				} else {
					retrySettleCount = -1;
				}
				break;
			}
		}
		std::string testPrms[] = {AL_PRMDWN, AL_SETPRM, AL_PRMDWN + "_retry", AL_SETPRM + "_retry" };
		for (uint i=0; i<4;i++) {
			log.message(MSG_INFO"Testing " + testPrms[i]  + "\n");
			if (isSameAlarm(testPrms[i],alarmId))  {

				// Workaround so that popup messages when downloading does not get
				// hidden when you click on them
				uiDisplay("");

				za_co_vfi_Operator::Operator op;
				CStatus status;
				if (op.doParameterDownload(status,true,true)) {
					//termcfg.setSetupComplete(false);
					uint r = testPrms[i].rfind("_retry");
					if (r== std::string::npos) r = testPrms[i].length();
					resetFailedDownloadAlarm(termcfg,testPrms[i].substr(0,r),retryPrmCount);
					op.displayErrorMsg("PrmDownloadError");
				} else {
					retryPrmCount = -1;
				}
				break;
			}
		}
		setParameterAlarms();

		CStatus status;
		status.setStatus(TERMINAL_READY);
	}
};

IdleAlarmCPA *cpaAlarms = NULL;

int AlarmResetParmAlarm(void)
{
	log.message(MSG_INFO "AlarmResetParmAlarm()\r\n");

	if(cpaAlarms != NULL){
		cpaAlarms->setParameterAlarms();
		return 1;
	}

	return 0;
}

#define POS_INT_NAME		"POSINT"
#define POS_INT_CMD_THR		"POS_INT_CMD_THR"
pthread_t pos_int_thread_id;
pthread_t command_thread_id;

static bool posint_busy=false;
static bool send_message_success = false;
static bool send_message_cancelled = false;

struct CmdParm {
	unsigned char rxPacket[8192];
    unsigned int  rxLen;
    char cmdFrom[100];
};

void *CommandThread(void *parm)
{
	string tag, len, val;
	string tmpl;
	int ret;

	log.message(MSG_INFO "Started Command Thread\r\n");
	com_verifone_ipc::init(string(POS_INT_CMD_THR));

	uiSetPropertyString (UI_PROP_RESOURCE_PATH, "flash/"CPA_NAME"rsc/");
	uiReadConfig();

	CmdParm *cmd = (CmdParm*)parm;
	std::string ipcBuffer(reinterpret_cast<const char *>(cmd->rxPacket), cmd->rxLen);
	std::string ipcFrom(reinterpret_cast<const char *>(cmd->cmdFrom), strlen(cmd->cmdFrom));

	free(parm);

	BERTLV cmdTlv(ipcBuffer);
	cmdTlv.getTagLenVal(0, tag, len, val);

	log.message(MSG_INFO "Command: CPA_CMD_POS_TX_RQST\r\n");

	BERTLV txTlv(val);
	string amount, txnCurrency, txtype, dontRemoveCard, transitData, hcommand, pan;
	map<string,string>txdata;

	log.message(MSG_INFO "--> Incoming " + Utils::UtilsHexToString(val.c_str(),val.length()) + "\r\n");

	if(txTlv.getValForTag(CMD_PARAM_TX_H_COMMAND_ENABLED, hcommand)) {
		txdata[TX_DATA_TX_H_CMD_ENABLED] = hcommand;
		log.message(MSG_INFO "--> CMD_PARAM_TX_H_COMMAND_ENABLED " + hcommand + "\r\n");
	}


	if(txTlv.getValForTag(CMD_PARAM_TX_AMOUNT, amount)){
		amount = UtilsBcdToString((const char*)amount.c_str(), (unsigned int)amount.length());
		log.message(MSG_INFO "--> CMD_PARAM_TX_AMOUNT "+UtilsIntToString(amount.length())+" [" + amount + "]\r\n");
		txdata[TX_DATA_TX_AMOUNT] = amount;
	}

	if(txTlv.getValForTag(CMD_PARAM_TX_CURRENCY, txnCurrency)){
		log.message(MSG_INFO "--> CMD_PARAM_TX_CURRENCY " + txnCurrency + "\r\n");
		txdata[TX_DATA_TX_CURRENCY] = txnCurrency;
	}

	if(txTlv.getValForTag(CMD_PARAM_TX_TYPE, txtype)){
		log.message(MSG_INFO "--> CMD_PARAM_TX_TYPE " + txtype + "\r\n");
		txdata[TX_DATA_TX_TYPE] = txtype;
	}

	// check for other tags
	if(txTlv.getValForTag(CMD_PARAM_TX_DONT_REMOVE_CARD, dontRemoveCard)){
		log.message(MSG_INFO "--> CMD_PARAM_TX_DONT_REMOVE_CARD " + dontRemoveCard + "\r\n");
		txdata[TX_DATA_TX_DONT_REMOVE_CARD] = dontRemoveCard;
	}

	if(txTlv.getValForTag(CMD_PARAM_TX_DATA_TX_PRODUCT, transitData)){
		log.message(MSG_INFO "--> CMD_PARAM_TX_DATA_TX_PRODUCT " + UtilsHexToString(transitData.c_str(),transitData.length()) + "\r\n");
		txdata[TX_DATA_TX_PRODUCT] = transitData;
	}

	if(txTlv.getValForTag(CMD_PARAM_PAN, pan)){
		log.message(MSG_INFO "--> TX_DATA_VALIDATE_PAN " + dontRemoveCard + "\r\n");
		txdata[TX_DATA_VALIDATE_PAN] = pan;
	}

	ret = process_pos_tx(txdata);
	if (ret < 0){
		send_message_cancelled = true;
		send_message_success = false;
	}else{
		send_message_success = true;
		send_message_cancelled = false;
	}

	CStatus status;
	status.setStatus(RETURN_TO_IDLE);

	com_verifone_ipc::deinit(0);

	return NULL;
}

void *PosInterfaceThread(void *parm)
{
	(void)parm;

	log.message(MSG_INFO "Started Pos Interface Thread\r\n");

	com_verifone_ipc::init(string(POS_INT_NAME));

	string ipcBuffer;
	string ipcFrom;
	string ipcFromPos;

	uiSetPropertyString (UI_PROP_RESOURCE_PATH, "flash/"CPA_NAME"rsc/");
	uiReadConfig();
  
	while(1){
		if(com_verifone_ipc::receive(ipcBuffer,"",ipcFrom)==com_verifone_ipc::IPC_SUCCESS){
			string tag, len, val;

			log.message(MSG_INFO "POS interface Got message ["+UtilsHexToString((const char*)ipcBuffer.c_str(), (unsigned int)ipcBuffer.length())+"]\r\n");

			BERTLV cmdTlv(ipcBuffer);
			cmdTlv.getTagLenVal(0, tag, len, val);

			if(tag.length() == 0){
				log.message(MSG_INFO "Invalid Tlv received!\n");
				continue;
			}

			if(tag.compare(CPA_CMD_POS_STATUS_RQST) == 0) {
				//log.message(MSG_INFO "POS interface - Status Request\r\n");
				CStatus status;
				STATUS statusCode = status.getStatus();
				com_verifone_ipc::send("Status:"+SSTR(statusCode),ipcFrom);
				//log.message(MSG_INFO "Sent reply to ["+ipcFrom+"]\r\n");
			} else if (tag.compare(CPA_CMD_POS_CANCEL_RQST) == 0) {
				log.message(MSG_INFO "Send cancel tx command to PAM\r\n");

				if (currTx != NULL) {
					log.message(MSG_INFO "Use existing transaction object\r\n");
					currTx->cancelTransaction();
					com_verifone_ipc::send("Cancelled",ipcFrom);
				} else {
/*
					log.message(MSG_INFO "Create PAM object\r\n");
					PAM pam(POS_INT_NAME,true);
					pam.removeCard();
*/
					Transaction txn(0);
					txn.removeCardPrompt();

					CStatus status;
					status.statusClose();

					com_verifone_ipc::send("Cancelled",ipcFrom);
				}
			}
			else if (tag.compare(CPA_CMD_POS_RETURN_IDLE) == 0) {
								CStatus status;
								status.statusClose();

								com_verifone_ipc::send("Cancelled",ipcFrom);
						}
			else if (tag.compare(CPA_CMD_POS_REVERSAL_RQST) == 0) {
				log.message(MSG_INFO "Request reverse last\r\n");

				// Check if there is something to reverse
				com_verifone_batchmanager::CBatchManager batchManager;
				com_verifone_batch::CBatch cbatch;
				CBatchRec batchrec;
				za_co_vfi_Operator::Operator op;

				batchManager.getBatch(cbatch);

				int iret = get_last_complete_rec(batchrec);

				if((iret < 0) ){
				   op.displayErrorMsg("TxNotFoundError");
				}else{
					if(!batchrec.isTxCanceled()){
					  Him him;
					  iret = him.ReverseLast();

					  get_last_complete_rec(batchrec);	// Update the batch record with changes from HIM

					  Receipt receipt(batchrec);

					  receipt.merchantCopy(false);
					  receipt.customerCopy(false);
					}else{
					  op.displayErrorMsg("ReverseError");
					}
				}

				CStatus status;
				status.statusClose();

				com_verifone_ipc::send("Result:"+SSTR(iret),ipcFrom);

			}
			else if (tag.compare(CPA_CMD_POS_VOID_RQST) == 0) {
				log.message(MSG_INFO "Request VOID \r\n");

				// Check if there is something to void
				com_verifone_batchmanager::CBatchManager batchManager;
				com_verifone_batch::CBatch cbatch;
				CBatchRec batchrec;
				za_co_vfi_Operator::Operator op;
				string amount,txnCurrency, ecrBcd, ecr;
				batchManager.getBatch(cbatch);
				bool isCancelled=false;
				BERTLV txTlv(val);


				log.message(MSG_INFO "--> Incoming " + Utils::UtilsHexToString(val.c_str(),val.length()) + "\r\n");

				if(txTlv.getValForTag(CMD_PARAM_TX_AMOUNT, amount)){
					amount = UtilsBcdToString((const char*)amount.c_str(), (unsigned int)amount.length());
					log.message(MSG_INFO "iq: --> CMD_PARAM_TX_AMOUNT "+UtilsIntToString(amount.length())+" [" + amount + "]\r\n");
				}

				if(txTlv.getValForTag(CMD_PARAM_TX_CURRENCY, txnCurrency)){
					log.message(MSG_INFO "--> CMD_PARAM_TX_CURRENCY " + txnCurrency + "\r\n");
				}

				if(txTlv.getValForTag(CMD_PARAM_ECR_NUMBER, ecrBcd)){
					ecr = UtilsBcdToString((const char*)ecrBcd.c_str(), (unsigned int)ecrBcd.length());
					log.message(MSG_INFO "iq: --> CMD_PARAM_ECR_NUMBER "+UtilsIntToString(ecr.length())+" [" + ecr + "]\r\n");
				}

				int iret =  get_sale_rec(ecr, batchrec);

				if((iret < 0) || (batchrec.getTransCurrency()!=txnCurrency)){
				   op.displayErrorMsg("TxNotFoundError");
				   isCancelled=true;
				   sleep(2);
				}else{

					string amountRec =zero_pad(SSTR(batchrec.getTxAmount()),12);

					log.message(MSG_INFO "iq: --> amountRec "+UtilsIntToString(amountRec.length())+" [" + amountRec + "]\r\n");

					if(amount==amountRec)
					{
						if(!batchrec.isTxVoided()){
						  Him him;
						  iret = him.VoidSale(ecrBcd);

						  get_sale_rec(ecr, batchrec);

						  Receipt receipt(batchrec);

						  receipt.merchantCopy(false);
						  receipt.customerCopy(false);
						}else{
						  op.displayErrorMsg("VoidError");
						  isCancelled=true;
						  sleep(2);
						}
					}
					else{
						 op.displayErrorMsg("VoidAmountMisMatch");
						 isCancelled=true;
						 sleep(2);
					}
				}

				CStatus status;
				status.statusClose();

				if(isCancelled)
					com_verifone_ipc::send("Error",ipcFrom);
				else
					com_verifone_ipc::send("Finished:"+get_tx_responseBatch(batchrec),ipcFrom);

			}
			else if (tag.compare(CPA_CMD_POS_CLEAR_REVERSAL) == 0) {
				log.message(MSG_INFO "Request reversal clear\r\n");

				Him him;

				int iret = him.ClearReversal();

				za_co_vfi_Operator::Operator op;

				if(!iret)
				{
					op.clearReversalDisp();
					sleep(1);
				}

				CStatus status;
				status.statusClose();
				com_verifone_ipc::send("Result:"+SSTR(iret),ipcFrom);

			}
			else if  (tag.compare(CPA_CMD_POS_BANKING_RQST) == 0) {
				log.message(MSG_INFO "Request banking\r\n");
				Him him;
				int ret = him.DoBanking();
				com_verifone_ipc::send("Result:"+SSTR(ret),ipcFrom);
			} else if  (tag.compare(CPA_CMD_POS_PARAMETER_RQST) == 0) {
				log.message(MSG_INFO "Request parameters\r\n");
				Him him;
			//					int ret = him.FullParameters();
				int ret = him.PartialParameters();
				com_verifone_ipc::send("Result:"+SSTR(ret),ipcFrom);
			} else {

				ipcFromPos = ipcFrom;
				if(!posint_busy){
					struct CmdParm *poscmd;
					posint_busy = true;

					poscmd = (struct CmdParm*)malloc(sizeof(struct CmdParm));

					if(poscmd == NULL){
						cout << "Failed to allocate memory for CommandThread" << endl;
						continue;
					}

					memset(poscmd->cmdFrom,0,sizeof(poscmd->cmdFrom));
					memcpy(poscmd->cmdFrom,ipcFrom.c_str(),ipcFrom.length());

					// Make sure we dont try and process too much data for our buffer size
					if(ipcBuffer.length() < sizeof(poscmd->rxPacket)){
						CStatus status;
						status.setStatus(TERMINAL_BUSY);

						memcpy(poscmd->rxPacket,ipcBuffer.c_str(),ipcBuffer.length());
						poscmd->rxLen = ipcBuffer.length();
						int iret = pthread_create(&command_thread_id, NULL, CommandThread, (void*)poscmd);

						if(iret != 0){
							log.message(MSG_INFO "PosInterfaceThread(): ------>!!!CommandThread failed to start!!!<-----\r\n");
						}else{
							pthread_detach(command_thread_id);
						}
					}else{
						cout << "Failed to allocate sufficient memory for CommandThread" << endl;
						free(poscmd);
						posint_busy = false;
						com_verifone_ipc::send("Cancelled",ipcFrom);
					}
				} else {
					// send back busy
					com_verifone_ipc::send("Busy",ipcFrom);
					log.message(MSG_INFO "Sent Busy reply to ["+ipcFrom+"]\r\n");
				}
			}
		} else {
			// check if message to return
			if (send_message_success) {
				// Need to send transaction result info back
				log.message(MSG_INFO "Before send reply to ["+ipcFromPos+"]\r\n");

				com_verifone_ipc::send("Finished:"+get_tx_response(),ipcFromPos);

				log.message(MSG_INFO "Sent reply to ["+ipcFromPos+"]\r\n");

				send_message_success = false;
				send_message_cancelled = false;
				posint_busy = false;

			} else if (send_message_cancelled) {
				// Need to send transaction result info back
				log.message(MSG_INFO "Before send reply to ["+ipcFromPos+"]\r\n");

				com_verifone_ipc::send("Finished:Cancelled=1;",ipcFromPos);

				log.message(MSG_INFO "Sent reply to ["+ipcFromPos+"]\r\n");

				send_message_success = false;
				send_message_cancelled = false;
				posint_busy = false;
			}
		}

		usleep(100000);
	}

	com_verifone_ipc::deinit(0);

	return NULL;
}

pthread_t tVersion;

void *update_versions(void*)
{
	com_verifone_versions::CVersions versions;
	com_verifone_versionsfile::CVersionsFile vfile;

	log.message(MSG_INFO "CPA start writing versions\r\n");

	vfile.setName("");
	vfile.setVersion(CPA_VERSION);

	// Keep trying every second, until we succeed
	while(1){
		if(versions.addFile(CPA_NAME, vfile) == DB_OK)
			break;

		sleep(1);
	}

	log.message(MSG_INFO "CPA versions complete\r\n");

	return NULL;
}
void uiMain(int argc, char *argv[])
{
	(void)argc;			// Suppresses -> warning: unused parameter
	(void)argv;			// Suppresses -> warning: unused parameter

	dlog_init(CPA_NAME);

	SignalsConfigureHandlers();

	log.message(MSG_INFO CPA_NAME " " + UtilsTimestamp() + " starting...\r\n");

	pthread_create(&pos_int_thread_id,NULL,PosInterfaceThread,NULL);
	pthread_detach(pos_int_thread_id);

//	pthread_create(&tVersion,NULL,&update_versions,NULL);

	com_verifone_ipc::init(string(CPA_NAME));

	int ret = TuiInit(CPA_NAME);

	if(ret < 0){
		char buffer[10];
		sprintf(buffer, "%d", ret);
		log.message(MSG_INFO "TuiInit returned "+string(buffer)+"\r\n");
	}

	uiSetPropertyString (UI_PROP_RESOURCE_PATH, "flash/"CPA_NAME"rsc/");
	uiReadConfig();

//	com_verifone_ipc::init(string(CPA_NAME));

	//// Comment out next 2 lines if you do not want to authenticate
	//CAuthUser cAuthUser;
	//checkAuthenticated(cAuthUser);


	while(1){
		string ipcBuffer;
		string ipcFrom;

		if(com_verifone_ipc::receive(ipcBuffer,string(""),ipcFrom)==com_verifone_ipc::IPC_SUCCESS){

			log.message(MSG_INFO "Got message ["+UtilsHexToString((const char*)ipcBuffer.c_str(), (unsigned int)ipcBuffer.length())+"]\r\n");

			if (cpaAlarms == NULL) {
				cpaAlarms = new IdleAlarmCPA(CPA_NAME);
			}
			// First check for alarm
			std::string resp = cpaAlarms->alarmProcessIPC(ipcFrom,ipcBuffer);
			if (!resp.empty()) {
				if (resp.find("Error") == 0) {
					log.message((MSG_INFO  "Alarm Error response received by CPA from ") + ipcFrom + ":["+resp+"]\r\n");
				} else {
					// IdleApp alarm command processed correctly, do nothing
				}
			} else if(ipcBuffer.find("Menu") == 0){
				log.message(MSG_INFO "Got Menu command\n");
				za_co_vfi_Operator::Operator op;
				std::string resp =  op.doOperatorMenu(ipcBuffer);
				com_verifone_ipc::send("Finished:Menu="+resp,ipcFrom);
			} else if(ipcBuffer.find("Setup") == 0){
				log.message(MSG_INFO "Got Setup command\n");
				za_co_vfi_Operator::Operator op;
				int rc =  op.doCpaSetup();
				if (rc == 0) {
					// Now set alarms
					cpaAlarms->setParameterAlarms();
				}
				com_verifone_ipc::send("Finished:Setup="+UtilsIntToString(rc),ipcFrom);
			}else{
				CStatus status;
				status.setStatus(TRANSACTION_START);

				process_command(ipcFrom, ipcBuffer);

				status.setStatus(TRANSACTION_COMPLETE);
			}
		}

		usleep(500000);		// Sleep for 100 milli seconds
	}
}

int main(int argc, char *argv[])
{
	uiMain (argc, argv);
	return 0;
}

// Used to debug setting alarms
void addToConnectTime(bool isParameters) {
	com_verifone_terminalconfig::CTerminalConfig termcfg;
	time_t t = time(NULL);
	t += 60; // add 60 secs

	char buf[10];
	strftime(buf, sizeof(buf), "%H%M", localtime(&t));
	if (isParameters)
		termcfg.setParamDnldTime(buf);
	else
		termcfg.setSettlementTime(buf);

	cpaAlarms->setParameterAlarms();
}

void sanitise_db()
{
	com_verifone_batchmanager::CBatchManager batchManager;
	com_verifone_batch::CBatch cbatch;

	int ret = batchManager.getBatch(cbatch);

	if (ret != DB_OK){
		log.message(MSG_INFO "Failure retrieving batch record\r\n");
		return;
	}

	cbatch.delLastRecFromBatchIfIncomplete();
}

string get_tx_response(void)
{
	za_co_verifone_txResult::TxResult txres;
/*
	if(!txres.IsComplete())
		return string("Cancelled=1;");
*/
	string result;
	for(std::map<string,string>::iterator it=txres.txdata.begin(); it!=txres.txdata.end(); ++it){
		log.message(MSG_INFO+it->first+"="+it->second+";\n");
		result.append(it->first+"="+it->second+";");
	}

	return result;
}

string get_tx_responseBatch(CBatchRec batchrec)
{
	za_co_verifone_txResult::TxResult txres(batchrec);
	string result;
	for(std::map<string,string>::iterator it=txres.txdata.begin(); it!=txres.txdata.end(); ++it){
		log.message(MSG_INFO+it->first+"="+it->second+";\n");
		result.append(it->first+"="+it->second+";");
	}

	return result;
}

int doTransitCommands(int txType,map<string,string> &txdata) {
	log.message(MSG_INFO "Transit cmd:" + UtilsIntToString(txType) + "\r\n");
	int ret=-1;

	// Check that we dont exceed the maximum transactions allowed in the batch
	if(maxBatchExceeded()){
		log.message(MSG_INFO "Canceling ...\r\n");
		za_co_vfi_Operator::Operator op;
		op.batchFull();

		return -1;
	}

	sanitise_db();

	CStatus status;
	status.setStatus(TRANSACTION_START);

	switch (txType) {
		case 0: //Sale
		{
			if( UtilsStringToInt(txdata[TX_DATA_TX_AMOUNT]) == 0 ){
				log.message(MSG_INFO "tx_select(): Invalid 0 amount\n");
				za_co_vfi_Operator::Operator op;
				op.displayErrorMsg("ZeroAmountError");
				ret = -1;
				break;
			}
					
			log.message(MSG_INFO "process_pos_tx(): Financial request (Goods and Services)\r\n");
			log.message(MSG_INFO "doTransitCommands():******************************************** Instantiating Sale object **************************************\r\n");
			Sale sale;
			do {
				currTx = &sale;
				ret = sale.DoTransaction(txdata);
			} while(ret > 0);

			sale.removeCardPrompt();

			log.message(MSG_INFO "doTransitCommands():************************************************** Request Complete ********************************************\r\n");
			currTx = NULL;
			break;
		}
		case 20: //Refund
		{
			break;
		}
		case 30: // Pre Deposit
		{
			break;
		}
		case 32: // Transit Data Enquiry
		{
			log.message(MSG_INFO "doTransitCommands():************************************ Transit Data Enq: Instantiating Transit object ****************************\r\n");

			TxDataEnq transCmd;
			do {
				currTx = &transCmd;
				ret = transCmd.DoTransaction(txdata,EMV_TX_TRANS_DATA_ENQ);
			} while(ret > 0);

			transCmd.removeCardPrompt();

			log.message(MSG_INFO "doTransitCommands():************************************************** Request Complete ********************************************\r\n");
			currTx = NULL;
			break;
		}
		case 31: // Online Balance Enquiry
		{
			log.message(MSG_INFO "doTransitCommands():************************************ Online Bal Request: Instantiating Transit object ****************************\r\n");

			TransitCmd transCmd;
			do {
				currTx = &transCmd;
				ret = transCmd.DoTransaction(txdata,EMV_TX_ONLINE_BAL_ENQ);
			} while(ret > 0);

			transCmd.removeCardPrompt();

			log.message(MSG_INFO "doTransitCommands():************************************************** Request Complete ********************************************\r\n");
			currTx = NULL;
			break;
		}
		case 33: // Offline Balance Enquiry
		{

			log.message(MSG_INFO "doTransitCommands():************************************ Offline Bal Request: Instantiating Transit object ****************************\r\n");

			OfflineBal transCmd;
			do {
				currTx = &transCmd;
				ret = transCmd.DoTransaction(txdata,EMV_TX_OFFLINE_BAL_ENQ);
			} while(ret > 0);

			transCmd.removeCardPrompt();

			log.message(MSG_INFO "doTransitCommands():************************************************** Request Complete ********************************************\r\n");
			currTx = NULL;
			break;
		}
		case 93: // Offline Mini Statement
		{
			log.message(MSG_INFO "doTransitCommands():************************************ Offline Mini Statement Request: Instantiating Transit object ****************************\r\n");

			OfflineMini transCmd;
			do {
				currTx = &transCmd;
				ret = transCmd.DoTransaction(txdata,EMV_TX_OFFLINE_MIN_STAT);
			} while(ret > 0);

			transCmd.removeCardPrompt();

			log.message(MSG_INFO "doTransitCommands():************************************************** Request Complete ********************************************\r\n");
			currTx = NULL;
			break;
		}
		case 84: // Cash Load
		{

			log.message(MSG_INFO "doTransitCommands():************************************ Cash Load Request: Instantiating Transit object ****************************\r\n");

			TransitCmd transCmd;
			do {
				currTx = &transCmd;
				ret = transCmd.DoTransaction(txdata,EMV_TX_CASH_LOAD);
			} while(ret > 0);

			transCmd.removeCardPrompt();

			log.message(MSG_INFO "doTransitCommands():************************************************** Request Complete ********************************************\r\n");
			currTx = NULL;
			break;
		}
		case 86: // Load Transit Product
		{
			log.message(MSG_INFO "doTransitCommands():************************************ Load Product Request: Instantiating Transit object ****************************\r\n");

			TransitCmd transCmd;
			do {
				currTx = &transCmd;
				ret = transCmd.DoTransaction(txdata,EMV_TX_LOAD_TRANS_PROD);
			} while(ret > 0);

			transCmd.removeCardPrompt();

			log.message(MSG_INFO "doTransitCommands():************************************************** Request Complete ********************************************\r\n");
			currTx = NULL;
			break;
		}
		case 92: // PIN Change
		{
			log.message(MSG_INFO "doTransitCommands():************************************ Pin Change Request: Instantiating Transit object ****************************\r\n");

			TransitCmd transCmd;
			do {
				currTx = &transCmd;
				ret = transCmd.DoTransaction(txdata,EMV_TX_PIN_CHANGE);
			} while(ret > 0);

			transCmd.removeCardPrompt();

			log.message(MSG_INFO "doTransitCommands():************************************************** Request Complete ********************************************\r\n");
			currTx = NULL;
			break;
		}
		default:
			break;

// Future stuff
//	case 33: // Offline Balance Enquiry
//	case 40: // Nominated Account Transfer
//	case 41: // Card to Card Transfer
//	case 42: // Funds Transfer
//	case 93: // Mini Statement
	}

	status.setStatus(TRANSACTION_COMPLETE);

	return ret;
}

int process_command(string received_from, string command)
{
	int iret=0;
	BERTLV cmdTlv(command);
	string tag, len, val;
	string tmpl;

	cmdTlv.getTagLenVal(0, tag, len, val);

	// Check that we dont exceed the maximum transactions allowed in the batch
	if(maxBatchExceeded()){
		log.message(MSG_INFO "Canceling ...\r\n");
		za_co_vfi_Operator::Operator op;
		op.batchFull();

		goto CANCELLED;
	}

	sanitise_db();

	if(tag.compare(CPA_CMD_PURCHASE_RQST) == 0){
		BERTLV txTlv(val);
		string amount, txnCurrency;
		map<string,string>txdata;

		log.message(MSG_INFO "Command: CPA_PURCHASE_RQST\r\n");

		if(txTlv.getValForTag(CMD_PARAM_TX_AMOUNT, amount)){
			amount = UtilsBcdToString((const char*)amount.c_str(), (unsigned int)amount.length());
			log.message(MSG_INFO "--> CMD_PARAM_TX_AMOUNT "+UtilsIntToString(amount.length())+" [" + amount + "]\r\n");
			txdata[TX_DATA_TX_AMOUNT] = amount;
		}

		// Check that the amount is valid
		if(UtilsStringToInt(amount) == 0){
			log.message(MSG_INFO "tx_select(): Invalid 0 amount\n");
			za_co_vfi_Operator::Operator op;
			op.displayErrorMsg("ZeroAmountError");
			goto CANCELLED;
		}

		if(txTlv.getValForTag(CMD_PARAM_TX_CURRENCY, txnCurrency)){
			log.message(MSG_INFO "--> CMD_PARAM_TX_CURRENCY " + txnCurrency + "\r\n");
			txdata[TX_DATA_TX_CURRENCY] = txnCurrency;
		}


		log.message(MSG_INFO "process_command():************************************ Purchase Request: Instantiating Sale object ****************************\r\n");

		Sale sale;
		int saleret;

		do{
			log.message(MSG_INFO "Calling DoTransaction()\n");
			saleret = sale.DoTransaction(txdata);
		}while(saleret > 0);

		sale.removeCardPrompt();

		log.message(MSG_INFO "process_command():************************************************** Sale Complete ********************************************\r\n");

		if(saleret < 0)
			goto CANCELLED;

	}else if(tag.compare(CPA_CMD_TX_RQST) == 0){
		BERTLV txTlv(val);
		string amount;
		map<string,string>txdata;

		log.message(MSG_INFO "Command: CPA_CMD_TX_RQST\r\n");

		if(txTlv.getValForTag(CMD_PARAM_TX_AMOUNT, amount)){
			log.message(MSG_INFO "--> CMD_PARAM_TX_AMOUNT [" + UtilsHexToString((const char*)amount.c_str(), (unsigned int)amount.length()) + "]\r\n");
			txdata[TX_DATA_TX_AMOUNT] = amount;
		}

		if(tx_select(txdata) < 0)
			goto CANCELLED;

	}else if(tag.compare(CPA_CMD_SETTLE_RQST) == 0){
		log.message(MSG_INFO "Command: CPA_CMD_SETTLE_RQST\r\n");

		Him him;
		if(him.DoBanking() == 0){
			log.message(MSG_INFO "Printing batch report\n");
			za_co_vfi_reports::BatchReport brprt;
			brprt.previousBatch();
		}else{
			log.message(MSG_INFO "Banking failed\n");
		}

		com_verifone_ipc::send("Finished",received_from);
		log.message(MSG_INFO "Sent reply to ["+received_from+"]\r\n");

		return 0;

	}else if(tag.compare(CPA_CMD_POS_TX_RQST) == 0){
		log.message(MSG_INFO "Command: CPA_CMD_POS_TX_RQST\r\n");

		BERTLV txTlv(val);
		string amount, txnCurrency, txtype;
		map<string,string>txdata;

		if(txTlv.getValForTag(CMD_PARAM_TX_AMOUNT, amount)){
			amount = UtilsBcdToString((const char*)amount.c_str(), (unsigned int)amount.length());
			log.message(MSG_INFO "--> CMD_PARAM_TX_AMOUNT "+UtilsIntToString(amount.length())+" [" + amount + "]\r\n");
			txdata[TX_DATA_TX_AMOUNT] = amount;
		}

		if(txTlv.getValForTag(CMD_PARAM_TX_CURRENCY, txnCurrency)){
			log.message(MSG_INFO "--> CMD_PARAM_TX_CURRENCY " + txnCurrency + "\r\n");
			txdata[TX_DATA_TX_CURRENCY] = txnCurrency;
		}

		if(txTlv.getValForTag(CMD_PARAM_TX_TYPE, txtype)){
			log.message(MSG_INFO "--> CMD_PARAM_TX_TYPE " + UtilsIntToString((int)txtype.at(0)) + "\r\n");
			txdata[CMD_PARAM_TX_TYPE] = txtype;
		}

		if(process_pos_tx(txdata) < 0)
			goto CANCELLED;

	}else if(tag.compare(CPA_CMD_REP_CURR_BATCH) == 0){
		log.message(MSG_INFO "Command: CPA_CMD_REP_CURR_BATCH\r\n");

		BatchReport batchrprt;
		batchrprt.currentBatch();

	}else if(tag.compare(CPA_CMD_REP_PREV_BATCH) == 0){
		log.message(MSG_INFO "Command: CPA_CMD_REP_PREV_BATCH\r\n");

		BatchReport batchrprt;
		batchrprt.previousBatch();

	}else{
		log.message(MSG_INFO "Error: Unknown command [" + UtilsHexToString((const char*)command.c_str(), (unsigned int)command.length()) + "]\r\n");
		iret = -1;
		goto FINISHED;
	}

	// Need to send transaction result info back
	com_verifone_ipc::send("Finished:"+get_tx_response(),received_from);
	log.message(MSG_INFO "Sent reply to ["+received_from+"]\r\n");

	return 0;

CANCELLED:
	com_verifone_ipc::send("Finished:Cancelled=1;",received_from);
	log.message(MSG_INFO "Sent reply to ["+received_from+"]\r\n");
	return 0;

FINISHED:
	com_verifone_ipc::send("ERROR!",received_from);
	log.message(MSG_INFO "Sent reply to ["+received_from+"]\r\n");

	return iret;
}


int process_pos_tx(map<string,string>&txdata)
{
	int txtype=-1;

	txtype = atoi(txdata[TX_DATA_TX_TYPE].c_str());

	return (doTransitCommands(txtype,txdata));
}

int tx_select(map<string,string>&txdata)
{
	CTerminalConfig termcfg;
	MenuBmp menu(termcfg.getTerminalMenuBitMap());
	int iret = -1;

#if 1
	log.message(MSG_INFO "tx_select(): Purchase                 "); if(menu.SalesPurchases()) 				log.message("1\r\n"); else log.message("0\r\n");
	log.message(MSG_INFO "tx_select(): Refund                   "); if(menu.ReturnsRefunds()) 				log.message("1\r\n"); else log.message("0\r\n");
	log.message(MSG_INFO "tx_select(): Change PIN               "); if(menu.ChangePin()) 					log.message("1\r\n"); else log.message("0\r\n");
	log.message(MSG_INFO "tx_select(): Correction/Reversal      "); if(menu.CorrectionsReverse()) 			log.message("1\r\n"); else log.message("0\r\n");
	log.message(MSG_INFO "tx_select(): Balance Enquiry          "); if(menu.BalanceEnquiry()) 				log.message("1\r\n"); else log.message("0\r\n");
	log.message(MSG_INFO "tx_select(): Deposit/Payment          "); if(menu.DepositPayment()) 				log.message("1\r\n"); else log.message("0\r\n");
	log.message(MSG_INFO "tx_select(): Cash Withdrawel          "); if(menu.CashWithdrawal()) 				log.message("1\r\n"); else log.message("0\r\n");
	log.message(MSG_INFO "tx_select(): Sale with Cash           "); if(menu.SalesPurchasesWithCashback()) 	log.message("1\r\n"); else log.message("0\r\n");
	log.message(MSG_INFO "tx_select(): Card Activation          "); if(menu.CardActivation()) 				log.message("1\r\n"); else log.message("0\r\n");
	log.message(MSG_INFO "tx_select(): Mpad Funds Load          "); if(menu.MpadFundsLoad()) 				log.message("1\r\n"); else log.message("0\r\n");
	log.message(MSG_INFO "tx_select(): Mpad Funds Unload        "); if(menu.MpadFundsUnload()) 				log.message("1\r\n"); else log.message("0\r\n");
	log.message(MSG_INFO "tx_select(): Mpad Statement           "); if(menu.MpadStatement()) 				log.message("1\r\n"); else log.message("0\r\n");
	log.message(MSG_INFO "tx_select(): Mpad Baslance Enquiry    "); if(menu.MpadBalanceEnquiry()) 			log.message("1\r\n"); else log.message("0\r\n");
	log.message(MSG_INFO "tx_select(): Pre Auth                 "); if(menu.PreAuth()) 						log.message("1\r\n"); else log.message("0\r\n");
	log.message(MSG_INFO "tx_select(): Pre Auth Completion      "); if(menu.PreAuthCompletion()) 			log.message("1\r\n"); else log.message("0\r\n");
	log.message(MSG_INFO "tx_select(): Pre Auth Cancel          "); if(menu.PreAuthCancel()) 				log.message("1\r\n"); else log.message("0\r\n");
	log.message(MSG_INFO "tx_select(): Voice Override           "); if(menu.VoiceOverride()) 				log.message("1\r\n"); else log.message("0\r\n");
	log.message(MSG_INFO "tx_select(): Quasi Cash               "); if(menu.QuasiCashWithdrawal()) 			log.message("1\r\n"); else log.message("0\r\n");
	log.message(MSG_INFO "tx_select(): Card Issue               "); if(menu.CardIssue()) 					log.message("1\r\n"); else log.message("0\r\n");
	log.message(MSG_INFO "tx_select(): Card Reload              "); if(menu.CardReload()) 					log.message("1\r\n"); else log.message("0\r\n");
	log.message(MSG_INFO "tx_select(): Giftcard Balance Enquiry "); if(menu.GiftCardBalanceEnquiry()) 		log.message("1\r\n"); else log.message("0\r\n");
	log.message(MSG_INFO "tx_select(): Order Reserve            "); if(menu.OrderReserve()) 				log.message("1\r\n"); else log.message("0\r\n");
	log.message(MSG_INFO "tx_select(): Order Fullfill           "); if(menu.OrderFulfill()) 				log.message("1\r\n"); else log.message("0\r\n");
	log.message(MSG_INFO "tx_select(): Redemption               "); if(menu.Redemption()) 					log.message("1\r\n"); else log.message("0\r\n");
#endif


	struct UIMenuEntry *txmenu = new UIMenuEntry[menu.BitCount()];
	int option_count=0;

	if(menu.SalesPurchases()) 				{txmenu[option_count++].text = TuiString("MenuPurchase"); 		txmenu[option_count-1].value=MenuBmp::MENU_TX_Purchase;}
	if(menu.ReturnsRefunds()) 				{txmenu[option_count++].text = TuiString("MenuRefund"); 		txmenu[option_count-1].value=MenuBmp::MENU_TX_Refund;}
	if(menu.ChangePin()) 					{txmenu[option_count++].text = TuiString("MenuChangePin"); 		txmenu[option_count-1].value=MenuBmp::MENU_TX_ChangePin;}
	if(menu.CorrectionsReverse()) 			{txmenu[option_count++].text = TuiString("MenuCorrection"); 	txmenu[option_count-1].value=MenuBmp::MENU_TX_Correction;}
	if(menu.BalanceEnquiry()) 				{txmenu[option_count++].text = TuiString("MenuBalEnq"); 		txmenu[option_count-1].value=MenuBmp::MENU_TX_BalEnq;}
	if(menu.DepositPayment()) 				{txmenu[option_count++].text = TuiString("MenuPayment"); 		txmenu[option_count-1].value=MenuBmp::MENU_TX_Payment;}
	if(menu.CashWithdrawal()) 				{txmenu[option_count++].text = TuiString("MenuCash"); 			txmenu[option_count-1].value=MenuBmp::MENU_TX_Cash;}
	if(menu.SalesPurchasesWithCashback()) 	{txmenu[option_count++].text = TuiString("MenuSaleCash"); 		txmenu[option_count-1].value=MenuBmp::MENU_TX_SaleCash;}
	if(menu.CardActivation()) 				{txmenu[option_count++].text = TuiString("MenuCardAct"); 		txmenu[option_count-1].value=MenuBmp::MENU_TX_CardAct;}
	if(menu.MpadFundsLoad()) 				{txmenu[option_count++].text = TuiString("MenuFundsLoad"); 		txmenu[option_count-1].value=MenuBmp::MENU_TX_FundsLoad;}
	if(menu.MpadFundsUnload()) 				{txmenu[option_count++].text = TuiString("MenuFundsUnload"); 	txmenu[option_count-1].value=MenuBmp::MENU_TX_MenuFundsUnload;}
	if(menu.MpadStatement()) 				{txmenu[option_count++].text = TuiString("MenuMPSatement"); 	txmenu[option_count-1].value=MenuBmp::MENU_TX_MPSatement;}
	if(menu.MpadBalanceEnquiry()) 			{txmenu[option_count++].text = TuiString("MenuMPBalEnq"); 		txmenu[option_count-1].value=MenuBmp::MENU_TX_MPBalEnq;}
	if(menu.PreAuth()) 						{txmenu[option_count++].text = TuiString("MenuPreAuth"); 		txmenu[option_count-1].value=MenuBmp::MENU_TX_PreAuth;}
	if(menu.PreAuthCompletion()) 			{txmenu[option_count++].text = TuiString("MenuPreAuthCompl"); 	txmenu[option_count-1].value=MenuBmp::MENU_TX_PreAuthCompl;}
	if(menu.PreAuthCancel()) 				{txmenu[option_count++].text = TuiString("MenuPreAuthCancel"); 	txmenu[option_count-1].value=MenuBmp::MENU_TX_PreAuthCancel;}
	if(menu.VoiceOverride()) 				{txmenu[option_count++].text = TuiString("MenuVoiceOverride"); 	txmenu[option_count-1].value=MenuBmp::MENU_TX_VoiceOverride;}
	if(menu.QuasiCashWithdrawal()) 			{txmenu[option_count++].text = TuiString("MenuQuasiCash"); 		txmenu[option_count-1].value=MenuBmp::MENU_TX_QuasiCash;}
	if(menu.CardIssue()) 					{txmenu[option_count++].text = TuiString("MenuCardIssue"); 		txmenu[option_count-1].value=MenuBmp::MENU_TX_CardIssue;}
	if(menu.CardReload()) 					{txmenu[option_count++].text = TuiString("MenuCardReload"); 	txmenu[option_count-1].value=MenuBmp::MENU_TX_CardReload;}
	if(menu.GiftCardBalanceEnquiry()) 		{txmenu[option_count++].text = TuiString("MenuGiftBalEnq"); 	txmenu[option_count-1].value=MenuBmp::MENU_TX_GiftBalEnq;}
	if(menu.OrderReserve()) 				{txmenu[option_count++].text = TuiString("MenuOrderRes"); 		txmenu[option_count-1].value=MenuBmp::MENU_TX_OrderRes;}
	if(menu.OrderFulfill()) 				{txmenu[option_count++].text = TuiString("MenuOrderFulF"); 		txmenu[option_count-1].value=MenuBmp::MENU_TX_OrderFulF;}
	if(menu.Redemption()) 					{txmenu[option_count++].text = TuiString("MenuRedeem"); 		txmenu[option_count-1].value=MenuBmp::MENU_TX_Redeem;}

	for (int i=0;i<option_count;i++)
		txmenu[i].options = 0;

	string menu_header = TuiString("TxSelectMenuHeader");
	int selection = uiMenu("menu", menu_header, txmenu, menu.BitCount(), 1);

	if(selection >= MenuBmp::MENU_TX_Purchase){
		switch(selection)
		{
			case MenuBmp::MENU_TX_Purchase:
				log.message(MSG_INFO "tx_select(): Selected Purchase\r\n");
				{
					log.message(MSG_INFO "tx_select():******************************************** Instantiating Sale object **************************************\r\n");
					Sale sale;
					iret = sale.DoTransaction(txdata);

					sale.removeCardPrompt();

					log.message(MSG_INFO "tx_select():************************************************** Sale Complete ********************************************\r\n");

					break;
				}
			case MenuBmp::MENU_TX_Refund:
				log.message(MSG_INFO "tx_select(): Selected Refund\r\n");
				break;
			case MenuBmp::MENU_TX_ChangePin:
				log.message(MSG_INFO "tx_select(): Selected Change PIN\r\n");
				break;
			case MenuBmp::MENU_TX_Correction:
				log.message(MSG_INFO "tx_select(): Selected Correction\r\n");
				break;
			case MenuBmp::MENU_TX_BalEnq:
				log.message(MSG_INFO "tx_select(): Selected Balance Enquiry\r\n");
				break;
			case MenuBmp::MENU_TX_Payment:
				log.message(MSG_INFO "tx_select(): Selected Deposit/Payment\r\n");
				break;
			case MenuBmp::MENU_TX_Cash:
				log.message(MSG_INFO "tx_select(): Selected Cash Withdrawel\r\n");
				break;
			case MenuBmp::MENU_TX_SaleCash:
				log.message(MSG_INFO "tx_select(): Selected Sale with Cash\r\n");
				break;
			case MenuBmp::MENU_TX_CardAct:
				log.message(MSG_INFO "tx_select(): Selected Card Activate\r\n");
				break;
			case MenuBmp::MENU_TX_FundsLoad:
				log.message(MSG_INFO "tx_select(): Selected Funds Load\r\n");
				break;
			case MenuBmp::MENU_TX_MenuFundsUnload:
				log.message(MSG_INFO "tx_select(): Selected Funds Unload\r\n");
				break;
			case MenuBmp::MENU_TX_MPSatement:
				log.message(MSG_INFO "tx_select(): Selected MPad Statement\r\n");
				break;
			case MenuBmp::MENU_TX_MPBalEnq:
				log.message(MSG_INFO "tx_select(): Selected MPad Balance Enquiry\r\n");
				break;
			case MenuBmp::MENU_TX_PreAuth:
				log.message(MSG_INFO "tx_select(): Selected Pre Auth\r\n");
				break;
			case MenuBmp::MENU_TX_PreAuthCompl:
				log.message(MSG_INFO "tx_select(): Selected Pre Auth Complete\r\n");
				break;
			case MenuBmp::MENU_TX_PreAuthCancel:
				log.message(MSG_INFO "tx_select(): Selected Pre Auth Cancel\r\n");
				break;
			case MenuBmp::MENU_TX_VoiceOverride:
				log.message(MSG_INFO "tx_select(): Selected Voice Override \r\n");
				break;
			case MenuBmp::MENU_TX_QuasiCash:
				log.message(MSG_INFO "tx_select(): Selected Quasi Cash\r\n");
				break;
			case MenuBmp::MENU_TX_CardIssue:
				log.message(MSG_INFO "tx_select(): Selected Card Issue\r\n");
				break;
			case MenuBmp::MENU_TX_CardReload:
				log.message(MSG_INFO "tx_select(): Selected Card Reload\r\n");
				break;
			case MenuBmp::MENU_TX_GiftBalEnq:
				log.message(MSG_INFO "tx_select(): Selected Giftcard Balance Enquiry\r\n");
				break;
			case MenuBmp::MENU_TX_OrderRes:
				log.message(MSG_INFO "tx_select(): Selected Order Reserve\r\n");
				break;
			case MenuBmp::MENU_TX_OrderFulF:
				log.message(MSG_INFO "tx_select(): Selected Order Fullfill\r\n");
				break;
			case MenuBmp::MENU_TX_Redeem:
				log.message(MSG_INFO "tx_select(): Selected Redemption\r\n");
				break;
		}
	}else{
		log.message(MSG_INFO "tx_select(): Timeout or menu error\r\n");
	}

	delete[] txmenu;

	return iret;
}

bool maxBatchExceeded(void)
{
	com_verifone_batchmanager::CBatchManager batchManager;
	com_verifone_batch::CBatch cbatch;

	int ret = batchManager.getBatch(cbatch);

	if (ret != DB_OK){
		log.message(MSG_INFO "Failure retrieving batch record\r\n");
		return false;
	}

	com_verifone_terminalconfig::CTerminalConfig termcfg;
	int max_count = termcfg.getBatchMax();
	std::map<unsigned int, CBatchRec> records;
	int current_count = 0;

	cbatch.getRecords(records);

	std::map<unsigned int,CBatchRec>::iterator it;
	for (it=records.begin(); it!=records.end(); ++it) {
		CBatchRec batchRec = it->second;

		if(!batchRec.isInProgress())
			current_count++;
	}

	if(current_count >= max_count){
		log.message(MSG_INFO "maxBatchExceeded(): Maximum batch count of "+UtilsIntToString(max_count)+" exceeded\r\n");
		return true;
	}

	return false;
}

int get_last_complete_rec(CBatchRec& rec)
{
  com_verifone_batchmanager::CBatchManager batchManager;
  com_verifone_batch::CBatch cbatch;
  std::map<unsigned int, CBatchRec> batchRecs;

  int iso_tx_code;
  batchManager.getBatch(cbatch);
  cbatch.getRecords(batchRecs);

  std::map<unsigned int,CBatchRec>::reverse_iterator itr;

  for (itr=batchRecs.rbegin(); itr!=batchRecs.rend(); ++itr) {
    CBatchRec batchRec = itr->second;

    if(!batchRec.isInProgress() && UtilsStringToInt(batchRec.getRespCode()) == 0){
    	iso_tx_code = UtilsStringToInt(batchRec.getTxCode());
		if( iso_tx_code == TX_ISO_SALE || iso_tx_code == TX_ISO_CASH_ADVANCE || iso_tx_code == TX_ISO_SALE_CASH ||
				iso_tx_code == TX_ISO_DEPOSIT || iso_tx_code == TX_ISO_PMNT_DEPOSIT )	{
			  rec = batchRec;
			  log.message(MSG_INFO "get_last_complete_rec - found it\n");
			  return 0;
		}
    }
  }

  log.message(MSG_INFO "get_last_complete_rec did not find a record\r\n");

  return -1;
}

//Get sale record based on ECR number
int get_sale_rec(string ecr, CBatchRec& rec)
{
  com_verifone_batchmanager::CBatchManager batchManager;
  com_verifone_batch::CBatch cbatch;
  std::map<unsigned int, CBatchRec> batchRecs;

  int iso_tx_code;
  batchManager.getBatch(cbatch);
  cbatch.getRecords(batchRecs);

//  log.message(MSG_INFO "iq: Void ECR ["+ecr+"]\r \n");

  std::map<unsigned int,CBatchRec>::reverse_iterator itr;

  for (itr=batchRecs.rbegin(); itr!=batchRecs.rend(); ++itr) {
	CBatchRec batchRec = itr->second;

	string ecrRec=zero_pad(batchRec.getEcrNo(), 8);

//	  log.message(MSG_INFO "iq:  ECR ["+ecrRec+"] \r\n");
//	  if(ecrRec==ecr)
//		  log.message(MSG_INFO "iq: batchRec.getEcrNo()==ecr \r\n");
//	  if(memcmp(ecrRec.c_str(),ecr.c_str(),8)==0)
//		  log.message(MSG_INFO "iq: memcmp \r\n");

	if(ecrRec==ecr &&!batchRec.isInProgress() && (!batchRec.isDeclined()) && !batchRec.isTxCanceled()/* &&
			UtilsStringToInt(batchRec.getRespCode()) == 0*/){
		iso_tx_code = UtilsStringToInt(batchRec.getTxCode());

		 log.message(MSG_INFO "iq: batchRec.getTxCode()["+batchRec.getTxCode()+ "]\r\n");

		if( iso_tx_code == TX_ISO_SALE || iso_tx_code == TX_ISO_VOID /*|| iso_tx_code == TX_ISO_CASH_ADVANCE || iso_tx_code == TX_ISO_SALE_CASH ||
				iso_tx_code == TX_ISO_DEPOSIT || iso_tx_code == TX_ISO_PMNT_DEPOSIT*/ )	{
			  rec = batchRec;
			  log.message(MSG_INFO "iq: Record Found \n");
			  return 0;
		}
	}
  }

  log.message(MSG_INFO "get_sale_rec did not find a record\r\n");

  return -1;
}




//void checkAuthenticated(CAuthUser &cu) {
//
//	if (!cu.isAuthenticated()) {
//		int r = cu.authenticate();
//
//		if (r == 0) {
//
//			log.message("isAuthenticated=" + SSTR(cu.isAuthenticated()) + "\r\n");
//			log.message("isBankingAllowed=" + SSTR(cu.isBankingAllowed())+ "\r\n");
//			log.message("getDateAdded=" + SSTR(cu.getDateAdded())+ "\r\n");
//			log.message("getId=" + SSTR(cu.getId())+ "\r\n");
//			log.message("getName=" + SSTR(cu.getName())+ "\r\n");
//			log.message("getPin=" + SSTR(cu.getPin())+ "\r\n");
//			log.message("isPinRequired=" + SSTR(cu.isPinRequired())+ "\r\n");
//			log.message("isRefundsAllowed=" + SSTR(cu.isRefundsAllowed())+ "\r\n");
//			log.message("getUserType=" + SSTR(cu.getUserType())+ "\r\n");
//		}
//
//		// Test BatchReport
//		using namespace za_co_vfi_reports;
//		BatchReport br;
//
//		//br.previousBatch();
//		br.currentBatch();
//
//
//	}
//}
