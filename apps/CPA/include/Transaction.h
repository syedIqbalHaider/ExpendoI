#ifndef _TRANSACTION_H_
#define _TRANSACTION_H_

#include <string>
#include <map>
#include <libda/ccardconfig.h>
#include <libda/cbatchrec.h>
#include <libda/cpaninfo.h>
#include <libhim.h>
#include <libpam.h>

#include "CPAver.h"
#include "TrackData.h"
#include "bertlv.h"

//using namespace std;
using std::iterator;
using std::string;
using std::set;
using std::map;

using namespace za_co_vfi_TrackData;
using namespace za_co_verifone_bertlv;
using namespace com_verifone_cardconfig;
using namespace com_verifone_batchrec;
using namespace za_co_vfi_Him;

namespace za_co_vfi_Transaction
{
// String ID's of parameters in txdata map
#define TX_DATA_TX_AMOUNT				"TxAmount"			// 6 byte BCD amount in cents
#define TX_DATA_TX_CURRENCY				"TxCurrency"
#define TX_DATA_TX_TYPE					"TxType"
#define TX_DATA_TX_DONT_REMOVE_CARD		"DontRemoveCard"
#define TX_DATA_TX_PRODUCT				"ProductData"
#define TX_DATA_TX_H_CMD_ENABLED		"HCommandEnabled"
#define TX_DATA_VALIDATE_PAN			"ValidatePan"

// Iso transaction codes
#define TX_ISO_SALE                0x00
#define TX_ISO_CASH_ADVANCE        0x01
#define TX_ISO_VOID                0x02
#define TX_ISO_SALE_CASH           0x09
#define TX_ISO_REFUND              0x20
#define TX_ISO_DEPOSIT             0x21
#define TX_ISO_CORRECTION          0x22
#define TX_ISO_BAL_ENQUIRY         0x31
#define TX_ISO_DCC_LOOKUP		   0x32
#define TX_ISO_ACC_STAT		       0x38
#define TX_ISO_TRANSFER			   0x40
#define TX_ISO_PMNT_DEPOSIT        0x51
#define TX_ISO_PIN_CHANGE          0x92
#define TX_ISO_LOAD_PRODUCT        0x91

typedef enum
{
	EMV_TX_SALE=0,
	EMV_TX_CASH=1,
	EMV_TX_SALE_CASH=9,
	EMV_TX_REFUND=0x20,

	// Below added for Transit
//	EMV_TX_PRE_DEPOSIT=0x30,
	EMV_TX_ONLINE_BAL_ENQ=0x31,
	EMV_TX_TRANS_DATA_ENQ=0x32,
	EMV_TX_OFFLINE_BAL_ENQ=0x33,
	EMV_TX_OFFLINE_MIN_STAT=0x34,
//	//EMV_TX_NOM_ACC_XFER=0x40,
//	//EMV_TX_CARD_TO_CARD_XFER=0x41,
//	//EMV_TX_FUNDS_XFER=0x42,
	EMV_TX_CASH_LOAD=0x21,
	EMV_TX_LOAD_TRANS_PROD=0x91,
	EMV_TX_PIN_CHANGE=0x92,
//	//EMV_TX_MINI_XFER=0x93,


}EMVTxType;

typedef enum
{
	DCM_INVALID,
	DCM_ONLINE,
	DCM_OFFLINE
}DraftCaptureMode;

typedef enum
{
	PEM_UNKNOWN=0,
	PEM_SWIPED,
	PEM_ENTERED,
	PEM_INSERTED,
	PEM_TAPPED,
	PEM_FALLBACK
}PanEntryMode;

typedef enum
{
	INTF_ICC=0x21,
	INTF_CTLS=0x22,
	INTF_MAG=0x23,
	INTF_BAR=0x24,
	INTF_MANUAL=0x25
}PmtEntryInterface;

typedef enum
{
	FIRST_PIN,
	CONFIRM_PIN,
	NORMAL_PIN
}PinType;

typedef enum
{
	TERMINAL,
	HOST,
	VOICE,
	DEF_ANALYSIS
}AuthMode;


class Transaction : public Him, public PAM
{
public:
	Transaction(int iso_code);

	virtual int DoTransaction(map<string, string>&txdata){(void)txdata; return -1;}

	int txComplete(bool waitForHCommandresponse);
	int txCompleteOffline(EMVTxType txType);
	int emvComplete(string status);
	int getPin(PinType type);
	int emvFallback(void);
	int manualEntry(void);
	int removeCardPrompt(void);
	bool ccdCheck(void);		// Should be called right after account selection, for mag cards

	BERTLV getEMVTagTxType(EMVTxType tx_type);
	BERTLV getEMVTagTxDate(void);
	BERTLV getEMVTagTxTime(void);
	BERTLV getEMVTagTxAmount(void);
	BERTLV getEMVTagTxCurreny(void);
	unsigned int getEMVTagCID(void);
	unsigned int getEMVCompleteTagCID(void);

	void printCardData(CCardData cdata);
	void printAccProf(CAccountProfile accountProfile);

	void TraceEMVTags(string tags);

	// PAM virtual function implementations
	int startTransactionResult(string sStatusCode, string sActiveInterface,
			string sEmvTlv, string sTrack1, string sTrack2, string sTrack3,
			string sSwipeStatus, string sManualPan, string sManualExpiryDate,
			string sManualCvv, string sBarcodeData);
	int modifyLimitsRequest(string sAid,string sPan,  string sEmvTlv);
	int completeTransactionResult(string sStatusCode,string sEmvTlv);
	int apduResult(string sStatusCode, string sApduResponse) {
		(void)sStatusCode; (void)sApduResponse; // get rid of warnings
		return (-1);
	}
	int readMediaResult(string sStatusCode, string sActiveInterface,
			string sAtr, string sTrack1, string sTrack2, string sTrack3,
			string sSwipeStatus, string sManualPan, string sManualExpiryDate,
			string sManualCvv, string sBarcodeData);
	int removeCardResult(string sStatusCode);
	int setGetTlvResult(string sStatusCode, string sEmvTlv) {
		(void)sStatusCode; (void)sEmvTlv; // get rid of warnings
		return (-1);
	}
	int transactionStatusResult(string sStatusCode);
	int cancelTransactionResult(string sStatusCode);

	bool isMagEnabled(void);
	bool isManualEntryEnabled(void);
	string getAmount(const string sVal);
	string getTxCurrency(void);

	void initBatchRec(void);

	unsigned int translateISORespCode(unsigned int recv_resp);

protected:
	string start_timestamp;
	string tx_sale_amount;								// 6 byte BCD amount in cents
	string tx_cash_amount;								// 6 byte BCD amount in cents
	string auth_code;
	int batchno;
	int rcptno;
	int ecrno;	//iqbal_audi_301216
	int tsn;
	int iso_tx_code;
	int acc_type;
	int budget_period;									// Number of months
	int account_profile_index;
	bool tx_complete;
	int pam_status;
	bool dont_remove_card;
	bool wait_for_h_command_response;

	CCardData cardData;									// Only used for mag transactions
	CAccountProfile accprof;
	CEmvData emvcfg;
	string emvtlv;
	string emv_complete_tlv;
	string logEntry;

	string pinblock;
	string ksn;
	string pan;
	string reason;
	string service_restriction_code;
	TrackData track_data;
	IccValidation iccValidation;
	CBatchRec batchrec;

	int response_code;
	int tx_reason_code;

	DraftCaptureMode dcm;
	bool completion_code;
	CAPTURE_FLAG capture_flag;

	bool emv_fallback_required;
	bool emv_fallback;
	bool manual_entry_required;
	bool above_floor_limit;
	bool will_go_online;
	bool went_online;
	bool signature_required;
	bool velocity_count_exceeded;
	bool velocity_period_exceeded;
	bool card_approved_offline;

	// Service code flags
	bool check_src;
	bool icc_card;
	bool atm_card;
	bool foreign_card;
	bool force_online;
	bool pin_required;
	bool exclude_cash;
	bool global_approved_flag;
	bool global_declined_flag;

	PanEntryMode entry_mode;

	bool validate_pan;
	bool pan_validation_failed;
	string required_pan;

	com_verifone_paninfo::CPanInfo paninfo;				// Used for keeping track of card usage velocity
	AuthMode auth_mode;
	string txn_currency;

	bool cancel_transaction;	// This is mainly used to inform get_pin_entry to cancel
private:
	void checkSrc(void);
	bool checkOnline(void);
	void draftCapture(void);
	void writeBatch(void);
	void readBatch(void);
	int printReceipt(void);
	int checkVelocity(void);
	int updateVelocity(void);

	void Feedback(string message);

	void traceTags(string tags);

};

class Sale : public Transaction
{
public:
	Sale():Transaction(TX_ISO_SALE){}
	~Sale();
	int DoTransaction(map<string, string>&txdata);

protected:
	int account_type;
	string auth_timestamp;

private:
	bool isServiceAllowed(void);
	int doBudget(void);
	int checkLimits(void);

	int import_txdata(map<string, string>&txdata);
};

class TransitCmd: public Transaction {
public:
	TransitCmd() :
			Transaction(TX_ISO_DEPOSIT) {
	}
	TransitCmd(int iso_code) :
			Transaction(iso_code) {
	}
	~TransitCmd();
	virtual int DoTransaction(map<string, string>&txdata, EMVTxType txType);
	//{ (void)txdata;(void)txType; return 0;};

protected:
	string emv_apdu_result;
	string emv_apdu_status;
	string emv_tag_data;
	string tag_9f_50;

	int import_txdata(map<string, string>&txdata);
	int emvSendApduCB(void);
	int emvSendApdu9F50(void);
	int processApduData(std::string sTagExpected);
	int apduResult(std::string sStatusCode,std::string sApduResponse);

private:
};

class OfflineBal: public TransitCmd {
public:
	OfflineBal() :
		TransitCmd(TX_ISO_BAL_ENQUIRY) {
	}
	~OfflineBal();
	int DoTransaction(map<string, string>&txdata, EMVTxType txType);

protected:
	string sBalance;

private:

};

class OfflineMini: public TransitCmd {
public:
	OfflineMini() :
		TransitCmd(TX_ISO_BAL_ENQUIRY) {
	}
	~OfflineMini();
	int DoTransaction(map<string, string>&txdata, EMVTxType txType);

protected:
	typedef enum
	{
		APD_NONE=0,
		APD_GOT_FMT,
		APD_GOT_REC,
		APD_DONE
	} ApduMode;

	string dataDesc;
	char maxRecs;
	char recNr;
	char sfi;
	ApduMode apdMode;
	map<string,string> statementEntries;

private:
	int emvSendOfflineMiniStat(void);
	int emvSendOfflineMiniStatRecNr(char nr, char sfi);
	int addMiniStatementLine(int nr, string dDesc, string sNextApdu); // nr is 1-based

};

class TxDataEnq: public TransitCmd {
public:
	TxDataEnq() :
		TransitCmd(TX_ISO_BAL_ENQUIRY) {
	}
	~TxDataEnq();
	int DoTransaction(map<string, string>&txdata, EMVTxType txType);

protected:
	typedef enum
	{
		APD_NONE=0,
		APD_TAG_9F50,
//		APD_TAG_CB,
		APD_TAG_70,
		APD_TAG_71,
		APD_TAG_72,
//		APD_TAG_7B,
		APD_TAG_75,
		APD_TAG_76,
		APD_TAG_77,
		APD_TAG_78,
		APD_TAG_C0,
		APD_DONE
	} ApduMode;

	ApduMode apdMode;

private:
	int emvSendApduCommand(ApduMode apdTag);

};


}
#endif // _TRANSACTION_H_
