/*
 * security.h
 *
 *  Created on: Aug 8, 2017
 *      Author: vfisdk
 */

#ifndef APPS_CPA_INCLUDE_SECURITY_H_
#define APPS_CPA_INCLUDE_SECURITY_H_

#include <libseccmd.h>

// host ids
#define HID01       1
#define HID02       2
#define HID03       3
#define HID04       4
#define HID05       5

#define MAC_MS 1
#define MAC_DUKPT 2
// KeySetIDs
#define KEYSET00    0
#define KEYSET01    1
#define KEYSET02    2

#define BUFF_SIZE 1024
#define TIMEOUT 5000

#define ENC        1
#define DEC        2

#define VSS_SUBDEV  0x00

using namespace com_verifone_seccmd;

int ippPinEncrypt(unsigned char *WorkingKey_GISKE, std::string pan, std::string amount, uint8_t *pinBlock);
void vdGetVersions();
int inUpdateAdditionalKeys(unsigned char ucHostId, unsigned int ucKeySetId, unsigned char inKeyType, unsigned char * pucKey, int inKeySize);
int inIPPEnterAndEncryptPIN(int inHostId, Ksn_v2* stKSN, std::string PANfield, std::string AmountText, uint8_t * pinBlock);


#endif /* APPS_CPA_INCLUDE_SECURITY_H_ */
