#ifndef _OPERATOR_H_
#define _OPERATOR_H_

#include <libda/ccarddata.h>
#include "cstatus.h"

namespace za_co_vfi_Operator
{
	class Operator
	{
		public:
			Operator();
			int accountSelection(std::map<std::string, com_verifone_accountprofile::CAccountProfile>&accounts);
			int budgetPeriodSelection(void);
			void waitDisp(void);
			void clearReversalDisp(void);
			void approvedDisp(void);
			void pinChangedDisp(void);
			void declinedDisp(int response_code);
			void declinedRespDisp(string response_msg);
			void bankingFailed(void);
			void batchFull(void);
			void batchEmpty(void);
			void BatchNoxTx(void);
			int getCCD(string &ccd);
			void balanceDisp(string sBalance);
			void miniStatDisp(map<string,string> &statementEntries);
			int reprintReceipt(int receiptNo);		// receiptNo == 0 prints the last receipt
			//int menuAction(int selValue, std::string menuId);
			std::string doOperatorMenu(std::string command);
			int  doSettlement(void);
			int  doCpaSetup(void);
			int doParameterDownload(com_verifone_status::CStatus &status, bool isFull, bool mustPrintMessage);

			static std::string displayErrorMsg(string id);

		private:
			int do_ux_setup(void);
			int do_mx_setup(void);
	};


	std::string MsgRespCode(int respCode);

}

#endif // _OPERATOR_H_
