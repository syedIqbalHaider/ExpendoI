#ifndef _PRN_H_
#define _PRN_H_

#include <string>

//using namespace std;
using std::string;

namespace za_co_vfi_prn
{
class Prn
{
public:
	Prn();
	~Prn();

	bool status();
	int justify_centre(bool mode);
	int justify_right(bool mode);
	int initialise(void);
	int underline(bool mode);
	int bold(bool mode);
	int inverse(bool mode);
	int cut(void);
	int upsidedown(bool mode);
	int print(string msg);
	int feed(int no);
	int getWidth(void);

private:
	int fd;
};

}

#endif // _PRN_H_
