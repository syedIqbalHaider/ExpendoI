#ifndef _MENUBMP_H_
#define _MENUBMP_H_

#include <string>

//using namespace std;
using std::string;
using std::istringstream;
using std::hex;

namespace za_co_vfi_MenuBmp
{

class MenuBmp
{
public:
	enum MENU_TYPE{
		MENU_BMP_RETAIL=1,
		MENU_BMP_FORECOURT,
		MENU_BMP_WORKSHOP,
		MENU_BMP_RESTAURANT
	};

	enum MENU_TXTYPE{
		MENU_TX_Purchase=1,
		MENU_TX_Refund,
		MENU_TX_ChangePin,
		MENU_TX_Correction,
		MENU_TX_BalEnq,
		MENU_TX_Payment,
		MENU_TX_Cash,
		MENU_TX_SaleCash,
		MENU_TX_CardAct,
		MENU_TX_FundsLoad,
		MENU_TX_MenuFundsUnload,
		MENU_TX_MPSatement,
		MENU_TX_MPBalEnq,
		MENU_TX_PreAuth,
		MENU_TX_PreAuthCompl,
		MENU_TX_PreAuthCancel,
		MENU_TX_VoiceOverride,
		MENU_TX_QuasiCash,
		MENU_TX_CardIssue,
		MENU_TX_CardReload,
		MENU_TX_GiftBalEnq,
		MENU_TX_OrderRes,
		MENU_TX_OrderFulF,
		MENU_TX_Redeem
	};

	MenuBmp(string mbmp):bmpstr(mbmp){}

	MENU_TYPE getMenuType();

	bool SalesPurchases();
	bool ReturnsRefunds();
	bool ChangePin();
	bool CorrectionsReverse();
	bool BalanceEnquiry();
	bool DepositPayment();
	bool CashWithdrawal();
	bool SalesPurchasesWithCashback();
	bool CardActivation();
	bool MpadFundsLoad();
	bool MpadFundsUnload();
	bool MpadStatement();
	bool MpadBalanceEnquiry();
	bool PreAuth();
	bool PreAuthCompletion();
	bool PreAuthCancel();
	bool VoiceOverride();
	bool QuasiCashWithdrawal();
	bool CardIssue();
	bool CardReload();
	bool GiftCardBalanceEnquiry();
	bool OrderReserve();
	bool OrderFulfill();
	bool Redemption();
	int BitCount();

private:
	string bmpstr;

	bool is_set(unsigned int bitno);
};

}

#endif // _MENUBMP_H_
