#ifndef _TRACKDATA_H_
#define _TRACKDATA_H_

#include <string>
#include <iostream>

//using namespace std;
using std::string;
using std::cout;
using std::endl;


namespace za_co_vfi_TrackData{

class TrackData
{
public:
	TrackData(string sTrack1, string sTrack2, string sTrack3):track1(sTrack1),track2(sTrack2),track3(sTrack3),manual(false){}
	TrackData(string sTrack2):track2(sTrack2),manual(false){}
	TrackData(string pan, string expiry, string cvv, string name):manpan(pan),manexpiry(expiry),mancvv(cvv),manname(name),manual(true){}
	TrackData():manual(true){}

	string pan(void);
	string expiry(void);
	string cardholder(void);
	string src(void);
	string getTrack1(void);
	string getTrack2(void);
	string getTrack3(void);
	string CVV(void);
	bool isExpired(void);
	bool isManualEntered(void);
	int isLuhnVarified( const char *id); //iqbal_audi_100117

private:
	string track1;
	string track2;
	string track3;

	string manpan;
	string manexpiry;
	string mancvv;
	string manname;
	bool manual;

	int toInt(const char c);
};


class IccValidation 		//iqbal_audi_100117
{
public:
	IccValidation(){}

	bool isExpired(string expiryDate);
	int isLuhnVarified(const char *id);

private:
	int toInt(const char c);

};

}

#endif // _TRACKDATA_H_
