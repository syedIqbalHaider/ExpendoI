#ifndef _CPATAGS_H_
#define _CPATAGS_H_

namespace CPATags
{
	// Internal Tag values
	#define CPA_TAG					"\xFF\xA1"

	// CPA commands
	#define CMD_SIZE						3
	#define CPA_CMD_TX_RQST					CPA_TAG"\x01"
	#define CPA_CMD_SETTLE_RQST				CPA_TAG"\x02"
	#define CPA_CMD_PURCHASE_RQST			CPA_TAG"\x03"
	#define CPA_CMD_POS_TX_RQST				CPA_TAG"\x04"
	#define CPA_CMD_POS_STATUS_RQST			CPA_TAG"\x05"
	#define CPA_CMD_POS_CANCEL_RQST			CPA_TAG"\x06"
	#define CPA_CMD_POS_BANKING_RQST		CPA_TAG"\x07"
	#define CPA_CMD_POS_PARAMETER_RQST		CPA_TAG"\x08"
	#define CPA_CMD_POS_REVERSAL_RQST		CPA_TAG"\x09"
	#define CPA_CMD_REP_CURR_BATCH			CPA_TAG"\x0a"
	#define CPA_CMD_REP_PREV_BATCH			CPA_TAG"\x0b"
	#define CPA_CMD_POS_REMOVE_CARD			CPA_TAG"\x0c"
	#define CPA_CMD_POS_RETURN_IDLE			CPA_TAG"\x0d"
//iq_audi_02022017
	#define CPA_CMD_POS_CLEAR_REVERSAL		CPA_TAG"\x20"
	#define CPA_CMD_POS_VOID_RQST			CPA_TAG"\x21"

	// CPA command parameter tags
	#define CMD_PARAM_TX_AMOUNT					"\x9f\x02"		// 6 byte BCD amount in cents
	#define CMD_PARAM_TX_TYPE					"\x9c"			// Transaction type
	#define CMD_PARAM_PAN						"\x5a"

	#define CMD_PARAM_ECR_NUMBER						"\x6a" //iq_audi_10052017 for proprietray use
	#define CMD_PARAM_TX_CURRENCY						"\x6b" //iq_audi_23082017 for proprietray use

	// CPA Flags
	#define CMD_PARAM_TX_DONT_REMOVE_CARD		CPA_TAG"\x10"    // Dont Remove Card
	#define CMD_PARAM_TX_DATA_TX_PRODUCT		CPA_TAG"\x11"
	#define CMD_PARAM_TX_H_COMMAND_ENABLED		CPA_TAG"\x12"
}

#endif // _CPATAGS_H_
