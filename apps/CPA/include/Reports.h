#ifndef _REPORTS_H_
#define _REPORTS_H_

//#include <string>
//#include <map>
#include <libda/cbatch.h>
#include <libda/cbatchrec.h>
#include <libpam.h>

#include "CPAver.h"
#include "TrackData.h"

//using namespace std;
using namespace com_verifone_batch;

namespace za_co_vfi_reports
{

class Receipt
{
public:
	Receipt(CBatchRec &batch);
	int merchantCopy(bool reprint);
	int customerCopy(bool reprint);
	int declined(void);
	int getTransactionCount(void);

private:
	std::map<std::string, std::string>rcpt_values;
	std::map<std::string, std::string>values;
	com_verifone_batchrec::CBatchRec batchrec;
	int iso_tx_code;
	bool signature_required;

	string datestr;
	string timestr;

	//void addIfNonEmpty(map<string, string> &values, string key, string val);
	int format(void);
};

class BatchReport
{
public:
	BatchReport();
	int currentBatch(void);
	int previousBatch(void);

private:

	//com_verifone_batchrec::CBatchRec batchrec;

	int doBatch(bool isCurBatch);
	int printHeaders(CBatch &batch, size_t sz);
	int printBatchHeader(int cnt, std::string headerPhrase);
	void printSuperTotals(std::map<unsigned int, CBatchRec>  &batchRecs);
	int printFooter(bool isCurBatch);
	int printTx(CBatchRec &batchRec);
	//void addIfNonEmpty(map<string, string> &values, string key, string val);
};

class SetupReport
{
public:
	SetupReport(){};

	int ConfigReport(bool sigPanel);
	int MessageToMerchantReport(void);

private:
//	int printHeaders(void);
};

class EmvDataLogReport
{
public:
	EmvDataLogReport(){};

	int doReport(void);

private:
//	int printHeaders(void);
};


class CardParameterReport: public PAM
{
public:
	CardParameterReport():PAM(CPA_NAME){};

	int CardParamReport(void);

private:
	za_co_vfi_TrackData::TrackData track_data;
	int pam_status;
	bool bad_swipe;

	int readMediaResult(string sStatusCode, string sActiveInterface,
				string sAtr, string sTrack1, string sTrack2, string sTrack3,
				string sSwipeStatus, string sManualPan, string sManualExpiryDate,
				string sManualCvv, string sBarcodeData);
};

}

#endif
