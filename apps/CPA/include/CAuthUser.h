#ifndef _CAUTHUSER_H_
#define _CAUTHUSER_H_

#include <libda/cuser.h>


namespace com_verifone_user
{
	class CAuthUser
	{
		public:
			CAuthUser();
			int authenticate(); // show auth screen and get auth details
			bool isAuthenticated();
			//virtual void editUser();  // ask for and add/edit user

			bool isBankingAllowed() ;
			std::string& getDateAdded() ;
			std::string& getId() ;
			std::string& getName() ;
			std::string& getPin() ;
			bool isPinRequired() ;
			bool isRefundsAllowed() ;
			CUser::USER_TYPE getUserType() ;

		private:
			CUser mUser;
			bool mIsAuthenticated;
	};

}

#endif // _CAUTHUSER_H_
