#ifndef _TX_RESULT_H_
#define _TX_RESULT_H_

#include <string>

namespace za_co_verifone_txResult
{

#define TXRES_COMPLETE				"IsComplete"
#define TXRES_ONLINE				"WentOnline"
#define TXRES_BATCHNO				"BatchNo"
#define TXRES_TSN					"Tsn"
#define TXRES_RCPTNO				"ReceiptNo"
#define TXRES_RESPCODE				"ResponseCode"
#define TXRES_AUTHCODE				"AuthorisationCode"
#define TXRES_SALE_AMNT				"SaleAmount"
#define TXRES_CASH_AMNT				"CashAmount"
#define TXRES_PAN					"Pan"
#define TXRES_TIMESTAMP				"TimeStamp"
#define TXRES_ISO_CODE				"IsoCode"
#define TXRES_RRN					"Rrn"
#define TXRES_REASON_CODE			"ReasonCode"
#define TXRES_EMV_DATA  			"EmvData"
#define TXRES_CARD_EXPIRY  	 		"CardExpiryDate"
#define TXRES_CARD_FEES      		"CardFees"
#define TXRES_TRANSACTION_FEES      "TransactionFees"
#define TXRES_APPROVED				"Approved"
#define TXRES_SRC					"ServiceRestrictionCode"
#define TXRES_INPUT_TYPE			"InputType"

#define TXRES_CARD_TYPE				"CardType"
#define TXRES_ACC_TYPE				"AccType"
#define TXRES_SUPERVISOR			"Supervisor"
#define TXRES_OPERATOR				"Operator"
#define TXRES_MERCHANT_NAME			"MerchantName"
//iq_audi_301216
#define TXRES_ECR_NUMBER			"EcrNumber"
#define TXRES_RESP_MSG				"RespMessage"
#define TXRES_IS_VOIDED				"IsTrxVoided"
#define TXRES_CURREYNCY				"TxCurrency"

//using namespace std;

class TxResult
{
public:
	TxResult(void);
	TxResult(CBatchRec batchrec); //for void iq_audi_12052017

	// Setters

	bool IsComplete(void);
	bool WentOnline(void);
	int BatchNo(void);
	int Tsn(void);
	int RcptNo(void);
	string ResponseCode(void);
	string AuthCode(void);
	string SaleAmount(void);
	string CashAmount(void);
	string Pan(void);
	string TimeStamp(void);
	string ISOCode(void);
	string Rrn(void);
	string CardType(void);
	string AccType(void);
	string Supervisor(void);
	string Cashier(void);
	string MerchantName(void);

	map<string,string>txdata;

private:
	bool is_complete;
	bool went_online;

	int batchno;
	int tsn;
	int rcptno;
	int ecrno;	//iq_audi_301216

	string auth_code;
	string response_code;
	string tx_sale_amount;
	string tx_cash_amount;
	string tx_currency;
	string pan;
	string start_timestamp;
	string iso_tx_code;
	string rrn;
	string reason_code;
	string emv_data;
	string cardExpiryDate;
	string cardFees;
	string transactionFees;

	string card_type;
	string acc_type;
	string supervisor_id;
	string operator_id;
	string merchant_name;
};

}

#endif // _TX_RESULT_H_
