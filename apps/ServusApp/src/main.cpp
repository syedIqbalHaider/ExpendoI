#include <svc.h>
#include <svc_net.h>
#include <ceif.h>
#include <ceifConst.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <libpml/pml.h>
#include <libpml/pml_abstracted_api.h>
#include <libpml/pml_port.h>
#include <libipc/ipc.h>
#include <libminini/minini.h>
#include <liblog/logsys.h>
#include <bertlv.h>

#define VERSION_MAJOR       3
#define VERSION_MINOR       0
#define VERSION_REVISION    0

#define STX 0x02
#define ETX 0x03
#define ESC 0x1b
#define ACK 0x06
#define NAK 0x15

#define MY_TASK_NAME "SERVUS"
#define CPA_SERVICE_TASK_NAME "CPAM"
#define SECURITY_SERVICE_TASK_NAME "SCAPP"
#define GUI_SERVICE_TASK_NAME "GUIBRIDGEAPP"

//using namespace std;
using namespace za_co_verifone_bertlv;

typedef enum {
	NORMAL,
	FBI_CANCEL,
	FBI_REMOVE_CARD,
	FBS_CANCEL
} STATE;

extern unsigned char vmcMDAppId;

static int ghCom;
static int giSocket;
static struct sockaddr_in tAddress;
static string transactionPacket;
static int giFallbackInserts;
static int giFallbackSwipes;
static STATE gState;

int sendPacket(const string &inPacket);

int processCpaServicePacket(string &inPacket)
{
	BERTLV inTlv(inPacket);
	string packetType,statusCode;
	
	inTlv.getValForTag(string("\xDF\xAF\x01"),packetType);
	inTlv.getValForTag(string("\xDF\xAF\x04"),statusCode);
	
	if(gState==FBI_CANCEL && packetType.length()==1 && packetType.at(0)==0xf2) {
		// show remove card
		gState=FBI_REMOVE_CARD;
		string rcCmd("\xDF\xAF\x01\x01\x25\xDF\xAF\x02\x01\xFF");
		return(com_verifone_ipc::send(rcCmd,string(CPA_SERVICE_TASK_NAME))?0:1);
	}
	
	if(gState==FBI_REMOVE_CARD && packetType.length()==1 && packetType.at(0)==0xa5) {
		// show reinsert card
		gState=NORMAL;
		return(com_verifone_ipc::send(transactionPacket,string(CPA_SERVICE_TASK_NAME))?0:1);
	}
	
	if(gState==FBS_CANCEL && packetType.length()==1 && packetType.at(0)==0xf2) {
		// show reswipe
		gState=NORMAL;
		return(com_verifone_ipc::send(transactionPacket,string(CPA_SERVICE_TASK_NAME))?0:1);
	}
	
	if(packetType.length()==1 && packetType.at(0)==0xa1) {
		if(statusCode.length()==1) {
			if(statusCode.at(0)==0x09) {	// fallback to mag
				if(giFallbackInserts>1) {
					giFallbackInserts--;
					// show error
					gState=FBI_CANCEL;
					string cancelCmd("\xDF\xAF\x01\x01\x72");
					return(com_verifone_ipc::send(cancelCmd,string(CPA_SERVICE_TASK_NAME))?0:1);
				} else {
					if(giFallbackInserts==1) giFallbackInserts=0;
					BERTLV txnTlv(transactionPacket);
					txnTlv.updateTag(string("\xDF\xAF\x03"),string("\x23"));
					transactionPacket=txnTlv.getStringData();
					// show fallback to mag message
					gState=FBI_CANCEL;
					string cancelCmd("\xDF\xAF\x01\x01\x72");
					return(com_verifone_ipc::send(cancelCmd,string(CPA_SERVICE_TASK_NAME))?0:1);
				}
			}
			if(statusCode.at(0)==0x0a) {	// fallback to manual
				if(giFallbackSwipes>1) {
					giFallbackSwipes--;
					// show error
					gState=FBS_CANCEL;
					string cancelCmd("\xDF\xAF\x01\x01\x72");
					return(com_verifone_ipc::send(cancelCmd,string(CPA_SERVICE_TASK_NAME))?0:1);
				} else {
					BERTLV txnTlv(transactionPacket);
					txnTlv.updateTag(string("\xDF\xAF\x03"),string("\x25"));
					transactionPacket=txnTlv.getStringData();
					// cancel fallback to manual message
					gState=FBS_CANCEL;
					string cancelCmd("\xDF\xAF\x01\x01\x72");
					return(com_verifone_ipc::send(cancelCmd,string(CPA_SERVICE_TASK_NAME))?0:1);
				}
			}
			if(statusCode.at(0)==0x00) {	// check for swiped icc card
				string dfcc42;
				if(inTlv.getValForTag(string("\xDF\xCC\x42"),dfcc42) && dfcc42.length()>0 && giFallbackInserts>0) {
					for(int iIndex=0;iIndex<dfcc42.length();iIndex++) {
						if(dfcc42.at(iIndex)=='=') {
							iIndex+=5;
							if(iIndex<dfcc42.length() && (dfcc42.at(iIndex)=='2' || dfcc42.at(iIndex)=='6')) {
								return(com_verifone_ipc::send(transactionPacket,string(CPA_SERVICE_TASK_NAME))?0:1);
							}
						}
					}
				}
			}
		}
	}
	
	return(sendPacket(inPacket));
}

int processSecurityServicePacket(string &inPacket)
{
	if(inPacket.length()==0) return(0);
	
	char cCmd=inPacket.at(0);
	BERTLV inTlv(inPacket.substr(1));
	
	if(cCmd==24) {
		string df30,df6c,df8103;
		inTlv.getValForTag(string("\xDF\x30"),df30);
		inTlv.getValForTag(string("\xDF\x6C"),df6c);
		inTlv.getValForTag(string("\xDF\x81\x03"),df8103);
				
		BERTLV resTlv;
		resTlv.addTag(string("\xDF\xAF\x01"),string("\xB4"));
		resTlv.addTag(string("\xDF\xAF\x04"),string("\xFF"));
		if(df30.length()==1) {
			switch(df30.at(0)) {
				case 0:		// ok
					resTlv.updateTag(string("\xDF\xAF\x04"),string("\x00",1));
					if(df6c.length()>0) resTlv.addTag(string("\xDF\x6C"),df6c);
					if(df8103.length()>0) resTlv.addTag(string("\xDF\x81\x03"),df8103);
					break;
				case 2:		// timeout
					resTlv.updateTag(string("\xDF\xAF\x04"),string("\x01"));
					break;
				case 20:	// cancel pressed
					resTlv.updateTag(string("\xDF\xAF\x04"),string("\x04"));
					break;
				case 26:	// sequence error
					resTlv.updateTag(string("\xDF\xAF\x04"),string("\x10"));
					break;
				case 93:	// verify pin failed
					resTlv.updateTag(string("\xDF\xAF\x04"),string("\x17"));
					break;
				default:
					return(sendPacket(string("\xDF\xAF\x01\x01\xB4\xDF\xAF\x04\x01\xFF")));
			}
		}
		return(sendPacket(resTlv.getStringData()));
	}
	if(cCmd==43) {
		string df30;
		inTlv.getValForTag(string("\xDF\x30"),df30);
		
		BERTLV resTlv;
		resTlv.addTag(string("\xDF\xAF\x01"),string("\xB5"));
		resTlv.addTag(string("\xDF\xAF\x04"),string("\xFF"));
		if(df30.length()==1) {
			switch(df30.at(0)) {
				case 0:		// ok
					resTlv.updateTag(string("\xDF\xAF\x04"),string("\x00",1));
					break;
				case 2:		// timeout
					resTlv.updateTag(string("\xDF\xAF\x04"),string("\x01"));
					break;
				default:
					return(sendPacket(string("\xDF\xAF\x01\x01\xB5\xDF\xAF\x04\x01\xFF")));
			}
		}
		return(sendPacket(resTlv.getStringData()));
	}

	return(0);
}

int processGuiServicePacket(string &inPacket)
{
	BERTLV inTlv(inPacket);

	string dfaf01;
	inTlv.getValForTag(string("\xDF\xAF\x01"),dfaf01);
	
	if(dfaf01.length()==1 && dfaf01.at(0)==0xf2) return(1);
	
	return(sendPacket(inPacket));
}

int processTcpEvent(int iSocket,string &inPacket)
{
	char cData;
	static int iState=0;
	static int iEsc=0;
	static string comPacket;
	
	while(recv(iSocket,&cData,1,MSG_DONTWAIT)==1) {
		if(cData==STX) iState=0;
		switch(iState) {
			case 0:	// wait stx
				if(cData==STX) {
					iEsc=0;
					comPacket.clear();
					iState=1;
				}
				break;
			case 1: // data
				if(cData==ETX) {
					iState=0;
					inPacket=comPacket;
					return(comPacket.length());
				}
				if(cData==ESC) {
					iEsc=1;
					break;
				}
				if(iEsc==1) {
					iEsc=0;
					cData-=0x20;
				}
				comPacket.append(&cData,1);
				break;
		}
	}
	
	return(0);
}

int processComEvent(int hCom,string &inPacket)
{
	char cData;
	static char cLrc=0;
	static int iState=0;
	static int iEsc=0;
	static string comPacket;
	
	while(read(hCom,&cData,1)==1) {
		if(cData==STX) iState=0;
		switch(iState) {
			case 0:	// wait stx
				if(cData==STX) {
					iEsc=0;
					cLrc=0;
					comPacket.clear();
					iState=1;
				}
				break;
			case 1: // data
				if(cData==ETX) {
					iState=2;
					break;
				}
				if(cData==ESC) {
					iEsc=1;
					break;
				}
				if(iEsc==1) {
					iEsc=0;
					cData-=0x20;
				}
				cLrc^=cData;
				comPacket.append(&cData,1);
				break;
			case 2:	// lrc
				if(cData==ESC) {
					iEsc=1;
					break;
				}
				if(iEsc==1) {
					iEsc=0;
					cData-=0x20;
				}
				if(cData==cLrc) {
					cData=ACK;
					write(hCom,&cData,1);
					iState=0;
					inPacket=comPacket;
					return(comPacket.length());
				} else {
					cData=NAK;
					write(hCom,&cData,1);
					iState=0;
				}
				break;
		}
	}
	
	return(0);
}

int sendComPacket(int hCom,const string &inPacket)
{
	char cLrc;
	char cData;
	int iIndex;
	int iRc;
	unsigned long ulTimeout;
	string packet;
	
	if(inPacket.length()==0) return(0);
	
	packet.clear();
	packet.append("\x02");
	
	cLrc=0;
	for(iIndex=0;iIndex<inPacket.length();iIndex++) {
		cData=inPacket.at(iIndex);
		cLrc^=cData;
		if(cData==STX || cData==ETX || cData==ESC) {
			packet.append("\x1b");
			cData+=0x20;
		}
		packet.append(&cData,1);
	}
	
	packet.append("\x03");
	if(cLrc==STX || cLrc==ETX || cLrc==ESC) {
		packet.append("\x1b");
		cLrc+=0x20;
	}
	packet.append(&cLrc,1);
	
	iIndex=3;
	while(iIndex) {
		iRc=write(hCom,packet.data(),packet.length());
		if(iRc!=packet.length()) return(0);
		ulTimeout=read_ticks()+2000;
		while(read_ticks()<=ulTimeout) {
			if(read(hCom,&cData,1)==1) {
				if(cData==ACK) return(inPacket.length());
				if(cData==NAK) break;
			}
			SVC_WAIT(0);
		}
		iIndex--;
	}
	
	return(0);
}

int sendTcpPacket(int iSocket,const string &inPacket)
{
	char cData;
	int iRc;
	int iIndex;
	string packet;

	if(inPacket.length()==0) return(0);

	packet.clear();
	packet.append("\x02");

	for(iIndex=0;iIndex<inPacket.length();iIndex++) {
		cData=inPacket.at(iIndex);
		if(cData==STX || cData==ETX || cData==ESC) {
			packet.append("\x1b");
			cData+=0x20;
		}
		packet.append(&cData,1);
	}
	
	packet.append("\x03");

	iRc=send(iSocket,packet.data(),packet.length(),MSG_DONTWAIT);

	return(iRc);
}

int sendPacket(const string &inPacket)
{

	if(giSocket>=0)
		return(sendTcpPacket(giSocket,inPacket));

	if(ghCom>=0)
		return(sendComPacket(ghCom,inPacket));
	
	return(0);
}

int capturePin(BERTLV &inTlv)
{
	string timeout,keyScheme,pan,stan,min,max,pinmsg,amtmsg,verifymsg,verify;
	
	inTlv.getValForTag(string("\xDF\xAF\x02"),timeout);
	inTlv.getValForTag(string("\xDF\x23"),keyScheme);
	inTlv.getValForTag(string("\xDF\x36"),pan);
	inTlv.getValForTag(string("\xDF\x7C"),stan);
	inTlv.getValForTag(string("\xDF\x81\x04"),min);
	inTlv.getValForTag(string("\xDF\x81\x05"),max);
	inTlv.getValForTag(string("\xDF\x81\x1A"),pinmsg);
	inTlv.getValForTag(string("\xDF\x81\x1B"),amtmsg);
	inTlv.getValForTag(string("\xDF\x81\x13"),verifymsg);
	inTlv.getValForTag(string("\xDF\xAF\x2F"),verify);
	
	if(keyScheme.length()==0) return(sendPacket(string("\xDF\xAF\x01\x01\xB4\xDF\xAF\x04\x01\x11",10)));
	
	BERTLV eepTlv;
	eepTlv.addTag(string("\xDF\x1F"),string("\x00",1));
	if(timeout.length()==1) {
		eepTlv.addTag(string("\xDF\x1F"),timeout);
		timeout=string("\x00",1)+timeout;
		eepTlv.addTag(string("\xDF\x81\x06"),timeout);
	}
	eepTlv.addTag(string("\xDF\x23"),keyScheme);
	eepTlv.addTag(string("\xDF\x81\x10"),string("\x00",1));
	if(pan.length()>0) eepTlv.addTag(string("\xDF\x36"),pan);
	if(stan.length()>0) eepTlv.addTag(string("\xDF\x7C"),stan);
	if(min.length()>0) eepTlv.addTag(string("\xDF\x81\x04"),min);
	if(max.length()>0) eepTlv.addTag(string("\xDF\x81\x05"),max);
	if(pinmsg.length()>0) eepTlv.addTag(string("\xDF\x81\x1A"),pinmsg);
	if(amtmsg.length()>0) eepTlv.addTag(string("\xDF\x81\x1B"),amtmsg);
	if(verifymsg.length()>0) eepTlv.addTag(string("\xDF\x81\x13"),verifymsg);
	if(verify.length()==1 && verify.at(0)==0x01) eepTlv.addTag(string("\xDF\x81\x0D"),string("\x08"));
	eepTlv.addTag(string("\xDF\x05"),string("\x00",1));
	string eepCmd("\x18");
	eepCmd.append(eepTlv.getStringData());
	return(com_verifone_ipc::send(eepCmd,string(SECURITY_SERVICE_TASK_NAME))?0:1);
}

int updateSessionKey(BERTLV &inTlv)
{
	string keyScheme,keyType,keyBlock;
	
	inTlv.getValForTag(string("\xDF\x23"),keyScheme);
	inTlv.getValForTag(string("\xDF\x46"),keyType);
	inTlv.getValForTag(string("\xDF\x2E"),keyBlock);
	
	if(keyScheme.length()==0 || keyType.length()==0 || keyBlock.length()==0) return(sendPacket(string("\xDF\xAF\x01\x01\xB5\xDF\xAF\x04\x01\x11",10)));
	
	BERTLV uskTlv;
	uskTlv.addTag(string("\xDF\x1F"),string("\x00",1));
	uskTlv.addTag(string("\xDF\x23"),keyScheme);
	uskTlv.addTag(string("\xDF\x46"),keyType);
	uskTlv.addTag(string("\xDF\x2E"),keyBlock);
	string uskCmd("\x2B");
	uskCmd.append(uskTlv.getStringData());
	return(com_verifone_ipc::send(uskCmd,string(SECURITY_SERVICE_TASK_NAME))?0:1);
}

int cancelTransaction(string &inPacket)
{
	int iRc;
	BERTLV secTlv;
	
	gState=NORMAL;
	
	secTlv.addTag(string("\xDF\x1F"),string("\x00",1));
	secTlv.addTag(string("\xDF\x2A"),string("\x80"));
	string secCmd("\x24");
	secCmd.append(secTlv.getStringData());

	iRc=com_verifone_ipc::send(secCmd,string(SECURITY_SERVICE_TASK_NAME))?0:1;
	
	if(iRc) {
		iRc=com_verifone_ipc::send(inPacket,string(CPA_SERVICE_TASK_NAME))?0:1;
	} else {
		com_verifone_ipc::send(inPacket,string(CPA_SERVICE_TASK_NAME));
	}
	
// 	if(iRc) {
// 		iRc=com_verifone_ipc::send(inPacket,string(GUI_SERVICE_TASK_NAME))?0:1;
// 	} else {
// 		com_verifone_ipc::send(inPacket,string(GUI_SERVICE_TASK_NAME));
// 	}
	
	return(iRc);
}

static int watchdog_timer=-1;
static const long custom_timer_evt = EVT_BIO;	// This is messy, stealing biometric event to id my timer :-(

int doWatchDog(string &inPacket)
{
	string tag, len, val, timeoutVal;
	BERTLV resTlv, inTlv(inPacket);
	int iPosition=0, iTimeout=0;
	
	inTlv.getTagLenVal(iPosition,tag,len,val);

	if(tag.compare("\xDF\xAF\x01")==0) {
	}
	
	iPosition=inTlv.getNextPosition();
	
	inTlv.getTagLenVal(iPosition,tag,len,val);

	if(tag.compare("\xDF\xAF\x02")==0) {
		if(inTlv.getValForTag(string("\xDF\xAF\x02"),timeoutVal)){
			
			iTimeout = timeoutVal.at(0) * 1000;
			
			if(iTimeout == 0){
				// Disable timer
				clr_timer(watchdog_timer);
				watchdog_timer = -1;
			}
			else{
				clr_timer(watchdog_timer);	// Make sure we don't leak timer handles
				watchdog_timer = set_timer(iTimeout, custom_timer_evt);	
			}
		}
	}

	resTlv.addTag(string("\xDF\xAF\x01"),string("\xF3"));
	resTlv.addTag(string("\xDF\xAF\x04"),string("\x00", 1));

	return(sendPacket(resTlv.getStringData())); 
}

int mapSystemParameter(const string &inTag,string &outKey)
{
	outKey.clear();
	if(inTag.compare("\xDF\xAF\x50")==0) outKey=string("FALLBACKTAPS");
	if(inTag.compare("\xDF\xAF\x51")==0) outKey=string("FALLBACKINSERTS");
	if(inTag.compare("\xDF\xAF\x52")==0) outKey=string("FALLBACKSWIPES");
	if(inTag.compare("\xDF\xAF\x53")==0) outKey=string("TIMEOUT");
	if(inTag.compare("\xDF\xAF\x54")==0) outKey=string("COMPORT");
	if(inTag.compare("\xDF\xAF\x55")==0) outKey=string("BITRATE");
	if(inTag.compare("\xDF\xAF\x56")==0) outKey=string("HOST");
	if(inTag.compare("\xDF\xAF\x57")==0) outKey=string("HOSTPORT");
	if(inTag.compare("\xDF\xAF\x58")==0) outKey=string("DEFAULTKEYSCHEME");
	if(inTag.compare("\xDF\xAF\x59")==0) outKey=string("COMTYPE");
	if(inTag.compare("\xDF\xAF\x5A")==0) outKey=string("SSL");
	if(inTag.compare("\xDF\xAF\x70")==0) outKey=string("USER1");
	if(inTag.compare("\xDF\xAF\x71")==0) outKey=string("USER2");
	
	return(outKey.length());
}

int updateSystemParameter(BERTLV &inTlv)
{
	string tag,len,val,iniKey;
	int iPosition;
	int iRc;
	
	BERTLV resTlv;
	resTlv.addTag(string("\xDF\xAF\x01"),string("\xC7"));
	resTlv.addTag(string("\xDF\xAF\x04"),string("\x15"));
	
	iPosition=0;
	inTlv.resetPosition();
	while(1) {
		inTlv.getTagLenVal(iPosition,tag,len,val);
		// handle date/time
		if(tag.compare("\xDF\xAF\x61")==0 && val.length()==14) {
			resTlv.updateTag(string("\xDF\xAF\x04"),string("\xFF"));
			int hClock=open(DEV_CLOCK,0);
			if(hClock>=0) {
				if(write(hClock,val.c_str(),14)>=0) resTlv.updateTag(string("\xDF\xAF\x04"),string("\x00",1));
				close(hClock);
			}
		}
		// other system parms
		if(mapSystemParameter(tag,iniKey)>0) {
			iRc=ini_puts("SERVUS",iniKey.c_str(),val.c_str(),"SERVUS.INI");
			if(iRc==1) resTlv.updateTag(string("\xDF\xAF\x04"),string("\x00",1));
		}
		iPosition=inTlv.getNextPosition();
		if(iPosition<=0) break;
	}
	
	return(sendPacket(resTlv.getStringData()));
}

int deleteSystemParameter(BERTLV &inTlv)
{
	string dfcc11;
	int iIndex,iIndex2;
	int iRc;
	string iniKey;
	
	inTlv.getValForTag(string("\xDF\xCC\x11"),dfcc11);
	
	BERTLV resTlv;
	resTlv.addTag(string("\xDF\xAF\x01"),string("\xC8"));
	resTlv.addTag(string("\xDF\xAF\x04"),string("\x15"));

	string tag;
	for(iIndex=0;iIndex<dfcc11.length();) {
		for(iIndex2=0;iIndex+iIndex2<dfcc11.length();iIndex2++) {
			if(iIndex2==0 && (dfcc11.at(iIndex+iIndex2)&0x1f)!=0x1f) break;
			if(iIndex2>0 && (dfcc11.at(iIndex+iIndex2)&0x80)!=0x80) break;
		}
		tag=dfcc11.substr(iIndex,iIndex2+1);
		iIndex+=(iIndex2+1);
		if(mapSystemParameter(tag,iniKey)>0) {
			iRc=ini_puts("SERVUS",iniKey.c_str(),"","SERVUS.INI");
			if(iRc==1) resTlv.updateTag(string("\xDF\xAF\x04"),string("\x00",1));
		}
	}

	return(sendPacket(resTlv.getStringData()));
}

int readSystemParameter(BERTLV &inTlv)
{
	string dfcc11;
	int iIndex,iIndex2;
	int iRc;
	char szBuffer[INI_BUFFERSIZE];
	string iniKey;
	
	inTlv.getValForTag(string("\xDF\xCC\x11"),dfcc11);
	
	BERTLV resTlv;
	resTlv.addTag(string("\xDF\xAF\x01"),string("\xC9"));
	resTlv.addTag(string("\xDF\xAF\x04"),string("\x15"));

	string tag;
	for(iIndex=0;iIndex<dfcc11.length();) {
		for(iIndex2=0;iIndex+iIndex2<dfcc11.length();iIndex2++) {
			if(iIndex2==0 && (dfcc11.at(iIndex+iIndex2)&0x1f)!=0x1f) break;
			if(iIndex2>0 && (dfcc11.at(iIndex+iIndex2)&0x80)!=0x80) break;
		}
		tag=dfcc11.substr(iIndex,iIndex2+1);
		iIndex+=(iIndex2+1);
		// handle serial number

		if(tag.compare("\xDF\xAF\x60")==0) {
			char szSerialNo[15];//11+1];
			memset(szSerialNo,0,sizeof(szSerialNo));
			SVC_INFO_SERLNO(szSerialNo);
			resTlv.updateTag(string("\xDF\xAF\x04"),string("\x00",1));
			resTlv.updateTag(tag,string(szSerialNo));
		}
		// handle date/time
		if(tag.compare("\xDF\xAF\x61")==0) {
			char szDateTime[15];
			read_clock(szDateTime);
			szDateTime[14]=0;
			resTlv.updateTag(string("\xDF\xAF\x04"),string("\x00",1));
			resTlv.updateTag(tag,string(szDateTime));
		}
        
        // handle release version number
        if(tag.compare("\xDF\xAF\x62")==0) {
			char szVersion[20];
			resTlv.updateTag(string("\xDF\xAF\x04"),string("\x00",1));
            sprintf(szVersion, "%d.%d.%d", VERSION_MAJOR, VERSION_MINOR, VERSION_REVISION);
			resTlv.updateTag(tag,string(szVersion));
		}

		// other system parms
		if(mapSystemParameter(tag,iniKey)>0) {
			iRc=ini_gets("SERVUS",iniKey.c_str(),"",szBuffer,sizeof(szBuffer),"SERVUS.INI");
			if(iRc>0) {
				resTlv.updateTag(string("\xDF\xAF\x04"),string("\x00",1));
				resTlv.updateTag(tag,string(szBuffer));
			}
		}
	}

	return(sendPacket(resTlv.getStringData()));
}

int processClientPacket(string &inPacket)
{
	BERTLV inTlv(inPacket);
	string packetType;
	int iRc;

	iRc=0;
	if(inTlv.getValForTag(string("\xDF\xAF\x01"),packetType) && packetType.length()==1) {
		switch(packetType.at(0)) {
			case 0x21:	// start transaction
				transactionPacket=inPacket;
				giFallbackInserts=ini_getl("SERVUS","FALLBACKINSERTS",3,"SERVUS.INI");
				giFallbackSwipes=ini_getl("SERVUS","FALLBACKSWIPES",3,"SERVUS.INI");
				gState=NORMAL;
				iRc=com_verifone_ipc::send(inPacket,string(CPA_SERVICE_TASK_NAME))?0:1;
				break;				
			case 0x22:	// complete transaction
			case 0x23:	// modify limits
			case 0x24:	// apdu
			case 0x25:	// remove card
			case 0x26:	// read media
			case 0x27:	// set get tlv
			case 0x41:	// update capk
			case 0x42:	// delete capk
			case 0x43:	// read capk
			case 0x44:	// update emv parameter
			case 0x45:	// delete emv parameter
			case 0x46:	// read emv parameter
			case 0x71:	// transaction status
				iRc=com_verifone_ipc::send(inPacket,string(CPA_SERVICE_TASK_NAME))?0:1;
				break;
			case 0x72:	// cancel transaction
				iRc=cancelTransaction(inPacket);
				break;
			case 0x73:	// watchdog timer
				iRc = doWatchDog(inPacket);
				break;
			case 0x31:	// display message
			case 0x32:	// display menu
			case 0x33:	// display template
				iRc=com_verifone_ipc::send(inPacket,string(GUI_SERVICE_TASK_NAME))?0:1;
				break;
			case 0x34:	// capture pin
				iRc=capturePin(inTlv);
				dlog_msg("capturePin: %d",iRc);
				break;
			case 0x35:	// update session key
				iRc=updateSessionKey(inTlv);
				break;
			case 0x47:	// update system parameter
				iRc=updateSystemParameter(inTlv);
				break;
			case 0x48:	// delete system parameter
				iRc=deleteSystemParameter(inTlv);
				break;
			case 0x49:	// read system parameter
				iRc=readSystemParameter(inTlv);
				break;
			default:	// unknown packet type
				string response("\xDF\xAF\x01\x01\xFF\xDF\xAF\x04\x01\x12");
				response.at(4)=packetType.at(0)^0x80;
				iRc=sendPacket(response);
				break;
		}
	} else {
		// unknown packet
		iRc=sendPacket(string("\xDF\xAF\x04\x01\x13"));
	}
	
	return(iRc);
}

int CeNetworkIsUp()
{
	int iNetworkCount;
	unsigned int uiActualCount;
	stNIInfo *pNIInfo;
	int iIndex;
	int iRc;
	
	iRc=0;
	iNetworkCount=ceGetNWIFCount();
	pNIInfo=(stNIInfo *)malloc(iNetworkCount*sizeof(stNIInfo));
	if(!pNIInfo) return(0);
	
	if(ceGetNWIFInfo(pNIInfo,iNetworkCount,&uiActualCount)<0) {
		free(pNIInfo);
		return(0);
	}
	
	for(iIndex=0;iIndex<uiActualCount;iIndex++) {
		if(pNIInfo[iIndex].niRunState==1) {
			iRc=1;
			break;
		}
	}
	
	free(pNIInfo);
	return(iRc);
}

int InitComms()
{
	char szType[80+1];
	char szComPort[80+1];
	char szHost[80+1];
	char rawData[CEIF_EVT_DATA_SZ];
	int iHostPort;
	char szSSL[80+1];
	int iIndex;
	int iRc;
	
	if(ghCom>=0) {
		close(ghCom);
		ghCom=-1;
	}
	
	if(giSocket>=0) {
		socketclose(giSocket);
		giSocket=-1;
	}
	
	ini_gets("SERVUS","COMTYPE","SERIAL",szType,sizeof(szType),"SERVUS.INI");
	dlog_msg("InitComms(): SERVUS.INI: COMTYPE='%s'",szType);
	if(strcmp(szType,"TCP")==0){

		iRc=ceRegister();
		dlog_msg("InitComms(): ceRegister=%d",iRc);

		if(iRc!=0) return(0);

		while(!CeNetworkIsUp()) {
			dlog_msg("InitComms(): waiting for network");
			SVC_WAIT(1000);
		}

		ini_gets("SERVUS","HOST","127.0.0.1",szHost,sizeof(szHost),"SERVUS.INI");
		iHostPort=ini_getl("SERVUS","HOSTPORT",8001,"SERVUS.INI");
		ini_gets("SERVUS","SSL","N",szSSL,sizeof(szSSL),"SERVUS.INI");
		dlog_msg("InitComms(): SERVUS.INI: HOST='%s' PORT=%d",szHost,iHostPort);

		giSocket=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
		dlog_msg("InitComms(): socket=%d",giSocket);

		if(giSocket<0)
			return(0);

		memset(&tAddress,0,sizeof(tAddress));
	    tAddress.sin_len=sizeof(tAddress);
    	tAddress.sin_family=AF_INET;
		tAddress.sin_port=htons(iHostPort);

		// check for non ip address host name
		iRc=0;
		for(iIndex=0;iIndex<strlen(szHost);iIndex++) {
			if((szHost[iIndex]<'0' || szHost[iIndex]>'9') && szHost[iIndex]!='.') {
				iRc=1;
				break;
			}
		}
		
		if(iRc>0) {
			while((iRc=DnsGetHostByName(szHost,(in_addr_t *)&tAddress.sin_addr))!=0) {
				dlog_msg("InitComms(): DnsGetHostByName=%d errno=%d",iRc,errno);
				SVC_WAIT(1000);
			}
		} else {
			inet_aton(szHost,(ip_addr *)&tAddress.sin_addr);
		}

		iRc=-1;
		while(iRc<0) {
			iRc=connect(giSocket,(struct sockaddr *)&tAddress,sizeof(tAddress));
			dlog_msg("InitComms(): connect=%d errno=%d",iRc,errno);
			SVC_WAIT(1000);
		}

		if(iRc!=0) return(0);
	} else {
		ini_gets("SERVUS","COMPORT","/DEV/COM1",szComPort,sizeof(szComPort),"SERVUS.INI");

		ghCom=open(szComPort,0);

		if(ghCom<0) return(0);

		open_block_t tOpenBlk;

	    tOpenBlk.rate=Rt_19200;
	    tOpenBlk.format=Fmt_A8N1;
	    tOpenBlk.protocol=P_char_mode;

	    set_opn_blk(ghCom,&tOpenBlk);
	}

	return(1);
}

typedef enum{
    STATE_EMPTY=0,
    STATE_INIT=1,
    STATE_START,
    STATE_CONNECTING,
    STATE_DISCONNECTING,
    STATE_CONNECTED,
    STATE_DISCONNECTED,
    STATE_INCOMING,
    STATE_PROCESSING,
    STATE_RESPONDING,
    STATE_ERROR
}State;

void comms_state_machine(int hEvent)
{
	long lEvents;
	int iRc=0;
	string display;
	string inBuffer;
	string receivedFrom;

	while(1) {
		lEvents=wait_evt(EVT_PIPE|EVT_COM1|EVT_USB_CLIENT|EVT_SOKT);

		if(lEvents&EVT_PIPE) {
			iRc=com_verifone_ipc::receive(inBuffer,string(""),receivedFrom);
			if(iRc==0) {
				if(receivedFrom.compare(string(CPA_SERVICE_TASK_NAME))==0) {
					iRc=processCpaServicePacket(inBuffer);
				}
				if(receivedFrom.compare(string(SECURITY_SERVICE_TASK_NAME))==0) {
					iRc=processSecurityServicePacket(inBuffer);
				}
				if(receivedFrom.compare(string(GUI_SERVICE_TASK_NAME))==0) {
					iRc=processGuiServicePacket(inBuffer);
				}
			}
		}
		if(lEvents&EVT_COM1 || lEvents&EVT_USB_CLIENT) {
			iRc=processComEvent(ghCom,inBuffer);

			if(iRc>0) {
				iRc=processClientPacket(inBuffer);
			}
		}
		if(lEvents&EVT_SOKT) {
			int iSockEvent;

			iRc=getsocketevents(giSocket,&iSockEvent);

			if(iRc==0 && (iSockEvent&SE_RECV)) {
				iRc=processTcpEvent(giSocket,inBuffer);

				if(iRc>0) {
					iRc=processClientPacket(inBuffer);
				}
			}
		}
	}
}

int main(int argc,char *argp[])
{
	char szText[255+1];
	char buffer[30], device[13];
    string display;
    int hEvent;
    int iRc;

	LOG_INIT("SERVUSAPP",LOGSYS_COMM,LOGSYS_PRINTF_FILTER);
	dlog_msg("*****SERVUSAPP has STARTED - task id: %d*****",get_task_id());

	// log device name on which we are running
	memset(device, 0, sizeof(device));
	SVC_INFO_MODELNO (device);
	dlog_msg("Device %s",device);

	// log version
    sprintf(buffer, "%d.%d.%d", VERSION_MAJOR, VERSION_MINOR, VERSION_REVISION);
	dlog_msg("Version %s",buffer);

	// set working directory
	com_verifone_pml::set_working_directory(com_verifone_pml::getRAMRoot().c_str());

	// init ipc
	iRc=com_verifone_ipc::init(string(MY_TASK_NAME));
	if(iRc<0) {
		dlog_msg("com_verifone_ipc::init failed [%d]",iRc);
		return(-1);
	}

	// init pml
	iRc=com_verifone_pml::pml_init(MY_TASK_NAME);
	if(iRc<0) {
		dlog_msg("com_verifone_pml::pml_init failed [%d]",iRc);
		return(-2);
	}

	// init event handler
	hEvent=com_verifone_pml::event_open();
	if(hEvent<0) {
		dlog_msg("com_verifone_pml::event_open, errno [%d]",errno);
		return(-3);
	}

	// add ipc event
	com_verifone_pml::event_ctl(hEvent,com_verifone_pml::ctl::ADD,com_verifone_ipc::get_events_handler());

	// connect to gui
	while(com_verifone_ipc::connect_to(string(GUI_SERVICE_TASK_NAME))!=0) {
		dlog_msg("Waiting for gui");
		SVC_WAIT(1000);
    }
	
	// connect to pam	
	while(com_verifone_ipc::connect_to(string(CPA_SERVICE_TASK_NAME))!=0) {
		dlog_msg("Waiting for pam");
		SVC_WAIT(1000);
    }
	
	// connect to scapp
	while(com_verifone_ipc::connect_to(string(SECURITY_SERVICE_TASK_NAME))!=0) {
		dlog_msg("Waiting for scapp");
		SVC_WAIT(1000);
    }
	
	// init comms
	if(!InitComms()) {
		dlog_msg("InitComms failed");
		error_tone();
		SVC_WAIT(1000);
    }

	// display servus ready
	BERTLV tlvIn;
	sprintf(szText,"<center><h1>Servus I</h1>%d.%d.%d<br /><h2>Ready for Transaction</h2></center>",VERSION_MAJOR,VERSION_MINOR,VERSION_REVISION);
	tlvIn.addTag("\xDF\xAF\x01","\x31");
	tlvIn.addTag("\xDF\xAF\x34",szText);
	tlvIn.addTag("\xDF\xAF\x31","\x01");
	com_verifone_ipc::send(tlvIn.getStringData(),GUI_SERVICE_TASK_NAME);
    
    // main loop
    comms_state_machine(hEvent);
    
    // we shound never get here
    return(0);
}
