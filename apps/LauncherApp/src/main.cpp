#ifdef VFI_PLATFORM_VOS
	#include <unistd.h>
#else
	#include <svc.h>
	#include <errno.h>
#endif
#include <liblog/logsys.h> 
#include <stdio.h>

#define MY_TASK_NAME	"LAUNCHERAPP"

int main(int argc,char *argvp[])
{
	int iIndex;
	int iRc;
	static const char *szAppToStart[] = {
		"F:GUISERVER.OUT",
		"F:GUIBRIDGEAPP.OUT",
		"F:SCAPP.OUT",
		"F:CARDAPP.OUT",
 		"F:PAMAPP.OUT",
 		"F:SERVUSAPP.OUT",
	};

	// initialise logging
	LOG_INIT(MY_TASK_NAME,LOGSYS_COMM,LOGSYS_PRINTF_FILTER);
	dlog_msg("started %s, task no %d",MY_TASK_NAME,get_task_id());
	
	for (iIndex=0;iIndex<sizeof(szAppToStart)/sizeof(szAppToStart[0]);iIndex++) {
		iRc=run(szAppToStart[iIndex],NULL);
		dlog_msg("run(%s)=%d",szAppToStart[iIndex],iRc);
		if(iRc<0) {
			error_tone();
			dlog_msg("failed to launch %s, rc=%d, errno=%d",szAppToStart[iIndex],iRc,errno);
		}
		SVC_WAIT(100);
	}

	return(0);
}
