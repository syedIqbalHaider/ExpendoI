#!/bin/bash
set -e
unset PKG_REMOVEBUNDLE
#export LD_PRELOAD=libfaketime-preload.so


#Print usage
_pkg_print_usage()
{
cat << EOF
usage: $0 [--help] --version RELEASE [--destdir DESTDIR] --build BUILD --terminal TERMTYPE --platform PLATFORM --name NAME

Create VIPA release script
OPTIONS:
	--help                  Show this help string
	--version RELEASE       Release version
	--destdir DESTDIR       Destination directory
	--build BUILD           Build type debug/release
	--terminal TEMTYPE      Terminal type
	--platform PLATFORM     verixevo or vos
	--name NAME             package name
    --devcert [0/1]         sign package using devcert [no/yes]
    --gui					guiapp or directgui
EOF
}


#Sanity check the required components
_pkg_binaries_check()
{
	local req_components=( basename dirname find sed filesignature grep gawk)
	if [ $PKG_PLATFORM == "vos" ]; then
		req_components+=( 7z )
	fi
	if [ $PKG_PLATFORM == "vos" ]; then
		req_components+=( mxpackage )
	fi
	for item in ${req_components[*]}
	do
		if [[ ! $(type -P "$item") ]]; then
			echo "Executable command $item required by script is not in path. Please install required util"
			exit -1
		fi
	done
}

# -------------------------------------------------------------------------- 
############################ Variables #####################################
PKG_DESTDIR=$(pwd)
pkg_src_root=$(pwd)
PKG_GUIMODE="guiapp"
# -------------------------------------------------------------------------- 
############################ Main entry point ##############################
for fv in $@; do
case $1 in
	'--help')
		_pkg_print_usage
		exit -1
		;;
	'--version')
		PKG_VERSION=$2
		if [ -z $PKG_VERSION ]; then
			echo "Package version not supplied"
			exit -1
		fi
		shift 2
		;;
	'--destdir') 
		if [ -z $2 ]; then
			shift 1
		else
#			PKG_DESTDIR=$(realpath $2)
			PKG_DESTDIR=$2
			shift 2
		fi
		;;
	'--name') 
		if [ -z $2 ]; then
			shift 1
		else
			PKG_PACKNAME=$2
			shift 2
		fi
		;;
	'--build') 
		PKG_BUILDTYPE=$2
		if [ -z $PKG_BUILDTYPE ]; then
			echo "Package build type not supplied"
			exit -1
		fi
		shift 2
		;;
	'--terminal') 
		PKG_TERMTYPE=$2
		if [ -z $PKG_TERMTYPE ]; then
			echo "Package terminal type not supplied"
			exit -1
		fi
		shift 2
		;;
	'--platform') 
		PKG_PLATFORM=$2
		if [ -z $PKG_PLATFORM ]; then
			echo "Package terminal type not supplied"
			exit -1
		fi
		shift 2
		;;
	'--removebundle')
		if [ -z "$2" ]; then
			shift 1
		else
			PKG_REMOVEBUNDLE+="--removebundle $2 "
			shift 2
		fi
		;;
    '--devcert')
        if [ -z "$2" ]; then
			shift 1
		else
			PKG_DEVCERTMODE="$2"
			shift 2
		fi
        ;;
    '--gui')
        if [ -z "$2" ]; then
			shift 1
		else
			PKG_GUIMODE="$2"
			shift 2
		fi
        ;;
    '--'*)
       echo "Invalid parameter $1. Try --help"
       exit -1
       ;;
esac
done

#Check argument count
if [ -z $PKG_VERSION ] || [ -z $PKG_BUILDTYPE ] || [ -z $PKG_TERMTYPE ] || [ -z $PKG_PLATFORM ] || [ -z $PKG_PACKNAME ]; then
	echo "Invalid argument count. Try --help"
	exit -1
fi

#Compare valid types of args
if [ $PKG_PLATFORM != "verixevo" ] && [ $PKG_PLATFORM != "vos" ]; then
	echo "Invalid platform $PKG_PLATFORM"
	exit -1
fi
if [ $PKG_BUILDTYPE != "debug" ] && [ $PKG_BUILDTYPE != "release" ]; then
	echo "Invalid buildtype $PKG_BUILDTYPE"
	exit -1
fi
#Platform compiler
if [ $PKG_PLATFORM == "verixevo" ]; then
PKG_PLATFORM_COMPILER="rvds4"
else
PKG_PLATFORM_COMPILER="gcc"
fi
# check for required binaries
_pkg_binaries_check

# --------------------------------------------------------------------------
