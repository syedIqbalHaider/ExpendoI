/****************************************************************************
*  Product:     InFusion
*  Company:     VeriFone
*  Author:      GSS R&D Germany
*  Content:     Memory pool
****************************************************************************/

#ifndef MEM_POOL_H
#define MEM_POOL_H

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _VRXEVO
#   ifdef TLV_UTIL_EXPORT
#       define TLV_UTIL_INTERFACE __declspec(dllexport)
#   elif defined TLV_UTIL_IMPORT
#       define TLV_UTIL_INTERFACE __declspec(dllimport)
#   else
#       define TLV_UTIL_INTERFACE
#   endif
#else
#   define TLV_UTIL_INTERFACE
#endif


/** structure for managing free chunks of memory 
 * \date 4.12.2006 \author M. Meixner
 */
struct PoolNode {
   struct PoolNode *pxNext; /**< pointer to next free memory chunk */
   unsigned uSize;          /**< size of this PoolNode i.e. this free memory */
};

/** Memory pool structure.
 *
 *  The memory pool allows to allocate/free memory from a given region of memory.
 *  Using such a pool has some interesting properties:
 *   - By releasing the memory of the pool itself all allocations that have been
 *     made from the pool are released.
 *   - The allocatable size is limited to the size of the pool.
 *   - Memory fragmentation is limited to the pool.
 *   .
 * \sa vPoolInit(), pvPoolMalloc(),pvPoolRealloc(),vPoolFree()
 * \date 4.12.2006 \author M. Meixner
 */
struct MemoryPool {
   struct PoolNode *pxFirst; /**< pointer to the first free memory chunk */
   void *pvStart; /**< start of the managed memory region */
   void *pvEnd;   /**< end of the managed memory region (first addres past the end) */
};

/** statistics information of free memory 
 * \date 8.2.2007 \author M. Meixner
 */
struct MemoryPoolInfo {
   unsigned uSegments; /**< number of segments of free memory */
   unsigned uFree;     /**< number of free bytes in the pool */
   unsigned uLargest;  /**< size of the largest segment in the memory pool */
};

TLV_UTIL_INTERFACE void vPoolInit(struct MemoryPool *pool, void *mem, unsigned size);
TLV_UTIL_INTERFACE void *pvPoolMalloc(struct MemoryPool *pool, unsigned size);
TLV_UTIL_INTERFACE void *pvPoolRealloc(struct MemoryPool *pool, void *memory, unsigned newsize);
TLV_UTIL_INTERFACE void vPoolFree(struct MemoryPool *pool, void *memory);
TLV_UTIL_INTERFACE void vPoolFreeAll(struct MemoryPool *pool);
TLV_UTIL_INTERFACE void vPoolInfo(struct MemoryPool *pool, struct MemoryPoolInfo *info);

#ifdef __cplusplus
}
#endif

#endif  // #ifndef MEM_POOL_H

