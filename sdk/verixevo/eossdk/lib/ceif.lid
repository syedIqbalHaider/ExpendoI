# ceif lid - lid file for the VxEOS CEIF
# shared library on Vx
CEIF 	id=18	addr=0	ver=0.1	thumb

1	ceRegister
2 	ceUnregister
3	ceSetCommDevMgr
4	ceRequestDevice 
5	ceReleaseDevice
6	ceGetDDParamValue
7	ceSetDDParamValue
8	ceExCommand
9	ceGetNWIFCount
10	ceGetNWIFInfo
11	ceStartNWIF
12	ceStopNWIF
13	ceSetNWIFStartMode
14	ceGetNWIFStartMode
15	ceSetNWParamValue
16	ceGetNWParamValue
17	ceStartDialIF
18	ceStopDialIF
19	ceEnableEventNotification
20	ceDisableEventNotification
21  	ceGetEventCount
22  	ceGetEvent
23	ceSetSignalNotification
24	ceIsEventNotificationEnabled
25	ceActivateNCP
26	ceGetVersion
27	ceGetDialHandle
28	ceSetDialMode
29	ceGetDialMode
30	ceSetBattThreshold
31	ceGetBattThreshold
32    ceDualSIMSwitch
#	Message Exchange API / Starting at 51
51	mxCreatePipe
52	mxClosePipe
53	mxGetPipeHandle
54	mxSend
55	mxPending
56	mxRecv
57  	mxRecvEx
# 	Message Formatting API / Starting at 71
71	mfCreateHandle
72	mfDestroyHandle
73	mfAddInit
74	mfAddClose
75	mfAddTLV
76	mfFetchInit
77	mfFetchClose
78	mfFetchReset
79	mfPeekNextTLV
80	mfFetchNextTLV
81	mfFindTLV
82	mfEstimate

