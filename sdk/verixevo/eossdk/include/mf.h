//----------------------------------------------------------------------
// Copyright (C) 2009 by VeriFone Inc. All rights reserved.
//
// No part of this software may be used, stored, compiled, reproduced,
// modified, transcribed, translated, transmitted, or transferred, in
// any form or by any means  whether electronic, mechanical,
// magnetic, optical, or otherwise, without the express prior written
// permission of VeriFone, Inc.
//----------------------------------------------------------------------

#ifndef __MF_H
#define __MF_H

#ifdef __cplusplus
extern "C" {
#endif

// mfAPI Constants
//#include <mfConst.h>

// mfAPI Constants
#define MF_BUFFER_SIZE_MIN  (sizeof(int)) // Minimum size of TLV buffer

// mfAPI Error codes
#define EMF_TAG_SIZE      -3001 // Tag size should be greater than zero.
#define EMF_VALUE_SIZE    -3002 // Value size should be greater than zero.
#define EMF_PARAM_NULL    -3003 // Parameter is null when one is not expected.
#define EMF_PARAM_INVALID -3004 // Parameter is invalid or out of range
#define EMF_TLV_BUF_SIZE  -3005 // The buffer size is less than minimum required size
#define EMF_UNKWN_HANDLE  -3006 // Unknown session handle
#define EMF_BUFFER_FULL   -3007 // No space in buffer to add tag.
#define EMF_OP_NOT_SUPP   -3008 // Operation not supported. 
#define EMF_TAG_NO_MATCH  -3009 // No matching tag was found.
#define EMF_TAG_EOL       -3010 // End of list. No more tags in list.

#define EMF_SUCCESS 0

// Handle Management
void *mfCreateHandle(void);
void mfDestroyHandle(const void *hF);

// Add Management
int mfAddInit(const void *hF, void *tlvBuffer, const unsigned int tlvBufferSize);
int mfAddClose(const void *hF);

// Add Tags
int mfAddTLV(const void *hF, const void *tag, const unsigned int tagLen, 
 const void *value, const unsigned int valueLen);

// Fetch Management Operations
int mfFetchInit(const void *hF, const void *tlvBuffer);
int mfFetchClose(const void *hF);
int mfFetchReset(const void *hF);

// Peek & Fetch Tags
int mfPeekNextTLV(const void *hF, unsigned int *tagLen, unsigned int *valueLen);
int mfFetchNextTLV(const void *hF, 
 const unsigned int tagSize, void *tag, unsigned int *tagLen, 
 const unsigned int valueSize, void *value, unsigned int *valueLen);

// Find TLV
int mfFindTLV(const void *hF, const void *tag, const unsigned int tagLen, 
 const unsigned int valueSize, void *value, unsigned int *valueLen);

// Helper function
int mfEstimate(const unsigned int tagCount, const unsigned int sigmaTag, 
  const unsigned int sigmaValue);

#ifdef __cplusplus
}
#endif

#endif // __MF_H
