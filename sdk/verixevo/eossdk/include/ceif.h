//----------------------------------------------------------------------
// Copyright (C) 2009 by VeriFone Inc. All rights reserved.
//
// No part of this software may be used, stored, compiled, reproduced,
// modified, transcribed, translated, transmitted, or transferred, in
// any form or by any means  whether electronic, mechanical,
// magnetic, optical, or otherwise, without the express prior written
// permission of VeriFone, Inc.
//----------------------------------------------------------------------

#ifndef __CEIF_H
#define __CEIF_H

#ifdef __cplusplus
extern "C" {
#endif
#include <mx.h>
#include <mf.h>
#include <ceifConst.h>


//======================================================================
// ceAPI
//======================================================================


//----------------------------------------------------------------------
// Registration
//----------------------------------------------------------------------
int ceRegister(void);

int ceUnregister(void);

int ceSetCommDevMgr(void);

//----------------------------------------------------------------------
// Device Management
//----------------------------------------------------------------------
int ceRequestDevice(const char deviceName[]);
int ceReleaseDevice(const char deviceName[]);


//----------------------------------------------------------------------
// Device Driver (DDI) configuration
//----------------------------------------------------------------------
int ceGetDDParamValue(const unsigned int niHandle, 
                      const char paramNameStr[], 
                      const unsigned int paramValueSize, 
                      void *paramValue, 
                      unsigned int *paramValueLen);

int ceSetDDParamValue(const unsigned int niHandle, 
                      const char paramNameStr[], 
                      const void *paramValue, 
                      const unsigned int paramValueLen);

//----------------------------------------------------------------------
// Sending Commands to Device
//----------------------------------------------------------------------
int ceExCommand(const unsigned int niHandle, 
                const void *cmdStr, 
                const unsigned int cmdStrLen, 
                const unsigned int cmdRespSize, 
                void *cmdResp, 
                unsigned int *cmdRespLen, 
                const unsigned long timeoutMS);


//----------------------------------------------------------------------
// Communication Infrastructure Configuration, Management & Control
//----------------------------------------------------------------------
int ceGetNWIFCount(void);

int ceGetNWIFInfo(stNIInfo stArray[], 
                  const unsigned int stArraySize,
                  unsigned int *arrayCount);

int ceStartNWIF(const unsigned int niHandle, 
                const int cl);

int ceStopNWIF(const unsigned int niHandle, 
               const int cl);

int ceDualSIMSwitch(const unsigned int niHandle,
		const int niSIM);

int ceSetNWIFStartMode(const unsigned int niHandle, 
                       const unsigned int startmode);

int ceGetNWIFStartMode(const unsigned int niHandle);

int ceSetNWParamValue(const unsigned int niHandle, 
                      const char paramNameStr[], 
                      const void *paramValue, 
                      const unsigned int paramValueLen);

int ceGetNWParamValue(const unsigned int niHandle, 
                      const char paramNameStr[], 
                      void *paramValue, 
                      const unsigned int paramValueSize, 
                      unsigned int *paramValueLen);

int ceSetBattThreshold(const unsigned int iLevel);

int ceGetBattThreshold(void);


//----------------------------------------------------------------------
// Dial Interface Control -- Functions for Dial w/o PPP
//----------------------------------------------------------------------
int ceStartDialIF(const unsigned int diHandle, 
                  const unsigned int deSize, 
                  char dialErrorStr[]);

int ceStopDialIF(const unsigned int diHandle);

int ceGetDialHandle(const unsigned diHandle, 
			const unsigned int deSize, 
			char *dialErrorStr);

int ceSetDialMode(const unsigned int mode);

int ceGetDialMode(void);


//----------------------------------------------------------------------
// Event Notification
//----------------------------------------------------------------------
int ceEnableEventNotification(void);

int ceDisableEventNotification(void);

int ceGetEventCount(void);

int ceGetEvent(stceNWEvt *ceEvent, 
               const int applExtEvtDataSize,
               void *applExtEvtData, 
               int *applExtEvtDataLen);

int ceSetSignalNotification(const unsigned int flag);

int ceIsEventNotificationEnabled(unsigned int *flag);

//----------------------------------------------------------------------
// Miscellany
//----------------------------------------------------------------------
int ceActivateNCP(void);

int ceGetVersion(const unsigned int component, 
                 const unsigned int verStrSize, 
                 char verStr[]);

#ifdef __cplusplus
}
#endif

#endif // __CEIF_H
