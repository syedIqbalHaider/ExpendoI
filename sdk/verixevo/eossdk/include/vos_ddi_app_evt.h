/* 
 *	This is the header file that contains application events of DDI drivers
 */

/*********************************************************************
 *	GENERAL APPLICATION EVENTS
 *********************************************************************/
#define NO_BATTERY			200
#define NO_SIM_CARD		201
#define SIM_PIN_RETRY		202

/* SMS notifications */
#define NOTIFY_SEND_TEXT_SMS			210
#define NOTIFY_SEND_PDU_SMS			211

/*********************************************************************
 *	GPRS
 *********************************************************************/
#define GPRS_SIGNAL_QUALITY_STATUS	300
#define GPRS_ROAM_STATUS			301
#define GPRS_MESSAGE_STATUS			302
#define GPRS_CALL_STATUS			303
#define GPRS_SMSFULL_STATUS			304
#define GPRS_SERVICE_STATUS			305
#define GPRS_PKT_SWITCHED_STATUS	306

/*********************************************************************
 *	GSM
 *********************************************************************/
#define GSM_SIGNAL_QUALITY_STATUS	GPRS_SIGNAL_QUALITY_STATUS
#define GSM_ROAM_STATUS				GPRS_ROAM_STATUS
#define GSM_MESSAGE_STATUS			GPRS_MESSAGE_STATUS
#define GSM_CALL_STATUS				GPRS_CALL_STATUS
#define GSM_SMSFULL_STATUS			GPRS_SMSFULL_STATUS
#define GSM_SERVICE_STATUS			GPRS_SERVICE_STATUS

/*********************************************************************
 *	CDMA
 *********************************************************************/
	
#define CDMA_EVDO_NOTIFY_DATA_EVT   600
#define CDMA_ROAM_STATUS   601
#define CDMA_DORMANT_STATUS   602
#define CDMA_CALL_STATUS   603	
#define CDMA_COVERAGE_STATUS   604

/*********************************************************************
 *	WIFI
 *********************************************************************/
	
#define WIFI_SCAN_RESULTS   700

/*********************************************************************
 *	BLUETOOTH
 *********************************************************************/

#define BT_SEARCH_DONE				800
#define BT_QUERY_REMOTE_NAME_DONE	801
#define BT_FIRMWARE_UPGRADE_INIT	802
#define BT_FIRMWARE_DOWNLOAD_DONE	803
#define BT_FIRMWARE_UPGRADE_DONE	804
#define BT_FIRMWARE_UPGRADE_FAIL	805
#define BT_SEARCH_INIT				806
#define BT_SEARCH_FAIL				807
#define BT_QUERY_REMOTE_NAME_FOUND	808
#define BT_MDM_PROF_UPDATE_INIT		809
#define BT_MDM_PROF_UPDATE_DONE		810
#define BT_MDM_PROF_UPDATE_FAIL		811

#define BT_RESTORE_FACTORY_INIT		812
#define BT_RESTORE_FACTORY_DONE		813
#define BT_RESTORE_FACTORY_FAIL		814
