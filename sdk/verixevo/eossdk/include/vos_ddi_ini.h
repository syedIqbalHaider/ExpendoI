/*
 *	This header file contains the list of INI parameters that CommEngine can access
 */

/*********************************************************************
 *	COMMON INI parameters
 *********************************************************************/

 #define		INI_SUPP_NO_PPP_DISC 			"SUPP_NO_PPP_DISC"
/* Version of the driver */
#define			INI_DRIVER_VERSION						"DRIVER_VER"

/* Does the driver support AT commands? */
#define			INI_SUPPORTS_AT							"SUPP_AT"

/* Does the driver report signal strength information? */
#define			INI_SUPPORTS_RSSI						"SUPP_RSSI"

/* Does the driver report abortion while connecting? */
#define			INI_SUPPORTS_ABORT						"SUPP_ABORT"

/* Maximum connection retries */
#define			INI_MAX_CONNECTION_RETRIES				"CONN_RET"

/* Maximum retries during poor network coverage */
#define			INI_POOR_NETWORK_RETRIES				"PR_NET_RET"

/* Maximum retries during no network coverage */
#define			INI_NO_NETWORK_RETRIES					"NO_NET_RET"

/* Network retry timeout in milliseconds */
#define			INI_NETWORK_RETRY_TIMEOUT				"NET_RET_TM"

/* Connection retry timeout in milliseconds */
#define			INI_CONNECTION_RETRY_TIMEOUT			"CONNRET_TM"


/*********************************************************************
 *	ETHERNET INI parameters
 *********************************************************************/

/* Set if to 1 if you want to allow multicast else set to 0 */
#define			INI_ALLOW_MULTICAST	"ET_MULCAST"

/* Below are LAN G4 INI parameters */

/* Use hardware flow control - boolean */
#define			INI_ET_HW_FCTL		"ET_HW_FCTL"

/* Modem send timeout in milliseconds */
#define			INI_ET_ATSNDTO		"ET_ATSNDTO"

/* Response timeout used for all init AT commands */
#define			INI_ET_INIT_TO		"ET_INIT_TO"

/* Using a single intecharacter timeout for all AT commands */
#define			INI_ET_ICHAR_TO		"ET_ICHAR_TO"

/* This is the number of times the 'modem_start' string is sent before OK is returned */
#define			INI_ET_INITRET		"ET_INITRET"


/*********************************************************************
 *	GPRS INI parameters
 *********************************************************************/

/* Start up string */
#define			INI_GPRS_START							"GP_START"

/* Init String */
#define			INI_GPRS_INIT_1						"GP_INIT_1"

/* Second init string */
#define			INI_GPRS_INIT_2						"GP_INIT_2"

/* Third init string */
#define			INI_GPRS_INIT_3						"GP_INIT_3"

/* Fourth init string */
#define			INI_GPRS_INIT_4						"GP_INIT_4"

/* GPRS Access point name */
#define			INI_GPRS_APN							"GP_APN"

/* GPRS Access point name for Secondary SIM */
#define			INI_GPRS_APN2							"GP_APN2"

/* GPRS Number to dial */
#define			INI_GPRS_PRIMARY						"GP_PRIMARY"

/* Does the driver need to check is GPRS is available before connecting? */
#define			INI_GPRS_MONITOR						"GP_MONITOR"

/* PIN of the SIM card */
#define			INI_GPRS_SIM_PIN						"GP_SIM_PIN"

/* PIN2 of the SIM card */
#define			INI_GPRS_SIM_PIN2						"GP_SIM_PIN2"

/* PIN of the Secondary SIM card */
#define			INI_GPRS_SIM2_PIN						"GP_SIM2_PIN"

/* PIN2 of the Secondary SIM card */
#define			INI_GPRS_SIM2_PIN2						"GP_SIM2_PIN2"

/* PUK of the SIM card */
#define			INI_GPRS_SIM_PUK						"GP_SIM_PUK"

/* PUK2 of the SIM card */
#define			INI_GPRS_SIM_PUK2						"GP_SIM_PUK2"

/* PUK of the Secondary SIM card */
#define			INI_GPRS_SIM2_PUK						"GP_SIM2_PUK"

/* PUK of the Secondary SIM card */
#define			INI_GPRS_SIM2_PUK2						"GP_SIM2_PUK2"

/* Maximum retries during poor network */
#define			INI_GPRS_MIN_RSSI						"GP_MINRSSI"

/* Timeout in milliseconds when sending an AT Command to the modem */
#define			INI_GPRS_AT_SEND_TIMEOUT				"GP_SEND_TO"

/* Timeout in milliseconds when receiving a response from an AT Command is sent to the modem */
#define			INI_GPRS_AT_RESPONSE_TIMEOUT			"GP_RESP_TO"

/* Data link connection timeout in milliseconds */
#define			INI_GPRS_CONNECT_TIMEOUT				"GP_CONN_TO"

/* Network negotiation timeout in milliseconds */
#define			INI_GPRS_NETWORK_TIMEOUT				"GP_NET_TO"

/* Data link disconnection timeout in milliseconds */
#define			INI_GPRS_DISCONNECT_TIMEOUT				"GP_DISC_TO"

/* Interchar timeout in milliseconds */
#define			INI_GPRS_INTERCHAR_TIMEOUT				"GP_CHAR_TO"

/* Delay in between retries in sending initialization strings to the modem (milliseconds) */
#define			INI_GPRS_AT_DELAY						"GP_ATDELAY"

/* GPRS network attach timeout in milliseconds */
#define			INI_GPRS_ATTACH_TIMEOUT					"GP_ATCH_TO"

/* Retries in sending the initialization strings in the modem */
#define			INI_GPRS_INIT_RETRIES					"GP_INIT_RT"

/* Retries in powering up the GPRS modem */
#define			INI_GPRS_POWER_UP_RETRIES				"GP_POWR_RT"

/* Detach GPRS network before making a connection */
#define			INI_GPRS_DETACH							"GP_DETACH"

/* Default setting for network registration notification to be sent to application */
#define			INI_GPRS_SERVICE_NOTIFICATION			"GP_SERVSTS"

/* Default setting for Bit Error Rate notification to be sent to application */
#define			INI_GPRS_BER_NOTIFICATION				"GP_BER_STS"

/* Default setting for roaming notification to be sent to application */
#define			INI_GPRS_ROAM_NOTIFICATION				"GP_ROAMSTS"

/* Default setting for unread short message notification to be sent to application */
#define			INI_GPRS_SMS_NOTIFICATION				"GP_SMS_STS"

/* Default setting for call notification to be sent to application */
#define			INI_GPRS_CALL_NOTIFICATION				"GP_CALLSTS"

/* Default setting for SMS full memory notification to be sent to application */
#define			INI_GPRS_SMS_FULL_NOTIFICATION			"GP_SMSFSTS"

/* Defines if the driver issues shutdown AT command to the modem before closing port */
#define			INI_GPRS_RADIO_SHUTDOWN					"GP_SHUTDWN"

/* Timeout in milliseconds when waiting for DCD to go low before the driver does a force disconnection */
#define			INI_GPRS_WAIT_DCD_LOW_TIMEOUT			"GP_DCDLWTO" 

/* Maximum consecutive failed AT commands sent due to time-out before device is deemed not operable */
#define			INI_GPRS_MAX_AT_TIMEOUT_FAIL			"GP_ATMAXRT"

/* SIM Slot to use for initial connection */
#define			INI_GPRS_DSIM_PRIMARY					"GP_DSIM_PRI"
/* Default setting to indicate whether to check for registration notifications */
#define			INI_GPRS_REGISTRATION						"GP_REG"

/* Timeout in milliseconds to wait for successful registration */
#define			INI_GPRS_REGISTRATION_TIMEOUT			"GP_REG_TO"

/* AT+SCID boolean parameter for determining if it should be issued or not, default is FALSE */
#define			INI_GPRS_SIMID_CHK						"GP_SIMID_CHK"

/*********************************************************************
 *	GSM INI parameters
 *********************************************************************/

/* Start up string */
#define			INI_GSM_START							"GS_START"

/* Init String */
#define			INI_GSM_INIT_1							"GS_INIT_1"

/* Second init string */
#define			INI_GSM_INIT_2							"GS_INIT_2"

/* Third init string */
#define			INI_GSM_INIT_3							"GS_INIT_3"

/* Fourth init string */
#define			INI_GSM_INIT_4							"GS_INIT_4"

/* GSM Number to dial */
#define			INI_GSM_PRIMARY							"GS_PRIMARY"

/* PIN of the SIM card */
#define			INI_GSM_SIM_PIN							"GS_SIM_PIN"

/* PIN of the SIM card */
#define			INI_GSM_SIM_PIN2							"GS_SIM_PIN2"

/* PIN of the Secondary SIM card */
#define			INI_GSM_SIM2_PIN							"GS_SIM2_PIN"

/* PIN of the Secondary SIM card */
#define			INI_GSM_SIM2_PIN2							"GS_SIM2_PIN2"

/* PUK of the SIM card */
#define			INI_GSM_SIM_PUK							"GS_SIM_PUK"

/* PUK of the SIM card */
#define			INI_GSM_SIM_PUK2							"GS_SIM_PUK2"

/* PUK of the Secondary SIM card */
#define			INI_GSM_SIM2_PUK							"GS_SIM2_PUK"

/* PUK of the Secondary SIM card */
#define			INI_GSM_SIM2_PUK2							"GS_SIM2_PUK2"

/* Maximum retries during poor network */
#define			INI_GSM_MIN_RSSI						"GS_MINRSSI"

/* Timeout in milliseconds when sending an AT Command to the modem */
#define			INI_GSM_AT_SEND_TIMEOUT					"GS_SEND_TO"

/* Timeout in milliseconds when receiving a response from an AT Command is sent to the modem */
#define			INI_GSM_AT_RESPONSE_TIMEOUT				"GS_RESP_TO"

/* Data link connection timeout in milliseconds */
#define			INI_GSM_CONNECT_TIMEOUT					"GS_CONN_TO"

/* Network negotiation timeout in milliseconds */
#define			INI_GSM_NETWORK_TIMEOUT				"GS_NET_TO"

/* Data link disconnection timeout in milliseconds */
#define			INI_GSM_DISCONNECT_TIMEOUT				"GS_DISC_TO"

/* Interchar timeout in milliseconds */
#define			INI_GSM_INTERCHAR_TIMEOUT				"GS_CHAR_TO"

/* Delay in between retries in sending initialization strings to the modem (milliseconds) */
#define			INI_GSM_AT_DELAY						"GS_ATDELAY"

/* Retries in sending the initialization strings in the modem */
#define			INI_GSM_INIT_RETRIES					"GS_INIT_RT"

/* Retries in powering up the GSM modem */
#define			INI_GSM_POWER_UP_RETRIES				"GS_POWR_RT"

/* Default setting for network registration notification to be sent to application */
#define			INI_GSM_SERVICE_NOTIFICATION			"GS_SERVSTS"

/* Default setting for Bit Error Rate notification to be sent to application */
#define			INI_GSM_BER_NOTIFICATION				"GS_BER_STS"

/* Default setting for roaming notification to be sent to application */
#define			INI_GSM_ROAM_NOTIFICATION				"GS_ROAMSTS"

/* Default setting for unread short message notification to be sent to application */
#define			INI_GSM_SMS_NOTIFICATION				"GS_SMS_STS"

/* Default setting for call notification to be sent to application */
#define			INI_GSM_CALL_NOTIFICATION				"GS_CALLSTS"

/* Default setting for SMS full memory notification to be sent to application */
#define			INI_GSM_SMS_FULL_NOTIFICATION			"GS_SMSFSTS"

/* Defines if the driver issues shutdown AT command to the modem before closing port */
#define			INI_GSM_RADIO_SHUTDOWN					"GS_SHUTDWN"

/* Maximum consecutive failed AT commands sent due to time-out before device is deemed not operable */
#define			INI_GSM_MAX_AT_TIMEOUT_FAIL				"GS_ATMAXRT"

/* SIM Slot to use for initial connection */
#define			INI_GSM_DSIM_PRIMARY					"GS_DSIM_PRI"

/* AT+CBST command parameters for setting bearer service type */
#define			INI_GSM_BEARER_SVC_TYPE					"GS_BEARER_SVC_TYPE"

/* AT+SCID boolean parameter for determining if it should be issued or not, default is FALSE */
#define			INI_GSM_SIMID_CHK						"GS_SIMID_CHK"

/*********************************************************************
 *	CDMA INI parameters
 *********************************************************************/

// Number of millisenconds to wait between sending AT commands
#define INI_EVDO_AT_DELAY                 "EV_AT_DELY" 

//Number of milliseconds to wait for AT command send to be successfull
#define INI_EVDO_AT_SEND_TIMEOUT          "EV_AT_TO"

//Modem Baud rate
#define INI_EVDO_BAUD                     "EV_BAUD"

//Modem Format
#define INI_EVDO_FMT                      "EV_FMT"

//Enable default CnS notifications
#define INI_EVDO_ENABLE_DFLT_NOTIFY       "EV_NOTIFY"

//CnS command/response Time out
#define INI_EVDO_CNS_CMD_RESP_TO           "EV_CNS_TO"

//RSSI threshold value(should be in Hex value)
#define INI_EVDO_MINIMUM_RSSI              "EV_MINRSSI"

//Battery charge threshold 
#define INI_EVDO_LOW_BATTERY_LIMIT         "EV_BATTERY"

// This is the number of times the 'modem_start' string is sent before OK is returned
#define INI_EVDO_RETRIES                   "EV_INITRRY"

// Response timeout in milliseconds used for all init AT commands  
#define INI_EVDO_INIT_TIMEOUT              "EV_INIT_TO"

/* Interchar timeout in milliseconds */
#define INI_EVDO_INTERCHAR_TIMEOUT         "EV_INTCHTO"

//In case of no response for AT commands, modem will be reset. The below parameter is no. of retries checking for COM2 enumeration after reset
#define INI_EVDO_PWR_UP_RETRIES            "EV_PWR_TRY"

// Data connection timeout in milliseconds 
#define INI_EVDO_CONNECT_TIMEOUT	       "EV_CON_TO"

/* Data link disconnection timeout in milliseconds */
#define INI_EVDO_DISCONNECT_TIMEOUT	       "EV_DISCNTO"

//whether CnS command for radio power down should be sent 
#define INI_EVDO_SHUTDOWN_ON_CLOSE          "EV_SDN_CLS"

/*********************************************************************
 *	DIAL INI parameters
 *********************************************************************/
/* Start up string */
#define			INI_MODEM_START					"DL_START"

/* AT command string to dial */
#define			INI_MODEM_DIAL					"DL_DIAL"

/* Number of times in sending the start up string until an "OK" is returned*/
#define			INI_MODEM_INIT_RETRIES			"DL_INITRET"

/* Number of millisenconds to wait between sending AT commands */
#define			INI_MODEM_AT_DELAY				"DL_ATDELAY"

/* Init String */
#define			INI_MODEM_INIT_1				"DL_INIT_1"

/* Second init string. This is optional */
#define			INI_MODEM_INIT_2				"DL_INIT_2"

/* Third init string. This is optional */
#define			INI_MODEM_INIT_3				"DL_INIT_3"

/* Fourth init string. This is optional */
#define			INI_MODEM_INIT_4				"DL_INIT_4"

/* Fifth init string. This is optional */
#define			INI_MODEM_INIT_5				"DL_INIT_5"

/* Sixth init string. This is optional */
#define			INI_MODEM_INIT_6				"DL_INIT_6"

/* Seventh init string. This is optional */
#define			INI_MODEM_INIT_7				"DL_INIT_7"

/* Eigth init string. This is optional */
#define			INI_MODEM_INIT_8				"DL_INIT_8"

/* Ninth init string. This is optional */
#define			INI_MODEM_INIT_9				"DL_INIT_9"

/* Tenth init string. This is optional */
#define			INI_MODEM_INIT_10				"DL_INIT_10"

/* Regular expression for Verbal Responses */
#define			INI_VERBAL_RESPONSE_EXP			"DL_VER_EXP"

/* Regular expression for Numeric Responses */
#define			INI_NUMERIC_RESPONSE_EXP		"DL_NUM_EXP"

/* baudrate */
#define			INI_MODEM_BAUD					"DL_BAUD"

/* format */
#define			INI_MODEM_FORMAT				"DL_FORMAT"

/* Timeout to send the AT command */
#define			INI_MODEM_AT_SEND_TIMEOUT		"DL_ATSNDTO"

/* Hardware flow control */
#define			INI_MODEM_HW_FLOW_CONTROL		"DL_HW_FCTL"

/* Response timeout used for all init AT commands */
#define			INI_MODEM_INIT_TIMEOUT			"DL_INIT_TO"

/* Data connection timeout in milliseconds */
#define			INI_MODEM_CONNECT_TIMEOUT		"DL_CONN_TO"

/* Data disconnect timeout in milliseconds */
#define			INI_MODEM_DISCONNECT_TIMEOUT	"DL_DISC_TO"

/* Intecharacter timeout for all AT commands */
#define			INI_MODEM_INTERCHAR_TIMEOUT		"DL_ICHAR_TO"

/* Primary phone number to dial */
#define			INI_DIAL_PRIMARY				"DL_PHO_PRI"

/* Secondary phone number to dial */
#define			INI_DIAL_SECONDARY				"DL_PHO_SEC"

/* Set up to use SDLC protocol */
#define			INI_SDLC_PROTOCOL				"DL_SDLC_ON"

/* SDLC init string */
#define			INI_SDLC_INIT					"DL_SDLCINT"

/* AT Command string to set the modulation */
#define			INI_SDLC_MODULATION				"DL_SDLC_MOD"

/* Set up to use fast connect */
#define			INI_SDLC_FAST_CONNECT			"DL_FAST_CO"

/* Set Dial Type */
#define			INI_DIAL_TYPE					"DL_DIALTYPE"

/*********************************************************************
 *	WIFI INI parameters
 *********************************************************************/
/* wifi multi-cast */
#define INI_WI_ET_MULCAST "ET_MULCAST"

/*  wifi pm */
#define INI_WI_PM "WI_PM"

/* wifi roaming */
#define INI_WI_ROAM "WI_ROAM"

/* wifi beacon timeout */
#define INI_WI_BEACON_TM "WI_BEACON_TM"

/* wifi connection timeout */
#define INI_WI_CONN_TO "WI_CONN_TO"

/* wifi response timeout */
#define INI_WI_RESP_TO "WI_RESP_TO"

/* wifi minimun rssi */
#define INI_WI_MINRSSI "WI_MINRSSI"

/* wifi minimum rssi for roaming */
#define INI_WI_ROAM_MINRSSI "WI_ROAM_MINRSSI"

/* wifi roaming scan timeout */
#define INI_WI_ROAM_SCAN_TM "WI_ROAM_SCAN_TM"

/* wifi rssi status */
#define INI_WI_RSSI_NOTIFICATION "RSSI_STS"

/* wifi lowbatt ce-shared */
#define INI_WI_LOWBATT "LOWBATT"

/* wifi connection check timeout */
#define INI_WI_CONNCHK_TM "CONNCHK_TM"

/* wifi search count */
#define INI_WI_SEARCH_CNT "SEARCH_CNT"

/* wifi search result */
#define INI_WI_SEARCH_RESULT "SEARCH_RESULT"

/* wifi network count */
#define INI_WI_NW_CNT "NW_CNT"

/* wifi 802.11N enable */
#define INI_WI_80211N "WI_80211N"

/* wifi keep alive packet interval */
#define INI_WI_KEEP_ALIVE_TO "WI_KEEP_ALIVE_TO"

/* wifi Threshold from last value that RSSI event is generated */
#define INI_WI_RSSI_STS_THLD "WI_RSSI_STS_THLD"


/* wifi time before scan for roaming is performed after association */
#define INI_WI_ROAM_SCAN_PERIOD "WI_ROAM_SCAN_PERIOD" 

/* wifi renew IP on roaming */
#define INI_WI_ROAM_RENEW_IP "WI_ROAM_RENEW_IP"

/*********************************************************************
 *	Bluetooth INI parameters
 *********************************************************************/
/*Friendly name prefix of VFI base stations*/
#define			INI_VFI_BASE_PREFIX				"VFI_BASE_PREFIX"

/*Device class of VFI base stations*/
#define			INI_VFI_BASE_CLASS				"VFI_BASE_CLASS"

/*Filter the search to only VFI base stations*/
#define			INI_SRCH_VFI_BASE				"SRCH_VFI_BASE"

/*Maximum number of devices to search*/
#define			INI_SRCH_MAX					"SRCH_MAX"

/*Search timeout in milliseconds*/
#define			INI_SRCH_TIMEOUT				"SRCH_TIMEOUT"

/*Maximum number of paired devices*/
#define			INI_PAIR_MAX					"PAIR_MAX"

/*Connect timeout in milliseconds*/
#define			INI_CONN_TIMEOUT				"CONN_TIMEOUT"

/*Enable/disable encryption*/
#define			INI_BT_ENCRYPT					"BT_ENCRYPT" // This is for future use

/*Enable/disable authentication*/
#define			INI_BT_AUTH						"BT_AUTH"	 // This is for future use

/*Event timeout in milliseconds*/
#define			INI_BT_EVT_TIMEOUT				"BT_EVT_TIMEOUT"

/* Set Secure Simple Pairing */
#define			INI_BT_ENABLE_SSP				"BT_ENABLE_SSP"

/* Timer for Bluetooth reconnection when BT link goes out of range */
#define			INI_NO_NET_RET_TM				"NO_NET_RET_TM"

/* Enable/disable loading of modem profile from terminal */
#define			INI_LD_MDM_PROF					"LD_MDM_PROF"

/*********************************************************************
 *	HSPA INI parameters
 *********************************************************************/
/* Network selection. 0 is auto, 1 is 2G, 2 is 3G */
#define INI_HS_NETWORK							"HS_NETWORK"

/* Allow DDI startup without SIM */
#define INI_GPRS_ALLOW_NO_SIM					"GP_ALLOW_NO_SIM"
