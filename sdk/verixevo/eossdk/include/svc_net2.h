/* SVC_NET2.H - Verix Socket Services plus restricted functions */
/* Copyright 2009 by VeriFone, Inc.  All Rights Reserved. */

#ifndef _SVC_NET2_H
#define _SVC_NET2_H

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

/* Include all functions in svc_net.h plus functions that are restricted to
 * GID1 and 46.
 */

#include <svc_net.h>

#define NETMIB_INTERFACE    (0)
#define NETMIB_IP           (1)
#define NETMIB_ICMP         (2)
#define NETMIB_UDP          (3)
#define NETMIB_TCP          (4)

typedef struct tsIpMib
{
    int32_t        ipForwarding;
    int32_t        ipDefaultTTL;
    uint32_t       ipInReceives;
    uint32_t       ipInHdrErrors;
    uint32_t       ipInAddrErrors;
    uint32_t       ipForwDatagrams;
    uint32_t       ipInUnknownProtos;
    uint32_t       ipInDiscards;
    uint32_t       ipInDelivers;
    uint32_t       ipOutRequests;
    uint32_t       ipOutDiscards;
    uint32_t       ipOutNoRoutes;
    int32_t        ipReasmTimeout;
    uint32_t       ipReasmReqds;
    uint32_t       ipReasmOKs;
    uint32_t       ipReasmFails;
    uint32_t       ipFragOKs;
    uint32_t       ipFragFails;
    uint32_t       ipFragCreates;
    uint32_t       ipRoutingDiscards;
    int32_t        filler;
} ttIpMib;

typedef struct tsIcmpMib
{
    uint32_t       icmpInMsgs;
    uint32_t       icmpInErrors;
    uint32_t       icmpInDestUnreachs;
    uint32_t       icmpInTimeExcds;
    uint32_t       icmpInParmProbs;
    uint32_t       icmpInSrcQuenchs;
    uint32_t       icmpInRedirects;
    uint32_t       icmpInEchos;
    uint32_t       icmpInEchoReps;
    uint32_t       icmpInTimestamps;
    uint32_t       icmpInTimestampReps;
    uint32_t       icmpInAddrMasks;
    uint32_t       icmpInAddrMaskReps;
    uint32_t       icmpOutMsgs;
    uint32_t       icmpOutErrors;
    uint32_t       icmpOutDestUnreachs;
    uint32_t       icmpOutTimeExcds;
    uint32_t       icmpOutParmProbs;
    uint32_t       icmpOutSrcQuenchs;
    uint32_t       icmpOutRedirects;
    uint32_t       icmpOutEchos;
    uint32_t       icmpOutEchoReps;
    uint32_t       icmpOutTimestamps;
    uint32_t       icmpOutTimestampReps;
    uint32_t       icmpOutAddrMasks;
    uint32_t       icmpOutAddrMaskReps;
    char            valid[4];
} ttIcmpMib;

typedef struct ttTcpMib
{
    int32_t        tcpRtoAlgorithm;
    int32_t        tcpRtoMin;
    int32_t        tcpRtoMax;
    int32_t        tcpMaxConn;
    uint32_t       tcpActiveOpens;
    uint32_t       tcpPassiveOpens;
    uint32_t       tcpAttemptFails;
    uint32_t       tcpEstabResets;
    uint32_t       tcpCurrEstab;
    uint32_t       tcpRetransSegs;
    uint64_t       tcpHCInSegs;
    uint64_t       tcpHCOutSegs;
    uint32_t       tcpInErrs;
    uint32_t       tcpOutRsts;
    int32_t        filler;
} ttTcpMib;

typedef struct tsUdpMib
{
    uint32_t       udpNoPorts;
    uint32_t       udpInErrors;
    uint64_t       udpHCInDatagrams;
    uint64_t       udpHCOutDatagrams;
    int32_t        filler;
} ttUdpMib;

typedef struct tsIfStatMib
{
    uint64_t ipIfStatsHCInReceives;
    uint64_t ipIfStatsHCInOctets;
    uint64_t ipIfStatsHCOutTransmits;
    uint64_t ipIfStatsHCOutOctets;
    uint64_t ipIfStatsHCInMcastPkts;
    uint64_t ipIfStatsHCInMcastOctets;
    uint64_t ipIfStatsHCOutMcastPkts;
    uint64_t ipIfStatsHCOutMcastOctets;
    uint64_t ipIfStatsHCInBcastPkts;
    uint64_t ipIfStatsHCOutBcastPkts;
    uint32_t ipIfStatsInHdrErrors;
    uint32_t ipIfStatsInNoRoutes;
    uint32_t ipIfStatsInAddrErrors;
    uint32_t ipIfStatsInUnknownProtos;
    uint32_t ipIfStatsInTruncatedPkts;
    uint32_t ipIfStatsInForwDatagrams;
    uint32_t ipIfStatsReasmReqds;
    uint32_t ipIfStatsReasmOKs;
    uint32_t ipIfStatsReasmFails;
    uint32_t ipIfStatsInDiscards;
    uint32_t ipIfStatsInDelivers;
    uint32_t ipIfStatsOutRequests;
    uint32_t ipIfStatsOutNoRoutes;
    uint32_t ipIfStatsOutForwDatagrams;
    uint32_t ipIfStatsOutDiscards;
    uint32_t ipIfStatsOutFragReqds;
    uint32_t ipIfStatsOutFragOKs;
    uint32_t ipIfStatsOutFragFails;
    uint32_t ipIfStatsOutFragCreates;
} ttIfStatMib;

/* netstat API */
/* for the tableId parameter for tfNetstat */
typedef enum teNtTableId
{
    TM_NT_TABLE_ARP,
    TM_NT_TABLE_RTE,
    TM_NT_TABLE_TCP,
    TM_NT_TABLE_UDP,
    TM_NT_TABLE_DEVICE

} ttNtTableId;

/* 
 * TM_NT_MAX_IP_PER_IF
 * Max number of IP address per interface supported by netstat
 * which should be:
 *            TM_MAX_IPS_PER_IF
 */

#define TM_NT_MAX_IPS_PER_IF 4


#define TM_NT_MAX_IP_PER_IF TM_NT_MAX_IPS_PER_IF

#define TM_NT_ENTRY_STR_LEN     116 /* TCP round(114 + 1) */

/* max length of a physical addr */
#define TM_NT_MAX_DEVICE_NAME   14
#define TM_NT_MAX_PHYS_ADDR     6

/* routing entry flag bits */
#define TM_NT_RTE_UP         0x0001
#define TM_NT_RTE_HOST       0x0002
#define TM_NT_RTE_GW         0x0004
#define TM_NT_RTE_DYNAMIC    0x0008
#define TM_NT_RTE_REJECT     0x0010
#define TM_NT_RTE_REDIRECT   0x0020
#define TM_NT_RTE_CLONABLE   0x0040
#define TM_NT_RTE_CLONED     0x0080
#define TM_NT_RTE_STATIC     0x0100
#define TM_NT_RTE_LINK_LAYER 0x0200

/* HW types */
#define TM_NT_HWTYPE_ETHERNET   0
#define TM_NT_HWTYPE_TOKENRING  1
#define TM_NT_HWTYPE_TOKENBUS   2
#define TM_NT_HWTYPE_NULL       3

/* TCP state */
#define TM_NT_TCPS_CLOSED       0
#define TM_NT_TCPS_LISTEN       1
#define TM_NT_TCPS_SYN_SENT     2
#define TM_NT_TCPS_SYN_RECEIVED 3
#define TM_NT_TCPS_ESTABLISHED  4
#define TM_NT_TCPS_CLOSE_WAIT   5
#define TM_NT_TCPS_FIN_WAIT_1   6
#define TM_NT_TCPS_CLOSING      7
#define TM_NT_TCPS_LAST_ACK     8
#define TM_NT_TCPS_FIN_WAIT_2   9
#define TM_NT_TCPS_TIME_WAIT    10
#define TM_NT_TCPS_INVALID      20 /* invalid state */

/* information of a ARP entry */
typedef struct tsNtArpEntry
{
    struct sockaddr_storage ntArpSockAddr;
    int                     ntArpHwLength;
    int                     ntArpHwType;
    int                     filler1;
    uint32_t                ntArpOwnerCount;
    uint32_t                ntArpTtl;
    uint8_t                 ntArpHwAddress[TM_NT_MAX_PHYS_ADDR];
    uint8_t                 ntArpDeviceName[TM_NT_MAX_DEVICE_NAME];
    uint8_t                 ntArpNudState;
} ttNtArpEntry;
typedef ttNtArpEntry * ttNtArpEntryPtr;

/* information of a routing entry */
typedef struct tsNtRteEntry
{
    struct sockaddr_storage ntRteDestSockAddr;
    uint32_t                ntRteOwnerCount;
    uint32_t                ntRteMhomeIndex;
    int                     filler1;
    struct sockaddr_storage ntRteGwSockAddr;
    int                     ntRteHwLength;
    uint32_t                ntRteMtu;
    uint32_t                ntRteHops;
    uint32_t                ntRteTtl;
    uint16_t                ntRteFlags;
    uint8_t                 ntRtePrefixLength;
    char                    ntRteDeviceName[TM_NT_MAX_DEVICE_NAME];
    uint8_t                 ntRteHwAddress[TM_NT_MAX_PHYS_ADDR];
    uint8_t                 ntRteClonePrefixLength;
} ttNtRteEntry;
typedef ttNtRteEntry * ttNtRteEntryPtr;

/* information for a TCP socket entry */
typedef struct tsNtTcpEntry
{
    uint32_t                ntTcpSockDesc;
    uint32_t                ntTcpBytesInRecvQ;
    uint32_t                ntTcpBytesInSendQ;
    uint32_t                ntTcpRecvQSize;
    uint32_t                ntTcpSendQSize;
/*
 * V4 mapped address for a socket entry is stored as a V4 address
 * in ntTcpEntryPtr->ntTcpLocalSockAddr
 */
    struct sockaddr_storage ntTcpLocalSockAddr;
    uint32_t                ntTcpOwnerCount;
/*
 * V4 mapped address for a socket entry is stored as a V4 address
 * in ntTcpEntryPtr->ntTcpPeerSockAddr
 */
    struct sockaddr_storage ntTcpPeerSockAddr;
    uint16_t            ntTcpFlags;
    uint32_t            ntTcpRto;
    uint32_t            ntTcpReXmitCnt;
    uint32_t            ntTcpSndUna;
    uint32_t            ntTcpIss;
    uint32_t            ntTcpSndNxt;
    uint32_t            ntTcpMaxSndNxt;
    uint32_t            ntTcpIrs;
    uint32_t            ntTcpSndWL1;
    uint32_t            ntTcpSndWL2;
    uint32_t            ntTcpMaxSndWnd;
    uint32_t            ntTcpRcvNxt;
    uint32_t            ntTcpRcvWnd;
    uint32_t            ntTcpRcvAdv;
    uint32_t            ntTcpCwnd;
    uint32_t            ntTcpSsthresh;
    uint16_t            ntTcpBackLog;
    uint8_t             ntTcpDupAck;
    uint8_t             ntTcpAcksAfterRexmit;
    uint8_t             ntTcpState;
    uint8_t             ntTcpFiller[3];
} ttNtTcpEntry;
typedef ttNtTcpEntry * ttNtTcpEntryPtr;

/* information for a UDP socket entry */
typedef struct tsNtUdpEntry
{
    uint32_t            ntUdpSockDesc;
    uint32_t            ntUdpBytesInRecvQ;
    uint32_t            ntUdpBytesInSendQ;
    uint32_t            ntUdpRecvQSize;
    uint32_t            ntUdpSendQSize;
/*
 * V4 mapped address for a socket entry is stored as a V4 address
 * in ntUdpEntryPtr->ntTcpLocalSockAddr
 */
    struct sockaddr_storage ntUdpLocalSockAddr;
    uint16_t             ntUdpOwnerCount;
} ttNtUdpEntry;
typedef ttNtUdpEntry * ttNtUdpEntryPtr;

/* address configuration type */
#define TM_NT_ADDR_CONF_TYPE_NONE     0
#define TM_NT_ADDR_CONF_TYPE_MANUAL   1
#define TM_NT_ADDR_CONF_TYPE_BOOTP    2
#define TM_NT_ADDR_CONF_TYPE_DHCP     3
#define TM_NT_ADDR_CONF_TYPE_AUTO     4
#define TM_NT_ADDR_CONF_TYPE_DHCPV6   5
#define TM_NT_ADDR_CONF_TYPE_PROXY    6

/* IPV4 address configuration status */
#define TM_NT_ADDR_STATUS_UNKOWN      0
#define TM_NT_ADDR_STATUS_CONFIGURED  1
#define TM_NT_ADDR_STATUS_CONFIGURING 2
#define TM_NT_ADDR_STATUS_DEPRECATED  3

/* device status */
#define TM_NT_DEVICE_STATUS_CLOSED      0
#define TM_NT_DEVICE_STATUS_OPEN        1
#define TM_NT_DEVICE_STATUS_CONNECTING  2
#define TM_NT_DEVICE_STATUS_CONNECTED   3

/* information for a IPv4 address configuration entry */
typedef struct tsNtAddrEntry
{
    struct in_addr      ntAddrAddr;
    uint8_t          ntAddrPrefixLen;
    uint8_t          ntAddrStatus;
    uint8_t          ntAddrMHome;
    uint8_t          ntAddrType;
} ttNtAddrEntry;
typedef ttNtAddrEntry * ttNtAddrEntryPtr;

typedef struct tsNtDevEntry
{
    char            ntDevName[TM_NT_MAX_DEVICE_NAME];
    char            ntDevPhyAddr[TM_NT_MAX_PHYS_ADDR];
    uint8_t      ntDevPhyAddrLen;
    uint8_t      ntDevStatus;
    ttNtAddrEntry   ntDevAddrConfArray[TM_NT_MAX_IP_PER_IF];
} ttNtDevEntry;
typedef ttNtDevEntry * ttNtDevEntryPtr;

/* union of the netstat entries */
typedef union tuNtEntryU
{
    ttNtRteEntry        ntRteEntry;
    ttNtArpEntry        ntArpEntry;
    ttNtUdpEntry        ntUdpEntry;
    ttNtTcpEntry        ntTcpEntry;
    ttNtDevEntry        ntDevEntry;
} ttNtEntryU;
typedef ttNtEntryU * ttNtEntryUPtr;

/* Get the ARP table header as a string. */
char * tfNtGetArpHeaderStr(char *buffer, int  *sizePtr);

/* Get the routing table header as a string. */
char * tfNtGetRteHeaderStr(char *buffer, int  *sizePtr);

/* Get the TCP socket table header as a string. */
char * tfNtGetTcpHeaderStr(char *buffer, int  *sizePtr);

/* Get the UDP socket table header as a string. */
char * tfNtGetUdpHeaderStr(char *buffer, int  *sizePtr);

char * tfNtGetDeviceHeaderStr(char *buffer, int  *sizePtr);

char * tfNtGetAddrConfHeaderStr(char *buffer, int  *sizePtr);

/* print the ARP entry as s string to the user provided buffer*/
char * tfNtArpEntryToStr(ttNtArpEntryPtr ntArpEntryPtr, 
        char *buffer, int  *sizePtr);

/* print the route entry as s string to the user provided buffer*/
char * tfNtRteEntryToStr(ttNtRteEntryPtr ntRteEntryPtr,
        char *buffer, int  *sizePtr);

/* print the TCP entry as s string to the user provided buffer*/
char * tfNtTcpEntryToStr(ttNtTcpEntryPtr ntTcpEntryPtr,
    char *buffer, int  *sizePtr);

/* print the UDP entry as s string to the user provided buffer*/
char * tfNtUdpEntryToStr(ttNtUdpEntryPtr ntUdpEntryPtr,
    char *buffer, int  *sizePtr);

/* print the device entry as s string to the user provided buffer*/
char * tfNtDeviceEntryToStr(ttNtDevEntryPtr ntDevEntryPtr,
    char *buffer, int  *sizePtr);

/* print the IPv4 addr conf entry as s string to the user provided buffer */
char * tfNtAddrConfEntryToStr(ttNtAddrEntryPtr ntAddrEntryPtr,
    char *buffer, int  *sizePtr);


#ifndef __GNUC__
#define _SYS static
/* Network Interface API ************************************************* */
#define LL_ETHERNET (0)     // Link layer Ethernet
#define LL_PPP      (1)     // Link layer PPP
_SYS int net_addif(int devhdl, int task_id);
_SYS int net_delif(int inthdl);
_SYS int net_stopif(int inthdl);
_SYS int net_startif(int inthdl);
_SYS int openSockets(void);
_SYS int GetDhcpEvent(int inthdl);
_SYS unsigned long GetPppEvents(int inthdl);
_SYS int GetPppDnsIpAddress(int inthdl, ip_addr *dnsIpAddressPtr, int flag);
_SYS int GetPppPeerIpAddress(int inthdl, ip_addr *ifIpAddress);
_SYS int PppSetOption(int inthdl, int protocolLevel, int remoteLocalFlag,
        int optionName, const char *optionValuePtr, int optionLength);
_SYS int SetPppPeerIpAddress(int inthdl, ip_addr ifIpAddress);
_SYS int AddInterface(int inthdl, int linktype, long event);
_SYS int UseDhcp(int inthdl);
_SYS int UseBootp(int inthdl);
_SYS int OpenInterface(int inthdl, ip_addr myaddr, ip_addr mynetmask);
_SYS int CloseInterface(int inthdl);
_SYS int GetBroadcastAddress(int inthdl, ip_addr *bcast);
_SYS int GetIpAddress(int inthdl, ip_addr *ipaddr);
_SYS int GetNetMask(int inthdl, ip_addr *mask);
_SYS int SetIfMtu(int inthdl, int mtu);
_SYS int InterfaceSetOptions(int inthdl, int optName, void *optVal, int optLen);
_SYS int ConfGetBootEntry(int inthdl, bootEntry *be);
_SYS int AddArpEntry(ip_addr arpIpAddress, char *physAddrPtr, int physAddrLength);
_SYS int AddDefaultGateway(int handle, ip_addr gatewayIpAddress);
_SYS int AddProxyArpEntry(ip_addr arpIpAddress);
_SYS int AddStaticRoute(int handle, ip_addr destIpAddress, ip_addr destNetMask, ip_addr gateway, int hops);
_SYS int DelArpEntryByIpAddr(ip_addr arpIpAddress);
_SYS int DelArpEntryByPhysAddr(char *physAddrPtr, int physAddrLength);
_SYS int DelDefaultGateway(ip_addr gatewayIpAddress);
_SYS int DelProxyArpEntry(ip_addr arpIpAddress);
_SYS int DelStaticRoute(ip_addr destIpAddress, ip_addr destNetMask);
_SYS int DisablePathMtuDisc(ip_addr destIpAddress, unsigned short pathMtu);
_SYS int GetArpEntryByIpAddr(ip_addr arpIpAddress, char *physAddrPtr, int physAddrLength);
_SYS int GetArpEntryByPhysAddr(char *physAddrPtr, int physAddrLen, ip_addr *arpIpAddressPtr);
_SYS int ArpFlush(int addr_family);
_SYS int GetDefaultGateway(ip_addr *gwayIpAddPtr);
_SYS int RtDestExists(ip_addr destIpAddress, ip_addr destNetMask);
_SYS int NetStat(int tableId, void *buff, size_t buffLen);
_SYS int GetSnmpMib(int mibtype, void *mibstruct, size_t structlen);
_SYS int SetDhcpStartupDelayWindow(int inthdl,int windowSize); // 0 to 10 seconds
#endif //__GNUC__

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif  /* _SVC_NET2_H */
