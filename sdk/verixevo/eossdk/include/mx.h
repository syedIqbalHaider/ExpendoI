//----------------------------------------------------------------------
// Copyright (C) 2009 by VeriFone Inc. All rights reserved.
//
// No part of this software may be used, stored, compiled, reproduced,
// modified, transcribed, translated, transmitted, or transferred, in
// any form or by any means  whether electronic, mechanical,
// magnetic, optical, or otherwise, without the express prior written
// permission of VeriFone, Inc.
//----------------------------------------------------------------------

#ifndef __MX_H
#define __MX_H

#ifdef __cplusplus
extern "C" {
#endif

// -----------------------------------------------------------
// Pipe Message format. The first 16 bytes pipe header 
// the remaining n bytes is application payload.
// Note: This implementation supports pipe_init_msgX()
// 
// +------------+----------------------------------+
// |  header    |             data bytes           |
// |  16 byte   |<--------- Max 4096 bytes ------->|
// +------------+----------------------------------+
// |pipe_exten  |              PayLoad             |
// |sion_t      |                Data              |
// +------------+----------------------------------+
// |<--16byte-->|<------------ n bytes ----------->|
// +------------+----------------------------------+

#define EMX_SUCCESS 0

#define MX_MAX_DATA_SIZE            4096
#define MX_MIN_PIPENAME_SIZE        2   // Min len of P: (Anon pipe)
#define MX_MAX_PIPENAME_SIZE        (8+MX_MIN_PIPENAME_SIZE)
#define MX_MAX_MSGS                 10
#define MX_MIN_MSGS                 0
#define MX_ANONYMOUS_PIPE           "P:"

// MX API Error Codes
#define EMX_UNKNOWN         -2000 // Undetermined error
#define EMX_PIPE_NAME       -2001 // Pipe name too long. May not exceed 8 characters.
                                  // Pipe name has invalid non-ASCII characters.
#define EMX_MSG_DEPTH       -2002 // The message depth parameter should be in the range 0 to
                                  // 10. Any value outside this range will result in this error.
#define EMX_PIPE_NOMEM      -2003 // No memory to create pipe.
#define EMX_PIPE_MATCH      -2004 // Pipe name does not match currently open named pipe(s)
#define EMX_PIPE_HANDLE     -2005 // Pipe handle not valid.
#define EMX_PAYLOAD_SIZE    -2006 // Payload size should positive non-zero value and less than limit
#define EMX_NULL_PARAM      -2007 // Input parameter is unexpected null
#define EMX_DEST_PIPE       -2008 // Target pipe for mxSend() does not exist or not configured for messages
#define EMX_PIPE_FULL       -2009 // The destination pipe is full and the message cannot be sent

// Creates a VerixV message pipe and returns handle to pipe
// Input: pipeName -- up to 8char, can be NULL or "" for anonymous pipe
//        messageDepth -- max number of incoming messages bufferred
// Returns: Pipe Handle. Returns < 0 in case of error
int mxCreatePipe(const char pipeName[], const int messageDepth);


// Close VerixV message pipe created via mxCreatePipe
// Input: Pipe Handle -- returned by mxCreatePipe
// Returns: 0
int mxClosePipe(const int pipeHandle);


// Obtains pipe handle associated with pipeName
// Input: Target Pipe Name -- up to 8 char. Cannot be NULL
// Returns: Pipe handle associated with pipe created with name targetPipeName
//          < 0 in case of error.
int mxGetPipeHandle(const char targetPipeName[]);


// Sends data message to destination pipe
// Input: pHSrc -- Source Pipe Handle. Created using mxCreatePipe()
//        pHDest -- Destination Pipe Handle.
//        dataPayload -- Data to send to destination pipe
//        dataLength -- Buffer size. Max 4096bytes
// Return: >=0 Send successful
//         < 0 Error code returned
int mxSend(const int pHSrc, const int pHDest, const void *dataPayload, const unsigned int dataLength);

// Determines number of messages in the queue that are pending read
// Input: pH -- Pipe Handle
// Returns: >=0, number of pending messages
//          < 0 in case of error
int mxPending(const int pH);


// Reads pending message from queue
// Input: pH -- Pipe Handle. Created using mxCreatePipe()
//        dataPayloadSize -- size of dataPayload buffer
// Output:pHFrom -- Pipe Handle of sender
//        dataPayloadLen -- payload data from sender
// Returns: >0 -- Message read the number of bytes in payload returned
//           0 -- No data in message (length = 0)
//          <0 -- Read error or no pending message and errno is set
int mxRecv(const int pH, int *pHFrom, void *dataPayload, 
           const unsigned int dataPayloadSize, 
           unsigned int *dataPayloadLen);

// Reads pending message from queue and provides extended information
// Input: pH -- Pipe Handle. Created using mxCreatePipe()
//        dataPayloadSize -- size of dataPayload buffer
// Output:pipe_extension_t -- Extended pipe msg structure
//        dataPayloadLen -- payload data from sender
// Returns: >0 -- Message read the number of bytes in payload returned
//           0 -- No data in message (length = 0)
//          <0 -- Read error or no pending message and errno is set
int mxRecvEx(const int pH, pipe_extension_t *extPI, void *dataPayload, 
           const unsigned int dataPayloadSize, 
           unsigned int *dataPayloadLen);

#ifdef __cplusplus
}
#endif

#endif // __MX_H
