#ifndef __VERIXVHEADERS_H
#define __VERIXVHEADERS_H
//#include <algo.h>
//#include <alloc.h>
#include <assert.h>
//#include <bvector.h>
#include <ctype.h>
//#include <defalloc.h>

//#include <deque.h>
#include <errno.h>
#include <float.h>
//#include <function.h>
//#include <hash_map.h>
//#include <hash_set.h>
//#include <hashtable.h>
//#include <heap.h>
//#include <iterator.h>
//#include <LibCtls.h>
//#include <libvoy.h>

#include <limits.h>
//#include <list.h>
#include <locale.h>
//#include <map.h>
#include <math.h>
//#include <multimap.h>
//#include <multiset.h>
//#include <pair.h>
//#include <pthread_alloc.h>
//#include <rope.h>
//#include <ropeimpl.h>

//#include <sdkver.h>
//#include <sequence_concepts.h>
//#include <set.h>
#include <setjmp.h>
//#include <slist.h>
//#include <stack.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <svc.h>
#include <svc_sec.h>
#include <svctxo.h>
#include <time.h>
//#include <tree.h>
//#include <type_traits.h>

#define false 0
#define true  1

#ifndef __cplusplus
typedef int bool;
#endif
typedef int sig_atomic_t;
typedef int ssize_t;
typedef unsigned int	speed_t;
typedef unsigned int	uint;

#endif //__VERIXVHEADERS_H

