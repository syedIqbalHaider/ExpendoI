#ifndef __EOSHEADERS_H
#define __EOSHEADERS_H

#include <SVC_NET.h>
#include <svc_net2.h>
//#include <svc_nwi.h>
#include "eoslog.h"


#define GPS_LAST_LOCATION_FILE				"GPSLastLoc.vlr"
#define GPS_LAST_LOCATION_FILE_HDR_LEN      11


#define AF_UNSPEC AF_INET
#define AF_UNIX AF_INET
typedef uint16_t in_port_t;
typedef unsigned short int sa_family_t;
#ifndef INADDR_LOOPBACK
#define       INADDR_LOOPBACK         (uint32_t)0x7F000001
#endif

#endif //__EOSHEADERS_H
