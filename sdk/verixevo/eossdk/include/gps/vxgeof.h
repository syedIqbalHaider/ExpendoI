#ifndef VXGEOF_H
#define VXGEOF_H

//==============================================================================
// DEFINES
//==============================================================================

// Debug levels
#define GEOF_DBG_INFO   0x00001000
#define GEOF_DBG_TRACE  0x00002000
#define GEOF_DBG_ERROR  0x0000F000

// geof_check return codes
#define GEOF_E_CHK_FOUND               (1)
#define GEOF_E_CHK_NOTFOUND            (0)

// function return codes
#define GEOF_E_NOERR                   (0)
#define GEOF_E_PARAM                   (-2)
#define GEOF_E_NOMEM                   (-3)
#define GEOF_E_NOTFOUND                (-4)
#define GEOF_E_FILEOPEN                (-5)
#define GEOF_E_FILEDATA                (-6)
#define GEOF_E_MAXPOINTS               (-7)

#define GEOF_POLYPT_MAX                12
#define GEOF_NAME_MAXSZ                12

//==============================================================================
// STRUCTURES
//==============================================================================

// Geo-fence private date
typedef struct _geofINFO {
	int   init_code;
	void *p_priv_data;
} geofINFO;

// Circular geo-fence information
typedef struct _geof_circle {
	double radius;   // Radius
	double lat;   // Latitude
	double lon;   // Longitude
} geof_circle;

// Polygon geo-fence information
typedef struct _geof_poly {
	int num_points;   // Number of points/vertices. Range is 3-12
	double lat[GEOF_POLYPT_MAX];   // Latitude points
	double lon[GEOF_POLYPT_MAX];   // Longitude points
} geof_poly;

typedef struct _geof_cfg {
	char name[GEOF_NAME_MAXSZ]; // Geo-fence name
	int  method;   // Geo-fence metho. Valid values:
	               //   1: circular geo-fence
	               //   2: polygon geo-fence
	union {
		geof_circle circle; // Radius and center points for a circular geo-fence
		geof_poly poly;     // Polygon points for a polygon geo-fence
	} geof_info;
} geof_cfg;

//==============================================================================
// PROTOTYPES
//==============================================================================

#ifdef __cplusplus
extern "C" {
#endif

int geof_version(char *buf);
int geof_open(geofINFO *info);
int geof_destroy(geofINFO *info);
int geof_set(geofINFO *info, geof_cfg *cfg);
//int geof_get(geofINFO *info, geof_cfg *cfg);
int geof_get_count(geofINFO *info);
int geof_get_by_index(geofINFO *info, int index, geof_cfg *cfg);
int geof_get_by_name(geofINFO *info, char *name, geof_cfg *cfg);
int geof_del(geofINFO *info, char *name);
int geof_del_all(geofINFO *info);
int geof_set_from_file(geofINFO *info, char *file);
int geof_save_to_file(geofINFO *info, char *file);
int geof_check(geofINFO *info, double lat, double lon, char *name);

#ifdef __cplusplus
}
#endif

//==============================================================================
// 
//==============================================================================

#endif // VXGEOF_H
