/* SVC_NET.H - Verix Socket Services */
/* Copyright 2009-2011 by VeriFone, Inc.  All Rights Reserved. */

#ifndef _SVC_NET_H
#define _SVC_NET_H

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

typedef unsigned long ip_addr;
typedef unsigned long in_addr_t;

typedef unsigned int socklen_t;

#ifndef  _INT_T_DEFINED
#define  _INT_T_DEFINED
typedef unsigned int size_t;

typedef unsigned char   uint8_t;
typedef unsigned short  uint16_t;
typedef unsigned int    uint32_t;
typedef unsigned long long uint64_t;
typedef signed char     int8_t;
typedef short           int16_t;
typedef int             int32_t;
typedef long long       int64_t;
#endif


/* Convert IP address in decimal 4-tuple form to 32-bit binary
 * form in network order
 */
#define dotted2binary(w,x,y,z) ((((ip_addr)w)<<24)+((x)<<16)+((y)<<8)+(z))

/*
 * Socket address structure used by kernel to store generic addresses.
 * Not specific to IP.
 */
struct sockaddr
{
    uint8_t  sa_len;        /* length of struct sockaddr */
    uint8_t  sa_family;     /* address family */
#ifdef IP_V6   /* V6 only or dual stacks */
    char     sa_data[32];      /* big enough for unpacked sockaddr_in6 */
#else          /* Ancient IPv4 only version */
    char     sa_data[14];      /* up to 14 bytes of direct address */
#endif
};

/*
 * IPv4 address
 */
struct in_addr
{
   in_addr_t s_addr;
};

/*
 * IPv4 address structure.
 */
struct sockaddr_in
{
    uint8_t         sin_len;    /* length of struct sockaddr_in */
    uint8_t         sin_family; /* AF_INET */
    uint16_t        sin_port;   /* 16bit Port Number in network byte order */
    struct  in_addr sin_addr;   /* 32bit netid/hostid in network byte order */
    char            sin_zero[8];/* unused */
};

struct sockaddr_storage
{
    union
    {
        struct sockaddr_in      ipv4;
    } addr;
};

#define  INADDR_ANY     0L
#define  INADDR_NONE    (0xFFFFFFFFUL)

/*
 * Protocol family constants for socket()
 */

#define AF_INET        2     /* internetwork: UDP, TCP, etc. */
#define AF_INET6       3
#define PF_INET        AF_INET

/* Currently always big-endian */
#define      htonl(x) (x)
#define      ntohl(x) (x)
#define      htons(x) (x)
#define      ntohs(x) (x)

/*
 * Socket types for socket()
 */
#define SOCK_STREAM    1     /* stream socket */
#define SOCK_DGRAM     2     /* datagram socket */
#define SOCK_RAW       3     /* raw-protocol interface */

/*
 * Socket protocol for socket()
 */
/*
 * Protocol Numbers
 * (OLD BSD WAY)
 */
#define IP_PROTOIP        0
#define IP_PROTOICMP      1
#define IP_PROTOIGMP      2
#define IP_PROTOIPV4      4 /* IP-in-IP encapsulation */
#define IP_PROTOTCP       6
#define IP_PROTOUDP      17
#define IP_PROTORDP      46
#define IP_PROTOOSPF     89
#define IP_PROTOTPACKET 127

/*
 * Protocol Numbers
 * (New BSD way)
 */
#define IPPROTO_IP      IP_PROTOIP
#define IPPROTO_ICMP    IP_PROTOICMP
#define IPPROTO_IGMP    IP_PROTOIGMP
#define IPPROTO_IPV4    IP_PROTOIPV4 /* IP-in-IP encapsulation */
#define IPPROTO_TCP     IP_PROTOTCP
#define IPPROTO_UDP     IP_PROTOUDP
#define IPPROTO_RDP     IP_PROTORDP
#define IPPROTO_OSPF    IP_PROTOOSPF
#define IPPROTO_TPACKET IP_PROTOTPACKET

/* IPv6 protocol IDs, refer to [RFC2292]. */
#define IPPROTO_HOPOPTS     0
#define IPPROTO_IPV6        41
#define IPPROTO_ROUTING     43
#define IPPROTO_FRAGMENT    44
#define IPPROTO_ESP         50
#define IPPROTO_AH          51
#define IPPROTO_ICMPV6      58
#define IPPROTO_NONE        59
#define IPPROTO_DSTOPTS     60

#define SOL_SOCKET      0xFFFF    /* compatability parm for set/get sockopt */

#define MSG_OOB               0x0001  /* process out-of-band data */
#define MSG_PEEK              0x0002  /* peek at incoming message */
#define MSG_DONTROUTE         0x0004  /* send without using routing tables */
#define MSG_EOR               0x0008  /* data completes record */
#define MSG_TRUNC             0x0010  /* data discarded before delivery */
#define MSG_CTRUNC            0x0020  /* control data lost before delivery */
#define MSG_WAITALL           0x0040  /* wait for full request or error */
#define MSG_DONTWAIT          0x0080  /* this message should be nonblocking */
#define MSG_EOF               0x0100  /* data completes connection */
#define MSG_BROADCAST         0x0200  /* Broadcast Packet */
/* User send data or non TCP recv data is scattered: */
#define MSG_SCATTERED         0x0400
#define MSG_IPHDRINCL         0x0800  /* IP header included in data */

#define MSG_MASK              0x0fff  /* Upper Nibble is reserved */

/*
 * Option flags per-socket.
 */
#define     SO_DEBUG        0x0001      /* turn on debugging info recording */
#define     SO_ACCEPTCONN   0x0002      /* socket has had listen() */
#define     SO_REUSEADDR    0x0004      /* allow local address reuse */
#define     SO_KEEPALIVE    0x0008      /* keep connections alive */
#define     SO_DONTROUTE    0x0010      /* just use interface addresses */
#define     SO_BROADCAST    0x0020      /* permit sending of broadcast msgs */
#define     SO_USELOOPBACK  0x0040      /* bypass hardware when possible */
#define     SO_LINGER       0x0080      /* linger on close if data present */
#define     SO_OOBINLINE    0x0100      /* leave received OOB data in line */
#define     SO_REUSEPORT    0x0200      /* allow local address & port reuse */
#define     SO_TIMESTAMP    0x0400      /* timestamp received dgram traffic */
#define     SO_IPHDRINCL    0x0800      /* IP Header included on the socket */

/*
 * Additional options, not kept in so_options.
 */
#define  SO_SNDBUF      0x1001      /* send buffer size */
#define  SO_RCVBUF      0x1002      /* receive buffer size */
#define  SO_SNDLOWAT    0x1003      /* send low-water mark */
#define  SO_RCVLOWAT    0x1004      /* receive low-water mark */
#define  SO_SNDTIMEO    0x1005      /* send timeout */
#define  SO_RCVTIMEO    0x1006      /* receive timeout */
#define  SO_ERROR       0x1007      /* get error status and clear */
#define  SO_TYPE        0x1008      /* get socket type */
#define  SO_HOPCNT      0x1009      /* Hop count to get to dst   */
#define  SO_MAXMSG      0x1010      /* get TCP_MSS (max segment size) */

/* socket ioctl requests */
/* set/clear nonblocking I/O */
#define FIONBIO             (0x01)
/* Get #bytes to read */
#define FIONREAD            (0x02)
/* At out-of-band mark? */
#define SIOCATMARK          (0x03)

/*
 * Structure used for manipulating linger option.
 */
struct   linger
{
   int   l_onoff;    /* option on/off */
   int   l_linger;   /* linger time */
};


/* Options for the shutdown() howto parameter.
 *
 */
#define SHUT_RD         (0)     /* Close the read half of the socket */
#define SHUT_WR         (1)     /* Close the write half of the socket */
#define SHUT_RDWR       (2)     /* Close both read and write */

#ifndef TRUE
#define TRUE            (1)
#define FALSE           (0)
#endif

#define MAXDNSNAME      (256)
#define MAXDNSADDRS     (10)

struct hostent
{
   char *   h_name;        /* Official name of host. */
   char **h_aliases;       /* Alias list.  */
   int   h_addrtype;       /* Host address type.  */
   int   h_length;         /* Length of address.  */
   char **h_addr_list;     /* List of addresses from name server.  */
#define  h_addr   h_addr_list[0] /* Address, for backward compatibility.  */
};

#define  FD_SETSIZE     32
#define NBBY 8 /* Number of bits in a byte */

typedef unsigned long ttUserS32Bit;
typedef ttUserS32Bit   fd_mask;
#define NFDBITS (sizeof(fd_mask) * NBBY)    /* bits per mask */

#ifndef howmany
#define howmany(x, y)   (((x) + ((y) - 1)) / (y))
#endif

typedef struct fd_set
{
    fd_mask fds_bits[howmany(FD_SETSIZE, NFDBITS)];
} fd_set;

struct timeval
{
    unsigned long tv_sec;    /* Number of Seconds */
    unsigned long tv_usec;   /* Number of micro seconds */
};

/* Ping API *************************************************************** */

typedef struct PingInfo
{
    /* Peer socket address */
    struct sockaddr_storage      pgiPeerSockAddr;
    /* Last round trip time computed */
    unsigned long                pgiLastRtt;
    /* maximum round trip time */
    unsigned long                pgiMaxRtt;
    /* minimum round trip time */
    unsigned long                pgiMinRtt;
    /* average round trip time */
    unsigned long                pgiAvgRtt;
    /* sum of all round trip time received so far (to compute average) */
    unsigned long                pgiSumRtt;
    /* number of ping transmitted packets */
    unsigned long                pgiTransmitted;
    /*
     * number of received echo replies packets (not including duplicated
     * packets)
     */
    unsigned long                pgiReceived;
    /* Number of duplicated ping echo replies packets */
    unsigned long                pgiDuplicated;
    /* Send Error code if any that occured */
    int                          pgiSendErrorCode;
    /* Recv Error code if any that occured */
    int                          pgiRecvErrorCode;
} PingInfo;

#define socketread(sockfd,buf,buflen)   recv(sockfd,buf,buflen,0)
#define socketwrite(sockfd,buf,buflen)  send(sockfd,buf,buflen,0)

/*
 * Network errors.
 */
/*
 * non-blocking and interrupt i/o
 */
#define TM_ERR_BASE      200

/* Operation would block */
#define EWOULDBLOCK     (TM_ERR_BASE+35)
/* Resource temporarily unavailable */
/* #define EAGAIN          TM_EWOULDBLOCK */
/* Operation now in progress */
#define EINPROGRESS     (TM_ERR_BASE+36)
/* Operation already in progress*/
#define EALREADY        (TM_ERR_BASE+37)

/* Socket operation on non-socket */
#define ENOTSOCK        (TM_ERR_BASE+38)
/* Destination address required */
#define EDESTADDRREQ    (TM_ERR_BASE+39)
/* Message too long */
#define EMSGSIZE        (TM_ERR_BASE+40)
/* Protocol wrong type for socket */
#define EPROTOTYPE      (TM_ERR_BASE+41)
/* Protocol not available */
#define ENOPROTOOPT     (TM_ERR_BASE+42)
/* Protocol not supported */
#define EPROTONOSUPPORT (TM_ERR_BASE+43)
/* Socket type not supported */
#define ESOCKTNOSUPPORT (TM_ERR_BASE+44)
/* Operation not supported */
#define EOPNOTSUPP      (TM_ERR_BASE+45)
/* Protocol family not supported */
#define EPFNOSUPPORT    (TM_ERR_BASE+46)
/* Address family not supported by protocol family */
#define EAFNOSUPPORT    (TM_ERR_BASE+47)
/* Address already in use */
#define EADDRINUSE      (TM_ERR_BASE+48)
/* Can't assign requested address */
#define EADDRNOTAVAIL   (TM_ERR_BASE+49)

/* Network is down */
#define ENETDOWN        (TM_ERR_BASE+50)
/* Network is unreachable */
#define ENETUNREACH     (TM_ERR_BASE+51)
/* Network dropped connection on reset */
#define ENETRESET       (TM_ERR_BASE+52)
/* Software caused connection abort */
#define ECONNABORTED    (TM_ERR_BASE+53)
/* Connection reset by peer */
#define ECONNRESET      (TM_ERR_BASE+54)
/* No buffer space available */
#define ENOBUFS         (TM_ERR_BASE+55)
/* Socket is already connected */
#define EISCONN         (TM_ERR_BASE+56)
/* Socket is not connected */
#define ENOTCONN        (TM_ERR_BASE+57)
/* Can't send after socket shutdown */
#define ESHUTDOWN       (TM_ERR_BASE+58)
/* Too many references: can't splice */
#define ETOOMANYREFS    (TM_ERR_BASE+59)
/* Operation timed out */
#define ETIMEDOUT       (TM_ERR_BASE+60)
/* Connection refused */
#define ECONNREFUSED    (TM_ERR_BASE+61)
/* Host is down */
#define EHOSTDOWN       (TM_ERR_BASE+64)
/* No route to host */
#define EHOSTUNREACH    (TM_ERR_BASE+65)

#define TM_ERR_BASE_LAST   EHOSTUNREACH

/* Error extensions to handle ICMP errors */
#define TM_EXT_ERROR_BASE   300
/* ICMP type unreach with code >= 13 */
#define EXT_EHOSTUNREACH (TM_EXT_ERROR_BASE+1)
/* ICMP source quench */
#define EXT_SRCQNCH      (TM_EXT_ERROR_BASE+2)
/* ICMP time xceeded in transit */
#define EXT_TTLTRANSIT   (TM_EXT_ERROR_BASE+3)
/* ICMP time xceeded in reassemly */
#define EXT_TTLREASS     (TM_EXT_ERROR_BASE+4)

/* Additional error codes for FTP application program interface */

/* Command requires user to be loggedin, and user is not. */
#define ENOTLOGIN        (TM_EXT_ERROR_BASE+5)
/* Temporary server error. Re-sending the same command could succeed */
#define ESERVERTEMP      (TM_EXT_ERROR_BASE+6)
/* Permanent server error. Re-sending the same command will always fail */
#define ESERVERPERM      (TM_EXT_ERROR_BASE+7)
/* server replies are out of SYNC. FTP user should call tfFtpQuit() */
#define ESERVERSYNC      (TM_EXT_ERROR_BASE+8)

/* Socket error code used to report ICMPv6 errors of unknown type.
   ([RFC2463].R2.4:10) */
#define EXT_UNKNOWN_ERR_TYPE (TM_EXT_ERROR_BASE+9)

/* POSIX generic protocol error, not yet part of BSD */
#define EPROTO           (TM_EXT_ERROR_BASE+10)

#define TM_EXT_ERROR_LAST   EPROTO

/* Error extensions to handle IPv6 errors. */
#define TM_IPV6_ERROR_BASE      400
/* IPv6 functionality currently not implemented. */
#define TM_6_ENOTIMPLEMENTED    (TM_IPV6_ERROR_BASE+1)

#define VX_EXT_ERROR_BASE       (700)
/* Remote peer has closed the socket */
#define EREMCLOSE               (VX_EXT_ERROR_BASE+1)
/* Remote peer sent a RESET */
#define EREMRESET               (VX_EXT_ERROR_BASE+2)
/* An error has occurred on the socket */
#define ESCKERROR               (VX_EXT_ERROR_BASE+3)
/* Network interface disabled */
#define ENOINT                  (VX_EXT_ERROR_BASE+4)

/*
 * Treck Extended
 */
/* Turn ON/OFF Selective Acknolwlegements (default off) */
#define TCP_SEL_ACK         0x0040
/* Turn ON/OFF Big Window Scaling (default off) */
#define TCP_WND_SCALE       0x0080
/* Turn ON/OFF Time stamp (default off) */
#define TCP_TS              0x0100
/* Turn ON/OFF Slow Start (defaul on) */
#define TCP_SLOW_START      0x0200
/*
 * Set PUSH on every packet even when not emptying the send queue! Use
 * sparingly. TCP_NODELAY option is preferable to this option.
 * OL 8/27/01 change: Also preserve packet boundaries when sending data.
 *                    (See TCP_PACKET flag below.)
 */
#define TCP_PUSH_ALL        0x0400
/*
 * Preserve user packet boundary when sending data:
 * . Set PUSH on every packet even when not emptying the send queue.
 *   (TCP_PUSH_ALL)
 * . Preserve packet boundaries even on retransmissions.
 */
#define TCP_PACKET          0x0400
/* 0x0800, and beyond are reserved */

/* Set Delay Ack time (default 200ms) */
#define TCP_DELAY_ACK       0x4002
/* Set Maximum number of retransmissions (default 12) */
#define TCP_MAX_REXMIT      0x4003
/* Set keep alive interval probes (default 75 seconds) */
#define TCP_KEEPALIVE_INTV  0x4004
/* Set keep alive probe count (default 8). */
#define TCP_KEEPALIVE_CNT   0x4005
/*
 * Set amount of time to stay in FIN WAIT 2 when socket has been closed
 * (default 600 seconds).
 */
#define TCP_FINWT2TIME      0x4006
/*
 * set TIME WAIT time
 * (Default 2*Maximum segment life time (2*30 seconds, or 60 seconds))
 */
#define TCP_2MSLTIME        0x4007
/*
 * Set default retransmission timeout value in milliseconds
 * (default 3,000 milliseconds)
 */
#define TCP_RTO_DEF         0x4008
/*
 * Set minimum retransmission timeout value in milliseconds
 * (default 100 milliseconds)
 */
#define TCP_RTO_MIN         0x4009
/*
 * Set maximum retransmission timeout value in milliseconds
 * (default 64,000 milliseconds)
 */
#define TCP_RTO_MAX         0x400A
/*
 * Set minimum zero window probe timeout value in milliseconds
 * (default 500 milliseconds)
 */
#define TCP_PROBE_MIN       0x400B
/*
 * Set maximum zero window probe timeout value in milliseconds
 * (default 60,000 milliseconds)
 */
#define TCP_PROBE_MAX       0x400C

/*
 * Set the window size of the listening socket to be different than
 * specified by SO_RCVBUF flag. This will allow listening socket's
 * window size to be different than the active socket spawned when
 * accept() is called.
*/
#define TCP_PEND_ACCEPT_RECV_WND 0x400D

/* Get the effective maximum segment size */
#define TCP_EFF_MAXSEG           0x400E

/* Enable control over TCP retransmission timer */
#define TCP_REXMIT_CONTROL       0x400F

/*
 * Interface options flags.
 */
/* Options not allowed on a Pt2Pt connection requiring byte stuffing */
#define DEV_OFLAG_NOT_BYTE_STUFF 0x0100
/* Option length is a short */
#define DEV_OFLAG_SHORT          0x0200
/* Option value should be >= 0 */
#define DEV_OFLAG_ABSOLUTE       0x0400
/* Option only allowed when device is not opened (or configured) */
#define DEV_OFLAG_NOT_OPEN       0x0800
/* Option length is an unsigned character */
#define DEV_OFLAG_UCHAR          0x1000
/* Option length is an int */
#define DEV_OFLAG_INT            0x2000
/* Option length is variable */
#define DEV_OFLAG_STRING         0x4000

/*
 * Interface options macros
 */
/*
 * Copy received driver buffers whose length is smaller than a given
 * user threshold. This option does not need to be used on a point to point
 * device that supports byte stuffing, because buffer is copied anyways in
 * the link layer recv function.
 */
#define DEV_OPTIONS_RECV_COPY  (   0x01                   \
                                    | DEV_OFLAG_SHORT     \
                                    | DEV_OFLAG_ABSOLUTE  \
                                    | DEV_OFLAG_NOT_BYTE_STUFF )
/*
 * Turn on/off a transmit task for flow control between the TCP/IP stack and
 * a device driver. Can only be changed when device is not opened
 * (i.e not configured). Cannot be set if the transmit buffer queue is used.
 */
#define DEV_OPTIONS_XMIT_TASK  (   0x02                   \
                                    | DEV_OFLAG_SHORT     \
                                    | DEV_OFLAG_ABSOLUTE  \
                                    | DEV_OFLAG_NOT_OPEN )

/*
 * Set the hardware transmit buffer alignement on that interface.
 * Default is TM_ETHER_HW_TX_ALIGN (2 bytes).
 */
#define DEV_OPTIONS_TX_ALIGN   (   0x03                   \
                                    | DEV_OFLAG_SHORT      \
                                    | DEV_OFLAG_ABSOLUTE   \
                                    | DEV_OFLAG_NOT_OPEN )

/* Disable gratuitous ARP's */
#define DEV_OPTIONS_NO_GRAT_ARP  (   0x04                   \
                                      | DEV_OFLAG_UCHAR     \
                                      | DEV_OFLAG_ABSOLUTE  \
                                      | DEV_OFLAG_NOT_OPEN )

/* BOOTP/DHCP option: Number of retries */
#define DEV_OPTIONS_BOOT_RETRIES (   0x05                   \
                                      | DEV_OFLAG_UCHAR     \
                                      | DEV_OFLAG_ABSOLUTE )

/* BOOTP/DHCP option: Initial timeout value */
#define DEV_OPTIONS_BOOT_TIMEOUT (   0x06                   \
                                      | DEV_OFLAG_UCHAR     \
                                      | DEV_OFLAG_ABSOLUTE )

/*
 * DHCP/BOOTP option: Number of ARP probe retries before configuring
 * a DHCP/BOOTP address:
 * -1 means no ARP probe
 *  0 means use default value
 * (Default value is MAX_PROBE retries)
 */
#define DEV_OPTIONS_BOOT_ARP_RETRIES                        \
                                    (   0x08                   \
                                      | DEV_OFLAG_INT )
/*
 * DHCP/BOOTP option: Interval in seconds between ARP probes if ARP
 * probes are to be sent prior to configuring a DHCP/BOOTP address.
 * 0 means use default value
 * (Default value is TM_PROBE_INTERVAL/TM_UL(1000))
 */
#define DEV_OPTIONS_BOOT_ARP_INTVL                          \
                                    (   0x09                   \
                                      | DEV_OFLAG_UCHAR     \
                                      | DEV_OFLAG_ABSOLUTE )
/*
 * DHCP/BOOTP option: number of seconds to wait after sending the
 * first ARP probe/Arp Request before finishing configuring
 * a DHCP/BOOTP address.
 * 0 means use default value.
 */
#define DEV_OPTIONS_BOOT_ARP_TIMEOUT                        \
                                    (   0x0a                   \
                                      | DEV_OFLAG_UCHAR     \
                                      | DEV_OFLAG_ABSOLUTE )


/* DHCP option: Do not send a RELEASE message when closing DHCP */
#define DEV_OPTIONS_NO_DHCP_RELEASE                         \
                                    (   0x0b                   \
                                      | DEV_OFLAG_UCHAR     \
                                      | DEV_OFLAG_ABSOLUTE )
/*
 * DHCP option:
 * Pick which host name to send to the server in the
 * host name option and/or FQDN option.
 * 0 means pick user set values (default)
 * 1 means pick server set values
 */
#define DEV_OPTIONS_BOOT_PK_HOST_NM                         \
                                    (   0x0c                   \
                                      | DEV_OFLAG_UCHAR     \
                                      | DEV_OFLAG_ABSOLUTE )

/*
 * DHCP option:
 * Pick which domain name to send to the server in the FQDN option
 * 0 means pick user set values (default)
 * 1 means pick server set values
 */
#define DEV_OPTIONS_BOOT_PK_DOMAIN_NM                       \
                                    (   0x0d                   \
                                      | DEV_OFLAG_UCHAR     \
                                      | DEV_OFLAG_ABSOLUTE )

/* Enable or disable forwarding */
#define DEV_OPTIONS_FORWARDING   (   0x0f                   \
                                      | DEV_OFLAG_UCHAR )

/*
 * Socket event flags. See the function getsocketevents.
 */
#define SE_CONNECT_COMPLT            0x0001
#define SE_ACCEPT                    0x0002
#define SE_RECV                      0x0004
#define SE_RECV_OOB                  0x0008
#define SE_SEND_COMPLT               0x0010
#define SE_REMOTE_CLOSE              0x0020
#define SE_SOCKET_ERROR              0x0040
#define SE_RESET                     0x0080
#define SE_CLOSE_COMPLT              0x0100
#define SE_WRITE_READY               0x0200


/* PPP Interface API ***************************************************** */

/*
 * PPP Macros
 */

#define PPP_OPT_WANT             0
#define PPP_OPT_ALLOW            1

/*
 * LCP Options
 */
/* The maximum receive unit that we support RFC1661*/
#define LCP_MAX_RECV_UNIT        1
/* The Async Control Character Map RFC1662*/
#define LCP_ACCM                 2
/* The authentication protocol that we require to us RFC1661*/
#define LCP_AUTH_PROTOCOL        3
/* The link quality protocol we wish to use RFC1661*/
#define LCP_QUALITY_PROTOCOL     4
/* Magic Number for loopback Detection RFC1661*/
#define LCP_MAGIC_NUMBER         5
/* Turn on protocol field compression RFC1661*/
#define LCP_PROTOCOL_COMP        7
/* Turn on address/control field compression RFC1661*/
#define LCP_ADDRCONTROL_COMP     8
/* LCP timeout, retry options */
#define LCP_TERM_RETRY           9
#define LCP_CONFIG_RETRY         10
#define LCP_TIMEOUT              11
/* LCP maximum failure option */
#define LCP_MAX_FAILURES         12

/*
 * IPCP Options
 */
/* The compression protocol that the requesting system wishes to use */
#define IPCP_COMP_PROTOCOL       2
/* The IP Address the requesting system wishes to use */
#define IPCP_IP_ADDRESS          3
/* The number of VJ compression slots to use */
#define IPCP_VJ_SLOTS            4
/* IPCP Timeout, retry options */
#define IPCP_RETRY               5
#define IPCP_TIMEOUT             6
/* LCP maximum failure option */
#define IPCP_MAX_FAILURES        7
/* RFC-2507 IPv4 header compression parameters. */
#define IPCP_IPHC_TCP_SPACE      8
#define IPCP_IPHC_NON_TCP_SPACE  9
#define IPCP_IPHC_MAX_PERIOD     10
#define IPCP_IPHC_MAX_TIME       11
#define IPCP_IPHC_MAX_HEADER     12

/*
 * We overlap these with the option numbers since 29/31 are
 * unused by the stack to save space
 * We simple add 100 or subtract 100 when sending an receiving
 */
#define IPCP_DNS_PRI     29
#define IPCP_DNS_SEC     31

/*
 * LQM (Link Quality Monitoring) macros
 */

/* values for reasonCode passed to user-defined link quality
   monitoring function (refer to tfLqmRegisterMonitor) */
#define LQM_MONITOR_LQR          0
#define LQM_MONITOR_TIMEOUT      1

/* [RFC1989].R2.9:40
   max retransmission count for retransmission of Link-Quality-Report if we
   expect a LQR response from the peer and we receive no response. */
#define LQM_RETX_MAX_RETRIES     1

/*
 * DNS Flags
 */
#define DNS_PRI_SERVER          0
#define DNS_SEC_SERVER          1



/*
 * Structure used to return address information for a hostname to a user by
 * getaddrinfo, and used by a user to specify what sort of addresses should be
 * returned by getaddrinfo.
 */
struct addrinfo
{
/* AI_PASSIVE, AI_CANONNAME, AI_NUMERICHOST */
    int                      ai_flags;

/* PF_INET, PF_INET6 */
    int                      ai_family;

/* SOCK_xxx (not used) */
    int                      ai_socktype;

/* 0 or IPPROTO_xxx (not used) */
    int                      ai_protocol;

/* length of ai_addr */
    unsigned int             ai_addrlen;
#define ai_mxpref ai_addrlen

/* canonical name for nodename */
    char  *                  ai_canonname;
#define ai_mxhostname ai_canonname

/* binary address */
    struct sockaddr *        ai_addr;

/* next structure in linked list */
    struct addrinfo *        ai_next;
};
/*
 * Flags for addrinfo structure for getaddrinfo.
 */

/* Not supported */
/* #define AI_PASSIVE  0x0001 */

/* Request for canonical name */
#define AI_CANONNAME   0x0002

/* Don't use name resolution */
#define AI_NUMERICHOST 0x0004

/* Not supported */
/* #define AI_NUMERICSERV 0x0008 */

/* Return IPv4 addresses in IPv4-mapped format if no native IPv6 are found. */
#define AI_V4MAPPED    0x0010

/* Return all IPv4 and IPv6 addresses found. */
#define AI_ALL         0x0020

/*
 * Return IPv4 addresses only if IPv4 is configured on this device; return
 * IPv6 addresses only if IPv6 is configured on this device.
 */
#define AI_ADDRCONFIG 0x0040

/* Allowed flags for getaddrinfo call */
#define GETADDRINFO_FLAGS (AI_CANONNAME  | AI_NUMERICHOST | \
                              AI_V4MAPPED   | AI_ALL         | \
                              AI_ADDRCONFIG )

/*
 * Flags for getnameinfo
 */
/* Not supported */
/* #define NI_NOFQND       0x0001 */

/* Don't do a DNS lookup for this address - just return the address as a
   numeric string. */
#define NI_NUMERICHOST  0x0002
/* Return an error if the host's name cannot be found. */
#define NI_NAMEREQD     0x0004

/* Not supported. */
/* #define NI_NUMERICSERV  0x0008 */
/* Not supported. */
/* #define NI_NUMERICSCOPE 0x0010 */
/* Not supported. */
/* #define NI_DGRAM        0x0020 */


/*
 * PAP Options
 */
/* The local username to authenticate with */
#define PAP_USERNAME 1
/* The local password to authenticate with */
#define PAP_PASSWORD 2
/* PAP timeout, retry options */
#define PAP_RETRY    3
#define AP_TIMEOUT  4


/* Authentication methods, user may add more methods (we call it methods
 * here not protocol, because we don't want confuse with macro like
 * PPP_CHAP_PROTOCOL),
 * however, the value needs to be an integer between 1 and 8
 */
#define PPP_AUTHMETHOD_MIN        (ttUser8Bit)0
#define PPP_AUTHMETHOD_NULL       (ttUser8Bit)0
#define PPP_AUTHMETHOD_EAP        (ttUser8Bit)1
#define PPP_AUTHMETHOD_CHAP       (ttUser8Bit)2
#define PPP_AUTHMETHOD_PAP        (ttUser8Bit)3
#define PPP_AUTHMETHOD_MSCHAP_V1  (ttUser8Bit)4
#define PPP_AUTHMETHOD_MAX        (ttUser8Bit)8

#define PPP_AUTHPRIORITY_MIN      (ttUser8Bit)0x0
#define PPP_AUTHPRIORITY_MAX      (ttUser8Bit)0x0f


/*
 * CHAP options
 */
/* The local username to authenticate with */
#define CHAP_USERNAME 1
/* The local secret to authenticate with */
#define CHAP_SECRET   2
/* CHAP timeout, retry options */
#define CHAP_RETRY    3
#define CHAP_TIMEOUT  4
/* Add CHAP auth algorihtms */
#define CHAP_ALG_ADD  5
/* Delete CHAP auth algorihtms */
#define CHAP_ALG_DEL  6
/* MS-chap password */
#define CHAP_MSSECRET 7

/* CHAP encryption types (MD5,DES,etc) */
#define CHAP_MD5          0x05
#define CHAP_MSV1         0x80
#define CHAP_MSV2         0x81

/*
 * General PPP options
 */
#define PPP_SEND_BUFFER_SIZE 1
#define PPP_OPEN_TIMEOUT     2

/*
 * PPP Protocols
 */
#define PPP_PROTOCOL                 (unsigned short)(0x0000)
#define PPP_LCP_PROTOCOL             (unsigned short)(0xc021)
#define PPP_IPCP_PROTOCOL            (unsigned short)(0x8021)
#define PPP_PAP_PROTOCOL             (unsigned short)(0xc023)
#define PPP_CHAP_PROTOCOL            (unsigned short)(0xc223)
#define PPP_COMP_TCP_PROTOCOL        (unsigned short)(0x002d)
#define PPP_UNCOMP_TCP_PROTOCOL      (unsigned short)(0x002f)
#define PPP_IP_PROTOCOL              (unsigned short)(0x0021)
#define PPP_LQR_PROTOCOL             (unsigned short)(0xc025)
#define PPP_EAP_PROTOCOL             (unsigned short)(0xc227)

/*
 * PPP and PPPoE Link Layer User Notification Flags (ttUserLnkNotifyFuncPtr)
 */
/* LL_OPEN_COMPLETE is notified when at least one NCP (IPv4, IPv6) is
   opened successfully */
#define LL_OPEN_COMPLETE         0x0002
/* LL_OPEN_FAILED is notified when all NCPs (IPv4, IPv6) have failed
   to open */
#define LL_OPEN_FAILED           0x0004
#define LL_CLOSE_STARTED         0x0008
#define LL_CLOSE_COMPLETE        0x0010
#define LL_LCP_UP                0x0020
#define LL_PAP_UP                0x0040
#define LL_CHAP_UP               0x0080
#define LL_IP4_EVENT             0x0100 /* conserve bit flags */
#define LL_IP6_EVENT             0x0200 /* conserve bit flags */
#define LL_IP4_OPEN_COMPLETE     (LL_IP4_EVENT | LL_OPEN_COMPLETE)
#define LL_IP6_OPEN_COMPLETE     (LL_IP6_EVENT | LL_OPEN_COMPLETE)
#define LL_NCP_OPEN_FAILED       0x0400 /* conserve bit flags */
#define LL_IP4_OPEN_FAILED       (LL_IP4_EVENT | LL_NCP_OPEN_FAILED)
#define LL_IP6_OPEN_FAILED       (LL_IP6_EVENT | LL_NCP_OPEN_FAILED)
#define LL_EAP_UP                0x0800
/* LQM has been enabled on the link */
#define LL_LQM_UP                0x1000
/* LQM has been disabled on the link */
#define LL_LQM_DISABLED          0x2000
/* link quality is bad and recovery should be attempted */
#define LL_LQM_LINK_BAD          0x4000

/*
 * No error return value from various link layer functions.
 */
#define LL_OKAY                  0x0000

#define SOC_NO_INDEX                ((unsigned int)0xFFFF) /* invalid index */
#define WILD_PORT                   ((uint16_t)0xffff)
#define SOCKET_ERROR                (-1)
#define SO_SNDAPPEND                (0x1009)   /* send buffer copy threshold in bytes */
#define SO_RCVCOPY                  (0x100A)   /* recv buffer copy fraction */
#define SO_SND_DGRAMS               (0x100B)   /* send datagram count (non-TCP) */
#define SO_RCV_DGRAMS               (0x100C)   /* receive datagram count (non-TCP) */
#define IPO_HDRINCL                 (0x0002)
#define IPO_TOS                     (0x0003)
#define IPO_TTL                     (0x0004)
#define IPO_SRCADDR                 (0x1000)
/*
 * Turn ON/OFF disabling Nagle Algorithm (default OFF, Nagle algoritm
 * enabled: delay sending non full-size segments when it does not empty
 * the queue.)
 */
#define TCP_NODELAY           0x0001
#define TCP_MAXSEG            0x0002 /* Set Maximum TCP segment */
/*
 * Turn ON/OFF non usage of push bit when send queue is emptied.
 * (Default OFF: Push bit set when send queue is emptied)
 */
#define TCP_NOPUSH            (0x0004)
/*
 * Make urgent pointer point to last byte of urgent data (RFC1122)
 * (Default on)
 */
#define TCP_STDURG            (0x0008)
/*
 * Set amount of time before a connection is broken once TCP starts
 * retransmission, or probing a zero window. 0 means use default value,
 * -1 means retransmit forever. If a positive value is specified, it may be
 * rounded up to the implementation's next retrasmission time.
 * (Note set both a value and a flag.)
 */
#define TCP_MAXRT              (0x0010)
/* 0x0020 is reserved */

/* Set keep alive timeout (default 7200 seconds) */
#define TCP_KEEPALIVE          (0x4001)

/* DHCP Interface API ***************************************************** */

/*
 * Flags bit used in the flags field of tfDhcpUserSet(), or tfDhcpConfSet()
 */
#define DHCPF_FQDN_FLAG_S_ZERO     0x01 /* Do not set the FQDN S bit */
#define DHCPF_FQDN_PARTIAL         0x02 /* FQDN domain name is partial */
/* FQDN domain name should be sent in ASCII */
#define DHCPF_FQDN_ASCII           0x04
#define DHCPF_FQDN_FLAG_N_ONE      0x08 /* Set the FQDN N bit */
#define DHCPF_FQDN_ENABLE          0x10 /* DHCP FQDN is enabled */
#define DHCPF_INIT_REBOOT          0x20 /* Start in INIT-REBOOT state */
#define DHCPF_REQUESTED_IP_ADDRESS 0x40 /* Use requested IP address field */
#define DHCPF_SUPPRESS_CLIENT_ID   0x80 /* Suppress Client ID option */
#define DHCPF_HOST_NAME            0x100 /* Host name option */
#define DHCPF_FQDN_DISABLE         0x200 /* Disable DHCP FQDN */


/* Status bits */
#define FQDNF_PARTIAL         0x10 /* Indicates the FQDN is partial */
#define FQDNF_MALFORMED       0x20 /* Invalid FQDN format           */
#define FQDNF_NOT_SUPPORTED   0x40 /* server doesn't support FQDN   */
#define FQDNF_NAMEREPLY       0x80 /* server reply contained FQDN   */

#define DHCPF_FQDN_FLAGS                                             \
                        (  DHCPF_FQDN_ENABLE                         \
                         | DHCPF_FQDN_PARTIAL                        \
                         | DHCPF_FQDN_ASCII                          \
                         | DHCPF_FQDN_FLAG_S_ZERO                    \
                         | DHCPF_FQDN_FLAG_N_ONE  )

#define DHCPF_FLAGS (   DHCPF_INIT_REBOOT                         \
                         | DHCPF_REQUESTED_IP_ADDRESS                \
                         | DHCPF_SUPPRESS_CLIENT_ID                  \
                         | DHCPF_FQDN_FLAGS                          \
                         | DHCPF_FQDN_DISABLE                        \
                         | DHCPF_HOST_NAME )

/* Flag bits used in the flags field of tfBootpConfSet()/tfBootpUserSet() */
#define BOOTPF_HOST_NAME 0x01

/* All valid flags used by tfBootpConfSet()/tfBootpUserSet() */
#define BOOTPF_FLAGS ( BOOTPF_HOST_NAME )

typedef struct bootEntry {
/* unused */
    ip_addr                           btuBootSIpAddress;
/* Domain name server (for DNS) */
    ip_addr                           btuDns1ServerIpAddress;
/* Second domain name server (for DNS) */
    ip_addr                           btuDns2ServerIpAddress;
/* Our (leased) IP address */
    ip_addr                           btuYiaddr;
/* Our subnet mask */
    ip_addr                           btuNetMask;
/* Default router */
    ip_addr                           btuDefRouter;
/* Default router after lease renewal (if changed) */
    ip_addr                           btuLeaseDefRouter;
/* DHCP selected server IP address */
    ip_addr                           btuDhcpServerId;
/* Time stamp of DHCP request (Internal) */
    unsigned long                     btuDhcpRequestTimeStamp;
/* DHCP Address lease time (internal) */
    unsigned long                     btuDhcpLeaseTime;
/* DHCP T1 time (internal) */
    unsigned long                     btuDhcpT1;
/* DHCP T2 time (internal) */
    unsigned long                     btuDhcpT2;
/* unused */
    unsigned short                    btuBootFileSize;
/* seconds since we started sending a DHCP discover*/
    unsigned short                    btuSeconds;
    ip_addr                           btuNetBiosNameServers[2];
    unsigned char                     btuNetBiosNumNameServers;
    unsigned char                     btuDhcpClientIdLength;
    unsigned char                     btuDhcpHostNameLength;
    unsigned char                     btuDhcpClientIDPtr[16];
    unsigned char                     btuDhcpHostNamePtr[16];
/*
 * btuNames contains btuDhcpRxHostNamePtr, btuDomainName,
 * and btuServerFqdn (ascii, and binary) pointers
 */
#define TM_BTU_NAMES_MAX_INDEX  6
    unsigned char                    * btuNames[TM_BTU_NAMES_MAX_INDEX];
/* Rx Host name */
#define TM_BT_RXHNAME_INDEX 0
#define btuDhcpRxHostNamePtr    btuNames[TM_BT_RXHNAME_INDEX]
/* Domain Name */
#define TM_BT_DOMAIN_INDEX  1
#define btuDomainName    btuNames[TM_BT_DOMAIN_INDEX]
/* Domain name (64 bytes) (for DNS) */
#define TM_BOOTSNAME_SIZE             64
/*
 * btuNamesLengths contains btuDhcpRxHostNameLength and btuDomainName length.
 */
#define btuDhcpRxHostNameLength btuNamesLengths[TM_BT_RXHNAME_INDEX]
#define btuDhcpFqdnALength       btuNamesLengths[TM_BT_FQDN_AINDEX]
#define btuDhcpFqdnBLength       btuNamesLengths[TM_BT_FQDN_BINDEX]
    unsigned short                    btuNamesLengths[
                                        ((TM_BTU_NAMES_MAX_INDEX + 2)/2) * 2];
/*
 * btuDhcpRxHostNamePtr and btuDomainName point into this array when they are 
 * empty so that they can point to a null terminated empty string.
 */
    unsigned char                      btuNamesZeroArray[
                                        ((TM_BTU_NAMES_MAX_INDEX + 3)/4) * 4];
    int                                btuServerFqdnLen;
    int                                btuFqdnStatus;
#define btuFqdnOptionFlags      btuFqdnArray[0]
#define btuFqdnRCode1           btuFqdnArray[1]
#define btuFqdnRCode2           btuFqdnArray[2]
#define btuFqdnRcvOffset        btuFqdnArray[3]
    unsigned char                      btuFqdnArray[4];
} bootEntry;


/* DNS Resolver API ******************************************************* */

/*
 * DNS error codes
 */

#define TM_DNS_ERROR_BASE   7000

/* Format error - The name server was unable to interpret the query */
#define TM_DNS_EFORMAT      (TM_DNS_ERROR_BASE + 1)

/*
 * Server Failure - The name server was unable to process this query due to a
 * problem with the name server.
 */
#define TM_DNS_ESERVER      (TM_DNS_ERROR_BASE + 2)

/*
 * Name Error - Meaningful only for responses from an authoritative name
 * server, this code signifies that the domain name referenced in the query
 * does not exist.
 */
#define TM_DNS_ENAME_ERROR  (TM_DNS_ERROR_BASE + 3)

/*
 * Not Implemented - The name server does not support the requested kind of
 * query.
 */
#define TM_DNS_ENOT_IMPLEM  (TM_DNS_ERROR_BASE + 4)

/*
 * Refused - The name server refuses to perform the specified operation for
 * policy reasons.  For example, a name server may not wish to provide the
 * information to the particular requester, or a name server may not wish to
 * perform a particular operation (e.g., zone transfer) for particular data.
 */
#define TM_DNS_EREFUSED     (TM_DNS_ERROR_BASE + 5)

/* Error codes 6-15 are reserved, per RFC */

/*
 * No answer received from name server (i.e., response packet received,
 * but it did not contain the answer to our query).
 */
#define TM_DNS_EANSWER      (TM_DNS_ERROR_BASE + 16)

/*
 * DNS request/reply cache is full.
 */
#define TM_DNS_ECACHE_FULL  (TM_DNS_ERROR_BASE + 17)


/*
 * DNS Option types
 */
#define DNS_OPTION_CACHE_SIZE            0
#define DNS_OPTION_RETRIES               1
#define DNS_OPTION_TIMEOUT               2
#define DNS_OPTION_CACHE_TTL             3
#define DNS_OPTION_MAX_SOCKETS_ALLOWED   4
#define DNS_OPTION_BINDTODEVICE          5



/*
 * FTP easy to use functions
 */
typedef struct {
    char ftpHost[96];
    char port[8];
    char userID[32];
    char password[32];
    char localFile[96];
    char remoteFile[96];
    char errorMsg[128];

} ftp_parm_t;

/*
 *  Get file specified by parameters in ftp_parm_t.
 *  Return 0 on success. Non-zero on failure.
 *
 *  Example:
 *  ftp_parm_t parms;
 *  strcpy(parms.ftpHost, "ftp.verifone.com");
 *  strcpy(parms.port, "23");
 *  strcpy(parms.userID, "anonymous");
 *  strcpy(parms.password, "test@verifone.com");
 *  strcpy(parms.localFile, "I:ftp.dat");
 *  strcpy(parms.remoteFile, "pub/testfile.dat");
 *  ftpGet(&parms);
*/

int ftpGet(ftp_parm_t *ftp);
/*
 *  Put file specified by parameters in ftp_parm_t.
*/

int ftpPut(ftp_parm_t *ftp);


int accept (int sockfd, struct sockaddr *cliaddr, socklen_t *addrlen);
int recv (int sockfd, void *buff, size_t nbytes, int flags);
int recvfrom (int sockfd, void * buff, size_t nbytes, int flags,
                    struct sockaddr *from, socklen_t *addrlen);
int DnsGetHostByName(const char *hostname, in_addr_t *ip);
int DnsGetHostByAddr(in_addr_t ip, char *hostname, int hostnameLen);
int freehostent(struct hostent *he);
int gethostbyname(const char *hostname, struct hostent **he);
int gethostbyaddr(const char *addr, socklen_t len, int family,
                                                           struct hostent **he);
int getaddrinfo(const char *hostname, const char *servname,
        const struct addrinfo *hints, struct addrinfo **result);
int select(int maxfd, fd_set *in, fd_set *out, fd_set *ex,
                                                    struct timeval *timeout);
const char *netlib_version(void);

#ifndef __GNUC__
#define _SYS static

_SYS int socket (int family, int type, int protocol);
_SYS int bind (int sockfd, const struct sockaddr *myaddr, socklen_t addrlen);
_SYS int listen (int sockfd, int backlog);
_SYS int connect (int sockfd, const struct sockaddr *servaddr,
                    socklen_t addrlen);
_SYS int getpeername (int sockfd, struct sockaddr *localaddr,
                    socklen_t *addrlen);
_SYS int getsockname (int sockfd, struct sockaddr *peeraddr,
                socklen_t *addrlen);
_SYS int setsockopt (int sockfd, int level, int optname,
                        const void *optval, socklen_t optlen);
_SYS int getsockopt (int sockfd, int level, int optname,
                        void *optval, socklen_t *optlen);
_SYS int send (int sockfd, const void *buff, size_t nbytes, int flags);
_SYS int sendto (int sockfd, void * buff, size_t nbytes, int flags,
                    const struct sockaddr *to, socklen_t addrlen);
_SYS int shutdown (int sockfd, int howto);
_SYS int socketclose (int sockfd);
_SYS int socketerrno(int sockfd);
_SYS int socketset_owner(int sockfd, int task_id);
_SYS int socketioctl(int sockfd, int cmd, int*arg);
_SYS int DnsSetServer(ip_addr dnsserver, int servernum);
_SYS int DnsSetUserOption(int optType, void *optValue, int optLen);
_SYS int blockingIO(int sockfd);
_SYS in_addr_t inet_addr(const char *strPtr);
_SYS int inet_aton(const char *strPtr, ip_addr *addrptr);
_SYS int inet_ntoa(ip_addr ipAddress, char *outputIpAddressString);
_SYS int PingOpenStart(const char *remoteHost, int seconds, int datalen);
_SYS int PingGetStats(int sockfd, PingInfo *pingInfoptr);
_SYS int PingCloseStop(int sockfd);
_SYS int FD_SET(int n, fd_set *p);
_SYS int FD_SET(int n, fd_set *p);
_SYS int FD_CLR(int n, fd_set *p);
_SYS int FD_ISSET(int n, fd_set *p);
_SYS int FD_ZERO(fd_set *p);
_SYS int FD_COPY(const fd_set *f, fd_set *t);
_SYS int getsocketevents(int sockfd, int *events);
_SYS int peeksocketevents(int sockfd, int *events);
_SYS int vxtcp_version(char *versionstring, size_t ver_len);
#endif //__GNUC__

#undef _SYS

#include <svc_nwi.h>

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif  /* _SVC_NET_H */
