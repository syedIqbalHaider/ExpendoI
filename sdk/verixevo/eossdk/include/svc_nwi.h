/*****************************************************************************
 * Copyright 2011 by VeriFone, Inc. All Rights Reserved.
 * 
 * Accessing, using, copying, modifying or distributing this source code is
 * expressly not permitted without the prior written authorization of VeriFone,
 * Inc.  Engaging in any such activities without such authorization is an
 * infringement of VeriFone's copyrights and a misappropriation of VeriFone's
 * trade secrets in this source code, which will subject the perpetrator to
 * certain civil damages and criminal penalties.
 *****************************************************************************/

#include <svc.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

/* ------------------------------------------------------------------------ */

#define NET_SWI     (17)
#define NETIF_SWI   (16)

int _net1(int)                              __swi(NET_SWI);
int _net2(int,int)                          __swi(NET_SWI);
int _net3(int,int,int)                      __swi(NET_SWI);
int _net4(int,int,int,int)                  __swi(NET_SWI);
int _net5(int,int,int,void*)                __swi(NET_SWI);
int _net6(int,int,int,void*)                __swi(NET_SWI);
int _net7(int,int,int,void*)                __swi(NET_SWI);

int _netif1(int)                            __swi(NETIF_SWI);
int _netif2(int,int)                        __swi(NETIF_SWI);
int _netif3(int,int,int)                    __swi(NETIF_SWI);
int _netif4(int,int,int,int)                __swi(NETIF_SWI);
int _netif5(int,int,int,void*)              __swi(NETIF_SWI);
int _netif6(int,int,int,void*)              __swi(NETIF_SWI);
int _netif7(int,int,int,void*)              __swi(NETIF_SWI);

/* User functions which invoke SWIs */

#if defined(__cplusplus)
#define _INLINE static inline
#define _UNUSED(type, name) type
#elif defined(__GNUC__)
#define _INLINE static inline
#define _UNUSED(type, name) type name
#else
#define _INLINE static __inline
#define _UNUSED(type, name) type name
#endif

//_SYS int t_accept (int sockfd, struct sockaddr *cliaddr, socklen_t *addrlen);
//_SYS int t_recv (int sockfd, void *buff, size_t nbytes, int flags);
//_SYS int t_recvfrom (int sockfd, void * buff, size_t nbytes, int flags, 
//                    struct sockaddr *from, socklen_t *addrlen);
//_SYS int t_DnsGetHostByName(const char *hostname, in_addr_t *ip);
//_SYS int t_select(int maxfd, fd_set *in, fd_set *out, fd_set *ex, struct timeval *timeout);


_INLINE int socket (int family, int type, int protocol)
    { return _net4(0, family, type, protocol); }
_INLINE int bind (int sockfd, const struct sockaddr *myaddr, socklen_t addrlen)
    { return _net4(1, sockfd, (int)myaddr, addrlen); }
_INLINE int listen (int sockfd, int backlog)
    { return _net3(2, sockfd, backlog); }
_INLINE int t_accept (int sockfd, struct sockaddr *cliaddr, socklen_t *addrlen)
    { return _net4(3, sockfd, (int)cliaddr, (int)addrlen); }
_INLINE int connect (int sockfd, const struct sockaddr *servaddr, 
                    socklen_t addrlen)
    { return _net4(4, sockfd, (int)servaddr, addrlen); }
_INLINE int getpeername (int sockfd, struct sockaddr *localaddr, 
                    socklen_t *addrlen)
    { return _net4(5, sockfd, (int)localaddr, (int)addrlen); }
_INLINE int getsockname (int sockfd, struct sockaddr *peeraddr, 
                socklen_t *addrlen)
    { return _net4(6, sockfd, (int)peeraddr, (int)addrlen); }
_INLINE int setsockopt (int sockfd, int level, int optname, 
                        const void *optval, socklen_t optlen)
    {   
        int parms[3];
        parms[0] = optname;
        parms[1] = (int)optval;
        parms[2] = optlen;
        return _net6(7, sockfd, level, parms);
    }
_INLINE int getsockopt (int sockfd, int level, int optname, 
                        void *optval, socklen_t *optlen)
    {   
        int parms[3];
        parms[0] = optname;
        parms[1] = (int)optval;
        parms[2] = (int)optlen;
        return _net6(8, sockfd, level, parms);
    }
_INLINE int t_recv (int sockfd, void *buff, size_t nbytes, int flags)
    {   
        int parms[2];
        parms[0] = nbytes;
        parms[1] = flags;
        return _net5(9, sockfd, (int)buff, parms);
    }
_INLINE int send (int sockfd, const void *buff, size_t nbytes, int flags)
    {   
        int parms[2];
        parms[0] = nbytes;
        parms[1] = flags;
        return _net5(10, sockfd, (int)buff, parms);
    }
_INLINE int t_recvfrom (int sockfd, void * buff, size_t nbytes, int flags, 
                    struct sockaddr *from, socklen_t *addrlen)
    {   
        int parms[4];
        parms[0] = nbytes;
        parms[1] = flags;
        parms[2] = (int)from;
        parms[3] = (int)addrlen;
        return _net7(11, sockfd, (int)buff, parms);
    }
_INLINE int sendto (int sockfd, void * buff, size_t nbytes, int flags, 
                    const struct sockaddr *to, socklen_t addrlen)
    {   
        int parms[4];
        parms[0] = nbytes;
        parms[1] = flags;
        parms[2] = (int)to;
        parms[3] = addrlen;
        return _net7(12, sockfd, (int)buff, parms);
    }
_INLINE int shutdown (int sockfd, int howto)
    { return _net3(13, sockfd, howto); }
_INLINE int socketclose (int sockfd)
    { return _net2(14, sockfd); }
_INLINE int socketerrno(int sockfd)
    { return _net2(15, sockfd); }
_INLINE int socketioctl(int sockfd, int cmd, int *arg)
    { return _net4(16, sockfd, cmd, (int)arg); }

_INLINE int t_select(int maxfd, fd_set *in, fd_set *out, fd_set *ex, 
        struct timeval *timeout)
    {
        int parms[3];

        parms[0] = (int) out;
        parms[1] = (int) ex;
        parms[2] = (int) timeout;
        return _net6(17, (int)maxfd, (int)in, parms);
    }

_INLINE int FD_SET(int n, fd_set *p)
    {   return (_net3(29, n, (int)p)); }

_INLINE int FD_CLR(int n, fd_set *p)
    {   return (_net3(30, n, (int)p)); }

_INLINE int FD_ISSET(int n, fd_set *p) 
    {   return (_net3(31, n, (int)p)); }

_INLINE int FD_ZERO(fd_set *p)
    {   return (_net2(32, (int)p)); }

_INLINE int FD_COPY(const fd_set *f, fd_set *t)
    {   return (_net3(33, (int)f, (int)t)); }

_INLINE int getsocketevents(int sockfd, int *events)
    {   return (_net3(35,sockfd,(int)events));    }

_INLINE int vxtcp_version(char *versionstring, size_t ver_len)
    {   return (_net3(38,(int)versionstring,(int)ver_len)); }

_INLINE int peeksocketevents(int sockfd, int *events)
    {   return (_net3(39,sockfd,(int)events));    }

_INLINE int socketset_owner(int sockfd, int task_id)
    { return _net3(18, sockfd, task_id); }

_INLINE int t_DnsGetHostByName(const char *hostname, in_addr_t *ip)
    { return _net3(19, (int)hostname, (int)ip); }

_INLINE int t_DnsGetHostByAddr(in_addr_t ip, char *hostname, int hostnameLen)
    { return _net4(34, (int)ip, (int)hostname, (int)hostnameLen); }

_INLINE int t_getaddrinfo(const char *hostname, const char *servname,
        const struct addrinfo *hints, struct addrinfo **result)
    {
        int parms[2];
        parms[0] = (int)hints;
        parms[1] = (int)result;
        return _net5(36, (int)hostname, (int)servname, parms);
    }

_INLINE int blockingIO(int sockfd)
    { return _net2(20, sockfd); }

_INLINE in_addr_t inet_addr(const char *strPtr)
    { return (in_addr_t)_net2(26, (int)strPtr); }

_INLINE int inet_aton(const char *strPtr, ip_addr *addrptr )
    { return _net3(27, (int)strPtr, (int)addrptr); }

_INLINE int inet_ntoa(ip_addr ipAddress, char *outputIpAddressString)
    { return _net3(28, (int)ipAddress, (int)outputIpAddressString); }

/* Ping API */
_INLINE int PingOpenStart(const char *remoteHost, int seconds, int datalen)
    { return _net4(21, (int)remoteHost, seconds, datalen); }

_INLINE int PingGetStats(int sockfd, PingInfo *pingInfoptr)
    { return _net3(22, sockfd, (int)pingInfoptr); }

_INLINE int PingCloseStop(int sockfd)
    { return _net2(23, sockfd); }

/* ****************************
 * Services for network devices
 * ****************************
 */

_INLINE int GetDhcpEvent(int inthdl)
    { return _netif2(10,inthdl); }

_INLINE unsigned long GetPppEvents(int inthdl)
    { return _netif2(12,inthdl); }

_INLINE int net_addif(int devhdl, int task_id)
    { return _netif3(0, devhdl, task_id); }

_INLINE int net_delif(int inthdl)
    { return _netif2(11, inthdl); }

_INLINE int net_stopif(int inthdl)
    { return _netif2(18, inthdl); }

_INLINE int net_startif(int inthdl)
    { return _netif2(19, inthdl); }

_INLINE int openSockets(void)
    { return _netif1(34); }

_INLINE int GetPppDnsIpAddress(int inthdl, ip_addr *dnsIpAddressPtr, int flag)
    { return _netif4(13,inthdl,(int)dnsIpAddressPtr,flag); }

_INLINE int GetPppPeerIpAddress(int inthdl, ip_addr *ifIpAddress)
    { return _netif3(14,inthdl,(int)ifIpAddress); }

_INLINE int PppSetOption(int inthdl, int protocolLevel, int remoteLocalFlag,
        int optionName, const char *optionValuePtr, int optionLength)
{
    int opts[4];

    opts[0] = remoteLocalFlag;
    opts[1] = optionName;
    opts[2] = (int)optionValuePtr;
    opts[3] = optionLength;
    return _netif7(1, inthdl, protocolLevel, opts);
}

_INLINE int SetPppPeerIpAddress(int inthdl, ip_addr ifIpAddress)
    { return _netif3(2,inthdl,(int)ifIpAddress); }

_INLINE int AddInterface(int inthdl, int linktype, long event)
    { return _netif4(3, inthdl, linktype, event); }

_INLINE int UseDhcp(int inthdl)
    { return _netif2(4,inthdl); }

_INLINE int UseBootp(int inthdl)
    { return _netif2(5,inthdl); }

_INLINE int ConfGetBootEntry(int inthdl, bootEntry *be)
    { return _netif3(36, inthdl, (int)be); }

_INLINE int OpenInterface(int inthdl, ip_addr myaddr, ip_addr mynetmask)
    { return _netif4(6,inthdl,myaddr,mynetmask); }

_INLINE int CloseInterface(int inthdl)
    { return _netif2(7, inthdl); }

_INLINE int GetBroadcastAddress(int inthdl, ip_addr *bcast)
    { return _netif3(15, inthdl, (int)bcast); }

_INLINE int GetIpAddress(int inthdl, ip_addr *ipaddr)
    { return _netif3(16, inthdl, (int)ipaddr); }

_INLINE int GetNetMask(int inthdl, ip_addr *mask)
    { return _netif3(17, inthdl, (int)mask); }

_INLINE int SetIfMtu(int inthdl, int mtu)
    { return _netif3(8,inthdl,mtu); }

_INLINE int InterfaceSetOptions(int inthdl, int optName, void *optVal, int optLen)
{
    int opts[2];
    opts[0] = (int)optVal;
    opts[1] = optLen;
    return _netif5(9, inthdl,optName,opts);
}

_INLINE int AddArpEntry(ip_addr arpIpAddress, char *physAddrPtr, int physAddrLength)
    { return _netif4(20, arpIpAddress, (int)physAddrPtr, physAddrLength); }

_INLINE int AddDefaultGateway(int handle, ip_addr gatewayIpAddress)
    { return _netif3(21, handle, gatewayIpAddress); }

_INLINE int AddProxyArpEntry(ip_addr arpIpAddress)
    { return _netif2(22, arpIpAddress); }

_INLINE int AddStaticRoute(int handle, ip_addr destIpAddress, ip_addr destNetMask, ip_addr gateway, int hops)
{
    int parms[3];

    parms[0] = destNetMask;
    parms[1] = gateway;
    parms[2] = hops;
    return _netif6(23, handle, destIpAddress, parms);
}

_INLINE int DelArpEntryByIpAddr(ip_addr arpIpAddress)
    { return _netif2(24, arpIpAddress); }

_INLINE int DelArpEntryByPhysAddr(char *physAddrPtr, int physAddrLength)
    { return _netif3(25, (int)physAddrPtr, physAddrLength); }

_INLINE int DelDefaultGateway(ip_addr gatewayIpAddress)
    { return _netif2(26, gatewayIpAddress); }

_INLINE int DelProxyArpEntry(ip_addr arpIpAddress)
    { return _netif2(27, arpIpAddress); }

_INLINE int DelStaticRoute(ip_addr destIpAddress, ip_addr destNetMask)
    { return _netif3(28, destIpAddress, destNetMask); }

_INLINE int DisablePathMtuDisc(ip_addr destIpAddress, unsigned short pathMtu)
    { return _netif3(29, destIpAddress, pathMtu); }

_INLINE int GetArpEntryByIpAddr(ip_addr arpIpAddress, char *physAddrPtr, int physAddrLength)
    { return _netif4(30, arpIpAddress, (int)physAddrPtr, physAddrLength); }

_INLINE int GetArpEntryByPhysAddr(char *physAddrPtr, int physAddrLen, ip_addr *arpIpAddressPtr)
    { return _netif4(31, (int)physAddrPtr, physAddrLen, (int)arpIpAddressPtr); }

_INLINE int GetDefaultGateway(ip_addr *gwayIpAddPtr)
    { return _netif2(32, (int)gwayIpAddPtr); }

_INLINE int RtDestExists(ip_addr destIpAddress, ip_addr destNetMask)
    { return _netif3(33, destIpAddress, destNetMask); }

_INLINE int DnsSetServer(ip_addr dnsserver, int servernum)
    { return _netif3(35, dnsserver, servernum); }

_INLINE int DnsSetUserOption(int optType, void *optValue, int optLen)
    { return _netif4(37, optType, (int)optValue, optLen); }

_INLINE int NetStat(int tableId, void *buff, size_t buffLen)
    { return _netif4(38, tableId, (int)buff, (int)buffLen); }

_INLINE int GetSnmpMib(int mibtype, void *mibstruct, size_t structlen)
    { return _netif4(39, mibtype, (int)mibstruct, structlen); }

_INLINE int ArpFlush(int addr_family)
    { return _netif2(40, addr_family); }

_INLINE int SetDhcpStartupDelayWindow(int inthdl,int windowSize)
    { return _netif3(41,inthdl,windowSize); }


#undef _INLINE
#undef _UNUSED
