#ifndef _VOS_IOCLTL_H
#define _VOS_IOCLTL_H

/* 
 *	This is the IOCTL header file for CommEngine and DDI Drivers.
 */

/*********************************************************************
 *	COMMON IOCTL
 *********************************************************************/

/* Retrieves the driver version */
#define		IOCTL_GET_DRIVER_BUILD_DATE						"GET_DRIVER_BUILD_DATE"

/* Retrieves the extended error message */
#define 	IOCTL_GET_EXTENDED_ERROR						"GET_EXTENDED_ERROR" 

/* Retrieves the extended error message */
#define 	IOCTL_GET_EXTENDED_ERROR_CODE						"GET_EXTENDED_ERROR_CODE" 

/* Retrives the states of the state machines */
#define 	IOCTL_GET_DRIVER_STATES							"GET_DRIVER_STATES"

/* Retrieves the received signal strength indication of the radio modem (CDMA, GPRS, GSM) */
#define 	IOCTL_GET_RSSI 									"GET_RSSI" 

/* Retrieves the absolute received signal strength indication of the radio modem (CDMA, GPRS, GSM) */
#define 	IOCTL_GET_RSSI_ABS 								"GET_RSSI_ABS" 

/* Retrieves the equivalent dBm of the received signal strength indication of the radio modem (CDMA, GPRS, GSM) */
#define 	IOCTL_GET_RSSI_DBM 								"GET_RSSI_DBM" 

/* Retrieves the firmware version of the modem (CDMA, GPRS, GSM) */
#define 	IOCTL_GET_FW_VERSION 							"GET_FW_VERSION" 

/* Retrieves the firmware version of the modem (GPRS, GSM) */
#define 	IOCTL_GET_FW_SUB_VERSION 						"GET_FW_SUB_VERSION" 

/* Retrieves the connection baud rate (DIAL, GSM) */
#define 	IOCTL_GET_CONNECTION_BAUD 						"GET_CONNECTION_BAUD" 

/* Set a Shot Message Format (GPRS, GSM) */
#define	IOCTL_SET_SMS_FORMAT								"SET_SMS_FORMAT"

/* Send a Text Short Message (GPRS, GSM) */
#define	IOCTL_SEND_TEXT_SMS								"SEND_TEXT_SMS"

/* Send a PDU Short Message (GPRS, GSM) */
#define	IOCTL_SEND_PDU_SMS								"SEND_PDU_SMS"


/*********************************************************************
 *	ETHERNET IOCTL
 *********************************************************************/

/* Retrieves MAC address associated to the device (Ethernet) */
#define		IOCTL_GET_MAC									"GET_MAC" 

/* Retrieves link speed of the connection (Ethernet) */
#define		IOCTL_GET_LINK_SPEED							"GET_LINK_SPEED"


/*********************************************************************
 *	GPRS AND GSM IOCTL
 *********************************************************************/

/* Retrieves the bit error rate  */
#define		IOCTL_GET_BER									"GET_BER" 

/* Retrieves the link status of the device */
#define		IOCTL_GET_LINK_STATUS							"GET_LINK_STATUS" 

/* Retrieves International Mobile Equipment Identity code  */
#define		IOCTL_GET_IMEI									"GET_IMEI" 

/* Retrieves SIM card Identification number  */
#define		IOCTL_GET_ICCID									"GET_ICCID" 

/* Retrieves Internation Mobile Subscriber Identity code  */
#define		IOCTL_GET_IMSI									"GET_IMSI" 

/* Retrieves the operator name */
#define		IOCTL_GET_OPERATOR_NAME						"GET_OPERATOR_NAME"

/* Toggles notification to user regarding network registration */
#define		IOCTL_SET_SERVICE_NOTIFICATION					"SET_SERVICE_NOTIFICATION"

/* Toggles notification to user regarding signal quality (in terms of bit error rate) */
#define		IOCTL_SET_BER_NOTIFICATION						"SET_BER_NOTIFICATION"

/* Retrieves the SIM that is currently being used in the connection */
#define		IOCTL_DSIM_GET_USED							"GET_USED"

/* Retrieves the last URC packet switch status received from the radio */
#define		IOCTL_GET_PKT_SWITCHED_STATUS					"GET_PKT_SWITCHED_STATUS"

/* Model ID */
#define		IOCTL_GET_MODEL_ID								"GET_MODEL_ID"

/* Manufacturer ID */
#define		IOCTL_GET_MFG_ID								"GET_MFG_ID"

/* Operator MCC (Mobile Country Code) and MNC (Mobile Network Code) */
#define		IOCTL_GET_OPERATOR_LAI							"GET_OPERATOR_LAI"


/*********************************************************************
 *	GPRS, GSM AND CDMA/EVDO IOCTL
 *********************************************************************/
/* Toggles notification to user regarding roaming status */
#define		IOCTL_SET_ROAM_NOTIFICATION						"SET_ROAM_NOTIFICATION"

/* Toggles notification to user regarding unread short messages */
#define		IOCTL_SET_SMS_NOTIFICATION						"SET_SMS_NOTIFICATION"

/* Toggles notification to user regarding call status */
#define		IOCTL_SET_CALL_NOTIFICATION						"SET_CALL_NOTIFICATION"

/* Toggles notification to user regarding shor message memory */
#define		IOCTL_SET_SMS_FULL_NOTIFICATION					"SET_SMS_FULL_NOTIFICATION"


/*********************************************************************
 *	CDMA/EVDO IOCTL
 *********************************************************************/


/* Turns on Dormancy notification */
#define 	IOCTL_SET_DORMANCY_NOTIFICATION 				"SET_DORMANCY_NOTIFICATION"

/* Turns on coverage notification */
#define 	IOCTL_SET_COVERAGE_NOTIFICATION 				"SET_COVERAGE_NOTIFICATION"

/* Retrieves the CDMA carrier ID */
#define 	IOCTL_GET_CARRIER_ID 							"GET_CARRIER_ID" 
				
/* Retrieves the call status of the CDMA modem */
#define 	IOCTL_GET_CALL_STATUS 							"GET_CALL_STATUS" 

/* Retrieves the CDMA device status */
#define 	IOCTL_GET_DEVICE_STATUS 						"GET_DEVICE_STATUS" 

/* Retrieves the CDMA 1XRTT roam status */
#define 	IOCTL_GET_ROAM_STATUS 							"GET_ROAM_STATUS" 

/* Retrieves the HDR roam status of the CDMA modem */
#define 	IOCTL_GET_HDR_ROAM_STATUS 						"GET_HDR_ROAM_STATUS" 

/* Retrieves the protocol revision number CDMA modem */
#define 	IOCTL_GET_PROTOCOL_REVISION						"GET_PROTOCOL_REVISION" 

/* Retrieves the HDR service state */
#define 	IOCTL_GET_HDR_SERVICE_STATE 					     "GET_HDR_SERVICE_STATE" 
				
/* Retrieves the coverage status of the CDMA modem */
#define 	IOCTL_GET_COVERAGE_STATUS 						"GET_COVERAGE_STATUS" 

/* Retrieves the HDR DRC value of the CDMA modem */
#define 	IOCTL_GET_HDR_DRC_VALUE 						"GET_HDR_DRC_VALUE" 
				
/* Retrieves the SID value of the CDMA modem */ 
#define 	IOCTL_GET_SID_VALUE 							"GET_SID_VALUE" 

/* Retrieves the NID value of the CDMA modem */ 
#define 	IOCTL_GET_NID_VALUE 							"GET_NID_VALUE" 

/* Retrieves the channel number of the CDMA modem */ 
#define 	IOCTL_GET_CHANNEL_NUMBER 						"GET_CHANNEL_NUMBER" 

/* Retrieves the PRL version of the CDMA modem */ 
#define 	IOCTL_GET_PRL_VERSION 							"GET_PRL_VERSION" 

/* Retrieves the Mobile IP error code of the CDMA modem */ 
#define 	IOCTL_GET_MOBILEIP_ERRCODE 						"GET_MOBILEIP_ERRCODE" 

/* Retrieves the 1XRTT RSSI of the CDMA modem */ 
#define 	IOCTL_GET_CDMA1XRTT_RSSI 						"GET_CDMA1XRTT_RSSI" 

/* Retrieves activation status of the CDMA modem */ 
#define 	IOCTL_GET_ACTIVATION_STATUS 					      "GET_ACTIVATION_STATUS" 
				
/* Retrieves the current roaming settings of the CDMA modem */ 
#define 	IOCTL_GET_ROAMING_PREF_SETTING 					"GET_ROAMING_PREF_SETTING" 

/* Retrieves the electronic serial number of the CDMA modem */ 
#define 	IOCTL_GET_ELECTRONIC_SRL_NO 					      "GET_ELECTRONIC_SRL_NO" 
				
/* Phone No associated with modem is retrieved*/ 
#define 	IOCTL_GET_PHONE_NUMBER							"GET_PHONE_NUMBER" 

/* Write here what phone number is retrieved. */ 
#define 	IOCTL_GET_MIN_NUMBER 							"GET_MIN_NUMBER" 

/* Enables CNS notifications */
#define 	IOCTL_ENABLE_ALL_NOTIFY							"ENABLE_ALL_NOTIFY" 
	
/* Disables CNS notifications */
#define 	IOCTL_DISABLE_ALL_NOTIFY 						"DISABLE_ALL_NOTIFY" 

/* Enables roaming */
#define 	IOCTL_ENABLE_ROAMING 							"ENABLE_ROAMING" 

/* Disables roaming */
#define 	IOCTL_DISABLE_ROAMING							"DISABLE_ROAMING"

/* Disables roaming */
#define 	IOCTL_DISABLE_ROAMING							"DISABLE_ROAMING"


/* Set HDR or 1xRTT */
#define 	IOCTL_SET_NETWORK							"SET_NETWORK"


/*********************************************************************
 *	HSPA/GPS IOCTL
 *********************************************************************/

/* Gets the type of network the terminal is connected to */
#define IOCTL_GET_CON_NETWORK					"CON_NETWORK"

/* Trigger start of GPS navigation */
#define IOCTL_GPS_START							"GPS_START"

/* Trigger stop of GPS navigation */
#define IOCTL_GPS_STOP							"GPS_STOP"

/* Get current GPS configuration */
#define IOCTL_GPS_GET_CONFIG					"GPS_GET_CONFIG"

/* Set GPS configuration */
#define IOCTL_GPS_SET_CONFIG					"GPS_SET_CONFIG"

/* Reset GPS configuration */
#define IOCTL_GPS_RESET_CONFIG					"GPS_RESET_CONFIG"

/* Get GPS last location */
#define IOCTL_GPS_LAST_LOC						"GPS_LAST_LOC"

/* Gets information of the serving cell */
#define IOCTL_GET_SERVCELL_INFO			"GET_SERVCELL_INFO"

/* Gets packet domain network registation */
#define IOCTL_GET_PDNET_REG				"GET_PDNET_REG"

/* Gets network availability */
#define IOCTL_GET_NET_AVAIL				"GET_NET_AVAIL"

/* Sets GSM location information */
#define IOCTL_GET_GSM_LOCINFO				"GET_GSM_LOCINFO"

/* Sets GPRS location information */
#define IOCTL_GET_GPRS_LOCINFO			"GET_GPRS_LOCINFO"

/* Sets forbidden PLMN location information */
#define IOCTL_GET_FORBDN_LOCINFO			"GET_FORBDN_LOCINFO"


/*********************************************************************
 *	BCM Bluetooth & Wi-Fi IOCTL
 *********************************************************************/
#define IOCTL_SEARCH									"EX_SEARCH"
#define IOCTL_GET_SEARCH_RESULT							"GET_SEARCH_RESULT"
#define IOCTL_GET_SEARCH_CNT							"GET_SEARCH_CNT"
// Wi-Fi Only
#define IOCTL_ADD_NW_PROFILE							"ADD_NW_PROFILE"
#define IOCTL_SET_NW_PROFILE							"SET_NW_PROFILE"
#define IOCTL_GET_NW_PROFILE							"GET_NW_PROFILE"
#define IOCTL_GET_NW_PROFILE_LIST						"GET_NW_PROFILE_LIST"
#define IOCTL_GET_NW_CNT								"GET_NW_CNT"
#define IOCTL_DEL_NW_PROFILE							"DEL_NW_PROFILE"
#define IOCTL_SET_DEFAULT_PROFILE						"SET_DEFAULT_PROFILE"
#define IOCTL_SET_NW_ID									"SET_NW_ID"
#define IOCTL_GET_BSSINFO								"GET_BSSINFO"
#define IOCTL_GET_AP_MAC								"GET_AP_MAC"
#define IOCTL_GET_TXRATE								"GET_TXRATE"
// Bluetooth Only
#define IOCTL_BT_PAIR									"EX_BT_PAIR"
#define IOCTL_GET_BT_PAIR_CNT							"GET_BT_PAIR_CNT"
#define IOCTL_GET_BT_PAIRED_DEVICES						"GET_BT_PAIRED_DEVICES"
#define IOCTL_EX_BT_REMOVE_PAIR							"EX_BT_REMOVE_PAIR"
#define IOCTL_SET_CONNECT_INFO							"SET_CONNECT_INFO"
#define IOCTL_EX_BTBASE_FW_UP							"EX_BTBASE_FW_UP"
#define IOCTL_GET_BT_LOCAL_ADDRESS						"GET_LOCAL_BT_ADDRESS"
#define IOCTL_GET_BTBASE_INFO							"GET_BTBASE_INFO"
#define IOCTL_EX_BTBASE_RESTORE_FACTORY					"EX_BTBASE_RESTORE_FACTORY"
#define IOCTL_EX_BTBASE_MDM_PROF_UPDATE					"EX_BTBASE_MDM_PROF_UPDATE"
#define IOCTL_EX_SELECT_REMOTE_BT_AP					"EX_SELECT_REMOTE_BT_AP"
#define IOCTL_GET_PAN_DEVICE							"GET_PAN_DEVICE"
#define IOCTL_GET_DUN_DEVICE							"GET_DUN_DEVICE"
#define IOCTL_GET_SPP_DEVICE							"GET_SPP_DEVICE"

/* BT Port Bits: These bits are used by port_bt_bit in the structure
ddi_bt_device */
static const char BT_PAN_PORT_BIT						= (0x01 << 0);
static const char BT_MDM_PORT_BIT						= (0x01 << 1);
static const char BT_SPP1_PORT_BIT						= (0x01 << 2);
static const char BT_OBEX_PORT_BIT						= (0x01 << 3);

/* BT Port Bits: Alternate values for GET_PAN_DEVICE, GET_DUN_DEVICE and GET_SPP_DEVICE */
static const char BT_PORT_BIT_CONN					= 1; /* Port is connected */
static const char BT_PORT_BIT_NOT_CONN				= 0; /* Port is not connected */

typedef struct ddi_bss_info_t
{
	unsigned char	BSSID[6];
	char			SSID[34];
	int     		freq;
	int     		RSSI;
	char 			NWAuth[14]; 
	char			DataEncrypt[10];
	int				bss_type;
	int	    		SSID_len;
} ddi_bss_info;

typedef struct sec_key_t
{
  char	   key[4][32+4];//Key data (_WF_WEPKEY_MAXSZ (32) + 4)
  int	   key_len;//Key data length
  int	   index;//Key index
} sec_key;

typedef struct wpa_sec_key_t
{
  int      eap_type;
  char	   password[128+4];
  char	   identity[128+4];
  char	   ca_cert[128+4];//path and filename of CA certificate
  char	   client_cert[128+4];//path and filename of client certificate
  char	   private_key[128+4];// File path to client private key file (PEM/DER/PFX)
  char	   private_key_passwd[128+4];// Password for private key file
} wpa_sec_key;

typedef struct wpa_psk_key_t
{
  char	   key[64];//Key data
  int	   key_len;//Key data length
} wpa_psk_key;

typedef struct ddi_nw_profile_t
{
  int       network_id;
  unsigned char   BSSID[8]; // for internal use only ; Used for getting the BSS info (IOCTL_GET_BSSINFO)
  char	    ssid[34]; //SSID of AP
  char      flags[64];
  int	    ssid_len; //SSID length
  int       bssid_len;
  int	    auth_type;
  int       wsec;
  int       bss_type;
  int	    channel;//channel of the adhoc network
  union key_u
  {
      wpa_sec_key wpa_auth;//for wpa,wpa2-eap authentication
      sec_key wep_auth;//for wep
      wpa_psk_key wpa_psk; // for wpa,wpa2-psk authentication
  } uKey;
} ddi_nw_profile;

typedef struct ddi_nw_list_t
{
  int       network_id;
  char	    SSID[34];
  char      BSSID[6];
  char      flags[64];
  int       bssid_len;
  int	    ssid_len;
} ddi_nw_list;

// Enumeration for ddi_nw_profile->auth_type
enum 
{
	AUTH_NONE_OPEN,
	AUTH_NONE_WEP,
	AUTH_NONE_WEP_SHARED,
	AUTH_IEEE8021X,
	AUTH_WPA_PSK,
	AUTH_WPA_EAP,
	AUTH_WPA2_PSK,
	AUTH_WPA2_EAP,
#ifdef BRCM_CCX	
	AUTH_WPA_CCKM,
	AUTH_WPA2_CCKM,
#endif /* BRCM_CCX */
	AUTH_WPA_NONE
};

// Enumeration for ddi_nw_profile->bss_type
enum {
	INFRASTRUCTURE =1,
	ADHOC
};

// Enumeration for ddi_nw_profile->uKey.wpa_auth.eap_type
enum { NO_EAP, EAP_PEAP, EAP_TLS, EAP_FAST,EAP_LEAP }; 

// Enumeration for ddi_nw_profile->wsec
enum { ALGO_OFF =1, 
       ALGO_WEP1, 
       ALGO_TKIP, 
       ALGO_WEP128,
       ALGO_AES_CCM };


/* Struture used for GET_BT_DEVICES, GET_BT_PAIRED_DEVICES, BT_REMOVE_PAIR and SET_CONNECT_INFO */
typedef struct _ddi_bt_device 
{
    char bd_addr[16]; // bt address
    char bt_class[8]; // class code
    char name[64]; // friendly name
    char port_bt[16]; // Bluetooth device port
	int port_bt_bit; // Bits that give info on the device port/s configured
}ddi_bt_device;


/* Structure used for BT_PAIR IOCTL call */
typedef struct _ddi_bt_pair 
{
    char bd_addr[16];	// address of remote device
    char pin[16];		// optional pin
    char name[64];		// friendly name to use when pin is empty
}ddi_bt_pair;

typedef struct _ddi_bt_base_file_info
{
	char bd_addr[16]; // address of the remote BT device
	char filename[40]; // name of the file to be loaded
	int gid; // group ID where the file is located
}ddi_bt_base_file_info;

typedef ddi_bt_base_file_info ddi_bt_base_fw_info;


typedef struct _ddi_bt_device_query_info 
{
    char bd_addr[16]; // bt address
    char bt_class[8]; // class code
    char name[64]; // friendly name
}ddi_bt_device_query_info;

typedef struct _ddi_bt_base_info 
{
	char fw_ver[16]; //Firmware version following the format �xx.xx.xx.xx�.
	int pan_type; //PAN connection type. The following values are 1=Bridge, 2=Router or -1=Unknown 
	int eth_cable_stat; // Ethernet cable state whether connected or not connected.
	char ip_addr[16]; //Router�s IPv4 address
	char serial_no[16]; // Serial number following the format �xxx-xxx-xxx�.

}ddi_bt_base_info ;


// GPS Default Values
#define GPS_DEFAULT_POLL_TIME     1
#define GPS_DEFAULT_LSTN_PORT     2947
#define GPS_DEFAULT_LSTN_IP       "localhost"
#define GPS_DEFAULT_AGPS_SET      2
#define GPS_DEFAULT_AGPS_FILE     "I:1/xtra.bin"
#define GPS_DEFAULT_AGPS_LINK1    "xtra1.gpsonextra.net"
#define GPS_DEFAULT_AGPS_LINK2    "xtra2.gpsonextra.net"
#define GPS_DEFAULT_AGPS_LINK3    "xtra3.gpsonextra.net"

typedef struct _ddi_gps_cfg
{
	int  pollTime;        // Polling interval in seconds of receiving NMEA
	                      // sentences from the GPS port.
	int  lstnPort;        // Port number that GPSD will listen for incoming clients.
	char lstnIP[20];      // IP address that GPSD will listen for incoming clients.
	int  agpsSet;         // Enable or disable A-GPS.
	char agpsFile[20];    // Specifies the complete path and filename of the GpsOneXTRA
	                      // binary file (named xtra.bin) to use when A-GPS is enabled
	char agpsLink[3][64]; // Link to Web servers that EOS will connect to download the binary file.
} ddi_gps_cfg;


typedef struct __ddi_gps_loc
{
	double time;		// Time of update.
	double ept;			// Expected time uncertainty.
	double latitude;	// Latitude in degrees.
	double epy;			// Latitude position uncertainty in meters.
	double longitude;	// Longitude in degrees.
	double epx;			// Longitude position uncertainty in meters.
	double altitude;	// Altitude in meters.
	double epv;			// Vertical position uncertainty in meters.


}ddi_gps_loc;


#define NWTYPE_2G         2
#define NWTYPE_3G         3
#define SVCSTATE_SEARCH   1
#define SVCSTATE_NOCONN   2
#define SVCSTATE_LIMSRV   3
#define SVCSTATE_CELLRESEL 4

typedef struct ddi_StCellInfo_t
{
	int ACT;
	int ARFCN;
	int MCC;
	int MNC;
	int LAC;
	int CELL;
	int SvcState;
} ddi_StCellInfo;

#endif //_VOS_IOCLTL_H
