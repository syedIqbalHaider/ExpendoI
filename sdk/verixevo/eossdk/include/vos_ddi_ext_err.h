/*
 *	This header file contains the list of INI parameters that CommEngine can access
 */


/*********************************************************************
 *	COMMON Extended error code
 *********************************************************************/

#define E_DDI_COMMON_BASE -100
#define E_DDI_INVL_IOCTL            (E_DDI_COMMON_BASE - 1)
#define E_DDI_PORT_OPEN_FAIL        (E_DDI_COMMON_BASE - 2)
#define E_DDI_STAT_PORT_OPEN_FAIL   (E_DDI_COMMON_BASE - 3)
#define E_DDI_NO_IOCTL_ID           (E_DDI_COMMON_BASE - 4)
#define E_DDI_AT_CMD_FAIL           (E_DDI_COMMON_BASE - 5)
#define E_DDI_INI_PARAM_NOT_FOUND   (E_DDI_COMMON_BASE - 6) // DIAL, GPRS, GSM
#define E_DDI_STAT_PORT_CLOSE_FAIL  (E_DDI_COMMON_BASE - 7)
#define E_DDI_PORT_CLOSE_FAIL       (E_DDI_COMMON_BASE - 8)
#define E_DDI_NO_CARRIER            (E_DDI_COMMON_BASE - 9) // DIAL, GPRS, GSM
#define E_DDI_GENERIC_ERROR         (E_DDI_COMMON_BASE - 10)// DIAL, GPRS, GSM
#define E_DDI_NO_DIAL_TONE          (E_DDI_COMMON_BASE - 11)// DIAL, GPRS, GSM
#define E_DDI_BUSY                  (E_DDI_COMMON_BASE - 12)// DIAL, GPRS, GSM
#define E_DDI_NO_ANSWER             (E_DDI_COMMON_BASE - 13)// DIAL, GPRS, GSM
#define E_DDI_INVL_IOCTL_DATA       (E_DDI_COMMON_BASE - 14)
#define E_DDI_NO_IOCTL_DATA         (E_DDI_COMMON_BASE - 15)
#define E_DDI_INIT_AT_CMD_FAIL      (E_DDI_COMMON_BASE - 16) // DIAL, GPRS, GSM
#define E_DDI_START_AT_CMD_FAIL     (E_DDI_COMMON_BASE - 17) // DIAL, GPRS, GSM
#define E_DDI_PORT_NOT_SET	    (E_DDI_COMMON_BASE - 18) // DIAL, GPRS, GSM
#define E_DDI_DISCONNECTION_FAIL    (E_DDI_COMMON_BASE - 19) // DIAL, GPRS, GSM
#define E_DDI_NO_CONNECTION_BAUD    (E_DDI_COMMON_BASE - 20) // DIAL, GSM
#define E_DDI_NOT_CONNECTED	    (E_DDI_COMMON_BASE - 21) // DIAL, GSM
#define E_DDI_LOW_BATT		    (E_DDI_COMMON_BASE - 22) // DIAL, GSM, GPRS, ETH
#define E_DDI_CONNECT_TIMEOUT	    (E_DDI_COMMON_BASE - 23) // DIAL, GSM, GPRS
#define E_DDI_DEVICE_NOT_OPERABLE   (E_DDI_COMMON_BASE - 24) // GPRS, GSM
#define E_DDI_POWER_UP_FAIL	        (E_DDI_COMMON_BASE - 25) // GPRS, GSM 
#define E_DDI_DEVICE_POWER_FAIL	    (E_DDI_COMMON_BASE - 26) // GPRS, GSM
#define E_DDI_IOCTL_ERR		        (E_DDI_COMMON_BASE - 27) // WIFI
#define E_DDI_IOCTL_TIMEOUT         (E_DDI_COMMON_BASE - 28) // WIFI
#define E_DDI_DEVICE_DETACHED		(E_DDI_COMMON_BASE - 29) // Dial, ETH
#define E_DDI_DISCONNECTED			(E_DDI_COMMON_BASE - 30)
#define E_DDI_NO_MORE_AP			(E_DDI_COMMON_BASE - 31)
#define E_DDI_INI_ACCESS			(E_DDI_COMMON_BASE - 32)


/*********************************************************************
 *	DDI Manager Extended error code
 *********************************************************************/
#define E_DDI_DDI_MGR_BASE -200
#define E_DDI_POOR_NET_RESTORE_FAIL (E_DDI_DDI_MGR_BASE - 1)
#define E_DDI_NO_NET_RESTORE_FAIL   (E_DDI_DDI_MGR_BASE - 2)
#define E_DDI_INVL_DDI_RESP         (E_DDI_DDI_MGR_BASE - 3)
#define E_DDI_OPEN_STATE            (E_DDI_DDI_MGR_BASE - 4)
#define E_DDI_INIT_STATE            (E_DDI_DDI_MGR_BASE - 5)
#define E_DDI_DEINIT_STATE          (E_DDI_DDI_MGR_BASE - 6)
#define E_DDI_CLOSE_STATE           (E_DDI_DDI_MGR_BASE - 7)
#define E_DDI_RESET_STATE           (E_DDI_DDI_MGR_BASE - 8)


/*********************************************************************
 *	ETHERNET Extended Error code
 *********************************************************************/

#define E_DDI_ETH_BASE -300
#define E_DDI_GET_MAC_FAIL         (E_DDI_ETH_BASE - 1)
#define E_DDI_SET_RX_FAIL          (E_DDI_ETH_BASE - 2)
#define E_DDI_INVL_NET_STAT        (E_DDI_ETH_BASE - 3)
#define E_DDI_SWITCH_CMD_MODE_FAIL (E_DDI_ETH_BASE - 4)


/*********************************************************************
 *	GPRS and GSM Extended Error code
 *********************************************************************/
#define E_DDI_GSM_BASE -400
#define E_DDI_GSM_MUX_NOT_OPEN		(E_DDI_GSM_BASE - 1) 
#define E_DDI_CYCLIC_POW_SAVE		(E_DDI_GSM_BASE - 2) 
#define E_DDI_SIM_PIN_FAIL		(E_DDI_GSM_BASE - 3) 
#define	E_DDI_URC_DISABLED		(E_DDI_GSM_BASE - 4)
#define	E_DDI_URC_NOT_REG		(E_DDI_GSM_BASE - 5)
#define E_DDI_PDP_NOT_SET		(E_DDI_GSM_BASE - 6) 
#define E_DDI_NO_BATT			(E_DDI_GSM_BASE - 7) 
#define E_DDI_NO_SIM			(E_DDI_GSM_BASE - 8) 
#define E_DDI_AUTO_BAUD_FAIL		(E_DDI_GSM_BASE - 9) 
#define E_DDI_BAUD_NOT_SET		(E_DDI_GSM_BASE - 10) 


/*********************************************************************
 *	CDMA Extended Error code
 *********************************************************************/
#define E_DDI_CDMA_BASE 500


/*********************************************************************
 *	DIAL Extended Error code
 *********************************************************************/
#define E_DDI_DIAL_BASE -600
#define E_DDI_BAUDRATE_NOT_SUPPORTED		(E_DDI_DIAL_BASE - 1)
#define E_DDI_FORMAT_NOT_SUPPORTED		(E_DDI_DIAL_BASE - 2)
#define E_DDI_SDLC_INITIALIZATION_FAIL	        (E_DDI_DIAL_BASE - 3)
#define E_DDI_CONNECT_RESPONSE_ERROR		(E_DDI_DIAL_BASE - 4)
#define E_DDI_RESPONSE_CODE_NOT_FOUND		(E_DDI_DIAL_BASE - 5)
#define E_DDI_SWITCH_TO_SDLC_FAIL		(E_DDI_DIAL_BASE - 6)
#define E_DDI_DCD_SIGNAL_NOT_SET_TO_ON	        (E_DDI_DIAL_BASE - 7)
#define E_DDI_NO_LINE				(E_DDI_DIAL_BASE - 8)
#define E_DDI_INVALID_DIAL_TYPE			(E_DDI_DIAL_BASE - 9)
#define E_DDI_NO_PRIMARY_NUMBER			(E_DDI_DIAL_BASE - 10)
#define E_DDI_CALL_ABORT_FAIL			(E_DDI_DIAL_BASE - 11)
#define E_DDI_LINE_IN_USE				(E_DDI_DIAL_BASE - 12)

/*********************************************************************
 *	Wifi Extended Error code
 *********************************************************************/
#define E_DDI_WIFI_BASE -700
#define E_DDI_CTRL_CONN_FAILED  		(E_DDI_WIFI_BASE - 1)
#define E_DDI_MON_CONN_FAILED		    (E_DDI_WIFI_BASE - 2)
#define E_DDI_MON_ATTACH_FAIL	        (E_DDI_WIFI_BASE - 3)
#define E_DDI_SUPP_INVALID_RESP 		(E_DDI_WIFI_BASE - 4)
#define E_DDI_CTRL_REQ_TIMEOUT  		(E_DDI_WIFI_BASE - 5)
#define E_DDI_CTRL_REQ_FAIL     		(E_DDI_WIFI_BASE - 6)

#define E_DDI_NW_CONNECTED              (E_DDI_WIFI_BASE - 13)
#define E_DDI_NWPROF_EXIST              (E_DDI_WIFI_BASE - 14)
#define E_DDI_INVALID_NWPROF            (E_DDI_WIFI_BASE - 15)
#define E_DDI_INVALID_NW_ID             (E_DDI_WIFI_BASE - 16)
#define E_DDI_NW_ID_NOT_SET             (E_DDI_WIFI_BASE - 17)
#define E_DDI_SSID_NOT_SET              (E_DDI_WIFI_BASE - 18)
#define E_DDI_INVALID_CHANNEL           (E_DDI_WIFI_BASE - 19)
#define E_DDI_INVALID_AUTH_TYPE         (E_DDI_WIFI_BASE - 20)
#define E_DDI_INVALID_ALGO_TYPE         (E_DDI_WIFI_BASE - 21)
#define E_DDI_INVALID_BSS_TYPE          (E_DDI_WIFI_BASE - 22)
#define E_DDI_INVALID_WEPKEY_IDX        (E_DDI_WIFI_BASE - 23)
#define E_DDI_INVALID_WEPKEY            (E_DDI_WIFI_BASE - 24)
#define E_DDI_NO_NW_PROFILE		(E_DDI_WIFI_BASE - 25)
     
/*********************************************************************
 *	BLUETOOTH Extended Error code
 *********************************************************************/   
#define E_DDI_BT_BASE -800
#define E_DDI_DEV_INQUIRY_FAIL			(E_DDI_BT_BASE - 1)
#define E_DDI_PAIR_FAIL					(E_DDI_BT_BASE - 2)
#define E_DDI_PAIR_TIMEOUT				(E_DDI_BT_BASE - 3)
#define E_DDI_INITIATE_PAIRING_FAIL		(E_DDI_BT_BASE - 4)
#define E_DDI_PAIR_AUTHENTICATION_FAIL	(E_DDI_BT_BASE - 5)
#define E_DDI_BT_ADDRESS_NOT_PRESENT	(E_DDI_BT_BASE - 6)
#define E_DDI_GET_SEARCH_COUNT_FAIL		(E_DDI_BT_BASE - 7)
#define E_DDI_GET_PAIR_COUNT_FAIL		(E_DDI_BT_BASE - 8)
#define E_DDI_REMOVE_PAIR_FAIL			(E_DDI_BT_BASE - 9)
#define E_DDI_SET_CONNECT_INFO_FAIL		(E_DDI_BT_BASE - 10)
#define E_DDI_GET_SEARCH_RESULT_FAIL	(E_DDI_BT_BASE - 11)
#define E_DDI_GET_PAIR_DEVICES_FAIL		(E_DDI_BT_BASE - 12)
#define E_DDI_EXCEEDS_PAIR_MAX			(E_DDI_BT_BASE - 13)
#define E_DDI_CREATE_PIPE_FAIL			(E_DDI_BT_BASE - 14)
#define E_DDI_CLOSE_PIPE_FAIL			(E_DDI_BT_BASE - 15)
#define E_DDI_END_BOND_FAIL				(E_DDI_BT_BASE - 16)
#define E_DDI_BT_CONNECT_FAIL			(E_DDI_BT_BASE - 17)
#define E_DDI_LOAD_MDM_PROFILE_FAIL		(E_DDI_BT_BASE - 18)
#define E_DDI_REMOTE_BT_DEVICE_NOT_READY (E_DDI_BT_BASE - 19)
#define E_DDI_BT_FIRMWARE_UPGRADE_FAIL	(E_DDI_BT_BASE - 20)
#define E_DDI_GET_LOCAL_BT_ADDRESS_FAIL (E_DDI_BT_BASE - 21)
#define E_DDI_GET_FW_VERSION_FAIL		(E_DDI_BT_BASE - 22)
#define E_DDI_BT_DEV_PORT_NOT_SET		(E_DDI_BT_BASE - 23)
#define E_DDI_BT_RESTORE_FACTORY_FAIL	(E_DDI_BT_BASE - 24)
#define E_DDI_GET_BT_AP_INFO_FAIL		(E_DDI_BT_BASE - 25)
#define E_DDI_SELECT_REMOTE_BT_AP_FAIL	(E_DDI_BT_BASE - 26)
#define E_DDI_GET_CONNECTED_DEV_FAIL	(E_DDI_BT_BASE - 27)

/*********************************************************************
 *	GPS Extended Error code
 *********************************************************************/   
#define E_DDI_GPS_BASE -900
#define E_DDI_ALREADY_STARTED			(E_DDI_GPS_BASE - 1)
#define E_DDI_ALREADY_STOPPED			(E_DDI_GPS_BASE - 2)
#define E_DDI_BIN_FILE_ACCESS			(E_DDI_GPS_BASE - 3)
#define E_DDI_WEB_DLD_FAILED			(E_DDI_GPS_BASE - 4)
#define E_DDI_BIN_FILE_OUTDATED			(E_DDI_GPS_BASE - 5)
#define E_DDI_GPS_LICENSE_NOT_FOUND		(E_DDI_GPS_BASE - 6)
#define E_DDI_GPS_PORT_IN_USE			(E_DDI_GPS_BASE - 7)

