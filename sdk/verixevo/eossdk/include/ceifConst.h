//----------------------------------------------------------------------
// Copyright (C) 2009 by VeriFone Inc. All rights reserved.
//
// No part of this software may be used, stored, compiled, reproduced,
// modified, transcribed, translated, transmitted, or transferred, in
// any form or by any means  whether electronic, mechanical,
// magnetic, optical, or otherwise, without the express prior written
// permission of VeriFone, Inc.
//----------------------------------------------------------------------

#ifndef __CEIFCONST_H
#define __CEIFCONST_H

#ifdef __cplusplus
extern "C" {
#endif

//----------------------------------------------------------------------
// Parameter Minimum and Maximum Length
//----------------------------------------------------------------------

#define MAX_USERNAME	(64+1)
#define MAX_PASSWORD	(64+1)
#define MAX_DRIVER_LEN	18
#define MAX_DATE_LEN	14
#define MAX_IP_LEN		(16+1)
#define MAX_ERR_LEN		21

// For Auto-reconnection parameters
#define MAX_RECONN_MAX_VALUE		10
#define MAX_RECONN_INTERVAL_VALUE	3600
#define MAX_RECONN_TO_VALUE			18000
#define MAX_RECONN_EXEMPT_LEN		500

// For DUAL SIM parameters
#define DUALSIM_INTERVAL_MAX_VALUE	3600
#define DUALSIM_INTERVAL_MIN_VALUE	5
#define DUALSIM_REVERT_TO_MIN_VALUE	15
#define DUALSIM_REVERT_TO_MAX_VALUE	7205

#define CEIF_PARAM_NAME_LEN	64
#define CEIF_PARAM_NAME_SZ	(CEIF_PARAM_NAME_LEN+1)
#define CEIF_PARAM_VALUE_SZ	512
#define CEIF_EX_CMD_SZ			CEIF_PARAM_VALUE_SZ

#define CEIF_EVT_DATA_SZ	512 // Size of application event from DDI Driver

//----------------------------------------------------------------------
// Network Configuration
//----------------------------------------------------------------------

// Network Info
typedef struct
{
  unsigned int niHandle;		// Handle to Network Interface
  char niCommTech[16];            // Null terminted str.
  char niDeviceName[32];         // /DEV/COMx /DEV/WLN1 /DEV/ETH1
  unsigned int niRunState;		// 1-Running, 2-Ready, 3-Not Ready, 0-Failed to run
  int niErrorCode;				// Error code associated with 
  unsigned int niStartUpMode;	// 0-Auto startup, 1-Manual startup
  char niDeviceDriverName[MAX_DRIVER_LEN+1];  // Format <[2 char media type] [2 char vendor ID][4 char device id]>.lib
  int niDeviceDriverType;		//ETHERNET / PPP / DIAL
} stNIInfo;

// IP Configuration
typedef struct
{
  unsigned int ncDHCP;		// 1 = DHCP, 0 = Static
  unsigned long ncIPAddr;	//Mandatory is Static IP
  unsigned long ncSubnet;	//Mandatory if Static IP
  unsigned long ncGateway; //Mandatory if Static IP
  unsigned long ncDNS1;	//Optional but usually required
  unsigned long ncDNS2;	//Optional
  char dhcpLeaseStartTime[MAX_DATE_LEN+1];	// YYYYMMDDHHMMSS format (read only)
  char dhcpLeaseEndTime[MAX_DATE_LEN+1];	// YYYYMMDDHHMMSS format (read only)
  unsigned int ncIsConnected; // Yes or No
} stNI_IPConfig;

// PPP Configuration Authentication Types
enum pppAuthType 
{
  PPP_AUTH_NONE	= 1, 
  PPP_AUTH_PAP		= 2, 
  PPP_AUTH_CHAP	= 3, 
  PPP_AUTH_MSCHAP	= 4
};

// PPP Configuration
typedef struct
{
  enum pppAuthType ncAuthType;	// Type of PPP authentication
  char ncUsername[MAX_USERNAME]; // PPP username, optional
  char ncPassword[MAX_PASSWORD]; // PPP password, optional
} stNI_PPPConfig;

// Network Event
typedef struct
{
  unsigned int niHandle;  // Network Interface handle
  unsigned int neEvt;     // Network Interface event, see CommEngine Events 
  int neParam1;           // Where applicable. Used with CE_EVT_SIGNAL (signal in percentage) and CE_EVT_NET_FAILED (see ceAPI / DDI Driver list of error codes).
  int neParam2;           // Where applicable. Used with CE_EVT_SIGNAL (signal in dBm).
  int neParam3;		//Where applicable. Used with CE_EVT_SIGNAL (signal in RSSI).
} stceNWEvt;

//Signal Levels
typedef struct
{
	int iSignal1;		//signal in percentage
	int iSignal2;		//signal in dBm
	int iSignal3;		//signal in RSSI
}stSignalLevel;

//NWIF State
typedef struct		
{		
  int nsTargetState;	// Target State. See NWIF_CONN_STATE_*.
  int nsCurrentState; 	// Current State. See NWIF_CONN_STATE_*.
  unsigned int nsEvent;	// Last Broadcast event.
  int nsErrorCode;	// Error / failure code if an error occurred, zero otherwise.
  char nsErrorString[MAX_ERR_LEN];	 // This will hold the last known error string (if a CE_EVT_NET_FAILED was sent) or network outage string (if a CE_EVT_NET_OUT / CE_EVT_NET_POOR is sent) or network restore string (if a CE_EVT_NET_RES is sent). Null if there is no error.
  char nsTimeStamp[MAX_DATE_LEN+1];	//	 Timestamp for when the last event was sent. Timestamp in YYYYMMDDhhmmss format. This is a null terminated string.
} stNI_NWIFState;	

//Bluetooth Parameter
typedef struct		
{		
	char bcDevPort[16]; // Virtual port to use
} stNI_BTConfig;

enum btDialDevice
{
	BT_VX680_BASE	= 1, 
	BT_MOBILE_PHONE	= 2
};

//----------------------------------------------------------------------
// String Constants
//----------------------------------------------------------------------

//Set / Get Network Parameter macro
#define IP_CONFIG		"IP_CONFIG"
#define PPP_CONFIG		"PPP_CONFIG"
#define SIGNAL_LEVEL	"SIGNAL_LEVEL"
#define AUTO_RECONNECT	"AUTO_RECONNECT"
#define NWIF_STATE		"NWIF_STATE"
#define BT_CONFIG		"BT_CONFIG"
#define BT_SUPPORT		"BT_SUPPORT"
#define BT_DIAL_DEVICE	"BT_DIAL_DEVICE"

//DUAL SIM Parameters
#define IP_CONFIG2		"IP_CONFIG2"
#define PPP_CONFIG2		"PPP_CONFIG2"
#define DSIM_AUTO_SWITCH_MODE				"DSIM_AUTO_SWITCH_MODE"
#define DSIM_AUTO_SWITCH_NET_RES_INTERVAL	"DSIM_AUTO_SWITCH_NET_RES_INTERVAL"
#define DSIM_AUTO_SWITCH_REVERT_INTERVAL	"DSIM_AUTO_SWITCH_REVERT_INTERVAL"
#define DSIM_AUTO_SWITCH_REVERT_TO			"DSIM_AUTO_SWITCH_REVERT_TO"

// Dual SIM auto-switch modes
#define DSIM_AUTO_SWITCH_DISABLED	0
#define DSIM_AUTO_SWITCH_MODE1		1
#define DSIM_AUTO_SWITCH_MODE2		2

//Connection Maintenance
#define RECONN_MAX			"RECONN_MAX"
#define RECONN_INTERVAL 	"RECONN_INTERVAL"
#define RECONN_TO			"RECONN_TO"
#define RECONN_EXEMPT		"RECONN_EXEMPT"

//Set / Get Device Driver Parameter macro
#define FACTORY_DEFAULT	"FACTORY_DEFAULT"

//CommTech strings
#define CE_COMM_TECH_ETH		"Ethernet"
#define CE_COMM_TECH_PPPDIAL	"PPP/Dial"
#define CE_COMM_TECH_DIALONLY "Dial"
#define CE_COMM_TECH_GPRS		"GPRS"
#define CE_COMM_TECH_PPPGSM	"PPP/GSM"
#define CE_COMM_TECH_GSMONLY	"GSM"
#define CE_COMM_TECH_CDMA		"CDMA"
#define CE_COMM_TECH_WIFI		"WiFi"
#define CE_COMM_TECH_BT		"Bluetooth"
#define CE_COMM_TECH_ETHBT	"Ethernet/BT"
#define CE_COMM_TECH_PPPBT	"PPP/BT"
#define CE_COMM_TECH_DIALBT	"Dial/BT"

//CONFIG.SYS parameter strings
//These are the configuration section names
#define CE_CT_CFG_PREFIX_ETH      	"ETH"
#define CE_CT_CFG_PREFIX_PPPDIAL   "PPPDIAL"
#define CE_CT_CFG_PREFIX_DIAL     	"DIAL"
#define CE_CT_CFG_PREFIX_GPRS     	"GPRS"
#define CE_CT_CFG_PREFIX_PPPGSM    "PPPGSM"
#define CE_CT_CFG_PREFIX_GSM      	"GSM"
#define CE_CT_CFG_PREFIX_CDMA     	"CDMA"
#define CE_CT_CFG_PREFIX_BT		"BT"
#define CE_CT_CFG_PREFIX_ETHBT	"ETHBT"
#define CE_CT_CFG_PREFIX_PPPBT		"PPPBT"
#define CE_CT_CFG_PREFIX_DIALBT	"DIALBT"
#define CE_CT_CFG_PREFIX_WIFI         "WIFI"

//CONFIG.SYS variables
//These are the configuration key names
#define CE_CT_CFG_SUFFIX_STARTMODE	"startmode"
#define CE_CT_CFG_SUFFIX_DHCP			"dhcp"
#define CE_CT_CFG_SUFFIX_IPADDRESS	"ipaddr"
#define CE_CT_CFG_SUFFIX_SUBNETMASK	"subnet"
#define CE_CT_CFG_SUFFIX_GATEWAY		"gateway"
#define CE_CT_CFG_SUFFIX_DNS1			"dns1"
#define CE_CT_CFG_SUFFIX_DNS2			"dns2"
#define CE_CT_CFG_SUFFIX_USERNAME	"username"
#define CE_CT_CFG_SUFFIX_PASSWORD	"password"
#define CE_CT_CFG_SUFFIX_AUTHTYPE		"authtype"
#define CE_CT_CFG_SUFFIX_DEV_PORT		"devPort"
#define CE_CT_CFG_SUFFIX_DIAL_DEVICE	"dialDev"

//Error / Network string macros
#define ERR_TCPIP_FAILURE					"TCPIP Failure"
#define ERR_PPP_AUTH_FAILURE				"PPP Auth Failure"
#define ERR_CONNECTION_LOST				"Connection Lost"
#define ERR_TCPIP_LANDLINE_OPEN_FAILURE	"TCP PPP Open Fail"
#define ERR_DLL_LOAD_ERROR					"DLL Load Error"
#define ERR_DRIVER_INTERNAL_FAILURE		"Driver failure"
#define ERR_NO_NETSTATUS					"Net Status Unknown"
#define ERR_ETH_HOSTDOWN					"ETH Host Down"
#define ERR_ETH_TIMEOUT					"ETH Timeout"
#define ERR_HARDWARE_INIT_FAILURE		"Hardware Init Fail"
#define ERR_PPP_TIMEOUT_FAILURE			"PPP Timeout"

//----------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------
#define CEIF_DEVICE_TIMEOUT_MIN   30 // Time in seconds
#define CEIF_DEVICE_TIMEOUT_MAX  600 // Time in seconds
#define CEIF_VERSION_STR_LEN	31 // Version String Length
#define CE_VERSION_STR_LEN        31 // Version String Length

// Starting and Stopping the connection
#define CE_CONNECT	10 // Establish the full connection (normal operation).
#define CE_DISCONNECT 11 // Disconnect and close the communication device (normal operation)
#define CE_OPEN		12 // Open and prep the communication device.
#define CE_LINK		13 // On PPP devices the data connection is established.
#define CE_NETWORK	14 // Establish the network connection.
#define CE_CLOSE	15 // Close the device.

// NWIF Start mode
#define CE_SM_AUTO		0 // Start Mode - Automatic
#define CE_SM_MANUAL	1 // Start Mode - Manual

// Signal Strength Notification Flag
// NOTE: CE_SF_VALUE is only for range checking
#define CE_SF_VALUE		CE_SF_ON
#define CE_SF_ON		1 // Notifications are sent.
#define CE_SF_OFF		0 // No notifications are sent.

// Component Name for Version Information
#define CE_VER_CEIF         1 // CommEngine Interface Library
#define CE_VER_VXCE         2 // CommEngine

// NWIF Run State (see struct element stNIInfo.niRunState)
#define CE_RUN_STATE_FAILED		0 // NWIF failed to start
#define CE_RUN_STATE_RUNNING		1 // NWIF is successfully running
#define CE_RUN_STATE_READY		2 // NWIF is not running and is pending start
#define CE_RUN_STATE_NOT_READY	3 // NWIF is cannot run as device is unavailable

// Device Driver Type
#define CE_DRV_TYPE_ETHERNET	1	//Driver Type - Ethernet
#define CE_DRV_TYPE_PPP		2	//Driver Type - PPP
#define CE_DRV_TYPE_DIAL     	3	//Driver Type - Dial
#define CE_DRV_TYPE_BT			4	//Driver Type - Bluetooth

//PreDial Options
//Indicates if ceStartDialIF() or ceStopDialIF() is blocking / non-blocking
#define CE_DIALONLY_BLOCKING		0
#define CE_DIALONLY_NONBLOCKING	1

//NWIF Connection States
#define NWIF_CONN_STATE_INIT	0	//DDI Driver is not yet loaded.
#define NWIF_CONN_STATE_OPEN	1	//NWIF has now been initialized.
#define NWIF_CONN_STATE_LINK	2	//NWIF has established data link connection.
#define NWIF_CONN_STATE_NET 	3	//TCPIP is up and running.
#define NWIF_CONN_STATE_ERR	-1	//An error has occurred.

//Battery Threshold MIN and MAX values
//Note: CE_BATT_LEVEL_MIN and _MAX are only for range checking
#define CE_BATT_LEVEL_MIN	10
#define CE_BATT_LEVEL_MAX	100

//Type of reconnection pending
#define CE_PENDING_AUTO_RECONN	1
#define CE_PENDING_SIM_SWITCH		2

//----------------------------------------------------------------------
// CommEngine Events
//----------------------------------------------------------------------
#define CE_EVT_NO_EVENT 0x0000 //No broadcast event has been sent by CE.
#define CE_EVT_NET_UP     0x0001 //Broadcast: Sent when network is up and running.
#define CE_EVT_NET_DN     0x0002 //Broadcast: Sent when the network is torn down.
#define CE_EVT_NET_FAILED 0x0003 //Broadcast: Sent when attempting to establish the network, the connection failed. The extended event data contains the error string or the reason for a failure.
#define CE_EVT_NET_OUT    0x0004 //Broadcast: The network is up but not active. This event is sent when there is either no coverage (cellular) or out of range (WiFi) or cable has been removed (Ethernet). This is a temporary "outage"
#define CE_EVT_NET_RES    0x0005 //Broadcast: The network is restored and is running. This event usually follows CE_EVT_NET_OUT or CE_EVT_NET_POOR. This event occurs after the outage has been fixed.
#define CE_EVT_SIGNAL     0x0006 //Subscribed: The event data contains the signal strength as percentage and the signal strength in dBm. The extended event data will contain the actual RSSI value.
#define CE_EVT_START_OPEN 0x0007 //Single App: Posted to requesting app after successful ceStartNWIF(CE_OPEN).
#define CE_EVT_START_LINK 0x0008 //Single App: Posted to requesting app after successful ceStartNWIF(CE_LINK).
#define CE_EVT_START_NW   0x0009 //Single App: Posted to requesting app after successful ceStartNWIF(CE_NETWORK) or ceStartNWIF(CE_CONNECT).
#define CE_EVT_START_FAIL 0x000A //Single App: Posted to requesting app after failed ceStartNWIF().
#define CE_EVT_STOP_NW    0x000B //Single App: Posted to requesting app after successful ceStopNWIF(CE_NETWORK).
#define CE_EVT_STOP_LINK  0x000C //Single App: Posted to requesting app after successful ceStopNWIF(CE_LINK).
#define CE_EVT_STOP_CLOSE 0x000D //Broadcast: Sent once the interface has been de-initialized. This is received after successful ceStopNWIF(CE_CLOSE) or ceStopNWIF(CE_DISCONNECT).  This can also be received if there are errors encountered during ceStartNWIF(), ceStopNWIF() or ceStartDialIf()/ceStopDialIF() if it is set to non-blocking mode.
#define CE_EVT_STOP_FAIL  0x000E //Single App: Posted to requesting app after failed ceStopNWIF().
#define CE_EVT_DDI_APPL   0x000F //Subscribed: The extended event data contains the ddi application event data.
#define CE_EVT_START_DIAL	0x0010 //Single App: Posted to requesting app after ceStartDialIF() if it is non-blocking. neParam1 may contain the dial device handle or the error code in case of failure.
#define CE_EVT_NET_POOR	0x0011 //Broadcast: The network is up but is in a poor condition. This event is sent when the coverage is below the minimum threashold.
#define CE_EVT_PROCESSING_NET_UP 0x0012 //Broadcast: Sent when CE has received a request for CE_CONNECT or CE is auto-reconnecting from a CE_CLOSE state
#define CE_EVT_PROCESSING_NET_DN 0x0013 //Broadcast: Sent when CE has received a request for CE_CLOSE ot CE is tearing down the connection to a CE_CLOSE state
#define CE_EVT_RECONNECT_PENDING 0x0014 //Broadcast: Sent when CE has a pending reconnection attempt for a NWIF that failed to connect

//----------------------------------------------------------------------
// ceAPI Return / Error Codes
//----------------------------------------------------------------------
#define ECE_SUCCESS           0 // Success return code
#define ECE_NO_MEMORY     -1000 // No memory
#define ECE_REGAGAIN      -1001 // Application is attempting to register again after a successful registration. If necessary unregister by calling ceAPI ceUnregister() and register again using API ceRegister()
#define ECE_REGFAILED     -1002 // Registration failed as CommEngine is not running.
#define ECE_CREATEPIPE    -1003 // Application creates a pipe as part of the registration process. This error is returned if the API has failed to create a pipe.
#define ECE_CREATEMON     -1004 // Appliation creates a monitoring thread as part of the registration. This error occur if the monitor task has failed.
#define ECE_SENDCEREQ     -1005 // Application unable to send request event to CommEngine
#define ECE_RESPFAIL      -1006 // Application failed to receive a response
#define ECE_TIMEOUT       -1007 // No response, timeout error
#define ECE_NOTREG        -1008 // Application has not successfully registered with CommEngine. This error returned if an application is attempting a ceAPI prior to successful registration.
#define ECE_DEVNAMELEN    -1009 // Device name length is beyond limits. Verify device name is accurate and null terminated.
#define ECE_PARAM_NULL    -1010 // Parameter is null when one is not expected.
#define ECE_PARAM_INVALID	-1011 // Parameter is invalid or out of range
#define ECE_VXNCP_PIPE    -1012 // Pipe VxNCP is not running
#define ECE_VXNCP_PIPECR	-1013 // Unable to create temp pipe to create with VxNCP
#define ECE_VXNCP_CONSOLE	-1014 // Application is not console owner
#define ECE_VER_UNAVAIL	-1015 // CommEngine version information is unvailable
#define ECE_CDMAPP	-1016 // Application has failed to register as CommEngine's communication device management application. Either another application has successful registered or this application is attempting more than once. See API ceSetCommDevMgr() for additional details
#define ECE_NOCDM	-1017 // Application is attempting to register itself as CommEngine's communication device management application when none is required. See API ceSetCommDevMgr() for additional details.
#define ECE_DEVNAME	-1018 // Unknown device name. This is not a communication device that CommEngine is aware of.
#define ECE_DEVOWNER	-1019 // CommEngine is not the owner of the device. This is a communication device that CommEngine is aware of but currently not is its possession.
#define ECE_DEVBUSY		-1020 // CommEngine is currently the owner of this device. CommEngine is unable to release this device as it is busy. This error is returned if there are open sockets.
#define ECE_NOTASKID	-1021 // Task Id provided as parameter does not exist.
#define ECE_NODEVREQ	-1022 // No prior device request was made. Nothing to cancel.
#define ECE_CANNOT_UNREG	-1023 // Application cannot unregister for one of the following reasons: (a) Application has registered itself as CommEngine's device management application. (b) This application has devices checked out or has pending requests for devices. Check in all devices or cancel all requests before attempting to unregister again.
#define ECE_NOT_SUPPORTED	-1024 // The feature requested is not supported. Check version information of ceAPI and CommEngine.
#define ECE_NWIF_BUSY     -1025 // The configuration cannot be changed as the network interface is in a run state. To change configuration -- stop, change configuration and restart.
#define ECE_DD_NOT_READY	-1026 // The Device Driver is not in a state to provide response to parameter requests. A case may be is when an application is trying to do an ioclt() to a Driver that is not yet ready.
#define ECE_NO_CE_EVENT	-1027 // No CommEngine Notification Event in queue or Events are not enabled
#define ECE_EX_CMD_FAILED	-1028 // Command Execute Failed
#define ECE_NOTDEVOWNER	-1029 //Application did not check out this device or application is attempting to check in a device that is did not check out apriori
#define ECE_PARAM_NOT_FOUND	-1030 // Parameter not found
#define ECE_PARAM_READ_ONLY	-1031 // Parameter is read only
#define ECE_PARAM_SAVE_FAIL	-1032 // Saving to Delta file failed
#define ECE_STATE_ERR	-1033 // start/stop nwif state error
#define ECE_EVENTS_AGAIN	   -1034 //Application is attempting to enable events again after a successful event enabling.
#define ECE_DIAL_ONLY_FAILED	-1035 //ceStartDialIF() failed
#define ECE_SUBNET_MASK_INVALID	-1036 //An invalid subnet mask was passed
#define ECE_START_STOP_BUSY	-1037 //A previous call to ceStartNWIF() or ceStopNWIF() is still being processed.
#define ECE_PPP_AUTH_FAILURE	-1038 //This is returned in as a value of neParam1 in the structure stceNWEvt.
#define ECE_INIT_FAILURE		-1039 //This is returned when ddi driver can not be initialized.
#define ECE_SIGNAL_FAILURE		-1040 //This is returned when query of signal level failed.
#define ECE_TCPIP_STACK_FAILURE	-1041 //This is returned in as a value of neParam1 in the structure stceNWEvt when a TCPIP Stack error occurred. Possible reasons are: (a) TCPIP stack APIs returned an error. (b) A PPP stack event of LL_OPEN_FAILED was received.
#define ECE_CONNECTION_LOST	-1042 //This is returned in as a value of neParam1 in the structure stceNWEvt when a connection lost happens.
#define ECE_ETHERNET_HOSTDOWN	-1043 //This is returned in as a value of neParam1 in the structure stceNWEvt when Ethernet Renewal failed because the DHCP renewal re-trans-mechanism timed out (and the server never responded).
#define ECE_ETHERNET_REBIND_TIMEOUT	-1044 //This is returned in as a value of neParam1 in the structure stceNWEvt when rebind failed or the address expired before the rebind completed.
#define ECE_BATTERY_THRESHOLD_FAILURE	-1045 //This is returned in the event that OS API set_battery_value() fails or if no battery is attached to the terminal.
#define ECE_PPP_TIMEOUT_FAILURE	-1046 //This is returned as a value of neParam1 in the structure stceNWEvt.
#define ECE_IOCTL_FAILURE	-1047 //This is returned if a call to ddi_ioctl() returned error.
#define ECE_RESTORE_DEFAULT_FAILURE	-1048 //This is returned if restoring factory default has failed. Probable cause is due to IOCTL to restore defaults returned failure.
#define ECE_PARAM_WRITE_ONLY	-1049 // Parameter is write only

#ifdef __cplusplus
}
#endif

#endif // __CEIFCONST_H

