/********************************
* <ctype.h>  Character Handling *
********************************/

#ifndef __CTYPE
#define __CTYPE

#ifndef __CTYPE_DECLS
#define __CTYPE_DECLS

#undef __CLIBNS
#ifdef __cplusplus
namespace std {
#define _INLINE static inline
#define __CLIBNS ::std::
extern "C" {
#else /* ndef __cplusplus */
#define _INLINE static __inline
#define __CLIBNS
#endif /* ndef __cplusplus */

#define _U      0x01    /* upper case letter */
#define _L      0x02    /* lower case letter */
#define _N      0x04    /* digit [0 - 9] */
#define _S      0x08    /* white space */
#define _P      0x10    /* all chars that are not control or alphanumeric */
#define _C      0x20    /* control character */
#define _B      0x40    /* just the space (0x20) character */
#define _X      0x80    /* hexadecimal digit */
extern const char _uctype[];

#ifdef __cplusplus
    _INLINE int iscntrl(int c) 
    {return ((__CLIBNS _uctype+1)[(unsigned char)(c)] & _C);}
    _INLINE int isupper(int c) 
    {return ((__CLIBNS _uctype+1)[(unsigned char)(c)] & _U);}
    _INLINE int islower(int c) 
    {return ((__CLIBNS _uctype+1)[(unsigned char)(c)] & _L);}
    _INLINE int isdigit(int c) 
    {return ((__CLIBNS _uctype+1)[(unsigned char)(c)] & _N);}
    _INLINE int isxdigit(int c) 
    {return ((__CLIBNS _uctype+1)[(unsigned char)(c)] & _X);}
    _INLINE int isspace(int c) 
    {return ((__CLIBNS _uctype+1)[(unsigned char)(c)] & _S);}
    _INLINE int ispunct(int c) 
    {return ((__CLIBNS _uctype+1)[(unsigned char)(c)] & _P);}
    _INLINE int isalpha(int c) 
    {return ((__CLIBNS _uctype+1)[(unsigned char)(c)] & (_U | _L));}
    _INLINE int isalnum(int c) 
    {return ((__CLIBNS _uctype+1)[(unsigned char)(c)] & (_U | _L | _N));}
    _INLINE int isgraph(int c) 
    {return ((__CLIBNS _uctype+1)[(unsigned char)(c)] & (_U | _L | _N | _P));}
    _INLINE int isprint(int c) 
    {return ((__CLIBNS _uctype+1)[(unsigned char)(c)] & (_U | _L | _N | _P | _B));}
    _INLINE int isascii(int c) {return ((unsigned)(c)<=0x7f);}
    _INLINE int toascii(int c) {return ((c) & 0x7f);}
#else
    int iscntrl( int );
    #define iscntrl(c)      ((_uctype+1)[(unsigned char)(c)] & _C)
    int isupper( int );
    #define isupper(c)      ((_uctype+1)[(unsigned char)(c)] & _U)
    int islower( int );
    #define islower(c)      ((_uctype+1)[(unsigned char)(c)] & _L)
    int isdigit( int );
    #define isdigit(c)      ((_uctype+1)[(unsigned char)(c)] & _N)
    int isxdigit(int );
    #define isxdigit(c)     ((_uctype+1)[(unsigned char)(c)] & _X)
    int isspace( int );
    #define isspace(c)      ((_uctype+1)[(unsigned char)(c)] & _S)
    int ispunct( int );
    #define ispunct(c)      ((_uctype+1)[(unsigned char)(c)] & _P)
    int isalpha( int );
    #define isalpha(c)      ((_uctype+1)[(unsigned char)(c)] & (_U | _L))
    int isalnum( int );
    #define isalnum(c)      ((_uctype+1)[(unsigned char)(c)] & (_U | _L | _N))
    int isgraph( int );
    #define isgraph(c)      ((_uctype+1)[(unsigned char)(c)] & (_U | _L | _N | _P))
    int isprint( int );
    #define isprint(c)      ((_uctype+1)[(unsigned char)(c)] & (_U | _L | _N | _P | _B))
#if !defined(__STRICT_ANSI__) && !defined(_ANSI_STRICT)
    int isascii( int );
    #define isascii(c)      ((unsigned)(c)<=0x7f)
    int toascii( int );
    #define toascii(c)      ((c) & 0x7f)
#endif	//__strict_ansi__

#endif	//__cplusplus
int toupper( int );
int tolower( int );

#ifdef __cplusplus
}  /* extern "C" */
}  /* namespace std */
#endif
#endif /* __STDIO_DECLS */

  #if defined(__cplusplus) && !defined(__CTYPE_NO_EXPORTS)
    using ::std::isalnum;
    using ::std::isalpha;
    using ::std::iscntrl;
    using ::std::isdigit;
    using ::std::isgraph;
    using ::std::islower;
    using ::std::isprint;
    using ::std::ispunct;
    using ::std::isspace;
    using ::std::isupper;
    using ::std::isxdigit;
    using ::std::tolower;
    using ::std::toupper;
    using ::std::_uctype;
#if !defined(__STRICT_ANSI__) && !defined(_ANSI_STRICT)
    using ::std::isascii;
    using ::std::toascii;
#endif
  #endif

#endif
