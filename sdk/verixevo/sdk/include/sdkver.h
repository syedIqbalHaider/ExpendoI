/* sdkver.H - defines the Verix V SDK version */
/* Copyright 2008-2011 by VeriFone, Inc.  All Rights Reserved. */

#ifndef _SDKVER_H
#define _SDKVER_H

#define VVSDK_VER   "3.8.3"

#endif
