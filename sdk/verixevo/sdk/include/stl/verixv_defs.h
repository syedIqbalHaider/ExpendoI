/*
 * File to have Windows CE Toolkit for VC++ 5.0 working with STL
 * 09 - 03 - 1999
 * Rev 1.0 - Now eVC++ compatible
 * 20 - 07 - 2001
 * Giuseppe Govi - g.govi@iol.it
 */

#ifndef INC_WCE_DEFS_H
#define INC_WCE_DEFS_H

#if defined (UNDER_VERIXV)

#include <svc.h>
#include <stddef.h>
#include <string.h>
#include <limits.h>
#include <math.h>

#define _MIPS_SZLONG 32				// sizeof (long)

#define __STL_USE_NEW_IOSTREAMS
#define __STL_NO_USING_CLAUSE_IN_CLASS

#define _STLP_LIMITED_DEFAULT_TEMPLATES 1

/* default parameters as template types derived from arguments ( not always supported ) */
#if defined (_STLP_LIMITED_DEFAULT_TEMPLATES)
#  define __DFL_TMPL_PARAM( classname, defval ) class classname
#  define __DFL_TMPL_ARG(classname) , classname
#else
#  if !defined (_STLP_DEFAULT_TYPE_PARAM)
#    define _STLP_DEFAULT_TYPE_PARAM 1
#  endif
#  define __DFL_TMPL_PARAM( classname, defval ) class classname = defval
#  define __DFL_TMPL_ARG(classname)  
#endif

/* default parameters as complete types */
#if defined (_STLP_DEFAULT_TYPE_PARAM)
#  define __DFL_TYPE_PARAM( classname, defval ) class classname = defval
#  define __DFL_NON_TYPE_PARAM(type,name,val) type name = val
#  define __DFL_TYPE_ARG(classname)
#else
#  define __DFL_TYPE_PARAM( classname, defval ) class classname
#  define __DFL_NON_TYPE_PARAM(type,name,val) type name
#  define __DFL_TYPE_ARG(classname) , classname
#endif

typedef long    wint_t;
typedef char    mbstate_t;

enum _Tag {_S_leaf, _S_concat, _S_substringfn, _S_function};

/* IEEE recommended functions */
#define _FPCLASS_SNAN   0x0001  // signaling NaN
#define _FPCLASS_QNAN   0x0002  // quiet NaN
#define _FPCLASS_NINF   0x0004  // negative infinity
#define _FPCLASS_NN     0x0008  // negative normal
#define _FPCLASS_ND     0x0010  // negative denormal
#define _FPCLASS_NZ     0x0020  // -0
#define _FPCLASS_PZ     0x0040  // +0
#define _FPCLASS_PD     0x0080  // positive denormal
#define _FPCLASS_PN     0x0100  // positive normal
#define _FPCLASS_PINF   0x0200  // positive infinity

char * _ecvt(double, int, int *, int *);
char * _fcvt(double, int, int *, int *);

//int _finite(double);
//int _fpclass(double);
//int    _isnan(double);
//double _copysign (double, double);



#endif //UNDER_VERIXV

#endif //INC_WCE_DEFS_H


