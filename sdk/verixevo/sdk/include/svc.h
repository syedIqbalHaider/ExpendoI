/* SVC.H - Verix Kernel Services and System Library Interface */
/* ARM Version */
/* Copyright 1997-2011 by VeriFone, Inc.  All Rights Reserved. */

#ifndef _SVC_H
#define _SVC_H

#ifdef __cplusplus
extern "C" {
#endif

#ifndef NULL
#ifdef __cplusplus
#define NULL 0
#else
#define NULL (void*)0
#endif
#endif

#ifdef __TARGET_CPU_ARM1136J_S      // Only available for Trident
#define _TRIDENT_
#endif

/* Device Names */
extern const char DEV_CONSOLE[];    /* keyboard & display */
extern const char DEV_BEEPER[];     /* beeper (open to get exclusive access) */
extern const char DEV_CLOCK[];      /* clock (open only to set) */
extern const char DEV_COM1[];       /* com 1 */
extern const char DEV_COM2[];       /* com 2 */
extern const char DEV_COM3[];       /* com 3 */
extern const char DEV_COM4[];       /* com 4 */
extern const char DEV_COM5[];       /* com 5 */
extern const char DEV_COM6[];       /* com 6 */
extern const char DEV_COM7[];       /* com 7 */
extern const char DEV_COM8[];       /* com 8 */
extern const char DEV_COM9[];       /* com 9 */
extern const char DEV_COM10[];      /* com 10 */
extern const char DEV_COM20[];      /* COM20 */
extern const char DEV_COM21[];      /* COM21 */
extern const char DEV_COM22[];      /* COM22 */
extern const char DEV_COM23[];      /* COM23 */
extern const char DEV_COM24[];      /* COM24 */
extern const char DEV_COM25[];      /* COM25 */
extern const char DEV_COM26[];      /* COM26 */
extern const char DEV_COM27[];      /* COM27 */
extern const char DEV_COM28[];      /* COM28 */
extern const char DEV_COM29[];      /* COM29 */
extern const char DEV_COM30[];      /* COM30 */
extern const char DEV_TEST[];       /* test port (development use only) */
extern const char DEV_CARD[];       /* mag card */
extern const char DEV_BAR[];        /* bar code reader */
extern const char DEV_ICC1[];       /* customer smart card */
extern const char DEV_ICC2[];       /* merchant smart card */
extern const char DEV_ICC3[];       /* merchant SAM */
extern const char DEV_ICC4[];       /* merchant SAM */
extern const char DEV_ICC5[];       /* merchant SAM */
extern const char DEV_ICC6[];       /* merchant SAM */
extern const char DEV_ETH1[];       /* SOC Ethernet */
extern const char DEV_ETH2[];       /* USB Ethernet */
extern const char DEV_WLN1[];       /* USB wireless LAN WiFi */
extern const char DEV_USBD[];       /* USB device */
extern const char DEV_BIO[];        /* MSO300 Biometric device*/
extern const char DEV_CTLS[];       /* Contactless device */
extern const char DEV_KYBD[];       /* USB Keyboard HID converted to make and break code*/
extern const char DEV_USBSER[];     /* PP1000SE and Vx810 device */
extern const char DEV_SEMTEK[];     /* Semtek device driver */
extern const char DEV_COM1A[];      /* com 1A */
extern const char DEV_COM1B[];      /* com 1B */
extern const char DEV_COM1C[];      /* com 1C */
extern const char DEV_COM1D[];      /* com 1D */
extern const char DEV_COM1E[];      /* com 1E */
extern const char DEV_COM_FM[];     /* Turkey FInancial Module */
extern const char DEV_AS2522[];     /* telephony */
extern const char DEV_PCM[];        /* speaker */
extern const char DEV_XCDMA[];      /* External USB CDMA modem dongle */
extern const char DEV_XCDMA_A[];    /* Command Channel External USB CDMA modem dongle */

/* Event Codes returned by wait_event() */
#define EVT_KBD         (1L<<0 )    /* keyboard input */
#define EVT_CONSOLE     (1L<<1 )    /* display output complete */
#define EVT_CLK         (1L<<2 )    /* one second clock ticks */
#define EVT_TIMER       (1L<<3 )    /* user-defined timer */
#define EVT_PIPE        (1L<<4 )    /* pipe I/O */
#define EVT_SOKT        (1L<<5 )    /* socket I/O */
#define EVT_NETWORK     (1L<<5 )    /* compatibility */
#define EVT_COM1        (1L<<6 )    /* com 1 I/O */
#define EVT_COM2        (1L<<7 )    /* com 2 I/O */
#define EVT_COM3        (1L<<8 )    /* com 3 I/O */
#define EVT_COM4        (1L<<9 )    /* com 4 I/O */
#define EVT_COM5        (1L<<10)    /* com 5 I/O */
#define EVT_COM8        (1L<<11)    /* com 8 I/O */
#define EVT_MAG         (1L<<12)    /* mag card swipe */
#define EVT_ICC1_INS    (1L<<13)    /* customer smart card I/O */
#define EVT_USB_CLIENT   (1L<<15)    /* USB-slave events */
#define EVT_ACTIVATE    (1L<<16)    /* now can use console handle */
#define EVT_DEACTIVATE  (1L<<17)    /* now cannot use console handle */
#define EVT_BAR         (1L<<18)    /* Barcode */
#define EVT_ICC1_REM    (1L<<19)    /* customer smart card removed */
#define EVT_REMOVED     (1L<<20)    /* UPT case removal event */
#define EVT_IFD_READY   (1L<<21)    /* smart card response available */
#define EVT_IFD_TIMEOUT (1L<<22)    /* smart card response timed out */
#define EVT_USB         (1L<<23)    /* any (usually, "the") USB device */
#define EVT_COM6        (1L<<24)    /* com 6 I/O */
#define EVT_COM7        (1L<<25)    /* com 7 I/O */
#define EVT_WLN         (1L<<26)    /* Wireless LAN WiFi */
#define EVT_SDIO        (1L<<27)    /* SD/SDIO device */
#define EVT_BIO         (1L<<28)    /* MSO300 biometric device */
#define EVT_USER        (1L<<29)    /* post_user_event summary bit */
#define EVT_SHUTDOWN    (1L<<30)    /* powering off soon: all awake! */
#define EVT_SYSTEM      ((long)(1UL<<31)) /* dock, unzip completion, ... */

/* Flags for open() */
#define O_RDONLY    0x0000
#define O_WRONLY    0x0001
#define O_RDWR      0x0002
#define O_NDELAY    0x0004
#define O_APPEND    0x0008
#define O_CREAT     0x0100
#define O_TRUNC     0x0200
#define O_EXCL      0x0400
#define O_MULTI_DEV 0x0000
#define O_SINGLE_DEV 0x1000
#define O_CODEFILE  0x4000

/* Origin codes for lseek() */
#define SEEK_SET 0
#define SEEK_CUR 1
#define SEEK_END 2

/* File attributes for dir_{get/set/reset}_attributes */
#define ATTR_READONLY  (1<<0)
#define ATTR_NO_CKSUM  (1<<1)
#define ATTR_NO_GROW   (1<<2) /* formerly ATTR_NO_SHIFT */
#define ATTR_NOT_AUTH  (1<<3)
#define ATTR_SUBDIR    (1<<13)  // As reported by dir_get_attributes
#define ATTR_CODEFILE  (1<<4)   //reported by dir_get_all_attributes
#define ATTR__SUBDIR   (1<<5)   // As reported by dir_get_all_attributes
#define ATTR__CODEFILE (1<<12)  //reported by dir_get_attributes

/* Result of dir_get_sizes */
struct fs_size {
    short Count;        /* number of files */
    long InUse;         /* file system bytes unsed */
    long Avail;         /* file sytstem bytes available */
};

/* Result of get_task_info */
struct task_info {
     short id;          /* internal identifier */
     char group_id;     /* file ownership group */
     signed char sts;   /* status (see below) */
     long stacksize;    /* bytes allocated to stack */
     long datasize;     /* bytes allocated to globals+heap */
     long msecs;        /* accumulated process time */
     long ct;           /* number times scheduled */
     char path[33];     /* name of codefile */
};

/* Task status codes */
#define TASK_READY      0
#define TASK_WAIT_EVENT 1
#define TASK_WAIT_TIME  2
#define TASK_WAIT_SEM   32
#define TASK_EXIT      -1
#define TASK_DEBUG      0x80

/* Clock-related definitions */
#define TICKS_PER_SEC       1000
#define seconds_offset_1970 315532800L /* seconds in [1jan70..1jan80) */

/* Result of SVC_INFO_CRASH */
struct info_crash {
     unsigned long usr_regs[16]; /* R0-R15 */
     unsigned long cpsr;         /* CPSR */
     unsigned long und_regs[3];  /* SPSR_und, R13_und, R14_und */
     unsigned long abt_regs[3];  /* SPSR_abt, R13_abt, R14_abt */
     unsigned long irq_regs[3];  /* SPSR_irq, R13_irq, R14_irq */
     unsigned long fiq_regs[8];  /* SPSR_fiq, R08_fiq..R14_fiq */
     unsigned long svc_regs[3];  /* SPSR_svc, R13_svc, R14_svc */
     unsigned long fault_addr;   /* bad address for data abort */
     int      abort_type;        /* 1 = data, 2 = prog, 3 = undef */
     short    parent_id;         /* parent's task ID, if a thread */
     short    task_id;           /* which task */
     char     time[6];           /* time of crash: BCD format, yymmddhhmmss */
};

/* Result of SVC_INFO_BUFMGR */
typedef struct buf_info {
    int cur_avl;        // Number of system buffers available now
    int buf_sz;         // Size of each buffer, in bytes, including overhead
    int star_B;         // Maximum number of system buffers
    int reserved[5];    // For future use
} buf_info_t;           // Structure returned by SVC_INFO_BUFMGR
/* SVC_CHECKFILE() results */
#define FILE_OK          0
#define FILE_BAD         1
#define FILE_NOT_FOUND   2

/* FIFO Definition */
typedef struct fifo {
    long ldata [3];
    char cdata [1]; /* dummy - actual length varies */
} fifo_t;

/* Semaphore used by sem_init, sem_wait, and sem_post calls */
typedef struct {
    unsigned char ownr_id;       /* Thread ID of owner, if any; or 0 */
    unsigned char next_id;       /* Thread ID of oldest sem_waiter, if any */
    unsigned short value;        /* 0=now owned by ownr_id; 1=available */
    } sem_t;

/* timezone structure*/
typedef struct tz {
    char tz_std[20];        // standard timezone definition
    char offset1[15];       // standard offset time with respect to UTC
    char tz_dst[20];        // daylight time zone definition
    char offset2[15];       // daylight offset time with respect to UTC
    char start_date[20];    // start date of DST
    char end_date[20];      // end date of DST
}timezone_t;

/* Console function arguments */
#define DEFAULT_KEY_BEEP    1       /* on */
#define DEFAULT_BACKLIGHT   1       /* on */
#define KEY_BUF_SZ         20       /* size of type-ahead key buffer */

// Display Information data structure for the function SVC_INFO_DISPLAY_EXT()
//  NOTE: structure is duplicated in SYS.H
typedef struct {
    char  moduleName[20];
    char  controllerName[20];
    int   driverVersion;      // ie. version 2.52.142  = 0x00 02 34 8E
    int   width;
    int   height;
    int   bitsPerPixel;
    int   pixelFormat;
} displayInfo_t;

// Touchscreen key mapping
typedef struct touchkey_map_item {      // For consistency with window() etc
    unsigned short x1;  // Upper left column, 0..239
    unsigned short y1;  // Upper left row, 0..319
    unsigned short x2;  // Lower right column, 0..239
    unsigned short y2;  // Lower right row, 0..319
    unsigned int key;   // Encoded key value
} tkm_t;

/* Character sizes (pixel width x pixel height) */
#define CHAR_SIZE_6x8       1
#define CHAR_SIZE_8x16      2
#define CHAR_SIZE_16x16     3
#define DEFAULT_CHAR_SIZE   CHAR_SIZE_6x8

/* Font grid sizes for getgrid and resetdisplay */
#define GRID_4x12           0 /* 8x16 char size */
#define GRID_4x6            0 /* 16x16 char size */
#define GRID_8x17           2 /* 6x8 char size */

/* Color Type definitions for SET_DISPLAY_COLOR and GET_DISPLAY_COLOR */
#define FOREGROUND_COLOR    0
#define BACKGROUND_COLOR    1
#define   WHITE               0
#define   LIGHTGRAY           2
#define   DARKGRAY            3
#define   BLACK               1
#define CURRENT_PALETTE     2
#define   RGB                 1
#define   MONOCHROME          2
#define   GRAY4               4
#define AVAILABLE_PALETTE   3

/* Mode constants for set_display_coordinate_mode() and get_display_coordinate_mode() */
#define CHARACTER_MODE      0
#define PIXEL_MODE          1

/* Defines for set_fkey_panel() */
#define TITLE_PANEL 1
#define FKEY_PANEL  2
#define HKEY_PANEL  3

/* Fixed STDIN and STDOUT handles are defined for compatibility, but it is
 * recommended to use the handle returned by open() or get_console(). */
#define STDIN               1
#define STDOUT              1

/* ICC_mode_select arguments */
#define NON_EMV_MODE        0
#define EMV_MODE            1

/* Master-session key options */
#define KEY_PARITY_ZERO     1
#define KEY_PARITY_EVEN     2
#define KEY_PARITY_ODD      3
#define KEY_SEEDED          4
#define KEY_ERASE           8

/* select_pinpad arguments */
#define PINPAD_INTERNAL 0
#define PINPAD_EXTERNAL 1

/* IPP_power arguments  */
#define PINPAD_OFF      0
#define PINPAD_ON       1
/* #define PINPAD_DOZABLE  2  */
/* #define PINPAD_NO_DOZE  3  */

/* SVC_LEDS Control arguments */
#define LED_READ_MONO   0                       // SVC_LEDS( int 0 )
#define LED_WRITE_MONO  1                       // SVC_LEDS( int 1, int LEDS )

/* select_modem arguments */
#define RADIO_MDM       0
#define LANDLINE_MDM    1

/* radio_power arguments */
#define RADIO_OFF       0
#define RADIO_ON        1

/* get_battery_value arguments */
#define FULLCHARGE      0
#define REMAININGCHARGE 1
#define BATTERYVOLTAGE  2
#define BATTERYAVAIL    3
#define BATTERYTEMP     9
#define BATTERYCURRENT 10
#define CHARGERSTATUS  13
/* set_battery_value argument */
#define LOWBATTLEVEL   11

/* COM3 combo I/O module arguments */
#define COMBO_MODEM      0
#define COMBO_TCPIP      1

// Values returned by SVC_INFO_MODEM_TYPE_EX
typedef enum { ModemNone, ModemHarleyInternal, ModemHarleyRemovable, ModemSiLabsRemovable } modem_type_t;

/* Values returned by SVC_INFO_MOD_ID and SVC_INFO_MODULE_ID */
/* copied over from 68K, to support one source code for both Arm and 68K */
/* If you add a new value to this table, update the corresponding table in SYS.H */
#define MID_NO_MODEM            (0)     // No modem on COM3
#define MID_UNKNOWN_MODEM       (2)     // Unknown modem on COM3
#define MID_TDK_ONLY            (3)     // TDK 2.4 modem only
#define MID_BANSHEE_ONLY        (4)     // BANSHEE 144 modem, also in _info.c
#define MID_CARLOS_ONLY         (5)     // Carlos (Aspen144) modem only
#define MID_CO561_ONLY          (6)     // Connect One 10BaseT only
#define MID_CARLOS_CO561        (7)     // Carlos/CO 10baseT combo
#define MID_MC56_ONLY           (8)     // GSM/GPRS US only
#define MID_MC55_ONLY           (9)     // GSM/GPRS International only
#define MID_EM3420_ONLY         (10)    // CDMA 1xRTT only
#define MID_CO710_ONLY          (11)    // Connect One WiFi
#define MID_CARLOS_MC56         (12)    // Carlos/GSM US combo
#define MID_CARLOS_MC55         (13)    // Carlos/GSM Int combo
#define MID_CARLOS_EM3420       (14)    // Carlos/CDMA combo
#define MID_CARLOS_CO710        (15)    // Carlos/WiFi combo
#define MID_EISENHOWER_ONLY     (16)    // Conexant Eisenhower modem only
#define MID_EISEN_USB_ETHERNET  (17)    // Eisenhower/USB Ethernet combo
#define MID_EISEN_EM3420        (18)    // Eisenhower/CDMA combo
#define MID_EISEN_MC56          (19)    // Eisenhower/GPRS USA combo
#define MID_EISEN_MC55          (20)    // Eisenhower/GPRS International combo
#define MID_EISEN_USB_WIFI      (21)    // Eisenhower/USB WiFi combo
#define MID_BANSHEE_CO210       (22)    // Banshee/CO210 Ethernet combo
#define MID_CO210_ONLY          (23)    // CO210 Ethernet
#define MID_ISDN_ONLY           (24)    // ISDN
#define MID_BANSHEE_USB_ETHER   (25)    // Banshee/USB Ethernet combo
#define MID_HARLEY_MODEM        (40)    // Trident Harley Modem
#define MID_COM2_UART           (42)    // COM2 is configured as 2 wire UART
#define MID_USB_MODEM           (50)    // USB modem
#define MID_TELEPHONE           (51)    // Vx525 TelePOS
#define MID_SPEAKER             (52)    // Speaker
/* -------------------- Bluetooth module and radio defines -------------------- */
#define MID_BTEZ1               (60)    // BT Ezurio brand module 1
#define MID_BTEZ2               (61)    // BT Ezurio brand module 2
#define MID_BTEZ3               (62)    // BT Ezurio brand module 3
#define MID_BTEZ4               (63)    // BT Ezurio brand module 4
#define MID_BTAA1               (64)    // BT alternate vendor module 1
#define MID_BTAA2               (65)    // BT alternate vendor module 2
#define MID_BTAA3               (66)    // BT alternate vendor module 3
#define MID_BTAA4               (67)    // BT alternate vendor module 4
#define MID_M200                (70)    // Kyocera M200 CDMA
#define MID_MC55i_ONLY          (72)    // Sierra MC55i GPRS
#define MID_MC5727              (73)    // Sierra MC5727 CDMA
#define MID_SOC_ETH             (74)    // Internal Ethernet
#define MID_USB_HOST_PWR        (75)    // Powered USB Host
#define MID_USB_HOST_NO_PWR     (76)    // USB Host not powered
#define MID_USB_HOST_HUB        (77)    // USB with internal hub
#define MID_USB_DEV             (78)    // USB device
#define MID_CTLS                (79)    // Contactless for the Vx680
#define MID_SD_A                (80)    // SD Slot A
#define MID_SD_B                (81)    // SD Slot B
#define MID_TOUCH_RES           (82)    // touchscreen type - resistive
#define MID_TOUCH_CAP           (83)    // touchscreen type - capacitive
#define MID_HAUWEI_EM660        (84)    // HAUWEI EVDO radio
#define MID_DUET820_MCU         (85)    // Vx820 DUET base
#define MID_BCM43291_WIFI       (86)    // BCM432291 WiFi
#define MID_BCM43291_BT         (87)    // BCM432291 BlueTooth
#define MID_BGS2                (88)    // Cinterion BGS2 GSM/GPRS radio
#define MID_PHS8P               (89)    // Cinterion PHS8-P 3G radio + GPS
#define MID_PHS8P_NOGPS         (90)    // Cinterion PHS8-P 3G radio - GPS
#define MID_MC55IW              (91)    // Cinterion MC55i-W 2G radio
#define MID_TBD                 (99)    // To Be Determined

/* --------------------- Device Presence bit map defines ---------------------- */
#define INFO_PRES_MODEM         (0x01)  // Internal Landline modem presence
#define INFO_PRES_GPRS          (0x02)  // GPRS modem presence
#define INFO_PRES_CDMA          (0x04)  // CDMA modem presence
#define INFO_PRES_WIFI          (0x08)  // WIFI presence
#define INFO_PRES_BT            (0x10)  // BlueTooth presence
#define INFO_PRES_SOC_ETH       (0x20)  // Internal Ethernet presence
#define INFO_PRES_USB_HOST      (0x40)  // USB Host port presence
#define INFO_PRES_USB_DEV       (0x80)  // USB Device port presence
#define INFO_PRES_CTLS         (0x100)  // Contactless Device presence
#define INFO_PRES_SD_A         (0x200)  // SD Device presence
#define INFO_PRES_SD_B         (0x400)  // SD Device presence
#define INFO_PRES_TOUCH        (0x800)  // Touchscreen presence
#define INFO_PRES_PRIMARY     (0x1000)  // Primary smartcard presence
#define INFO_PRES_MSAM        (0x2000)  // MSAM slots presence
#define INFO_PRES_BARCODE     (0x4000)  // Internal BarCode Reader presence
#define INFO_PRES_DSPLITE     (0x8000)  // display backlight presence
#define INFO_PRES_KBDLITE    (0x10000)  // keypad backlight presence
#define INFO_PRES_TELEPHONE  (0x20000)  // Vx525 TelePOS
#define INFO_PRES_SPEAKER    (0x40000)  // Speaker

/* set_com1_pwr arguments */
#define COM1_PWR_ON         (1<<0)      // turns COM1 rs232 level translator chip on/off
#define COM1_PWR_PIN_ON     (1<<1)      // turns COM1 device power on/off

/* set_com_pwr arguments */
#define COM_PWR_PIN_ON      (1<<1)      // turns COM1 device power on/off
/* -------------------- Opn_Blk Defines -------------------- */

struct sdlc_parm_t{
   unsigned char address;
   unsigned char option;
   unsigned int address_mask;
};

struct net_parm_t{
   unsigned int retries;
   unsigned int timeout;
};

struct packet_parm {
   unsigned char stx_char;
   unsigned char etx_char;
   unsigned char count;
};

struct word_t { unsigned int  w[4]; };
struct byte_t { unsigned char b[8]; };

typedef struct Opn_Blk {
    unsigned char rate;
    unsigned char format;
    unsigned char protocol;
    unsigned char parameter;
    union {
        struct sdlc_parm_t sdlc_parms;
        struct net_parm_t  net_parms;
        struct packet_parm packet_parms;
        struct word_t      words;
        struct byte_t      bytes;
    } trailer;
} open_block_t;

/* Rate Codes for Opn_Blk.rate */
#define Rt_300      0
#define Rt_600      1
#define Rt_1200     2
#define Rt_2400     3
#define Rt_4800     4
#define Rt_9600     5
#define Rt_19200    6
#define Rt_38400    7
#define Rt_57600    8
#define Rt_115200   9
#define Rt_12000    10
#define Rt_14400    11
#define Rt_28800    12
#define Rt_33600    13
#define Rt_MAX      13  /* highest code, not highest baud rate! */
#define Rt_user_defined 0xFF    // Specify actual rate in trailer.words.w[0]

#define user_defined_rate trailer.words.w[0]

/* Format codes for Opn_Blk.format */
#define Fmt_A7E1    0       /* async, 7 data bits, even parity, one stop */
#define Fmt_A7N1    1       /* async, 7 data bits,   no parity, one stop */
#define Fmt_A7O1    2       /* async, 7 data bits,  odd parity, one stop */
#define Fmt_A8E1    3       /* async, 8 data bits, even parity, one stop */
#define Fmt_A8N1    4       /* async, 8 data bits,   no parity, one stop */
#define Fmt_A8O1    5       /* async, 8 data bits,  odd parity, one stop */
#define Fmt_A9N1    6       /* async, 9 data bits,   no parity, one stop, requires MDB dongle */
#define Fmt_SDLC    7       /* SDLC,  8 data bits,   no parity           */
#define Fmt_2stp    0x80    /* (SIO) Add, if two stop bits desired       */
#define Fmt_AFC     0x40    /* True AFC RTS not accessable by application*/
#define Fmt_auto    0x20    /* (SIO) Add, if Auto-enables for flow ctl   */
#define Fmt_DTR     0x10    /* (SIO) Add, if DTR to be asserted          */
#define Fmt_RTS     0x08    /* (SIO) Add, if RTS to be asserted          */

/* Protocol options */
#define P_char_mode      0
#define P_packet_mode    2
#define P_gsm_mux        3
#define P_sdlc_mode     13
#define P_sdlc_sec      13  /* SDLC originate fast connect   */
#define P_sdlc_norm     14  /* SDLC originate normal connect */
#define P_sdlc_pri      17  /* SDLC answer normal connect    */
#define P_sdlc_pfast    18  /* SDLC answer fast connect      */

// Radio control outputs
#define RAD_MOD                 (1<<0)
#define RAD_RST                 (1<<1)
#define RAD_OFF                 (1<<2)
// Radio control inputs
#define RAD_INT                 (1<<0)
#define RAD_INT2                (1<<1)

// Connect One CO710 WiFi
#define CO710_MSEL_ASSERT       (0)
#define CO710_MSEL_DEASSERT     (RAD_MOD)
#define CO710_RESET_ASSERT      (0)
#define CO710_RESET_DEASSERT    (RAD_RST)
#define CO710_ON_ASSERT         (RAD_OFF)
#define CO710_ON_DEASSERT       (0)
#define CO710_RI_DCD_ASSERTED   (RAD_INT)
#define CO710_DCD_ASSERTED      (RAD_INT2)

// Sierra Wireless/AirPrime EM3420 CDMA 1xRTT
#define EM3420_MODULE_WAKE_ASSERT       (RAD_MOD)
#define EM3420_MODULE_WAKE_DEASSERT     (0)
#define EM3420_RADIO_RST_ASSERT         (0)
#define EM3420_RADIO_RST_DEASSERT       (RAD_RST)
#define EM3420_ON_ASSERT                (RAD_OFF)
#define EM3420_ON_DEASSERT              (0)
#define EM3420_RST_OUT_ASSERTED         (RAD_INT)
#define EM3420_HOST_WAKE_ASSERTED       (RAD_INT2)

// Siemens MC55/56 GSM/GPRS
#define MC5X_IGT_ASSERT                 (0)
#define MC5X_IGT_DEASSERT               (RAD_RST)
#define MC5X_EOFF_ASSERT                (0)
#define MC5X_EOFF_DEASSERT              (RAD_OFF)
#define MC5X_VDD_ASSERTED               (RAD_INT)
#define MC5X_RI_ASSERTED                (RAD_INT2)

// Kyocera M200 CDMA
#define M200_SEL_UART1                  (0)
#define M200_SEL_UART2                  (RAD_MOD)
#define M200_ON_ASSERT                  (0)
#define M200_ON_DEASSERT                (RAD_OFF)
#define M200_VEXT_ASSERT                (0)
#define M200_VEXT_DEASSERT              (RAD_RST)
#define M200_XCVR_ON                    (RAD_INT)

// The following structure will be prefixed to incoming pipe messages
// if the pipe was configured using pipe_init_msgX.
typedef struct {
    short sndr_pipe_id;
    char sndr_task_id;
    char sndr_group_id;
    unsigned long sndr_time;
    char reserved[8];
} pipe_extension_t;             // Prefixed when writing to extended msg pipes
#define PIPE_EXT_SZ sizeof(pipe_extension_t)

//

/* -------------------- pcm_blk Defines -------------------- */

// width and endianess of audio samples
typedef enum{
    DA_INT16_BE,        // Native format
    DA_INT16_LE_MONO,   // 16 bit WAV file format
    DA_INT16_LE_STEREO, // 16 bit WAV file format
    DA_UINT8_MONO,      // 8 bit WAV file format
    DA_UINT8_STEREO,    // 8 bit stereo WAV file format
}da_format_t;

typedef struct pcm_blk {
    unsigned int sample_rate; // in samples per second
    da_format_t  sample_format; // Endian & size
    unsigned int fifo_low_water;// threshold below which to generate events
    unsigned int low_water_event;
    unsigned int done_event;
    char reserved[12];
} pcm_blk_t;

typedef struct pcm_status {
    unsigned int buffered; // number of samples in FIFO
    unsigned int free_space; // number of additional samples that can be written
    char pause_state;       // 0-not paused 1-paused
    char done_state;        // 0-not done   1-done
    char reserved[6];
} pcm_status_t;

typedef enum {
    PCM_PAUSE = 1,
    PCM_RESUME,
    PCM_ABORT
} pcm_action;

/* -------------------- Feature Enablement Defines -------------------- */

// All sizes include null terminator
#define ENABLEMENT_TAG_SZ       5   // Major/Minor tag size
#define ENABLEMENT_GEN_SZ       65  // Generic field size
#define ENABLEMENT_CREATION_SZ  11  // Creation date size
#define ENABLEMENT_VALID_SZ     18  // Active date range size
#define ENABLEMENT_STARTEND_SZ  5   // Relative day size

// Token file specifics
#define ENABLEMENT_ISSUER       "ISSUER"
#define ENABLEMENT_CUSTOMER     "CUSTOMER"
#define TOKEN_DESCRIPTION       "DESCRIPTION"
#define TOKEN_VALID             "VALID"
#define TOKEN_START             "START"
#define TOKEN_END               "END"
#define TOKEN_COUNT             "COUNT"

// Unit values
#define ENABLEMENT_UNIT_DAYS    1
#define ENABLEMENT_UNIT_USES    2

// Structure to store token major,minor values
typedef struct {
    char major[ENABLEMENT_TAG_SZ];
    char minor[ENABLEMENT_TAG_SZ];
} enablementTokenTag;

/*****************************************************************************/
/*                             Kernel Functions                              */
/*****************************************************************************/

#ifndef __GNUC__
#define _SYS static

// ------------ Basic Posix I/O
_SYS int write (int hdl, const char *buf, int len);
_SYS int read (int hdl, char *buf, int len);
_SYS int close (int hdl);
_SYS int open (const char *id, int flags);
_SYS long lseek (int hdl, long off, int orig);

// ------------ Directory-oriented file system functions
_SYS int dir_get_first (char *buf);
_SYS int dir_get_next (char *buf);
_SYS int _remove (const char *file);
_SYS int _rename (const char *oldname, const char *newname);
_SYS int dir_get_sizes (const char *drive, struct fs_size* sizes);
_SYS int SVC_CHECKFILE (const char *file);
_SYS int dir_get_file_date (const char *file, char *date);
_SYS int dir_put_file_date (const char *file, const char *date);
_SYS int dir_get_attributes (const char *file);
_SYS int dir_get_all_attributes (const char *file);
_SYS int dir_set_attributes (const char *file, int attr);
_SYS int dir_reset_attributes (const char *file, int attr);
_SYS int dir_flash_coalesce (void);
_SYS long dir_get_file_size (const char *file);
_SYS int get_component_vars (int hdl, char *buf, int len);
_SYS long dir_get_file_sz (const char *file);
_SYS int dir_flash_coalesce_size (long *size);
_SYS int getkey (const char *key, char *buf, int size, const char *file);
_SYS int putkey (const char *key, const char *buf, int size, const char *file);
_SYS int get_env (const char *key, char *buf, int size);
_SYS int put_env (const char *key, const char *buf, int size);
_SYS int chdir (const char *file);
_SYS int getcwd (char *buf, int len);
_SYS int mkdir (const char *file);
_SYS int rmdir (const char *file);

// ------------ Variable length records
#ifndef __cplusplus /* conflicts with C++ keyword - use delete_ */
_SYS int delete (int hdl, unsigned int count);
#endif
_SYS int delete_ (int hdl, unsigned int count);
_SYS int insert (int hdl, const char *buf, int size);
_SYS int read_vlr (int hdl, char *buf, int size);
_SYS int write_vlr (int hdl, const char *buf, int size);
_SYS long seek_vlr (int hdl, long off, int orig);
_SYS int insert_vlr (int hdl, const char *buf, int size);
_SYS int delete_vlr (int hdl, unsigned int count);
_SYS int read_cvlr (int hdl, char *buf, int size);
_SYS int write_cvlr (int hdl, const char *buf, int size);
_SYS long seek_cvlr (int hdl, long off, int orig);
_SYS int insert_cvlr (int hdl, const char *buf, int size);
_SYS int delete_cvlr (int hdl, unsigned int count);

// ------------ Other file functions
_SYS int lock (int hdl, long off, long len);
_SYS int unlock (int hdl, long off, long len);
_SYS int get_file_size (int hdl, long *size);
_SYS int get_file_date (int hdl, char *buf);
_SYS int put_file_date (int hdl, const char *buf);
_SYS int get_file_attributes (int hdl);
_SYS int set_file_attributes (int hdl, int attr);
_SYS int reset_file_attributes (int hdl, int attr);
_SYS long get_file_max (int hdl);
_SYS int set_file_max (int hdl, long maxsize);
_SYS int tell_page (int hdl);
_SYS int flash_diag_data (int diag_type, int *diag_buf, int diag_ct);

// ------------ Miscellaneous device manager calls
_SYS int   get_owner (const char *id, int *task_id);
_SYS int   set_owner (int handle, int task_id);
_SYS int   set_owner_all (int hdl);
_SYS int   get_name (int handle, char *dev_id);
_SYS int   dbdump (const void *buf, int len);
_SYS int   grant_owner (int handle, int task_id);
_SYS int   revoke_owner (int handle);
extern int dbprintf (const char *fmt, ...);
// Sleep and wake calls for Conexant Harley modem
int modem_sleep(int handle);
int modem_wake(int handle);

_SYS unsigned long get_usb_device_bits (void);
#define UDB_FLASH   (1<<0)
#define UDB_ETHER   (1<<1)
#define UDB_WIFI    (1<<2)
#define UDB_COM3    (1<<3)
#define UDB_COM6    (1<<4)
#define UDB_COM4    (1<<6)
#define UDB_KYBD    (1<<7)
#define UDB_VX810   (1<<8)
#define UDB_PP1000SE (1<<9)
#define UDB_BIO     (1<<10)
#define UDB_BAR     (1<<11)
#define UDB_COM2    (1<<12)
#define UDB_COM9    (1<<13)
#define UDB_COM10   (1<<14)
#define UDB_VX820   (1<<19)
#define UDB_VX805   (1<<20)
#define UDB_APL     (1<<21)
#define UDB_XCDMA   (1<<21)
#define UDB_XCDMA_A (1<<22)
#define UDB_USBHUB  (1<<23)
#define UDB_IBHUB   (1<<29)
#define UDB_SINGLE   (1<<30)

_SYS unsigned long get_usb_switch_bits (void);
#define uUSB_UARTMODE      (1<<0)
#define uUSB_HOSTMODE      (1<<1)
#define uUSB_DEVICEMODE    (1<<2)
#define uUSB_1AMPCHARGER   (1<<3)
#define uUSB_2AMPCHARGER   (1<<4)
#define uUSB_UNKNOWN       (1<<5)

_SYS unsigned long get_sd_device_bits (void);
#define SD_FLASH    (1<<0)
#define SD_CTLS     (1<<1)

// ------------ Console Interface
_SYS int activate_task (int task_id);
_SYS int clreol (void);
_SYS int clrscr (void);
_SYS int contrast_down (void);
_SYS int contrast_up (void);
_SYS int delline(void);
_SYS int set_display_color(int type, int color);
_SYS int get_display_color(int type);
_SYS int set_display_coordinate_mode(int mode);
_SYS int get_display_coordinate_mode(void);
_SYS int get_character_size(int *rowSize, int *colSize);
_SYS int disable_hot_key (void);
_SYS void disable_key_beeps (void);
_SYS int enable_hot_key (void);
_SYS void enable_key_beeps (void);
_SYS int getcontrast (void);
_SYS void  getfont (char *font);
_SYS int getgrid (void);
_SYS int getinverse (void);
_SYS int getscrollmode (void);
_SYS int get_console (int clear_keys);
_SYS int get_font (char *font_name);
_SYS int get_font_mode (void);
     long get_hot_key_sts (void);
_SYS int gotoxy (int x,int y);
_SYS int insline(void);
_SYS int inverse_toggle (void);
_SYS int kbd_pending_count (void);
_SYS int kbd_pending_test (int t);
_SYS int key_beeps (int flag);
_SYS int putpixelcol (const char *buffer, int len);
_SYS int put_graphic (const char *buf, int len, int x1, int y1, int x2, int y2);
_SYS int resetdisplay (const char *font, int grid_id);
_SYS int screen_size (char *buf);
_SYS int setcontrast (int flag);
_SYS int setfont (const char *font);
_SYS int setinverse (int flag);
_SYS int setscrollmode (int mode);
_SYS int set_backlight (int flag);
_SYS int set_kb_backlight (int value);
_SYS int set_backlight_level (int value);
_SYS int get_backlight_level (void);
_SYS int get_kb_backlight (void);
_SYS int set_cursor (int flag);
_SYS int set_fkey_panel(char *bmp_filename, int which_panel);
_SYS int set_font (const char *font_name);
_SYS int set_hot_key (int keycode);
_SYS int set_touchscreen_keymap(tkm_t *map, int ct);
_SYS int wherecur (int *x, int *y);
_SYS int wherewin (int *x1, int *y1, int *x2, int *y2);
_SYS int wherewincur (int *x, int *y);
_SYS int window (int startX, int startY, int endX, int endY);
_SYS int write_at (const char *b, int l, int x, int y);
_SYS int put_BMP ( char *file);
_SYS int put_BMP_at ( int x, int y, char *file);
_SYS int get_BMP (int startX, int startY, int endX, int endY, char *buffer, int color);
_SYS int get_touchscreen(int *x, int *y);
_SYS int cs_hard_reset(void);
_SYS int cs_soft_reset(void);
_SYS int cs_set_baseline(int scantype);
_SYS int cs_spi_read(int encrypted, int command);
_SYS int cs_spi_write(int encrypted, int command, unsigned char *data);
_SYS int cs_spi_cmd_status(void);
_SYS int cs_spi_cmd_data(char *data);
_SYS int cs_read_temperature(void);
_SYS int is_keypad_secure(void);
#define KEYPAD_NOT_SECURE 0
#define KEYPAD_SECURE 1
#define KEYPAD_SECURITY_UNKNOWN 2 // security check in progress
_SYS int cs_set_sleep_state(int sleep);
_SYS int cs_overlay_scan(int scan_type);

// GRAPHICS
_SYS int write_pixels (int startX, int startY, int endX, int endY, int color);
_SYS int draw_line(int startX,int startY,int endX,int endY,int width,int color);
_SYS int copy_pixel_block (int srcStartX,int srcStartY,int srcEndX,int srcEndY,
                       int dstStartX, int dstStartY, int dstEndX, int dstEndY);
_SYS int invert_pixel_block (int startX, int startY, int endX, int endY);
_SYS int display_frame_buffer(int x, int y, int w, int h, short * buffer);
// end GRAPHICS

_SYS int lock_kbd(int flag);
_SYS int sts_kbd_lock(void);
_SYS int set_bcm(int mode);
_SYS int get_battery_icon (char *buff3);

// ------------ Serial ports
_SYS int get_opn_blk (int hdl, struct Opn_Blk *ob);
_SYS int get_port_status (int hdl, char *buf);
//PayWare Mobile Gen2 Host Interface only.  get_port_status event cause bits (buf[1])
#define HOST_CONNECT    (1<<0)
#define HOST_DISCONNECT (1<<1)
#define HOST_RX_READY   (1<<2)
_SYS int reset_port_error (int hdl);
_SYS int set_opn_blk (int hdl, const struct Opn_Blk *ob);
_SYS int set_serial_lines (int hdl, const char *mask);
_SYS int set_radio_ctl (int hdl, const char *sigs);
_SYS int get_radio_sts (int hdl, char *sigs);
_SYS int set_com1_pwr (const char *sigs);
_SYS int set_com_pwr (int hdl, const char *sigs);
_SYS int set_signal_events (int hdl, char *evts);
_SYS int set_combo_mode (int mode);
_SYS int get_bits_per_second (int hdl);
_SYS int set_gsm_powersave(int hdl, unsigned int power);
_SYS int set_gsm_break(int hdl);
_SYS int set_com1_event_bit (int hdl, long flag);
_SYS long get_com1_event_bit (int hdl);
_SYS int get_iap_state (int hdl);
_SYS int get_ipod_status (int hdl);
_SYS int set_ipod_pins (int hdl, int mask);
_SYS int reset_ipod_pins (int hdl, int mask);
_SYS int get_protocol_string (int hdl, char *buf);
_SYS int get_bundle_id (int hdl, char *buf);
_SYS int iap_control_function (int hdl, int func);
#define IAP_CONTROL_KEYPAD_SLEEP 0
#define IAP_CONTROL_KEYPAD_WAKE  1
#define IAP_CONTROL_DISABLE_KEYBEEP  2
#define IAP_CONTROL_ENABLE_KEYBEEP   3
_SYS int iap_get_keypad_info(int hdl, char *buf);
_SYS int iap_get_keypad_state(int hdl, char *buf);

_SYS int bt_get_version(int hdl, char *buf, int len);       // PayWare Moblie Bluetooth
_SYS int bt_get_name(int hdl, char *buf, int len);          // PayWare Moblie Bluetooth
_SYS int bt_get_pin(int hdl, char *buf, int len);           // PayWare Moblie Bluetooth
_SYS int bt_get_ssp_en(int hdl);                            // PayWare Moblie Bluetooth
_SYS int bt_get_ssp_pk_en(int hdl);                         // PayWare Moblie Bluetooth
_SYS int data_sync_en(int hdl, int mode);                   // PayWare Moblie Host Sync Gen3
typedef enum{                                               // PayWare Moblie Host Sync Gen3
    IAP2_DATA_SYNC_BUTTONS = 1,     // got new packet from transport
    IAP2_DATA_SYNC,                 // got new packet from session
    IAP2_IAP_MODE,                  // tx packet not acked in time
}iap_cnct_msg_t;

_SYS int iAP_power_share(int hdl, int mode);  // PayWare Mobile Share Host Power
typedef enum{
    SHARE_BAT=1,                    // turn on iPOD sharing battery power
    DISABLE_SHARE_BAT,              // turn off iPOD sharing battery power
    IAP2_STATUS,                    // Retrieve IAP Status
}iap_pwr_share_msg_t;

_SYS int iAP_power_status(int hdl);
// IAP2_STATUS Bits
#define HOST_PWR        0x01        // iPod on Battery Power
#define EXT_PWR_IPOD    0x02        // iPod on external Power
#define POWERSHARE_ON   0x04        // Power Share currently active
#define DATA_SYNC_EN    0x08        // Data Sync is enabled (iPod connected to Host)
#define IPOD_PRESENT    0x10        // iDevice is connected to terminal
#define GANG_CHARGER    0x20        // terminal on Gang Charger


_SYS int write_9bit(int hdl, short *buf, int len);
_SYS int read_9bit(int hdl, short *buf, int len);

// ------------ Authentec/UPEK biometric fingerprint reader


// ------------ Stack interface
_SYS int openaux(int devhdl);
_SYS int closeaux(int devhdl);

// ------------ USB Device calls
_SYS int get_usbd_status (int hdl);
_SYS int usb_pending_out (int hdl);


// ------------ Beeper
_SYS int sound (int note, int msec);
_SYS void normal_tone (void);
_SYS void error_tone (void);
_SYS void beeper_off (void);

// ------------ Speaker
_SYS int set_pcm_volume(int handle, char vol);
_SYS int get_pcm_volume(int handle, char *vol);
_SYS int get_pcm_blk (int hdl, struct pcm_blk *pb);
_SYS int set_pcm_blk (int hdl, const struct pcm_blk *pb);
//_SYS int set_pcm_rate(int handle, int rate);
//_SYS int get_pcm_rate(int handle, int *rate);

_SYS int set_pcm_playback(int handle, pcm_action action);
_SYS int get_pcm_status(int handle, struct pcm_status *status);

// ------------ Other devices
_SYS int card_pending (void);
_SYS int card_raw_data (char *buf);
_SYS int card_mode (int mode);
_SYS void magprt_mode_control (int mode);
_SYS int card_magprint_stat (char *buf);
_SYS int card_magprint_data (char *buf);
_SYS int card_magprint_count (void);
_SYS int barcode_pending (void);
_SYS int barcode_raw_data (char *buf);
_SYS int select_pinpad (int type);
_SYS int IPP_power (int type);
_SYS int PINentryStatus(void);
_SYS int TerminatePinEntry(void);
_SYS int select_modem (int type);
_SYS int radio_power (int type);
_SYS int EPP_present (void);
     int  get_icc_device (int phys_dev);
_SYS int BT_Si2434_profile_load(void);
_SYS int set_telephone_state(int state);
_SYS int get_telephone_status(void);
_SYS int set_SIM_slot (int hdl, int slotNumber);
_SYS int get_SIM_slot (int hdl);
_SYS int SIM_SELECT(int simindex);

// ------------ Contactless
_SYS int reset_ctls (void);
_SYS int set_event_bit (int hdl, long flag);
_SYS long get_event_bit (int hdl);

// ------------ Pipes
_SYS int pipe_init_msg (int hdl, int msgct);
_SYS int pipe_init_msgX (int hdl, int msgct);
_SYS int pipe_init_char (int hdl, int charct);
_SYS int pipe_connect (int hdl, int dhdl);
_SYS int pipe_pending (int hdl);

// ------------ Scheduling and process-oriented calls
_SYS int set_timer (long msecs, long eventmask);
_SYS int  SVC_WAIT (unsigned int msec);
_SYS int clr_timer (int timer_id);
_SYS unsigned long read_ticks (void);
_SYS int read_clock (char *yyyymmddhhmmssw);
_SYS int read_RTC (int hdl, char *yyyymmddhhmmssw);
_SYS int _exit (int status);
_SYS long wait_event (void);
_SYS long peek_event (void);
_SYS long read_event (void);
_SYS long wait_evt (long interesting_events);
_SYS long read_evt (long interesting_events);
_SYS int post_user_event (int task_id, long user_event);
_SYS long read_user_event (void);
_SYS int get_task_id (void);
_SYS int get_task_info (int id, struct task_info *results);
_SYS int run (const char *file, const char *parms);
_SYS int run_thread (int rtnaddr, int rtnparm, int stksize);
_SYS int set_group (int group_id);
_SYS int get_group (void);
_SYS int get_native_group (void);
_SYS int SVC_RESTART (const char *file);
_SYS int unzip (const char *zipfile);
_SYS int authenticate (const char *signature_file_name);
_SYS int sem_init (sem_t *semaphore, unsigned short value);
_SYS int sem_wait (sem_t *semaphore);
_SYS int sem_post (sem_t *semaphore);
_SYS int set_errno_ptr (int *ptr);
_SYS volatile int* get_errno_ptr (void);
_SYS unsigned long get_performance_counter (void);
_SYS unsigned long get_performance_frequency (void);
_SYS int unload_DLL (int funtbl);
_SYS sem_t* sem_open (char *sem_id,unsigned short unused);
_SYS int sem_close (sem_t *semaphore);
_SYS int sem_prop (sem_t *semaphore,unsigned short mode);
    #define sem_owner_id(sem) sem_prop(sem,0)
    #define sem_next_id(sem)  sem_prop(sem,1)
    #define sem_value(sem)    sem_prop(sem,2)
_SYS int thread_origin (void);
_SYS int thread_cancel(int id);
_SYS int thread_join (int thread_id, int *retval);
_SYS void *shm_open (const char *id, int flag, int mode);
_SYS int shm_close (void *hdl);
_SYS int memory_access (const void* buffer, int length);
    #define readable_memory(buf,len) (memory_access(buf,len)>0)
    #define writable_memory(buf,len) (memory_access(buf,len)>1)

// ------------ Information-retrieval calls for terminal management
_SYS void SVC_INFO_PTID (char *buf);
_SYS void SVC_INFO_EPROM (char *buf);
_SYS void SVC_VERSION_INFO (char *buf);
_SYS int SVC_INFO_MODEM_TYPE (void);
_SYS modem_type_t SVC_INFO_MODEM_TYPE_EX(void);
_SYS int SVC_INFO_KEYBRD_TYPE (void);
_SYS int SVC_LEDS (int mode, void *param );
_SYS int SVC_RAM_SIZE (void);
_SYS int SVC_INFO_KBD (char *buf);
_SYS int SVC_INFO_MAG (char *buf);
_SYS void SVC_INFO_CRASH (struct info_crash *buf);
_SYS long SVC_INFO_LIFETIME (void);
_SYS int SVC_INFO_RESET (char *yymmddhhmmss);
_SYS int SVC_INFO_MFG_BLK (char *buf);
_SYS int SVC_INFO_MFG_BLK_EXT (char *buf, int len);
_SYS int SVC_INFO_MODELNO (char *buf);
_SYS int  SVC_INFO_MODELNO_EXT (char *buf, int len);
_SYS int SVC_INFO_COUNTRY (char *buf);
_SYS int SVC_INFO_COUNTRY_EXT (char *buf, int len);
_SYS int SVC_INFO_PARTNO (char *buf);
_SYS int SVC_INFO_PARTNO_EXT(char *buf, int len);
_SYS int SVC_INFO_HW_VERS (char *buf);
_SYS int SVC_INFO_HW_VERS_EXT (char *buf, int len);
_SYS int SVC_INFO_SERLNO (char *buf);
_SYS int SVC_INFO_SERLNO_EXT (char *buf, int len);
_SYS int SVC_INFO_LOTNO (char *buf);
_SYS int SVC_INFO_LOTNO_EXT (char *buf, int len);
_SYS int SVC_FLASH_SIZE (void);
_SYS int SVC_INFO_PORT_IR (void);
_SYS int SVC_INFO_PORT_MODEM (void);
_SYS int SVC_INFO_DISPLAY (char *buf);
_SYS int SVC_INFO_PRNTR (char *buf);
_SYS int SVC_INFO_PIN_PAD (char *buf);
_SYS int SVC_INFO_MOD_ID (void);
_SYS int SVC_INFO_MODULE_ID (int d);
_SYS int SVC_INFO_OS_HASH (char *hashout20,char *keyin,int keyinsz);
_SYS int SVC_INFO_OS_HMAC_SHA1 (char *hashout20,char *keyin,int keyinsz);
_SYS int SVC_INFO_OS_HMAC_SHA256 (char *hashout32,char *keyin,int keyinsz);
_SYS int SVC_INFO_OS_EXPIRY_STS (int *remaining);
_SYS int SVC_INFO_RELEASED_OS (void);
_SYS int SVC_INFO_DISPLAY_EXT(displayInfo_t * info);
_SYS int SVC_INFO_READ_MIB(char *buf, int len);
_SYS int SVC_INFO_PRESENT (void);
_SYS int SVC_INFO_DEV_TYPE (int type);
_SYS int SVC_INFO_DEV (int type, char *dev_name);
_SYS int SVC_INFO_PORTABLE(char *char1);
_SYS int SVC_INFO_BAT_REQ(char *char1);
_SYS int SVC_INFO_BIO(void);
_SYS int SVC_INFO_PWR_MGMT(int *buf, int len);
_SYS int SVC_INFO_DUAL_SIM(void);
_SYS int SVC_INFO_BUFMGR(buf_info_t *buf_info_ptr);
_SYS int SVC_INFO_ETH_MAC (char *buf);
_SYS int SVC_INFO_BT_MAC (char *buf);


// ------------ CIB ID (part number)
_SYS int SVC_INFO_CIB_ID(char *CIBid11, int buflen);

// ------------ CIB version
_SYS int SVC_INFO_CIB_VER(char *CIBver5, int buflen);

// ------------ SBI version
_SYS int SVC_INFO_SBI_VER(char *SBIver5, int buflen);

// ------------ FIFOs
_SYS int set_fifo_config (int hdl, const char *buf);
_SYS int set_fc_config (int hdl, const char *buf);
_SYS int get_fifo_config (int hdl, char *buf);
_SYS int get_fc_config (int hdl, char *buf);

// ------------ APACS 40
_SYS int Init_MAC (void);
_SYS int Create_MAC_Key (int hostnum, const char *A, const char *B);
_SYS int Calc_Auth_Parm (const char *TranData, char *AuthParm);
_SYS int Calc_MAC (const char *buffer, int len, char *mac8);
_SYS int New_Host_Key (int hostnum, const char *rqst_residue, const char *resp_residue);
_SYS int Reset_Key (int hostnum);
_SYS int Term_MAC (void);

// ------------ Power Functions
_SYS int get_battery_sts (void);
_SYS int get_battery_value (int type);
_SYS int set_battery_value (int type, int value);
_SYS int SVC_SHUTDOWN (void);
_SYS int get_powersw_sts (void);
_SYS int get_dock_sts (void);
_SYS int BatteryRegs (char* buf);
_SYS int BatteryMfgID (char* buf);
_SYS int BatterySN (char* buf);
_SYS int SVC_SLEEP (void);
_SYS int get_battery_initialization_status (void);
_SYS int USB_WIFI_POWER(int pwr);
_SYS int USB_COM2_RESET (void);
_SYS int set_usb_multi_device(int hdl, int onOff);
_SYS int start_battery_conditioner(void);
_SYS int battery_conditioner_status(void);
_SYS int reg_presleep(int bitnum);
_SYS int set_host_power_event(long evt);  // Task owning the console will see the host power event.
_SYS int battery_remain_charge(void);
_SYS void set_wakeup_event(int bitnum);
_SYS int disable_host_power(void);
_SYS int enable_host_power(void);
_SYS int host_power_status(void);


// ------------ Ethernet related functions
_SYS int get_enet_MAC(int hdl, char *MACbuf);
_SYS int get_enet_status(int hdl, char *status);
_SYS int set_enet_rx_control(int hdl, int rx_control);

// ------------ UPT Case Removal functions
_SYS int get_latch_status(void);
_SYS int reset_latch(char *password);
_SYS int set_activation_hash(char *oldHash, char *newHash);

// ------------ VeriShield Protect
_SYS int VSP_Crypto (int h, char *s);
_SYS int VSP_Xeq (char *s);
_SYS int VSP_Result (void);
_SYS int VSP_Init (char *st);
_SYS int VSP_Status (char *st);
_SYS int VSP_Disable (char *st);
_SYS int VSP_Enable (char *st);
_SYS int VSP_Encrypt_MSR (char *MSRc, char *MSRe);
_SYS int VSP_Encrypt_KBD (char *PANc, char *EXPc, char *PANe, char *EXPe);
_SYS int VSP_Decrypt_PAN (char *PANe, char *PANc);
_SYS int VSP_Decrypt_MSR (char *MSRe, char *MSRc);
_SYS int VSP_Status_Ex (char *st);
_SYS int VSP_Reset (char *st);
_SYS int VSP_Passthru (char *in, int iLen, char *out, int oLen);

// ------------ RKL
_SYS int get_rkl_krd_cert (char *filename);

// ------------ Feature Enablement
_SYS int enablementTokenCount (void);
_SYS int enablementTokenExpiration (void* tag, int* pUnit);
_SYS int enablementTokenStatus (void* tag);
_SYS int enablementTokenDemandStatus (void* tag);
_SYS int enablementTokenIndex (int index, void* tag);
_SYS int enablementTokenDetail (void* tag, char* name, char* value, int len);

// ------------ Payware Mobile Reader Gen2.5 (agnostic) Bluetooth functions

_SYS int set_bt_pins(int hdl, int pins);    // Temp debugging function
_SYS int get_bt_pins(int hdl);              // Temp debugging function
_SYS int bt_read_eeprom(int hdl, int address, char *buffer, int length);
_SYS int bt_write_eeprom(int hdl, int address, char *buffer, int length);
_SYS int bt_dump_flash(int hdl);            // Temp debugging function
_SYS int bt_disconnect(int hdl);            // PayWare Moblie Bluetooth
_SYS int bt_discard_pairing(int hdl);       // PayWare Moblie Bluetooth

// ------------
#endif //__GNUC__

/* Software interrupt definitions for above */
#include <svc_swi.h>

#undef _SYS

/*****************************************************************************/
/*                         System Library Functions                          */
/*****************************************************************************/

/* Strings and Numbers */
char  *ltoa (long value, char *buffer, int radix);
char  *ultoa (unsigned long value, char *buffer, int radix);
int   dtoa (double f, char *buf, int bufsz, int fmt, int prec);
void  SVC_INT2 (unsigned int source, char *target);
unsigned int SVC_2INT (const char *source);
int   SVC_AZ2CS (char *cstring, const char *zstring);
int   SVC_CS2AZ (char *zstring, const char *cstring);
void  strnlwr (char *target, const char *source, int size);
void  strnupr (char *target, const char *source, int size);
void  SVC_DSP_2_HEX (const char *dsp, char *hex, int pairs);
void  SVC_HEX_2_DSP (const char *hex, char *dsp, int pairs);
int   SVC_PACK4 (char *target, const char *source, int size);
int   SVC_UNPK4 (char *target, const char *source, int size);
int   alpha_shift (int c);
int   alpha_multi_shift(int c, int *shift);
void  reset_interval(void);

/* Time and Date */
void  date2days (const char *yyyymmdd, long *days);
int   days2date (const long *days, char *yyyymmdd);
void  time2secs (const char *hhmmss, long *secs);
void  secs2time (const long *secs, char *hhmmss);
void  datetime2seconds (const char *yyyymmddhhmmss, unsigned long *seconds);
int   seconds2datetime (const unsigned long *seconds, char *yyyymmddhhmmss);
int   SVC_VALID_DATE (const char *yyyymmddhhmmss);
int   getTZdata(timezone_t *tzd);

/* CRCs and Checksums */
unsigned int  SVC_CRC_CALC (int type, const char *buffer, int size);
unsigned long SVC_CRC_CALC_L (int type, const char *buffer, int size);
unsigned int  SVC_LRC_CALC (const void *buffer, int sz, unsigned int seed);
unsigned int  SVC_CRC_CCITT_L (const void *buffer, int sz, unsigned int seed);
unsigned int  SVC_CRC_CCITT_M (const void *buffer, int sz, unsigned int seed);
unsigned int  SVC_CRC_CRC16_L (const void *buffer, int sz, unsigned int seed);
unsigned int  SVC_CRC_CRC16_M (const void *buffer, int sz, unsigned int seed);
unsigned long SVC_CRC_CRC32_L (const void *buffer, int sz, unsigned long seed);
unsigned int  SVC_MEMSUM (const char *buffer, long size);
unsigned int  SVC_MOD_CK (const char *cs_acct);

/* Fifos */
int   SVC_CHK_FIFO (const fifo_t *fifo);
int   SVC_CLR_FIFO (fifo_t *fifo, int datasize);
int   SVC_GET_FIFO (fifo_t *fifo);
int   SVC_PUT_FIFO (fifo_t *fifo, int next_item);
int   SVC_READ_FIFO (fifo_t *fifo, char *buffer, int size);
int   SVC_WRITE_FIFO (fifo_t *fifo, const char *buffer, int size);

/* play tune */
void play_RTTTL(const char *music);

/* Miscellaneous */
int   file_copy (const char *source, const char *target);
int   file_copy_auth_bit (const char *source, const char *target);
int   download (int port, const void *parms);
int   download_net (int sock, int *SSLstruc, const void *parms);
int   SVC_ZONTALK(unsigned char type);
int   SVC_ZONTALK_NET(unsigned char type);
int   SVC_CHK_PASSWORD (const char *buffer);
int   set_GIDSmask(unsigned short GIDSmask);
int   get_GIDSmask(unsigned short *GIDSmask);

/* Unsupported system functions implemented in library for compatibility */
int   SVC_LED (int id, int mode);
int   get_TDK_ident (int hdl, char *buf, int sz);

/* Dynamically loading shared libraries */
int load_named_DLL (const char *dllFileName);
int DLL_function_address (int H, int N);

/* validate heap pointer */
int isValidHeapPtr (void *ptr);	//only for Trident dynamic heap
#define FREE_TYPE_VALIDATE    0	//validates pointer before free
#define FREE_TYPE_NO_VALIDATE 1 //no validation, faster
void setFree(int type);
#ifdef __cplusplus
}
#endif

#endif
