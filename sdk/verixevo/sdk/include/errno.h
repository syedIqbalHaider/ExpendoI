/* Verix error definitions */


#ifndef __ERRNO
#define __ERRNO

/* It is highly recommended that this errno.h file is included in an application
 * or library to declare errno, however, you could use 
 extern int errno;
 * instead, in an application (not shared library) to declare errno, but, this
 * errno always point to a fixed address, not the address set by 
 * set_errno_ptr();
 */
#ifndef __ERRNO_DECLS
#define __ERRNO_DECLS

#undef __CLIBNS
#ifdef __cplusplus
namespace std {
#define __CLIBNS ::std::
extern "C" {
#else /* ndef __cplusplus */
#define __CLIBNS
#endif /* ndef __cplusplus */

volatile int *__rt_errno_addr (void);

#ifdef __cplusplus
}  /* extern "C" */
}  /* namespace std */
#endif
#endif /* __ERRNO_DECLS */

  #if defined(__cplusplus) && !defined(__ERRNO_NO_EXPORTS)
    using ::std::__rt_errno_addr;
  #endif

#define errno (*__CLIBNS __rt_errno_addr())

/* This list includes all error codes defined by POSIX.1.  Those which are 
 * known to be used on this system are marked with "!". */

#define EPERM      1    /*  Caller does not have necessary privileges */
#define ENOENT     2    /*! No such file or directory */
#define ESRCH      3    /*  No such process */
#define EINTR      4    /*  Interrupted system call */
#define EIO        5    /*! I/O error */
#define ENXIO      6    /*  No such device */
#define E2BIG      7    /*  Argument list too long */
#define ENOEXEC    8    /*  Executable file format error */
#define EBADF      9    /*! Invalid file handle */
#define ECHILD     10   /*  No child processes */
#define EAGAIN     11   /*  No more processes, or read would block */
#define ENOMEM     12   /*! No memory available */
#define EACCES     13   /*! Permission denied */
#define EFAULT     14   /*  Bad address (hardware fault using argument) */
#define ENOTBLK    15   /*  Block device required */
#define EBUSY      16   /*! Device or directory in use */
#define EEXIST     17   /*! File already exists */
#define EXDEV      18   /*  Cross-device link (link to another device) */
#define ENODEV     19   /*! No such device (or inappropriate call) */
#define ENOTDIR    20   /*  Not a directory (eg, in a path prefix) */
#define EISDIR     21   /*  Operation invalid on directory */
#define EINVAL     22   /*! Invalid argument */
#define ENFILE     23   /*  Too many open files */
#define EMFILE     24   /*! Too many file handles in use */
#define ENOTTY     25   /*  Not a typewriter */
#define ETXTBSY    26   /*  Executable file in use */
#define EFBIG      27   /*  File too large */
#define ENOSPC     28   /*! No space left or other write error on device */
#define ESPIPE     29   /*  Illegal seek on pipe */
#define EROFS      30   /*  Read-only file system or device */
#define EMLINK     31   /*  Too many links (to a file) */
#define EPIPE      32   /*  Broken pipe */
#define EDOM       33   /*! Input value to math function not in domain */
#define ERANGE     34   /*! Output value from math function out of range */
#define ETIMEOUT   35   /* Device timeout */
#define EFILESYS   36   /* file system corrupted */
#define EDEADLK    45   /*  Operation would cause deadlock */
#define ENOLCK     46   /*  No locks available */
#define ENOSYS     48   /*  Function not implemented */
#define ENOTEMPTY  49   /*  Directory is not empty */
#define ENAMETOOLONG 50 /*  Path name too long */

#endif
