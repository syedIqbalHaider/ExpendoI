/*************************
* <math.h> - Mathematics *
*************************/

#ifndef __MATH
#define __MATH

/* Parts of this based on ADS 1.2 <math.h>:
 * Copyright (C) Codemist Ltd., 1988
 * Copyright 1991-1998 ARM Limited. All rights reserved
 */

#ifndef __MATH_DECLS
#define __MATH_DECLS
#undef __CLIBNS
#ifdef __cplusplus
namespace std {
#define _INLINE static inline
#define __CLIBNS ::std::
extern "C" {
#else /* ndef __cplusplus */
#define _INLINE static __inline
#define __CLIBNS
#endif /* ndef __cplusplus */

#ifndef HUGE_VAL
extern const double __huge_val;
#define HUGE_VAL (__huge_val)
#endif

extern double acos(double /*x*/);
extern double asin(double /*x*/);
extern __pure double atan(double /*x*/);
extern double atan2(double /*y*/, double /*x*/);
extern __pure double cos(double /*x*/);
extern __pure double sin(double /*x*/);
extern double tan(double /*x*/);
#if !defined(__STRICT_ANSI__) && !defined(_ANSI_STRICT)
_INLINE double cotan(double x) { return 1.0/tan(x); }
#endif

extern double cosh(double /*x*/);
extern double sinh(double /*x*/);
extern __pure double tanh(double /*x*/);

extern double exp(double /*x*/);
extern double frexp(double /*value*/, int * /*exp*/);
extern double ldexp(double /*x*/, int /*exp*/);
extern double log(double /*x*/);
extern double log10(double /*x*/);
extern double modf(double /*value*/, double * /*iptr*/);

extern double pow(double /*x*/, double /*y*/);
extern double sqrt(double /*x*/);

extern __pure double ceil(double /*x*/);
extern __pure double fabs(double /*x*/);
extern __pure double floor(double /*d*/);
extern double fmod(double /*x*/, double /*y*/);

#if !defined(__STRICT_ANSI__) && !defined(_ANSI_STRICT)
extern double __softfp_hypot (double /*x*/, double /*y*/);
_INLINE double hypot(double x, double y) { return __CLIBNS __softfp_hypot(x, y); }
_INLINE float hypotf(float x, float y) {return (float)__CLIBNS __softfp_hypot(x,y);}
_INLINE long double hypotl(long double x, long double y) 
 {return (long double)__CLIBNS __softfp_hypot(x, y);}
//extern int isnan(double);
#endif

// Long Maths functions

_INLINE long double atan2l(long double x, long double y)
{ return (__CLIBNS atan2((double)x, (double)y)); }
_INLINE long double fmodl(long double x, long double y)
{ return (__CLIBNS fmod((double)x, (double)y)); }
_INLINE long double frexpl(long double x, int *y)
{ return (__CLIBNS frexp((double)x, y)); }
_INLINE long double ldexpl(long double x, int y)
{ return (__CLIBNS ldexp((double)x, y)); }
_INLINE long double powl(long double x, long double y)
{ return (__CLIBNS pow((double)x, (double)y)); }

_INLINE long double acosl(long double x)   { return (__CLIBNS acos((double)x)); }
_INLINE long double asinl(long double x)   { return (__CLIBNS asin((double)x)); }
_INLINE long double atanl(long double x)   { return (__CLIBNS atan((double)x)); }
_INLINE long double ceill(long double x)   { return (__CLIBNS ceil((double)x)); }
_INLINE long double cosl(long double x)    { return (__CLIBNS cos((double)x)); }
_INLINE long double coshl(long double x)   { return (__CLIBNS cosh((double)x)); }
_INLINE long double expl(long double x)    { return (__CLIBNS exp((double)x)); }
_INLINE long double fabsl(long double x)   { return (__CLIBNS fabs((double)x)); }
_INLINE long double floorl(long double x)  { return (__CLIBNS floor((double)x)); }
_INLINE long double logl(long double x)    { return (__CLIBNS log((double)x)); }
_INLINE long double log10l(long double x)  { return (__CLIBNS log10((double)x)); }
_INLINE long double sinl(long double x)    { return (__CLIBNS sin((double)x)); }
_INLINE long double sinhl(long double x)   { return (__CLIBNS sinh((double)x)); }
_INLINE long double sqrtl(long double x)   { return (__CLIBNS sqrt((double)x)); }
_INLINE long double tanl(long double x)    { return (__CLIBNS tan((double)x)); }
_INLINE long double tanhl(long double x)   { return (__CLIBNS tanh((double)x)); }
_INLINE long double modfl(long double x, long double *y)
{
	double _Di, _Df = __CLIBNS modf((double)x, &_Di);
    *y = (long double)_Di;
    return (_Df); 
}

// Float Maths functions
_INLINE float acosf(float x)	{ return ((float)__CLIBNS acos((double)x)); }
_INLINE float asinf(float x)	{ return ((float)__CLIBNS asin((double)x)); }
_INLINE float atanf(float x)	{ return ((float)__CLIBNS atan((double)x)); }
_INLINE float atan2f(float x, float y) 
{ return ((float)__CLIBNS atan2((double)x, (double)y)); }
_INLINE float ceilf(float x)	{ return ((float)__CLIBNS ceil((double)x)); }
_INLINE float cosf(float x)	{ return ((float)__CLIBNS cos((double)x)); }
_INLINE float coshf(float x)   { return ((float)__CLIBNS cosh((double)x)); }
_INLINE float expf(float x)    { return ((float)__CLIBNS exp((double)x)); }
_INLINE float fabsf(float x)   { return ((float)__CLIBNS fabs((double)x)); }
_INLINE float floorf(float x)  { return ((float)__CLIBNS floor((double)x)); }
_INLINE float fmodf(float x, float y)  
{ return ((float)__CLIBNS fmod((double)x, (double)y)); }
_INLINE float logf(float x)
{ return ((float)__CLIBNS log((double)x)); }
_INLINE float log10f(float x)  { return ((float)__CLIBNS log10((double)x)); }
_INLINE float powf(float x, float y)
{ return ((float)__CLIBNS pow((double)x, (double)y)); }
_INLINE float sinf(float x)    { return ((float)__CLIBNS sin((double)x)); }
_INLINE float sinhf(float x)   { return ((float)__CLIBNS sinh((double)x)); }
_INLINE float sqrtf(float x)   { return ((float)__CLIBNS sqrt((double)x)); }
_INLINE float tanf(float x)    { return ((float)__CLIBNS tan((double)x)); }
_INLINE float tanhf(float x)   { return ((float)__CLIBNS tanh((double)x)); }
_INLINE float modff(float x, float *y) 
{ 
	double _Di, _Df = __CLIBNS modf((double)x, &_Di);
        *y = (float)_Di;
        return ((float)_Df); 
}

//C++ supports conversion
#ifdef __cplusplus
  extern "C++" {
    inline float abs(float __x)   { return __CLIBNS fabsf(__x); }
    inline float acos(float __x)  { return __CLIBNS acosf(__x); }
    inline float asin(float __x)  { return __CLIBNS asinf(__x); }
    inline float atan(float __x)  { return __CLIBNS atanf(__x); }
    inline float atan2(float __y, float __x)    { return __CLIBNS atan2f(__y,__x); }
    inline float ceil(float __x)  { return __CLIBNS ceilf(__x); }
    inline float cos(float __x)   { return __CLIBNS cosf(__x); }
    inline float cosh(float __x)  { return __CLIBNS coshf(__x); }
    inline float exp(float __x)   { return __CLIBNS expf(__x); }
    inline float frexp(float __x, int *__y)   
    { return ((float)__CLIBNS frexp((double)__x, __y)); }
    inline float ldexp(float __x, int __y)   
    { return ((float)__CLIBNS ldexp((double)__x, __y)); }
    inline float fabs(float __x)  { return __CLIBNS fabsf(__x); }
    inline float floor(float __x) { return __CLIBNS floorf(__x); }
    inline float fmod(float __x, float __y)     { return __CLIBNS fmodf(__x, __y); }
    inline float log(float __x)   { return __CLIBNS logf(__x); }
    inline float log10(float __x) { return __CLIBNS log10f(__x); }
    inline float modf(float __x, float* __iptr) { return __CLIBNS modff(__x, __iptr); }
    inline float pow(float __x, float __y)      { return __CLIBNS powf(__x,__y); }
    inline float pow(float __x, int __y)     { return __CLIBNS powf(__x, (float)__y); }
    inline float sin(float __x)   { return __CLIBNS sinf(__x); }
    inline float sinh(float __x)  { return __CLIBNS sinhf(__x); }
    inline float sqrt(float __x)  { return __CLIBNS sqrtf(__x); }
    inline float tan(float __x)   { return __CLIBNS tanf(__x); }
    inline float tanh(float __x)  { return __CLIBNS tanhf(__x); }

    inline double abs(double __x) { return __CLIBNS fabs(__x); }
    inline double pow(double __x, int __y)
                { return __CLIBNS pow(__x, (double) __y); }

    inline long double abs(long double __x)
                { return (long double) __CLIBNS fabsl(__x); }
    inline long double acos(long double __x)
                { return (long double) __CLIBNS acosl(__x); }
    inline long double asin(long double __x)
                { return (long double) __CLIBNS asinl(__x); }
    inline long double atan(long double __x)
                { return (long double) __CLIBNS atanl(__x); }
    inline long double atan2(long double __y, long double __x)
                { return (long double) __CLIBNS atan2l(__y, __x); }
    inline long double ceil(long double __x)
                { return (long double) __CLIBNS ceill( __x); }
    inline long double cos(long double __x)
                { return (long double) __CLIBNS cosl(__x); }
    inline long double cosh(long double __x)
                { return (long double) __CLIBNS coshl(__x); }
    inline long double exp(long double __x)
                { return (long double) __CLIBNS expl(__x); }
    inline long double fabs(long double __x)
                { return (long double) __CLIBNS fabsl(__x); }
    inline long double floor(long double __x)
                { return (long double) __CLIBNS floorl(__x); }
    inline long double fmod(long double __x, long double __y)
                { return (long double) __CLIBNS fmodl(__x, __y); }
    inline long double frexp(long double __x, int* __p)
                { return (long double) __CLIBNS frexpl(__x, __p); }
    inline long double ldexp(long double __x, int __exp)
                { return (long double) __CLIBNS ldexpl(__x, __exp); }
    inline long double log(long double __x)
                { return (long double) __CLIBNS logl(__x); }
    inline long double log10(long double __x)
                { return (long double) __CLIBNS log10l(__x); }
    inline long double modf(long double __x, long double* __p)
                { return (long double) __CLIBNS modfl(__x, __p); }
    inline long double pow(long double __x, long double __y)
                { return (long double) __CLIBNS powl(__x, __y); }
    inline long double pow(long double __x, int __y)
                { return (long double) __CLIBNS powl(__x, __y); }
    inline long double sin(long double __x)
                { return (long double) __CLIBNS sinl(__x); }
    inline long double sinh(long double __x)
                { return (long double) __CLIBNS sinhl(__x); }
    inline long double sqrt(long double __x)
                { return (long double) __CLIBNS sqrtl(__x); }
    inline long double tan(long double __x)
                { return (long double) __CLIBNS tanl(__x); }
    inline long double tanh(long double __x)
                { return (long double) __CLIBNS tanhl(__x); }
  }
}  /* extern "C" */
}  /* namespace std */
#endif
#endif /* __ERRNO_DECLS */
  #if defined(__cplusplus) && !defined(__MATH_NO_EXPORTS)
    using ::std::acos;
    using ::std::asin;
    using ::std::atan;
    using ::std::atan2;
    using ::std::cos;
    using ::std::sin;
    using ::std::tan;
    using ::std::cosh;
    using ::std::sinh;
    using ::std::tanh;
    using ::std::exp;
    using ::std::frexp;
    using ::std::ldexp;
    using ::std::log;
    using ::std::log10;
    using ::std::modf;
    using ::std::pow;
    using ::std::sqrt;
    using ::std::ceil;
    using ::std::fabs;
    using ::std::floor;
    using ::std::fmod;
#if !defined(__STRICT_ANSI__) && !defined(_ANSI_STRICT)
    using ::std::cotan;
    using ::std::__softfp_hypot;
    using ::std::hypot;
    using ::std::hypotf;
    using ::std::hypotl;
//    using ::std::isnan;
    using ::std::atan2l;
    using ::std::fmodl;
    using ::std::frexpl;
    using ::std::ldexpl;
    using ::std::powl;
    using ::std::acosl;
    using ::std::asinl;
    using ::std::atanl;
    using ::std::ceill;
    using ::std::cosl;
    using ::std::coshl;
    using ::std::expl;
    using ::std::fabsl;
    using ::std::floorl;
    using ::std::logl;
    using ::std::log10l;
    using ::std::sinl;
    using ::std::sinhl;
    using ::std::sqrtl;
    using ::std::tanl;
    using ::std::tanhl;
    using ::std::modfl;
    using ::std::acosf;
    using ::std::asinf;
    using ::std::atanf;
    using ::std::atan2f;
    using ::std::ceilf;
    using ::std::cosf;
    using ::std::coshf;
    using ::std::expf;
    using ::std::fabsf;
    using ::std::floorf;
    using ::std::fmodf;
    using ::std::logf;
    using ::std::log10f;
    using ::std::powf;
    using ::std::sinf;
    using ::std::sinhf;
    using ::std::sqrtf;
    using ::std::tanf;
    using ::std::tanhf;
    using ::std::modff;
#endif
  #endif

#endif 
