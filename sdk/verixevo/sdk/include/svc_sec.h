/* SVC_SEC.H - Verix Security Services Interface */
/* Copyright 2003-2010 by VeriFone, Inc.  All Rights Reserved. */

#ifndef  __SVC_SEC_H
#define  __SVC_SEC_H

#include <svc.h>
#include <errno.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
;|===========================================================================+
;| NOTE: | ERROR CODES
;|===========================================================================+
*/

#define LIBERROR            (int)0x60000000

/* Key Management & Pin Entry */
#define E_LKM                       LIBERROR+0x00060000
#define E_KM_NO_KEY_LOADED          E_LKM+0x0400
#define E_KM_NO_ALGO_SELECTED       E_KM_NO_KEY_LOADED
#define E_KM_NO_DUKPT_INIT          E_KM_NO_KEY_LOADED
#define E_KM_KEY_INTEGRITY_ERROR    E_LKM+0x0800
#define E_KM_FEATURE_DISABLED       E_LKM+0x0C00
#define E_KM_OUT_OF_RANGE           E_LKM+0x1000
#define E_KM_INVALID_PARAMETER      E_KM_OUT_OF_RANGE
#define E_KM_BAD_SEQUENCE           E_LKM+0x1400
#define E_KM_DUKPT_EOL              E_LKM+0x1800
#define E_KM_BUSY                   E_LKM+0x1C00
#define E_KM_HW_PB                  E_LKM+0x2000
#define E_KM_ACCESS_DENIED          E_LKM+0x2400
#define E_KM_DUPLICATE_KEY          E_LKM+0x2800
#define E_KM_NOT_IN_PIN             E_LKM+0x2C00
#define E_KM_WRITE_ERR_PIN          E_LKM+0x3000
#define E_KM_SYSTEM_ERROR           E_LKM+0x8000

/* VeriShield Security Script */
#define E_LVS                       LIBERROR+0x00070000
#define E_VS_SCRIPT_NOT_LOADED      E_LVS+0x0400
#define E_VS_MACRO_NOT_EXIST        E_LVS+0x0800
#define E_VS_MACRO_EXECUTION        E_LVS+0x0C00
#define E_VS_BAD_LENGTH             E_LVS+0x1000
#define E_VS_BAD_CHAINING           E_LVS+0x1400
#define E_VS_LOADING                E_LVS+0x1800
#define E_VS_ERR_RET                E_LVS+0x2000
#define E_VS_SYSTEM_ERROR           E_LVS+0x8000

/* Crypto Algorithms */
#define E_LCA                       LIBERROR+0x00090000
#define E_LCA_KEY_PARITY_ERROR      E_LCA+0x0400
#define E_LCA_MEMORY_ERROR          E_LCA+0x0800
#define E_LCA_WRONG_SIGNATURE       E_LCA+0x0C00
#define E_LCA_INVALID_PARAMETER     E_LCA+0x1000

/*
;|===========================================================================+
;| NOTE: | DEFINITION
;|===========================================================================+
*/

/* iPS_LoadMasterClearKey() */
/* iPS_LoadMasterEncKey() */
#define LD_VSS0         0
#define LD_VSS1         1
#define LD_VSS2         2
#define LD_VSS3         3
#define LD_VSS4         4
#define LD_VSS5         5
#define LD_VSS6         6
#define LD_VSS7         7
#define LD_VSS8         8
#define LD_VSS9         9
#define LD_VSS10       10
#define LD_VSS11       11
#define LD_VSS12       12
#define LD_VSS13       13
#define LD_VSS14       14
#define LD_VSS15       15
#define LD_VSS16       16
#define LD_VSS17       17
#define LD_VSS18       18
#define LD_VSS19       19
#define LD_VSS20       20
#define LD_VSS21       21
#define LD_VSS22       22
#define LD_VSS23       23
#define LD_VSS24       24
#define LD_VSS25       25
#define LD_VSS26       26
#define LD_VSS27       27
#define LD_VSS28       28
#define LD_VSS29       29
#define LD_VSS30       30
#define LD_VSS31       31
#define LD_VSS32       32
#define LD_VSS33       33
#define LD_VSS34       34
#define LD_VSS35       35
#define LD_VSS36       36
#define LD_VSS37       37
#define LD_VSS38       38
#define LD_VSS39       39
#define LD_VSS40       40
#define LD_VSS41       41
#define LD_VSS42       42
#define LD_VSS43       43
#define LD_VSS44       44
#define LD_VSS45       45
#define LD_VSS46       46
#define LD_VSS47       47
#define LD_VSS48       48
#define LD_VSS49       49
#define LD_VSS50       50
#define LD_VSS51       51
#define LD_VSS52       52
#define LD_VSS53       53
#define LD_VSS54       54
#define LD_VSS55       55
#define LD_VSS56       56
#define LD_VSS57       57
#define LD_VSS58       58
#define LD_VSS59       59
#define LD_VSS60       60
#define LD_VSS61       61
#define LD_VSS62       62
#define LD_VSS63       63

/* iPS_DeleteKeys() */
#define DEL_SYSTEM      (unsigned long)0x00000001
#define DEL_DUKPT0      (unsigned long)0x00000002
#define DEL_DUKPT1      (unsigned long)0x00000004
#define DEL_DUKPT2      (unsigned long)0x00000008
#define DEL_VSS0        (unsigned long)0x00000020
#define DEL_VSS1        (unsigned long)0x00000040
#define DEL_VSS2        (unsigned long)0x00000080
#define DEL_VSS3        (unsigned long)0x00000100
#define DEL_VSS4        (unsigned long)0x00000200
#define DEL_VSS5        (unsigned long)0x00000400
#define DEL_VSS6        (unsigned long)0x00000800
#define DEL_VSS7        (unsigned long)0x00001000
#define DEL_IPP         (unsigned long)0x00008000
#define DEL_ALL         (unsigned long)0xFFFFFFFF

/* iPS_DeleteKeys2() */
#define DEL_VSS8        (unsigned long)0x00000001
#define DEL_VSS9        (unsigned long)0x00000002
#define DEL_VSS10       (unsigned long)0x00000004
#define DEL_VSS11       (unsigned long)0x00000008
#define DEL_VSS12       (unsigned long)0x00000010
#define DEL_VSS13       (unsigned long)0x00000020
#define DEL_VSS14       (unsigned long)0x00000040
#define DEL_VSS15       (unsigned long)0x00000080
#define DEL_VSS16       (unsigned long)0x00000100
#define DEL_VSS17       (unsigned long)0x00000200
#define DEL_VSS18       (unsigned long)0x00000400
#define DEL_VSS19       (unsigned long)0x00000800
#define DEL_VSS20       (unsigned long)0x00001000
#define DEL_VSS21       (unsigned long)0x00002000
#define DEL_VSS22       (unsigned long)0x00004000
#define DEL_VSS23       (unsigned long)0x00008000
#define DEL_VSS24       (unsigned long)0x00010000
#define DEL_VSS25       (unsigned long)0x00020000
#define DEL_VSS26       (unsigned long)0x00040000
#define DEL_VSS27       (unsigned long)0x00080000
#define DEL_VSS28       (unsigned long)0x00100000
#define DEL_VSS29       (unsigned long)0x00200000
#define DEL_VSS30       (unsigned long)0x00400000
#define DEL_VSS31       (unsigned long)0x00800000
#define DEL_VSS32       (unsigned long)0x01000000
#define DEL_VSS33       (unsigned long)0x02000000
#define DEL_VSS34       (unsigned long)0x04000000
#define DEL_VSS35       (unsigned long)0x08000000
#define DEL_VSS36       (unsigned long)0x10000000
#define DEL_VSS37       (unsigned long)0x20000000
#define DEL_VSS38       (unsigned long)0x40000000
#define DEL_VSS39       (unsigned long)0x80000000

/* iPS_DeleteKeys3() */
#define DEL_VSS40       (unsigned long)0x00000001
#define DEL_VSS41       (unsigned long)0x00000002
#define DEL_VSS42       (unsigned long)0x00000004
#define DEL_VSS43       (unsigned long)0x00000008
#define DEL_VSS44       (unsigned long)0x00000010
#define DEL_VSS45       (unsigned long)0x00000020
#define DEL_VSS46       (unsigned long)0x00000040
#define DEL_VSS47       (unsigned long)0x00000080
#define DEL_VSS48       (unsigned long)0x00000100
#define DEL_VSS49       (unsigned long)0x00000200
#define DEL_VSS50       (unsigned long)0x00000400
#define DEL_VSS51       (unsigned long)0x00000800
#define DEL_VSS52       (unsigned long)0x00001000
#define DEL_VSS53       (unsigned long)0x00002000
#define DEL_VSS54       (unsigned long)0x00004000
#define DEL_VSS55       (unsigned long)0x00008000
#define DEL_VSS56       (unsigned long)0x00010000
#define DEL_VSS57       (unsigned long)0x00020000
#define DEL_VSS58       (unsigned long)0x00040000
#define DEL_VSS59       (unsigned long)0x00080000
#define DEL_VSS60       (unsigned long)0x00100000
#define DEL_VSS61       (unsigned long)0x00200000
#define DEL_VSS62       (unsigned long)0x00400000
#define DEL_VSS63       (unsigned long)0x00800000

/* DES() */
#define DESX1KE     0x02    /* DEAX encryption with single-length key */
#define DESX1KD     0x03    /* DEAX decryption with single-length key */
#define DESX2KE     0x04    /* DEAX encryption with double-length key */
#define DESX2KD     0x05    /* DEAX decryption with double-length key */
#define DESX3KE     0x06    /* DEAX encryption with triple-length key */
#define DESX3KD     0x07    /* DEAX decryption with triple-length key */
#define DESE        0x08    /* DEA encryption with single-length key  */
#define DESD        0x09    /* DEA decryption with single-length key  */
#define TDES2KE     0x0C    /* TDEA encryption with double-length key */
#define TDES2KD     0x0D    /* TDEA decryption with double-length key */
#define TDES3KE     0x0E    /* TDEA encryption with triple-length key */
#define TDES3KD     0x0F    /* TDEA decryption with triple-length key */

/* AES() */
#define AES128E     0x04    /* AES encryption using a 128-bit key */
#define AES128D     0x05    /* AES decryption using a 128-bit key */
#define AES192E     0x06    /* AES encryption using a 192-bit key */
#define AES192D     0x07    /* AES decryption using a 192-bit key */
#define AES256E     0x08    /* AES encryption using a 256-bit key */
#define AES256D     0x09    /* AES decryption using a 256-bit key */

/* SHA digest sizes */
#define SHA1_DIGEST_SIZE    20
#define SHA256_DIGEST_SIZE  32

/* Account Data Encryption */

// ADE Encryption Algorithms
#define ADE_ALG_TDEA    0

// ADE Encryption Modes of Operation
#define ADE_MODE_ECB    0
#define ADE_MODE_CBC    1

// ADE IV Settings
#define ADE_IV_NONE     0
#define ADE_IV_ZERO     1
#define ADE_IV_RAND     2

// ADE Padding Schemes
#define ADE_PAD_NONE    0
#define ADE_PAD_PKCS7   1
#define ADE_PAD_X923    2
#define ADE_PAD_ISO7816 3

// ADE Return Codes
#define ADE_SUCCESS 0
#define ADE_ERR_PM_PTR  -1
#define ADE_ERR_PM_LEN  -2
#define ADE_ERR_PM_KEY  -3
#define ADE_ERR_PM_ALG  -4
#define ADE_ERR_PM_MODE -5
#define ADE_ERR_PM_IV   -6
#define ADE_ERR_PM_PAD  -7
#define ADE_ERR_NO_KEY  -11
#define ADE_ERR_OFF     -12
#define ADE_ERR_GENERIC -99

// ADE Status Codes (bitmap)
#define ADE_STS_ENG0    1<<0
#define ADE_STS_ENG1    1<<1
#define ADE_STS_ENG2    1<<2
#define ADE_STS_ENG3    1<<3
#define ADE_STS_ENG4    1<<4
#define ADE_STS_ENG5    1<<5
#define ADE_STS_ENG6    1<<6
#define ADE_STS_ENG7    1<<7
#define ADE_STS_ENG8    1<<8
#define ADE_STS_ENG9    1<<9
#define ADE_STS_ON      1<<10
#define ADE_STS_ENABLED 1<<11
#define ADE_STS_ENG_ALL 0x03FF

/*
;|===========================================================================+
;| NOTE: | TYPEDEF
;|===========================================================================+
*/
typedef struct {
    unsigned char   ucMin;
    unsigned char   ucMax;
    unsigned char   ucEchoChar;
    unsigned char   ucDefChar;
    unsigned char   ucOption;
} PINPARAMETER;


typedef struct {
    unsigned char   nbPinDigits;
    unsigned char   encPinBlock[8];
} PINRESULT;

/* Account Data Encryption */

typedef struct
{
    unsigned char* ptext;   // Plaintext
    int ptextLen;           // Plaintext Length
    int keyIndex;           // Key Index
    int encAlg;             // Encryption Algorithm
    int encMode;            // Encryption Mode of Operation
    int iv;                 // IV Setting
    int pad;                // Padding Scheme
} ade_encrypt_in;

typedef struct
{
    unsigned char* ctext;   // Ciphertext
    int ctextLen;           // Ciphertext Length
    unsigned char* ivData;  // IV Data
    unsigned char* supData; // Supplementary Data
} ade_encrypt_out;

/*
;|===========================================================================+
;| NOTE: | PUBLIC FUNCTION PROTOTYPES
;|===========================================================================+
*/
#ifndef __GNUC__
#define _SYS static

/* FUNCTIONS CALLABLE ONLY IF THE CRYPTO DRIVER IS OPENED */

/* Key Loading */
_SYS int iPS_DeleteKeys (unsigned long ulKeyType);
_SYS int iPS_DeleteKeys2 (unsigned long ulKeyType);
_SYS int iPS_DeleteKeys3 (unsigned long ulKeyType);
_SYS int iPS_LoadSysClearKey (unsigned char ucKeyID,
                              unsigned char *pucINKeyValue);
_SYS int iPS_LoadSysEncKey (unsigned char ucKeyID,
                            unsigned char *pucINKeyValue);
_SYS int iPS_LoadMasterClearKey (unsigned char ucKeySetID,
                                 unsigned char ucKeyID,
                                 unsigned char *pucINKeyValue);
_SYS int iPS_LoadMasterEncKey (unsigned char ucKeySetID,
                               unsigned char ucKeyID,
                               unsigned char *pucINKeyValue);
_SYS int iPS_CheckMasterKey(unsigned char ucKeySetID,
                            unsigned char ucKeyID,
                            unsigned char *pucINKVC);

/* PIN entry */
_SYS int iPS_SetPINParameter (PINPARAMETER *psKeypadSetup);
_SYS int iPS_SetPINBypassKey (unsigned char ucPinBypassKey);
_SYS int iPS_SelectPINAlgo (unsigned char ucPinFormat);
_SYS int iPS_RequestPINEntry (unsigned char ucPANDataSize,
                              unsigned char *pucINPANData);
_SYS int iPS_GetPINResponse (int *piStatus, PINRESULT *pOUTData);
_SYS int iPS_CancelPIN (void);
_SYS int iPS_WriteAtDuringPINEntry(unsigned char * buf, int len, int x, int y);
_SYS int iPS_DisableOKEnterAfterPINEntry(void);
_SYS int iPS_EnableOKEnterAfterPINEntry(void);

/* VeriShield Security Script */
_SYS int iPS_GetScriptStatus (unsigned char ucScriptNumber,
                              unsigned char *pucINName );
_SYS int iPS_InstallScript (char *pucINName);
_SYS int iPS_UninstallScript (unsigned char ucScriptNumber);
_SYS int iPS_ExecuteScript (unsigned char ucScriptNumber,
                            unsigned char ucMacroID,
                            unsigned short usINDataSize,
                            unsigned char *pucINData,
                            unsigned short usMaximumOUTDataSize,
                            unsigned short *pusOUTDataSize,
                            unsigned char *pucOUTData );
_SYS char* pcPS_GetVSSVersion (void);


/* FUNCTIONS CALLABLE EVEN IF THE CRYPTO DRIVER IS NOT OPENED: */

/* Miscellaneous Crypto Functions */
_SYS int SHA1 (unsigned char *unused, unsigned char *input_buffer,
               unsigned long nb, unsigned char *sha20);
_SYS int SHA256 (unsigned char *unused, unsigned char *input_buffer,
                 unsigned long nb, unsigned char *sha32);
_SYS int rsa_calc (unsigned short *msg, unsigned short *mod,
                   int wds, int exp, unsigned short *result);
_SYS int DES (unsigned char ucDeaOption, unsigned char *pucDeaKey8N,
              unsigned char *pucInputData, unsigned char *pucOutputData);
_SYS int AES (unsigned char ucAesOption, unsigned char *pucAesKey8N,
              unsigned char *pucInputData, unsigned char *pucOutputData);
_SYS int get_file_hash_SHA1 (const char *file, int *result5);

/* PRNG */
_SYS int GenerateRandom (unsigned char *random8);

/* File Encryption */
_SYS int crypto_write (int hdl, const char *buf, int len);
_SYS int crypto_read (int hdl, char *buf, int len);

/* Attack Flag */
_SYS int isAttacked (void);
/* Get stored tamper info */
_SYS int get_tamper (int index, unsigned char *tamperInfo);

/* Key Storage */
_SYS int CheckKeyAreaIntegrity (void);

/* Account Data Encryption */
_SYS int ade_encrypt (ade_encrypt_in* in, ade_encrypt_out* out);
_SYS int ade_status (void);
_SYS int ade_active (void);

#undef _SYS
#endif //__GNUC__

/*
;|===========================================================================+
;| NOTE: | PUBLIC FUNCTION
;|===========================================================================+
*/

#if defined(__cplusplus) || defined(__GNUC__)
#define _INLINE static inline
#else
#define _INLINE static __inline
#endif

/* FUNCTIONS CALLABLE ONLY IF THE CRYPTO DRIVER IS OPENED */

/* Key Loading */
_INLINE int iPS_DeleteKeys(unsigned long ulKeyType)
   { return (_control(11,8,(char*)ulKeyType))?errno:0; }

_INLINE int iPS_DeleteKeys2(unsigned long ulKeyType)
   { return (_control(11,13,(char*)ulKeyType))?errno:0; }

_INLINE int iPS_DeleteKeys3(unsigned long ulKeyType)
   { return (_control(11,14,(char*)ulKeyType))?errno:0; }

_INLINE int iPS_LoadSysClearKey (unsigned char ucKeyID,
                                  unsigned char *pucINKeyValue)
    { struct {unsigned char a1, *a2;} w;
      w.a1=ucKeyID; w.a2=pucINKeyValue;
      return (_control(11,9,(char*)&w))?errno:0;
    }

_INLINE int iPS_LoadSysEncKey (unsigned char ucKeyID,
                                unsigned char *pucINKeyValue)
    { struct {unsigned char a1, *a2;} w;
      w.a1=ucKeyID; w.a2=pucINKeyValue;
      return (_control(11,10,(char*)&w))?errno:0;
    }

_INLINE int iPS_LoadMasterClearKey (unsigned char ucKeySetID,
                                     unsigned char ucKeyID,
                                     unsigned char *pucINKeyValue)
    { struct {unsigned char a1, a2, *a3;} w;
      w.a1=ucKeySetID; w.a2=ucKeyID; w.a3=pucINKeyValue;
      return (_control(11,11,(char*)&w))?errno:0;
    }

_INLINE int iPS_LoadMasterEncKey (unsigned char ucKeySetID,
                                   unsigned char ucKeyID,
                                   unsigned char *pucINKeyValue)
    { struct {unsigned char a1, a2, *a3;} w;
      w.a1=ucKeySetID; w.a2=ucKeyID; w.a3=pucINKeyValue;
      return (_control(11,12,(char*)&w))?errno:0;
    }

_INLINE int iPS_CheckMasterKey (unsigned char ucKeySetID,
                                 unsigned char ucKeyID,
                                 unsigned char *pucINKVC)
    { struct {unsigned char a1, a2, *a3;} w;
      w.a1=ucKeySetID; w.a2=ucKeyID; w.a3=pucINKVC;
      return (_status(11, 1, (char*)&w))?errno:0;
    }

/* PIN entry */
_INLINE int iPS_SetPINParameter (PINPARAMETER *psKeypadSetup)
    { return (_control(11,24,(char*)psKeypadSetup))?errno:0; }

_INLINE int iPS_SetPINBypassKey (unsigned char ucPinBypassKey)
    { return (_control(11,34,(char*)ucPinBypassKey))?errno:0; }

_INLINE int iPS_SelectPINAlgo (unsigned char ucPinFormat)
    { return (_control(11,25,(char*)ucPinFormat))?errno:0; }

_INLINE int iPS_RequestPINEntry (unsigned char ucPANDataSize,
                                 unsigned char *pucINPANData)
    { struct {unsigned char a1, *a2;} w;
      w.a1=ucPANDataSize; w.a2=pucINPANData;
      return (_control(11,26,(char*)&w))?errno:0;
    }

_INLINE int iPS_GetPINResponse (int *piStatus, PINRESULT *pOUTData)
    { struct {int *a1; PINRESULT *a2;} w;
      w.a1=piStatus; w.a2=pOUTData;
      return (_status(11,3,(char*)&w))?errno:0;
    }

_INLINE int iPS_CancelPIN (void)
    { return (_ctl2(11,27))?errno:0; }

_INLINE int iPS_WriteAtDuringPINEntry(const char * buf, int len, int x, int y)
	{	struct {int x; int y; int l; const char *b;} w;
		w.x=x; w.y=y; w.l=len; w.b=buf;
		return (_control(11, 35, (char*)&w))?errno:0;
	}
	
_INLINE int iPS_DisableOKEnterAfterPINEntry(void)
	{
		return (_control(11, 36, NULL))?errno:0;
	}
	
_INLINE int iPS_EnableOKEnterAfterPINEntry(void)
	{
		return (_control(11, 37, NULL))?errno:0;
	}
	
/* VeriShield Security Script */
_INLINE int iPS_InstallScript (char *pucINName)
    { return (_control(11,28,pucINName))?errno:0; }

_INLINE int iPS_GetScriptStatus (unsigned char ucScriptNumber,
                                  unsigned char *pucINName)
    { struct {unsigned char a1, *a2;} w;
      w.a1=ucScriptNumber; w.a2=pucINName;
      return (_status(11,4,(char*)&w))?errno:0;
    }

_INLINE int  iPS_UninstallScript (unsigned char ucScriptNumber)
   { return (_control(11,29,(char*)ucScriptNumber))?errno:0; }

_INLINE int iPS_ExecuteScript (unsigned char ucScriptNumber,
                                unsigned char ucMacroID,
                                unsigned short usINDataSize,
                                unsigned char *pucINData,
                                unsigned short usMaximumOUTDataSize,
                                unsigned short *pusOUTDataSize,
                                unsigned char *pucOUTData)
    { struct {unsigned char a1, a2; unsigned short a3; unsigned char *a4;
              unsigned short a5, *a6; unsigned char *a7;} w;
      w.a1=ucScriptNumber; w.a2=ucMacroID; w.a3=usINDataSize; w.a4=pucINData;
      w.a5=usMaximumOUTDataSize; w.a6=pusOUTDataSize; w.a7=pucOUTData;
      return (_control(11,30,(char*)&w))?errno:0;
    }
_INLINE char* pcPS_GetVSSVersion (void)
    {
      static char sVSSVersion[] = "PSVSSv1.0";

      _control(11,33,(char*)&sVSSVersion[0]);
      return (&sVSSVersion[0]);
    }

/* FUNCTIONS CALLABLE EVEN IF THE CRYPTO DRIVER IS NOT OPENED: */

/* Miscellaneous Crypto Functions */
_INLINE int SHA1 (unsigned char *unused,
                  unsigned char *input_buffer,
	              unsigned long nb,
	              unsigned char *sha20)
    { return _sec4(12, (int)input_buffer, (int)nb, (int)sha20); }

_INLINE int SHA256 (unsigned char *unused,
                  unsigned char *input_buffer,
                  unsigned long nb,
                  unsigned char *sha32)
    { return _sec4(35, (int)input_buffer, (int)nb, (int)sha32); }

_INLINE int rsa_calc (unsigned short *msg,
                      unsigned short *mod,
                      int wds,
                      int exp,
                      unsigned short *result)
    { struct {unsigned short *a1, *a2; int a3, a4; unsigned short *a5;} w;
      w.a1=msg; w.a2=mod; w.a3=wds; w.a4=exp; w.a5=result;
      return _sec2(13, (int)&w);
    }

_INLINE int DES (unsigned char ucDeaOption, unsigned char *pucDeaKey8N,
                 unsigned char *pucInputData, unsigned char *pucOutputData)
    { struct {unsigned char a1, *a2, *a3, *a4;} w;
      w.a1=ucDeaOption; w.a2=pucDeaKey8N;
      w.a3=pucInputData; w.a4=pucOutputData;
      return _sec2(18, (int)&w);
    }

_INLINE int AES (unsigned char ucAesOption, unsigned char *pucAesKey8N,
                 unsigned char *pucInputData, unsigned char *pucOutputData)
    { struct {unsigned char a1, *a2, *a3, *a4;} w;
      w.a1=ucAesOption; w.a2=pucAesKey8N;
      w.a3=pucInputData; w.a4=pucOutputData;
      return _sec2(19, (int)&w);
    }

_INLINE int GenerateRandom (unsigned char *random8)
    { return _sec2(14,(int)random8); }

_INLINE int get_file_hash_SHA1 (const char *file, int *result5)
    { return _dir3(41, (char*)file, (char*)result5); }

/* File Encryption */
_INLINE int crypto_write (int hdl, const char *buf, int len)
    { return _sec4(15, hdl, (int)buf, len); }

_INLINE int crypto_read (int hdl, char *buf, int len)
    { return _sec4(16, hdl, (int)buf, len); }

/* Attack Flag */
_INLINE int isAttacked (void)
    { return _sec1(17); }

/* get stored tamper info */
_INLINE int get_tamper(int index, unsigned char *tamperInfo)
    { return _sec3(20, index, (int)tamperInfo); }

/* Key Storage */
_INLINE int CheckKeyAreaIntegrity (void)
    { return _sec1(21); }

/* Account Data Encryption */
_INLINE int ade_encrypt (ade_encrypt_in* in, ade_encrypt_out* out)
    { return _sec3(37, (int)in, (int)out); }

_INLINE int ade_status (void)
    { return _sec1(38); }

_INLINE int ade_active (void)
    { return _sec1(39); }


#undef _INLINE

#ifdef __cplusplus
}
#endif

#endif /* _SVC_SEC_H */

/*
;|===========================================================================+
;|          END   END   END   END     END    END   END   END  END  END
;|===========================================================================+
*/
