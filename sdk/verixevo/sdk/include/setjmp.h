/******************************
* <setjmp.h> - Nonlocal Jumps *
******************************/
#ifndef __SETJMP
#define __SETJMP

/* This is a simplified version of the ADS 1.2 <setjmp.h>:
 * Copyright (C) ARM Ltd., 1999
 * All rights reserved
 * Copyright (C) Codemist Ltd., 1988
 * Copyright 1991 ARM Limited. All rights reserved. */

#ifndef __SETJMP_DECLS
#define __SETJMP_DECLS
#undef __CLIBNS
#ifdef __cplusplus
namespace std {
#define __CLIBNS ::std::
extern "C" {
#else /* ndef __cplusplus */
#define __CLIBNS
#endif /* ndef __cplusplus */

/*
 * setjmp.h declares two functions and one type, for bypassing the normal
 * function call and return discipline (useful for dealing with unusual
 * conditions encountered in a low-level function of a program).
 */

/* Note: This buffer is probably much larger than necessary.  The EABI library
 * document says that 128 bytes are currently used but makes the standard size
 * 256 for future growth.  I don't know where the 384-byte size declared in the
 * RVCT 2.0 header comes from, but since we are calling the RVCT functions we
 * had better define it in the same way. */
typedef __int64 jmp_buf[48];

extern int setjmp(jmp_buf /*env*/);
   /* Saves its calling environment in its jmp_buf argument, for later use
    * by the longjmp function.
    * Returns: If the return is from a direct invocation, the setjmp function
    *          returns the value zero. If the return from a call to the longjmp
    *          function, the setjmp function returns a non zero value.
    */

extern void longjmp(jmp_buf /*env*/, int /*val*/);
   /* Restores the environment saved by the most recent call to setjmp in the
    * same invocation of the program, with the corresponding jmp_buf argument.
    * If there has been no such call, or if the function containing the call
    * to setjmp has terminated execution (eg. with a return statement) in the
    * interim, the behaviour is undefined.
    * All accessible objects have values as of the time longjmp was called,
    * except that the values of objects of automatic storage duration that do
    * not have volatile type and have been changed between the setjmp and
    * longjmp calls are indeterminate.
    * As it bypasses the usual function call and return mechanism, the longjmp
    * function shall execute correctly in contexts of interrupts, signals and
    * any of their associated functions. However, if the longjmp function is
    * invoked from a nested signal handler (that is, from a function invoked as
    * a result of a signal raised during the handling of another signal), the
    * behaviour is undefined.
    * Returns: After longjmp is completed, program execution continues as if
    *          the corresponding call to setjmp had just returned the value
    *          specified by val. The longjmp function cannot cause setjmp to
    *          return the value 0; if val is 0, setjmp returns the value 1.
    */

#ifdef __cplusplus
}  /* extern "C" */
}  /* namespace std */
#endif /* __SETJMP_DECLS */
#endif
  #if defined(__cplusplus) && !defined(__SETJMP_NO_EXPORTS)
    using ::std::jmp_buf;
    using ::std::setjmp;
    using ::std::longjmp;
  #endif

#endif
