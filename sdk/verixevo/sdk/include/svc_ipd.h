/* SVC_ipd.H - Verix Kernel IP download Services Interface */
/* Copyright 2011 by VeriFone, Inc.  All Rights Reserved. */

#ifndef _SVC_IPD_H
#define _SVC_IPD_H

#ifdef __cplusplus
extern "C" {
#endif

/* Auxilliary function for SVC_DOWNLOAD_NET and download_net. */
typedef short (*fpNotify)(short shActionId, void *vActionValue);
typedef short (*fpshPostProcess) (void * vDataReceived, short shReceivedLen,
               void * vDataToProcess, short *pshDataLength );
typedef short (*fpshPreProcess) ( void * vData, short shDataLen,
               void * vPreProcessedData, short *pshProcessedDataLength );
short register_extended_processing(fpshPreProcess PreProcess, fpshPostProcess PostProcess);
short register_notify_cb(fpNotify CbNotify,short usermask);
short set_param(unsigned short shParamID, void *ParamValue);
// Values for events handled in the call back functions
// Bit mask for performing actions
#define UPDATE_DISPLAY      0x01
#define VERIFY_CANCEL       0x02
#define NEW_FILE            0x04
#define SWITCH_DRIVE        0x08
#define SWITCH_GROUP        0x10
#define TIME_CHANGE         0x20
#define INVALID_PACKET      0x30
// OSDL Parameter values for set_param
#define IPDL_TCPTIMEOUT     900
#define IPDL_ENQTIMEOUT     901
// Return codes while receiving/processing packets
#define DLE_SUCCESS             0
#define DLE_USER_CANCEL     -1      // user pressed cancel key
#define DLE_TCPIP_SSL_ERROR -2      // for error encountered on TCPIP and SSL
#define DLE_BAD_TX_COM      -3      // received too many NAKs
#define DLE_HOST_CANCEL     -4      // remote host sent 'U' packet
#define DLE_NO_SSL_SUPPORT  -5      // Cannot link N:/SSL.LIB
#define DLE_BAD_WRITE       -6      // opsys write() to file failed
#define DLE_HOST_TO         -7      // *timed out waiting for host
#define DLE_LOST_CARRIER    -8      // lost carrier from modem
#define DLE_BAD_RX_COM      -9      // *sent too many NAKs
#define DLE_BAD_TIMER       -10     // *opsys set_timer() failed
#define DLE_BAD_COMPORT     -11     // *opsys write() to COMx failed
#define DLE_ENQ_TO          -14     // *timed out waiting for ENQ
#define DLE_BAD_SOCKET      -15     // socket connect not successful
//set_param() return codes
#define DLE_INVALID_PARAM   -20    // Invalid parameter
#define DLE_NOT_SUPPORTED   -21    // Parameter is not supported
#define DLE_CONTINUE_PROCESS_SKIP_RECV 95
#define DLE_CONTINUE_RECV_RESET_TIMER  96 // Server sent HTTP 100. Continue
                                          // receive after resetting the timer.
#define DLE_CONTINUE_RECV        97 // Partial data received: continue
#define DLE_CONTINUE_PROCESS     98 // Data receive completed. Continue processing.
#define DLE_MEANINGFUL           99 // Successful download
#define DLE_MEANINGLESS         100 // Successful (meaningless) download,
                                    // since no data downloaded.
#define DLE_GOT_PACKET          101 // Got packet with good CRC
#define DLE_ICHARTIMEOUT        102 // Inter-character time out
#define DLE_ABORT_DOWNLOAD      103 // Received data is invalid. Abort!

#ifdef __cplusplus
}
#endif

#endif
