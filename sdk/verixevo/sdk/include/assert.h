/***************************
* <assert.h>               *
***************************/

/*
 * Note that <assert.h> may be included more that once in a program with
 * different setting of NDEBUG. Hence the slightly unusual first-time
 * only flag.
 */

#undef assert
#ifndef __ASSERT
#define __ASSERT
    #undef __CLIBNS
    #ifdef __cplusplus
    namespace std {
    #define __CLIBNS ::std::
    extern "C" {
    #else /* ndef __cplusplus */
    #define __CLIBNS
    #endif /* ndef __cplusplus */
    void _assert(char *, unsigned short, char *);
    #ifdef __cplusplus
    }  /* extern "C" */
    }  /* namespace std */
    #endif
#endif	/* __ASSERT */

#ifdef NDEBUG
    #define assert(test) ((void)0)
#else /* NDEBUG */
#define _STR(x) _VAL(x)
#define _VAL(x) #x
#define assert(test) ((test) ? (void) 0 \
 : __CLIBNS _assert(__FILE__, __LINE__, __FILE__ ":" _STR(__LINE__) " " #test))
#endif	/* NDEBUG */
