/*********************************
* <stdlib.h> - General Utilities *
*********************************/
#ifndef __STDLIB
#define __STDLIB

#ifndef __STDLIB_DECLS
#define __STDLIB_DECLS

#undef __CLIBNS
#ifdef __cplusplus
namespace std {
#define _INLINE static inline
#define __CLIBNS ::std::
extern "C" {
#else /* ndef __cplusplus */
#define _INLINE static __inline
#define __CLIBNS
#endif /* ndef __cplusplus */

#if defined(__cplusplus) || !defined(__STRICT_ANSI__)
 /* unconditional in C++ and non-strict C for consistency of debug info */
  typedef unsigned int size_t;
#elif !defined(__size_t)
  #define __size_t 1
  typedef unsigned int size_t;   /* see <stddef.h> */
#endif

#undef NULL
#ifdef __cplusplus
# define NULL 0
#else
# define NULL (void *)0
#endif                   /* see <stddef.h> */

#ifndef __cplusplus  /* wchar_t is a builtin type for C++ */
  #if !defined(__STRICT_ANSI__)
   /* unconditional in non-strict C for consistency of debug info */
    typedef unsigned short wchar_t; /* see <stddef.h> */
  #elif !defined(__wchar_t)
    #define __wchar_t 1
    typedef unsigned short wchar_t; /* see <stddef.h> */
  #endif
#endif

typedef struct { int  quot; int  rem; }  div_t;
typedef struct { long quot; long rem; } ldiv_t;

#define EXIT_FAILURE    1
#define EXIT_SUCCESS    0
#define RAND_MAX        32767
#define MB_CUR_MAX      1

double atof( const char * );
int atoi( const char * );
long atol( const char * );
double strtod( const char *, char ** );
long strtol( const char *, char **, int );
unsigned long strtoul( const char *, char **, int );
int rand( void );
void srand( unsigned );
void *calloc( size_t, size_t );
void free( void * );
void *malloc( size_t );
void *realloc( void *, size_t );
void exit( int );
char *getenv( const char * );
void *bsearch( const void *, const void *, size_t, size_t,
              int (*)(const void*, const void*) );
void qsort( void *, size_t, size_t, int (*)(const void*, const void*) );
int abs( int );
div_t div( int, int );
ldiv_t ldiv( long, long );
long labs( long );
#ifdef __cplusplus
   extern "C++" inline long abs(long x) { return labs(x); }
   extern "C++" inline ldiv_t div(long __numer, long __denom) {
       return ldiv(__numer, __denom);
   }
#endif

#if !defined(__STRICT_ANSI__) && !defined(_ANSI_STRICT)

void *malloc_all (long*);
int check_heap (void **addr, unsigned long *data);

#ifdef __GNUC__
#define _INLINE static inline
#endif
_INLINE void *mallocl (long size)
  { return malloc(size); }
_INLINE void *reallocl (void *ptr, long size)
  { return realloc(ptr, size); }
  
extern const unsigned short _version;       /* this program's version */
extern const unsigned short _SYS_VERSION;   /* sys. lib. interface version */
unsigned short _syslib_version ( void );    /* system library version */
long _heap_max( void );
long _heap_current( void );
long _stack_max( void );
int  _debugging( void );

#ifdef _SHARED_LIB
extern const short _app_version;
#else
extern const long _heapsize;
extern const long _stacksize;
#endif

#endif

#ifdef __cplusplus
}  /* extern "C" */
}  /* namespace std */
#endif
#endif /* __STDLIB_DECLS */
#if defined(__cplusplus) && !defined(__STDLIB_NO_EXPORTS)
    using ::std::div_t;
    using ::std::ldiv_t;
    using ::std::atof;
    using ::std::atoi;
    using ::std::atol;
    using ::std::strtod;
    using ::std::strtol;
    using ::std::strtoul;
    using ::std::rand;
    using ::std::srand;
    using ::std::calloc;
    using ::std::free;
    using ::std::malloc;
    using ::std::realloc;
    using ::std::exit;
    using ::std::getenv;
    using ::std::bsearch;
    using ::std::qsort;
    using ::std::abs;
    using ::std::div;
    using ::std::labs;
    using ::std::ldiv;
#if !defined(__STRICT_ANSI__) && !defined(_ANSI_STRICT)
    using ::std::malloc_all;
    using ::std::check_heap;
    using ::std::mallocl;
    using ::std::reallocl;
    using ::std::_version;
    using ::std::_SYS_VERSION;
    using ::std::_syslib_version;
    using ::std::_heap_max;
    using ::std::_heap_current;
    using ::std::_stack_max;
    using ::std::_debugging;
#ifdef _SHARED_LIB
    using ::std::_app_version;
#else
    using ::std::_heapsize;
    using ::std::_stacksize;
#endif
#endif
#endif
#endif
