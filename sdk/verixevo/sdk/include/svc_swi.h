/* Verix Software Interrupt Definitions */

/* Basic SWI handler functions.  Application code should never call these
 * directly!  Some calls have multiple names to support different numbers
 * and types of arguments. */

/* Copyright 2002-2010 by VeriFone, Inc. All Rights Reserved. */

#ifdef __GNUC__
#define __swi(n)
#endif

int  _write (int, const char*, int)             __swi( 0);
int  _read (int, char*, int)                    __swi( 1);
int  _control (int, int, char*)                 __swi( 2);
int  _ctl2 (int, int)                           __swi( 2);
int  _ctl3 (int, int, int)                      __swi( 2);
int  _ctl4 (int, int, char*, int)               __swi( 2);
int  _status (int, int, char*)                  __swi( 3);
int  _sts2 (int, int)                           __swi( 3);
long _sts2l (int, int)                          __swi( 3);
int  _sts3 (int, int, int)                      __swi( 3);
int  _sts4 (int, int, char*, int)               __swi( 3);
unsigned int _sts2ul (int, int)                 __swi( 3);
int  _close (int)                               __swi( 4);
int  _open (const char*, int)                   __swi( 5);
long _lseek (int, long, int)                    __swi( 6);
int  _dir1 (int)                                __swi( 7);
int  _dir2 (int, char*)                         __swi( 7);
int  _dir3 (int, char*, char*)                  __swi( 7);
int  _dir4 (int, int, char*, int)               __swi( 7);
int  _getkey (const char*, char*, int, const char*)       __swi( 8);
int  _putkey (const char*, const char*, int, const char*) __swi( 9);
int  _sch1 (int)                                __swi(10);
int  _sch2 (int, int)                           __swi(10);
int  _sch3 (int, int, int)                      __swi(10);
int  _sch4 (int, int, int, int)                 __swi(10);
int  _sch2A(int, char*)                         __swi(10);
int  _info1 (int)                               __swi(11);
int  _info2 (int, char*)                        __swi(11);
int  _info2i (int, int)                         __swi(11);
int  _info3 (int, int, char*)                   __swi(11);
int  _info3i (int, int, int)                    __swi(11);
int  _info3pi (int, char*, int)                 __swi(11);
int  _info4 (int, char*, char*, int)            __swi(11);
int  _info4i (int, int, int, int)               __swi(11);
int  _dvc1 (int)                                __swi(12);
int  _dvc2 (int,int)                            __swi(12);
int  _dvc3 (int,int,int)                        __swi(12);
int  _sec1(int)                                 __swi(14);
int  _sec2(int, int)                            __swi(14);
int  _sec3(int, int, int)                       __swi(14);
int  _sec4(int, int, int, int)                  __swi(14);
int  _secc2(int, char*)                         __swi(14);
int  _secc3(int, char*, char*)                  __swi(14);
int  _rkl1(int)                                 __swi(15);
int  _rkl2(int, int)                            __swi(15);
int  _rkl3(int, int, int)                       __swi(15);
int  _rkl4(int, int, int, int)                  __swi(15);


/* User functions which invoke SWIs */

#if defined(__cplusplus)
#define _INLINE static inline
#define _UNUSED(type, name) type
#elif defined(__GNUC__)
#define _INLINE static inline
#define _UNUSED(type, name) type name
#else
#define _INLINE static __inline
#define _UNUSED(type, name) type name
#endif

// ------------ Basic Posix I/O
_INLINE int write (int hdl, const char *buf, int len)
    { return _write(hdl, buf, len); }
_INLINE int read (int hdl, char *buf, int len)
    { return _read(hdl, buf, len); }
_INLINE int close (int hdl)
    { return _close(hdl); }
_INLINE int open (const char *id, int flags)
    { return _open(id, flags); }
_INLINE long lseek (int hdl, long off, int orig)
    { return _lseek(hdl, off, orig); }

// ------------ Directory-oriented file system functions
_INLINE int dir_get_first (char *buf)
    { return _dir2(2, buf); }
_INLINE int dir_get_next (char *buf)
    { return _dir2(3, buf); }
_INLINE int _remove (const char *file)
    { return _dir2(4, (char*)file); }
_INLINE int _rename (const char *oldname, const char *newname)
    { return _dir3(5, (char*)oldname, (char*)newname); }
_INLINE int dir_get_sizes (const char *drive, struct fs_size* sizes)
    { return _dir3(7, (char*)drive, (char*)sizes); }
_INLINE int SVC_CHECKFILE (const char *file)
    { return _dir2(8, (char*)file); }
_INLINE int dir_get_file_date (const char *file, char *date)
    { return _dir3(12, (char*)file, date); }
_INLINE int dir_put_file_date (const char *file, const char *date)
    { return _dir3(13, (char*)file, (char*)date); }
_INLINE int dir_get_attributes (const char *file)
    { return _dir2(17, (char*)file); }
_INLINE int dir_set_attributes (const char *file, int attr)
    { return _dir3(18, (char*)file, (char*)attr); }
_INLINE int dir_reset_attributes (const char *file, int attr)
    { return _dir3(19, (char*)file, (char*)attr); }
_INLINE int dir_flash_coalesce (void)
    { return _dir3(20, (char*)"F:", (char*)0); }
_INLINE long dir_get_file_size (const char *file)
    { return (long)_dir2(21, (char*)file); }
_INLINE int get_component_vars (int hdl, char *buf, int len)
    { return _dir4(23, hdl, buf, len); }
_INLINE long dir_get_file_sz (const char *file)
    { return (long)_dir2(26, (char*)file); }
_INLINE int dir_flash_coalesce_size (long *size)
    { return _dir3(27, (char*)"F:", (char*)size); }
_INLINE int getkey (const char *key, char *buf, int size, const char *file)
    { return _getkey(key, buf, size, (char*)file); }
_INLINE int putkey (const char *key, const char *buf, int size, const char *file)
    { return _putkey(key, buf, size, (char*)file); }
_INLINE int get_env (const char *key, char *buf, int size)
    { return getkey(key, buf, size, 0); }
_INLINE int put_env (const char *key, const char *buf, int size)
    { return putkey(key, buf, size, 0); }
_INLINE int dir_get_all_attributes (const char *file)
    { return _dir2(58, (char*)file); }
_INLINE int chdir (const char *file)
    { return _dir2(59, (char*)file); }
_INLINE int getcwd (char *buf, int len)
    { return _dir3(60, (char*)buf,(char*)len); }
_INLINE int mkdir (const char *file)
    { return _dir2(62,(char*)file); }
_INLINE int rmdir (const char *file)
    { return _dir2(63,(char*)file); }

// ------------ Variable length records
#ifndef __cplusplus /* conflicts with C++ keyword - use delete_ */
_INLINE int delete (int hdl, unsigned int count)
    { return _control(hdl, 0, (char*)count); }
#endif
_INLINE int delete_ (int hdl, unsigned int count)
    { return _control(hdl, 0, (char*)count); }
_INLINE int insert (int hdl, const char *buf, int size)
    { return _ctl4(hdl, 1, (char*)buf, size); }
_INLINE int read_vlr (int hdl, char *buf, int size)
    { return _sts4 (hdl, 2, buf, size); }
_INLINE int write_vlr (int hdl, const char *buf, int size)
    { return _ctl4 (hdl, 2, (char*)buf, size); }
_INLINE long seek_vlr (int hdl, long off, int orig)
    { return _sts4 (hdl, 4, (char*)off, orig); }
_INLINE int insert_vlr (int hdl, const char *buf, int size)
    { return _ctl4 (hdl, 3, (char*)buf, size); }
_INLINE int delete_vlr (int hdl, unsigned int count)
    { return _control(hdl, 6, (char*)count); }
_INLINE int read_cvlr (int hdl, char *buf, int size)
    { return _sts4 (hdl, 3, buf, size); }
_INLINE int write_cvlr (int hdl, const char *buf, int size)
    { return _ctl4 (hdl, 4, (char*)buf, size); }
_INLINE long seek_cvlr (int hdl, long off, int orig)
    { return _sts4 (hdl, 4, (char*)off, orig); }
_INLINE int insert_cvlr (int hdl, const char *buf, int size)
    { return _ctl4 (hdl, 5, (char*)buf, size); }
_INLINE int delete_cvlr (int hdl, unsigned int count)
    { return _control(hdl, 6, (char*)count); }

// ------------ Other file functions
_INLINE int lock (int hdl, _UNUSED(long, off), _UNUSED(long, len))
    { return _ctl2(hdl, 7); }
_INLINE int unlock (int hdl, _UNUSED(long, off), _UNUSED(long, len))
    { return _ctl2(hdl, 8); }
_INLINE int get_file_size (int hdl, long *size)
    { return _status(hdl, 0, (char*)size); }
_INLINE int get_file_date (int hdl, char *buf)
    { return _status(hdl, 1, buf); }
_INLINE int put_file_date (int hdl, const char *buf)
    { return _control(hdl, 11, (char*)buf); }
_INLINE int get_file_attributes (int hdl)
    { return _status(hdl, 6, (char*)0); }
_INLINE int set_file_attributes (int hdl, int attr)
    { return _control(hdl, 9, (char*)attr); }
_INLINE int reset_file_attributes (int hdl, int attr)
    { return _control(hdl, 10, (char*)attr); }
_INLINE long get_file_max (int hdl)
    { return _sts2l(hdl, 8); }
_INLINE int set_file_max (int hdl, long maxsize)
    { return _control(hdl, 12, (char*)maxsize); }
_INLINE int file_copy_auth_bit (const char *source, const char *target)
    { return _dir3(55,(char*)source, (char*)target); }
_INLINE int tell_page (int hdl)
    { return _sts2(hdl, 9); }
_INLINE int flash_diag_data (int diag_type, int *diag_buf, int diag_ct)
    { return _dir4(76, diag_type, (char*)diag_buf, diag_ct); }

// ------------ Miscellaneous device manager calls
_INLINE int get_owner (const char *id, int *task_id)
    { return _dvc3(0, (int)id, (int)task_id); }
_INLINE int set_owner (int handle, int task_id)
    { return _dvc3(1, handle, task_id); }
_INLINE int get_name (int handle, char *dev_id)
    { return _dvc3(2, handle, (int)dev_id); }
_INLINE unsigned long get_usb_device_bits (void)
    { return (unsigned long) _dvc1(4); }
_INLINE unsigned long get_usb_switch_bits (void)
    { return (unsigned long) _dvc1(13); }
_INLINE unsigned long get_sd_device_bits (void)
    { return (unsigned long) _dvc1(6); }
_INLINE int grant_owner (int handle, int task_id)
    { return _dvc3(10, handle, task_id); }
_INLINE int revoke_owner (int handle)
    { return _dvc2(11, handle); }


// ------------ Console Interface
_INLINE int activate_task (int task_id)
    { return _control(1, 101, (char*)task_id); }
_INLINE int clreol (void)
    { return _ctl2(1, 19); }
_INLINE int clrscr (void)
    { return _ctl2(1, 17); }
_INLINE int contrast_down (void)
    { return _control(1, 28, (char*)65); }
_INLINE int contrast_up (void)
    { return _control(1, 28, (char*)64); }
_INLINE int delline(void)
    { return _ctl2(1, 25); }

_INLINE int set_display_color(int type, int color)
    { return _ctl4(1, 33, (char*)type, color); }
_INLINE int get_display_color(int type)
    { return _status(1, 10, (char*)type); }

_INLINE int set_display_coordinate_mode(int mode)
    { return _control(1, 34, (char*)mode); }
_INLINE int get_display_coordinate_mode(void)
    { return _sts2(1, 11); }


_INLINE int disable_hot_key (void)
    { return _status(1, 50, (char*)0); }
_INLINE int key_beeps (int flag)
    { return _control(1, 51, (char*)flag); }
_INLINE void disable_key_beeps (void)
    { key_beeps(0); }
_INLINE void enable_key_beeps (void)
    { key_beeps(1); }
_INLINE int enable_hot_key (void)
    { return _status(1, 50, (char*)1); }

_INLINE int sts_kbd_lock (void)
    { return _sts2(1,56); }
_INLINE int lock_kbd (int flag)
    { return _control(1,55,(char*)flag); }
_INLINE int enable_kbd (void)
    { return lock_kbd(0); }
_INLINE int disable_kbd (void)
    { return lock_kbd(1); }

_INLINE int cs_hard_reset(void)
    { return _control(1, 59, (char*)0); }
_INLINE int cs_soft_reset(void)
    { return _control(1, 58, (char*)0); }
_INLINE int cs_set_baseline(int scantype)
    { return _control(1, 57, (char*)scantype); }
_INLINE int cs_spi_read(int encrypted, int command)
    {   struct {int encrypt; int cmd; char *data;} w;
        w.encrypt=encrypted; w.cmd=command;
        return _control(1, 63, (char*)&w);
    }
_INLINE int cs_read_temperature(void)
    { return _status(1, 64, (char*)0); }
_INLINE int is_keypad_secure(void)
    { return _status(1, 65, (char*)0); }
_INLINE int cs_set_sleep_state(int sleep)
    { return _control(1, 66, (char*)sleep); }
_INLINE int cs_overlay_scan(int scantype)
    { return _control(1, 67, (char*)scantype); }


_INLINE int cs_spi_write(int encrypted, int command, unsigned char *data)
    {   struct {int encrypt; int cmd; char *data;} w;
        w.encrypt=encrypted; w.cmd=command; w.data=(char *)data;
        return _control(1, 62, (char*)&w); }
_INLINE int cs_spi_cmd_status(void)
    { return _status(1, 60, (char*)0); }
_INLINE int cs_spi_cmd_data(char *data)
    { return _status(1, 61, (char*)data); }

_INLINE int getcontrast (void)
    { return _sts2(1, 6); }
_INLINE void getfont (char *font)
    { _status(1, 0, (char*)font); }
_INLINE int getgrid (void)
    { int rc = _sts2(1, 1);
      if (rc==CHAR_SIZE_6x8) rc= GRID_8x17; else if (rc>0) rc=GRID_4x12;
      return rc;
    }
_INLINE int getinverse (void)
    { return _sts2(1, 9); }
_INLINE int getscrollmode (void)
    { return _sts2(1, 5); }
_INLINE int get_console (int clear_keys)
    { return _control(1, 54, (char*)clear_keys); }
_INLINE int get_font (char *font_name)
    { return _status(1, 0, (char*)font_name); }
_INLINE int get_font_mode (void)
    { return _sts2(1, 2); }
_INLINE int gotoxy (int x,int y)
    { int p[2]; p[0]=x; p[1]=y; return _control(1, 21, (char*)p); }
_INLINE int insline(void)
    { return _ctl2(1, 24); }
_INLINE int inverse_toggle (void)
    { return _control(1, 31, (char*)3); }
_INLINE int kbd_pending_count (void)
    { return _sts2(1, 48); }
_INLINE int kbd_pending_test (int t)
    { return _status(1,49,(char*)t); }
_INLINE int putpixelcol (const char *buffer, int len)
    { return _ctl4(1, 26, (char*)buffer, len); }
_INLINE int put_graphic (const char *buf, int len, int x1, int y1, int x2, int y2)
    { struct {int x1; int y1; int x2; int y2; int l; const char *b;} w;
      w.x1=x1; w.y1=y1; w.x2=x2; w.y2=y2; w.l=len; w.b=buf;
      return _control(1,27,(char*)&w);
    }
_INLINE int window (int startX, int startY, int endX, int endY)
    { int p[4];
      p[0]=startX; p[1]=startY; p[2]=endX; p[3]=endY;
      return _control(1,18,(char*)p);
    }
_INLINE int resetdisplay (const char *font, _UNUSED(int, grid_id))
    { int rc = _control(1, 16, (char*)font);
      if (rc == 0) rc = window (-1,-1,-1,-1); // create "full screen" window
                            //  not specific cuz we don't know what platform or addressing mode
      if (rc == 0) rc = clrscr();
      return rc;
    }
_INLINE int put_BMP (char *file)
    { return _control(1,37,(char*)file); }

_INLINE int put_BMP_at (int x, int y, char *file)
    { int p[3];
      p[0]=x; p[1]=y; p[2]=(int)file;
      return _control(1,43,(char*)p); }

_INLINE int get_BMP (int startX, int startY, int endX, int endY, char *buffer, int color)
    { int p[6];
      p[0]=startX; p[1]=startY; p[2]=endX; p[3]=endY; p[4]=(int)buffer; p[5]=color;
      return _status(1,15,(char*)p); }

_INLINE int write_pixels (int startX, int startY, int endX, int endY, int color)
    { int p[5];
      p[0]=startX; p[1]=startY; p[2]=endX; p[3]=endY; p[4]=color;
      return _control(1,35,(char*)p);
    }
_INLINE int draw_line (int startX, int startY, int endX, int endY, int width, int color)
    { int p[6];
      p[0]=startX; p[1]=startY; p[2]=endX; p[3]=endY; p[4]=width; p[5]=color;
      return _control(1,36,(char*)p);
    }
_INLINE int copy_pixel_block (int srcStartX, int srcStartY, int srcEndX, int srcEndY, int dstStartX, int dstStartY, int dstEndX, int dstEndY)
    { int p[8];
      p[0]=srcStartX; p[1]=srcStartY; p[2]=srcEndX; p[3]=srcEndY;
      p[4]=dstStartX; p[5]=dstStartY; p[6]=dstEndX; p[7]=dstEndY;
      return _control(1,38,(char*)p);
    }
_INLINE int invert_pixel_block (int startX, int startY, int endX, int endY)
    { int p[4];
      p[0]=startX; p[1]=startY; p[2]=endX; p[3]=endY;
      return _control(1,39,(char*)p);
    }
_INLINE int display_frame_buffer(int x, int y, int w, int h, short * buffer)
    { int p[5];
      p[0]=x; p[1]=y; p[2]=w; p[3]=h; p[4]=(int)buffer;
      return _control(1,42,(char*)p);
    }
_INLINE int screen_size (char *buf)
    { return _status(1, 7, buf); }
_INLINE int get_battery_icon(char *buff3)
    { return _status(1, 8, (char*)buff3); }
_INLINE int setcontrast (int flag)
    { return _control(1, 28, (char*)flag); }
_INLINE int setfont (const char *font)
    { return _control(1, 16, (char*)font); }
_INLINE int setinverse (int flag)
    { return _control(1, 31, (char*)flag); }
_INLINE int setscrollmode (int mode)
    { return _control(1, 20, (char*)mode); }
_INLINE int set_backlight (int flag)
    { return _control(1, 29, (char*)flag); }
_INLINE int set_kb_backlight (int value)
    { return _control(1, 45, (char*)value); }
_INLINE int set_backlight_level (int value)
    { return _control(1, 41, (char*)value); }
_INLINE int get_backlight_level (void)
    { return _sts2(1, 14); }
_INLINE int get_kb_backlight (void)
    { return _sts2(1, 200); }
_INLINE int set_cursor (int flag)
    { return _control(1,22,(char*)flag); }
_INLINE int set_font (const char *font_name)
    { return _control(1, 16, (char*)font_name); }
_INLINE int set_hot_key (int keycode)
    { return _control(1, 52, (char*)keycode); }
_INLINE int set_bcm(int mode)
    { return _control(1, 30, (char*)mode); }
_INLINE int wherecur (int *x, int *y)
    { int rc, p[3]; p[2]=0;
      rc = _status(1, 4, (char*)p);
      if (rc==0) {*x=p[0], *y=p[1];}
      return rc;
    }

_INLINE int get_character_size(int *rowSize, int *colSize)
    { int rc, p[2];
      rc= _status(1, 12, (char*)p);
      if (rc == 0) {*rowSize=p[0]; *colSize=p[1]; }
      return rc;
    }

_INLINE int wherewin (int *x1, int *y1, int *x2, int *y2)
    { int rc, p[4];
      rc = _status(1, 3, (char*)p);
      if (rc == 0) { *x1=p[0]; *y1=p[1]; *x2=p[2]; *y2=p[3]; }
      return rc;
    }
_INLINE int wherewincur (int *x, int *y)
    { int rc, p[3]; p[2]=1;
      rc = _status(1, 4, (char*)p);
      if (rc == 0) { *x=p[0]; *y=p[1]; }
      return rc;
    }
#ifdef _O3XXX_COMPATIBLE_CONSOLE
// On O3xxx TXO, write_at interprets coordinates as
// screen vs window relative.  Default behavior on
// Predator is window relative coordinates.  If the
// O3xxx TXO behavior is required on Predator, apps
// can #define _O3XXX_COMPATIBLE_CONSOLE
// but use of this feature is deprecated.
_INLINE int write_at (const char *b, int l, int x, int y)
    { struct {int x; int y; int l; const char *b;} w;
      int x1, y1, x2, y2;
      wherewin(&x1,&y1,&x2,&y2);
      w.x=x-x1+1; w.y=y-y1+1; w.l=l; w.b=b;
      return _control(1,23,(char*)&w);
    }
#else
_INLINE int write_at (const char *b, int l, int x, int y)
    { struct {int x; int y; int l; const char *b;} w;
      w.x=x; w.y=y; w.l=l; w.b=b;
      return _control(1,23,(char*)&w);
    }
#endif
// Load compatibility frame panel
_INLINE int set_fkey_panel(char *bmp_file_ptr, int which_panel)
    // which_panel values: TITLE_PANEL, FKEY_PANEL, HKEY_PANEL in conspriv.h
    {   int p[2];
        p[0] = (int)bmp_file_ptr;
        p[1] = (int)which_panel;
        return _control(1,46, (char *)p);
    }

// Get the processed touchscreen coordinates
_INLINE int get_touchscreen (int *x, int *y)
    { int rc, p[3];
      p[0]=1;
      rc = _status(1, 13, (char*)p);
      if (rc) {*x=p[1], *y=p[2];}
      return rc;
    }
// Position touchscreen key
_INLINE int set_touchscreen_keymap(tkm_t *map, int ct)
    { return _ctl4(1,95,(char*)map,ct); }

// ------------ Serial ports
_INLINE int get_opn_blk (int hdl, struct Opn_Blk *ob)
    { return _status(hdl, 1, (char*)ob); }
_INLINE int get_port_status (int hdl, char *buf)
    { return _status(hdl, 0, buf); }
_INLINE int reset_port_error (int hdl)
    { return _ctl2(hdl, 1); }
_INLINE int set_opn_blk (int hdl, const struct Opn_Blk *ob)
    { return _control(hdl, 0, (char*)ob); }
_INLINE int set_serial_lines (int hdl, const char *mask)
    { return _control(hdl, 2, (char*)mask); }
_INLINE int set_radio_ctl (int hdl, const char *sigs)
    { return _control(hdl, 4, (char*)sigs); }
_INLINE int get_radio_sts (int hdl, char *sigs)
    { return _status(hdl, 4, (char*)sigs); }
_INLINE int set_com1_pwr (const char *sigs)
    { return _control(5, 5, (char*)sigs); }
_INLINE int set_com_pwr (int hdl, const char *sigs)
    { return _control(hdl, 5, (char*)sigs); }
_INLINE int set_signal_events (int hdl, char *evts)
    { return _control(hdl, 6, (char*)evts); }
_INLINE int set_combo_mode (int mode)
    { return _control(7, 12, (char*)mode); }
_INLINE int get_bits_per_second (int hdl)
    { return _sts2(hdl, 5); }
_INLINE int write_9bit(int hdl, short *buf, int len)
    { return _write(hdl, (char*)buf, len); }
_INLINE int read_9bit(int hdl, short *buf, int len)
    { return _read(hdl, (char*)buf, len); }
_INLINE int openaux(int devhdl)
    { return _control(devhdl,10,NULL); }
_INLINE int closeaux(int devhdl)
    { return _control(devhdl,11,NULL); }
_INLINE int set_gsm_powersave(int devhdl, unsigned int power)
    { return _control(devhdl,12,(char *)power); }
_INLINE int set_gsm_break(int devhdl)
    { return _ctl2(devhdl,13); }
_INLINE int set_com1_event_bit (int hdl, long flag)
    { return _control(hdl, 96, (char*)flag); }
_INLINE long get_com1_event_bit (int hdl)
    { return _sts2l(hdl, 97); }
_INLINE int get_iap_state (int hdl)
    { return _sts2(hdl, 7); }
_INLINE int get_ipod_status (int hdl)
    { return _sts2(hdl, 8); }
_INLINE int set_ipod_pins (int hdl, int mask)
    { return _control(hdl, 15, (char*)mask); }
_INLINE int reset_ipod_pins (int hdl, int mask)
    { return _control(hdl, 16, (char*)mask); }
_INLINE int get_protocol_string (int hdl, char *buf)
    { return _status(hdl, 9, buf); }
_INLINE int get_bundle_id (int hdl, char *buf)
    { return _status(hdl, 10, buf); }
_INLINE int iap_control_function (int hdl, int func)
    { return _control(hdl, 17, (char*)func); }
_INLINE int iap_get_keypad_info(int hdl, char *buf)
    { return _status(hdl, 11, buf); }
_INLINE int iap_get_keypad_state(int hdl, char *buf)
    { return _status(hdl, 12, buf); }
_INLINE int set_SIM_slot (int hdl, int slotNumber)
    { return _ctl3(hdl, 18, slotNumber); }
_INLINE int get_SIM_slot (int hdl)
    { return _ctl2(hdl, 20); }
_INLINE int SIM_SELECT(int simIndex)
    { return _dvc2(12, simIndex); }


// ------------ USB Device calls
_INLINE int get_usbd_status (int hdl)
    { return _status(hdl, 4, 0); }
_INLINE int usb_pending_out (int hdl)
    { return _status(hdl, 5, 0); }
// ------------ Beeper
_INLINE int sound (int note, int msec)
    { return _control(2, note, (char*)msec); }
_INLINE void normal_tone (void)
    { (void)sound(54, 50); }
_INLINE void error_tone (void)
    { (void)sound(48, 100); }
_INLINE void beeper_off (void)
    { (void)sound(0, 0); }

// ------------ PCM device (speaker)
_INLINE int set_pcm_volume(int handle, char vol)
    { return _ctl4(handle, 0, 0, (int)vol); }
_INLINE int get_pcm_volume(int handle, char *vol)
    { return _status(handle, 0, (char*)vol);  }
_INLINE int get_pcm_blk (int hdl, struct pcm_blk *pb)
    { return _status(hdl, 1, (char*)pb); }
_INLINE int set_pcm_blk (int hdl, const struct pcm_blk *pb)
    { return _control(hdl, 1, (char*)pb);  }
//_INLINE int set_pcm_rate(int handle, int rate)
//    { return _ctl4(handle, 1, 0, (int)rate); }
//_INLINE int get_pcm_rate(int handle, int *rate)
//    { return _status(handle, 1, (char*)rate);  }

_INLINE int set_pcm_playback(int handle, pcm_action action)
    { return _ctl4(handle, 2, 0, (int)action); }
_INLINE int get_pcm_status (int hdl, struct pcm_status *status)
    { return _status(hdl, 2, (char*)status); }

// ------------ Other devices
_INLINE int card_pending (void)
    { return _sts2(12, 0); }
_INLINE int card_raw_data (char *buf)
    { return _status(12, 1, buf); }
_INLINE int card_mode (int mode)
    { return _control(12, 1, (char *)mode); }
_INLINE void magprt_mode_control (int mode)
    { card_mode((((~(1<<2))<<16) | ((mode & 1)<<2))); }
_INLINE int card_magprint_stat (char *buf)
    { return _status(12, 3, buf); }
_INLINE int card_magprint_data (char *buf)
    { return _status(12, 4, buf); }
_INLINE int card_magprint_count (void)
    { return _sts2(12, 5); }
_INLINE int barcode_pending (void)
    { return _sts2(15, 0); }
_INLINE int barcode_raw_data (char *buf)
    { return _status(15, 1, buf); }
_INLINE int select_pinpad (int type)
    { return _control(9, 5, (char*)type); }
_INLINE int IPP_power (int type)
    { return _control(9, 6, (char*)type); }
_INLINE int PINentryStatus (void)
    { return _sts2(9, 6); }
_INLINE int TerminatePinEntry (void)
    { return _ctl2(9, 11); }
_INLINE int select_modem (int type)
    { return _control(7, 7, (char*)type); }
_INLINE int radio_power (int type)
    { return _control(7, 8, (char*)type); }
_INLINE int EPP_present (void)
    { return _sts2(9, 4); }
_INLINE int BT_Si2434_profile_load(void)
    { return _control(6, 9, 0); }
_INLINE int set_telephone_state(int state)
    { return _control(132, 80, (char*)state); }
_INLINE int get_telephone_status(void)
    { return _status(132, 81, 0); }

// ------------ Contactless
_INLINE int reset_ctls (void)
    { return _ctl2(17, 14); }
_INLINE int set_event_bit (int hdl, long flag)
    { return _control(hdl, 96, (char*)flag); }
_INLINE long get_event_bit (int hdl)
    { return _sts2l(hdl, 97); }
// ------------ Pipes
_INLINE int pipe_init_msg (int hdl, int msgct)
    { return _control(hdl, 0, (char*)msgct); }
_INLINE int pipe_init_char (int hdl, int charct)
    { return _control(hdl, 1, (char*)charct); }
_INLINE int pipe_connect (int hdl, int dhdl)
    { return _control(hdl, 2, (char*)dhdl); }
_INLINE int pipe_init_msgX (int hdl, int msgct)
    { return _control(hdl, 3, (char*)msgct); }
_INLINE int pipe_pending (int hdl)
    { return _sts2(hdl, 0); }

// ------------ Scheduling and process-oriented calls
_INLINE int set_timer (long msecs, long eventmask)
    { return _sch3(1, msecs, eventmask); }
_INLINE int SVC_WAIT (unsigned int msec)
    { return _sch2(2, msec); }
_INLINE int clr_timer (int timer_id)
    { return _sch2(6, timer_id); }
_INLINE unsigned long read_ticks (void)
    { return _sch1(9); }
_INLINE int read_clock (char *yyyymmddhhmmssw)
    { return _sch2A(10, yyyymmddhhmmssw); }
_INLINE int read_RTC (int hdl, char *yyyymmddhhmmssw)
    { return _status(hdl, 0, yyyymmddhhmmssw); }
_INLINE int _exit (int status)
    { return _sch2(4, status); }
_INLINE long wait_event (void)
    { return (long)_sch1(0); }
_INLINE long peek_event (void)
    { return (long)_sch1(8); }
_INLINE long read_event (void)
    { return (long)_sch1(17); }
_INLINE long wait_evt (long interesting_events)
    { return (long)_sch2(31,interesting_events); }
_INLINE long read_evt (long interesting_events)
    { return (long)_sch2(32,interesting_events); }
_INLINE int post_user_event (int task_id, long user_event)
    { return _sch3(33,task_id,user_event); }
_INLINE long read_user_event (void)
    { return (long)_sch1(34); }
_INLINE int get_task_id (void)
    { return _sch1(7); }
_INLINE int get_task_info (int id, struct task_info *results)
    { return _sch3(3, id, (int)results); }
_INLINE int run (const char *file, const char *parms)
    { return _sch3(12, (int)file, (int)parms); }
_INLINE int run_thread (int rtnaddr, int rtnparm, int stksize)
    { return _sch4(27, rtnaddr,rtnparm,stksize); }
_INLINE int set_group (int group_id)
    { return _sch2(14, group_id); }
_INLINE int get_group (void)
    { return _sch1(15); }
_INLINE int get_native_group (void)
    { return _sch1(37); }
_INLINE int SVC_RESTART (const char *file)
    { return _sch2A(16, (char*)file); }
_INLINE int unzip (const char *file)
    { return _sch2A(23,(char*)file); }
_INLINE int authenticate (const char *file)
    { return _sch2A(45,(char*)file); }
_INLINE int sem_init (sem_t *semaphore,unsigned short value)
    { return _sch3(28,(int)semaphore,(int)value); }
_INLINE int sem_wait (sem_t *semaphore)
    { return _sch2(29,(int)semaphore); }
_INLINE int sem_post (sem_t *semaphore)
    { return _sch2(30,(int)semaphore); }
_INLINE int set_errno_ptr (int *ptr)
    { return _sch2(11, (int)ptr); }
_INLINE volatile int* get_errno_ptr (void)
    { return (volatile int*)_sch1(42); }
_INLINE unsigned long get_performance_counter (void)
    { return _sch1(60); }
_INLINE unsigned long get_performance_frequency (void)
    { return _sch1(61); }
_INLINE int unload_DLL (int funtbl)
    { return _sch2(63, funtbl); }
_INLINE sem_t* sem_open (char *sem_id,unsigned short unused)
    { return (sem_t*)_sch3(65,(int)sem_id,(int)unused); }
_INLINE int sem_close (sem_t *semaphore)
    { return _sch2(66,(int)semaphore); }
_INLINE int sem_prop (sem_t *semaphore,unsigned short mode)
    { return _sch3(67,(int)semaphore,(int)mode); }
_INLINE int thread_origin (void)
    { return _sch1(68); }
_INLINE int thread_cancel(int id)
    { return _sch2(69,id); }
_INLINE int thread_join (int thread_id, int *retval)
    { return _sch3(70, thread_id, (int)retval); }
_INLINE void *shm_open (const char *id, int flag, int mode)
    { return (void*)_sch4(71,(int)id,flag,mode); }
_INLINE int shm_close (void *hdl)
    { return _sch2(72,(int)hdl); }
_INLINE int memory_access (const void* buffer, int length)
    { return _sch3(73,(int)buffer,length); }

// ------------ Information-retrieval calls for terminal management
_INLINE void SVC_INFO_PTID (char *buf)
    { _info2(0, buf); }
_INLINE void SVC_INFO_EPROM (char *buf)
    { _info2(1, buf); }
_INLINE void SVC_VERSION_INFO (char *buf)
    { _info2(2, buf); }
_INLINE int SVC_INFO_MODEM_TYPE (void)
    { return _info1(47); }
_INLINE modem_type_t SVC_INFO_MODEM_TYPE_EX(void)
    { return (modem_type_t)_info1(83); }
_INLINE int SVC_INFO_KEYBRD_TYPE (void)
    { return _info1(53); }
_INLINE int SVC_INFO_KBD_STYLE (void)
    { return _info1(53); }
_INLINE int SVC_LEDS(int mode, void *param )
    { return _info3 (48, mode, (char *)param); }
_INLINE int SVC_RAM_SIZE (void)
    { return _info1(3); }
_INLINE int SVC_INFO_KBD (char *buf)
    { return _info2(4, buf); }
_INLINE int SVC_INFO_MAG (char *buf)
    { return _info2(5, buf); }
_INLINE void SVC_INFO_CRASH (struct info_crash *buf)
    { _info2(6, (char*)buf); }
_INLINE long SVC_INFO_LIFETIME (void)
    { return (long)_info1(7); }
_INLINE int SVC_INFO_RESET (char *yymmddhhmmss)
    { return _info2(8, yymmddhhmmss); }

// ------------ Manufacturing Block retrieval calls
_INLINE int SVC_INFO_MFG_BLK (char *buf)
    { return _info2( 9, buf); }
_INLINE int SVC_INFO_MODELNO (char *buf)
    { return _info2(10, buf); }
_INLINE int SVC_INFO_COUNTRY (char *buf)
    { return _info2(11, buf); }
_INLINE int SVC_INFO_PARTNO (char *buf)
    { return _info2(12, buf); }
_INLINE int SVC_INFO_HW_VERS (char *buf)
    { return _info2(13, buf); }
_INLINE int SVC_INFO_SERLNO (char *buf)
    { return _info2(14, buf); }
_INLINE int SVC_INFO_LOTNO (char *buf)
    { return _info2(15, buf); }
_INLINE int SVC_INFO_MFG_BLK_EXT (char *buf, int len)
    { return _info3pi(61, buf, len); }
_INLINE int SVC_INFO_MODELNO_EXT (char *buf, int len)
    { return _info3pi(62, buf, len); }
_INLINE int SVC_INFO_COUNTRY_EXT (char *buf, int len)
    { return _info3pi(63, buf, len); }
_INLINE int SVC_INFO_PARTNO_EXT(char *buf, int len)
    { return _info3pi(64, buf, len); }
_INLINE int SVC_INFO_HW_VERS_EXT (char *buf, int len)
    { return _info3pi(65, buf, len); }
_INLINE int SVC_INFO_SERLNO_EXT (char *buf, int len)
    { return _info3pi(66, buf, len); }
_INLINE int SVC_INFO_LOTNO_EXT (char *buf, int len)
    { return _info3pi(67, buf, len); }
_INLINE int SVC_INFO_ETH_MAC (char *buf)
    { return _info2(81, buf); }
_INLINE int SVC_INFO_BT_MAC (char *buf)
    { return _info2(82, buf); }

_INLINE int SVC_FLASH_SIZE (void)
    { return _info1(16); }
_INLINE int SVC_INFO_PORT_IR (void)
    { return _info1(17); }
_INLINE int SVC_INFO_PORT_MODEM (void)
    { return _info1(18); }
_INLINE int SVC_INFO_DISPLAY (char *buf)
    { return _info2(19, buf); }
_INLINE int SVC_INFO_PRNTR (char *buf)
    { return _info2(20, buf); }
_INLINE int SVC_INFO_PIN_PAD (char *buf)
    { return _info2(21, buf); }
_INLINE int SVC_INFO_MOD_ID (void)
    { return _info1(23); }
_INLINE int SVC_INFO_MODULE_ID (int d)
    { return _info2i(27, d); }
_INLINE int SVC_INFO_OS_HASH (char *hashout20,char *keyin,int keyinsz)
    { return _info4(35,hashout20,keyin,keyinsz);}
_INLINE int SVC_INFO_OS_HMAC_SHA1 (char *hashout20,char *keyin,int keyinsz)
    { return _info4(35,hashout20,keyin,keyinsz);}
_INLINE int SVC_INFO_OS_EXPIRY_STS (int *remaining)
    { return _info2(44,(char *)remaining); }
_INLINE int SVC_INFO_RELEASED_OS (void)
    { return _info1(45); }
_INLINE int SVC_INFO_DISPLAY_EXT(displayInfo_t * info)
    { return _info2(46, (char*)info); }
_INLINE int SVC_INFO_OS_HMAC_SHA256 (char *hashout32,char *keyin,int keyinsz)
    { return _info4(49,hashout32,keyin,keyinsz);}
_INLINE int SVC_INFO_READ_MIB(char *buf, int len)
    { return _info3pi(50, buf, len); }
_INLINE int SVC_INFO_PRESENT (void)
    { return _info1(54); }
_INLINE int SVC_INFO_DEV_TYPE (int type)
    { return _info2i(55, type); }
_INLINE int SVC_INFO_DEV (int type, char *dev_name)
    { return _info3(56, type, dev_name); }
_INLINE int SVC_INFO_CIB_VER (char *CIBver5, int bufLen)
    { return _info3pi(57, (char*)CIBver5, bufLen); }
_INLINE int SVC_INFO_SBI_VER (char *SBIver5, int bufLen)
    { return _info3pi(58, (char*)SBIver5, bufLen); }
_INLINE int SVC_INFO_CIB_ID (char *CIBid11, int bufLen)
    { return _info3pi(59, (char*)CIBid11, bufLen); }
_INLINE int SVC_INFO_URT0_TYPE(char *char1)
    { return _info2(68, char1); }
_INLINE int SVC_INFO_URT1_TYPE(char *char1)
    { return _info2(69, char1); }
_INLINE int SVC_INFO_URT2_TYPE(char *char1)
    { return _info2(70, char1); }
_INLINE int SVC_INFO_URT3_TYPE(char *char1)
    { return _info2(71, char1); }
_INLINE int SVC_INFO_PORTABLE(char *char1)
    { return _info2(72, char1);}
_INLINE int SVC_INFO_BAT_REQ(char *char1)
    { return _info2(73, char1);}
_INLINE int SVC_INFO_BIO (void)
    { return _info1(75); }
_INLINE int SVC_INFO_PWR_MGMT(int *buf, int len)
    { return _info3(76, (int)buf, (char*)len); }
_INLINE int SVC_INFO_DUAL_SIM()
    { return _info1(77); }
_INLINE int SVC_INFO_BUFMGR(buf_info_t *buf_info_ptr)
    { return _info2(78, (char*)buf_info_ptr); }

// ------------ FIFOs
_INLINE int set_fifo_config (int hdl, const char *buf)
    { return _control(hdl, 3, (char*)buf); }
_INLINE int set_fc_config (int hdl, const char *buf)
    { return _control(hdl, 4, (char*)buf); }
_INLINE int get_fifo_config (int hdl, char *buf)
    { return _status(hdl, 2, buf); }
_INLINE int get_fc_config (int hdl, char *buf)
    { return _status(hdl, 3, buf); }

// ------------ APACS 40
_INLINE int Init_MAC (void)
    { return _sec1(1); }
_INLINE int Create_MAC_Key (int hostnum, const char *A, const char *B)
    { return _sec4(2, hostnum, (int)A, (int)B); }
_INLINE int Calc_Auth_Parm (const char *TranData, char *AuthParm)
    { return _sec3(3, (int)TranData, (int)AuthParm); }
_INLINE int Calc_MAC (const char *buffer, int len, char *mac8)
    { return _sec4(4, (int)buffer, len, (int)mac8); }
_INLINE int New_Host_Key (int hostnum, const char *rqst_residue,
                                        const char *resp_residue)
    { return _sec4(5, hostnum, (int)rqst_residue, (int)resp_residue); }
_INLINE int Reset_Key (int hostnum)
    { return _sec2(6, hostnum); }
_INLINE int Term_MAC (void)
    { return _sec1(7); }

// ------------ Power Functions

#define BATTERYAVAIL 3
_INLINE int get_battery_value (int type)
    { return _sch2(24, type); }
_INLINE int set_battery_value (int type, int value)
    { return _sch3(64, type, value); }
_INLINE int get_battery_sts (void)
    { return _sch1(20); }
_INLINE int get_powersw_sts (void)
    { return _sch1(21); }
_INLINE int get_dock_sts (void)
    { return _sch1(22); }
_INLINE int SVC_SHUTDOWN (void)
    { return _sch1(36); }
_INLINE int BatteryRegs (char *buf)
    { return _sch2A(39,buf); }
_INLINE int BatterySN (char *buf)
    { return _sch2A(74,buf); }
_INLINE int BatteryMfgID (char *buf)
    { return _sch2A(75,buf); }
_INLINE int SVC_SLEEP (void)
    { return _sch1(40); }
_INLINE int get_battery_initialization_status (void)
    { return _sch1(41); }
_INLINE int USB_WIFI_POWER (int pwr)
    { return _dvc2(5, pwr); }
_INLINE int USB_COM2_RESET (void)
    { return _dvc1(7); }
_INLINE int dbdump (const void *buf, int len)
    { return _dvc3(8, (int)buf, len); }
_INLINE int set_owner_all (int hdl)
    { return _dvc2(9, hdl); }
_INLINE int set_usb_multi_device (int hdl, int onOff)
    { return _control(hdl, 99, (char *)onOff); }
_INLINE int start_battery_conditioner(void)
    { return _sch2(24,20); }
_INLINE int battery_conditioner_status(void)
    { return _sch2(24,21); }
_INLINE int reg_presleep(int bitnum)
    { return _sch2(62,bitnum); }
_INLINE int get_host_status(void)
    { return _sch1(75); }
_INLINE int disable_host_power(void)
    { return _control(79, 4, (char *)2); }
_INLINE int enable_host_power(void)
    { return _control(79, 4, (char *)1); }
_INLINE int host_power_status(void)
    { return _sch2(79, 4); }
_INLINE int set_host_power_event(long evt)
    { return _sch3(64, 83, (int)evt); }
// Vx600 VTM use only during download.  Interaction between COM1 and 1Wire
// interfaces.  This is expected to be a tempoary workaround.
_INLINE int pause_battery_monitor(long on_off)
    { return _sch3(64, 84, (int)on_off); }
_INLINE int battery_remain_charge(void)
    { return _sch2(24, 85); }
_INLINE void set_wakeup_event(int bitnum)
    { _sch2(75,bitnum); }

// ------------ Ethernet related functions
_INLINE int get_enet_MAC(int hdl, char *MACbuf)
    { return _status(hdl, 0, MACbuf); }
_INLINE int get_enet_status(int hdl, char *status)
    { return _status(hdl, 1, status); }
_INLINE int set_enet_rx_control(int hdl, int rx_control)
    { return _control(hdl, 1, (char *)rx_control); }

// ------------ UPT Case Removal functions
_INLINE int get_latch_status(void)
    { return _sec1(22); }
_INLINE int reset_latch(char *password)
    { return _secc2(23, password); }
_INLINE int set_activation_hash(char *oldHash, char *newHash)
    { return _secc3(24, oldHash, newHash); }
_INLINE int chk_case_password(char *password)
    { return _secc2(25, password); }
// ------------ VeriShield Protect
_INLINE int VSP_Crypto (int h, char *s)
  { return _control(h, 0, s); }
_INLINE int VSP_Xeq (char *s)
  { return _control(12, 10, s); }
_INLINE int VSP_Result (void)
  { return _ctl2(12, 11); }
_INLINE int VSP_Init (char *st)
  { struct { char c, *p1; } s;
    s.c = 0;
    s.p1 = st;    // status
    return VSP_Xeq((char *)&s); }
_INLINE int VSP_Status (char *st)
  { struct { char c, *p1; } s;
    s.c = 1;
    s.p1 = st;    // status
    return VSP_Xeq((char *)&s); }
_INLINE int VSP_Disable (char *st)
  { struct { char c, *p1; } s;
    s.c = 2;
    s.p1 = st;    // status
    return VSP_Xeq((char *)&s); }
_INLINE int VSP_Enable (char *st)
  { struct { char c, *p1; } s;
    s.c = 3;
    s.p1 = st;    // status
    return VSP_Xeq((char *)&s); }
_INLINE int VSP_Encrypt_MSR (char *MSRc, char *MSRe)
  { struct { char c, *p1, *p2; } s;
    s.c = 4;
    s.p1 = MSRc;  // 3-track card image; for each track: count, status, data
    s.p2 = MSRe;  //  data, if any, includes Start and End sentinels, and LRC
    return VSP_Xeq((char *)&s); }
_INLINE int VSP_Encrypt_KBD (char *PANc, char *EXPc, char *PANe, char *EXPe)
  { struct { char c, *p1, *p2, *p3, *p4; } s;
    s.c = 5;
    s.p1 = PANc;  // 2-19 ASCII digits, null terminated
    s.p2 = EXPc;  // 4 ASCII digits, null terminated
    s.p3 = PANe;
    s.p4 = EXPe;
    return VSP_Xeq((char *)&s); }
_INLINE int VSP_Decrypt_PAN (char *PANe, char *PANc)
  { struct { char c, *p1, *p2; } s;
    s.c = 6;
    s.p1 = PANe;  // 2-19 ASCII digits, null terminated
    s.p2 = PANc;
    return VSP_Xeq((char *)&s); }
_INLINE int VSP_Decrypt_MSR (char *MSRe, char *MSRc)
  { struct { char c, *p1, *p2; } s;
    s.c = 7;
    s.p1 = MSRe;  // 3-track card image; for each track: count, status, data
    s.p2 = MSRc;  //  data, if any, includes Start and End sentinels, and LRC
    return VSP_Xeq((char *)&s); }
_INLINE int VSP_Status_Ex (char *st)
  { struct { char c, *p1; } s;
    s.c = 8;
    s.p1 = st;    // extended status
    return VSP_Xeq((char *)&s); }
_INLINE int VSP_Reset (char *st)
  { struct { char c, *p1; } s;
    s.c = 9;
    s.p1 = st;    // status
    return VSP_Xeq((char *)&s); }
_INLINE int VSP_Passthru (char *in, int iLen, char *out, int oLen)
  { struct { char *p1; int p2; char *p3; int p4; } s;
    s.p1 = in;
    s.p2 = iLen;
    s.p3 = out;
    s.p4 = oLen;
    return _control(12, 12, (char *)&s); }

// ------------ RKL
_INLINE int get_rkl_krd_cert (char *filename)
    { return _rkl2(10, (int)filename);}

// ------------ Feature Enablement
_INLINE int enablementTokenCount (void)
    { return _info2i(79, 6); }
_INLINE int enablementTokenExpiration (void* tag, int* pUnit)
    { return _info4i(79, 7, (int) tag, (int) pUnit); }
_INLINE int enablementTokenStatus (void* tag)
    { return _info3i(79, 8, (int) tag); }
_INLINE int enablementTokenDemandStatus (void* tag)
    { return _info3i(79, 9, (int) tag); }
_INLINE int enablementTokenIndex (int index, void* tag)
    { return _info4i(79, 10, index, (int) tag); }
_INLINE int enablementTokenDetail (void* tag, char* name, char* value, int len)
    {
        struct { void* p1; char* p2; char* p3; int p4; } s;
        s.p1 = tag;
        s.p2 = name;
        s.p3 = value;
        s.p4 = len;
        return _info3i(79, 11, (int) &s);
    }


// ------------ Payware Mobile Reader Gen2.5 (agnostic) Bluetooth functions

_INLINE int set_bt_pins(int hdl, int pins) { return _control(hdl, 50, (char *)pins); }

_INLINE int get_bt_pins(int hdl) { return _sts2l(hdl, 51); }

_INLINE int bt_read_eeprom(int hdl, int address, char *buffer, int length) {
    int result;
    struct {int addr; char *buff; int len;} s;
    s.addr = address;
    s.buff = buffer;
    s.len = length;
    result = _control(hdl, 52, (char *)&s); // this suspends us
    if (result < 0) return result;
    return _status(hdl, 52, (char *)&s);
}

_INLINE int bt_write_eeprom(int hdl, int address, char *buffer, int length) {
    struct {int addr; char *buff; int len;} s;
    s.addr = address;
    s.buff = buffer;
    s.len = length;
    return _control(hdl, 53,  (char *)&s);
}

_INLINE int bt_dump_flash(int hdl) {
    return _control(hdl, 54,  (char *)0);
}

_INLINE int bt_disconnect(int hdl) {
    return _control(hdl, 55,  (char *)0);
}

_INLINE int bt_discard_pairing(int hdl) {
    return _control(hdl, 56,  (char *)0);
}

_INLINE int bt_get_version(int hdl, char *buf, int len)
    { return _sts4(hdl, 13,buf,len); }
_INLINE int bt_get_name(int hdl, char *buf, int len)
    { return _sts4(hdl, 14,buf,len); }
_INLINE int bt_get_pin(int hdl, char *buf, int len)
    { return _sts4(hdl, 15,buf,len); }
_INLINE int bt_get_ssp_en(int hdl)
    { return _sts4(hdl, 16,0,0); }
_INLINE int bt_get_ssp_pk_en(int hdl)
    { return _sts4(hdl, 17,0,0); }

// ------------ Payware Mobile Reader Gen3 functions

_INLINE int data_sync_en(int hdl, int mode)
    { return _control(hdl, 18,(char *)mode); }
_INLINE int iAP_power_share(int hdl, int mode)
    { return _control(hdl, 20,(char *)mode); }
_INLINE int iAP_power_status(int hdl)
    { return _control(hdl, 20,(char*)IAP2_STATUS); }

// ------------


#undef _INLINE
#undef _UNUSED
