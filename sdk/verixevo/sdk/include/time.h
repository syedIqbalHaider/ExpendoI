/***************************
* <time.h>                 *
***************************/

#ifndef _TIME
#define _TIME

#ifndef __TIME_DECLS
#define __TIME_DECLS
#undef __CLIBNS
#ifdef __cplusplus
namespace std {
#define __CLIBNS ::std::
extern "C" {
#else /* ndef __cplusplus */
#define __CLIBNS
#endif /* ndef __cplusplus */

#if defined(__cplusplus) || !defined(__STRICT_ANSI__)
 /* unconditional in C++ and non-strict C for consistency of debug info */
  typedef unsigned int size_t;
#elif !defined(__size_t)
  #define __size_t 1
  typedef unsigned int size_t;   /* see <stddef.h> */
#endif

//((70 * 365LU +17) * 86400 + seconds_offset_1970, [1Jan1900..1Jan1980)
#define seconds_offset_1900	2524521600LL
#define CLOCKS_PER_SEC	    1000
#define TZ_SYSVERSION	0x100 //sys.lib version that implements TimeZone 1.0	
typedef unsigned int clock_t;
typedef unsigned long time_t;
struct tm {
    int tm_sec;	    // 0-61
    int tm_min;	    //0-59
    int tm_hour;    //0-23
    int tm_mday;    //1-31
    int tm_mon;	    //0-11
    int tm_year;    //0-..years since 1900
    int tm_wday;    //0-6, days since Sunday
    int tm_yday;    //0-365, days since January 1
    int tm_isdst;   //DST flag
};
char *asctime(const struct tm *);
clock_t clock(void);
char *ctime(const time_t *);
double difftime(time_t, time_t);
struct tm *gmtime(const time_t *);
struct tm *localtime(const time_t *);
time_t mktime(struct tm *);
size_t strftime(char *, size_t, const char *, const struct tm *);
time_t time(time_t *);

#ifdef __cplusplus
}  /* extern "C" */
}  /* namespace std */
#endif
#endif /* __TIME_DECLS */
  #if defined(__cplusplus) && !defined(__TIME_NO_EXPORTS)
    using ::std::tm;
    using ::std::clock_t;
    using ::std::time_t;
    using ::std::asctime;
    using ::std::clock;
    using ::std::ctime;
    using ::std::difftime;
    using ::std::gmtime;
    using ::std::localtime;
    using ::std::mktime;
    using ::std::strftime;
    using ::std::time;
  #endif
#endif
