/******************************
* <limits.h> - Integer Limits *
******************************/
#ifndef __LIMITS
#define __LIMITS

#define CHAR_BIT        8               /* bits in a char */

#define SCHAR_MAX       127             /* max signed char */
#define SCHAR_MIN       (-128)          /* min signed char */
#define UCHAR_MAX       255             /* max unsigned char */
#define CHAR_MAX        UCHAR_MAX       /* max char */
#define CHAR_MIN        0               /* min char */
#define WCHAR_MAX        UCHAR_MAX       /* max char */
#define WCHAR_MIN        0               /* min char */

#define SHRT_MAX        32767           /* max short */
#define SHRT_MIN        (-32768)        /* min short */
#define USHRT_MAX       65535           /* max unsigned short */

#define LONG_MAX        2147483647L     /* max long */
#define LONG_MIN        (-LONG_MAX - 1) /* min long */
#define ULONG_MAX       4294967295UL    /* max unsigned long int */

#define INT_MAX         LONG_MAX        /* max int */
#define INT_MIN         LONG_MIN        /* min int */
#define UINT_MAX        ULONG_MAX       /* max unsigned int */

#define MB_LEN_MAX      1               /* bytes in multibyte character */

#if !defined(__STRICT_ANSI__) && !defined(_ANSI_STRICT)
#define LLONG_MIN  (~0x7fffffffffffffffLL)  /* min long long */
#define LLONG_MAX    0x7fffffffffffffffLL   /* max long long */
#define ULLONG_MAX   0xffffffffffffffffULL  /* max unsigned long long */
#endif

#endif
