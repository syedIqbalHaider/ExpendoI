/**********************************
* <stddef.h> - Common Definitions *
**********************************/
#ifndef __STDDEF
#define __STDDEF

#ifndef __STDDEF_DECLS
#define __STDDEF_DECLS
#undef __CLIBNS
#ifdef __cplusplus
namespace std {
#define __CLIBNS ::std::
extern "C" {
#else /* ndef __cplusplus */
#define __CLIBNS
#endif /* ndef __cplusplus */

typedef int ptrdiff_t;

#if defined(__cplusplus) || !defined(__STRICT_ANSI__)
 /* unconditional in C++ and non-strict C for consistency of debug info */
  typedef unsigned int size_t;
#elif !defined(__size_t)
  #define __size_t 1
  typedef unsigned int size_t;   /* others (e.g. <stdio.h>) also define */
   /* the unsigned integral type of the result of the sizeof operator. */
#endif

#ifndef __cplusplus  /* wchar_t is a builtin type for C++ */
  #if !defined(__STRICT_ANSI__)
  /* unconditional in non-strict C for consistency of debug info */
    typedef unsigned short wchar_t; /* also in <stdlib.h> and <inttypes.h> */
  #elif !defined(__wchar_t)
    #define __wchar_t 1
    typedef unsigned short wchar_t; /* also in <stdlib.h> and <inttypes.h> */
  #endif
#endif

#ifndef _WINT_T
#define _WINT_T
typedef long    wint_t;
#endif /* _WINT_T */

#ifndef _MBSTATE_T
#define _MBSTATE_T
typedef char    mbstate_t;
#endif /* _MBSTATE_T */

#undef NULL
#ifdef __cplusplus
# define NULL 0
#else
# define NULL (void *)0
#endif

#define offsetof(t,m)   ((__CLIBNS size_t)(((char*)&(((t*)0)->m))-(char*)0))

#ifdef __cplusplus
}  /* extern "C" */
}  /* namespace std */
#endif
#endif /* __STDDEF_DECLS */
  #if defined(__cplusplus) && !defined(__STDDEF_NO_EXPORTS)
    using ::std::ptrdiff_t;
    using ::std::size_t;
    using ::std::wint_t;
    using ::std::mbstate_t;
  #endif
#endif
