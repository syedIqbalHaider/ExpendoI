/***************************
* <stdio.h> - Input/Output *
***************************/
#ifndef __STDIO
#define __STDIO

#ifndef __STDIO_DECLS
#define __STDIO_DECLS

#undef __CLIBNS
#ifdef __cplusplus
namespace std {
#define _INLINE static inline
#define __CLIBNS ::std::
extern "C" {
#else /* ndef __cplusplus */
#define _INLINE static __inline
#define __CLIBNS
#endif /* ndef __cplusplus */

#if defined(__cplusplus) || !defined(__STRICT_ANSI__)
 /* unconditional in C++ and non-strict C for consistency of debug info */
  typedef unsigned int size_t;
#elif !defined(__size_t)
  #define __size_t 1
  typedef unsigned int size_t;   /* see <stddef.h> */
#endif

#undef NULL
#ifdef __cplusplus
# define NULL 0
#else
# define NULL (void *)0
#endif                   /* see <stddef.h> */

/* ANSI forbids va_list to be defined here */
/* _va_list must match va_list in <stdarg.h> */
#if __ARMCC_VERSION < 200000 || defined(__APCS_ADSABI)
typedef int *_va_list[1];
#else
typedef struct __va_list _va_list;
#endif

typedef long int fpos_t;

typedef struct __iobuf {
    int		    _fd;
    unsigned char   _cbuf;
    short           _count;
    short           _flags;
    short           _bufsiz;
    unsigned char   *_buf;
    unsigned char   *_ptr;
} FILE;

/* __iobuf flags */
#define _IOFBF          0x000
#define _IOREAD         0x001
#define _IOWRITE        0x002
#define _IONBF          0x004
#define _IOMYBUF        0x008
#define _IOEOF          0x010
#define _IOERR          0x020
#define _IOLBF          0x040
#define _IOREADING      0x080
#define _IOWRITING      0x100
#define _IOWAIT         0x400
#define _IOMYFILE       0x800
#define _IOTMPFILE      0x1000

extern FILE  __stdin, __stdout, __stderr;
#define stdin           (&__CLIBNS __stdin)
#define stdout          (&__CLIBNS __stdout)
#define stderr          (&__CLIBNS __stderr)

#define EOF             (-1)
#define BUFSIZ          128
#define FILENAME_MAX    (32+1)
#define SUBDIRNAME_MAX  (32+1)
//Including drive, colon, slashes and filename
#define PATHNAME_MAX    (256+1)
//FOPEN_MAX is defined here because ANSI C requires FOPEN_MAX, but, in VerixV,
//the max open files is limited by a config.sys variable. 
#define FOPEN_MAX	10
#define TMP_MAX         26
#define L_tmpnam        16

#define SEEK_SET        0
#define SEEK_CUR        1
#define SEEK_END        2

int remove ( const char * );
int rename ( const char *, const char * );
char *tmpnam( char *);
FILE *tmpfile( void );
int fclose( FILE * );
int fflush( FILE * );
FILE *fopen( const char *, const char * );
FILE *freopen( const char *, const char *, FILE * );
void setbuf( FILE *, char * );
int setvbuf( FILE *, char *, int, size_t );
int fprintf( FILE *, const char *, ... );
int fscanf( FILE *, const char *, ... );
int printf( const char *, ... );
int scanf( const char *, ... );
int sprintf( char *, const char *, ... );
int sscanf( const char *, const char *, ... );
int vfprintf( FILE *, const char *, _va_list );
int vprintf( const char *, _va_list );
int vsprintf( char *, const char *, _va_list );
int fgetc( FILE * );
char *fgets( char *, int n, FILE * );
int fputc( int, FILE * );
int fputs( const char *, FILE * );
int getc( FILE * );
#ifdef __cplusplus
    _INLINE int getchar() { return getc(stdin);}
#else
    int getchar( void );
    #define getchar()       getc(stdin)
#endif
int putc( int, FILE * );
#ifdef __cplusplus
    _INLINE int putchar(int c) { return putc(c,stdout);}
#else
    int putchar( int );
    #define putchar(c)      putc(c,stdout)
#endif
int puts ( const char * );
int ungetc( int, FILE * );
size_t fread( void *, size_t, size_t, FILE * );
size_t fwrite( const void *, size_t, size_t, FILE * );
int fgetpos( FILE *, fpos_t * );
int fseek( FILE *, long, int );
int fsetpos( FILE *, const fpos_t * );
long ftell( FILE * );
void rewind( FILE * );
#ifdef __cplusplus
    _INLINE int clearerr(FILE *p) { return ((p)->_flags &= ~(_IOERR|_IOEOF));}
#else
    void clearerr ( FILE * );
    #define clearerr(p)     ((p)->_flags &= ~(_IOERR|_IOEOF))
#endif
#ifdef __cplusplus
    _INLINE int feof(FILE *p) { return (((p)->_flags & _IOEOF) != 0);}
#else
    int feof( FILE * );
    #define feof(p)         (((p)->_flags & _IOEOF) != 0)
#endif
#ifdef __cplusplus
    _INLINE int ferror(FILE *p) { return (((p)->_flags & _IOERR) != 0);}
#else
    int ferror( FILE * );
    #define ferror(p)       (((p)->_flags & _IOERR) != 0)
#endif


#if !defined(__STRICT_ANSI__) && !defined(_ANSI_STRICT)
/* Verix Extensions */
#define fileno(stream)  ((stream)->_fd)
FILE *fdopen( int fildes, const char *type );
int debugio_write (char *buf, int size);
#endif

#if (__ARMCC_VERSION >= 220000 )
int snprintf( char *, size_t, const char *, ... );
int vsnprintf( char *, size_t, const char *, _va_list );
#endif

/* printf variants which don't support floating point */
int _printf( const char *, ... );
int _fprintf( FILE *, const char *, ... );
int _sprintf( char *, const char *, ... );
int _vprintf( const char *, _va_list );
int _vfprintf( FILE *, const char *, _va_list );
int _vsprintf( char *, const char *, _va_list );
#if (__ARMCC_VERSION >= 220000 )
int _snprintf( char *, size_t, const char *, ... );
int _vsnprintf( char *, size_t, const char *, _va_list );
#endif

#ifdef _PRINTF_NO_FLOAT
#define printf   _printf
#define fprintf  _fprintf
#define sprintf  _sprintf
#define vprintf  _vprintf
#define vfprintf _vfprintf
#define vsprintf _vsprintf
#if (__ARMCC_VERSION >= 220000 )
#define snprintf  _snprintf
#define vsnprintf _vsnprintf
#endif
#endif

#ifdef __cplusplus
}  /* extern "C" */
}  /* namespace std */
#endif

#endif /* __STDIO_DECLS */

  #if defined(__cplusplus) && !defined(__STDIO_NO_EXPORTS)
    using ::std::size_t;
    using ::std::fpos_t;
    using ::std::FILE;
    using ::std::remove;
    using ::std::rename;
    using ::std::tmpnam;
    using ::std::fclose;
    using ::std::fflush;
    using ::std::fopen;
    using ::std::freopen;
    using ::std::setbuf;
    using ::std::setvbuf;
    using ::std::fprintf;
    using ::std::_fprintf;
    using ::std::printf;
    using ::std::_printf;
    using ::std::sprintf;
    using ::std::_sprintf;
#if (__ARMCC_VERSION >= 220000 )
    using ::std::snprintf;
    using ::std::_snprintf;
    using ::std::vsnprintf;
    using ::std::_vsnprintf;
#endif
    using ::std::fscanf;
    using ::std::scanf;
    using ::std::sscanf;
    using ::std::vprintf;
    using ::std::_vprintf;
    using ::std::vfprintf;
    using ::std::_vfprintf;
    using ::std::vsprintf;
    using ::std::_vsprintf;
    using ::std::fgetc;
    using ::std::fgets;
    using ::std::fputc;
    using ::std::fputs;
    using ::std::getc;
    using ::std::getchar;
    using ::std::putc;
    using ::std::putchar;
    using ::std::puts;
    using ::std::ungetc;
    using ::std::fread;
    using ::std::fwrite;
    using ::std::fgetpos;
    using ::std::fseek;
    using ::std::fsetpos;
    using ::std::ftell;
    using ::std::rewind;
    using ::std::clearerr;
    using ::std::feof;
    using ::std::ferror;
    using ::std::__stdin;
    using ::std::__stdout;
    using ::std::__stderr;
    using ::std::tmpfile;
  #endif

#endif	// __STDIO
