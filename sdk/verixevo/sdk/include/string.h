/*******************************
* <string.h> - String Handling *
*******************************/
#ifndef __STRING
#define __STRING

#ifndef __STRING_DECLS
#define __STRING_DECLS
#undef __CLIBNS
#ifdef __cplusplus
namespace std {
#define __CLIBNS ::std::
extern "C" {
#else /* ndef __cplusplus */
#define __CLIBNS
#endif /* ndef __cplusplus */

#if defined(__cplusplus) || !defined(__STRICT_ANSI__)
 /* unconditional in C++ and non-strict C for consistency of debug info */
  typedef unsigned int size_t;
#elif !defined(__size_t)
  #define __size_t 1
  typedef unsigned int size_t;   /* see <stddef.h> */
#endif

#undef NULL
#ifdef __cplusplus
# define NULL 0
#else
# define NULL (void *)0
#endif                   /* see <stddef.h> */

void   *memcpy  ( void *, const void *, size_t );
void   *memmove ( void *, const void *, size_t );
int     memcmp  ( const void *, const void *, size_t );
void   *memchr  ( const void *, int, size_t );
void   *memset  ( void *, int, size_t );
#if !defined(__STRICT_ANSI__) && !defined(_ANSI_STRICT)
void   *memccpy ( void *, const void *, int, size_t);
#endif

char   *strcpy  ( char *, const char * );
char   *strncpy ( char *, const char *, size_t );
char   *strcat  ( char *, const char * );
char   *strncat ( char *, const char *, size_t );
int     strcmp  ( const char *, const char * );
int     strncmp ( const char *, const char *, size_t );
char   *strchr  ( const char *, int );
size_t  strcspn ( const char *, const char * );
char   *strpbrk ( const char *, const char * );
char   *strrchr ( const char *, int );
size_t  strspn  ( const char *, const char * );
char   *strstr  ( const char *, const char * );
char   *strtok  ( char *, const char * );
size_t  strlen  ( const char * );
char   *strerror( int );

#ifdef __cplusplus
}  /* extern "C" */
}  /* namespace std */
#endif
#endif /* __STRING_DECLS */
  #if defined(__cplusplus) && !defined(__STRING_NO_EXPORTS)
    using ::std::size_t;
    using ::std::memcpy;
    using ::std::memmove;
    using ::std::memcmp;
    using ::std::memchr;
    using ::std::memset;
#if !defined(__STRICT_ANSI__) && !defined(_ANSI_STRICT)
    using ::std::memccpy;
#endif
    using ::std::strcpy;
    using ::std::strncpy;
    using ::std::strcat;
    using ::std::strncat;
    using ::std::strcmp;
    using ::std::strncmp;
    using ::std::strchr;
    using ::std::strcspn;
    using ::std::strpbrk;
    using ::std::strrchr;
    using ::std::strspn;
    using ::std::strstr;
    using ::std::strtok;
    using ::std::strlen;
    using ::std::strerror;
  #endif
#endif
