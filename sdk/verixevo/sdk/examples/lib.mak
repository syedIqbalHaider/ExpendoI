# Example makefile for Verix/ARM position independent shared library that uses
# stdio functions

# This is written for Microsoft nmake, but modifications for other makes
# should be easy.  You would use this as, for example,
#   nmake -f lib.mak
# This is just an example -- the files referenced don't exist.

# Get standard tool definitions
!include "TOOLS.MAK"

# This is set up so you can turn the debug flag on and off from the make
# command line, for example "nmake -f lib.mak DEBUG=1".  You might prefer
# to define DEBUG here or just hard code the -g flags.
!ifdef DEBUG
!if $(DEBUG)
DBFLAG = -g
!endif
!endif

# clibPI.a for link with PI shared library
CLIBPI_A = $(VRXSDK)\lib\clibPI.a
# Compile and link options
CFLAGS = -v $(DBFLAG) -shlpic

AR = "$(RVCTDIR)\armar" --create

all : myPI.lib

OBJS = mysrc.o mysrc2.o

# build myPI.lib
# Link clibPI.a or clibPI11.a (for ARM11) so that this position independent
# libary myPI.lib can use stdio functions
myPI.lib: $(OBJS)
    $(AR) -o myPI.a $(OBJS) $(CLIBPI_A) 
    $(VRXLIB) myPI 
	
# Dummy target to clean up.
clean:
    del *.o
    del *.axf
    del *.map
    del *.bak
