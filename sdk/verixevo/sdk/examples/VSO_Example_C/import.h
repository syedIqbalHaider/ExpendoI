/*
 * This is the header file to be included to the application which uses these
 * functions exported by lib.so and imported by the application
 */
#define DllImport   __declspec(dllimport)

DllImport void setName (char* str);
DllImport void getName (char* str);
DllImport int getCount(void);
DllImport int increment (void);
DllImport int decrement(void);
DllImport int setCount (int i);
