
#include <svc.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "import.h"

int global1=100;

int main()
{
   int h=0;

    dbprintf("app: start\n");
    h=10; 
    dbprintf("local variable h=%d, global variable=%d\n",h,global1);
    h=getCount();
    dbprintf("getCount:%d\n",h);
    h = decrement();
    dbprintf("decremrnt:%d\n",h);
    dbprintf("app: end\n");
    return 0;
}
