/*
 * This is the header file to be included to the library source file which 
 * declares these functions. 
 * DllExport should also be specified at the function declaration in the
 * source code (a compiler warning will be generated if not), for example:
DllExport int getCount(void)
{
    count++;
    return count;
}
 */
#define DllExport   __declspec(dllexport)

DllExport void setName (char* str);
DllExport void getName (char* str);
DllExport int getCount(void);
DllExport int increment (void);
DllExport int decrement(void);
DllExport int setCount (int i);
