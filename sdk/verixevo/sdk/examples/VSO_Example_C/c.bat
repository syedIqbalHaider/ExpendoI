@set OLDPATH=%PATH%
@set VRXSDK=c:\vrxsdk
@set RVCTDIR=C:\Program Files\ARM\RVCT\Programs\4.0\650\multi1\win_32-pentium
@set PATH=%VRXSDK%\bin;%RVCTDIR%;C:\vrxsrc\bin

@del *.so
@del *.axf
@del *.vsl
@del *.vsa
@del *.s
@del *.o
@del *.map
@del *.p7s
@del *.crt

@rem ****************build lib***********************
vrxcc -v -vsolib lib.c -o lib.vsl
@if not exist lib.so goto END
@if not exist lib.vsl goto END

@rem ****************build app***********************
vrxcc -v -vsoapp app.c lib.so -o app.vsa
@if not exist app.so goto END
@if not exist app.vsa goto END

:END
