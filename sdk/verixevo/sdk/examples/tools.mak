#### Verix SDK Tool Definitions ####

# This file can be included at or near the top of an application makefile to
# define tool paths and required options.  The including file should define
# the desired compile and/or link options in CFLAGS and/or LFLAGS.  The VRXSDK
# and ARMHOME environment variables must be set.

SDKBIN = $(VRXSDK)\\BIN
VRXCC  = $(SDKBIN)\vrxcc
VRXHDR = $(SDKBIN)\vrxhdr
VLR    = $(SDKBIN)\vlr

.SUFFIXES: #erase default list
.SUFFIXES: .c .o .out

.c.o:
    $(VRXCC) -c $(CFLAGS) $<

.c.out: # build a simple one-file program
    $(VRXCC) $(CFLAGS) $(LFLAGS) $<
