# Example makefile for Verix/ARM applications

# This is written for Microsoft nmake, but modifications for other makes
# should be easy.  You would use this as, for example,
#   nmake -f app.mak
# This is just an example -- the files referenced don't exist.

# Get standard tool definitions
!include "TOOLS.MAK"

# This is set up so you can turn the debug flag on and off from the make
# command line, for example "nmake -f app.mak DEBUG=1".  You might prefer
# to define DEBUG here or just hard code the -g flags.
!ifdef DEBUG
!if $(DEBUG)
DBFLAG = -g
!endif
!endif

# Compile and link options
CFLAGS = -v $(DBFLAG) -DVERIX
LFLAGS = -v $(DBFLAG) -map

# It is conventional to define a dummy "all" target first.  It will be built
# if you do not specify a specific target on the make command line.
all : myapp.out

OBJS = myapp.o utils.o

# "myapp.c" is compiled using the default rule from TOOLS.MAK and the default
# options defined above.  We only need to define dependencies.  If there are
# no dependencies other than the source file itself no definition is needed.
myapp.o : utils.h

# "util.c" is compiled with non-default options.  The following rule overrides
# the defaults from TOOLS.MAK.
utils.o : utils.h
    $(VRXCC) -c $(CFLAGS) -Otime utils.c

# Link and generate .OUT file
myapp.out : $(OBJS)
    $(VRXCC) $(LFLAGS) $(OBJS)

# Dummy target to clean up.
clean:
    del *o
    del *.axf
    del *.map
    del *.bak
