//----------------------------------------
// "Duck" - test class in a shared object, exported in duck.h. Imported by
//          app.cpp  
//----------------------------------------

#include <string.h>
#include "duck.h"
#include <svc.h>

Duck::Duck()
{
    dbprintf("Duck::Duck()\n");
    count = 10;
    quackStr[0]=0;
    strcpy(quackStr,"QUACK");
}

Duck::Duck( int n)
{
    dbprintf("Duck::Duck(%d)\n",n);
    count = n;
    quackStr[0]=0;
    strcpy(quackStr,"QUACK");
}

int Duck::Quack(int n)
{
	dbprintf("Duck: Quack(%d)\n", n);

	if (n<=0)
	    n=count;
	if (n>25 || n<0)	//quackStr only holds 5 QUACK!
	    n = 1;
	while (n) {
		strcat(quackStr,"!");
		--n;
	}
	dbprintf("%s\n", quackStr);
	return 0;
}

int main(void)
{
    dbprintf("Duck main\n");
}
