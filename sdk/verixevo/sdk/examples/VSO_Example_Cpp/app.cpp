#include <svc.h>
#include "lib.h"

/*
 * the class "Aclass" is to test virtual function. Defined 
 */
class Aclass 
{
public:
	Aclass();
	virtual int Afun(int n);
	//int Afun(int n);
};

Aclass::Aclass()
{}
int Aclass::Afun(int n)
{
	dbprintf("Aclass: Afun(%d)\n", n);
	return 0;
}
class BabyDuck : public Duck {
public:
	BabyDuck();
	int Quack(int n);
	int TestProtected();
};

BabyDuck::BabyDuck()
{}

int BabyDuck::Quack(int n)
{
    dbprintf("BabyDuck::Quack(%d)\n", n);
    return 0;
}
int BabyDuck::TestProtected()
{
    dbprintf("BabyDuck::TestProtected() imported protected member quackStr=%s\n", quackStr);
    return 0;
}


//global
Aclass *pAclass;

int main(int argc, char **argv)
{
    Duck *pDuck;
    BabyDuck *pBabyDuck;

    dbprintf("APP: new Duck\n");
    pDuck = new Duck(20);
    dbprintf("APP: imported public member count=%d\n", pDuck->count);
    pDuck->Quack(-1);

    dbprintf("APP: new Aclass\n");
    pAclass = new Aclass;
    pAclass->Afun(100);
   
    dbprintf("APP: new BabyDuck\n");
    pBabyDuck = new BabyDuck;
    pBabyDuck->Quack(1);
    pBabyDuck->TestProtected();

    delete pDuck;
    delete pAclass;
    delete pBabyDuck;
}
