#ifndef _duck
#define _duck

#define DllExport   __declspec(dllexport)

/* to export a class, just place DllExport right after "class", no need to 
 * specify DllExport in the class implementation if using RVDS4.0 build 650.
 * But, RVDS4.0 build 436 requires DllExport to preceeds the member function 
 * declaration. It is thus highly recommended that everyone use RVDS4.0 build 650
 */
class DllExport Duck {
protected:
	char quackStr[32];
public:
	int count;
	Duck();
	Duck(int n);
	virtual int Quack(int n);
};

#if 0
/* to export a member */
/* DllExport can be put here or in the declaration in duck.cpp
   A warning is generated if DllExport is not in the actual declaration, but the
   code works:

"duck.cpp", line 10: Warning:  #654-D: declaration modifiers are incompatible with previous declaration
  Duck::Duck()
        ^
"duck.cpp", line 13: Warning:  #654-D: declaration modifiers are incompatible with previous declarat
ion
  int Duck::Quack(int n)
            ^
duck.cpp: 2 warnings, 0 errors

   The ARM compiler ref guide (4-24) states that "__declspec keyword must prefix
   the declarration specification", so that means DllExport must be used here.
   I tried to just use DllExport in duck.cpp, that also worked!
*/
class Duck {
protected:
	DllExport char quackStr[32];
public:
	DllExport int count;
	DllExport Duck();
	DllExport Duck(int n);
	DllExport virtual int Quack(int n);
	//DllExport int Quack(int n);
};
#endif

#endif
