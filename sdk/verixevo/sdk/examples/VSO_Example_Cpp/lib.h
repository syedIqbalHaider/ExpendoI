#define DllImport   __declspec(dllimport)

/* to import a class */
class DllImport Duck {
protected:
	char quackStr[32];
public:
	int count;
	Duck();
	Duck(int n);
	virtual int Quack(int n);
};

#if 0
/* to import specific members of a class */
class Duck {
protected:
	DllImport char quackStr[32];
public:
	DllImport int count;
	DllImport Duck();
	DllImport Duck(int n);
	DllImport virtual int Quack(int n);
};
#endif
