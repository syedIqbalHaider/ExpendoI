@goto Begin 
This is an example dos batch file of how to set environment variable RVCTDIR 
to build applications with app.mak and tools.mak
This batch file alters the PATH variable, restores it when done.
:Begin
@set OLDPATH=%PATH%
@rem set VRXSDKS to the Verix V SDK directory 
@set VRXSDK=c:\vrxsdk
@rem Set RVCTDIR to RVDS2.2
@set RVCTDIR=C:\Program Files\ARM\RVCT\Programs\2.2\349\win_32-pentium
@rem or, Set RVCTDIR to RVDS2.1
@rem set RVCTDIR=C:\Program Files\ARM\RVCT\Programs\2.0.1\277\win_32-pentium
@set PATH=%VRXSDK%\bin\;%RVCTDIR%
@rem use app.mak to buid application
nmake /f app.mak 
@rem or, use vrxcc directly here to build a simple application
@rem %VRXSDK%\bin\vrxcc app.c
@set PATH=%OLDPATH%
@set RVCTDIR=
