                        Verix V SDK Release Notes
                                Version 3.8.3
                              December 16, 2013

This is the Verix/ARM application tool kit.  Combined with the ARM
Developer Suite or RealView Compiler Tools it lets you build Verix
applications which will run on ARM-based platforms.  See the "howto"
section of the Verix V OS Tools manual for more details.

NOTE: 
1) RVDS4.0 build 902 and SDK version 3.5.0 are required to build Verix Shared Object. 
2) SDK version 2.5.0 or higher is required for RVDS4.0. Use vrxdb_RVD4.exe for 
   debugging with RVD4.0. SDK verson 2.5.0 or higher is required for building
   applications/libraries for VxOS11.
3) Voyager Library version is 3.12 (except voy.lib, see below), and they are
   built into differnet formats as below:
   - voy.lib: static library built with RVCT2.0.1, version 2.11
   - voyn.lib: static library built with RVCT4.0
   - voyns.lib: PI shared library built with RVCT4.0
   - voyns.o: stub of voyns.lib, built with RVCT4.0
   - voys.o: stub of voyns.lib, built with RVCT2.0.1
4) Use the vxuart.inf in the config directory to install the USB Client RS232 driver.
5) See config\crash.pdf for details of how to use findsrc.exe, appmap.exe and
   findlib.out. Crash.pdf will be removed from the SDK once these information is
   incorporated into VDN:23231. 
6) Now if built with RVDS4.0, a Verix V shared library, both absolute and
   position independent,can use stdio functions. An extra step is needed for a
   position independent Shared library to use stdio functions, that is to link
   with clibPI.a (compiled with -b) or clibPI11.a (compiled with -p). See the
   example makefile examples\lib.mak for more instructions. An absolute shared
   library doesn't need to link with clibPI.a or clibPI11.a for using stdio
   funcitons.

Warning: PI shared libraries built with RVDS4.0 and SDK2.5.1 or later must link
   with clibPI.a/clibPI11.a if using any stdio functions, including sprintf().
   Otherwise a system crash will happen when running the shared library.
Warning: Even though the STL portion of the C++ Standard Library implemented in
   this SDK is quite stable, the IOStreams and Locales are not complete. Please
   check with the ERS: VDN 28957 for details of what is not implemented or not 
   working. If you find some must-have functions not in this SDK, please
   contact the Verix V. OS group.

Changes since last SDK version 3.8.2
------------------------------------

1) svc_sec.h
 - Added APIs for $custom IC00709

2) verix11.lib, clibPI11.a, verixn.lib, clibPI.a, verix.so, vrxDbg.a
 - Changed check_heap() to work with the OS that supports both dynamic heap and
   classic heap, and it is backwards compatible with older OS versions. 
   You can use syslib version number to check:
	if (_SYS_VERSION>=0x305)			
	{
		//SDK versions with this new check_heap
		....
	}
	if (_syslib_version()>=0x305) )
	{
		//OS supports both dynamic&classic
	}

 - check_heap() was modified further to work with OS versions that has extra
   debugging capabilities for dynamic heap. The _syslib_version() is 0x306 for
   this change. By default, the extra dynamic heap debugging is off, setting
   bit 0x8 of the flag byte in the application's header turns on the extra
   debugging. This extra debugging keeps track of the allocated blocks(normally, 
   the heap manager only keeps track of the "free" blocks) and checks the 
   validity of these blocks when check_heap() is called. This extra debugging 
   helps developers finding memory corruptions but costs a lot extra CPU time,
   so make sure to turn this off before production. 

	the following code is not really usefull...
	if (_SYS_VERSION>=0x306)			
	{
		//SDK versions with this new check_heap
		....
	}
	if (_syslib_version()>=0x306) )
	{
		//OS supports extra dynamic heap debugging
	}

3) defalloc.h, pthread_alloc, ropeimpl.h, stl_alloc.h, stl_config.h, stl_locale.h
 - STL now generates a "Data Abort" in case of "out of memory". The abort
   address has the format: 0xA110C#xx, where # is a number (such as 1, or 2,..)
   and xx is the the current task ID in hex. 

4) svc.h svc_swi.h
 - Add a new return value MID_MC55IW for existing functions SVC_INFO_MOD_ID and
   SVC_INFO_DEV_TYPE for Cinterion MC55i-W radio, 
 - Added API get_usb_switch_bits() for testing USB Host-Device Switching Hardware
   on the Vx675 3G and Vx675 BT-WiFi terminals.

Known Issues in RVD4.0
-----------------------
1) The following warnings/errors are produced by RVD4.0, they are harmless, 
   and please ignore them:

From RVD4.0 Build 436:

Warning: 0x02190102: No access is provided to the register 'R9_fiq'. 
Warning: 0x02190102: No access is provided to the register 'R10_fiq'. 
Warning: 0x02190102: No access is provided to the register 'R11_fiq'. 
Warning: 0x02190102: No access is provided to the register 'R12_fiq'. 
Warning: 0x02190102: No access is provided to the register 'R13_fiq'. 
Warning: 0x02190102: No access is provided to the register 'R14_fiq'. 
Warning: 0x02190102: No access is provided to the register 'SPSR_svc'. 
Warning: 0x02190102: No access is provided to the register 'R13_svc'. 
Warning: 0x02190102: No access is provided to the register 'R14_svc'. 
Warning: 0x02190102: No access is provided to the register 'SPSR_abt'. 
Warning: 0x02190102: No access is provided to the register 'R13_abt'. 
Warning: 0x02190102: No access is provided to the register 'R14_abt'. 
Warning: 0x02190102: No access is provided to the register 'SPSR_irq'. 
Warning: 0x02190102: No access is provided to the register 'R13_irq'. 
Warning: 0x02190102: No access is provided to the register 'R14_irq'. 
Warning: 0x02190102: No access is provided to the register 'SPSR_und'. 
Warning: 0x02190102: No access is provided to the register 'R13_und'. 
Warning: 0x02190102: No access is provided to the register 'R14_und'. 
Warning: 0x02190102: No access is provided to the register 'R8_USR'. 
Warning: 0x02190102: No access is provided to the register 'R9_USR'. 
Warning: 0x02190102: No access is provided to the register 'R10_USR'. 
Warning: 0x02190102: No access is provided to the register 'R11_USR'. 

From RVD4.0 Build 650 or higher:

Warning: Possible endianness configuration mismatch. The target is configured as big endian. RVD can not determine if that
is correct.
Warning: Failed to configure Target Abstraction: 0x021a0101: One or more of the requested capabilities could not be set:
vector-catch-svc.
Warning: Insufficient hardware resources to enable requested vector catch events. Some vector catch events have been
disabled.
Warning: Unknown core type ARM7TDMI I1 t.out       ; target will be treated as an ARM7TDMI.
Warning: 26 registers defined for this core are not available from the target.

Error: 0x000d0101: CarvValue type mismatch


2) Same as before that each VerixV task is a "target" of RVD. In the 
   "Target->Connect To Target" window, and the "Home Page", our targets
   are preceded with "ARM7TDMI", this is required by RVD4.0 but dosn't mean 
   that our processor is ARM7TDMI. 

Known Issues in RVD1.7
------------------------
1) High level stepping over (F10) an OS call in Src window (C source level) 
   sometimes results in the application running (ie. debugger behaves as if 
   Execute/F5 vs Step/F10 was pressed). Typically, this problem occurs when 
   there is also a breakpoint on the OS call. For example, the following 
   sequence can be successfully stepped through. But if a breakpoint is set 
   on the get_env() line, and the program is run to that line (F5), and then 
   step is requested (F10), the app will run instead of stepping. But, the 
   same sequence works if stepping in Dsm window (Assembler lever) instead 
   of Src window.
           ....

           memset (buff, 0, sizeof(buff));
           errno = 0;
           rc = get_env("var",buff, sizeof(buff)-1);
           val = atoi(buff);

2) High level stepping into (F11) a function through a function pointer does
   not step into the function (it goes to the next line in the same source 
   file instead). But, if the call is done directly (not through a function 
   pointer), the high level stepping into works. A workaround for this is to
   do a low level stepping into. 

3) Sometimes short (16 bit) variables are displayed/watched in RVD as 0 when 
   the code is compiled with the big-endian option. 

4) Enhancements:
   a) RVD does not remember breakpoints and "symbols only" between debugging
      sessions. So, every time when a new session is started the "symbols only"
      button needs to be checked when the image is loaded and the breakpoints
      need to be set again.
   b) RVD does not display a list of functions for source files. 

Resolved Issues in RVD1.7
-------------------------

1) Pop-up values (mouse pointer over variable name) are displayed 
   in little endian (ie. wrong byte order).

2) Displaying/Watching a string (char[]) field in a structure often
   results in an invalid memory error.

3) Array display/watch : data is often offset by 1 element.

4) Stepping over some function calls sometimes results in the application 
   running (ie. debugger behaves as if Execute/F5 vs Step/F10 was pressed).
   This issue is partially resolved. With RVD1.7, the problem only exists
   if the stepped function is an OS call. See the above Known Issues in RVD1.7 1)
   for details. With RVD1.6.1, this problem happen on both OS functions 
   as well as some other functions that calls inline functions.
   For example, if a breakpoint is set on otherFunc() line, and the program 
   is run to that line (F5), and then step is requested (F10), the app will 
   run instead of stepping.
         
	    .....

	static int otherFunc (char* s, char* b, int i);
	static __inline int otherFunc2 (char* s, char* b, int i, int x)
	    { return ++i; }
	static __inline int otherFunc (char* s, char* b, int i)
	    { return otherFunc2(s, b, i, 0); }
 
	    .....

        memset(buff, 0, sizeof(buff));
	errno = 0;
	rc = otherFunc ("maintid", buff, sizeof(buff));
	rc = atoi(buff);


