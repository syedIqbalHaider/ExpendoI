//----------------------------------------------------------------------------
//
// CTLSInterface.h - header file for the Vx EOS CTLS Application Interface
//                   library.
//
//----------------------------------------------------------------------------
	/******************************************************************
	   Copyright (C) 2009 by VeriFone Inc. All rights reserved.

	 No part of this software may be used, stored, compiled, reproduced,
	 modified, transcribed, translated, transmitted, or transferred, in
	 any form or by any means  whether electronic, mechanical,  magnetic,
	 optical, or otherwise, without the express prior written permission
							  of VeriFone, Inc.
	*******************************************************************/

#ifndef _CTLSI
#define _CTLSI

#define CTLSINTERFACE_VERSION		"CTLSINTF01.00.00.72"
#define CTLSINTERFACE_VERSION_LEN	19

#ifndef BOOLEAN
#define BOOLEAN int
#endif
#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif

// Logging filter
#define CTLS_INTERFACE_FILTER	(1<<27) 
#define CTLS_UI_FILTER			(1<<0) 
#define CTLS_SRV_PIPE_FILTER	(1<<1)		// only used by VxCTLS app

//---------------------------------------
// Return Values and other constants
//---------------------------------------
#define CTLS_OK				0

#define OPSYS_ERROR			-1		// check errno for further details

#define	E_CTLS_APP_ERROR		-101
#define E_CTLS_UNKNOWN_ERROR	-102	// Client application and CTLS app are out of sync or some other bad thing....
#define E_CTLS_APP_NO_RESPONSE	-103
#define E_IN_USE				-104
#define E_NOT_AVAIL				-105
#define E_INVAL					-106	// Invalid parameter (most likely size for send/receive APIs)
#define E_CTLS_APP_RESPONSE		-107	// Payload size for CTLS app response is incorrect or CTLS app didn't send it...
#define E_CTLS_UNKNOWN_CLIENT	-108	// Task sending request is not a known client (ie. not using ctls interface lib)
#define E_CTLS_UNKNOWN_REQUEST	-109	// request message has undefined request code.
#define E_CTLS_UI_NOT_INIT		-110	// ui framework thread initialization is in progress (or failed).  Try again...
#define E_CTLS_BAD_ARG			-111

// Default timeout for InitializeInterface
#define CTLS_DEFAULT_INIT_TIMEOUT	500

// Maximum size for CTLSSend()/Receive
#define CTLS_MAX_MESSAGE_SIZE		3072
#define CTLS_MAX_UI_MESSAGE_SIZE	256

// Maximum file name size (for BMP files)
#define CTLS_MAX_FILE_NAME		32

// For UI re-assignment, the maximum size (not including NULL terminator) for a UI logical name
// NOTE: The UI name is for UI re-assignment.  This is planned as a future enhancement to the library
#define CTLS_UI_MAX_NAME_LENGTH		15	

// UI Thread Stack
#define CTLS_UI_THREAD_STACK_CFG	"CTLSUIStack"		// Config.sys variable for configuring thread stack
#define CTLS_UI_THREAD_STACK_SIZE	3000				// Default stack size
//---------------------------------------
// Constants for UI Handlers
//---------------------------------------
#define CTLS_MAX_LED			8	// Note that VxCTLS interface supports 8 but user interface display only supports 4
#define CTLS_LED_OFF			0
#define CTLS_LED_ON				1
#define CTLS_LED_FLASH			2
#define CTLS_LED_FLASH_TIME		500	  // 500 msec
#define CTLS_OFF_LED_BMP		"N:/OFFLED.BMP"
#define CTLS_GREEN_LED_BMP		"N:/GREENLED.BMP"
#define CTLS_RED_LED_BMP		"N:/REDLED.BMP"
#define CTLS_BLUE_LED_BMP		"N:/BLUELED.BMP"
#define CTLS_YELLOW_LED_BMP		"N:/YELLOWLED.BMP"
#define CTLS_RAINBOW_STYLE_LED	1		// used with the UI Framework set param function for red/yellow/green/blue LED
#define CTLS_GREEN_STYLE_LED	0		// used with the UI Framework set param function for green led config

// format/alingment constants used by the LCD Text Handler
#define VFI_SET_LEFT            0
#define VFI_SET_RIGHT           0x10
#define VFI_SET_CENTER          0x20
#define VFI_SET_HPIXEL          0x40    /* Use specific column */ 
#define VFI_SET_TOP             0
#define VFI_SET_BOTTOM          0x01
#define VFI_SET_VCENTER         0x02
#define VFI_SET_SPREAD          0x03
#define VFI_SET_VPIXEL          0x08    /* Use specific row */
// font ids used by the LCD Text Hanlder
#define FONT_10X7               1		/* normal, default font */
#define FONT_13X7               8		/* for digits, very bold */
#define FONT_CHINESE_16x16      9
#define CHINESE_DEFAULT_FONT_VER_SIZE  16
#define FONT_USER				19
// UI Parameter id's
#define UI_LANGUAGE_PARAM			1
#define UI_FONT_PARAM				2
#define UI_LED_STYLE_PARAM			3
#define UI_CARD_ISSUER_LOGO_PARAM	4

// UI Styles - For EMV Style, Card Issuer BMPs should be displayed.
#define UI_EMV_STYLE		1
#define UI_PASSTHRU_STYLE	2

// UI Constant id's (for use with CTLSClientGetUIConstants)
#define CTLS_CONST_TEXT_LINES			1
#define CTLS_CONST_SOFT_LED_POSITIONS	2
#define CTLS_CONST_LOGO_POSITION		3
#define CTLS_CONST_ANTENNA_CENTER		4
#define CTLS_CONST_TAP_BMP_NAME			5
#define CTLS_CONST_TAP_BMP_DIMENSIONS	6

// UI Buzzer 
#define UI_BUZZER_FREQUENCY_OFF			-1	// frequency code -1 means turn the buzzer off

// UI Enabled bit settings
#define CTLS_APP_BIT	(1<<2) 
#define CTLS_OWNER_BIT	(1<<1) 

// Console Styles
#define VX680_BACKWARDS_COMPATIBILITY_STYLE	1		// also applies to 820, 825, etc.
#define VX680_STYLE							2		// also applies to 820, 825, etc.
#define VX520_STYLE							3		
#define VX600_STYLE							4
#define VX675_STYLE							5
#define VX805_STYLE							6
//---------------------------------------
// CTLSSysInfo
// This structure contains various
// information about the system resources
// used by VxCTLS.
//---------------------------------------
typedef struct {
	int vxCTLSId;	// task id for VxCTLS
	int uiThread;	// client UI thread id
	char reserved[24];
} CTLSSysInfo;

//---------------------------------------
// User Interface Handlers -
// These definitions are used for 
// applications that want to override
// the default user inteface handlers.
//---------------------------------------

// CTLSUIParms defines parameters related starting the UI
typedef struct {
	int uiMode;			// EMV or pass through.
} CTLSUIParms;

typedef struct {
	int nLED;
	unsigned char led[CTLS_MAX_LED];
} CTLSLEDStatus;

typedef struct {
   int frequency;		
   int pause;
   int duration;
   int nTimes;
} CTLSBuzzerRequest;

typedef struct {
   char stringToDisplay[64];	
   int backgroundColor;
   int foregroundColor;
   int fontId;
   int format;		// alignment
   int column;
   int row;
} CTLSLCDText;

typedef void (*CTLSLEDFunc) (CTLSLEDStatus* leds);
typedef void (*CTLSStartUIFunc) (CTLSUIParms* pParms);
typedef void (*CTLSLCDTextFunc) (CTLSLCDText* pText);
typedef void (*CTLSLCDClearFunc) (void* tbd);
typedef void (*CTLSBuzzerFunc) (CTLSBuzzerRequest* pParms);
typedef void (*CTLSStopUIFunc) (void* tbd);
typedef void (*CTLSUIParamFunc) (int paramId, void* paramVal, int paramLength);
typedef void (*CTLSDimBacklight) (int onOff);
typedef void (*CTLSKeyBacklight) (int onOff);

#define CTLS_UI_BACKLIGHT		11
#define CTLS_UI_KEYLIGHT		12
#define MAX_HANDLERS			2

typedef struct {
	CTLSStartUIFunc startUIHandler; 
	CTLSStopUIFunc stopUIHandler;
	CTLSUIParamFunc uiParamHandler;
	CTLSLEDFunc ledHandler;
	CTLSLCDTextFunc textHandler;
	CTLSLCDClearFunc clearDisplayHandler;
	CTLSBuzzerFunc buzzerHandler;
	void* AdditionalHandlers;
} CTLSUIFuncs;

typedef struct {
	unsigned int	tag;
	void*			handler;
} CTLSHandler;

typedef struct {
	int			num_handlers;
	CTLSHandler	handlers[MAX_HANDLERS];
} CTLSUIHandlers;

// "generic" UI Event handler - this is for use with the RegisterEvent function
typedef void (*CTLSUIEventHandler) (void* p);

//---------------------------------------
// CTLS Device Control
//---------------------------------------
#define CTLS_DEVICE_CONFIG	1
typedef struct {
	int baud;		// integer value of baud rate
	int nBits;		// number of bits per byte;
	int rsrvd1;		// for stop bits, parity, etc.
	int rsrvd2;	
} CTLSSerialDeviceCfg;
typedef union {
	CTLSSerialDeviceCfg serialCfg;		
} CTLSDeviceControl;
// prototype for function to receive device control notifications
typedef void (*CTLSDeviceControlHandler) (int ctlOpt, CTLSDeviceControl* pParms);


//---------------------------------------
// Function Prototypes
//---------------------------------------
#ifdef __cplusplus
extern "C"
{
#endif

//--------------------------------------------------
// Initialize Library Interface - Initializing
// the interface requires establishing communication
// with the VxCTLS app.  Depending upon the terminal
// configuration, system startup times can vary and
// the VxCTLS app may not be immediately ready to 
// check for client apps.  
// In CTLSInitInterface(), if ticks > 0, it is the 
// maximum time the app is willing to wait to 
// initialize the interface. If ticks is <= 0,
// the library will use a default maximum timeout
// (CTLS_DEFAULT_INIT_TIMEOUT)
//---------------------------------------------
int CTLSInitInterface(int ticks);	

// check if user interface framework is initialized
int CTLSClientIsUIInit(void);

// CTLSInitUIInterface - just initialise the UI interface.
// No command interface/pipe is initialized.
// The timeout ticks is the same as CTLSInitInterface.
// The logical name can later be used by CTLSAssignUIInterface().
// This API is automatically called as part of CTLSInitInterface()
int CTLSInitUIInterface(char* logicalName, int tickTimeout);

// CTLSAssignUIInterface - re-assign the user interface for this
// task's command interface.
int CTLSAssignUIInterface(char* logicalName);


//---------------------------------------------
// Device owner/management APIs
//---------------------------------------------
// establish ownership of the reader device
int CTLSOpen(void);

// close the device and free it for other applications
int CTLSClose(void);

// get the CTLS owner task id
int CTLSGetOwner(void);

// transfer ownership of the reader
int CTLSSetOwner (int newOwnerId);

// Retrieves task/thread and other info that can be used to retrieve additional OS info
void CTLSGetInfo(CTLSSysInfo* pSysInfo);

//---------------------------------------------
// Reader Communications - buff data should
// conform to reader protocol such as Vivopay
//---------------------------------------------
// read a message from the reader
int CTLSReceive (char* buff, int maxLength);

// send a message to the reader
int CTLSSend (char* buff, int length);


//---------------------------------------------
// Device Control Notification 
//---------------------------------------------
void CTLSClientSetDeviceControlNotification(CTLSDeviceControlHandler pFunc);


//---------------------------------------------
// User Interface Handling 
//---------------------------------------------
// retrieve the current callback function pointers
int CTLSGetUI(CTLSUIFuncs* uiFuncs);

// update the callback function pointers
int CTLSSetUI(CTLSUIFuncs* uiFuncs);

// set the console style.  The console style normally
// defaults to the correct type based on the terminal
// model number.  Use this API to select backwards
// compatibility mode style or an alternative terminal
// style
int CTLSSetConsoleStyle (int styleId);

//---------------------------------------------
// UI Card Issuer Logo BMP  
//---------------------------------------------
int CTLSClientUIGetCardLogoBMP(char* buff, int len);
int CTLSClientUISetCardLogoBMP(char* fileName);

//-------------------------------------------
// Default User Interface Handlers - these
// should not normally be directly called
// by the application.   
// These APIs are made public for applications 
// that need to override default UI Handling
// (ie. call CTLSSetUI())
//------------------------------------------
// ClientUIInit() is called by the UI Thread at start up time. This
// just gives the UI handlers an opportunity to initialize any 
// control variables.
void CTLSClientUIInit(void);

// Default for startUIHandler
void CTLSClientStartUI(CTLSUIParms* pParms);

// Default for stopUIHandler
void CTLSClientStopUI (void* tbd);

// Default for setting UI parameters
void CTLSClientUIParamHandler(int paramId, void* paramVal, int paramLength);

// Default for managing LEDs
void CTLSClientLEDHandler (CTLSLEDStatus* leds);

// Default for managing text display
void CTLSClientLCDTextHandler (CTLSLCDText* pText);

// Default for clearing display (CTLS message area)
void CTLSClientLCDClearHandler (void* tbd);

// Default for managing buzzer requests
void CTLSClientBuzzerHandler (CTLSBuzzerRequest* pParms);

// Default handler for backlight requests
void CTLSBackLightHandler(int onOff);

// Default handler for Keypad backlight requests
void CTLSKeyBackLightHandler(int onOff);

// APIs for manually enabling/disabling user interface
void CTLSClientUIEnable(int onOff);
int	CTLSClientIsUIEnabled(void);

// UI Framework Thread - Event Dispatching
// Event registration requests the UI Framework thread execute 
// a particular function when the OS returns an event.
// Note that only 1 function can be associated with any event,
// and caller should ensure that the param address is valid
// when the event occurs.
int CTLSClientUIRegisterEvent (int ev, CTLSUIEventHandler fptr, void* param);
void CTLSClientUIClearEvent (int ev);

//-------------------------------------------
// UI Framework helper functions. 
// These functions should not normally be 
// directly called // by the application.   
// These APIs are made public for applications 
// that need to override default UI Handling
// (ie. call CTLSSetUI())
//------------------------------------------
// configure bmps to use for soft LEDs
int CTLSClientSoftLEDConfig(int led, char* bmp);

// enable/disable the LEDs
void CTLSClientSoftLEDEnable(int onOff);
void CTLSClientLEDEnable(int on);
void CTLSClientHWLEDEnable(int on);

// set (turn on/off/flash) the soft LEDs
void CTLSClientSoftLEDSet(int l1, int l2, int l3, int l4);
void CTLSClientLEDSet(int l1, int l2, int l3, int l4);
void CTLSClientHWLEDSet(int l1, int l2, int l3, int l4);

// retrieve constants used for display positioning
int CTLSClientGetUIConstants(int paramId, void* buff, int max);
//Spesial function to display chinese messages
void CTLSClientUIWrite_at_Chinese(char *str, int x, int y,BOOLEAN IsPixel);
// retrive CTLSInterface version
int CTLSClientGetVersion(char* cVersion);

#ifdef __cplusplus
}
#endif


#endif
