REM *** To create the configuration data files ***

echo *** Generating data file MVT.txt


REM *************** For AllCVMs *********************

pushd .\AllCVMs

if exist MVT.dat del MVT.dat 
  ..\gendata -A MVT.txt


if exist EST.dat del EST.dat 
  ..\gendata -A EST.txt

popd

REM *************** For NoCVMOnly  *********************

pushd .\NoCVMOnly

if exist MVT.dat del MVT.dat 
  ..\gendata -A MVT.txt


if exist EST.dat del EST.dat 
  ..\gendata -A EST.txt

popd

REM *************** For NoNCVM  *********************

pushd .\NoNCVM

if exist MVT.dat del MVT.dat 
  ..\gendata -A MVT.txt


if exist EST.dat del EST.dat 
  ..\gendata -A EST.txt

popd

REM *************** For NoOnlinePIN *********************

pushd .\NoOnlinePIN

if exist MVT.dat del MVT.dat 
  ..\gendata -A MVT.txt


if exist EST.dat del EST.dat 
  ..\gendata -A EST.txt

popd

REM *************** For NoOnPINoNCVM *********************

pushd .\NoOnPINoNCVM

if exist MVT.dat del MVT.dat 
  ..\gendata -A MVT.txt


if exist EST.dat del EST.dat 
  ..\gendata -A EST.txt

popd

REM *************** For OfflineOnlin *********************

pushd .\OfflineOnlin

if exist MVT.dat del MVT.dat 
  ..\gendata -A MVT.txt


if exist EST.dat del EST.dat 
  ..\gendata -A EST.txt

popd

REM *************** For OfflineOnly *********************

pushd .\OfflineOnly

if exist MVT.dat del MVT.dat 
  ..\gendata -A MVT.txt


if exist EST.dat del EST.dat 
  ..\gendata -A EST.txt

popd

REM *************** For OffOnNoOnlPI *********************

pushd .\OffOnNoOnlPI

if exist MVT.dat del MVT.dat 
  ..\gendata -A MVT.txt


if exist EST.dat del EST.dat 
  ..\gendata -A EST.txt

popd

REM *************** For SigOnly *********************

pushd .\SigOnly

if exist MVT.dat del MVT.dat 
  ..\gendata -A MVT.txt


if exist EST.dat del EST.dat 
  ..\gendata -A EST.txt

popd
