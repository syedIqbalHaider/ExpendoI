/*
 * FILENAME       : mvtfuncs.h
 * PRODUCT        : Verix/Vx.
 * VERSION        : 4.0.0
 * AUTHOR(s)      : Pavan_k1
 * CREATED        : 11/Dec/06
 *
 * DESCRIPTION    : 
 *
 * CONTENTS   :
 * MODIFICATION HISTORY    :
 *
 * #    Date        Who         History
 * ---  --------    ----------  ----------------------------------------
 * 1    11 Dec 06   Pavan_k1    Created for Vx EMV Module 4.0.0
 * 2    06 Jul 09	Mary_F1		BUFSIZE
 *								Buffer size is checked for memcpy() and strcpy() 
 *
 * Copyright (C) 2006 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language, in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual or otherwise, without the prior written
 * permission of VeriFone Inc.
 *                                                2099 Gateway Place
 *                                                  Suite 600
 *                                                  San Jose  CA  95110 
 *                                                  USA
 *
 *
 *
 */
#ifndef VSMVTFUNCS_H
#define VSMVTFUNCS_H

#include "VXEMVAP_define.h"
#include "emvsizes.h"

#define EMV_TABLE_NOT_USED   -1
#define NO_ISSUER_SPECIFIED  -1
#define NO_SCHEME_SPECIFIED  -1

#define MATCH_NOT_NEEDED      0
#define SCHEME_MUST_MATCH     1
#define ISSUER_MUST_MATCH     2
#define BOTH_MUST_MATCH       3

//BUFSIZE	strcpy()
//Buffer size check cannot be done as second parameter in each strcpy() is a pointer
#define szGetMVTEMVTACOnline(a)   {strcpy(a, szGetEMVTACOnline());}
#define szGetMVTDefaultDDOL(a)    {strcpy(a, szGetDefaultDDOL());}
#define szGetMVTEMVTACDefault(a)  {strcpy(a, szGetEMVTACDefault());}
#define szGetMVTEMVTACDenial(a)   {strcpy(a, szGetEMVTACDenial());}
#define vdGetMVTDefaultTDOL(a)    {strcpy(a, szGetDefaultTDOL());}
#define szGetEMVMerchantCatCode() szGetEMVMerchantCategoryCode()
#define vdGetMVTDefaultDDOL(a)    {strcpy(a, szGetDefaultDDOL());}
#define inGetMVTTRMDataPresent()  inGetTRMDataPresent()

    int     inGetMVTRequiredDefDDOL(char *szDefaultDDOL, int inSpecifiedIssuer, 
                                       int inSpecifiedScheme, int inEMVTabRecord);
    int     inGetMVTRequiredDefTDOL(char *szDefaultTDOL, int inSpecifiedIssuer, 
                                       int inSpecifiedScheme, int inEMVTabRecord);
    int     inGetMVTRequiredROLSParams(long *inpFloorLimit, long *inpRSThreshold, int *inpTargetRSPercent, 
                int *inpMaxTargetRSPercent, int inSpecifiedIssuer, int inSpecifiedScheme, 
                        int inEMVTabRecord, int *inpDataFound);
    int     vdGetMVTRequiredTermActionCodes(char* szTACDefault, char* szTACDenial, char* szTACOnline, 
                        int inSpecifiedIssuer, int inSpecifiedScheme, int inEMVTabRecord);
    void    vdGetMVTTermActionCodes(char* szTACDefault, char* szTACDenial, char* szTACOnline);
    void    vdGetMVTRandOnlineParams(long *inpFloorLimit, long *inpRSThreshold, 
                                     int *inpTargetRSPercent, int *inpMaxTargetRSPercent);
    int     inIncEMVCounter (void);


    int     inLoadMVTRec (int inRecNumber);  
    void    *pvdGetMVTRec(void);
    int     inSaveMVTRec (int inRecNumber);
    int     inGetMVTRecNumber (void);
    int     inUpdateMVTRec (void);
    void    vdResetMVTRec (void);
    short   inGetSchemeReference(void);
    void    vdSetSchemeReference(short inSchemeReference);
    short   inGetIssuerReference(void);
    void    vdSetIssuerReference(short inIssuerReference);
    void    vdSetIssuerReference(short inIssuerReference);
    short   inGetTRMDataPresent(void);
    void    vdSetTRMDataPresent(short inTRMDataPresent);
    long    lnGetEMVFloorLimit(void);
    void    vdSetEMVFloorLimit(long lnEMVFloorLimit);
    long    lnGetEMVRSThreshold(void);
    void    vdSetEMVRSThreshold(long lnEMVRSThreshold);
    short   inGetEMVTargetRSPercent(void);
    void    vdSetEMVTargetRSPercent(short inEMVTargetRSPercent);
    short   inGetEMVMaxTargetRSPercent(void);
    void    vdSetEMVMaxTargetRSPercent(short inEMVMaxTargetRSPercent);
    char*   szGetEMVTACDefault(void);
    void    vdSetEMVTACDefault(char* szEMVTACDefault);
    char*   szGetEMVTACDenial(void);
    void    vdSetEMVTACDenial(char* szEMVTACDenial);
    char*   szGetEMVTACOnline(void);
    void    vdSetEMVTACOnline(char* szEMVTACOnline);
    char*   szGetDefaultTDOL(void);
    void    vdSetDefaultTDOL(char* szDefaultTDOL);
    char*   szGetDefaultDDOL(void);
    void    vdSetDefaultDDOL(char* szDefaultDDOL);
    short   inGetNextRecord(void);
    void    vdSetNextRecord(short inNextRecord);
    unsigned long ulGetEMVCounter(void);
    void    vdSetEMVCounter(unsigned long ulEMVCounter);
    char*   szGetEMVTermCountryCode(void);
    void    vdSetEMVTermCountryCode(char* szEMVTermCountryCode);
    char*   szGetEMVTermCurrencyCode(void);
    void    vdSetEMVTermCurrencyCode(char* szEMVTermCurrencyCode);
    char*   szGetEMVTermCapabilities(void);
    void    vdSetEMVTermCapabilities(char* szEMVTermCapabilities);
    char*   szGetEMVTermAddCapabilities(void);
    void    vdSetEMVTermAddCapabilities(char* szEMVTermAddCapabilities);
    char*   szGetEMVTermType(void);
    void    vdSetEMVTermType(char* szEMVTermType);
    char*   szGetEMVMerchantCategoryCode(void);
    void    vdSetEMVMerchantCategoryCode(char* szEMVMerchantCategoryCode);

    /* MVT structure is modified to accomadate two new fields for set online functionality and 
    check for blacklisted card. Corresponding functions are added for extended fields. 
    This is done for implementing APR ID. 03 & 05.
    */
    short   inGetMerchantForcedOnlineFlag(void);
    void    vdSetMerchantForcedOnlineFlag(short inForceOnline);
    short   inGetBlackListedCardSupportFlag(void);
    void    vdSetBlackListedCardSupportFlag(short inBlackListedCardSupport);

    short   inGetFallbackAllowedFlag(void);
    void    vdSetFallbackAllowedFlag(short inFallbackAllowed);
    short   inGetAutoSelectApplnFlag(void);
    void    vdSetAutoSelectApplnFlag(short inAutoSelectAppl);
    short   inGetTermCurExp(void);
    void    vdSetTermCurExp(short inEMVTermCurExp);
    char*   szGetEMVTerminalCategoryCode(void);
    void    vdSetEMVTerminalCategoryCode(char* szEMVTerminalCategoryCode);
    short   inGetShortRFU1(void);
    void    vdSetShortRFU1(short inRFU1);
    short   inGetShortRFU2(void);
    void    vdSetShortRFU2(short inRFU2);
    short   inGetShortRFU3(void);
    void    vdSetShortRFU3(short inRFU3);
    char*   szGetStringRFU1(void);
    void    vdSetStringRFU1(char* szRFU1);
    char*   szGetStringRFU2(void);
    void    vdSetStringRFU2(char* szRFU2);
    char*   szGetStringRFU3(void);
    void    vdSetStringRFU3(char* szRFU3);

    short   inGetModifyCandListFlag(void);
    void    inSetModifyCandListFlag(short inModifyCandListFlag);
    void    vdLoadData(MVT_REC * stMVTTempRec);

#endif
