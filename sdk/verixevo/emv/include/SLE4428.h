/*-----------------------------------------------------------------------------
 *
 *      Filename:       SLE4428.h
 *
 *      Product:        Card Slot Library
 *      Version:        1.6     
 *      Author :        Pavan_K1
 *  	Module :	
 *      Defines all macros needed for SLE4428 synchronous card.
 *
 * MODIFICATION HISTORY    :
 *
 * #      Date		    Who			History
 * ---  ---------    ---------		----------------------------------------
 * 1.   23 Oct 06    Pavan_k1		Created.
 *
 * Copyright (C) 2005 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language, in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual or otherwise, without the prior written
 * permission of VeriFone Inc.
 *                                                
 
	2099 Gateway Place
	Suite 600
	San Jose CA 95110 
	USA	 

 */

/***************************************************************/




// Macros for SLE4428 sync card. 

#define SLE4428											(DWORD)0x0004

// SLE4428 SYNC CARD
// Descriptor - "SLE4428   "  ( there are 3 spaces in the end. )   
// Version - 1.00
// SDM file - 4428.sdm


/*****************************************************************
Macro : SLE4428_GET_ATR_BYTES

Input : None

Output: ucBuff - Cmd data that is fed into Transmit_APDU
        ucBuffLen - Buffer length to send to Transmit_APDU

Description:This command will reset the card and retrieve ATR bytes 
			and returns ATR bytes received from the card. The card must 
			be powered up using the power on command. The response buffer 
			must be large enough to store 4 bytes.

*******************************************************************/


#define SLE4428_GET_ATR_BYTES(ucBuff,ucBuffLen) {	\
memset(ucBuff,0x00, 0x6);			\
ucBuff[1] = 0x1;				\
ucBuffLen = 0x6 ;				\
}

/*****************************************************************
Macro : SLE4428_READ_PROTECTED

Input : addrMSB - MSB of the address from where the data needs to be read 
		addrLSB - LSB of the address from where the data needs to be read 
		len - length of the data to be read
        
Output: ucBuff - Cmd data that is fed into Transmit_APDU
        ucBuffLen - Buffer length to send to Transmit_APDU

Description:This command reads the requested number of bytes from 
			the card with the protected bit beginning with the 
			location specified in "addr". Data is returned in a two 
			byte field. First byte is the data value read from the 
			memory location. Second byte is the value of the protection 
			bit in bit position 7. An erased protection bit will yield 
			the value of 0x80. Mximum value of len may not exceed 256/2(128)
			bytes. Value of zero will yield 128 bytes.			
*******************************************************************/

#define SLE4428_READ_PROTECTED(ucBuff, addrMSB, addrLSB, len, ucBuffLen) {	\
memset(ucBuff,0x00, 0x6);			\
ucBuff[1] = 0x2;				\
ucBuff[2] = addrMSB;	            \
ucBuff[3] = addrLSB;	            \
ucBuff[4] = len;				\
ucBuffLen = 0x6 ;				\
}

/*****************************************************************
Macro : SLE4428_READ_UNPROTECTED

Input : addrMSB - MSB of the address from where the data needs to be read 
		addrLSB - LSB of the address from where the data needs to be read 
		len - length of the data to be read
        
Output: ucBuff - Cmd data that is fed into Transmit_APDU
        ucBuffLen - Buffer length to send to Transmit_APDU

Description:This command reads the requested number of bytes from 
			the card beginning with the location given in "addr".
			Value of Len equal to zero will yield 256 bytes.
*******************************************************************/

#define SLE4428_READ_UNPROTECTED(ucBuff, addrMSB, addrLSB, len, ucBuffLen) {	\
memset(ucBuff,0x00, 0x6);			\
ucBuff[1] = 0x3;				\
ucBuff[2] = addrMSB;	            \
ucBuff[3] = addrLSB;	            \
ucBuff[4] = len;				\
ucBuffLen = 0x6 ;				\
}

/*****************************************************************
Macro : SLE4428_WRITE_ERASE_PROTECTED


Input : addrMSB - MSB of the address from where the data needs to be read 
		addrLSB - LSB of the address from where the data needs to be read 
		numBytes - number of bytes of data to be written
		databytes - data to be written

Output: ucBuff - Cmd data that is fed into Transmit_APDU
        ucBuffLen - Buffer length to send to Transmit_APDU

Description:This command performs the write and erase with protected 
			bit. Operation performed on "NumBytes" number of bytes of 
			memory space starting at the "Addr" address. Writing into 
			the secure area is not permitted. This is to protect against 
			accidental modification of the PSC. 
			The area that needs to be written should be unprotected area
*******************************************************************/

#define SLE4428_WRITE_ERASE_PROTECTED(ucBuff, addrMSB, addrLSB, numBytes, dataBytes, ucBuffLen) {	\
memset(ucBuff,0x00, (numBytes+0x6));			/*code-review-7 Dec 2004. Add another bracket at the end */ \
ucBuff[1] = 0x4;				\
ucBuff[2] = addrMSB;	            \
ucBuff[3] = addrLSB;	            \
ucBuff[4] = numBytes;				\
memcpy(&ucBuff[5], dataBytes, numBytes);				\
ucBuff[4 + numBytes + 1] = 0x00;/*Assigning the Le */ /*code-review-7 Dec 2004. change the assignment from 5 + numBytes to
										4 + numBytes + 1 */ \
ucBuffLen = 0x5 + numBytes + 0x1;				/*code-review-7 Dec 2004. change the assignment from 6 + numBytes to 
										5 + numBytes + 1 */ \
}

/*****************************************************************
Macro : SLE4428_WRITE_ERASE_UNPROTECTED

Input : addrMSB - MSB of the address from where the data needs to be read 
		addrLSB - LSB of the address from where the data needs to be read 
		numBytes - number of bytes of data to be written
		databytes - data to be written
        
Output: ucBuff - Cmd data that is fed into Transmit_APDU
        ucBuffLen - Buffer length to send to Transmit_APDU

Description:This command performs the write and erase without protected 
			bit. Operation performed on "NumBytes" number of bytes of 
			memory space starting at the "Addr" address. Writing into 
			the secure area is not permitted. This is to protect against 
			accidental modification of the PSC. 
			The area that needs to be written should be unprotected area
*******************************************************************/

#define SLE4428_WRITE_ERASE_UNPROTECTED(ucBuff, addrMSB, addrLSB, numBytes, dataBytes, ucBuffLen) {	\
memset(ucBuff,0x00, (numBytes+0x6));			/*//code-review-7 Dec 2004. Add another bracket at the end */ \
ucBuff[1] = 0x5;				\
ucBuff[2] = addrMSB;	            \
ucBuff[3] = addrLSB;	            \
ucBuff[4] = numBytes;				\
memcpy(&ucBuff[5], dataBytes, numBytes);				\
ucBuff[4 + numBytes + 1] = 0x00;/*Assigning the Le */ /*code-review-7 Dec 2004. change the assignment from 5 + numBytes to
										4 + numBytes + 1 */ \
ucBuffLen = 0x5 + numBytes + 0x1;				/*code-review-7 Dec 200. change the assignment from 6 + numBytes to 
										5 + numBytes + 1 */ \
}

/*****************************************************************
Macro : SLE4428_PROTECT_COMPARE

Input : addrMSB - MSB of the address from where the data needs to be read 
		addrLSB - LSB of the address from where the data needs to be read 
		numBytes - number of bytes of data to be written
		databytes - data to be written
        
Output: ucBuff - Cmd data that is fed into Transmit_APDU
        ucBuffLen - Buffer length to send to Transmit_APDU

Description:This command performs comparsion of the data and protects the data
			if comparison is successful.Operation performed on "numBytes" number of bytes of 
			memory space starting at the "addr" address. Writing into the 
			secure area is not permitted.This is to protect against the 
			accidental modification of the PSC.
			The area that needs to be written should be unprotected area
*******************************************************************/

#define SLE4428_PROTECT_COMPARE(ucBuff, addrMSB, addrLSB, numBytes, dataBytes, ucBuffLen) {	\
memset(ucBuff,0x00, (numBytes+0x6));			/*//code-review-7 Dec 2004. Add another bracket at the end */ \
ucBuff[1] = 0x6;				\
ucBuff[2] = addrMSB;	            \
ucBuff[3] = addrLSB;	            \
ucBuff[4] = numBytes;				\
memcpy(&ucBuff[5], dataBytes, numBytes);				\
ucBuff[4 + numBytes + 1] = 0x00;/*Assigning the Le */ /*code-review-7 Dec 2004. change the assignment from 5 + numBytes to
										4 + numBytes + 1 */ \
ucBuffLen = 0x5 + numBytes + 0x1;				/*code-review-7 Dec 2004. change the assignment from 6 + numBytes to 
										5 + numBytes + 1 */ \
}

/*****************************************************************
Macro : SLE4428_PSC_VERIFICATION

Input : PSC1 - first byte of PSC to be verified
		PSC2 - second byte of PSC to be verified
        
Output: ucBuff - Cmd data that is fed into Transmit_APDU
        ucBuffLen - Buffer length to send to Transmit_APDU

Description:This command decrements the error counter, then performs 
			Compare Verification data between the 2 byte PSC. The error
			counter is erased. Success is determined by reading back 
			the erased counter with the value of 0xFF. 
*******************************************************************/
#define SLE4428_PSC_VERIFICATION(ucBuff, psc, ucBuffLen) {	\
memset(ucBuff,0x00, 0x6);			\
ucBuff[1] = 0x7;				\
memcpy(&ucBuff[2], psc, 2);		\
ucBuffLen = 0x6  ;				\
}

/*****************************************************************
Macro : SLE4428_SET_PSC

Input : PSC1 - first byte of PSC to be compared
		PSC2 - second byte of PSC to be compared 
        
Output: ucBuff - Cmd data that is fed into Transmit_APDU
        ucBuffLen - Buffer length to send to Transmit_APDU

Description:This command is used to change the secret code after 
			it has been validated.
*******************************************************************/
#define SLE4428_SET_PSC(ucBuff, psc, ucBuffLen) {	\
memset(ucBuff,0x00, 0x6);			\
ucBuff[1] = 0x8;				\
memcpy(&ucBuff[2], psc, 2);		\
ucBuffLen = 0x6  ;				\
}


//END

