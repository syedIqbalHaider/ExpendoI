/* LIBVOY.H - Voyager library smart card interface */
/* Copyright 1999-2002 by VeriFone.  All Rights Reserved. */

#ifndef __EXTLIBVOY__
#define __EXTLIBVOY__

#ifdef __cplusplus
extern "C" {
#endif

/*=======================================================================*/
/*                              TYPEDEF                                  */
/*=======================================================================*/
#ifndef BYTE_COUNT  // ACT 2000 also defines BYTE
#ifndef __BYTE__
#define __BYTE__
typedef unsigned char BYTE;
#endif
#endif // BYTE_COUNT

#ifndef __WORD__
#define __WORD__
typedef unsigned short  WORD;
#endif
#ifndef __DWORD__
#define __DWORD__
typedef unsigned long DWORD;
#endif
#ifndef __EXTRESPONSECODE__
#define __EXTRESPONSECODE__
typedef DWORD         EXTRESPONSECODE;
#endif

/*=======================================================================*/
/*               DEFINE :  SYNC DRIVER DESCRITOR LIST STRUCTURE          */
/*=======================================================================*/

/*=======================================================================*/
/*                         DEFINE :  VOYAGER                             */
/*=======================================================================*/

#define STANDARD_EMV311                 (DWORD)0x0000
#define ISO_7816                        (DWORD)0x0001
#define NO_STANDARD                     (DWORD)0x0003

#define ICC_CNT                         6

#define CLASS_A                         (DWORD)0x0001
#define CLASS_B                         (DWORD)0x0002
#define CLASS_AB                        (DWORD)0x0003
#define VOLT1_8                         (DWORD)0x0004

#define MANUAL_PTS                      (DWORD)0x0001
#define AUTO_PTS                        (DWORD)0x0002
#define NO_PTS                          (DWORD)0x0003

#define EXT_IFD_POWER_UP                (WORD)1  /* Cold_ATR()  */
#define EXT_IFD_POWER_DOWN              (WORD)2  /* Power_Off() */
#define EXT_IFD_RESET                   (WORD)3  /* Warm_ATR()  */

#define BYTE_ABSENT                     0xFFFF
#define PROTOC_T0                       1
#define PROTOC_T1                       2

#define UNKNOWN                         (BYTE)0
#define ASYNC_7816                      (BYTE)1
#define SYNC_7816                       (BYTE)2
#define ASYNC                           (DWORD)0x00

#define VENDOR_NAME_SIZE                32
#define VENDOR_IFDTYPE_SIZE             32
#define VENDOR_SERIAL_NO_SIZE           32
#define ATR_SIZE                        33 // Should be 32 as per PC/SC as per ISO 33

// To remap name to slot number change these values so that the name matches the slot
#define EXT_CUSTOMER_CARD               (DWORD)0x10000001
#define EXT_MERCHANT_FULL_SIZE_CARD     EXT_UNSUPPORTEDSLOT
#define EXT_MERCHANT_SLOT_1             EXT_UNSUPPORTEDSLOT
#define EXT_MERCHANT_SLOT_2             EXT_UNSUPPORTEDSLOT
#define EXT_MERCHANT_SLOT_3             EXT_UNSUPPORTEDSLOT
#define EXT_MERCHANT_SLOT_4             EXT_UNSUPPORTEDSLOT
#define EXT_UNSUPPORTEDSLOT             (DWORD)500

#define Tag_Logical_Handle              (DWORD)0x020C
#define Tag_Vendor_Name                 (DWORD)0x0100
#define Tag_Vendor_IFD_Type             (DWORD)0x0101
#define Tag_Vendor_IFD_Version          (DWORD)0x0102
#define Tag_IFD_Serial_Number           (DWORD)0x0103
#define Tag_Channel_ID                  (DWORD)0x0110
#define Tag_Asynch_protocol_types       (DWORD)0x0120
#define Tag_Default_CLK                 (DWORD)0x0121
#define Tag_Max_CLK                     (DWORD)0x0122
#define Tag_Default_Data_Rate           (DWORD)0x0123
#define Tag_Max_Data_Rate               (DWORD)0x0124
#define Tag_Max_IFSD                    (DWORD)0x0125
#define Tag_Synch_protocol_types        (DWORD)0x0126
#define Tag_Power_Management            (DWORD)0x0131
#define Tag_UserToCard_Auth_Device      (DWORD)0x0140
#define Tag_User_Auth_Device            (DWORD)0x0142
#define Tag_Mechanical_Characteristics  (DWORD)0x0150
#define Tag_Open_SCard_Reader           (DWORD)0x0180
#define Tag_Close_SCard_Reader          (DWORD)0x0181
#define Tag_Current_IFM_Version         (DWORD)0x0183
#define Tag_Override_Card_ATR           (DWORD)0x0184
#define Tag_Reader_IFSD                 (DWORD)0x0185
#define Tag_Open_ICC                    (DWORD)0x0188
#define Tag_Close_ICC                   (DWORD)0x0189
#define Tag_Select_ICC                  (DWORD)0x0190
#define Tag_Nad_Management              (DWORD)0x0191
#define Tag_Convention                  (DWORD)0x0192
#define Tag_WTX_Management              (DWORD)0x0193
#define Tag_Class_Management            (DWORD)0x0194
#define Tag_Pts_Management              (DWORD)0x0195
#define Tag_Error_Management            (DWORD)0x0196
#define Tag_Standard                    (DWORD)0x0197
#define Tag_Card_Type                   (DWORD)0x01A0
#define Tag_ICC_Structure               (DWORD)0x01B0
#define Tag_ICC_Structure_Size          (DWORD)0x01B2
#define Tag_PSCR_Insertion_Count        (DWORD)0x01D5  // jeff_s9 04/20/2006
#define Tag_Sync_List                   (DWORD)0x01E0

#define Tag_ICC_Presence                (DWORD)0x0300
#define Tag_ICC_Interface_Status        (DWORD)0x0301
#define Tag_ATR_String                  (DWORD)0x0303
#define Tag_ICC_Type                    (DWORD)0x0304
#define Tag_ATR_Length                  (DWORD)0x0305

#define Tag_Current_Protocol_Type       (DWORD)0x0201
#define Tag_Current_CLK                 (DWORD)0x0202
#define Tag_Current_F                   (DWORD)0x0203
#define Tag_Current_D                   (DWORD)0x0204
#define Tag_Current_N                   (DWORD)0x0205
#define Tag_Current_W                   (DWORD)0x0206
#define Tag_Current_IFSC                (DWORD)0x0207
#define Tag_Current_IFSD                (DWORD)0x0208
#define Tag_Current_BWT                 (DWORD)0x0209
#define Tag_Current_CWT                 (DWORD)0x020A
#define Tag_Current_EBC                 (DWORD)0x020B


#define EXT_IFD_Success                     0
#define EXT_IFD_Failure                     1
#define EXT_IFD_Error_Tag                   2  /* Tag does not exist.         */
#define EXT_IFD_Error_Not_Supported         3  /* Tag not supported.          */
/*** Set and Get Capabilities ***/
#define EXT_IFD_Error_Set_Failure           5  /* Operation failed.           */
#define EXT_IFD_Error_Value_Read_Only       6  /* Value cannot be modified.   */
/*** PTS management ***/
#define EXT_IFD_Error_PTS_Failure           7  /* PTS Operation failed.       */
#define EXT_IFD_Protocol_Not_Supported      8  /* protocol not supported.     */
/*** Power management ***/
#define EXT_IFD_Error_Power_Action          9  /* Action not carried out.     */
/*** Swallow management ***/
#define EXT_IFD_Error_Swallow              10 /* Card not swallowed.          */
/*** Swallow management ***/
#define EXT_IFD_Error_Eject                11 /* Card not ejected.            */
/*** Swallow management ***/
#define EXT_IFD_Error_Confiscate           12 /* Card not confiscated.        */
/*** Transmit management ***/
#define EXT_IFD_Communication_Error        13 /* Request not send.            */
#define EXT_IFD_response_TimeOut           14 /* IFD time-out waiting the ICC */
#define EXT_IFD_Error_BadFormat            15 /* input message bad format.   */

// Purvin_P1 Added New macros for Key Introduction 
#define EXT_IFD_KEYS_NOT_PRESENT           16
#define EXT_IFD_SIG_ERROR                  17
#define EXT_IFD_ENCRYPT_ERROR              18
#define EXT_IFD_KEY_FILE_ABSENT            19   
#define EXT_IFD_SERVICE_ALARM_SET          20

//MAGCARD: purvin_p1 date 30/05/08 Added For Mag card
#define EXT_IFD_DATA_NOT_PRESENT           21





//MAGCARD: purvin_p1 date 30/05/08 Added For Mag card
#define MAG_SWIP_STATUS_SIZE  3   
#define TRACK1_SIZE           80            /* Size of track one data       */
#define TRACK2_SIZE           40            /* Size of track two data       */
#define TRACK3_SIZE          150            /* Size of track three data     */

typedef struct 
{
	BYTE uchStatus[3]; // 0th Byte indicates TRACK1 status,1th Byte indicates TRACK2 status,2th Byte indicates TRACK3 status
	BYTE uchTrack1[TRACK1_SIZE + 1];
    BYTE uchTrack2[TRACK2_SIZE + 1];
    BYTE uchTrack3[TRACK3_SIZE + 1];
    BYTE ushTrack1_Len;
    BYTE ushTrack2_Len;
    BYTE ushTrack3_Len;   
}MAG_DATA;





#define Voy_Cmd_Timeout                (30*1000) /* 30 sec default timeout used by TransmitReceive */

extern EXTRESPONSECODE EXT_IFD_Is_ICC_Present (void);
extern EXTRESPONSECODE EXT_IFD_Is_ICC_Absent (void);
extern EXTRESPONSECODE EXT_IFD_Get_Capabilities (DWORD Tag, BYTE *Value) ;
extern EXTRESPONSECODE EXT_IFD_Set_Capabilities (DWORD Tag, BYTE *Value) ;
extern EXTRESPONSECODE EXT_IFD_Power_ICC (WORD ActionRequested);
extern EXTRESPONSECODE EXT_IFD_Transmit_to_ICC (BYTE *CommandData,  BYTE *ResponseData);
extern EXTRESPONSECODE EXT_IFD_Set_Protocol_Parameters (DWORD ProtocolType,
                                                 BYTE  SelectionFlags,
                                                 BYTE  PTS1,
                                                 BYTE  PTS2,
                                                 BYTE  PTS3);
extern EXTRESPONSECODE EXT_IFD_Confiscate_ICC (void);
extern EXTRESPONSECODE EXT_IFD_Eject_ICC (void);
extern EXTRESPONSECODE EXT_IFD_Swallow_ICC (void);


extern EXTRESPONSECODE EXT_IFD_PresentClearTextPIN(BYTE *RespVerifyPIN);
extern EXTRESPONSECODE EXT_IFD_PresentEncryptEMVPIN(BYTE * INICCRandom,
                                         BYTE * PubKeyData,
                                         int PubKeyLen,
                                         int Exponent,
                                         BYTE *ResponseData);

extern EXTRESPONSECODE EXT_IFD_Key_Introduction(void);
extern EXTRESPONSECODE EXT_IFD_RESET_SERVICE_ALARM(BYTE* uscPassword);

//Added Below API for MAG card
EXTRESPONSECODE EXT_IFD_READMAGDATA(WORD sec,MAG_DATA *stMagData);


EXTRESPONSECODE EXT_IFD_INIT_SCR(void);

EXTRESPONSECODE EXT_IFD_CLEARMAGDATA(void);

EXTRESPONSECODE EXT_IFD_RESET_READER(void);

//Added To support PCI requirement 
EXTRESPONSECODE EXT_IFD_CHECK_ALARM_STATUS(void);




// Philips generic error definition
#define CARD_DEACTIVATED        0x10000001
#define CARD_MOVED              0x10000002
#define CARD_NOT_PRESENT        0x10000003

// Philips Error definition for ATR
#define ATR_MUTE                0x10000010
#define EARLY_ANSWER    		0x10000011
#define ATR_PARITY_ERROR  		0x10000012
#define ATR_WWT_EXCEEDED        0x10000013
#define ATR_DURATION_EXCEEDED   0x10000014
#define TB1_NOT_ZERO    		0x10000015
#define TB1_NOT_PRESENT 		0x10000016
#define NO_T0_NO_T1     		0x10000017
#define B5_OF_TA2_SET           0x10000018
#define TB2_PRESENT     		0x10000019
#define WRONG_WI        		0x1000001A
#define PROTOCOL_MISMATCH       0x1000001B
#define WRONG_IFSC      		0x1000001C
#define WRONG_CWI       		0x1000001D
#define WRONG_BWI       		0x1000001E
#define WRONG_TC1_CWT   		0x1000001F
#define TB3_ABSENT      		0x10000020
#define WRONG_TC3       		0x10000021
#define BAD_FiDi        		0x10000022
#define ATR_CHECKSUM_ERROR      0x10000023
#define ATR_LEN_EXCEEDED        0x10000024
#define TS_NEITHER_3B_OR_3F     0x10000025
#define ATR_NOT_SUPPORTED   	0x10000026

// Philips Error definition for T=0 protocol
#define TOO_SHORT_APDU          0x10000030
#define WRONG_APDU              0x10000031
#define WWT_EXCEEDED            0x10000032
#define INS_ERROR               0x10000033
#define T0_PARITY_ERROR  		0x10000034

// Philips Error definition for T=1 protocol
#define CARD_MUTE_NOW   		0x10000050
#define RESYNCHRONISED  		0x10000051
#define CHAIN_ABORT     		0x10000052
#define BAD_NAD         		0x10000053
#define IFSD_NOT_ACCEPTED		0x10000054

// Philips Error definition for PPS negotiation
#define PARAM_ERROR     		0x10000070
#define PPS_NOT_ACCEPTED 		0x10000071
#define RESPONSE_ERROR  		0x10000072
#define PCK_ERROR       		0x10000073
#define PPS_PARITY_ERROR  		0x71000004

// Philips Hardware error
#define CARD_ERROR              0x100000E0
#define BAD_CLOCK_CARD          0x100000E1
#define UART_OVERFLOW           0x100000E2
#define SUPERVISOR_ACTIVATED    0x100000E3
#define TEMPERATURE_ALARM       0x100000E4
#define FRAMING_ERROR           0x100000E9

// NON-Philips System Errors
#define TOO_MANY_CARDS_POWERED  0x00000109


// Added to get the version number of the ESCR Library
#define ESCR_VERSION 						"2.1.4.01"


#ifdef __cplusplus
}
#endif

#endif // __LIBVOY__
