/*
 * FILENAME       : VXEMVAP_confio.h
 * PRODUCT        : Verix/Vx.
 * VERSION        : 4.0.0
 * AUTHOR(s)      : Pavan_k1
 * CREATED        : 12/Dec/06
 *
 * DESCRIPTION    : 
 *
 * CONTENTS   :
 * MODIFICATION HISTORY    :
 *
 * #    Date        Who         History
 * ---  --------    ----------  ----------------------------------------
 * 1    12 Dec 06   Pavan_k1    Created for Vx EMV Module 4.0.0
 *
 * 2    28 Jun 07   Brabhu_H1   TXNSQNUM:Added a max limit for transaction 
 *                              sequence counter
 *
 * Copyright (C) 2006 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language, in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual or otherwise, without the prior written
 * permission of VeriFone Inc.
 *                                                2099 Gateway Place
 *                                                  Suite 600
 *                                                  San Jose  CA  95110 
 *                                                  USA
 *
 *
 *
 */
#ifndef vsconfio_H
#define vsconfio_H

#define GEN_VER_SIZE             16
#define DATA_FILE_NOT_FOUND      147
#define INVALID_INDEX            148 

//Brabhu_H1 dated 28 June 2007
//TXNSQNUM: Fix made for onTime ID 13166
//Added a max limit for transaction sequence counter
#define MAX_TXNSEQCOUNTER (unsigned long)99999999


    int     inLoadConfigRec (char *szFileName, int inRecSize, int inRecNum, char *pchConfRec);
    int     inSaveConfigRec(char *szFileName, int inRecSize, int inRecNum, char *pchConfRec);
    long    lnGetSizeofFile (char *pchFileName);

    int     inGetNumberOfConfigRecs (char *szFileName, int inRecSize); 
    int     inAppendConfigRec (char *szFileName, int inRecSize, char *pchConfRec);
    int     inDeleteConfigRec (char *szFileName, int inRecSize, int inRecNum);
    int     inSearchConfigRec (char *szFileName, int inRecSize, int *inRecNum, char *pchConfRec, int (*inCompare)(), char *pchCompRec);


#endif

