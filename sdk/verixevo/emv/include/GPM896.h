/*-----------------------------------------------------------------------------
 *
 *      Filename:       GPM896.h
 *
 *      Product:        Card Slot Library
 *      Version:        1.1     
 *      Author :        Asha_S2
 *  	Module :	
 *      Defines all macros needed for GPM896 synchronous card.
 *
 * MODIFICATION HISTORY    :
 *
 * #      Date		    Who			History
 * ---  ---------    ---------		----------------------------------------
 * 1.   12 Dec 02    Asha_s2		Created.
 * 
 * 2    13 Dec 02    Krishnan_S1    Code review. CodeReview-13-Dec-2002
 *                   Chandran_D1
 *
 * 3.	18 Dec 02	 Chandran_D1	Fix in macro GPM896_ERASE_AREA2 and GPM896_ERASE_AREA1
 *
 * 4    13 Jan 03    Asha_S2        Fix made after Ricardo's feedback.
 *
 * 5    16 Jan 03    Asha_s2        Fix made after Ricardo's feedback.
 *
 * 6.	24 Jan 03	 Chandran_D1	Fix in macro GPM896_ERASE_BYTE
 *									Added the Descriptor, Version and SDM file name
 *
 *******************************************************************************
 * Copyright (C) 2000 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language 
 * or computer language, in any form or by any means, electronic, mechanical, 
 * magnetic, optical, chemical, manual or otherwise, without the prior written 
 * permission of VeriFone Inc.
 *                                                VeriFone Inc.               
 *                                                III Lagoon Drive, Suite 400  
 *                                                Redwood City, CA 94065      
 ******************************************************************************
 */

/***************************************************************/


// Macros for version 1.00 of GPM896
#define GPM896											(DWORD)0x0003

// GPM896 SYNC CARD
// Descriptor - GPM896      
// Version - 1.00
// SDM file - GPM896.sdm

/*****************************************************************
Macro : GPM896_READ_BYTE

Input : addrMSB - address from where the data needs to be read 
        addrLSB - address from where the data needs to be read 
        bytesToRead - Number of bytes to read


Output: ucBuff - Cmd data that is fed into Transmit APDU
        ucBuffLen - Buffer length to send to Transmit_APDU
*******************************************************************/


#define GPM896_READ_BYTE(ucBuff, addrMSB, addrLSB, bytesToRead, ucBuffLen) {	\
memset(ucBuff,0x00, 6);	\
ucBuff[1] = 0x1;		\
ucBuff[2] = addrMSB;	\
ucBuff[3] = addrLSB;	\
ucBuff[5] = bytesToRead;\
ucBuffLen = 0x6 ;		\
}


/*******************************************************************
Macro : GPM896_WRITE_BYTE

Input : addrMSB - address from where the data needs to be read 
        addrLSB - address from where the data needs to be read  
        data - data to be written
        dataLen - length of the data

Output: ucBuff - Cmd data that is fed into Transmit APDU
        ucBuffLen - Buffer length to send to Transmit_APDU
********************************************************************/

#define GPM896_WRITE_BYTE(ucBuff, addrMSB, addrLSB, data, dataLen, ucBuffLen) {	\
memset(ucBuff, 0x00, 7);						\
ucBuff[1] = 0x2;								\
ucBuff[2] = addrMSB;							\
ucBuff[3] = addrLSB;							\
ucBuff[4] = dataLen;							\
memcpy(&ucBuff[5], data, dataLen);				\
ucBuff[5 + dataLen] = 0x00;/*Assigning the Le */\
ucBuffLen = 5 + dataLen + 1;					\
}

/********************************************************************
Macro : GPM896_ERASE_BYTE

Input : addrMSB - address from where the data needs to be read 
        addrLSB - address from where the data needs to be read 
        wordToErase - Number of words to erase

Output: ucBuff - Cmd data that is fed into Transmit APDU
        ucBuffLen - Buffer length to send to Transmit_APDU
*********************************************************************/


#define GPM896_ERASE_BYTE(ucBuff, addrMSB, addrLSB, wordsToErase, ucBuffLen) {	\
memset(ucBuff,0x00, 6);		\
ucBuff[1] = 0x3;			\
ucBuff[2] = addrMSB;		\
ucBuff[3] = addrLSB;		\
ucBuff[4] = wordsToErase;	\
ucBuffLen = 0x6;			\
}

/************************************************************************
Macro : GPM896_PRESENT_SECRET_CODE

Input : CSCMSB - Card Secret Code address
        CSCLSB - Card Secret Code address

Output: ucBuff - Cmd data that is fed into Transmit APDU
        ucBuffLen - Buffer length to send to Transmit_APDU
**************************************************************************/


#define GPM896_PRESENT_SECRET_CODE(ucBuff, CSCMSB, CSCLSB, ucBuffLen ) {		\
memset(ucBuff,0x00, 6);     \
ucBuff[1] = 0x4;			\
ucBuff[2] = CSCMSB;			\
ucBuff[3] = CSCLSB;			\
ucBuffLen = 0x6 ;	        \
}

/************************************************************************
Macro : GPM896_ERASE_AREA1

Input : ucEraseCode - EC1. code to erase the application area

Output: ucBuff - Cmd data that is fed into Transmit APDU
        ucBuffLen - Buffer length to send to Transmit_APDU
**************************************************************************/

#define GPM896_ERASE_AREA1(ucBuff, ucEraseCode, ucBuffLen) {	\
memset(ucBuff,0x00, 7);		\
ucBuff[1] = 0x5;            \
ucBuff[4] = 6;            /*change based on Ricardo's feedback*/ \
memcpy(&ucBuff[5],ucEraseCode,6) ; /*6 byte ersase code to be passed by user */ \
ucBuff[11] = 0x0;            \
ucBuffLen = 12 ; /* change made based on Ricardo's feedback */	        \
}


/************************************************************************
Macro : GPM896_ERASE_AREA2

Input : ucEraseCode - EC2. code to erase the application area

Output: ucBuff - Cmd data that is fed into Transmit APDU
        ucBuffLen - Buffer length to send to Transmit_APDU
**************************************************************************/

#define GPM896_ERASE_AREA2(ucBuff, ucEraseCode, ucBuffLen) {	\
memset(ucBuff,0x00, 7);		\
ucBuff[1] = 0x6;	        \
ucBuff[4] = 4;             /*change based on Ricardo's feedback*/ \
memcpy(&ucBuff[5],ucEraseCode,4) ; /*4 byte ersase code to be passed by user */ \
ucBuff[9] = 0x0;              /* change made based on Ricardo's feedback*/   \
ucBuffLen = 10 ;	        /* change made based on Ricardo's feedback*/     \
}

/************************************************************************
Macro : GPM896_BLOW_FUSE

Input : None

Output: cBuff - Cmd data that is fed into Transmit APDU
        ucBuffLen - Buffer length to send to Transmit_APDU
**************************************************************************/


#define GPM896_BLOW_FUSE(ucBuff, ucBuffLen) {\
memset(ucBuff,0x00, 6); \
ucBuff[1] = 0x7;	    \
ucBuffLen = 0x6 ;	    \
}

