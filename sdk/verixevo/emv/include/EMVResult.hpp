/*
 * FILENAME       : emvResult.hpp
 * PRODUCT        : Verix/Vx.
 * VERSION        : 4.0.0
 * AUTHOR(s)      : Pavan_k1
 * CREATED        : 07/Dec/06
 *
 * @Description	:This file has the definitions for error codes and return values.
 *
 * CONTENTS   :
 * MODIFICATION HISTORY    :
 *
 * #    Date        Who         History
 * ---  --------    ----------  ----------------------------------------
 * 1    07 Dec 06   Pavan_k1    Created for Vx EMV Module 4.0.0

 * 2    31/Jan/06   Soumya_G1   Chnaged the VERSION_NUM to 4.0.0.01

 * 3    28 Feb 07   Pavan_K1    Added macros for the Common module.
 
 * 4    8/March/07  Soumya_G1   Added a new return value from the library E_CAPK_FILE_EXPIRED
 
 * 5    09 Mar 07   Pavan_K1    Changed the Version number to "Ver 4.0.0.02".
 *
 * 6    10 Apr 07   Pavan_K1    Changed the version number of the EMV Module to 4.0.0.03 from 4.0.0.02
 * 7    24 Apr 07   Purvin_p1   Fix for Ontime Defect# 8151.Added 2 macros
 * 8    14 May 07   Pavan_K1    Added the macros for the Account type selection implementation.
 * 
 * 9    21 May 07   Pavan_K1    Added macros to represent the transaction states.
 *
 * 10   31 May 07   Pavan_K1    Changed the version number of the EMV Module to 4.0.0.04 from 4.0.0.03
 *
 * 11   15 Jun 07   Pavan_K1    Changed the version number from 4.0.0.04 to 4.0.0.05.
 *
 * 12   13 Jul 07   Soumya_g1   Changed the version number from 4.0.0.05 to 4.0.0.06.
 * 13   18 Jul 07   Purvin_p1   Ontime Id :13809, Change E_PP_ABSENT value from -1 to 0xff93.
 * 14   20 Jul 07   Pavan_K1    Changed the version number from 4.0.0.06 to 4.0.0.07.
 * 15   25 Jul 07   Pavan_K1    Changed the version number from 4.0.0.07 to 4.0.0.08.
 *
 * 16   21 Feb 08   Mary_F1     Version number is changed to 1.0.0.01 for Vx UPT Module
 * 17   17 Mar 08   Purvin_P1   Version number is changed to 1.0.0.02 for Vx UPT Module
 * 18   7  May 08  Purvin_P1    Version number is changed to 1.0.1.01 for Vx UPT Module

 * 19	15/Jun/09	Mary_F1		Changed the version number to 2.0.0.01 for Vx UPT Module 2.0.0
 * 20   16/June/09	Kamlesh_K1  080715-551: Fix made to handle more than one AID in API getAIDStatus/usEMVGetAllAIDStatus.
 * 21	28/Jul/09	Mary_F1		Changed the version number to 5.0.0.01
 * 21	19/Aug/09	Mary_F1		Changed the version number to 5.0.0.03
 * 22   04/Sep/09	Mary_F1		INVMOD:
 *								If this module is downloaded to a Vx700 terminal, it should return
 *								E_INVALID_MODULE and the application should not continue.
 * 23	19/Aug/09	Mary_F1		Changed the version number to 5.0.0.04
 * 24	10/Nov/09	Mary_F1		Changed the version number to 5.0.0.05
 * 25   27/Jan/10	Mary_F1		Changed the version number to 5.0.0.07
 * 26	27/Jan/11	Kishor_S1	MULTIAPP_PIN Changes : Added the changes from SAKO
 * 27   03/Jun/11   Mary_F1     MULTIAVN: Support for multiple application version number check
 * 28   03/Jun/11   Mary_F1     BKSPKEY: Terminal should re-prompt the previous prompt if backspace
 *								key is pressed during the "ENTER PIN" prompt
 * 29   03/Jun/11   Mary_F1		ExtAuth6985: The EMV Kernel behavior is modified to cater to the different application needs,
 *								to terminate the transaction or to continue with the transaction on receiving the status word
 *								as 0x6985 for the external authenticate command.
 * 30 	17/Nov/11	Kishor_S1	SAKO MAPP Addition: MULTIAPP_SECURE_PIN version addition 
 *
 * Copyright (C) 2011 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language, in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual or otherwise, without the prior written
 * permission of VeriFone Inc.
 *                                                2099 Gateway Place
 *												  Suite 600
 *											      San Jose  CA  95110 
 *												  USA
 *
 *
 *
 */

#ifndef _EMVRESULT
#define _EMVRESULT

#include    "EMVTypeDefs.hpp"


//Version number is changed to 5.0.0.09

#ifdef MULTIAPP_SECURE_PIN //Defined only for SAKO project

//SAKO Version No.
#define 	VERSION_NUM	                    "2.0.0.04" 

#else

//Vx 6.0.0 Version No.
	#ifdef __thumb
	#define 	VERSION_NUM	                    "6.2.0.05"
	#endif

	#ifdef  _TARG_68000
	#define 	VERSION_NUM	                    "6.0.1.01" 
	#endif

#endif

#define     EMV_FAILURE                     (Ushort)0x9999 
#define     EMV_SUCCESS                     (Ushort)0x9000 

#define	Vx700_NO_SUPPORT		(Ushort)0x9990

//Pavan_K1 dated 4-Jun-07 added the macro for handling the unexpected tags.
#define     EMV_IGNORE						(Ushort)0x9009
#define     EMV_UNDEFINED					(Ushort)0x9191

#define     EMV_SIGNATURE                   (Ushort)0x9090 
#define     EMV_ONLINE_PIN                  (Ushort)0x9095 
#define     EMV_COMBINED_DDA_AC_REQD        (Ushort)0x9096 
                                            
#define     E_TRANS_CANCELLED               (Ushort)0xffe1 
#define     E_NONE_COMMON                   (Ushort)0xffE2 
#define     E_INVALID_PARAM                 (Ushort)0xffe3 
#define     E_INVALID_APP                   (Ushort)0xffe4 
#define     E_DO_ABSENT                     (Ushort)0xffe5 
#define     E_DO_REPEAT                     (Ushort)0xffe6 
#define     E_INVALID_PIN                   (Ushort)0xffe7 
                                            
#define     E_PIN_LAST_CHANCE               (Ushort)0xffe8 
#define     E_PIN_RETRYLIMIT                (Ushort)0xffe9 
#define     E_GO_OFFLINE                    (Ushort)0xffea 
#define     E_GO_ONLINE                     (Ushort)0xffeb 
#define     E_DECLINE                       (Ushort)0xffec 
#define     E_INVALID_SEQUENCE              (Ushort)0xffed 
                                            
#define     E_TLVCOLLECTION_FULL            (Ushort)0xffee 
#define     E_TLVFORMAT                     (Ushort)0xffef 
#define     E_INVALID_PDOL                  (Ushort)0xfff0 
#define     E_INVALID_CDOL                  (Ushort)0xfff1 
#define     E_INVALID_TDOL                  (Ushort)0xfff2 
#define     E_INVALID_DDOL                  (Ushort)0xfff3 
#define     E_SDA_FAILED                    (Ushort)0xfff4 
#define     E_DDA_FAILED                    (Ushort)0xfff5 
#define     E_AID_LIST_FULL                 (Ushort)0xfff6
#define     E_ICC_BLOCKED                   (Ushort)0x6A81
#define     ICC_PSE_NOT_FOUND               (Ushort)0x6A82
#define     E_BAD_ICC_RESPONSE              (Ushort)0xfff9
#define     E_ICC_DATA_MISSING              (Ushort)0xfffa
#define     E_CANDIDATELIST_EMPTY           (Ushort)0xfffb
                                            
#define     E_6A02_FORMAT                   (Ushort)0x6a02
#define     E_6A03_FORMAT                   (Ushort)0x6a03
#define     E_6A04_FORMAT                   (Ushort)0x6a04
#define     E_6A05_FORMAT                   (Ushort)0x6a05
#define     E_0x6983                        (Ushort)0x6983
#define     E_0x6984                        (Ushort)0x6984
#define     E_HASH_FAILED                   (Ushort)0xff10
#define     E_PAN_FAILED                    (Ushort)0xff11
#define     E_CERTIFICATE_EXPIRED           (Ushort)0xff12
#define     E_HASHALGO                      (Ushort)0xff13
#define     E_IPKALGO                       (Ushort)0xff14
#define     E_9F37_ABSENT                   (Ushort)0xff15
                                            
#define     E_DIFF_AVN_ERR                  (Ushort)0xff16
#define     E_SERVICE_NOT_ALLOWED           (Ushort)0xff17
#define     E_APP_NOTYET_EFFECTIVE          (Ushort)0xff18
#define     E_APP_EXPIRED                   (Ushort)0xff19
#define     E_TXN_RAND_SELECT               (Ushort)0xff1a
#define     E_TXN_OVER_FLOOR                (Ushort)0xff1b
#define     E_UPPER_LOWER_LMT_EXCEED        (Ushort)0xff1c
#define     E_LOWER_LMT_EXCEED              (Ushort)0xff1d
#define     E_UPPER_LMT_EXCEED              (Ushort)0xff1e
#define     E_NEW_CARD                      (Ushort)0xff1f
#define     E_CVM_FAILED                    (Ushort)0xff21
#define     E_PINPAD_ABSENT                 (Ushort)0xff22
#define     E_PIN_REQD_ABSENT               (Ushort)0xff23
#define     E_UNRECOGNIZED_CVM              (Ushort)0xff24
#define     E_CVM_PASSED                    (Ushort)0xff25
#define     E_BAD_DATA_FORMAT               (Ushort)0xff26
#define     E_BAD_HASHALGO                  (Ushort)0xff27
#define     E_BAD_IPKALGO                   (Ushort)0xff28
#define     E_STATWORD_WARNING              (Ushort)0xff2b
#define     E_STATWORD_NORMAL               (Ushort)0xff2c
#define     E_SCRIPTPROCFAILBEFORE_GENAC    (Ushort)0xff2d
#define     E_SCRIPTPROCFAILAFTER_GENAC     (Ushort)0xff2e
                                            
#define     E_INVALID_LENGTH                (Ushort)0xff2f
#define     E_TAG_NOTFOUND                  (Ushort)0xff30
#define     E_NO_MORE_TLV                   (Ushort)0xff31
#define     E_INVALID_OFFSET                (Ushort)0xff32
                                            
#define     E_CHIP_ERROR                    (Ushort)0xff33
#define     E_BAD_COMMAND                   (Ushort)0xff34
#define     E_TAG_ALREADY_PRESENT           (Ushort)0xff35
#define     E_CARD_REMOVED                  (Ushort)0xff36
#define     E_READ_RECORD_FAILED            (Ushort)0xff37
#define     E_INVALID_SDOL                  (Ushort)0xff38    
#define     E_EXPLICIT_SELEC_REQD           (Ushort)0xff39
#define     E_COND_NOT_SATISFIED            (Ushort)0xff40
#define     E_CSN_FAILURE                   (Ushort)0xff41
#define		E_TAG_NOT_SUPPORTED			    (Ushort)0xff42

#define	    E_WRONG_PIN_PARAM			    (Ushort)0xff63

//Purvin_P1 date 18 Jul 07 Ontime Id :13809
//Change E_PP_ABSENT value from -1 to 0xff93
//#define		E_PP_ABSENT					    (Ushort)-1
#define		E_PP_ABSENT					    (Ushort)0xff93


#define		E_PIN_REQD					    (Ushort)0xff43
#define		E_LAST_PIN_TRY				    (Ushort)0xff45
#define		E_PIN_TRY_LT_EXCEED			    (Ushort)0xff46
#define		E_INVALID_CAPK  			    (Ushort)0xff47
#define		E_CAPK_FILE_NOT_FOUND		    (Ushort)0xff48
#define		E_COMBINED_DDA_AC_FAILED	    (Ushort)0xff49

#define		E_BLACKLISTED_CARD      	    (Ushort)0xff50
#define     NOT_USED                        (Ushort)0

#define     E_NO_ATR                        (Ushort)0xff51

#define		E_INVALID_KEY_LENGTH		    (Ushort)0xff52
#define		E_CVM_NOT_PERFORMED				(Ushort)0xff53

#define		E_PIN_BLOCKED				    (Ushort)0xff54

#define     UNRECOGNIZED_CVM                (Ushort)0xff79
#define     ONLINE_PIN_PERFORMED            (Ushort)0xff80

#define     E_INVALID_ATR	                (Ushort)0xff81
#define		E_APP_BLOCKED				    (Ushort)0x6283
#define 	EMV_PIN_SESSION_IN_PROGRESS		(Ushort)0x9110
#define 	EMV_NEXT_CVM					(Ushort)0x9101
#define 	EMV_ABORT						(Ushort)0x9102
#define 	NO_PINBYPASS					(Ushort)0x9108
#define 	EMV_PIN_SESSION_COMPLETE		(Ushort)0x9109

#define 	E_USR_ABORT	 					(Ushort)0xff82

// 28/Feb/07 Purvin_p1 TC # V2CE0030202 
//Added a new return value for length wrror for validateResp() function in EMVUtils.cpp
#define     E_LENGTH_ERROR                  (Ushort)0xff83


//30/Jan/07  Soumya_G1  TC# 2CM.042.06
//Introduced a new return value from the library to indicate CDA failed after First GenAC & the card returned ARQC.
#define     E_CDA_ARQC_FAILED   			(Ushort)0xff84

// Tisha_K1 16/Jul/07
// For verix changed the value of DECLINE_CDA_ARQC_FAILED as 0x66ff 
#ifdef _TARG_68000
 #define     DECLINE_CDA_ARQC_FAILED         (Ushort)0x66ff
#elif __thumb
 #define     DECLINE_CDA_ARQC_FAILED         (Ushort)0xff85
#endif
#define     E_CAPK_FILE_EXPIRED             (Ushort)0xff86 // 08/March/07 Soumya_G1 Added new return value from Library.
#define     E_CERT_FAILED                   (Ushort)0xff87 //Asha_s2 added new return val from library if data auth fails
#define     E_NO_CAPK_DATA                  (Ushort)0xff88 //Pavan_K1 dated 28-Mar-07 added for the display if the getcapk function pointer is not set.

// Purvin_p1 19/Apr/07 Fix for Ontime Defect# 8151.
// Added a new error code for tags with length greater than 3 bytes
#define E_TAG_LEN_ERR                        (Ushort)0xff89

// Added a new error code for handling the GetNextTLV error in the appendTLVSelection
#define E_GET_TLV_ERR						 (Ushort)0xff90

//CDAMode
#define E_CDA_NOTREQUIRED					 (Ushort)0xff94

//MWR_SECURITY_CHANGES: GETTLV_Changes : New API insufficient buffer return value addition
#define     E_INSUFFICIENT_BUFF_SIZE 		 (Ushort)0xff95 

#define     E_USR_PIN_CANCELLED             512
#define     E_USR_PIN_BYPASSED              513

//BKSPKEY:
#define     E_USR_BACKSPACE_KEY_PRESSED     516
#define		AID_MATCH_FOUND			    	1
#define		AID_BLOCKED					    2
#define		AID_NO_MATCH_FOUND			    0

#define 	E_PERFORM_SECURE_PIN			514
#define 	E_MULTIAPP_PERFORM_SECURE_PIN	515//MULTIAPP_PIN Changes

//SCPINBYPASS:
//Defined two new constants as return values from library to check
//whether the previous PIN is bypassed
#define     E_PREV_PIN_BYPASSED         (Ushort)516
#define     E_PREV_PIN_NOT_BYPASSED		(Ushort)517


//POS_CNCL
#define     E_EXT_PIN_CANCELLED             518
#define     E_EXT_PIN_BYPASSED              519

//MULTIAPP_PIN Changes
// tisha 28 Jun 05
// Added for MultiApp secuer PIN Handling
#define EMV_OFFLINE_PLAIN_TEXT_PIN 'P'
#define EMV_OFFLINE_ENCIPHERED_PIN 'E'
#define EMV_MULTIAPP_ONLINE_PIN	   'O'

#define 	SC_CC_EV                        0x08
#define 	ENTER_KEY_PINBYPASS				0x0D
#define 	F1_KEY_PINBYPASS				0x7A
#define 	F2_KEY_PINBYPASS				0x7B
#define 	F3_KEY_PINBYPASS				0x7C
#define 	F4_KEY_PINBYPASS				0x7D
#define 	F5_KEY_PINBYPASS				0x61
#define 	F6_KEY_PINBYPASS				0x62
#define 	F7_KEY_PINBYPASS				0x63
#define 	F8_KEY_PINBYPASS				0x64
#define 	CLEAR_KEY_PINBYPASS				0x08

#define     CHECK_ICC_RESPONSE              (Ushort)0x910a
#define     IS_VELOCITY_CHECK_REQD          (Ushort)0x910b
#define     RESET_CHECK_ICC_PTR             (Ushort)0x910c
#define     RESET_VELOCITY_CHECK_PTR        (Ushort)0x910d
#define     E_WRONG_FUNCTION_CODE           (Ushort)0x910e
#define     E_WRONG_FUNCTION_POINTER        (Ushort)0x910f
#define     APPL_TXN_CONTINUE               (Ushort)0x9111  
#define     VEL_CHECK_REQD					(Ushort)0x9112
#define     VEL_CHECK_NOT_REQD				(Ushort)0x9113
#define     APPL_SELECT						(Ushort)0x9114
#define     RESET_APPL_SELECT_PTR           (Ushort)0x9115


//Pavan_K1 dated 26-Feb-07 added the following macros for the common module implementation.

#define     GET_USER_PIN                    (Ushort)0x9116
#define     GET_LAST_TXN_AMT                (Ushort)0x9117
#define     PERFORM_ISS_ACQ_CVM             (Ushort)0x9118
#define     PERFORM_SIGNATURE               (Ushort)0x9119
#define     PERFORM_ONLINE_PIN              (Ushort)0x911a
#define     DISPLAY_ERROR_PROMPT            (Ushort)0x911b
#define     IS_CARD_BLACKLISTED             (Ushort)0x911c
#define     GET_TXN_AMT                     (Ushort)0x911d
#define     IS_CSN_VALID                    (Ushort)0x911e
#define     DISPLAY_PIN_PROMPT              (Ushort)0x911f
#define     GET_TERMINAL_PARAMETER          (Ushort)0x9120
#define     GET_CAPK_DATA                   (Ushort)0x9121
#define     IS_PARTIAL_SELECT_ALLOWED       (Ushort)0x9122


//Pavan_K1 dated 08-May-07 added to represent the Transaction states.

#define     INIT_STATE						(Ushort)0x9123
#define     PSE_SEL_STATE					(Ushort)0x9124
#define     DIR_SEL_STATE					(Ushort)0x9125
#define     FINAL_SEL_STATE					(Ushort)0x9126
#define     RD_REC_PSE_STATE				(Ushort)0x9127
#define     RD_AP_DATA_STATE				(Ushort)0x9128
#define     GET_PROC_STATE 					(Ushort)0x9129
#define 	INT_AUTH_STATE					(Ushort)0x912a
#define 	GEN_AC1_STATE					(Ushort)0x912b
#define 	GEN_AC2_STATE					(Ushort)0x912c
//kishor_s1 added 4-4-08 for Mx-->Vx porting changes
//#define		CDA_GEN_AC_STATE				(Ushort)0x912d
//CDAFRPIS: Introduced a new state for CDA
#define		CDA_GEN_AC_STATE				(Ushort)0x9130

//Pavan_K1 dated 14-May-07 added the below 3 macros for handling the Account type selection
//after select application.

#define 	AMT_ACCT_TYPE_REQD 				(Ushort)0x912d
#define     ACCT_TYPE_REQD 					(Ushort)0x912e
#define     AMT_REQD	 					(Ushort)0x912f
// Tisha_K1 11/July/07
// AMTCLBK: New IDs added for the amount entry callback API
#define     AMT_CASHBACK_ACCT_TYPE_REQD     (Ushort)0x921a
#define    	CASHBACK_ACCT_TYPE_REQD         (Ushort)0x921b
#define     AMT_CASHBACK_REQD               (Ushort)0x921c
#define  	CASHBACK_REQD                   (Ushort)0x921d


#define     DF_NOT_EQUAL					0
#define		DF_EQUAL						1
#define     DF_EQUAL_FAIL					2
#define     DF_GREATER						3

//Clarify ID : 080715-551
//Kamlesh_k1 16 June 2009 : Fix made to handle more than one AID
//A new return value added in the API EMVSelection::getAIDStatus

#define	EMV_MORE_DATA				(Ushort)0x9133

//INVMOD:
#define		E_INVALID_MODULE			    (Ushort)0xff55


// Tisha_K1 - 25/Mar/2010
// SU76 : Language selection should be done before GPO. 		
#define		PERFORM_LANGUAGE_SEL			(Ushort)0x9134

//MULTIAVN:
#define		GET_TERMINAL_AVN			(Ushort)0x9135

//ExtAuth6985:
#define		Get_EXT_AUTH_DECISION  		(Ushort)0x9136

//GDFix:
#define		GD_CHECK  		(Ushort)0x9137

// POS_CNCL
#define     GET_POS_CVM_DECISION (Ushort)0x9138
#define     EXT_PIN_CNCL		 (Ushort)0x9139
#define		EXT_PIN_BYPASS       (Ushort)0x9140
#define		STOP_POLL_EXT_DESCN	 (Ushort)0x9141

// SPAIN_REQ
#define		GET_BYPASS_DECISION	(Ushort)0x9142
#define 	PIN_MANDATORY		 (Ushort)0x9143


#endif //_EMVRESULT


