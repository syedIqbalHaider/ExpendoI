/*
 * FILENAME       : VXEMVAP_confproto.h
 * PRODUCT        : Verix/Vx.
 * VERSION        : 4.0.0
 * AUTHOR(s)      : Pavan_k1
 * CREATED        : 12/Dec/06
 *
 * DESCRIPTION    : 
 *
 * CONTENTS   :
 * MODIFICATION HISTORY    :
 *
 * #    Date        Who         History
 * ---  --------    ----------  ----------------------------------------
 * 1    12 Dec 06   Pavan_k1    Created for Vx EMV Module 4.0.0
 * 2    08 Aug 07   Pavan_K1    KEYREVCN: Added a new function prototype inUpdateESTRecVal
 *
 * Copyright (C) 2006 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language, in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual or otherwise, without the prior written
 * permission of VeriFone Inc.
 *                                                2099 Gateway Place
 *                                                  Suite 600
 *                                                  San Jose  CA  95110 
 *                                                  USA
 *
 *
 *
 */
#ifndef CONFIG_PROTOTYPE_ALL
#define CONFIG_PROTOTYPE_ALL

//NAND_FLASH
	int    inLoadAllESTRecords(void);



    int     inLoadESTRec (int inRecNumber);
    void    *pvdGetESTRec(void);
    void    registerEST(void *ptr);
    int     inLoadESTRecFromStruct (int inRecNumber, void *vdStruct);
    void    vdRestoreESTRecFromStruct(void *vdStruct);
    void    vdCopyESTRecFromStruct(void *vdStruct);
    int     inSaveESTRec (int inRecNumber);
    int     inSaveESTRecFromStruct (int inRecNumber,  void *vdStruct);
    int     inGetESTRecNumber (void);
	//KEYREVCN: Pavan_K1 added the below inUpdateESTRecVal prototype
	//on 08-Aug-07 for keyrevocation implementation.
	int     inUpdateESTRecVal (int inFldNum);
	
    int     inUpdateESTRec (void);
    int     inUpdateESTRecFromStruct (void *vdStruct);
    void    vdResetESTRec (void);
    long    lnGetESTNbrOfTransactions(void);
    void    vdSetESTNbrOfTransactions(long lnNbrOfTransactions);
    void    vdSetESTNbrOfTransactionsFromStruct(long lnNbrOfTransactions, void*vdStruct);
    void    szGetESTSchemeLabel(char*param);
    void    vdSetESTSchemeLabel(char* szSchemeLabel);
    void    vdSetESTSchemeLabelFromStruct(char* szSchemeLabel, void*vdStruct);
    void    szGetESTRID(char*param);
    void    vdSetESTRID(char* szRID);
    void    vdSetESTRIDFromStruct(char* szRID, void*vdStruct);
    void    szGetESTCSNList(char*param);
    void    vdSetESTCSNList(char* szCSNList);
    void    vdSetESTCSNListFromStruct(char* szCSNList, void*vdStruct);
    short   inGetESTPublicKeyIndex1(void);
    void    vdSetESTPublicKeyIndex1(short inPublicKeyIndex1);
    void    vdSetESTPublicKeyIndex1FromStruct(short inPublicKeyIndex1, void*vdStruct);
    void    szGetESTCAPKFile1(char*param);
    void    vdSetESTCAPKFile1(char* szCAPKFile1);
    void    vdSetESTCAPKFile1FromStruct(char* szCAPKFile1, void*vdStruct);
    void    szGetESTCAPKExpDate1(char*param);
    void    vdSetESTCAPKExpDate1(char* szCAPKExpDate1);
    void    vdSetESTCAPKExpDate1FromStruct(char* szCAPKExpDate1, void*vdStruct);
    short   inGetESTPublicKeyIndex2(void);
    void    vdSetESTPublicKeyIndex2(short inPublicKeyIndex2);
    void    vdSetESTPublicKeyIndex2FromStruct(short inPublicKeyIndex2, void*vdStruct);
    void    szGetESTCAPKFile2(char*param);
    void    vdSetESTCAPKFile2(char* szCAPKFile2);
    void    vdSetESTCAPKFile2FromStruct(char* szCAPKFile2, void*vdStruct);
    void    szGetESTCAPKExpDate2(char*param);
    void    vdSetESTCAPKExpDate2(char* szCAPKExpDate2);
    void    vdSetESTCAPKExpDate2FromStruct(char* szCAPKExpDate2, void*vdStruct);
    short   inGetESTPublicKeyIndex3(void);
    void    vdSetESTPublicKeyIndex3(short inPublicKeyIndex3);
    void    vdSetESTPublicKeyIndex3FromStruct(short inPublicKeyIndex3, void*vdStruct);
    void    szGetESTCAPKFile3(char*param);
    void    vdSetESTCAPKFile3(char* szCAPKFile3);
    void    vdSetESTCAPKFile3FromStruct(char* szCAPKFile3, void*vdStruct);
    void    szGetESTCAPKExpDate3(char*param);
    void    vdSetESTCAPKExpDate3(char* szCAPKExpDate3);
    void    vdSetESTCAPKExpDate3FromStruct(char* szCAPKExpDate3, void*vdStruct);
    short   inGetESTPublicKeyIndex4(void);
    void    vdSetESTPublicKeyIndex4(short inPublicKeyIndex4);
    void    vdSetESTPublicKeyIndex4FromStruct(short inPublicKeyIndex4, void*vdStruct);
    void    szGetESTCAPKFile4(char*param);
    void    vdSetESTCAPKFile4(char* szCAPKFile4);
    void    vdSetESTCAPKFile4FromStruct(char* szCAPKFile4, void*vdStruct);
    void    szGetESTCAPKExpDate4(char*param);
    void    vdSetESTCAPKExpDate4(char* szCAPKExpDate4);
    void    vdSetESTCAPKExpDate4FromStruct(char* szCAPKExpDate4, void*vdStruct);
    short   inGetESTPublicKeyIndex5(void);
    void    vdSetESTPublicKeyIndex5(short inPublicKeyIndex5);
    void    vdSetESTPublicKeyIndex5FromStruct(short inPublicKeyIndex5, void*vdStruct);
    void    szGetESTCAPKFile5(char*param);
    void    vdSetESTCAPKFile5(char* szCAPKFile5);
    void    vdSetESTCAPKFile5FromStruct(char* szCAPKFile5, void*vdStruct);
    void    szGetESTCAPKExpDate5(char*param);
    void    vdSetESTCAPKExpDate5(char* szCAPKExpDate5);
    void    vdSetESTCAPKExpDate5FromStruct(char* szCAPKExpDate5, void*vdStruct);

    short   inGetESTPublicKeyIndex11(void);
    void    vdSetESTPublicKeyIndex11(short inPublicKeyIndex11);
    void    vdSetESTPublicKeyIndex11FromStruct(short inPublicKeyIndex11, void*vdStruct);
    void    szGetESTCAPKFile11(char*param);
    void    vdSetESTCAPKFile11(char* szCAPKFile11);
    void    vdSetESTCAPKFile11FromStruct(char* szCAPKFile11, void*vdStruct);
    void    szGetESTCAPKExpDate11(char*param);
    void    vdSetESTCAPKExpDate11(char* szCAPKExpDate11);
    void    vdSetESTCAPKExpDate11FromStruct(char* szCAPKExpDate11, void*vdStruct);
    short   inGetESTPublicKeyIndex12(void);
    void    vdSetESTPublicKeyIndex12(short inPublicKeyIndex12);
    void    vdSetESTPublicKeyIndex12FromStruct(short inPublicKeyIndex12, void*vdStruct);
    void    szGetESTCAPKFile12(char*param);
    void    vdSetESTCAPKFile12(char* szCAPKFile12);
    void    vdSetESTCAPKFile12FromStruct(char* szCAPKFile12, void*vdStruct);
    void    szGetESTCAPKExpDate12(char*param);
    void    vdSetESTCAPKExpDate12(char* szCAPKExpDate12);
    void    vdSetESTCAPKExpDate12FromStruct(char* szCAPKExpDate12, void*vdStruct);
    short   inGetESTPublicKeyIndex13(void);
    void    vdSetESTPublicKeyIndex13(short inPublicKeyIndex13);
    void    vdSetESTPublicKeyIndex13FromStruct(short inPublicKeyIndex13, void*vdStruct);
    void    szGetESTCAPKFile13(char*param);
    void    vdSetESTCAPKFile13(char* szCAPKFile13);
    void    vdSetESTCAPKFile13FromStruct(char* szCAPKFile13, void*vdStruct);
    void    szGetESTCAPKExpDate13(char*param);
    void    vdSetESTCAPKExpDate13(char* szCAPKExpDate13);
    void    vdSetESTCAPKExpDate13FromStruct(char* szCAPKExpDate13, void*vdStruct);
    short   inGetESTPublicKeyIndex14(void);
    void    vdSetESTPublicKeyIndex14(short inPublicKeyIndex14);
    void    vdSetESTPublicKeyIndex14FromStruct(short inPublicKeyIndex14, void*vdStruct);
    void    szGetESTCAPKFile14(char*param);
    void    vdSetESTCAPKFile14(char* szCAPKFile14);
    void    vdSetESTCAPKFile14FromStruct(char* szCAPKFile14, void*vdStruct);
    void    szGetESTCAPKExpDate14(char*param);
    void    vdSetESTCAPKExpDate14(char* szCAPKExpDate14);
    void    vdSetESTCAPKExpDate14FromStruct(char* szCAPKExpDate14, void*vdStruct);
    short   inGetESTPublicKeyIndex15(void);
    void    vdSetESTPublicKeyIndex15(short inPublicKeyIndex15);
    void    vdSetESTPublicKeyIndex15FromStruct(short inPublicKeyIndex15, void*vdStruct);
    void    szGetESTCAPKFile15(char*param);
    void    vdSetESTCAPKFile15(char* szCAPKFile15);
    void    vdSetESTCAPKFile15FromStruct(char* szCAPKFile15, void*vdStruct);
    void    szGetESTCAPKExpDate15(char*param);
    void    vdSetESTCAPKExpDate15(char* szCAPKExpDate15);
    void    vdSetESTCAPKExpDate15FromStruct(char* szCAPKExpDate15, void*vdStruct);
    short   inGetESTPublicKeyIndex6(void);
    void    vdSetESTPublicKeyIndex6(short inPublicKeyIndex6);
    void    vdSetESTPublicKeyIndex6FromStruct(short inPublicKeyIndex6, void*vdStruct);
    void    szGetESTCAPKFile6(char*param);
    void    vdSetESTCAPKFile6(char* szCAPKFile6);
    void    vdSetESTCAPKFile6FromStruct(char* szCAPKFile6, void*vdStruct);
    void    szGetESTCAPKExpDate6(char*param);
    void    vdSetESTCAPKExpDate6(char* szCAPKExpDate6);
    void    vdSetESTCAPKExpDate6FromStruct(char* szCAPKExpDate6, void*vdStruct);
    short   inGetESTPublicKeyIndex7(void);
    void    vdSetESTPublicKeyIndex7(short inPublicKeyIndex7);
    void    vdSetESTPublicKeyIndex7FromStruct(short inPublicKeyIndex7, void*vdStruct);
    void    szGetESTCAPKFile7(char*param);
    void    vdSetESTCAPKFile7(char* szCAPKFile7);
    void    vdSetESTCAPKFile7FromStruct(char* szCAPKFile7, void*vdStruct);
    void    szGetESTCAPKExpDate7(char*param);
    void    vdSetESTCAPKExpDate7(char* szCAPKExpDate7);
    void    vdSetESTCAPKExpDate7FromStruct(char* szCAPKExpDate7, void*vdStruct);
    short   inGetESTPublicKeyIndex8(void);
    void    vdSetESTPublicKeyIndex8(short inPublicKeyIndex8);
    void    vdSetESTPublicKeyIndex8FromStruct(short inPublicKeyIndex8, void*vdStruct);
    void    szGetESTCAPKFile8(char*param);
    void    vdSetESTCAPKFile8(char* szCAPKFile8);
    void    vdSetESTCAPKFile8FromStruct(char* szCAPKFile8, void*vdStruct);
    void    szGetESTCAPKExpDate8(char*param);
    void    vdSetESTCAPKExpDate8(char* szCAPKExpDate8);
    void    vdSetESTCAPKExpDate8FromStruct(char* szCAPKExpDate8, void*vdStruct);
    short   inGetESTPublicKeyIndex9(void);
    void    vdSetESTPublicKeyIndex9(short inPublicKeyIndex9);
    void    vdSetESTPublicKeyIndex9FromStruct(short inPublicKeyIndex9, void*vdStruct);
    void    szGetESTCAPKFile9(char*param);
    void    vdSetESTCAPKFile9(char* szCAPKFile9);
    void    vdSetESTCAPKFile9FromStruct(char* szCAPKFile9, void*vdStruct);
    void    szGetESTCAPKExpDate9(char*param);
    void    vdSetESTCAPKExpDate9(char* szCAPKExpDate9);
    void    vdSetESTCAPKExpDate9FromStruct(char* szCAPKExpDate9, void*vdStruct);
    short   inGetESTPublicKeyIndex10(void);
    void    vdSetESTPublicKeyIndex10(short inPublicKeyIndex10);
    void    vdSetESTPublicKeyIndex10FromStruct(short inPublicKeyIndex10, void*vdStruct);
    void    szGetESTCAPKFile10(char*param);
    void    vdSetESTCAPKFile10(char* szCAPKFile10);
    void    vdSetESTCAPKFile10FromStruct(char* szCAPKFile10, void*vdStruct);
    void    szGetESTCAPKExpDate10(char*param);
    void    vdSetESTCAPKExpDate10(char* szCAPKExpDate10);
    void    vdSetESTCAPKExpDate10FromStruct(char* szCAPKExpDate10, void*vdStruct);
    void    szGetESTSupportedAID1(char*param);
    void    vdSetESTSupportedAID1(char* szSupportedAID1);
    void    vdSetESTSupportedAID1FromStruct(char* szSupportedAID1, void*vdStruct);
    short   inGetESTPartialNameAllowedFlag1(void);
    void    vdSetESTPartialNameAllowedFlag1(short inPartialNameAllowedFlag1);
    void    vdSetESTPartialNameAllowedFlag1FromStruct(short inPartialNameAllowedFlag1, void*vdStruct);
    void    szGetESTTermAVN1(char*param);
    void    vdSetESTTermAVN1(char* szTermAVN1);
    void    vdSetESTTermAVN1FromStruct(char* szTermAVN1, void*vdStruct);
    void    szGetESTSecondTermAVN1(char*param);
    void    vdSetESTSecondTermAVN1(char* szSecondTermAVN1);
    void    vdSetESTSecondTermAVN1FromStruct(char* szSecondTermAVN1, void*vdStruct);
    void    szGetESTSupportedAID2(char*param);
    void    vdSetESTSupportedAID2(char* szSupportedAID2);
    void    vdSetESTSupportedAID2FromStruct(char* szSupportedAID2, void*vdStruct);
    short   inGetESTPartialNameAllowedFlag2(void);
    void    vdSetESTPartialNameAllowedFlag2(short inPartialNameAllowedFlag2);
    void    vdSetESTPartialNameAllowedFlag2FromStruct(short inPartialNameAllowedFlag2, void*vdStruct);
    void    szGetESTTermAVN2(char*param);
    void    vdSetESTTermAVN2(char* szTermAVN2);
    void    vdSetESTTermAVN2FromStruct(char* szTermAVN2, void*vdStruct);
    void    szGetESTSecondTermAVN2(char*param);
    void    vdSetESTSecondTermAVN2(char* szSecondTermAVN2);
    void    vdSetESTSecondTermAVN2FromStruct(char* szSecondTermAVN2, void*vdStruct);
    void    szGetESTSupportedAID3(char*param);
    void    vdSetESTSupportedAID3(char* szSupportedAID3);
    void    vdSetESTSupportedAID3FromStruct(char* szSupportedAID3, void*vdStruct);
    short   inGetESTPartialNameAllowedFlag3(void);
    void    vdSetESTPartialNameAllowedFlag3(short inPartialNameAllowedFlag3);
    void    vdSetESTPartialNameAllowedFlag3FromStruct(short inPartialNameAllowedFlag3, void*vdStruct);
    void    szGetESTTermAVN3(char*param);
    void    vdSetESTTermAVN3(char* szTermAVN3);
    void    vdSetESTTermAVN3FromStruct(char* szTermAVN3, void*vdStruct);
    void    szGetESTSecondTermAVN3(char*param);
    void    vdSetESTSecondTermAVN3(char* szSecondTermAVN3);
    void    vdSetESTSecondTermAVN3FromStruct(char* szSecondTermAVN3, void*vdStruct);
    void    szGetESTSupportedAID4(char*param);
    void    vdSetESTSupportedAID4(char* szSupportedAID4);
    void    vdSetESTSupportedAID4FromStruct(char* szSupportedAID4, void*vdStruct);
    short   inGetESTPartialNameAllowedFlag4(void);
    void    vdSetESTPartialNameAllowedFlag4(short inPartialNameAllowedFlag4);
    void    vdSetESTPartialNameAllowedFlag4FromStruct(short inPartialNameAllowedFlag4, void*vdStruct);
    void    szGetESTTermAVN4(char*param);
    void    vdSetESTTermAVN4(char* szTermAVN4);
    void    vdSetESTTermAVN4FromStruct(char* szTermAVN4, void*vdStruct);
    void    szGetESTSecondTermAVN4(char*param);
    void    vdSetESTSecondTermAVN4(char* szSecondTermAVN4);
    void    vdSetESTSecondTermAVN4FromStruct(char* szSecondTermAVN4, void*vdStruct);
    void    szGetESTSupportedAID5(char*param);
    void    vdSetESTSupportedAID5(char* szSupportedAID5);
    void    vdSetESTSupportedAID5FromStruct(char* szSupportedAID5, void*vdStruct);
    short   inGetESTPartialNameAllowedFlag5(void);
    void    vdSetESTPartialNameAllowedFlag5(short inPartialNameAllowedFlag5);
    void    vdSetESTPartialNameAllowedFlag5FromStruct(short inPartialNameAllowedFlag5, void*vdStruct);
    void    szGetESTTermAVN5(char*param);
    void    vdSetESTTermAVN5(char* szTermAVN5);
    void    vdSetESTTermAVN5FromStruct(char* szTermAVN5, void*vdStruct);
    void    szGetESTSecondTermAVN5(char*param);
    void    vdSetESTSecondTermAVN5(char* szSecondTermAVN5);
    void    vdSetESTSecondTermAVN5FromStruct(char* szSecondTermAVN5, void*vdStruct);
    void    szGetESTSupportedAID6(char*param);
    void    vdSetESTSupportedAID6(char* szSupportedAID6);
    void    vdSetESTSupportedAID6FromStruct(char* szSupportedAID6, void*vdStruct);
    short   inGetESTPartialNameAllowedFlag6(void);
    void    vdSetESTPartialNameAllowedFlag6(short inPartialNameAllowedFlag6);
    void    vdSetESTPartialNameAllowedFlag6FromStruct(short inPartialNameAllowedFlag6, void*vdStruct);
    void    szGetESTTermAVN6(char*param);
    void    vdSetESTTermAVN6(char* szTermAVN6);
    void    vdSetESTTermAVN6FromStruct(char* szTermAVN6, void*vdStruct);
    void    szGetESTSecondTermAVN6(char*param);
    void    vdSetESTSecondTermAVN6(char* szSecondTermAVN6);
    void    vdSetESTSecondTermAVN6FromStruct(char* szSecondTermAVN6, void*vdStruct);
    void    szGetESTSupportedAID7(char*param);
    void    vdSetESTSupportedAID7(char* szSupportedAID7);
    void    vdSetESTSupportedAID7FromStruct(char* szSupportedAID7, void*vdStruct);
    short   inGetESTPartialNameAllowedFlag7(void);
    void    vdSetESTPartialNameAllowedFlag7(short inPartialNameAllowedFlag7);
    void    vdSetESTPartialNameAllowedFlag7FromStruct(short inPartialNameAllowedFlag7, void*vdStruct);
    void    szGetESTTermAVN7(char*param);
    void    vdSetESTTermAVN7(char* szTermAVN7);
    void    vdSetESTTermAVN7FromStruct(char* szTermAVN7, void*vdStruct);
    void    szGetESTSecondTermAVN7(char*param);
    void    vdSetESTSecondTermAVN7(char* szSecondTermAVN7);
    void    vdSetESTSecondTermAVN7FromStruct(char* szSecondTermAVN7, void*vdStruct);
    void    szGetESTSupportedAID8(char*param);
    void    vdSetESTSupportedAID8(char* szSupportedAID8);
    void    vdSetESTSupportedAID8FromStruct(char* szSupportedAID8, void*vdStruct);
    short   inGetESTPartialNameAllowedFlag8(void);
    void    vdSetESTPartialNameAllowedFlag8(short inPartialNameAllowedFlag8);
    void    vdSetESTPartialNameAllowedFlag8FromStruct(short inPartialNameAllowedFlag8, void*vdStruct);
    void    szGetESTTermAVN8(char*param);
    void    vdSetESTTermAVN8(char* szTermAVN8);
    void    vdSetESTTermAVN8FromStruct(char* szTermAVN8, void*vdStruct);
    void    szGetESTSecondTermAVN8(char*param);
    void    vdSetESTSecondTermAVN8(char* szSecondTermAVN8);
    void    vdSetESTSecondTermAVN8FromStruct(char* szSecondTermAVN8, void*vdStruct);
    void    szGetESTSupportedAID9(char*param);
    void    vdSetESTSupportedAID9(char* szSupportedAID9);
    void    vdSetESTSupportedAID9FromStruct(char* szSupportedAID9, void*vdStruct);
    short   inGetESTPartialNameAllowedFlag9(void);
    void    vdSetESTPartialNameAllowedFlag9(short inPartialNameAllowedFlag9);
    void    vdSetESTPartialNameAllowedFlag9FromStruct(short inPartialNameAllowedFlag9, void*vdStruct);
    void    szGetESTTermAVN9(char*param);
    void    vdSetESTTermAVN9(char* szTermAVN9);
    void    vdSetESTTermAVN9FromStruct(char* szTermAVN9, void*vdStruct);
    void    szGetESTSecondTermAVN9(char*param);
    void    vdSetESTSecondTermAVN9(char* szSecondTermAVN9);
    void    vdSetESTSecondTermAVN9FromStruct(char* szSecondTermAVN9, void*vdStruct);
    void    szGetESTSupportedAID10(char*param);
    void    vdSetESTSupportedAID10(char* szSupportedAID10);
    void    vdSetESTSupportedAID10FromStruct(char* szSupportedAID10, void*vdStruct);
    short   inGetESTPartialNameAllowedFlag10(void);
    void    vdSetESTPartialNameAllowedFlag10(short inPartialNameAllowedFlag10);
    void    vdSetESTPartialNameAllowedFlag10FromStruct(short inPartialNameAllowedFlag10, void*vdStruct);
    void    szGetESTTermAVN10(char*param);
    void    vdSetESTTermAVN10(char* szTermAVN10);
    void    vdSetESTTermAVN10FromStruct(char* szTermAVN10, void*vdStruct);
    void    szGetESTSecondTermAVN10(char*param);
    void    vdSetESTSecondTermAVN10(char* szSecondTermAVN10);
    void    vdSetESTSecondTermAVN10FromStruct(char* szSecondTermAVN10, void*vdStruct);
    short   inGetESTEMVTableRecord(void);
    void    vdSetESTEMVTableRecord(short inEMVTableRecord);
    void    vdSetESTEMVTableRecordFromStruct(short inEMVTableRecord, void*vdStruct);
    void    szGetESTCSNListFile(char*param);
    void    vdSetESTCSNListFile(char* szCSNListFile);
    void    vdSetESTCSNListFileFromStruct(char* szCSNListFile, void*vdStruct);
    int     inLoadMVTRec (int inRecNumber);
    void    *pvdGetMVTRec(void);
    void    registerMVT(void *ptr);
    int     inLoadMVTRecFromStruct (int inRecNumber, void *vdStruct);
    void    vdRestoreMVTRecFromStruct(void *vdStruct);
    void    vdCopyMVTRecFromStruct(void *vdStruct);
    int     inSaveMVTRec (int inRecNumber);
    int     inSaveMVTRecFromStruct (int inRecNumber,  void *vdStruct);
    int     inGetMVTRecNumber (void);
    int     inUpdateMVTRec (void);
    int     inUpdateMVTRecFromStruct (void *vdStruct);
    void    vdResetMVTRec (void);
    short   inGetSchemeReference(void);
    short   inGetSchemeReferenceFromStruct(void*vdStruct);
    void    vdSetSchemeReference(short inSchemeReference);
    void    vdSetSchemeReferenceFromStruct(short inSchemeReference, void*vdStruct);
    short   inGetIssuerReference(void);
    short   inGetIssuerReferenceFromStruct(void*vdStruct);
    void    vdSetIssuerReference(short inIssuerReference);
    void    vdSetIssuerReferenceFromStruct(short inIssuerReference, void*vdStruct);
    short   inGetTRMDataPresent(void);
    short   inGetTRMDataPresentFromStruct(void*vdStruct);
    void    vdSetTRMDataPresent(short inTRMDataPresent);
    void    vdSetTRMDataPresentFromStruct(short inTRMDataPresent, void*vdStruct);
    long    lnGetEMVFloorLimit(void);
    long    lnGetEMVFloorLimitFromStruct(void*vdStruct);
    void    vdSetEMVFloorLimit(long lnEMVFloorLimit);
    void    vdSetEMVFloorLimitFromStruct(long lnEMVFloorLimit, void*vdStruct);
    long    lnGetEMVRSThreshold(void);
    long    lnGetEMVRSThresholdFromStruct(void*vdStruct);
    void    vdSetEMVRSThreshold(long lnEMVRSThreshold);
    void    vdSetEMVRSThresholdFromStruct(long lnEMVRSThreshold, void*vdStruct);
    short   inGetEMVTargetRSPercent(void);
    short   inGetEMVTargetRSPercentFromStruct(void*vdStruct);
    void    vdSetEMVTargetRSPercent(short inEMVTargetRSPercent);
    void    vdSetEMVTargetRSPercentFromStruct(short inEMVTargetRSPercent, void*vdStruct);
    short   inGetEMVMaxTargetRSPercent(void);
    short   inGetEMVMaxTargetRSPercentFromStruct(void*vdStruct);
    void    vdSetEMVMaxTargetRSPercent(short inEMVMaxTargetRSPercent);
    void    vdSetEMVMaxTargetRSPercentFromStruct(short inEMVMaxTargetRSPercent, void*vdStruct);
    char*   szGetEMVTACDefault(void);
    char*   szGetEMVTACDefaultFromStruct(void*vdStruct);
    void    vdSetEMVTACDefault(char* szEMVTACDefault);
    void    vdSetEMVTACDefaultFromStruct(char* szEMVTACDefault, void*vdStruct);
    char*   szGetEMVTACDenial(void);
    char*   szGetEMVTACDenialFromStruct(void*vdStruct);
    void    vdSetEMVTACDenial(char* szEMVTACDenial);
    void    vdSetEMVTACDenialFromStruct(char* szEMVTACDenial, void*vdStruct);
    char*   szGetEMVTACOnline(void);
    char*   szGetEMVTACOnlineFromStruct(void*vdStruct);
    void    vdSetEMVTACOnline(char* szEMVTACOnline);
    void    vdSetEMVTACOnlineFromStruct(char* szEMVTACOnline, void*vdStruct);
    char*   szGetDefaultTDOL(void);
    char*   szGetDefaultTDOLFromStruct(void*vdStruct);
    void    vdSetDefaultTDOL(char* szDefaultTDOL);
    void    vdSetDefaultTDOLFromStruct(char* szDefaultTDOL, void*vdStruct);
    char*   szGetDefaultDDOL(void);
    char*   szGetDefaultDDOLFromStruct(void*vdStruct);
    void    vdSetDefaultDDOL(char* szDefaultDDOL);
    void    vdSetDefaultDDOLFromStruct(char* szDefaultDDOL, void*vdStruct);
    short   inGetNextRecord(void);
    short   inGetNextRecordFromStruct(void*vdStruct);
    void    vdSetNextRecord(short inNextRecord);
    void    vdSetNextRecordFromStruct(short inNextRecord, void*vdStruct);
    unsigned long ulGetEMVCounter(void);
    unsigned long ulGetEMVCounterFromStruct(void*vdStruct);
    void    vdSetEMVCounter(unsigned long ulEMVCounter);
    void    vdSetEMVCounterFromStruct(unsigned long ulEMVCounter, void*vdStruct);
    char*   szGetEMVTermCountryCode(void);
    char*   szGetEMVTermCountryCodeFromStruct(void*vdStruct);
    void    vdSetEMVTermCountryCode(char* szEMVTermCountryCode);
    void    vdSetEMVTermCountryCodeFromStruct(char* szEMVTermCountryCode, void*vdStruct);
    char*   szGetEMVTermCurrencyCode(void);
    char*   szGetEMVTermCurrencyCodeFromStruct(void*vdStruct);
    void    vdSetEMVTermCurrencyCode(char* szEMVTermCurrencyCode);
    void    vdSetEMVTermCurrencyCodeFromStruct(char* szEMVTermCurrencyCode, void*vdStruct);
    char*   szGetEMVTermCapabilities(void);
    char*   szGetEMVTermCapabilitiesFromStruct(void*vdStruct);
    void    vdSetEMVTermCapabilities(char* szEMVTermCapabilities);
    void    vdSetEMVTermCapabilitiesFromStruct(char* szEMVTermCapabilities, void*vdStruct);
    char*   szGetEMVTermAddCapabilities(void);
    char*   szGetEMVTermAddCapabilitiesFromStruct(void*vdStruct);
    void    vdSetEMVTermAddCapabilities(char* szEMVTermAddCapabilities);
    void    vdSetEMVTermAddCapabilitiesFromStruct(char* szEMVTermAddCapabilities, void*vdStruct);
    char*   szGetEMVTermType(void);
    char*   szGetEMVTermTypeFromStruct(void*vdStruct);
    void    vdSetEMVTermType(char* szEMVTermType);
    void    vdSetEMVTermTypeFromStruct(char* szEMVTermType, void*vdStruct);

    char*   szGetEMVMerchantCategoryCode(void);
    char*   szGetEMVMerchantCategoryCodeFromStruct(void*vdStruct);
    void    vdSetEMVMerchantCategoryCode(char* szEMVMerchantCategoryCode);
    void    vdSetEMVMerchantCategoryCodeFromStruct(char* szEMVMerchantCategoryCode, void*vdStruct);

    short   inGetMerchantForcedOnlineFlag(void);
    void    vdSetMerchantForcedOnlineFlag(short inForceOnline);
    short   inGetBlackListedCardSupportFlag(void);
    void    vdSetBlackListedCardSupportFlag(short inBlackListedCardSupport);

    char*   szGetEMVTerminalCategoryCode(void);
    char*   szGetEMVTerminalCategoryCodeFromStruct(void*vdStruct);
    void    vdSetEMVTerminalCategoryCode(char* szEMVTerminalCategoryCode);
    void    vdSetEMVTerminalCategoryCodeFromStruct(char* szEMVTerminalCategoryCode, void*vdStruct);

    short   inGetFallbackAllowedFlag(void);
    void    vdSetFallbackAllowedFlag(short inFallbackAllowed);
    short   inGetAutoSelectApplnFlag(void);
    void    vdSetAutoSelectApplnFlag(short inAutoSelectAppl);
    short   inGetTermCurExp(void);
    void    vdSetTermCurExp(short inEMVTermCurExp);

    short   inGetFallbackAllowedFlagFromStruct(void*vdStruct);
    void    vdSetFallbackAllowedFlagFromStruct(short inEMVFallbackAllowed, void*vdStruct);
    short   inGetAutoSelectApplnFlagFromStruct(void*vdStruct);
    void    vdSetAutoSelectApplnFlagFromStruct(short inEMVAutoSelectAppln, void*vdStruct);
    short   inGetTermCurExpFromStruct(void*vdStruct);
    void    vdSetTermCurExpFromStruct(short inEMVTermCurExp, void*vdStruct);

    short   inGetShortRFU1(void);
    void    vdSetShortRFU1(short inRFU1);
    short   inGetShortRFU2(void);
    void    vdSetShortRFU2(short inRFU2);
    short   inGetShortRFU3(void);
    void    vdSetShortRFU3(short inRFU3);

    short   inGetShortRFU1FromStruct(void*vdStruct);
    void    vdSetShortRFU1FromStruct(short shRFU1, void*vdStruct);
    short   inGetShortRFU2FromStruct(void*vdStruct);
    void    vdSetShortRFU2FromStruct(short shRFU2, void*vdStruct);
    short   inGetShortRFU3FromStruct(void*vdStruct);
    void    vdSetShortRFU3FromStruct(short shRFU3, void*vdStruct);

    char *  szGetStringRFU1(void);
    void    vdSetStringRFU1(char* inRFU1);
    char *  szGetStringRFU2(void);
    void    vdSetStringRFU2(char* inRFU2);
    char *  szGetStringRFU3(void);
    void    vdSetStringRFU3(char* inRFU3);

    char*   szGetStringRFU1FromStruct(void*vdStruct);
    void    vdSetStringRFU1FromStruct(char* szRFU1, void*vdStruct);
    char*   szGetStringRFU2FromStruct(void*vdStruct);
    void    vdSetStringRFU2FromStruct(char* szRFU2, void*vdStruct);
    char*   szGetStringRFU3FromStruct(void*vdStruct);
    void    vdSetStringRFU3FromStruct(char* szRFU3, void*vdStruct);

    short   inGetForcedOnlineFromStruct(void*vdStruct);
    void    vdSetForcedOnlineFromStruct(short inMerchantForcedOnlineFlag, void*vdStruct);
    short   inGetBlackListedCardSupportFromStruct(void*vdStruct);
    void    vdSetBlackListedCardSupportFromStruct(short inBlackListedCardSupportFlag, void*vdStruct);
    void    vdSetszEMVTerminalCategoryCodeFromStruct(char* szEMVTerminalCategoryCode, void*vdStruct);

    char*   strlcopy (char *szString1, char *szString2, int count);

    void    szGetESTRecAppNameAID1(char*param) ;
    void    vdSetESTRecAppNameAID1(char*param) ;
    void    szGetESTRecAppNameAID2(char*param) ;
    void    vdSetESTRecAppNameAID2(char*param) ;
    void    szGetESTRecAppNameAID3(char*param) ;
    void    vdSetESTRecAppNameAID3(char*param) ;
    void    szGetESTRecAppNameAID4(char*param) ;
    void    vdSetESTRecAppNameAID4(char*param) ;
    void    szGetESTRecAppNameAID5(char*param) ;
    void    vdSetESTRecAppNameAID5(char*param) ;
    void    szGetESTRecAppNameAID6(char*param) ;
    void    vdSetESTRecAppNameAID6(char*param) ;
    void    szGetESTRecAppNameAID7(char*param) ;
    void    vdSetESTRecAppNameAID7(char*param) ;
    void    szGetESTRecAppNameAID8(char*param) ;
    void    vdSetESTRecAppNameAID8(char*param) ;
    void    szGetESTRecAppNameAID9(char*param) ;
    void    vdSetESTRecAppNameAID9(char*param) ;
    void    szGetESTRecAppNameAID10(char*param) ;
    void    vdSetESTRecAppNameAID10(char*param) ;


#endif

