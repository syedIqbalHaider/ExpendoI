/*
 * FILENAME       : common.h
 * PRODUCT        : Verix/Vx.
 * VERSION        : 4.0.0
 * AUTHOR(s)      : Pavan_k1
 * CREATED        : 07/Dec/06
 *
 * @Description	: This file has external function declarations.
 *
 * CONTENTS   :
 * MODIFICATION HISTORY    :
 *
 * #    Date        Who         History
 * ---  --------    ----------  ----------------------------------------
 * 1    07 Dec 06   Pavan_k1    Created for Vx EMV Module 4.0.0
 * 2    03 Jan 07   t_arvind_g1 Added for debugging facility using LOGSYS
 * 
 * 3    30/Jan/07   Soumya_G1  TC# 2CM.042.06 - Introduced a new return value E_CDA_ARQC_FAILED.
 * 4    23/Feb/07   Soumya_G1  CDA Changes for Bulletin # 44.Added a new constant TC_GEN2AC_APPROVE
 *                             for the library
 * 5    25 Apr 07   Pavan_K1   Removed all the three standard implementation prototypes
 *
 * Copyright (C) 2006 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language, in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual or otherwise, without the prior written
 * permission of VeriFone Inc.
 *                                                2099 Gateway Place
 *												  Suite 600
 *											      San Jose  CA  95110 
 *												  USA
 *
 *
 *
 */

#ifndef _COMMON
#define _COMMON
 
#include "EMVTypeDefs.hpp"

//all definitions
#define PSE_SELECT      		1
#define DIRECT_SELECT   		2

#define CVM_VERIFY_PLAIN      	3

//t_arvind_g1 dated 03 Jan 07 Added for debugging facility using LOGSYS
#define EMVLIBFIL               0x00000001
#define EMVAPP                  "EMV"
#define VXEMVAPFIL              0x00000002

#define TC_GEN2AC_APPROVE       "Y3"

#ifdef  __cplusplus
extern "C" {
#endif

//asha_s2 dated 26 Feb 07, removed GEN_CDDAAAC since this is not reqd.
//enum cryptogram{ GEN_AAR = 0xC0, GEN_ARQC = 0x80, GEN_TC = 0x40, GEN_AAC = 0x00, GEN_CDDAAAC = 0x10, GEN_CDDATC = 0x50, GEN_CDDAARQC = 0x90};
enum cryptogram{ GEN_AAR = 0xC0, GEN_ARQC = 0x80, GEN_TC = 0x40, GEN_AAC = 0x00, GEN_CDDATC = 0x50, GEN_CDDAARQC = 0x90};

#ifdef  __cplusplus
}
#endif


#endif
