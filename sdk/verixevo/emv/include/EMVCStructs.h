/*
 * FILENAME       : EMVCStructs.h
 * PRODUCT        : Verix/Vx.
 * VERSION        : 4.0.0
 * AUTHOR(s)      : Pavan_k1
 * CREATED        : 07/Dec/06
 *
 * @Description	: This file contains definitions for structures used in C API's for EMV Libraries
 *
 * CONTENTS   :
 * MODIFICATION HISTORY    :
 *
 * #    Date        Who         History
 * ---  --------    ----------  ----------------------------------------
 * 1    07 Dec 06   Pavan_k1    Created for Vx EMV Module 4.0.0
 * 2    03 Jun 11   Mary_F1     MULTIAVN: Support for multiple application version number check
 * 3	06 Mar 12	Kishor_S1	SUB-PINBYPASS : Changes made to use variable set through test application 
 *								instead of reading environment variable BYPASS_PIN
 * 
 *
 * Copyright (C) 2006 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language, in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual or otherwise, without the prior written
 * permission of VeriFone Inc.
 *                                                2099 Gateway Place
 *												  Suite 600
 *											      San Jose  CA  95110 
 *												  USA
 *
 *
 *
 */
#ifndef _EMVCStruct
#define _EMVCStruct

#include "EMVGlobals.hpp"

typedef struct emvaid
{
	byte 		stAID[MAX_AID_LEN];
	byte 		lenOfAID; 
	byte 		stAppLabel[MAX_LABEL_LEN];
	byte 		appLabel; 
	byte 		stAppPreferName[MAX_LABEL_LEN]; 
	byte 		appPreferName; 
	byte 		appPriority;
	byte 		bIssuerCodeTableIndex;	
	byte        stIssuerCountryCode[ISSUER_CNTRY_SIZE]; // Support_5F56 
	EMVBoolean 	inCandidateList;
	EMVBoolean 	bExcludedAID;
}EMV_AID;

typedef struct _srAIDList
{  
	EMV_AID		arrAIDList[MAX_AID_LIST]; // Max. Number of AIDs is set to 20
	Ushort      countAID; //Number of records.
	Ushort      currentIndex;
}srAIDList;


typedef struct _srTxnResult
{
	byte stTVR[TVR_SIZE];
	byte stTSI[TSI_SIZE];
}srTxnResult;

typedef struct _srRiskMgmtData
{
	Ulong   threshold;
	Ulong   floorLimit;
	Ulong   maxPerc;
	Ulong   targetPerc;
}srRiskMgmtData;

typedef struct _srPINData
{  
	short pinTryCount;
	short pinDataLen;
	byte  stPINData[MAX_PIN_SIZE];
}srPINData;

typedef struct _srEMVInit {
	byte *pTlvCollectionArray;
	byte *pTempBuf;
	short maxTlvs;
	short tlvColxnAryLen;
	short tempBufLen;
} srEMVInit;

typedef struct _srAIDListStatus
{  
	byte stAID[MAX_AID_LEN];
	unsigned short lenOfAID; 
	unsigned short status;
}srAIDListStatus;

//MULTIAVN:
typedef struct _srMultiAVN
{  
	byte stAVN[MAX_AVN_LEN];
	
}srMultiAVN;
typedef struct _srPINParams
{  
	unsigned char	ucMin;		 // Minimum number of digits for PIN
	unsigned char 	ucMax;		 // Maximum number of digits for PIN
	unsigned char   ucEchoChar;  // Echo character to be displayed when PIN 
							 	 // is entered
	unsigned char   ucDefChar;	 // Default fill character. This should be set
							 	 // to 0x20 if no fill character 
	unsigned char 	ucDspLine;	 // Line number to display the PIN
	unsigned char 	ucDspCol;	 // Position to display the PIN
	unsigned char 	ucOption;	 // bit0 = 1, allows Auto Enter feature.

							 // bit1 = 1, allows ENTER key to be used 
							 // without entering any key digits.
							 // bit2 = 1, allows the PIN entry to be
							 // displayed from right to left
							 // bit3 = 1, makes the CLEAR key behave 
							 // as backspace key.
							 // bit4 = 1, cancels the PIN session if 
							 // the CLEAR key is pressed with no key digits.
							 // bit5-7 - RFU, must be set to 0
							 
	unsigned long   ulWaitTime;  		// Total wait time for the PIN entry 
	
	short  			abortOnPINEntryTimeOut;//SECPINTIMOUT
										// If this is set to 1, the PIN session is 
										// aborted if the PIN entry session has 
										// timed out.
	unsigned long 	ulFirstKeyTimeOut; 	// Time to wait before the user presses  
								 		// the first digit.

	unsigned long 	ulInterCharTimeOut; // Time to wait for key presses between 
								  	// the PIN digits

	unsigned char 	ucPINBypassKey;	//0 - No PIN bypass allowed
	                             	//1 - Use ENTER key as PIN bypass key
	                             	//2 - Use F1 key as PIN bypass key
	                             	//3 - Use F2 key as PIN bypass key
	                             	//4 - Use F3 key as PIN bypass key
	                             	//5 - Use F4 key as PIN bypass key

	unsigned char	ucSubPINBypass; //0 - No Subsequent pinbypass allowed  SUB-PINBYPASS
									//1 - Subsequent pinbypass allowed

}srPINParams;

// Tisha_K1 12/Aug/2010
// VFITAGFIX : Fix to maintain the VeriFone Proprietary tag values
// stored seperately from the global TLV collection.
typedef struct _srKernelTags
{  
	unsigned short usKernelTag;
	unsigned short usKernelTagLength;
	unsigned char ucKernelTagValBuff[10];
}srKernelTags;


#endif
