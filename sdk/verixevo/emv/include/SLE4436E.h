/*-----------------------------------------------------------------------------
 *
 *      Filename:       SLE4436.h
 *
 *      Product:        Card Slot Library
 *      Version:        1.7     
 *      Author :        Asha_S2
 *  	Module :	
 *      Defines all macros needed for SLE4436 synchronous card.
 *
 * MODIFICATION HISTORY    :
 *
 * #      Date		    Who			History
 * ---  ---------    ---------		----------------------------------------
 * 1.   1 June 05    Asha_S2		Created.
 *
 * 2.   4 Jan  07    Brabhu_H1      Modified the Macro value for SLE4436
 *
 *******************************************************************************
 * Copyright (C) 2000 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language 
 * or computer language, in any form or by any means, electronic, mechanical, 
 * magnetic, optical, chemical, manual or otherwise, without the prior written 
 * permission of VeriFone Inc.
 *                                                VeriFone Inc.               
 *                                                III Lagoon Drive, Suite 400  
 *                                                Redwood City, CA 94065      
 ******************************************************************************
 */

/***************************************************************/




// Macros for SLE4436 sync card. 
//Brabhu_H1 dated 4 Jan 07
//Modified the macro value as ASYNC_7816 is having the same value
//#define SLE4436											(DWORD)0x0001
#define SLE4436											(DWORD)0x0005

#include <svc_sec.h>
// SDM file - 4436.sdm


/*****************************************************************
Macro : SLE4436_READ_BYTES() 

Input : addrMSB - MSB of the address in main memory from where the data is to be read.
		addrLSB - LSB of the address in main memory from where the data is to be read.
		Len - Length of data to be read from the card
        
Output: ucBuff - Cmd data to send to Transmit_APDU
        ucBuffLen - Buffer length to send to Transmit_APDU

Description:This macro will be used by the application to read the requested
            number of bytes beginning at the specified bit address.
*******************************************************************/


#define SLE4436_READ_BYTES(ucBuff, addrMSB, addrLSB, len, ucBuffLen) { \
memset(ucBuff,0x00, 6);			\
ucBuff[1] = 0x01 ;					\
ucBuff[2] = addrMSB;	            \
ucBuff[3] = addrLSB;	            \
ucBuff[5] = len;					\
ucBuffLen = 0x06;					\
}

/*****************************************************************
Macro : SLE4436_WRITE_BYTES


Input : addrMSB - MSB of the address in main memory from where the data is to be written.
		addrLSB - LSB of the address in main memory from where the data is to be written.
		numBytes - number of bytes of data to be written
		databytes - data to be written

Output: ucBuff - Cmd data that is fed into Transmit_APDU
        ucBuffLen - Buffer length to send to Transmit_APDU

Description:This macro will be used by the application to write bits to the
         specified bit address on the card. 
*******************************************************************/

#define SLE4436_WRITE_BYTES(ucBuff, addrMSB, addrLSB, numBytes, dataBytes, ucBuffLen) {	\
memset(ucBuff,0x00, (numBytes+6));		\
ucBuff[1] = 0x02;				\
ucBuff[2] = addrMSB;	            \
ucBuff[3] = addrLSB;	            \
ucBuff[4] = numBytes;				\
memcpy(&ucBuff[5], dataBytes, numBytes);				\
ucBuffLen = 0x05 + numBytes;				\
}

/*****************************************************************
Macro : SLE4436_WRITE_ERASE_CARRY 

Input : addrMSB - MSB of the address from where the data needs to be read 
		addrLSB - LSB of the address from where the data needs to be read 
		        
Output: ucBuff - Cmd data that is fed into Transmit_APDU
        ucBuffLen - Buffer length to send to Transmit_APDU

Description:This macro will be used by the application to perform a 
		write/erase/carry procedure at bit address in the counter area provided. 
*******************************************************************/

#define SLE4436_WRITE_ERASE_CARRY(ucBuff, addrMSB, addrLSB, ucBuffLen) {	\
memset(ucBuff,0x00, 6);			\
ucBuff[1] = 0x03;					\
ucBuff[2] = addrMSB;	            \
ucBuff[3] = addrLSB;	            \
ucBuffLen = 0x06;					\
}

/*****************************************************************
Macro : SLE4436_WRITE_ERASE_CARRY_BACKUP 

Input : addrMSB - MSB of the address from where the data needs to be read 
		addrLSB - LSB of the address from where the data needs to be read 
		        
Output: ucBuff - Cmd data that is fed into Transmit_APDU
        ucBuffLen - Buffer length to send to Transmit_APDU

Description:This macro will be used by the application to perform a 
		write/erase/carry procedure at bit address in the counter area provided. 
*******************************************************************/

#define SLE4436_WRITE_ERASE_CARRY_BACKUP(ucBuff, addrMSB, addrLSB, ucBuffLen) {	\
memset(ucBuff,0x00, 6);			\
ucBuff[1] = 0x04;					\
ucBuff[2] = addrMSB;	            \
ucBuff[3] = addrLSB;	            \
ucBuffLen = 0x06;					\
}

/*****************************************************************
Macro : SLE4436_AUTHENTICATE_CARD

Input : keyAddr - address where the secure key is stored in the card.
		Valid addresses are 110 and 111
		RndData - 6 byte random data 
		        
Output: ucBuff - Cmd data that is fed into Transmit_APDU
        ucBuffLen - Buffer length to send to Transmit_APDU

Description:This macro will be used by the application to perform the 
		authentication procedure using the random number provided for 
		challenge data.
*******************************************************************/

#define SLE4436_AUTHENTICATE_CARD(ucBuff, keyAddr, RndData, ucBuffLen) {	\
memset(ucBuff,0x00, 12);			\
ucBuff[1] = 0x05;					\
ucBuff[2] = keyAddr;	            \
ucBuff[4] = 6;						\
memcpy(&ucBuff[5], RndData, 6);     \
ucBuff[11] = 0x02;						\
ucBuffLen = 12;					\
}

/*****************************************************************
Macro : SLE4436_AUTHENTICATE_CARD_GENERATE_RANDOM

Input : keyAddr - address where the secure key is stored in the card.
		Valid addresses are 110 and 111
				        
Output: ucBuff - Cmd data that is fed into Transmit_APDU
        ucBuffLen - Buffer length to send to Transmit_APDU

Description:This macro will be used by the application to perform the 
		authentication procedure using the random number provided for 
		challenge data.
*******************************************************************/

#define SLE4436_AUTHENTICATE_CARD_GENERATE_RANDOM(ucBuff, keyAddr, ucBuffLen) {	\
unsigned char    timeSecs[8]={0};	\
GenerateRandom(timeSecs);			\
memset(ucBuff,0x00, 12);			\
ucBuff[1] = 0x05;					\
ucBuff[2] = keyAddr;	            \
ucBuff[4] = 6;						\
memcpy(&ucBuff[5], timeSecs, 6);    \
ucBuff[11] = 0x02;						\
ucBuffLen = 12;						\
}

/*****************************************************************
Macro : SLE4436_PRESENT_CODE

Input : Code - 3 byte Transport Code to be verified
        
Output: ucBuff - Cmd data that is fed into Transmit_APDU
        ucBuffLen - Buffer length to send to Transmit_APDU

Description:This macro will be used by the application to present a 3 byte 
			transport code to the card.
*******************************************************************/

#define SLE4436_PRESENT_CODE(ucBuff, Code, ucBuffLen) {	\
memset(ucBuff,0x00, 6);			\
ucBuff[1] = 0x06;				\
ucBuff[4] = 3;				\
memcpy(&ucBuff[5], Code, 3);		\
ucBuffLen = 0x08  ;				\
}

//END

