define MVT BIN
{
(short, inSchemeReference, "")
(short, inIssuerReference, "")
(short, inTRMDataPresent, "")
(long, lnEMVFloorLimit, "")
(long, lnEMVRSThreshold, "")
(short, inEMVTargetRSPercent, "")
(short, inEMVMaxTargetRSPercent, "")

// Two new fields are added for APR ID:3,5
(short, inMerchantForcedOnlineFlag,"")
(short, inBlackListedCardSupportFlag, "")

(char, szEMVTACDefault[EMV_TAC_SIZE], "")
(char, szEMVTACDenial[EMV_TAC_SIZE], "")
(char, szEMVTACOnline[EMV_TAC_SIZE], "")
(char, szDefaultTDOL[EMV_MAX_TDOL_SIZE], "")
(char, szDefaultDDOL[EMV_MAX_DDOL_SIZE], "")
(short,  inEMVFallbackAllowed,"")
(short, inNextRecord, "")
(long, ulEMVCounter, "")
(short,    inEMVAutoSelectAppln,"")
(char, szEMVTermCountryCode[EMV_COUNTRY_CODE_SIZE], "")
(char, szEMVTermCurrencyCode[EMV_CURRENCY_CODE_SIZE], "")
(short, inEMVTermCurExp,"")
(char, szEMVTermCapabilities[EMV_TERM_CAPABILITIES_SIZE], "")
(char, szEMVTermAddCapabilities[EMV_ADD_TERM_CAPABILITIES_SIZE], "")
(char, szEMVTermType[EMV_TERM_TYPE_SIZE], "")
(char, szEMVMerchantCategoryCode[EMV_MERCH_CAT_CODE_SIZE], "")
(char, szEMVTerminalCategoryCode[EMV_TERM_CAT_CODE_SIZE],"")
(short, inModifyCandListFlag,"")        
(short, shRFU1,"")
(short, shRFU2,"")
(short, shRFU3,"")
(char, szRFU1[EMV_STRING_SIZE],"")
(char, szRFU2[EMV_STRING_SIZE],"")
(char, szRFU3[EMV_STRING_SIZE],"")

}
