/*
 * FILENAME       : Emvtypedefs.hpp
 * PRODUCT        : Verix/Vx.
 * VERSION        : 4.0.0
 * AUTHOR(s)      : Pavan_k1
 * CREATED        : 07/Dec/06
 *
 * @Description	: This file has typedefs used in the library.
 *
 * CONTENTS   :
 * MODIFICATION HISTORY    :
 *
 * #    Date        Who         History
 * ---  --------    ----------  ----------------------------------------
 * 1    07 Dec 06   Pavan_k1    Created for Vx EMV Module 4.0.0
 *
 * Copyright (C) 2006 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language, in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual or otherwise, without the prior written
 * permission of VeriFone Inc.
 *                                                2099 Gateway Place
 *												  Suite 600
 *											      San Jose  CA  95110 
 *												  USA
 *
 *
 *
 */

#ifndef _EMV_TYPEDEFS
#define _EMV_TYPEDEFS

#define     EMV_FALSE       (short)0
//#define     EMV_TRUE        (short)(! EMV_FALSE)
#define     EMV_TRUE        (short)1

//#define     SIGN			2
//#define		ONLINE_PIN		4

#ifndef		__byte__
#define		__byte__
	typedef     unsigned char   byte;
#endif

typedef     unsigned short  Ushort;
typedef     unsigned long   Ulong;
typedef     Ushort   		EMVResult;
typedef     short    		EMVBoolean;

typedef short (*shUserFunc( unsigned char * ));

typedef union LongCONVERT
{
   Ulong 					longVal;
   byte  					longStr[4];
}LCONVERT;


#endif
