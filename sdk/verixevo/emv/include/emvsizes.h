/*
 * FILENAME       : emvsizes.h
 * PRODUCT        : Verix/Vx.
 * VERSION        : 4.0.0
 * AUTHOR(s)      : Pavan_k1
 * CREATED        : 11/Dec/06
 *
 * DESCRIPTION    : 
 *
 * CONTENTS   :
 * MODIFICATION HISTORY    :
 *
 * #    Date        Who         History
 * ---  --------    ----------  ----------------------------------------
 * 1    11 Dec 06   Pavan_k1    Created for Vx EMV Module 4.0.0
 * 2    22 Jun 07   Brabhu_H1   Changes to hold 20 bytes of data for RFU fields
 *
 * Copyright (C) 2006 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language, in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual or otherwise, without the prior written
 * permission of VeriFone Inc.
 *                                                2099 Gateway Place
 *                                                  Suite 600
 *                                                  San Jose  CA  95110 
 *                                                  USA
 *
 *
 *
 */
#ifndef _EMVSIZES_H
#define _EMVSIZES_H

/* EMV-related definitions */

#define        EMV_COUNTRY_CODE_SIZE                5       // ASCII rep. of 2 bytes binary
#define        EMV_CURRENCY_CODE_SIZE               5       // ASCII rep. of 2 bytes binary
#define        EMV_TERM_CAPABILITIES_SIZE           7       // ASCII rep. of 3 bytes binary
#define        EMV_ADD_TERM_CAPABILITIES_SIZE       11      // ASCII rep. of 5 bytes binary
#define        EMV_TERM_TYPE_SIZE                   3       // ASCII rep. of 1 byte
#define        EMV_TAC_DEFAULT_SIZE                 11      // ASCII rep. of 5 bytes binary
#define        EMV_TAC_DENIAL_SIZE                  11      // ASCII rep. of 5 bytes binary
#define        EMV_TAC_ONLINE_SIZE                  11      // ASCII rep. of 5 bytes binary
#define        EMV_TAC_SIZE                         11      // ASCII rep. of 5 bytes binary
#define        EMV_MAX_TDOL_SIZE                    512     // ASCII rep. of 32 bytes binary
#define        EMV_MAX_DDOL_SIZE                    512     // ASCII rep. of 32 bytes binary
#define        EMV_CURR_CODE_SIZE                   5       // ASCII rep. of 2 bytes binary
#define        EMV_MERCH_CAT_CODE_SIZE              5       // ASCII rep. of 2 bytes binary

#define        EMV_RID_SIZE                         11      // ASCII rep. of 5 bytes binary
#define        EMV_MAX_PK_MODULUS_SIZE              497     // ASCII rep. of 248 bytes binary
#define        EMV_MAX_PK_EXPONENT_SIZE             7       // ASCII rep. of 3 bytes binary
#define        EMV_MAX_AID_SIZE                     33      // ASCII rep. of 16 bytes binary
#define        EMV_AVN_SIZE                         5       // ASCII rep. of 2 bytes binary
#define        EMV_SCHEME_NAME_SIZE                 33      // Name of Card Scheme
#define        EMV_PK_FILE_NAME                     31      // Name of file containing Public Key + Exponent
#define        EMV_PK_EXPDATE_SIZE                  7       // DDMMYY
#define        EMV_CSN_FILE_NAME_SIZE               31      // Name of file containing CSN validation list
#define        EMV_TERM_ID_SIZE                     8
#define        EMV_TERM_CAT_CODE_SIZE               3 
//Brabhu_H1 dated 22 Jun 07
//Decreased the RFU string size to 20 from 25
#define        EMV_STRING_SIZE                      20            
#define        MAX_APPNAME_LEN                      15

/* End of EMV stuff */

#endif
