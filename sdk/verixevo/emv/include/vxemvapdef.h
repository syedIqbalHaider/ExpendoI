/*
 * FILENAME       : vxemvapdef.h
 * PRODUCT        : Verix/Vx.
 * VERSION        : 4.0.0
 * AUTHOR(s)      : Pavan_k1
 * CREATED        : 12/Dec/06
 *
 * DESCRIPTION    : 
 *
 * CONTENTS   :
 * MODIFICATION HISTORY    :
 *
 * #    Date        Who         History
 * ---  --------    ----------  ----------------------------------------
 * 1    12 Dec 06   Pavan_k1    Created for Vx EMV Module 4.0.0
 *
 * 2    31 May 07   Purvin_p1    Added Checking for macro SUCCESS and FAILURE.
 *
 * 3	04 Sep 09	Mary_F1		INVMOD:
 *								If this module is downloaded to a Vx700 terminal, it should return
 *								E_INVALID_MODULE and the application should not continue.
 * 4    12 Jan 12    Mary_F1		Ontime ID: 56193  Vx EMV 6.0.0 Terminal allowing cardholder 
 *								confirmation should display "Try Again" message if the status
 *								word for the SELECT response is other than 9000
 * 5    09 Feb 12    Mary_F1		Ontime ID: 61519  Vx EMV 6.0.0
 *								Terminal should display "Not Accepted" message. 
 *
 * Copyright (C) 2006 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language, in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual or otherwise, without the prior written
 * permission of VeriFone Inc.
 *                                                2099 Gateway Place
 *                                                  Suite 600
 *                                                  San Jose  CA  95110 
 *                                                  USA
 *
 *
 *
 */
#ifndef _VXEMVAPDEF_H
#define _VXEMVAPDEF_H

//Purvin_p1 date 31/05/2007
//Checking for macro whether it is already defined or not
#ifndef SUCCESS
	#define SUCCESS                 0
#endif

#ifndef FAILURE 
	#define FAILURE                 -1
#endif

#define EMV_CHIP_ERROR          102
#define CARD_REMOVED            103
#define CARD_BLOCKED            104
#define APPL_BLOCKED            105
#define CHIP_ERROR              107
#define BAD_DATA_FORMAT         108
#define APPL_NOT_AVAILABLE      109
#define TRANS_CANCELLED         110
#define EASY_ENTRY_APPL         111
#define ICC_DATA_MISSING        112
#define CONFIG_FILE_NOT_FOUND   113
#define FORCED_ONLINE           114
#define FORCED_DECLINE          115
#define DO_PIN_ENTRY            116
#define PIN_OK                  117
#define BAD_PIN_RETRY           118
#define BAD_PIN_NO_RETRY        119
#define BLOCKED_CARD            120
#define NO_PIN_ENTERED          121
#define LAST_PIN_RETRY          122
#define EMV_CANCELLED           123
#define TIMED_OUT               124
#define SELECT_FAILED           125
#define USE_CHIP                126
#define REMOVE_CARD             127
#define NOT_EUROPAY_COMPLIANT   128
#define BAD_SCRIPT              129


#define INVALID_PARAMETER       130
#define INSERT_CARD             131
#define OFFLINE_APPROVED        133
#define OFFLINE_DECLINED        134
#define OFFLINE_NOTALLOWED      135
#define REFERRAL_REQST          136
#define ONLINE_REQST            137
#define INITIALIZATION_ERROR    138
#define CARDSLOT_FAILURE        139
#define NO_AID_FOUND            140
#define INVALID_CAPK_FILE       141
#define SLOT_INITIALIZE_ERROR   142
#define SLOT_SELECT_ERROR       143
#define TAG_ALREADY_PRESENT     144
#define BAD_ICC_RESPONSE        145
#define CANDIDATELIST_EMPTY     146
#define DATA_FILE_NOT_FOUND     147
#define INVALID_INDEX           148   
#define USE_MAG_CARD            149
#define  TRANS_APPROVED         150
#define  TRANS_DECLINED         151
#define  HOST_REFERRAL          152
#define  REFERRAL_REQUEST       153
#define  DO_REVERSAL            154
#define  TAG_NOTFOUND           155
#define  TAG_NOT_SUPPORTED      156
#define  NO_ATR                 157
#define INVALID_ATR             158
#define SERVICE_ALARM_SET       159
#define TRY_AGAIN       		160 //Ontime ID: 56193
#define NOT_ACCEPTED			161//Ontime ID:61519
#define MAG_SUCCESS				162//Ontime ID:55409
#define TERM_AID_CNT_EXEED      163 // 99_AID_SUPPORT


#define TRANS_TIMED_OUT         164


#define MAX_MESSAGE_SIZE        268

#define TC_GEN1AC_APPROVE       "Y1"
#define AAC_GEN1AC_DECLINE      "Z1"
#define TC_GEN2AC_APPROVE       "Y3"
#define AAC_GEN2AC_DECLINE      "Z3"

//INVMOD
#define E_INVALID_MODULE		(Ushort)0xff55

#endif

