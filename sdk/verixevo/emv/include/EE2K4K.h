/*-----------------------------------------------------------------------------
 *
 *      Filename:       EE2K4K.h
 *
 *      Product:        Card Slot Library
 *      Version:           
 *      Author :        
 *  	  Module :	
 *      Defines all macros needed for I2C synchronous card.
 *
 * MODIFICATION HISTORY    :
 *
 * #      Date		    Who			History
 * ---  ---------    ---------		----------------------------------------
 * 1.  
 *
 *******************************************************************************
 * Copyright (C) 2011 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language 
 * or computer language, in any form or by any means, electronic, mechanical, 
 * magnetic, optical, chemical, manual or otherwise, without the prior written 
 * permission of VeriFone Inc.
 *                                                VeriFone Inc.               
 *                                                III Lagoon Drive, Suite 400  
 *                                                Redwood City, CA 94065      
 ******************************************************************************
 */

/***************************************************************/
// Macros for I2C sync card. 
// Note that ee2k4k.sdm supports I2C cards.


#ifndef _EE2K4K_H
#define _EE2K4K_H



#define EE2K4K											(DWORD)0x0007
/*****************************************************************
Macro : 	EE2K4K_WRITE_BYTES

Input :	addrMSB - MSB of the address from where the data needs to be written 
          	addrLSB - LSB of the address from where the data needs to be written
          	Len - length of the data
          	data - data to be written

Output: 	ucBuff - Cmd data that is fed into Transmit_APDU
        	ucBuffLen - Buffer length to send to Transmit_APDU

Description:	This command reads card memory beginning at the specified address for up to 255 bytes.
                 

*******************************************************************/


#define EE2K4K_WRITE_BYTES(ucBuff, addrMSB, addrLSB,data, len, ucBuffLen) {	\
memset(ucBuff,0x00, (len+0x06));	\
ucBuff[1] = 0x01;						\
ucBuff[2] = addrMSB;					\
ucBuff[3] = addrLSB;					\
ucBuff[4] = len;						\
memcpy(&ucBuff[5],data, len);			\
ucBuffLen = 0x05 + len + 0x01;			\
}


/*****************************************************************
Macro : 	EE2K4K_READ_BYTES

Input : 	addrMSB - MSB of the addr to be read
           	addrLSB - LSB of the addr to be read
           	bytesToRead - Numbytes to be read from the card

Output: 	ucBuff - Cmd data that is fed into Transmit_APDU
       	ucBuffLen - Buffer length to send to Transmit_APDU


Description:	Reads the requested number of bytes from the specified address

*******************************************************************/


#define EE2K4K_READ_BYTES(ucBuff, addrMSB, addrLSB, bytesToRead, ucBuffLen) {	\
memset(ucBuff,0x00, 0x06);			\
ucBuff[1] = 0x02;					\
ucBuff[2] = addrMSB;	            \
ucBuff[3] = addrLSB;	            \
ucBuff[5] = bytesToRead;			\
ucBuffLen = 0x06 ;					\
}


/*****************************************************************
Macro : 	EE2K4K_READ_CURRENT

Input : 	bytesToRead - Number of bytes to read from current address.

Output: 	ucBuff -	Cmd data that is fed into Transmit_APDU
		ucBuffLen - Buffer length to send to Transmit_APDU

Description:	This command reads card memory beginning at the current card address location.  
                  	A length of 1 is equivalent to the "Current Address Read" function

*******************************************************************/

#define EE2K4K_READ_CURRENT(ucBuff, bytesToRead, ucBuffLen) {	\
memset(ucBuff,0x00, 0x06);			\
ucBuff[1] = 0x03;					\
ucBuff[5] = bytesToRead;			\
ucBuffLen = 0x06 ;					\
}


#endif



