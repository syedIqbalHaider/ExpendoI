/*
 * FILENAME       : EUtils.hpp
 * PRODUCT        : Verix/Vx.
 * VERSION        : 4.0.0
 * AUTHOR(s)      : Pavan_k1
 * CREATED        : 07/Dec/06
 *
 * @Description	: This class provides general utility funtions which are not specific 
 * to EMV specifications.
 *
 * CONTENTS   :
 * MODIFICATION HISTORY    :
 *
 * #    Date        Who         History
 * ---  --------    ----------  ----------------------------------------
 * 1    07 Dec 06   Pavan_k1    Created for Vx EMV Module 4.0.0
 *
 * Copyright (C) 2006 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language, in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual or otherwise, without the prior written
 * permission of VeriFone Inc.
 *                                                2099 Gateway Place
 *												  Suite 600
 *											      San Jose  CA  95110 
 *												  USA
 *
 *
 *
 */
#ifndef     _E_UTILS
#define     _E_UTILS


#include    "EMVTypeDefs.hpp"


#define SIZE_ULONG  (sizeof(Ulong))  


#ifdef  __cplusplus
extern "C" {
#endif

    EMVResult 		hex2Asc(byte *pOutData, const byte *pInData, short length);
    EMVResult 		getCAPKData( const byte* aid, byte capkIndex, 
                   		byte* pstCapk, short *capkLen, 
                   		byte* pstCapkExp, short *pcapkExpLen );

    EMVBoolean 		isBitSet(const byte inpVal, short bitNum );

    Ulong 			hexToUlong( byte* pStr,Ushort size );
    Ulong  			makeUnsignedLong(byte *str);

    long 			getRandomNum( short min, short max);

    void 			getRandomStr(byte* pRandStr, Ushort size);
    void 			asciiToBin(const byte* pstSrc,  byte* pstDest, const Ushort inpLen);
    void  			Ulong2Amtstr(Ulong amount, byte *pAmtStr);
    void 			Ulong2ByteArray(Ulong val, byte* pStr);
    void 			bcd1ToHex(byte inpVal, short* hexVal );
    short 			debugLog(char *fmt, ...);
    short 			dumpHexBuffer(unsigned char *fmt, unsigned short len);
    short 			debugSetup(void);
	short			printHexBuffer(char *pMsg, unsigned char *fmt, unsigned short len);
	short 			printIntValue(char *pMsg, short value);
	short 			printShortValue(char *pMsg, unsigned short value);
	short 			printDateTime(void);
	short 			printTvrTsi(byte *pTvr, byte *pTsi);
	short 			printString(char *pMsg);
	short 			printRetValue(char *pMsg, unsigned short value);

    
#ifdef  __cplusplus
}
#endif

#endif // endof EUtils.hpp


