/*
 * FILENAME       : emvTags.hpp
 * PRODUCT        : Verix/Vx.
 * VERSION        : 4.0.0
 * AUTHOR(s)      : Pavan_k1
 * CREATED        : 07/Dec/06
 *
 * @Description	:This file has the definitions for the EMV tags used
 * in the library.
 *
 * CONTENTS   :
 * MODIFICATION HISTORY    :
 *
 * #    Date        Who         History
 * ---  --------    ----------  ----------------------------------------
 * 1    07 Dec 06   Pavan_k1    Created for Vx EMV Module 4.0.0
 * 
 * 2    23 May 07   Brabhu_H1   Interac specific properatory Tag DF62 is added. 
 *
 * 3    24 Jun 09   Kishor_S1	Get last status word :Adde private tag for new requirment of updating 
 *								the status word to Tag 0xDF09 for all responses received from ICC.
 *
 * 4	30 Jun 09	Mary_F1		ESTAVN2	
 *								Added this user defined tag, to store the second
 *								terminal version number 
 *
 * 5	07 Jul 09   Mary_F1		CodeReview: 
 *								TAG_9F53_TRAN_CURR_CODE is replaced with TAG_9F53_TERM_CAT_CODE
 *
 * 6	29 Jul 09	Kishor_S1	EC specific properatory Tag Added
 *
 * 7 10/08/09 Tisha_K1      PARTIAL: Added user defined tag 0xDF11, to store the length of the AID length to be
 *					      used in the module function bIsMultipleOccurencesAllowed
 * 8   27/Jan/10    Soumya_G1   Fix for the DOL handling - Added all the known tags to the taglist to be inlcuded
                                                     in the CDOLs.
 *9		31/03/11 	Ganesh_p1		Changed TAG_9F55_ISS_AUTH_FLAG to TAG_9F55_ISSUER_AUTH_FLAG
 * Copyright (C) 2006 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language, in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual or otherwise, without the prior written
 * permission of VeriFone Inc.
 *                                                2099 Gateway Place
 *												  Suite 600
 *											      San Jose  CA  95110 
 *												  USA
 *
 *
 *
 */

#ifndef _EMVTAGS
#define _EMVTAGS

#define     TAG_42_ISSUER_ID_NUM         	(Ushort)0x4200
#define     TAG_4F_AID                  	(Ushort)0x4f00
#define     TAG_50_APPL_LABEL           	(Ushort)0x5000
#define     TAG_52_CMD_TO_PERFORM       	(Ushort)0x5200
#define     TAG_57_TRACK2_EQ_DATA       	(Ushort)0x5700
#define     TAG_5A_APPL_PAN             	(Ushort)0x5a00
#define     TAG_5F20_CARDHOLDER_NAME    	(Ushort)0x5f20
#define     TAG_5F24_EXPIRY_DATE        	(Ushort)0x5f24
#define     TAG_5F25_EFFECT_DATE        	(Ushort)0x5f25
#define     TAG_5F28_ISSUER_COUNTY_CODE 	(Ushort)0x5f28
#define     TAG_5F2A_TRANS_CURCY_CODE   	(Ushort)0x5f2a
#define     TAG_5F2D_LANG_PREFERENCE    	(Ushort)0x5f2d
#define     TAG_5F30_SERVICE_CODE       	(Ushort)0x5f30
#define     TAG_5F34_APPL_PAN_SEQNUM    	(Ushort)0x5f34
#define     TAG_5F36_TRANS_CURR_EXP     	(Ushort)0x5f36
#define 	TAG_5F50_ISSUER_URL			    (Ushort)0x5F50 
#define     TAG_5F53_INT_BANK_ACC_NO    	(Ushort)0x5F53
#define     TAG_5F54_BANK_ID_CODE       	(Ushort)0x5F54
#define     TAG_5F55_ISSUER_COUNTRY_CODE 	(Ushort)0x5F55
#define     TAG_5F56_ISSUER_COUNTRY_CODE 	(Ushort)0x5F56
#define 	TAG_5F57_ACCOUNT_TYPE    	    (Ushort)0x5F57

#define     TAG_APPL_TEMPL_61           	(Ushort)0x6100     
#define     TAG_FCI_TEMPL_6F            	(Ushort)0x6F00

#define     TAG_ISUER_SCRPT_TEMPL_71    	(Ushort)0x7100
#define     TAG_ISUER_SCRPT_TEMPL_72    	(Ushort)0x7200
#define     TAG_DIR_DISCR_TEMPL_73      	(Ushort)0x7300
#define     TAG_RESPMSG_FMT1_TEMPL_77   	(Ushort)0x7700
#define     TAG_RESPMSG_FMT2_TEMPL_80   	(Ushort)0x8000
#define     TAG_81_AMOUNT_AUTH          	(Ushort)0x8100
#define     TAG_8200_APPL_INTCHG_PROFILE  	(Ushort)0x8200
#define     TAG_83_CMD_TEMPLATE         	(Ushort)0x8300
#define     TAG_84_DF_NAME              	(Ushort)0x8400
#define     TAG_86_ISSUER_SCRIPT_CMD    	(Ushort)0x8600
#define     TAG_87_APPL_PRIORITY_IND    	(Ushort)0x8700
#define     TAG_88_SFI                  	(Ushort)0x8800
#define     TAG_89_AUTH_CODE            	(Ushort)0x8900
#define     TAG_8A_AUTH_RESP_CODE       	(Ushort)0x8a00
#define     TAG_8C_CDOL1                	(Ushort)0x8c00
#define     TAG_8D_CDOL2                	(Ushort)0x8d00
#define     TAG_8E00_CVM_LIST           	(Ushort)0x8e00
#define     TAG_8F_AUTH_PUBKEY_INDEX    	(Ushort)0x8f00
#define     TAG_90_ISS_PUBKEY_CERT      	(Ushort)0x9000
#define     TAG_91_ISS_AUTH_DATA        	(Ushort)0x9100
#define     TAG_92_ISS_PUBKEY_REM       	(Ushort)0x9200
#define     TAG_93_SIGNED_SAD           	(Ushort)0x9300
#define     TAG_94_AFL                  	(Ushort)0x9400
#define     TAG_9500_TVR                	(Ushort)0x9500
#define     TAG_97_TDOL                 	(Ushort)0x9700
#define     TAG_98_TC_HASH              	(Ushort)0x9800
#define     TAG_99_PIN_DATA             	(Ushort)0x9900
#define     TAG_9A_TRAN_DATE            	(Ushort)0x9a00
#define     TAG_9B00_TSI                	(Ushort)0x9b00
#define     TAG_9C00_TRAN_TYPE          	(Ushort)0x9c00
#define     TAG_9D_DDF_NAME             	(Ushort)0x9d00        
#define     TAG_9F01_ACQ_ID             	(Ushort)0x9f01
#define     TAG_9F02_AMT_AUTH_NUM       	(Ushort)0x9f02
#define     TAG_9F03_AMT_OTHER_NUM      	(Ushort)0x9f03
#define     TAG_9F04_AMT_OTHER_BIN      	(Ushort)0x9f04
#define     TAG_9F05_APPL_DISC_DATA     	(Ushort)0x9f05
#define     TAG_9F06_APPL_ID            	(Ushort)0x9f06
#define     TAG_9F07_APPL_USE_CNTRL     	(Ushort)0x9f07
#define     TAG_9F08_APP_VER_NUM        	(Ushort)0x9f08
#define     TAG_9F09_TERM_VER_NUM       	(Ushort)0x9f09
#define     TAG_9F0B_CRDHLDRNAME_EXT    	(Ushort)0x9f0b
#define     TAG_9F0D_IAC_DEFAULT        	(Ushort)0x9f0d
#define     TAG_9F0E_IAC_DENIAL         	(Ushort)0x9f0e
#define     TAG_9F0F_IAC_ONLINE         	(Ushort)0x9f0f
#define     TAG_9F10_ISSUER_APP_DATA    	(Ushort)0x9f10
#define     TAG_9F11_ISSSUER_CODE_TBL   	(Ushort)0x9f11
#define     TAG_9F12_APPL_PRE_NAME      	(Ushort)0x9f12
#define     TAG_9F13_LAST_ONLINE_ATC    	(Ushort)0x9f13
#define     TAG_9F14_LC_OFFLINE_LMT     	(Ushort)0x9f14
#define     TAG_9F15_MER_CAT_CODE       	(Ushort)0x9f15
#define     TAG_9F16_MER_ID             	(Ushort)0x9f16
#define     TAG_9F17_PIN_TRY_COUNTER    	(Ushort)0x9f17
#define     TAG_9F18_ISSUER_SCRIPT_ID   	(Ushort)0x9f18
#define     TAG_9F1A_TERM_COUNTY_CODE   	(Ushort)0x9f1a
#define     TAG_9F1B_TERM_FLOOR_LIMIT   	(Ushort)0x9f1b
#define     TAG_9F1C_TEMR_ID            	(Ushort)0x9f1c
#define     TAG_9F1D_TERM_RISKMGMT_DATA 	(Ushort)0x9f1d
#define     TAG_9F1E_IFD_SER_NUM        	(Ushort)0x9f1e
#define     TAG_9F1F_TRACK1_DISC_DATA   	(Ushort)0x9f1f
#define     TAG_9F20_TRACK2_DISC_DATA   	(Ushort)0x9f20
#define     TAG_9F21_TRANS_TIME         	(Ushort)0x9f21
#define     TAG_9F22_CERT_PUBKEY_IND    	(Ushort)0x9f22
#define     TAG_9F23_UC_OFFLINE_LMT     	(Ushort)0x9f23
#define     TAG_9F26_APPL_CRYPTOGRAM    	(Ushort)0x9f26
#define     TAG_9F27_CRYPT_INFO_DATA    	(Ushort)0x9f27
#define     TAG_9F2D_ICC_PIN_CERT       	(Ushort)0x9f2d
#define     TAG_9F2E_ICC_PIN_EXP        	(Ushort)0x9f2e
#define     TAG_9F2F_ICC_PIN_REM        	(Ushort)0x9f2f
#define     TAG_9F32_ISS_PUBKEY_EXP     	(Ushort)0x9f32
#define     TAG_9F33_TERM_CAP           	(Ushort)0x9f33
#define     TAG_9F34_CVM_RESULTS        	(Ushort)0x9f34
#define     TAG_9F35_TERM_TYPE          	(Ushort)0x9f35
#define     TAG_9F36_ATC                	(Ushort)0x9f36
#define     TAG_9F37_UNPRED_NUM         	(Ushort)0x9f37
#define     TAG_9F38_PDOL               	(Ushort)0x9f38
#define     TAG_9F39_POS_ENTRY_MODE     	(Ushort)0x9f39
#define     TAG_9F3A_AMT_REF_CURR       	(Ushort)0x9f3a
#define     TAG_9F3B_APPL_REF_CURR      	(Ushort)0x9f3b
#define     TAG_9F3C_TRANS_REF_CURR     	(Ushort)0x9f3c
#define     TAG_9F3D_TRANS_REFCURR_EXP  	(Ushort)0x9f3d
#define     TAG_9F40_TERM_ADTNAL_CAP    	(Ushort)0x9f40
#define     TAG_9F41_TRANS_SEQ_COUNTER  	(Ushort)0x9f41
#define     TAG_9F42_APPL_CURCY_CODE    	(Ushort)0x9f42
#define     TAG_9F43_APPL_REF_CURR_EXP  	(Ushort)0x9f43
#define     TAG_9F44_APPL_CURR_EXP      	(Ushort)0x9f44
#define     TAG_9F45_DATA_AUTH_CODE     	(Ushort)0x9f45
#define     TAG_9F46_ICC_PUBKEY_CERT    	(Ushort)0x9f46
#define     TAG_9F47_ICC_PUBKEY_EXP     	(Ushort)0x9f47 
#define     TAG_9F48_ICC_PUBKEY_REM     	(Ushort)0x9f48     
#define     TAG_9F49_DDOL               	(Ushort)0x9f49
#define     TAG_9F4A_SDA_TAGLIST        	(Ushort)0x9f4a
#define     TAG_9F4B_DYNAMIC_APPL_DATA  	(Ushort)0x9f4b
#define     TAG_9F4C_ICC_DYNAMIC_NUM    	(Ushort)0x9f4c
#define 	TAG_9F4D_LOG_ENTRY				(Ushort)0x9F4D
#define 	TAG_9F4E_MERCHANT_NAME_LOCN   	(Ushort)0x9F4E
#define 	TAG_9F4F_LOG_FORMAT				(Ushort)0x9F4F
#define     TAG_BF0C_FCI_DATA           	(Ushort)0xbf0c
#define     TAG_FCI_PROPTRY_TEMPL_A5    	(Ushort)0xA500

//CodeReview: TAG_9F53_TRAN_CURR_CODE is replaced with TAG_9F53_TERM_CAT_CODE
#define     TAG_9F53_TERM_CAT_CODE     	(Ushort)0x9f53
#define     TAG_AEF_TEMPL_70            	(Ushort)0x7000

//Ganesh_p1: Commented because 0x9F55 is defined by Master card as Issuer Auth flag
//#define     TAG_9F55_ISSUER_SCRPT_RESULTS 	(Ushort)0x9F55

//TRMCIDUP: Brabhu_H1 Dated 03 April 2008
//New Tag introduced to keep track of the terminal decision
#define		TAG_DF21_TERM_DECISION			(Ushort)0xDF21
//INTERAC:
//Brabhu_H1 Dated 29 May 07
//Interac specific properatory Tag
#define     TAG_DF62_APPL_SEL_FLAG          (Ushort)0xDF62 

//Get last status word : Kishor_S1 added on 24-6-09 for new requirment of updating 
// the status word to Pivate Tag 0xDF09 for all responses received from ICC
#define     TAG_DF09_LAST_STATUS_WORD       (Ushort)0xDF09 

#define     NO_TEMPLATE                 	(Ushort)0xFFFF

//ESTAVN2	Mary_F1		30/Jun/09
//Added this user defined tag, to store the second terminal version number 
#define     TAG_DF10_SEC_TERM_VER_NUM          (Ushort)0xDF10 


// Tisha_K1 10/08/09
//PARTIAL: Added this user defined tag, to store the length of the AID length to be used in the module
// function bIsMultipleOccurencesAllowed
#define     TAG_DF11_AID_LEN         (Ushort)0xDF11 

//Kishor_S1 Dated 29 July 09 (Moved the changes)
//EC specific properatory Tag
#define     TAG_9F79_EC_BALANCE             (Ushort)0x9F79
#define     TAG_9F77_EC_BALANCE_LIMIT       (Ushort)0x9F77
#define     TAG_9F74_EC_ISSUER_AUTH_CODE    (Ushort)0x9F74
#define     TAG_9F78_EC_SINGLE_TXN_LIMIT    (Ushort)0x9F78
#define     TAG_9F6D_EC_RESET_THRESHOLD     (Ushort)0x9F6D
#define     TAG_9F7A_EC_TERM_SUPPORT_IND    (Ushort)0X9F7A
#define     TAG_9F7B_EC_TERM_TXN_LIMIT      (Ushort)0x9F7B

//Soumya_G1 - 25/Jan /10
//Added the following tag list to the CDOL

#define TAG_9F7E_APPL_LF_CYCL                    (Ushort)0x9F7E

//Ganesh_p1: Changed TAG_9F55_ISS_AUTH_FLAG to TAG_9F55_ISSUER_AUTH_FLAG
//#define TAG_9F55_ISS_AUTH_FLAG					 (Ushort)0x9F55
#define TAG_9F55_ISSUER_AUTH_FLAG		(Ushort)0x9F55

#define TAG_9F56_APP_DATA   					 (Ushort)0x9F56
#define TAG_9F50_OFFLINE_BAL                     (Ushort)0x9F50
#define TAG_9F52_APP_DATA                        (Ushort)0x9F52  
#define TAG_9F57_APP_DATA                        (Ushort)0x9F57
#define TAG_BF5A_AIPAFLENT_TEMP                  (Ushort)0xBF5A
#define TAG_BF58_AMT_DATA_TEMPL                  (Ushort)0xBF58 
#define TAG_DF01_APPL_CAPAB                      (Ushort)0xDF01
#define TAG_9F51_APPL_CURR_CODE                  (Ushort)0x9F51
#define TAG_BF5B_APPL_INT_DATA_TEMPL             (Ushort)0xBF5B
#define TAG_9F5D_AVA_OFFLINE_SPEND_AMT           (Ushort)0x9F5D
//#define TAG_DF11_CONSEC_TRAN_CTR                 (Ushort)0xDF11
//#define TAG_DF51_CONSEC_TRAN_CTR_INTLCNT         (Ushort)0xDF51
#define TAG_9F72_CONSEC_TRAN_CTR_INTLCNTLMT      (Ushort)0x9F72
//#define TAG_DF61_CONSEC_TRAN_CTR_INTLCNTLMT      (Ushort)0xDF61
//#define TAG_DF21_APP_DATA                        (Ushort)0xDF21
#define TAG_9F5E_CONSEC_TRAN_INT_UPP_LMT         (Ushort)0x9F5E
#define TAG_DF71_CONSEC_TRAN_INT_UPP_LMT         (Ushort)0xDF71
#define TAG_9F58_APP_DATA                        (Ushort)0x9F58     
#define TAG_9F59_APP_DATA                        (Ushort)0x9F59
//#define TAG_DF31_CONSEC_TRAN_CNT_UPP_LMT         (Ushort)0xDF31
#define TAG_BF56_COUNT_DATA_TEMPL                (Ushort)0xBF56
#define TAG_BF57_INT_COUNT_DATA_TEMPL            (Ushort)0xBF57
#define TAG_9F73_CONV_CURR_CODE                  (Ushort)0x9F73
#define TAG_9F54_APP_DATA                        (Ushort)0x9F54
#define TAG_9F5C_CUM_TXN_AMT_UPP_LMT             (Ushort)0x9F5C

#define TAG_BF59_PROF_CNTRL_TEMPL                (Ushort)0xBF59
#define TAG_DF02_PROF_SEL_FL_ENTRY               (Ushort)0xDF02

#define TAG_DF03_APPL_DEF_ACTION                 (Ushort)0xDF03

#define TAG_5F84_APPL_PAN_SEQ_NUM                (Ushort)0x5F84
#define TAG_9F61_APPL_DEF_ACTION                 (Ushort)0x9F61
#define TAG_9F62_ISS_AUTH_INDICATOR              (Ushort)0x9F62
#define TAG_9F63_CRD_ACT_ANYS_SUPP_INFO	         (Ushort)0x9F63
#define TAG_9F64_CUM_TOT_TXN_AMT                 (Ushort)0x9F64
#define TAG_9F65_TXN_CURR_CONV_TABLE	         (Ushort)0x9F65
#define TAG_9F66_CRD_ACTION_CODE                 (Ushort)0x9F66
#define TAG_9F5A_LOW_CONSEC_INT_OFFLN_LIMIT      (Ushort)0x9F5A
#define TAG_9F5B_MAX_DOMST_OFFL_TXN_AMT          (Ushort)0x9F5B

#define TAG_DF10_AIPAFLENT0                      (Ushort)0xDF10
//#define TAG_DF11_AIPAFLENT1                      (Ushort)0xDF11
#define TAG_DF12_AIPAFLENT2                      (Ushort)0xDF12 
#define TAG_DF13_AIPAFLENT3                      (Ushort)0xDF13
#define TAG_DF14_AIPAFLENT4                      (Ushort)0xDF14
#define TAG_DF15_AIPAFLENT5                      (Ushort)0xDF15
#define TAG_DF16_AIPAFLENT6                      (Ushort)0xDF16
#define TAG_DF17_AIPAFLENT7                      (Ushort)0xDF17
#define TAG_DF18_AIPAFLENT8                      (Ushort)0xDF18
#define TAG_DF19_AIPAFLENT9                      (Ushort)0xDF19
#define TAG_DF1A_AIPAFLENTA                      (Ushort)0xDF1A
#define TAG_DF1B_AIPAFLENTB                      (Ushort)0xDF1B
#define TAG_DF1C_AIPAFLENTC                      (Ushort)0xDF1C
#define TAG_DF1D_AIPAFLENTD                      (Ushort)0xDF1D
#define TAG_DF1E_AIPAFLENTE                      (Ushort)0xDF1E
#define TAG_DF1F_AIPAFLENTF                      (Ushort)0xDF1F

#define TAG_DF20_CTCIL0                          (Ushort)0xDF20
//#define TAG_DF21_CTCIL1                          (Ushort)0xDF21
#define TAG_DF22_CTCIL2                          (Ushort)0xDF22
#define TAG_DF23_CTCIL3                          (Ushort)0xDF23
#define TAG_DF24_CTCIL4                          (Ushort)0xDF24
#define TAG_DF25_CTCIL5                          (Ushort)0xDF25
#define TAG_DF26_CTCIL6                          (Ushort)0xDF26
#define TAG_DF27_CTCIL7                          (Ushort)0xDF27
#define TAG_DF28_CTCIL8                          (Ushort)0xDF28
#define TAG_DF29_CTCIL9                          (Ushort)0xDF29
#define TAG_DF2A_CTCILA                          (Ushort)0xDF2A
#define TAG_DF2B_CTCILB                          (Ushort)0xDF2B
#define TAG_DF2C_CTCILC                          (Ushort)0xDF2C
#define TAG_DF2D_CTCILD                          (Ushort)0xDF2D
#define TAG_DF2E_CTCILE                          (Ushort)0xDF2E
#define TAG_DF2F_CTCILF                          (Ushort)0xDF2F

#define TAG_DF30_CTIUL0                          (Ushort)0xDF30
#define TAG_DF31_CTIUL1                          (Ushort)0xDF31
#define TAG_DF32_CTIUL2                          (Ushort)0xDF32
#define TAG_DF33_CTIUL3                          (Ushort)0xDF33
#define TAG_DF34_CTIUL4                          (Ushort)0xDF34
#define TAG_DF35_CTIUL5                          (Ushort)0xDF35
#define TAG_DF36_CTIUL6                          (Ushort)0xDF36
#define TAG_DF37_CTIUL7                          (Ushort)0xDF37
#define TAG_DF38_CTIUL8                          (Ushort)0xDF38
#define TAG_DF39_CTIUL9                          (Ushort)0xDF39
#define TAG_DF3A_CTIULA                          (Ushort)0xDF3A
#define TAG_DF3B_CTIULB                          (Ushort)0xDF3B
#define TAG_DF3C_CTIULC                          (Ushort)0xDF3C
#define TAG_DF3D_CTIULD                          (Ushort)0xDF3D
#define TAG_DF3E_CTIULE                          (Ushort)0xDF3E
#define TAG_DF3F_CTIULF                          (Ushort)0xDF3F

#define TAG_DF50_CTCIC0                          (Ushort)0xDF50  
#define TAG_DF51_CTCIC1                          (Ushort)0xDF51
#define TAG_DF52_CTCIC2                          (Ushort)0xDF52
#define TAG_DF53_CTCIC3                          (Ushort)0xDF53
#define TAG_DF54_CTCIC4                          (Ushort)0xDF54
#define TAG_DF55_CTCIC5                          (Ushort)0xDF55
#define TAG_DF56_CTCIC6                          (Ushort)0xDF56
#define TAG_DF57_CTCIC7                          (Ushort)0xDF57
#define TAG_DF58_CTCIC8                          (Ushort)0xDF58
#define TAG_DF59_CTCIC9                          (Ushort)0xDF59
#define TAG_DF5A_CTCICA                          (Ushort)0xDF5A
#define TAG_DF5B_CTCICB                          (Ushort)0xDF5B
#define TAG_DF5C_CTCICC                          (Ushort)0xDF5C
#define TAG_DF5D_CTCICD                          (Ushort)0xDF5D
#define TAG_DF5E_CTCICE                          (Ushort)0xDF5E
#define TAG_DF5F_CTCICF                          (Ushort)0xDF5F

#define TAG_DF60_CTCICL0                         (Ushort)0xDF60
#define TAG_DF61_CTCICL1                         (Ushort)0xDF61
//#define TAG_DF62_CTCICL2                         (Ushort)0xDF62
#define TAG_DF63_CTCICL3                         (Ushort)0xDF63
#define TAG_DF64_CTCICL4                         (Ushort)0xDF64
#define TAG_DF65_CTCICL5                         (Ushort)0xDF65
#define TAG_DF66_CTCICL6                         (Ushort)0xDF66
#define TAG_DF67_CTCICL7                         (Ushort)0xDF67
#define TAG_DF68_CTCICL8                         (Ushort)0xDF68
#define TAG_DF69_CTCICL9                         (Ushort)0xDF69
#define TAG_DF6A_CTCICLA                         (Ushort)0xDF6A
#define TAG_DF6B_CTCICLB                         (Ushort)0xDF6B
#define TAG_DF6C_CTCICLC                         (Ushort)0xDF6C
#define TAG_DF6D_CTCICLD                         (Ushort)0xDF6D
#define TAG_DF6E_CTCICLE                         (Ushort)0xDF6E
#define TAG_DF6F_CTCICLF                         (Ushort)0xDF6F


#endif

