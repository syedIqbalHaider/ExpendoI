/*
 * FILENAME       : EMVCWrappers.h
 * PRODUCT        : Verix/Vx.
 * VERSION        : 4.0.0
 * AUTHOR(s)      : Pavan_k1
 * CREATED        : 07/Dec/06
 *
 * @Description	: This file conmtains C API's for EMV Libraries
 *
 * CONTENTS   :
 * MODIFICATION HISTORY    :
 *
 * #    Date        Who         History
 * ---  --------    ----------  ----------------------------------------
 * 1    07 Dec 06   Pavan_k1    Created for Vx EMV Module 4.0.0
 * 2    23 Jan 07   Pavan_K1    Removed the prototype of the function usEMVHostData
 *                              as it is not called anywhere as part of code cleanup
 * 3    28 Feb 07   Pavan_K1    Added the set APIS for the Common module.
 *
 * 4    13 Mar 07   Purvin_p1   Added new API vEMVGetEncPINBlk() to get Enciphered PIN block.
 * 5    14 May 07   Pavan_K1    Changed the prototype of the get transaction amount for the
 *                              account type selection.
 * 6    17 Jul 07   Purvin_p1   RETURN_TYPE: Change return type for pfuncPinPointer  from short to UShort
 * 7    24 Jul 07   Brabhu_H1   ENCPINBK: For made for onTimeID 13990 to display 
 *                              the deciphered PIN block
 * 8    29 Jul 09 	Kishor_S1   ECTXNFLW: usECTransactionCheck Function created for ECTransaction check.
 * 9	21/Dec/10	Kishor_S1	CDAMode : Moved Fix made to have configurable CDA Mode. Mode can be configured
 *											by changing the 2nd and 3rd bit from LSB of Short RFU1
 * 10	27/Jan/11	Kishor_S1	MULTIAPP_PIN Changes : Added the changes from SAKO
 * 11   03/Jun/11   Mary_F1     MULTIAVN: Support for multiple application version number check
 * 12   03/Jun/11   Mary_F1		ExtAuth6985: The EMV Kernel behavior is modified to cater to the
 *                              different application needs, to terminate the transaction or to continue
 *                              with the transaction on receiving the status word as 0x6985 for the external authenticate command.
 * 13  11/Apr/2012 Soumya_G1	MULTI_BYTE_TAG: Added code for new requirement. 
 * 14   21/Aug/12   Kishor_S1   MWR_SECURITY_CHANGES: GETTLV_Changes : New API with buffer length parameter check added 
 * 
 * Copyright (C) 2011 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language, in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual or otherwise, without the prior written
 * permission of VeriFone Inc.
 *                                                2099 Gateway Place
 *												  Suite 600
 *											      San Jose  CA  95110 
 *												  USA
 *
 *
 *
 */
#ifndef     _EMV_CWRAPPERS
#define     _EMV_CWRAPPERS

#include    "EMVTypeDefs.hpp"
#include    "EMVCStructs.h"


//Pavan dated 26-Feb-07 added a function pointer for the replacement of call back functions.
//Purvin_p1 date 12/July/07 
//RETURN_TYPE: Change return type from short to UShort
//typedef short (*pfuncPinPointer)(unsigned char *);

typedef unsigned short (*pfuncPinPointer)(unsigned char *);
typedef unsigned short (*pfuncLastTrnsAmtPtr)(Ulong *);
typedef unsigned short (*pfuncIssAcqCvm)(unsigned short, unsigned short *);
typedef unsigned short (*pfuncPerformSig)(void);
typedef unsigned short (*pfuncPerformOnlinePin)(void);
typedef short (*pfuncIsCardBlackList)(unsigned char *, unsigned short, unsigned char *, unsigned short);
//typedef unsigned short (*pfuncGetTransAmt)(void);
typedef unsigned short (*pfuncGetTransAmt)(unsigned short);
typedef unsigned short (*pfuncIsCSNValid)(unsigned char *);
typedef void (*pfuncDispPinPrompt)(void);
typedef unsigned short (*pfuncGetTermParam)(unsigned short, unsigned char *, unsigned short *);
typedef void (*pfuncDispErrorPrompt)(unsigned short);
typedef unsigned short (*pfuncGetCapkData)(const unsigned char *, const unsigned char, unsigned char *, short *, unsigned char *, short *);

typedef short (*pfuncIsPartialSelAllowed)(unsigned char *);

//MULTIAVN:
typedef unsigned short (*pfuncMultiAppVerNum)(srMultiAVN *, int *);

//ExtAuth6985:
typedef unsigned short (*pfuncGetEADecision)(unsigned short);

// POS_CNCL
typedef unsigned short(*pfuncGetPOSDecsn)(void);

// SPAIN_REQ
typedef unsigned short (*pfuncGetBypassDecsn)(void);

//Soumya_G1 28/Feb/07 - Common Module Implementation
typedef unsigned short (*pfuncprocICCResp)(unsigned char *, unsigned short) ;
typedef EMVResult (*pfuncVelChkReqd)(void);

#ifdef  __cplusplus
extern "C" {
#endif

    Ushort  usEMVInit( srEMVInit *emvInit );

    Ushort  usEMVCandidateList (char chSelectionMethod, const srAIDList  *psrTermAIDList, 
                srAIDList *psrCandidateList);

    Ushort  usEMVSelectApplication (srAIDList  *psrAIDChosen, Ushort index);

    Ushort 	usEMVInitApplication (Ulong ulTxnAmt, srTxnResult *psrTVR_TSI);

    Ushort  usEMVReadAppData (srTxnResult *psrTVR_TSI);

    Ushort  usEMVProcRestrict( srTxnResult *psrTVR_TSI );

    Ushort  usEMVTermRiskMgmt (Ulong ulTxnAmt, const srRiskMgmtData *psrData,
                              srTxnResult *psrTVR_TSI);

	Ushort	usEMVCardholderVerify( srTxnResult *psrTVR_TSI );

    Ushort  usEMVScriptProcessing (short sProc, byte *issuerScriptResults, srTxnResult  *psrTVR_TSI);
                              
    Ushort  usEMVTermActionAnalysis (byte ucFailedOnLine,srTxnResult *psrTVR_TSI);

    Ushort  usEMVDataAuthenticate( srTxnResult *psrTVR_TSI );

    Ushort  usEMVGenerateFirstAC(byte termDecision, byte *flag, srTxnResult* psrTVR_TSI);

    Ushort  usEMVGenerateSecondAC(byte termDecision, byte *flag, srTxnResult* psrTVR_TSI);
	//MULTI_BYTE_TAG : Soumya_G1 29/March/12
       //Changed the prototypes of the below interface functions of the TLV collection
    //Ushort  usEMVUpdateTLVInCollxn( const Ushort tag, byte* pData, Ushort dLen );

    //Ushort  usEMVGetTLVFromColxn( const Ushort tag, byte* pData, Ushort* dLen );

    //Ushort  usEMVAddTLVToCollxn(const Ushort tag, byte* pData, Ushort dLen );

	//Ushort	usEMVRemoveTLVFromCollxn(unsigned short tag );

	
	Ushort	usEMVUpdateTLVInCollxn( const Ushort tag, ...  );
	
	Ushort  usEMVGetTLVFromColxn( const Ushort tag, ...  );

	//MWR_SECURITY_CHANGES: GETTLV_Changes :New API with buffer length parameter		
	Ushort	usEMVRetrieveTLVFromColxn( const Ushort tag, ...   );
	
	Ushort  usEMVAddTLVToCollxn(const Ushort tag, ...  );
	
	Ushort	usEMVRemoveTLVFromCollxn(const Ushort tag, ...  );

    Ushort  usEMVSetTxnStatus (srTxnResult  *psrTVR_TSI);

    Ushort  usEMVGetTxnStatus(srTxnResult  *psrTVR_TSI);

	Ushort  usEMVGetData(Ushort usTag, byte* pstDataBuf, Ushort* usLen);

    Ushort  usEMVReadRecord(const byte usSfi,const byte usRecNo,byte* pstResp,Ushort* usRespLen);

    Ushort  usEMVSelect(const byte* pstData,const Ushort usDataLen,const short usSelectType);

    Ushort  usEMVResetPrimaryCard( unsigned long slotNo);

    Ushort 	usEMVInitTransaction(void);

    Ushort 	usEMVExternalAuth(srTxnResult  *psrTVR_TSI);

    Ushort 	usEMVSetFloorLimitChkReqd(short flrLtChk);     

    Ushort 	usVerifyResult(Ushort retVal);

    Ushort 	usEMVSetForceOnlineBit(void);

    Ushort 	usEMVAddAmtToCollxn(Ulong ulTxnAmt);

    Ushort 	usEMVGetAIDStatus(byte *stAID, unsigned short len);

    Ushort 	usEMVGetAllAIDStatus(srAIDListStatus *paidStatus, unsigned short *aidCount, unsigned short *blockAidCount);

	Ushort 	usEMVVerifyPIN(unsigned char byPINVerifyType, unsigned char  * bypPINData, unsigned char  byDataLen);

	Ushort 	usEMVSetPinParams(srPINParams *psKeypadSetup);
	
	Ushort 	usEMVGetPinParams(srPINParams *psKeypadSetup);

	void 	usEMVGetVersion(unsigned char* ucVerBuf);

	void 	usEMVResetLog(void);

	//Soumya_G1 28/Feb/07 - Common Module Implementation - The france & china 
	//set api for call back function pointers.
	unsigned short usEMVFR_Set_Functionality(unsigned short usCode,void(*fnPtrSet));
	
	unsigned short usEMVCN_Set_Functionality(unsigned short usCode);

    //Pavan_K1 dated 28-Feb-07 Added the set API for call back function pointers. 
    //Common module implementation.
	unsigned short usEMV_Set_Functionality(unsigned short usCode, void (*fnPtrSet));
	
	//SCPINBYPASS:
	//Added a wrapper API so that application can access Bypass Flag to check whether 
	//the preceding PIN CVM is bypassed
	Ushort usEMVGetPINBypassFlag(void);

	unsigned short GetTxnState( void );//Codehook

	//Brabhu_H1 dated 24 July 2007
	//ENCPINBK: For made for onTimeID 13990 to display the deciphered PIN block
	//Purvin_P1 dated 13-Mar-07 Added to get Enciphered PIN block
	//void vEMVGetEncPINBlk(byte *pinblk);
	//unsigned short usEMVGetEncPINBlk(byte *pinblk);
	//Kishor_S1: date 29/07/2009
	//ECTXNFLW: Function created for ECTransaction check

	Ushort usECTransactionCheck(EMVResult usTAA);
	void iEMVDoSystemInitialize(void);
//kishor_s1 added on 4-4-08 for Mx-->Vx porting
//Used for loading the Tag Tables
int iEMVLoadTables (void);

//CDAMode
unsigned short usEMVSetCDAMode(unsigned short usCDAMode);
//End of CDAMode

void vdEMVClearSensitiveData(void);

//BKSPENABLE:
EMVResult usEMVSetBackSpaceKeyFnlty( int inFlag);

// Tisha_K1 14/Dec/2012
//NUM_PIN_DIGITS
unsigned short usEMVGetEchoCount(void);

// EXT_VER
void usEMVGetExtendedVersion(unsigned char* ucVerBuf);

// Tisha_K1 12/Aug/2010
// VFITAGFIX : Fix to maintain the VeriFone Proprietary tag values
// stored seperately from the global TLV collection.
unsigned short usEMVSetKernelTags(unsigned short usTag, unsigned short usLen, unsigned char* pucValue);
unsigned short usEMVGetKernelTags(unsigned short usTag, unsigned char* pucValue, unsigned short* pusLen);

//End: VFITAGFIX

#ifdef MULTIAPP_SECURE_PIN

//MULTIAPP_PIN Changes :Added for SAKO

// tisha 01/Jun/05
// Added one function pointer for Secure PIN handling
typedef unsigned short (*FnPtrSetSecurePIN)
					(unsigned char *ucICCPK, 
					 unsigned short usICCPkLen, 
					 unsigned char *ucChallengeData,
					 srPINParams *srParams, 
					 int iICCExp,
					 short shPinTryCounter,
					 char cvmType) ;

// tisha 30 Jun 05
	// added the function to set the function pointer for PINIF library
	void usEMVSetSecurePINFnPtr(FnPtrSetSecurePIN);
	
#endif

#ifdef  __cplusplus
}
#endif




#endif
