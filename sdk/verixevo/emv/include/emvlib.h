/*
 * FILENAME       : emvlib.h
 * PRODUCT        : Verix/Vx.
 * VERSION        : 4.0.0
 * AUTHOR(s)      : Pavan_k1
 * CREATED        : 11/Dec/06
 *
 * DESCRIPTION    : 
 *
 * CONTENTS   :
 * MODIFICATION HISTORY    :
 *
 * #    Date        Who         History
 * ---  --------    ----------  ----------------------------------------
 * 1    11 Dec 06   Pavan_k1    Created for Vx EMV Module 4.0.0
 *
 * Copyright (C) 2006 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language, in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual or otherwise, without the prior written
 * permission of VeriFone Inc.
 *                                                2099 Gateway Place
 *                                                  Suite 600
 *                                                  San Jose  CA  95110 
 *                                                  USA
 *
 *
 *
 */
#ifndef EMVLIB_H
#define EMVLIB_H


// Structure used to keep all necessary EMV configuration data after it has
// been assigned by scheme/issuer/acquirer

#define MAX_DEF_TDOL            255
#define MAX_DEF_DDOL            255
#define MAX_ISS_SCRIPT_RES      25        // Max size of Issuer Script Results (5 scripts processed)


#ifndef __BYTE__
#define __BYTE__
typedef unsigned char     BYTE;
#endif

typedef struct _EMVconfig
{
    // Info needed for Random Online Selection
    unsigned long floor_limit;
    unsigned long threshold;      // Threshold Value for Biased random Selection.
    int  random_select_pc;        // Target Percentage for Random Selection.
    int  max_biased_pc;           // Maximum Target Percentage.

    byte TACdenial[5];           // Terminal Action Codes
    byte TAConline[5];
    byte TACdefault[5];

    byte DefaultTDOL[MAX_DEF_TDOL+1]; // Includes length BYTE
    byte DefaultDDOL[MAX_DEF_DDOL+1]; // Includes length BYTE

    byte AutoSelect;     // not 0 => don't ask user to select application,
                         // just take appl. with highest priority.
    byte EasyEntry;      // Is this an Easy Entry transaction?

    byte IssuerScriptResults[MAX_ISS_SCRIPT_RES + 1];   // This is not config. data, but it has no EMV tag
                                                        // so put it in global structure
} EMV_CONFIG;

#define FLOOR_LIMIT       EMVGlobConfig.floor_limit
#define RS_THRESHOLD      EMVGlobConfig.threshold
#define RS_TARGET_PERCENT EMVGlobConfig.random_select_pc
#define RS_MAX_PERCENT    EMVGlobConfig.max_biased_pc
#define TACDENIAL         EMVGlobConfig.TACdenial
#define TACONLINE         EMVGlobConfig.TAConline
#define TACDEFAULT        EMVGlobConfig.TACdefault
#define DEFAULT_TDOL      EMVGlobConfig.DefaultTDOL
#define DEFAULT_DDOL      EMVGlobConfig.DefaultDDOL
#define EASY_ENTRY        EMVGlobConfig.EasyEntry

#define EMV_MAX_ISSUER_AUTH_SIZE    16

#define ISS_SCRIPT_RES    EMVGlobConfig.IssuerScriptResults

#endif
