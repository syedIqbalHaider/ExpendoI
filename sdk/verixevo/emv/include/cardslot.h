/*-----------------------------------------------------------------------------
 *
 *      Filename:       CardSlot.h
 *
 *      Product:        Card Slot Library
 *      Version:        1.00
 *      Author :        Vipul_K1
 *  	Module :	
 *          Defines all methods pertaining to CardSlot class.
 *
 * MODIFICATION HISTORY    :
 *
 * #      Date		    Who			History
 * ---  ---------    ---------		----------------------------------------
 * 1.   15 June 00    Vipul_K1		Created.
 * 
 * 2.	25 July 00	  Vipul_K1		Added calls for MAM support.
 *
 * 3.	1  Oct  03	  Asha_S2		Changed the #defines for Verix V support 
 *
 * 4.   21 June 04	  Asha_s2		LONG_APDU:Fix for clarify issue 040407-62 of not supporting
 *									APDUs > 254
 *
 * 5.   9  Nov  06    Pavan_K1      Added a macro CARDSLOT_VERSION
 *
 * 6.   5  Jan  07    Brabhu_H1     Included the GetVersion_CardSlot() for C compilation 
 *                                  support
 * 7.   17 Mar 07     Purvin_P1     Version change from 1.8.0.01 to 1.9.0.01
 *
 * 8.   14 Oct 09	  Mary_F1	    Changed the version number to 2.0.0.02
 *
 * 9.   14/Oct/09  Soumya_G1        Changed Ver # from 2.0.0.01 to 2.1.0.01 to add Sync card 
 *                                                          driver AT24C64A.
 *
 *10.   18/Oct/09	  Mary_F1       Changed the version number to 2.1.1.01                                                 
 *11.   12/May/10	  Kishor_S1     Changed the version number to 2.1.2.01                                                  
 *
 *
 *******************************************************************************
 * Copyright (C) 2000 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language 
 * or computer language, in any form or by any means, electronic, mechanical, 
 * magnetic, optical, chemical, manual or otherwise, without the prior written 
 * permission of VeriFone Inc.
 *                                                VeriFone Inc.               
 *                                                III Lagoon Drive, Suite 400  
 *                                                Redwood City, CA 94065      
 ******************************************************************************
 */

#ifndef _CARD_SLOT
#define _CARD_SLOT   

#ifdef  __cplusplus

#include <NAD.h>
#include <ATR.h>

#endif

#include <libvoy.h>
#if !defined( __LIBVOY__ )

#ifndef BYTE_COUNT  // ACT 2000 also defines BYTE
#ifndef __BYTE__
#define __BYTE__
typedef unsigned char     BYTE;
#endif
#endif // BYTE_COUNT

#ifndef __WORD__
#define __WORD__
typedef unsigned int      WORD; 
#endif
#ifndef __DWORD__
#define __DWORD__
typedef unsigned long     DWORD;
#endif
#ifndef __RESPONSECODE__
#define __RESPONSECODE__
typedef DWORD   RESPONSECODE;
#endif

#define STANDARD_EMV311					(DWORD)0x0000
#define ISO_7816								(DWORD)0x0001
#define VISACASH							(DWORD)0x0002
#define NO_STANDARD                                       (DWORD)0x0003
#define NOT_ISO_38400                                     (DWORD)0x0004
#define CHINA_SOCIAL                                       (DWORD)0x0005


#define CLASS_A									(DWORD)0x0001
#define CLASS_B									(DWORD)0x0002
#define CLASS_AB								(DWORD)0x0003
#define VOLT1_8									(DWORD)0x0004

#define MANUAL_PTS								(DWORD)0x0001
#define AUTO_PTS								(DWORD)0x0002
#define NO_PTS									(DWORD)0x0003

#define ASYNC									(DWORD)0x00
#define ASYNC_7816								(DWORD)0x01
#define GEM256									(DWORD)0x03
#define GEM416									(DWORD)0x04
#define GEM2K4K									(DWORD)0x06
#define GEM103									(DWORD)0x07
#define GEM8K									(DWORD)0x08
#define GEM2K									(DWORD)0x09
#define GEM276									(DWORD)0x0D
#define GEM275									(DWORD)0x0D
#define GEM271									(DWORD)0x0E
#define GEM273									(DWORD)0x0E
#define GEM226									(DWORD)0x0F
#define GEM326									(DWORD)0x0F
#define GEM896									(DWORD)0x14
#define SLE4463									(DWORD)0x0101

/*#ifdef __thumb
#define CUSTOMER_CARD						(DWORD)0x00000001
#define MERCHANT_SLOT_1						(DWORD)0x00000002
#define MERCHANT_SLOT_2						(DWORD)0x00000003
#define MERCHANT_SLOT_3						(DWORD)0x00000004
#define MERCHANT_FULL_SIZE_CARD				(DWORD)0x00000000
#define MERCHANT_SLOT_4						(DWORD)0x00000000
#endif

#ifdef _TARG_68000
#define CUSTOMER_CARD						(DWORD)0x00000001
#define MERCHANT_FULL_SIZE_CARD				(DWORD)0x00000002
#define MERCHANT_SLOT_1						(DWORD)0x00000004
#define MERCHANT_SLOT_2						(DWORD)0x00000008
#define MERCHANT_SLOT_3						(DWORD)0x00000010
#define MERCHANT_SLOT_4						(DWORD)0x00000020
#endif*/

#define Tag_Logical_Handle         						(DWORD)0x020C
#define Tag_Vendor_Name         						(DWORD)0x0100
#define Tag_Vendor_IFD_Type  							(DWORD)0x0101
#define Tag_Vendor_IFD_Version                      	(DWORD)0x0102
#define Tag_IFD_Serial_Number 							(DWORD)0x0103
#define Tag_Channel_ID                              	(DWORD)0x0110
#define Tag_Asynch_protocol_types                   	(DWORD)0x0120
#define Tag_Default_CLK                             	(DWORD)0x0121
#define Tag_Max_CLK                                 	(DWORD)0x0122
#define Tag_Default_Data_Rate                       	(DWORD)0x0123
#define Tag_Max_Data_Rate                           	(DWORD)0x0124
#define Tag_Max_IFSD                                	(DWORD)0x0125
#define Tag_Synch_protocol_types                    	(DWORD)0x0126
#define Tag_Power_Management                        	(DWORD)0x0131
#define Tag_UserToCard_Auth_Device                  	(DWORD)0x0140
#define Tag_User_Auth_Device                        	(DWORD)0x0142
#define Tag_Mechanical_Characteristics              	(DWORD)0x0150
#define Tag_Open_SCard_Reader                       	(DWORD)0x0180
#define Tag_Close_SCard_Reader                      	(DWORD)0x0181
#define Tag_Open_ICC                                	(DWORD)0x0188
#define Tag_Close_ICC                               	(DWORD)0x0189
#define Tag_Select_ICC                              	(DWORD)0x0190
#define Tag_Nad_Management                          	(DWORD)0x0191
#define Tag_Convention                              	(DWORD)0x0192
#define Tag_WTX_Management                          	(DWORD)0x0193
#define Tag_Class_Management                        	(DWORD)0x0194
#define Tag_Pts_Management                          	(DWORD)0x0195
#define Tag_Error_Management							(DWORD)0x0196
#define Tag_Standard									(DWORD)0x0197
#define Tag_Card_Type                        			(DWORD)0x01A0
#define Tag_ICC_Structure								(DWORD)0x01B0
#define Tag_ICC_Structure_Size							(DWORD)0x01B2

#define Tag_ICC_Presence                    			(DWORD)0x0300
#define Tag_ICC_Interface_Status                    	(DWORD)0x0301
#define Tag_ATR_String                    				(DWORD)0x0303
#define Tag_ICC_Type                                	(DWORD)0x0304
#define Tag_ATR_Length                              	(DWORD)0x0305

#define Tag_Current_Protocol_Type                   	(DWORD)0x0201
#define Tag_Current_CLK                             	(DWORD)0x0202
#define Tag_Current_F                               	(DWORD)0x0203
#define Tag_Current_D                               	(DWORD)0x0204
#define Tag_Current_N                               	(DWORD)0x0205
#define Tag_Current_W                               	(DWORD)0x0206
#define Tag_Current_IFSC                            	(DWORD)0x0207
#define Tag_Current_IFSD                            	(DWORD)0x0208
#define Tag_Current_BWT                             	(DWORD)0x0209
#define Tag_Current_CWT                             	(DWORD)0x020A
#define Tag_Current_EBC                             	(DWORD)0x020B


#define READBYTE_GENERIC(buf, st, len) {				\
	memset(buf,0x00,8);									\
	buf[7]=0x05;										\
	buf[8]=0x00; buf[9]=0xB0; buf[10]=0x00;				\
	buf[11]=(st); buf[12]=(len);						\
	}    

#define WRITEBYTE_GENERIC(buf, st, len, data) {			\
	memset(buf,0x00,8);									\
	buf[7]=(len)+5;										\
	buf[8]=0x00; buf[9]=0xD0; buf[10]=0x00;				\
	buf[11]=(st); buf[12]=(len);						\
	memcpy(&buf[13],data,(len));						\
	}


#define READBYTE_GEM256(buf, st, len)	READBYTE_GENERIC(buf, st, len)

#define READBYTE_GEM416(buf, st, len)   READBYTE_GENERIC(buf, st, len)

#define WRITEBYTE_GEM416(buf, st, len, data)	WRITEBYTE_GENERIC(buf, st, len, data)

#define ERASEWORD_GEM416(buf, st, len) {				\
	memset(buf,0x00,8);									\
	buf[7]=5;											\
	buf[8]=0x00; buf[9]=0xDE; buf[10]=(len);			\
	buf[11]=(st); buf[12]=0x00;							\
	}

#define PRESENTCODE_GEM416(buf, data) {					\
	memset(buf,0x00,8);									\
	buf[7]=2+5;											\
	buf[8]=0x00; buf[9]=0x20; buf[10]=0x04;				\
	buf[11]=0x08; buf[12]=0x02;							\
	memcpy(&buf[13],data,2);							\
	}


#define PRESENTECODE_GEM416(buf, data) {				\
	memset(buf,0x00,8);									\
	buf[7]=4+5;											\
	buf[8]=0x00; buf[9]=0x20; buf[10]=0x40;				\
	buf[11]=0x28; buf[12]=0x04;							\
	memcpy(&buf[13],data,4);							\
	}

#define CHANGEFUSESTATE_GEM416(buf) {					\
	memset(buf,0x00,8);									\
	buf[7]=5;											\
	buf[8]=0x00; buf[9]=0xD4; buf[10]=0x00;				\
	buf[11]=0x00; buf[12]=0x00;							\
	}


#define READBYTE_GEM896(buf, st, len)	READBYTE_GENERIC(buf, st, len)

#define WRITEBYTE_GEM896(buf, st, len, data)	WRITEBYTE_GENERIC(buf, st, len, data)

#define ERASEWORD_GEM896(buf, st, len) {				\
	memset(buf,0x00,8);									\
	buf[7]=5;											\
	buf[8]=0x00; buf[9]=0xDE; buf[10]=(len);			\
	buf[11]=(st); buf[12]=0x00;							\
	}

#define PRESENTCODE_GEM896(buf, data) {					\
	memset(buf,0x00,8);									\
	buf[7]=2+5;											\
	buf[8]=0x00; buf[9]=0x20; buf[10]=0x04;				\
	buf[11]=0x0A; buf[12]=0x02;							\
	memcpy(&buf[13],data,2);							\
	}



#define PRESENTECODE1_GEM896(buf, data) {				\
	memset(buf,0x00,8);									\
	buf[7]=6+5;											\
	buf[8]=0x00; buf[9]=0x20; buf[10]=0x00;				\
	buf[11]=0x36; buf[12]=0x06;							\
	memcpy(&buf[13],data,6);							\
	}

#define PRESENTECODE2_GEM896(buf, data) {				\
	memset(buf,0x00,8);									\
	buf[7]=4+5;											\
	buf[8]=0x00; buf[9]=0x20; buf[10]=0x80;				\
	buf[11]=0x5C; buf[12]=0x04;							\
	memcpy(&buf[13],data,4);							\
	}

#define CHANGEFUSESTATE_GEM896(buf) {					\
	memset(buf,0x00,8);									\
	buf[7]=5;											\
	buf[8]=0x00; buf[9]=0xD4; buf[10]=0x00;				\
	buf[11]=0x00; buf[12]=0x00;							\
	}

#define READBYTE_GEM2K4K(buf, stH, stL, len) {			\
	memset(buf,0x00,8);									\
	buf[7]=0x05;										\
	buf[8]=0x00; buf[9]=0xB0; buf[10]=(stH);			\
	buf[11]=(stL); buf[12]=(len);						\
	}

#define WRITEBYTE_GEM2K4K(buf, stH, stL, len, data) {	\
	memset(buf,0x00,8);									\
	buf[7]=(len)+5;										\
	buf[8]=0x00; buf[9]=0xD0; buf[10]=(stH);			\
	buf[11]=(stL); buf[12]=(len);						\
	memcpy(&buf[13],data,(len));						\
	}

#define READBYTE_GEM103(buf, st, len)   READBYTE_GENERIC(buf, st, len)

#define WRITEBYTE_GEM103(buf, st, len, data)	WRITEBYTE_GENERIC(buf, st, len, data)

#define READCNTR_GEM103(buf) {							\
	memset(buf,0x00,8);									\
	buf[7]=5;											\
	buf[8]=0x00; buf[9]=0xB2; buf[10]=0x05;				\
	buf[11]=0x08; buf[12]=0x02;							\
	}

#define WRITECNTR_GEM103(buf, data) {					\
	memset(buf,0x00,8);									\
	buf[7]=5+2;											\
	buf[8]=0x00; buf[9]=0xD2; buf[10]=0x05;				\
	buf[11]=0x08; buf[12]=0x02;							\
	memcpy(&buf[13],data,2);							\
	}

#define ERASEWRITECARRY_GEM103(buf, st) {				\
	memset(buf,0x00,8);									\
	buf[7]=0x05;										\
	buf[8]=0x00; buf[9]=0xE0; buf[10]=0x01;				\
	buf[11]=(st); buf[12]=00;							\
	}


#define READBYTE_GEM8K(buf, stH, stL, len)	READBYTE_GEM2K4K(buf, stH, stL, len)

#define WRITEBYTE_GEM8K(buf, stH, stL, len, data)	WRITEBYTE_GEM2K4K(buf, stH, stL, len, data)

#define PRESENTCODE_GEM8K(buf, data) {					\
	memset(buf,0x00,8);									\
	buf[7]=2+5;											\
	buf[8]=0x00; buf[9]=0x20; buf[10]=0x00;				\
	buf[11]=0x00; buf[12]=0x02;							\
	memcpy(&buf[13],data,2);							\
	}

#define READPROTAREA_GEM8K(buf, st) {					\
	memset(buf,0x00,8);									\
	buf[7]=0x05;										\
	buf[8]=0x00; buf[9]=0xB0; buf[10]=(st)+0x80;		\
	buf[11]=0x00; buf[12]=0x20;							\
	}    

#define WRITEPROTAREA_GEM8K(buf, stH, stL, data) {		\
	memset(buf,0x00,8);									\
	buf[7]=1+5;											\
	buf[8]=0x00; buf[9]=0xD0; buf[10]=(stH)+0x80;		\
	buf[11]=(stL); buf[12]=0x01; buf[13]=(data);		\
	}


#define READSECAREA_GEM8K(buf) {						\
	memset(buf,0x00,8);									\
	buf[7]=0x05;										\
	buf[8]=0x00; buf[9]=0xB0; buf[10]=0xC0;				\
	buf[11]=0x00; buf[12]=0x03;							\
	}    

#define WRITESECAREA_GEM8K(buf, st, len, data) {		\
	memset(buf,0x00,8);									\
	buf[7]=(len)+5;										\
	buf[8]=0x00; buf[9]=0xD0; buf[10]=0xC0;				\
	buf[11]=(st); buf[12]=(len);						\
	memcpy(&buf[13],data,(len));						\
	}

#define READBYTE_GEM2K(buf, st, len)   READBYTE_GENERIC(buf, st, len)

#define WRITEBYTE_GEM2K(buf, st, len, data)	WRITEBYTE_GENERIC(buf, st, len, data)

#define READPROTAREA_GEM2K(buf) {						\
	memset(buf,0x00,8);									\
	buf[7]=0x05;										\
	buf[8]=0x00; buf[9]=0xB0; buf[10]=0x80;				\
	buf[11]=0x00; buf[12]=0x04;							\
	}    

#define WRITEPROTAREA_GEM2K(buf, st, len, data) {		\
	memset(buf,0x00,8);									\
	buf[7]=(len)+5;										\
	buf[8]=0x00; buf[9]=0xD0; buf[10]=0x80;				\
	buf[11]=(st); buf[12]=(len); 						\
	memcpy(&buf[13],data,(len));						\
	}

#define READSECAREA_GEM2K(buf) {						\
	memset(buf,0x00,8);									\
	buf[7]=0x05;										\
	buf[8]=0x00; buf[9]=0xB0; buf[10]=0xC0;				\
	buf[11]=0x00; buf[12]=0x04;							\
	}    

#define WRITESECAREA_GEM2K(buf, st, len, data) WRITESECAREA_GEM8K(buf, st, len, data)

#define PRESENTCODE_GEM2K(buf, data) {					\
	memset(buf,0x00,8);									\
	buf[7]=3+5;											\
	buf[8]=0x00; buf[9]=0x20; buf[10]=0x00;				\
	buf[11]=0x00; buf[12]=0x03;							\
	memcpy(&buf[13],data,3);							\
	}

#define READBYTE_GEM276(buf, st, len)   READBYTE_GENERIC(buf, st, len)

#define WRITEBYTE_GEM276(buf, st, len, data)	WRITEBYTE_GENERIC(buf, st, len, data)

#define ERASEWRITECARRY_GEM276(buf, st) {				\
	memset(buf,0x00,8);									\
	buf[7]=0x04;										\
	buf[8]=0x00; buf[9]=0xE0; buf[10]=0x01;				\
	buf[11]=(st);										\
	}

#define PRESENTCODE_GEM276(buf, data) {					\
	memset(buf,0x00,8);									\
	buf[7]=3+5;											\
	buf[8]=0x00; buf[9]=0x20; buf[10]=0x00;				\
	buf[11]=0x00; buf[12]=0x03;							\
	memcpy(&buf[13],data,3);							\
	}


#define RESTORE_GEM276(buf) {							\
	memset(buf,0x00,8);									\
	buf[7]=4;											\
	buf[8]=0x00; buf[9]=0xD4; buf[10]=0x00;				\
	buf[11]=0x00;										\
	}

#define BLOWFUSE_GEM276(buf) {							\
	memset(buf,0x00,8);									\
	buf[7]=4;											\
	buf[8]=0x00; buf[9]=0xDA; buf[10]=0x00;				\
	buf[11]=0x00;										\
	}

#define READBYTE_GEM271(buf, st, len)   READBYTE_GENERIC(buf, st, len)

#define WRITEBYTE_GEM271(buf, st, len, data)	WRITEBYTE_GENERIC(buf, st, len, data)

#define ERASEWRITECARRY_GEM271(buf, st) ERASEWRITECARRY_GEM276(buf, st)

#define PRESENTCODE_GEM271(buf, data) PRESENTCODE_GEM276(buf, data)

#define RESTORE_GEM271(buf) RESTORE_GEM276(buf)

#define BLOWFUSE_GEM271(buf) BLOWFUSE_GEM276(buf)

#define READBYTE_GEM226(buf, st, len)   READBYTE_GENERIC(buf, st, len)

#define WRITEBYTE_GEM226(buf, st, len, data)	WRITEBYTE_GENERIC(buf, st, len, data)

#define ERASEWRITECARRY_GEM226(buf, st) ERASEWRITECARRY_GEM276(buf, st)

#define PRESENTCODE_GEM226(buf, data) PRESENTCODE_GEM276(buf, data)

#define RESTORE_GEM226(buf) RESTORE_GEM276(buf)

#define AUTHENTICATE_GEM226(buf, data) {				\
	memset(buf,0x00,8);									\
	buf[7]=6+5+1;										\
	buf[8]=0x00; buf[9]=0x88; buf[10]=0x01;				\
	buf[11]=0xA0; buf[12]=0x06;							\
	memcpy(&buf[13],data,6);							\
	buf[19] = 0x02;										\
	}

#define READBYTE_GEM326(buf, st, len)   READBYTE_GENERIC(buf, st, len)

#define WRITEBYTE_GEM326(buf, st, len, data)	WRITEBYTE_GENERIC(buf, st, len, data)

#define ERASEWRITECARRY_GEM326(buf, st) ERASEWRITECARRY_GEM276(buf, st)

#define PRESENTCODE_GEM326(buf, data) PRESENTCODE_GEM276(buf, data)

#define RESTORE_GEM326(buf) RESTORE_GEM276(buf)

#define AUTHENTICATE_GEM326(buf, data) AUTHENTICATE_GEM226(buf, data)

#define READBYTE_GEM273(buf, st, len)   READBYTE_GENERIC(buf, st, len)

#define WRITEBYTE_GEM273(buf, st, len, data)	WRITEBYTE_GENERIC(buf, st, len, data)

#define ERASEWRITECARRY_GEM273(buf, st) ERASEWRITECARRY_GEM276(buf, st)

#define PRESENTCODE_GEM273(buf, data) PRESENTCODE_GEM276(buf, data)

#define AUTHENTICATE_GEM273(buf, data) {				\
	memset(buf,0x00,8);									\
	buf[7]=4+5+1;										\
	buf[8]=0x00; buf[9]=0x88; buf[10]=0x00;				\
	buf[11]=0x00; buf[12]=0x04;							\
	memcpy(&buf[13],data,4);							\
	buf[17] = 0x01;										\
	}

#define RESTORE_GEM273(buf) RESTORE_GEM276(buf)

#define BLOWFUSE_GEM273(buf) BLOWFUSE_GEM276(buf)

#define READBYTE_GEM275(buf, st, len)   READBYTE_GENERIC(buf, st, len)

#define WRITEBYTE_GEM275(buf, st, len, data)	WRITEBYTE_GENERIC(buf, st, len, data)

#define ERASEWRITECARRY_GEM275(buf, st) ERASEWRITECARRY_GEM276(buf, st)

#define PRESENTCODE_GEM275(buf, data) PRESENTCODE_GEM276(buf, data)

#define AUTHENTICATE_GEM275(buf, data) AUTHENTICATE_GEM273(buf, data)

#define RESTORE_GEM275(buf) RESTORE_GEM276(buf)

#define BLOWFUSE_GEM275(buf) BLOWFUSE_GEM276(buf)

#endif // __LIBVOY__ 


// Added to get the version number of the CardSlot
#define CARDSLOT_VERSION 						"3.0.0.02"
/*********************************************************************/

// New Slot creation
#define O_CREATE				(char) 1
#define O_NOCREATE				(char) 0
/*********************************************************************/

#define CARDSLOT_SUCCESS        (char) 1
#define CS_SUCCESS				(char) 20
/*********************************************************************/

// Card Status
#define CARD_PRESENT  			(char) 21
#define CARD_ABSENT				(char) 22
/*********************************************************************/

// Protocol types
#define PROTOCOL_T0  			(char) 23
#define PROTOCOL_T1				(char) 24
#define PROP_PROTOCOL			(char) 25
/*********************************************************************/

// Reset types
#define RESET_COLD				(char) 26
#define RESET_WARM				(char) 27
#define RESET					(char) 28
/*********************************************************************/

// Card termination func
#define EJECT_THE_CARD			(char) 29
#define SWALLOW_THE_CARD		(char) 30
#define CONFISCATE_THE_CARD		(char) 31
#define SWITCH_OFF_CARD			(char) 32
/*********************************************************************/

#define CARDSLOT_ERROR          (char) -1
/*********************************************************************/

// Error Codes
#define E_DEVICE_SPECIFIC		(char) -21	// SW1 BYTE returned  
#define E_CARDSLOT_SPECIFIC		(char) -22
/*********************************************************************/

#define E_READER_NO				(char) -23
#define E_SLOT_SELECT			(char) -24
#define E_PROTOCOL				(char) -25
#define E_TIME_OUT				(char) -26
#define E_NUL_RESP_BUF	        (char) -27
#define E_ICC_RESET				(char) -28	
#define E_ICC_HIST_BYTES		(char) -29
#define E_ICC_IF_BYTES			(char) -30
#define E_ATR					(char) -31
#define E_SLOT_INITIALIZE		(char) -32
#define E_TRANSMIT				(char) -33

#define E_CLASS_SELECTION       (char) -34
#define E_PTSMGT                (char) -35
#define E_DEFMODULE             (char) -36
#define E_CARDTYPE              (char) -37

#define E_TAG					(char) -38
#define E_NOT_SUPPORTED			(char) -39
#define E_SET_FAILURE			(char) -40
#define E_READ_ONLY				(char) -41
/*********************************************************************/

#define E_SYNC_FILE_NOT_FOUND	(char) -42
#define E_NO_SDM_LOADED     	(char) -44	

#define E_SIM_FILE_NOT_FOUND    (char) -46
#define E_PTS_FAILURE			(char) -47

#define CARDSLOT_TRUE			1
#define CARDSLOT_FALSE			0

// Purvin_P1 added Return type for E_SERVICE_ALARM_SET
#define E_SERVICE_ALARM_SET			    33

#ifdef  __cplusplus

//Data structure to store the status BYTEs from the ICC
#ifdef _WINDOWS

	typedef union {
				struct {
                   BYTE SW2;	//  order Intel
                   BYTE SW1;
                }SWBytes;
                BYTE ucSw1Sw2[2];
                unsigned short  SW1SW2;
	}StatusBytes;

#else

	typedef union {
				struct {
                   BYTE SW1;	//  Motorola - Hardware testing
                   BYTE SW2;
                }SWBytes;
                BYTE ucSw1Sw2[2];
                unsigned short SW1SW2;
	}StatusBytes;

#endif


    typedef struct {
        DWORD           protocolUsed;
        DWORD           cmdLen;
    }SCARD_IO_HEADER;

    typedef struct {
        SCARD_IO_HEADER scIOHeader;
        //BYTE            commandData[256];
//LONG_APDU
//asha_S2 dated 21 June 04
//fix for clarify issue: 040407-62
//Issue: not supporting APDUs > 254
		BYTE            commandData[270];
    } VOYAGER_CARD_CMD;

    typedef struct {
        WORD    respLen;
      //  BYTE    respData[256];
//LONG_APDU
//asha_S2 dated 21 June 04
//fix for clarify issue: 040407-62
//Issue: not supporting APDUs > 254
		  BYTE    respData[270];
    } VOYAGER_CARD_RESP;


class CardSlot {

private:	
	static CardSlot* custCardInstance;		
	static CardSlot* mercCardInstance;		
	static CardSlot* mercSlot1Instance;		
	static CardSlot* mercSlot2Instance;		
	static CardSlot* mercSlot3Instance;		
	static CardSlot* mercSlot4Instance;		
	static CardSlot* extCustCardInstance;		
	static CardSlot* extMercCardInstance;		
	static CardSlot* extMercSlot1Instance;		
	static CardSlot* extMercSlot2Instance;		
	static CardSlot* extMercSlot3Instance;		
	static CardSlot* extMercSlot4Instance;		

	static BYTE initDone ;			// for checking initialization. 

	BYTE	retryLimit;				// Number of times reset() should be 
									// tried for the Reader.
	char	cardState;				// Card status will  be stored. 
									// valid values - CARD_PRESENT, CARD_ABSENT								
	DWORD	resetState;				// stores the result of reset()
									// method.
	DWORD	cardType;				// can be ASYNC or any of the
									// SYNC types
    DWORD   classSelection;			// should be CLASS_A, CLASS_B, CLASS_AB
									// or VOLT1_8
    DWORD   ptsMgmt;				// should be MANUAL_PTS, AUTO_PTS
									// or NO_PTS (protocol management)
    DWORD   defModule;				// should be GemCore_Module or
									// Saturn_Module
	DWORD   iccSlot;                // refers to one of the six
									// card slots
    ATR		*atr;					// class storing the data pertaining
									// to Answer To Reset
    StatusBytes statusBytes;        // Used to store the status BYTEs 

    VOYAGER_CARD_CMD    voyagerCmd;
    VOYAGER_CARD_RESP   voyagerResp;

//SYNC CARD SUPPORT:
//asha_s2 dated 1 June 05
//change CardSlot library to support synchronous cards
    DWORD syncCardType;
    char  syncCardDescriptor[10];
	char  syncCardVersionNo[4];
    char  SDMFileName[32];

	bool   bExtSlot;                // refers to one of the six

#ifdef CS_SHORT_RETVAL
 
#ifdef _WINDOWS

	typedef union {
				struct {
                   BYTE CSError2;	//  order Intel
                   BYTE CSError1;
                }CSErrorBYTEs;                
                BYTE CSE1E2[2];
                unsigned short CSError;
	}CardSlotBYTEs;

#else

	typedef union {
				struct {
                   BYTE CSError1;	//  Motorola - Hardware testing
                   BYTE CSError2;
                }CSErrorBYTEs;                
                BYTE CSE1E2[2];
                unsigned short CSError;
	}CardSlotBYTEs;
#endif

CardSlotBYTEs	cardSlotBYTEs;

#endif // CS_SHORT_RETVAL

// Methods:

	// Constructor
    CardSlot(DWORD rdrNo, DWORD classSeln, DWORD ptsMngt, DWORD defnMod);
    CardSlot(DWORD rdrNo);
    CardSlot();// will initialize customer card slot by default

	int Simulate(VOYAGER_CARD_RESP * resp,VOYAGER_CARD_CMD *cmd ) ;
	int Simulate(VOYAGER_CARD_RESP * resp) ;
	
	void		initCardSlot(); 
	
#ifdef CS_SHORT_RETVAL
	short		deviceError(char status); // use if error status needs to be 2 BYTEs
	short		cardSlotError(char error); // use if error status needs to be 2 BYTE
#endif // CS_SHORT_RETVAL


private: 
	
    DWORD		issueATR(char resetType);
	static		char clearSlotInstance(DWORD rdrNo);
    char		setICC(DWORD reader);
    void		fillRespBuf(unsigned short length);
	char		fillResetInfo();
public:
  
    DWORD		selectICC();
	char		setCardType(DWORD cardType, DWORD cardStandard);
	char		getCapability(DWORD tag, BYTE* value);
	char		setCapability(DWORD tag, BYTE* value);
    ATR*		getATR();	
    DWORD		getProtocol();
    char		setParams(DWORD classSeln,DWORD ptsMgt,DWORD defnModule);//end
	static		CardSlot* getCardSlot(DWORD rdrNo, char createNew);
	void		setHeaderAPDU(BYTE cla, BYTE ins, BYTE P1, BYTE P2, BYTE P3);
	char		reset(char resetType); 
	char		reset(char resetType, BYTE rtrLim );
    char		getState();
	char		reinit(BYTE* resetData);
    char		transmitAPDU(BYTE *txData, unsigned short txLen, BYTE *rxData, 
					unsigned short *rxLen);
    char		transmitAPDU(BYTE *txData, unsigned short txLen, BYTE *rxData, 
					unsigned short *rxLen, NAD *nad);  
    char		terminate(BYTE param);
    char		closeICC();
    char		closeReader();
    char        getSyncCardDetails(DWORD cardType);
    int         readRecord(int fHandle , char *buf);
    int        parseRecord(char *buf, int fldnum, unsigned char sep, char *dest);
    int         fieldray (char *buf, int start, char stop, char *dest);
	int			Sim(VOYAGER_CARD_RESP * resp,VOYAGER_CARD_CMD *cmd ) ;
	int			Simulate();
	int			IFDSet_Protocol_Parameters(DWORD protocolType, BYTE selectionFlags, BYTE pts1, BYTE pts2, BYTE pts3);

    ~CardSlot();
};

#endif


#ifndef __cplusplus


char Init_CardSlot(unsigned long slotNo);

char Set_Card_Type(unsigned long slotNo, unsigned long cardType, DWORD cardStandard );

char Get_Card_State(unsigned long slotNo);

char Set_Capability(unsigned long slotNo, unsigned long tag, unsigned char* value);

char Get_Capability(unsigned long slotNo, unsigned long tag, unsigned char* value);

char Reset_CardSlot(unsigned long slotNo, char resetType);

char Reinit_CardSlot(unsigned long slotNo, unsigned char *dataBuffer);

char Get_Protocol(unsigned long slotNo);

char Get_Interface_Bytes(unsigned long slotNo, unsigned char *IFbuff);

char Get_Historical_Bytes(unsigned long slotNo, unsigned char *Hbuff);//end,10th may 00 amarnath_s1

char Transmit_APDU(unsigned long slotNo, 
                            unsigned char *txData,
                            unsigned short txLen,
                            unsigned char *rxData,
                            unsigned short *rxLen);

//Added for NAD management.

char Transmit_APDU_NAD(unsigned long slotNo, 
					   unsigned char *txData, 
					   unsigned short txLen, 
					   unsigned char *rxData, 
					   unsigned short *rxLen, 
					   BYTE *sad, BYTE *dad);

char Terminate_CardSlot(unsigned long slotNo, char param);

char Close_CardSlot(unsigned long slotNo);

int Simulate_CS(unsigned long slotNo);
void Simulate_Close(void);

//Brabhu_H1 dated 05 Jan 07
//Included the function for C compilation support
void GetVersion_CardSlot(unsigned char *ucVersion);

char Set_Protocol_Parameters(unsigned long slotNo,DWORD protocolType, BYTE selectionFlags, BYTE pts1, BYTE pts2, BYTE pts3);


#else


extern "C" char Init_CardSlot(unsigned long slotNo);

extern "C" char Set_Card_Type(unsigned long slotNo, unsigned long cardType, DWORD cardStandard );

extern "C" char Get_Card_State(unsigned long slotNo);

extern "C" char Set_Capability(unsigned long slotNo, unsigned long tag, unsigned char* value);

extern "C" char Get_Capability(unsigned long slotNo, unsigned long tag, unsigned char* value);

extern "C" char Reset_CardSlot(unsigned long slotNo, char resetType);

extern "C" char Reinit_CardSlot(unsigned long slotNo, unsigned char *dataBuffer);

extern "C" char Get_Protocol(unsigned long slotNo);

extern "C" char Get_Interface_Bytes(unsigned long slotNo, unsigned char *IFbuff);

extern "C" char Get_Historical_Bytes(unsigned long slotNo, unsigned char *Hbuff);//end,10th may 00 amarnath_s1

extern "C" char Transmit_APDU(unsigned long slotNo, 
                            unsigned char *txData,
                            unsigned short txLen,
                            unsigned char *rxData,
                            unsigned short *rxLen);


extern "C" char Transmit_APDU_NAD(unsigned long slotNo,unsigned char *txData, unsigned short  txLen, unsigned char *rxData,   unsigned short *rxLen,   BYTE *sad, BYTE *dad);/**changed byte ---> BYTE****changed_sena***15-04-2003***/


extern "C" char Terminate_CardSlot(unsigned long slotNo, char param);

extern "C" char Close_CardSlot(unsigned long slotNo);

extern "C" int Simulate_CS(unsigned long slotNo);

extern "C" void Simulate_Close(void);

extern "C" void GetVersion_CardSlot(unsigned char *ucVersion);

extern "C" char Set_Protocol_Parameters(unsigned long slotNo, DWORD protocolType, BYTE selectionFlags, BYTE pts1, BYTE pts2, BYTE pts3);



#endif //__cplusplus


#endif // End of CardSlot.h

