/*
 * FILENAME       : VXEMVAP_define.h
 * PRODUCT        : Verix/Vx.
 * VERSION        : 4.0.0
 * AUTHOR(s)      : Pavan_k1
 * CREATED        : 12/Dec/06
 *
 * DESCRIPTION    : 
 *
 * CONTENTS   :
 * MODIFICATION HISTORY    :
 *
 * #    Date        Who         History
 * ---  --------    ----------  ----------------------------------------
 * 1    12 Dec 06   Pavan_k1    Created for Vx EMV Module 4.0.0
 *
 * Copyright (C) 2006 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language, in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual or otherwise, without the prior written
 * permission of VeriFone Inc.
 *                                                2099 Gateway Place
 *                                                  Suite 600
 *                                                  San Jose  CA  95110 
 *                                                  USA
 *
 *
 *
 */
#ifndef DEF_DEFINED
#define DEF_DEFINED

typedef char VS_BOOL;

#define VS_FALSE           ((VS_BOOL) 0)
#define VS_TRUE            ((VS_BOOL) 1)

#define VS_SUCCESS         0               /* General purpose error code */
#define VS_ERR             (-1)
#define VS_ERROR           (-1)            /* Operator error */
#define VS_FAILURE         (-2)            /* System-level error */
#define VS_ESCAPE          (-3)            /* Operator quit transaction */
#define CAPK_EXPIRED       (-4)           /*08/March/07 Soumya_G1 Added new return value from module*/

#define FHANDLE            int

#endif  /* DEF_DEFINED */


