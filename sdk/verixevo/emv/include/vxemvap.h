/*
 * FILENAME       : vxemvap.h
 * PRODUCT        : Verix/Vx.
 * VERSION        : 4.0.0
 * AUTHOR(s)      : Pavan_k1
 * CREATED        : 12/Dec/06
 *
 * DESCRIPTION    : 
 *
 * CONTENTS   :
 * MODIFICATION HISTORY    :
 *
 * #    Date        Who         History
 * ---  --------    ----------  ----------------------------------------
 * 1    12 Dec 06   Pavan_k1    Created for Vx EMV Module 4.0.0
 * 2    31 Jan 07   Pavan_K1    Removed the prototypes of the functions inVXEMVAPDoTrans,inVXEMVAPSaveROSParams
 *                              inVXEMVAPSetDisplayPINPrompt which are not used as a part of code clean up
 * 3    28 Feb 07   Pavan_K1    Added the function inVXEMVAPSetDisplayPINPrompt as it will be used in secure pin.
 * 
 * 4    8/March/07  Soumya_g1   Added the new function to validate the CAPK expiry date.
 * 5    25 Apr 07   Pavan_K1    Added the prototypes of all standard implementations.
 * 6    08 Aug 07   Pavan_K1    KEYREVCN: Added key revocation function prototype. 
 * 7    18 Feb 07   Mary_F1		ISRSCRPBO: Clarify Case ID: 080110-11
 *								Fix made to process issuer script, without making use of global buffer
 * 8    15 Apr 08 	Kishor_S1	Mx-->Vx porting 
 * 9     8 Jul 09	Kishor_S1	NOGPOINSEL : GPO & App selection seperation changes
 *10 	12/Jan/11	Ganesh_P1	POWER_MGMT: Power Management changes
 *11    03/Jun/11   Mary_F1     MULTIAVN: Support for multiple application version number check
 *12    03/Jun/11   Mary_F1 	ExtAuth6985: The EMV Kernel behavior is modified to cater to the different application needs,
 *								to terminate the transaction or to continue with the transaction on receiving the status word
 *								as 0x6985 for the external authenticate command.
 * 13	18/Aug/11	Kishor_S1	NAND_FLASH : Changes for Issuer Script processing accept through buffer instead of file
 * Copyright (C) 2006 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language, in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual or otherwise, without the prior written
 * permission of VeriFone Inc.
 *                                                2099 Gateway Place
 *                                                  Suite 600
 *                                                  San Jose  CA  95110 
 *                                                  USA
 *
 *
 *
 */
#ifndef _VXEMVAP_H
#define _VXEMVAP_H

#include <stdlib.h>
#include <svc.h>
#include <string.h>
#include <common.h>
#include <libvoy.h>
#include <cardslot.h>
#include <EMVCWrappers.h>
#include <EMVResult.hpp>
#include <emvTags.hpp>
#include <EMVTypeDefs.hpp>
#include <EMVCStructs.h>

#include "estfuncs.h"
#include "VXEMVAP_define.h"
#include "vxemvapdef.h"
#include "emvlib.h"
#include "emvsizes.h"
#include "est.h"
#include "mvt.h"
#include "mvtfuncs.h"

//POWER_MGMT : Ganesh_P1 Added 2 API's on 6-01-11 for Power Management changes

int inVXEMVAPInitCardslot(void);
int inVXEMVAPCloseCardslot(void);

// Highest level toolkit functions
unsigned short  usVXEMVAPGetVersion(char * szVersionNo); 
int             inVXEMVAPSCInit(void);
int             inVXEMVAPTransInit(int inTerminalRecord, int (*inGetTermID)(char *) );
int             inVXEMVAPCardPresent(void);
int             inVXEMVAPCardInit(void);
int             inVXEMVAPCheckFallback(char *szServiceCode, int (*inOverrideChipScreen)(void), void (*vdPromptManager)(unsigned short));
int             inVXEMVAPSelectApplication(int inSelectMethodFlag, int (*inMenuFunc) (char **, int), void (*vdPromptManager) (unsigned short), unsigned short (*usAmtEntryFunc) (unsigned long *), unsigned short (*usCandListModify)(srAIDList *psrCandidateList));

//NOGPOINSEL : kishor_S1 added on 1-7-09 for GPO & App selection seperation changes
int 			inVXEMVAPPerformGPO(void);
int				inAppSelectionAndGPO(unsigned short usSelectAppFlag);

//Mx-->Vx porting kishor_s1 added 15-04-08 addditional param
unsigned short  usEMVSelAppFromAIDList(int inAutoSelect, const srAIDList *psrCandidateList, int inOrigNumApps, int (*inMenuFunc) (char **, int), int inSuppNumApps);
//unsigned short  usEMVSelAppFromAIDList(int inAutoSelect, const srAIDList *psrCandidateList, int inOrigNumApps, int (*inMenuFunc) (char **, int));
int             inVXEMVAPProcessAFL(void);
int             inVXEMVAPDataAuthentication(unsigned short (*bIsCSNValid)(unsigned char *CardCSN));
int             inVXEMVAPProcRisk(EMVBoolean (*bIsCardBlackListed)(byte *,unsigned short,byte *,unsigned short));
int             inVXEMVAPVerifyCardholder(unsigned short (*bIsCSNValid)(unsigned char *CardCSN));
int             inVXEMVAPFirstGenerateAC(int inTerminalDecision );
//int             inVXEMVAPUseHostData(int inHostDecision, byte *pIssuerScripResults, int *pinNumScripts);
//NAND_FLASH changes
int 			inVXEMVAPUseHostData(int inHostDecision, byte *pIssuerScriptResults, int *pinNumScripts, byte* pScript71, unsigned short usScript71Len, byte* pScript72, unsigned short usScript72Len );
int             inVXEMVAPGetCardConfig(int inIssuerNumber, int inAquirerNumber);
int             inVXEMVAPRemoveCard(void (*vdPromptManager)(unsigned short));
int             inVXEMVAPEuropaySpecificTests(void);

// Lower level toolkit functions

int             inVXEMVAPProcRestrictions(void);
int             inVXEMVAPTermRisk(EMVBoolean (*bIsCardBlackListed)(byte *,unsigned short,byte *,unsigned short));

#ifdef __thumb
    int         inVXEMVAPGetCAPK(byte *pAID, byte pCAPKIndex, byte *pCAPK, short *pinCAPKLen, byte *pCAPKExp, short *pinCAPKExpLen);
#endif

#ifdef  _TARG_68000
    int         inVXEMVAPGetCAPK(byte *pAID, byte pCAPKIndex, byte *pCAPK, int *pinCAPKLen, byte *pCAPKExp, int *pinCAPKExpLen);
#endif
int             inVXEMVAPSecondGenerateAC(int inHostDecision);
int             inVXEMVAPProcessScript(byte *pScript, byte *pIssuerScriptResults);
int             inVXEMVAPAuthIssuer(void);
int             inVXEMVAPAPDU(byte *header, byte lc, byte *sendData, byte le, byte *pReceiveDataLen, byte *receivedData);
int             inVXEMVAPSetDefDDOL(byte *pDefDDOL, int inLenDefDDOL);
int             inVXEMVAPSetDefTDOL(byte *pDefTDOL, int inLenDefTDOL);
int             inVXEMVAPSetTACs(byte *pTACDecline, byte *pTACOnline, byte *pTACDefault);
int             inVXEMVAPSetROSParams(unsigned long ulFloorLimit, unsigned long ulRSThreshold, int inRSTarget, int inRSMax);

int             inVXEMVAPCallBackInit(int (*inMenuFunc) (char **, int),
                    void (*vdPromptManager) (unsigned short), unsigned short (*usAmtEntryFunc) (unsigned long *),     
                    EMVBoolean (*bIsCardBlackListed)(byte *,unsigned short,byte *,unsigned short),
                    unsigned short (*usProcessRequest)(unsigned short , char *, unsigned short),int (*inGetTermID)(char *),unsigned short (*bIsCSNValid)(unsigned char *CardCSN));

// Other functions

int 			inVXEMVAPKeyRollOver(void); //NAND_FLASH : KeyRollover Functionality changes as per Vx 6.0.0 FRD.

void            vdEMVSetFloorLimitUsed(unsigned fFlag);
void            vdEMVSetICCReadFailure(unsigned fFlag);
int             inEMVGetFallbackToMSR(void);
void            vdEMVSetFallbackToMSR(unsigned fFlag);
int             inEMVValidateTrack2Data(byte *Track2Data, int inLenTrack2, byte *PAN, int inLenPAN, byte *ServiceCode, int inLenServiceCode, byte *ExpiryDate);
/*int             inResetEMVScriptFile(void);
VS_BOOL         fScript71(void);
VS_BOOL         fScript72(void);
void            vdGetEMVS71FName(char *pchFName);
void            vdGetEMVS72FName(char *pchFName);*/
//Mary_F1 18/Feb/08
//ISRSCRPBO: Clarify Case ID: 080110-11
//Fix made to process issuer script, without making use of global buffer
//unsigned short  inProcessScripts(char *szFileName, int inScriptType, srTxnResult *psrTvrTsi);
//unsigned short inProcessScripts(char *szFileName, int inScriptType, srTxnResult *psrTvrTsi, byte *pIssuerScriptResults, int *pinNumScripts);
//NAND_FLASH changes
unsigned short inProcessScripts( byte* pScript,  unsigned short usScriptLen, int inScriptType, srTxnResult *psrTvrTsi, byte *pIssuerScriptResults, int *pinNumScripts);


int             isCardInserted(unsigned long slotNo);
//int             inCheckFileExist (char *szFileName);
char*           szGetPTID(char *ptid);
int             inEMVGetICCReadFailure(void);
int             IsAmountinPDOL(void);

int             inVXEMVAPSetDisplayPINPrompt( void (*fptr) (void) );
// This callback API is used to display the PIN prompt if the secure PIN
// functionality is to be used.

//void            usEMVDisplayPINPrompt(void);

// Soumya_G1 28/Feb/07 - Common Module Implementation - The france & china
// set api for call back function pointers.
unsigned short inVxEMVFRSetFunctionality(unsigned short usCode,void(*fnPtrSet));
unsigned short inVxEMVApCNSetFunctionality(unsigned short usCode);

// Soumya_G1 28/Feb/07 - Common Module Implementation - The france & china
//Default implementations in the module for the application call back functions for 
//france requirements.
unsigned short usDefaultEMVCheckICCResponse(byte *pCmd, Ushort usRetVal);
unsigned short usDefaultEMVIsVelocityCheckReqd(void);

//Pavan_K1 dated 25-Apr-07 added the below 3 prototypes.
EMVResult   getTerminalParam(Ushort,byte*,Ushort*);
EMVResult   getCapkExp( const byte* aid, const byte capkIndex, \
                        byte* pstCapk, short *capkLen, \
                        byte* pstCapkExp, short* pCapkExpLen );

EMVBoolean  bIsMultipleOccurencesAllowed(byte *pAid);



//Pavan_K1 dated 27-Feb-07 added the function prototypes.
unsigned short inVxEMVAPSetFunctionality(Ushort usCode, void(* fnPtrSet));
short          shDefaultGetUsrPin(unsigned char *pin);
unsigned short usDefaultgetLastTxnAmt(Ulong *amt);
unsigned short usDefaultEMVIssAcqCVM(unsigned short issacq, unsigned short *code);
unsigned short usDefaultEMVPerformSignature(void);
unsigned short usDefaultEMVPerformOnlinePIN(void);
void           vDefaultEMVDisplayErrorPrompt(unsigned short errorID);
unsigned short getDefaultTransactionAmount(void);
void 		   vDefaultEMVDisplayPINPrompt(void);
short          bDefaultIsCardBlackListed(byte * pan, unsigned short panLen, byte * panSeqNo, unsigned short panSeqLen);

//Soumya_g1 8/March/07 Added the following function to validate the CAPK expiry date.
short          shCheckCAPKExpiry(char* szCAPKExpDate);

//Pavan_K1 dated 29-Mar-07 added the prototype for setting the standard implementation.
void           vdSetSandardFuncPointers(void);

//KEYREVCN: Pavan_K1 dated 08-Aug-07 added the function for key revocation. 
int   inVXEMVAPKeyRevocation(void);

//MULTIAVN:
unsigned short usDefaultMultiAppVerNum(srMultiAVN *pMultiAVN, int *iAVNCount);

//ExtAuth6985:
unsigned short usDefaultGetEADecision(unsigned short usStatusWord);





#endif
