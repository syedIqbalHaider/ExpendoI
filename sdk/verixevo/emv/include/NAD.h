/*-----------------------------------------------------------------------------
 *
 *      Filename:       NAD.h
 *
 *      Product:        Card Slot Library
 *      Version:        1.00
 *      Author :        Vipul_K1
 *  	Module :	
 *          Defines all methods pertaining to NAD class.
 *
 * MODIFICATION HISTORY    :
 *
 * #      Date		    Who			History
 * ---  ---------    ---------		----------------------------------------
 * 1.   15 June 00    Vipul_K1		Created.
 *
 *
 *******************************************************************************
 * Copyright (C) 2000 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language 
 * or computer language, in any form or by any means, electronic, mechanical, 
 * magnetic, optical, chemical, manual or otherwise, without the prior written 
 * permission of VeriFone Inc.
 *                                                VeriFone Inc.               
 *                                                III Lagoon Drive, Suite 400  
 *                                                Redwood City, CA 94065      
 ******************************************************************************
 */

#ifndef _NAD
#define _NAD

#ifndef BYTE_COUNT  // ACT 2000 also defines BYTE
#ifndef __BYTE__
#define __BYTE__
typedef unsigned char     BYTE;
#endif
#endif // BYTE_COUNT

#ifndef __WORD__
#define __WORD__
typedef unsigned int      WORD; 
#endif
#ifndef __DWORD__
#define __DWORD__
typedef unsigned long     DWORD;
#endif

class NAD{

private:

	BYTE sad;				// Source Address
	BYTE dad;				// Destination Address 
	BYTE nad;				// Node Address. Most significant nibble
							// is sad, least significant nibble is dad.


public :

	NAD(BYTE SAD, BYTE DAD); 
	void	setNAD(BYTE SAD, BYTE DAD);
	void	setSAD(BYTE SAD); 
	void	setDAD(BYTE DAD);
	BYTE	getNAD(void);
	BYTE	getSAD(void);
	BYTE	getDAD(void);	
	~NAD();
		
};


#endif
