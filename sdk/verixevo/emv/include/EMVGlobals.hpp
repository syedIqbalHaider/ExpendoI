/*
 * FILENAME       : emvGlobals.hpp
 * PRODUCT        : Verix/Vx.
 * VERSION        : 4.0.0
 * AUTHOR(s)      : Pavan_k1
 * CREATED        : 07/Dec/06
 *
 * @Description	:This file has definitions used in the emv libraries.
 *
 * CONTENTS   :
 * MODIFICATION HISTORY    :
 *
 * #    Date        Who         History
 * ---  --------    ----------  ----------------------------------------
 * 1    07 Dec 06   Pavan_k1    Created for Vx EMV Module 4.0.0
 *
 * 2    30/Jan/07   Soumya_G1   TC# 2CM.042.06 - Introduced a constant for the application to 
                                indicate CDA failed after First GenAC & the card returned ARQC.
 * 
 * 3    30 Jan 07   Pavan_K1    Added extra macros for handling the magic numbers
 * 4    21 May 07   Pavan_K1    Added an extra macro MAX_NUM_TAGS for new design changes.
 * 5    28 May 07   Purvin_p1    change size of MAX_AID_LIST from 20 to 21.
 * 6    30 May 07   Brabhu_H1   Added macros for Interac specific implentation.
 * 7    20 Jun 07   Pavan_K1    Added macro MAX_NON_CRC_TAGS for handling the duplication of 
 *                              non critical data elements.
 * 8    22 Jun 07   Pavan_K1    Updated the Max ICC Response value form 255 to 256.
 * 9    12 Sep 07   Brabhu_H1   ICCPK255: ClarifyID 070906-102, Fix made 
 *                              consider max resp data as 255 bytes.
 * 10   18 Feb 08   Mary_F1     TERBUL48: Fix made for Specification update Bulletin #48 
 * 11   30 Jul 09   Kishor_S1   ECTXNFLW: Added macros to check the EC Tags(Moved the fix)
 * 12   21 Dec 10   Kishor_S1   CDAMode: Moved the CDAMode fix
 * 13   03 Jun 11   Mary_F1     MULTIAVN: Support for multiple application version number check
 *
 * Copyright (C) 2011 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language, in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual or otherwise, without the prior written
 * permission of VeriFone Inc.
 *                                                2099 Gateway Place
 *												  Suite 600
 *											      San Jose  CA  95110 
 *												  USA
 *
 *
 *
 */
#ifndef _GLOBALS
#define _GLOBALS

//Pavan_K1 dated 18-Jan-07 added for CAPK enhancement
//#define MIN_CAPK_DATA_SIZE              ((unsigned short)254)
#define MIN_CAPK_DATA_SIZE              ((unsigned short)248)
//Brabhu_H1 dated 12 September 2007
//ICCPK255: ClarifyID 070906-102, Fix made consider max resp data as 255 bytes
//#define TLV_DATA_SIZE                   ((unsigned char)254)
#define TLV_DATA_SIZE                   ((unsigned char)255)
#define TVR_SIZE                        ((unsigned short) 5)
#define TSI_SIZE                        ((unsigned short) 2)
#define DATA_AUTH_CODE_SIZE             ((unsigned short) 2)
#define ASCII_PAN_SIZE                  ((unsigned char)22)
#define TAC_SIZE                        ((unsigned short)5)


#define MAX_AID_LEN                     ((unsigned char)16)
#define MAX_LABEL_LEN                   ((unsigned char)16)
#define MAX_COMMAND_LEN                 ((unsigned char)32)
//#define MAX_AID_LIST                    ((unsigned char)21) //  Purvin_p1 date 28 May 2007  change size of MAX_AID_LIST from 20 to 21.
//16/July/2012 , Soumya_G1:- Increased the number AIDs supported to 99 from 21
// 99_AID_SUPPORT
#define MAX_AID_LIST                    ((unsigned char)99)

#define ISSUER_CNTRY_SIZE		((unsigned char)3) // Support_5F56

//MULTIAVN:
#define MAX_AVN_LEN                     ((unsigned char)2)

//DOL_BUFF_OVR_FLOW
#define MAX_DOL_BUFF_SIZE				255
#define MAX_PDOL_BUFF_SIZE				252

//According to the EMV Spec Book 3 - Chapter 6 , sec 6.1 
//The max number of data bytes in the response can be up to 256 bytes.
//Pavan_K1 dated 22-Jun-07 updated the value from 255 to 256
//#define MAX_ICC_RESPONSE                ((unsigned char)255)
#define MAX_ICC_RESPONSE                256
#define MAX_TLV_NAME                    ((unsigned char)20)
#define MAX_TLV_OBJECTS                 ((unsigned int)50)

#define MAX_ICC_RECORDS                 ((unsigned char)255)
#define MAX_CHALLENGE                   ((unsigned short) 255)
#define MAX_ICC_PK_LEN                  ((unsigned short) 300)
#define MAX_ISSUER_PK_LEN               ((unsigned short) 300) 
#define MAX_CAPK_EXP_LEN                ((unsigned short) 300)
#define MAX_ICC_PIN                     ((unsigned short) 10)
#define MAX_ICC_PIN_ENCRYPTED           ((unsigned short) 300)

#define MAX_STATIC_DATA                 ((unsigned short) 256)
#define MAX_ICC_PK                      ((unsigned short) 400)

#define ICC_NUM_SIZE                    8
#define MIN_PAN_DIGIT_TO_COMPARE        3
#define MAX_PAN_DIGIT_TO_COMPARE        8

// Constants used in Process restrictions
#define GOODS_SERVICES_TRAN             0x00
#define CASH_TRAN                       0x01
#define CASHBACK_TRAN                   0x09

#define TERM_TYPE14                     0x14
#define TERM_TYPE15                     0x15
#define TERM_TYPE16                     0x16

// Byte definition
#define     BYTE_0                      0
#define     BYTE_1                      1
#define     BYTE_2                      2
#define     BYTE_3                      3
#define     BYTE_4                      4
#define     BYTE_5                      5
#define     BYTE_6                      6
#define     BYTE_7                      7

#define     BITS_8                      8
#define     BITS_4                      4

#define     BITMASK_0x01                0x01
#define     BITMASK_0x02                0x02
#define     BITMASK_0x04                0x04
#define     BITMASK_0x08                0x08
#define     BITMASK_0x10                0x10
#define     BITMASK_0x20                0x20
#define     BITMASK_0x40                0x40
#define     BITMASK_0x80                0x80
#define     BITMASK_0xC0                0xC0  
#define     BITMASK_0x1F                0x1F
#define     BITMASK_0x7F                0x7F
#define     BYTE_0x7F                   0x7F
#define     BYTE_0x6A                   0x6A
#define     BYTE_0x03                   0x03
#define     BYTE_0x05                   0x05
#define     BITMASK_0x2000              ((Ushort)0x2000)
#define     ICC_RAND_LEN                8

#define     SIZE_MONTH                  2
#define     NORMALIZE_YEAR              50 

//defines for TLV Objs
#define LEN_BYTES_2                     2
#define LEN_BYTES_1                     1


#define MAX_CVM                         10      //Cardholder Verification Methods chk
#define TERM_CAP_SIZE                   3
#define TERM_ADD_CAP_SIZE               5
#define CVM_RESULT_SIZE                 3 
#define AMT_FLD_LEN                     4
#define CVM_NOT_SATISFIED               1
#define CVM_FAILED                      -1
#define CVM_SUCCESS                     0
#define CVM_PASSED                      0
#define TRAN_TYPE_SIZE                  1
#define CVM_AMT_X                       0
#define CVM_AMT_Y                       4

//MAX SIZE
#define CAPK_SIZE                       248 
#define CAPK_EXP_SIZE                   3 
//MAX PIN SIZE
#define MAX_PIN_SIZE                    12

#ifdef __thumb
	#define EMV_MAX_DATA_SIZE               1030
#endif

#ifdef _TARG_68000
	#define EMV_MAX_DATA_SIZE               256
#endif

#define MAX_ICC_CMD_SIZE                ((Ushort)270)
#define MAX_ICC_RESP_SIZE               ((Ushort)270)

#define SIZE_STATUSWORD                 2

#define CMD_DATA_SIZE                   256
#define CMD_SIZE                        5
#define LEN_EXPECTED_SIZE               1

#define ZERO_PADDING                    0x00
#define FF_PADDING                      0xFF

//MAX SIZE:
#define MAX_TDOL_LEN                    252 
#define MAX_DDOL_LEN                    252 
#define MAX_DO_IN_LIST                  20      //Max number of tags expected in a DOL
#define SIZE_CRYPTOGRAM                 8
#define SIZE_ATC                        2
#define SIZE_ISSUER_APP_DATA            32
#define SIZE_CID                        1
#define LEN_GENAC_DO                    43
#define LEN_GENAC_MANDATORY_DO          11

#define FMT_NUMERIC                     1
#define FMT_COMPNUMERIC                 2
#define FMT_OTHER                       0


//Id's used in getTerminalParam()
#define TAC_DENIAL                      1
#define TAC_ONLINE                      2
#define TAC_DEFAULT                     3

// Host Decision
#define HOST_AUTHORISED                 1
#define HOST_DECLINED                   2
#define FAILED_TO_CONNECT               3

#define OFFLINE_TERMINAL                FAILED_TO_CONNECT


#define TERMTYPE_03                     0x03
#define TERMTYPE_06                     0x06

//defines for TLV Collection
#define MULTI_TAG_ALLOWED               1
#define MULTI_TAG_NOT_ALLOWED           0  

//Constants used in script processing
#define SCRIPT_PROC_BEFORE_FINAL_GENAC  1
#define SCRIPT_PROC_AFTER_FINAL_GENAC   2


//Refer Part II data elements & commands, page IV-8, EMV Specs for the 
//below values.


#define ISSUER_PK_DATA_TRAILER          0xBC
#define HEADER_HASHRESULT_TRAILER_SIZE  0x16
#define HASHRESULT_TRAILER_SIZE         0x15
#define MAX_HASH_BUFFER                 0x14

//Pavan_K1 dated 29-Jan-07 added for the replacement of magic number
#define MAX_CSN_SIZE					0x03
#define MAX_PK_REMINDER_LEN             21
#define AMT_LEN							3
#define MAX_DATE_FLD_SIZE               5
#define YEAR_SIZE                       4
#define MONTH_SIZE                      2
#define DAY_SIZE                        2
#define HOUR_SIZE                       2
#define MINUTE_SIZE                     2
#define SEC_SIZE                        2
#define IPK_REM_LEN                     22
#define MAX_ATC_SIZE                    8

#define IPK_LEN_POSITION                13
#define IPK_LEN_CONSTANT                36
#define IPKEXP_LEN_POSITION             14
#define ISSUER_ID_POSITION              2
#define ISSUER_ID_SIZE                  4
#define YEAR_CORRECTION                 100
#define YEAR_CORRECTION_CUTOFF          50
#define IPK_YEAR_POSITION               7
#define IPK_HASHALGO_POSITION           11
#define IPK_ALGO_POSITION               12

#define ICCPK_LEN_POSITION              19
#define ICCPK_LEN_CONSTANT              42
#define ICCPKEXP_LEN_POSITION           20
#define ICCPK_YEAR_POSITION             13
#define ICCPK_MONTH_POSITION            12
#define ICCPK_HASHALGO_POSITION         17
#define ICCPK_ALGO_POSITION             18
#define MAX_ISS_PK_LEN                  256
#define MASK_0x3F                       0x3f
#define BITMASK_0xA0                    0xA0
#define BITMASK_0x30                    0x30
#define AMOUNT_SIZE                     4

#define CVM_ALWAYS_SUPPORTED            0x00
#define CVM_UNATTENDED_CASH             0x01
#define CVM_NOTCASH_OR_CASHBACK         0x02
#define CVM_TERMINAL_SUPPORTS           0x03
#define CVM_MANUAL_CASH                 0x04
#define CVM_CASHBACK                    0x05
#define CVM_APPLCURCY_BELOW_X           0x06
#define CVM_APPLCURCY_OVER_X            0x07
#define CVM_APPLCURCY_BELOW_Y           0x08
#define CVM_APPLCURCY_OVER_Y            0x09

#define FAIL_CVM_PROCESSING             0x00
#define PT_PINVERIFY                    0x01
#define ENCIPH_PINVERIFY_ONLINE         0x02
#define PT_PINVERIFY_N_SIG              0x03
#define ENCIPH_PINVERIFY                0x04
#define ENCIPH_PINVERIFY_N_SIG          0x05
#define SIGNATURE                       0x1E
#define CVM_NOT_REQD                    0x1F
//RSA Related defines
#define RABIN_VARIANT                   1
#define NORMAL_RSA                      0

//constants used in EMVSelection class
#define NORMAL_SELECTION                ((byte)0x00)
#define PARTIAL_SELECTION               ((byte)0x02)

//Max Sfi should be in the range 1 - 10 - Dcs fix
#define MIN_SFI_NUMBER                  0x01
#define MAX_SFI_NUMBER                  10 
#define MIN_LANG_PREF_LEN               2
#define MAX_LANG_PREF_LEN               8
#define ISSUER_CODE_INDEX_LEN           1
#define CAPDU_DATA_OFFSET               5
#define CAPDU_P3_OFFSET                 4
#define MIN_AID_LEN                     5
#define APP_PREFERNAME_LEN              16

// EMV defined TAG sizes
#define AID_MIN_SIZE                    5
#define AID_MAX_SIZE                    16

#define APPL_LABEL_MIN_SIZE             1
#define APPL_LABEL_MAX_SIZE             16

#define CMD_TO_PERFORM_MAX_SIZE         239
#define TRACK2_EQ_DATA_MAX_SIZE         19
#define APPL_PAN_MAX_SIZE               10

#define CARDHOLDER_NAME_MIN_SIZE        2
#define CARDHOLDER_NAME_MAX_SIZE        26

#define EMV_DATE_SIZE                   3
#define COUNTRY_CODE_SIZE               2
#define CURCY_CODE_SIZE                 2

#define LANG_PREFERENCE_MIN_SIZE        2
#define LANG_PREFERENCE_MAX_SIZE        8

#define EMV_SERVICE_CODE_SIZE           2
#define PAN_SEQNUM_SIZE                 1
#define CURR_EXP_SIZE                   1
#define AMOUNT_AUTH_SIZE                4
#define APPL_INTCHG_PROFILE_SIZE        2

#define DF_FILE_MIN_SIZE                5
#define DF_FILE_MAX_SIZE                16

#define ISSUER_SCRIPT_CMD_MAX_SIZE      261
#define APPL_PRIORITY_IND_SIZE          1
#define SFI_SIZE                        1
#define EMV_AUTH_CODE_SIZE              6 
#define AUTH_RESP_CODE_SIZE             2 

#define CDOL_MAX_SIZE                   252
#define CVM_LIST_MAX_SIZE               252

//Mary_F1	18/02/08
//TERBUL48: Fix made for Specification update Bulletin #48
//CVM List is checked to be of valid min length(X + Y)
#define CVM_LIST_MIN_SIZE               8

#define CERT_AUTH_PUBKEY_INDEX_SIZE     1


#define ISS_AUTH_DATA_MIN_SIZE          8
#define ISS_AUTH_DATA_MAX_SIZE          16


#define AFL_MAX_SIZE                    252

//MAX SIZE
#define TDOL_MAX_SIZE                   252 
#define TC_HASH_SIZE                    20
#define ACQ_ID_SIZE                     6
#define NUMERIC_AMT_SIZE                6
#define BINARY_AMT_SIZE                 4

#define APPL_DISC_DATA_MIN_SIZE         1
#define APPL_DISC_DATA_MAX_SIZE         32

#define APPL_USE_CNTRL_SIZE             2
#define APPL_VERSION_SIZE               2

#define CRDHLDRNAME_EXT_MIN_SIZE        27
#define CRDHLDRNAME_EXT_MAX_SIZE        45

#define IAC_SIZE                        5

#define ISSUER_APP_DATA_MAX_SIZE        32
#define ISSSUER_CODE_TBLIDX_SIZE        1

#define APPL_PREF_NAME_MIN_SIZE         1
#define APPL_PREF_NAME_MAX_SIZE         16

#define LAST_ONLINE_ATC_SIZE            2
#define LC_OFFLINE_LMT_SIZE             1
#define MER_CAT_CODE_SIZE               2
#define EMV_MERCH_ID_SIZE               15
#define PIN_TRY_COUNTER_SIZE            1
#define ISSUER_SCRIPT_ID_SIZE           4
#define FLOOR_LIMIT_SIZE                4
#define EMV_TERM_ID_SIZE                8

#define RISKMGMT_DATA_MIN_SIZE          1
#define RISKMGMT_DATA_MAX_SIZE          8

#define IFD_SER_NUM_SIZE                8
#define EMV_TIME_SIZE                   3
#define CERT_PUBKEY_IND_SIZE            1
#define UC_OFFLINE_LMT_SIZE             1
#define APPL_CRYPTOGRAM_SIZE            8
#define CRYPT_INFO_DATA_SIZE            1


#define ISS_PUBKEY_EXP_MIN_SIZE         1
#define ISS_PUBKEY_EXP_MAX_SIZE         3

#define CVM_RESULTS_SIZE                3
#define TERM_TYPE_SIZE                  1
#define ATC_SIZE                        2
#define UNPRED_NUM_SIZE                 4
#define POS_ENTRY_MODE_SIZE             1
#define AMT_REF_CURCY_SIZE              4

#define APPL_REF_CURR_MIN_SIZE          2
#define APPL_REF_CURR_MAX_SIZE          8

#define TRANS_REF_CURR_SIZE             2
#define TRANS_REFCURR_EXP_SIZE          1
#define TERM_ADTNAL_CAP_SIZE            5

#define TRANS_SEQ_COUNTER_MIN_SIZE      2
#define TRANS_SEQ_COUNTER_MAX_SIZE      4

#define APPL_REF_CURR_EXP_MIN_SIZE      1
#define APPL_REF_CURR_EXP_MAX_SIZE      4

#define APPL_CURR_EXP_SIZE              1

#define DDOL_MAX_SIZE                   252

#define ICC_DYNAMIC_NUM_MIN_SIZE        2
#define ICC_DYNAMIC_NUM_MAX_SIZE        8

#define FCI_DATA_MAX_SIZE               222
#define APPL_TEMPL_MAX_SIZE             252
#define FCI_TEMPL_MAX_SIZE              252
#define DIR_DISCR_TEMPL_MAX_SIZE        252

#define E_INVALID_INDEX                 -1

#define MAX_SELECT_TLVS                 40

#define ISS_CVM                         91
#define ACQ_CVM                         92

#define MAX_PIN_SIZE_ASC                8 

#define NO_WAIT                      	0x00000000
#define WAIT_FOREVER                 	0x00FFFFFF

#define MAX_NUM_TAGS					100 //Maximum number of tags in a response.
#define MAX_NON_CRC_TAGS				20  //Maximum number of non critical tags in a response.

//INTERAC:
//Brabhu_h1 dated 30 May 07
//Added Interac specific macro's
#define MIN_ASF_SIZE                	((Ushort) 2)
#define MAX_ASF_SIZE                	((Ushort) 32)

#define ISSUER_COUNTRY_CODE_SIZE        ((Ushort) 3)


//090218-9
//Delete Buf array stores a set of Tags
//which will be used to remove the matching Tags from Global TLV Collection 
#define MAX_DELBUF_TAGS		300		//Max Tags that Delete Buf array can accommodate
//end of 090218-9

//Brabhu_H1 dated 08 July 2008
//ECTXNFLW: Added macros to check the EC Support Tags
#define EC_BALANCE_SIZE                 ((Ushort) 6)
#define EC_RESET_THRESHOLD_SIZE         ((Ushort) 6)

// Tisha_K1 12/Aug/2010
// VFITAGFIX : Fix to maintain the VeriFone Proprietary tag values
// stored seperately from the global TLV collection.
#define KERNELTAGCOUNT 10
#define TAG_DF21_LOCTN 0
#define TAG_DF09_LOCTN 1
#define TAG_DF10_LOCTN 2
#define TAG_DF11_LOCTN 3
// END: VFITAGFIX

//CDAMode
enum Modes {CDA_MODE_3 = 0, CDA_MODE_1, CDA_MODE_2, CDA_MODE_4};
//End of CDAModes


#endif //_GLOBALS

