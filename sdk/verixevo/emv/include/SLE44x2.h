/*-----------------------------------------------------------------------------
 *
 *      Filename:       SLE44x2.h
 *
 *      Product:        Card Slot Library
 *      Version:        1.2     
 *      Author :        Asha_S2
 *  	Module :	
 *      Defines all macros needed for SLE44x2 synchronous card.
 *
 * MODIFICATION HISTORY    :
 *
 * #      Date		    Who			History
 * ---  ---------    ---------		----------------------------------------
 * 1.   30 Sep 03    Asha_S2		Created.
 *
 * 2    03 Oct 03    Asha_s2        Made changes after Code Review
 *
 * 3    16 Oct 03    Asha_s2        Added a length parameter to READ_MAIN_MEMORY
 *
 * 4    31 Oct 03    Asha_s2        Changed the INS byte of SLE4442_PSC_VERIFICATION
 *******************************************************************************
 * Copyright (C) 2000 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language 
 * or computer language, in any form or by any means, electronic, mechanical, 
 * magnetic, optical, chemical, manual or otherwise, without the prior written 
 * permission of VeriFone Inc.
 *                                                VeriFone Inc.               
 *                                                III Lagoon Drive, Suite 400  
 *                                                Redwood City, CA 94065      
 ******************************************************************************
 */

/***************************************************************/


// Macros for SLE44x2 sync card. Version is ignored.
// Note that 44x2.sdm supports both 4432 and 4442 cards.

//The macros begin with SLE44X2 if the macro is supported by both
//SLE4432 and SLE4442.
//The macros begin with SLE4442 if the macro is supported by both
//SLE4442.


#define SLE44X2											(DWORD)0x0002

// SLE44x2 SYNC CARD
// Descriptor - "SLE44x2  "  ( there is a space in the end. )   
// Version - X.XX
// SDM file - 44x2.sdm


/*****************************************************************
Macro : SLE44X2_GET_ATR_BYTES

Input : None

Output: ucBuff - Cmd data that is fed into Transmit_APDU
        ucBuffLen - Buffer length to send to Transmit_APDU

Description: This command will reset the card and retrieve ATR bytes 
			and returns ATR bytes received from the card. The card must 
			be powered up using the power on command.

*******************************************************************/

//code-review-3 Oct 2003
//changed the title from SLE44X2_GET_ATR_BYTE to SLE44X2_GET_ATR_BYTES

#define SLE44X2_GET_ATR_BYTES(ucBuff,ucBuffLen) {	\
memset(ucBuff,0x00, 6);			\
ucBuff[1] = 0x1;				\
ucBuffLen = 0x6 ;				\
}

/*****************************************************************
Macro : SLE44X2_READ_MAIN_MEMORY

Input : addr - address from where the data needs to be read 
        
Output: ucBuff - Cmd data that is fed into Transmit_APDU
        ucBuffLen - Buffer length to send to Transmit_APDU

Description: This command reads the remaining bytes from the card 
			beginning with the location given in addr
*******************************************************************/

#define SLE44X2_READ_MAIN_MEMORY(ucBuff, addr, len, ucBuffLen) {	\
memset(ucBuff,0x00, 6);			\
ucBuff[1] = 0x2;				\
ucBuff[2] = addr;	            \
ucBuff[3] = len;	\
ucBuffLen = 0x6 ;				\
}

/*****************************************************************
Macro : SLE44X2_UPDATE_MAIN_MEMORY

Input : addr - address from where the data needs to be read 
		numBytes - length of APDU data
		cmdData - Command Data field
        
Output: ucBuff - Cmd data that is fed into Transmit_APDU
        ucBuffLen - Buffer length to send to Transmit_APDU

Description: This command updates specified number of bytes of 
			main memory space starting at the specified address
*******************************************************************/


#define SLE44X2_UPDATE_MAIN_MEMORY(ucBuff, addr, numBytes, cmdData, ucBuffLen) {	\
memset(ucBuff,0x00, (numBytes + 6));			\
ucBuff[1] = 0x3;				\
ucBuff[2] = addr;	\
ucBuff[4] = numBytes;	\
memcpy(&ucBuff[5], cmdData, numBytes); \
ucBuffLen = numBytes + 6 ;				\
}


/*****************************************************************
Macro : SLE44X2_READ_PROTECTION_MEMORY

Input : None
        
Output: ucBuff - Cmd data that is fed into Transmit_APDU
        ucBuffLen - Buffer length to send to Transmit_APDU

Description: This command reads 32 bits of protection memory and returns 
			 the 32-bit value where the most significat bit of the 4 byte 
			 array is bit 31 and the least significant bit of the 4 byte 
			 array is bit 0.
*******************************************************************/


#define SLE44X2_READ_PROTECTION_MEMORY(ucBuff, ucBuffLen) {	\
memset(ucBuff,0x00, 6);			\
ucBuff[1] = 0x4;				\
ucBuffLen = 6 ;				\
}

/*****************************************************************
Macro : SLE44X2_WRITE_PROTECTION_MEMORY

Input : addr - address from where the data needs to be read 
		dataByte - value used to compare with the one at the address
					specified on the card.
        
Output: ucBuff - Cmd data that is fed into Transmit_APDU
        ucBuffLen - Buffer length to send to Transmit_APDU

Description: This command manages the protection of one byte address 
			 in the protected card.
			 DataByte is the value of the byte in main memory that 
			 is to be protected at location addr.
			 If DataByte is equal to the main memory byte at location 
			 addr, the protection bit for location addr is written to zero.
*******************************************************************/

#define SLE44X2_WRITE_PROTECTION_MEMORY(ucBuff, addr, dataByte, ucBuffLen) {	\
memset(ucBuff,0x00, 6);			\
ucBuff[1] = 0x5;				\
ucBuff[2] = addr;				\
ucBuff[3] = dataByte;				\
ucBuffLen = 6 ;				\
}


/*****************************************************************
Macro : SLE4442_READ_SECURITY_MEMORY

Input : None

Output: ucBuff - Cmd data that is fed into Transmit_APDU
        ucBuffLen - Buffer length to send to Transmit_APDU

Description: Reads the Error Counter and 3 bytes of PSC.
*******************************************************************/

#define SLE4442_READ_SECURITY_MEMORY(ucBuff, ucBuffLen) {	\
memset(ucBuff,0x00, 6);			\
ucBuff[1] = 0xA;				\
ucBuffLen = 6 ;				\
}


/*****************************************************************
Macro : SLE4442_UPDATE_SECURITY_MEMORY

Input : addr - address from where the data needs to be read 
		dataByte - value to write
        
Output: ucBuff - Cmd data that is fed into Transmit_APDU
        ucBuffLen - Buffer length to send to Transmit_APDU

Description: Updates the given address of the security area regarding 
			conditions given in the card specification. A successful 
			PSC presentation must occur before updating the security 
			area.
*******************************************************************/

#define SLE4442_UPDATE_SECURITY_MEMORY(ucBuff, addr, dataByte, ucBuffLen) {	\
memset(ucBuff,0x00, 6);			\
ucBuff[1] = 0xB;				\
ucBuff[2] = addr;				\
ucBuff[3] = dataByte;				\
ucBuffLen = 6 ;				\
}


/*****************************************************************
Macro : SLE4442_COMPARE_VERIFICATION_DATA

Input : addr - address from where the data needs to be read 
		dataByte - value to write
        
Output: ucBuff - Cmd data that is fed into Transmit_APDU
        ucBuffLen - Buffer length to send to Transmit_APDU

Description: This command compares the given dataByte with one PSC byte
			 of the card at the specified address in Secuiity Memory.
			 
*******************************************************************/

#define SLE4442_COMPARE_VERIFICATION_DATA(ucBuff, addr, dataByte, ucBuffLen) {	\
memset(ucBuff,0x00, 6);			\
ucBuff[1] = 0xC;				\
ucBuff[2] = addr;				\
ucBuff[3] = dataByte;				\
ucBuffLen = 6 ;				\
}

/*****************************************************************
Macro : SLE4442_PSC_VERIFICATION

Input : psc - 3 byte array to compare to the 3 security referene data bytes
//Code-Review-3 Oct 2003
//Added the following line
		The array should have the PSC1, PSC2 and PSC3 in that order
        
Output: ucBuff - Cmd data that is fed into Transmit_APDU
        ucBuffLen - Buffer length to send to Transmit_APDU

Description: This command decrements the error counter, then performs
			compare verification data between the 3 byte arrray provided
			and the Reference Data Bytes 1-3
			Next clearing the erro counter is attempted. Upon Success,
			the least significant 3 bits of error counter will be set to 1.
			The value of error counter is returned.
			 
*******************************************************************/

/*
asha_s2 dated 31 Oct 2003
Changed the INS byte from 0x0C to 0x0D
*/

#define SLE4442_PSC_VERIFICATION(ucBuff, psc, ucBuffLen) {	\
memset(ucBuff,0x00, 6);			\
ucBuff[1] = 0xD;				\
memcpy(&ucBuff[2], psc, 3);		\
ucBuffLen = 6 ;				\
}

//END

