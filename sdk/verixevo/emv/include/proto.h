/*
 * FILENAME       : proto.h
 * PRODUCT        : Verix/Vx.
 * VERSION        : 4.0.0
 * AUTHOR(s)      : Pavan_k1
 * CREATED        : 11/Dec/06
 *
 * DESCRIPTION    : 
 *
 * CONTENTS   :
 * MODIFICATION HISTORY    :
 *
 * #    Date        Who         History
 * ---  --------    ----------  ----------------------------------------
 * 1    11 Dec 06   Pavan_k1    Created for Vx EMV Module 4.0.0
 *
 * 2    29/Jan/07   Soumya_G1   Added  the new function inGetESTSchemeNumberInclNonEMV()
 * Copyright (C) 2006 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language, in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual or otherwise, without the prior written
 * permission of VeriFone Inc.
 *                                                2099 Gateway Place
 *                                                  Suite 600
 *                                                  San Jose  CA  95110 
 *                                                  USA
 *
 *
 *
 */
#ifndef _PROTO_H
#define _PROTO_H

#include <EMVTypeDefs.hpp>
#include <emvlib.h>


    //Internal Include file for defining the prototype for functions  used by Lib.
    char 		*strlcpy (char *szString1, char *szString2, int iCount);
    char             *strlcopy (char *szString1, char *szString2, int count);
    void             get_unpredict(BYTE *inpbuff, int size);
    void             hex_2_asc( BYTE *outp, BYTE *inp, int length );
    void             ascii_to_bin(BYTE *dest, BYTE *src, int length);
    unsigned char    LRC(char *buffer, int len);
    short            inGetESTSchemeNumber(void);
    short            inGetESTCAPublicKey(BYTE *CAPKBuff, short *CAPKLenp, BYTE *CAPKExpBuff, short *CAPKExpLenp);
    int              inGetESTPKDataFromFile(char *szCAPKFileName, BYTE *CAPKBuff, short *CAPKLenp, BYTE *CAPKExpBuff, short *CAPKExpLenp);
    short            shGetESTCAPublicKeyExp(BYTE *CAPKBuff, short *CAPKLenp, BYTE *CAPKExpBuff, short *CAPKExpLenp, char  *szRID);

    void             vdGetESTTermAVN(char *szTAVN);
    void             vdGetESTSecondTermAVN(char *szSecondTAVN);
    int              inCreateFileList(void);
    int              inCheckAndCleanPKFiles(void);
    short            inGetTerminalMVTIndex(void);
    int              inGetPTID(char *ptid);  

	//Soumya_G1 29/Jan/07 MVT chaining issue : Added this new function to get the scheme number from 
	//EST file correctly while loading AID specific record of MVT
	short inGetESTSchemeNumberInclNonEMV(void);

#endif
