/*
 * FILENAME       : EMVGenConfig.h
 * PRODUCT        : Vx.
 * VERSION        : 6.0.0
 * AUTHOR(s)      : Ganesh_P1
 * CREATED        : 12/Jan/11
 *
 * DESCRIPTION    : 
 *
 * CONTENTS   :
 * MODIFICATION HISTORY    :
 *
 * #    Date        Who         History
 * ---  --------    ----------  ----------------------------------------
 *
 * Copyright (C) 2006 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language, in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual or otherwise, without the prior written
 * permission of VeriFone Inc.
 *                                                2099 Gateway Place
 *                                                  Suite 600
 *                                                  San Jose  CA  95110 
 *                                                  USA
 *
 *
 *
 */


#define 	    GEN_CONFIG_INVALID_PARAMETER		-1
#define 		CONFIG_KEY_FILE_NOT_FOUND		-2
#define		EST_FILE_CREATE_ERROR	-3
#define		MVT_FILE_CREATE_ERROR	-4
#define		BUF_SIZE_ERROR			-5
#define		GEN_CONFIG_FAILURE				0
#define		GEN_CONFIG_SUCCESS				1
#define		CAPK_FILE_CREATE_ERROR -6
#define		CONFIG_DATA_FILE_NOT_FOUND	-7
#define		DAT_FILE_CREATE_ERROR	-8
#define		INVALID_KEYWORD		-9
#define 		AID_FILE_ERROR			-10
#define		EXP_DATE_ERROR			-11
#define		WRONG_CAPK_DATA		-12
#define		CAPK_DATA_ERROR		-13
#define		GEN_RID_FORMAT_ERROR		-14
#define		GEN_AID_FORMAT_ERROR		-15
#define		GEN_DEFAULT_SEC_ERROR		-16
#define		BLOCK_DAT_FILE_NOT_FOUND	-17
#define		DAT_FILELIST_FILE_NOT_FOUND  -18


//Ganesh_p1 : 16-11-11 Changed the API name to match with that in FRD.
int inVXEMVAPConvertConfigData (char* const chKeyFileName, char* const chDataFileName);
//int inVxGenConfigData(char* const chKeyFileName, char* const chDataFileName);
int inVxEMVAPUpdateConfigData( const char* chBlockValue );
int inVXEMVAPGetLastErrCode (void);


