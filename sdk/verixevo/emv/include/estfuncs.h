/*
 * FILENAME       : estfuncs.h
 * PRODUCT        : Verix/Vx.
 * VERSION        : 4.0.0
 * AUTHOR(s)      : Pavan_k1
 * CREATED        : 11/Dec/06
 *
 * DESCRIPTION    : 
 *
 * CONTENTS   :
 * MODIFICATION HISTORY    :
 *
 * #    Date        Who         History
 * ---  --------    ----------  ----------------------------------------
 * 1    11 Dec 06   Pavan_k1    Created for Vx EMV Module 4.0.0
 * 2    05 Mar 07   Pavan_K1    changed the return value of inESTValidateCSN from int to ushort.
 *                              changed the name of the function to usESTValidateCSN for common module.
 *
 * Copyright (C) 2006 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language, in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual or otherwise, without the prior written
 * permission of VeriFone Inc.
 *                                                2099 Gateway Place
 *												  Suite 600
 *											      San Jose  CA  95110 
 *												  USA
 *
 *
 *
 */
#ifndef _ESTFUNCS_H
#define _ESTFUNCS_H

#define EST_NUM_OF_CAPKS		15  // For supporting CAPK Slots. 
#define EST_NUM_OF_SUPP_AIDS	10

#define vdGetESTCSNListFile(a) szGetESTCSNListFile(a)	


//NAND_FLASH	
	int    inLoadAllESTRecords(void);




	short 	inGetESTPKIndex(int);
	short 	inGetESTSupportedAIDs(srAIDList *pAidList);
	void  	vdGetESTSuppAIDData(char *, char *, char *, int *, int );
	void  	vdGetESTPKDataFile(char *, char *, int);
	short  	GetPublicKeyIndex(void);
	void  	vdGetAIDinASCII(char *szAID);
	//Pavan_K1 dated 05-Mar-07 changed the return value and the function name.
	//int inESTValidateCSN(byte *CardCSN);
	unsigned short usESTValidateCSN(byte *CardCSN);
	int		inESTVerifyCAPKFile(char *szCAPKFileName);
	int		inESTVerifyAllCAPKs(void);

	//Added for getting the recommended app name for the Aids in the candidate list.
	//Shon_p1  8/Feb/2012 - TC # 2CA.033.01- 2 AIDs displaying the same recommended name.
	//Added the new parameter to be passed , cAIDLen.
	short	inGetESTRecAppNameForAID(char * szAIDstr1, byte cAIDLen, char * szAppName);
	int 	inGetRecAppName(int inFldNum, char * szAppName);
	int 	inPKFIleNameCompFunc (char* pszCAPKFileName1, char *pszCAPKFileName2);


	int		inLoadESTRec (int inRecNumber);
	void 	*pvdGetESTRec(void);
	int		inSaveESTRec (int inRecNumber);
	int		inGetESTRecNumber (void);
	int		inUpdateESTRec (void);
	void	vdResetESTRec (void);

	// Set/Get functions for each field in teh EST structure.

	long 	lnGetESTNbrOfTransactions(void);
	void 	vdSetESTNbrOfTransactions(long lnNbrOfTransactions);
	void 	szGetESTSchemeLabel(char*param);
	void 	vdSetESTSchemeLabel(char* szSchemeLabel);
	void 	szGetESTRID(char*param);
	void 	vdSetESTRID(char* szRID);
	void 	szGetESTCSNList(char*param);
	void 	vdSetESTCSNList(char* szCSNList);
	short 	inGetESTPublicKeyIndex1(void);
	void 	vdSetESTPublicKeyIndex1(short inPublicKeyIndex1);
	void 	szGetESTCAPKFile1(char*param);
	void 	vdSetESTCAPKFile1(char* szCAPKFile1);
	void 	szGetESTCAPKExpDate1(char*param);
	void 	vdSetESTCAPKExpDate1(char* szCAPKExpDate1);
	short 	inGetESTPublicKeyIndex2(void);
	void 	vdSetESTPublicKeyIndex2(short inPublicKeyIndex2);
	void 	szGetESTCAPKFile2(char*param);
	void 	vdSetESTCAPKFile2(char* szCAPKFile2);
	void 	szGetESTCAPKExpDate2(char*param);
	void 	vdSetESTCAPKExpDate2(char* szCAPKExpDate2);
	short 	inGetESTPublicKeyIndex3(void);
	void 	vdSetESTPublicKeyIndex3(short inPublicKeyIndex3);
	void 	szGetESTCAPKFile3(char*param);
	void 	vdSetESTCAPKFile3(char* szCAPKFile3);
	void 	szGetESTCAPKExpDate3(char*param);
	void 	vdSetESTCAPKExpDate3(char* szCAPKExpDate3);
	short 	inGetESTPublicKeyIndex4(void);
	void 	vdSetESTPublicKeyIndex4(short inPublicKeyIndex4);
	void 	szGetESTCAPKFile4(char*param);
	void 	vdSetESTCAPKFile4(char* szCAPKFile4);
	void 	szGetESTCAPKExpDate4(char*param);
	void 	vdSetESTCAPKExpDate4(char* szCAPKExpDate4);
	short 	inGetESTPublicKeyIndex5(void);
	void 	vdSetESTPublicKeyIndex5(short inPublicKeyIndex5);
	void 	szGetESTCAPKFile5(char*param);
	void 	vdSetESTCAPKFile5(char* szCAPKFile5);
	void 	szGetESTCAPKExpDate5(char*param);
	void 	vdSetESTCAPKExpDate5(char* szCAPKExpDate5);
	short 	inGetESTPublicKeyIndex6(void);
	void 	vdSetESTPublicKeyIndex6(short inPublicKeyIndex6);
	void 	szGetESTCAPKFile6(char*param);
	void 	vdSetESTCAPKFile6(char* szCAPKFile6);
	void 	szGetESTCAPKExpDate6(char*param);
	void 	vdSetESTCAPKExpDate6(char* szCAPKExpDate6);
	short 	inGetESTPublicKeyIndex7(void);
	void 	vdSetESTPublicKeyIndex7(short inPublicKeyIndex7);
	void 	szGetESTCAPKFile7(char*param);
	void 	vdSetESTCAPKFile7(char* szCAPKFile7);
	void 	szGetESTCAPKExpDate7(char*param);
	void 	vdSetESTCAPKExpDate7(char* szCAPKExpDate7);
	short 	inGetESTPublicKeyIndex8(void);
	void 	vdSetESTPublicKeyIndex8(short inPublicKeyIndex8);
	void 	szGetESTCAPKFile8(char*param);
	void 	vdSetESTCAPKFile8(char* szCAPKFile8);
	void 	szGetESTCAPKExpDate8(char*param);
	void 	vdSetESTCAPKExpDate8(char* szCAPKExpDate8);
	short 	inGetESTPublicKeyIndex9(void);
	void 	vdSetESTPublicKeyIndex9(short inPublicKeyIndex9);
	void 	szGetESTCAPKFile9(char*param);
	void 	vdSetESTCAPKFile9(char* szCAPKFile9);
	void 	szGetESTCAPKExpDate9(char*param);
	void 	vdSetESTCAPKExpDate9(char* szCAPKExpDate9);
	short 	inGetESTPublicKeyIndex10(void);
	void 	vdSetESTPublicKeyIndex10(short inPublicKeyIndex10);
	void 	szGetESTCAPKFile10(char*param);
	void 	vdSetESTCAPKFile10(char* szCAPKFile10);
	void 	szGetESTCAPKExpDate10(char*param);
	void 	vdSetESTCAPKExpDate10(char* szCAPKExpDate10);
	short 	inGetESTPublicKeyIndex11(void);
	void 	vdSetESTPublicKeyIndex11(short inPublicKeyIndex11);
	void 	szGetESTCAPKFile11(char*param);
	void 	vdSetESTCAPKFile11(char* szCAPKFile11);
	void 	szGetESTCAPKExpDate11(char*param);
	void 	vdSetESTCAPKExpDate11(char* szCAPKExpDate11);
	short 	inGetESTPublicKeyIndex12(void);
	void 	vdSetESTPublicKeyIndex12(short inPublicKeyIndex12);
	void 	szGetESTCAPKFile12(char*param);
	void 	vdSetESTCAPKFile12(char* szCAPKFile12);
	void 	szGetESTCAPKExpDate12(char*param);
	void 	vdSetESTCAPKExpDate12(char* szCAPKExpDate12);
	short 	inGetESTPublicKeyIndex13(void);
	void 	vdSetESTPublicKeyIndex13(short inPublicKeyIndex13);
	void 	szGetESTCAPKFile13(char*param);
	void 	vdSetESTCAPKFile13(char* szCAPKFile13);
	void 	szGetESTCAPKExpDate13(char*param);
	void 	vdSetESTCAPKExpDate13(char* szCAPKExpDate13);
	short 	inGetESTPublicKeyIndex14(void);
	void 	vdSetESTPublicKeyIndex14(short inPublicKeyIndex14);
	void 	szGetESTCAPKFile14(char*param);
	void 	vdSetESTCAPKFile14(char* szCAPKFile14);
	void 	szGetESTCAPKExpDate14(char*param);
	void 	vdSetESTCAPKExpDate14(char* szCAPKExpDate14);
	short 	inGetESTPublicKeyIndex15(void);
	void 	vdSetESTPublicKeyIndex15(short inPublicKeyIndex15);
	void 	szGetESTCAPKFile15(char*param);
	void 	vdSetESTCAPKFile15(char* szCAPKFile15);
	void 	szGetESTCAPKExpDate15(char*param);
	void 	vdSetESTCAPKExpDate15(char* szCAPKExpDate15);
	void 	szGetESTSupportedAID1(char*param);
	void 	vdSetESTSupportedAID1(char* szSupportedAID1);
	short 	inGetESTPartialNameAllowedFlag1(void);
	void 	vdSetESTPartialNameAllowedFlag1(short inPartialNameAllowedFlag1);
	void 	szGetESTTermAVN1(char*param);
	void 	vdSetESTTermAVN1(char* szTermAVN1);
	void 	szGetESTSecondTermAVN1(char*param);
	void 	vdSetESTSecondTermAVN1(char* szSecondTermAVN1);
	void 	szGetESTSupportedAID2(char*param);
	void 	vdSetESTSupportedAID2(char* szSupportedAID2);
	short 	inGetESTPartialNameAllowedFlag2(void);
	void 	vdSetESTPartialNameAllowedFlag2(short inPartialNameAllowedFlag2);
	void 	szGetESTTermAVN2(char*param);
	void 	vdSetESTTermAVN2(char* szTermAVN2);
	void 	szGetESTSecondTermAVN2(char*param);
	void 	vdSetESTSecondTermAVN2(char* szSecondTermAVN2);
	void 	szGetESTSupportedAID3(char*param);
	void 	vdSetESTSupportedAID3(char* szSupportedAID3);
	short 	inGetESTPartialNameAllowedFlag3(void);
	void 	vdSetESTPartialNameAllowedFlag3(short inPartialNameAllowedFlag3);
	void 	szGetESTTermAVN3(char*param);
	void 	vdSetESTTermAVN3(char* szTermAVN3);
	void 	szGetESTSecondTermAVN3(char*param);
	void 	vdSetESTSecondTermAVN3(char* szSecondTermAVN3);
	void 	szGetESTSupportedAID4(char*param);
	void 	vdSetESTSupportedAID4(char* szSupportedAID4);
	short 	inGetESTPartialNameAllowedFlag4(void);
	void 	vdSetESTPartialNameAllowedFlag4(short inPartialNameAllowedFlag4);
	void 	szGetESTTermAVN4(char*param);
	void 	vdSetESTTermAVN4(char* szTermAVN4);
	void 	szGetESTSecondTermAVN4(char*param);
	void 	vdSetESTSecondTermAVN4(char* szSecondTermAVN4);
	void 	szGetESTSupportedAID5(char*param);
	void 	vdSetESTSupportedAID5(char* szSupportedAID5);
	short 	inGetESTPartialNameAllowedFlag5(void);
	void 	vdSetESTPartialNameAllowedFlag5(short inPartialNameAllowedFlag5);
	void 	szGetESTTermAVN5(char*param);
	void 	vdSetESTTermAVN5(char* szTermAVN5);
	void 	szGetESTSecondTermAVN5(char*param);
	void 	vdSetESTSecondTermAVN5(char* szSecondTermAVN5);
	void 	szGetESTSupportedAID6(char*param);
	void 	vdSetESTSupportedAID6(char* szSupportedAID6);
	short 	inGetESTPartialNameAllowedFlag6(void);
	void 	vdSetESTPartialNameAllowedFlag6(short inPartialNameAllowedFlag6);
	void 	szGetESTTermAVN6(char*param);
	void 	vdSetESTTermAVN6(char* szTermAVN6);
	void 	szGetESTSecondTermAVN6(char*param);
	void 	vdSetESTSecondTermAVN6(char* szSecondTermAVN6);
	void 	szGetESTSupportedAID7(char*param);
	void 	vdSetESTSupportedAID7(char* szSupportedAID7);
	short 	inGetESTPartialNameAllowedFlag7(void);
	void 	vdSetESTPartialNameAllowedFlag7(short inPartialNameAllowedFlag7);
	void 	szGetESTTermAVN7(char*param);
	void 	vdSetESTTermAVN7(char* szTermAVN7);
	void 	szGetESTSecondTermAVN7(char*param);
	void 	vdSetESTSecondTermAVN7(char* szSecondTermAVN7);
	void 	szGetESTSupportedAID8(char*param);
	void 	vdSetESTSupportedAID8(char* szSupportedAID8);
	short	inGetESTPartialNameAllowedFlag8(void);
	void 	vdSetESTPartialNameAllowedFlag8(short inPartialNameAllowedFlag8);
	void 	szGetESTTermAVN8(char*param);
	void 	vdSetESTTermAVN8(char* szTermAVN8);
	void 	szGetESTSecondTermAVN8(char*param);
	void 	vdSetESTSecondTermAVN8(char* szSecondTermAVN8);
	void 	szGetESTSupportedAID9(char*param);
	void 	vdSetESTSupportedAID9(char* szSupportedAID9);
	short 	inGetESTPartialNameAllowedFlag9(void);
	void 	vdSetESTPartialNameAllowedFlag9(short inPartialNameAllowedFlag9);
	void 	szGetESTTermAVN9(char*param);
	void 	vdSetESTTermAVN9(char* szTermAVN9);
	void 	szGetESTSecondTermAVN9(char*param);
	void 	vdSetESTSecondTermAVN9(char* szSecondTermAVN9);
	void 	szGetESTSupportedAID10(char*param);
	void 	vdSetESTSupportedAID10(char* szSupportedAID10);
	short 	inGetESTPartialNameAllowedFlag10(void);
	void 	vdSetESTPartialNameAllowedFlag10(short inPartialNameAllowedFlag10);
	void 	szGetESTTermAVN10(char*param);
	void 	vdSetESTTermAVN10(char* szTermAVN10);
	void 	szGetESTSecondTermAVN10(char*param);
	void 	vdSetESTSecondTermAVN10(char* szSecondTermAVN10);
	short	inGetESTEMVTableRecord(void);
	void 	vdSetESTEMVTableRecord(short inEMVTableRecord);
	void 	szGetESTCSNListFile(char*param);
	void 	vdSetESTCSNListFile(char* szCSNListFile);

	//  New functions added for Recommended App label name fior each aid. 03/06/03 Ravi_B3
	void 	szGetESTRecAppNameAID1(char*param) ;
	void 	szSetESTRecAppNameAID1(char*param) ;
	void 	szGetESTRecAppNameAID2(char*param) ;
	void 	szSetESTRecAppNameAID2(char*param) ;
	void 	szGetESTRecAppNameAID3(char*param) ;
	void 	szSetESTRecAppNameAID3(char*param) ;
	void 	szGetESTRecAppNameAID4(char*param) ;
	void 	szSetESTRecAppNameAID4(char*param) ;
	void 	szGetESTRecAppNameAID5(char*param) ;
	void 	szSetESTRecAppNameAID5(char*param) ;
	void 	szGetESTRecAppNameAID6(char*param) ;
	void 	szSetESTRecAppNameAID6(char*param) ;
	void 	szGetESTRecAppNameAID7(char*param) ;
	void 	szSetESTRecAppNameAID7(char*param) ;
	void 	szGetESTRecAppNameAID8(char*param) ;
	void 	szSetESTRecAppNameAID8(char*param) ;
	void 	szGetESTRecAppNameAID9(char*param) ;
	void 	szSetESTRecAppNameAID9(char*param) ;
	void 	szGetESTRecAppNameAID10(char*param) ;
	void 	szSetESTRecAppNameAID10(char*param) ;

#endif 
