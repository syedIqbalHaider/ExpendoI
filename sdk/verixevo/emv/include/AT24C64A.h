/*-----------------------------------------------------------------------------
 *
 *      Filename:       AT24C64A.h
 *
 *      Product:        Card Slot Library
 *      Version:        2.1.0   
 *      Author :        Soumya_G1
 *  	  Module :	
 *      Defines all macros needed for AT24C64A synchronous card.
 *
 * MODIFICATION HISTORY    :
 *
 * #      Date		    Who			History
 * ---  ---------    ---------		----------------------------------------
 * 1.   01 Oct 09    Soumya_G1		Created.
 * 2.   23 Nov 10    Purvin_P1              Added Support for Synchronous Card AT24C01A
 *
 * Copyright (C) 2005 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language, in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual or otherwise, without the prior written
 * permission of VeriFone Inc.
 *                                                
 
	2099 Gateway Place
	Suite 600
	San Jose CA 95110 
	USA	 

 */

/***************************************************************/
// Macros for AT24C64A sync card. 


#ifndef _AT24C64A_H
#define _AT24C64A_H



#define AT24C64A											(DWORD)0x0006
/*****************************************************************
Macro : AT24C64A_READ_CARD_MEMORY

Input : MSB of the addr to be read
           LSB of the addr to be read
           Numbytes to be read from the card

Output: ucBuff - Cmd data that is fed into Transmit_APDU
        ucBuffLen - Buffer length to send to Transmit_APDU

Description:This command reads card memory beginning at the specified address for up to 255 bytes.
                  The card specification call this a "Random Read.

*******************************************************************/


#define AT24C64A_READ_CARD_MEMORY(ucBuff, addrMSB, addrLSB, numBytes, ucBuffLen) {	\
memset(ucBuff,0x00, 0x6);			\
ucBuff[1] = 0x1;				\
ucBuff[2] = addrMSB;	            \
ucBuff[3] = addrLSB;	            \
ucBuff[4] = numBytes;				\
ucBuffLen = 0x6 ;				\
}


/*****************************************************************
Macro : AT24C01A_READ_CARD_MEMORY

Input : MSB of the addr to be read
           LSB of the addr to be read
           Numbytes to be read from the card

Output: ucBuff - Cmd data that is fed into Transmit_APDU
        ucBuffLen - Buffer length to send to Transmit_APDU

Description:This command reads card memory beginning at the specified address for up to 255 bytes.
                  The card specification call this a "Random Read.

*******************************************************************/


#define AT24C01A_READ_CARD_MEMORY(ucBuff, addrMSB, addrLSB, numBytes, ucBuffLen) {	\
memset(ucBuff,0x00, 0x6);			\
ucBuff[0] = 0x02;				\
ucBuff[1] = 0x1;				\
ucBuff[2] = addrMSB;	            \
ucBuff[3] = addrLSB;	            \
ucBuff[4] = numBytes;				\
ucBuffLen = 0x6 ;				\
}


/*****************************************************************
Macro : AT24C64A_READ_CARD_MEM_SEQUL

Input : Num Bytes to be read from the card

Output: ucBuff - Cmd data that is fed into Transmit_APDU
        ucBuffLen - Buffer length to send to Transmit_APDU

Description:This command reads card memory beginning at the current card address location.  
                  It is called a "Sequential Read" in the card specification.  
                  A length of 1 is equivalent to the "Current Address Read" function

*******************************************************************/

#define AT24C64A_READ_CARD_MEM_SEQUL(ucBuff, numBytes, ucBuffLen) {	\
memset(ucBuff,0x00, 0x6);			\
ucBuff[1] = 0x2;				\
ucBuff[4] = numBytes;				\
ucBuffLen = 0x6 ;				\
}


/*****************************************************************
Macro : AT24C01A_READ_CARD_MEM_SEQUL

Input : Num Bytes to be read from the card

Output: ucBuff - Cmd data that is fed into Transmit_APDU
        ucBuffLen - Buffer length to send to Transmit_APDU

Description:This command reads card memory beginning at the current card address location.  
                  It is called a "Sequential Read" in the card specification.  
                  A length of 1 is equivalent to the "Current Address Read" function

*******************************************************************/

#define AT24C01A_READ_CARD_MEM_SEQUL(ucBuff, numBytes, ucBuffLen) {	\
memset(ucBuff,0x00, 0x6);			\
ucBuff[0] = 0x02;				\
ucBuff[1] = 0x2;				\
ucBuff[4] = numBytes;				\
ucBuffLen = 0x6 ;				\
}

/*****************************************************************
Macro : AT24C64A_WRITE_CARD_MEMORY

Input : addrMSB - MSB of the address from where the data needs to be written 
		addrLSB - LSB of the address from where the data needs to be written
		numBytes - number of bytes of data to be written
		databytes - data to be written
        
Output: ucBuff - Cmd data that is fed into Transmit_APDU
        ucBuffLen - Buffer length to send to Transmit_APDU


Description:Performs a card write beginning at the 16 bit address provided.  
                  Up to 255 bytes may be written at a time.

*******************************************************************/

#define AT24C64A_WRITE_CARD_MEMORY(ucBuff, addrMSB, addrLSB, numBytes, dataBytes, ucBuffLen) {	\
memset(ucBuff,0x00, (numBytes+0x6));			 \
ucBuff[1] = 0x3;				\
ucBuff[2] = addrMSB;	            \
ucBuff[3] = addrLSB;	            \
ucBuff[4] = numBytes;				\
memcpy(&ucBuff[5], dataBytes, numBytes);				\
ucBuff[4 + numBytes + 1] = 0x00;/*Assigning the Le */ \
ucBuffLen = 0x5 + numBytes + 0x1;				 \
}



/*****************************************************************
Macro : AT24C01A_WRITE_CARD_MEMORY

Input : addrMSB - MSB of the address from where the data needs to be written 
		addrLSB - LSB of the address from where the data needs to be written
		numBytes - number of bytes of data to be written
		databytes - data to be written
        
Output: ucBuff - Cmd data that is fed into Transmit_APDU
        ucBuffLen - Buffer length to send to Transmit_APDU


Description:Performs a card write beginning at the 16 bit address provided.  
                  Up to 255 bytes may be written at a time.

*******************************************************************/

#define AT24C01A_WRITE_CARD_MEMORY(ucBuff, addrMSB, addrLSB, numBytes, dataBytes, ucBuffLen) {	\
memset(ucBuff,0x00, (numBytes+0x6));			 \
ucBuff[0] = 0x02;				\
ucBuff[1] = 0x3;				\
ucBuff[2] = addrMSB;	            \
ucBuff[3] = addrLSB;	            \
ucBuff[4] = numBytes;				\
memcpy(&ucBuff[5], dataBytes, numBytes);				\
ucBuff[4 + numBytes + 1] = 0x00;/*Assigning the Le */ \
ucBuffLen = 0x5 + numBytes + 0x1;				 \
}


/*****************************************************************
Macro : AT24C64A_MEM_RESET

Input : None

Output: ucBuff - Cmd data that is fed into Transmit_APDU
        ucBuffLen - Buffer length to send to Transmit_APDU

Description:Performs a card "Memory Reset" function as described in the AT24C64A specification.

*******************************************************************/

#define AT24C64A_MEM_RESET(ucBuff,ucBuffLen) {	\
memset(ucBuff,0x00, 0x6);			\
ucBuff[1] = 0x4 ;				\
ucBuffLen = 0x6 ;               \
}



/*****************************************************************
Macro : AT24C01A_MEM_RESET

Input : None

Output: ucBuff - Cmd data that is fed into Transmit_APDU
        ucBuffLen - Buffer length to send to Transmit_APDU

Description:Performs a card "Memory Reset" function as described in the AT24C64A specification.

*******************************************************************/

#define AT24C01A_MEM_RESET(ucBuff,ucBuffLen) {	\
memset(ucBuff,0x00, 0x6);			\
ucBuff[0] = 0x02 ;				\
ucBuff[1] = 0x4 ;				\
ucBuffLen = 0x6 ;               \
}


#endif



