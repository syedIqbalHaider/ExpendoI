/*-----------------------------------------------------------------------------
 *
 *      Filename:       ATR.h
 *
 *      Product:        Card Slot Library
 *      Version:        1.00
 *      Author :        Vipul_K1
 *  	Module :	
 *          Defines all methods pertaining to ATR class.
 *
 * MODIFICATION HISTORY    :
 *
 * #      Date		    Who			History
 * ---  ---------    ---------		----------------------------------------
 * 1.   15 June 00    Vipul_K1		Created.
 *
 * 2    06 May 02	   Asha_s2	    Changed the variable "protocol" from 
 *									protected to public.
 * 3.   30 May 05     Asha_s2       ATR VALIDATION:fix for the wrong historical 
 *									bytes returned for a T=1 card  
 *******************************************************************************
 * Copyright (C) 2000 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language 
 * or computer language, in any form or by any means, electronic, mechanical, 
 * magnetic, optical, chemical, manual or otherwise, without the prior written 
 * permission of VeriFone Inc.
 *                                                VeriFone Inc.               
 *                                                III Lagoon Drive, Suite 400  
 *                                                Redwood City, CA 94065      
 ******************************************************************************
 */

#ifndef _ATR
#define _ATR  

#ifndef BYTE_COUNT  // ACT 2000 also defines BYTE
#ifndef __BYTE__
#define __BYTE__
typedef unsigned char     BYTE;
#endif
#endif // BYTE_COUNT

#ifndef __WORD__
#define __WORD__
typedef unsigned int      WORD; 
#endif
#ifndef __DWORD__
#define __DWORD__
typedef unsigned long     DWORD;
#endif

// ATR Status
#define ATR_SUCCESS				(char) 00

// Length of Historical and Interface BYTEs
#define MIN_HIST_LEN			(char) 0
#define MAX_HIST_LEN			(char) 15
#define MIN_IF_LEN				(char) 0
#define MAX_IF_LEN				(char) 33

// Errors 
#define ATR_FAILURE				(char) -11
#define SCHEME_NOT_SUPPORTED	(char) -12
#define UNINITIALIZED		    (char) -13

#define  E_INV_PARAM			(char) -14
#define  E_HIST_BYTES			(char) -15
#define  E_IF_BYTES				(char) -16

//ATR VALIDATION
//asha_s2 dated 30 May 05
//fix for the wrong historical bytes returned for a T=1 card
#define  E_INV_ATR				(char) -17

class ATR
{				

protected:

	char  statusATR;				// atr should be read only if this BYTE is 
									// set to ATR_SUCCESS during reset().
	char  histLen;					// stores length of Historical BYTEs. 
	BYTE  histBytes[MAX_HIST_LEN];	// Historical BYTEs.
	char  IFLen;					// stores length of InterFace BYTEs.
	BYTE  IFBytes[MAX_IF_LEN];		// Interface BYTEs.
    //char  protocol;

public:

	ATR(); 

	DWORD   setATR(BYTE *atrData, DWORD atrLen);
	char    getATRStatus(void);
	char    getHistoricalBytes(BYTE *histBuf);
	char    getInterfaceBytes(BYTE *IFBuf);
	char  protocol;
	

	~ATR();

};
#endif

