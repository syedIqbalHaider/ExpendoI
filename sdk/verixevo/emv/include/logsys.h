 /******************************************************************
  Filename:	    logsys.h
  Product:      Multiple Application Manager
  Version:      2.0
  Module:       LOGSYS
  Description:	interface file for LOGSYS API

  Modification History:
      #       Date         Who         Comments
#   Date    	Who     			Comments
1   22-Feb-00   Bruce Girdlestone	Initial Draft.
2. 11-May-2006  Vijay Kumar Jitta	ATS issue 398 fixed.
*******************************************************************/
/******************************************************************
   Copyright (C) 2006-2008 by VeriFone Inc. All rights reserved.

 No part of this software may be used, stored, compiled, reproduced,
 modified, transcribed, translated, transmitted, or transferred, in
 any form or by any means  whether electronic, mechanical,  magnetic,
 optical, or otherwise, without the express prior written permission
                          of VeriFone, Inc.
*******************************************************************/



#ifndef _LOGSYS
#define _LOGSYS 1

char* GetLogsysVersion(void);


#ifdef LOGSYS_FLAG

/* Logsys include file */

/* Different logging mechanisms */
#define LOGSYS_NONE				0
#define LOGSYS_FILE				1
#define LOGSYS_COMM				2
#define LOGSYS_COMM_FILE		3
/*
Add a pipe functionality as a new method of channelling debug information
*/
#define LOGSYS_PIPE				4 //BRUCE_G1: WWAF ENHANCEMENT 02/15/06


#define LOGSYS_PRINTF_FILTER		 0x00000400L

/* Logfile structure */
typedef struct LOGFILE_HEADER
{
#ifdef _TARG_68000
	int maxsize;
	int offset;
#elif __thumb 	//predator
	long maxsize;	//changed to long, OT : 33930, Santosh_R1 - 7 May 2009.
    long offset;	//changed to long, OT : 33930, Santosh_R1 - 7 May 2009.
#endif 	//_TARG_68000
}
PFR_LOGFILE_HEADER_STR;


/* Logfile size range */
#define PFR_CUST_MAX_LOGFILE_SIZE 102400 //15360
#define PFR_CUST_MIN_LOGFILE_SIZE 500
#define PFR_CUST_DEF_LOGFILE_SIZE 102400 //15360


#define PFR_DATE_SIZE 9
#define PFR_TIME_SIZE 7


#define POWER_FAIL_STAT_TABLE "PFR.DAT"




#define CRITICAL_ERROR			1
#define NON_CRITICAL_ERROR		2
#define NULL_POINTER_ERROR		3
#define LOG_MESSAGE				4
#define LOG_MESSAGE_FILTER		5


#ifdef __cplusplus
extern "C"
{
#endif

#ifdef _TARG_68000
void LOG_INIT(char * appName,int logType,unsigned long filter);
#elif __thumb 	//predator
void LOG_INIT(char * appName,short  logType,unsigned long filter);
#endif		//_TARG_68000
void vPfrStoreErrorMessage(char * message);
void vLogMessage (char * pchFormat,...);
void vLogMessageFilter (unsigned long filter,char * pchFormat,...);

void vLogMessageBinary(long lFilter ,const char * pchBuffer, short shLen) ;
#ifdef _TARG_68000
void vLogError(long lnError,char * pchFile, int inLineNumber,int inErrorType,unsigned long filter);
#elif __thumb 	//predator
void vLogError(long lnError,const char * pchFile, short  inLineNumber,short  inErrorType,unsigned long filter);
#endif		//_TARG_68000
void vLogComm(const char * message, short shLen);

#ifdef __cplusplus
}
#endif


/* Error logging MACRO - Use this to log the File, and Line number where a nonzero error has occured */
#define LOG_NONZERO_ERROR(a,b)	 if (a != 0) {vLogError((long) a,__FILE__,__LINE__,NON_CRITICAL_ERROR,b);}

/* Error logging MACRO - Use this to log the File, and Line number where a negative error has occured */
#define LOG_NEGATIVE_ERROR(a,b)	 if (a < 0) {vLogError((long) a,__FILE__,__LINE__,NON_CRITICAL_ERROR,b);}

/* Error logging MACRO - Use this to log a zero error response, i.e. Zero bytes were returned from a file/comm read */
#define LOG_ZERO_ERROR(a,b)	 if (a == 0) {vLogError((long) a,__FILE__,__LINE__,NON_CRITICAL_ERROR,b);}

/* Error logging MACRO - Use this to log the File, and Line number where a nonzero error has occured */
#define LOG_NONZERO_ERROR_CRIT(a,b)	 if (a != 0) {vLogError((long) a,__FILE__,__LINE__,CRITICAL_ERROR,b);}

/* Error logging MACRO - Use this to log the File, and Line number where a negative error has occured */
#define LOG_NEGATIVE_ERROR_CRIT(a,b)	 if (a < 0) {vLogError((long) a,__FILE__,__LINE__,CRITICAL_ERROR,b);}

/* Error logging MACRO - Use this to log a zero error response, i.e. Zero bytes were returned from a file/comm read */
#define LOG_ZERO_ERROR_CRIT(a,b)	 if (a == 0) {vLogError((long) a,__FILE__,__LINE__,CRITICAL_ERROR,b);}

/* Error logging MACRO - Use this to check for null pointer errors*/
#define LOG_NULL_POINTER(a,b)	 if ((char *) a == NULL) {vLogError((long) a,__FILE__,__LINE__,NULL_POINTER_ERROR,b);}

/* Log using printf format */


#define LOG_PRINTF(a)	{ vLogMessage a; vLogError((long) 0,__FILE__,__LINE__,LOG_MESSAGE,0xFFFFFFFF); }
#define LOG_PRINTFF(a)	{ vLogMessageFilter a; vLogError((long) 0,__FILE__,__LINE__,LOG_MESSAGE_FILTER,0xFFFFFFFF); }

#ifdef __cplusplus
extern "C"
{
#endif
int log_packet(long lnError, char *pchFile, int inLineNumber, int inErrorType, unsigned long filter, char *prefix,
					  unsigned long prefix_length, char *data, unsigned long data_length);
#ifdef __cplusplus
}
#endif
#define LOG_PACKET(a,b,c,d)	\
	do { \
		log_packet(0, __FILE__, __LINE__, LOG_MESSAGE, 0xFFFFFFFF, a,b,c,d); \
	} while(0)
#define LOG_PRINTF_BIN(a,b,c)	 if (b != 0) {vLogMessageBinary(a,(const char *) b,c);}
#ifdef __cplusplus
extern "C"
{
#endif
short log_hex_printf(char *file_name,unsigned int line_number,char  * tag, unsigned char*data , unsigned int length);
#ifdef __cplusplus
}
#endif

#define LOG_HEX_PRINTF(a,b,c) \
	do{ \
		log_hex_printf(__FILE__,__LINE__,a,b,c); \
	}while(0)


#else	// LOGSYS_FLAG not defined

#define LOG_INIT(a,b,c)
#define vPfrStoreErrorMessage(a)
#define vLogMessage (a,...)
#define vLogMessageFilter (a,b,...)
#define vLogError(a,b,c,d,e)
#define GetLogsysVersion()


#define LOG_NONZERO_ERROR(a,b)
#define LOG_NEGATIVE_ERROR(a,b)
#define LOG_ZERO_ERROR(a,b)
#define LOG_NONZERO_ERROR_CRIT(a,b)
#define LOG_NEGATIVE_ERROR_CRIT(a,b)
#define LOG_ZERO_ERROR_CRIT(a,b)
#define LOG_NULL_POINTER(a,b)
#define LOG_PRINTF(a)
#define LOG_PRINTFF(a)
#define LOG_PRINTF_BIN(a,b)
#define LOG_PACKET(a,b,c,d)
#define LOG_HEX_PRINTF(a,b,c)
#endif	// of LOGSYS_FLAG

#endif
