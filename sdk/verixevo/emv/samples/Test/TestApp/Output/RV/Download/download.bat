@echo off
rem ***************************************************************************
rem WARNING : PLEASE DO NOT ALTER THE CONTENTS OF THIS BATCH FILE
rem ***************************************************************************
rem Please set the ddl.exe location as per your system configuration.
rem ***************************************************************************
cls


if "%1" == "" goto INVALIDPARAMS

if "%1" == "1" goto AllCVMs

if "%1" == "2" goto NoNCVM

if "%1" == "3" goto NoOnlinePIN

if "%1" == "4" goto NoOnPINoNCVM

if "%1" == "5" goto OfflineOnlin

if "%1" == "6" goto OfflineOnly

if "%1" == "7" goto OffOnNoOnlPI

if "%1" == "8" goto NoCVMOnly

if "%1" == "9" goto SigOnly

if "%1" == "10" goto NoOffPIN

if "%1" == "11" goto NoNCVMT21

goto INVALIDPARAMS

:AllCVMs
echo Downloading for AllCVMs configuration

ddl -b115200 -p1 -i..\..\..\..\..\..\Resources\AllCVMs\mvt.dat -i..\..\..\..\..\..\Resources\AllCVMs\est.dat *Go=emvtest.out -Femvtest.dld 

goto exit


:NoNCVM 
echo Downloading for NoNCVM configuration

ddl -b115200 -p1 -i..\..\..\..\..\..\Resources\NoNCVM\mvt.dat -i..\..\..\..\..\..\Resources\NoNCVM\est.dat *Go=emvtest.out -Femvtest.dld 

goto exit


:NoOnlinePIN
echo Downloading for NoOnlinePIN configuration

ddl -b115200 -p1 -i..\..\..\..\..\..\Resources\NoOnlinePIN\mvt.dat -i..\..\..\..\..\..\Resources\NoOnlinePIN\est.dat *Go=emvtest.out -Femvtest.dld 

goto exit


:NoOnPINoNCVM
echo Downloading for NoOnPINoNCVM configuration

ddl -b115200 -p1 -i..\..\..\..\..\..\Resources\NoOnPINoNCVM\mvt.dat -i..\..\..\..\..\..\Resources\NoOnPINoNCVM\est.dat *Go=emvtest.out -Femvtest.dld 

goto exit


:OfflineOnlin 
echo Downloading for OfflineOnlin configuration

ddl -b115200 -p1 -i..\..\..\..\..\..\Resources\OfflineOnlin\mvt.dat -i..\..\..\..\..\..\Resources\OfflineOnlin\est.dat *Go=emvtest.out -Femvtest.dld 

goto exit


:OfflineOnly 
echo Downloading for OfflineOnly configuration

ddl -b115200 -p1 -i..\..\..\..\..\..\Resources\OfflineOnly\mvt.dat -i..\..\..\..\..\..\Resources\OfflineOnly\est.dat *Go=emvtest.out -Femvtest.dld 

goto exit


:OffOnNoOnlPI
echo Downloading for OffOnNoOnlPI configuration

ddl -b115200 -p1 -i..\..\..\..\..\..\Resources\OffOnNoOnlPI\mvt.dat -i..\..\..\..\..\..\Resources\OffOnNoOnlPI\est.dat *Go=emvtest.out -Femvtest.dld 

goto exit

:SigOnly
echo Downloading for SigOnly configuration

ddl -b115200 -p1 -i..\..\..\..\..\..\Resources\SigOnly\mvt.dat -i..\..\..\..\..\..\Resources\SigOnly\est.dat *Go=emvtest.out -Femvtest.dld 

goto exit


:NoCVMOnly
echo Downloading for NoCVMOnly configuration

ddl -b115200 -p1 -i..\..\..\..\..\..\Resources\NoCVMOnly\mvt.dat -i..\..\..\..\..\..\Resources\NoCVMOnly\est.dat *Go=emvtest.out -Femvtest.dld 

goto exit

:NoOffPIN
echo Downloading for NoOffPIN configuration

ddl -b115200 -p1 -i..\..\..\..\..\..\Resources\NoOffPIN\mvt.dat -i..\..\..\..\..\..\Resources\NoOffPIN\est.dat *Go=emvtest.out -Femvtest.dld 

goto exit

:NoNCVMT21

echo Downloading for NoNCVMT21 configuration



ddl -b115200 -p1 -i..\..\..\..\..\..\Resources\NoNCVMT21\mvt.dat -i..\..\..\..\..\..\Resources\NoNCVMT21\est.dat *Go=emvtest.out -Femvtest.dld



goto exit


:INVALIDPARAMS
echo -----------------------------------------------------------------------------
echo       : Download  1 for Downloading for AllCVMs configuration
echo       : Download  2 for Downloading for NoNCVM configuration
echo       : Download  3 for Downloading for NoOnlinePIN configuration
echo       : Download  4 for Downloading for NoOnPINoNCVM configuration
echo       : Download  5 for Downloading for OfflineOnlin configuration
echo       : Download  6 for Downloading for OfflineOnly configuration
echo       : Download  7 for Downloading for OffOnNoOnlPI configuration
echo       : Download  8 for Downloading for NoCVMOnly configuration
echo       : Download  9 for Downloading for SigOnly configuration
echo       : Download  10 for Downloading for NoOffPIN configuration
echo       : Download  11 for Downloading for NoNCVMT21 configuration
echo -----------------------------------------------------------------------------

:exit


