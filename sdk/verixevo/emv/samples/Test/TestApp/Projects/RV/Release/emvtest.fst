; File syntax :
; - lines beginning with ';' and empty lines are ignored
; - filenames are in "quotation marks"
; - do not remove the '/1', ..., '/4' section delimiters

[Files to sign]/1

;(1=in flash, 0=not in flash)

0 "W:\VXEMVAP\Samples\Test\TestApp\Output\RV\Files\Release\emvtest.out"



[Security Level]/2

;(0=SECURED, 1=CONTROLLED, 2=DEFAULT)

2



[Certificate name & location]/3

"W:\VXEMVAP\Samples\Test\TestApp\Output\RV\Files\Release\Certif.crt"



[Notes]/4



