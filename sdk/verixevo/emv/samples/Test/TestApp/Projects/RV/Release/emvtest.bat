REM *** To create the testharness application and also the application batch files ***

del /f ..\..\..\Output\RV\obj\*.o
del /f ..\..\..\Output\RV\files\debug\emvtest.out
del /f ..\..\..\Output\RV\files\debug\Signfile\RAM\emvtest.out.p7s
make -f emvtestmakefile -B

pushd ..\..\..\source\Resources
call buildAppDat.bat
popd