/******************************************************************
  Filename		:	emvappmsg.h
  Product		:     
  Version		:     
  Module		:   TestApp
  Description	:   Contains the main flow of EMV test harness. It makes use of the 
*					api provided by the Verix EMV Application to complete its transaction.
	
  
  Modification History:
    #		Date        Who				Comments
 *  1       06 Feb 07   Pavan_K1        Added a macro MSG_FORCE_DECLINE to handle the
 *                                      display in both the languages.
 *  2       08/Mar/07   Soumya_g1       Added CAPK expiry msg.
 *
 *  3       04/Sep/09	Mary_f1			INVMOD:
 *										If this module is downloaded to a Vx700 terminal, it should return
 *										E_INVALID_MODULE and the application should not continue.
 *  4	  12/Jan/12	Mary_F1		Ontime ID: 56193  Vx EMV 6.0.0 Terminal allowing cardholder 
 *										confirmation should display "Try Again" message if the status
 *										word for the SELECT response is other than 9000
 * 5		  10/Feb/12	Mary_F1		Ontime ID: 55409
 *										For a magstripe transaction, "Failed" message is displayed instead of "Success"	
 *	
 * Copyright (C) 2006 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language, in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual or otherwise, without the prior written
 * permission of VeriFone Inc.
 *                                                2099 Gateway Place
 *             					                  Suite 600
 * 					                              San Jose  CA  95110 
 *              				                  USA
 *
 *
 *****************************************************************************/

#ifndef EMVAPMSG_H
#define EMVAPMSG_H 

#define MSG_ERR_UNKNOWN_PACKET  	                0      
#define MSG_TRANS_APPROVED			                1
#define MSG_TRANS_DECLINED			                2
#define MSG_OFFLINE_APPROVED	                    3
#define MSG_OFFLINE_DECLINED	                    4
#define MSG_OFFLINE_NOT_ALLOWED	                    5
#define MSG_NON_EMV	                                6
#define MSG_FAILURE	                                7
#define MSG_CARD_REMOVED	                        8
#define MSG_CARD_BLOCKED_BY_ISS 	                9
#define MSG_APPLICATION_BLOCKED	                    10
#define MSG_CANDIDATELIST_EMPTY	                    11
#define MSG_EMV_CHIP_ERROR	                        12
#define MSG_BAD_DATA_FORMAT	                        13
#define MSG_APP_NOT_AVAIL	                        14
#define MSG_TRANS_CANCELLED     	                15
#define MSG_EASY_ENTRY_APPL	                        16
#define MSG_ICC_DATA_MISSING	                    17
#define MSG_CONFIG_FILE_NOT_FOUND	                18
#define MSG_SELECT_FAILED	                        19
#define MSG_USE_CHIP_READER     	                20
#define MSG_PLS_REMOVE_CARD	                        21
#define MSG_BAD_ICC_RESPONSE	                    22
#define MSG_EMV_FAILURE	                            23
#define MSG_USE_MAG_STRIPE      	                24
#define MSG_TAG_NOT_FOUND	                        25
#define MSG_INVALID_CAPK_FILE	                    26
#define MSG_FAILED	                                27    
#define MSG_SIGNATURE_REQUIRED	                    28
#define MSG_ENTER_PIN	                            29
#define MSG_INSERT_CARD         	                30
#define MSG_PIN_REQD	                            31
#define MSG_PIN_LAST_CHANCE	                        32
#define MSG_PIN_TRYLIMIT_EXCEEDED	                33
#define MSG_PIN_CANCELLED	                        34
#define MSG_PIN_BYPASSED	                        35
#define MSG_CAPK_CHECKSUM_FAILED	                36
#define MSG_ERROR	                                37
#define MSG_PERFORM_SIGNATURE	                    38
#define MSG_INVALID_PIN	                            39
#define MSG_SELECT_STRING	                        40
#define MSG_PROCESSING                              41
#define MSG_LANGUAGE_STRING 	                    42
#define MSG_CARD_INSERTED	                        43
#define MSG_ERR_INVALID_PARAM   	                44
#define MSG_COMM_ERROR          	                45
#define MSG_SERVICE_NOT_ALLOWED 	                46
#define MSG_APP_NOTYET_EFFECTIVE	                47
#define MSG_APP_EXPIRED         	                48
#define MSG_CHIP_MALFUNCTION    	                49
#define MSG_ENTER_ONLINE_PIN    	                50
#define MSG_INSERT_OR           	                51
#define MSG_SWIPE_CARD          	                52
#define MSG_PIN_BLOCKED         	                53
 
#define MSG_DO_YOU_WANT_TO                          54
#define MSG_GO_FOR_FALLBACK                         55
#define MSG_YES                                     56
#define MSG_NO                                      57
#define MSG_SWIPE_MAGNETIC_CARD                     58
#define MSG_FAILED_TO_CONNECT                       59
#define MSG_PRESS_ANY_KEY                           60
#define MSG_PLEASE_WAIT                             61
#define MSG_PLEASE_CALL_BANK                        62
#define MSG_SALE_AMOUNT                             63
#define MSG_APPROVE_TRANS                           64
#define MSG_SENDING_REVERSAL                        65
#define MSG_SENDING_DATA                            66
#define MSG_RECEIVING                               67
#define MSG_FORCE_ONLINE                            68
#define MSG_WAIT_FOR_CUSTOMER                       69
#define MSG_PRINT_RECEIPT                           70
#define MSG_NEXT                                    71
#define MSG_PREV                                    72
#define MSG_PINPAD_NOT_PRESENT                      73
#define MSG_ENTER_KEY                               74
#define MSG_DIVERSIFICATION_VALUE                   75
#define MSG_PINPAD_INITIALISED                      76
#define MSG_SCRIPT_PROCESSING_RESULTS               77
#define MSG_INITIALIZE_PINPAD                       78        

#define MSG_CASHBACK_AMOUNT                         79
#define MSG_PIN_SESSIONPROGRESS          	        80
#define MSG_PIN_SESSIONCOMPLETE         	        81
#define MSG_INVALID_ATR                             82
#define MSG_CAPK_FILE_NOT_FOUND                     83
#define MSG_FORCE_DECLINE                           84 //Pavan dated 6-Feb-07 added for the Force decline message.
#define MSG_EXPIRED_CAPK_FILE                       85 //Soumya_g1 8/Feb/07 added to display expired capk msg.
#define MSG_NO_CAPK_DATA                            86 //Pavan_K1 dated 28-Mar-07 added to display the capk data not found message.
#define MSG_INVALID_MODULE                          87 //INVMOD:
#define MSG_TRY_AGAIN                         		88 //Ontime ID: 56193
//Commented as this display not needed
//#define MSG_SUCCESS                         		89 //Ontime ID: 55409

//POS_CNCL
#define MSG_EXT_PIN_CNCL							89
#define MSG_EXT_PIN_BYPASS							90
#define MSG_TRANS_TIMED_OUT                    		91


#endif 
