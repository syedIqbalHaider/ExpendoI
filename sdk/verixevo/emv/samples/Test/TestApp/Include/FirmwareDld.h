#ifndef __FIRMWAREDLD_H__
#define __FIRMWAREDLD_H__

//Firmware Dld 
#define	EXT_IFD_FW_RESTART_DLD  25
#define	EXT_IFD_FW_INVALID_PARAMETER	26

#define ESCR_VERSION_NUM 						"3.0.0.01"



 int EXT_IFD_Scrdownload(const char *bin_file, const char *sig_file, void (*callbackfn)(int cnt, int tot));
 void EXT_IFD_Scrversion(unsigned long *l2_ver,unsigned long *bl_ver);
 void EXT_IFD_ESCRver(unsigned char* ucVerBuf) ;


#endif 


