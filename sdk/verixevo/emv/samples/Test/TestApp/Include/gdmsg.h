/******************************************************************
  Filename		:	gdmsg.h
  Product		:     
  Version		:     
  Module		:   TestApp
  Description	:   Contains the main flow of EMV test harness. It makes use of the 
*					api provided by the Verix EMV Application to complete its transaction.
	
  
  Modification History:
    #		Date        Who				Comments
	
 * Copyright (C) 2006 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language, in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual or otherwise, without the prior written
 * permission of VeriFone Inc.
 *                                                2099 Gateway Place
 *             					                  Suite 600
 * 					                              San Jose  CA  95110 
 *              				                  USA
 *
 *
 *****************************************************************************/
define MSG BIN
{
   (char,  szMessage[21],          "Display Message")
}
