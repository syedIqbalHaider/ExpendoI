/******************************************************************
 * Filename		:	emvproto.h
 * Product		:     
 * Version		:     
 * Module		:   TestApp
 * Description	:   Contains the main flow of EMV test harness. It makes use of the 
 *					api provided by the Verix EMV Application to complete its transaction.
 *	
 * 
 * CONTENTS   :
 * MODIFICATION HISTORY    :
 *
 * #    Date        Who            History
 * ---  --------    ----------     ----------------------------------------
 * 1    22 Nov 06   t_arvind_g1    Created for Vx EMV Module 4.0.0
 * 2    06 Mar 07   Pavan_K1       Added the function prototypes which are added newly for the Common module.
 * 3    14 May 07   Pavan_K1       Changed the signature of getTransactionAmount tohandle Account type slection.
 * 4    10 Jul 07   Purvin_p1      Changed return type of getUsrPin()   from short to UShort and change for bAppIsMultipleOccurencesAllowed from short to bool.
 * 5    13 Jul 07   Purvin_P1      Changed return type of inOnlinePINFunc()   from int to UShort.
 * 6	10 Jul 09   Mary_F1		   RETVALUSH:
 *								   Changed the return value from short to unsigned short    	
 * 7    03 Aug 09   Kishor_S1      ECTXNFLW: usGetECData API is added to Issue
 *                                 GetData CMD if 9F74 Tag is present(Moved the Fix)
 * 8	22 Oct 09	Mary_F1		   UPTMAGREAD
 *								   Added for mag read testing.	
 * 9	29/10/09	Kishor_S1	   UNATTENDED:Changes for unattended terminal types moved
 * 10   03/Jun/11   Mary_F1        MULTIAVN: Support for multiple application version number check
 * 11   03/Jun/11   Mary_F1        ExtAuth6985: The EMV Kernel behavior is modified to cater to the different application needs,
 *	  							   to terminate the transaction or to continue with the transaction on receiving the status word
 *								   as 0x6985 for the external authenticate command.
 * 12	18/Aug/11	Kishor_S1	   NAND_FLASH : Changes for Issuer Script processing accept through buffer instead of file
 *
 * Copyright (C) 2009 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language, in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual or otherwise, without the prior written
 * permission of VeriFone Inc.
 *                                                2099 Gateway Place
 *             					                  Suite 600
 * 					                              San Jose  CA  95110 
 *              				                  USA
 *
 *
 *****************************************************************************/

#ifndef _EMVPROTO_H
#define _EMVPROTO_H
// emvtest.c
int inRunEMVTransaction(void);
int inBuildAuthRequest(byte *message);
byte processOnline(void);
void sendReversal(void);
//byte processReferral(unsigned char source);//NAND_FLASH changes

byte processReferral(unsigned char source, byte* pScript71, unsigned short usScript71Len, byte* pScript72, unsigned short usScript72Len );
int inSendToHost(byte *request, byte *response, int size);
//int createScriptFiles(byte *buffer);//NAND_FLASH changes

int createScriptFiles(byte *scriptBuf, byte* pScript71, unsigned short *usScript71Len, byte* pScript72, unsigned short *usScript72Len );
void initPinPad(void);
//T_nabakishore_b1 18/07/07
//added for the function short shRetrieveMessage(short shMsgID,char * smsgBuffer)
short shRetrieveMessage(short shMsgID, char * sMsgBuffer);

// emvui.c
int inGetString(char *string, int size);
int inPINEntryFunc(char *pchPinBuff, int inOption);
//RETURN_TYPE: change return type from int to UShort.
//int inOnlinePINFunc(void);
unsigned short inOnlinePINFunc(void);
//T_nabakishore_b1 18/July/07
//added for the function void resetPinTries(unsigned char ucOpt, unsigned short ucBPKey );
void vSetPinParams(void);
//T_nabakishore_b1 18/07/07
//added for the function int inGetTermDecisnOnForceDecline(void)
int inGetTermDecisnOnForceDecline(void);
//T_nabakishore_b1 18/07/07
//added for the function void displayPINPrompt(void)
void displayPINPrompt(void);

void vdPromptManager(unsigned short inCondition);
int inMenuFunc(char **labels, int numLabels);
int inEnterAmountWithPrompt(char *prompt, char *amount, int maxlen);
unsigned short usAmtEntryFunc(unsigned long * lnAmountValue);
char chGetKeyPress(void);
void vdFlushKeyboard(void);
short shGetAutoSelectFlag(void);
int inGetTerminalDecision(void);
byte inSetTransactionType(void);
int inManualApproval(void);
void vdGetApprCode(byte *code);
int inOverrideChipScreen(void);
char inGetOption(void);
char inGetKeyOption(void);
int inGetPinPadInit(void);
unsigned short getTermUsrPin(unsigned char *pin); //RETVALUSH:
int inGetPrintReceipt(void);
void vdPrintReceipt(short inCondition);
void ascii_to_binary(BYTE *dest, BYTE *src, int length);
void hex2asc(BYTE *outp, BYTE *inp, int length);
int magneticStripe(char* track2);
EMVBoolean bIsCardBlackListed(byte * pan, unsigned short panlen,byte * panSeqNo, unsigned short panSeqlen);
int inGetPTermID(char *ptid);
EMVBoolean bUpdateLogforSplitSales(void);
int inRunMgnStripeTransaction(char* track2);
int inBuildFallbackMsg(byte *message);
int inBuildReversalMsg(byte *message);
//int inOnlinePINFunc(void);
int inGetStringWithPrompt(char *prompt, char *string, int maxlen);
char chConvertKey(char key);
short readRecord(int , char *);
// Added for Multi language support.
short inInitDspMsgs(void);
short shLoadDSPMsgFile(char * shFileName);
short shDisplayMessage(short shMsgID, short shCol, short shRow);
short shDSPRetrieveString (char * shFileName, char * szRetMsg, short shMsgID);
short vdSelPreferredLang(void);

int  ValidateTrack2Data(void);

#ifdef _TARG_68000

int findTag(unsigned int tag, byte *value, int *length, const byte *buffer);
int getNextRawTLVData(unsigned int *tag, byte *data, const byte *buffer);
int getNextTLVObject(unsigned int *tag, int *length, byte *value, const byte *buffer);

#endif

#ifdef __thumb

short getNextTLVObject(unsigned short *tag, short *length, byte *value, const byte *buffer);
short findTag(unsigned short tag, byte *value, short *length, const byte *buffer);
short getNextRawTLVData(unsigned short *tag, byte *data, const byte *buffer);

#endif

//Pavan_K1 dated 28-Mar-07 moved all application implemented prototypes of the coomon.h to here.
EMVResult   getLastTxnAmt(Ulong *amt);    
//EMVResult   getCSNValid(byte *CardCSN);
EMVResult   usEMVPerformSignature(void);
EMVResult   usEMVPerformOnlinePIN(void);
//EMVBoolean  bEMVIsCardBlackListed(byte *pan, unsigned short panLen, byte *panSeqNo, unsigned short panSeqLen);
EMVResult   usEMVIssAcqCVM(unsigned short issacq, unsigned short *code); 

//Pavan_K1 dated 14-May-07 changed the prototype to handle the Account selection after app sel.
//EMVResult   getTransactionAmount(void);
EMVResult getTransactionAmount(unsigned short usID);

EMVResult   usEMVCheckICCResponse(byte *pCmd, unsigned short usRetVal);
EMVResult   usEMVIsVelocityCheckReqd(void);
void        usEMVDisplayPINPrompt(void);
void        usEMVDisplayErrorPrompt(Ushort errorID);
unsigned short  getUsrPin( unsigned char* );

int inMenuFunction(char **labels, int numLabels);
//Pavan_K1 dated 27-Feb-07 added the function prototypes.
void vSetDefaultFunctionPointers(void);
void vSetAppFunctionPointers(void);
//Pavan_K1 dated 07-Mar-07 added the function prototypes used in emvui.c
void vAppDisplayPINPrompt(void);
EMVBoolean bAppIsMultipleOccurencesAllowed(byte *pAid);
unsigned short usAppESTValidateCSN(byte *CardCSN);
unsigned short getAppCapkExp( const byte* aid, const byte capkIndex, \
                       byte* pstCapk, short *capkLen, \
                       byte* pstCapkExp, short* pCapkExpLen );
EMVResult getAppTerminalParam(Ushort id,byte* data, Ushort *pRetLen);
unsigned short getAppTransactionAmount(void);
//T_Gangadhar_S1   15-5-08 BATCH_DATA_CAPTURE: 
int iBatchCapture(void); 
int iGetPrintBatchFile(void);
int iPrintBatchFile(int iRes);
int SVC_wAIT( unsigned long max_wait);
void Write(short prn,char *out,int len);
//Brabhu_H1 Dated 08 July 2008
//ECTXNFLW: usGetECData API is added to Issue Get Data CMD's if EC Issuer Auth 
//Code Tag is present
unsigned short usGetECData(void);

//VERIXPRTNG: Porting to Verix
#ifdef __thumb
//UPTMAGREAD
void vdMagStripeTrans(void);
#endif
//UNATTENDED:
//Added this prototype for unattended terminals
EMVBoolean Is_Unattended_Terminal(unsigned char term_type);

//MULTIAVN:
Ushort usGetTermAVN(srMultiAVN *pMultiAVN, int *iAVNCount);

//ExtAuth6985:	Ontime ID: 55477
Ushort usGetExtAuthDecision(unsigned short usStatusWord);

//POS_CNCL
unsigned short usGetPOSDecsn(void);


#endif 
