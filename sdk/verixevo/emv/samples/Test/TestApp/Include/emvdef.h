/******************************************************************
  Filename		:	emvdef.h
  Product		:     
  Version		:     
  Module		:   TestApp
  Description	:   Contains the main flow of EMV test harness. It makes use of the 
*					api provided by the Verix EMV Application to complete its transaction.
	
  
  Modification History:
    #		Date        Who				Comments
 *  1.  14 May 2007   Brabhu_H1   Interac specific changes are done for language 
 *                                selection by introducing support for 3rd language
 *  2.	29 Oct 2009	  Kishor_S1	  UNATTENDED:Changes for unattended terminal types moved	      
 *  3.	13 Nov 2009	  Kishor_S1	  AIPBATCHADD: AIP addition fix for Ontime ID 38310  
 *	
 * Copyright (C) 2009 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language, in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual or otherwise, without the prior written
 * permission of VeriFone Inc.
 *                                                2099 Gateway Place
 *             					                  Suite 600
 * 					                              San Jose  CA  95110 
 *              				                  USA
 *
 *
 *****************************************************************************/

#ifndef _EMVDEF_H
#define _EMVDEF_H

#include "vxemvap.h"
#include "VXEMVAP_confio.h"

#define MAX_MESSAGE_SIZE 268
#define CARD_REFERRAL 204
#define NONEMV 207

#define SALE 0x00
#define CASH_ADVANCE 0x01
#define ADJUSTMENT 0x00
#define SALE_WITH_CASHBACK 0x09

#ifdef __thumb
#define MAX_DISP_MSG_LENGTH   24
#endif

#ifdef  _TARG_68000

 #define MAX_DISP_MSG_LENGTH   22

#endif

typedef struct TagMSG_REC
{
    char szDisplayMessages[MAX_DISP_MSG_LENGTH];		 
}MSG_REC;


#define SIZE_MSG_REC		sizeof(MSG_REC)

//UNATTENDED:
//Kishor_S1 added on 29-10-09
//Added a new constant for unattended terminal types
#define UNATTENDED_TERM_MASK   0x0F 
#define MSG_ENG_FILENAME	"MSG_ENG.DAT"
#define MSG_SPN_FILENAME	"MSG_SPN.DAT"
#define MSG_FRN_FILENAME	"MSG_FRN.DAT"


#define INVALID_MSG_FILE   299

//Brabhu_H1 dated 14 May 2007
//Interac specific changes are done for language selection
//by introducing support for 3rd language
struct stLangConfig
{
    char szSupportedlanguage1[2];		 
    char szSupportedlanguage2[2];
	char szSupportedlanguage3[2];
    char szDefaultlanguage;		 
    char szActiveLanguage;
};

//T_Gangadhar_S1 
typedef struct BatchRecord
    {

	 unsigned char			ucAIP[2];//AIPBATCHADD: Ontime ID 38310 AIP addition Kishor_s1 added on 13-11-09 
	 unsigned char			ucAtc[3];
	 unsigned char			ucAuc[3];
	 unsigned char			ucAppCrypto[9];
	 unsigned char			ucCid[2];
	 unsigned char		    ucCVMList[253];
 	 unsigned char			ucCVMResult[4];
	 unsigned char			ucIFDSerialNum[9];
	 unsigned char        	ucIACdef[6];
	 unsigned char        	ucIACden[6];
	 unsigned char        	ucIAConline[6];
	 unsigned char			ucIad[33];
	 int 					iNumScripts;
	 unsigned char			ucIssuerScriptResults[70];
	 unsigned char			ucTermCap [4];
	 unsigned char			ucTermType[2];
	 unsigned char   		ucTvr[6] ;
	 unsigned char			ucTsi[3];
	 unsigned char			ucUnpredictNum[5];
	 unsigned char 			ucAcqID[7];
	 unsigned long			ulAuthAmt;
	 unsigned long			ulOtherAmt;
	 unsigned char			ucAppEffDate[7]; //store as YYMMDD
	 unsigned char			ucAppExpDate[7]; //store as YYMMDD
	 unsigned char			ucPanNum[21];
	 unsigned char			ucPanSeqNum[4];
	 unsigned char			ucAuthCode[7];
	 unsigned char			ucARC[3] ;
	 unsigned char			ucIssuerCtryCode[3];
	 unsigned char			ucMercCatCode[3];
	 unsigned char			ucMercID[16];
//	unsigned char			cMsgType[2];
	 unsigned char			ucPOSEntryMode[2];
	 unsigned char			ucTermCtryCode[3];
	 unsigned char			ucTermID[9];
	 unsigned char			ucTranCurrCode[3];
	 unsigned char			ucTxnDate[7]; //store as YYMMDD
	 unsigned char			ucTxnTime[7]; //store as hhmmss
	 unsigned char			ucTransType[2] ;

   }BATCH_REC;

#endif
