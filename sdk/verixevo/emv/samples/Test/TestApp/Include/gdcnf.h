define CNF BIN
{
   (char,  szUARTBaud[6],				"Baud Rate")
   (char,  szUARTDataBits[1],			"Data Bits")
   (char,  szUARTParity[1],				"Parity")
   (char,  szUARTStopBits[1],			"Stop Bits")
   (char,  szUARTTimeout[3],			"Block Waiting Time")
}
