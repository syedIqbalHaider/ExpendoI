/******************************************************************
  Filename		:	emvui.c
  Product		:     
  Version		:     
  Module		:   TestApp
  Description	:   Contains the main flow of EMV test harness. It makes use of the 
*					api provided by the Verix EMV Application to complete its transaction.
	
  
  Modification History:
    #		Date        Who				Comments
 *  1       08 Jan 07   t_arvind_g1     Added as per new requi. to decline the transaction before 
 *                                      GEN AC(FORCE_DECLINE).
 *  2       17 Jan 07   t_arvind_g1     Added new requi. to bypass offline pin processing 
 *                                      if any one offline pin is bypassed
 *  3       06 Feb 07   Pavan_K1        Handled the VMP displays and handled the force decline
 *                                      message in the languages.
 *  4       08 Feb 07   Pavan_K1        Increased the display time of the mesg to 3 sec and
 *                                      changed the amount entry to the 2nd line of the display.
 *	
 *  5       19 Feb 07   Purvin_p1       Fix Made for Printing Application label and Application Preferred Name.
 *  6       20 Feb 07   Purvin_p1       Fix made for printing correct value for Pan Seq no.

 *  7       27/Feb/07   Soumya_G1       TC# V2CL0320001 - getTermUsrPin() - Send E_USR_ABORT to the library
 *                                      when pin is cancelled.
 *  8       28 Feb 07   t_arvind_g1     Added to set Pin Parameters for Secure Pin.
 *  9       06 Mar 07   Pavan_K1        Added Application implemented functions for the common module.
 *	
 *  10      03 Mar 07   Purvin_p1       New ERS requirement Print Encihphered Pin block on Print Receipt.
 *  11      12 Apr 07   Pavan_K1        Fix for OntimeId 8459, bypassing all the offline cvms even the offline bypass flag is not set.
 *                                      Added Invalid pin message in the displayerrorpromt function.
 *  12      14 May 07   Pavan_K1        Changed the amount entry function to handle the Account type selection.
 *
 *  13      18 mar 07   Purvin_p1       Checking for Terminal Additional Capabality if Cashback bit is not set in byte1 then it will not display "Sale with Cashback" option.
 *  14      31/May/07   Purvin_p1       Fix made for Clerify ID 070530-5.

 *  15      18/Jun/07  Soumya_G1        This is for the changed requirement on the Bypass feature.
                                        Now if Any One pin is bypassed, then any consecutive pin should be bypassed.
 *  16      14 Jun 07   Brabhu_H1       SPLTAMT: Increased the size of amount buffer for split sales
 *  17      10 July 07  Purvin_p1       RETURN_TYPE: Changed return type of getUsrPin(),inOnlinePINFunc()   from short to UShort.
 *  18      11/Jul/07   Tisha_K1        AMTCLBK: Modified the usAmtEntryFunc callback API invoking. New IDs are
 *                                                 are identified to describe whether the callback API is invoked for amount/cashback/account type
 *  19      18/Jul/07   Purvin_P1       Ontime Id :13809, Change data type from short to Ushort.
 *  20      19 Jul 07   Pavan_K1        Fix made to update the 81 and 9F02 tag with the cash back amount if the transaction is cashback.
 * 	21      24 Jul 07   Brabhu_H1       ENCPINBK: For made for onTimeID 13990 
 *                                      to display the deciphered PIN block
 *  22      04/Oct/07   Pavan_K1        SIGPERFM: Setting the global signature flag to 0 if the retvalue
 *                                      is other than EMV_SUCCESS.
 * 23       15/Mar/08 Purvin_P1 		Vx700: there is not function key for vx700 terminal, So all function key are mapped with number key.
		                                         F1=Numeric 1,F2=Numeric 2,F3=Numeric 3 and F4=Numeric 4
 *	                                         
 * 24      24/April/08 T_gangadhar_s1   Vx700: for ONTIME id 22518 added to support to work all function keys for all Vx terminal execept Vx700
 * 25      13/Jun/08   Purvin_P1          FIME ISSUE : To display Application Cryptogram in batchdata.
 * 26		18/Jun/09	Mary_F1			090202-1383
 *										oLFlag is not properly reset
 * 27		06/Jul/09	Mary_F1			BUFSIZE
 *										Buffer size is checked for memcpy() and strcpy()
 * 28		07/Jul/09	Mary_F1			CodeReview:
 *										Incorporated code review feedback
 * 29		10/Jul/09	Mary_F1			RETVALUSH:
 *								   		Changed the return value from short to unsigned short    
 * 30		21/Jul/09	Mary_F1			TG99TOCLXN
 *										Tag 99 (PIN Block) should be updated in the collection
 * 31       03/Aug/09   Kishor_S1       ECTXNFLW: Issue Get Data CMD's if EC Issuer Auth Code Tag is present (Moved the fix)
 *
 * 32		28/Aug/09	Mary_F1			Ontime ID : 36805 
 *	  									Cashback amount should not be prompted, if the PDOL has cashback(9F03)
 *	  									but the transaction type is other than SALE_WITH_CASHBACK
 * 33		04/Sep/09	Mary_F1			INVMOD:
 *										If this module is downloaded to a Vx700 terminal, it should return
 *										E_INVALID_MODULE and the application should not continue.
 * 34		18/Sep/09	Kishor_S1		Ontime Fix for 37306/36684 : No exact match b/w Card PAN and PAN in balcklistedcard.txt, 
 *										still terminal is setting Card appears in Exception file bit.
 * 35		06/oct/09	Mary_F1			SPLTSLES: 
 *										Reverted the support for split sales log processing for unattended terminals
 * 36		14/Oct/09	Kishor_S1		INVMOD: commented the INVMOD changes as per the confimation mail		
 * 37		27/Oct/09	Kishor_S1		MENU Changes: For Terminals other than Vx700			
 * 38		13/Nov/09	Kishor_S1		AIPBATCHADD: AIP addition fix for Ontime ID 38310 
 * 39 		17/Nov/09	Kishor_S1		CAPK_DISP_BLOCK : CAPK display messages blocking as per confirmation mail
 * 40		 5/Jan/10	Kishor_S1	  	ENABLENUMKEYS:Changes made to support numeric keys selection
 *										adding "ucAllowKeys" variable for Vx680 & Vx520 terminals. 
 * 41       03/Mar/10	Mary_F1			CM.091	Transaction should fallback to magswipe after an ICC read failure
 *										for offline only configuration
 * 42       01/Feb/11   Mary_F1			TxnTpeMenu: Transaction type should be displayed 
 *                                      acc. to the additional terminal capabilities
 * 43       03/Jun/11   Mary_F1			BKSPKEY: Terminal should re-prompt the previous prompt if backspace
 *										key is pressed during the "ENTER PIN" prompt
 * 44		07/Sep/11	Kishor_S1		SECURE_PIN_MODULE : Changes to accept SECURE PIN entry only.
 * 45		01/Dec/11	Kishor_S1		NAND_FLASH  : Config.sys File access changes incorporated.
 * 46		12/Jan/12 Mary_F1		Ontime ID: 56193  Vx EMV 6.0.0 Terminal allowing cardholder 
 *										confirmation should display "Try Again" message if the status
 *										word for the SELECT response is other than 9000
 * 47           25/01/12  Mary_F1			Ontime ID: 61398	Vx EMV 6.0.0 Enter OL PIN is not displayed for SECURE PIN.
 * 48    		05/Feb/12 Mary_F1		Ontime ID: 61709	Vx EMV 6.0.0 
 *										PAN is printed twice in Batch Data Capture
 * 49		09/Feb/12 Mary_F1		Ontime ID: 61519	Vx EMV 6.0.0
 *									      Terminal should display "Not Accepted" message.
 * 50		09/Feb/12 Mary_F1		Ontime ID: 61517, 61520
 *										Terminal should display that "chip cannot be read"
 * 51           09/Feb/12 Mary_F1		Ontime ID: 61562 Vx EMV 6.0.0
 *										TVR/TSI are displayed along with "Try Again" message
 * 52		10/Feb/12 Mary_F1		Ontime ID: 55409 Vx EMV 6.0.0
 *										For a magstripe transaction, "Failed" message is displayed instead of "Success"
 * 53		06 Mar 12	Kishor_S1	SUB-PINBYPASS : Changes made to use variable set through test application 
 *									instead of reading environment variable BYPASS_PIN
 * 54           25 May 12  Mary_F1             CJ.119.00
 *										 Fix for the issue floor limit exceeded bit is not set if the PAN sequence number
 *										 is missing in the card.
 *
 * 55           24 July 12   Tisha_K1            Fix for the OnTime Issue: #66572
 * 56           03 Dec  12   Mary_F1		ICTIMOUTDELAY: reduced the wait time from 3sec to 1sec as this is causing delay
 *									for Inter Char Time Out in case of secure PIN. 
 *
 * Copyright (C) 2010 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language, in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual or otherwise, without the prior written
 * permission of VeriFone Inc.
 *                                                2099 Gateway Place
 *             					                  Suite 600
 * 					                              San Jose  CA  95110 
 *              				                  USA
 *
 *
 *****************************************************************************/

#include <svc.h>

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <common.h>

#include "emvdef.h"
#include "emvproto.h"
#include "apacspin.h" 
#include "emvpin.h" 
#include "emvappmsg.h"
#include "Eutils.hpp"
#include "logsys.h"

#define usAmtEntryAccountSelFunc usAmtEntryFunc


extern short          shDispModFlag;
extern int            con;
extern int            pinSigRequired;
extern unsigned char  CKclear[16];
extern unsigned char  CKencr[16];
extern int            masterKeyIndex;
extern int            ppPresent;
extern byte           *issuerScriptResults;
extern int            numScripts;
extern short          Bypass_Flag;
extern short 		  gshUnAttended ; //Purvin_p1 date 30/April/2008 added to Support Unattended Terminal.
extern unsigned char  termTypeBuf[2]; //CM.091
//SECPINTIMOUT
extern unsigned char ucAbrtOnPINTOut;

//NUM_PIN_DIGITS
extern short	shLogSysDebug;
extern short   shmaxDebugmode;


unsigned long         glnAmount = 0;
static int            oLFlag=0;
//t_arvind_g1 dated 17 Jan 07 
//added as per new requ. to bypass the offline pin 
//flag is used to set when offline pin is bypassed.
short                 shPin_Bypass = 0;

//A+AUTOFT: Flag used to avoid user intervention	
extern int gAPPFlag;
extern short			gshInterac; //Purvin_p1 date 05/May/2007 added for Interac.

extern unsigned char 		ucAllowKeys;
extern short		  shVx700Flag; //T_gangadhar_s1  date 24/April/2008 added to support all vx terminal 
unsigned long         ulCheckSplitSalesAmt(void);

void         usEMVDisplayErrorPrompt(unsigned short ID);
EMVResult    usEMVPerformSignature(void);
EMVResult    usEMVPerformOnlinePIN(void);
extern short shRetrieveMessage(short shMsgID, char * sMsgBuffer);
	
//Ganesh_P1 30-Jun-10 PCI_CHANGE : Padding PAN data
extern unsigned char apppan[32];
extern unsigned char ucCardholdername[64], ucSvcCode[8],ucAppExDate[8], ucTrk2Data[48] , ucPinData[16], ucARC[3], ucTermID[9+8],ucUnpredictNum[16] ;
//extern unsigned short panlen;
extern unsigned short panlen, cnamelen,svclen,explen,trk2len,pinlen, shARClen, shTermIDlen, shAmt_Auth_len, shOther_Amt_len, shUnpredictNum_len;
extern unsigned long ulAuthAmt, ulOtherAmt;
struct spsales
{
    char panStr[21];
	//CJ.119.00
	//char panSeqStr[3];
	//Brabhu_H1 dated 14 June 2007
	//SPLTAMT: Increased the size of amount buffer from 9
	char trnAmt[12];
};

struct spsales stSpsales;

//NAND_FLASH  : Config.sys File access changes
extern char	szChinaFlag[2]; 
extern char	gszECTXLimitAmount[13];		



//t_arvind_g1 dated 28 Feb 07 Added to prompt "Enter Pin" 
//when "E_SECURE_PIN" is returned from getusrpin
/*--------------------------------------------------------------------------
    Function:		displayPINPrompt
    Description:	To prompt "Enter Pin" when "E_SECURE_PIN" is returned from getusrpin
    Parameters:		void
    Returns:		
	Notes:          
--------------------------------------------------------------------------*/

void displayPINPrompt(void)
{	
	clrscr();
//	write_at("Enter PIN\n",strlen("Enter PIN\n"),1,1); 

	shDisplayMessage(MSG_ENTER_PIN, 1,1);

	gotoxy(1,2);

}

//t_arvind_g1 dated 28 Feb 07 Added to set Pin Parameters for secure pin
/*--------------------------------------------------------------------------
    Function:		vSetPinParams
    Description:	To set Pin Parameters for secure pin
    Parameters:		void
    Returns:		
	Notes:          
--------------------------------------------------------------------------*/

void vSetPinParams(void) //SECURE_PIN_MODULE changes
{
	short	shRetVal				=	0;
	char	chFirstKeyTimeout[20]	=	{0};
	char	chInterKeyTimeout[20]	=	{0};
    char    chTotalTimeOutVal[20]   =   {0};
    srPINParams psParams;
    
    psParams.ucMin = 4;
    psParams.ucMax = 12;
    psParams.ucEchoChar = '*';
    psParams.ucDefChar = '-';
    psParams.ucDspLine = 2;  
    psParams.ucDspCol = 1;
    psParams.ucOption = 0x02; 


	// Tisha_K1 :  13/March/2012
	// Changed the first char and inter char timeout values as configurable items.
	shRetVal = get_env("FIRST_CHAR_PIN_TIMEOUT", (char *)chFirstKeyTimeout, sizeof(chFirstKeyTimeout));
	if(shRetVal != 0 ) 
		psParams.ulFirstKeyTimeOut = atoi(chFirstKeyTimeout);
	else
		psParams.ulFirstKeyTimeOut = 0;

	
	shRetVal = get_env("INTER_CHAR_PIN_TIMEOUT", (char *)chInterKeyTimeout, sizeof(chInterKeyTimeout));
	if(shRetVal != 0 ) 
		psParams.ulInterCharTimeOut = atoi(chInterKeyTimeout);
	else
		psParams.ulInterCharTimeOut = 0;

	shRetVal = get_env("TOTAL_PIN_TIMEOUT", (char *)chTotalTimeOutVal, sizeof(chTotalTimeOutVal));
	if(shRetVal != 0 ) 
	  psParams.ulWaitTime = atoi(chTotalTimeOutVal);
	else
	  psParams.ulWaitTime = 0;
  
	
  //  psParams.ulFirstKeyTimeOut = 7000; //this value should be in ms
  //  psParams.ulInterCharTimeOut = 2000; //this value should be in ms
  //   psParams.ulWaitTime =0;

  
    //SECPINTIMOUT
    if(ucAbrtOnPINTOut)
    	psParams.abortOnPINEntryTimeOut = 1;
    psParams.ucPINBypassKey = ENTER_KEY_PINBYPASS;

  if (Bypass_Flag) //SUB-PINBYPASS changes
		psParams.ucSubPINBypass = EMV_TRUE; 	  
  else
		psParams.ucSubPINBypass = EMV_FALSE;	  

  //Use this API to set the PIN parameters.
#ifdef __thumb
   usEMVSetPinParams(&psParams);
#endif

}

/*--------------------------------------------------------------------------
    Function:		ascii_to_binary
    Description:	Packs a string of ASCII hex digits into the 
                    destination buffer in binary form
    Parameters:		void
    Returns:		
	Notes:          
--------------------------------------------------------------------------*/

void ascii_to_binary(BYTE *dest, BYTE *src, int length)
{	
	/* length = length of source */
	short   i=0;
	BYTE    digit_val;

	if (length & 0x0001)
	{	// length is odd, so do some adjustment to put a zero at the start
        if ((src[0] - '0') > 9)
		{
            if ((src[0] - 'A') > 5)
                digit_val = (src[0] - 'a' + 10);
            else
                digit_val = (src[0] - 'A' + 10);
		}
		else
            digit_val = (src[0] - '0');

        dest[0] = 0;
        dest[0] |= (digit_val & 0x0F);
        dest++;
        src++;
        length--;
	}

    while(i < length)
	{
        if ((src[i] - '0') > 9)
		{
            if ((src[0] - 'A') > 5)
                digit_val = (src[i] - 'a' + 10);
            else
                digit_val = (src[i] - 'A' + 10);
		}
        else
            digit_val = (src[i] - '0');

        if (i & 0x0001)
            dest[i/2] |= (digit_val & 0x0F);
        else
		{
            dest[i/2] = 0;
            dest[i/2] |= ((digit_val << 4) & 0xF0);
		}

        i++;
	}

    return;
}

/*--------------------------------------------------------------------------
    Function:		inGetStringWithPrompt
    Description:	
    Parameters:		char *, char *, int
    Returns:		Returns number of characters input
	Notes:          
--------------------------------------------------------------------------*/

int inGetStringWithPrompt(char *prompt, char *string, int maxlen)
{
    char          key;
    unsigned char buf[15] = {0};
    short         i = 0;

    //A+AUTOFT: Check introduced to avoid user intervention	
    if (gAPPFlag != 1)
    {
    clrscr();
   	if(oLFlag ==1)
	{
        shDisplayMessage(MSG_ENTER_ONLINE_PIN, 1,1);  
	}
    else
	{
        shDisplayMessage(MSG_ENTER_PIN, 1,1);  
	}
    key = chGetKeyPress();
    while (key != 13)
    {		
        if (key == 27)
		{
            return (TRANS_CANCELLED);
		}

        if (key == 8)
        {
            if (i == 0)
            	{
                	error_tone();
					//BKSPKEY:
					return(E_USR_BACKSPACE_KEY_PRESSED);
            	}
            else
            {
                i--;
                buf[i] = 0;
                write_at(" ", strlen(" "), i+1, 2);  //value displayed in the 2nd line
            }
        }
        else if ((key >= 0x30) && (key <= 0x39))
        {
            if (i >= maxlen)
                error_tone();
            else
            {
                buf[i] = key;
                i++;
                write_at("*", 1, i, 2);   //value displayed in the 2nd line
			
            }
        }
        else
        {
            error_tone();
        }
        key = chGetKeyPress();
    }
	// Modified to copy only the specified length
    strncpy(string, (char*)buf,maxlen);
	}
	
	else
	{
		strncpy(string, "1234",4);
		printf("PIN : %s",string);
		return (4);			
	}

    return (i);
}

/*--------------------------------------------------------------------------
    Function:		inEnterAmountWithPrompt
    Description:	To get Amount 
    Parameters:		char *, char *, int
    Returns:		
	Notes:          
--------------------------------------------------------------------------*/

int inEnterAmountWithPrompt(char *prompt, char *amount, int maxlen)
{
    char    keyvalue  = 0;
    char    buf[15]   = {0};
    char    disp[22]  = {0};
    double  val;
    short   i         = 0;

    //A+AUTOFT: Check introduced to avoid user intervention		
   if (gAPPFlag != 1)
   {
    write_at("0.00", 4, 18, 2);

    keyvalue = chGetKeyPress();
	while (keyvalue != 13) 
    {		
        if (keyvalue == 27)
            return (TRANS_CANCELLED);
		
        if (keyvalue == 8)
        {
            if (i == 0)
                error_tone();
            else
            {
                i--;
                buf[i] = 0;     
#ifdef  __thumb
                val = atoi(buf); 
#endif

#ifdef  _TARG_68000
                val = atof(buf);
#endif
				//Brabhu_H1 dateed 14 Jun 2007
	            //SPLTAMT:Fix made to support 10 digit inputs
                sprintf(disp, "%9.2f", val / 100);
                //SVC_DSP_2_HEX(buf,disp,6);
                write_at(" ", 1, (21 - strlen(disp)), 2);        //value displayed in the 2nd line
                write_at(disp, strlen(disp), (21 - strlen(disp)) + 1, 2);  //value displayed in the 2nd line
            }
        }
        else if ((keyvalue >= 0x30) && (keyvalue <= 0x39))
        {
            if ((keyvalue == 0x30) && (i == 0))
                error_tone();
            else if (i >= maxlen)
                error_tone();
            else
            {
                buf[i] = keyvalue;
                i++;           
#ifdef  __thumb
                val = atoi(buf); 
#endif

#ifdef  _TARG_68000
                val = atof(buf);
#endif

                sprintf(disp, "%8.2f", val / 100);
                //SVC_DSP_2_HEX(buf,disp,6);
                write_at(disp, strlen(disp), (21 - strlen(disp)) + 1, 2);  //value displayed in the 2nd line
            }
        }
        else
        {
            error_tone();
        }
        keyvalue = chGetKeyPress();
    }
	// Modified to copy only the specified length.
    strncpy(amount, buf, maxlen);
    }	
    else
    {
        // Modified to copy only the specified length.
        strncpy(amount, "100", 2);
        //printf("Amount = %s\n", amount);
    }

    return (i);
}


/*--------------------------------------------------------------------------
    Function:		usAmtEntryFunc
    Description:	
    Parameters:		unsigned long *
    Returns:		
	Notes:          
--------------------------------------------------------------------------*/
// Return value is RFU
unsigned short usAmtEntryFunc(unsigned long *lnAmountValue)
{
	//Brabhu_H1 dateed 14 Jun 2007
	//SPLTAMT:Increased the buffer size from 11
    char           buf[12]           = {0};  
    char           tmp[13]           = {0};
	short          key 				 = 0;
    unsigned char  ucharBuf[6]       = {0};
    unsigned long  sale 			 = 0L;
	unsigned long  cashback 		 = 0;
	unsigned char  AccountType 		 = 0;
	unsigned long  ulParamVal  		 = 0L;
    char           *AccountTypeLabels_menu[] = {"1. Default", "2. Savings ", "3. Cheque or Debit ", "4. Credit "};
	unsigned char  ucTransType		= 0xFF;	  
    unsigned short usLen       	    = 0;      
	unsigned short usRetVal		    = 0;	  
	unsigned long  ulECTrmTxnLmt 	= 0L;	  
    unsigned char ucECSuppIndicator = 0;	  
   	char		    szAmount[13]    = {0};		
//   	char		    szChinaFlag[2]  = {0};

	//Purvin_p1 21/May/2007 added For Interac
	char               *AccountTypeLabels_menu_Interac[] = {"1. Savings ","2. Cheque "};

	//MENU Changes: Kishor_S1 27/Oct/2009	added menu for terminals other than Vx700
    char           *AccountTypeLabels_menu_general[] = {"Default", "Savings ", "Cheque or Debit ", "Credit "};

	//Ontime ID : 36805
	unsigned short      len 				 = 0;
	byte                transType = 0;
	
	ulParamVal = *lnAmountValue;
	// On transaction cancelled from amount entry, cancel the transaction.
	// Array size increased to accommodate 10 chars for amount entry.
    // CashBack Message is added and displayed from this function.

	//Ontime ID : 36805
	//Cashback amount should not be prompted, if the PDOL has cashback(9F03)
	//but the transaction type is other than SALE_WITH_CASHBACK
	usEMVGetTLVFromColxn(TAG_9C00_TRAN_TYPE, &transType, &len);


	// Tisha_K1 11/July/07
    // AMTCLBK: Check the ID passed and prompt for the respective inputs 
    // Prompt for amount
	if((ulParamVal == AMT_ACCT_TYPE_REQD)          || (ulParamVal == AMT_REQD) ||
	   (ulParamVal == AMT_CASHBACK_ACCT_TYPE_REQD) || (ulParamVal == AMT_CASHBACK_REQD))
    {
        //A+AUTOFT: Check introduced to avoid user intervention	
        if (gAPPFlag != 1)
    	{
      clrscr();
      shDisplayMessage(MSG_SALE_AMOUNT, 1,1);  

	  //Brabhu_H1 dateed 14 Jun 2007
	  //SPLTAMT:Fix made to get 10 digit input
      if(inEnterAmountWithPrompt("Sale Amt:", buf, 10) == TRANS_CANCELLED)
		return (TRANS_CANCELLED);

       sale = atol(buf);
        }
        else
           sale = atol("100");
	}

	// Prompt for cashback amount	
	if((ulParamVal == AMT_CASHBACK_ACCT_TYPE_REQD) || (ulParamVal == AMT_CASHBACK_REQD) ||
		    (ulParamVal == CASHBACK_ACCT_TYPE_REQD) || (ulParamVal == CASHBACK_REQD))
	{	
	  //Ontime ID : 36805
	  //Cashback amount should not be prompted, if the PDOL has cashback(9F03)
	  //but the transaction type is other than SALE_WITH_CASHBACK	
   	  if(SALE_WITH_CASHBACK == transType)
   	  {
   		  //A+AUTOFT: Check introduced to avoid user intervention	
   	      if (gAPPFlag != 1)
   	      {
   			  // Array size increased to accomadate 10 chars for amount entry.
   			  // CashBack Message is added and displayed from this function.
   			  clrscr();
   			  memset(buf, 0x00, sizeof(buf));
   			  shDisplayMessage(MSG_CASHBACK_AMOUNT, 1,1);
   			  if(inEnterAmountWithPrompt("Cashback:", buf, 9) == TRANS_CANCELLED)
   			 	return (TRANS_CANCELLED);
   			 
   		      cashback = atol(buf);
   	      }
   	      else
   	          cashback = atol("100");	
   	  }
	}
	
	//Pavan_K1 dated 19-Jul-07: Fix to update the 81 and 9f02 tags with the cash back amount.
	(*lnAmountValue) = glnAmount = (sale + cashback );
	
    //If amount and cashback both are requested, then update 0x81,0x9F02, 0x9F03 and 0x9F04 to the collection
	if((ulParamVal == AMT_CASHBACK_ACCT_TYPE_REQD) || (ulParamVal == AMT_CASHBACK_REQD))
	{
	    //Update amount - binary
        usEMVUpdateTLVInCollxn(TAG_81_AMOUNT_AUTH, (byte *) &glnAmount, 4);

		//Update amount -numeric
		memset(tmp,0x00,sizeof(tmp));
		sprintf(tmp, "%012lu", glnAmount);
		ascii_to_binary(ucharBuf, (byte *) tmp, 12);
		usEMVUpdateTLVInCollxn(TAG_9F02_AMT_AUTH_NUM, (byte *) ucharBuf, 6);		

	    //Update cashback amount - binary
		usEMVUpdateTLVInCollxn(TAG_9F04_AMT_OTHER_BIN, (byte *) &cashback, 4);
		
		//Brabhu_H1 dateed 14 Jun 2007
		//SPLTAMT:Fix made to have 4 byte represention, hence changed from %u to %lu
		//Purvin_p1 31/May/2007  Fix made for Clerify ID 070530-5.
		//change %ld to %u for unsigned long.
		memset(tmp,0x00,sizeof(tmp));
		memset(ucharBuf, 0x00, sizeof(ucharBuf));
		sprintf(tmp, "%012lu", cashback);
		ascii_to_binary(ucharBuf, (byte *) tmp, 12);
		usEMVUpdateTLVInCollxn(TAG_9F03_AMT_OTHER_NUM, (byte *) ucharBuf, 6);	
	}

	 //(*lnAmountValue) = glnAmount = (sale + cashback );
	
    // Prompt for account type
	if ((ulParamVal == AMT_ACCT_TYPE_REQD) || (ulParamVal == ACCT_TYPE_REQD) ||
		(ulParamVal == AMT_CASHBACK_ACCT_TYPE_REQD) || (ulParamVal == CASHBACK_ACCT_TYPE_REQD))
	{
        //A+AUTOFT: Check introduced to avoid user intervention	
        if (gAPPFlag != 1)
    	{
		clrscr();
	    write_at("    Account Type   ",strlen("    Account Type   "),1,3);
	    write_at("      Selection    ",strlen("      Selection    "),1,4);
	    SVC_wAIT((unsigned long)2000);  //VERIXPRTNG: Porting to Verix
		//Purvin_p1 date 10/May/2007
		//Display only  "Savings","Cheque" for Interac.
		if(gshInterac > 0)
		{
			key = inMenuFunc(AccountTypeLabels_menu_Interac, 2);
		}
		else
		{
			//MENU Changes: Kishor_S1 27/Oct/2009 added menu for terminals other than Vx700			
			//Kishor_S1 14/Dec/2009 added support for ucAllowKeys variable to display menu with keys supported
			if( (gshUnAttended == 1) || (ucAllowKeys == 1) )
				key = inMenuFunc(AccountTypeLabels_menu, 4);
			else
				key = inMenuFunc(AccountTypeLabels_menu_general, 4);							
		}
        }
        else		
            key = 1;
	    switch(key)
	    {
	       case 1:
	           AccountType = 0x00;  //Default Unspecified.
	           break;
	       case 2:
	           AccountType = 0x10;  //Savings 
	           break;
	       case 3:
	           AccountType = 0x20;  //Cheque/Debit
	           break;
	       case 4:
	           AccountType = 0x30;  //Credit
	           break;
		   case TRANS_CANCELLED:
			   return (TRANS_CANCELLED);
		}

	    usEMVUpdateTLVInCollxn (TAG_5F57_ACCOUNT_TYPE, (unsigned char *)&AccountType, 1);
	}
    //Brabhu_H1 dated 08 July 2008
    //ECTXNFLW: Perform an EC transaction when the transaction type is SALE.
    usRetVal = usEMVGetTLVFromColxn(TAG_9C00_TRAN_TYPE, (byte* )&ucTransType, &usLen);
	if(usRetVal != EMV_SUCCESS)
		return (EMV_SUCCESS);	
	
	if(ucTransType == SALE)
	{
	    //ECTXNFLW: Check for the presence of the EC txn limit env variable
		//usRetVal = get_env("ECTXNLIMIT", (char *)szAmount, sizeof(szAmount));		
        //if (usRetVal !=0)

		//Fix for Ontime 61475: Environment variable have to read once from config.sys file		
        if( strlen(gszECTXLimitAmount) )//NAND_FLASH  : Config.sys File access changes
        {//If amount environment variable is present use it
        	ulECTrmTxnLmt = atol(szAmount);
        	memset(tmp,0x00,sizeof(tmp));
        	ascii_to_binary((byte *)tmp,(byte *)szAmount, 12);
        }
        else
        { 
            //ECTXNFLW: Terminal floor limit is taken to consideration when the
            //EC txn limit is not set in the terminal
        	inVXEMVAPGetCardConfig(-1,-1 );
        	usRetVal = usEMVGetTLVFromColxn(TAG_9F1B_TERM_FLOOR_LIMIT, (byte* )&ulECTrmTxnLmt,&usLen);
			if(usRetVal != EMV_SUCCESS)
		        return (EMV_SUCCESS);	
        }

       	usRetVal = usEMVUpdateTLVInCollxn (TAG_9F7B_EC_TERM_TXN_LIMIT, (unsigned char *)tmp, 6);
		if(usRetVal != EMV_SUCCESS)
			return (EMV_SUCCESS);
        
        //ECTXNFLW: Set the EC support indicator flag to 1, when the txn amount
        //is less than the txn limit
        if (glnAmount < ulECTrmTxnLmt)
        	ucECSuppIndicator = 1;

 	}

	//ECTXNFLW: Check for the presence of the EC txn limit env variable

	//NAND_FLASH  : Config.sys File access changes - commented the code below
	
//	usRetVal = get_env("CHINAMOD", (char *)szChinaFlag, sizeof(szChinaFlag));

	if(szChinaFlag[0]=='1')
	{
        //ECTXNFLW: Update the EC support indicator as value part for the 9F7A tag
        //in the collection buffer
        usRetVal = usEMVUpdateTLVInCollxn (TAG_9F7A_EC_TERM_SUPPORT_IND, (unsigned char *)&ucECSuppIndicator, 1);
	    if(usRetVal != EMV_SUCCESS)
		    return (EMV_SUCCESS);	
	}

    return (EMV_SUCCESS);
}

/*--------------------------------------------------------------------------
    Function:		inOnlinePINFunc
    Description:	To Get Online PIN
    Parameters:		void
    Returns:		
	Notes:          
--------------------------------------------------------------------------*/
// Return value is RFU
//RETURN_TYPE: change return type from int to unsigned short.
unsigned short inOnlinePINFunc(void)
{
    unsigned short   len = 0;
    srTxnResult      TVRTSI;
    short            retVal = 0;
    char             key[17] = {0};
    char             binKey[8] = {0};
    byte             track2[50] = {0};
    char             track2Chars[101] = {0};

    usEMVGetTxnStatus(&TVRTSI);
	//BUFSIZE	strcpy()
	//Added buffer size check
	if(sizeof(key) >= strlen("1234567890123456"))
    	strcpy(key, "1234567890123456");
    ascii_to_binary((BYTE *)binKey, (BYTE *)key, 16);
	oLFlag=1; //flag is set to prompt the user for online pin entry
	
    //t_arvind_g1 dated 17 Jan 07 
    //added as per new requ. to bypass the offline pin 
    //flag is reset for online pin
   // 18/Jun/07  Soumya_G1 Commented the flag reset here.
   // No need to reset the flag here , as the behaviour of Bypass is 
   // applied to the Online pin as well.
   
    //shPin_Bypass = 0;

	
    OnlinePIN((unsigned char *) binKey, 0, ADD_NEW_KEY);
    usEMVGetTLVFromColxn(0x5700, track2, &len);
    memset(track2Chars, 0x00,sizeof(track2Chars));
    hex2asc((byte *) track2Chars, track2, len);
    retVal=OnlinePIN((unsigned char *) track2Chars, 0, BUILD_PINBLOCK); 	// Encrypted PIN block should be in track2

	//BKSPKEY:
    if(retVal == E_USR_BACKSPACE_KEY_PRESSED)
			return E_USR_BACKSPACE_KEY_PRESSED;
	
	if(retVal>0)
    {
		ascii_to_binary(track2, (BYTE *)track2Chars, 16);
		//TG99TOCLXN
		//Tag 99 (PIN Block) should be updated in the collection
		//usEMVAddTLVToCollxn(0x9900, track2, 8);
        usEMVUpdateTLVInCollxn(0x9900, track2, 8);
				
        TVRTSI.stTVR[2] |= 0x4;                 // Set bit in tvr for online PIN
        retVal = SUCCESS;
        usEMVSetTxnStatus(&TVRTSI);

    }
	//	Check for the PIN bypass/ cancel is done. 
    else if(retVal==0)
        retVal = E_USR_PIN_BYPASSED ;
    return retVal;
}

/*--------------------------------------------------------------------------
    Function:		vdPromptManager
    Description:	To display Appropriate messages 
    Parameters:		unsigned short
    Returns:		
	Notes:          
--------------------------------------------------------------------------*/

void vdPromptManager(unsigned short inCondition)
{
    char outp[11] = {0};
    srTxnResult TVRTSI;
    unsigned char ucDspMsg = 0;
	EMVBoolean bTAFlag = EMV_FALSE;		//Ontime ID: 61562
	
    switch (inCondition)
    {
        case TRANS_APPROVED:
            ucDspMsg  = MSG_TRANS_APPROVED ;
            break;
        case TRANS_DECLINED:
            ucDspMsg = MSG_TRANS_DECLINED ;
            break;
        case OFFLINE_APPROVED:
            ucDspMsg = MSG_OFFLINE_APPROVED ;
            break;
        case OFFLINE_DECLINED:
            ucDspMsg = MSG_OFFLINE_DECLINED ;
            break;
        case OFFLINE_NOTALLOWED:
            ucDspMsg = MSG_OFFLINE_NOT_ALLOWED ;
            break;
        case NONEMV:
            ucDspMsg = MSG_NON_EMV; 
            break;
        case FAILURE:
            ucDspMsg = MSG_FAILURE ;
            break;
        case EMV_CHIP_ERROR:
            ucDspMsg =  MSG_EMV_CHIP_ERROR;
            break;
        case CARD_REMOVED:
            ucDspMsg = MSG_CARD_REMOVED ;
            break;
        case CARD_BLOCKED:
            ucDspMsg = MSG_CARD_BLOCKED_BY_ISS ;
            break;
        case APPL_BLOCKED:
            ucDspMsg = MSG_APPLICATION_BLOCKED ;
            break;
        case CANDIDATELIST_EMPTY:
            ucDspMsg = MSG_CANDIDATELIST_EMPTY ;
            break;
        case CHIP_ERROR:
            ucDspMsg = MSG_EMV_CHIP_ERROR ;
            break;
        case BAD_DATA_FORMAT:
            ucDspMsg = MSG_BAD_DATA_FORMAT ;
            break;
        case APPL_NOT_AVAILABLE:
            ucDspMsg = MSG_APP_NOT_AVAIL ;
            break;

        case TRANS_CANCELLED:
            ucDspMsg = MSG_TRANS_CANCELLED ;
            break;
        case EASY_ENTRY_APPL:
            ucDspMsg = MSG_EASY_ENTRY_APPL ;
            break;
        case ICC_DATA_MISSING:
            ucDspMsg = MSG_ICC_DATA_MISSING ;
            break;
        case CONFIG_FILE_NOT_FOUND:
            ucDspMsg = MSG_CONFIG_FILE_NOT_FOUND ;
            break;
        case FAILED_TO_CONNECT:
            ucDspMsg = MSG_FAILED_TO_CONNECT;
            break;
        case SELECT_FAILED:
			bTAFlag = EMV_TRUE;		//Ontime ID: 61562
            ucDspMsg = MSG_SELECT_FAILED ;
            break;
        case USE_CHIP:
            ucDspMsg = MSG_USE_CHIP_READER ;
            break;
        case REMOVE_CARD:
            ucDspMsg = MSG_PLS_REMOVE_CARD ;
            break;
        case BAD_ICC_RESPONSE:
            ucDspMsg = MSG_BAD_ICC_RESPONSE ;
            break;
		case EMV_FAILURE:
	        ucDspMsg = MSG_EMV_FAILURE ;
            break;
		case USE_MAG_CARD:
            ucDspMsg = MSG_USE_MAG_STRIPE ;
            break;
		case E_TAG_NOTFOUND:
            ucDspMsg = MSG_TAG_NOT_FOUND;
            break;
			
		case INVALID_CAPK_FILE:
	/*	CAPK_DISP_BLOCK :Kishor_S1 commented the below code on 17-11-09 as per email confirmation								
            ucDspMsg = MSG_INVALID_CAPK_FILE ;
			break ;*/
		//Return without displaying any messages
			return;
	
        case INVALID_ATR:
            ucDspMsg = MSG_INVALID_ATR ;
			break ;
		case 0x6985: 
			ucDspMsg = MSG_OFFLINE_NOT_ALLOWED ;
			break;
		//INVMOD:	
		//Kishor_S1 on 14-10-09 commented the INVMOD changes as per the confimation mail		
/*		
		case E_INVALID_MODULE: 
			ucDspMsg = MSG_INVALID_MODULE ;
			break;*/
		//Ontime ID: 56193
		case TRY_AGAIN:
			bTAFlag = EMV_TRUE;		//Ontime ID: 61562
            ucDspMsg = MSG_TRY_AGAIN ;
			break ;
		case NOT_ACCEPTED://Ontime ID: 61519
            ucDspMsg = MSG_OFFLINE_NOT_ALLOWED ;
            break;	
		case 0x6800://Ontime ID: 61517, 61520
            ucDspMsg =  MSG_EMV_CHIP_ERROR;
            break;	

		// Tisha_K1 : 24/July/2012
		// Fix for the OnTime Issue: #66572
		case TERM_AID_CNT_EXEED: //99_AID_SUPPORT
			ucDspMsg = MSG_FAILED ;
			bTAFlag = EMV_TRUE;
			break;
		case TRANS_TIMED_OUT:
            		ucDspMsg = MSG_TRANS_TIMED_OUT ;
            	break;
		default:
            ucDspMsg = MSG_FAILED ;
            break;
    }

	//INVMOD:
	//Kishor_S1 on 14-10-09 commented the INVMOD changes as per the confimation mail		
/*	
	if(ucDspMsg == MSG_INVALID_MODULE)
	{
		clrscr();
		shDisplayMessage(ucDspMsg, 1,4);
		return;
	}*/

    if (pinSigRequired)
    {
        if (shDispModFlag)
            shDisplayMessage(MSG_SIGNATURE_REQUIRED, 1,3);
		else
            shDisplayMessage(MSG_SIGNATURE_REQUIRED, 1,5);
    }

	// Tisha_K1 : 24/July/2012
	// Fix for the OnTime Issue: #66572
	if(!bTAFlag)
	{
    	usEMVGetTxnStatus(&TVRTSI);
	
	    hex2asc((byte *) outp, TVRTSI.stTVR, 5);
    	outp[10] = '\0';
	}

    if (shDispModFlag)
    {
	    clrscr() ;
		//Ontime ID: 61562
		if(!bTAFlag)
		{
		    write_at("TVR ", 4, 1, 2);
	        write_at(outp, 10, 5, 2);
	        hex2asc((byte *) outp, TVRTSI.stTSI, 2);
	        outp[4] = '\0';
	        write_at("TSI ", 4, 1, 3);
	        write_at(outp, 4, 5, 3);
		}
	    shDisplayMessage(ucDspMsg, 1,4);
    }
    else
    {
	    clrscr() ;
		//Ontime ID: 61562
		if(!bTAFlag)
		{
	        write_at("TVR ", 4, 1, 6);
	        write_at(outp, 10, 5, 6);
	        hex2asc((byte *) outp, TVRTSI.stTSI, 2);
	        outp[4] = '\0';
	        write_at("TSI ", 4, 1, 7);
	        write_at(outp, 4, 5, 7);
		}
        shDisplayMessage(ucDspMsg, 1,8);    
    }
    SVC_wAIT((unsigned long)4000);  //VERIXPRTNG: Porting to Verix
    error_tone();
}

/*--------------------------------------------------------------------------
    Function:		inMenuFunc
    Description:	To prompt for Account type selection
    Parameters:		char **, int
    Returns:		
    Notes:          
--------------------------------------------------------------------------*/
int inMenuFunc(char **labels, int numLabels)
{
    short  i = 0;
    short  j = 0, k = 0;
    char   nextKey = 0;
    char   prevKey = 0;
    char   menuKeys[4] = {0};
    char   key;

    //A+AUTOFT: Check introduced to avoid user intervention		
    if (gAPPFlag == 1)
        return (1); //select the first AID

    //Purvin_P1: date 15-03-08 
	//Vx700: there is not function key for vx700 terminal, So all function key are mapped with number key.
	//F1=Numeric 1,F2=Numeric 2,F3=Numeric 3 and F4=Numeric 4		
	while (1)
	{
        clrscr();
        memset(menuKeys, 0x00, 4);
        nextKey = 0;
        prevKey = 0;

        j = 0;
        while (j < 4)
		{
            if ((i + j) >= numLabels)
                break;
            else
			{
                for (k=0; (labels[i + j][k] == 0x00);(labels[i + j][k] = 0x20),k++);
                //menuKeys[j] = 1;
				// for ONTIME id 22518 T_gangadhar_s1  24/April/08  support to work all function keys for all Vx terminals
				if(ucAllowKeys == 1)
                menuKeys[j] = 49;
				else
					menuKeys[j] = 1;
                if (shDispModFlag)
                    write_at(labels[i + j], strlen(labels[i + j]), 1, ((j * 1) + 1));
                else
                    write_at(labels[i + j], strlen(labels[i + j]), 1, ((j * 2) + 1));
			}
            j++;
		}

        // No. of labels on screen is now j
        if ((i + j) < numLabels)
           nextKey = 1;

        if (i)
            prevKey = 1;

        if (nextKey)         
		{
		   //Support For multiple Application.
	       if( ucAllowKeys  == 1 )
	           shDisplayMessage(MSG_NEXT, 2,8);
           else if (shDispModFlag)
               shDisplayMessage(MSG_NEXT, 18,4);
		   else
               shDisplayMessage(MSG_NEXT, 18,8);
		}

        if (prevKey)
		{
		    if (shDispModFlag)
                shDisplayMessage(MSG_PREV, 13,4);
            else
                shDisplayMessage(MSG_PREV, 13,8);
		}

        key = chGetKeyPress();

       //Purvin_P1 date 11-June-08
       //Support For multiple Application.
	   if( ucAllowKeys  == 1 )
	   {
//Kishor_S1 added on 5-1-10 for "#" And "*" keys implementation	   
	        if((key == 109) || (key == 35)) //UP
				key=99;
			if((key == 108) || (key == 42)) //DOWN
			    key = 100;
	   }

        if (key == 100)
		{	// Next
            if (nextKey)
                i += 4;
            else
                error_tone();
		}
        else
            if (key == 99)
			{	// Previous
                if (prevKey)
                    i -= 4;
                else
                    error_tone();
			}
            else
				// for ONTIME id 22518 T_gangadhar_s1  24/April/08  support to work all function keys for all Vx terminals
				if( ucAllowKeys  == 1 )
					{
                if (key >= 49 && key <= 52) 
				{	// Menu selection
                    if (menuKeys[key - 49])
                        return(i + (key - 49) + 1);
                    else
                        error_tone();
				}
				else
					if (key == 27)
						return (TRANS_CANCELLED);
					else
					    error_tone();
					}
				else if( ucAllowKeys == 0)
					{
						if (key >= 1 && key <= 4) 
						{	// Menu selection
                    		if (menuKeys[key - 1])
                        		return(i + (key - 1) + 1);
							else
                        		error_tone();
							
						}
						else
						if (key == 27)
							return (TRANS_CANCELLED);
						else
					  	  error_tone();
					}
	}
}

/*--------------------------------------------------------------------------
    Function:		chConvertKey
    Description:	
    Parameters:		char
    Returns:		key
	Notes:          
--------------------------------------------------------------------------*/

char chConvertKey(char key)
{



    if ((unsigned char) key > 0xf9) 
        key -= 0xf9;                    // Screen keys fa-fd -> 1-4
    else if ((unsigned char) key < 0xef) 
        key &= 0x7f;	                // Keypad keys 0-ef -> 0-7f

    return key;
}

/*--------------------------------------------------------------------------
    Function:		chGetKeyPress
    Description:	
    Parameters:		void
    Returns:		key
	Notes:          
--------------------------------------------------------------------------*/


char chGetKeyPress(void)
{
    char key;
    
    vdFlushKeyboard();

    while (1)
    {
        if (wait_event() & EVT_KBD)
        {
            read(con, &key, 1);
            break;
        }
    }
    key = chConvertKey(key);

    return (key);
}


/*--------------------------------------------------------------------------
    Function:		vdFlushKeyboard
    Description:	
    Parameters:		void
    Returns:		
	Notes:          
--------------------------------------------------------------------------*/
void vdFlushKeyboard(void)
{
    char key[20] = {0};
    
    read(con, key, 20);
}

/*--------------------------------------------------------------------------
    Function:		inGetOption
    Description:	Returns either f1 or f2 key
    Parameters:		void
    Returns:		key
	Notes:          
--------------------------------------------------------------------------*/
char inGetOption(void)
{
    char key;

    //A+AUTOFT: Check introduced to avoid user intervention	
    if (gAPPFlag != 1)
    {
    key = chGetKeyPress();
	//Purvin_P1: date 15-03-08 
	//Vx700: there is not function key for vx700 terminal, So all function key are mapped with number key.
	//F1=Numeric 1,F2=Numeric 2,F3=Numeric 3 and F4=Numeric 4
	// for ONTIME id 22518 T_gangadhar_s1  24/April/08  support to work all function keys for all Vx terminals
	if(ucAllowKeys == 1)
		{
    while ((key != 49) && (key != 50))   	
        key = chGetKeyPress();
    }
    else
		{
			while ((key != 1) && (key != 2))   	
        	key = chGetKeyPress();
		}
    }
    else if(ucAllowKeys == 1)
        key = 50;
	else
		key = 2;

    return (key);
}


/*--------------------------------------------------------------------------
    Function:		inGetKeyOption
    Description:	
    Parameters:		void
    Returns:		key
	Notes:          
--------------------------------------------------------------------------*/
char inGetKeyOption(void)
{
    char key;

    key = chGetKeyPress();
	//Purvin_P1: date 15-03-08 
	//Vx700: there is not function key for vx700 terminal, So all function key are mapped with number key.
	//F1=Numeric 1,F2=Numeric 2,F3=Numeric 3 and F4=Numeric 4
	// for ONTIME id 22518 T_gangadhar_s1  24/April/08  support to work all function keys for all Vx terminals
	if(ucAllowKeys == 1)
		{
    while ((key != 49) && (key != 50) && (key !=27))
        key = chGetKeyPress();
		}
	else
		{
			while ((key != 1) && (key != 2) && (key !=27))
        	key = chGetKeyPress();
		}

    return (key);
}


/*--------------------------------------------------------------------------
    Function:		inOverrideChipScreen
    Description:	
    Parameters:		void
    Returns:		SUCCESS, FAILURE
	Notes:          
--------------------------------------------------------------------------*/
int inOverrideChipScreen(void)
{
    char key;

    //A+AUTOFT: Check introduced to avoid user intervention	
    if (gAPPFlag != 1)
    {
    clrscr();
    write_at("Override chip", 13, 1, 1);
    write_at("Yes", 3, 19, 1);
    write_at("requirement?", 12, 1, 2);
    write_at("No", 2, 20, 3);
    key = inGetOption();
    }	
    else
        key = 49;
	
	// for ONTIME id 22518 T_gangadhar_s1  24/April/08  support to work all function keys for all Vx terminals
	if(ucAllowKeys == 1)
		{
    if (key == 49)
        return SUCCESS;
    else
        return FAILURE;
		}
	else
		{
    		if (key == 1)
      		  return SUCCESS;
    		else
       		 return FAILURE;
		}
}

/*--------------------------------------------------------------------------
    Function:		inGetTermDecisnOnForceDecline
    Description:	To prompt the user to decline the transaction or not
    Parameters:		void
    Returns:		
	Notes:          
--------------------------------------------------------------------------*/
//FORCE_DECLINE
//t_arvind_g1 dated 08 Jan 07 Added as per new requi. to decline the transaction before GEN AC
int inGetTermDecisnOnForceDecline(void)
{
    char key;

    //A+AUTOFT: Check introduced to avoid user intervention	
    if (gAPPFlag != 1)
    {
    clrscr();
	//Pavan_K1 dated 6-Feb-07 added the macro for the force decline case.
    //write_at("Force Decline...?",17, 1,1);
	shDisplayMessage(MSG_FORCE_DECLINE, 1, 1);
    shDisplayMessage(MSG_YES, 19,1);
    shDisplayMessage(MSG_NO, 19,3);

    key = inGetKeyOption();
    }// for ONTIME id 22518 T_gangadhar_s1  24/April/08	support to work all function keys for all Vx terminals
    else if(ucAllowKeys == 1)
        key = 50;
	else
		key = 2;
	// for ONTIME id 22518 T_gangadhar_s1  24/April/08  support to work all function keys for all Vx terminals
	if(ucAllowKeys == 1)
	{
	if(key ==27)
		return (TRANS_CANCELLED);
    else if (key == 49)
        return FORCED_DECLINE;   // Forced Decline
    else
        return 0;               // No decision
	}
	else
	{
		if(key ==27)
			return (TRANS_CANCELLED);
    	else if (key == 1)
        	return FORCED_DECLINE;   // Forced Decline
    	else
        	return 0;               // No decision
	}
}

/*--------------------------------------------------------------------------
    Function:		inGetTerminalDecision
    Description:	Force Online or not
    Parameters:		void
    Returns:		key
	Notes:          
--------------------------------------------------------------------------*/

int inGetTerminalDecision(void)
{
    char key;
    //A+AUTOFT: Check introduced to avoid user intervention	
    if (gAPPFlag != 1)
    {
	          clrscr();
               shDisplayMessage(MSG_FORCE_ONLINE, 1,1);
               shDisplayMessage(MSG_YES, 19,1);
               shDisplayMessage(MSG_NO, 19,3);

                key = inGetKeyOption();
	}
    else if(ucAllowKeys == 1)
        key = 50;
	else
		key = 2;

	
	// for ONTIME id 22518 T_gangadhar_s1  24/April/08  support to work all function keys for all Vx terminals
	if(ucAllowKeys == 1)
	{
	if(key ==27)
		return (TRANS_CANCELLED);
    else if (key == 49)
        return FORCED_ONLINE;   // Forced online
    else
        return 0;               // No decision
	}
	else
	{
	if(key ==27)
		return (TRANS_CANCELLED);
    else if (key == 1)
        return FORCED_ONLINE;   // Forced online
    else
        return 0;               // No decision
	}
}

/*--------------------------------------------------------------------------
    Function:		inManualApproval
    Description:	
    Parameters:		void
    Returns:		
	Notes:          
--------------------------------------------------------------------------*/

int inManualApproval(void)
{
    char key;
    //A+AUTOFT: Check introduced to avoid user intervention	
    if (gAPPFlag != 1)
    {
    clrscr();
    shDisplayMessage(MSG_APPROVE_TRANS, 1,1);
    shDisplayMessage(MSG_YES, 19,1);
    shDisplayMessage(MSG_NO, 19,3);

    key = inGetOption();
    }
    else
        key = 1;
	
	// for ONTIME id 22518 T_gangadhar_s1  24/April/08  support to work all function keys for all Vx terminals
	if(ucAllowKeys == 1)
	{
    if (key == 49)
        return 1;               // Approve
    else
        return 0;               // Decline
	}
	else
	{
	if (key == 1)
        return 1;               // Approve
    else
        return 0;               // Decline
    }
}

/*--------------------------------------------------------------------------
    Function:		inSetTransactionType
    Description:	
    Parameters:		void
    Returns:		
	Notes:          
--------------------------------------------------------------------------*/

byte inSetTransactionType(void)
{
    short key = 0;
	//MENU Changes: Kishor_S1 27/Oct/2009	changed menu without number for terminals other than Vx700		
    char *transTypeLabels[] = {"1.Sale", "2.Cash", "3.Sale with cashback"};
	char *transTypeLabels1[] = {"1.Sale","2.Sale with cashback"};
	char *transTypeLabels2[] = {"1.Cash", "2.Sale with cashback"};
	//Kishor_S1 14/Dec/2009 added support or ucAllowKeys variable to display menu with keys supported
    //char *transTypeLabels_with_keys[] = {"1.Sale ", "2.Cash advance", "3.Adjustment", "4.Sale with cashback"};	
	unsigned short  retVal            = 0;
	byte type;
	byte             TermAddCaps[5]         = {0};    // Terminal Additional Capabilities (tag 9F40)
	unsigned short len;
	
	//Purvin_p1 date 18/May/2007
	//Checking for Terminal Additional Capabality if Cashback bit is not set in byte1 then it will not display "Sale with Cashback" option.
	// Terminal Additional Capabilities   
	retVal = usEMVGetTLVFromColxn(TAG_9F40_TERM_ADTNAL_CAP, (byte *)TermAddCaps, &len);

	 if (retVal != EMV_SUCCESS) 
		return(retVal);
	
	//TxnTpeMenu: Transaction type should be displayed acc. to the additional terminal capabilities 
 	//T_Gangadhar_s1
	//UNATTENDED_TERMINAL:Commented for Not to support Print Receipt, It is not supported for Unattended terminal
	//if(gshUnAttended == 1)
	//{
    //A+AUTOFT: Check introduced to avoid user intervention		
	 			//Checking For Sale with Cashback bit in terminal add cap
				//if(TermAddCaps[0] == 0xE0)
				//key = inMenuFunc(transTypeLabels, 1);
				//else
				//Purvin_P1 :date 19/Jun/08
				//Hard coaded sale key for Vx700
				//key = inMenuFunc(transTypeLabels, 1);
	/*if (TermAddCaps[0] == 0x60) 
	{ 
				key=1;
	}
	else 
	{
    if (gAPPFlag != 1)
    {	
	 //Checking For Sale with Cashback bit in terminal add cap
	if(TermAddCaps[0] == 0xE0)
		key = inMenuFunc(transTypeLabels, 3);
	else
		{
		//Kishor_S1 14/Dec/2009 added support for ucAllowKeys variable to display menu with keys supporte		
		  if(ucAllowKeys == 1)
			  key = inMenuFunc(transTypeLabels_with_keys, 4);
		  else
			key = inMenuFunc(transTypeLabels, 4);
		}
    }
    else
        key = 1;
	}*/

	//TxnTpeMenu: Transaction type should be displayed acc. to the additional terminal capabilities
	if (gAPPFlag != 1)
	{
		switch(TermAddCaps[0])
		{
			case 0xF0:
				key = inMenuFunc(transTypeLabels, 3);
				break;
			case 0xE0:
				key = inMenuFunc(transTypeLabels, 2);
				break;
			case 0x70:
				key = inMenuFunc(transTypeLabels1, 2);
				if(key == 2)
					key = 3;
				break;
			case 0x90:
				key = inMenuFunc(transTypeLabels2, 2);
				if(key == 1)
					key = 2;
				else if(key == 2)
					key = 3;
				break;
			case 0x60:
				key = 1;
				break;
			case 0x80:	
				key = 2;
				break;
			case 0x10:
				key = 3;
				break;
			default:
				key = 1;
				break;

		}
	}
	else
		key = 1;
		

    switch(key)
    {
        case 1:
            type = SALE;
            break;
        case 2:
            type = CASH_ADVANCE;
            break;
        case 3:
            type = SALE_WITH_CASHBACK;
            break;
        default:
            type = TRANS_CANCELLED;
            break;
    }

    return (type);
}

/*--------------------------------------------------------------------------
    Function:		getUsrPin
    Description:	To get PIN from the user
    Parameters:		unsigned char *
    Returns:		E_USR_PIN_BYPASSED, E_PP_ABSENT, E_PERFORM_SECURE_PIN, Pin length
	Notes:          
--------------------------------------------------------------------------*/
//Purvin_p1 date 12/July/07 
//RETURN_TYPE: Change return type from short to UShort
unsigned short getUsrPin(unsigned char *pin)
{
    //Purvin_P1 date 18 Jul 07 Ontime Id :13809
	//Change data type from short to Ushort
	Ushort       len = E_PP_ABSENT; //change from (short)E_PP_ABSENT to E_PP_ABSENT
	srTxnResult TVRTSI;
	//Kishor_S1 added on 14-7-11		
	//SECURE_PIN_MODULE : Return E_PERFORM_SECURE_PIN directly from the application api.
	//Ontime ID: 61398	Vx EMV 6.0.0 Enter OL PIN is not displayed for SECURE PIN.
	
#ifdef _SECURE_PIN	
	if(!oLFlag)
		return(E_PERFORM_SECURE_PIN);
#endif

	
	usEMVGetTxnStatus(&TVRTSI);
	
	
	//t_arvind_g1 dated 17 Jan 07 
	//added as per new requ. to bypass the offline pin 
	//if any one offline pin is bypassed
	//Bypass_Flag will get set when env variable BYPASS_OFFLINE_PIN is set to 1

	//Soumya_G1 18/Jun/07
    //Changed the Env Variable to BYPASS_PIN from BYPASS_OFFLINE_PIN
    //This is for the changed requirement on the Bypass feature.
    //Now if Any One pin is bypassed, then any consecutive pin should be bypassed.
      if(Bypass_Flag ==1) 
	{
        if(shPin_Bypass)
		    return E_USR_PIN_BYPASSED;		
	}    
	
    if (ppPresent)
    {
        clrscr();
        shDisplayMessage(MSG_WAIT_FOR_CUSTOMER, 1,1);
        if(inEMVPPPresent() == ROK)
			len = (Ushort) getPPUsrPin(pin, CKclear, CKencr, &masterKeyIndex); //Purvin_P1 date 18 Jul 07 Ontime Id :13809, change from (short) getPPUsrPin to (Ushort) getPPUsrPin.

        else
            len = E_PP_ABSENT;   //Purvin_P1 date 18 Jul 07 Ontime Id :13809, change from (short)E_PP_ABSENT to E_PP_ABSENT 
    }

    else
	   //RETVALUSH:
 	   len =  getTermUsrPin(pin);//Purvin_P1 date 18 Jul 07 Ontime Id :13809, change from (short)getTermUsrPin to (Ushort)getTermUsrPin.

	return(len);
	

	//return(E_PERFORM_SECURE_PIN);
}

/*--------------------------------------------------------------------------
    Function:		getTermUsrPin
    Description:	To get PIN from the user
    Parameters:		unsigned char *
    Returns:		TRANS_CANCELLED, E_USR_ABORT, E_USR_PIN_BYPASSED, E_USR_PIN_CANCELLED
	Notes:          
--------------------------------------------------------------------------*/
//RETVALUSH:
//Changed the return value from short to unsigned short	
unsigned short getTermUsrPin(unsigned char *pin)
{
    unsigned char    str[13] ={0} ; 
    unsigned short   size = 0;	//RETVALUSH:
    
	//Purvin_p1 date 10/05/2007
	//For Interac PinBypass is not allowed.
	//T_Gangadhar_S1 UNATTENDED_TERMINAL: PIN bypass is not supported for unattended terminals

	if(!(gshInterac > 0) && (!(gshUnAttended > 0))) //Check for if not Interac then allow PIN BYPASS
	{
		do
		{	
			size = inGetStringWithPrompt("Enter PIN: ", (char*)str, 12);
		} while ((size < 4) && (size != 0));

	}
	else
	{
		//PinBypass is not allowed in INTERAC
		//It prompts Enter PIN untill user will not enter pin.
		do
		{	
			size = inGetStringWithPrompt("Enter PIN: ", (char*)str, 12);
		} while ((size < 4));
	}
	//Soumya_G1 18/Jun/07
    //Changed the Env Variable to BYPASS_PIN from BYPASS_OFFLINE_PIN
    //This is for the changed requirement on the Bypass feature.
    //Now if Any One pin is bypassed, then any consecutive pin should be bypassed.
    //Here no need to do the special processing for the ONLINE Pin.
	if(oLFlag == 1)
	{
		oLFlag=0;//flag reset for online pin entry
	}
    /*    if( size == 0)
            return (E_USR_PIN_BYPASSED); 	
	}*/

	//Soumya_G1 27/Feb/07 TC# V2CL0320001 
	//This a new implementation w.r.t Pin Cancellation.
	//When ever the pin is cancelled irrespective of the type of the CVM,
	//the appln will return E_USR_ABORT so that library returns EMV_FAILURE and the txn is terminated.
	//RETVALUSH:
   	if( size == TRANS_CANCELLED)
        return (E_USR_ABORT);  
	//BKSPKEY:
	if(size == E_USR_BACKSPACE_KEY_PRESSED)
		return size;
	
	if( size == 0)
	{
	    //t_arvind_g1 dated 17 Jan 07 
	    //added as per new requ. to bypass the offline pin 
	    //flag is set when offline pin is bypassed.
		if(Bypass_Flag)
		{
		    shPin_Bypass = 1;
	    	return (E_USR_PIN_BYPASSED); //Pavan_K1 dated 12-Apr-07.
	    	                             //Return pin bypass only when the bypass flag is set
		}
		else
			return (size);
	}

	//BUFSIZE  CodeReview:Check for pointer validity
	//Buffer size check is not reqd as pin is a pointer
	if(pin != NULL)
		memcpy(pin, str, size);

    return (size);
}

/*--------------------------------------------------------------------------
    Function:		inGetPinPadInit
    Description:	To prompt the user whether to initialise external PinPad
    Parameters:		void
    Returns:		
	Notes:          
--------------------------------------------------------------------------*/

int inGetPinPadInit(void)
{
    char key;

	//A+AUTOFT: Check introduced to avoid user intervention 
	if (gAPPFlag != 1)
	{
        clrscr();
        shDisplayMessage(MSG_INITIALIZE_PINPAD, 1,1);
        shDisplayMessage(MSG_YES, 19,1);
        shDisplayMessage(MSG_NO, 19,3);

        key = inGetOption();

	}
	else
	   key = 0;

	// for ONTIME id 22518 T_gangadhar_s1  24/April/08  support to work all function keys for all Vx terminals
	if(ucAllowKeys == 1)
	{
    if (key == 49)
        return 1;
    else
        return 0;
	}
	else
	{
	if (key == 1)
        return 1;
    else
        return 0;
	}
}

/*--------------------------------------------------------------------------
    Function:		inGetPrintReceipt
    Description:	To prompt the user whether to print the print reciept
    Parameters:		void
    Returns:		
	Notes:          
--------------------------------------------------------------------------*/
int inGetPrintReceipt(void)
{
    char key;

    //A+AUTOFT: Check introduced to avoid user intervention 
	if (gAPPFlag != 1)
	{
    clrscr();
    shDisplayMessage(MSG_PRINT_RECEIPT, 1,1);
    shDisplayMessage(MSG_YES, 19,1);
    shDisplayMessage(MSG_NO, 19,3);

    key = inGetOption();
	}
    else
        key = 1;
	// for ONTIME id 22518 T_gangadhar_s1  24/April/08  support to work all function keys for all Vx terminals
	if(ucAllowKeys == 1)
	{
    if (key == 49)
        return 1;               
    else
        return 0;               
	}
	else
	{
	if (key == 1)
        return 1;
    else
        return 0;
	}             
}

/*--------------------------------------------------------------------------
    Function:		vdPrintReceipt
    Description:	Print reciept
    Parameters:		short
    Returns:		
	Notes:          
--------------------------------------------------------------------------*/
void vdPrintReceipt(short inCondition)
{
    short           prn = 0,i = 0;
    struct          Opn_Blk prnBlock;
    char            out[128], panStr[21];
	char            tempBufferStr[80], yyyy[5]={0}, mm[5]={0}, dd[5]={0}, date[15]={0};	
	char            tvrStr[11] = {0}, tsiStr[5] = {0}, aidStr[33] = {0};
    char            arcStr[11] = {0}, transTypeStr[19] = {0};
	char            szBuffer[150]={0};    
	char            scriptResStr[11] = {0};
	unsigned char   ucDspMsg = 0;
	short           shRetVal = 0; 
	unsigned short  len = 0;
	byte            pan[10];        
    byte            tvr[5] = {0}, tsi[2] = {0};    
    byte            aid[16] = {0};    
	byte            arc[2] = {0};	
    byte            transType = 0;            
	byte            tempBuffer[40] = {0};	
	byte            stTac[5] = {0};
	Ushort          tacLen=0;	
	byte            buffer[5] = {0};
    //byte            CardHoldName[30] = {0};
	byte            szApplicationLabelName[33] = {0};
    //byte            ucPinBlk[254]={0};
	//char			outBuf[280]={0};    
	char tmpstr[32];

	static unsigned char   ucETECCardFlag = 0xFF; //NAND_FLASH : Use the static variable populated
	
//VERIXPRTNG: Porting to Verix
#ifdef __thumb
    prnBlock.rate = Rt_9600;
#else
	prnBlock.rate = Rt_19200;
#endif
    prnBlock.format = Fmt_A8N1;
    prnBlock.protocol = P_char_mode;
    prn = open(DEV_COM4, 0);
    set_opn_blk(prn, &prnBlock);

    // Transaction type
    usEMVGetTLVFromColxn(0x9c00, &transType, &len);
    switch (transType)
    {
        case SALE:
			//BUFSIZE	strcpy()
			//Added buffer size check
			if(sizeof(transTypeStr) >= strlen("Sale"))
            	strcpy(transTypeStr, "Sale");
            break;
        case CASH_ADVANCE:
			//BUFSIZE	strcpy()
			//Added buffer size check
			if(sizeof(transTypeStr) >= strlen("Cash advance"))
            	strcpy(transTypeStr, "Cash advance");
            break;
	    case SALE_WITH_CASHBACK:
			//BUFSIZE	strcpy()
			//Added buffer size check
			if(sizeof(transTypeStr) >= strlen("Sale with cashback"))
            	strcpy(transTypeStr, "Sale with cashback");
            break;
    }
    memset(out, 0x00,sizeof(out));
    sprintf(out, "%s\n\n", transTypeStr);
    write(prn, out, strlen(out));
    SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix
	
	read_clock(date);
	//BUFSIZE
	//Added buffer size check
	if(sizeof(yyyy) >= 4)
    	memcpy(yyyy, &date[0], 4);
	if(sizeof(mm) >= 2)
    	memcpy(mm, &date[4], 2);
	if(sizeof(dd) >= 2)
    	memcpy(dd, &date[6], 2);
    memset(out, 0x00,sizeof(out));
    sprintf(szBuffer, "%s-%s-%s\r\n", yyyy, mm, dd);
    sprintf(out, "Date: %s\n", szBuffer);
    write(prn, out, strlen(out));
    SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix

    // AID
    usEMVGetTLVFromColxn(0x4f00, aid, &len);
    hex2asc((byte *) aidStr, aid, len);
    memset(out, 0x00,sizeof(out));
    sprintf(out, "AID: %s\n", aidStr);
    write(prn, out, strlen(out));
    SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix

	//Application Label
	//Change by Purvin_p1: date 19-Feb-07
	memset(szApplicationLabelName,0x00,sizeof(szApplicationLabelName));
	if(usEMVGetTLVFromColxn(TAG_50_APPL_LABEL, szApplicationLabelName, &len)== EMV_SUCCESS)
	{
	    	memset(out,0x00,sizeof(out));
	    	sprintf(out, "App Label: %s\n", szApplicationLabelName);
	    	write(prn, out, strlen(out));
	    	SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix
	}

	//Application Preferred Name 
	//Change by Purvin_p1: date 19-Feb-07
	memset(szApplicationLabelName,0x00,sizeof(szApplicationLabelName));
	if(usEMVGetTLVFromColxn(TAG_9F12_APPL_PRE_NAME, szApplicationLabelName, &len)== EMV_SUCCESS)
	{
	        memset(out,0x00,sizeof(out));
	        sprintf(out, "App Prer Name: %s\n", szApplicationLabelName);
	        write(prn, out, strlen(out));
	        SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix
	}
     //  CardHolder Name is added in the printing parameters.
    // Cardholder name
	//Ganesh_P1 30-Jun-10 PCI_CHANGE : Getting CardHolder Name from global buffer
	
	//usEMVGetTLVFromColxn(0x5f20, CardHoldName, &len);
    memset(out,0x00,sizeof(out));
    sprintf(out, "CHName: %s\n", ucCardholdername);
    write(prn, out, strlen(out));
    SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix





//Ganesh_P1 30-Jun-10 PCI_CHANGE : Padding PAN data
	if(panlen!=0)
	{
	hex2asc((byte *) panStr, apppan, panlen);
		
		len=strlen(panStr)-1;
		while(panStr[len] == 'F')	   // Get rid of any filler before displaying.
		{
			len--;
			panStr[strlen(panStr) - 1] = '\0';
		}

		memset(tmpstr,0x00,sizeof(tmpstr));	
		//memcpy(&tmpstr[0],&panStr[0],6);
		if(len >=4 )
		{
			for(i=0; i < (len - 3); i++ )
			{
				tmpstr[i]='*';
			}
			
			memcpy(&tmpstr[i],&panStr[len-3],4);
			tmpstr[++len]='\0';
		}
		else
			memcpy(tmpstr,&panStr[0],strlen(panStr));

	}
		
		memset(out, 0x00,sizeof(out));
		sprintf(out, "PAN: %s\n", tmpstr);
		write(prn, out, strlen(out));
		SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix
		
				 
   	//Purvin_p1 date 20-02-2007
	//Moved below statement outside of IF.

	memset(pan, 0x00,sizeof(pan));

	if(usEMVGetTLVFromColxn(TAG_5F34_APPL_PAN_SEQNUM, pan, &len)== EMV_SUCCESS)
	{
        memset(panStr, 0x00,sizeof(panStr));
        hex2asc((byte *) panStr, pan, len);
        memset(out, 0x00,sizeof(out));
        sprintf(out, "PAN Seq: %s\n", panStr);
        write(prn, out, strlen(out));
        SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix
	}

	// This is done as part of increasing the no. of digits for amount entry. ravi_B3. ATS Bug NO.11
	// Now the amount is stored as a global variable and printed directly. Format is changed to 8.2
    memset(out, 0x00,sizeof(out));
    sprintf(out, "Amount: %8.2f\n\n", (double) glnAmount / 100);
    write(prn, out, strlen(out));
    SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix

	if(usEMVGetTLVFromColxn(TAG_8A_AUTH_RESP_CODE, arc, &len)== EMV_SUCCESS)
	{
    memset(arcStr,0x00,sizeof(arcStr));
	//BUFSIZE
	//Added buffer size check
	if(sizeof(arcStr) >= len)
    	memcpy(arcStr,arc,len);
    memset(out, 0x00,sizeof(out));
    sprintf(out, "ARC: %s\n\n", arcStr);
    write(prn, out, strlen(out));
    SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix
	}
	
    memset(tempBuffer, 0x00, sizeof(tempBuffer));
    memset(tempBufferStr, 0x00, sizeof(tempBufferStr));
	if(usEMVGetTLVFromColxn(0x9f27, tempBuffer, &len)== EMV_SUCCESS)
	{
	    	hex2asc((byte *) tempBufferStr, tempBuffer, len);
   	memset(out, 0x00,sizeof(out));
    sprintf(out, "CID: %s\n", tempBufferStr);
    write(prn, out, strlen(out));
    SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix
	}
    // Tag 9f10 - Issuer Application Data
    memset(tempBuffer, 0x00, sizeof(tempBuffer));
    memset(tempBufferStr, 0x00, sizeof(tempBufferStr));

    if (usEMVGetTLVFromColxn(0x9f10, tempBuffer, &len) == EMV_SUCCESS)
    {
        hex2asc((byte *) tempBufferStr, tempBuffer, len);

   	    memset(out, 0x00,sizeof(out));
        sprintf(out, "IAD: %s\n", tempBufferStr);
        write(prn, out, strlen(out));
        SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix
     }


	//NAND_FLASH : Use the static variable populated
	//Fix for Ontime 61475: Environment variable have to read once from config.sys file
	if ( ucETECCardFlag == 0xFF )
		{
			shRetVal = get_env("ETEC_Card", (char *)buffer, sizeof(buffer));
			if(shRetVal!=0 && buffer[0]>='1')
				ucETECCardFlag = 1;
			else
				ucETECCardFlag = 0;				
		}

/*    memset(buffer, 0x00, sizeof(buffer));
    shRetVal = get_env("ETEC_Card", (char *)buffer, sizeof(buffer));
    if(shRetVal!=0 && buffer[0]>='1')		*/
    if ( ucETECCardFlag == 1 )
	{
        memset(tempBuffer, 0x00, sizeof(tempBuffer));
        memset(tempBufferStr, 0x00, sizeof(tempBufferStr));
		//IAC
        if(usEMVGetTLVFromColxn(0x9f0e, tempBuffer, &len) == EMV_SUCCESS)
		{
            hex2asc((byte *) tempBufferStr, tempBuffer, len);
            memset(out, 0x00,sizeof(out));
            sprintf(out, "IAC:\n\t Denial : %s\n", tempBufferStr);
            write(prn, out, strlen(out));
            SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix
		}
        memset(tempBuffer, 0x00, sizeof(tempBuffer));
        memset(tempBufferStr, 0x00, sizeof(tempBufferStr));

        if(usEMVGetTLVFromColxn(0x9f0f, tempBuffer, &len) == EMV_SUCCESS)
		{
            hex2asc((byte *) tempBufferStr, tempBuffer, len);
            memset(out, 0x00,sizeof(out));
            sprintf(out, "\t Online : %s\n", tempBufferStr);
            write(prn, out, strlen(out));
            SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix
		}
        memset(tempBuffer, 0x00, sizeof(tempBuffer));
        memset(tempBufferStr, 0x00, sizeof(tempBufferStr));

        if(usEMVGetTLVFromColxn(0x9f0d, tempBuffer, &len) == EMV_SUCCESS)
		{
            hex2asc((byte *) tempBufferStr, tempBuffer, len);
            memset(out, 0x00,sizeof(out));
            sprintf(out, "\t Default: %s\n", tempBufferStr);
            write(prn, out, strlen(out));
            SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix
		}
		//TAC
        memset(tempBufferStr, 0x00, sizeof(tempBufferStr));
        getTerminalParam(TAC_DENIAL,stTac,&tacLen);
        hex2asc((byte *) tempBufferStr, stTac, tacLen);
        memset(out, 0x00,sizeof(out));
        sprintf(out, "TAC:\n\t Denial : %s\n", tempBufferStr);
        write(prn, out, strlen(out));
        SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix

        memset(tempBufferStr, 0x00, sizeof(tempBufferStr));
        getTerminalParam(TAC_ONLINE,stTac,&tacLen);
        hex2asc((byte *) tempBufferStr, stTac, tacLen);
        memset(out, 0x00,sizeof(out));
        sprintf(out, "\t Online : %s\n", tempBufferStr);
        write(prn, out, strlen(out));
        SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix
		
        memset(tempBufferStr, 0x00, sizeof(tempBufferStr));
        getTerminalParam(TAC_DEFAULT,stTac,&tacLen);
        hex2asc((byte *) tempBufferStr, stTac, tacLen);
        memset(out, 0x00,sizeof(out));
        sprintf(out, "\t Default: %s\n", tempBufferStr);
        write(prn, out, strlen(out));
        SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix
     }

    // Tag 9f36 - Application Transaction Counter
	if(usEMVGetTLVFromColxn(0x9f36, tempBuffer, &len)== EMV_SUCCESS)
	{
	    hex2asc((byte *) tempBufferStr, tempBuffer, len);
	    memset(out, 0x00,sizeof(out));
	    sprintf(out, "ATC: %s\n", tempBufferStr);
	    write(prn, out, strlen(out));
	    SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix
	}


	// Tag 9f26 - Application Cryptogram
	if(usEMVGetTLVFromColxn(0x9f26, (byte *) tempBuffer, &len)== EMV_SUCCESS)
	{		
		hex2asc((byte *) tempBufferStr, tempBuffer, len);
		memset(out, 0x00,sizeof(out));
		sprintf(out, "App Cryptogram: %s\n", tempBufferStr);
		write(prn, out, strlen(out));
		SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix
	}
	//CM.091	Transaction should fallback to magswipe after an ICC read failure
	//for offline only configuration
	if((termTypeBuf[0] == 0x23) || (termTypeBuf[0] == 0x26))
	{
	
		// Tag 9F39 - POS Entry Mode
		if(usEMVGetTLVFromColxn(0x9f39, (byte *) tempBuffer, &len)== EMV_SUCCESS)
		{		
			    hex2asc((byte *) tempBufferStr, tempBuffer, len);
			    memset(out, 0x00,sizeof(out));
			    sprintf(out, "POS Entry Mode: %s\n", tempBufferStr);
			    write(prn, out, strlen(out));
			    SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix
		}
		// Tag 57 - Track2 Equivalent Data

	//	if(usEMVGetTLVFromColxn(0x5700, (byte *) tempBuffer, &len)== EMV_SUCCESS)
		if(trk2len)
		{		
	    
			    hex2asc((byte *) tempBufferStr, ucTrk2Data, trk2len);
			    memset(out, 0x00,sizeof(out));
			    sprintf(out, "Track2 Equi Data: %s\n", tempBufferStr);
			    write(prn, out, strlen(out));
			    SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix
		}	
	}
     
    // Approval
    memset(out, 0x00,sizeof(out));
    switch (inCondition)
    {
        case TRANS_APPROVED:
            ucDspMsg  = MSG_TRANS_APPROVED ;
            break;
        case TRANS_DECLINED:
            ucDspMsg = MSG_TRANS_DECLINED ;
            break;
        case OFFLINE_APPROVED:
            ucDspMsg = MSG_OFFLINE_APPROVED ;
            break;
        case OFFLINE_DECLINED:
            ucDspMsg = MSG_OFFLINE_DECLINED ;
            break;
        case OFFLINE_NOTALLOWED:
            ucDspMsg = MSG_OFFLINE_NOT_ALLOWED ;
            break;
        case NONEMV:
            ucDspMsg = MSG_NON_EMV; 
            break;
    }

    shRetrieveMessage(ucDspMsg, out);
    strcat(out,"\n\n"); 
    write(prn, out, strlen(out));
    SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix
    
    // Signature
    if (pinSigRequired)
    {
        memset(out, 0x00,sizeof(out));
        sprintf(out, "Signature: ...............................\n\n", tsiStr);
        write(prn, out, strlen(out));
        SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix
    }

    // TVR and TSI
    
	if(usEMVGetTLVFromColxn(0x9500, tvr, &len)== EMV_SUCCESS)
	{	
		    hex2asc((byte *) tvrStr, tvr, len);
		    memset(out, 0x00,sizeof(out));
		    sprintf(out, "TVR: %s\n", tvrStr);
		    write(prn, out, strlen(out));
		    SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix
	}  

	
	if(usEMVGetTLVFromColxn(0x9b00, tsi, &len)== EMV_SUCCESS)
	{			     
		    hex2asc((byte *) tsiStr, tsi, len);
		    memset(out, 0x00,sizeof(out));
		    sprintf(out, "TSI: %s\n\n", tsiStr);
		    write(prn, out, strlen(out));
		    SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix
	}

    if (numScripts > 0)
    {
        memset(out, 0x00,sizeof(out));
		//BUFSIZE	strcpy()
		//Added buffer size check
		if(sizeof(out) >= strlen("Script processing results:\n"))
        	strcpy(out, "Script processing results:\n");
        write(prn, out, strlen(out));
        SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix
    }
    for (i = 0; i < numScripts; i++)
    {
        hex2asc((byte *) scriptResStr, &issuerScriptResults[i * 5], 5);
        memset(out, 0x00,sizeof(out));
        sprintf(out, "%s\n", scriptResStr);
        write(prn, out, strlen(out));
        SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix
    }

	// Tisha_K1 20/Oct/10
	// Commented the global buffers used for storing the deciphered pin block.

	//Purvin_p1 Added on 13-Mar-07
	//added below code to Print Encihphered Pin block on Print Receipt.
	//memset(buffer, 0x00, sizeof(buffer));
	//Brabhu_H1 dated 24 July 2007
	//ENCPINBK: For made for onTimeID 13990 to maintain ICC PK length for
	//displaying the deciphered PIN block
	//len = 0;
   // shRetVal = get_env("PRINT_ENC_PIN_BLK", (char *)buffer, sizeof(buffer));
	//if(shRetVal!=0 && buffer[0]>='1')
	//{
	//	len = usEMVGetEncPINBlk(ucPinBlk);
	//	memset(outBuf, 0x00,sizeof(outBuf));
		//write(prn, outBuf, strlen(outBuf));
	//	hex2asc((byte *) outBuf, ucPinBlk, len);
		//sprintf(outBuf, "Enciphered PIN block: %s\n", outBuf);
	//	write(prn, "Enciphered PIN block:\n", 22);
	//	write(prn, outBuf, len*2);
	//	SVC_wAIT(100);
	//}

    memset(out, 0x00,sizeof(out));
	//BUFSIZE	strcpy()
	//Added buffer size check
	if(sizeof(out) >= strlen("\n\n\n\n\n\n\n"))
		strcpy(out, "\n\n\n\n\n\n\n");
    write(prn, out, strlen(out));
close(prn);
}

/*--------------------------------------------------------------------------
    Function:		Batchcapture
    Description:	batch data capture storeing all OFFLINE transction Data.
    Parameters:		void
    Returns:		
	Notes:          
--------------------------------------------------------------------------*/
int iBatchCapture(void)
{
    short           i = 0;
	struct          Opn_Blk;
//	char            panStr[21] = {0};
	//char   			tempBufferStr[505] = {0};	
    unsigned short  len = 0;
//	byte            pan[10]={0};        
    byte            transType = 0x00;            
	byte            tempBuffer[40] = {0};	
//	byte            buffer[5] = {0};
	BATCH_REC		RECORD;
	int			    fd = -1;

	memset((void *)&RECORD, 0x00, sizeof(RECORD));

	if( (fd = open("batch_file.dat", O_CREAT | O_RDWR) ) == -1) 
		return -1;
		
	if (lseek(fd, 0L, SEEK_END) == -1)  //VERIXPRTNG: Porting to Verix
		return -1;
	
	
		// Tag 9f36 - Application Transaction Counter
		/*if(usEMVGetTLVFromColxn(TAG_9F36_ATC,(BYTE*) RECORD.ucAtc, &len) != EMV_SUCCESS)
			memset(RECORD.ucAtc,0x00,sizeof(RECORD.ucAtc)); 

		// Tag 9f07 - Application Usage Control
		if(usEMVGetTLVFromColxn(TAG_9F07_APPL_USE_CNTRL, (BYTE*)RECORD.ucAuc, &len) != EMV_SUCCESS)
			memset(RECORD.ucAuc,0x00,sizeof(RECORD.ucAuc)); 

		// Tag 9f27 - CID
		if(usEMVGetTLVFromColxn( TAG_9F27_CRYPT_INFO_DATA , (BYTE*)RECORD.ucCid, &len) != EMV_SUCCESS)
			memset(RECORD.ucCid,0x00,sizeof(RECORD.ucCid)); 

		// Tag 0x8E  CVM List
   		memset(tempBufferStr, 0x00, sizeof(tempBufferStr));
		if(usEMVGetTLVFromColxn(TAG_8E00_CVM_LIST, (BYTE*)RECORD.ucCVMList, &len) != EMV_SUCCESS) 
			memset(RECORD.ucCVMList,0x00,sizeof(RECORD.ucCVMList)); 
		
		// Tag 9f34 - CVM Result
		if(usEMVGetTLVFromColxn(TAG_9F34_CVM_RESULTS, (BYTE*)RECORD.ucCVMResult, &len) != EMV_SUCCESS)
			memset(RECORD.ucCVMResult,0x00,sizeof(RECORD.ucCVMResult)); 
			
		// Tag 9f1E - IFD Serial Num
		if(usEMVGetTLVFromColxn(TAG_9F1E_IFD_SER_NUM, (BYTE*)RECORD.ucIFDSerialNum, &len) != EMV_SUCCESS) 
				memset(RECORD.ucIFDSerialNum,0x00,sizeof(RECORD.ucIFDSerialNum)); 

			
		//IAC
		memset(buffer, 0x00, sizeof(buffer));
	 	
    		if(usEMVGetTLVFromColxn(TAG_9F0D_IAC_DEFAULT, (BYTE*)RECORD.ucIACdef, &len) != EMV_SUCCESS)
				memset(RECORD.ucIACdef,0x00,sizeof(RECORD.ucIACdef)); 
			
    		if(usEMVGetTLVFromColxn(TAG_9F0E_IAC_DENIAL, (BYTE*)RECORD.ucIACden, &len) != EMV_SUCCESS)
				memset(RECORD.ucIACden,0x00,sizeof(RECORD.ucIACden)); 

    		if(usEMVGetTLVFromColxn( TAG_9F0F_IAC_ONLINE, (BYTE*)RECORD.ucIAConline, &len) != EMV_SUCCESS)
				memset(RECORD.ucIAConline,0x00,sizeof(RECORD.ucIAConline)); 

		

		// Tag 9f10 - Issuer Application Data
   		memset(tempBufferStr, 0x00, sizeof(tempBufferStr));
		if (usEMVGetTLVFromColxn(TAG_9F10_ISSUER_APP_DATA, (BYTE*)RECORD.ucIad, &len) != EMV_SUCCESS) 
			memset(RECORD.ucIad,0x00,sizeof(RECORD.ucIad)); 
		*/		

		//AIPBATCHADD: Ontime ID 38310 AIP addition Kishor_s1 added on 13-11-09 
		usEMVGetTLVFromColxn(TAG_8200_APPL_INTCHG_PROFILE,(BYTE*) RECORD.ucAIP, &len);
		usEMVGetTLVFromColxn(TAG_9F36_ATC,(BYTE*) RECORD.ucAtc, &len);
		usEMVGetTLVFromColxn(TAG_9F07_APPL_USE_CNTRL, (BYTE*)RECORD.ucAuc, &len);
        //Purvin_P1 Date 13-JUN-08
        // FIME ISSUE : To display Application Cryptogram in batchdata
		usEMVGetTLVFromColxn(TAG_9F26_APPL_CRYPTOGRAM, (BYTE*)RECORD.ucAppCrypto, &len);
		usEMVGetTLVFromColxn( TAG_9F27_CRYPT_INFO_DATA , (BYTE*)RECORD.ucCid, &len);
   		usEMVGetTLVFromColxn(TAG_8E00_CVM_LIST, (BYTE*)RECORD.ucCVMList, &len);
		usEMVGetTLVFromColxn(TAG_9F34_CVM_RESULTS, (BYTE*)RECORD.ucCVMResult, &len);
		usEMVGetTLVFromColxn(TAG_9F1E_IFD_SER_NUM, (BYTE*)RECORD.ucIFDSerialNum, &len);
 		usEMVGetTLVFromColxn(TAG_9F0D_IAC_DEFAULT, (BYTE*)RECORD.ucIACdef, &len);
	    usEMVGetTLVFromColxn(TAG_9F0E_IAC_DENIAL, (BYTE*)RECORD.ucIACden, &len);
		usEMVGetTLVFromColxn( TAG_9F0F_IAC_ONLINE, (BYTE*)RECORD.ucIAConline, &len);
		usEMVGetTLVFromColxn(TAG_9F10_ISSUER_APP_DATA, (BYTE*)RECORD.ucIad, &len);
		// Issure Script Results
		RECORD.iNumScripts=numScripts;
		
		if (numScripts > 0) 
		{
			for (i = 0; i < numScripts; i++)
			{
				//BUFSIZE
				//Added buffer size check
				if(sizeof(RECORD.ucIssuerScriptResults) >= (5 + (i * 5)))
					memcpy((BYTE*)&RECORD.ucIssuerScriptResults[i * 5], &issuerScriptResults[i * 5],5);	
			}
		}

		usEMVGetTLVFromColxn(TAG_9F33_TERM_CAP, (BYTE*)RECORD.ucTermCap, &len);
		usEMVGetTLVFromColxn(TAG_9F35_TERM_TYPE, (BYTE*)RECORD.ucTermType, &len);
		usEMVGetTLVFromColxn(TAG_9500_TVR, (BYTE*)RECORD.ucTvr, &len);
		usEMVGetTLVFromColxn(TAG_9B00_TSI, (BYTE*)RECORD.ucTsi, &len);
		//Ganesh_P1 30-Jun-10 PCI_CHANGE : Getting Data from global buffer	
		//usEMVGetTLVFromColxn( TAG_9F37_UNPRED_NUM, (BYTE*)RECORD.ucUnpredictNum, &len);
		memcpy(RECORD.ucUnpredictNum,ucUnpredictNum,shUnpredictNum_len);
		
		usEMVGetTLVFromColxn(TAG_9F01_ACQ_ID, (BYTE*)RECORD.ucAcqID, &len);

	
	//Ganesh_P1 30-Jun-10 PCI_CHANGE : Getting Data from global buffer
	/*	usEMVGetTLVFromColxn(TAG_9F02_AMT_AUTH_NUM,(BYTE*)tempBuffer, &len); 

		if(len > 0)
		{
			hex2asc((byte *) tempBufferStr, (BYTE*)tempBuffer, len);
			RECORD.ulAuthAmt = atol(tempBufferStr);
   	    }
    */
    		RECORD.ulAuthAmt = ulAuthAmt;
		
		memset(tempBuffer,0x00,sizeof(tempBuffer));    
		RECORD.ulOtherAmt = ulOtherAmt;
	/*
		usEMVGetTLVFromColxn(TAG_9F03_AMT_OTHER_NUM, (BYTE*)tempBuffer, &len);

		if(len > 0)
		{
			hex2asc((byte *) tempBufferStr,(BYTE*) tempBuffer, len);
			RECORD.ulOtherAmt = atol(tempBufferStr);
		}
	*/		
		usEMVGetTLVFromColxn(TAG_5F25_EFFECT_DATE , (BYTE*)RECORD.ucAppEffDate, &len);


		//Ganesh_P1 30-Jun-10 PCI_CHANGE : Getting Expiry Date from global buffer
		//usEMVGetTLVFromColxn(TAG_5F24_EXPIRY_DATE, (BYTE*)RECORD.ucAppExpDate, &len);
		memcpy(RECORD.ucAppExpDate,ucAppExDate,explen );
		
		//ret=usEMVGetTLVFromColxn(0x5a00, RECORD.ucPanNum, &len);
		//hex2asc((byte *) panStr, apppan, applen);

		
		//Ganesh_P1 30-Jun-10 PCI_CHANGE : Padding PAN data
		memcpy(RECORD.ucPanNum, apppan, panlen);
		strcat((char *)RECORD.ucPanNum,"\0");
		
	    usEMVGetTLVFromColxn(TAG_5F34_APPL_PAN_SEQNUM, (BYTE*)RECORD.ucPanSeqNum, &len);
		usEMVGetTLVFromColxn(TAG_89_AUTH_CODE, (BYTE*)RECORD.ucAuthCode, &len);

		//Ganesh_P1 30-Jun-10 PCI_CHANGE : Getting Data from global buffer
		//usEMVGetTLVFromColxn(TAG_8A_AUTH_RESP_CODE, (BYTE*)RECORD.ucARC, &len);
		memcpy(RECORD.ucARC,ucARC,shARClen);
		
		usEMVGetTLVFromColxn(TAG_5F28_ISSUER_COUNTY_CODE ,(BYTE*) RECORD.ucIssuerCtryCode, &len);
		usEMVGetTLVFromColxn(TAG_9F15_MER_CAT_CODE,(BYTE*) RECORD.ucMercCatCode, &len);
		usEMVGetTLVFromColxn(TAG_9F16_MER_ID, (BYTE*)RECORD.ucMercID, &len);
		usEMVGetTLVFromColxn(TAG_9F39_POS_ENTRY_MODE, (BYTE*)RECORD.ucPOSEntryMode, &len);
		usEMVGetTLVFromColxn( TAG_9F1A_TERM_COUNTY_CODE , (BYTE*)RECORD.ucTermCtryCode, &len);

		
		//Ganesh_P1 30-Jun-10 PCI_CHANGE : Getting Data from global buffer
		//usEMVGetTLVFromColxn(TAG_9F1C_TEMR_ID, (BYTE*)RECORD.ucTermID, &len);
		memcpy(RECORD.ucTermID,ucTermID,shTermIDlen);
		
		usEMVGetTLVFromColxn(TAG_5F2A_TRANS_CURCY_CODE , (BYTE*)RECORD.ucTranCurrCode, &len);
		usEMVGetTLVFromColxn(TAG_9A_TRAN_DATE , (BYTE*)RECORD.ucTxnDate, &len);
		usEMVGetTLVFromColxn(TAG_9F21_TRANS_TIME , (BYTE*)RECORD.ucTxnTime, &len);
		usEMVGetTLVFromColxn(TAG_9C00_TRAN_TYPE, &transType, &len);
		RECORD.ucTransType[0] =  transType;


		
		if(write(fd, (const char *)&RECORD, sizeof(RECORD)) < sizeof(RECORD)) 
			return -1;

		if (close(fd) == -1) 
			return  -1;
		
		return 1;
}


/*--------------------------------------------------------------------------
    Function:		inGetPrintBatchfile
    Description:	To prompt the user whether to print the print all transaction
    Parameters:		void
    Returns:		
	Notes:          
--------------------------------------------------------------------------*/
int iGetPrintBatchFile(void)
{
    int key;
	//char *transTypeLabels[] = {"1. Print Batch File  ", "2.Clear Batch File ","3.Exit" };
 
 	char *pcBatchMenuLabels[] = {"1.Print Batch File ", "2.Clear Batch File", "3.Exit"};

  //A+AUTOFT: Check introduced to avoid user intervention	
    if (gAPPFlag != 1)
 	   key = inMenuFunc(pcBatchMenuLabels, 3);
	else
	   key = 2;
 

	return key;               
}

/*--------------------------------------------------------------------------
    Function:		vdPrintReceiptall
    Description:	Print reciept
    Parameters:		short
    Returns:		
	Notes:          
-------------------------------------------------------------------------- */
int iPrintBatchFile(int iRes) 
{

    
    short           prn = 0,i = 0;
	struct          Opn_Blk prnBlock;
    char            out[512] = {0};
	char   			tempBufferStr[512] = {0}; 	
	int				fd = -1;
	BATCH_REC   	RECORD;
	int 			REC_SIZE = 0;
	char            panStr[21] = {0};
	char tmpstr[32];
	int len =0;
	REC_SIZE = sizeof(RECORD);

    if (iRes == 1) 
	{
		if( (fd = open("batch_file.dat", O_RDONLY) ) == -1) 
			return -1;
    }
    else if  (_remove("batch_file.dat") == -1) 
    		return -1;
//VERIXPRTNG: Porting to Verix      
#ifdef __thumb
		  prnBlock.rate = Rt_9600;
#else
		  prnBlock.rate = Rt_19200;
#endif

    prnBlock.format = Fmt_A8N1;
    prnBlock.protocol = P_char_mode;

//VERIXPRTNG: Porting to Verix
#ifdef __thumb
	if( shVx700Flag  == 1 )
	{
	    prn = open(DEV_COM8, 0);
    	set_opn_blk(prn, &prnBlock);
	}
	else
#endif		
	{
		prn = open(DEV_COM4, 0);
    	set_opn_blk(prn, &prnBlock);
	}
	
  	while(read(fd, (char *)&RECORD, REC_SIZE) == REC_SIZE)  
  	{
		//AIPBATCHADD: Ontime ID 38310 AIP addition Kishor_s1 added on 13-11-09 
		//AIP
		memset(tempBufferStr, 0x00, sizeof(tempBufferStr));
  		hex2asc((byte *) tempBufferStr, (BYTE*)RECORD.ucAIP, 2);
  		memset(out, 0x00,sizeof(out));
    	sprintf(out, "AIP: %s\n", tempBufferStr);
		Write( prn,out,strlen(out));
		SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix
		//ATC
		memset(tempBufferStr, 0x00, sizeof(tempBufferStr));
  		hex2asc((byte *) tempBufferStr, (BYTE*)RECORD.ucAtc, 2);
  		memset(out, 0x00,sizeof(out));
    	sprintf(out, "ATC: %s\n", tempBufferStr);
		Write( prn,out,strlen(out));
		SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix

		//AUC
 		memset(tempBufferStr, 0x00, sizeof(tempBufferStr));		
		hex2asc((byte *) tempBufferStr, (BYTE*)RECORD.ucAuc, 2);
  		memset(out, 0x00,sizeof(out));
		sprintf(out, "AUC: %s\n", tempBufferStr);
		Write( prn,out,strlen(out));
		SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix


        //Purvin_P1 Date 13-JUN-08
        // FIME ISSUE : To display Application Cryptogram in batchdata
		//APPLICATION CRYPTOGRAM (0x9F26)
 		memset(tempBufferStr, 0x00, sizeof(tempBufferStr));		
		hex2asc((byte *) tempBufferStr, (BYTE*)RECORD.ucAppCrypto, 8);
  		memset(out, 0x00,sizeof(out));
		sprintf(out, "AC: %s\n", tempBufferStr);
		Write( prn,out,strlen(out));
		SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix
		
		//CID
		memset(tempBufferStr, 0x00, sizeof(tempBufferStr));
		hex2asc((byte *) tempBufferStr, (BYTE*)RECORD.ucCid, 1);
		memset(out, 0x00,sizeof(out));
		sprintf(out, "CID: %s\n", tempBufferStr);
		Write( prn,out,strlen(out));
		SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix

		// CVM List
		memset(out, 0x00,sizeof(out));
		hex2asc((byte *) tempBufferStr, (BYTE*)RECORD.ucCVMList,252);
		sprintf(out, "CVM List: %s\n", (BYTE*)tempBufferStr);
		i=0;
		while(i<500)
		{
			Write( prn,&out[i],100);
			SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix
			i+=100;
		}
		Write(prn,&out[500],12);
		SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix

		// CVM Result
		memset(tempBufferStr, 0x00, sizeof(tempBufferStr));
		hex2asc((byte *) tempBufferStr, (BYTE*)RECORD.ucCVMResult, 3);
		memset(out, 0x00,sizeof(out));
		sprintf(out, "\nCVM Result: %s\n", tempBufferStr);
		Write( prn,out,strlen(out));
		SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix

		//IFD Serial Num
		memset(out, 0x00,sizeof(out));
		sprintf(out, "IFD Serial Number: %s\n", (BYTE*)RECORD.ucIFDSerialNum);
		Write(prn, out, strlen(out));
		SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix
				
  		//IAC default
		memset(tempBufferStr, 0x00, sizeof(tempBufferStr));
		hex2asc((byte *) tempBufferStr, (BYTE*)RECORD.ucIACdef, 5);
		memset(out, 0x00,sizeof(out));
		sprintf(out, "IAC Default: %s\n", tempBufferStr);
		Write( prn,out,strlen(out));
		SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix
		
		//IAC denial
		memset(tempBufferStr, 0x00, sizeof(tempBufferStr));
		hex2asc((byte *) tempBufferStr, (BYTE*)RECORD.ucIACden, 5);
		memset(out, 0x00,sizeof(out));
		sprintf(out, "IAC Denial: %s\n", tempBufferStr);
		Write( prn,out,strlen(out));
		SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix

		//IAC online
		memset(tempBufferStr, 0x00, sizeof(tempBufferStr));
		hex2asc((byte *) tempBufferStr,(BYTE*)RECORD.ucIAConline, 5);
		memset(out, 0x00,sizeof(out));
		sprintf(out, "IAC Online: %s\n", tempBufferStr);
		Write( prn,out,strlen(out));
		SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix

		//IAD
		memset(tempBufferStr, 0x00, sizeof(tempBufferStr));
		hex2asc((byte *) tempBufferStr,RECORD.ucIad, 32);
		memset(out, 0x00,sizeof(out));
		sprintf(out, "IAD: %s\n", tempBufferStr);
		Write( prn,out,strlen(out));
		SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix
		  
		//Script Results
		memset(out, 0x00,sizeof(out));
		sprintf(out, "Number of Scripts: %d\n", (BYTE*)RECORD.iNumScripts);
		Write(prn, out, strlen(out));
		SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix
				
		if (RECORD.iNumScripts > 0)
   		{
	    	for (i = 0; i < RECORD.iNumScripts; i++)
	    	{
	        	memset(out, 0x00,sizeof(out));
			    memset(tempBufferStr, 0x00, sizeof(tempBufferStr));
				//Purvin_P1 date : 24/Jun/08
				//Fix made for Reading Issuer scripts from Batchdata file
				hex2asc((byte *) tempBufferStr, &RECORD.ucIssuerScriptResults[i * 5], 5);				
				sprintf(out, "Issuer Script %s\n",tempBufferStr);
	        	Write( prn,out,strlen(out));
				SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix
			}
		}

		//Terminalcapabilties
		memset(tempBufferStr, 0x00, sizeof(tempBufferStr));
	    hex2asc((byte *) tempBufferStr,(BYTE*)RECORD.ucTermCap, 3);
	    memset(out, 0x00,sizeof(out));
		sprintf(out, "Terminal Capabilities: %s\n", tempBufferStr);
		Write( prn,out,strlen(out));
		SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix

		//TerminalType
		memset(tempBufferStr, 0x00, sizeof(tempBufferStr));
		hex2asc((byte *) tempBufferStr, (BYTE*)RECORD.ucTermType, 1);
		memset(out, 0x00,sizeof(out));
		sprintf(out, "Terminal Type: %s\n", tempBufferStr);
		Write( prn,out,strlen(out));
		SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix

		//TVR
		memset(tempBufferStr, 0x00, sizeof(tempBufferStr));
		hex2asc((byte *) tempBufferStr,(BYTE*)RECORD.ucTvr, 5);
		memset(out, 0x00,sizeof(out));
		sprintf(out, "TVR: %s\n", tempBufferStr);
		Write( prn,out,strlen(out));
		SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix


		//TSI
		memset(tempBufferStr, 0x00, sizeof(tempBufferStr));
		hex2asc((byte *) tempBufferStr,(BYTE*)RECORD.ucTsi, 2);
		memset(out, 0x00,sizeof(out));
		sprintf(out, "TSI: %s\n", tempBufferStr);
		Write( prn,out,strlen(out));
		SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix

		//UnpredicatableNum
		memset(tempBufferStr, 0x00, sizeof(tempBufferStr));
		hex2asc((byte *) tempBufferStr,(BYTE*)RECORD.ucUnpredictNum, 4);
		memset(out, 0x00,sizeof(out));
		sprintf(out, "Unpredictable Num: %s\n", tempBufferStr);
		Write(prn, out, strlen(out));
		SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix

		//AquierIdentifier
		memset(tempBufferStr, 0x00, sizeof(tempBufferStr));
		hex2asc((byte *) tempBufferStr,(BYTE*)RECORD.ucAcqID, 6);
		memset(out, 0x00,sizeof(out));
		sprintf(out, "Aquirer Identifier: %s\n", tempBufferStr);
		Write( prn,out,strlen(out));
		SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix

 		//AuthorisedAmt
		 sprintf(out, "Authorised Amt: %8.2f\n", (double)RECORD.ulAuthAmt/100);
		 Write( prn,out,strlen(out));
		 SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix

		//OtherAmt
		 sprintf(out, "Other Amt: %8.2f\n", (double)RECORD.ulOtherAmt/100);
		 Write( prn,out,strlen(out));
		 SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix

		 //Application Effective Date
		 memset(tempBufferStr, 0x00, sizeof(tempBufferStr));
		 hex2asc((byte *) tempBufferStr,(BYTE*)RECORD.ucAppEffDate, 3);
		 memset(out, 0x00,sizeof(out));
		 sprintf(out, "Application Effective Date: %s\n", tempBufferStr);
		 Write( prn,out,strlen(out));
		 SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix

		 //Application Expiration Date
		 memset(tempBufferStr, 0x00, sizeof(tempBufferStr));
		 hex2asc((byte *) tempBufferStr,(BYTE*)RECORD.ucAppExpDate ,3);
		 memset(out, 0x00,sizeof(out));
		 sprintf(out, "Application Expiration Date: %s\n", tempBufferStr);
		 Write( prn,out,strlen(out));
		 SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix
		 
		 //PAN 
		 memset(panStr, 0x00, sizeof(panStr));

		// for(i=0;RECORD.ucPanNum[i]!='^Z';i++ );
		 
   		 hex2asc((byte *) panStr, RECORD.ucPanNum, panlen);
		
		/*
		for(i=0;panStr[i]!='\0';i++ );
		
		clrscr();
		 printf("len : %d",i);
		 SVC_WAIT(2000);
		 clrscr();
		 */
				
		 len=strlen(panStr)-1;
		 while (panStr[strlen(panStr) - 1] == 'F')      // Get rid of any filler before displaying.
		 {
		   	len--;
			panStr[strlen(panStr) - 1] = '\0';
		 }
		 //BUFSIZE
		 //Added buffer size check
		 if(sizeof(RECORD.ucPanNum) >= strlen(panStr))
		 	memcpy(RECORD.ucPanNum,panStr,strlen(panStr)); 
	  	 strcat((char*)RECORD.ucPanNum, "\0");
		
		memset(tmpstr,0x00,sizeof(tmpstr));
		//Ganesh_P1 30-Jun-10 PCI_CHANGE : Padding PAN data
		if(len >= 4)
		{
			for(i=0; i < (len - 3); i++ )
			{
				tmpstr[i]='*';
			}
			memcpy(&tmpstr[i],&panStr[len-3],4);
			tmpstr[++len]='\0';
		}
		else
			memcpy(tmpstr,&panStr[0],strlen(panStr));

		
		 memset(out, 0x00,sizeof(out));
		 sprintf(out, "PAN: %s\n",(BYTE*)tmpstr);
		 Write( prn,out,strlen(out));
		 SVC_wAIT((unsigned long)100); //VERIXPRTNG: Porting to Verix

		 //Ontime ID: 61709	Vx EMV 6.0.0 
		 /*memset(out, 0x00,sizeof(out));
		 sprintf(out, "PAN: %s\n",(BYTE*)panStr);
		 Write( prn,out,strlen(out));
		 SVC_wAIT((unsigned long)100); //VERIXPRTNG: Porting to Verix*/
		/*
		 memset(out, 0x00,sizeof(out));
		 sprintf(out, "PAN: %s\n",(BYTE*)panStr);
		 Write( prn,out,strlen(out));
		 */
		 
		//PAN Seq
		 memset(tempBufferStr, 0x00, sizeof(tempBufferStr));
		 hex2asc((byte *) tempBufferStr,(BYTE*)RECORD.ucPanSeqNum, 1);
		 memset(out, 0x00,sizeof(out));
		 sprintf(out, "PAN Seq: %s\n", tempBufferStr);
		 Write( prn,out,strlen(out));
		 SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix

		 //AuthCode
		 memset(tempBufferStr, 0x00, sizeof(tempBufferStr));
		 hex2asc((byte *) tempBufferStr,RECORD.ucAuthCode, 6);
		 memset(out, 0x00,sizeof(out));
		 sprintf(out, "Authorization Code: %s\n", (BYTE*)RECORD.ucAuthCode);
		 Write( prn,out,strlen(out));
		 SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix

		//ARC
		 memset(out, 0x00,sizeof(out));
		 sprintf(out, "ARC: %s\n", RECORD.ucARC);
		 Write(prn, out, strlen(out));
		 SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix
		 		 
		//IssuerCtryCode
		 memset(tempBufferStr, 0x00, sizeof(tempBufferStr));
		 hex2asc((byte *) tempBufferStr,(BYTE*)RECORD.ucIssuerCtryCode, 2);
		 memset(out, 0x00,sizeof(out));
		 sprintf(out, "Issuer Country Code: %s\n", tempBufferStr);
		 Write( prn,out,strlen(out));
		 SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix

		 //MercCatCode
		 memset(tempBufferStr, 0x00, sizeof(tempBufferStr));
		 hex2asc((byte *) tempBufferStr,(BYTE*)RECORD.ucMercCatCode,2);
		 memset(out, 0x00,sizeof(out));
		 sprintf(out, "Merchant Category Code: %s\n", tempBufferStr);
		 Write( prn,out,strlen(out));
		 SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix

		 //MercID
		 memset(out, 0x00,sizeof(out));
		 sprintf(out, "Merchant Identifier: %s\n", (BYTE*)RECORD.ucMercID);
		 Write( prn,out,strlen(out));
		 SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix

		 //POSEntryMode
		 memset(tempBufferStr, 0x00, sizeof(tempBufferStr));
		 hex2asc((byte *) tempBufferStr,(BYTE*)RECORD.ucPOSEntryMode, 1);
		 strcat(tempBufferStr, "\0");
		 memset(out, 0x00,sizeof(out));
		 sprintf(out, "POS Entry Mode: %s\n", tempBufferStr);
		 Write( prn,out,strlen(out));
		 SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix
		
		  //TermCtryCode
		 memset(tempBufferStr, 0x00, sizeof(tempBufferStr));
		 hex2asc((byte *) tempBufferStr,(BYTE*)RECORD.ucTermCtryCode, 2);
		 memset(out, 0x00,sizeof(out));
		 sprintf(out, "Terminal Country Code: %s\n", tempBufferStr);
		 Write( prn,out,strlen(out));
		 SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix

		  //TermID
		  memset(out, 0x00,sizeof(out));
		  sprintf(out, "Terminal Identifier: %s\n", (BYTE*)RECORD.ucTermID);
		  Write( prn,out,strlen(out));
		  SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix

		  //TranCurrCode
		  memset(tempBufferStr, 0x00, sizeof(tempBufferStr));
		  hex2asc((byte *) tempBufferStr,(BYTE*)RECORD.ucTranCurrCode, 2);
		  memset(out, 0x00,sizeof(out));
		  sprintf(out, "Transaction Currency Code: %s\n", tempBufferStr);
		  Write( prn,out,strlen(out));
		  SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix

		  //TxnDate
		 memset(tempBufferStr, 0x00, sizeof(tempBufferStr));
		 hex2asc((byte *) tempBufferStr,(BYTE*)RECORD.ucTxnDate, 3);
		 memset(out, 0x00,sizeof(out));
		 sprintf(out, "Transaction Date(yymmdd): %s\n", tempBufferStr);
		 Write( prn,out,strlen(out));
		 SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix

		  //TxnTime
		 memset(tempBufferStr, 0x00, sizeof(tempBufferStr));
		 hex2asc((byte *) tempBufferStr,(BYTE*)RECORD.ucTxnTime, 3);
		 memset(out, 0x00,sizeof(out));
		 sprintf(out, "Transaction Time(hhmmss): %s\n", tempBufferStr);
		 Write( prn,out,strlen(out));
		 SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix

		  //TransType
		  memset(out, 0x00,sizeof(out));

		  //if(RECORD.ucTransType[0] == SALE)
		  {
			//	  strcpy(tempBufferStr,"Sale");
				  sprintf(out, "TransType: %2X  \n\n\n", RECORD.ucTransType[0]);
		  }
		  			Write( prn,out,strlen(out));
					SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix

		}

	
		if (close(prn) == -1) 
			return -1 ;
		if (close(fd) == -1) 
			return -1 ; 

		return 1;
}




/*--------------------------------------------------------------------------
    Function:		hex2asc
    Description:	
    Parameters:		BYTE *, BYTE *, int
    Returns:		
	Notes:          
--------------------------------------------------------------------------*/
void hex2asc(BYTE *outp, BYTE *inp, int length)
{
    short i = 0;

    for(i=0;i<length;i++)
	{
        if((inp[i] >> 4) > 9)
            outp[i*2] = (BYTE) ((inp[i]>>4) + 'A' - 10);
        else
            outp[i*2] = (BYTE) ((inp[i]>>4) + '0');

        if((inp[i] & 0x0f) > 9)
            outp[i*2+1] = (BYTE) ((inp[i] & 0x0f) + 'A' - 10);
        else
            outp[i*2+1] = (BYTE) ((inp[i] & 0x0f) + '0');
	}
    outp[i*2] = 0;

    return;
}

/*--------------------------------------------------------------------------
    Function:		magneticStripe
    Description:	
    Parameters:		char *
    Returns:		
	Notes:          
--------------------------------------------------------------------------*/
int magneticStripe(char* track2)
{
    char      buffer[400] = {0};
	short     reader = open(DEV_CARD,0);
	short     size = 0;
	short     i = 0, track = 0, count = 0;
	long      trm_event = 0L;
	short     retVal = 0;

		
    retVal = RERROR;
    trm_event = wait_event();
    if (trm_event & EVT_MAG)
	{
        size = read(reader, buffer, sizeof(buffer));
        if(size > 0)
		{
            retVal = ROK;
            i = 0;
            for(track = 1; track <= 2; track++)
			{
                count = (unsigned char) buffer[i];
                strncpy(track2, &buffer[i+2], count-2);
                track2[count] = 0;
                i += count;
			}
		}
	
	}
    return (retVal);
}

/*--------------------------------------------------------------------------
    Function:		usEMVDisplayErrorPrompt
    Description:	To display Appropriate error msgsfrom the config file
    Parameters:		unsigned short
    Returns:		
	Notes:          
--------------------------------------------------------------------------*/

void usEMVDisplayErrorPrompt(unsigned short errorID)
{
    unsigned char ucDspMsg = 0;
      
    switch (errorID)
    {

//	CAPK_DISP_BLOCK :Kishor_S1 commented the below code on 17-11-09 as per email confirmation		

	case E_CAPK_FILE_NOT_FOUND:    
    case E_INVALID_CAPK:		
    case E_CAPK_FILE_EXPIRED:		
	case E_NO_CAPK_DATA:		
		//Return without displaying any messages
			return;
		
/*	CAPK_DISP_BLOCK :Kishor_S1 commented the below code on 17-11-09 as per email confirmation		
		case E_CAPK_FILE_NOT_FOUND:
            ucDspMsg =  MSG_CAPK_FILE_NOT_FOUND;
		    break;
        case E_INVALID_CAPK:
            ucDspMsg =  MSG_INVALID_CAPK_FILE;
            break;
			//Soumya_g1 08/Mar/07 Added new Msg for CAPK expiry.
        case E_CAPK_FILE_EXPIRED:
            ucDspMsg =  MSG_EXPIRED_CAPK_FILE;
            break;
		//Pavan_K1 28/Mar/07 Added new Msg for no CAPK data.
		case E_NO_CAPK_DATA:
			ucDspMsg =  MSG_NO_CAPK_DATA;
            break;		*/
            
        case E_ICC_BLOCKED:
            ucDspMsg =  MSG_CARD_BLOCKED_BY_ISS;
            break;
        case E_PIN_REQD:
            ucDspMsg =  MSG_PIN_REQD;
            break;
        case E_LAST_PIN_TRY:
            ucDspMsg = MSG_PIN_LAST_CHANCE ;
            break;
        case E_PIN_TRY_LT_EXCEED:
            ucDspMsg = MSG_PIN_TRYLIMIT_EXCEEDED ;
            break;        	
		case E_USR_ABORT:
		case E_USR_PIN_CANCELLED:
            ucDspMsg = MSG_PIN_CANCELLED ;
            break;
        case E_USR_PIN_BYPASSED:
            ucDspMsg = MSG_PIN_BYPASSED ;
            break;        
        case E_PIN_BLOCKED:
            ucDspMsg =  MSG_PIN_BLOCKED;
            break;

        case EMV_PIN_SESSION_IN_PROGRESS:	
			
			//NUM_PIN_DIGITS
			if(shLogSysDebug && shmaxDebugmode)
				LOG_PRINTFF((EMVLIBFIL,"No.of PIN Digits Entered: %d", usEMVGetEchoCount()));					
			
	        ucDspMsg = MSG_PIN_SESSIONPROGRESS ;
             break;

        case EMV_PIN_SESSION_COMPLETE:
            ucDspMsg = MSG_PIN_SESSIONCOMPLETE ;
            break;
		case E_APP_EXPIRED:
			ucDspMsg = MSG_APP_EXPIRED;
			break;
		case E_INVALID_PIN: //Pavan_K1 dated 10-Apr-07 added to display invalid pin.
            ucDspMsg =  MSG_INVALID_PIN;
            break;

		//POS_CNCL
		case E_EXT_PIN_BYPASSED:
			ucDspMsg = MSG_EXT_PIN_BYPASS;
			break;
		//POS_CNCL
		case E_EXT_PIN_CANCELLED:
			ucDspMsg = MSG_EXT_PIN_CNCL;
			break;
			
        default:
            ucDspMsg = MSG_ERROR ;
            break;
    }

    clrscr();	
    shDisplayMessage(ucDspMsg, 1,8);
    //Pavan_K1 dated 08-Feb-07 changed the wait time to 3000.
	//ICTIMOUTDELAY: reduced the wait time from 3sec to 1sec as this is causing delay
	 //for Inter Char Time Out in case of secure PIN.
	 //SVC_wAIT((unsigned long)3000);  //VERIXPRTNG: Porting to Verix
	 SVC_wAIT((unsigned long)1000);  //VERIXPRTNG: Porting to Verix
	
    error_tone();
   
}



/*--------------------------------------------------------------------------
    Function:		usEMVPerformSignature
    Description:	
    Parameters:		void
    Returns:		
	Notes:          
--------------------------------------------------------------------------*/
unsigned short usEMVPerformSignature(void)
{
	unsigned short ushRet = EMV_SUCCESS;

	clrscr();

	//Pavan_K1 dated 04-Oct-07:
	//SIGPERFM: Reset the global var when the ret vlaue is other than success.
	
	if (ushRet != EMV_SUCCESS)
		pinSigRequired = 0;
	else
		pinSigRequired = 1;
	
	return (ushRet);
}


/*--------------------------------------------------------------------------
    Function:		usEMVPerformOnlinePIN
    Description:	To get Online PIN
    Parameters:		void
    Returns:		
	Notes:          
--------------------------------------------------------------------------*/
unsigned short usEMVPerformOnlinePIN(void)
{
	unsigned short retVal = 0;
	int iRet = 0;
	clrscr();

	//SCPINBYPASS:
	if(Bypass_Flag)
	{
		iRet = usEMVGetPINBypassFlag();

		if(iRet == E_PREV_PIN_NOT_BYPASSED)
		{

			retVal = inOnlinePINFunc();
			
			//090202-1383	Mary_F1		18/Jun/09
			oLFlag=0;

			if (retVal == SUCCESS)
				return (EMV_SUCCESS);
		
			else
				return retVal ;
		
		}
		else
			return E_USR_PIN_BYPASSED;
	}
	else
	{
		retVal = inOnlinePINFunc();

		//090202-1383	Mary_F1		18/Jun/09
		oLFlag=0;

		if (retVal == SUCCESS)
			return (EMV_SUCCESS);
		
		else
			return retVal ;
		
			
	}

}

/*--------------------------------------------------------------------------
    Function:		bIsCardBlackListed
    Description:	
    Parameters:		byte *, unsigned short, byte *, unsigned short
    Returns:		
	Notes:          
--------------------------------------------------------------------------*/
EMVBoolean bIsCardBlackListed(byte * pan, unsigned short panLen, byte * panSeqNo, unsigned short panSeqLen)
{

    char       panStr[21] = {0};
	char       panSeqStr[3] = {0};
	char       buf[25] = {0};
    char       panRead[21] = {0};
	char       panseqRead[3] = {0};
	short      fHandle = 0;
	short      retVal = 0;
	char       *pSeploc; 
	hex2asc((byte *) panStr, pan, panLen);
	hex2asc((byte *) panSeqStr, panSeqNo, panSeqLen);
	
	fHandle = open ("BlackListedCards.txt", O_RDONLY);
 
 	if(fHandle < 0)
 	    return(EMV_FALSE);
 
     memset(buf, 0x00, sizeof(buf));    
 
     retVal = readRecord(fHandle, buf);
 
     //check for read() returning 0 for EOF. 
     while(retVal != 0)
     {      
		if( (pSeploc = strchr( buf, ',' )) != NULL)
		{
			//BUFSIZE
		 	//Added buffer size check
		 	if(sizeof(panRead) >= (pSeploc-buf))
				memcpy(panRead,buf, (pSeploc-buf));
			if(sizeof(panseqRead) >= 2)
				memcpy(panseqRead, &buf[pSeploc-buf+1],2);

//Kishor_S1 added on 18-09-09 : If condition to check the length first
//Ontime Fix for 37306/36684 : V2CI0110002 there is not exact match b/w Card PAN and 
//PAN in balcklistedcard.txt, still terminal is setting Card appears in Exception file bit.
			if(strlen(panStr) == strlen(panRead))
				{
					if( !(memcmp (panStr,panRead,strlen(panStr))))
						if( !(memcmp (panSeqStr,panseqRead,strlen(panSeqStr))))
						{
							close (fHandle);
							return(EMV_TRUE);
						}
				}
		}
		
         retVal = readRecord(fHandle, buf);
 
         if(retVal == EMV_FALSE)
		 {	
			 close (fHandle);
             return(retVal); 
		 }
 
     } //end of while
 
     close (fHandle);
 
	return (EMV_FALSE);
}


/*--------------------------------------------------------------------------
    Function:		bUpdateLogforSplitSales
    Description:	
    Parameters:		void
    Returns:		
	Notes:          
--------------------------------------------------------------------------*/


EMVBoolean bUpdateLogforSplitSales(void)
{
	byte             pan[10] = {0};
	byte             panSeq[4] = {0};
	unsigned short   len = 0;
	short            fHandle = -1;
	char             RecStr[30] = {0};
	char             *temp;
	struct spsales   stSpsales;

	memset(&stSpsales,0x00, sizeof(stSpsales));

	// Tisha_K1 27/Oct/2010  - Fx for the OnTime Issue #47592
	
	//usEMVGetTLVFromColxn(0x5a00, pan, &len);
    hex2asc((byte *) stSpsales.panStr, apppan, panlen);
    
    while( (temp =  (char*)strrchr((char*)stSpsales.panStr,'F')) != NULL) 
	stSpsales.panStr[temp - stSpsales.panStr] = 0;

	//CJ.119.00
	//usEMVGetTLVFromColxn(0x5f34, panSeq, &len);
   	//hex2asc((byte *) stSpsales.panSeqStr, panSeq, len);
   	
	//Brabhu_H1 dateed 14 Jun 2007
	//SPLTAMT:Fix made to have 4 byte represention, hence changed from %ld to %lu
	sprintf(stSpsales.trnAmt,"%lu",glnAmount);

	//CJ.119.00
	//sprintf(RecStr,"%s,%s,%s\n\r",stSpsales.panStr,stSpsales.panSeqStr, stSpsales.trnAmt );
	sprintf(RecStr,"%s,%s\n\r",stSpsales.panStr, stSpsales.trnAmt );
	
	fHandle = open ("splitsales.log",  O_CREAT | O_RDWR);
 
 	if(fHandle < 0)
 	    return(EMV_FALSE);

	lseek(fHandle, 0L, SEEK_SET);
	insert(fHandle, RecStr, strlen(RecStr));

	if ((lseek(fHandle,(long)(4L * sizeof(stSpsales)),SEEK_SET)) == (4 * sizeof(stSpsales)))
	{
		delete(fHandle,sizeof(stSpsales));
	}
	
	close(fHandle);
	return(SUCCESS);

}

/*--------------------------------------------------------------------------
    Function:		ulCheckSplitSalesAmt
    Description:	
    Parameters:		void
    Returns:		
	Notes:          
--------------------------------------------------------------------------*/
unsigned long ulCheckSplitSalesAmt(void)
{

    char            panStr[21] = {0};
	char            panSeqStr[3] = {0};
	byte            pan[10] = {0};
	byte            panSeq[4] = {0};
	unsigned short  len = 0;
	short           fHandle = 0;  
	char            buf[150] = {0};
	char            *temp;
	unsigned  short retVal = 0; 
	char            *pSeploc; 

	usEMVGetTLVFromColxn(0x5a00, pan, &len);
    hex2asc((byte *) panStr, pan, len);
    
    while( (temp =  (char*)strrchr((char*)panStr,'F')) != NULL) 
	panStr[temp - panStr] = 0;

	//CJ.119.00
	//usEMVGetTLVFromColxn(0x5f34, panSeq, &len);
   	//hex2asc((byte *) panSeqStr, panSeq, len);

	memset(&stSpsales,0x00, sizeof(stSpsales));

	fHandle = open ("splitsales.log", O_RDONLY);
 
 	if(fHandle < 0)
 	    return(SUCCESS);

     memset(buf, 0x00, sizeof(buf));   
 
     retVal = readRecord(fHandle, buf);
 
     //check for read() returning 0 for EOF. 
     while(retVal != 0)
     {             
		if( (pSeploc = strchr( buf, ',' )) != NULL)
		{
			//BUFSIZE
		 	//Added buffer size check
			if(sizeof(stSpsales.panStr) >= (pSeploc-buf))
				memcpy(stSpsales.panStr,buf, (pSeploc-buf));
			
			//CJ.119.00
			//if(sizeof(stSpsales.panSeqStr) >= 2)
				//memcpy(stSpsales.panSeqStr, &buf[pSeploc-buf+1],2);				
			//if(sizeof(stSpsales.trnAmt) >= 8)
				//memcpy(stSpsales.trnAmt, &buf[pSeploc-buf+4],8);

			//PAN + 1
			if(sizeof(stSpsales.trnAmt) >= 8)
				memcpy(stSpsales.trnAmt, &buf[(pSeploc-buf) + 1],8);

			if( !(memcmp (panStr,stSpsales.panStr,strlen(panStr))))
				//if( !(memcmp (panSeqStr,stSpsales.panSeqStr,strlen(panSeqStr))))  //CJ.119.00
				{					
					close (fHandle);
					return (atol (stSpsales.trnAmt));
				}
		}
		
         retVal = readRecord(fHandle, buf);
         if(retVal == EMV_FALSE)
		 {	
			 close (fHandle);
            return(SUCCESS); 
		 }
 
     } //end of while
 
     close (fHandle);
	 return(SUCCESS);

}

/*--------------------------------------------------------------------------
    Function:		inGetPTermID
    Description:	Function to return a pointer to the PTID of the terminal
    Parameters:		char *
    Returns:		
	Notes:          
--------------------------------------------------------------------------*/
int inGetPTermID(char *ptid)
{
    char    tempPTID [EMV_TERM_ID_SIZE + 2] = {0};

    SVC_INFO_PTID (tempPTID);
    strncpy (ptid, tempPTID+1, EMV_TERM_ID_SIZE);

    ptid[EMV_TERM_ID_SIZE+1] = '\0';

    return (EMV_SUCCESS);
}


/*--------------------------------------------------------------------------
    Function:		readRecord
    Description:	
    Parameters:		int, char *
    Returns:		
	Notes:          
--------------------------------------------------------------------------*/
short readRecord(int fHandle , char *buf)
{
    
    char    readBytes = 0;
    short   retVal = 0;
    short   i = 0;

    while(readBytes != '\r')
    {
        retVal = read(fHandle, &readBytes, 1);
        if(retVal == 0)
               return 0 ;
		if(readBytes != '\n')
			buf[i++] = readBytes;

        if(retVal < 0)
	    {
		    close(fHandle);
		    return(EMV_FALSE);
	    }
    }
    buf[i] = '\0';    

    return (i);

} 

// Moved from the EMV MOdule to the Teset Harness code. These are EMV library call back functions.

EMVResult getLastTxnAmt(Ulong *amt)
{
	*amt = 0L;
	
	// Implementation for Split sales funcitonality..
	//T_Gangadhar_S1  UNATTENDED_TERMINAL : not supported Splitsalelog
	//SPLTSLES: Reverted the support for split sales log processing for unattended terminals
	//if(gshUnAttended != 1)
	
	*amt = ulCheckSplitSalesAmt();
	
	return (EMV_SUCCESS);
}


//added new callback for supporting acquirer/issuer specific CVMs.
EMVResult usEMVIssAcqCVM(unsigned short issacq, unsigned short *code)
{
    return(E_CVM_NOT_PERFORMED);
}

//Added the following callback during direct application selection 
//if the card returns failure or success during application selection.
EMVResult usEMVCheckICCResponse(byte *pCmd, Ushort usRetVal)
{
    return (APPL_TXN_CONTINUE); // This will allow the library to continue with the normal transaction flow.
}

//Added the following callback for velocity checking to perform or not
EMVResult usEMVIsVelocityCheckReqd()
{
    return VEL_CHECK_NOT_REQD ;
}
//Pavan_K1 dated 06-Mar-07 added all the below default application functions.

EMVResult getAppTerminalParam(Ushort id,byte* data, Ushort *pRetLen)
{
    byte buffer[25];

	//CodeReview:Check for pointer validity
	if(data == NULL)
		return( EMV_FAILURE );

    memset(buffer, 0x00, sizeof(buffer)); 
    switch(id)
    {
    case TAC_DENIAL:
       	 //TAC Denial
         //BUFSIZE
         //Buffer size check is not reqd as data is a pointer,care should be taken to pass
         //the buffer of enough size as a parameter while calling the API getAppTerminalParam
         memcpy(data, "\x00\x10\x00\x00\x00", 5);
		*pRetLen = 5;		    
        break;

    case TAC_ONLINE:
        //TAC Online
        memset(buffer, 0x00, sizeof(buffer)); 
        //BUFSIZE
        //Buffer size check is not reqd as data is a pointer
        memcpy(data, "\xD8\x40\x00\xf8\x00", 5);
        *pRetLen = 5;
        break;


    case TAC_DEFAULT:
        memset(buffer, 0x00, sizeof(buffer)); 
        //TAC Default
       	//BUFSIZE
       	//Buffer size check is not reqd as data is a pointer
        memcpy(data, "\xD8\x40\x00\xA8\x00", 5);
		*pRetLen = 5;
        break;
                	

    case TAG_97_TDOL:
        memset(buffer, 0x00, sizeof(buffer)); 
		//Default TDOL
   		//BUFSIZE
   		//Buffer size check cannot be done as data is a pointer
        memcpy(data, "\x95\x05", 2);
		*pRetLen = (strlen((char *)data)) / 2;
        break;

    case TAG_9F49_DDOL:
        memset(buffer, 0x00, sizeof(buffer)); 
		//Default DDOL
   		//BUFSIZE
   		//Buffer size check cannot be done as data is a pointer
        memcpy(data, "\x9F\x02\x06\x9F\x03\x06\x9F\x1A\x02\x95\x05\x9F\x37\x04", 14);
   		*pRetLen = (strlen((char *)data)) / 2;
        break;

    default:
        break;
    }
       
    return( EMV_SUCCESS );
}

unsigned short getAppCapkExp( const byte* aid, const byte capkIndex, \
                       byte* pstCapk, short *capkLen, \
                       byte* pstCapkExp, short* pCapkExpLen )
{
    char filename[20];
    int  fhandle;
    short asckeylen;
    short  ascexplen;
    short  bytes_read;
    char tmpBuf[500];   
	char keyHashBuf[40];
	//byte unused[200];
	unsigned char resBuf[1000];
	//char resBufHash[1000];
	char RID_WITH_CAPK_INDX[20];

	//short result;
	//unsigned long ulLeng = 0L; 

	write_at ("getCapkExp",strlen("getCapkExp"), 1, 3);
	write_at ("From Application",strlen("From Application"), 1, 4);
	SVC_wAIT((unsigned long)1000);  //VERIXPRTNG: Porting to Verix

	/* First create file name to look for - this will be of the form: */
	/* RIDNAME.NN, where NN is the index as an ASCII hex number */
	hex2Asc((byte *)filename, &aid[0], 5);	/* Only need RID - first 5 bytes */
	filename[10] = '.';
	hex2Asc((byte *)&filename[11], &capkIndex, 1);
	//BUFSIZE	strcpy()
	//Added buffer size check
	if(sizeof(RID_WITH_CAPK_INDX) >= strlen((const char *)filename))
    	strcpy(RID_WITH_CAPK_INDX,filename);

	fhandle = open(filename, O_RDONLY);

	if (fhandle <= 0)
        return(E_CAPK_FILE_NOT_FOUND);

	/* First read key length - this is 3 decimal digits */
	bytes_read = read(fhandle, (char *)tmpBuf, 3);
	if (bytes_read != 3)
	{
		close(fhandle);
		return(EMV_FAILURE);
	}
	tmpBuf[3] = 0;
	asckeylen = (atoi((char *)tmpBuf) * 2);


//asha_s2 dated 23 April 04
//fix for checksum not working properly
	hex2Asc((byte *)resBuf, &aid[0], 5);	/* Only need RID - first 5 bytes */
	hex2Asc((byte *)&resBuf[10], &capkIndex, 1);


	/* Now read in all of the key - it is in hex digits */
	bytes_read = read(fhandle, (char *)tmpBuf, asckeylen);
	if (bytes_read != asckeylen)
	{
		close(fhandle);
		return(EMV_FAILURE);
	}

	asciiToBin((byte *)tmpBuf, pstCapk, asckeylen);
	*capkLen = (asckeylen / 2);

//asha_s2 dated 23 April 04
//fix for checksum not working properly
	strncat((char *)&resBuf[11], (char *)tmpBuf,asckeylen);

	/* Now read exponent length - this is 2 decimal digits */
	bytes_read = read(fhandle, (char *)tmpBuf, 2);
	if (bytes_read != 2)
	{
		close(fhandle);
		return(EMV_FAILURE);
	}
	tmpBuf[2] = 0;
	ascexplen = (atoi((char *)tmpBuf) * 2);

	/* Now read in all of the exponent - it is in hex digits */
	bytes_read = read(fhandle, (char *)tmpBuf, ascexplen);
	if (bytes_read != ascexplen)
	{
		close(fhandle);
		return(EMV_FAILURE);
	}

//asha_s2 dated 23 April 04
//fix for checksum not working properly
	strncat((char *)&resBuf[11], (char *)tmpBuf,ascexplen);
	asciiToBin((byte *)tmpBuf, pstCapkExp, ascexplen);
	*pCapkExpLen = (ascexplen / 2);

	printIntValue("CAPK exp len", *pCapkExpLen);

    //adding code for hash checking.
	//Read the hash checksum - in hex.

	bytes_read = read(fhandle, (char *)tmpBuf, 40);

	if(bytes_read != 40)
	{
		close(fhandle);
		return(EMV_SUCCESS);
	}

	asciiToBin((byte *)tmpBuf, (byte *)keyHashBuf, 40);

	//do the hash calculation on the key.

	//ulLeng = (unsigned long)*capkLen ;

//asha_s2 dated 23 April 04
//fix for checksum not working properly
#if 0
	ulLeng = (unsigned long)(*capkLen+*pCapkExpLen+6);
	printIntValue("SHA1 Starring",result ) ;

//asha_s2 dated 23 April 04
//fix for checksum not working properly
	asciiToBin((byte *)resBuf, (byte *)resBufHash, 12+asckeylen+ascexplen);

#ifdef _TARG_68000
	//result = G_SHA(compHashBuf, pstCapk, SHA_1, (unsigned long)(*capkLen));
//asha_s2 dated 23 April 04
//fix for checksum not working properly
	result = G_SHA(compHashBuf, resBufHash, SHA_1, (unsigned long)(*capkLen+*pCapkExpLen+6)); //5 bytes of RID+1 byte of CAPK index
#endif

#ifdef __thumb
	//result = SHA1(unused,pstCapk,ulLeng ,compHashBuf) ;
//asha_s2 dated 23 April 04
//fix for checksum not working properly
	//result = SHA1(unused,pstCapk,ulLeng ,compHashBuf) ;
	result = SHA1(unused,resBufHash,ulLeng ,compHashBuf) ;
#endif

	printIntValue("SHA1",result ) ;

	if(memcmp(keyHashBuf,compHashBuf,20))
	{
		close(fhandle);
		return(EMV_FAILURE);
	}
#endif
	close(fhandle);

	return(EMV_SUCCESS);
}

unsigned short usAppESTValidateCSN(byte *CardCSN)
{
	write_at ("usESTValidateCSN",strlen("usESTValidateCSN"), 1, 3);
	write_at ("From Application",strlen("From Application"), 1, 4);
	SVC_wAIT((unsigned long)1000);  //VERIXPRTNG: Porting to Verix

	//return(VS_SUCCESS); //Pavan_K1 dated 05-Mar-07 changed to EMV_SUCCESS.
    return(EMV_SUCCESS);
}

EMVBoolean bAppIsMultipleOccurencesAllowed(byte *pAid)
{
    write_at ("bIsMultipleOccurencesAllowed",strlen("bIsMultipleOccurencesAllowed"), 1, 3);
	write_at ("From Application",strlen("From Application"), 1, 4);
	//return(VS_SUCCESS); //Pavan_K1 dated 05-Mar-07 changed to EMV_SUCCESS.
    return(EMV_TRUE);

}

void vAppDisplayPINPrompt ()
{
    return;
}

unsigned short getAppTransactionAmount()
{
    unsigned long ulAmount = 0;

    if (usAmtEntryFunc(&ulAmount) == TRANS_CANCELLED)
		return TRANS_CANCELLED;
	else
		return EMV_SUCCESS;
}
/*--------------------------------------------------------------------------
    Function:		SVC_wAIT
    Description:	 which works like SVC_wAIT
    Parameters:		unsigned long
    Returns:		
	Notes:          
--------------------------------------------------------------------------*/
int SVC_wAIT( unsigned long max_wait)
{
	unsigned long timeout;
	timeout = read_ticks() + max_wait;
	for (;;) 
	{
		
		//No input yet.  Check for timeout.
		if (read_ticks() > timeout)
		{
			return 0;
		}
	}
}

/*--------------------------------------------------------------------------
    Function:		WRITE 
    Description:	 which works like SVC_wAIT
    Parameters:		unsigned long
    Returns:		
	Notes:          
--------------------------------------------------------------------------*/
void Write(short prn,char *out,int len)
{
			
			write(prn, out, len);
			write(prn,"\r",1);
			SVC_wAIT((unsigned long)100);


}
//Brabhu_H1 Dated 08 July 2008
//ECTXNFLW: Following API is added to Issue Get Data CMD's if EC Issuer Auth 
//Code Tag is present
/*--------------------------------------------------------------------------
    Function:		getECData
    Description:	
    Parameters:		void
    Returns:		unsigned short which is the EMV result
	Notes:          
--------------------------------------------------------------------------*/
unsigned short usGetECData()
{
    Ushort      usDataLen                        = 0;
    byte        szRawBuffer[MAX_ICC_RESPONSE]    = {0};
    EMVResult   emvResult                        = EMV_SUCCESS;
	
	//Command issued to card to retrive the EC Balance value
	emvResult = usEMVGetData(TAG_9F79_EC_BALANCE, szRawBuffer, &usDataLen);
	
	//EC Balance is added to the collection buffer		
	if(emvResult != EMV_SUCCESS)
		return (emvResult);
	
	//EC Balance is validated for the length- incorporated as part CR 22/07/08
	if((usDataLen - 3) != EC_BALANCE_SIZE)
		return (BAD_ICC_RESPONSE);
	
    //Added the tags and there return values checked as part CR 22/07/08	
    emvResult = usEMVUpdateTLVInCollxn(TAG_9F79_EC_BALANCE, (byte *) &szRawBuffer[3], EC_BALANCE_SIZE);	
	if(emvResult != EMV_SUCCESS)
		return (emvResult);

	//Re-initialize the buffer values
	memset(szRawBuffer, 0x00, MAX_ICC_RESPONSE);
	usDataLen = 0;
	
	//Command issued to card to retrive the EC RST value	
	emvResult = usEMVGetData(TAG_9F6D_EC_RESET_THRESHOLD, szRawBuffer, &usDataLen);
	
	//EC Reset Threshold is added to the collection buffer		
	if(emvResult != EMV_SUCCESS)
		return (emvResult);
	
	//EC RST Tag is validated for the length- incorporated as part CR 22/07/08
	if((usDataLen - 3) != EC_RESET_THRESHOLD_SIZE)
		return (BAD_ICC_RESPONSE);

    //Added the tags and there return values checked as part CR 22/07/08
	emvResult = usEMVUpdateTLVInCollxn(TAG_9F6D_EC_RESET_THRESHOLD, (byte *) &szRawBuffer[3], EC_RESET_THRESHOLD_SIZE);	
	if(emvResult != EMV_SUCCESS)
		return (emvResult);

	return (EMV_SUCCESS);
}

/*Shiva_H2 : GetData Padding parse. GDFix:

	If there are padding bytes, GetData terminates if 
	this function returns 0. Else it continues.

*/
unsigned short GDCheck( void )
{
	return 1;
}
