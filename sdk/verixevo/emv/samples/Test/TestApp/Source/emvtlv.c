
/******************************************************************
  Filename		:	emvtlv.c
  Product		:     
  Version		:     
  Module		:   TestApp
  Description	:   Contains the TLV api for reading TLV objects from a buffer.
	
  
  Modification History:
    #		Date        Who				Comments
	
 * Copyright (C) 2006 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language, in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual or otherwise, without the prior written
 * permission of VeriFone Inc.
 *                                                2099 Gateway Place
 *             					                  Suite 600
 * 					                              San Jose  CA  95110 
 *              				                  USA
 *
 *
 *****************************************************************************/


#include "emvdef.h"
#include "emvproto.h"


/*--------------------------------------------------------------------------
    Function:		findTag
    Description:	To find a Tag in Raw Buffer
    Parameters:		unsigned short, byte *, short *, const byte *
    Returns:		
	Notes:          
--------------------------------------------------------------------------*/

#ifdef __thumb

short getNextTLVObject(unsigned short *tag, short *length, byte *value, const byte *buffer);
// Find a tag and return the data part of that tlv object in the value parameter and the length of the data part
short findTag(unsigned short tag, byte *value, short *length, const byte *buffer)
{
    byte            *ptr;
    unsigned short  nextTag = 0;
    short           found = 0;
    short           bufLen = 0;
    short           len = 0;
    short           bytesRead = 0;
    
	bufLen = buffer[10];
	ptr =(byte *) &buffer[11];                                                  

 // Start of data section
    found = 0;
    bytesRead = 0;
    do
    {
        bytesRead += getNextTLVObject(&nextTag, &len, value, ptr + bytesRead);
        if (nextTag == tag)
        {
            found = 1;
            *length = len;
            break;
        }
    } while (bytesRead < bufLen);

    if (found)
        return (1);

    return (0);
}

/*--------------------------------------------------------------------------
    Function:		getNextRawTLVData
    Description:	Gets the next tag and the raw tlv data
    Parameters:		unsigned short, byte *, const byte *
    Returns:		No. of bytes read
	Notes:          
--------------------------------------------------------------------------*/

short getNextRawTLVData(unsigned short *tag, byte *data, const byte *buffer)
{
    short   bytesRead = 0;
    byte    *ptr;
    byte    tagByte1 = '0';
    byte    tagByte2 = '0';
    byte    dataByte = '0';
    short   numTagBytes = 0;
    short   dataLength = 0;
    short   numLengthBytes = 0;
    short   i = 0;

    ptr = (byte *)buffer;   
    tagByte1 = *ptr;

    //    EMV specification says, any tag with == 0x1F (31) must be treated as two byte tags.

    if ((tagByte1 & 31) == 31)                                        // Bit pattern must be 10011111 or greater
    {
        ptr++;
        tagByte2 = *ptr;
        *tag = (short) ((tagByte1 << 8) + tagByte2);
        numTagBytes = 2;
    }
    else
    {
        *tag = (short) tagByte1;
        numTagBytes = 1;
    }

    // Get the data
    ptr++;

// Tisha_K1 08/Jul/09
// Commented the following block of code since ICCSim Host is sending the length in one byte. 
// For e.g. if the length is 129 then it will be sent as 0x81. No two byte length field handling is required.
numLengthBytes  = 1;
dataLength = *ptr;

#if 0	
    dataByte = *ptr;
    if (dataByte & 128)                                                 // If last bit is set
    {
        dataLength = 0;
        numLengthBytes = (short) dataByte & 127;                          // b7 - b1 represent the number of subsequent length bytes
        ptr++;
        for (i = 0; i < numLengthBytes; i++)
        {
            dataLength = (dataLength << 8) + (short) *ptr;
            ptr++;
        }   
    }
    else                                                                // Length field consists of 1 byte max value of 127
    {
        numLengthBytes = 1;
        dataLength = (short) *ptr;
        ptr++;
    }
#endif
    bytesRead = numTagBytes + numLengthBytes + dataLength;
	ptr = (byte *)buffer;   

    for (i = 0; i < bytesRead; i++)
    {
        data[i] = *ptr;
        ptr++;
    }

    return (bytesRead);
}

/*--------------------------------------------------------------------------
    Function:		getNextTLVObject
    Description:	Gets next tlv object separated into tag, length and value
    Parameters:		unsigned short *, short *, byte *, const byte *
    Returns:		No. of bytes read
	Notes:          
--------------------------------------------------------------------------*/

short getNextTLVObject(unsigned short *tag, short *length, byte *value, const byte *buffer)
{
    byte       *ptr;
    byte       tagByte1 = 0, tagByte2 = 0;
    byte       dataByte = 0;
    short      numLengthBytes = 0;
    short      dataLength = 0;
    short      i = 0;
    short      numTagBytes = 0;
    short      bytesRead = 0;

    ptr = (byte *)buffer;   

    tagByte1 = *ptr;

    //    EMV specification says, any tag with == 0x1F (31) must be treated as two byte tags.
    // 
    if ((tagByte1 & 31) == 31)                                        // Bit pattern must be 10011111 or greater
    {
        ptr++;
        tagByte2 = *ptr;
        *tag = (short) ((tagByte1 << 8) + tagByte2);
        numTagBytes = 2;
    }
    else
    {
        *tag = (short) tagByte1;
        numTagBytes = 1;
    }

    // Get the data
    ptr++;
    dataByte = *ptr;
    if (dataByte & 128)                                                 // If last bit is set
    {
        dataLength = 0;
        numLengthBytes = (short) dataByte & 127;                          // b7 - b1 represent the number of subsequent length bytes
        ptr++;
        for (i = 0; i < numLengthBytes; i++)
        {
            dataLength = (dataLength << 8) + (short) *ptr;
            ptr++;
        }   
    }
    else                                                                // Length field consists of 1 byte max value of 127
    {
        numLengthBytes = 1;
        dataLength = (short) *ptr;
        ptr++;
    }

    *length = dataLength;

    // ptr should now be pointing at the data
    for (i = 0; i < dataLength; i++)
    {
        value[i] = *ptr;
        ptr++;
    }

    bytesRead = numTagBytes + numLengthBytes + dataLength;

    return (bytesRead);
}

#endif


#ifdef _TARG_68000

// Find a tag and return the data part of that tlv object in the value parameter and the length of the data part
int findTag(unsigned int tag, byte *value, int *length, const byte *buffer)
{
    byte             *ptr;
    unsigned int     nextTag = 0;     //T_nabakishore_b1 17/07/07;changed unsigned short to unsigned int
    short            found = 0;
    short            bufLen = 0;
    int              len = 0;         //T_nabakishore_b1 17/07/07 changed short to int 
    short            bytesRead = 0;
    
    bufLen = buffer[10];
    ptr = &buffer[11];                                                  // Start of data section

    found = 0;
    bytesRead = 0;
    do
    {
        bytesRead += getNextTLVObject(&nextTag, &len, value, ptr + bytesRead);
        if (nextTag == tag)
        {
            found = 1;
            *length = len;
            break;
        }
    } while (bytesRead < bufLen);

    if (found)
        return (1);

    return (0);
}


// Gets the next tag and the raw tlv data
int getNextRawTLVData(unsigned int *tag, byte *data, const byte *buffer)
{
    short    bytesRead = 0;
    byte     *ptr;
    byte     tagByte1 = '0x00';
	byte     tagByte2 = '0x00';
    short    numTagBytes = 0;
    byte     dataByte = '0x00';
    short    dataLength = 0;
    short    numLengthBytes = 0;
    short    i = 0;

    // Get the tag
    ptr = buffer;
    tagByte1 = *ptr;

    //    EMV specification says, any tag with == 0x1F (31) must be treated as two byte tags.

    if ((tagByte1 & 31) == 31)                                        // Bit pattern must be 10011111 or greater
    {
        ptr++;
        tagByte2 = *ptr;
        *tag = (int) ((tagByte1 << 8) + tagByte2);
        numTagBytes = 2;
    }
    else
    {
        *tag = (int) tagByte1;
        numTagBytes = 1;
    }

    // Get the data
    ptr++;
    dataByte = *ptr;
    if (dataByte & 128)                                                 // If last bit is set
    {
        dataLength = 0;
        numLengthBytes = (int) dataByte & 127;                          // b7 - b1 represent the number of subsequent length bytes
        ptr++;
        for (i = 0; i < numLengthBytes; i++)
        {
            dataLength = (dataLength << 8) + (int) *ptr;
            ptr++;
        }   
    }
    else                                                                // Length field consists of 1 byte max value of 127
    {
        numLengthBytes = 1;
        dataLength = (int) *ptr;
        ptr++;
    }

    bytesRead = numTagBytes + numLengthBytes + dataLength;

    ptr = buffer;
    for (i = 0; i < bytesRead; i++)
    {
        data[i] = *ptr;
        ptr++;
    }

    return (bytesRead);
}


// Gets next tlv object separated into tag, length and value
int getNextTLVObject(unsigned int *tag, int *length, byte *value, const byte *buffer)
{
    byte      *ptr;
    byte      tagByte1 = 0, tagByte2 = 0;
    byte      dataByte = 0;
    short     numLengthBytes = 0;
    short     dataLength = 0;
    short     i = 0;
    short     numTagBytes = 0;
    short     bytesRead = 0;

    // Get the tag
    ptr = buffer;
    tagByte1 = *ptr;

    //    EMV specification says, any tag with == 0x1F (31) must be treated as two byte tags.

    if ((tagByte1 & 31) == 31)                                        // Bit pattern must be 10011111 or greater
    {
        ptr++;
        tagByte2 = *ptr;
        *tag = (int) ((tagByte1 << 8) + tagByte2);
        numTagBytes = 2;
    }
    else
    {
        *tag = (int) tagByte1;
        numTagBytes = 1;
    }

    // Get the data
    ptr++;
    dataByte = *ptr;
    if (dataByte & 128)                                                 // If last bit is set
    {
        dataLength = 0;
        numLengthBytes = (int) dataByte & 127;                          // b7 - b1 represent the number of subsequent length bytes
        ptr++;
        for (i = 0; i < numLengthBytes; i++)
        {
            dataLength = (dataLength << 8) + (int) *ptr;
            ptr++;
        }   
    }
    else                                                                // Length field consists of 1 byte max value of 127
    {
        numLengthBytes = 1;
        dataLength = (int) *ptr;
        ptr++;
    }

    *length = dataLength;

    // ptr should now be pointing at the data
    for (i = 0; i < dataLength; i++)
    {
        value[i] = *ptr;
        ptr++;
    }

    bytesRead = numTagBytes + numLengthBytes + dataLength;

    return (bytesRead);
}

#endif
