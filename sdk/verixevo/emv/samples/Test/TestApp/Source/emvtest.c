/******************************************************************
  Filename		:	emvtest.c
  Product		:     
  Version		:     
  Module		:   TestApp
  Description	:   Contains the main flow of EMV test harness. It makes use of the 
*					api provided by the Verix EMV Application to complete its transaction.
	
  
  Modification History:
    #		Date        Who				Comments
 *  1    08 Jan 07      t_arvind_g1     Added as per new requi. to decline the transaction before 
 *                                      GEN AC(FORCE_DECLINE).
 *  2    17 Jan 07      t_arvind_g1     Added to get environ variable to enable offline PinBypass
 *	
 *  3    30 Jan 07      Soumya_G1       TC# 2CM.042.06 - If the Combined CDA fails after first Gen AC 
                                        & the ICC decision is ARQC,then send an immediate Second GenAC
										to decline the Txn without connecting to the host.
 *  4    06 Feb 07      Pavan_K1        Corrected the Offline pinbyass environment variable.
 *  5    06 Feb 07      Pavan_K1        Added a global variable shDispModFlag for handling the 
 *                                      the displays in the VMP, VMT, V5 terminals.Handled the 
 *                                      language selection display.
 *  6    14 Feb 07      Purvin_p1 		Fix made foe OntimeDefect ID : 5085 
 *										If card returns application block then we should not fallback.
 *  7    23 Feb 07      Purvin_p1       Fix made foe OntimeDefect ID : 5463
 *										in FIRSTGENAC it returns 6A81 or 6983 then it should display emv failure on terminal.
 *  8    28 Feb 07      t_arvind_g1     Added to set pinparamwters for Secure Pin, when getusrpin 
 *                                      returns E_SECURE_PIN.
 *  9    07 Mar 07      Purvin_p1       fix made for DEFECT # 6763 serialBuf was padded with 0xFF, 
 *                                      so before retriving TERMSERNUM from terminal.we should set whole buffer to 0x00.
 *  10   07 Mar 07      Pavan_K1        Added the files vSetDefaultFunctionPointers, 
 *                                      vSetAppFunctionPointers for the Common module Implementation test.
 *  11   30 Apr 07      Purvin_p1		defect ID 8876.Internal Auth Cmd didn't have the value for the txn amount (0x9F02).
 *  12   10 May 07      Purvin_p1	    Application changes for Interac.
 *  13   14 May 07      Brabhu_H1       Interac specific changes are done for language selection.
 *  14   14 May 07      Pavan_K1        Implemented the Accoun type selection before Application selection.
 *
 *  15   14 May 07      Purvin_p1       Reset Issuerscriptresult buffer to null at begining of transaction for interac. 
 *  16   14 Jun 07      Brabhu_H1       SPLTAMT:Fix made to have 4 byte unsigned represention, hence changed from %ld to %lu
 *  17   14 Jan 07      Brabhu_H1       FLBKAMT:Fix made for onTime ID 12177, to request amount during fallback
 *
 *  18   15 Jun 07      Purvin_p1       Fix made for Amount screen should come before Account type selection as per Interac requirement,Ontime Id : 12796.
 *  19   18 Jun 07      Purvin_p1       if we get chip error or Invalid ATR from library then we should prompt for fallback.

 *  20   18/Jun/07      Soumya_G1       Changed the Env Variable to BYPASS_PIN from BYPASS_OFFLINE_PIN
                                        This is for the changed requirement on the Bypass feature.
                                        Now if Any One pin is bypassed, then any consecutive pin should be bypassed.
 *  21   20 Jun 07      Brabhu_H1	    FLBK6985: To allow fallback for a card response 6985
 *  22   21 Jun 07      Brabhu_H1       Fix made to display new modified checksum
 *  23   11/Jul/07      Tisha_K1        AMTCLBK: Modified the usAmtEntryFunc callback API invoking. New IDs are
 *                                      are identified to describe whether the callback API is invoked for amount/cashback/account type
 *  24   13 Jul 07      Brabhu_H1       LANGIEMV:Language selection has to be done is same way for both INTERAC and non-INTERAC transactions
 *
 *  25   18 Jul 07      Pavan_K1        Fix for Ontime ID #13808. Handling the return value of the function.
 *  26   19 Jul 07      Pavan_K1        Fix made to update the 81 and 9F02 tag with the cash back amount if the transaction is cashback.
 *  27   08 Aug 07      Pavan_K1        KEYREVCN: calling key revocation function from main.
 *  28   16 Aug 07      Brabhu_H1       BL54TRME: Changes are made as per bulletin #54
 *  29   23 Aug 07      Brabhu_H1       NOPRNTRP: Fix made, not to do print 
 *                                      reciept for fallback processing
 *  30   24 Oct 07      Brabhu_H1       FALLBKFX: Fix made to avoid fallback for
 *                                      offline configuration
 *  31  15 Mar 08       Purvin_P1 		Vx700: there is not function key for vx700 terminal, So all function key are mapped with number key.
		                                         F1=Numeric 1,F2=Numeric 2,F3=Numeric 3 and F4=Numeric 4
 *  32  30 Apr 08       Purvin_P1       UNATTENDED_TERMINAL : Added Support for UNATTENDED_TERMINAL.
 *  33  05 May 08 		T_Gangadhar_S1  BATCH_DATA_CAPTURE : Added support for BATCH_DATA_CAPTURE.
 *  34  08 May 08       T_Gangadhar_S1  UNATTENDED_TERMINAL : Issuer Referrals menu will not be displayed 
 *	34	11/Jun/08		Mary_F1			FORCETRM: Added the new env. variable EMVFORCETRM
 *	35	06/Jul/09		Mary_F1			BUFSIZE
 *										Buffer size is checked for memcpy() and strcpy()	
 *	36	07/Jul/09		Mary_F1			CodeReview:
 *										Incorporated code review feedback
 *  37  14/Jul/09		Kishor_S1		NOGPOINSEL : Added for GPO & App selection separation changes
 *  38  03/Aug/09       Kishor_S1       CHINAFTR: Added the support for China EMV Module feature.(Moved the fix)
 *  39  03/Aug/09       Kishor_S1       ECTXNFLW: Issue Get Data CMD's if EC Issuer Auth Code Tag is present(Moved the fix)
 *  40  03/Aug/09       Kishor_S1       BATCHDFX: Fix made to avoid duplication of batch data.(Moved the fix)
 *	41	06/Aug/09		Mary_F1			CA.103.01
 *										Tag99 should be sent as part of online message only incase of
 *										online PIN CVM.
 *  42	18/Aug/09		Mary_F1			Ontime ID: 36560
 *         								Fix for the issue that, the transaction should fallback to magswipe if Gen AC returns
 *          							6985, incase of attended terminals (other than Vx700)
 *  43  18/Aug/09		Mary_F1			Ontime ID :36562
 *										Fix for the issue that, the terminal should allow fallback if the card returns
 *										invalid ATR 
 *  44	20/Aug/09		Mary_F1			MAGTRANS
 *										Terminal should communicate with the online host when a magswipe transaction 
 *										goes online
 *  45	20/Aug/09		Mary_F1			Changed the version num. buffer size from 11 to 13
 *  
 *  46  04/Sept/09		Mary_F1			INVMOD:
 *										If this module is downloaded to a Vx700 terminal, it should return
 *										E_INVALID_MODULE and the application should not continue.
 *
 *  47  30/Sept/09		Mary_F1			NoPPSUP: PINPad is not supported for Vx700 terminals
 *
 *  48	14/Oct/09		Kishor_S1		INVMOD: commented the INVMOD changes as per the confimation mail		
 *
 *  49	22/Oct/09		Mary_F1			UPTMAGREAD
 *										Implementation for testing mag card read
 *
 *  50	29/10/09		Kishor_S1	  	UNATTENDED:Changes for unattended terminal types moved	     
 *
 *  51	 5/1/10		    Kishor_S1	  	ENABLENUMKEYS:Changes made to support numeric keys selection
 *										adding "ucAllowKeys" variable for Vx680 & Vx520 terminals.
 *
 *  52   5/1/10	 	    Kishor_S1		Ontime Fix for 39220: When the terminal asks for the approving 
 *										of the transaction ,pressing of key 1 or 2 should be accepted.
 *  53  02/Mar/09		Mary_F1			CA.035  Transaction should be terminated after 2nd GenAC if the status word
 *										of the response to the 2nd GenAC command is other than 9000
 *
 *  54	03/Mar/09		Mary_F1			CM.091	Transaction should fallback to magswipe after an ICC read failure
 *										for offline only configuration
 *
 *  55	12/Mar/09		Mary_F1			2CO.018	Print Receipt should be supported to print cardholder copy as "Print Cardholder"
 *		  								is "YES" for unattended terminals
 *
 *  56	12/Mar/09		Mary_F1			UNATNDMAGREAD
 *										Terminal should be able to perform independent magstripe
 *										transaction for unattended terminals
 *
 *	57	09/Jun/10		Mary_F1 		CA.112.00	Ontime ID: 43998
 *
 *	58	09/Jun/10		Mary_F1			CN.012.00.01	Ontime ID: 44225
 *
 *  59	23/Jun/10		Mary_F1			2CO.006.00	Ontime ID: 44566
 *	
 *	60	12/Jan/11		Ganesh_P1		POWER_MGMT: Power Management changes
 *  61  03/Jun/11       Mary_F1         MULTIAVN: Support for multiple application version number check
 *  62  03/Jun/11       Mary_F1			BKSPKEY: Terminal should re-prompt the previous prompt if backspace
 *										key is pressed during the "ENTER PIN" prompt
 *  63  03/Jun/11       Mary_F1			ExtAuth6985: The EMV Kernel behavior is modified to cater to the different application needs,
 *										to terminate the transaction or to continue with the transaction on receiving the status word
 *										as 0x6985 for the external authenticate command.
 * 	64	18/Aug/11		Kishor_S1		NAND_FLASH : Changes for Issuer Script processing accept through buffer instead of file
 *									    NAND_FLASH  : Config.sys File access changes incorporated.
 *   65   09/Feb/12		Mary_F1	    Ontime ID: 61519	Vx EMV 6.0.0
 *									    Terminal should display "Not Accepted" message.
 *   66   11/Feb/12		Mary_F1	    Ontime ID: 55409 Vx EMV 6.0.0
 *									    For a magstripe transaction, "Failed" message is displayed instead of "Success"
 *   67   27/Apr/12        Mary_F1         ZEROAMTBC: Fix for amount value being zero in batch data capture.
 *   68   28/Nov/12       Mary_F1	    BKSPENABLE: Enables the backspace key feature.		 
 *
 * Copyright (C) 2010 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language, in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual or otherwise, without the prior written
 * permission of VeriFone Inc.
 *                                                2099 Gateway Place
 *             					                  Suite 600
 * 					                              San Jose  CA  95110 
 *              				                  USA
 *
 *
 *****************************************************************************/

#include "emvdef.h" 
#include "emvproto.h"
#include "emvpin.h" 
#include "emvappmsg.h" 
#include "vxemvapdef.h"
#include "EMVCWrappers.h"
#include "EUtils.hpp"
#include <svc.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include "logsys.h"
#ifdef __thumb		//VERIXPRTNG: Porting to Verix
#include "extlibvoy.h"
#endif

#ifdef VxGENCONFIG
#include "EMVGenConfig.h"
#endif
#ifdef NONSECURE_PIN
#include "FirmwareDld.h"
#endif

int                con;
int                pinSigRequired;
int                masterKeyIndex;
int                ppPresent=0;
int                numScripts;
short              shDispModFlag = 0; //Pavan_K1 dated 5-Feb-07 added for the display issues for VMT
short              gshNoRec;
short              Bypass_Flag = 0;
unsigned char      CKclear[16];
unsigned char      CKencr[16];
unsigned char      termTypeBuf[2] = {0};
EMVBoolean         bFallbackAllowed = EMV_FALSE ;
extern short       shPin_Bypass;
byte               *issuerScriptResults = NULL;
MSG_REC			   *gstDspMsg ;
short              shVx700Flag = 0; 
short				gshInterac = 0; //Purvin_p1 date 05/May/2007 added for Interac.
short				gshUnAttended = 0; //Purvin_p1 date 30/April/2008 added to Support Unattended Terminal.

unsigned char 		ucAllowKeys=0;
//A+AUTOFT: Added Global Flag	
int                 gAPPFlag = 0;
//Ganesh_p1	10-Aug-11 Bug fix for Ontime Id :55409 : Unbale to carry out Magstripe transaction
char				tk2[100] 		   = {0};

//SECPINTIMOUT
unsigned char 		ucAbrtOnPINTOut = 0;

extern int         inGetTermDecisnOnForceDecline(void);

//Ganesh_P1 30-Jun-10 PCI_CHANGE : Padding PAN data
unsigned char apppan[32];
unsigned char ucCardholdername[64], ucSvcCode[8],ucAppExDate[8], ucTrk2Data[48] , ucPinData[16],ucARC[3], ucTermID[9+8],ucUnpredictNum[16] ;
unsigned short panlen = 0, cnamelen=0,svclen=0,explen=0,trk2len=0,pinlen=0, shARClen, shTermIDlen, shAmt_Auth_len, shOther_Amt_len, shUnpredictNum_len;
unsigned long ulAuthAmt, ulOtherAmt;
//Start ticks and end ticks variable
unsigned long ulTimerStart = 0L;
unsigned long ulTimerEnd   = 0L;
byte ucEnvVar[5];
unsigned long ulTxnTime    = 0L;
#if _SECURE_PIN // Enable it incase of the secure pin
extern void displayPINPrompt(void);
extern int inVXEMVAPSetDisplayPINPrompt(void (*fptr) (void));
#endif

struct stLangConfig gstLangConfig;

//NAND_FLASH  : Config.sys File access changes
char	  szSupp_Lang[10] = {0};
char	  szChinaFlag[2]	= {0}; //Kishor_S1 dated 03 Aug 2009 added to set functionality if its a EC Txn
char	  gszECTXLimitAmount[13]	= {0};		
char	  gserialBuf[12] = {0};

#ifdef NONSECURE_PIN
void callbackfn(int cnt, int tot);

#endif

//SPAIN_REQ
unsigned short usGetBypassDecsn(void);

Ushort usStoreBatchData();



/*-------------------------------------------------------------------------
    Function:		main
    Description:	
    Parameters:		void
    Returns:		
	Notes:
--------------------------------------------------------------------------*/
int main()
{
	unsigned short iRet=0;
    unsigned short      res                    = 0;
    unsigned char       ucChkSumBuffer[25]     = {0};
    short               init                   = 0; 
	short               size                   = 0;
    short               hostDecision           = 0;
    short               key                    = 0;
	short               shRetVal               = 0;
    char                VerNo[13]              = {0};//Changed the version num. buffer size from 11 to 13
    char                track2[100]            = {0};
	char                strModelno[15]         = {0}; //Pavan_K1 dated 5-Feb-07 added to get the model number
    byte                msg[MAX_MESSAGE_SIZE]  = {0};
    byte                resp[MAX_MESSAGE_SIZE] = {0};
    byte                Bypass[5]              = {0};
	int 				resBatch = -1; //T_Gangadhar_S1 
	byte				szInterac[2] ={0}; //Purvin_p1 added for INTTERAC
    //A+AUTOFT: Added a buffer to get the AUTOFLAG settings
    byte                szAutoFlag[3]          = {0};	
    //A+AUTOFT: Introduced configurable Delay per transaction
    unsigned int        uiDelay                = 0;
    char                szDelay[7]             = {0};	
	int                 iModelNo = 0;
	char                szModelNo[6]= {0};
	//Purvin_P1 dated 29/04/08 Added For Testing of KeyIntroduction
    DWORD                ushRetVal;

	//MAGCARD : Added Variable for testing MAG Card Data
	byte szMagCard[2];
	
    byte             	btTermType             = 0;      // Terminal Type (tag 9F35)	
    short               sUnattendTrmType       = 0;

//   	char		    	szChinaFlag[2]  = {0}; //Kishor_S1 dated 03 Aug 2009 added to set functionality if its a EC Txn

	//MAGTRANS
	short shindex = 0;
	
#ifdef NONSECURE_PIN	
		
	byte				szFWDLDFlag[3]			= {0};	
	unsigned long l2_ver=0, bl_ver=0;
	int ret=0;
	
   unsigned  char                FwVerNo[32]              = {0};
#endif

#ifdef VxGENCONFIG
    	byte                szConfigFlag[3]          = {0};	

#endif
#ifdef PERFORM_CMD
	unsigned long ulStartTime = 0L;
	unsigned long ulEndTime   = 0L;
	char buf[50]              = {0};
#endif

    con = open(DEV_CONSOLE, O_RDWR);	
    
    clrscr();
    //Pavan_K1 dated 5-Feb-07 added the below function to get the model no.

#ifdef NONSECURE_PIN	
	
    iModelNo = SVC_INFO_MODELNO (strModelno);


	if(iModelNo == 0)//success
	{
		if((strncmp(strModelno, "VMP", 3) == 0) || (strncmp(strModelno, "VMT", 3) == 0) 
		                                    || (strncmp(strModelno, "V5", 2) == 0))
			 shDispModFlag = 1;
	
		if((strncmp(strModelno, "Vx700", 5) == 0) || (strncmp(strModelno, "VX700", 5) == 0) 
		                                    )
			 shVx700Flag = 1;
	}

	if(!shVx700Flag)
	{
		iModelNo = get_env("MODELNO", (char *)szModelNo, sizeof(szModelNo));    

		if(iModelNo !=0 )
		{
			if((memcmp(szModelNo, "Vx700",5) == 0) || (memcmp(szModelNo, "VX700",5) == 0))
				shVx700Flag = 1;
			else
				shVx700Flag = 0;
		}
		else
			shVx700Flag = 0;
	}

#endif	
	//Kishor_S1 added on 5-1-10 :To read the environment variable

	shRetVal = SVC_INFO_SERLNO (gserialBuf) ;
	if(!(isalnum(gserialBuf[0])))
	{
	 //Purvin_p1 date 07/Mar/2007 DEFECT # 6763
		 //serialBuf was padded with 0xFF, so before retriving TERMSERNUM from terminal.
		 //we should set whole buffer to 0x00
	 	 memset(gserialBuf,0x00,sizeof(gserialBuf));
		shRetVal = get_env("TERMSERNUM", gserialBuf, sizeof(gserialBuf));
		//BUFSIZE
		//Buffer size check is not reqd we are copying sizeof(serialBuf) bytes to serialBuf
		if (shRetVal == 0)
			memcpy(gserialBuf, "10102397", strlen("10102397"));
	}

	//SECPINTIMOUT	
	shRetVal = get_env("ABORTONPINTIMOUT", (char *)szAutoFlag, sizeof(szAutoFlag));
	if( (shRetVal != 0 ) &&  (szAutoFlag[0] == '1') )
		ucAbrtOnPINTOut = 1;

	

    //A+AUTOFT: Initialized the Global Flag	
    shRetVal = get_env("NUMKEYS", (char *)szAutoFlag, sizeof(szAutoFlag));
    if( (shRetVal != 0 ) &&  (szAutoFlag[0] == '1') )
        ucAllowKeys = 1;

	if( (shVx700Flag) && (ucAllowKeys == 0) )
        ucAllowKeys = 1;
		

	shRetVal=0;
	memset(szAutoFlag,0x00,sizeof(szAutoFlag));
    //A+AUTOFT: Initialized the Global Flag	
    shRetVal = get_env("AUTOFLAG", (char *)szAutoFlag, sizeof(szAutoFlag));
    if(shRetVal != 0 && szAutoFlag[0] == '1')
        gAPPFlag = 1;

    //Initialising the message buffer with the default language
    
    if ((res = inInitDspMsgs()) != SUCCESS)
        vdPromptManager(INVALID_MSG_FILE);
    
    write_at("EMV MODULE APPL", 15, 1, 1);
    usVXEMVAPGetVersion(VerNo);

    if (shDispModFlag)
    {
        write_at(VerNo, strlen(VerNo), 5, 2);
        shDisplayMessage(MSG_PRESS_ANY_KEY, 1,4);
    }
    else
    {
        write_at(VerNo, strlen(VerNo), 5, 4);
        shDisplayMessage(MSG_PRESS_ANY_KEY, 1,8);
    }

	
    chGetKeyPress();
    clrscr();

#ifdef VxGENCONFIG
	//Support for Text based config files
	shRetVal = get_env("EMVTEXTCONFIG", (char *)szConfigFlag, sizeof(szConfigFlag));

	if(shRetVal!=0 && szConfigFlag[0] == '1')
	{
		clrscr();
		write_at("CREATING CONFIG FILES...",strlen("CREATING CONFIG FILES..."), 1, 3);
		res = inVXEMVAPConvertConfigData("ICCKEYS.KEY","ICCDATA.DAT");

		if(res==GEN_CONFIG_SUCCESS)
		{
			remove("MVT.DAT");
			remove("EST.DAT");
			rename("MVT_DAT.DAT","MVT.DAT");
			rename("EST_DAT.DAT","EST.DAT");
		}
		else
		{
			res=inVXEMVAPGetLastErrCode();
			clrscr();
			printf("ERROR CODE :%d\n",res);
			SVC_WAIT(2000);
			clrscr(); 
		}

		clrscr();
	}
#endif
//VERIXPRTNG: Porting to Verix
#ifdef __thumb
	//Purvin_P1 dated 29/04/08 Added For Testing of KeyIntroduction
		//UNATTENDED_TERMINAL : For Vx700 and OpScr

#ifdef NONSECURE_PIN
		if(shVx700Flag==1)	
		{
					shRetVal = get_env("FWDLD", (char *)szFWDLDFlag, sizeof(szFWDLDFlag));

					if(shRetVal!=0 && szFWDLDFlag[0] == '1')
					{

						EXT_IFD_ESCRver(FwVerNo);
						
						
						clrscr();
						write_at("ESCR Ver. No. :", 15, 1, 1);
						write_at((char *)FwVerNo, strlen((char *)FwVerNo), 5, 4);
						shDisplayMessage(MSG_PRESS_ANY_KEY, 1,8);
						
						chGetKeyPress();
						clrscr();

						do
						{
							EXT_IFD_Scrversion((unsigned long *)&l2_ver,(unsigned long *)&bl_ver);
							
						}while((l2_ver==0) && (bl_ver==0));
						
						clrscr();
						printf("\nLayer 2 Ver : %ld",l2_ver);
						SVC_WAIT(2000);

						clrscr();
						printf("\nBoot Loader Ver : %ld",bl_ver);
						SVC_WAIT(2000);
						clrscr();

						printf("\nDownload will Start....");

						do
						{								
							ret=EXT_IFD_Scrdownload("layer2_v119_ftv01.bin","layer2_v119_ftv01.sig",callbackfn);
							if(ret != 0)
							{							
								clrscr();
								printf("\nDownload Failed : %d",ret);
								SVC_WAIT(2000);
								
								clrscr();
								printf("\nDownload will Restart....");
								SVC_WAIT(1000);
							}
						}while(ret!=0);
						
											
					}
					
					clrscr();
					printf("Please Wait....\n");
					SVC_wAIT((unsigned long)4000);	//VERIXPRTNG: Porting to Verix
					ushRetVal = EXT_IFD_Key_Introduction();
					
					if(ushRetVal != EXT_IFD_Success)
					{
						printf("Key Introduction Fail");
						return ushRetVal;
					}
					else
					   clrscr();
		}
#endif		
		// End change
#endif		

	//Added Purvin_p1 date 09/May/2007
	//Retrive environment variable for INTERAC.
	shRetVal = get_env("INTERAC", (char *)szInterac, sizeof(szInterac));
	if((shRetVal != 0) && (szInterac[0] != '0'))
		gshInterac = 1;

	//Added Purvin_p1 date 30/Apr/2008
	//UNATTENDED_TERMINAL: Retrive environment variable for UNATTENDED_TERMINAL
	//UNATTENDED: Commented the below code as Unattended/Attended variable 
	//will be set based on the TERMINAL TYPE
	/*
	memset(szInterac,0x00,sizeof(szInterac));
	shRetVal = get_env("UNATTENDED_TERMINAL", (char *)szInterac, sizeof(szInterac));
	if((shRetVal != 0) && (szInterac[0] != '0'))
	    gshUnAttended = 1;
	*/


    //   Code for displaying checksum calculation.
    // Added for displaying checksum calculation for different configuration

    res = inLoadMVTRec(0);  // Load the MVT REC 1 and read the RFU string 
    if (res != VS_SUCCESS) 
    {
            write_at("Error Loading",strlen("Error Loading"), 1, 1);
            write_at("MVT record", strlen("MVT record"),1, 2);
            SVC_wAIT((unsigned long)3000);   //VERIXPRTNG: Porting to Verix

    }
    else
    {
        memset(ucChkSumBuffer,0x00,sizeof(ucChkSumBuffer));
		//BUFSIZE
		//Buffer size check is not reqd we are copying sizeof(ucChkSumBuffer)bytes to ucChkSumBuffer
		memcpy (ucChkSumBuffer,szGetStringRFU1(),sizeof(ucChkSumBuffer));
        if (strlen((char*)ucChkSumBuffer) > 0)
		{
                write_at("CHECK SUM :", strlen("CHECK SUM :"),5,1);
				//Brabhu_H1 dated 22 June 2007
				//Fix made to display New modified checksums
                if (shDispModFlag)
	            {       			    
                    //write_at((char *)ucChkSumBuffer, 10, 5, 2);
                    write_at((char *)&ucChkSumBuffer, strlen((const char *)&ucChkSumBuffer[0]), 3, 2);
                    shDisplayMessage(MSG_PRESS_ANY_KEY, 1,4);
                }
				else
				{
                    //write_at((char *)ucChkSumBuffer, 10, 5, 4);
                    write_at((char *)&ucChkSumBuffer, strlen((const char *)&ucChkSumBuffer[0]), 3, 4);
                    shDisplayMessage(MSG_PRESS_ANY_KEY, 1,8);
				}
                chGetKeyPress();  // Display the checksum value and wait for a key press
        }

     // Terminal Type
	 //UNATTENDED:
	 //Mary_F1	14/11/07
	 //Pin Bypass is not allowed for unattended terminal types
	 ascii_to_bin(&btTermType, (byte *)szGetEMVTermType(), 2);
	 sUnattendTrmType = Is_Unattended_Terminal(btTermType);
	 if(sUnattendTrmType)
	 	{
	 		gshUnAttended = 1;			
	 	}		
    }

    
	vSetDefaultFunctionPointers();
	//BKSPENABLE:
//	usEMVSetBackSpaceKeyFnlty(EMV_TRUE);

	//Kishor_S1 dated 03 Aug 2009 added to set functionality if its a EC Txn
	//ECTXNFLW: Check for the presence of the EC txn limit env variable
	shRetVal = get_env("CHINAMOD", (char *)szChinaFlag, sizeof(szChinaFlag));
	if(szChinaFlag[0]=='1')
		{
			//Brabhu_H1 dated 10 October 2007
			//CHINAFTR: Added the support for China EMV Module feature.
			usEMVCN_Set_Functionality(APPL_SELECT);
		}

	//ECTXNFLW: Check for the presence of the EC txn limit env variable	
	//NAND_FLASH  : Config.sys File access changes
	shRetVal = get_env("ECTXNLIMIT", (char *)gszECTXLimitAmount, sizeof(gszECTXLimitAmount));

	
    ppPresent = 0;
#ifdef NONSECURE_PIN	
	
	#ifdef __thumb		//VERIXPRTNG: Porting to Verix	
	//NoPPSUP: PINPad is not supported for Vx700 terminals

		if(!shVx700Flag)
		{
			init = inGetPinPadInit();
		    if (init)
		        initPinPad();
		}
	#endif	
#endif

    res = inVXEMVAPSCInit();

	// 99_AID_SUPPORT
	if(res == TERM_AID_CNT_EXEED)
	{
	   vdPromptManager(res);
	   return EMV_FAILURE;
	}

	
	if(res==Vx700_NO_SUPPORT)
	{
		clrscr();
		write_at("Vx700 Not Supported", strlen("Vx700 Not Supported"), 1, 3);
		return 0;
	}
	
//NANDFLASH  Additions
	inVXEMVAPKeyRollOver();


//Ganesh_p1 : 10-Aug-11 : Ontime ID 55983 Power Mgmt changes
#if 0
	//Purvin_P1 date 12-May-08 Added to support temper from terminal
	
	//INVMOD:
	//Kishor_S1 on 14-10-09 commented the INVMOD changes as per the confimation mail			
/*	if(res == E_INVALID_MODULE)
    {
		vdPromptManager(res);
		return ;
    }	*/
	// Tisha_K1 08/July/09
	// Check for the return value SERVICE_ALARM_SET only in case of Vx 700

//VERIXPRTNG: Porting to Verix
#ifdef __thumb

		if((res == SERVICE_ALARM_SET) && (shVx700Flag))
		 {
			clrscr();
			printf("Service Alarm is Set\n");
			SVC_wAIT((unsigned long)1000);   //VERIXPRTNG: Porting to Verix
			ushRetVal = EXT_IFD_RESET_SERVICE_ALARM("67366377");
			if(ushRetVal != EXT_IFD_Success)
			{
				printf("Service Alarm Reset Failed\n");
				SVC_wAIT((unsigned long)2000);  //VERIXPRTNG: Porting to Verix
				return EMV_FAILURE;
			} 
			else
			{
				SVC_RESTART("");
	//			clrscr();	
			}
		 }
    else
#endif		
		if (res != SUCCESS)
        vdPromptManager(res);


#endif
	//UNATTENDED: Kishor_S1 added on 30-10-09 
	// Bypass_Flag should not be enabled for unattended terminals.
	if( gshUnAttended != 1)
		{
		
		    //t_arvind_g1 dated 17 Jan 07 added to get environ variable to enable offline PinBypass
		    //Soumya_G1 18/Jun/07
		    //Changed the Env Variable to BYPASS_PIN from BYPASS_OFFLINE_PIN
		    //This is for the changed requirement on the Bypass feature.
		    //Now if Any One pin is bypassed, then any consecutive pin should be bypassed.
			shRetVal = get_env("BYPASS_PIN", (char *)Bypass, sizeof(Bypass));//Soum_temp may be remove this var.
		    if(shRetVal != 0 && Bypass[0] == '1')
		        Bypass_Flag = 1;
		}

	//KEYREVCN: Pavan_K1 dated 08-Aug-07 added for key revocation.
	inVXEMVAPKeyRevocation();


	if(get_env("TIMER", (char *)ucEnvVar, sizeof(ucEnvVar)) > 0)
		ulTxnTime = atoi(ucEnvVar);		
	
	
    //A+AUTOFT: Introduced configurable Delay per transaction
    shRetVal = get_env("DELAY", (char *)szDelay, sizeof(szDelay));
    if(shRetVal != 0)
        uiDelay = atoi(szDelay);
    else
        uiDelay = 1000;


	//UPTMAGREAD
	//This is for testing mag read. This should be performed only when env. var.
	//UPTMAGCARD is enabled.
	shRetVal = get_env("UPTMAGCARD", (char *)szMagCard, sizeof(szMagCard));
	if((shRetVal != 0) && (szMagCard[0] != '0'))
	{
		while(1)
			{
				clrscr();
		        write_at("Mag Test", strlen("Mag Test"),1,1);
		       			
		        shDisplayMessage(MSG_YES, 19,1);
		        shDisplayMessage(MSG_NO, 19,3);

				key = inGetOption();
				if(ucAllowKeys == 1)
				{
				    if (key == 49)
				    	{
//VERIXPRTNG: Porting to Verix
#ifdef __thumb				    	
							//UNATNDMAGREAD

							if(shVx700Flag)
								vdMagStripeTrans();
							else
#endif								
							{
								clrscr();
    							shDisplayMessage(MSG_SWIPE_MAGNETIC_CARD, 1,1);    
      
    							while(!(magneticStripe(track2)==ROK));
						
    							clrscr();
    							inRunMgnStripeTransaction(track2);

								write_at("Track2 Data", strlen("Track2 Data"), 1, 1);
								write_at(track2, strlen(track2), 1, 2);
								shDisplayMessage(MSG_PRESS_ANY_KEY, 1,8);
	                            chGetKeyPress();

														
							}	
							continue;
				    	}
				    else
				        break;
				}
				else
				{
					if (key == 1)
						{
//VERIXPRTNG: Porting to Verix
#ifdef __thumb						
							//UNATNDMAGREAD

							if(shVx700Flag)
								vdMagStripeTrans();
							else
#endif								
							{
								clrscr();
    							shDisplayMessage(MSG_SWIPE_MAGNETIC_CARD, 1,1);    
      
    							while(!(magneticStripe(track2)==ROK));
						
    							clrscr();
    							inRunMgnStripeTransaction(track2);

								write_at("Track2 Data", strlen("Track2 Data"), 1, 1);
								write_at(track2, strlen(track2), 1, 2);
								shDisplayMessage(MSG_PRESS_ANY_KEY, 1,8);
	                            chGetKeyPress();

															
							}
							continue;			
						}
				    else
				        break;
				}	
		}//End of While
	}

	
	while (1)
    {
		//POWER_MGMT : Ganesh_P1 added on 6-01-11 for Power Management changes
		//INIT CARDSLOT AFTER CARD INSERTION
    		while(1)
    		{
	    		clrscr();
			shDisplayMessage(MSG_INSERT_CARD, 1,4);			 
			shDisplayMessage(MSG_PRESS_ANY_KEY, 1,6);		

			//A+AUTOFT: Check introduced to avoid user intervention	
	            if (gAPPFlag != 1)
			chGetKeyPress();

			iRet=inVXEMVAPInitCardslot();
			
			//Ganesh_p1 : 10-Aug-11 : Ontime ID 55983 Power Mgmt changes
#ifdef __thumb
			
			if((iRet == SERVICE_ALARM_SET) && (shVx700Flag))
			 {
				clrscr();
				printf("Service Alarm is Set\n");
				SVC_wAIT((unsigned long)1000);	 //VERIXPRTNG: Porting to Verix
				ushRetVal = EXT_IFD_RESET_SERVICE_ALARM("67366377");
				if(ushRetVal != EXT_IFD_Success)
				{
					printf("Service Alarm Reset Failed\n");
					SVC_wAIT((unsigned long)2000);	//VERIXPRTNG: Porting to Verix
					return EMV_FAILURE;
				} 
				else
				{
					SVC_RESTART("");
		//			clrscr();	
				}
			 }
			else
	#endif
		
			if(iRet != SUCCESS)
				 vdPromptManager(iRet);

			if(inVXEMVAPCardPresent() == SUCCESS)
				break;
			
			//Ganesh_p1 10-Aug-11 Bug fix for Ontime Id :55409 : Unbale to carry out Magstripe transaction
			else if(magneticStripe(tk2)==ROK)
				break;
			else
				inVXEMVAPCloseCardslot();
    		}

		
	        numScripts = 0;
	        pinSigRequired = 0;
	        res = 0;
		
			//Purvin_p1 date 14/May/2007 Reset Issuerscriptresult buffer to null at begining of transaction for interac.
			if(gshInterac > 0)
				issuerScriptResults=NULL;
	        //A+AUTOFT: Introduced configurable Delay per transaction
	        SVC_wAIT(uiDelay);

	#ifdef PERFORM_CMD
				ulStartTime = read_ticks();
	#endif	

	        memset(track2,0x00,sizeof(track2) );

			res = inRunEMVTransaction();

	#ifdef PERFORM_CMD
		        ulEndTime = read_ticks();
				clrscr();
	        	printf("Txn Ended\n");
	        	printf("ulENDTime=%ld\n ", ulEndTime);
				printf("ulStartTime=%ld \n ",ulStartTime);
				printf("ultTime=%ld \n", (ulEndTime-ulStartTime));
			    SVC_wAIT((unsigned long)3000);  //VERIXPRTNG: Porting to Verix
	#endif		
	        if ((res == TRANS_APPROVED) || (res == TRANS_DECLINED) || (res == OFFLINE_APPROVED) || (res == OFFLINE_DECLINED) || (res==OFFLINE_NOTALLOWED) || (res== NONEMV))		
	        {
			  //Purvin_P1 Date 29/04/08 
			  //UNATTENDED_TERMINAL:Commented for Not to support Print Receipt, It is not supported for Unattended terminal
			  //2CO.018	Print Receipt should be supported to print cardholder copy as "Print Cardholder"
			  //is "YES" for unattended terminals
			  //if(gshUnAttended != 1)
			  //{
	          	if(inGetPrintReceipt())
	               	vdPrintReceipt(res);
			  
			  //T_Gangadhar_S1 
			  resBatch = iGetPrintBatchFile();
			  if( resBatch == 1 || resBatch == 2)
			  {				
				iPrintBatchFile(resBatch);
			  }
			  
			}

			if(res == MAG_SUCCESS)	//Ontime ID: 55409
				goto JUMPHERE;
			
	            clrscr();
	            vdPromptManager(res);

	//VERIXPRTNG: Porting to Verix			
	#ifdef __thumb		    
	
				if(shVx700Flag)
					Terminate_CardSlot(0x10000001,SWITCH_OFF_CARD);
				else
	#endif				
					Terminate_CardSlot(1,SWITCH_OFF_CARD);


			
			//POWER_MGMT : Ganesh_P1 added on 6-01-11 for Power Management changes
			//CLOSE CARDSLOT AT END OF TRANSACTION
			iRet=inVXEMVAPCloseCardslot();

	            if ((res == TRANS_APPROVED) || (res == OFFLINE_APPROVED) )
	                bUpdateLogforSplitSales();
	            
	            if( bFallbackAllowed == EMV_TRUE )
	            {
					//CM.091	Transaction should fallback to magswipe after an ICC read failure
					//for offline only configuration
				    /* if((termTypeBuf[0] == 0x23) || (termTypeBuf[0] == 0x26)) //offline only terminals
					{
	                    bFallbackAllowed = EMV_FALSE;
	                    res = TRANS_DECLINED;
						//Brabhu_H1 dated 23 August 2007
						//NOPRNTRP: Fix made, not to do print reciept for fallback processing
	                    //if (inGetPrintReceipt())
	                    //    vdPrintReceipt(res);
					}
	                else
					{*/
					
					//Purvin_P1 Date 29/04/08 
					//UNATTENDED_TERMINAL: FALLback is not supported for UnAttended Terminal.
				      if(gshUnAttended != 1)
					{
	                    //A+AUTOFT: Check introduced to avoid user intervention	
	                    if (gAPPFlag != 1)
	                    {
	                    clrscr();
	                    shDisplayMessage(MSG_DO_YOU_WANT_TO, 1,1);
	                    shDisplayMessage(MSG_GO_FOR_FALLBACK, 1,2);    
	                    shDisplayMessage(MSG_YES, 19,1);
	                    shDisplayMessage(MSG_NO, 19,3);    
	                
	                    key = inGetOption();
	                    }			// for ONTIME id 22518 T_gangadhar_s1  24/April/08	support to work all function keys for all Vx terminals

	                    else if(ucAllowKeys == 1)
	                    	key = 50;
						else
							key = 2;
						
						//Ontime ID :36562
						//Fix for the issue that, the terminal should allow fallback if the card returns
						//invalid ATR
	                    //if (key == 49)
	                    //if (key == 1)
	                    //Kishor_S1 modified the below if condtion to work without function keys on 14-12-09
	                    if ((key == 1) || ((ucAllowKeys == 1) && (key == 49)) )
						{
	                        clrscr();
	                        shDisplayMessage(MSG_SWIPE_MAGNETIC_CARD, 1,1);    
	                    
	                        while(!(magneticStripe(track2)==ROK));
							
	                        clrscr();
	                        inRunMgnStripeTransaction(track2);
//Ontime ID: 55409
JUMPHERE:
							//CM.091	Transaction should fallback to magswipe after an ICC read failure
							//for offline only configuration						
							if((termTypeBuf[0] != 0x23) && (termTypeBuf[0] != 0x26)) //offline only terminals
							{
							
		                        size = inBuildFallbackMsg(msg);

								//MAGTRANS
		                        shindex = 0;
								hostDecision = inSendToHost(msg, resp, size);
								while ((hostDecision == FAILED_TO_CONNECT) && (shindex < 2))
								{
									clrscr();
		        					shDisplayMessage(MSG_FAILED_TO_CONNECT, 1,1);  
									SVC_wAIT((unsigned long)4000);  //VERIXPRTNG: Porting to Verix
		                           	hostDecision = inSendToHost(msg, resp, size);
									shindex++;
								}
		                        if(hostDecision == FAILED_TO_CONNECT)
								{
		                            clrscr();
		                            shDisplayMessage(MSG_FAILED_TO_CONNECT, 1,1);    
		                            shDisplayMessage(MSG_PRESS_ANY_KEY, 1,8);
		                            chGetKeyPress();
		                        }
		                        
		                        if(hostDecision == HOST_AUTHORISED)
		                             res = TRANS_APPROVED;
		                        else
		                             res = TRANS_DECLINED;

														
							}
							else
							{
								res = OFFLINE_APPROVED;
								
							} 
							
							if (inGetPrintReceipt())
	                            vdPrintReceipt(res);  
	                                           
							
						}
				      }
					//}
	                bFallbackAllowed = EMV_FALSE ;
	                clrscr();	   
	                shDisplayMessage(MSG_PRESS_ANY_KEY, 1,8);            
	                
	            }
	            
	            //A+AUTOFT: Check introduced to avoid user intervention	
	            if (gAPPFlag != 1)
	            chGetKeyPress();
	            
	            // Re initialising to the original configuration.
	            gstLangConfig.szActiveLanguage = gstLangConfig.szDefaultlanguage;
	            
	            if(gstLangConfig.szActiveLanguage == '0')
	                shLoadDSPMsgFile(MSG_ENG_FILENAME);
	            else
	                shLoadDSPMsgFile(MSG_SPN_FILENAME);
	    }
}


/*-------------------------------------------------------------------------
    Function:		initPinPad
    Description:	To Intialise the external PinPad
    Parameters:		void
    Returns:		
	Notes:
--------------------------------------------------------------------------*/

void initPinPad(void)
{
    clrscr();
    shDisplayMessage(MSG_PLEASE_WAIT,1,1);    
    
    if (inEMVPrintPP("PINPAD READY") != ROK)
    {
        clrscr();
        shDisplayMessage(MSG_PINPAD_NOT_PRESENT,1,1);    
        shDisplayMessage(MSG_PRESS_ANY_KEY, 1,2);    
        
        chGetKeyPress();
    }
    else
    {
        clrscr();
        shDisplayMessage(MSG_ENTER_KEY,1,1);    
        shDisplayMessage(MSG_DIVERSIFICATION_VALUE, 1,2);    
        
        inEMVBuildCK((unsigned char*) "1234", CKclear, CKencr, &masterKeyIndex);
        ppPresent = 1;
        clrscr();
        shDisplayMessage(MSG_PINPAD_INITIALISED,1,1);    
        shDisplayMessage(MSG_PRESS_ANY_KEY, 1,2);    
        
        chGetKeyPress();
    }
}

/*-------------------------------------------------------------------------
    Function:		inRunMgnStripeTransaction
    Description:	Retrieve the data from Magnetic Stripe for processing Track2 data
    Parameters:		char *
    Returns:		SUCCESS, FAILURE, TRANS_CANCELLED
	Notes:
--------------------------------------------------------------------------*/

int inRunMgnStripeTransaction(char* track2)
{
    char             pan[20] = {0};
    char             valid[4+1] = {0};
    char             service[3+1] = {0};
    unsigned char    buffer_hex[50] = {0};  
    unsigned char    Tag57[50] = {0};
    short            shindex = 0;
    short            iLenPan = 0;
    byte             transType = 0;
    unsigned long    amount = 0;
    unsigned short   result = 0;
    
    memset(pan, 0, 20);
    memset(valid, 0, 5);
    memset(service, 0, 4);
    for(shindex=0; shindex<50; shindex++)
    {
        if(track2[shindex]==0) break;
        if(track2[shindex]=='=') break;
    }
    if(shindex>0)
    {
        strncpy(pan, &track2[0], shindex);
        iLenPan = shindex;
        strncpy(valid, &track2[shindex+1], 4);
        strncpy(service, &track2[shindex+5], 3);
        
        strncpy((char *)Tag57, &track2[0], shindex);    //  PAN
        Tag57[shindex] = 0xD;                   //  Field Separator 
        strncpy((char *)&Tag57[shindex+1], &track2[shindex+1], 4);   //  Expiry date
        strncpy((char *)&Tag57[shindex+5], &track2[shindex+5], 3);   //  Service Code
        
        for(shindex = shindex+6 ; shindex<50; shindex++)                  //  Discretionary Data (Card Sopecific)
        {
            if(track2[shindex]==0x3F) break;
            if(track2[shindex]== 0) break;
            
            Tag57[shindex] = track2[shindex];
            
        }

		if(((service[0]=='2') || (service[0]=='6')) && ( bFallbackAllowed != EMV_TRUE ))
			return(USE_CHIP);
			
		//CM.091	Transaction should fallback to magswipe after an ICC read failure
		//for offline only configuration
		bFallbackAllowed = EMV_FALSE ;
        ascii_to_binary(buffer_hex, (unsigned char *) pan, iLenPan);
        usEMVUpdateTLVInCollxn(0x5A00, buffer_hex , iLenPan/2);
        ascii_to_binary(buffer_hex, (unsigned char *)"A000000004", 10);
        usEMVUpdateTLVInCollxn(0x4F00, buffer_hex, 5);
        transType = inSetTransactionType();
        if (transType == TRANS_CANCELLED)
            return (TRANS_CANCELLED);
        
        if (shindex % 2)
        {
            Tag57[shindex] = 0xF;
            shindex++ ;
        }
        
        ascii_to_binary(buffer_hex, (unsigned char *) Tag57, shindex);
        usEMVUpdateTLVInCollxn(0x5700, buffer_hex , shindex/2);
        usEMVUpdateTLVInCollxn(0x9c00, (byte *) &transType, 1);
		//Brabhu_H1 dated 14 Jun 2007
		//FLBKAMT: Fix for onTime ID 12177
		//Request for amount during a fallback
        amount = AMT_REQD;
        //added 
        usEMVUpdateTLVInCollxn(TAG_9F39_POS_ENTRY_MODE, (byte *)"\x80", 1);
        result=usAmtEntryFunc(&amount);
        if (result == TRANS_CANCELLED)
            return (TRANS_CANCELLED);
        
        usEMVUpdateTLVInCollxn(TAG_81_AMOUNT_AUTH, (byte *) &amount, 4);
		//Brabhu_H1 dated 14 Jun 2007
		//SPLTAMT:Fix made to have 4 byte unsigned represention, hence changed from %ld to %lu
        sprintf(pan, "%012lu", amount);
        ascii_to_binary(buffer_hex, (byte *) pan, 12);
        usEMVUpdateTLVInCollxn(0x9f02, (byte *) buffer_hex, 6);
        return(SUCCESS);
        
    }
    
    return(FAILURE);
}


/*-------------------------------------------------------------------------
    Function:		inRunEMVTransaction
    Description:	To process an EMV Transaction 
    Parameters:		void
    Returns:		TRANS_APPROVED, TRANS_DECLINED, OFFLINE_APPROVED, OFFLINE_DECLINED, 
	                OFFLINE_NOTALLOWED, NONEMV
	Notes:
--------------------------------------------------------------------------*/

int inRunEMVTransaction(void)
{
    unsigned short      result = 0;
	unsigned short      usRes  		= 0;
	unsigned short      usAccLen 	= 0;
	unsigned short      tagLen = 0;
	unsigned long       ulAmt = 0L;
	//unsigned long       ulIDVal = 0L;
	unsigned long       ulCashbackAmt = 0L;
    unsigned short      aidcount = 0;
    unsigned short      blockAidCount;
	short               terminalRecord = 0;
    short               autoFlag = 0;
    short               terminalDecision = 0;
    byte                cryptInfoData = 0;
    byte                transType = 0;
    byte                authRespbuf[2] = {0};
	byte                AccountType =0;
	byte                ucAmt[16] = {0};
    //char                track2[100] = {0};
    char                serialBuf[15] = {0};
	//Brabhu_H1 dated 08 July 2008
	//ECTXNFLW: Added the szECAuthCode to store the value part
	char                szECAuthCode[7] = {0};
	// 99_AID_SUPPORT
    srAIDListStatus     paidStatus[MAX_AID_LIST];			//Ontime ID: 36560
    char                tmp[13]           = {0};
    unsigned char       ucharBuf[6]       = {0};
    unsigned short      len 				 = 0;
	EMVResult           emvRet        = EMV_SUCCESS;
	EMVResult           amountReqd    = EMV_FAILURE;
	EMVResult           cashbackReqd  = EMV_FAILURE;
    //char               *AccountTypeLabels_menu[] = {"Default", "Savings", "Cheque or Debit", "Credit"};
	//Added For Interac
	unsigned long ulAmount=0;
    unsigned short      usDefaultRecordRFU1	= 0;
	char tempBuffer[32] = {0}, tempBufferStr[32] = {0};
	byte    ucData[2]							 = {0};//Ontime ID: 61519
	unsigned short  usLen						 = 0;
	short shMagRetVal = 0;//Ontime ID: 55409


    memset(serialBuf, 0x00 ,sizeof(serialBuf) );
    clrscr();
    
    if((result = inVXEMVAPTransInit(terminalRecord,inGetPTermID)) != SUCCESS)
        return (result);

	//NOGPOINSEL : kishor_S1 added on 14-7-09 for GPO & App selection separation changes
	usDefaultRecordRFU1 = inGetShortRFU1(); 
    
	//POWER_MGMT : Ganesh_P1 commented on 6-01-11 for Power Management changes
    //shDisplayMessage(MSG_INSERT_CARD, 1,1);
    
    //Purvin_P1 : date 14-03-08 Added wait statement
    //SVC_WAIT(10);
    
    vdFlushKeyboard();
	
    while(inVXEMVAPCardPresent() != SUCCESS)
	{
		//Ganesh_p1 10-Aug-11 Bug fix for Ontime Id :55409 : Unbale to carry out Magstripe transaction
	        //if(magneticStripe(track2)==ROK)
	        {
				//Ontime ID: 55409	Vx EMV 6.0.0
				shMagRetVal = inRunMgnStripeTransaction(tk2);

				if(shMagRetVal == SUCCESS)
				{
					write_at("Track2 Data", strlen("Track2 Data"), 1, 1);
					write_at(tk2, strlen(tk2), 1, 2);
					shDisplayMessage(MSG_PRESS_ANY_KEY, 1,8);
	            	chGetKeyPress();
					return(MAG_SUCCESS);	//Ontime ID: 55409
				}

				return(shMagRetVal);

			}
   	}
	ulTimerStart = ulTimerEnd = 0;
	ulTimerStart = read_ticks();
	transType = inSetTransactionType();
    if (transType == TRANS_CANCELLED)
        return (TRANS_CANCELLED);

	if(ulTxnTime)
	{
		ulTimerEnd = read_ticks();
		if((ulTimerEnd-ulTimerStart)> (ulTxnTime * 1000))
			return(TRANS_TIMED_OUT);
	}
	
	//Pavan_K1 dated 14-May-07 added to display the please wait messg.
	clrscr();
  	shDisplayMessage(MSG_PLEASE_WAIT, 1,1);  
    
    //Indicates the type of financial transaction . RAVI_B3
    usEMVAddTLVToCollxn(0x9c00, (byte *) &transType, 1);

//Fix for Ontime 61475: Environment variable have to read once from config.sys file
// Moved the code to main and useing global array 
//	result = SVC_INFO_SERLNO (serialBuf) ;
/*	 
	 if(!(isalnum(serialBuf[0])))
	 {
		 //Purvin_p1 date 07/Mar/2007 DEFECT # 6763
		 //serialBuf was padded with 0xFF, so before retriving TERMSERNUM from terminal.
		 //we should set whole buffer to 0x00
		 memset(serialBuf,0x00,sizeof(serialBuf));
		 result = get_env("TERMSERNUM", serialBuf, sizeof(serialBuf));
		 //BUFSIZE
		 //Buffer size check is not reqd we are copying sizeof(serialBuf) bytes to serialBuf
		 if (result == 0)
			 memcpy(serialBuf, "10102397", sizeof(serialBuf));  		 
	 }*/	

	 memcpy(serialBuf,gserialBuf,sizeof(serialBuf));	 
	 usEMVAddTLVToCollxn(TAG_9F1E_IFD_SER_NUM , (byte *)serialBuf, 8);

    //Brabhu_H1 dated 24 October 2007
	//FALLBKFX: Fix made to avoid fallback for offline configuration
	memset(termTypeBuf, 0x00, sizeof(termTypeBuf));
    usEMVGetTLVFromColxn(TAG_9F35_TERM_TYPE , termTypeBuf, &tagLen);
    
    if ((result = inVXEMVAPCardInit()) != SUCCESS)
    {
        //Purvin_p1 date 18/Jun/07
        //if we get chip error or Invalid ATR from library then we should prompt for fallback.
        if((result == NO_ATR) || (result==CHIP_ERROR) || (result==INVALID_ATR))
            bFallbackAllowed = EMV_TRUE ;
        return (result);
    }
    
	// Begin EMV transaction
    // Get the Auto APP selection flag from the default table. 
    autoFlag = inGetAutoSelectApplnFlag() ;   

	if(ulTxnTime)
	{
		ulTimerEnd =0;
		ulTimerEnd = read_ticks();
		if((ulTimerEnd-ulTimerStart)> (ulTxnTime * 1000))
			return(TRANS_TIMED_OUT);
	}
	
	//Pavan_K1 dated 14-May-07 commented the below code as it is handled in the
	//Amount entry function.
	#if 0
		clrscr();
		write_at("    Account Type   ",strlen("    Account Type   "),1,3);
		write_at("      Selection    ",strlen("      Selection    "),1,4);
		SVC_wAIT(2000);

		//Purvin_p1 date 10/May/2007
		//Display only  "Savings","Cheque" for Interac.
		if(gshInterac > 0)
		{
			key = inMenuFunc(AccountTypeLabels_menu_Interac, 2);
		}
		else
		{
			key = inMenuFunc(AccountTypeLabels_menu, 4);
			
		}
		switch(key)
		{
		   case 1:
			   AccountType = 0x00;  //Default Unspecified.
			   break;
		   case 2:
			   AccountType = 0x10;  //Savings 
			   break;
		   case 3:
			   AccountType = 0x20;  //Cheque/Debit
			   break;
		   case 4:
			   AccountType = 0x30;  //Credit
			   break;
		   case TRANS_CANCELLED:
			   return (TRANS_CANCELLED);
			   
		}

		clrscr();
		shDisplayMessage(MSG_PLEASE_WAIT, 1,1);  

		usEMVAddTLVToCollxn (TAG_5F57_ACCOUNT_TYPE, (unsigned char *)&AccountType, 1);
	#endif
	//Brabhu_H1 dated 24 October 2007
    //FALLBKFX: Fix made to avoid fallback for offline configuration
	//memset(termTypeBuf, 0x00, sizeof(termTypeBuf));
    //usEMVGetTLVFromColxn(TAG_9F35_TERM_TYPE , termTypeBuf, &tagLen);

	//Purvin_p1 date 15/June/2007
	//Fix made for Ontime Id : 12784
	// Calling Amount Entry function  and updating to collxn.
	if( gshInterac > 0 )
	{
	        ulAmount = AMT_REQD;
	        result=usAmtEntryFunc(&ulAmount);
                  if (result == TRANS_CANCELLED)
                       return (TRANS_CANCELLED);
	        usEMVAddAmtToCollxn(ulAmount);
	}
    //End Change


	result = inVXEMVAPSelectApplication(autoFlag, inMenuFunc, vdPromptManager, usAmtEntryFunc, NULL);
    //if ((result = inVXEMVAPSelectApplication(autoFlag, inMenuFunc, vdPromptManager, usAmtEntryFunc, NULL)) != SUCCESS)
	
	//NOGPOINSEL : kishor_S1 added on 14-7-09 for GPO & App selection separation changes
	//Call Perform GPO if Bit 8 of RFU1 field is set
	//Kishor_S1 on 28-9-09 : Fix for Ontime 36823 Tranasaction continuing after cancel key is pressed.	
	if( (usDefaultRecordRFU1 & 0x0080) && (result == SUCCESS) )
	{
		result = inVXEMVAPPerformGPO();
	}

    if (result != SUCCESS)
    {

        if(result == CANDIDATELIST_EMPTY)
        {
			//Ontime ID: 36560
			
			// 99_AID_SUPPORT
			memset(paidStatus, 0x00, (sizeof(srAIDListStatus) * MAX_AID_LIST));
			aidcount = MAX_AID_LIST;
			usEMVGetAllAIDStatus(paidStatus, &aidcount, &blockAidCount);
			if (blockAidCount > 0)
                result = APPL_BLOCKED ;
        }
		//Changed By Purvin_p1 date: 14-Feb-2007
		//Fix made foe OntimeDefect ID : 5085
		//If card returns application block then we should not fallback.
        if((result != CARD_BLOCKED) && (result != CARD_REMOVED)&&(result != TRANS_CANCELLED) &&(result != INVALID_PARAMETER) && (result !=APPL_BLOCKED))
        	bFallbackAllowed = EMV_TRUE ;

		if(usEMVGetKernelTags(TAG_DF09_LAST_STATUS_WORD, ucData, &usLen) == EMV_SUCCESS)//Ontime ID: 61519
        {
			if(memcmp(ucData,"\x69\x85",2)==0)
				return(NOT_ACCEPTED);
        }
        
		
        return (result);
    }

	if(ulTxnTime)
	{
		ulTimerEnd =0;
		ulTimerEnd = read_ticks();
		if((ulTimerEnd-ulTimerStart) > (ulTxnTime * 1000))
			return(TRANS_TIMED_OUT);
	}
	
    clrscr();
    shDisplayMessage(MSG_PLEASE_WAIT, 1,1);  
    
	// Tisha_K1 - 25/Mar/2010
	// SU76 : Language selection should be done before GPO. A function pointer is set in the EMV Module which will 
	// invoke the callback for language selection.
    //vdSelPreferredLang();
    clrscr();
    
    shDisplayMessage(MSG_PROCESSING, 1,1);
    
    inVXEMVAPGetCardConfig(-1,-1 );
    
    // Read application data from card
    if ((result = inVXEMVAPProcessAFL()) != SUCCESS)
    {
        bFallbackAllowed = EMV_TRUE ;
          //ONTIME 22405/21368 kishor_s1 added on 25-04-08 for CARD BLOCK error and Not Accepted
		if(result == APPL_BLOCKED || result == CARD_BLOCKED || result == 0x6985)
		{
            return (EMV_FAILURE);
		}
        return (result);
    }
/*
	//Brabhu_H1 dated 08 July 2008
    //ECTXNFLW: Check for the Value of 9F7A Tag
    usEMVGetTLVFromColxn(TAG_9F7A_EC_TERM_SUPPORT_IND, (byte* )&ucharBuf[0], &tagLen);
	
    //ECTXNFLW: Check for the presence of 9F74 Tag
    emvRet = usEMVGetTLVFromColxn(TAG_9F74_EC_ISSUER_AUTH_CODE, (byte* )&szECAuthCode[0], &tagLen);

    //ECTXNFLW: Issue Get Data CMD's if EC Issuer Auth Code Tag is present
    if ((emvRet == EMV_SUCCESS) && (tagLen== 6) && (ucharBuf[0] == 1))
    {
       	emvRet = usGetECData();
		
    }
	*/
    // Data authentication
    //Accepting amount after data authentcation if amount is not part of PDOL.
    //If this has to be done before data auth, then application needs to handle this accordingly.

    // Tisha_K1: 12/July/07 - If the amount / cashback /account type are not present in the collection
    // and if they are required, then prompt for the same
	// AMTCLBK:
    amountReqd = usEMVGetTLVFromColxn(TAG_9F02_AMT_AUTH_NUM, (byte *)&ulAmt, &tagLen);

    usEMVGetTLVFromColxn(TAG_9C00_TRAN_TYPE, &transType, &len);

	if(SALE_WITH_CASHBACK == transType)
	    cashbackReqd = usEMVGetTLVFromColxn(TAG_9F03_AMT_OTHER_NUM, (byte *)&ulCashbackAmt, &tagLen);
	else
		cashbackReqd = EMV_SUCCESS;
	
	usRes = usEMVGetTLVFromColxn(TAG_5F57_ACCOUNT_TYPE, (byte *)&AccountType, &usAccLen);


    // AMTCLBK: If the amount and the cashback amount are not present
	if((EMV_SUCCESS != amountReqd) && (EMV_SUCCESS != cashbackReqd))
    {
       if(EMV_SUCCESS != usRes)
        ulAmt = AMT_CASHBACK_ACCT_TYPE_REQD;
	   else
	   	ulAmt = AMT_CASHBACK_REQD;
	   
		//Fix for Ontime ID #13808. Handling the return value of the function.
		emvRet = usAmtEntryFunc(&ulAmt);
		if (emvRet != EMV_SUCCESS)
			return (emvRet);
	}

	// AMTCLBK: If the amount is not present
	else if (EMV_SUCCESS != amountReqd)
	{	
    	if(EMV_SUCCESS != usRes)
	      ulAmt =  AMT_ACCT_TYPE_REQD;
		else
		  ulAmt =  AMT_REQD;
		
		//ulIDVal = ulAmt;	
		
		//Fix for Ontime ID #13808. Handling the return value of the function.
		emvRet = usAmtEntryFunc(&ulAmt);
		if (emvRet != EMV_SUCCESS)
			return (emvRet);

		// Update amount to the collection		
	    //Update amount - binary
        usEMVUpdateTLVInCollxn(TAG_81_AMOUNT_AUTH, (byte *) &ulAmt, 4);

		//Update amount -numeric
		memset(tmp,0x00,sizeof(tmp));
		sprintf(tmp, "%012lu", ulAmt);
		ascii_to_binary(ucharBuf, (byte *) tmp, 12);
		usEMVUpdateTLVInCollxn(TAG_9F02_AMT_AUTH_NUM, (byte *) ucharBuf, 6);		

	}
	
	// AMTCLBK: If the cashback amount is not present
	else if(EMV_SUCCESS != cashbackReqd)
    {
    	if(EMV_SUCCESS != usRes)
          ulAmt = CASHBACK_ACCT_TYPE_REQD;
		else
		  ulAmt = CASHBACK_REQD;

		//ulIDVal = ulAmt;
		
		//Fix for Ontime ID #13808. Handling the return value of the function.
		emvRet = usAmtEntryFunc(&ulAmt);
		if (emvRet != EMV_SUCCESS)
			return (emvRet);
		
		// Update cashback amount to the collection
	   //Update cashback amount - binary
		usEMVUpdateTLVInCollxn(TAG_9F04_AMT_OTHER_BIN, (byte *) &ulAmt, 4);
		
		//Brabhu_H1 dateed 14 Jun 2007
		//SPLTAMT:Fix made to have 4 byte represention, hence changed from %u to %lu
		//Purvin_p1 31/May/2007  Fix made for Clerify ID 070530-5.
		//change %ld to %u for unsigned long.
		memset(tmp,0x00,sizeof(tmp));
		memset(ucharBuf, 0x00, sizeof(ucharBuf));
		sprintf(tmp, "%012lu", ulAmt);
		ascii_to_binary(ucharBuf, (byte *) tmp, 12);
		usEMVUpdateTLVInCollxn(TAG_9F03_AMT_OTHER_NUM, (byte *) ucharBuf, 6);	

		//Pavan_K1 dated 18-Jul-07: Update the 9F02 tag with the cash back amount
		emvRet = usEMVGetTLVFromColxn(TAG_81_AMOUNT_AUTH, (byte *)&ucAmt, &tagLen);
		if (emvRet == EMV_SUCCESS)
		{
			ulAmount = hexToUlong(&ucAmt[0], tagLen);	
			if (ulAmount > 0)
				usEMVAddAmtToCollxn (ulAmount + ulAmt);
		}
	}
	else if(EMV_SUCCESS != usRes)
	{
		ulAmt = ACCT_TYPE_REQD;
		//ulIDVal = ulAmt;
		
		//Fix for Ontime ID #13808. Handling the return value of the function.
		emvRet = usAmtEntryFunc(&ulAmt);
		if (emvRet != EMV_SUCCESS)
			return (emvRet);
	}
	
	if(ulTxnTime)
	{
		ulTimerEnd =0;
		ulTimerEnd = read_ticks();
		if((ulTimerEnd-ulTimerStart) > (ulTxnTime * 1000))
			return(TRANS_TIMED_OUT);
	}
	
	//Kishor_S1 moved the code below on 16-4-10 for fixing the GETDATA command issue
	//Brabhu_H1 dated 08 July 2008
    //ECTXNFLW: Check for the Value of 9F7A Tag
    usEMVGetTLVFromColxn(TAG_9F7A_EC_TERM_SUPPORT_IND, (byte* )&ucharBuf[0], &tagLen);
	
    //ECTXNFLW: Check for the presence of 9F74 Tag
    emvRet = usEMVGetTLVFromColxn(TAG_9F74_EC_ISSUER_AUTH_CODE, (byte* )&szECAuthCode[0], &tagLen);

    //ECTXNFLW: Issue Get Data CMD's if EC Issuer Auth Code Tag is present
    if ((emvRet == EMV_SUCCESS) && (tagLen== 6) && (ucharBuf[0] == 1))
    {
       	emvRet = usGetECData();
		
    }
    
	//Purvin_p1 date 30/Apr/2007 defect ID 8876
	//call  inVXEMVAPDataAuthentication after usAmtEntryFunc so internal auth can have amount value(0x9F02).
	// Data authentication
    if ((result = inVXEMVAPDataAuthentication(NULL)) != SUCCESS)
        return (result);
    
    // EMV processing restrictions and terminal risk management
	result = inVXEMVAPProcRisk(bIsCardBlackListed);
	
    //if ((result = inVXEMVAPProcRisk(bIsCardBlackListed)) != SUCCESS)
	if (result != SUCCESS)
        return (result);

    // Cardholder verification
	//t_arvind_g1 dated 28 Feb 07 Added to set pinparamwters for Secure Pin,
	//when getusrpin returns E_SECURE_PIN
#ifdef __thumb
	#if _SECURE_PIN
	vSetPinParams();//SECURE_PIN_MODULE changes
	inVXEMVAPSetDisplayPINPrompt(&displayPINPrompt);
	#endif
	#endif
    //t_arvind_g1 dated 17 Jan 07 
    //added as per new requ. to bypass the offline pin 
    //flag is reset
    shPin_Bypass = 0;

	if(ulTxnTime)
	{
		ulTimerEnd =0;
		ulTimerEnd = read_ticks();
		if((ulTimerEnd-ulTimerStart) > (ulTxnTime * 1000))
			return(TRANS_TIMED_OUT);
	}

	//BKSPKEY:
	result = inVXEMVAPVerifyCardholder(NULL);

	//BKSPKEY:
	if(result == E_USR_BACKSPACE_KEY_PRESSED)
		result = inVXEMVAPVerifyCardholder(NULL);
				
	
    if (result!= SUCCESS)
    {
		//Ontime 22666 fix
	   	result = EMV_FAILURE;
	    return (result);
    }

	if(ulTxnTime)
	{	
		ulTimerEnd =0;
		ulTimerEnd = read_ticks();
		if((ulTimerEnd-ulTimerStart) > (ulTxnTime * 1000))
			return(TRANS_TIMED_OUT);
	}
    
    // Terminal action analysis and first generate application cryptogram
     //FORCE_DECLINE
    //t_arvind_g1 dated 08 Jan 07 Added as per new requi. to decline the transaction before GEN AC
    if((terminalDecision = inGetTermDecisnOnForceDecline()) == 0)
	{
	    if((termTypeBuf[0] == 0x23) || (termTypeBuf[0] == 0x26) || (termTypeBuf[0] == 0x36)) //offline only terminals
            terminalDecision = 0;
	    else
		{		 
            if(terminalDecision == 0)
			{
				if(!(gshInterac > 0) && (gshUnAttended != 1))
				      terminalDecision = inGetTerminalDecision();

			}
		}
	}

	if(ulTxnTime)
	{
		ulTimerEnd =0;	
		ulTimerEnd = read_ticks();
		if((ulTimerEnd-ulTimerStart) > (ulTxnTime * 1000))
			return(TRANS_TIMED_OUT);
	}


	//Store sensitive tags for printing batch data.
	//Clearing the global variables before updating from collection 
		usStoreBatchData(); 

    if ((result = inVXEMVAPFirstGenerateAC(terminalDecision)) != SUCCESS)
    {
		//Ontime : 18990
		//The condition checking "result == CARD_BLOCKED" was added for Ontime issue 18990, So that "card blocked" msg should
		//not be shown when card returns 6A81 during GenAC
		//if(result == APPL_BLOCKED)
        //ONTIME 22405 kishor_s1 added on 25-04-08 for CARD BLOCK error
        //ONTIME 21359 6985 condition return failure added by kishor_s1 on 8-05-08
        //SWRETURN: Brabhu_H1 Dated 03 April 2008
	    //Changes made to return the status words
		//if(result == APPL_BLOCKED || result == CARD_BLOCKED)
		if((result == 0x6283) || (result == 0x6A81)|| (result == 0x6985))
		{
			//Change by purvin_p1 
            //bFallbackAllowed = EMV_TRUE ;
            //Ontime ID: 36560
            //Fix for the issue that, the transaction should fallback to magswipe if Gen AC returns
            //6985, incase of attended terminals (other than Vx700)
            if((result == 0x6985) && (!shVx700Flag))
				bFallbackAllowed = EMV_TRUE ;
				
			return (EMV_FAILURE);
				
		}
		//Soumya_G1 CDA fixes. 30/Jan/07 TC# 2CM.042.06 
		//If the Combined CDA fails after first Gen AC & the ICC decision is ARQC,
	    //then send an immediate Second GenAC to decline the Txn without connecting to the host.
		else if(result == E_CDA_ARQC_FAILED)
		{
            inVXEMVAPSecondGenerateAC(DECLINE_CDA_ARQC_FAILED);
			//BATCHDFX: Brabhu_H1 dated 25 July 2008
			//Fix made to avoid duplication of batch data
			 //T_Gangadhar_S1 :BATCH_DATA_CAPTURE  26-05-08
			//iBatchCapture();
		}
		//Brabhu_H1 dated 20 June 2007
		//FLBK6985: To allow fallback for a card response 6985
		//Purvin_p1 23-Feb-07 
		// in firstgenac if it returns other than 9000 then it returns emv_failure.
		//ex if it returns 6A81 or 6983 then it should display emv failure on terminal.
		else if (((result == TRANS_APPROVED) || (result == TRANS_DECLINED) || (result == OFFLINE_APPROVED) \
					|| (result == OFFLINE_DECLINED) || (result == OFFLINE_NOTALLOWED) || (result == NONEMV)))		
		{
		if( ((result == TRANS_APPROVED) || (result == TRANS_DECLINED) || (result == OFFLINE_APPROVED)\
				|| (result == OFFLINE_DECLINED) ))
			{
			//T_Gangadhar_S1   15-5-08 BATCH_DATA_CAPTURE: 
				iBatchCapture();
			}
			  return (result);
		}
		else
		{
            bFallbackAllowed = EMV_TRUE ;
            return (result);
		}
    }

	
    
    
    // Make decision based on cryptogram information data returned by ICC
    if ((result = usEMVGetTLVFromColxn(0x9f27, &cryptInfoData, &tagLen)) != EMV_SUCCESS)
        return (result);
    
    usEMVGetTLVFromColxn(TAG_8A_AUTH_RESP_CODE , authRespbuf, &tagLen);
    
     if(memcmp(authRespbuf, AAC_GEN1AC_DECLINE, 2)==0)		//AAC requested, CID doesn't matter
	 {
	 //T_Gangadhar_S1   15-5-08 BATCH_DATA_CAPTURE: 
	 	iBatchCapture();
        return(OFFLINE_DECLINED);					//to make sure card is not tampered
     }
	 switch(cryptInfoData & 0xC0)
    {
    case 0:
        if((cryptInfoData & 0x07) == 0x01)		//Service not allowed
            return (OFFLINE_NOTALLOWED);
        else
        	{
        	//T_Gangadhar_S1   15-5-08 BATCH_DATA_CAPTURE: 
        	iBatchCapture();
            return (OFFLINE_DECLINED);
        	}
    case 0x40:
        if((cryptInfoData & 0x07) == 0x01)		//Service not allowed
            return (OFFLINE_NOTALLOWED);
        else
        	{
        	//T_Gangadhar_S1   15-5-08 BATCH_DATA_CAPTURE: 
        	iBatchCapture();
            return(OFFLINE_APPROVED);
        	}
    case 0xC0:
            return(BAD_DATA_FORMAT);
    case 0x80:
        if((cryptInfoData & 0x07) == 0x01)		//Service not allowed
            return (OFFLINE_NOTALLOWED);

        cryptInfoData = processOnline();
        if(cryptInfoData == BAD_DATA_FORMAT)
		{
            bFallbackAllowed = EMV_TRUE ;
			//Brabhu_h1 dated 16 Aug 2007
			//BL54TRME: Changes are made as per bulletin #54
			//Changes made to terminate the transaction when mandatory DO's
			//are missing, by checking the return values
		    //return  (BAD_DATA_FORMAT);
            return  (EMV_FAILURE);
		}

        if((cryptInfoData & 0x07) == 0x01)		//Service not allowed
            return (OFFLINE_NOTALLOWED);
		//T_Gangadhar_S1   15-5-08 BATCH_DATA_CAPTURE:  26-05-08
			iBatchCapture();
        if ((cryptInfoData & 0xC0) == 0x40)          // TC - approved
		{
			return (TRANS_APPROVED);
        }
      	else
   		{
            return (TRANS_DECLINED); //if result is not AAC -> card tampered
      	}
    }
   	
    
    return (SUCCESS);
}


/*-------------------------------------------------------------------------
    Function:		processReferral
    Description:	
    Parameters:		unsigned char
    Returns:		CryptoGram Information
	Notes:
--------------------------------------------------------------------------*/

//byte processReferral(unsigned char source)//NAND_FLASH changes

byte processReferral(unsigned char source, byte* pScript71, unsigned short usScript71Len, byte* pScript72, unsigned short usScript72Len )
{
    short           decision = 0;
    byte            buf[2] = {0};
    byte            auth1[3] = "Y2";
    byte            auth2[3] = "Z2";    
    byte            cryptInfoData = 0;
    unsigned short  len = 0;
    byte            buffer_ascii[30] = {0};
    byte            buffer_hex[20] = {0};
    char            key;
    unsigned short  usRes = 0;
   
   //T_Gangadhar_S1  Date 08-May-08 
   //UNATTENDED_TERMINAL: It is not supported for Unattended terminal
    if (!(gshInterac > 0)&& (gshUnAttended != 1)) //Purvin_p1 date 09/05/2007 Added For interac
	{
		clrscr();
		shDisplayMessage(MSG_PLEASE_CALL_BANK, 1,1);  
		usEMVGetTLVFromColxn(0x4F00,(byte*) &buffer_hex, &len);
		//BUFSIZE	strcpy()
		//Added buffer size check
		if(sizeof(buffer_ascii) >= strlen("AID:"))
			strcpy((char*)buffer_ascii, "AID:");
		hex2asc(&buffer_ascii[4], buffer_hex, len);
		write_at((char*)buffer_ascii, 4+2*len, 1, 2);
		usEMVGetTLVFromColxn(0x5A00,(byte*) &buffer_hex, &len);
		//BUFSIZE	strcpy()
		//Added buffer size check
		if(sizeof(buffer_ascii) >= strlen("PAN:"))
			strcpy((char*)buffer_ascii, "PAN:");
		hex2asc(&buffer_ascii[4], buffer_hex, len);
		write_at((char*)buffer_ascii, 4 + 2 * len, 1, 3);
		usEMVGetTLVFromColxn(0x5F24,(byte*) &buffer_hex, &len);
		//BUFSIZE	strcpy()
		//Added buffer size check
		if(sizeof(buffer_ascii) >= strlen("EXP:"))
			strcpy((char*)buffer_ascii, "EXP:");
		hex2asc(&buffer_ascii[4], buffer_hex, len);
		write_at((char*)buffer_ascii, 4 + 2 * len, 1, 4); 
    
		shDisplayMessage(MSG_APPROVE_TRANS, 1,6);  
		shDisplayMessage(MSG_YES, 19,6);
		shDisplayMessage(MSG_NO, 19,8);    
        //A+AUTOFT: Check introduced to avoid user intervention	
        if (gAPPFlag != 1)
        {
		key = chGetKeyPress();
		//Purvin_P1: date 15-03-08 
		//Vx700: there is not function key for vx700 terminal, So all function key are mapped with number key.
		//F1=Numeric 1,F2=Numeric 2,F3=Numeric 3 and F4=Numeric 4
			// for ONTIME id 22518 T_gangadhar_s1  24/April/08	support to work all function keys for all Vx terminals
		//Ontime Fix for 39220: Kishor_S1 changed the key values from "51" & "52" to "49" & "50"
		// To support 1& 2 keys for YES or NO OPTION when ucAllowKeys is enabled.
			if( ucAllowKeys  == 1 )
				{
					while ((key != 49) && (key != 50))
					key = chGetKeyPress();
				}
			else
				{
					while ((key != 3) && (key != 4))
					key = chGetKeyPress();
				}
        	}
		
        	else			       
            	key = 50;
			if( ucAllowKeys  == 1 )
			{
				if(key == 49)
		{
			//BUFSIZE
			//Buffer size check is not reqd we are copying 2 bytes to buf which is of size 2 bytes
			if (source == CARD_REFERRAL)
				memcpy(buf, auth1, 2);
			decision = HOST_AUTHORISED;
		}
		else
		{
			//BUFSIZE
			//Buffer size check is not reqd we are copying 2 bytes to buf which is of size 2 bytes
			if(source==CARD_REFERRAL)
				memcpy(buf, auth2, 2);
			decision = HOST_DECLINED;
				}
			}
			else
			{
				if(key == 3)
				{
				//BUFSIZE
				//Buffer size check is not reqd we are copying 2 bytes to buf which is of size 2 bytes
				if (source == CARD_REFERRAL)
					memcpy(buf, auth1, 2);
				decision = HOST_AUTHORISED;
				}
				else
				{
				//BUFSIZE
				//Buffer size check is not reqd we are copying 2 bytes to buf which is of size 2 bytes
				if(source==CARD_REFERRAL)
					memcpy(buf, auth2, 2);
				decision = HOST_DECLINED;
				}
		}
	}
	else 
	{
			//Purvin_p1 date 09/05/2007
			//For interac Host referral is declined.
			//BUFSIZE
			//Buffer size check is not reqd we are copying 2 bytes to buf which is of size 2 bytes
			memcpy(buf, auth2, 2);
			decision = HOST_DECLINED;
	}//End if gshInterac

    if(source == CARD_REFERRAL)
        usEMVUpdateTLVInCollxn(0x8a00, buf, 2);
    
    issuerScriptResults = (byte *) malloc(numScripts * 5);
//    usRes = inVXEMVAPUseHostData(decision, issuerScriptResults, &numScripts);//NAND_FLASH changes

	usRes = inVXEMVAPUseHostData(decision, issuerScriptResults, &numScripts,pScript71,usScript71Len,pScript72,usScript72Len);
	//Brabhu_h1 dated 16 Aug 2007
	//BL54TRME: Changes are made as per bulletin #54
	//Changes made to terminate the transaction when mandatory DO's
	//are missing, by checking the return values	
    if(usRes == BAD_DATA_FORMAT)
         //return (GEN_AAC);
         return (BAD_DATA_FORMAT);
	
    usEMVGetTLVFromColxn(0x9f27, &cryptInfoData, &len);
    
    return (cryptInfoData);
}



/*-------------------------------------------------------------------------
    Function:		processOnline
    Description:	
    Parameters:		void
    Returns:		Host Decision with CID
	Notes:
--------------------------------------------------------------------------*/

byte processOnline(void)
{
    byte                 req[MAX_MESSAGE_SIZE];         // Authorisation request message
    byte                 resp[MAX_MESSAGE_SIZE];        // Authorisation response message
    byte                 buffer[MAX_MESSAGE_SIZE] = {0};
	byte                 cid = 0;                       // Cryptogram Information Data
	byte                 termTypebuf[2] = {0};
    short                hostDecision = 0;
	short                len = 0;
    short                size = 0;
    short                shindex = 0;    
    unsigned short       tagLen = 0;
	unsigned short       usRes = 0;    
	char				szScript71[256]={0}, szScript72[256] = {0};
	unsigned short		us71ScriptLen =0, us72ScriptLen =0;//NAND_FLASH changes

	numScripts = 0;
    memset((byte *) &req, 0x00, sizeof(req));
    memset((byte *) &resp, 0x00, sizeof(resp));

	memset(termTypebuf, 0x00, sizeof(termTypebuf));
	usEMVGetTLVFromColxn(TAG_9F35_TERM_TYPE , termTypebuf, &tagLen);


      if((termTypeBuf[0] == 0x23 ) || (termTypeBuf[0] == 0x26)  || (termTypeBuf[0] == 0x36)) //terminal type is offline only
	  {
		//Brabhu_h1 dated 16 Aug 2007
		//BL54TRME: Changes are made as per bulletin #54
		//Changes made to terminate the transaction when mandatory DO's
		//are missing, by checking the return values
        usRes = inVXEMVAPSecondGenerateAC(OFFLINE_TERMINAL);
        usEMVGetTLVFromColxn(0x9f27, &cid, &tagLen);
		//CA.035  Transaction should be terminated after 2nd GenAC if the status word
		//of the response to the 2nd GenAC command is other than 9000
		//if (usRes == EMV_FAILURE)
		if (usRes != SUCCESS)
			return (BAD_DATA_FORMAT);
		else
        	return (cid);
	 }

    size = inBuildAuthRequest(req);
    hostDecision = inSendToHost(req, resp, size);
    while ((hostDecision == FAILED_TO_CONNECT) && (shindex < 2))
    {
        clrscr();
        shDisplayMessage(MSG_FAILED_TO_CONNECT, 1,1);    
        
        SVC_wAIT((unsigned long)4000);  //VERIXPRTNG: Porting to Verix
        if(inVXEMVAPCardPresent() != SUCCESS)
        {
            shDisplayMessage(MSG_CARD_REMOVED, 1,8);    
            
            SVC_wAIT((unsigned long)4000);
            return  GEN_AAC;  //declined
        }
        
        hostDecision = inSendToHost(req, resp, size);
        shindex++;
    }
    
    if (hostDecision == FAILED_TO_CONNECT)
    {
        sendReversal();
    }
    else if (hostDecision == HOST_REFERRAL)
    {
    #ifdef _TARG_68000
         if (findTag(0x91, buffer, (int*)&len, resp))              // Issuer auth data
            usEMVUpdateTLVInCollxn(0x9100, buffer, len);      
	#else
		if (findTag(0x91, buffer, (short *)&len, resp))              // Issuer auth data
            usEMVUpdateTLVInCollxn(0x9100, buffer, len); 
	#endif
  
       // _remove("script71.dat");
       // _remove("script72.dat");
     //   numScripts = createScriptFiles(resp);
	//	cid = processReferral(HOST_REFERRAL);
//NAND_FLASH changes
        numScripts = createScriptFiles(resp,(byte *)szScript71,&us71ScriptLen,(byte *)szScript72,&us72ScriptLen);
		cid = processReferral(HOST_REFERRAL,(byte *)szScript71,us71ScriptLen,(byte *)szScript72,us72ScriptLen);
		return (cid);
        
    }
    else
    {
    #ifdef _TARG_68000
        if (findTag(0x91, buffer, (int*)&len, resp))              // Issuer auth data
            usEMVUpdateTLVInCollxn(0x9100, buffer, len);
	#else
		if (findTag(0x91, buffer, (short*)&len, resp))              // Issuer auth data
            usEMVUpdateTLVInCollxn(0x9100, buffer, len); 
	#endif
       // _remove("script71.dat");
        //_remove("script72.dat");
 
       // numScripts = createScriptFiles(resp);
	 numScripts = createScriptFiles(resp,(byte *)szScript71,&us71ScriptLen,(byte *)szScript72,&us72ScriptLen);

    }

	//NAND_FLASH changes - Added the freeing of pointer variable as it was not done initiallay
	if(issuerScriptResults != NULL)
		{
			free(issuerScriptResults);
			issuerScriptResults = NULL;
		}
	issuerScriptResults = (byte *) malloc(numScripts * 5);
	
//	usRes = inVXEMVAPUseHostData(hostDecision, issuerScriptResults, &numScripts);
	
//NAND_FLASH changes
	usRes = inVXEMVAPUseHostData(hostDecision, issuerScriptResults, &numScripts,(byte *)szScript71,us71ScriptLen,(byte *)szScript72,us72ScriptLen);

	//Brabhu_h1 dated 16 Aug 2007
	//BL54TRME: Changes are made as per bulletin #54
	//Changes made to terminate the transaction when mandatory DO's
	//are missing, by checking the return values	
    if(usRes == BAD_DATA_FORMAT)
         //return (GEN_AAC);
         return (BAD_DATA_FORMAT);


	//Terminate the txn if the second GenAc returns bad data format
	if(usRes != SUCCESS)
         return usRes;

    usEMVGetTLVFromColxn(0x9f27, &cid, &tagLen);
	//CA.112.00	Ontime ID: 43998
	//Terminal can send the reversal when host decision is "Online Approved" and card's decision is AAC
	//only when online data capture is performed by the acquirer acc. to Book4 section 6.3.8
    /*if ((hostDecision == HOST_AUTHORISED) && ((cid & 0xC0) == 0x0))      // Host authorised but card declined
        sendReversal();*/
    
    return (cid);
}


/*-------------------------------------------------------------------------
    Function:		sendReversal
    Description:	
    Parameters:		void
    Returns:		
	Notes:
--------------------------------------------------------------------------*/

void sendReversal(void)
{
    byte      msg[MAX_MESSAGE_SIZE]  = {0};
    byte      resp[MAX_MESSAGE_SIZE] = {0};
    short     size = 0;
    
    memset((byte *) &msg, 0x00, sizeof(msg));
    size = inBuildReversalMsg(msg);
    clrscr();
    shDisplayMessage(MSG_SENDING_REVERSAL, 1,1);
    
    SVC_wAIT((unsigned long)4000);  //VERIXPRTNG: Porting to Verix
    inSendToHost(msg, resp, size);
}

/*-------------------------------------------------------------------------
    Function:		createScriptFiles
    Description:	
    Parameters:		char *
    Returns:		Returns number of scripts processed
	Notes:
--------------------------------------------------------------------------*/

//int createScriptFiles(byte *scriptBuf) //NAND_FLASH changes

int createScriptFiles(byte *scriptBuf, byte* pScript71, unsigned short *usScript71Len, byte* pScript72, unsigned short *usScript72Len )
{
    byte             *ptr;
    byte             data[512] = {0};
	#ifdef _TARG_68000
     unsigned int  tag = 0; //T_nabakishore_b1 17/July/07 changed from unsigned short to unsigned int for Verix
	#else
     unsigned short   tag = 0;
	#endif
    short            bufLen = 0;
//    short            h1 = -1, h2 = -1, handle;
    short            bytesRead = 0;
    short            bytes = 0;
    short            numScripts = 0;
    
    bufLen = scriptBuf[10];
	
//	printf(" BUFFER LEN %2X",bufLen);
//	SVC_WAIT(5000);
	
    ptr = &scriptBuf[11];              
/*    handle = 0;
    h1 = open("script71.dat", O_CREAT | O_WRONLY);
    h2 = open("script72.dat", O_CREAT | O_WRONLY);*/
	*usScript71Len = 0;
	*usScript72Len = 0;
    bytes = 0;
    bytesRead = 0;
    numScripts = 0;
    do
    {
        bytes = getNextRawTLVData(&tag, data, ptr + bytesRead);
	    bytesRead += bytes;

     /*   if (tag == 0x71)
            handle = h1;
        else if (tag == 0x72)
            handle = h2;

        if (handle)
        {
            numScripts++;
			write(handle, (char *) data, bytes);
            handle = 0;
        } */
		if ( (tag == 0x71) && (pScript71) )
			{
		        numScripts++;			
				memcpy((char *)pScript71,(char *) data, bytes); 			
				pScript71 += bytes;
				*usScript71Len += bytes;
			}
		else if ( (tag == 0x72) && (pScript72) )
			{
		        numScripts++;						
				memcpy((char *)pScript72,(char *) data, bytes); 						
				pScript72 += bytes;					
				*usScript72Len += bytes;				
			}
    } while (bytesRead < bufLen);

/*    close(h1);
    close(h2); */
    
    return (numScripts);
}


/*-------------------------------------------------------------------------
    Function:		inSendToHost
    Description:	
    Parameters:		byte *, byte *, int
    Returns:		Returns number of scripts processed
	Notes:          
--------------------------------------------------------------------------*/

int inSendToHost(byte *request, byte *response, int size)
{
//    long           event = 0L;
//    short          timer = 0;
    struct         Opn_Blk blk;
    short          port = 0;
    short          bytes = 0;
    short          timeout = 0;
    short          count = 0;
    byte           buffer[MAX_MESSAGE_SIZE] = {0};
    short          len = 0;
    short          hostDecision = 0;
	short          shRetVal = 0;
	char    		chBuf[5] = {0}; //NAND_FLASH : Read the Environment variable HOSPORT once and use it
	unsigned long  ulEVT = 0L;
 	unsigned long start_ticks, current_ticks;
    static short   shComPort=0;//NAND_FLASH : Read the Environment variable HOSPORT once and use it
    
    byte arc1[2] = {0x30, 0x30};        // All of these auth response codes mean transaction approved
    byte arc2[2] = {0x30, 0x38};
    byte arc3[2] = {0x31, 0x31};
    byte arcRef[2] = {0x30, 0x31};	    // Referral
    byte arcDen[2] = {0x30, 0x35};		//denial
    byte arcDen2[2] = {0x35, 0x31};		//denial
    
    blk.rate = Rt_9600;
    blk.format = Fmt_A8N1;
    blk.protocol = P_char_mode;

	//Fix for Ontime 61475: Environment variable have to read once from config.sys file	
	if( !shComPort )
		{
			shRetVal = get_env("HOSPORT", (char *)chBuf, sizeof(chBuf));
			if(shRetVal)
				shComPort = atoi(chBuf);			
			else
				shComPort = 1;//Default if there is no environment variable use COM1			
		}
	
        switch(shComPort)//NAND_FLASH : Use the static variable populated
		{
          case 1:
               port = open(DEV_COM1, O_RDWR);
               ulEVT =EVT_COM1;
               break;
          case 2:
               port = open(DEV_COM2, O_RDWR);
               ulEVT =EVT_COM2;
               break;
          case 3:
               port = open(DEV_COM3, O_RDWR);
               ulEVT =EVT_COM3;
               break;
          case 4:
               port = open(DEV_COM4, O_RDWR);
               ulEVT =EVT_COM4;
               break;
          case 5:
               port = open(DEV_COM5, O_RDWR);
               ulEVT =EVT_COM5;
               break;
          case 6:
               port = open(DEV_COM6, O_RDWR);
               ulEVT =EVT_COM6;
               break;
          default:
               port = open(DEV_COM1, O_RDWR);
               ulEVT =EVT_COM1;
               break;
		}

    if (port == -1)
        return (FAILED_TO_CONNECT);

    set_opn_blk(port, &blk);
    
    // Send authorisation request to ICC Solutions host ICCSUimHO.
    clrscr();
    shDisplayMessage(MSG_SENDING_DATA, 1,1);
    
    bytes = write(port, (char *) request, size);
    
    // Wait for response
/*    timeout = 0;
    timer = set_timer(15000L, EVT_TIMER);    // 15 second timeout
    while (1)
    {
        event = wait_event();
//		clrscr();
//		printf("Event %d event11 %d \n",event ,event & ulEVT);
//		SVC_WAIT(5000);
        if (event & ulEVT)
        {
//      		printf("INSIDE IF \n");
//		    SVC_WAIT(5000);
            clr_timer(timer);
            count = 0;
            while (1)
            {
                bytes = read(port, (char *) response + count, MAX_MESSAGE_SIZE - count);
//				printf("TOTAL BYTES %d\n",bytes);
//		        SVC_WAIT(5000);
                clrscr();
                shDisplayMessage(MSG_RECEIVING, 1,1);
                count += bytes;
                if ((response[count - 2] == '\x3') && ((&response[count - 2] - &response[11]) == response[10]))                            
                    break;
				
                SVC_wAIT(10);  
                if(peek_event() & EVT_TIMER)
                {
                    timeout = 1;
                    break;
                }
            }
            break;
        }
        else 
		if (event & EVT_TIMER)
        {
            timeout = 1;
            clr_timer(timer);            
            break;
        }
    }

    */
		start_ticks=read_ticks();
			timeout=0;
			count=0;
		while (1)
		{
			   current_ticks=read_ticks();
			
				if((current_ticks-start_ticks)>=15000)
				{
				   SVC_wAIT((unsigned long)100);  //VERIXPRTNG: Porting to Verix
					timeout = 1;
				   break;
				}
			
				   bytes = read(port, (char *) response + count, MAX_MESSAGE_SIZE - count);
			
			   if (bytes<=0)
				 continue;
			
				   shDisplayMessage(MSG_RECEIVING, 1,1);
				   count += bytes;
			   
			   if ((response[count - 2] == '\x3') && ((&response[count - 2] - &response[11]) == response[10])) 						   
			     break;
			
				   SVC_wAIT((unsigned long)10);  //VERIXPRTNG: Porting to Verix 
	   }


    close(port);
    
//    event = read_event();       // Flush out the event mask for next time around
    
    if (timeout)
        return (FAILED_TO_CONNECT);
    
    if (response[1] != 0)                       // Result code, bad lrc, missing etx etc. in request.
        return (FAILED_TO_CONNECT);
 #ifdef _TARG_68000
    findTag(0x8a, buffer, (int*)&len, response);      // Authentication response code
 #else
 	findTag(0x8a, buffer, (short*)&len, response);      // Authentication response code
 #endif
    if ((memcmp(buffer, arc1, 2) == 0) || (memcmp(buffer, arc2, 2) == 0) || (memcmp(buffer, arc3, 2) == 0))
    {
        hostDecision = HOST_AUTHORISED;
        usEMVUpdateTLVInCollxn(0x8a00, buffer, 2);
    }
    else if(memcmp(buffer, arcRef, 2) == 0)         // Host referral
   	{
        usEMVUpdateTLVInCollxn(0x8a00, buffer, 2);
        hostDecision = HOST_REFERRAL;
   	}
    else if(memcmp(buffer, arcDen, 2) == 0)
    {
        usEMVUpdateTLVInCollxn(0x8a00, buffer, 2);
        hostDecision = HOST_DECLINED;
    }
    else if(memcmp(buffer, arcDen2, 2) == 0)
    {
        usEMVUpdateTLVInCollxn(0x8a00, buffer, 2);
        hostDecision = HOST_DECLINED;
    }
    else
        hostDecision = FAILED_TO_CONNECT;
    
    return (hostDecision);
}


/*-------------------------------------------------------------------------
    Function:		inBuildAuthRequest
    Description:	
    Parameters:		byte *
    Returns:		Returns Authorisation request packet
	Notes:          
--------------------------------------------------------------------------*/
int inBuildAuthRequest(byte *message)
{
    byte            buf[255];
    byte            trans = 0;
    unsigned short  len = 0;
    short           i = 0;
    srTxnResult     TVRTSI;
	//CA.103.01
	byte 			bCVMRes[3] = {0};
    
	//CodeReview:Check for pointer validity
	if(message == NULL)
		return(FAILURE);

	memset(buf, '\0', 255);
    
    message[0] = '\x02';    // stx
    message[1] = 1;  		//Acc. to ISO 8583, the MTI for an Auth. message is x1xx
    message[2] = 0;         // type 0 for terminal to host
    
    i=11;                   // Start of data
    
    message[i] = 0x82;      // Tag 82 - Application Interchange Profile
    usEMVGetTLVFromColxn(0x8200, (byte *) buf, &len);
    message[++i] = 2;
	//BUFSIZE
	//Buffer size check is not reqd as message is a pointer, but application should take care
	//not to copy more no. of bytes than the size of the buffer passed as a parameter to inBuildAuthRequest
    memcpy((byte *) &message[++i], (byte *) buf, 2);
    i += 2;
    
    message[i] = 0x9f;      // Tag 9f36 - Appli\cation Transaction Counter
    message[++i] = 0x36;
    usEMVGetTLVFromColxn(0x9f36, (byte *) buf, &len);
    message[++i] = 2;
	//BUFSIZE
	//Buffer size check is not reqd as message is a pointer
    memcpy((byte *) &message[++i], (byte *) buf, 2);
    i += 2;
    
    
    message[i] = 0x9f;      // Tag 9f41 - Transacation Sequence Counter
    message[++i] = 0x41;
    usEMVGetTLVFromColxn(0x9f41, (byte *) buf, &len);
    message[++i] = len;
	//BUFSIZE
	//Buffer size check is not reqd as message is a pointer
    memcpy((byte *) &message[++i], (byte *) buf, len);
    i += len;
    
    
    message[i] = 0x9f;      // Tag 9f27 - Cryptogram Information Data
    message[++i] = 0x27;
    usEMVGetTLVFromColxn(0x9f27, (byte *) buf, &len);
    message[++i] = 1;
	//BUFSIZE
	//Buffer size check is not reqd as message is a pointer
    memcpy((byte *) &message[++i], (byte *) buf, 1);
    i += 1;
    
    message[i] = 0x9f;      // Tag 9f34 - Cardholder Verification Method (CVM) Results
    message[++i] = 0x34;
    usEMVGetTLVFromColxn(0x9f34, (byte *) buf, &len);
    message[++i] = 3;
	//CA.103.01 CVMResults are copied in to a temp. buffer
	memcpy(bCVMRes, (byte *) buf, 3);
	//BUFSIZE
	//Buffer size check is not reqd as message is a pointer
    memcpy((byte *) &message[++i], (byte *) buf, 3);
    i += 3;
    
    if (usEMVGetTLVFromColxn(0x9f1e, (byte *) buf, &len) == EMV_SUCCESS)     // If found
    {
        message[i] = 0x9f;  // Tag 9f1e - IFD number
        message[++i] = 0x1e;
        message[++i] = 8;
		//BUFSIZE
		//Buffer size check is not reqd as message is a pointer
        memcpy((byte *) &message[++i], (byte *) buf, 8);
        i += 8;
    }
    
    if (usEMVGetTLVFromColxn(0x9f10, (byte *) buf, &len) == EMV_SUCCESS)
    {
        message[i] = 0x9f;  // Tag 9f10 - Issuer Application Data
        message[++i] = 0x10;
        message[++i] = len;
		//BUFSIZE
		//Buffer size check is not reqd as message is a pointer
        memcpy((byte *) &message[++i], (byte *) buf, len);
        i += len;
    }
    
    message[i] = 0x9f;      // Tag 9f33 - Terminal Capabilities
    message[++i] = 0x33;
    usEMVGetTLVFromColxn(0x9f33, (byte *) buf, &len);
    message[++i] = 3;
	//BUFSIZE
	//Buffer size check is not reqd as message is a pointer
    memcpy((byte *) &message[++i], (byte *) buf, 3);
    i += 3;
    
    message[i] = 0x9f;      // Tag 9f35 - Terminal Type
    message[++i] = 0x35;
    usEMVGetTLVFromColxn(0x9f35, (byte *) buf, &len);
    message[++i] = 1;
	//BUFSIZE
	//Buffer size check is not reqd as message is a pointer
    memcpy((byte *) &message[++i], (byte *) buf, 1);
    i += 1;    
    
    message[i] = 0x95;      // Tag 95 - Terminal Verification Results
    message[++i] = 5;
    usEMVGetTxnStatus(&TVRTSI);
	//BUFSIZE
	//Buffer size check is not reqd as message is a pointer
    memcpy((byte *) &message[++i], TVRTSI.stTVR, 5);
    i += 5;
    
    if (usEMVGetTLVFromColxn(0x9f37, (byte *) buf, &len) == EMV_SUCCESS)
    {
        message[i] = 0x9f;  // Tag 9f37 - Unpredictable Number
        message[++i] = 0x37;
        message[++i] = 4;
		//BUFSIZE
		//Buffer size check is not reqd as message is a pointer
        memcpy((byte *) &message[++i], (byte *) buf, 4);
        i += 4;
    }    
    
    if (usEMVGetTLVFromColxn(0x9f01, (byte *) buf, &len) == EMV_SUCCESS)
    {
        message[i] = 0x9f;  // Tag 9f01 - Acquirer Identifier
        message[++i] = 0x01;
        message[++i] = 6;
		//BUFSIZE
		//Buffer size check is not reqd as message is a pointer
        memcpy((byte *) &message[++i], (byte *) buf, 6);
        i += 6;
    }    
    
    message[i] = 0x9c;          // Tag 9c - Transaction Type
    message[++i] = 1;
    usEMVGetTLVFromColxn(0x9c00, (byte *) buf, &len);
    trans = buf[0];
	//BUFSIZE
	//Buffer size check is not reqd as message is a pointer
    memcpy((byte *) &message[++i], (byte *) buf, 1);
    i += 1;
    
    message[i] = 0x9f;      // Tag 9f02 - Amount Authorised (Numeric)
    message[++i] = 0x02;
    usEMVGetTLVFromColxn(0x9f02, (byte *) buf, &len);
    message[++i] = 6;
	//BUFSIZE
	//Buffer size check is not reqd as message is a pointer
    memcpy((byte *) &message[++i], (byte *) buf, 6);
    i += 6;
    
    if (trans == 9)         // Transaction is sale with cashback
    {
        if (usEMVGetTLVFromColxn(0x9f03, (byte *) buf, &len) == EMV_SUCCESS)
        {
            message[i] = 0x9f;      // Tag 9f03 - Amount Other (Numeric)
            message[++i] = 0x03;
            message[++i] = 6;
			//BUFSIZE
			//Buffer size check is not reqd as message is a pointer
            memcpy((byte *) &message[++i], (byte *) buf, 6);
            i += 6;
        }
    }
    
    if (usEMVGetTLVFromColxn(0x5f25, (byte *) buf, &len) == EMV_SUCCESS)
    {
        message[i] = 0x5f;      // Tag 5f25 - Application Effective Date
        message[++i] = 0x25;
        message[++i] = 3;
		//BUFSIZE
		//Buffer size check is not reqd as message is a pointer
        memcpy((byte *) &message[++i], (byte *) buf, 3);
        i += 3;
    }    
    
    if (usEMVGetTLVFromColxn(0x5f24, (byte *) buf, &len) == EMV_SUCCESS)
    {
        message[i] = 0x5f;      // Tag 5f24 - Application Expiration Date
        message[++i] = 0x24;
        message[++i] = 3;
		//BUFSIZE
		//Buffer size check is not reqd as message is a pointer
        memcpy((byte *) &message[++i], (byte *) buf, 3);
        i += 3;
    }    
    
    if (usEMVGetTLVFromColxn(0x5a00, (byte *) buf, &len) == EMV_SUCCESS)
    {
        message[i] = 0x5a;      // Tag 5a - Application PAN
        message[++i] = len;
		//BUFSIZE
		//Buffer size check is not reqd as message is a pointer
        memcpy((byte *) &message[++i], (byte *) buf, len);
        i += len;
    }    
    
    if (usEMVGetTLVFromColxn(0x5f34, (byte *) buf, &len) == EMV_SUCCESS)
    {
        message[i] = 0x5f;      // Tag 5f34 - Application PAN Sequence Number
        message[++i] = 0x34;
        message[++i] = 1;
		//BUFSIZE
		//Buffer size check is not reqd as message is a pointer
        memcpy((byte *) &message[++i], (byte *) buf, 1);
        i += 1;
    }
	//CA.103.01
	//Tag99 should be sent in the online message only in case of online PIN CVM
	if(((bCVMRes[0] & 0x0F) == 0x02) && (bCVMRes[2] == 0x00))
	{
	    if (usEMVGetTLVFromColxn(0x9900, (byte *) buf, &len) == EMV_SUCCESS)
	    {
	        message[i] = 0x99;      // Tag 99 - Enciphered PIN Data
	        message[++i] = len;
			//BUFSIZE
			//Buffer size check is not reqd as message is a pointer
	        memcpy((byte *) &message[++i], (byte *) buf, len);
	        i += len;
	    }
    
	}
    if (usEMVGetTLVFromColxn(0x9f15, (byte *) buf, &len) == EMV_SUCCESS)
    {
        message[i] = 0x9f;      // Tag 9f15 - Merchant Category Code
        message[++i] = 0x15;
        message[++i] = 2;
		//BUFSIZE
		//Buffer size check is not reqd as message is a pointer
        memcpy((byte *) &message[++i], (byte *) buf, 2);
        i += 2;
    }
    
    if (usEMVGetTLVFromColxn(0x9f16, (byte *) buf, &len) == EMV_SUCCESS)
    {
        message[i] = 0x9f;      // Tag 9f16 - Merchant Identifier
        message[++i] = 0x16;
        message[++i] = 15;
		//BUFSIZE
		//Buffer size check is not reqd as message is a pointer
        memcpy((byte *) &message[++i], (byte *) buf, 15);
        i += 15;
    }
    
    message[i] = 0x9f;          // Tag 9f39 - POS Entry Mode
    message[++i] = 0x39;
    usEMVGetTLVFromColxn(0x9f39, (byte *) buf, &len);
    message[++i] = 1;
	//BUFSIZE
	//Buffer size check is not reqd as message is a pointer
    memcpy((byte *) &message[++i], (byte *) buf, 1);
    i += 1;    
    
    if (usEMVGetTLVFromColxn(0x9f1a, (byte *) buf, &len) == EMV_SUCCESS)
    {
        message[i] = 0x9f;      // Tag 9f1a - Terminal Country Code
        message[++i] = 0x1a;
        message[++i] = 2;
		//BUFSIZE
		//Buffer size check is not reqd as message is a pointer
        memcpy((byte *) &message[++i], (byte *) buf, 2);
        i += 2;
    }
    
    message[i] = 0x9f;          // Tag 9f1c - Terminal Identifier
    message[++i] = 0x1c;
    usEMVGetTLVFromColxn(0x9f1c, (byte *) buf, &len);
    message[++i] = 8;
	//BUFSIZE
	//Buffer size check is not reqd as message is a pointer
    memcpy((byte *) &message[++i], (byte *) buf, 8);
    i += 8;    
    
    if (usEMVGetTLVFromColxn(0x5700, (byte *) buf, &len) == EMV_SUCCESS)
    {
        message[i] = 0x57;      // Tag 57 - Track 2 Equivalent Data
        message[++i] = len;
		//BUFSIZE
		//Buffer size check is not reqd as message is a pointer
        memcpy((byte *) &message[++i], (byte *) buf, len);
        i += len;
    }
    
    if (usEMVGetTLVFromColxn(0x5f2a, (byte *) buf, &len) == EMV_SUCCESS)
    {
        message[i] = 0x5f;      // Tag 5f2a - Transaction Currency Code
        message[++i] = 0x2a;
        message[++i] = 2;
		//BUFSIZE
		//Buffer size check is not reqd as message is a pointer
        memcpy((byte *) &message[++i], (byte *) buf, 2);
        i += 2;
    }
    
    message[i] = 0x9a;          // Tag 9a - Transaction Date
    message[++i] = 3;
    usEMVGetTLVFromColxn(0x9a00, (byte *) buf, &len);
	//BUFSIZE
	//Buffer size check is not reqd as message is a pointer
    memcpy((byte *) &message[++i], (byte *) buf, 3);
    i += 3;
    
    if (usEMVGetTLVFromColxn(0x9f21, (byte *) buf, &len) == EMV_SUCCESS)
    {
        message[i] = 0x9f;      // Tag 9f21 - Transaction Time
        message[++i] = 0x21;
        message[++i] = 3;
		//BUFSIZE
		//Buffer size check is not reqd as message is a pointer
        memcpy((byte *) &message[++i], (byte *) buf, 3);
        i += 3;
    }
    
    message[i] = 0x9f;          // Tag 9f26 - Application Cryptogram
    message[++i] = 0x26;
    usEMVGetTLVFromColxn(0x9f26, (byte *) buf, &len);
    message[++i] = 8;
	//BUFSIZE
	//Buffer size check is not reqd as message is a pointer
    memcpy((byte *) &message[++i], (byte *) buf, 8);
    i += 8;    
    
    
    //// 9F4C is required to be sent to the HOST: Fix for FIME 2CC.131.00
    
    message[i] = 0x9f;          // Tag 9f26 - Application Cryptogram
    message[++i] = 0x4c;
    usEMVGetTLVFromColxn(0x9f4c, (byte *) buf, &len);
    message[++i] = len;
	//BUFSIZE
	//Buffer size check is not reqd as message is a pointer
    memcpy((byte *) &message[++i], (byte *) buf, len);
    i += len;    
    
    //  Ends here.
    message[i] = 0x9b;          // Tag 9b - Transaction Status Information
    message[++i] = 2;
    usEMVGetTLVFromColxn(0x9b00, (byte *) buf, &len);
	//BUFSIZE
	//Buffer size check is not reqd as message is a pointer
    memcpy((byte *) &message[++i], (byte *) buf, 2);
    i += 2;
    
    message[10] = i - 11;       // Data length
    
    message[i] = '\x03';        // etx
    message[++i] = SVC_CRC_CALC(0, (char *) message + 1, 10 + message[10] + 1);     // lrc
    
    return (11 + message[10] + 2);
}

/*-------------------------------------------------------------------------
    Function:		inBuildReversalMsg
    Description:	
    Parameters:		byte *
    Returns:		
	Notes:          
--------------------------------------------------------------------------*/

int inBuildReversalMsg(byte *message)
{
    byte             buf[255] = {0};
    byte             trans = 0;
    unsigned short   len = 0;
    short            i = 0;
    srTxnResult TVRTSI;

	//CodeReview:Check for pointer validity
	if(message == NULL)
		return(FAILURE);
    
    memset(buf, '\0', 255);
    
    message[0] = '\x02';    // stx
    message[1] = 4;         //Acc. to ISO 8583, the MTI for a reversal message is x4xx 
    message[2] = 0;         // type 0 for terminal to host
    
    i=11;                   // Start of data
    
    message[i] = 0x82;      // Tag 82 - Application Interchange Profile
    usEMVGetTLVFromColxn(0x8200, (byte *) buf, &len);
    message[++i] = 2;
	//BUFSIZE
	//Buffer size check is not reqd as message is a pointer, but application should take care
	//not to copy more no. of bytes than the size of the buffer passed as a parameter to inBuildReversalMsg
    memcpy((byte *) &message[++i], (byte *) buf, 2);
    i += 2;
    
    message[i] = 0x9f;      // Tag 9f36 - Application Transaction Counter
    message[++i] = 0x36;
    usEMVGetTLVFromColxn(0x9f36, (byte *) buf, &len);
    message[++i] = 2;
	//BUFSIZE
	//Buffer size check is not reqd as message is a pointer
    memcpy((byte *) &message[++i], (byte *) buf, 2);
    i += 2;
    
    message[i] = 0x9f;      // Tag 9f41 - Transacation Sequence Counter
    message[++i] = 0x41;
    usEMVGetTLVFromColxn(0x9f41, (byte *) buf, &len);
    message[++i] = len;
	//BUFSIZE
	//Buffer size check is not reqd as message is a pointer
    memcpy((byte *) &message[++i], (byte *) buf, len);
    i += len;
    
    
    if (usEMVGetTLVFromColxn(0x9f1e, (byte *) buf, &len) == EMV_SUCCESS)
    {
        message[i] = 0x9f;  // Tag 9f1e - IFD number
        message[++i] = 0x1e;
        message[++i] = 8;
		//BUFSIZE
		//Buffer size check is not reqd as message is a pointer
        memcpy((byte *) &message[++i], (byte *) buf, 8);
        i += 8;
    }
    
    if (usEMVGetTLVFromColxn(0x9f10, (byte *) buf, &len) == EMV_SUCCESS)
    {
        message[i] = 0x9f;  // Tag 9f10 - Issuer Application Data
        message[++i] = 0x10;
        message[++i] = len;
		//BUFSIZE
		//Buffer size check is not reqd as message is a pointer
        memcpy((byte *) &message[++i], (byte *) buf, len);
        i += len;
    }
    //BUFSIZE
	//Buffer size check is not reqd as message is a pointer
    memcpy((byte *) &message[i], (byte *) issuerScriptResults, numScripts * 5);
    i += numScripts * 5;
    
    message[i] = 0x9f;      // Tag 9f33 - Terminal Capabilities
    message[++i] = 0x33;
    usEMVGetTLVFromColxn(0x9f33, (byte *) buf, &len);
    message[++i] = 3;
	//BUFSIZE
	//Buffer size check is not reqd as message is a pointer
    memcpy((byte *) &message[++i], (byte *) buf, 3);
    i += 3;
    
    message[i] = 0x9f;      // Tag 9f35 - Terminal Type
    message[++i] = 0x35;
    usEMVGetTLVFromColxn(0x9f35, (byte *) buf, &len);
    message[++i] = 1;
	//BUFSIZE
	//Buffer size check is not reqd as message is a pointer
    memcpy((byte *) &message[++i], (byte *) buf, 1);
    i += 1;    
    
    message[i] = 0x95;      // Tag 95 - Terminal Verification Results
    message[++i] = 5;
    usEMVGetTxnStatus(&TVRTSI);
	//BUFSIZE
	//Buffer size check is not reqd as message is a pointer
    memcpy((byte *) &message[++i], TVRTSI.stTVR, 5);
    i += 5;
    
    if (usEMVGetTLVFromColxn(0x9f01, (byte *) buf, &len) == EMV_SUCCESS)
    {
        message[i] = 0x9f;  // Tag 9f01 - Acquirer Identifier
        message[++i] = 0x01;
        message[++i] = 6;
		//BUFSIZE
		//Buffer size check is not reqd as message is a pointer
        memcpy((byte *) &message[++i], (byte *) buf, 6);
        i += 6;
    }    
    
    if (usEMVGetTLVFromColxn(0x5f24, (byte *) buf, &len) == EMV_SUCCESS)
    {
        message[i] = 0x5f;      // Tag 5f24 - Application Expiration Date
        message[++i] = 0x24;
        message[++i] = 3;
		//BUFSIZE
		//Buffer size check is not reqd as message is a pointer
        memcpy((byte *) &message[++i], (byte *) buf, 3);
        i += 3;
    }
    
    if (usEMVGetTLVFromColxn(0x5a00, (byte *) buf, &len) == EMV_SUCCESS)
    {
        message[i] = 0x5a;      // Tag 5a - Application PAN
        message[++i] = len;
		//BUFSIZE
		//Buffer size check is not reqd as message is a pointer
        memcpy((byte *) &message[++i], (byte *) buf, len);
        i += len;
    }
    
    if (usEMVGetTLVFromColxn(0x5f34, (byte *) buf, &len) == EMV_SUCCESS)
    {
        message[i] = 0x5f;      // Tag 5f34 - Application PAN Sequence Number
        message[++i] = 0x34;
        message[++i] = 1;
		//BUFSIZE
		//Buffer size check is not reqd as message is a pointer
        memcpy((byte *) &message[++i], (byte *) buf, 1);
        i += 1;
    }
    
    message[i] = 0x8a;          // Tag 8a - Authorisation Response Code
    usEMVGetTLVFromColxn(0x8a00, (byte *) buf, &len);
    message[++i] = 2;
	//BUFSIZE
	//Buffer size check is not reqd as message is a pointer
    memcpy((byte *) &message[++i], (byte *) buf, 2);
    i += 2;
    
    if (usEMVGetTLVFromColxn(0x9f15, (byte *) buf, &len) == EMV_SUCCESS)
    {
        message[i] = 0x9f;      // Tag 9f15 - Merchant Category Code
        message[++i] = 0x15;
        message[++i] = 2;
		//BUFSIZE
		//Buffer size check is not reqd as message is a pointer
        memcpy((byte *) &message[++i], (byte *) buf, 2);
        i += 2;
    }
    
    if (usEMVGetTLVFromColxn(0x9f16, (byte *) buf, &len) == EMV_SUCCESS)
    {
        message[i] = 0x9f;      // Tag 9f16 - Merchant Identifier
        message[++i] = 0x16;
        message[++i] = 15;
		//BUFSIZE
		//Buffer size check is not reqd as message is a pointer
        memcpy((byte *) &message[++i], (byte *) buf, 15);
        i += 15;
    }
    
    // Original Data Elements should go here.
    
    message[i] = 0x9f;          // Tag 9f39 - POS Entry Mode
    message[++i] = 0x39;
    usEMVGetTLVFromColxn(0x9f39, (byte *) buf, &len);
    message[++i] = 1;
	//BUFSIZE
	//Buffer size check is not reqd as message is a pointer
    memcpy((byte *) &message[++i], (byte *) buf, 1);
    i += 1;    
    
    if (usEMVGetTLVFromColxn(0x9f1a, (byte *) buf, &len) == EMV_SUCCESS)
    {
        message[i] = 0x9f;      // Tag 9f1a - Terminal Country Code
        message[++i] = 0x1a;
        message[++i] = 2;
		//BUFSIZE
		//Buffer size check is not reqd as message is a pointer
        memcpy((byte *) &message[++i], (byte *) buf, 2);
        i += 2;
    }
    
    message[i] = 0x9f;          // Tag 9f1c - Terminal Identifier
    message[++i] = 0x1c;
    usEMVGetTLVFromColxn(0x9f1c, (byte *) buf, &len);
    message[++i] = 8;
	//BUFSIZE
	//Buffer size check is not reqd as message is a pointer
    memcpy((byte *) &message[++i], (byte *) buf, 8);
    i += 8;
    
    if (usEMVGetTLVFromColxn(0x5700, (byte *) buf, &len) == EMV_SUCCESS)
    {
        message[i] = 0x57;      // Tag 57 - Track 2 Equivalent Data
        message[++i] = len;
		//BUFSIZE
		//Buffer size check is not reqd as message is a pointer
        memcpy((byte *) &message[++i], (byte *) buf, len);
        i += len;
    }
    
    message[i] = 0x9c;          // Tag 9c - Transaction Type
    message[++i] = 1;
    usEMVGetTLVFromColxn(0x9c00, (byte *) buf, &len);
    trans = buf[0];
	//BUFSIZE
	//Buffer size check is not reqd as message is a pointer
    memcpy((byte *) &message[++i], (byte *) buf, 1);
    i += 1;
    
    message[i] = 0x9f;      // Tag 9f02 - Amount Authorised (Numeric)
    message[++i] = 0x02;
    usEMVGetTLVFromColxn(0x9f02, (byte *) buf, &len);
    message[++i] = 6;
	//BUFSIZE
	//Buffer size check is not reqd as message is a pointer
    memcpy((byte *) &message[++i], (byte *) buf, 6);
    i += 6;
    
    if (trans == 9)             // Transaction is sale with cashback
    {
        if (usEMVGetTLVFromColxn(0x9f03, (byte *) buf, &len) == EMV_SUCCESS)
        {
            message[i] = 0x9f;      // Tag 9f03 - Amount Other (Numeric)
            message[++i] = 0x03;
            message[++i] = 6;
			//BUFSIZE
			//Buffer size check is not reqd as message is a pointer
            memcpy((byte *) &message[++i], (byte *) buf, 6);
            i += 6;
        }
    }
    
    if (usEMVGetTLVFromColxn(0x5f2a, (byte *) buf, &len) == EMV_SUCCESS)
    {
        message[i] = 0x5f;      // Tag 5f2a - Transaction Currency Code
        message[++i] = 0x2a;
        message[++i] = 2;
		//BUFSIZE
		//Buffer size check is not reqd as message is a pointer
        memcpy((byte *) &message[++i], (byte *) buf, 2);
        i += 2;
    }
    
    message[i] = 0x9a;          // Tag 9a - Transaction Date
    message[++i] = 3;
    usEMVGetTLVFromColxn(0x9a00, (byte *) buf, &len);
	//BUFSIZE
	//Buffer size check is not reqd as message is a pointer
    memcpy((byte *) &message[++i], (byte *) buf, 3);
    i += 3;
    
    if (usEMVGetTLVFromColxn(0x9f21, (byte *) buf, &len) == EMV_SUCCESS)
    {
        message[i] = 0x9f;      // Tag 9f21 - Transaction Time
        message[++i] = 0x21;
        message[++i] = 3;
		//BUFSIZE
		//Buffer size check is not reqd as message is a pointer
        memcpy((byte *) &message[++i], (byte *) buf, 3);
        i += 3;
    }
    
    message[10] = i - 11;       // Data length
    
    message[i] = '\x03';        // etx
    message[++i] = SVC_CRC_CALC(0, (char *) message + 1, 10 + message[10] + 1);     // lrc
    
    return (11 + message[10] + 2);
}
/*--------------------------------------------------------------------------
    Function:		inBuildFallbackMsg
    Description:	
    Parameters:		byte *
    Returns:		
	Notes:          
--------------------------------------------------------------------------*/
int inBuildFallbackMsg(byte *message)
{
    byte             buf[255] = {0};
    byte             trans = 0;
    unsigned short   len = 0;
    short            i = 0;

	//CodeReview:Check for pointer validity
	if(message == NULL)
		return(FAILURE);
    
    memset(buf, '\0', 255);
    
    message[0] = '\x02';    // stx
    message[2] = 0;         // type 0 for terminal to host
    
    i=11;                   // Start of data
    
    message[i] = 0x9f;      // Tag 9f35 - Terminal Type
    message[++i] = 0x35;
    usEMVGetTLVFromColxn(0x9f35, (byte *) buf, &len);
    message[++i] = 1;
	//BUFSIZE
	//Buffer size check is not reqd as message is a pointer, but application should take care
	//not to copy more no. of bytes than the size of the buffer passed as a parameter to inBuildFallbackMsg
    memcpy((byte *) &message[++i], (byte *) buf, 1);
    i += 1;    
    
    if (usEMVGetTLVFromColxn(0x5a00, (byte *) buf, &len) == EMV_SUCCESS)
    {
        message[i] = 0x5a;      // Tag 5a - Application PAN
        message[++i] = len;
		//BUFSIZE
		//Buffer size check is not reqd as message is a pointer
        memcpy((byte *) &message[++i], (byte *) buf, len);
        i += len;
    }
    
    if (usEMVGetTLVFromColxn(0x9f15, (byte *) buf, &len) == EMV_SUCCESS)
    {
        message[i] = 0x9f;      // Tag 9f15 - Merchant Category Code
        message[++i] = 0x15;
        message[++i] = 2;
		//BUFSIZE
		//Buffer size check is not reqd as message is a pointer
        memcpy((byte *) &message[++i], (byte *) buf, 2);
        i += 2;
    }
    
    // Original Data Elements should go here.
    
    message[i] = 0x9f;          // Tag 9f39 - POS Entry Mode
    message[++i] = 0x39;
    usEMVGetTLVFromColxn(0x9f39, (byte *) buf, &len);
    message[++i] = 1;
	//BUFSIZE
	//Buffer size check is not reqd as message is a pointer
    memcpy((byte *) &message[++i], (byte *) buf, 1);
    i += 1;    
    
    if (usEMVGetTLVFromColxn(0x5700, (byte *) buf, &len) == EMV_SUCCESS)
    {
        message[i] = 0x57;      // Tag 57 - Track 2 Equivalent Data
        message[++i] = len;
		//BUFSIZE
		//Buffer size check is not reqd as message is a pointer
        memcpy((byte *) &message[++i], (byte *) buf, len);
        i += len;
    }
    
    message[i] = 0x9c;          // Tag 9c - Transaction Type
    message[++i] = 1;
    usEMVGetTLVFromColxn(0x9c00, (byte *) buf, &len);
    trans = buf[0];
	//BUFSIZE
	//Buffer size check is not reqd as message is a pointer
    memcpy((byte *) &message[++i], (byte *) buf, 1);
    i += 1;
    
    message[i] = 0x9f;      // Tag 9f02 - Amount Authorised (Numeric)
    message[++i] = 0x02;
    usEMVGetTLVFromColxn(0x9f02, (byte *) buf, &len);
    message[++i] = 6;
	//BUFSIZE
	//Buffer size check is not reqd as message is a pointer
    memcpy((byte *) &message[++i], (byte *) buf, 6);
    i += 6;
    
    if (trans == 9)             // Transaction is sale with cashback
    {
        if (usEMVGetTLVFromColxn(0x9f03, (byte *) buf, &len) == EMV_SUCCESS)
        {
            message[i] = 0x9f;      // Tag 9f03 - Amount Other (Numeric)
            message[++i] = 0x03;
            message[++i] = 6;
			//BUFSIZE
			//Buffer size check is not reqd as message is a pointer
            memcpy((byte *) &message[++i], (byte *) buf, 6);
            i += 6;
        }
    }
    
    if (usEMVGetTLVFromColxn(0x5f2a, (byte *) buf, &len) == EMV_SUCCESS)
    {
        message[i] = 0x5f;      // Tag 5f2a - Transaction Currency Code
        message[++i] = 0x2a;
        message[++i] = 2;
		//BUFSIZE
		//Buffer size check is not reqd as message is a pointer
        memcpy((byte *) &message[++i], (byte *) buf, 2);
        i += 2;
    }
    
    message[i] = 0x9a;          // Tag 9a - Transaction Date
    message[++i] = 3;
    usEMVGetTLVFromColxn(0x9a00, (byte *) buf, &len);
	//BUFSIZE
	//Buffer size check is not reqd as message is a pointer
    memcpy((byte *) &message[++i], (byte *) buf, 3);
    i += 3;
    
    if (usEMVGetTLVFromColxn(0x9f21, (byte *) buf, &len) == EMV_SUCCESS)
    {
        message[i] = 0x9f;      // Tag 9f21 - Transaction Time
        message[++i] = 0x21;
        message[++i] = 3;
		//BUFSIZE
		//Buffer size check is not reqd as message is a pointer
        memcpy((byte *) &message[++i], (byte *) buf, 3);
        i += 3;
    }
    
    message[10] = i - 11;       // Data length
    
    message[i] = '\x03';        // etx
    message[++i] = SVC_CRC_CALC(0, (char *) message + 1, 10 + message[10] + 1);     // lrc
    
    return (11 + message[10] + 2);
}

/*--------------------------------------------------------------------------
    Function:		inInitDspMsgs
    Description:	Function for loading the display messages from the binary file.
    Parameters:		void
    Returns:		
	Notes:          
--------------------------------------------------------------------------*/

short inInitDspMsgs(void)
{
    
   	short     inResult = 0;
    short     hConfFHandle = -1;
    long      lnSeekPos = 0L;
    char      buffer[10] = {0};
    short     i = 0, inLen = 0;
    
    get_env("SUPPORTEDLANGUAGE", (char *)szSupp_Lang, sizeof(szSupp_Lang));
    
	//Brabhu_H1 dated 14 May 2007
	//Interac specific changes are done for language selection
    if ((inLen = strlen(szSupp_Lang) ) > 1 )
    {
        for (i=0; i<inLen;i++)
            szSupp_Lang[i]=tolower(szSupp_Lang[i]);
		
		for(i=0;i<inLen;i=i+2)
			{
			   	//Purvin_p1 date 31/05/2006
			   	//Change for Language selection.
			 	//BUFSIZE
			 	//Buffer size check is not reqd as szSupportedlanguage1, szSupportedlanguage2
			 	//szSupportedlanguage3 buffers are of size 2 bytes
				if(memcmp(&szSupp_Lang[i],"en",2)==0)
			        memcpy(gstLangConfig.szSupportedlanguage1,szSupp_Lang+i,2);							
				else if(memcmp(&szSupp_Lang[i],"fr",2)==0)
					memcpy(gstLangConfig.szSupportedlanguage2,szSupp_Lang+i,2);
				else if(memcmp(&szSupp_Lang[i],"es",2)==0)
					memcpy(gstLangConfig.szSupportedlanguage3,szSupp_Lang+i,2);
			}

        

    }
    else
    {
       	//Commented by Purvin_p1 date 31/05/2006
    	/*if(gshInterac != 1)
    		{
		        memcpy(gstLangConfig.szSupportedlanguage1,"en",2);
		        memcpy(gstLangConfig.szSupportedlanguage2,"es",2);
	    	}
			else
		{*/
			//BUFSIZE
			//Buffer size check is not reqd as szSupportedlanguage1, szSupportedlanguage2
			//szSupportedlanguage3 buffers are of size 2 bytes
			memcpy(gstLangConfig.szSupportedlanguage1,"en",2);
        	memcpy(gstLangConfig.szSupportedlanguage2,"fr",2);
			memcpy(gstLangConfig.szSupportedlanguage3,"es",2);
//    	}
    }
    
    memset(buffer, 0x00, sizeof(buffer));
    
    if(get_env("DEFAULTLANGUAGE", (char *)buffer, sizeof(buffer)) > 0)
        gstLangConfig.szDefaultlanguage = buffer[0];
    else
        gstLangConfig.szDefaultlanguage = '0';
    
    gstLangConfig.szActiveLanguage = gstLangConfig.szDefaultlanguage;
    
    
    if(gstLangConfig.szActiveLanguage == '0')
        hConfFHandle = open(MSG_ENG_FILENAME, O_RDWR);
    else
        hConfFHandle = open(MSG_SPN_FILENAME, O_RDWR);
    
    if (hConfFHandle < 0)
    {
        return(FAILURE);
    }
    
    lnSeekPos = lseek(hConfFHandle, 0L, SEEK_END);
	//T_nabakishore_b1 18/07/07
	//changes in casting as gshNorec is short
    
    gshNoRec = (short)((lnSeekPos - GEN_VER_SIZE) / (SIZE_MSG_REC));
    
    gstDspMsg = malloc (gshNoRec * SIZE_MSG_REC);
    inResult = sizeof(gstDspMsg);
    
   	if (lseek(hConfFHandle, (long)GEN_VER_SIZE, SEEK_SET) == -1)
    {
        close(hConfFHandle);
        return(FAILURE);
    }
    
    inResult = read(hConfFHandle, (void *)gstDspMsg, (gshNoRec * SIZE_MSG_REC));
    
    if (inResult == 0L)          /* End of file reached, invalid index  */
    {
        close(hConfFHandle);
        return(FAILURE);
        
    }
    
    if (close(hConfFHandle) < 0)
    {
        return(FAILURE);
    }		
    
    return(SUCCESS);
    
}

/*--------------------------------------------------------------------------
    Function:		vdSelPreferredLang
    Description:	Function for loading the Language file.
    Parameters:		void
    Returns:		SUCCESS, INVALID_MSG_FILE
	Notes:          
--------------------------------------------------------------------------*/
short vdSelPreferredLang (void)
{
    short          i = 0;
    unsigned short usRet = 0, usLen = 0;
    unsigned char  vucAux[30] = {0}, fMatch = 0;
    short          shKey = 0;
    char           szRetMsg[MAX_DISP_MSG_LENGTH] = {0};
//	char      	   buffer[10] = {0};//CN.012.00.01
	short      	   inLen = 0;
    
    usRet = usEMVGetTLVFromColxn (TAG_5F2D_LANG_PREFERENCE, vucAux, &usLen);
    if ((usRet == EMV_SUCCESS) && (usLen > 0) && (vucAux != NULL))
    {
         //Converts strings in to  Lowercase
        for (i=0; i<usLen ; i++)
 	        vucAux[i]=tolower(vucAux[i]);
      
        for (i = 0; i < usLen; i += 2)
        {
            if (memcmp (&vucAux[i], gstLangConfig.szSupportedlanguage1, 2) == 0)
            {
                gstLangConfig.szActiveLanguage = '0';
                fMatch = 1;
                shLoadDSPMsgFile(MSG_ENG_FILENAME);
                break;
            }
			//Brabhu_H1 dated 14 May 2007
			//Interac specific changes are done for language selection
	    	//Commented by Purvin_p1 date 31/05/2006
           /* if ((memcmp (&vucAux[i], gstLangConfig.szSupportedlanguage2, 2) == 0) && (gshInterac != 1))
            {
                gstLangConfig.szActiveLanguage = '1';
                fMatch = 1;
                shLoadDSPMsgFile(MSG_SPN_FILENAME);
                break;
            }*/
            
			if ((memcmp (&vucAux[i], gstLangConfig.szSupportedlanguage2, 2) == 0))
            {
                gstLangConfig.szActiveLanguage = '1';
                fMatch = 1;
                shLoadDSPMsgFile(MSG_FRN_FILENAME);
                break;
            }
			if ((memcmp (&vucAux[i], gstLangConfig.szSupportedlanguage3, 2) == 0))
            {
                gstLangConfig.szActiveLanguage = '2';
                fMatch = 1;
                shLoadDSPMsgFile(MSG_SPN_FILENAME);
                break;
            }
            
        }
    }
    
    if (fMatch) return (SUCCESS) ;
    
	//CN.012.00.01	Ontime ID: 44225
	//If the terminal supports only one language, and if it does not match with the language
	//preference tag 5F2D of the card, then it should not prompt for cardholder confirmation.

//NAND_FLASH  : Config.sys File access changes - Commented the line below	
//	get_env("SUPPORTEDLANGUAGE", (char *)buffer, sizeof(buffer)); 	

	
	inLen = strlen(szSupp_Lang);
	
    for (i=0; i<inLen;i++)
         szSupp_Lang[i]=tolower(szSupp_Lang[i]);

	if (inLen != 2 )
	{
	    clrscr();

		//2CO.006.00	Ontime ID: 44566
		//If the terminal supports two languages, and if none of them match with the language
		//preference tag 5F2D of the card, then only 2 languages should be prompted for
		//cardholder confirmation.
	   	if((memcmp (gstLangConfig.szSupportedlanguage1, "en", 2) == 0) &&
			(memcmp (gstLangConfig.szSupportedlanguage3, "es", 2) == 0) && 
			(memcmp (gstLangConfig.szSupportedlanguage2, "fr", 2) != 0))
			{
				if(shDSPRetrieveString (MSG_ENG_FILENAME, szRetMsg, MSG_SELECT_STRING) != SUCCESS)
		            return (INVALID_MSG_FILE);
				
		        write_at(szRetMsg, strlen(szRetMsg), 1, 3);
				
		        if( shDSPRetrieveString (MSG_ENG_FILENAME, szRetMsg, MSG_LANGUAGE_STRING)!= SUCCESS)
		            return (INVALID_MSG_FILE);
			
		        write_at(szRetMsg, strlen(szRetMsg), 14, 3);

				if(shDSPRetrieveString (MSG_SPN_FILENAME, szRetMsg, MSG_SELECT_STRING) != SUCCESS)
	            	return (INVALID_MSG_FILE);
				
		        write_at(szRetMsg, strlen(szRetMsg), 1, 7);
				
		        if( shDSPRetrieveString (MSG_SPN_FILENAME, szRetMsg, MSG_LANGUAGE_STRING)!= SUCCESS)
		            return (INVALID_MSG_FILE);
						
		        write_at(szRetMsg, strlen(szRetMsg), 14, 7);

			}

			else if((memcmp (gstLangConfig.szSupportedlanguage1, "en", 2) == 0) &&
				(memcmp (gstLangConfig.szSupportedlanguage2, "fr", 2) == 0) && 
				(memcmp (gstLangConfig.szSupportedlanguage3, "es", 2) != 0))
			{
				if(shDSPRetrieveString (MSG_ENG_FILENAME, szRetMsg, MSG_SELECT_STRING) != SUCCESS)
		            return (INVALID_MSG_FILE);
				
		        write_at(szRetMsg, strlen(szRetMsg), 1, 3);
				
		        if( shDSPRetrieveString (MSG_ENG_FILENAME, szRetMsg, MSG_LANGUAGE_STRING)!= SUCCESS)
		            return (INVALID_MSG_FILE);
			
		        write_at(szRetMsg, strlen(szRetMsg), 14, 3);

				if(shDSPRetrieveString (MSG_FRN_FILENAME, szRetMsg, MSG_SELECT_STRING) != SUCCESS)
		            return (INVALID_MSG_FILE);
				
		        write_at(szRetMsg, strlen(szRetMsg), 1, 5);
				
		        if( shDSPRetrieveString (MSG_FRN_FILENAME, szRetMsg, MSG_LANGUAGE_STRING)!= SUCCESS)
		            return (INVALID_MSG_FILE);
			
		        write_at(szRetMsg, strlen(szRetMsg), 14, 5);



			}

			else if((memcmp (gstLangConfig.szSupportedlanguage3, "es", 2) == 0) &&
				(memcmp (gstLangConfig.szSupportedlanguage2, "fr", 2) == 0)&& 
				(memcmp (gstLangConfig.szSupportedlanguage1, "en", 2) != 0))
			{
								
				if(shDSPRetrieveString (MSG_FRN_FILENAME, szRetMsg, MSG_SELECT_STRING) != SUCCESS)
		            return (INVALID_MSG_FILE);
				
		        write_at(szRetMsg, strlen(szRetMsg), 1, 5);
				
		        if( shDSPRetrieveString (MSG_FRN_FILENAME, szRetMsg, MSG_LANGUAGE_STRING)!= SUCCESS)
		            return (INVALID_MSG_FILE);
			
		        write_at(szRetMsg, strlen(szRetMsg), 14, 5);

				if(shDSPRetrieveString (MSG_SPN_FILENAME, szRetMsg, MSG_SELECT_STRING) != SUCCESS)
	            	return (INVALID_MSG_FILE);
				
		        write_at(szRetMsg, strlen(szRetMsg), 1, 7);
				
		        if( shDSPRetrieveString (MSG_SPN_FILENAME, szRetMsg, MSG_LANGUAGE_STRING)!= SUCCESS)
		            return (INVALID_MSG_FILE);
						
		        write_at(szRetMsg, strlen(szRetMsg), 14, 7);


			}
			else
			{
				if(shDSPRetrieveString (MSG_ENG_FILENAME, szRetMsg, MSG_SELECT_STRING) != SUCCESS)
		            return (INVALID_MSG_FILE);
				
		        write_at(szRetMsg, strlen(szRetMsg), 1, 3);
				
		        if( shDSPRetrieveString (MSG_ENG_FILENAME, szRetMsg, MSG_LANGUAGE_STRING)!= SUCCESS)
		            return (INVALID_MSG_FILE);
			
		        write_at(szRetMsg, strlen(szRetMsg), 14, 3);
			
				if(shDSPRetrieveString (MSG_FRN_FILENAME, szRetMsg, MSG_SELECT_STRING) != SUCCESS)
		            return (INVALID_MSG_FILE);
		        write_at(szRetMsg, strlen(szRetMsg), 1, 5);
		        if( shDSPRetrieveString (MSG_FRN_FILENAME, szRetMsg, MSG_LANGUAGE_STRING)!= SUCCESS)
		            return (INVALID_MSG_FILE);
				//Brabhu_H1 dated 25 June 07
				//FRLANG:Modified the cordinates from 12 to 14
		        write_at(szRetMsg, strlen(szRetMsg), 14, 5);
	        	if(shDSPRetrieveString (MSG_SPN_FILENAME, szRetMsg, MSG_SELECT_STRING) != SUCCESS)
	            return (INVALID_MSG_FILE);
		        write_at(szRetMsg, strlen(szRetMsg), 1, 7);
		        if( shDSPRetrieveString (MSG_SPN_FILENAME, szRetMsg, MSG_LANGUAGE_STRING)!= SUCCESS)
		            return (INVALID_MSG_FILE);
				//Brabhu_H1 dated 25 June 07
				//FRLANG:Modified the cordinates from 12 to 14			
		        write_at(szRetMsg, strlen(szRetMsg), 14, 7);
				
			}
	        
	        
	    while(1)
	    {
	        //A+AUTOFT: Check introduced to avoid user intervention	
	        if (gAPPFlag != 1)
	        shKey = chGetKeyPress();
	        else
	        	{
	        	  if( ucAllowKeys  == 1 )
				  	    shKey  = 49;
				  else
				  	    shKey  = 2;
	        	}
			//Brabhu_H1 dated 14 May 2007
			//Interac specific changes are done for language selection
		//T_Gangadhar_S1 24/April/08 Vx700: for ONTIME id 22518 added to support to work all function keys for all Vx terminal execept Vx700

		if( ucAllowKeys  == 1 )
		{   
	        if (shKey == 49)
	        {
	            gstLangConfig.szActiveLanguage = '0' ;
	            shLoadDSPMsgFile(MSG_ENG_FILENAME);
	            break;
	        }
	        else if (shKey == 50)
	        {
	   		//Brabhu_H1 dated 13 Jul 2007
			//LANGIEMV:Language selection has to be done is same way for both INTERAC and non-INTERAC transactions

	        	/*if (gshInterac != 1)
	        	{
	            gstLangConfig.szActiveLanguage = '1' ;
	            	shLoadDSPMsgFile(MSG_SPN_FILENAME);
	        	}
				else
				{*/
					gstLangConfig.szActiveLanguage = '1' ;
	            	shLoadDSPMsgFile(MSG_FRN_FILENAME);
	            	break;
	        }
	   		//Brabhu_H1 dated 13 Jul 2007
			//LANGIEMV:Language selection has to be done is same way for both INTERAC and non-INTERAC transactions
			//else if (shKey == 4 && gshInterac == 1 )
			else if (shKey == 51)
	        {
	            gstLangConfig.szActiveLanguage = '2' ;
	            shLoadDSPMsgFile(MSG_SPN_FILENAME);
	            break;
	        }
		 }
		else
		{
			if (shKey == 2)
		        {
		            gstLangConfig.szActiveLanguage = '0' ;
		            shLoadDSPMsgFile(MSG_ENG_FILENAME);
		            break;
		        }
		        else if (shKey == 3)
		        {
		   		//Brabhu_H1 dated 13 Jul 2007
				//LANGIEMV:Language selection has to be done is same way for both INTERAC and non-INTERAC transactions

		        		gstLangConfig.szActiveLanguage = '1' ;
		            	shLoadDSPMsgFile(MSG_FRN_FILENAME);
		            	break;
		        }
		   		//Brabhu_H1 dated 13 Jul 2007
				//LANGIEMV:Language selection has to be done is same way for both INTERAC and non-INTERAC transactions
				//else if (shKey == 4 && gshInterac == 1 )
				else if (shKey == 4)
		        {
		            gstLangConfig.szActiveLanguage = '2' ;
		            shLoadDSPMsgFile(MSG_SPN_FILENAME);
		            break;
		        }
			
			}
		 }
	}
	else 
	{
			if(memcmp(szSupp_Lang,"en",2)==0)
			{
				gstLangConfig.szActiveLanguage = '0' ;
            	shLoadDSPMsgFile(MSG_ENG_FILENAME);
			}
			else if(memcmp(szSupp_Lang,"fr",2)==0)
			{
				gstLangConfig.szActiveLanguage = '1' ;
            	shLoadDSPMsgFile(MSG_FRN_FILENAME);
			}
			else if(memcmp(szSupp_Lang,"es",2)==0)
			{
				gstLangConfig.szActiveLanguage = '2' ;
            	shLoadDSPMsgFile(MSG_SPN_FILENAME);
			}


	}
    return SUCCESS;
    
}

short shDisplayMessage(short shMsgID, short shCol, short shRow)
{
    write_at(gstDspMsg[shMsgID].szDisplayMessages ,strlen(gstDspMsg[shMsgID].szDisplayMessages), shCol, shRow);
    return SUCCESS;
}

short shRetrieveMessage(short shMsgID, char * sMsgBuffer)
{
	//BUFSIZE  CodeReview:Check for pointer validity
	//Buffer size check cannot be done as sMsgBuffer is a pointer
	if(sMsgBuffer != NULL)
		memcpy(sMsgBuffer, gstDspMsg[shMsgID].szDisplayMessages, strlen(gstDspMsg[shMsgID].szDisplayMessages ));
    return SUCCESS;
}

/*--------------------------------------------------------------------------
    Function:		shLoadDSPMsgFile
    Description:	Function for reading the Message file.
    Parameters:		void
    Returns:		SUCCESS, FAILURE
	Notes:          
--------------------------------------------------------------------------*/
short shLoadDSPMsgFile(char * sFileName)
{
    
    short hConfFHandle = -1;
    short inResult = 0;

    hConfFHandle = open(sFileName, O_RDWR);
    
    if (hConfFHandle < 0)
    {
        return(FAILURE);
    }
    
    if (lseek(hConfFHandle, (long)GEN_VER_SIZE, SEEK_SET) == -1)
    {
        close(hConfFHandle);
        return(FAILURE);
    }
    
    inResult = read(hConfFHandle, (void *)gstDspMsg, (gshNoRec * SIZE_MSG_REC));
        
    if (inResult == 0L)          /* End of file reached, invalid index  */
    {
        close(hConfFHandle);
        return(FAILURE);
    }
    
    if (close(hConfFHandle) < 0)
    {
        return(FAILURE);
    }		
    
    return(SUCCESS);
    
}

/*--------------------------------------------------------------------------
    Function:		shLoadDSPMsgFile
    Description:	Function for retrieve the Message from config file.
    Parameters:		void
    Returns:		SUCCESS, INVALID_MSG_FILE
	Notes:          
--------------------------------------------------------------------------*/
short shDSPRetrieveString (char * sFileName, char * szRetMsg, short shMsgID)
{
    short hConfFHandle = -1;

    hConfFHandle = open(sFileName, O_RDWR);
    
    if (hConfFHandle < 0)
    {
        return(FAILURE);
    }
    
    if((lseek(hConfFHandle, (long)(GEN_VER_SIZE + (shMsgID * SIZE_MSG_REC)), SEEK_SET))== -1)
    {
        close(hConfFHandle);
        return(FAILURE);
    }
    
    read(hConfFHandle, szRetMsg, SIZE_MSG_REC);
    
    close(hConfFHandle);
    return (SUCCESS);
    
}

//Pavan_K1 dated 2-Mar-07 added the below two function for testing the common module.
/*--------------------------------------------------------------------------
    Function:		vSetDefaultFunctionPointers
    Description:	Function to set the global function pointer varaibles
                    to the appropriate function addresses using module API.
    Parameters:		void
    Returns:		None
	Notes:          
--------------------------------------------------------------------------*/
void vSetDefaultFunctionPointers()
{
	inVxEMVAPSetFunctionality(PERFORM_ISS_ACQ_CVM, (void*)&usEMVIssAcqCVM);
	inVxEMVAPSetFunctionality(PERFORM_SIGNATURE, (void *)&usEMVPerformSignature);
	inVxEMVAPSetFunctionality(PERFORM_ONLINE_PIN, (void *)&usEMVPerformOnlinePIN);
	inVxEMVAPSetFunctionality(GET_LAST_TXN_AMT, (void *)&getLastTxnAmt);
	inVxEMVAPSetFunctionality(DISPLAY_ERROR_PROMPT, (void *)&usEMVDisplayErrorPrompt);
	inVxEMVAPSetFunctionality(GET_USER_PIN, (void *)&getUsrPin);
	
	inVxEMVAPSetFunctionality(IS_PARTIAL_SELECT_ALLOWED, NULL);
	inVxEMVAPSetFunctionality(GET_CAPK_DATA, NULL);
	inVxEMVAPSetFunctionality(GET_TERMINAL_PARAMETER, NULL);
	//MULTIAVN:
	inVxEMVAPSetFunctionality(GET_TERMINAL_AVN, NULL);
	//ExtAuth6985:
	inVxEMVAPSetFunctionality(Get_EXT_AUTH_DECISION, NULL);
	// Tisha_K1 - 25/Mar/2010
	// SU76 : Language selection should be done before GPO. 		
	inVxEMVAPSetFunctionality(PERFORM_LANGUAGE_SEL, (void*)&vdSelPreferredLang);

	//GDFix:
	inVxEMVAPSetFunctionality(GD_CHECK, NULL);

	// POS_CNCL
	//inVxEMVAPSetFunctionality(GET_POS_CVM_DECISION, (void *)&usGetPOSDecsn);

	
	// SPAIN_REQ
	//inVxEMVAPSetFunctionality(GET_BYPASS_DECISION, (void *)&usGetBypassDecsn);
}

/*--------------------------------------------------------------------------
    Function:		vSetAppDefaultFunctionPointers
    Description:	Function to set the global function pointer varaibles
                    to the appropriate function addresses using Library API.
    Parameters:		void
    Returns:		None
	Notes:          
--------------------------------------------------------------------------*/
void vSetAppFunctionPointers()
{
	usEMV_Set_Functionality(PERFORM_ISS_ACQ_CVM, (void*)&usEMVIssAcqCVM);
	usEMV_Set_Functionality(PERFORM_SIGNATURE, (void *)&usEMVPerformSignature);
	usEMV_Set_Functionality(PERFORM_ONLINE_PIN, (void *)&usEMVPerformOnlinePIN);
	usEMV_Set_Functionality(GET_LAST_TXN_AMT, (void *)&getLastTxnAmt);
	usEMV_Set_Functionality(DISPLAY_ERROR_PROMPT, (void *)&usEMVDisplayErrorPrompt);
	usEMV_Set_Functionality(GET_USER_PIN, (void *)&getUsrPin);	
	
	usEMV_Set_Functionality(GET_CAPK_DATA, (void *)&getAppCapkExp);
	usEMV_Set_Functionality(GET_TERMINAL_PARAMETER, (void *)&getAppTerminalParam);
	usEMV_Set_Functionality(IS_PARTIAL_SELECT_ALLOWED, (void *)&bAppIsMultipleOccurencesAllowed);

	usEMV_Set_Functionality(IS_CSN_VALID, (void *)&usAppESTValidateCSN);
	usEMV_Set_Functionality(GET_TXN_AMT, (void *)&getAppTransactionAmount);
	usEMV_Set_Functionality(IS_CARD_BLACKLISTED, (void *)&bIsCardBlackListed);
	usEMV_Set_Functionality(DISPLAY_PIN_PROMPT, (void *)&vAppDisplayPINPrompt);
}


//VERIXPRTNG: Porting to Verix			
#ifdef __thumb
//UPTMAGREAD
void vdMagStripeTrans(void)
{
	MAG_DATA stMagneticData;
	unsigned short   ushRetVal				= 0;
	short    hostDecision           = 0;
	short 	 shindex 				= 0;
	byte     resp[MAX_MESSAGE_SIZE] = {0};
	byte     message[MAX_MESSAGE_SIZE] = {0};
	byte     buf[255] = {0};
	int 	 i = 0;
	WORD 	 sec=10;
	short    size                   = 0;
   
	clrscr();
	printf("Insert Mag Card\n");
	SVC_wAIT((unsigned long)3000);  //VERIXPRTNG: Porting to Verix

	ushRetVal = EXT_IFD_INIT_SCR();  
	
	if(ushRetVal == EXT_IFD_SERVICE_ALARM_SET)
	{
	    clrscr();
	    printf("Service Alarm is Set\n");
	    SVC_wAIT((unsigned long)1000);  //VERIXPRTNG: Porting to Verix
	    ushRetVal = EXT_IFD_RESET_SERVICE_ALARM("67366377");
	    if(ushRetVal != EXT_IFD_Success)
	    {
		     printf("Service Alarm Reset Failed\n");
		     SVC_wAIT((unsigned long)2000);  //VERIXPRTNG: Porting to Verix
		     return ;
	    } 
	    else
	    {
		    SVC_RESTART("");
		}
	}

	ushRetVal = EXT_IFD_READMAGDATA(sec,&stMagneticData);
	if(ushRetVal != EXT_IFD_Success)
	{
	  	printf("READMAGDATA Failed \n");
		SVC_wAIT((unsigned long)2000);  //VERIXPRTNG: Porting to Verix
		return;
	}

	//Form the online message to send the track data to the host
	
	    memset(buf, '\0', 255);
		
		message[0] = '\x02';	// stx
		message[2] = 0; 		// type 0 for terminal to host
		
		i=11;					// Start of data
		
		if (stMagneticData.ushTrack1_Len > 0)
		{
			clrscr();
			printf("TRACK1 : VALID DATA: %s \n",stMagneticData.uchTrack1);

			memset(buf, 0x00, 255);
		    ascii_to_binary(buf, stMagneticData.uchTrack1, (stMagneticData.ushTrack1_Len));

			if(stMagneticData.ushTrack1_Len)
			{
				memcpy((byte *) &message[i], buf, ((stMagneticData.ushTrack1_Len)/2) + 1);

			}

			i += (((stMagneticData.ushTrack1_Len)/2) + 1);
		}
		else
		{
			clrscr();
	 		printf("TRACK1 : NO DATA:\n");
		}

		
		shDisplayMessage(MSG_PRESS_ANY_KEY, 1,8);			 
		chGetKeyPress();

		if (stMagneticData.ushTrack2_Len > 0)
		{
			clrscr();
			printf("TRACK2 : VALID DATA: %s \n",stMagneticData.uchTrack2);

			memset(buf, 0x00, 255);
		    ascii_to_binary(buf, stMagneticData.uchTrack2, (stMagneticData.ushTrack2_Len));

			if(stMagneticData.ushTrack2_Len)
			{
				memcpy((byte *) &message[i], buf, ((stMagneticData.ushTrack2_Len)/2));

			}

			i += (((stMagneticData.ushTrack2_Len)/2));
		}
		else
		{
			clrscr();
	 		printf("TRACK2 : NO DATA:\n");
		}

		
		shDisplayMessage(MSG_PRESS_ANY_KEY, 1,8);			 
		chGetKeyPress();

		if (stMagneticData.ushTrack3_Len > 0)
		{
			
			clrscr();
			printf("TRACK3 : VALID DATA: %s \n",stMagneticData.uchTrack3);

			memset(buf, 0x00, 255);
		    ascii_to_binary(buf, stMagneticData.uchTrack3, (stMagneticData.ushTrack3_Len));

			if(stMagneticData.ushTrack3_Len)
			{
				memcpy((byte *) &message[i], buf, ((stMagneticData.ushTrack3_Len)/2));

			}

			i += (((stMagneticData.ushTrack3_Len)/2));
		}
		else
		{
			clrscr();
	 		printf("TRACK3 : NO DATA:\n");
		}

		
		shDisplayMessage(MSG_PRESS_ANY_KEY, 1,8);			 
		chGetKeyPress();
			
		message[10] = i - 11;		// Data length
		
		message[i] = '\x03';		// etx
		message[++i] = SVC_CRC_CALC(0, (char *) message + 1, 10 + message[10] + 1); 	// lrc
		
		
		size = 11 + message[10] + 2 ;

  	
    shindex = 0;
	hostDecision = inSendToHost(message, resp, size);
	while ((hostDecision == FAILED_TO_CONNECT) && (shindex < 2))
	{
		clrscr();
     	shDisplayMessage(MSG_FAILED_TO_CONNECT, 1,1);  
		SVC_wAIT((unsigned long)4000);  //VERIXPRTNG: Porting to Verix
       	hostDecision = inSendToHost(message, resp, size);
		shindex++;
	}
    if(hostDecision == FAILED_TO_CONNECT)
	{
        clrscr();
        shDisplayMessage(MSG_FAILED_TO_CONNECT, 1,1);    
        shDisplayMessage(MSG_PRESS_ANY_KEY, 1,8);
        chGetKeyPress();
    }
                        
     clrscr();	  	 
	 if(hostDecision == HOST_AUTHORISED)
	 	 shDisplayMessage(MSG_TRANS_APPROVED, 5,2);  		  
	 	
	 else
	 	 shDisplayMessage(MSG_TRANS_DECLINED, 5,2);  				  
	 						 
	  
    shDisplayMessage(MSG_PRESS_ANY_KEY, 1,8);         	 
	chGetKeyPress();

	ushRetVal = EXT_IFD_CLEARMAGDATA();
		  
	if(ushRetVal != EXT_IFD_Success)
	{
	  	printf("CLEARMAGDATA Failed \n");
	}

}

#endif

//UNATTENDED: Soumya_G1  14/11/07
//Added this function to check whether the terminal type is unattended or not
/*--------------------------------------------------------------------------
	Function:		Is_Unattended_Terminal
	Description:	This function to check whether the terminal type is unattended or not
	Parameters: 	unsigned char term_type
	Returns:		EMV_TRUE/EMV_FALSE
	Notes:			
--------------------------------------------------------------------------*/
EMVBoolean Is_Unattended_Terminal(unsigned char term_type)
{
   unsigned char unattended_term = {0};
   
   unattended_term = term_type & UNATTENDED_TERM_MASK;
   switch(unattended_term)
   {
     case 0x04:
  	 case 0x05:
  	 case 0x06:
     	return EMV_TRUE;
  	 default:
     	return EMV_FALSE;
   }
  
}

//MULTIAVN:
//Added this function to fill the application version numbers in the buffer given by the kernel
/*--------------------------------------------------------------------------------------------
	Function:		usGetTermAVN
	Description:	This function fills the application version numbers in the buffer given
					by the kernel
	Parameters: 	srMultiAVN *pszTermAVN, int *iCount
	Returns:		EMV_SUCCESS/EMV_FAILURE
	Notes:			
---------------------------------------------------------------------------------------------*/
Ushort usGetTermAVN(srMultiAVN *pszTermAVN, int *iCount)
{

	srMultiAVN szMulAppVer[10] = {0};
	int i = 0;
	
	if(iCount != NULL)
		*iCount = 10;

	memcpy(szMulAppVer[0].stAVN, "\x11\x22", 2);
	memcpy(szMulAppVer[1].stAVN, "\x11\x33", 2);
	memcpy(szMulAppVer[2].stAVN, "\x11\x44", 2);
	memcpy(szMulAppVer[3].stAVN, "\x11\x55", 2);
	memcpy(szMulAppVer[4].stAVN, "\x11\x66", 2);
	memcpy(szMulAppVer[5].stAVN, "\x00\x8C", 2);
	memcpy(szMulAppVer[6].stAVN, "\x11\x88", 2);
	memcpy(szMulAppVer[7].stAVN, "\x11\x99", 2);
	memcpy(szMulAppVer[8].stAVN, "\x11\x11", 2);
	memcpy(szMulAppVer[9].stAVN, "\x22\x22", 2);
	

	for(i=0; i < *iCount; i++)
	{
		if(pszTermAVN != NULL)
			memcpy(pszTermAVN[i].stAVN, szMulAppVer[i].stAVN, MAX_AVN_LEN);

	}

	return EMV_SUCCESS;

}

//ExtAuth6985:	Ontime ID: 55477 
//Added this function for the application to determine the result of the transaction if the card 
//returns 0x6985 for External Authenticate command
/*--------------------------------------------------------------------------------------------
	Function:		usGetExtAuthDecision
	Description:	This function determines result of the transaction if the card returns 0x6985 
					for External Authenticate command
	Parameters: 	unsigned short usStatusWord
	Returns:		EMV_SUCCESS/EMV_FAILURE
	Notes:			
---------------------------------------------------------------------------------------------*/
Ushort usGetExtAuthDecision(unsigned short usStatusWord)
{

	if(usStatusWord == 0x6985)
		return EMV_SUCCESS;


}

/*
	Firmware Download Progress Bar
*/

void callbackfn(int cnt, int tot)
{
	if (!cnt)
	{
		clrscr();
		printf("\n\n\nDownloading Signature ...\n");
	}
	else
	{
		clrscr();
		printf("\n\n\nDownloading %d [%d] ...", cnt, tot);
		//fflush(stdout);
		if (cnt == tot)
		{
			clrscr();
			printf("\n\n\nDownload Done!!");
			SVC_WAIT(2000);
			printf("\n\n\nResetting SCR");
		}
	}
}

//POS_CNCL
unsigned short usGetPOSDecsn(void)
{
	SVC_WAIT(500); 

	return EXT_PIN_CNCL;
}




//SPAIN_REQ
unsigned short usGetBypassDecsn(void)
{
	write_at("Mandatory PIN",strlen("Mandatory PIN"),1,8);
	SVC_WAIT(1000);

	return PIN_MANDATORY;
}




Ushort usStoreBatchData()
{
	char tempBuffer[32] = {0}, tempBufferStr[32] = {0};   

	//Clearing all variables before updating from collection
	memset(apppan, 0 , sizeof(apppan));
	memset(ucAppExDate, 0 , sizeof(ucAppExDate));
	memset(ucCardholdername, 0 , sizeof(ucCardholdername));
	memset(ucSvcCode, 0 , sizeof(ucSvcCode));
	memset(ucTrk2Data, 0 , sizeof(ucTrk2Data));
	memset(ucPinData, 0 , sizeof(ucPinData));
	memset(ucARC, 0 , sizeof(ucARC));
	memset(ucTermID, 0 , sizeof(ucTermID));
	ulAuthAmt = 0;
	ulOtherAmt = 0;
	memset(ucUnpredictNum, 0 , sizeof(ucUnpredictNum));

	panlen = explen = cnamelen = svclen = trk2len = pinlen = 0;
	shARClen = shTermIDlen = shAmt_Auth_len = shOther_Amt_len = shUnpredictNum_len = 0;
	
	//Ganesh_P1 30-Jun-10 PCI_CHANGE : storing all required data
	usEMVGetTLVFromColxn(TAG_5A_APPL_PAN, apppan, &panlen);

	usEMVGetTLVFromColxn(TAG_5F24_EXPIRY_DATE, ucAppExDate, &explen);
		
	usEMVGetTLVFromColxn(TAG_5F20_CARDHOLDER_NAME, ucCardholdername, &cnamelen);

	usEMVGetTLVFromColxn(TAG_5F30_SERVICE_CODE, ucSvcCode, &svclen);

	usEMVGetTLVFromColxn(TAG_57_TRACK2_EQ_DATA, ucTrk2Data, &trk2len);

	usEMVGetTLVFromColxn(TAG_99_PIN_DATA, ucPinData, &pinlen);

	usEMVGetTLVFromColxn(TAG_8A_AUTH_RESP_CODE, ucARC, &shARClen);

	usEMVGetTLVFromColxn(TAG_9F1C_TEMR_ID, (BYTE*)ucTermID, &shTermIDlen);

	usEMVGetTLVFromColxn(TAG_9F02_AMT_AUTH_NUM,(BYTE*)tempBuffer, &shAmt_Auth_len); 
	if(shAmt_Auth_len > 0)
	{
		hex2asc((byte *) tempBufferStr, (BYTE*)tempBuffer, shAmt_Auth_len);
		ulAuthAmt = atol(tempBufferStr);
	}

	usEMVGetTLVFromColxn(TAG_9F03_AMT_OTHER_NUM, (BYTE*)tempBuffer, &shOther_Amt_len);

		if(shOther_Amt_len > 0)
		{
			hex2asc((byte *) tempBufferStr,(BYTE*) tempBuffer, shOther_Amt_len);
			ulOtherAmt = atol(tempBufferStr);
		}

	usEMVGetTLVFromColxn( TAG_9F37_UNPRED_NUM, ucUnpredictNum, &shUnpredictNum_len);	

	return EMV_SUCCESS;
}

