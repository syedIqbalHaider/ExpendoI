@echo off
echo *** Generating data file MSG_ENG.txt
if exist MSG_ENG.dat del MSG_ENG.dat 
Gendata -A MSG_ENG.txt
copy MSG.dat ..\..\output\RV\files\resources\MSG_ENG.dat
del MSG.dat 

if exist MSG_SPN.dat del MSG_SPN.dat 
Gendata -A MSG_SPN.txt
copy MSG.dat ..\..\output\RV\files\resources\MSG_SPN.dat
del MSG.dat

if exist MSG_FRN.dat del MSG_FRN.dat 
Gendata -A MSG_FRN.txt
copy MSG.dat ..\..\output\RV\files\resources\MSG_FRN.dat
del MSG.dat 

if exist emvg_cnf.dat del emvg_cnf.dat 
Gendata -A emvg_cnf.txt
copy cnf.dat ..\..\output\RV\files\resources\emvg_cnf.dat
del cnf.dat 


