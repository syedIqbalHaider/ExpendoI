Version   "11/14/03"
target    390
header    <VBlct.hdr>

#include <..\..\include\gdMSG.h>

data MSG
{

       ("Unknown Packet  ")
       ("Approved")
       ("Declined")
       ("Offline approved")
       ("Offline declined")
       ("Not Accepted")
       ("Non EMV")
       ("Failure         ")
       ("Card removed    ")
       ("Card blocked    ")
       ("Appl blocked    ")
       ("cand. list empty")
       ("Chip error      ")
       ("Bad data format ")
       ("Appl not avlble ")
       ("Trans Cancelled ")
       ("Easy entry appl ")
       ("Icc data missing")
       ("Cnfg fl nt found")
       ("Selection failed")
       ("Use chip        ")
       ("Pls remove card ")
       ("Bad ICC Resp    ")
       ("Emv Failure     ")
       ("Use Mag Card    ")
       ("Tag Not Found   ")
       ("Invald Capk File")
       ("Failed          ")
       ("Sign required   ")
       ("Enter PIN       ")
       ("Insert Card     ")
       ("PIN Required    ")
       ("Last PIN Try    ")
       ("PIN Try Exceeded")
       ("PIN Cancelled   ")
       ("PIN Bypassed    ")
       ("Wrong Checksum  ")
       ("Error           ")
       ("PerformSignature")
       ("Incorrect PIN   ")
       ("SELECT          ")
       ("Processing...   ")
       ("ENGLISH         ")
       ("Card Inserted   ")
       ("Invalid Param   ")
       ("Commn. Error    ")
       ("Serv Not allowed")
       ("App Not Effect  ")
       ("App Expired     ")
       ("Chip Malfunction")
       ("Enter OL PIN    ")
       ("Insert  or      ")
       ("Swipe Card      ")
       ("PIN Blocked     ")
       ("Do you want to  ")
       ("go for fallback ")
       ("Yes")
       ("No")
       ("Swipe Magnetic Card")
       ("Failed to connect ")
       ("Press any key  ")
       ("Please wait... ")
       ("Call your bank...")
       ("Sale Amount  ")
       ("Approve trans?")
       ("Sending reversal")
       ("Sending...")
       ("Receiving...") 
       ("Force online? ")
       ("Wait for customer")
       ("Print receipt?")
       ("NEXT ")
       ("PREV ")
       ("Pinpad not present")
       ("Enter key ")
       ("diversification value")
       ("Pinpad initialised")
       ("Script proc. results")
       ("Initlze PINpad")
       ("CashBack Amt")
       ("PIN Progress")
       ("PIN Complete")
       ("Invalid ATR")
       ("CAPK fl nt found")
       ("Force Decline...?")
       ("CAPK Fl Expired")
       ("No CAPK data   ")
       ("Invalid Module")
       ("Try Again")       			
       ("EXT PIN Cancelled")
       ("EXT PIN Bypassed")
       ("Transaction Timed Out") 
}