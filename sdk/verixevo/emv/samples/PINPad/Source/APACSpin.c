/*
 * FILENAME       : APACSpin.c
 * PRODUCT        : Verix V.
 * VERSION        : 4.0.0
 * AUTHOR(s)      : T_Arvind_G1
 * CREATED        : 20/Dec/06
 *
 * DESCRIPTION    : EMVTransaction class provides methods to perform EMV 4.1.
 * transaction as per EMV application specifications Ver 4.1.
 * This class contains all the high level methods to handle an 
 * EMV transaction. This class uses other classes listed in this 
 * library to perform a transaction.
 * 
 *
 * CONTENTS   :
 * MODIFICATION HISTORY    :
 *
 * #    Date        Who         History
 * ---  --------    ----------  ----------------------------------------
 
 * 1   13 JULY 07   Purvin_P1   RETURN_TYPE:Return type change from short to UShort for getUsrPin().
 *
 * 2   31-Jul-07    Pavan_K1    FIMEDBG:Fix made for the (2CM.129.02). Decrypted pin not matching with PIN entered.
 *                                      If the PIN length is 12 then the pinblock is updated to reflect the complete PIN.
 * 3   06/Jul/09	Mary_F1		BUFSIZE
 *								Buffer size is checked for memcpy() and strcpy()
 *
 * 4   13/Jun/11    Mary_F1     BKSPKEY: Terminal should re-prompt the previous prompt if backspace
 *								key is pressed during the "ENTER PIN" prompt
 *
 *
 * Copyright (C) 2006 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language, in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual or otherwise, without the prior written
 * permission of VeriFone Inc.
 *                                                2099 Gateway Place
 *             					                  Suite 600
 * 					                              San Jose  CA  95110 
 *              				                  USA
 *
 *
 *****************************************************************************/

#include <string.h>
#include <svc.h>		
#include <stdio.h>
#include <ctype.h>

#include "APACSpin.h"
#include <EMVResult.hpp>

short deskey(unsigned char* ,short);
short ascii_to_binary(unsigned char *,unsigned char *, short);
short hex2asc(unsigned char *,unsigned  char *,short);
//13 JULY 07 Purvin_P1 
//RETURN_TYPE:Return type change from short to UShort for getUsrPin().
unsigned short getUsrPin(char *);
short des(unsigned char * ,unsigned char*);


/*-------------------------------------------------------------------------
    Function:		inAPACS_OWF
    Description:	
    Parameters:		unsigned char *, unsigned char *, unsigned char *
    Returns:		
	Notes:
--------------------------------------------------------------------------*/
int inAPACS_OWF(unsigned char *D_value, unsigned char *K_value, unsigned char *result)
{        
    unsigned char X_value[9]="\xA5\xC7\xB2\x82\x84\x76\xA8\x29";
	unsigned char Y_value[9]="\xB5\xE3\x7F\xC5\xD4\xF7\xA3\x93";
 	unsigned char D_buffer[8];
	unsigned char K_buffer[8];
	int i;

	for(i=0; i<8; i++)
		{
		D_buffer[i]=D_value[i]^X_value[i];
		K_buffer[i]=K_value[i]^Y_value[i];
		}

	for(i=0; i<8; i++)
		K_buffer[i]=makeOdd(K_buffer[i]);

	deskey(K_buffer,1);
	des(result, D_buffer);

	for(i=0; i<8; i++)
		result[i]=result[i]^D_buffer[i];

	return(ROK);
}

/*-------------------------------------------------------------------------
    Function:		makeOdd
    Description:	
    Parameters:		unsigned char 
    Returns:		
	Notes:
--------------------------------------------------------------------------*/
unsigned char makeOdd(unsigned char byte)
{
	unsigned char pos = 2;
	unsigned char res;
	int bitsSet = 0;

    while (1)
    {
        if (byte & pos)
            bitsSet++;
        if (pos == 128)
            break;
        pos <<= 1;
    }

	res = byte;
	if ((bitsSet % 2) == 0) 
		res |= 1;
	else
        res = (res & 1) ^ res;
	
	return (res);
}

/*-------------------------------------------------------------------------
    Function:		inAPACSclearPINblock
    Description:	
    Parameters:		char *, char *, char *
    Returns:		
	Notes:
--------------------------------------------------------------------------*/
int inAPACSclearPINblock(char *track2, char *pin, char *pinblock)
{

	int len;
	int i,j;
	unsigned char tmp[16];
	char PAN[20];

	len=strlen(track2);
	i=0;
	while(!isdigit(track2[i++])) if(i==len) return(RERROR);
	j=0;
	while(isdigit(track2[i]) && i<=len && j<20)
		PAN[j++]=track2[i++];

	PAN[j]=0;
	len=strlen(pin);

	//FIMEDBG:Pavan_K1 dated 31-Jul-07: Fix made for the (2CM.129.02.05). If the PIN length is 
	//12 then the last digit of the pin is added to the pinblock.
	//sprintf(pinblock, "0%1.1d%s", len, pin);
	sprintf(pinblock, "0%1.1X%s", len, pin);
	
	memset(&pinblock[len+2], 'F', 16-len-2); 
	pinblock[16]=0;
	ascii_to_binary(tmp, (unsigned char *)pinblock, 16);
    memset(&tmp[8], 0x00, 8);
	ascii_to_binary(&tmp[8+2], (unsigned char *)&PAN[strlen(PAN)-12-1], 12);
	for(i=0;i<8;i++) tmp[i]^=tmp[i+8];
	hex2asc((unsigned char *)pinblock, tmp, 8);

	return(ROK);

}

/*-------------------------------------------------------------------------
    Function:		OnlinePIN
    Description:	To perform Online Pin Encryption
    Parameters:		unsigned char *, int, int
    Returns:		
	Notes:
--------------------------------------------------------------------------*/
//RETURN_TYPE: change return type from int to UShort.
//int OnlinePIN(unsigned char *buffer, int keyset, int operation)

unsigned short OnlinePIN(unsigned char *buffer, int keyset, int operation)
{

	static struct
	{
		unsigned char 	seed[8];
		unsigned char		current[8];
	} key[10];
	
	//FIMEDBG:Pavan_K1 dated 31-Jul-07: Fix made for the (2CM.129.02). Decrypted pin
	//not matchin with the PIN entered. set the array to 0.
	char pin[13]={0};
	
	unsigned char val_A[4], val_B[4], val_C[4], val_D[4], KEYVAL[8], CardKey[8];
	unsigned char tmp[16+1];
	unsigned char pinblock[8];
	int i,j;
	int cfs;
	//RETURN_TYPE: return type change to unsigned short.
	unsigned short retVal;

	retVal=ROK;
	switch(operation)
	{
		case ADD_NEW_KEY:
					//BUFSIZE
					//Buffer size check is not reqd as seed and current are buffers of 8 bytes
					memcpy(key[keyset].seed, buffer, 8);
					memcpy(key[keyset].current, buffer, 8);
					break;

		case RESET_KEY:
					//BUFSIZE
					//Buffer size check is not reqd as seed and current are buffers of 8 bytes
					memcpy(key[keyset].current, key[keyset].seed, 8);
					break;
			
		case BUILD_PINBLOCK:
					i=0;
					cfs=0;
					while(!isdigit(buffer[i++]));
					while(isdigit(buffer[i])) i++;
					cfs=i;
					memset(tmp, '0', 16);
					tmp[16]=0;
					j=15;
					for(i=cfs-1; i>=0; i--)
						if(isdigit(buffer[i])) 
							tmp[j--]=buffer[i];
					ascii_to_binary(val_A, tmp, 8);
					ascii_to_binary(val_B, &tmp[8], 8);
					memset(tmp, '0', 16);
					tmp[16]=0;
					i=cfs+5;
					j=0;
					while(buffer[i]!=0) tmp[j++]=buffer[i++];
					ascii_to_binary(val_C, buffer, 8);
					ascii_to_binary(val_D, &buffer[8], 8);

					retVal=getUsrPin(pin);

					if((retVal == E_USR_PIN_BYPASSED) || ((retVal == E_USR_PIN_CANCELLED)))
						retVal = 0;

					//BKSPKEY:
					if(retVal == E_USR_BACKSPACE_KEY_PRESSED)
						return E_USR_BACKSPACE_KEY_PRESSED;
					
					if(retVal > 0)
					{

						inAPACSclearPINblock((char *)buffer, pin, (char *)tmp);
						ascii_to_binary(pinblock, tmp, 16);
						deskey(key[keyset].current, 1);
						des(pinblock, pinblock);
						hex2asc(buffer, pinblock, 8);
						//BUFSIZE
						//Buffer size check is not reqd as tmp is a buffer of 17 bytes
						memcpy(tmp, val_A, 4);
						memcpy(&tmp[4], val_B, 4);
						inAPACS_OWF(tmp, key[keyset].current, KEYVAL);
						memcpy(tmp, val_B, 4);
						memcpy(&tmp[4], val_D, 4);
						memcpy(&tmp[8], val_A, 4);
						memcpy(&tmp[12], val_C, 4);
						inAPACS_OWF(tmp, &tmp[8], CardKey);
						inAPACS_OWF(CardKey, KEYVAL, key[keyset].current);
					}
			
			break;

		}

	return(retVal);
}
