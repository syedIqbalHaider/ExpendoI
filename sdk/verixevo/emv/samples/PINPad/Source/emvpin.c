/*
 * FILENAME       : emvpin.c
 * PRODUCT        : Verix V.
 * VERSION        : 4.0.0
 * AUTHOR(s)      : T_Arvind_G1
 * CREATED        : 20/Dec/06
 *
 * DESCRIPTION    : EMVTransaction class provides methods to perform EMV 4.1.
 * transaction as per EMV application specifications Ver 4.1.
 * This class contains all the high level methods to handle an 
 * EMV transaction. This class uses other classes listed in this 
 * library to perform a transaction.
 * 
 *
 * CONTENTS   :
 * MODIFICATION HISTORY    :
 *
 * #    Date        Who         History
 * ---  --------    ----------  ----------------------------------------
 * 1	06/Jul/09	Mary_F1		BUFSIZE
 *								Buffer size is checked for memcpy() and strcpy()
 * 2	07/Jul/09	Mary_F1		CodeReview:
 *								Incorporated code review feedback
 *
 *
 * Copyright (C) 2006 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language, in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual or otherwise, without the prior written
 * permission of VeriFone Inc.
 *                                                2099 Gateway Place
 *             					                  Suite 600
 * 					                              San Jose  CA  95110 
 *              				                  USA
 *
 *
 *****************************************************************************/

#define BYTE	unsigned char
#define MAX_LOOPS	100

#define INCORRECT_PIN	"INCORRECT PIN"
#define LAST_PIN_TRY	"LAST PIN TRY"
#define ENTER_YOUR_PIN	"ENTER YOUR PIN"
#define PIN_OK			"PIN OK"
#define PINPAD_READY	"PINPAD READY"
#define PLEASE_WAIT		"PLEASE WAIT"

#define KEY_DIV_VALUE	"KEY DIV VALUE"



#include <string.h>
#include <svc.h>		
#include <stdio.h>
#include <ctype.h>

#include "emvdef.h"
#include "emvpin.h"

_emvck EMVCK;

 short  rand(void);
  short usEMVGetTLVFromColxn(short, unsigned char* , int *);

/*-------------------------------------------------------------------------
    Function:		LRC_calc
    Description:	
    Parameters:		char *, int
    Returns:		
	Notes:
--------------------------------------------------------------------------*/
unsigned char LRC_calc(char *buffer, int len)
{
	char lrc=0;
	int i=0;

        while(i<len)  lrc^=buffer[i++]; 
	
	return(lrc);
}

/* get_unpredict() - this function generates an unpredictable number of any 
 * size. To avoid rand() producing a predictable sequence of bytes from
 * when the terminal is switched on, use the time as well.
 */

/*-------------------------------------------------------------------------
    Function:		get_unpredict_No
    Description:	
    Parameters:		BYTE *, int
    Returns:		
	Notes:
--------------------------------------------------------------------------*/
void get_unpredict_No(BYTE *inpbuff, int size)
{
int  i;
char time[15]; // Format will be YYYYMMDDhhmmssW (W=weekday, 0=Sunday).
int  seconds;  // Time in seconds since last hour

	rand();		// always call at least once because rand() seems to
				// return zero when terminal first switched on.
	read_clock(time);
	seconds = ((time[10] - 0x30) * 600) + ((time[11] - 0x30) * 60)
		+ ((time[12] - 0x30) * 10) + time[13];

	// rand() returns a signed short with no negative values so just take
	// LS BYTE as random BYTE after multiplying with time.
	for (i=0; i < size; i++)
		inpbuff[i] = (BYTE) (rand() * seconds);

	return;
}

/*-------------------------------------------------------------------------
    Function:		getPPUsrPin
    Description:	
    Parameters:		unsigned char *, unsigned char *,, unsigned char *, int *
    Returns:		
	Notes:
--------------------------------------------------------------------------*/
short getPPUsrPin(unsigned char * pstPin, unsigned char *CKclear, unsigned char *CKencr, int *index)
{
	static unsigned char tag9F41_prev[4];	/*transaction sequence counter*/
	static unsigned char tag5A_prev[10+1];	/*PAN*/
	unsigned char tag9F41_curr[4];			/*transaction sequence counter*/
	unsigned char tag5A_curr[10+1];		/*PAN*/
	unsigned char tag9F17_curr[2];			/*PIN try counter*/
	char msg1[16+1];
	char msg2[16+1];
	char msg3[16+1];
	int taglen;

	//BUFSIZE	strcpy()
	//Added buffer size check
	if(sizeof(msg1) >= strlen(ENTER_YOUR_PIN))
		strcpy(msg1,ENTER_YOUR_PIN);
	if(sizeof(msg2) >= strlen(ENTER_YOUR_PIN))
		strcpy(msg2,ENTER_YOUR_PIN);
	if(sizeof(msg3) >= strlen(PLEASE_WAIT))
		strcpy(msg3,PLEASE_WAIT);
	taglen=0;
    if(usEMVGetTLVFromColxn(0x9f41, tag9F41_curr, &taglen)==EMV_SUCCESS);
	
	if(usEMVGetTLVFromColxn(0x5a00, tag5A_curr, &taglen)==EMV_SUCCESS);
	
	if(usEMVGetTLVFromColxn(0x9f17, tag9F17_curr, &taglen)==EMV_SUCCESS);

    if(memcmp(tag9F41_curr, tag9F41_prev, 2)==0)
        if(memcmp(tag5A_curr, tag5A_prev, 8)==0)
            //the same transaction, next call
            switch(tag9F17_curr[0])
            {
                case 0:
                    //card blocked
                    pstPin[0]=0;
                    return(0);
                case 1:
                    //last pin try
                    //BUFSIZE	strcpy()
					//Added buffer size check
					if(sizeof(msg1) >= strlen(LAST_PIN_TRY))
                    	strcpy(msg1, LAST_PIN_TRY);
                    break;
                    
                default:
                    strcpy(msg1, INCORRECT_PIN);
                    break;
                    
            }         
	//BUFSIZE
	//Buffer size check is not reqd as tag5A_prev is of size 11 bytes, tag9F41_prev is of 4 bytes
	memcpy(tag5A_prev, tag5A_curr, 8);
	memcpy(tag9F41_prev, tag9F41_curr, 2);
	
	return(inEMVGetPin((char *)pstPin, (char *)CKencr, (char *)CKclear, *index, msg1, msg2, msg3));
}

/*-------------------------------------------------------------------------
    Function:		inEMVPPPresent
    Description:	
    Parameters:		void
    Returns:		
	Notes:
--------------------------------------------------------------------------*/
int inEMVPPPresent(void)
{
	int retVal=RERROR;
	char tmp[10];
	memset(tmp, 0, 10);
	
	sprintf(tmp, "%c11%c", SI, SO);
	if(inEMVPPcom(tmp, tmp, 0)==ROK)
		retVal=ROK;

	return(retVal);
		
}

/*-------------------------------------------------------------------------
    Function:		inEMVBuildCK
    Description:	
    Parameters:		unsigned char *, unsigned char *,, unsigned char *, int *
    Returns:		
	Notes:
--------------------------------------------------------------------------*/
int inEMVBuildCK(unsigned char *pin, unsigned char *CKclear, unsigned char *CKencr, int *index)
{

	/*pin - diversification value*/
	/*CKclear - clear text key*/
	/*CKencr - key encrypted under MK*/
	/*index - index of MK used to encrypt CKclear*/
	
	int retVal=RERROR;
	unsigned char tmp[90];
	int len;
	int ind;
	char pinblock[16+1];
	char PAN[16+1];
	int i;

	get_unpredict_No(tmp, 8);
	sprintf(PAN, "%02.02d%02.02d%02.02d%02.02d%02.02d%02.02d%02.02d%02.02d", 
		tmp[0], tmp[1], tmp[2], tmp[3], tmp[4], tmp[5], tmp[6], tmp[7]);
	PAN[16]=0;

	len=strlen((char *)pin);
	sprintf(pinblock, "0%1.1d%s", len, pin);
	memset(&pinblock[len+2], 'F', 16-len-2); 
	pinblock[16]=0;
	ascii_to_binary(tmp, (unsigned char *)pinblock, 16);
	memset(&tmp[8], 0x00, 8);
	ascii_to_binary(&tmp[8+2], (unsigned char *)&PAN[strlen(PAN)-12-1], 12);
	for(i=0;i<8;i++) tmp[i]^=tmp[i+8];
	hex2asc(CKclear, tmp, 8);
	ind=0;
	while((retVal=inEMVCheckPPMK(ind))!=ROK && ind < 10) ind++;
	if(retVal==ROK)
		{
		*index=ind;
		sprintf((char *)tmp,"%c08%1.1d%c",SI, (char)ind, SO);
		inEMVPPcom((char *)tmp, (char *)tmp, 0);
		sprintf((char *)tmp, "%cZ62.%16.16s%c00000000000000000412N%0.16s%c%0.16s%c", 
		STX, PAN, FS, KEY_DIV_VALUE, FS, pin, ETX);
		retVal=inEMVPPcom((char *)tmp, (char *)tmp, 1);
		strncpy((char *)CKencr, (char *)&tmp[9], 16);
		CKencr[16]=0;
		sprintf((char *)tmp,"%cQ2%c", STX, ETX);
		inEMVPPcom((char *)tmp, (char *)tmp, 0);
		}
	return(retVal);
}

/*-------------------------------------------------------------------------
    Function:		inEMVGetPin
    Description:	
    Parameters:		char *, char *, char *, int, char *, char *, char *
    Returns:		
	Notes:
--------------------------------------------------------------------------*/
int inEMVGetPin(char *buffer, char *key, char *mk, int index, char *msg1, char *msg2, char *msg3)
{
	/*buffer - plain text pin buffer*/
	/*key - encrypted key value 0's if PP MK is used as CK*/
	/*mk - clear key value*/
	/*index - index of PP MK used to encrypt key*/
	/*msg1 - first message*/
	/*msg2 - second message*/
	/*msg3 - processing message*/
	
	int retVal=0;
	int i;
	unsigned char tmp[50] = {0};
	char PAN[16+1];
	unsigned char msg[256+1];

	get_unpredict_No(tmp, 8);
	sprintf(PAN, "%02.02d%02.02d%02.02d%02.02d%02.02d%02.02d%02.02d%02.02d", 
		tmp[0], tmp[1], tmp[2], tmp[3], tmp[4], tmp[5], tmp[6], tmp[7]);
	PAN[16]=0;
	sprintf((char *)msg,"%c08%1.1d%c",SI, (char)index, SO);
	if(inEMVPPcom((char *)msg, (char *)msg, 0)!=ROK)
		return(RERROR);
	SVC_WAIT(50);
	
    memset(msg, 0x00, 256+1);
	
	sprintf((char *)msg, "%cZ62.%8.19s%c%16.16s0412Y%0.16s%c%0.16s%c%0.16s%c", 
	STX, PAN, FS, key, msg1, FS, msg2, FS, msg3, ETX);
	retVal=inEMVPPcom((char *)msg, (char *)msg, 1);
	SVC_WAIT(50);

	sprintf((char *)msg,"%cQ2%c", STX, ETX);
	if(retVal==ROK) retVal=inEMVPPcom((char *)msg, (char *)msg, 0);
	
	memset(tmp,0x00,sizeof(tmp));
	SVC_WAIT(100);
	if(retVal==ROK) 
		{
		if(msg[9]==ETX)
			return(0);	/*no pinblock=pin entry canceled*/
		ascii_to_binary(tmp, (unsigned char *)mk, 16);
		deskey(tmp, 1);
		ascii_to_binary(tmp, &msg[9], 16);
		des(tmp, tmp);

		memset(&tmp[8], 0x00, 8);
		ascii_to_binary(&tmp[8+2], (unsigned char *)&PAN[strlen(PAN)-12-1], 12);
		for(i=0;i<8;i++) tmp[i]^=tmp[i+8];
		if(tmp[0]>12) tmp[0]=12;
		hex2asc((unsigned char *)buffer, &tmp[1], (int)(tmp[0]/2)+1);
		buffer[tmp[0]]=0;
		retVal=tmp[0];	/*pin lenght*/
		}
	else
		retVal=0;
	
	return(retVal);
}

/*-------------------------------------------------------------------------
    Function:		inEMVCheckPPMK
    Description:	
    Parameters:		int
    Returns:		
	Notes:
--------------------------------------------------------------------------*/
int inEMVCheckPPMK(int index)
{
	
	char msg[20];
	int retVal=RERROR;
	
	
	sprintf(msg,"%c04%1.1d%c", SI, (char)index, SO);
	if(inEMVPPcom(msg, msg, 1)==ROK)
		if(msg[3]=='F') 
			retVal=ROK;
		
	return(retVal);
}

/*-------------------------------------------------------------------------
    Function:		inEMVPrintPP
    Description:	
    Parameters:		char *
    Returns:		
	Notes:
--------------------------------------------------------------------------*/
int inEMVPrintPP(char *msg)
{
	char tmp[25];

	sprintf(tmp, "%cZ2%c%0.16s%c", STX, SUB, msg, ETX);
	return(inEMVPPcom(tmp, tmp, 0));

}

/*-------------------------------------------------------------------------
    Function:		inEMVPPcom
    Description:	
    Parameters:		char *, char *, char 
    Returns:		
	Notes:
--------------------------------------------------------------------------*/
int inEMVPPcom(char *command, char *answer, char complex)
{
	int retVal;
	struct Opn_Blk xcomblock;
	unsigned char msg[256+1];
	int i,j;
	int RepSend;
	int RepRec;
	int stxok;
	int etxok;
	int ackok;
	int nakok;
	int loops;
	int len;
	int ok;
	char chr;
	int xcom;
	retVal=RERROR;
	xcom = open(DEV_COM2, 0);
	xcomblock.rate = Rt_1200;
	xcomblock.format = Fmt_A7E1;
	xcomblock.protocol = P_char_mode;
	set_opn_blk(xcom,(struct Opn_Blk *)(& xcomblock));
	
	loops=0;
	RepSend=1;
	while(RepSend<4)
		{
		if(loops>MAX_LOOPS) return(RERROR);
		command[i=strlen(command)]=LRC_calc(&command[1], strlen(command)-1);
		command[i+1]=0;
		write(xcom, command, strlen(command));
		stxok=-1;
		etxok=-1;
		ackok=-1;
		nakok=-1;
		i=0;
		j=0;
		RepRec=1;
		while(RepRec<4)
			{
			if(loops>MAX_LOOPS) return(RERROR);
			SVC_WAIT(300);
			i=0;
			memset(msg, 0x00, 256+1);
			ok=1;
			len=0;
			while((ok) && (i<256))
				{
				if(j>1000) return(RERROR);		//timeout
				SVC_WAIT(30);
				read(xcom, (char *)&msg[i], 1) ;
	
				if(msg[i]!=0)
					{
					len++;
					switch(msg[i])
						{
						case NAK:						
							nakok=i;
							break;
						case ETX:
						case SO:
							etxok=i;
							ok=0;
							read(xcom, (char *)&msg[i+1], 1);
							len++;
							break;
						case STX:
						case SI:
							j=0;
							stxok=i;
							break;
						case ACK:
							j=0;
							ackok=i;
							if(!complex) ok=0;
							break;
						}
					
					i++;
					}
				j++;
				}
				
			if(len>0)
				{
				chr=0;

				if(nakok>=0)
					{
					RepRec=4;
					RepSend++;
					chr=0;
					}
				if(ackok>=0)
                	if(stxok>=0)
					{
                         if(etxok>=0)
						 {
							if(LRC_calc((char *)&msg[stxok+1], etxok-stxok)==msg[etxok+1])
								{
									//BUFSIZE strcpy() CodeReview:Check for pointer validity
									//Buffer size check cannot be done as answer is a pointer
									if(answer != NULL)
										strcpy(answer, (char *)&msg[stxok]);
									RepRec=4;
									RepSend=4;
									chr=ACK;
									retVal=ROK;
								}
							else
								{
									RepRec++;
									chr=NAK;
								}
						 }
						 else
						 {
							RepRec++;
							chr=NAK;
						 }
					}
                    else
					{
						RepRec=4;
						RepSend=4;
						retVal=ROK;
						chr=0;
					}
                }

			if(chr!=0) 
				write(xcom, &chr, 1);
			loops++;
			}
		loops++;
		}

	
	SVC_WAIT(200);
	close(xcom);
	return(retVal);
}

