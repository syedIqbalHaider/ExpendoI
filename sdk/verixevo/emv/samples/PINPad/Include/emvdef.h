/*
 * FILENAME       : emvdef.h
 * PRODUCT        : Verix V.
 * VERSION        : 4.0.0
 * AUTHOR(s)      : T_Arvind_G1
 * CREATED        : 20/Dec/06
 *
 * DESCRIPTION    : EMVTransaction class provides methods to perform EMV 4.1.
 * transaction as per EMV application specifications Ver 4.1.
 * This class contains all the high level methods to handle an 
 * EMV transaction. This class uses other classes listed in this 
 * library to perform a transaction.
 * 
 *
 * CONTENTS   :
 * MODIFICATION HISTORY    :
 *
 * #    Date        Who         History
 * ---  --------    ----------  ----------------------------------------
 
 *
 * Copyright (C) 2006 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language, in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual or otherwise, without the prior written
 * permission of VeriFone Inc.
 *                                                2099 Gateway Place
 *             					                  Suite 600
 * 					                              San Jose  CA  95110 
 *              				                  USA
 *
 *
 *****************************************************************************/

#ifndef _EMVDEF_H
#define _EMVDEF_H
#define EMV_SUCCESS 0

#define ROK				0
#define RERROR			-1




#define STX	0x02
#define ETX	0x03
#define EOT	0x04
#define ACK	0x06
#define NAK	0x15
#define SUB	0x1A
#define FS	0x1C
#define SI	0x0F
#define SO	0x0E

typedef struct
{
	char		CKclear[16];
	char		CKencr[16];
	int		index;
} _emvck;

#endif
