/*
 * FILENAME       : APACSpin.h
 * PRODUCT        : Verix V.
 * VERSION        : 4.0.0
 * AUTHOR(s)      : T_Arvind_G1
 * CREATED        : 20/Dec/06
 *
 * DESCRIPTION    : EMVTransaction class provides methods to perform EMV 4.1.
 * transaction as per EMV application specifications Ver 4.1.
 * This class contains all the high level methods to handle an 
 * EMV transaction. This class uses other classes listed in this 
 * library to perform a transaction.
 * 
 *
 * CONTENTS   :
 * MODIFICATION HISTORY    :
 *
 * #    Date        Who         History
 * ---  --------    ----------  ----------------------------------------
 
 * 1   13 July 07  Purvin_P1  RETURN_TYPE : CHANGE from int to USHORT.
 * Copyright (C) 2006 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language, in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual or otherwise, without the prior written
 * permission of VeriFone Inc.
 *                                                2099 Gateway Place
 *             					                  Suite 600
 * 					                              San Jose  CA  95110 
 *              				                  USA
 *
 *
 *****************************************************************************/

#ifndef _APACSPIN_H
#define _APACSPIN_H

#define ADD_NEW_KEY 0
#define BUILD_PINBLOCK 1
#define RESET_KEY 2

#define ROK 0
#define RERROR -1


int inAPACS_OWF(unsigned char *D_value, unsigned char *K_value, unsigned char *result);
unsigned char makeOdd(unsigned char oddByte);
//RETURN_TYPE : CHANGE from int to USHORT
unsigned short OnlinePIN(unsigned char *buffer, int keyset, int operation);
int inAPACSclearPINblock(char *track2, char *pin, char *pinblock);


#endif 
