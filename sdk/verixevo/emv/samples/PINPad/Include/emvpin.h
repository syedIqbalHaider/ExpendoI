/*
 * FILENAME       : emvpin.h
 * PRODUCT        : Verix V.
 * VERSION        : 4.0.0
 * AUTHOR(s)      : T_Arvind_G1
 * CREATED        : 20/Dec/06
 *
 * DESCRIPTION    : EMVTransaction class provides methods to perform EMV 4.1.
 * transaction as per EMV application specifications Ver 4.1.
 * This class contains all the high level methods to handle an 
 * EMV transaction. This class uses other classes listed in this 
 * library to perform a transaction.
 * 
 *
 * CONTENTS   :
 * MODIFICATION HISTORY    :
 *
 * #    Date        Who         History
 * ---  --------    ----------  ----------------------------------------
 
 *
 * Copyright (C) 2006 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language, in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual or otherwise, without the prior written
 * permission of VeriFone Inc.
 *                                                2099 Gateway Place
 *             					                  Suite 600
 * 					                              San Jose  CA  95110 
 *              				                  USA
 *
 *
 *****************************************************************************/

#ifndef _EMVPIN_H
#define _EMVPIN_H

#define ROK 0
#define RERROR -1

short getPPUsrPin(unsigned char * pstPin, unsigned char *CKclear, unsigned char *CKencr, int *index);
/*function called by EMV library*/

int inEMVBuildCK(unsigned char *pin, unsigned char *CKclear, unsigned char *CKencr, int *index);
/*
initialise keys neceseary for communication PinPad<->terminal
pin - value for key diversification - have to be exactly same like the one entered from pinpad keyboard
CKclear - generated key in clear form
CKencr - generated key in encrypted form
index - index of MK used for encryption
*/

int inEMVGetPin(char *buffer, char *key, char *mk, int index, char *msg1, char *msg2, char *msg3);
/*
asks for cardholder pin, returns it a 0 ended string
buffer - buffer to store plain text pin,
key - encrypted key,
mk - clear key,
index - index of PinPad MK used for encryption
msg1..3 - messages dispaled on PP screen
*/

int inEMVCheckPPMK(int index);
/* checks if index MK is present and valid*/

int inEMVPrintPP(char *msg);
/*displays the msg (max 16 characters) on PP dispaly*/

int inEMVPPcom(char *command, char *answer, char complex);
/*provides communication with PP
command - buffer containing command to send to PP (without LRC)
answer - answer from pinpad (can use the same buffer like command)
complex:
0 - command if confirmed by ACK only
1 - waits for ACK followed by complete response message
*/


void deskey(unsigned char *, short);
void des(unsigned char *, unsigned char *);
void ascii_to_binary(unsigned char *dest, unsigned char *src, int length);
void hex2asc(unsigned char *outp, unsigned char *inp, int length);
unsigned char LRC_calc(char *buffer, int len);
void get_unpredict_No(unsigned char *inpbuff, int size);

int inEMVPPPresent(void);

#endif
