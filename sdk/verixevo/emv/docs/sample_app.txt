/*****************************************************************************/
/** Copyright (C) 2006 by VeriFone Inc.
 * All rights reserved. No part of this software may be reproduced,transmitted,
 * transcribed, stored in a retrieval system, or translated into any language
 * or computer language, in any form or by any means, electronic, mechanical,
 * magnetic, optical, chemical, manual or otherwise, without the prior written
 * permission of VeriFone Inc.
 *                                                
 
	2099 Gateway Place
	Suite 600
	San Jose CA 95110 
	USA	 
                                   */
/*****************************************************************************/


/*
;|===========================================================================+
;| FILE NAME:  | SLE4428test.c
;|-------------+-------------------------------------------------------------+
;| DEVICE:     | Vx terminals
;|             |
;|-------------+-------------------------------------------------------------+
;| DESCRIPTION:| Tests for SLE4428 Sync Card Driver
;|             |
;|             |
;| LANGUAGE    | C  
;|-------------+-------------------------------------------------------------+
;| VERSION:    | 1.0
;|-------------+-------------------------------------------------------------+
;| DATE:       | October 25, 2006
;|-------------+-------------------------------------------------------------+
;| REVISION    | 1.0  : Pavan_K1
;| HISTORY:    |
;|             |
;|-------------+-------------------------------------------------------------+
;| COMMENTS:   |
;|             |
;|===========================================================================+
*/




/*
;|===========================================================================+
;| NOTE: | SYSTEM INCLUDE
;|       |
;|===========================================================================+
*/

#include "stdio.h"
#include "string.h"
//#include "libVoy.h"
//#include "TestMain.h"
#include "SLE4428.h"
#include "4428test.h"
#include "cardslot.h"
#include "ctestapp.h"
//#include "gpm896.h"
//#include <errno.h>
#include <svc.h>
/*
;|===========================================================================+
;|===========================================================================+
*/



/*
;|===========================================================================+
;| NOTE: | DEFINITION
;|       |
;|===========================================================================+
*/

#define ONE_SECOND         114
#define DISPLAY_DELAY      2*ONE_SECOND

/*------------------------------------------------------------------------------
 * Structure Definitions
 * ---------------------------------------------------------------------------*/
typedef struct
{
   unsigned char data;
   unsigned char protBit;
}DATA_PROT;


/*
;|===========================================================================+
;| NOTE: | LOCAL FUNCTIONS
;|       |
;|===========================================================================+
*/
static int SubmitPSC( unsigned char *pscCode, unsigned char checkCounter );
static void ReportError( char *msg, int status );
static void ReportTitle( char *msg );


/*
;|===========================================================================+
;| NOTE: | GLOBAL FUNCTIONS
;|       |
;|===========================================================================+
*/

/*
;|===========================================================================+
;| NOTE: | EXTERNAL FUNCTIONS
;|       |
;|===========================================================================+
*/
extern int dbprintf( const char *, ... );

/*
;|===========================================================================+
;| NOTE: | Global VARIABLES
;|       |
;|===========================================================================+
*/

#define MAX_PSC_LIST 2
/* entry zero is not a PSC but is used for navigation instructions.
   entry zero is not a test but is used for navigation instructions. */

const PSC_STRUCT pscList[] = {
   0xDE, 0xAD, 0x00,                            // dummy entry for selection 0
   0xAA, 0xAA, 0xAA,                            // PSC 1
   0xFF, 0xFF, 0xFF                             // PSC 2
   };

const unsigned char driverComparator[] = {"SLE4428"};

extern unsigned char ucCmdData[100] = {0};
extern unsigned char ucRespData[300] = {0};
extern unsigned char ucRespLen = 0;
extern unsigned char ucRetVal;
extern unsigned long ulCmdLen = 0;
extern unsigned short ulRespLen = 0;
extern unsigned char cardSelected;
extern int h_console;
extern int h_conn;
extern int hScreen ;

unsigned char currentPSC;
unsigned char RefBuf[300];
unsigned char atrData[25];
unsigned char dispBuf[25];
unsigned char ucSetData[5];
unsigned long memSize = 0;
unsigned long ulAtrLen = 0;
unsigned int  cardType;
unsigned char *pscPtr;
unsigned char PSCBuffer[6];
unsigned char currentTest;
unsigned int ErrCount;
unsigned char psc[2] = {0xAA, 0xAA};


/********************************************************************/
/************************** SLE 4428 Main Function ******************/
/********************************************************************/

int SLE4428main(void)
{
		
	write_at("Start Test",strlen("Start Test"), 1, 1);
    ErrCount = 0;
	 
   //-------------------------------------------------
   // Report Error count
   //-------------------------------------------------
    SLE4428_AtrTest();
	if ( ErrCount )
	{
	 fail_msg();
	}
	else
	{
	 pass_msg();
	}

	ErrCount = 0;
	SLE4428_MainMemTest();
	if ( ErrCount )
	{
	 fail_msg();
	}
	else
	{
	 pass_msg();
	}
	ErrCount = 0;
	SLE4428_ProtectionMemTest();
	if ( ErrCount )
	{
	 fail_msg();
	}
	else
	{
	 pass_msg();
	}
	SLE4428_ProtectCmpMemTest();
	if ( ErrCount )
	{
	 fail_msg();
	}
	else
	{
	 pass_msg();
	}
	
	return 0;
}

/*
;|=========================================================================+
;| TEST       |SLE4428_ChangePSC
 *            |
 * Description| Select PSC from PSC list above
 *            |
;|=========================================================================+
*/

void SLE4428_ChangePSC( void )
{
#if 0
   TestClearDisplay ();
   sprintf( (char *)dispBuf, "CurPSC = 0x%X%X", pscPtr[0], pscPtr[1] );
   write_at((char *)dispBuf, strlen((char *)dispBuf),1, 2);
   SelectPSC( );

   pscPtr = (unsigned char *)&pscList[currentPSC];
   sprintf( (char *)dispBuf, "NewPSC = %X%X", pscPtr[0], pscPtr[1] );
   write_at((char *)dispBuf, strlen((char *)dispBuf),1, 2);
   ErrCount = 0;
   WaitEnterPress();
#endif
}


/*************************************************************************************************************/
/****************************************** TEST FUNCTIONS ***************************************************/
/*************************************************************************************************************/
/*
;|=========================================================================+
;| TEST       |SLE4428_AtrTest
 *            |
 * Description| Power up and reset card reporting ATR data to display
 *            |
;|=========================================================================+
*/
void SLE4428_AtrTest( void )
{
   int iStatus;

   ReportTitle( "Get ATR Bytes");
   /*----------------------------------------------------
    * Get ATR bytes
    * --------------------------------------------------*/
   
   
   /******************************************************/
   /************ Call the GET_ATR_BYTES Macro ************/
   /******************************************************/
   
   SLE4428_GET_ATR_BYTES(ucCmdData,ulCmdLen);
   memset (ucRespData, 0x00, sizeof (ucRespData));
   iStatus = Transmit_APDU(1, ucCmdData, ulCmdLen, ucRespData, &ulRespLen);
   if( iStatus != CARDSLOT_SUCCESS)
   {
      ErrCount++;
      sprintf( (char *)dispBuf, "ATRERR %X", iStatus );
      write_at((char *)dispBuf, strlen((char *)dispBuf),  1, 3 );
   }
   else
   {
	   write_at("Transmit Success", 20, 1,1);
	   SVC_WAIT(3000);
   }

   if ( ulRespLen > 0 )
   {
      sprintf( (char *)dispBuf, "ATR=%02X%02X%02X%02X ln=%X", ucRespData[0], ucRespData[1],
                              ucRespData[2], ucRespData[3], ulRespLen );
	  
      clrscr();
      write_at((char *) dispBuf, strlen((char *)dispBuf), 1, 3 );
   }
   else
   {
      write_at( "NO ATR Data   ", strlen("NO ATR Data   "),  1, 3);
   }

}



/*
;|=========================================================================+
;| TEST       |SLE4428_MainMemTest
 *            |
 * Description| Write/Read Test of Main Memory area
 *            |
;|=========================================================================+
*/
void SLE4428_MainMemTest( void )
{
   unsigned char cmpByte;
   unsigned char ucCmdBuf[255] = {0};
   unsigned int i;
   int iStatus;

   /****************************************************************/
   /********** Read the complete 256 bytes of the data *************/
   /****************************************************************/
  
   SLE4428_READ_UNPROTECTED(ucCmdData, 0x00, 0x00, 0x00, ulCmdLen);
   memset (ucRespData, 0x00, sizeof (ucRespData));
   iStatus = Transmit_APDU(1, ucCmdData, ulCmdLen, ucRespData, &ulRespLen);
   sprintf( (char *)dispBuf, "RespLen %X, %d", ulRespLen, ulRespLen );
   write_at((char *)dispBuf, strlen((char *)dispBuf), 1, 5 );
   if ( iStatus != CARDSLOT_SUCCESS)
   {
      ReportError( "ReadFail", iStatus );
      return;
   }

   /*----------------------------------------------------
    * Read 16 bytes starting at address 32
    * --------------------------------------------------*/
   
   
   SLE4428_READ_UNPROTECTED(ucCmdData, 0x02, 0x00, 0x10, ulCmdLen);
   memset (ucRespData, 0x00, sizeof (ucRespData));
   iStatus = Transmit_APDU(1, ucCmdData, ulCmdLen, ucRespData, &ulRespLen);
   sprintf( (char *)dispBuf, "RespLen %X, %d", ulRespLen, ulRespLen );
   write_at((char *)dispBuf, strlen((char *)dispBuf), 1, 5 );
   if ( iStatus != CARDSLOT_SUCCESS)
   {
      ReportError( "ReadFail", iStatus );
      return;
   }

   memSize = ulRespLen;
   
  /*---------------------------------------------------
   * submit secret code for write access
   * --------------------------------------------------*/
   
   ReportTitle( "Submit PSC" );
   if ( SubmitPSC( psc, 1 ) )
   {
   		return;  		//abort if PSC fails
   }

   clrscr();
   ReportTitle("Erase Card 0xFF");

   for ( i=0; i<memSize; i++ )
   {
      ucCmdBuf[i] = 0xFF;                    // set all to FFh
   }

   ucCmdData[i+5] = 0;                          // Le byte
   ulCmdLen = 6 + memSize;                      // command hdr+data length+le byte

   SLE4428_WRITE_ERASE_UNPROTECTED(ucCmdData, 0X02, 0X00, memSize, ucCmdBuf, ulCmdLen);
   memset (ucRespData, 0x00, sizeof (ucRespData));
   iStatus = Transmit_APDU(1, ucCmdData, ulCmdLen, ucRespData, &ulRespLen);
   if ( iStatus != CARDSLOT_SUCCESS)
   {
      ReportError( "1WrFFFail", iStatus );
      return;
   }

   /*----------------------------------------------------
    * Read back incrementing pattern from different start
    * addresses and validate the data returned.
    * --------------------------------------------------*/

   clrscr();
   ReportTitle("Read/Cmp Erased Card");
   
   ////////////////////////////////////////////////////////
   // READ BACK/COMPARE FROM ADDRESS 32
   ////////////////////////////////////////////////////////
    
   SLE4428_READ_UNPROTECTED(ucCmdData, 0x02, 0x00, memSize, ulCmdLen);
   memset (ucRespData, 0x00, sizeof (ucRespData));
   iStatus = Transmit_APDU(1, ucCmdData, ulCmdLen, ucRespData, &ulRespLen);
   if ( iStatus != CARDSLOT_SUCCESS)
   {
      sprintf( (char *)dispBuf, "RdEraseFail %X", iStatus );
      clrscr();
      write_at((char *)dispBuf, strlen((char *)dispBuf), 1, 5 );

      WaitEnterPress();
      ErrCount++;
      return;
   }

   // base address is beginning of unprotected space (addr 32)
   for ( i=0; i<memSize; i++ )
   {
      if ( ucRespData[i] != 0xFF )
      {
         sprintf( (char *)dispBuf, "CmpEraseFail %X", i );
         clrscr();
		 write_at((char *)dispBuf, strlen((char *)dispBuf), 1, 5 );
         ErrCount++;
         return;
      }
   }

   /*----------------------------------------------------
    * 	Write incrementing pattern to entire card
    * --------------------------------------------------*/

   clrscr();
   ReportTitle("Write Inc Pattern");

   for ( i=0; i<memSize; i++ )
   {
      ucCmdBuf[i] = i;                       	// send incrementing pattern 
   }

   ucCmdData[i+5] = 0;                          // Le byte
   ulCmdLen = 6+memSize;                        // command hdr+data length+le byte

   SLE4428_WRITE_ERASE_UNPROTECTED(ucCmdData, 0X02, 0X00, memSize, ucCmdBuf, ulCmdLen);
   iStatus = Transmit_APDU(1, ucCmdData, ulCmdLen, ucRespData, &ulRespLen);
   if ( iStatus != CARDSLOT_SUCCESS)
   {
      sprintf( (char *)dispBuf, "1WrIncFail %X", iStatus );
	  write_at((char *)dispBuf, strlen((char *)dispBuf), 1, 3 );
      ErrCount++;
      return;
   }

   /*----------------------------------------------------------
    * 	Read back incrementing pattern from different start
    * 	addresses and validate the data returned.
    * --------------------------------------------------------*/

   clrscr();
   ReportTitle("Read/Cmp Inc Pattern");
   
   ////////////////////////////////////////////////////////
   // READ BACK/COMPARE FROM ADDRESS 32
   ////////////////////////////////////////////////////////
   
   SLE4428_READ_UNPROTECTED(ucCmdData, 0x02, 0x00, memSize, ulCmdLen);
   iStatus = Transmit_APDU(1, ucCmdData, ulCmdLen, ucRespData, &ulRespLen);
   if ( iStatus != CARDSLOT_SUCCESS)
   {
      sprintf( (char *)dispBuf, "0RdIncFail %X", iStatus );
	  write_at((char *)dispBuf, strlen((char *)dispBuf), 1, 3 );
      ErrCount++;
      return;
   }

   // base address is beginning of unprotected space (addr 32)
   for ( i=0; i<memSize; i++ )
   {
      cmpByte = (unsigned char)i;
      if ( ucRespData[i] != cmpByte )
      {
         sprintf( (char *)dispBuf, "0CmpIncFail %X", i );
		 write_at((char *)dispBuf, strlen((char *)dispBuf), 1, 3 );
         dbprintf( "%s cardData=%X, cmpByte=%X\n",dispBuf, ucRespData[i], cmpByte);
         WaitEnterPress();
         ErrCount++;
         return;
      }
   }
}


/*
;|=========================================================================+
;| TEST       |SLE4428_ProtectionMemTest
 *            |
 * Description| Write/Read Test of Protection Memory Area
 *            |
;|=========================================================================+
*/
void SLE4428_ProtectionMemTest( void )
{
   unsigned int iStatus, idx, readSize;
   unsigned char protByte[2] = {0};
   unsigned char msbAddr = 0x00, lsbAddr = 0x00;
   DATA_PROT *dataProt;
   
   if ( SubmitPSC( psc, 1 ) )
   {
   		return;  // abort if PSC fails
   }

   /*----------------------------------------------------
    * 		Read MFG memory area with protection bits
    * --------------------------------------------------*/
   ReportTitle( "Read Unprot Mem" );
   while ( 1 )
   {
      ////////////////////////////////////////////////////////
      // READ Main Memory
      ////////////////////////////////////////////////////////
      
	  SLE4428_READ_PROTECTED(ucCmdData, msbAddr, lsbAddr, 128, ulCmdLen);
      iStatus = Transmit_APDU(1, ucCmdData, ulCmdLen, ucRespData, &ulRespLen);
      if ( iStatus != CARDSLOT_SUCCESS)
      {
         ReportError( "RdMemFail1", iStatus );
         return;
      }

  	  readSize = ulRespLen;
      dataProt = (DATA_PROT *)&ucRespData;          // user pointer conversion to convert to long

      /*----------------------------------------------------
       * 			Choose byte to modify
       * --------------------------------------------------*/
      ReportTitle( "Find UnProtect Byte" );

      for ( idx=0; idx<readSize; idx++ )
      {
         if ( dataProt[idx].protBit != 0 )      // find erased protection bit
         {
            break;
         }
      }

      if ( idx >= readSize )                    // all protection bits written in buffer
      {
         if ( lsbAddr )                         // already read 2nd blk of 128
         {
            lsbAddr = 0;
            msbAddr++;
         }
         else
         {
            lsbAddr = 128;                      // read next block of 128
         }

         if ( msbAddr > 1 )                         // no unprotected space left.  up into mem test area.
         {
			ReportTitle( "All Protected" );
            return;
         }
         continue;
      }

      break;                                    // get here if erased protection bit found
   }  /// end while 1

   protByte[0] = dataProt[idx].data^0xFF;          // invert bits
   sprintf( (char *)dispBuf, "Byte%X = %02X new= %02X", idx, dataProt[idx].data, protByte[0] );
   write_at( (char *)dispBuf, strlen((char *)dispBuf),1, 2);
   sprintf( (char *)dispBuf, "Address %02X%02X", msbAddr, lsbAddr+idx );
   write_at( (char *)dispBuf, strlen((char *)dispBuf),1, 3);

   /*-------------------------------------------------------
    * 	Write selected byte out and clear protection bit
    * -----------------------------------------------------*/
   
   ReportTitle( "Update Protected Byte" );

   SLE4428_WRITE_ERASE_PROTECTED(ucCmdData, msbAddr, lsbAddr+idx, 1, &protByte, ulCmdLen);
   memset (ucRespData, 0x00, sizeof (ucRespData));
   iStatus = Transmit_APDU(1, ucCmdData, ulCmdLen, ucRespData, &ulRespLen);
   if ( iStatus != CARDSLOT_SUCCESS)
   {
      ReportError( "WrINVFail", iStatus );
      return;
   }

   SVC_WAIT( DISPLAY_DELAY );

   /*------------------------------------------------------
    * 	Read back byte and check protection bit written
    * -----------------------------------------------------*/
   
   ReportTitle( "Read updated Byte" );
  
   SLE4428_READ_PROTECTED(ucCmdData, msbAddr, lsbAddr+idx, 1, ulCmdLen);
   iStatus = Transmit_APDU(1, ucCmdData, ulCmdLen, ucRespData, &ulRespLen);
   if ( (iStatus != CARDSLOT_SUCCESS) || (ulRespLen != 2) )
   {
      ReportError( "RdMemFail2", iStatus );
      return;
   }

   if ( ucRespData[0] != protByte[0] )
   {
      sprintf( (char *)dispBuf, "CmpMemEr %X!=%X", ucRespData[0], protByte[0] );
	  write_at( (char *)dispBuf, strlen((char *)dispBuf),1, 3 );
      ErrCount++;
      return;
   }

   if ( ucRespData[1] != 0 )                    // protection bit written?
   {
      sprintf( (char *)dispBuf, "CmpProtEr %X!=0", ucRespData[1] );
	  write_at( (char *)dispBuf, strlen((char *)dispBuf),1, 3 );
      ErrCount++;
      return;
   }

   SVC_WAIT( DISPLAY_DELAY );

}

/*
;|=========================================================================+
;| TEST       |SLE4428_ProtectCmpMemTest
 *            |
 * Description| Write/Read Test of Protection Memory Area with compare feature
 *            |
;|=========================================================================+
*/
void SLE4428_ProtectCmpMemTest( void )
{

   unsigned int iStatus, idx, readSize;
   unsigned char msbAddr=0, lsbAddr=0;
   unsigned char protByte[2] = {0};
   DATA_PROT *dataProt;

   if ( SubmitPSC( psc, 1 ) )
   {
      return;  // abort if PSC fails
   }

   /*----------------------------------------------------
    * 	Read MFG memory area with protection bits
    * --------------------------------------------------*/
   ReportTitle( "Read wCmp Mem");
   while ( 1 )
   {
      ////////////////////////////////////////////////////////
      // READ Main Memory
      ////////////////////////////////////////////////////////
      
	  SLE4428_READ_PROTECTED(ucCmdData, msbAddr, lsbAddr, 128, ulCmdLen);
      iStatus = Transmit_APDU(1, ucCmdData, ulCmdLen, ucRespData, &ulRespLen);
      
      if ( iStatus != CARDSLOT_SUCCESS)
      {
         sprintf( (char *)dispBuf, "RdMemFail1 %X", iStatus );
		 write_at( (char *)dispBuf, strlen((char *)dispBuf),1, 3 );
         return;
      }

      readSize = ulRespLen;
      dataProt = (DATA_PROT *)&ucRespData;          // user pointer conversion to convert to long

      /*----------------------------------------------------
       * 	Choose byte to modify
       * --------------------------------------------------*/
      ReportTitle( "Find UnProtect Byte");
      
      for ( idx=0; idx<readSize; idx++ )
      {
         if ( dataProt[idx].protBit != 0 )      // find erased protection bit
         {
            break;
         }
      }

      if ( idx >= readSize )                         // all protection bits written in buffer
      {
         if ( lsbAddr )                         // already read 2nd blk of 128
         {
            lsbAddr = 0;
            msbAddr++;
         }
         else
         {
            lsbAddr = 128;                      // read next block of 128
         }

         if ( msbAddr > 1 )                         // no unprotected space left.  up into mem test area.
         {
			ReportTitle( "All Protected" );
            return;
         }
         continue;
      }

      break;                                    // get here if erased protection bit found
   }  /// end while 1

   protByte[0] = dataProt[idx].data;  
   sprintf( (char *)dispBuf, "Byte%X = %02X", idx, protByte[0] );
   write_at( (char *)dispBuf, strlen((char *)dispBuf),1, 2);
   
   sprintf( (char *)dispBuf, "Address %02X%02X", msbAddr, lsbAddr+idx );
   write_at( (char *)dispBuf, strlen((char *)dispBuf),1, 3);
   
   SVC_WAIT( DISPLAY_DELAY );

   /*-----------------------------------------------------
    * 	Write selected byte out and clear protection bit
    * --------------------------------------------------*/
   ReportTitle( "Wr Protect wCmp");
   
   SLE4428_PROTECT_COMPARE(ucCmdData, msbAddr, lsbAddr+idx, 1, &protByte, ulCmdLen);
   memset (ucRespData, 0x00, sizeof (ucRespData));
   iStatus = Transmit_APDU(1, ucCmdData, ulCmdLen, ucRespData, &ulRespLen);
   
   if ( iStatus != CARDSLOT_SUCCESS)
   {
      sprintf( (char *)dispBuf, "WrFail %X", iStatus );
      write_at( (char *)dispBuf, strlen((char *)dispBuf),1, 3);
      ErrCount++;
      return;
   }
   SVC_WAIT( DISPLAY_DELAY );

   /*-----------------------------------------------------
    * 	Read back byte and check protection bit written
    * --------------------------------------------------*/
   ReportTitle( "Read updated Byte");

   SLE4428_READ_PROTECTED(ucCmdData, msbAddr, lsbAddr+idx, 1, ulCmdLen);
   iStatus = Transmit_APDU(1, ucCmdData, ulCmdLen, ucRespData, &ulRespLen);
   
   if ( (iStatus != CARDSLOT_SUCCESS) || (ulRespLen != 2) )
   {
      sprintf( (char *)dispBuf, "RdMemFail2 %X", iStatus );
      write_at( (char *)dispBuf, strlen((char *)dispBuf),1, 3);
      ErrCount++;
      return;
   }

   if ( ucRespData[0] != protByte[0] )
   {
      sprintf( (char *)dispBuf, "CmpMemEr %X!=%X", ucRespData[0], dataProt[idx].data );
	  write_at( (char *)dispBuf, strlen((char *)dispBuf),1, 3);
      ErrCount++;
      return;
   }

   if ( ucRespData[1] != 0 )                    // protection bit written?
   {
      sprintf( (char *)dispBuf, "CmpProtEr %X!=0", ucRespData[1] );
	  write_at( (char *)dispBuf, strlen((char *)dispBuf),1, 3);
      ErrCount++;
      return;
   }

   SVC_WAIT( DISPLAY_DELAY );

}

/*
;|=========================================================================+
;| TEST       | SLE4428_NewPSCTest
 *            |
 * Description| New PSC Test
 *            |
;|=========================================================================+
*/

void SLE4428_NewPSCTest (void)
{
	int iStatus;
	unsigned char ucPsc[2] = {0};
	if ( SubmitPSC( psc, 1 ) )
	{
		return;  // abort if PSC fails
	}
	
	/*----------------------------------------------------
    * 				Change PSC
    * --------------------------------------------------*/
   	ReportTitle( "Invert PSC");
   
	////////////////////////////////////////////////////////
	// Change PSC on card to inverse of current
	////////////////////////////////////////////////////////
	ucPsc[0] = psc[0]^0xFF; 
	ucPsc[1] = psc[1]^0xFF; 
	
	SLE4428_SET_PSC(ucCmdData, ucPsc, ulCmdLen);
	iStatus = Transmit_APDU(1, ucCmdData, ulCmdLen, ucRespData, &ulRespLen);

	if ( iStatus != CARDSLOT_SUCCESS)
	{
	  sprintf( (char *)dispBuf, "SetPSCFail %X", iStatus );
	  write_at( (char *)dispBuf, strlen((char *)dispBuf),1, 3);
	  ErrCount++;
	  return;
	}

	/*----------------------------------=------------------
	* 			Read back PSC bytes
	* --------------------------------------------------*/
	ReportTitle( "Read Invert PSC");

	SLE4428_READ_UNPROTECTED(ucCmdData, 0x03, 0xFE, 2, ulCmdLen);
	iStatus = Transmit_APDU(1, ucCmdData, ulCmdLen, ucRespData, &ulRespLen);

	if (iStatus != CARDSLOT_SUCCESS)                       // read error counter failed
	{
	  sprintf( (char *)dispBuf, "RdPSCMemEr %X", iStatus );
	  write_at( (char *)dispBuf, strlen((char *)dispBuf),1, 3);
	  ErrCount++;
	  return ;
	}

	if ( (ucRespData[0] != (psc[0]^0xFF)) || (ucRespData[1] != (psc[1]^0xFF)) )
	{
	  sprintf( (char *)dispBuf, "PSCCmpFail %02X %02X", ucRespData[0], ucRespData[1] );
	  write_at( (char *)dispBuf, strlen((char *)dispBuf),1, 3);
	  ErrCount++;
	  return ;
	}

	////////////////////////////////////////////////////////
	// Change PSC back to original value
	////////////////////////////////////////////////////////
		
	ucPsc[0] = psc[0]; 
	ucPsc[1] = psc[1]; 
	SLE4428_SET_PSC(ucCmdData, ucPsc, ulCmdLen);
	iStatus = Transmit_APDU(1, ucCmdData, ulCmdLen, ucRespData, &ulRespLen);

	if ( iStatus != CARDSLOT_SUCCESS)
	{
	  sprintf( (char *)dispBuf, "1SetPSCFail %X", iStatus );
	  write_at( (char *)dispBuf, strlen((char *)dispBuf),1, 3);
	  ErrCount++;
	  return;
	}
}


/*
;|=========================================================================+
;| TEST       |SLE4428_PSCVerificationTest
 *            |
 * Description| Test ability to change PSC
 *            |
;|=========================================================================+
*/
void SLE4428_PSCVerificationTest( void )
{

   ReportTitle( "PSC Submit Check" );
   if ( SubmitPSC( psc, 1 ) )
   {
     return;  // abort if PSC fails
   }
   ReportTitle( "PSC Submit Success" );
   SVC_WAIT( DISPLAY_DELAY );
}


/*************************************************************************************************************/
/******************************************** UTILTY FUNCTIONS ***********************************************/
/*************************************************************************************************************/
/*
;|===========================================================================+
;| UTILTY     |static char SubmitPSC( unsigned char *pscCode )
;|------------+--------------------------------------------------------------+
;| DESCRIPTION| Submits 3 byte pscCode for verification to the smart card.
;|            |
;|===========================================================================+
*/
static int SubmitPSC(unsigned char *pscCode, unsigned char checkCounter)
{

   int iStatus;

   /*----------------------------------------------------
    * 	Read Error Counter and give option to continue.
    * --------------------------------------------------*/
   // Report error count
   SLE4428_READ_UNPROTECTED(ucCmdData, 0x03, 0xFD, 0x01, ulCmdLen);
   iStatus = Transmit_APDU(1, ucCmdData, ulCmdLen, ucRespData, &ulRespLen);
   
   if ( iStatus != CARDSLOT_SUCCESS)
   {
      sprintf( (char *)dispBuf, "SecMemFail %X", iStatus );
	  write_at( (char *)dispBuf, strlen((char *)dispBuf),1, 3);
      ErrCount++;
      return -1;
   }

   if ( ulRespLen != 1 )
   {
      sprintf( (char *)dispBuf, "ErrCntFail %X", ulRespLen );
	  write_at( (char *)dispBuf, strlen((char *)dispBuf),1, 3);
      ErrCount++;
      return -1;
   }

   if ( ucRespData[0] != 0xFF )              // not full count, give chance to quit
   {
      sprintf( (char *)dispBuf, "ErrCount=0x%X", ucRespData[0] );
      write_at( (char *)dispBuf, strlen((char *)dispBuf),1, 3);
	  sprintf( (char *)dispBuf, "%s", "1=cont. other=abort" );
      write_at( (char *)dispBuf, strlen((char *)dispBuf),1, 4);
      if ( WaitKeyPress() != '1' )
            return -1;
      
   }


   /*----------------------------------------------------
    * 		Verify PSC(programmable secret code)
    * --------------------------------------------------*/
   
   SLE4428_PSC_VERIFICATION(ucCmdData, pscCode, ulCmdLen);
   iStatus = Transmit_APDU(1, ucCmdData, ulCmdLen, ucRespData, &ulRespLen);
   
   if( (iStatus & 0xFF) == ESC_PSC_FAILED )               // PSC Verification Failed?
   {
      ErrCount++;
      SLE4428_READ_UNPROTECTED(ucCmdData, 0x03, 0xFD, 0x01, ulCmdLen);
      iStatus = Transmit_APDU(1, ucCmdData, ulCmdLen, ucRespData, &ulRespLen);
      
      if ( iStatus != CARDSLOT_SUCCESS)                       // read error counter failed
      {
         sprintf( (char *)dispBuf, "PSCRdErCnt %X", iStatus );
         write_at( (char *)dispBuf, strlen((char *)dispBuf),1, 3);
         ErrCount++;
         return -1;
      }

      sprintf( (char *)dispBuf, "PSCFail ErrCnt=%X", ucRespData[0] );
      write_at( (char *)dispBuf, strlen((char *)dispBuf),1, 3);
      WaitEnterPress();
      return -1;
   }
   else if( (iStatus & 0xFF) == ESC_CARD_LOCKED )          // card locked?
   {
      ErrCount++;
      sprintf( (char *)dispBuf, "Card Locked" );
      write_at( (char *)dispBuf, strlen((char *)dispBuf),1, 3);
      WaitEnterPress();
      return -1;
   }
   else if(iStatus != CARDSLOT_SUCCESS)                       // other error
   {
      ErrCount++;
      sprintf( (char *)dispBuf, "VerPSCFail %X", iStatus );
      write_at( (char *)dispBuf, strlen((char *)dispBuf),1, 3);
      WaitEnterPress();
      return -1;
   }

   return 0;                                    // good PSC

}

/*
;|=========================================================================+
;| FUNCTION   |void ReportError( char * msg, int status )
;|            |
;|            |Show error message plus SC Driver related codes starting on
;|            |on lines 3 and 4.  Message passed in should be short only to
;|            |identify code location where error occurred.
;|=========================================================================+
*/
static void ReportError(char *msg, int status)
{
   unsigned int tagValue;

   ErrCount++;
   Get_Capability(CUSTOMER_CARD, Tag_Error_Management, (BYTE *)&tagValue);
   sprintf( (char *)dispBuf, "%s %X", msg, status );
   write_at( (char *)dispBuf, strlen((char *)dispBuf) , 1, 3);
   dbprintf( "   %s", dispBuf );
  
}

static void ReportTitle(char *msg)
{
   sprintf( (char *)dispBuf, "%s", msg );
   write_at( (char *)dispBuf, strlen((char *)dispBuf) ,1, 2);
   dbprintf( "   %s\n", dispBuf );
}

/*
;|=========================================================================+
;| FUNCTION  WaitKeyPress
;|=========================================================================+
*/
unsigned char WaitKeyPress( void )
{
   int   length;
   unsigned char buffer[20];
   long  event;

   length = sizeof(buffer);

   do {
     event = read_event();
   }
   while(( event & EVT_KBD ) != EVT_KBD);

   length = read( hScreen,(char *) buffer, length );
   return(buffer[length-1]);
}


/*
;|=========================================================================+
;| FUNCTION  PollFunctionKeys
;|=========================================================================+
*/
unsigned char PollFunctionKeys( void )
{
   int   length;
   unsigned char buffer[20];
   length = sizeof(buffer);
   buffer[0] = 0;
   do
   {
      if ( (peek_event() & EVT_KBD) !=0 )
      {
         read_event();
         read( hScreen,(char *) buffer, length );
      }
   } while ( (buffer[0] < KEY_F1) || (buffer[0] > KEY_F4) );      // wait for Fkey

   return(buffer[0]);
}


/*
;|=========================================================================+
;| FUNCTION:  WaitEnterPress
;|=========================================================================+
*/
unsigned char WaitEnterPress( void )
{
 unsigned char keyValue;

   write_at ("Enter to Continue ",strlen("Enter to Continue "), 1, 7);

   do
   {
      keyValue = WaitKeyPress();
   } while ( keyValue != KEY_ENTER );

   return keyValue;
}


/*
;|=========================================================================+
;| FUNCTION   BlankTest
;|=========================================================================+
*/
void BlankTest( void )
{
   write_at ("   BLANK TEST   ",strlen("   BLANK TEST   "), 1, 2);
   //WaitEnterPress();                            // wait for key for time to download smart card app
}


/*
;|===========================================================================+
;| FUNCTION  fail_msg
;|------------+--------------------------------------------------------------+
;| DESCRIPTION|
;|            |
;|===========================================================================+
*/
void fail_msg( void )
{
   //TestClearDisplay();
   write_at ("     TEST FAILED   ", strlen("     TEST FAILED   "), 1, 2);
   //WaitEnterPress();
}


/*
;|===========================================================================+
;| FUNCTION   pass_msg
;|------------+--------------------------------------------------------------+
;| DESCRIPTION|
;|            |
;|===========================================================================+
*/
void pass_msg( void )
{
   //TestClearDisplay();
   write_at ("     TEST PASSED  ", strlen("     TEST PASSED  "), 1, 2);
   //WaitEnterPress();
}

/*
;|===========================================================================+
;| 		 END   END   END   END     END    END   END   END  END  END
;|===========================================================================+
*/


void TestClearDisplay ()
{
	clrscr();
}
